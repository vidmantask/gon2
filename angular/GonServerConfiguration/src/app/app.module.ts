import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { persistState, enableAkitaProdMode } from '@datorama/akita';

import { AppRoutingModule } from './app-routing.module';
import { DevExtremeModule } from './dev-extreme.module';


import { AppComponent } from './app.component';

if (environment.production) {
  enableProdMode();
  enableAkitaProdMode();
}

persistState({
  storage: sessionStorage,
  key: 'akitaStorage',
  include: ['tokenStore'] //Probably will be changed
});

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DevExtremeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
