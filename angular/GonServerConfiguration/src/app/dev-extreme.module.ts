import { NgModule } from '@angular/core';
import { DxButtonModule, DxFormModule, DxBoxModule } from 'devextreme-angular';

@NgModule({
  imports: [],
  exports: [DxFormModule,
    DxButtonModule,
    DxBoxModule],
})
export class DevExtremeModule { }
