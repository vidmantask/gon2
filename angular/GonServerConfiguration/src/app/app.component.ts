import { Component } from '@angular/core';
import { loadMessages, locale } from 'devextreme/localization';

declare var require: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  private dictionary = require('src/assets/localization/properties.json');

  constructor() {
    this.initMessages();
    locale('en');
  }

  initMessages(): void {
    loadMessages(this.dictionary);
  }


}
