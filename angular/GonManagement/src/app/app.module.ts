import { BrowserModule } from '@angular/platform-browser';
import { NgModule, enableProdMode } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { persistState, enableAkitaProdMode } from '@datorama/akita';

import { AppRoutingModule } from './app-routing.module';
import { DevExtremeModule } from './dev-extreme.module';

import { BackEndUrlInterceptor } from './core/interceptors/backend-url.interceptor';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';

if (environment.production) {
  enableProdMode();
  enableAkitaProdMode();
}

persistState({
  storage: sessionStorage,
  key: 'akitaStorage',
  include: ['tokenStore']
});

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DevExtremeModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BackEndUrlInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
