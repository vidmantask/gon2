export interface SessionConfig {
  sessionId: string;
  rc: boolean;
}
