import { CountTypeLicense } from './count-type-license.model';

export interface LicenseInfo {
  licenseExpires: string;
  licensedTo: string;
  licenseFileNumber: string;
  countTypeLicense: CountTypeLicense[];
  licenseNumber: string;
  updateLicenseAllowed: boolean;
  maintenanceExpires: string;
  licenseStatusOk: boolean;
  licenseFileContent: string;
}
