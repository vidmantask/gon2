import { LicenseInfo } from '../model/license-info.model';

export interface License {
  licenseInfo: LicenseInfo;
}
