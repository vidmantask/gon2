export interface CountTypeLicense {
  licensedItems: number;
  itemName: string;
  actualItems: number;
}
