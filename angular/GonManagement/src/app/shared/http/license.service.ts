import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { License } from '../model/license.model';

@Injectable({ providedIn: 'root' })
export class LicenseService {

  constructor(private http: HttpClient) { }

  getLicenseInfo(): Observable<License> {
    return this.http.post<License>('/admin/getLicenseInfo', {}).pipe(
      catchError((error) => {
        return throwError(error);
      }));
  }

}
