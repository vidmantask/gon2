import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { catchError, shareReplay, timeout } from 'rxjs/operators';

import { CustomOperators } from '../../core/services/custom-operator.service';

import { SessionConfig } from '../model/session-config.model';
import { Login } from '../model/login.model';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private http: HttpClient, private customOperators: CustomOperators) { }

  handleOpenSession(delayMs: number): Observable<SessionConfig> {
    return this.http.post<SessionConfig>('/admin/openSession', {}).pipe(
      this.customOperators.delayedRetry(delayMs),
      catchError((error) => {
        return throwError(error);
      }),
      shareReplay());
  }

  handleLogin(loginRequest: Login): Observable<void> {
    return this.http.post<void>('/admin/login', loginRequest).pipe(
      catchError((error) => {
        return throwError(error);
      }));
  }

  handlePing(): Observable<SessionConfig> {
    return this.http.post<SessionConfig>('/admin/ping', null).pipe(
      catchError((error) => {
        return throwError(error);
      })
    );
  }

  handleCloseSession(): Observable<SessionConfig> {
    return this.http.post<SessionConfig>('/admin/closeSession', {}).pipe(
      catchError((error) => {
        return throwError(error);
      }));
  }


}
