import { NgModule } from '@angular/core';
import { DxButtonModule, DxFormModule, DxBoxModule, DxCheckBoxModule } from 'devextreme-angular';

@NgModule({
  imports: [],
  exports: [DxFormModule,
    DxButtonModule,
    DxBoxModule,
    DxCheckBoxModule],
})
export class DevExtremeModule { }
