import { Injectable } from '@angular/core';
import { Query, toBoolean } from '@datorama/akita';

import { Observable } from 'rxjs';

import { Token } from '../model/token.model';
import { TokenStore } from '../store/token.store';


@Injectable({ providedIn: 'root' })
export class TokenQuery extends Query<Token> {
  token: Observable<Token> = this.select();

  constructor(protected tokenStore: TokenStore) {
    super(tokenStore);
  }

  isLoggedIn(): boolean {
    return toBoolean(this.getValue().token && this.getValue().expires && this.getValue().expires > new Date().getTime());
  }
}
