import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';
import { createInitialToken, Token } from '../model/token.model';

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'tokenStore', resettable: true })
export class TokenStore extends Store<Token> {
  constructor() {
    super(createInitialToken());
  }

  login(token: Token): void {
    this.update(token);
  }

  logout(): void {
    this.reset();
  }
}
