
import { Injectable } from '@angular/core';
import { formatMessage } from 'devextreme/localization';
import notify from 'devextreme/ui/notify';

import { DEFAULT_MSG_SHOW_DELAY } from 'src/app/shared/constants/constants';

@Injectable({ providedIn: 'root' })
export class MessageUtils {

  showSuccessfulMessage(message: string): void {
    notify({
      message: formatMessage(message, ''),
      position: {
        my: 'center top',
        at: 'center top'
      }
    }, 'success', 5000);
  }

  showErrorMessage(message: string, delay = DEFAULT_MSG_SHOW_DELAY): void {
    notify({
      message: formatMessage(message, ''),
      position: {
        my: 'center top',
        at: 'center top'
      }
    }, 'error', delay);
  }

  showErrorMessageNoFormat(formattedMessage: string, delay = DEFAULT_MSG_SHOW_DELAY): void {
    notify({
      message: formattedMessage,
      position: {
        my: 'center top',
        at: 'center top'
      }
    }, 'error', delay);
  }

  showWarningMessage(message: string): void {
    notify({
      message: formatMessage(message, ''),
      position: {
        my: 'center top',
        at: 'center top'
      }
    }, 'warning', 5000);
  }

  showInfoMessage(message: string): void {
    notify({
      message: formatMessage(message, ''),
      position: {
        my: 'center top',
        at: 'center top'
      }
    }, 'info', 5000);
  }
}
