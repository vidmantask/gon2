import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { retryWhen, delay, mergeMap } from 'rxjs/operators';

import { DEFAULT_MAX_RETRIES } from '../../shared/constants/constants';



@Injectable({ providedIn: 'root' })
export class CustomOperators {

  delayedRetry(delayMs: number, maxRetry = DEFAULT_MAX_RETRIES) {
    let retries = maxRetry;

    return (src: Observable<any>) => src.pipe(
      retryWhen((errors: Observable<any>) => errors.pipe(
        delay(delayMs),
        mergeMap(error => retries-- > 0 ? of(error) : throwError('Max retries reached'))
      ))
    );
  }

}
