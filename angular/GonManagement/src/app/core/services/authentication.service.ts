import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';

import { TokenQuery } from '../store/token.query';
import { TokenStore } from '../store/token.store';

import { Token } from '../model/token.model';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  tokenValueChanged: Subject<boolean> = new Subject();

  constructor(private tokenStore: TokenStore, private tokenQuery: TokenQuery, private router: Router) { }

  login(token: Token): void {
    this.tokenStore.login(token);
    this.tokenValueChanged.next(true);
  }

  isLoggedIn(): boolean {
    return this.tokenQuery.isLoggedIn();
  }

  getToken(): string {
    return this.tokenQuery.getValue().token;
  }

  logout(): void {
    this.tokenStore.logout();
    this.tokenValueChanged.next(true);
    this.router.navigateByUrl('');
  }
}

