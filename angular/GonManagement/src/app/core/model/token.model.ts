export class Token {
  token: string;
  expires: number;
}

export function createInitialToken(): Token {
  return {
    token: null,
    expires: null,
  };
}
