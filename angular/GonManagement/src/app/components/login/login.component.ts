import { Component, OnInit } from '@angular/core';
import { formatMessage } from 'devextreme/localization';

import * as jwt_decode from 'jwt-decode';
import * as sha512 from 'js-sha512';

import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { UserService } from 'src/app/shared/http/user.service';
import { LicenseService } from 'src/app/shared/http/license.service';
import { MessageUtils } from '../../core/services/message-util.service';

import { DELAY_BETWEEN_RETRIES_MS } from 'src/app/shared/constants/constants';

import { LoginForm } from '../login/login-form.model';
import { SessionConfig } from 'src/app/shared/model/session-config.model';
import { Token } from 'src/app/core/model/token.model';
import { Login } from 'src/app/shared/model/login.model';
import { LicenseInfo } from 'src/app/shared/model/license-info.model';
import { License } from 'src/app/shared/model/license.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  formatMessage = formatMessage;
  loginForm: LoginForm = new LoginForm();
  isSessionOpen = false;

  constructor(private authService: AuthenticationService,
    private usersService: UserService,
    private messageService: MessageUtils,
    private licenseService: LicenseService) {
  }

  ngOnInit(): void {
    this.authService.logout();

    this.usersService.handleOpenSession(DELAY_BETWEEN_RETRIES_MS).subscribe(
      (response: SessionConfig) => {
        const jwtToken = jwt_decode(response.sessionId);
        const token: Token = new Token();
        token.token = response.sessionId;
        token.expires = (jwtToken.exp * 1000);

        this.authService.login(token);

        setTimeout(() => {
          this.isSessionOpen = true;
        }, 5000);
      },
      () => {
        this.messageService.showErrorMessage('err_cant_reach_server', 300000);
      }
    );
  }

  onFormSubmit(): void {
    //sha512.sha512('Message to hash') //TODO hash password before sending

    const login: Login = {
      username: this.loginForm.username,
      password: this.loginForm.password
    };

    this.usersService.handleLogin(login).subscribe(
      () => {
        this.licenseService.getLicenseInfo().subscribe(
          (license: License) => {
            //TODO check if
          }
        );
      },
      (errorEvent: ErrorEvent) => {
        if (errorEvent.error.errorMsg !== undefined || errorEvent.error.errorMsg !== '') {
          this.messageService.showErrorMessageNoFormat(errorEvent.error.errorMsg);
        } else {
          this.messageService.showErrorMessage('err_connecting_to_server');
        }
      }
    );
  }

  test(): void {
    this.usersService.handleCloseSession().subscribe(
      (response: SessionConfig) => {
        this.authService.logout();
      }
    );
  }


}
