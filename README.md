gon

# # # # # # # # # # # # # # # # # # # # # # # # #
# Tools needed
# # # # # # # # # # # # # # # # # # # # # # # # #

Windows
* Visual Studio 2008 Professional SP1
* mercurial(hg), 32bit
* Python 2.7, 32bit
* Python 2.7 setup_tools
* cmake, 32bit
* Internet Explorer (for shdocvw.dll)


* eclipse_3.6.2_gon.tar.gz
* java (jre7)


Linux:

Mac:


# # # # # # # # # # # # # # # # # # # # # # # # #
# Install python packages
# # # # # # # # # # # # # # # # # # # # # # # # #
python gsetup.py --cmd_install_py_modules



# # # # # # # # # # # # # # # # # # # # # # # # #
# Build G/On Release
# # # # # # # # # # # # # # # # # # # # # # # # #

# Run on Mac, and Linux
python gsetup.py --cmd_devbotnet slave

# Run on windows
python gsetup.py --cmd_devbotnet master --devbotnet_slave_mac <ip-and-port-of-mac> --devbotnet_slave_linux <ip-and-port-of-linux>

example:
gsetup.py --cmd_devbotnet master --devbotnet_slave_mac 172.16.30.4:6090 --devbotnet_slave_linux 172.16.30.52:6090


# # # # # # # # # # # # # # # # # # # # # # # # #
# Build and Run unittest  
# # # # # # # # # # # # # # # # # # # # # # # # #

python gsetup.py --cmd_build_cpp --skip_clean
python gsetup.py --cmd_build --skip_clean


# Run py-unittest with coverage
python gsetup.py --cmd_unittest py --unittest_efilter .*gui.*

# Run py-unittest with coverage, only gui tests
python gsetup.py --cmd_unittest py --unittest_ifilter .*gui.*

# Run py-unittest with coverage, no  gui tests
python gsetup.py --cmd_unittest py --unittest_efilter .*gui.*

# Run cpp-unittest
python gsetup.py --cmd_unittest cpp



# # # # # # # # # # # # # # # # # # # # # # # # #
# Setup Notarization of mac app-bundle  
# # # # # # # # # # # # # # # # # # # # # # # # #

# Store username/password of the account holder for the Apple Development Account in key chain item "AC_PASSWORD"
xcrun altool --store-password-in-keychain-item "AC_PASSWORD" \
               -u "bsh@solitonsystems.com" \
               -p <secret_password>

-----------------------------
# Notarize existing .app file
# Instructions from https://developer.apple.com/documentation/xcode/notarizing_your_app_before_distribution/customizing_the_notarization_workflow

APP_PATH="/Users/bostighansen/Downloads/gon_client.app"
ZIP_PATH="/Users/bostighansen/Downloads/gon_client.zip"

# Create a ZIP archive suitable for altool.
/usr/bin/ditto -c -k --keepParent "$APP_PATH" "$ZIP_PATH"

# Make sure to run the right version of xcode (at least version 10)
sudo xcode-select -s /Applications/Xcode.app

xcrun altool --notarize-app \
             --primary-bundle-id "com.giritech.gonclient.zip" \
             --username "bsh@solitonsystems.com" \
             --password "@keychain:AC_PASSWORD" \
             --file $ZIP_PATH
# Expected result from this command is something like this:
#    No errors uploading '/Users/bostighansen/Downloads/gon_client.zip'.
#    RequestUUID = 688510ae-bd22-4885-9278-e692a843534c

# Note: the --primary-bundle-id can be anything that you choose for identifying the notarization request - but it is not allowed to contain underscores
# See https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundleidentifier
