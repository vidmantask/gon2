"""
This module builds 3rd part C++ libs on our supporte platforms.
"""
import sys
import os
import os.path
import optparse
import subprocess
import shutil
import glob
import re
import ConfigParser
import tarfile
import zipfile

MAC_OSX_SDK = '10.13'

ROOT = os.path.abspath(os.path.dirname(__file__))
ROOT_SOURCE = os.path.join(ROOT, 'src')

ROOT_BUILD = os.path.join(ROOT, '..', 'gon_build', 'build')
ROOT_INSTALL = os.path.join(ROOT, '..', 'gon_build', 'dist')

TARGET_PLATFORM_MAC    = "mac_64"
TARGET_PLATFORM_LINUX  = "linux_64"
TARGET_PLATFORM_WIN    = "win_32"

def detect_platform():
    if sys.platform == "win32":
        return TARGET_PLATFORM_WIN
    elif sys.platform in [ "linux2", "linux" ]:
        return TARGET_PLATFORM_LINUX
    elif sys.platform == "darwin":
        return TARGET_PLATFORM_MAC
    return "unknown"


class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--list',  action='store_true', help='List all available libs')
        self._parser.add_option('--build', action='store_true', help='Build all libs')
        self._parser.add_option('--filter',  type='string', default='.*', help='Regular expression for lib selection, for --list and --build')

        (self._options, self._args) = self._parser.parse_args()

    def cmd_list(self):
        return self._options.list

    def cmd_clean(self):
        return self._options.clean

    def cmd_build(self):
        return self._options.build

    def option_filter(self):
        return self._options.filter


class LibBuilder(object):
    def __init__(self, platform, name):
        self.name = name
        self.src_root = os.path.join(ROOT_SOURCE, name)
        self.platform = platform

    def get_build_root(self):
        return os.path.join(ROOT_BUILD, self.platform)

    def get_build_folder(self):
        return os.path.join(self.get_build_root(), self.name)

    def get_install_root(self):
        return os.path.join(ROOT_INSTALL, self.platform)

    def get_install_root_lib(self):
        return os.path.join(self.get_install_root(), 'lib')

    def get_install_root_bin(self):
        return os.path.join(self.get_install_root(), 'bin')

    def get_install_root_include(self):
        return os.path.join(self.get_install_root(), 'include')

    def copy_file_to_lib(self, filename, dest_root_sufix=None, remove_after_copy=False, from_root=None):
        dest_root = self.get_install_root_lib()
        if dest_root_sufix is not None:
            dest_root = os.path.join(self.get_root(), dest_root_sufix)
        if not os.path.exists(dest_root):
            os.makedirs(dest_root)
        print "COPY file %s" % filename

        src_root = from_root
        if src_root is None:
            src_root = self.get_build_folder()
        shutil.copy(os.path.join(src_root, filename), dest_root)
        if remove_after_copy:
            os.unlink(os.path.join(src_root, filename))

    def copy_file_to_bin(self, filename, dest_root_sufix=None):
        dest_root = self.get_root_bin()
        if dest_root_sufix is not None:
            dest_root = os.path.join(dest_root, dest_root_sufix)
        if not os.path.exists(dest_root):
            os.makedirs(dest_root)
        print "COPY file %s" % filename
        shutil.copy(os.path.join(self.src_root, filename), dest_root)

    def copy_file_to_include(self, filename, include_folder=None, from_root=None):
        if include_folder is None:
            dest_root = self.get_install_root_include()
        else:
            dest_root = os.path.join(self.get_install_root_include(), include_folder)
        if not os.path.exists(dest_root):
            os.makedirs(dest_root)
        print "COPY file %s" % filename

        src_root = from_root
        if src_root is None:
            src_root = self.get_build_folder()
        shutil.copy(os.path.join(src_root, filename), dest_root)

    def copy_h_files_to_include(self, src_include_folder_rel=None, dest_include_folder_rel=None, from_root=None):
        src_include_folder = from_root
        if src_include_folder is None:
            src_include_folder = os.path.join(self.src_root)

        if src_include_folder_rel is not None:
            src_include_folder = os.path.join(src_include_folder, src_include_folder_rel)
        if dest_include_folder_rel is None:
            dest_root = self.get_install_root_include()
        else:
            dest_root = os.path.join(self.get_install_root_include(), dest_include_folder_rel)
        if not os.path.exists(dest_root):
            os.makedirs(dest_root)
        for h_filename in glob.glob(os.path.join(src_include_folder, '*.h')):
            print "COPY file %s" % h_filename
            shutil.copy(h_filename, dest_root)

    def extract_all(self, archive_filename, dest_root):
        print "extract_all", archive_filename, dest_root
        extension = os.path.splitext(archive_filename)[1]
        if extension in ['.tar', '.bz2', '.gz', '.tgz']:
            archive_file = tarfile.open(archive_filename)
            archive_file.errorlevel = 0
            archive_file.extractall(dest_root)
        elif extension in ['.zip']:
            archive_file = zipfile.ZipFile(archive_filename)
            archive_file.extractall(dest_root)
        else:
            print ("ERROR, extension %s not known to extract_all" % extension)

    def build(self):
        print "---------------------------------------------------------------"
        print "BUILDING %s" % self.name
        print "---------------------------------------------------------------"
        getattr(self, "build_%s" % self.platform)()

    def build_linux_64(self):
        print 'Not supported on this platform'

    def build_mac_64(self):
        print 'Not supported on this platform'

    def build_win_64(self):
        print 'Not supported on this platform'

    def build_win_32(self):
        print 'Not supported on this platform'

    def install(self):
        pass

    def clean(self):
        print 'Not supported on this platform'

    def exec_command(self, command, env=None, check_rc=True, shell=True, cwd=None, ignore_error=False):
        if cwd is None:
            cwd = self.src_root
        print cwd
        if shell:
            command = " ".join(command)
        print command
        rc =  subprocess.call(command, env=env, cwd=cwd, shell=shell)
        if check_rc and rc:
            print "ERROR " * 15
            print "ERROR " * 15
            print 'Execution of command failed %r' % command
            print "ERROR " * 15
            print "ERROR " * 15
            if not ignore_error:
                exit(1)

class LibBuilderZLib(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'zlib-1.2.11')

    def extract(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

    def build_linux_64(self):
        self.extract();
        command_env = os.environ
        command = ['./configure', '--static', '--prefix=%s' % self.get_install_root() ]
        self.exec_command(command, env=command_env, shell=True, cwd=self.get_build_folder())
        command = ['make', 'install']
        self.exec_command(command, shell=False, cwd=self.get_build_folder())

    def build_mac_64(self):
        self.extract();
        command_env = os.environ
        command_env['MACOSX_DEPLOYMENT_TARGET'] = MAC_OSX_SDK
        command = ['./configure', '--static', '--prefix=%s' % self.get_install_root()]
        self.exec_command(command, env=command_env, shell=True, cwd=self.get_build_folder())
        command = ['make', 'install']
        self.exec_command(command, env=command_env, shell=True, cwd=self.get_build_folder())

    def build_win_32(self):
        self.extract();

        cwd = os.path.join(self.get_build_root(), self.name)

        command = ['cmake', '-G', '"Visual Studio 16 2019"', '.']
        self.exec_command(command, cwd=cwd, shell=True)

        command = ['devenv', 'zlib.sln', '/Rebuild', 'Release', '/project', 'zlib']
        self.exec_command(command, cwd=cwd, shell=True)

        self.copy_file_to_lib(os.path.join('Release', 'zlib.lib'))
        self.copy_h_files_to_include(from_root=self.get_build_folder(), dest_include_folder_rel='zlib')

class LibBuilderSwissbit(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'swissbit')

    def build_linux_64(self):
        self.copy_file_to_lib(os.path.join('linux', 'static_x64', 'libSfcCardReader.a'), from_root=self.src_root)
        self.copy_h_files_to_include(from_root=self.src_root)

    def build_mac_64(self):
        self.copy_file_to_lib(os.path.join('mac', 'static_x64', 'libSfcCardReader.a'), from_root=self.src_root)
        self.copy_h_files_to_include(from_root=self.src_root)

    def build_win_32(self):
        self.copy_file_to_lib(os.path.join('win', 'static_x32', 'SfcCardReader.lib'), from_root=self.src_root)
        self.copy_h_files_to_include(from_root=self.src_root)


class LibBuilderOpenSSL(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'openssl-1.1.1g')

    def build_linux_64(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        command = ['./config', 'shared', '--prefix=%s' % self.get_install_root(), '--openssldir=%s' % self.get_install_root()]
        self.exec_command(command, shell=False, cwd=self.get_build_folder())

        command = ['make', 'install']
        self.exec_command(command, shell=False, cwd=self.get_build_folder())

    def build_mac_64(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        command = ['./Configure', '--prefix=%s' % self.get_install_root(), '--openssldir=%s' % self.get_install_root(), 'darwin64-x86_64-cc', '-mmacosx-version-min=%s' % MAC_OSX_SDK]
        self.exec_command(command, shell=True, cwd=os.path.join(self.get_build_root(), self.name))

        command = ['make', 'install']
        self.exec_command(command, shell=False, cwd=self.get_build_folder())

    def build_win_32(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        cwd = os.path.join(self.get_build_root(), self.name)

        command = ['perl.exe', 'Configure', 'VC-WIN32', '--prefix=%s' % self.get_install_root(), '--openssldir=%s' % self.get_install_root()]
        self.exec_command(command, cwd=cwd, shell=True)

        command = ['perl.exe', 'Configure', 'VC-WIN32', 'no-asm', '--prefix=%s' % self.get_install_root(), '--openssldir=%s' % self.get_install_root()]
        self.exec_command(command, cwd=cwd, shell=True)

        command = ['nmake']
        self.exec_command(command, cwd=cwd, shell=True)

        command = ['nmake', 'install']
        self.exec_command(command, cwd=cwd, shell=True)

        self.copy_file_to_lib(os.path.join(cwd, 'libssl_static.lib'))
        self.copy_file_to_lib(os.path.join(cwd, 'libcrypto_static.lib'))


class LibBuilderBoost(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'boost_1_73_0')
        self.zlib_src = os.path.join(self.get_build_root(), 'zlib-1.2.11')

    def build_linux_64(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        command_env = os.environ
        command_env['NO_BZIP2'] = '1'
        command = ['./bootstrap.sh',
                   '--prefix=%s' % self.get_install_root(),
                   '--includedir=%s' % self.get_install_root_include(),
                  ]
        self.exec_command(command, env=command_env, shell=False, cwd=self.get_build_folder())

        command = ['./b2',
                   'release',
                   'cxxflags=-fpic',
                   'link=static',
                   'threading=multi',
                   '--without-wave',
                   '--without-mpi',
                   '--libdir=%s'       % self.get_install_root_lib(),
                   '--includedir=%s'   % self.get_install_root_include(),
                   '-sZLIB_SOURCE=%s' % self.zlib_src,
                   'install',
                   ]
        self.exec_command(command, env=command_env, shell=False, cwd=self.get_build_folder())


    def build_mac_64(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        command_env = os.environ
        command_env['NO_BZIP2'] = '1'

        command = ['./bootstrap.sh']
        self.exec_command(command, env=command_env, cwd=self.get_build_folder(), shell=False)

        command = ['./b2',
                   'release',
                   'toolset=clang',
                   'cxxflags=-stdlib=libc++',
                   'cxxflags=-ftemplate-depth=256',
                   'cxxflags=-fvisibility=hidden',
                   'cxxflags=-fvisibility-inlines-hidden',
                   'cxxflags=-mmacosx-version-min=%s' % MAC_OSX_SDK,
                   'cflags=-mmacosx-version-min=%s' % MAC_OSX_SDK,
                   'linkflags=-stdlib=libc++',
                   'link=static',
                   'threading=multi',
                   '--without-wave',
                   '--without-mpi',
                   '--libdir=%s'      % self.get_install_root_lib(),
                   '--includedir=%s'  % self.get_install_root_include(),
                   '-sZLIB_SOURCE=%s' % self.zlib_src,
                   'install'
                   ]
        self.exec_command(command, env=command_env, cwd=self.get_build_folder(), shell=False)


    def build_win_32(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_root())

        command_env = os.environ
        command_env['NO_BZIP2'] = '1'

        cwd = os.path.join(self.get_build_root(), self.name)

        command = ['bootstrap.bat']
        self.exec_command(command, env=command_env, cwd=cwd, shell=True)

        command = ['b2.exe',
                   'release',
                   'link=static',
                   'threading=multi',
				   'address-model=32',
                   '--layout=tagged',
                   '--without-wave',
                   '--without-mpi',
                   '--libdir=%s'      % self.get_install_root_lib(),
                   '--includedir=%s'  % self.get_install_root_include(),
                   '-sZLIB_SOURCE=%s' % self.zlib_src,
                   'install'
                   ]
        self.exec_command(command, env=command_env, cwd=cwd, shell=True)


class LibBuilderCryptoPP(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'cryptopp820')

    def build_linux_64(self):
        tar_filename = os.path.join(self.src_root, '%s.zip' % self.name)
        self.extract_all(tar_filename, self.get_build_folder())

        command = ['make', 'libcryptopp.a', 'cryptest.exe']
        self.exec_command(command, shell=False, cwd=self.get_build_folder())

        command_env = os.environ
        command_env['PREFIX'] = self.get_install_root()
        command = ['make', 'install' ]
        self.exec_command(command, env=command_env, shell=False, cwd=self.get_build_folder())


    def build_mac_64(self):
        tar_filename = os.path.join(self.src_root, '%s.zip' % self.name)
        self.extract_all(tar_filename, self.get_build_folder())

        command_env = os.environ
        command_env['MACOSX_DEPLOYMENT_TARGET'] = MAC_OSX_SDK
        command_env['PREFIX'] = self.get_install_root()
        command = ['make', 'libcryptopp.a', 'cryptest.exe']
        self.exec_command(command, env=command_env, shell=False,  cwd=self.get_build_folder())

        command = ['make', 'install' ]
        self.exec_command(command, env=command_env, shell=False, cwd=self.get_build_folder())


    def build_win_32(self):
        tar_filename = os.path.join(self.src_root, '%s.zip' % self.name)
        self.extract_all(tar_filename, self.get_build_folder())

        cwd = os.path.join(self.get_build_root(), self.name)
        shutil.copy(os.path.join(self.src_root, "cryptlib.vcxproj"), cwd)

        command = ['devenv', 'cryptest.sln', '/Rebuild', 'Release', '/project', 'cryptlib']
        self.exec_command(command, cwd=cwd, shell=True)

        self.copy_file_to_lib(os.path.join('Win32', 'Output', 'Release', 'cryptlib.lib'))
        self.copy_h_files_to_include(from_root=self.get_build_folder(), dest_include_folder_rel='cryptopp')


class LibBuilderTinyXML(LibBuilder):
    def __init__(self, platform):
        LibBuilder.__init__(self, platform, 'tinyxml2-8.0.0')

    def extract(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_folder())

    def build_linux_64(self):
        self.extract();

        command = ['make', 'libtinyxml2.a']
        self.exec_command(command, shell=False, cwd=os.path.join(self.get_build_folder(), 'tinyxml2'))

        self.copy_file_to_lib(os.path.join('tinyxml2','libtinyxml2.a'))
        self.copy_file_to_include(os.path.join('tinyxml2','tinyxml2.h'))

    def build_mac_64(self):
        self.extract();

        command_env = os.environ
        command_env['MACOSX_DEPLOYMENT_TARGET'] = MAC_OSX_SDK
        command = ['make', 'libtinyxml2.a']
        self.exec_command(command, env=command_env, shell=True, cwd=os.path.join(self.get_build_folder(), 'tinyxml2'))

        self.copy_file_to_lib(os.path.join('tinyxml2','libtinyxml2.a'))
        self.copy_file_to_include(os.path.join('tinyxml2','tinyxml2.h'))


    def build_win_32(self):
        tar_filename = os.path.join(self.src_root, '%s.tar.gz' % self.name)
        self.extract_all(tar_filename, self.get_build_folder())

        cwd = os.path.join(self.get_build_root(), self.name, self.name, 'tinyxml2')

        for file in glob.glob(os.path.join(os.path.join(self.src_root, 'win'), '*.*')):
            print "COPY file %s" % file
            shutil.copy(file, cwd)

        command = ['devenv', 'tinyxml2.sln', '/Rebuild', 'Release-Lib', '/project', 'tinyxml2']
        self.exec_command(command, cwd=cwd, shell=True)

        self.copy_file_to_lib(os.path.join(cwd, 'Release-Lib', 'tinyxml2.lib'))
        self.copy_h_files_to_include(from_root=os.path.join(self.get_build_folder(), self.name))


# ----------------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------------------------------------------
def cmd_list(cpp_libs, filter):
    filter_re = re.compile(filter)
    for cpp_lib in cpp_libs:
        if filter_re.match(cpp_lib.name):
            print cpp_lib.name

def cmd_build(cpp_libs, filter):

    if not os.path.exists(ROOT_INSTALL):
        os.makedirs(ROOT_INSTALL)
    if not os.path.exists(ROOT_BUILD):
        os.makedirs(ROOT_BUILD)

    filter_re = re.compile(filter)
    for cpp_lib in cpp_libs:
        if filter_re.match(cpp_lib.name):
            cpp_lib.build()

def cmd_clean(cpp_libs, filter):
    if os.path.isdir(ROOT_INSTALL):
        shutil.rmtree(ROOT_INSTALL)
    for cpp_lib in cpp_libs:
        cpp_lib.clean()


def main():
    commandline_options = CommandlineOptions()

    platform = detect_platform()

    cpp_libs = [
            LibBuilderZLib(platform),
            LibBuilderOpenSSL(platform),
            LibBuilderBoost(platform),
            LibBuilderCryptoPP(platform),
            LibBuilderTinyXML(platform),
            LibBuilderSwissbit(platform)
            ]

    if commandline_options.cmd_list():
        cmd_list(cpp_libs, commandline_options.option_filter())
        return 0

    if commandline_options.cmd_build():
        cmd_build(cpp_libs, commandline_options.option_filter())
        return 0


if __name__ == '__main__':
    main()
