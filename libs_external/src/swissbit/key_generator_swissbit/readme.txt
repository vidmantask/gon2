#
# Building PCSC
#
git clone https://github.com/LudovicRousseau/PCSC.git
cd PCSC
./bootstrap
./configure --enable-usbdropdir=/usr/lib64/pcsc/drivers --enable-confdir=/etc/reader.conf.d --disable-polkit
make


mkdir -p ../root/usr/local/bin
cp src/pcscd ../root/usr/local/bin
chmod +x ../root/usr/local/bin/pcscd


#
# Building docker
#
docker build  -t key_generator_swissbit key_generator_swissbit
docker save key_generator_swissbit > key_generator_swissbit.docker_image.tar
docker load < key_generator_swissbit.docker_image.tar




#
# Create distribution version of key_generator tool
#
rm -rf dist
mkdir -p dist/gon_key_generator_swissbit
docker save key_generator_swissbit > dist/gon_key_generator_swissbit/key_generator_swissbit.docker_image.tar
cp -r key_generator_swissbit_master/* dist/gon_key_generator_swissbit
cd dist
tar cfz gon_key_generator_swissbit.tar.gz gon_key_generator_swissbit/*
cd ..






#
# Running docker
#
docker run  --privileged -t -i key_generator_swissbit python /swissbit/key_generator_swissbit.py --device /dev/sdb1
