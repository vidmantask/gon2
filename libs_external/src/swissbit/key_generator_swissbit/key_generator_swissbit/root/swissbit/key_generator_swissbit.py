import sys
import optparse
import os
import os.path
import ConfigParser
import subprocess
import traceback
import shutil
import stat
import time
import uuid
import glob
import shutil


VERSION = "1.0.0"

SWISSBIT_ROOT = "/"
SWISSBIT_MOUNT = os.path.join(SWISSBIT_ROOT, "mount")
SWISSBIT_APPLET = os.path.join(SWISSBIT_ROOT, 'swissbit', 'applet')

TOKEN_TYPE_SWISSBIT = 'swissbit'
TOKEN_TYPE_SWISSBIT_2 = 'swissbit_2'
TOKEN_TYPE_SWISSBIT_PE = 'swissbit_pe'
TOKEN_TYPES = [TOKEN_TYPE_SWISSBIT, TOKEN_TYPE_SWISSBIT_2, TOKEN_TYPE_SWISSBIT_PE]

TOKEN_TYPES_DATA = {
    TOKEN_TYPE_SWISSBIT: {
        "applet_version": "0002",
        "init_folder": "gon_init_micro_smart_swissbit",
        "dp_enabled": False,
    },
    TOKEN_TYPE_SWISSBIT_2: {
        "applet_version": "0003",
        "init_folder": "gon_init_micro_smart_swissbit_2",
        "dp_enabled": False,
    },
    TOKEN_TYPE_SWISSBIT_PE: {
        "applet_version": "0003",
        "init_folder": "gon_init_micro_smart_swissbit_pe",
        "dp_enabled": True,
    }
}

def eprint(*args, **kwargs):
    sys.stderr.write(*args)

class KeyGenneratorOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--verify_only', action='store_true', help='')
        self._parser.add_option('--device', type='string', default=None, help='')
        self._parser.add_option('--token_type', type='string', default=TOKEN_TYPE_SWISSBIT, help='')
        (self._options, self._args) = self._parser.parse_args()

    def cmd_show_version(self):
        return self._options.version

    def get_verify_only(self):
        return self._options.verify_only

    def get_device(self):
        return self._options.device

    def get_token_type(self):
        return self._options.token_type


def get_swissbit_dp_enabled():
    args = ['/usr/local/bin/cardManagerCLI', '--mountpoint', SWISSBIT_MOUNT, '--status']
    print args
    env = os.environ
    env['LD_LIBRARY_PATH'] = '/usr/local/bin'
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, env=env, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    print "stdout:"
    print stdout
    print "stdout:"
    print stderr
    print ("rc:", process.returncode)
    if process.returncode != 0:
        return False

    if "Unique Card ID" in stdout:
        return True

    return False
#
# generate_key
#
def generate_key(config):
    env = {}
    env['LD_LIBRARY_PATH'] =  '/usr/local/lib64'

    # Verify device
    if not os.path.exists(config.get_device()):
        return (False, "ERROR: Device %s not found" % config.get_device())

    # Verify token_type
    if config.get_token_type() not in TOKEN_TYPES:
        return (False, "ERROR: Invalid token_type %s allowed values are %s" % (config.get_token_type(), TOKEN_TYPES))

    # Mount device
    if not os.path.exists(SWISSBIT_MOUNT):
        os.makedirs(SWISSBIT_MOUNT)
    print "Mounting %s to %s" % (config.get_device(), SWISSBIT_MOUNT)
    args = ['mount', config.get_device(), SWISSBIT_MOUNT, '-t', 'auto']
    print args
    process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    if process.returncode != 0:
        return (False, "ERROR: Unable to mount %s to %s" % (config.get_device(), SWISSBIT_MOUNT))

    # Start pcscd -f -d
    print "Starting pcscd"
    args = ['./pcscd', '-f', '-d']
    pcscd_process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False)
    time.sleep(4)

    # Delete applet
    print "Deleting applet"
    args = ['./gpshell', 'deleteApplet.txt']
    print args
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, env=env)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        print stderr
        print "Warning: Error durring deleteApplet"

    # Install applet
    print "Installing applet"
    args = ['./gpshell', 'installApplet.%s.txt' % TOKEN_TYPES_DATA[config.get_token_type()]['applet_version']]
    print args
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, env=env)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        return (False, "ERROR: Error durring installApplet")

    # Create init-folder
    print "Create init-folder"
    init_folder = os.path.join(SWISSBIT_MOUNT, 'gon_client', TOKEN_TYPES_DATA[config.get_token_type()]['init_folder'])
    if not os.path.exists(init_folder):
        os.makedirs(init_folder)

    args = ['/usr/local/bin/SWISSBIT_Verify', SWISSBIT_MOUNT, config.get_token_type()]
    print args
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        return (False, "ERROR: Error durring verification of applet")

    dp_enabled = get_swissbit_dp_enabled()
    if dp_enabled != TOKEN_TYPES_DATA[config.get_token_type()]['dp_enabled']:
        if dp_enabled:
            return (False, "ERROR: DP is availble on the card, but it should NOT be for the token_type %s" % config.get_token_type())
        else:
            return (False, "ERROR: DP is NOT availble on card, should be for the token_type %s" % config.get_token_type())

    time.sleep(2)

    # Eject device
    print "Eject"
    args = ['eject', SWISSBIT_MOUNT]
    print args
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE, env=env)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        print "Warning: Error durring eject"

    print "Done"
    return (True, 'ok')

#
# verify_key
#
def verify_key(config):
    # Verify device
    if not os.path.exists(config.get_device()):
        return (False, "ERROR: Device %s not found" % config.get_device())

    # Verify token_type
    if config.get_token_type() not in TOKEN_TYPES:
        return (False, "ERROR: Invalid token_type %s allowed values are %s" % (config.get_token_type(), TOKEN_TYPES))

    # Mount device
    if not os.path.exists(SWISSBIT_MOUNT):
        os.makedirs(SWISSBIT_MOUNT)
    print "Mounting %s to %s" % (config.get_device(), SWISSBIT_MOUNT)
    args = ['mount', config.get_device(), SWISSBIT_MOUNT, '-t', 'auto']
    print args
    process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        return (False, "ERROR: Unable to mount %s to %s" % (config.get_device(), SWISSBIT_MOUNT))

    args = ['/usr/local/bin/SWISSBIT_Verify', SWISSBIT_MOUNT, config.get_token_type()]
    print args
    process = subprocess.Popen(args=args, cwd=SWISSBIT_APPLET, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    print stdout
    print stderr
    if process.returncode != 0:
        return (False, "ERROR: Error durring verification of applet")

    dp_enabled = get_swissbit_dp_enabled()
    if dp_enabled != TOKEN_TYPES_DATA[config.get_token_type()]['dp_enabled']:
        if dp_enabled:
            return (False, "ERROR: DP is enabled for the token_type %s" % config.get_token_type())
        else:
            return (False, "ERROR: DP is NOT enabled for the token_type %s" % config.get_token_type())

    return (True, 'ok')

#
# Main
#
def main():
    config = KeyGenneratorOptions()

    if config.cmd_show_version():
        print VERSION
        return 0

    if config.get_verify_only():
        (rc, error_message) = verify_key(config)
        if not rc:
            print error_message
            eprint(error_message)
            return 1
        return 0

    (rc, error_message) = generate_key(config)
    if not rc:
        print error_message
        eprint(error_message)
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
