#!/bin/bash 

#
# called before login
#

# This script tests for sudo rights, because the other scripts will need it

# Test if sudo can be run without password prompt, 
# returns 0, if not
# otherwise 1
# works for German and English
function testsudo 
{
	A=$(sudo -n -v 2>&1);test -z "$A" 
	echo $A|grep -q asswor
}

#
# called before logout 
#

testsudo
if [[ $? -ne 1 ]]; then
	printf "\n Info: postLogin.sh, preLogout.sh and postLogout.sh must be run with root rights in case multiple partitions are to be managed!\n"
	exit 0
fi


set -e



exit 0
