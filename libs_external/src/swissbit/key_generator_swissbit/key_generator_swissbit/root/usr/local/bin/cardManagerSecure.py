#!/usr/bin/python
import subprocess
import hashlib
import binascii
import argparse

def getLoginToken(secret):
	# Create SHA256 from secret
	m = hashlib.sha256()
	m.update(secret)
	hashOfSecret = m.digest()        
    
    # Retrieve login challenge
	cmd = ["./cardManagerCLI", "-m", 
					args.m, "-C"  ]
	output = subprocess.check_output(cmd)
	challengeAsStringList = output.split()
	
	challengeAsByteList = bytearray(int(x, 16) for x in challengeAsStringList)

    # Concatenate challenge and hashed secret
	concatData = hashOfSecret + challengeAsByteList   

    # Create SHA256 from concatenated data
	m2 = hashlib.sha256()
	m2.update(concatData)
	allHash =  m2.digest()  
	allHashPin = binascii.hexlify(allHash)
	return allHashPin

def login():
	secret = bytearray(args.pin)    
	token = getLoginToken(secret)    

	cmd = ["./cardManagerCLI", "-m", 
				args.m, "-l", "-H", "-p",  token   ]
	

	output = subprocess.check_output(cmd)
	print(output)

def deactivate():  
	secret = bytearray(args.sopin)    
	token = getLoginToken(secret)    

	cmd = ["./cardManagerCLI", "-m", 
					args.m, "-d", "-H", "--sopin",  token   ]

	output = subprocess.check_output(cmd)
	print(output)


def logout():

	cmd = ["./cardManagerCLI", "-m", 
											args.m, "-q"  ]
	output = subprocess.check_output(cmd)
    # put result in label
	print(output)


def main():
    
    if args.doActivate:
        args.doDeactivate = 0 
        args.doLogin = 0 
        args.doLogout = 0
        if len(args.pin) != 32:
            print "User PIN must be 32 characters for activate"
            args.doActivate = 0
        if len(args.sopin) != 32:
            print "SO PIN must be 32 characters for activate"
            args.doActivate = 0
            
    if args.doDeactivate:
        args.doLogin = 0 
        args.doLogout = 0 

    if args.doLogin:
        args.doLogout = 0 
        
 
    if args.doActivate:
        cmd = ["./cardManagerCLI", "-m", 
                    args.m, "-A", "-p", args.pin, "-o", args.sopin  ]
        output = subprocess.check_output(cmd)
        print(output)
        
    if args.doDeactivate:
        deactivate()
        
    if args.doLogin:
        login()
        
    if args.doLogout:
        logout()
        
        
if __name__ == "__main__":
	global args

	parser = argparse.ArgumentParser(
				description='Manage Swissbit Data Protection Card Secure Login')

	parser.add_argument('-m','-mountpoint', 
			required=True,
			help='Mount point of  Swissbit Data Protection Card')

	parser.add_argument('-p', '--pin',
                    dest='pin',
					default="",
					help='User PIN of  Swissbit Data Protection Card')

	parser.add_argument('-s', '--sopin', 
					default="",
                    dest='sopin',
					help='SO PIN of  Swissbit Data Protection Card')
                    
	parser.add_argument('-a', '--activate', 
					action="store_true",
                    dest='doActivate',
					help='Secure Activate of Swissbit Data Protection Card')

	parser.add_argument('-d', '--deactivate', 
					action="store_true",
					dest='doDeactivate',
					help='Deactivate Swissbit Data Protection Card')

	parser.add_argument('-l', '--login', 
					action="store_true",
					dest='doLogin',
					help='Secure Login to Swissbit Data Protection Card')

	parser.add_argument('-q', '--logout', 
					action="store_true",
					dest='doLogout',
					help='Logout from Swissbit Data Protection Card')
               
                    
	args = parser.parse_args()

	main()
