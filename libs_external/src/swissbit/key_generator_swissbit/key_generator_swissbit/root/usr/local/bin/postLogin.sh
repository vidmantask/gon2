#!/bin/bash 

# Test if sudo can be run without password prompt, 
# returns 0, if not
# otherwise 1
# works for German and English
function testsudo 
{
	A=$(sudo -n -v 2>&1);test -z "$A" 
	echo $A|grep -q asswor
}

#
# called after login , i.e. partitions can be mounted
#

testsudo
if [[ $? -ne 1 ]]; then
	printf "\n Info: $0 must be run with root rights in case multiple partitions are to be managed!\n"
	exit 0
fi

set -e

# gather partiton info about unlocked partions
# sudo will automatically elevate user rights, becuase we checked above
# no prompt will happen here
sudo partprobe || true

exit 0
