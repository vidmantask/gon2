#!/bin/bash 

# Test if sudo can be run without password prompt, 
# returns 0, if not
# otherwise 1
# works for German and English
function testsudo 
{
	A=$(sudo -n -v 2>&1);test -z "$A" 
	echo $A|grep -q asswor
}

#
# called before logout 
#

testsudo
if [[ $? -ne 1 ]]; then
	printf "\n Info: $0 must be run with root rights in case partitions shall be unmounted!\n"
	exit 0
fi

set -e

# clean e.g. file dependencies
sudo umount -l /dev/sdb5 || true
sudo umount -l /dev/sdb6 || true

exit 0
