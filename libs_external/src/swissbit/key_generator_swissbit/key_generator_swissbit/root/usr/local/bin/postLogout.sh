#!/bin/bash 

# Test if sudo can be run without password prompt, 
# returns 0, if not
# otherwise 1
# works for German and English
function testsudo 
{
	A=$(sudo -n -v 2>&1);test -z "$A" 
	echo $A|grep -q asswor
}

#
# called after logout 
#

testsudo
if [[ $? -ne 1 ]]; then
	printf "\n Info: $0 programm must be run with root rights if multiple partitions are to be managed!\n"
	exit 0
fi

set -e

# remove  locked partiitons
sudo partprobe || true

exit 0
