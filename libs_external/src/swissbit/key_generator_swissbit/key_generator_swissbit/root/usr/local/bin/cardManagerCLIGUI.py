#!/usr/bin/python
from Tkinter import *
import Tkinter
import subprocess
import tkMessageBox
# This needs at least Python 3.2
import argparse
from tkFileDialog   import askdirectory 
import tkFont
import tkFont as font

def about():
	tkMessageBox.showinfo("About cardManagerCLIGUI.py",
                          "Version 1.0 Copyright (c) 2017 Swissbit AG ")

def callback_mountpoint():
	global mounpointEntry

	dirname = askdirectory()
	if dirname:
		mounpointEntry['state']   = 'normal'
		mounpointEntry.delete(0, END)
		mounpointEntry.insert(0, dirname)
		mounpointEntry['state']   = 'disabled'
		callback_status()		

def callback_login():
	global outputLabel
	global mounpointEntry
	global pinEntry
	global loginButton

	if loginButton['text'] == "Login":
		cmd = ["./cardManagerCLI", "-m", 
					mounpointEntry.get(), "-l", "-p", pinEntry.get()  ]
	else:
		cmd = ["./cardManagerCLI", "-m", 
											mounpointEntry.get(), "-q"  ]

	output = subprocess.check_output(cmd)
	# TODO: check for errors, if so we do not have to check the status
	callback_status()

def callback_statusEx():
	global outputLabel
	global mountpointButton

	cmd = ["./cardManagerCLI", "-m", 
												mounpointEntry.get(), "-s"  ]
	output = subprocess.check_output(cmd)

	tkMessageBox.showinfo("Status", output)
    # put result in label
	#outputLabel['text'] = output.decode('utf-8')

def callback_status():
	global outputLabel
	global mounpointEntry
	global loginButton
	global mountpointButton

	s = StringVar()

	cmd = ["./cardManagerCLI", "-m", 
												mounpointEntry.get(), "-s"  ]
	output = subprocess.check_output(cmd)

	res = output.split("Current card system status is")

	l = len(res)

	if l == 2:
		# This means the status command went through, block any modifications
		mountpointButton['state'] = 'disabled'
		mounpointEntry['state']   = 'disabled' 
		stripped = res[1].strip()
		if stripped[0] == '0':
			loginButton['state'] = 'disabled'
			outputLabel['text'] = "Card is in transparent mode"
			loginButton['text']
		elif stripped[0] == '1':
			loginButton['state'] = 'normal'
			outputLabel['text'] = "Card is unlocked"
			loginButton['text'] = "Logoff"
		elif stripped[0] == '2':
			loginButton['state'] = 'normal'
			outputLabel['text'] = "Card is locked"
			loginButton['text'] = "Login"
		else:
			loginButton['state'] = 'disabled'
			outputLabel['text'] = "Card is in unknown state"
	else:
		loginButton['state'] = 'disabled'

		s = "No Swisssbit Data Protection Card found at \n" +\
                                                     mounpointEntry.get()

		outputLabel['text'] = s

def callback_logoff():
	global outputLabel
	global mounpointEntry

	cmd = ["./cardManagerCLI", "-m", 
											mounpointEntry.get(), "-q"  ]
	output = subprocess.check_output(cmd)
    # put result in label
	outputLabel['text'] = output.decode('utf-8')


def main():
	global outputLabel
	global mounpointEntry
	global pinEntry
	global sopinEntry
	global newpinEntry
	global readexEntry
	global writeexEntry
	global top
	global args
	global loginButton
	global mountpointButton

	top = Tkinter.Tk()
	top.title("Manage Swissbit Data Protection Card")
	top.geometry("380x120")

	font.nametofont('TkDefaultFont').configure(size=11)
	font.nametofont('TkTextFont').configure(size=11)
	font.nametofont('TkMenuFont').configure(size=11)
	font.nametofont('TkCaptionFont').configure(size=11)
	font.nametofont('TkHeadingFont').configure(size=11)

	# Make the child frames occupy all the space
	top.columnconfigure(0, weight=1)
#	top.rowconfigure(2, weight=1)

	# Code to add widgets will go here...

	# Menus
	menubar = Menu(top)

	appmenu = Menu(menubar, tearoff=0)
	appmenu.add_command(label="About...", command=about)
	appmenu.add_command(label="Quit", command=top.quit)
	menubar.add_cascade(label="cardManagerPY", menu=appmenu)

	statusmenu = Menu(menubar, tearoff=0)
	statusmenu.add_command(label="Extended Status Information",
												 command=callback_statusEx)
	menubar.add_cascade(label="Status", menu=statusmenu)
	top.config(menu=menubar)


	frame1 = Frame(top)
	frame1.rowconfigure(1, weight=1)
	frame1.columnconfigure(1, weight=1)
	frame1.grid(row=0, column=0, sticky=NSEW)


	canvas = Canvas(top)
	canvas.grid(row=2, column=0, sticky=NSEW)
	outputLabel = Label(canvas, justify=LEFT)
	outputLabel.grid(row=0, column=1)

	mountpointButton = Button(frame1, text="...", command=callback_mountpoint)
	mountpointButton.grid(row=0, column=3, sticky=NSEW)
		
	loginButton = Button(frame1, text="Login", command=callback_login)
	loginButton.grid(row=1, column=3, sticky=NSEW)


	#Labels
	mounpointLabel = Label(frame1, text="Mountpoint:")
	mounpointLabel.grid(row=0, column=0,  sticky=E)

	pinLabel = Label(frame1, text="PIN:")
	pinLabel.grid(row=1, column=0, sticky=E)

	#Entries
	mounpointEntry = Entry(frame1)
	mounpointEntry.grid(row=0, column=1,  sticky=NSEW)
	mounpointEntry.insert(0, args.m)
	mounpointEntry['state']   = 'disabled' 

	pinEntry = Entry(frame1)
	pinEntry.grid(row=1, column=1, sticky=NSEW)
	pinEntry.insert(0, args.pin)

	# Initial status to get information on the card
	callback_status()

	top.mainloop()

if __name__ == "__main__":
	global args

	parser = argparse.ArgumentParser(
				description='Manage Swissbit Data Protection Card')

	parser.add_argument('-m','-mountpoint', 
			required=True,
			help='Mount point of  Swissbit Data Protection Card')

	parser.add_argument('-p', '--pin', 
					default="",
					help='User PIN of  Swissbit Data Protection Card')

	args = parser.parse_args()

	main()
