import sys
import optparse
import os
import os.path
import ConfigParser
import subprocess
import traceback
import shutil
import stat
import time
import uuid
import glob
import shutil
import threading
import re
import tempfile
import operator
import json

VERSION = "1.0.0"

VENDORS = [1507]
PRODUCTS = [1846]


DOCKER_IMAGE = "key_generator_swissbit"

TOKEN_TYPE_SWISSBIT = 'swissbit'
TOKEN_TYPE_SWISSBIT_2 = 'swissbit_2'
TOKEN_TYPE_SWISSBIT_PE = 'swissbit_pe'
TOKEN_TYPES = [TOKEN_TYPE_SWISSBIT, TOKEN_TYPE_SWISSBIT_2, TOKEN_TYPE_SWISSBIT_PE]

class KeyGenneratorOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--get_devices', action='store_true', help='List devices found')
        self._parser.add_option('--production', action='store_true', help='Format, install and verify applet on all devices found')
        self._parser.add_option('--install_applet', action='store_true', help='Install applet on all devices found')
        self._parser.add_option('--format', action='store_true', help='Format all devices found')
        self._parser.add_option('--verify_applet', action='store_true', help='Verify applet on all devices found')
        self._parser.add_option('--verify_count', type='int', default=None, help='')
        self._parser.add_option('--learn', type='int', default=None, help='')
        self._parser.add_option('--token_type', type='string', default=TOKEN_TYPE_SWISSBIT, help='Available token_types are %s'%TOKEN_TYPES)
        self._parser.add_option('--no_logo', action='store_true', help='')
        self._parser.add_option('--debug', action='store_true', help='')
        (self._options, self._args) = self._parser.parse_args()

    def cmd_show_version(self):
        return self._options.version

    def cmd_learn(self):
        return self._options.learn

    def cmd_get_devices(self):
        return self._options.get_devices

    def cmd_production(self):
        return self._options.production

    def cmd_install_applet(self):
        return self._options.install_applet

    def cmd_format(self):
        return self._options.format

    def cmd_verify_applet(self):
        return self._options.verify_applet

    def get_verify_count(self):
        return self._options.verify_count

    def get_no_logo(self):
        return self._options.no_logo

    def get_token_type(self):
        return self._options.token_type

    def get_debug(self):
        return self._options.debug


#
# Print Logo
#
def print_logo():
    print "   _____     ______           _____         _         _     _ _   "
    print "  / ____|   / / __ \         / ____|       (_)       | |   (_) |  "
    print " | |  __   / / |  | |_ __   | (_____      ___ ___ ___| |__  _| |_ "
    print " | | |_ | / /| |  | | '_ \   \___ \ \ /\ / / / __/ __| '_ \| | __|"
    print " | |__| |/ / | |__| | | | |  ____) \ V  V /| \__ \__ \ |_) | | |_ "
    print "  \_____/_/   \____/|_| |_| |_____/ \_/\_/ |_|___/___/_.__/|_|\__|"

def print_error(message):
    print "   _____     ______           _____         _         _     _ _          ______                     "
    print "  / ____|   / / __ \         / ____|       (_)       | |   (_) |        |  ____|                    "
    print " | |  __   / / |  | |_ __   | (_____      ___ ___ ___| |__  _| |_ ______| |__   _ __ _ __ ___  _ __ "
    print " | | |_ | / /| |  | | '_ \   \___ \ \ /\ / / / __/ __| '_ \| | __|______|  __| | '__| '__/ _ \| '__|"
    print " | |__| |/ / | |__| | | | |  ____) \ V  V /| \__ \__ \ |_) | | |_       | |____| |  | | | (_) | |   "
    print "  \_____/_/   \____/|_| |_| |_____/ \_/\_/ |_|___/___/_.__/|_|\__|      |______|_|  |_|  \___/|_|   "
    print message
    print ""

def print_ok():
    print "   _____     ______           _____         _         _     _ _           ____  _    "
    print "  / ____|   / / __ \         / ____|       (_)       | |   (_) |         / __ \| |   "
    print " | |  __   / / |  | |_ __   | (_____      ___ ___ ___| |__  _| |_ ______| |  | | | __"
    print " | | |_ | / /| |  | | '_ \   \___ \ \ /\ / / / __/ __| '_ \| | __|______| |  | | |/ /"
    print " | |__| |/ / | |__| | | | |  ____) \ V  V /| \__ \__ \ |_) | | |_       | |__| |   < "
    print "  \_____/_/   \____/|_| |_| |_____/ \_/\_/ |_|___/___/_.__/|_|\__|       \____/|_|\_\ "

#
# GenearateKey
#
class GenerateKey(object):
    def __init__(self, config, device, mount_point=None):
        self.config = config
        self.device = device
        self.mount_point = mount_point
        self._is_running = False
        self.process = None
        self.thread = threading.Thread(target=self._run, args=())
        self.thread.daemon = True
        self._stdout = None
        self._stderr = None
        self._returncode = None
        self.verify_only = False

    def is_running(self):
        return self._is_running

    def _run(self):
        self._is_running = True
        self.unmount()

        args = ['docker', 'run', '--privileged', '--rm=false', DOCKER_IMAGE, 'python', '/swissbit/key_generator_swissbit.py', '--device', self.device, '--token_type', self.config.get_token_type()]
        if self.verify_only:
            args.append('--verify_only')

        process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        self._stdout, self._stderr = process.communicate("")
        self._returncode = process.returncode
        self._is_running = False
#        if self._returncode != 0:
#            print self._stderr
#            out_tempfolder = os.path.abspath('temp')
#            if not os.path.exists(out_tempfolder):
#                os.makedirs(out_tempfolder)
#            out_tempfile = tempfile.NamedTemporaryFile(dir=out_tempfolder,delete=False)
#            out_tempfile.write(self._stderr)
#            out_tempfile.write("-"*120)
#            out_tempfile.write(self._stdout)
#            out_tempfile.close()

    def start(self, verify_only=False):
        self.verify_only = verify_only
        self.thread.start()

    def unmount(self):
        if self.mount_point is not None:
            args = ['umount', self.mount_point]
            process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate("")
            if process.returncode != 0:
                print stderr
                print "ERROR: Unable to umount %s" % (self.mount_point)
                return False
            self.mount_point = None
        return True

    def format(self):
        if self.mount_point is None:
            args = ['mkfs.vfat', self.device]
            process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate("")
            if process.returncode != 0:
                print stderr
                print "ERROR: Unable to format %s" % (self.device)
                return False
            args = ['dosfslabel', self.device, 'G-ON']
            process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate("")
            if process.returncode != 0:
                print stderr
                print "ERROR: Unable to set label %s" % (self.device)
                return False
        return True

    def map_to_usb_port(self):
        dev_disk = '/dev/disk/by-path'
        for element in os.listdir(dev_disk):
            if os.path.islink(os.path.join(dev_disk, element)):
                link_dst = os.path.basename(os.readlink(os.path.join(dev_disk, element)))
                link_dev = os.path.join('/dev', "%s1" %link_dst)
                if link_dev == self.device:
                    return element
        return None

    def learn(self, external_id):
        usb_port = self.map_to_usb_port()
        if usb_port is None:
            print "Unable to map to usb"
            return

        out_folder = os.path.abspath('learn')
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        out_file = open(os.path.join(out_folder, usb_port), 'w')
        out_file.write("%d" % external_id)
        out_file.close()
        print "%d mapped to %s" % (external_id, usb_port)

    def get_external_id(self):
        usb_port = self.map_to_usb_port()
        if usb_port is None:
            return ""

        out_folder = os.path.abspath('learn')
        external_id_filename = os.path.join(out_folder, usb_port)
        if not os.path.exists(external_id_filename):
            return ""

        external_id_file = open(external_id_filename, 'r')
        external_id = int(external_id_file.read())
        external_id_file .close()
        return external_id


    @classmethod
    def one_is_running(cls, devices):
        for device in devices:
            if device.is_running():
                return True
        return False

#
# generate_keys
#
def generate_keys(config, devices, verify_only=False):

    # Verify token_type
    if config.get_token_type() not in TOKEN_TYPES:
        print "ERROR: Invalid token_type %s allowed values are %s" % (config.get_token_type(), TOKEN_TYPES)
        return False

    print("Installing java-applets")
    for device in devices:
        device.start(verify_only=verify_only)
    time.sleep(2)

    sec_count = 0
    while GenerateKey.one_is_running(devices):
        time.sleep(1)
        sec_count += 1
        sys.stdout.write("  Please wait, has been working for %d sec.\r" % sec_count)
        sys.stdout.flush()

    print ""
    print"Installing java-applets, done"
    print ""
    print "Status:"

    sorted_devices = {}
    for device in devices:
        sorted_devices["%03s" % device.get_external_id()] = device
    sorted_devices = sorted(sorted_devices.items(), key=operator.itemgetter(0))

    for external_id, device in sorted_devices:
        print "- "*30
        if device._returncode != 0:
            print "  %s : %-12s ERROR(%d)" % (external_id, device.device, device._returncode)
            print device._stderr
        else:
            print "  %s : %-12s ok" % (external_id, device.device)
        if config.get_debug():
            print device._stdout

    all_ok = True
    for device in devices:
        if device._returncode != 0:
            all_ok = False
            break

    time.sleep(1)

    return all_ok

#
# format_keys
#
def format_keys(config, devices):
    print("Formating keys")
    for device in devices:
        if not device.unmount():
            return False
    time.sleep(1)
    for device in devices:
        if not device.format():
            return False

    print("Formating keys, done")

    return True

#
# get_devices
#
def get_devices(config):
    result = []
    args = ['lsblk', '--output=TRAN,KNAME,MOUNTPOINT,HCTL,MODEL,TYPE,SIZE', '-J']
    process = subprocess.Popen(args=args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    stdout, stderr = process.communicate("")
    if process.returncode == 0:
        devices_json = json.loads(stdout)
        for device_json in devices_json['blockdevices']:
            if device_json['tran'] in ['usb']:
                kname_root = device_json['kname']
                kname = '%s1' % kname_root
                mount_point = None
                for device_json_2 in devices_json['blockdevices']:
                    if kname == device_json_2['kname']:
                        mount_point = device_json_2['mountpoint']
                        result.append( ('/dev/%s' % kname, mount_point))

#        for line in stdout.splitlines():
#            print line
#            line_elements = re.split(r'\s+', line.strip())
#            print line_elements
#            if 'usb' in line_elements:
#                kname = line_elements[0]
#                mount_point = None
#                if len(line_elements) > 3:
#                    mount_point = line_elements[1]
#                result.append( ('/dev/%s' % kname, mount_point))
    return result

#
# Main
#
def main_internal(config):
    devices = get_devices(config)

    if config.cmd_show_version():
        print VERSION
        return 0

    device_generators = []
    for device, mount_point in devices:
        device_generators.append(GenerateKey(config, device, mount_point))

    if config.cmd_learn():
        if len(device_generators) != 1:
            print "Activate only one usb device"
            return -1
        device_generator = device_generators[0]
        device_generator.learn(config.cmd_learn())
        return 0

    if config.cmd_get_devices():
        out_elements = []
        for device, mount_point in devices:
            out_elements.append("  { 'device':'%s', mount_point:'%s' }" % (device, mount_point if mount_point else ""))
        print "["
        print ",\n".join(out_elements)
        print "]"
        return len(devices)

    if config.cmd_production():
        if format_keys(config, device_generators):
            if generate_keys(config, device_generators, verify_only=False):
                return len(devices)
        return -1

    if config.cmd_format():
        if format_keys(config, device_generators):
            return len(devices)
        return -1

    if config.cmd_install_applet():
        if generate_keys(config, device_generators, verify_only=False):
            return len(devices)
        return -1

    if config.cmd_verify_applet():
        if generate_keys(config, device_generators, verify_only=True):
            return len(devices)
        return -1
    return -1

def main():
    config = KeyGenneratorOptions()

    if not config.get_no_logo():
        print_logo()

    rc = main_internal(config)
    if config.get_verify_count() is not None:
        if rc != config.get_verify_count() and rc >= 0:
            print_error("The expected number of token is %d but %d was handled." %(config.get_verify_count(), rc))
            return rc
    if rc < 0:
        print_error("")
    else:
        print_ok();

if __name__ == '__main__':
    sys.exit(main())
