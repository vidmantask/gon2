This folder contains the "Card Reader" driver that links the usb-filesystem to the PCSC subsystem.

Install drivers:
sudo cp -r /home/twa/source/gon/cpp_lib3p/src/swissbit/applet/reader_driver_linux/export/ifd-sfc.bundle /usr/lib64/pcsc/drivers
sudo cp /home/twa/source/gon/cpp_lib3p/src/swissbit/applet/reader_driver_linux/export/reader.conf /etc/reader.conf.d/swissbit.conf


Install globalplatform and gpshell


Run PCSC Deamon in forground:
/usr/sbin/pcscd -f

Start PCSC Deamon:
systemctl start pcscd.service
