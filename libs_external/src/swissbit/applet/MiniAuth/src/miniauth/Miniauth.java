package miniauth;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.OwnerPIN;
import javacard.framework.Util;
import javacard.security.CryptoException;
import javacard.security.ECKey;
import javacard.security.ECPrivateKey;
import javacard.security.ECPublicKey;
import javacard.security.KeyBuilder;
import javacard.security.KeyPair;
import javacard.security.RSAPublicKey;
import javacard.security.Signature;
import javacardx.crypto.Cipher;

/*
 * Version history:
 * 
 * 0001 : Original version from swissbit
 * 0002 : TWA, added generation of key_pair
 * 0003 : Added ECDSA with P521 EC
 * 
 */
public class Miniauth extends Applet {
	
	/** Error code in case of generic/unexpected failure */
	private static final short SW_GENERIC = ISO7816.SW_UNKNOWN;

	public final static short SW_ILLEGAL_VALUE      = SW_GENERIC | CryptoException.ILLEGAL_VALUE;     //0x6F01
	public final static short SW_UNINITIALIZED_KEY  = SW_GENERIC | CryptoException.UNINITIALIZED_KEY; //0x6F02
	public final static short SW_NO_SUCH_ALGORITHM  = SW_GENERIC | CryptoException.NO_SUCH_ALGORITHM; //0x6303
	public final static short SW_INVALID_INIT       = SW_GENERIC | CryptoException.INVALID_INIT;      //0x6304
	public final static short SW_ILLEGAL_USE        = SW_GENERIC | CryptoException.ILLEGAL_USE;       //0x6305

	// RSA variables
	private KeyPair rsaKeyPair = null;
	private static final short RSAKEY_LENGTH = (short) 2048;
	private Cipher rsaCipher;
	
	// ECC variables
	private ECPublicKey ecPub;
	private ECPrivateKey ecPriv;
	private KeyPair ecKeyPair = null;
	private Signature ecdsa; 

	// store state of PIN auth here
	private final short m_sPinState[];

	// buffer for RSA operation;
	private byte[] rsaByteBuffer;

	// Maximum number of PINs
	final static private short MAX_NUM_OF_PINS = 1;

	// Maximum number of attempts
	final static private short MAX_NUM_OF_ATTEMPTS = 3;

	// Maximum number PIN digits
	final static private short MAX_PIN_SIZE = 8;

	// Maximum number of Files
	final static private short MAX_NUM_OF_FILES = 2;

	// PINs are used to protect the methods of usage and administration
	private javacard.framework.OwnerPIN m_PIN[] = new OwnerPIN[MAX_NUM_OF_PINS];

	// Initial PIN to allow usage Value "111111"
	static byte s_bPIN1_VALUE[] = { 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };

	// Files are used to store any binary data
	private Mau_file m_File[] = new Mau_file[MAX_NUM_OF_FILES];

	// Version string "0003"
	static byte s_bVersion[] = { 0x30, 0x30, 0x30, 0x33 };

	private Miniauth() {

		// RSA key pair is create in constructor but keys are generated in function
		rsaKeyPair = new KeyPair(KeyPair.ALG_RSA_CRT, RSAKEY_LENGTH);

		// instantiate public EC key object
		ecPub = (ECPublicKey) KeyBuilder.buildKey(
				KeyBuilder.TYPE_EC_FP_PUBLIC,
				ECGroup_NIST_P521.KEYLEN_BITS, 
				false);   
		setP521Key(ecPub);

		// instantiate private EC key object
		ecPriv = (ECPrivateKey) KeyBuilder.buildKey(
				KeyBuilder.TYPE_EC_FP_PRIVATE,
				ECGroup_NIST_P521.KEYLEN_BITS, 
				false);	
		setP521Key(ecPriv);
		
		// ECC ey pair is create and keys are generated here and NOT in a function
		ecKeyPair = new KeyPair(ecPub, ecPriv);
		ecKeyPair.genKeyPair();
		
		ecdsa = Signature.getInstance((byte)0x66, false);  // proprietary jTop class  CustomSignature.ALG_ECDSA_NONE = 0x66
		
		// Initialization of the PINs protecting MiniAuth Applet
		m_PIN[0] = new OwnerPIN((byte) (MAX_NUM_OF_ATTEMPTS), (byte) (MAX_PIN_SIZE));
		m_PIN[0].update(s_bPIN1_VALUE, (short) (0), (byte) s_bPIN1_VALUE.length);
		// m_PIN[1]....

		m_sPinState = JCSystem.makeTransientShortArray((short) 1, JCSystem.CLEAR_ON_DESELECT);

		// create ram resident ByteArrays, cleared on deselect
		// 2048 bit key requires 256 bytes
		rsaByteBuffer = JCSystem.makeTransientByteArray((short) 256, JCSystem.CLEAR_ON_DESELECT);

		// m_File[0] = new mau_file((short) 8192);
		// m_File[1] = new mau_file((short) 8192);

		m_sPinState[0] = 0;
	}

	private void setP521Key(ECKey ecKey) {
		// set the key's parameters of the public key according to the NIST P-521 curve
		ecKey.setA(ECGroup_NIST_P521.A, (short) 0, (short) ECGroup_NIST_P521.A.length);
		ecKey.setB(ECGroup_NIST_P521.B, (short) 0, (short) ECGroup_NIST_P521.B.length);
		ecKey.setR(ECGroup_NIST_P521.ORDER, (short) 0, (short) ECGroup_NIST_P521.ORDER.length);
		ecKey.setFieldFP(ECGroup_NIST_P521.PRIME, (short) 0, (short) ECGroup_NIST_P521.PRIME.length);
		ecKey.setG(ECGroup_NIST_P521.GENERATOR, (short) 0, (short) ECGroup_NIST_P521.GENERATOR.length);
		ecKey.setK(ECGroup_NIST_P521.COFACTOR);
	}

	public static void install(byte[] bArray, short bOffset, byte bLength) {
		(new Miniauth()).register();
	}

	public boolean select() {
		m_sPinState[0] = 0;
		return true;
	}

	// ===========================================================================
	// @func short | sign_hash_rsa_pkcs1 | signs buffer containing a hash value
	//
	// @parm byte[] | buffer | buffer is the array to provide the to be signed
	// hash and the signature
	//
	// @parm short | offset | offset where to be signed data begin
	//
	// @parm short | sLength | length of to be signed data
	//
	// @output short
	//
	// @rc SW_KEY_NOT_FOUND 6A88 | Key not found
	//
	// @desc Method is used to sign up to 256 Bytes of hash value
	// ===========================================================================
	private short handle_INS_MAU_KEY_SIGN_PKCS1(byte[] buffer, short offset, byte length) {
		// currently only 256 Bytes of hash supported
		if (length > (256 - 11))
			ISOException.throwIt(MiniAuthDeclarations.SW_WRONG_LENGTH);

		// memset array with 0xFF as default padding
		Util.arrayFillNonAtomic(rsaByteBuffer, (short) 0, (short) rsaByteBuffer.length, (byte) 0xFF);

		// append to be encrypted hash [128-length .. 127]
		Util.arrayCopyNonAtomic(buffer, offset, rsaByteBuffer, (short) (256 - length), length);
		// The encryption block(EB) during encryption with a Private key (used
		// to compute signatures when the message digest is computed off-card)
		// is built as follows:
		// EB = 00 || 01 || PS || 00 || D
		// :: D (input bytes) is the DER encoding of the hash computed elsewhere
		// with an algorithm ID prepended if appropriate
		// :: PS is an octet string of length k-3-||D|| with value FF. The
		// length of PS must be at least 8 octets.
		// :: k is the RSA modulus size.

		rsaByteBuffer[0] = 0x00;
		rsaByteBuffer[1] = 0x01;
		rsaByteBuffer[(short) (256 - length - 1)] = 0x00;

		rsaCipher = Cipher.getInstance((byte) Cipher.ALG_RSA_NOPAD, false);

		// TODO was ist mir dem padding, opad geht, rest nicht
		rsaCipher.init(rsaKeyPair.getPrivate(), (byte) Cipher.MODE_DECRYPT);
		return rsaCipher.doFinal(rsaByteBuffer, (short) 0, (short) rsaByteBuffer.length, buffer, (short) 0);
		/*
		 * RSACipher = Cipher.getInstance( (byte)Cipher.ALG_RSA_NOPAD, false);
		 * 
		 * RSACipher.init (RsaKeyPair.getPublic(), (byte)Cipher.MODE_ENCRYPT);
		 * 
		 * Util.arrayCopyNonAtomic( buffer, (short) 0, RSAByteBuffer, (short) 0,
		 * (short) 256 );
		 * 
		 * return RSACipher.doFinal( RSAByteBuffer, (short)0,
		 * (short)RSAByteBuffer.length, buffer, (short)0 );
		 */
	}

	private short handle_INS_MAU_KEY_SIGN_ECDSA(APDU apdu, byte[] buffer)
	{
		// check values of P1 and P2
		if ((byte)0x00 != buffer[ISO7816.OFFSET_P1] ||
				(byte)0x00 != buffer[ISO7816.OFFSET_P2])
		{
			ISOException.throwIt(ISO7816.SW_WRONG_P1P2);
		}
		apdu.setIncomingAndReceive();
		if (apdu.getCurrentState() != APDU.STATE_FULL_INCOMING) 
		{
			ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
		}

		short len = (short) (buffer[ISO7816.OFFSET_LC] & 0xff);

		// catch crypto exceptions
		try
		{
			// initialize the Signature object for signing with the card's private key
			ecdsa.init(ecPriv, Signature.MODE_SIGN);

			// sign the input data and store the result back into the APDU buffer
			len = ecdsa.sign(buffer, ISO7816.OFFSET_CDATA, len, buffer, (short) 0);
		}

		// in case of crypto exception throw defined status word
		// here: generic error code
		catch (CryptoException e)
		{
			if (e.getReason() == CryptoException.ILLEGAL_VALUE) 
				ISOException.throwIt(Miniauth.SW_ILLEGAL_VALUE);
			else ISOException.throwIt(SW_GENERIC); 
		}
		return len;
	}

	public void process(APDU apdu) {
		// in case of applet selection nothing to do here
		if (selectingApplet()) {
			return;
		}

		short sResultLength = 0;

		byte[] buf = apdu.getBuffer();

		// Verify CLA byte value
		if (buf[ISO7816.OFFSET_CLA] != MiniAuthDeclarations.MAU_CLA) {
			ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);
		}

		switch (buf[ISO7816.OFFSET_INS]) {
		case MiniAuthDeclarations.INS_MAU_PIN_VERIFY:
			handle_INS_MAU_PIN_VERIFY(buf);
			break;
		case MiniAuthDeclarations.INS_MAU_PIN_UPDATE:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			ISOException.throwIt(MiniAuthDeclarations.SW_INSTRUCTION_NOT_SUPPORTED);
			break;
		case MiniAuthDeclarations.INS_MAU_FILE_CREATE:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			if (buf[ISO7816.OFFSET_P1] >= MAX_NUM_OF_FILES)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);
			if (m_File[buf[ISO7816.OFFSET_P1]] != null)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_EXISTS);
			short size = (short) ((buf[ISO7816.OFFSET_CDATA] & 0xFF) << 8);
			size |= buf[ISO7816.OFFSET_CDATA + 1] & 0xFF;
			// int size = (buf[ISO7816.OFFSET_CDATA]&0xFF<<8)|
					// (buf[(short)(ISO7816.OFFSET_CDATA+1)]&0xFF);
			m_File[buf[ISO7816.OFFSET_P1]] = new Mau_file((short) size);
			break;
		case MiniAuthDeclarations.INS_MAU_FILE_DELETE:
			ISOException.throwIt(MiniAuthDeclarations.SW_INSTRUCTION_NOT_SUPPORTED);

			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			if (buf[ISO7816.OFFSET_P1] > MAX_NUM_OF_FILES)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);

			if (m_File[buf[ISO7816.OFFSET_P1]] == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_EXISTS);
			// Delete file
			m_File[buf[ISO7816.OFFSET_P1]] = null;
			JCSystem.requestObjectDeletion(); //no reference to m_File -> file will be deleted prior to next process()
			break;
		case MiniAuthDeclarations.INS_MAU_FILE_SELECT:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			if (buf[ISO7816.OFFSET_P1] > MAX_NUM_OF_FILES)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);
			if (m_File[buf[ISO7816.OFFSET_P1]] == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_EXISTS);
			sResultLength = m_File[buf[ISO7816.OFFSET_P1]].open(buf);
			break;
		case MiniAuthDeclarations.INS_MAU_FILE_READ:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			if (buf[ISO7816.OFFSET_P1] > MAX_NUM_OF_FILES)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);
			if (m_File[buf[ISO7816.OFFSET_P1]] == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_EXISTS);
			sResultLength = m_File[buf[ISO7816.OFFSET_P1]].readData(buf);
			break;
		case MiniAuthDeclarations.INS_MAU_FILE_WRITE:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			if (buf[ISO7816.OFFSET_P1] > MAX_NUM_OF_FILES)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);
			if (m_File[buf[ISO7816.OFFSET_P1]] == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_EXISTS);
			m_File[buf[ISO7816.OFFSET_P1]].writeData(buf);
			break;
		case MiniAuthDeclarations.INS_MAU_KEY_CREATE:
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			rsaKeyPair.genKeyPair();
			break;
		case MiniAuthDeclarations.INS_MAU_KEY_DELETE:
			if(rsaKeyPair == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_PW_NOT_CREATED);
			if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
				ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
			ISOException.throwIt(MiniAuthDeclarations.SW_INSTRUCTION_NOT_SUPPORTED);
			break;
		case MiniAuthDeclarations.INS_MAU_KEY_EXPORT:
			if (buf[ISO7816.OFFSET_P1] == 0) { //export public RSA key
				if(rsaKeyPair == null)
					ISOException.throwIt(MiniAuthDeclarations.SW_PW_NOT_CREATED);
				if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
					ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
				if (buf[ISO7816.OFFSET_P2] == 0x00)
					sResultLength = ((RSAPublicKey) rsaKeyPair.getPublic()).getExponent(buf, (short) 0);
				else if (buf[ISO7816.OFFSET_P2] == 0x01)
					sResultLength = ((RSAPublicKey) rsaKeyPair.getPublic()).getModulus(buf, (short) 0);
				else
					ISOException.throwIt(MiniAuthDeclarations.SW_P2_ADDRESS_WRONG);
			} else if (buf[ISO7816.OFFSET_P1] == 1) { //export public EC key
				if(ecKeyPair == null)
					ISOException.throwIt(MiniAuthDeclarations.SW_PW_NOT_CREATED);
				if (m_sPinState[0] != MiniAuthDeclarations.STATE_USAGE_ALLOWED)
					ISOException.throwIt(MiniAuthDeclarations.SW_SECURITY_STATE_NOT_SATISFIED);
				if (buf[ISO7816.OFFSET_P2] == 0x00)
					sResultLength = ((ECPublicKey) ecKeyPair.getPublic()).getW(buf, (short) 0);
				else
					ISOException.throwIt(MiniAuthDeclarations.SW_P2_ADDRESS_WRONG);
			} else {
				ISOException.throwIt(MiniAuthDeclarations.SW_P1_ADDRESS_WRONG);
			}
			break;
		case MiniAuthDeclarations.INS_MAU_KEY_SIGN_PKCS1:
			if(rsaKeyPair == null)
				ISOException.throwIt(MiniAuthDeclarations.SW_PW_NOT_CREATED);
			sResultLength = handle_INS_MAU_KEY_SIGN_PKCS1(buf, (short) ISO7816.OFFSET_CDATA, (byte) buf[ISO7816.OFFSET_LC]);
			break;
		case MiniAuthDeclarations.INS_MAU_KEY_SIGN_ECDSA:
			sResultLength = handle_INS_MAU_KEY_SIGN_ECDSA(apdu, buf);
			break;
		case MiniAuthDeclarations.INS_MAU_VERSION:
			Util.arrayCopyNonAtomic(s_bVersion, (short) 0, buf, (short) 0, (short) s_bVersion.length);
			sResultLength = (short) s_bVersion.length;
			break;
		default:
			ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
		}
		if (sResultLength > 0)
			apdu.setOutgoingAndSend((short) 0, sResultLength);
	}

	// 'MiniAuth'

	private void handle_INS_MAU_PIN_VERIFY(byte[] buf) {
		// PIN check successful, set the state machine accordingly and return.
		if (m_PIN[0].check(buf, (short) ISO7816.OFFSET_CDATA, (byte) buf[ISO7816.OFFSET_LC])) {
			m_sPinState[0] = MiniAuthDeclarations.STATE_USAGE_ALLOWED;
			return;
		}
		// verification failed -> reset security state
		m_sPinState[0] = 0;

		// If the PIN check failed, throw the apropiate ISOException.
		short sTries = m_PIN[0].getTriesRemaining();
		if (sTries != 0)
			ISOException.throwIt((short) (MiniAuthDeclarations.SW_PIN_FAILED + sTries));
		else
			ISOException.throwIt(MiniAuthDeclarations.SW_AUTH_METHOD_BLOCKED);
	}
}

class Mau_file {
	// Maximum number PIN digits

	byte bDataFile[];
	short sReservedLength = 0;

	// store state of PIN auth here

	public Mau_file(short length) {
		bDataFile = new byte[length];
		sReservedLength = length;
	}

	public short open(byte[] buf) {
		buf[0] = (byte) ((sReservedLength >> 8) & 0xFF);
		buf[1] = (byte) ((sReservedLength) & 0xFF);
		return 2;
	}

	public short readData(byte[] buf) {
		// P1 selects file;
		// get offset (P2)*256
		// try to read 256 bytes
		// return readbytes+2 (9F01)?
		short offset = (short) (buf[ISO7816.OFFSET_P2] * 256);
		if (offset >= sReservedLength)
			ISOException.throwIt((short) (MiniAuthDeclarations.SW_P2_ADDRESS_WRONG));

		short readSize = (short) (sReservedLength - offset);
		if (readSize > 256)
			readSize = 256;
		return Util.arrayCopyNonAtomic(bDataFile, offset, buf, (short) 0, readSize);
	}

	public void writeData(byte[] buf) {
		// P1 selects file;
		// get offset (P2)*252
		// try to write LC bytes
		short offset = (short) ((buf[ISO7816.OFFSET_P2] & 0xFF) * 252);
		if ((short) (offset + (buf[ISO7816.OFFSET_LC] & 0xFF)) > sReservedLength)
			ISOException.throwIt((short) (MiniAuthDeclarations.SW_P2_ADDRESS_WRONG));

		Util.arrayCopyNonAtomic(buf, ISO7816.OFFSET_CDATA, bDataFile, (short) offset, (short) (buf[ISO7816.OFFSET_LC] & 0xFF));
	}
}

interface MiniAuthDeclarations {
	final static byte MAU_CLA             = (byte) 0xB0; // CLA value for Mikey
	final static byte INS_MAU_PIN_VERIFY  = (byte) 0x10; // INS value VERIFY command
	final static byte INS_MAU_PIN_UPDATE  = (byte) 0x11; // INS value for Update PIN command
	final static byte INS_MAU_FILE_CREATE = (byte) 0x20; // INS value for DHPARA command
	final static byte INS_MAU_FILE_DELETE = (byte) 0x21; // INS value for PKE command
	final static byte INS_MAU_FILE_SELECT = (byte) 0x22; // INS value for KEMAC command
	final static byte INS_MAU_FILE_READ   = (byte) 0x23; // INS value for RAND command
	final static byte INS_MAU_FILE_WRITE  = (byte) 0x24; // INS value for RAND command
	final static byte INS_MAU_KEY_CREATE  = (byte) 0x30; // INS value for SIGMAC command
	final static byte INS_MAU_KEY_DELETE  = (byte) 0x31; // INS value for TEK command
	final static byte INS_MAU_KEY_EXPORT  = (byte) 0x32; // INS value for TEK command
	final static byte INS_MAU_KEY_SIGN_PKCS1   = (byte) 0x33; // INS value for Getting a public key
//	final static byte INS_MAU_KEY_VERIFY_PKCS1 = (byte) 0x34; // INS value for Getting a public key
	final static byte INS_MAU_KEY_SIGN_ECDSA   = (byte) 0x35; // INS value for signing with EC key
	final static byte INS_MAU_VERSION     = (byte) 0x40; // INS value for Getting a public key

	final static short SW_SECURITY_STATE_NOT_SATISFIED = (short) 0x6982; // Security state for the financial transaction is not satified
	final static short SW_PIN_FAILED          = (short) 0x63C0; // PIN Failed condition.  The last nibble is replaced with the number of remaining tries
	final static short SW_AUTH_METHOD_BLOCKED = (short) 0x6683; // PIN blocked.
	final static short SW_INSTRUCTION_NOT_SUPPORTED = (short) 0x6D00; // unsupported instruction code
	final static short SW_P1_ADDRESS_WRONG  = (short) 0x6F09; // PIN with the given ID was not found
	final static short SW_P1_ADDRESS_EXISTS = (short) 0x6F0A; // access beyond boundary
	final static short SW_P2_ADDRESS_WRONG  = (short) 0x6F0B; // access beyond boundary
	final static short SW_WRONG_LENGTH      = (short) 0x6F0C; // access beyond boundary
	final static short SW_PW_NOT_CREATED    = (short) 0x6F0D; // access beyond boundar
	final static short STATE_USAGE_ALLOWED  = 1;
} // interface

