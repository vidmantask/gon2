/**
 * @file sfcconstants.h
 * @author Swissbit AG
 * @brief Pubic constants that define error codes and parameter types.
 */

#ifndef SFCCONSTANTS_H
#define SFCCONSTANTS_H

/**
 * @brief Possible return values from all library calls.
 *
 * Every library call returns ::FSI_RET_OK on success.
 * On failure, one of the other ::fsiError codes will be returned.
 * <br>Generally, you should just check for ::FSI_RET_OK and abort on every other error code.
 */
typedef enum {
	FSI_RET_OK                  =   0,  //!< success

	FSI_RET_TIMEOUT             =  -1,  //!< function timed out
	FSI_RET_MEM_ALLOC_ERR       =  -2,  //!< memory allocation failed
	FSI_RET_OPEN_ERR            =  -3,  //!< opening a file failed
	FSI_RET_WRONG_COMM_DIR      =  -4,  //!< response from wrong source

	FSI_RET_MEM_REL_ERR         =  -5,  //!< memory release failed
	FSI_RET_CLOSE_ERR           =  -6,  //!< closing a file failed
	FSI_RET_APDU_SIZE_ERR       =  -7,  //!< size of response too big
	FSI_RET_FLUSH_ERR           =  -8,  //!< flushing a file failed
	FSI_RET_WRITE_ERR           =  -9,  //!< writing to file failed

	FSI_RET_FCNTL_ERR           = -10,  //!< fcntl function error
	FSI_RET_READ_ERR            = -11,  //!< reading from file failed
	FSI_RET_READ_BYTE_ERR       = -12,  //!< read function result error (wrong bytecount)
	FSI_RET_PARAM_APDU_SIZE_ERR = -13,  //!< input size of APDU was too big
	FSI_RET_PAYLOAD_TYPE_ERR    = -14,  //!< the payload type is wrong

	FSI_RET_INVALID_PARAMETER   = -15,  //!< wrong parameter, check log to see which
	FSI_FSI_NOT_SUPPORTED       = -16,  //!< the card does not support FSI
	FSI_RET_INTERNAL_ERROR      = -17,  //!< internal error in library, must be fixed
	// must be updated whenever a new enum value gets added
	FSI_RET_MAX_VALUE           = FSI_RET_INTERNAL_ERROR - 1
} fsiError;

#define FSI_MAX_ATR_SIZE 32 //!< Maximum size of an ATR.
#define FSI_MAX_COMMAND_SIZE 472 //!< Maximum allowed size of a command sent to the SecureFlashCard.
#define FSI_MAX_RESPONSE_SIZE 508 //!< Maximum possible response size received from the SecureFlashCard.

/**
 * @brief Destination of a command.
 *
 * A command can be either sent to the flash card firmware controller or to the secure element.
 */
typedef enum {
	FSI_DEST_FLASH_CONTROLLER = 0, //!< Send command to flash card firmware controller.
	FSI_DEST_SECURE_ELEMENT   = 1  //!< Send command to secure element.
} fsiCommandDestination;

/**
 * @brief Type of response.
 *
 * The response can either be from the flash card firmware controller or from the secure element.
 * <br>When the response is not available, yet, the type is ::FSI_RSP_NOT_COMPLETE.
 */
typedef enum {
	FSI_RSP_FLASH_CONTROLLER = 2, //!< Response from the flash card firmware controller.
	FSI_RSP_SECURE_ELEMENT   = 3, //!< Response from the secure element.
	FSI_RSP_NOT_COMPLETE     = 4  //!< Response is not available, yet. Try again.
} fsiResponseType;

/**
 * @brief Reset type to use in fsi_reset().
 */
typedef enum {
   FSI_RESET_COLD = 0x01, //!< Perform a cold reset.
   FSI_RESET_WARM = 0x81  //!< Perform a warm reset.
} fsiResetType;

/**
 * @brief Status type to use in fsi_status().
 */
typedef enum {
   FSI_STATUS_CARD_MANUFACTURER = 0x46,
   FSI_STATUS_SECURE_ELEMENT    = 0x80
} fsiStatusType;

/**
 * @brief Opaque pointer to a library internal data structure.
 *
 * An instance of this struct will be created by fsi_open() or fsi_open_raw() and destroyed by fsi_close().
 * <br>Make sure to always call fsi_close() when you are done to properly release all resources.
 */
typedef struct SFC_FSI_ *SFC_FSI;

#endif // SFCCONSTANTS_H
