// SfcCardReaderTest.cpp for testing of SfcCardReader library from command line
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdexcept>
#include <string>
#include "SfcCardReader/sfcconstants.h"
#include "SfcCardReader/CardReader.h"

using namespace sfc;

//static unsigned char APDU_TEST_COMMAND[] = { 0x00, 0xCA, 0x9F, 0x7F, 0x00 };
static unsigned char cmdSelectApplet[] =   { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0xD2, 0x76, 0x00, 0x01, 0x62, 0x4D, 0x69, 0x6E, 0x69, 0x41, 0x75, 0x74, 0x68, 0x01 }; //select MiniAuth Applet
static unsigned char cmdVerifyPin[] =      { 0xB0, 0x10, 0x00, 0x00, 0x06, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
static unsigned char cmdGenKey[] =         { 0xB0, 0x30, 0x00, 0x00, 0x00 };
static unsigned char cmdExportKey[] =      { 0xB0, 0x32, 0x00, 0x01, 0x00 };
static unsigned char cmdGetVersion[] =     { 0xB0, 0x40, 0x00, 0x00, 0x00 };
//static unsigned char cmdCreateDataFile[] = { 0xB0, 0x20, 0x00, 0x00, 0x02, 0x20, 0x00 };
//static unsigned char cmdSelectDataFile[] = { 0xB0, 0x22, 0x00, 0x00, 0x00 };
//static unsigned char cmdReadDataFile[] =   { 0xB0, 0x23, 0x00, 0x00, 0x00 };


void trace(const char *msg, unsigned char *data, unsigned int length)
{
	std::cout << msg;
	for (unsigned int i=0;i<length;i++)
	{
		printf("%02x ", data[i]);
	}
	std::cout << std::endl;
}


int main(int argc, char* argv[])
//int _tmain(int argv, TCHAR *argv[], TCHAR *envp[]);
{
	
	std::cout << "SfcCardReader Version: " << std::hex << CardReader::getVersion() << std::endl;
	if (argc!=3) {
		std::cerr << "Usage: SfcCardReaderTest [-f rootDir]" << std::endl;
		abort();
	}
	std::string option(argv[1]);
	CardReader *reader = NULL;
	try {
		if (option.compare("-f")==0){
			// use explicite filename
			reader = new CardReader(argv[2]);
		} else {
			std::cerr << "argument must be -f followed by root directory" << std::endl;
			abort();
		}

		// ATR buffer 
		unsigned char atr[FSI_MAX_ATR_SIZE];
		unsigned int atrLength;

		// Send some test APDU
		unsigned char response[FSI_MAX_RESPONSE_SIZE];
		unsigned int rspLength;

		for(int i=0;i<5;i++)
		{
			// Connect to card and get ATR
			std::cout << std::endl << std::endl << "Connect:" << std::endl;
			atrLength = FSI_MAX_ATR_SIZE;
			reader->connect(3000, atr, &atrLength);
			trace(" ATR: ", atr, atrLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdSelectApplet, sizeof(cmdSelectApplet));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(1000, cmdSelectApplet, sizeof(cmdSelectApplet), response, &rspLength);
			trace("<-- ", response, rspLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdGetVersion, sizeof(cmdGetVersion));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(1000, cmdGetVersion, sizeof(cmdGetVersion), response, &rspLength);
			trace("<-- ", response, rspLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdVerifyPin, sizeof(cmdVerifyPin));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(1000, cmdVerifyPin, sizeof(cmdVerifyPin), response, &rspLength);
			trace("<-- ", response, rspLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdGenKey, sizeof(cmdGenKey));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(60000, cmdGenKey, sizeof(cmdGenKey), response, &rspLength);
			trace("<-- ", response, rspLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdGetVersion, sizeof(cmdGetVersion));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(1000, cmdGetVersion, sizeof(cmdGetVersion), response, &rspLength);
			trace("<-- ", response, rspLength);

			//Display command and response
			std::cout << "Loop: " << i <<std::endl;
			trace("--> ", cmdExportKey, sizeof(cmdExportKey));
			rspLength = FSI_MAX_RESPONSE_SIZE;
			reader->transmit(1000, cmdExportKey, sizeof(cmdExportKey), response, &rspLength);
			trace("<-- ", response, rspLength);

			// Disconnect from card
			reader->disconnect(1000);
		}
		delete(reader);
	} catch(CardReaderException& e) {
		std::cout << e.what() << std::endl;
		std::cout << "error code: " << e.error() << std::endl;
		reader->disconnect(1000);
		delete(reader);
	} catch(...) {
		std::cout << "Unknown exception";
		reader->disconnect(1000);
		delete(reader);
	}
	return 0;
}

