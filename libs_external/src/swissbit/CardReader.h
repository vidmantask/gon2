/**
 * @file CardReader.h
 * @author Swissbit AG
 * @date 21 May 2014
 * @brief Card reader header file
 *
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Copyright (c) 2014 Swissbit AG.
 *
 * <p>
 * The class CardReader is a object oriented wrapper around
 * the FSI (File based Security Interface).
 * </p>
 */

#pragma once


#ifdef SFCCARDREADER_USE_DLL
#ifdef SFCCARDREADER_EXPORTS
#define CARDREADER_API __declspec(dllexport)
#else
#define CARDREADER_API __declspec(dllimport)
#endif
#else
#define CARDREADER_API
#endif

#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>
#include "sfcconstants.h"


namespace sfc {

/**
 * Exception for CardReader.
 */
class CardReaderException : public std::runtime_error
{

private:
    unsigned int m_error;
public:
	/** Creates a CardReaderException */
	CardReaderException(unsigned int error, const std::string& msg);

	CARDREADER_API short error();
};


class CardReader {
private:
	static unsigned int version;

	/* file handle of communication file */
	SFC_FSI fsiFile;

	/* filename corresponding to FILE_HANDLE */
	std::string filename;

public:
	/****************************************************************************
		@brief creates/opens the file fsiFilePath for communication with
		the secure element in the SD card.
		@details opens a file for rw and set fsiFile handle.
		For windows rootDir may be the drive letter followed by colon, e.g. 'f:'.
		This constructor is for windows OS only.
		@param rootDir: path of the directory for the SD card.
	 ****************************************************************************/
	CARDREADER_API CardReader(const char *rootDir);

	/****************************************************************************
		@brief closes the file fsiFilePath used for the communication
		with the secure element.
	 ****************************************************************************/
	CARDREADER_API ~CardReader();

	/****************************************************************************
		@brief static method that returns a version number of the library
		@details the version has a major and minor number.
		both numbers are coded as binary.
		The major number is increased in case of major feature changes.
		The minor number is encreased in case of minor feature changes.
		@return unsigned int containing the version number
	 ****************************************************************************/
	CARDREADER_API static unsigned int getVersion(void);

	/***************************************************************************
		@brief connects to the secure element (SE) inside the SD card
		       according to the activation sequence defined in ISO7816-3.
		@param timeout: a value in milliseconds that indicates when this function
						shall give up to receive a response from the secure element.
						Pasing a 0 will set the timeout to FSI_MAX_TIMEOUT.
		@param atr: the complete ATR (Answer To Reset) of the secure element or an
					empty byte array if the underlying reader does not support ATR
					requests. The caller is responsible to allocate a byte array
					(in general, the ATR has a maximum size of 33 bytes).
		@param atrLength: the length of the ATR that is returned by this function
	 ***************************************************************************/
	CARDREADER_API void connect(unsigned long timeout,
			unsigned char *atr,
			unsigned int *atrLength);

	/***************************************************************************
		@brief Disconnects from the secure element inside the SDCard
		       according to the deactivation sequence defined in ISO7816-3.
		@param timeout: a value in milliseconds that indicates when this function
						shall give up to receive a response from the secure element.
						Pasing a 0 will set the timeout to FSI_MAX_TIMEOUT.
	 ***************************************************************************/
	CARDREADER_API void disconnect(unsigned long timeout);

	/***************************************************************************
		@brief Convenience communication method: sends a command to the secure
			   element and receives the response.
		@param timeout: a value in milliseconds that indicates when this function
						shall give up to receive a response from the secure element.
						Pasing a 0 will set the timeout to FSI_MAX_TIMEOUT.
		@param command: Command APDU to be send to the secure element. The command is
						not checked to be a valid APDU.
		@param commandLength: length of the command in bytes.
		@param response: Response APDU received by the secure element. The command is
						 not checked to be a valid response APDU. The caller is responsible
						 to provide an array of bytes (a response must not exceed the size
						 of 512 bytes).
		@param responseLength: length of the response
	 ***************************************************************************/
	CARDREADER_API void transmit(unsigned long timeout,
			unsigned char *command,
			unsigned int commandLength,
			unsigned char *response,
			unsigned int *responseLength);

	/***************************************************************************
		@brief Convenience communication method: sends a command to the flash
			   controller and receives the response.
		@param timeout: a value in milliseconds that indicates when this function
						shall give up to receive a response from the secure element.
						Pasing a 0 will set the timeout to FSI_MAX_TIMEOUT.
		@param command: Command APDU to be send to the secure element. The command is
						not checked to be a valid APDU.
		@param commandLength: length of the command in bytes.
		@param response: Response APDU received by the secure element. The command is
						 not checked to be a valid response APDU. The caller is responsible
						 to provide an array of bytes (a response must not exceed the size
						 of 512 bytes).
		@param responseLength: length of the response
		@return 0 if success.
				negative result if error
	 ***************************************************************************/
	CARDREADER_API void control(unsigned long timeout,
			unsigned char *command,
			unsigned int commandLength,
			unsigned char *response,
			unsigned int *responseLength);
};
}
