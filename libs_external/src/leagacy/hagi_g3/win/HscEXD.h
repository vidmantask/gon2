
// HscEXD.h
//----------------------------------------------------------
#ifndef HSCEXD_H_772F895D_E23D_4D83_BE0C_E6BF3B45B828
#define HSCEXD_H_772F895D_E23D_4D83_BE0C_E6BF3B45B828
//----------------------------------------------------------
#include <windows.h>

#define HscEXD_SUCCESS									(0x00)
#define HscEXD_ERROR_INVALID_ARGUMENT					(0x8001)
#define HscEXD_ERROR_INVALID_COMPANY_CODE				(0x8002)
#define HscEXD_ERROR_DLL_IS_NOT_ENABLE					(0x8003)
#define HscEXD_ERROR_DEVICE_NOT_FOUND					(0x8004)
#define HscEXD_ERROR_MANY_DEVICE_FOUND					(0x8005)
#define HscEXD_ERROR_NOT_SUPPORT_FUNCTION				(0x8006)
#define HscEXD_ERROR_DRIVE_NOT_FOUND					(0x8007)
#define HscEXD_ERROR_DEVICE_NOT_READY					(0x8008)
#define HscEXD_ERROR_COMMAND_FAILED						(0x8009)

#define HscEXD_ERROR_ISO_FILE_NOT_FOUND					(0x800A)
#define HscEXD_ERROR_HIDDEN_FILE_NOT_FOUND				(0x800B)
#define HscEXD_ERROR_SINGLE_DRIVE_MODE					(0x800C)
#define HscEXD_ERROR_DEVICE_IS_LOCKED					(0x800D)
#define HscEXD_ERROR_EXCEED_DEVICE_CAPACITY				(0x800E)

#define HscEXD_ERROR_SOURCE_FOLDER_NOT_FOUND			(0x800F)
#define HscEXD_ERROR_FMTCNVEXE_NOT_FOUND				(0x8010)
#define HscEXD_ERROR_EXCEED_MAXPATH						(0x8011)
#define HscEXD_ERROR_INVALID_FILENAME					(0x8012)
#define HscEXD_ERROR_INVALID_VOLUMELABEL				(0x8013)
#define HscEXD_ERROR_OUTPUT_FOLDER_NOT_FOUND			(0x8014)
#define HscEXD_ERROR_CREATE_ISO_FILE					(0x8015)

#define HscEXD_ERROR_LOCK_FUNCTION_NOT_ENABLE			(0x8016)
#define HscEXD_ERROR_PASSWORD_MISS_COUNT_OVER			(0x8017)
#define HscEXD_ERROR_FLUSH_DRIVE						(0x8018)
#define HscEXD_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED	(0x8019)
#define HscEXD_ERROR_NO_INITIAL_PASSWORD_SET			(0x801A)
#define HscEXD_ERROR_DEVICE_IS_ALREADY_UNLOCKED			(0x801B)
#define HscEXD_ERROR_REMOVABLE_DISK_DOSFORMAT			(0x801C)
#define HscEXD_ERROR_LOCK_COMMAND_FAILED				(0x801D)

#ifdef __cplusplus
	extern "C" {
#endif

UINT __stdcall HscEXD_OpenDll(const char* inAccessCode);
UINT __stdcall HscEXD_CloseDll(const char* inAccessCode);

UINT __stdcall HscEXD_GetRemovableDriveLetter(char* outDrive);
UINT __stdcall HscEXD_GetRomDriveLetter(char* outDrive);

UINT __stdcall HscEXD_ReadUniqueID(UCHAR* outBuffer16);

UINT __stdcall HscEXD_ReadScalableHiddenTotalByte(UINT64* outTotal);
UINT __stdcall HscEXD_ReadScalableHidden(UCHAR* outBuffer, UINT64 inAddress, UINT64 inLength);
UINT __stdcall HscEXD_WriteScalableHidden(const UCHAR* inBuffer, UINT64 inAddress, UINT64 inLength);

UINT __stdcall HscEXD_BurnImage(const char* inIsoFilePath, const char* inHiddenFilePath, HWND inWindowHandle, UINT inProgressMessageID);
UINT __stdcall HscEXD_StrongBurnImage(const char* inIsoFilePath, const char* inHiddenFilePath, HWND inWindowHandle, UINT inProgressMessageID);
UINT __stdcall HscEXD_CreateIsoImage(const char* inSourceFolderPath, const char* inVolumeLabel, const char* inOutputFileName);

UINT __stdcall HscEXD_ReadFixedHiddenTotalByte(UINT64* outTotal);
UINT __stdcall HscEXD_ReadFixedHidden(UCHAR* outBuffer, UINT64 inAddress, UINT64 inLength);
UINT __stdcall HscEXD_WriteFixedHidden(const UCHAR* inBuffer, UINT64 inAddress, UINT64 inLength);

UINT __stdcall HscEXD_UnLockRemovableDisk(const char* inPassword);
UINT __stdcall HscEXD_ChangeRemovableDiskLockPassword(const char* inCurrentPassword, const char* inNewPassword);
UINT __stdcall HscEXD_GetRemovableDiskLockMissCountState(UINT* outMissCount, UINT* outMissMax);
UINT __stdcall HscEXD_GetRemovableDiskInitialLockState(BOOL* outInitialstate);
UINT __stdcall HscEXD_InitializeRemovableDiskLockPassword(const char* inNewPassword);

UINT __stdcall HscEXD_DestroyDisk(HWND inWindowHandle, UINT inProgressMessageID);

#ifdef __cplusplus
	}
#endif

#endif //HSCEXD_H_772F895D_E23D_4D83_BE0C_E6BF3B45B828


