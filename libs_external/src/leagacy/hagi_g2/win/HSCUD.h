
// HSCUD.h
//----------------------------------------------------------
#ifndef HSCUD_H_5EEB6317_E80C_419E_AD04_52239DE8182E
#define HSCUD_H_5EEB6317_E80C_419E_AD04_52239DE8182E
//----------------------------------------------------------
#include <windows.h>

#define		HSCUD_DO_NOT_OPEN				0x01
#define		HSCUD_INVALID_OS				0x02
#define		HSCUD_NO_Device					0x03
#define		HSCUD_ERR						0x05
#define		HSCUD_INVALID_Length			0x06
#define		HSCUD_NOT_CLOSE					0x07
#define		HSCUD_ALREADY_DLL_OPEN			0x08
#define		HSCUD_DONOT_USE_SCSI_COMMAND	0x09
#define		HSCUD_MANY_DEVICES				0x10
#define		HSCUD_DONOT_FIND_DEVICE			0x11
#define		HSCUD_UNSUCCESS					0x12
#define		HSCUD_NOT_Compare				0x13
#define		HSCUD_Complete					0x00

#ifdef HSCUD_EXPORTS
#define HSCUD_API __declspec(dllexport)
#else
#define HSCUD_API __declspec(dllimport)
#pragma comment(lib, "HSCUD.lib")
#endif

#ifdef __cplusplus
  extern "C"{
#endif

int __stdcall TEST(int a,int b );
UCHAR __stdcall HSCID_Read( UCHAR *ReadBuffer , CHAR *compData );
UCHAR __stdcall Hidden_Read( USHORT wRead_Ofset,USHORT wRead_Length,UCHAR *ReadBuffer,CHAR *compData );
UCHAR __stdcall Hidden_Write( USHORT wWrite_Ofset,USHORT wWrite_Length,UCHAR *WriteBuffer,CHAR *compData );
UCHAR __stdcall GetDriveList( UCHAR *List_buff );

#ifdef __cplusplus
}
#endif


#endif //HSCUD_H_5EEB6317_E80C_419E_AD04_52239DE8182E



