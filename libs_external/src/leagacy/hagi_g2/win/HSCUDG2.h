// HscUDG2.h
//----------------------------------------------------------
#ifndef HSCUDG2_H_7F8CA3FE_B596_474C_8F36_1CCFC43DB0AB
#define HSCUDG2_H_7F8CA3FE_B596_474C_8F36_1CCFC43DB0AB
//----------------------------------------------------------
#include <windows.h>

#ifdef HSCUDG2_EXPORTS
#define HSCUDG2_API __declspec(dllexport)
#else
#define HSCUDG2_API __declspec(dllimport)
#pragma comment(lib, "HSCUDG2.lib")
#endif

#define HSCUDG2_COMPLETE						0x00
#define HSCUDG2_ARGUMENT_ERR					0x01
#define HSCUDG2_DEVICE_NOT_FOUND_ERR			0x02
#define HSCUDG2_TOO_MANY_DEVICE_FOUND_ERR		0x03
#define HSCUDG2_COMMAND_ERR 					0x04

#ifdef __cplusplus
  extern "C"
  {
#endif

HSCUDG2_API UINT __stdcall HscOpenDll(const char *inAccessCode);
HSCUDG2_API UINT __stdcall HscCloseDll();
HSCUDG2_API UINT __stdcall HscGetRemovableDriveLetter(char* outDrive);
HSCUDG2_API UINT __stdcall HscGetRomDriveLetter(char* outDrive); 
HSCUDG2_API UINT __stdcall HscGetHiddenTotalByte(ULONG* outTotal);
HSCUDG2_API UINT __stdcall HscReadHidden(UCHAR* outBuffer, ULONG address,ULONG length);
HSCUDG2_API UINT __stdcall HscWriteHidden(UCHAR* inBuffer, ULONG address,ULONG length);
HSCUDG2_API UINT __stdcall HscGetUniqueID(UCHAR* outBuffer16);

#ifdef __cplusplus
  }
#endif


#endif //HSCUDG2_H_7F8CA3FE_B596_474C_8F36_1CCFC43DB0AB