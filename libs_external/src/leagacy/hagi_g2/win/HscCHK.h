
// HSCCHK.h
//----------------------------------------------------------
#ifndef HSCCHK_H_C200B371_5B4E_4BB3_83CC_F57F1CA137CC
#define HSCCHK_H_C200B371_5B4E_4BB3_83CC_F57F1CA137CC
//----------------------------------------------------------
#include <windows.h>

#ifdef HSCCHK_EXPORTS
#define HSCCHK_API __declspec(dllexport)
#else
#define HSCCHK_API __declspec(dllimport)
#pragma comment(lib, "HSCCHK.lib")
#endif

#define HscCHK_SUCCESS						(0x00)

#define HscCHK_ERROR_INVALID_ARGUMENT 		(0x01)
#define HscCHK_ERROR_INVALID_ACCESSCODE 	(0x02)
#define HscCHK_ERROR_DEVICE_NOT_FOUND 		(0x03)
#define HscCHK_ERROR_MANY_DEVICE_FOUND 		(0x04)
#define HscCHK_ERROR_COMMAND_FAILED 		(0x05)

#ifdef __cplusplus
  extern "C"
  {
#endif

HSCCHK_API UINT __stdcall HscCHK_ReadDeviceVersion(UINT *outDeviceVersion, const char* inAccessCode);

#ifdef __cplusplus
  }
#endif

#endif //HSCCHK_H_C200B371_5B4E_4BB3_83CC_F57F1CA137CC