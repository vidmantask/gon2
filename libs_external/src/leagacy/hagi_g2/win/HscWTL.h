
// HscWTL.h
//----------------------------------------------------------
#ifndef HSCWTL_H_92B852C8_F7ED_42BB_B8D0_383F8E42A495
#define HSCWTL_H_92B852C8_F7ED_42BB_B8D0_383F8E42A495
//----------------------------------------------------------
#include <windows.h>

#ifdef HSCWTL_EXPORTS
#define HSCWTL_API __declspec(dllexport)
#else
#define HSCWTL_API __declspec(dllimport)
#pragma comment(lib, "HscWTL.lib")
#endif

#ifdef __cplusplus
  extern "C"
  {
#endif

HSCWTL_API BOOL __stdcall HscWTL_IsAdministratorAccount();

#ifdef __cplusplus
  }
#endif

#endif //HSCWTL_H_92B852C8_F7ED_42BB_B8D0_383F8E42A495


