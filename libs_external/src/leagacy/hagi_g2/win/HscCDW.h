
// HscCDW.h
//----------------------------------------------------------
#ifndef HSCCDW_H_E11072B5_5193_425D_9A2E_994DBC7F4CAB
#define HSCCDW_H_E11072B5_5193_425D_9A2E_994DBC7F4CAB
//----------------------------------------------------------
#include <windows.h>


#ifdef HSCCDW_EXPORTS
#define HSCCDW_API __declspec(dllexport)
#else
#define HSCCDW_API __declspec(dllimport)
#pragma comment(lib, "HscCDW.lib")
#endif

#define HscCDW_COMPLETE							(0x0)
#define HscCDW_ERROR_INVALID_ACCESSCODE			(0x01)
#define HscCDW_ERROR_DLL_IS_NOT_ENABLED			(0x02)
#define HscCDW_ERROR_INVALID_ARGUMENT			(0x03)
#define HscCDW_ERROR_ISO_FILE_NOT_FOUND			(0x04)
#define HscCDW_ERROR_DEVICE_NOT_FOUND			(0x05)
#define HscCDW_ERROR_TOO_MANY_DEVICES			(0x06)
#define HscCDW_ERROR_SINGLE_DRIVE_MODE			(0x07)
#define HscCDW_ERROR_DEVICE_IS_LOCKED			(0x08)
#define HscCDW_ERROR_EXCEED_DEVICE_CAPACITY		(0x09)
#define HscCDW_ERROR_WRITE_COMMAND_FAILED		(0x0A)

#define HscCDW_ERROR_SOURCE_FOLDER_NOT_FOUND	(0x0B)
#define HscCDW_ERROR_FMTCNVEXE_NOT_FOUND		(0x0C)
#define HscCDW_ERROR_EXCEED_MAXPATH				(0x0D)
#define HscCDW_ERROR_INVALID_FILENAME	 		(0x0E)
#define HscCDW_ERROR_INVALID_VOLUMELABEL		(0x0F)
#define HscCDW_ERROR_CREATE_ISO_FILE	 		(0x10)

#ifdef __cplusplus
extern "C" {
#endif

HSCCDW_API UINT __stdcall HscCDW_OpenDLL(const char* inAccessCode);
HSCCDW_API UINT __stdcall HscCDW_CloseDLL();
HSCCDW_API UINT __stdcall HscCDW_BurnImage(const char* inIsoFilePath,
										   const char* inHiddenFilePath,
										   HWND inWindowHandle,
										   UINT inProgressMessageID);

HSCCDW_API UINT __stdcall HscCDW_CreateIsoImage(const char* inSourceFolderPath,
												const char* inVolumeLabel,
												const char* inOutputFileName);
#ifdef __cplusplus
}
#endif

#endif //HSCCDW_H_E11072B5_5193_425D_9A2E_994DBC7F4CAB


