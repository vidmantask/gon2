
// HscULK2.h
//----------------------------------------------------------
#ifndef HSCULK2_H_37281CF3_3CEA_4A5F_89FA_05E5A0979907
#define HSCULK2_H_37281CF3_3CEA_4A5F_89FA_05E5A0979907
//----------------------------------------------------------
#include <windows.h>


#ifdef HSCULK2_EXPORTS
#define HSCULK2_API __declspec(dllexport)
#else
#define HSCULK2_API __declspec(dllimport)
#pragma comment(lib, "HscULK2.lib")
#endif

#define HSCULK2_COMPLETE							0x00
#define HSCULK2_DLL_OPEN_ERR						0x01
#define HSCULK2_ARGUMENT_ERR						0x02
#define HSCULK2_DEVICE_NOT_FOUND	 				0x03
#define HSCULK2_SETTING_LOCK_FUNCTION_ERR			0x04
#define HSCULK2_DEVICE_IS_ALREADY_UNLOCKED			0x05
#define HSCULK2_PASSMISS_COUNT_OVER					0x06
#define HSCULK2_PASSWAORD_NOT_COMPARE				0x07
#define HSCULK2_FLUSH_DRIVE_ERR						0x08
#define HSCULK2_COMMAND_ERR							0x09
#define HSCULK2_INITIALPASS_IS_ALREADY_CONFIGURED	0x0A
#define HSCULK2_NO_INITIAL_PASSWORD_SET				0x0B

#ifdef __cplusplus
	extern "C" {
#endif

HSCULK2_API UINT  __stdcall HscUlk2OpenDll(const char* inAccessCode);
HSCULK2_API UINT  __stdcall HscUlk2CloseDll();

HSCULK2_API UINT  __stdcall CheckRemovableDiskInitialState(BOOL* outInitialstate);
HSCULK2_API UINT  __stdcall InitializeRemovableDiskPassword(const char* inNewPassword);

HSCULK2_API UINT  __stdcall UnLockRemovableDisk(const char* inPassword);
HSCULK2_API UINT  __stdcall ChangeRemovableDiskPassword(const char* inCurrentPassword, const char* inNewPassword);
HSCULK2_API UINT  __stdcall FormatRemovableDisk();

HSCULK2_API UINT  __stdcall GetRemovableDiskCommandMissCount(UINT* outMissCount);


#ifdef __cplusplus
	}
#endif

#endif //HSCULK2_H_37281CF3_3CEA_4A5F_89FA_05E5A0979907
