
// HscUDB.h
//----------------------------------------------------------
#ifndef HSCUDB_H_35332108_9133_4EFA_AFC7_60D2C064761B
#define HSCUDB_H_35332108_9133_4EFA_AFC7_60D2C064761B
//----------------------------------------------------------
#include <windows.h>

#ifdef HSCUDB_EXPORTS
#define HSCUDB_API __declspec(dllexport)
#else
#define HSCUDB_API __declspec(dllimport)
#pragma comment(lib, "HscUDB.lib")
#endif


#define HscUDB_SUCCESS						(0x00)

#define HscUDB_ERROR_INVALID_COMPANY_CODE 		(0x01)
#define HscUDB_ERROR_INVALID_ARGUMENT 			(0x02)
#define HscUDB_ERROR_DEVICE_NOT_FOUND 			(0x03)
#define HscUDB_ERROR_MANY_DEVICE_FOUND 			(0x04)

#define HscUDB_ERROR_DLL_IS_NOT_ENABLE 			(0x30)
#define HscUDB_ERROR_COMMAND_FAILED 			(0x31)
#define HscUDB_ERROR_MUST_NEED_ADMINISTRATOR 	(0xff)



#ifdef __cplusplus
  extern "C"
  {
#endif

HSCUDB_API UINT __stdcall HscUDB_OpenDLL(const char *inAccessCode);
HSCUDB_API UINT __stdcall HscUDB_CloseDLL();

HSCUDB_API UINT __stdcall HscUDB_GetRemovableDriveLetter(char* outDrive);
HSCUDB_API UINT __stdcall HscUDB_GetRomDriveLetter(char* outDrive);

HSCUDB_API UINT __stdcall HscUDB_GetHiddenTotalByte(ULONG* outTotal);

HSCUDB_API UINT __stdcall HscUDB_ReadHidden(UCHAR* outBuffer, ULONG address, ULONG length);
HSCUDB_API UINT __stdcall HscUDB_WriteHidden(UCHAR* inBuffer, ULONG address, ULONG length);

HSCUDB_API UINT __stdcall HscUDB_GetUniqueID(UCHAR* outBuffer16);

#ifdef __cplusplus
  }
#endif


#endif //HSCUDB_H_35332108_9133_4EFA_AFC7_60D2C064761B
