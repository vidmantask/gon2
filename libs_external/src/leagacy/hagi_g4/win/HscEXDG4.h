
// HscEXDG4.h
//----------------------------------------------------------
#ifndef HSCEXDG4_H_810F68BD_A55E_4F4F_A36B_1D8070F8F773
#define HSCEXDG4_H_810F68BD_A55E_4F4F_A36B_1D8070F8F773
//----------------------------------------------------------

#include <windows.h>

#define HscEXDG4_SUCCESS									(0x00)
#define HscEXDG4_ERROR_INVALID_ARGUMENT						(0x8001)
#define HscEXDG4_ERROR_INVALID_COMPANY_CODE					(0x8002)
#define HscEXDG4_ERROR_DLL_IS_NOT_ENABLE					(0x8003)
#define HscEXDG4_ERROR_DEVICE_NOT_FOUND						(0x8004)
#define HscEXDG4_ERROR_MANY_DEVICE_FOUND					(0x8005)
#define HscEXDG4_ERROR_NOT_SUPPORT_FUNCTION					(0x8006)
#define HscEXDG4_ERROR_DRIVE_NOT_FOUND						(0x8007)
#define HscEXDG4_ERROR_DEVICE_NOT_READY						(0x8008)
#define HscEXDG4_ERROR_COMMAND_FAILED						(0x8009)

#define HscEXDG4_ERROR_ISO_FILE_NOT_FOUND					(0x800A)
#define HscEXDG4_ERROR_HIDDEN_FILE_NOT_FOUND				(0x800B)
#define HscEXDG4_ERROR_SINGLE_DRIVE_MODE					(0x800C)
#define HscEXDG4_ERROR_DEVICE_IS_LOCKED						(0x800D)
#define HscEXDG4_ERROR_EXCEED_DEVICE_CAPACITY				(0x800E)

#define HscEXDG4_ERROR_SOURCE_FOLDER_NOT_FOUND				(0x800F)
#define HscEXDG4_ERROR_FMTCNVEXE_NOT_FOUND					(0x8010)
#define HscEXDG4_ERROR_EXCEED_MAXPATH						(0x8011)
#define HscEXDG4_ERROR_INVALID_FILENAME						(0x8012)
#define HscEXDG4_ERROR_INVALID_VOLUMELABEL					(0x8013)
#define HscEXDG4_ERROR_OUTPUT_FOLDER_NOT_FOUND				(0x8014)
#define HscEXDG4_ERROR_CREATE_ISO_FILE						(0x8015)

#define HscEXDG4_ERROR_LOCK_FUNCTION_NOT_ENABLE				(0x8016)
#define HscEXDG4_ERROR_PASSWORD_MISS_COUNT_OVER				(0x8017)
#define HscEXDG4_ERROR_FLUSH_DRIVE							(0x8018)
#define HscEXDG4_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED	(0x8019)
#define HscEXDG4_ERROR_NO_INITIAL_PASSWORD_SET				(0x801A)
#define HscEXDG4_ERROR_DEVICE_IS_ALREADY_UNLOCKED			(0x801B)
#define HscEXDG4_ERROR_REMOVABLE_DISK_DOSFORMAT				(0x801C)
#define HscEXDG4_ERROR_LOCK_COMMAND_FAILED					(0x801D)

#ifdef __cplusplus
	extern "C" {
#endif

UINT __stdcall HscEXDG4_OpenDll(const char* inAccessCode);
UINT __stdcall HscEXDG4_CloseDll(const char* inAccessCode);

UINT __stdcall HscEXDG4_GetRemovableDriveLetter(char* outDrive);
UINT __stdcall HscEXDG4_GetRomDriveLetter(char* outDrive);

UINT __stdcall HscEXDG4_ReadUniqueID(UCHAR* outBuffer16);

UINT __stdcall HscEXDG4_ReadScalableHiddenTotalByte(UINT64* outTotal);
UINT __stdcall HscEXDG4_ReadScalableHidden(UCHAR* outBuffer, UINT64 inAddress, UINT64 inLength);
UINT __stdcall HscEXDG4_WriteScalableHidden(const UCHAR* inBuffer, UINT64 inAddress, UINT64 inLength);

UINT __stdcall HscEXDG4_BurnImage(const char* inIsoFilePath, const char* inHiddenFilePath, HWND inWindowHandle, UINT inProgressMessageID);
UINT __stdcall HscEXDG4_StrongBurnImage(const char* inIsoFilePath, const char* inHiddenFilePath, HWND inWindowHandle, UINT inProgressMessageID);
UINT __stdcall HscEXDG4_CreateIsoImage(const char* inSourceFolderPath, const char* inVolumeLabel, const char* inOutputFileName);

UINT __stdcall HscEXDG4_ReadFixedHiddenTotalByte(UINT64* outTotal);
UINT __stdcall HscEXDG4_ReadFixedHidden(UCHAR* outBuffer, UINT64 inAddress, UINT64 inLength);
UINT __stdcall HscEXDG4_WriteFixedHidden(const UCHAR* inBuffer, UINT64 inAddress, UINT64 inLength);

UINT __stdcall HscEXDG4_UnLockRemovableDisk(const char* inPassword);
UINT __stdcall HscEXDG4_ChangeRemovableDiskLockPassword(const char* inCurrentPassword, const char* inNewPassword);
UINT __stdcall HscEXDG4_GetRemovableDiskLockMissCountState(UINT* outMissCount, UINT* outMissMax);
UINT __stdcall HscEXDG4_GetRemovableDiskInitialLockState(BOOL* outInitialstate);
UINT __stdcall HscEXDG4_InitializeRemovableDiskLockPassword(const char* inNewPassword);

UINT __stdcall HscEXDG4_DestroyDisk(HWND inWindowHandle, UINT inProgressMessageID);

#ifdef __cplusplus
	}
#endif

#endif //HSCEXDG4_H_810F68BD_A55E_4F4F_A36B_1D8070F8F773