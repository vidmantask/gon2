REM ---------------------------------------------------------------------
REM attachment_excel

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w96 -h96 -e iPhone/plus/attachment_excel@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w64 -h64 -e iPhone/retina/attachment_excel.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w32 -h32 -e iPhone/normal/attachment_excel.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w66 -h66 -e Android/hdpi/attachment_excel.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w44 -h44 -e Android/mdpi/attachment_excel.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3011 -w80 -h80 -e FileBrowser/attachment_excel.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_generic_audio

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w96 -h96 -e iPhone/plus/attachment_generic_audio@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w64 -h64 -e iPhone/retina/attachment_generic_audio.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w32 -h32 -e iPhone/normal/attachment_generic_audio.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w66 -h66 -e Android/hdpi/attachment_generic_audio.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w44 -h44 -e Android/mdpi/attachment_generic_audio.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997 -w80 -h80 -e FileBrowser/attachment_generic_audio.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_generic_document

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w96 -h96 -e iPhone/plus/attachment_generic_document@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w64 -h64 -e iPhone/retina/attachment_generic_document.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w32 -h32 -e iPhone/normal/attachment_generic_document.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w66 -h66 -e Android/hdpi/attachment_generic_document.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w44 -h44 -e Android/mdpi/attachment_generic_document.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8 -w80 -h80 -e FileBrowser/attachment_generic_document.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_generic_image

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w96 -h96 -e iPhone/plus/attachment_generic_image@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w64 -h64 -e iPhone/retina/attachment_generic_image.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w32 -h32 -e iPhone/normal/attachment_generic_image.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w66 -h66 -e Android/hdpi/attachment_generic_image.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w44 -h44 -e Android/mdpi/attachment_generic_image.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-1 -w80 -h80 -e FileBrowser/attachment_generic_image.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_generic_movie

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w96 -h96 -e iPhone/plus/attachment_generic_movie@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w64 -h64 -e iPhone/retina/attachment_generic_movie.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w32 -h32 -e iPhone/normal/attachment_generic_movie.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w66 -h66 -e Android/hdpi/attachment_generic_movie.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w44 -h44 -e Android/mdpi/attachment_generic_movie.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4 -w80 -h80 -e FileBrowser/attachment_generic_movie.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_generic_text

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w96 -h96 -e iPhone/plus/attachment_generic_text@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w64 -h64 -e iPhone/retina/attachment_generic_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w32 -h32 -e iPhone/normal/attachment_generic_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w66 -h66 -e Android/hdpi/attachment_generic_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w44 -h44 -e Android/mdpi/attachment_generic_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect2997-8-4-1 -w80 -h80 -e FileBrowser/attachment_generic_text.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_gif

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3336 -w96 -h96 -e iPhone/plus/attachment_gif@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3336 -w64 -h64 -e iPhone/retina/attachment_gif.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3336 -w32 -h32 -e iPhone/normal/attachment_gif.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3336 -w66 -h66 -e Android/hdpi/attachment_gif.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3336 -w44 -h44 -e Android/mdpi/attachment_gif.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_html

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3340 -w96 -h96 -e iPhone/plus/attachment_html@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3340 -w64 -h64 -e iPhone/retina/attachment_html.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3340 -w32 -h32 -e iPhone/normal/attachment_html.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3340 -w66 -h66 -e Android/hdpi/attachment_html.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3340 -w44 -h44 -e Android/mdpi/attachment_html.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_jpeg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3346 -w96 -h96 -e iPhone/plus/attachment_jpeg@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3346 -w64 -h64 -e iPhone/retina/attachment_jpeg.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3346 -w32 -h32 -e iPhone/normal/attachment_jpeg.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3346 -w66 -h66 -e Android/hdpi/attachment_jpeg.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3346 -w44 -h44 -e Android/mdpi/attachment_jpeg.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_keynote

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3350 -w96 -h96 -e iPhone/plus/attachment_keynote@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3350 -w64 -h64 -e iPhone/retina/attachment_keynote.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3350 -w32 -h32 -e iPhone/normal/attachment_keynote.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3350 -w66 -h66 -e Android/hdpi/attachment_keynote.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3350 -w44 -h44 -e Android/mdpi/attachment_keynote.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_numbers

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3358 -w96 -h96 -e iPhone/plus/attachment_numbers@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3358 -w64 -h64 -e iPhone/retina/attachment_numbers.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3358 -w32 -h32 -e iPhone/normal/attachment_numbers.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3358 -w66 -h66 -e Android/hdpi/attachment_numbers.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3358 -w44 -h44 -e Android/mdpi/attachment_numbers.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_odf_presentation

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3362 -w96 -h96 -e iPhone/plus/attachment_odf_presentation@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3362 -w64 -h64 -e iPhone/retina/attachment_odf_presentation.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3362 -w32 -h32 -e iPhone/normal/attachment_odf_presentation.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3362 -w66 -h66 -e Android/hdpi/attachment_odf_presentation.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3362 -w44 -h44 -e Android/mdpi/attachment_odf_presentation.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_odf_spreadsheet

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3559 -w96 -h96 -e iPhone/plus/attachment_odf_spreadsheet@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3559 -w64 -h64 -e iPhone/retina/attachment_odf_spreadsheet.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3559 -w32 -h32 -e iPhone/normal/attachment_odf_spreadsheet.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3559 -w66 -h66 -e Android/hdpi/attachment_odf_spreadsheet.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3559 -w44 -h44 -e Android/mdpi/attachment_odf_spreadsheet.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_odf_text

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3583 -w96 -h96 -e iPhone/plus/attachment_odf_text@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3583 -w64 -h64 -e iPhone/retina/attachment_odf_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3583 -w32 -h32 -e iPhone/normal/attachment_odf_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3583 -w66 -h66 -e Android/hdpi/attachment_odf_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3583 -w44 -h44 -e Android/mdpi/attachment_odf_text.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_pages

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3589 -w96 -h96 -e iPhone/plus/attachment_pages@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3589 -w64 -h64 -e iPhone/retina/attachment_pages.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3589 -w32 -h32 -e iPhone/normal/attachment_pages.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3589 -w66 -h66 -e Android/hdpi/attachment_pages.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3589 -w44 -h44 -e Android/mdpi/attachment_pages.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_pdf

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w96 -h96 -e iPhone/plus/attachment_pdf@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w64 -h64 -e iPhone/retina/attachment_pdf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w32 -h32 -e iPhone/normal/attachment_pdf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w66 -h66 -e Android/hdpi/attachment_pdf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w44 -h44 -e Android/mdpi/attachment_pdf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3595 -w80 -h80 -e FileBrowser/attachment_pdf.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_png

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3567 -w96 -h96 -e iPhone/plus/attachment_png@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3567 -w64 -h64 -e iPhone/retina/attachment_png.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3567 -w32 -h32 -e iPhone/normal/attachment_png.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3567 -w66 -h66 -e Android/hdpi/attachment_png.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3567 -w44 -h44 -e Android/mdpi/attachment_png.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_powerpoint

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w96 -h96 -e iPhone/plus/attachment_powerpoint@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w64 -h64 -e iPhone/retina/attachment_powerpoint.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w32 -h32 -e iPhone/normal/attachment_powerpoint.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w66 -h66 -e Android/hdpi/attachment_powerpoint.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w44 -h44 -e Android/mdpi/attachment_powerpoint.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3607 -w80 -h80 -e FileBrowser/attachment_powerpoint.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_rtf

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3613 -w96 -h96 -e iPhone/plus/attachment_rtf@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3613 -w64 -h64 -e iPhone/retina/attachment_rtf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3613 -w32 -h32 -e iPhone/normal/attachment_rtf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3613 -w66 -h66 -e Android/hdpi/attachment_rtf.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3613 -w44 -h44 -e Android/mdpi/attachment_rtf.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_text

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3619 -w96 -h96 -e iPhone/plus/attachment_text@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3619 -w64 -h64 -e iPhone/retina/attachment_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3619 -w32 -h32 -e iPhone/normal/attachment_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3619 -w66 -h66 -e Android/hdpi/attachment_text.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3619 -w44 -h44 -e Android/mdpi/attachment_text.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_tiff

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3575 -w96 -h96 -e iPhone/plus/attachment_tiff@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3575 -w64 -h64 -e iPhone/retina/attachment_tiff.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3575 -w32 -h32 -e iPhone/normal/attachment_tiff.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3575 -w66 -h66 -e Android/hdpi/attachment_tiff.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3575 -w44 -h44 -e Android/mdpi/attachment_tiff.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_word

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w96 -h96 -e iPhone/plus/attachment_word@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w64 -h64 -e iPhone/retina/attachment_word.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w32 -h32 -e iPhone/normal/attachment_word.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w66 -h66 -e Android/hdpi/attachment_word.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w44 -h44 -e Android/mdpi/attachment_word.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3631 -w80 -h80 -e FileBrowser/attachment_word.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_xml

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3637 -w96 -h96 -e iPhone/plus/attachment_xml@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3637 -w64 -h64 -e iPhone/retina/attachment_xml.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3637 -w32 -h32 -e iPhone/normal/attachment_xml.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3637 -w66 -h66 -e Android/hdpi/attachment_xml.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3637 -w44 -h44 -e Android/mdpi/attachment_xml.png attachment_icons.svg


REM ---------------------------------------------------------------------
REM attachment_zip

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w96 -h96 -e iPhone/plus/attachment_zip@3x.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w64 -h64 -e iPhone/retina/attachment_zip.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w32 -h32 -e iPhone/normal/attachment_zip.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w66 -h66 -e Android/hdpi/attachment_zip.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w44 -h44 -e Android/mdpi/attachment_zip.png attachment_icons.svg

"/Program Files (x86)/Inkscape/Inkscape.com" --export-background-opacity=0.0 --export-id=rect3643 -w80 -h80 -e FileBrowser/attachment_zip.png attachment_icons.svg


