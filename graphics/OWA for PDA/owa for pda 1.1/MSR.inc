<%
Sub displayAppointment(inMessage)

  Dim d, d2, locArrT, s
  locArrT = displayAppointment2(inMessage)
  writeTitleAndValue DATETEXT & "/" & TIMETEXT, locArrT(0) & vbCrLf & locArrT(1), True
  s = ""
  d = locArrT(2)
  d2 = Int(d / (24 * 60))
  If d2 <> 0 Then
    s = s & d2 & " " & DAYSTEXT & " "
    d = d - d2 * (24 * 60)
  End If
  d2 = Int(d / 60)
  If d2 <> 0 Then
    s = s & d2 & " " & HRSTEXT & " "
    d = d - d2 * 60
  End If
  If d <> 0 Then
    s = s & d & " " & MINTEXT
  End If
  writeTitleAndValue DURATIONTEXT, s, True
  writeTitleAndValue SUBJECTTEXT, locArrT(3), True
  writeTitleAndValue LOCATIONTEXT, locArrT(4), True
  writeUnderline
  doMessageText locArrT(5)
  If (locArrT(6) <> "") Or (locArrT(7) <> "") Or (locArrT(8) <> "") Then writeUnderline
  If locArrT(6) <> "" Then writeTitleAndValue ORGANIZERTEXT, locArrT(6), True
  If Instr(mytype, "Resp") = 0 Then
    If locArrT(7) <> "" Then writeTitleAndValue ATTENDEESTEXT, locArrT(7), True
    If locArrT(8) <> "" Then writeTitleAndValue OPTIONALTEXT, locArrT(8), True
  End If
  writeUnderline
  displayAppointment3 inMessage
End Sub

Function displayAppointment2(inMessage)

  Dim locArrOut(8)
  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<d:outlookmessageclass/>" & _
     "<h:dtstart/>" & _
     "<h:duration/>" & _
     "<h:location/>" & _
     "<h:meetingstatus/>" & _
     "<h:organizer/>" & _
     "<h:uid/>" & _
     "<j:bcc/>" & _
     "<j:cc/>" & _
     "<j:sendername/>" & _
     "<j:subject/>" & _
     "<j:textdescription/>" & _
     "<j:to/>" & _
   "</a:prop></a:propfind>"

  If Left(doWinHTTPPropfind(strBaseURL & inMessage, strXML), 1) = "2" Then

    locClass = getXMLNode("http://schemas.microsoft.com/exchange/", "outlookmessageclass")
    locDtStart = getXMLNode("urn:schemas:calendar:", "dtstart")
    locDuration = getXMLNode("urn:schemas:calendar:", "duration")
    locSender = getXMLNode("urn:schemas:httpmail:", "sendername")
    locSubject = getXMLNode("urn:schemas:httpmail:", "subject")
    locLocation = getXMLNode("urn:schemas:calendar:", "location")
    locMeetingStatus = getXMLNode("urn:schemas:calendar:", "meetingstatus")
    locOrganizer = getXMLNode("urn:schemas:calendar:", "organizer")
    locDetails = getXMLNode("urn:schemas:httpmail:", "textdescription")
    locBcc = getXMLNode("urn:schemas:httpmail:", "bcc")
    locCc = getXMLNode("urn:schemas:httpmail:", "cc")
    locTo = getXMLNode("urn:schemas:httpmail:", "to")
    locUid = getXMLNode("urn:schemas:calendar:", "uid")

    If st0 <> "u" Then

      If Instr(locClass, "IPM.Schedule.Meeting.Request") <> 0 Then

        ad0 = getParam("ad0", False)
        If ad0 = "" Then

          oldAd0 = ad0
          ad0 = 3
          writeLinkWParams "MSR.asp", ACCEPTTEXT, "AD0"
          ad0 = 2
          writeLinkWParams "MSR.asp", TENTATIVETEXT, "AD0"
          ad0 = 4
          writeLinkWParams "MSR.asp", DECLINETEXT, "AD0"
          ad0 = oldAd0

        Else

          Select Case ad0
            Case 3
              locCmd = "accept"
            Case 2
              locCmd = "tentative"
            Case 4
              locCmd = "decline"
          End Select

          strStatus = doWinHTTPGet(strBaseURL & inMessage & "?Cmd=" & locCmd, "")
          If Left(strStatus, 1) = "2" Then
            locT = objWinHTTP.ResponseText
            locP = Instr(UCase(locT), "<BASE HREF=""") + Len("<BASE HREF=""")
            locP2 = Instr(locP, UCase(locT), ".EML") + Len(".EML")
            locT2 = Mid(locT, locP, locP2 - locP)
            locT3 = Right(locT2, Len(locT2) - Len(strMailboxURL))
            doWinHTTPMove strMailboxURL & locT3, strMailboxURL & "%23%23DavMailSubmissionURI%23%23"
          Else
            rwbc ERRORTEXT : Response.End
          End If

        End If

      ElseIf Instr(locClass, "IPM.Schedule.Meeting.Resp") <> 0 Then

        writeTitle FROMTEXT & COLONSPACETEXT
        rwbc displayEncode0(locSender)
        writeTitle STATUSTEXT & COLONSPACETEXT
        If Instr(locClass, ".Pos") <> 0 Then
          rwbc displayEncode0(ACCEPTTEXT)
        ElseIf Instr(locClass, ".Tent") <> 0 Then
          rwbc displayEncode0(TENTATIVETEXT)
        ElseIf Instr(locClass, ".Neg") <> 0 Then
          rwbc displayEncode0(DECLINETEXT)
        End If
        ' This GET updates the Appointment attendee status.
        doWinHTTPGet strBaseURL & inMessage & "?Cmd=open", ""

      ElseIf Instr(locClass, "IPM.Appointment") <> 0 Then

        If em0 = "" Then
          If locMeetingStatus <> "TENTATIVE" Then
            If locOrganizer <> "" Then
              locArrAddress = getAddresses(locOrganizer)
              If (UCase(locArrAddress(1, 0)) = UCase(strSMTPAddress)) Then writeLinkWParams "MRE.asp", INVITETEXT, ""
            End If
          Else
            writeLinkWParams "MRE.asp", INVITETEXT, ""
          End If
        End If

      End If

    End If
    locT = getDateTimeFromTZ(locDtStart)
    locArrOut(0) = FormatDateTime(locT, vbLongDate)
    arrT = Split(locT, " ")
    If UBound(arrT) >= 1 Then
      tm = arrT(1)
    Else
      tm = "00:00:00"
    End If
    If UBound(arrT) >= 2 Then tm = tm & " " & arrT(2)
    locArrOut(1) = tm
    'locArrOut(1) = FormatDateTime(locT, vbShortTime)
    locArrOut(2) = locDuration / 60
    locArrOut(3) = locSubject
    locArrOut(4) = locLocation
    locArrOut(5) = locDetails
    locArrOut(6) = locOrganizer
    locArrOut(7) = locTo
    locArrOut(8) = locCc

  End If
  displayAppointment2 = locArrOut

End Function

Sub displayAppointment3(inMessage)
  strStatus = doWinHTTPGet(strBaseURL & inMessage, "f")
  If Left(strStatus, 1) = "2" Then
    Dim s
    s = objWinHTTP.ResponseText
    Dim arrAttendees
    arrAttendees = Split(s, "ATTENDEE;")
    Dim n
    n = UBound(arrAttendees)
    ReDim arrS(n - 1, 1)
    Dim i
    For i = 1 To n
      Dim s2
      s2 = arrAttendees(i)
      s2 = Replace(s2, vbCrLf & " ", "")
      Dim p
      p = Instr(s2, "MAILTO:")
      If p <> 0 Then
        p = p + 7
        Dim p2
        p2 = Instr(p, s2, vbCrLf)
        If p2 <> 0 Then
          Dim s3
          s3 = Mid(s2, p, p2 - p)
          p = Instr(s2, "PARTSTAT=")
          If p <> 0 Then
            p = p + 9
            p2 = Instr(p, s2, ";")
            If p2 <> 0 Then
              Dim s4
              s4 = Mid(s2, p, p2 - p)
              arrS(i - 1, 0) = s3
              arrS(i - 1, 1) = s4
            End If
          End If
        End If
      End If
    Next
    writeTitle ACCEPTTEXT & COLONSPACETEXT
    For i = 0 To n - 1
      If arrS(i, 1) = "ACCEPTED" Then rwbc arrS(i, 0)
    Next
    writeTitle TENTATIVETEXT & COLONSPACETEXT
    For i = 0 To n - 1
      If arrS(i, 1) = "TENTATIVE" Then rwbc arrS(i, 0)
    Next
    writeTitle DECLINETEXT & COLONSPACETEXT
    For i = 0 To n - 1
      If arrS(i, 1) = "DECLINED" Then rwbc arrS(i, 0)
    Next
  End If
End Sub

Sub displayContact(inMessage)

  Dim locArrT
  locArrT = displayContact2(inMessage)
  writeTitleAndValue NAMETEXT, locArrT(0), True
  writeTitleAndValue TITLETEXT, locArrT(1), True
  writeTitleAndValue COMPANYTEXT, locArrT(2), True
  writeTitleAndValue DEPARTMENTTEXT, locArrT(3), True
  writeTitle EMAILTEXT & COLONSPACETEXT
  strEmail = locArrT(4)
  If strEmail <> "" Then
    mt0 = OPWCMN + OPWFRFO
    oldRe0 = re0 : re0 = strEmail
    writeLinkWParams "MSE.asp", strEmail, "MT0,RE0"
    re0 = oldRe0
  End If
  writeTitleAndPhoneLink BUSINESSPHONETEXT, locArrT(5)
  writeTitleAndPhoneLink HOMEPHONETEXT, locArrT(6)
  writeTitleAndPhoneLink MOBILETEXT, locArrT(7)
  writeTitleAndValue BUSINESSFAXTEXT, locArrT(8), True
  writeTitleAndValue BUSINESSADDRESSTEXT, locArrT(9), True
  If locArrT(10) <> "" Then
    writeUnderline
    doMessageText locArrT(10)
  End If

End Sub

Function displayContact2(inMessage)

  Dim locArrOut(10)
  Dim strName
  strXML = "<a:propfind " & strXMLNS & _
   " xmlns:n='http://schemas.microsoft.com/mapi/id/{00062004-0000-0000-C000-000000000046}/'" & _
   ">" & _
   "<a:prop>" & _
     "<i:cn/>" & _
     "<i:department/>" & _
     "<i:facsimiletelephonenumber/>" & _
     "<i:homePhone/>" & _
     "<i:mobile/>" & _
     "<i:workaddress/>" & _
     "<i:o/>" & _
     "<i:telephoneNumber/>" & _
     "<i:title/>" & _
     "<j:textdescription/>" & _
     "<n:0x8084/>" & _
   "</a:prop></a:propfind>"

  If Left(doWinHTTPPropfind(strBaseURL & inMessage, strXML), 1) = "2" Then

    locArrOut(0) = getXMLNode("urn:schemas:contacts:", "cn")
    locArrOut(1) = getXMLNode("urn:schemas:contacts:", "title")
    locArrOut(2) = getXMLNode("urn:schemas:contacts:", "o")
    locArrOut(3) = getXMLNode("urn:schemas:contacts:", "department")
    If Instr(myType, "IPM.DistList") = 0 Then
      locArrOut(4) = getXMLNode("http://schemas.microsoft.com/mapi/id/{00062004-0000-0000-C000-000000000046}/", "0x8084")
    Else
      locArrOut(4) = getXMLNode("urn:schemas:contacts:", "cn")
    End If
    locArrOut(5) = getXMLNode("urn:schemas:contacts:", "telephoneNumber")
    locArrOut(6) = getXMLNode("urn:schemas:contacts:", "homePhone")
    locArrOut(7) = getXMLNode("urn:schemas:contacts:", "mobile")
    locArrOut(8) = getXMLNode("urn:schemas:contacts:", "facsimiletelephonenumber")
    locArrOut(9) = getXMLNode("urn:schemas:contacts:", "workaddress")
    locArrOut(10) = getXMLNode("urn:schemas:httpmail:", "textdescription")

    If Instr(myType, "IPM.DistList") <> 0 Then
      strURL = getXMLNode("DAV:", "href")
      strStatus = doWinHTTPGet(strURL & "/?Cmd=open", "")
      If Left(strStatus, 1) = "2" Then
        strResponse = objWinHTTP.ResponseText
        strResponse2 = UCase(strResponse)
        p = Instr(strResponse2, "MEMBERROWS")
        If p <> 0 Then
          p2 = Instr(p, strResponse2, "</TABLE>")
          p = Instr(p, strResponse2, "</TR>")
          Do While (p <> 0) And (p < p2)
            p = InstrRev(strResponse2, "<TD", p)
            p = Instr(p, strResponse2, ">") + 1
            p3 = Instr(p, strResponse2, "</TD>")
            strAddress = Mid(strResponse, p, p3 - p)
            strAddress = Replace(strAddress, "&nbsp;", "")
            If strDLNames <> "" Then strDLNames = strDLNames & ", "
            strDLNames = strDLNames & strAddress
            p = Instr(p3, strResponse2, "</TR>")
            p = Instr(p + 1, strResponse2, "</TR>")
          Loop
        End If
      End If
      strName = locArrOut(0) & " (DL):" & vbCrLf & strDLNames
      locArrOut(0) = strName
    End If

  End If
  displayContact2 = locArrOut

End Function

Sub displayMessage(inMessage)

  Dim locArrT
  locArrT = displayMessage2(inMessage)
  oldmi0 = mi0
  If locArrT(0) <> "" Then
    mi0 = locArrT(0)
    writeLinkWParams "MSR.asp", PREVIOUSMESSAGETEXT, "MI0,NOBR,MN0"
  End If
  If locArrT(1) <> "" Then
    mi0 = locArrT(1)
    writeLinkWParams "MSR.asp", NEXTMESSAGETEXT, "MI0,NOBR,MN0"
  End If
  mi0 = oldmi0
  hb0 = getParam("hb0", False)
  oldHb0 = hb0
  If Not((oldHb0 And 1) <> 0) Then
    hb0 = oldHb0 Or 1
    If (HTMLBODYCONTROL And 1) = 0 Then
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "HTML", "HB0,NOBR"
      blnShowHTML = False
    Else
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "TXT", "HB0,NOBR"
      blnShowHTML = True
    End If
  Else
    hb0 = oldHb0 And Not 1
    If (HTMLBODYCONTROL And 1) = 0 Then
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "TXT", "HB0,NOBR"
      blnShowHTML = True
    Else
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "HTML", "HB0,NOBR"
      blnShowHTML = False
    End If
  End If
  hb0 = oldHb0
  writeLinkWParams "MSRAU.asp", MARKUNREADTEXT, ""
  rwbc ""
  ' Are we in Sent Items?
  inSentItems = False
  If Left(fi0, Len(strSentItemsURL)) = strSentItemsURL Then inSentItems = True
  If Not inSentItems Then
    writeTitleAndValue FROMTEXT, locArrT(2), False
  End If
  maxRecipientsLength = 500
  strTo = locArrT(3)
  If Len(strTo) < maxRecipientsLength Then
    writeTitleAndValue TO1TEXT, strTo, False
  Else
    writeTitle TO1TEXT & COLONSPACETEXT
    arrT = Split(strTo, ";")
    n = UBound(arrT) + 1
    p = InstrRev(strTo, ";", maxRecipientsLength)
    strTo2 = Left(strTo, p - 1)
    arrT = Split(strTo2, ";")
    n2 = UBound(arrT) + 1
    rwbc displayEncode0("(" & n2 & " " & OFTEXT & " " & n & " " & RECIPIENTSTEXT & ")")
    rwbc displayEncode0(strTo2)
    oldPg0 = pg0 : pg0 = "R"
    writeLinkWParams "MSRAR.asp", VIEWRECIPIENTSTEXT, "PG0"
    pg0 = oldPg0
  End If
  strCc = locArrT(8)
  If Len(strCc) < maxRecipientsLength Then
    writeTitleAndValue CCTEXT, strCc, True
  Else
    writeTitle CCTEXT & COLONSPACETEXT
    arrT = Split(strCc, ";")
    n = UBound(arrT) + 1
    p = InstrRev(strCc, ";", maxRecipientsLength)
    strCc2 = Left(strCc, p - 1)
    arrT = Split(strCc2, ";")
    n2 = UBound(arrT) + 1
    rwbc displayEncode0("(" & n2 & " " & OFTEXT & " " & n & " " & RECIPIENTSTEXT & ")")
    rwbc displayEncode0(strCc2)
    oldPg0 = pg0 : pg0 = "R"
    writeLinkWParams "MSRAR.asp", VIEWRECIPIENTSTEXT, "PG0"
    pg0 = oldPg0
  End If
  strBcc = locArrT(9)
  If Len(strBcc) < maxRecipientsLength Then
    writeTitleAndValue BCCTEXT, strBcc, True
  Else
    writeTitle BCCTEXT & COLONSPACETEXT
    arrT = Split(strBcc, ";")
    n = UBound(arrT) + 1
    p = InstrRev(strBcc, ";", maxRecipientsLength)
    strBcc2 = Left(strBcc, p - 1)
    arrT = Split(strBcc2, ";")
    n2 = UBound(arrT) + 1
    rwbc displayEncode0("(" & n2 & " " & OFTEXT & " " & n & " " & RECIPIENTSTEXT & ")")
    rwbc displayEncode0(strBcc2)
    oldPg0 = pg0 : pg0 = "R"
    writeLinkWParams "MSRAR.asp", VIEWRECIPIENTSTEXT, "PG0"
    pg0 = oldPg0
  End If
  writeTitleAndValue RECEIVEDTEXT, locArrT(10), False
  writeTitleAndValue SUBJECTTEXT, locArrT(4), True
  If locArrT(5) = 2 Then writeTitleAndValue IMPORTANCETEXT, HIGHTEXT, True
  writeLinkWParams "MSRA.asp", ACTIONSTEXT, "NOBR"
  oldMt0 = mt0
  mt0 = OPWCMR + OPWFRMR
  writeLinkWParams "MSE.asp", REPLYTEXT, "MT0,NOBR"
  mt0 = OPWCMA + OPWFRMR
  writeLinkWParams "MSE.asp", REPLYTOALLTEXT, "MT0,NOBR"
  mt0 = OPWCMF + OPWFRMR
  writeLinkWParams "MSE.asp", FORWARDTEXT, "MT0"
  mt0 = oldMt0
  writeUnderline
  If Not blnShowHTML Then
    doMessageText locArrT(6)
  Else
    rwbc myUTF8ToAsciiH(locArrT(7))
  End If

End Sub

Function displayMessage2(inMessage)

  Dim locArrOut(10), locArrT

  locArrout(0) = strPreviousHref
  locArrOut(1) = strNextHref

  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<j:datereceived/>" & _
     "<j:displaybcc/>" & _
     "<j:displaycc/>" & _
     "<j:displayto/>" & _
     "<j:htmldescription/>" & _
     "<j:importance/>" & _
     "<j:sendername/>" & _
     "<j:subject/>" & _
     "<j:textdescription/>" & _
   "</a:prop></a:propfind>"
  strStatus = doWinHTTPPropfind(strBaseURL & inMessage, strXML)
  If Left(strStatus, 1) = "2" Then
    locArrOut(2) = getXMLNode("urn:schemas:httpmail:", "sendername")
    locArrOut(3) = getXMLNode("urn:schemas:httpmail:", "displayto")
    locArrOut(4) = getXMLNode("urn:schemas:httpmail:", "subject")
    locArrOut(5) = getXMLNode("urn:schemas:httpmail:", "importance")
    locArrOut(6) = getXMLNode("urn:schemas:httpmail:", "textdescription")
    locArrOut(7) = getXMLNode("urn:schemas:httpmail:", "htmldescription")
    locArrOut(8) = getXMLNode("urn:schemas:httpmail:", "displaycc")
    locArrOut(9) = getXMLNode("urn:schemas:httpmail:", "displaybcc")
    locArrOut(10) = getDateTimeFromTZ(getXMLNode("urn:schemas:httpmail:", "datereceived"))
  End If
  displayMessage2 = locArrOut

End Function

Sub displayNote(inMessage)

  Dim locArrT
  locArrT = displayNote2(inMessage)
  writeTitle NOTETEXT & COLONSPACETEXT
  doMessageText locArrT(0)

End Sub

Function displayNote2(inMessage)

  Dim locArrOut(0)
  strXML = "<a:propfind " & strXMLNS & ">" & _
   "<a:prop>" & _
     "<g:0x1000001F/>" & _
   "</a:prop></a:propfind>"
  If Left(doWinHTTPPropfind(strBaseURL & inMessage, strXML), 1) = "2" Then
    locArrOut(0) = getXMLNode("http://schemas.microsoft.com/mapi/proptag/", "0x1000001F")
  End If
  displayNote2 = locArrOut

End Function

Sub displayPost(inMessage)

  Dim locArrT
  locArrT = displayPost2(inMessage)
  hb0 = getParam("hb0", False)
  oldHb0 = hb0
  If Not((oldHb0 And 1) <> 0) Then
    hb0 = oldHb0 Or 1
    If (HTMLBODYCONTROL And 1) = 0 Then
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "HTML", "HB0,NOBR"
      blnShowHTML = False
    Else
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "TXT", "HB0,NOBR"
      blnShowHTML = True
    End If
  Else
    hb0 = oldHb0 And Not 1
    If (HTMLBODYCONTROL And 1) = 0 Then
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "TXT", "HB0,NOBR"
      blnShowHTML = True
    Else
      If (HTMLBODYCONTROL And 2) <> 0 Then writeLinkWParams "MSR.asp", "HTML", "HB0,NOBR"
      blnShowHTML = False
    End If
  End If
  hb0 = oldHb0
  rwbc ""
  writeTitleAndValue FROMTEXT, locArrT(0), True
  writeTitleAndValue SUBJECTTEXT, locArrT(1), True
  writeUnderline
  If Not blnShowHTML Then
    doMessageText locArrT(2)
  Else
    rwbc myUTF8ToAsciiH(locArrT(3))
  End If

End Sub

Function displayPost2(inMessage)

  Dim locArrOut(3)
  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<j:htmldescription/>" & _
     "<j:sendername/>" & _
     "<j:subject/>" & _
     "<j:textdescription/>" & _
   "</a:prop></a:propfind>"
  locStatus = doWinHTTPPropfind(strBaseURL & inMessage, strXML)
  'rwbc displayEncode0(objWinHTTP.ResponseText)
  If Left(locStatus, 1) = "2" Then
    locArrOut(0) = getXMLNode("urn:schemas:httpmail:", "sendername")
    locArrOut(1) = getXMLNode("urn:schemas:httpmail:", "subject")
    locArrOut(2) = getXMLNode("urn:schemas:httpmail:", "textdescription")
    locArrOut(3) = getXMLNode("urn:schemas:httpmail:", "htmldescription")
  End If
  displayPost2 = locArrOut

End Function

Sub displayTask(inMessage)

  Dim locArrT
  locArrT = displayTask2(inMessage)
  writeTitleAndValue SUBJECTTEXT, locArrT(0), True
  t = locArrT(1)
  Select Case t
    Case 0
      t2 = LOWTEXT
    Case 1
      t2 = NORMALTEXT
    Case 2
      t2 = HIGHTEXT
  End Select
  writeTitleAndValue IMPORTANCETEXT, t2, True
  t = locArrT(2)
  Select Case t
    Case 0
      t2 = OPENTEXT
    Case 1
      t2 = INPROGRESSTEXT
    Case 2
      t2 = COMPLETEDTEXT
    Case 3
      t2 = WAITINGONSOMEONEELSETEXT
    Case 4
      t2 = DEFERREDTEXT
  End Select
  writeTitleAndValue STATUSTEXT, t2, True
  t = locArrT(3)
  writeTitleAndValue PERCENTCOMPLETETEXT, 100 * t, True
  writeTitleAndValue STARTDATETEXT, locArrT(4), True
  writeTitleAndValue DUEDATETEXT, locArrT(5), True
  writeUnderline
  doMessageText locArrT(6)

End Sub

Function displayTask2(inMessage)

  Dim locArrOut(6), t
  strXML = "<a:propfind " & strXMLNS & _
   " xmlns:n='http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/'" & _
   ">" & _
   "<a:prop>" & _
     "<j:importance/>" & _
     "<j:subject/>" & _
     "<j:textdescription/>" & _
     "<n:0x8101/>" & _
     "<n:0x8102/>" & _
     "<n:0x8104/>" & _
     "<n:0x8105/>" & _
   "</a:prop></a:propfind>"
  strStatus = doWinHTTPPropfind(strBaseURL & inMessage, strXML)
  'dwr
  If Left(strStatus, 1) = "2" Then
    locArrOut(0) = getXMLNode("urn:schemas:httpmail:", "subject")
    locArrOut(1) = getXMLNode("urn:schemas:httpmail:", "importance")
    locArrOut(2) = getXMLNode("http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/", "0x8101")
    locArrOut(3) = getXMLNode("http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/", "0x8102")
    t = getXMLNode("http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/", "0x8104")
    If t <> "" Then t = getGMTDateTimeFromTZ(t)
    locArrOut(4) = t
    t = getXMLNode("http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/", "0x8105")
    If t <> "" Then t = getGMTDateTimeFromTZ(t)
    locArrOut(5) = t
    locArrOut(6) = getXMLNode("urn:schemas:httpmail:", "textdescription")
  End If
  displayTask2 = locArrOut

End Function

Sub doAttachments(inMessage)

  Dim blnShowLink, lngAttachSize
  'If OPENATTACHMENTS Then
    strStatus = doWinHTTPXMsEnumatts(strBaseURL & inMessage)
    'dwr
    If Left(strStatus, 1) = "2" Then
      Session("mb0") = mb0
      If Instr(UCase(objWinHTTP.ResponseText), ":ATTACHMENTFILENAME>") <> 0 Then
        locArrT = getXMLNodes("urn:schemas:httpmail:", "attachmentfilename")
      ElseIf Instr(UCase(objWinHTTP.ResponseText), ":X3704001E>") <> 0 Then
        locArrT = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x3704001e")
      ElseIf Instr(UCase(objWinHTTP.ResponseText), ":X3704001F>") <> 0 Then
        locArrT = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x3704001f")
      End If
      locArrT2 = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x0e210003")
      locArrT3 = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x0e200003")
      locArrT4 = getXMLNodes("DAV:", "href")
      If SHOWERRORS Then On Error Resume Next
      locD = UBound(locArrT)
      errNumber = Err.Number
      If SHOWERRORS Then On Error Goto 0
      If errNumber = 0 Then

  arrAllowFileTypes = Split(ALLOWATTACHMENTS, ",")

        For locN = LBound(locArrT) To UBound(locArrT)
          writeTitle ATTACHMENTTEXT & COLONSPACETEXT
          display = locArrT(locN)
          lngAttachSize = CLng(locArrT3(locN))

          blnShowLink = False


    For i2 = 0 To UBound(arrAllowFileTypes)
      strFileType = arrAllowFileTypes(i2)
      If (strFileType <> "") And (strFileType = "*" Or (Right(display, Len(strFileType)) = strFileType)) Then
        blnShowLink = True
      End If
    Next

'          blnShowLink = True
          If LIMITATTACHSIZEKB <> 0 Then
            If (lngAttachSize / 1024) > LIMITATTACHSIZEKB Then blnShowLink = False
          End If
          If blnShowLink Then
            If Not RELAYATTACHMENTS Then
              myURL = locArrT4(locN)
              locP = Instr(myURL, "//") + 2
              locP2 = Instr(locP, myURL, "/Exchange/")
              If locP2 = 0 Then locP2 = Instr(locP, myURL, "/Public/")
              If locP2 <> 0 Then myURL = Right(myURL, Len(myURL) - (locP2 - 1))
              writeLinkWParams myURL, display, "NOBR"
            Else
              's = myURLEncode(display)
              s = displayEncodeI0(display)
              ai0 = CStr(locArrT2(locN) + 1)
              writeLinkWParams s, display, "AI0,NOBR"

              's = Server.HTMLEncode(display)
              ''s = myURLEncode(display)
              'rw s & " "
              'ai0 = locArrT2(locN) + 1
              'writeLinkWParams GOTEXT, GOTEXT, "AI0,NOBR"
            End If
          Else
            rw display
          End If
          rwbc "&nbsp;(" & CLng(lngAttachSize / 1024) & displayEncode0(KILOBYTESTEXT) & ")"
        Next
      End If
    End If
  'End If

End Sub

Sub doMessageText(inText)

  Dim b, c, i, i2, l, p, p2, s
  l = Len(inText) : p2 = 0
  For i = 1 To pg1
    p = p2 + 1
    If p2 + (intMaxPageSize - 1) < l Then
      p2 = p + (intMaxPageSize - 1)
      b = False
      For i2 = p2 To (p + 1) Step - 1
        c = Mid(inText, i2, 1)
        If (c = " ") Or (c = vbCrLf) Or (c = vbTab) Then
          b = True
          Exit For
        End If
      Next
      If b Then p2 = i2
    Else
      p2 = l
    End If
  Next
  s = Mid(inText, p, (p2 - p) + 1)
  s = displayEncode0(s)
  s = Replace(s, "@", "<code>@</code>")
  rwbc s
  If p2 < l Then
    pg1 = pg1 + 1
    writeLinkWParams "MSR.asp", NEXTTEXT, "PG1"
  End If

End Sub

Function myURLEncode(inText)

  Dim c, i, n, s
  s = ""
  For i = 1 To LenB(inText) Step 2
    c = MidB(inText, i, 1)
    n = AscB(c)
    'If n < 128 Then
      s = s & Chr(n)
    'Else
    '  s = s & "%" & Hex(n)
    'End If
  Next
  s = myURLEncode2(s, "%")
  s = myURLEncode2(s, " ")
  s = myURLEncode2(s, "/")
  s = myURLEncode2(s, "\")
  s = myURLEncode2(s, ":")
  s = myURLEncode2(s, "?")
  s = myURLEncode2(s, "<")
  s = myURLEncode2(s, ">")
  s = myURLEncode2(s, """")
  s = myURLEncode2(s, "&")
  myURLEncode = s

End Function

Function myURLEncode2(inText, inChar)
  Dim s
  s = inText
  s = Replace(s, inChar, "%" & Right("0" & CStr(Hex(Asc(inChar))), 2))
  myURLEncode2 = s
End Function

Function myUTF8ToAsciiH(inString)

  ' Convert UTF8 to ASCII, but don't HTMLEncode non-extended characters
  Dim b, c, c2, c3, c4, c5, c6, i, i2, l, n, n2, n3, n4, n5, n6, s, t
  s = "" : l = LenB(inString) : i = 1 : b = False
  Do While i <= l
    If AscB(MidB(inString, i, 1)) > 127 Then : b = True : Exit Do
    i = i + 2
  Loop
  If b Then
    s = Left(inString, (i - 1) / 2)
    Do While i <= l
      c = MidB(inString, i, 1) : n = AscB(c)
      i2 = 0
      If n >= 192 Then
        If n <= 223 Then
          If i <= (l - 2) Then
            c2 = MidB(inString, i + 2, 1) : n2 = AscB(c2)
            If (n2 And &HC0) = &H80 Then
              t = (n - 192) * 64 + (n2 - 128)
              i2 = 2
            End If
          End If
        ElseIf n <= 239 Then
          If i <= (l - 4) Then
            c2 = MidB(inString, i + 2, 1) : n2 = AscB(c2) : c3 = MidB(inString, i + 4, 1) : n3 = AscB(c3)
            If ((n2 And &HC0) = &H80) And ((n3 And &HC0) = &H80) Then
              t = (n - 224) * 4096 + (n2 - 128) * 64 + (n3 - 128)
              i2 = 4
            End If
          End If
        ElseIf n <= 247 Then
          If i <= (l - 6) Then
            c2 = MidB(inString, i + 2, 1) : n2 = AscB(c2) : c3 = MidB(inString, i + 4, 1) : n3 = AscB(c3)
            c4 = MidB(inString, i + 6, 1) : n4 = AscB(c4)
            If ((n2 And &HC0) = &H80) And ((n3 And &HC0) = &H80) And ((n4 And &HC0) = &H80) Then
              t = (n - 240) * 262144 + (n2 - 128) * 4096 + (n3 - 128) * 64 + (n4 - 128)
              i2 = 6
            End If
          End If
        ElseIf n <= 251 Then
          If i <= (l - 8) Then
            c2 = MidB(inString, i + 2, 1) : n2 = AscB(c2) : c3 = MidB(inString, i + 4, 1) : n3 = AscB(c3)
            c4 = MidB(inString, i + 6, 1) : n4 = AscB(c4) : c5 = MidB(inString, i + 8, 1) : n5 = AscB(c5)
            If ((n2 And &HC0) = &H80) And ((n3 And &HC0) = &H80) And ((n4 And &HC0) = &H80) _
             And ((n5 And &HC0) = &H80) Then
              t = (n - 248) * 16777216 + (n2 - 128) * 262144 + (n3 - 128) * 4096 + (n4 - 128) * 64 + (n5 - 128)
              i2 = 8
            End If
          End If
        ElseIf n <= 253 Then
          If i <= (l - 10) Then
            c2 = MidB(inString, i + 2, 1) : n2 = AscB(c2) : c3 = MidB(inString, i + 4, 1) : n3 = AscB(c3)
            c4 = MidB(inString, i + 6, 1) : n4 = AscB(c4) : c5 = MidB(inString, i + 8, 1) : n5 = AscB(c5)
            c6 = MidB(inString, i + 10, 1) : n6 = AscB(c6)
            If ((n2 And &HC0) = &H80) And ((n3 And &HC0) = &H80) And ((n4 And &HC0) = &H80) _
             And ((n5 And &HC0) = &H80) And ((n6 And &HC0) = &H80) Then
              t = (n - 252) * 1073741824 + (n2 - 128) * 16777216 + (n3 - 128) * 262144 + (n4 - 128) * 4096 + (n5 - 128) * 64 + (n6 - 128)
              i2 = 10
            End If
          End If
        End If
      End If
      If i2 <> 0 Then
        s = s & "&#" & t & ";"
        i = i + i2
      Else
        s = s & Chr(n)
      End If
      i = i + 2
    Loop
    myUTF8ToAsciiH = s
  Else
    myUTF8ToAsciiH = inString
  End If

End Function

Sub writeTitleAndValue(inTitle, inValue, inBR)
  If Trim(inValue) <> "" Then
    rw "<b>" & displayEncode0(inTitle) & COLONSPACETEXT & "</b>"
    If inBR Then rwbc ""
    If Instr(inValue, "@") = 0 Then
      rwbc displayEncode0(inValue)
    Else
      rwbc "<code>" & displayEncode0(inValue) & "</code>"
    End If
  End If
End Sub

Sub writeTitleAndPhoneLink(inTitle, inValue)
  If Trim(inValue) <> "" Then
    writeTitle inTitle & COLONSPACETEXT
    If Not blnUseTelLinks Then
      rwbc displayEncode0(inValue)
    Else
      rwbc "<a href=""tel:" & Replace(Server.URLEncode(inValue), "+", " ") & """>" & displayEncode0(inValue) & "</a>"
    End If
  End If
End Sub
%>
