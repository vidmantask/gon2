<% @ LANGUAGE = "VBScript" %>
<!--#include file="Define.inc"-->
<!--#include file="Lang.inc"-->
<!--#include file="Include.inc"-->
<% writeHeaders %>

<!-- OWA For PDA COPYRIGHT 2001 ~ 2010 LEE DERBYSHIRE -->

<%
If Not SHOWERRORS Then On Error Resume Next

strDbg = Request("dbg")
Dim strWide
strWide = Request("wide")

writePageStart "v" & VERSION & " - Logon"
centred = True
writeBody

reauth = Request.QueryString("reauth")
If Not CUSTOMFBA Then
  If reauth <> "" Then
    If Application(reauth) = "" Then
      Application(reauth) = "Y"
      Response.Status = "401 Unauthorized"
      Response.Flush
    Else
      Application(reauth) = ""
    End If
  End If
Else
  If CUSTOMFBASSL And (UCase(Request.ServerVariables("HTTPS")) <> "ON") Then
    rwbc "Application requires HTTPS when using CUSTOMFBA = True"
    writePageEnd
    Response.End
  End If
  If reauth = CStr(Session("lastReauth")) Then Session("OPWLOGON") = "Y"
End If

If USELOGINNAME Then

  un0 = Request.QueryString("un0")
  If un0 = "" Then
    If (CUSTOMFBAALLOWSAVE = 1) Or (CUSTOMFBAALLOWSAVE = 2) Then un0 = Request.Cookies("un0")
  End If
  pw0 = Request.QueryString("pw0")
  If pw0 = "" Then
    If (CUSTOMFBAALLOWSAVE = 2) Then pw0 = Request.Cookies("pw0")
  End If
  If un0 = "" Then
    mb0 = Request.ServerVariables("LOGON_USER")
  Else
    Session("OPWLOGONUSER") = un0
    If pw0 <> "" Then Session("OPWLOGON") = pw0
    mb0 = un0
  End If
  p = InstrRev(mb0, "\")
  If p = 0 Then p = InstrRev(mb0, "/")
  If p <> 0 Then mb0 = Right(mb0, Len(mb0) - p)
  If SHOWERRORS Then On Error Resume Next
  Set fso = CreateObject("Scripting.FileSystemObject")
  If Err.Number = 0 Then
    Set f = fso.OpenTextFile(Server.MapPath("Logins.asp"))
    If Err.Number = 0 Then
      Do While Not f.AtEndOfStream
        l = Trim(f.ReadLine)
        If (l <> "") And (Left(l, 1) <> "#") Then
          p = Instr(l, ":")
          If p <> 0 Then
            u = Trim(Left(l, p - 1))
            m = Trim(Right(l, Len(l) - p))
            If LCase(u) = LCase(mb0) Then mb0 = m
          End If
        End If
      Loop
      f.Close
      Set f = Nothing
    End If
    Set fso = Nothing
  End If
  If SHOWERRORS Then On Error Goto 0
  If mb0 = "*" Then mb0 = ""

  If (Not MAILBOXPROMPT) And (mb0 <> "") And ((un0 = "") = (pw0 = "")) Then
    'rw "<meta http-equiv='refresh' content='0; URL=MBX.asp?mb0=" & Server.URLEncode(mb0) & "'>"
    If Not STARTININBOX Then
      strURL = "MBX.asp?mb0=" & Server.URLEncode(mb0)
    Else
      strURL = "MSL.asp?mb0=" & Server.URLEncode(mb0) & "&fi0=OPWINBOX"
    End If
    If strDbg <> "" Then strURL = strURL & "&dbg=" & strDbg
    If strWide <> "" Then strURL = strURL & "&wide=" & strWide
    Response.Redirect strURL
  End If

End If

If Not STARTININBOX Then
  rwc "<form action='MBX.asp' method='post'>"
Else
  rwc "<form action='MSL.asp' method='post'>"
    rwc "<input type='hidden' name='fi0' value='OPWINBOX'>"
End If

  If strDbg <> "" Then rwc "<input type='hidden' name='dbg' value='" & strDbg & "'>"
  If strWide <> "" Then rwc "<input type='hidden' name='wide' value='" & strWide & "'>"
  rwc "<p>"
  writeUnderline
  writeTitle "OWA For PDA " & VERSION
  rwbc "leederbyshire.com"
  writeUnderline
  If BANNERTEXT <> "" Then
    rwbc BANNERTEXT
  Else
    rwc "<p>"
  End If

  If Not CUSTOMFBA Then
    rwbc displayEncode0(MAILBOXNAMETEXT & COLONSPACETEXT)
    rwbc "<input type=""text"" class=""inputText"" name='mb0' size='12' value='" & mb0 & "'>"
  Else
    rwbc displayEncode0(USERNAMETEXT & COLONSPACETEXT)
    rwbc "<input type=""text"" class=""inputText"" name='un0' size='12' value=""" & un0 & """>"
    If (CUSTOMFBAALLOWSAVE = 1) Then
      rw "<input type=""checkbox"" name=""sc0"""
      If Request.Cookies("sc0") = "Y" Then rw " checked"
      rwc ">"
      rwbc SAVETEXT
    End If
    rwbc displayEncode0(PASSWORDTEXT & COLONSPACETEXT)
    rwbc "<input type='password' name='pw0' size='12'>"
    If (CUSTOMFBAALLOWSAVE = 2) Then
      rw "<input type=""checkbox"" name=""sc0"""
      If Request.Cookies("sc0") = "Y" Then rw " checked"
      rwc ">"
      rwbc SAVETEXT
    End If
    If MAILBOXPROMPT Then
      rwbc displayEncode0(MAILBOXNAMETEXT & COLONSPACETEXT)
      rwbc "<input type=""text"" class=""inputText"" name='mb0' size='12'>"
    End If
  End If
  doInputSubmit "", "    " & OKTEXT & "    ", True

rwc "</form>"

writePageEnd
%>
