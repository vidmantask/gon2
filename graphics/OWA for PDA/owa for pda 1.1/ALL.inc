<%
Sub doAddressLists

'  Dim arrAddressBookRoots, objCmd, objConn, objRs, p, s, s2, strAddressListsRoot
'
'  Set objConn = Server.CreateObject("ADODB.Connection")
'  objConn.Provider = "ADsDSOObject"
'  objConn.Properties("ADSI Flag") = 33
'  If Not CUSTOMFBA Then
'    objConn.Open "ADs Provider"
'  Else
'    objConn.Open "ADs Provider", strLogonUser, strPassword
'  End If
'  Set objCmd = Server.CreateObject("ADODB.Command")
'  objCmd.ActiveConnection = objConn
'
'  strSQL = "<" & strLDAPProtocol & "CN=Microsoft Exchange,CN=Services," & strLDAPConfigPath & ">;(objectClass=msExchConfigurationContainer);addressBookRoots"
'  objCmd.CommandText = strSQL
'  Set objRs = objCmd.Execute
'
'  If Not(objRs.BOF And objRs.EOF) Then
'    arrAddressBookRoots = objRs("addressBookRoots")
'    strAddressListsRoot = arrAddressBookRoots(0)
'    p = Instr(strAddressListsRoot, "CN=Address Lists Container")
'    If p <> 0 Then strAddressListsRoot = Right(strAddressListsRoot, Len(strAddressListsRoot) - (p - 2))
'  End If
'
'  strSQL = "<" & strLDAPProtocol & strLDAPConfigPath & ">;(&(objectCategory=addressBookContainer)(purportedSearch=*));displayName,distinguishedName"
'  objCmd.CommandText = strSQL
'  objCmd.Properties("Sort on") = "displayName"
'  Set objRs = objCmd.Execute

  rwc "<select name=""al0"">"
    doFolders strMailboxURL, True

    If USESPECIFIEDGAL = "" Then

      Dim arrAddressBookRoots, objCmd, objConn, objRs, p, s, s2, strAddressListsRoot
      Set objConn = Server.CreateObject("ADODB.Connection")
      objConn.Provider = "ADsDSOObject"
      objConn.Properties("ADSI Flag") = 33
      If Not CUSTOMFBA Then
        objConn.Open "ADs Provider"
      Else
        objConn.Open "ADs Provider", strLogonUser, strPassword
      End If
      Set objCmd = Server.CreateObject("ADODB.Command")
      objCmd.ActiveConnection = objConn
      strSQL = "<" & strLDAPProtocol & "CN=Microsoft Exchange,CN=Services," & strLDAPConfigPath & ">;(objectClass=msExchConfigurationContainer);addressBookRoots"
      objCmd.CommandText = strSQL
      Set objRs = objCmd.Execute
      If Not(objRs.BOF And objRs.EOF) Then
        arrAddressBookRoots = objRs("addressBookRoots")
        strAddressListsRoot = arrAddressBookRoots(0)
        p = Instr(strAddressListsRoot, "CN=Address Lists Container")
        If p <> 0 Then strAddressListsRoot = Right(strAddressListsRoot, Len(strAddressListsRoot) - (p - 2))
      End If
      strSQL = "<" & strLDAPProtocol & strLDAPConfigPath & ">;(&(objectCategory=addressBookContainer)(purportedSearch=*));displayName,distinguishedName"
      objCmd.CommandText = strSQL
      objCmd.Properties("Sort on") = "displayName"
      Set objRs = objCmd.Execute
      Do While Not objRs.EOF
        s = objRs("distinguishedName")
        s = Replace(s, strAddressListsRoot, "")
        s2 = objRs("displayName")
        rw "<option value=""" & displayEncodeI0(s) & """"
        If al0 = s Then rw " selected"
        rwc ">" & displayEncodeI0(s2) & "</option>"
        objRs.MoveNext
      Loop
      Set objConn = Nothing : Set objCmd = Nothing : Set objRs = Nothing

    Else

      Dim arrSpecifiedGAL, strGALDN, strGALDisplayName
      arrSpecifiedGAL = Split(USESPECIFIEDGAL, ":")
      If UBound(arrSpecifiedGAL) = 0 Then
        strGALDN = USESPECIFIEDGAL
        arrSpecifiedGAL = Split(USESPECIFIEDGAL, ",")
        strGALDisplayName = Replace(arrSpecifiedGAL(0), "CN=", "")
      Else
        strGALDN = arrSpecifiedGAL(0)
        strGALDisplayName = arrSpecifiedGAL(1)
      End If
      rw "<option value=""" & strGALDN & """"
      If al0 = strGALDN Then rw " selected"
      rwc ">" & strGALDisplayName & "</option>"

    End If

    doFolders strPublicURL, True
  rwbc "</select>"

'  Set objConn = Nothing : Set objCmd = Nothing : Set objRs = Nothing

End Sub

Sub doTopPreviousNextLinksContacts

  maxMessages = intAddressesPerPage
  t = False
  If mn0 > 1 Then
    oldMn0 = mn0 : mn0 = 1
    writeLinkWParams strPageName, TOPTEXT, "MN0,AC0,PG0,NOBR"
    mn0 = oldMn0
    t = True
  End If
  If mn0 > maxMessages Then
    oldMn0 = mn0 : mn0 = mn0 - maxMessages
    writeLinkWParams strPageName, PREVIOUSTEXT & " " & maxMessages, "MN0,AC0,PG0,NOBR"
    mn0 = oldmn0
    t = True
  End If
  If mn0 <= (numberOfMessages - maxMessages) Then
    oldmn0 = mn0 : mn0 = mn0 + maxMessages
    writeLinkWParams strPageName, NEXTTEXT & " " & maxMessages, "MN0,AC0,PG0,NOBR"
    mn0 = oldmn0
    t = True
  End If
  If t Then rwbc ""

End Sub

Function getContacts

  Dim locArrOut()
  strXML = "<a:searchrequest xmlns:a=""DAV:""><a:sql>" & _
     "SELECT" & _
     " ""urn:schemas:httpmail:subject""" & _
     ",""http://schemas.microsoft.com/exchange/outlookmessageclass""" & _
     ",""http://schemas.microsoft.com/mapi/id/{00062004-0000-0000-C000-000000000046}/0x8084""" & _
     " FROM scope('shallow traversal of """ & al0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False"
  If fl0 <> "" Then
    Select Case fl1
      Case "name"
        strXML = strXML & " AND ""urn:schemas:contacts:cn"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "address"
        strXML = strXML & " AND ""urn:schemas:contacts:mailingpostaladdress"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "phone"
        strXML = strXML & " AND ""urn:schemas:contacts:telephoneNumber"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "company"
        strXML = strXML & " AND ""urn:schemas:contacts:o"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "department"
        strXML = strXML & " AND ""urn:schemas:contacts:department"" LIKE '%" & displayEncodeX0(fl0) & "%'"
    End Select
  End If
  strXML = strXML & " ORDER BY ""DAV:displayname""" & _
   "</a:sql></a:searchrequest>"

  ' Don't use intAddressesPerPage - 1 so that we can see if limit would have exceeded
  strStatus = doWinHTTPSearch(al0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    d = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      locArrT2 = getXMLNodes("urn:schemas:httpmail:", "subject")
      locArrT3 = getXMLNodes("http://schemas.microsoft.com/exchange/", "outlookmessageclass")
      locArrT4 = getXMLNodes("http://schemas.microsoft.com/mapi/id/{00062004-0000-0000-C000-000000000046}/", "0x8084")
      n = 0
      For locN = LBound(locArrT) To d
        n = n + 1
        ReDim Preserve locArrOut(1, n - 1)
        locArrOut(0, n - 1) = locArrT2(locN)
        If locArrT3(locN) <> "IPM.DistList" Then
          re0 = locArrT4(locN)
        Else
          re0 = locArrT2(locN)
        End If
        locArrOut(1, n - 1) = re0
      Next
    End If
  End If

  getContacts = locArrOut

End Function

Function getGAL
  ReDim arrOut(4, -1)
  If RETURNUNFILTEREDGAL Or (fl0 <> "") Then
    Dim objConn, objCmd, objRs, strABRoot
    Set objConn = Server.CreateObject("ADODB.Connection")
    objConn.Provider = "ADsDSOObject"
    objConn.Properties("ADSI Flag") = 33
    If Not CUSTOMFBA Then
      objConn.Open "ADs Provider"
    Else
      objConn.Open "ADs Provider", strLogonUser, strPassword
    End If
    Set objCmd = Server.CreateObject("ADODB.Command")
    objCmd.ActiveConnection = objConn
    If ADDRESSBOOKROOT = "" Then
      objCmd.CommandText = "<" & strLDAPProtocol & "CN=Microsoft Exchange,CN=Services," & strLDAPConfigPath & ">;;addressBookRoots"
      Set objRs = objCmd.Execute
      If Not objRs.EOF Then
        Dim arrABRoots
        arrABRoots = objRs("addressBookRoots").Value
        strABRoot = arrABRoots(0)
        Dim p
        p = Instr(strABRoot, "CN=Address Lists Container")
        If p <> 0 Then strABRoot = Right(strABRoot, Len(strABRoot) - (p - 2))
      End If
    Else
      strABRoot = ADDRESSBOOKROOT
    End If
    If strABRoot <> "" Then
      Dim strDN
      If USESPECIFIEDGAL = "" Then
        objCmd.CommandText = "<" & strLDAPProtocol & al0 & strABRoot & ">;;distinguishedName"
        Set objRs = objCmd.Execute
        If Not objRs.EOF Then strDN = objRs("distinguishedName")
      Else
        Dim arrSpecifiedGAL
        arrSpecifiedGAL = Split(USESPECIFIEDGAL, ":")
        strDN = arrSpecifiedGAL(0) & strABRoot
      End If
      If strDN <> "" Then
        strSQL = "<" & strGCProtocol & strLDAPPath & ">;"
        If fl0 <> "" Then strSQL = strSQL & "(&"
        strSQL = strSQL & "(showInAddressBook=" & strDN & ")"
        If fl0 <> "" Then
          Dim strWildCard
          strWildCard = ""
          If SUBSTRINGSEARCH Then strWildCard = "*"
          If Not ISWAP Then
            Select Case fl1
              Case "name"
                strSQL = strSQL & "(displayName=" & strWildCard & fl0 & "*)"
              Case "address"
                strSQL = strSQL & "(|(streetAddress=" & strWildCard & fl0 & "*)(l=" & strWildCard & fl0 & "*)(st=" & strWildCard & fl0 & "*)(co=" & strWildCard & fl0 & "*)(postalCode=" & strWildCard & fl0 & "*))"
              Case "phone"
                strSQL = strSQL & "(telephoneNumber=" & strWildCard & fl0 & "*)"
              Case "company"
                strSQL = strSQL & "(company=" & strWildCard & fl0 & "*)"
              Case "department"
                strSQL = strSQL & "(department=" & strWildCard & fl0 & "*)"
            End Select
          Else
            strSQL = strSQL & "(displayName=" & strWildCard & fl0 & "*)"
          End If
          If fl0 <> "" Then strSQL = strSQL & ")"
        End If
        strSQL = strSQL & ";displayName,distinguishedName,proxyAddresses,department,title"
        objCmd.CommandText = strSQL
        objCmd.Properties("Asynchronous") = False
        objCmd.Properties("Cache results") = False
        objCmd.Properties("Page size") = intAddressesPerPage + 1
        objCmd.Properties("Size limit") = intAddressesPerPage
        If SORTGALON <> "" Then objCmd.Properties("Sort on") = SORTGALON
        objCmd.Properties("Time limit") = 5
        Set objRs = objCmd.Execute
        If Not(objRs.BOF And objRs.EOF) Then
          Dim arrRows
          arrRows = objRs.GetRows
          Dim n
          n = UBound(arrRows, 2)
          If n >= intAddressesPerPage Then n = intAddressesPerPage - 1
          ReDim arrOut(4, n)
          Dim i
          For i = 0 To n
            arrOut(0, i) = arrRows(0, i)
            arrOut(1, i) = Replace(arrRows(1, i), "/", "\/")
            arrOut(2, i) = getDefaultProxyAddress(arrRows(2, i))
            arrOut(3, i) = arrRows(3, i)
            arrOut(4, i) = arrRows(4, i)
          Next
        End If
      End If
    End If
    Set objConn = Nothing : Set objCmd = Nothing : Set objRs = Nothing
  End If
  getGAL = arrOut
End Function

Dim folderLevel

Sub doFolders(strFolderIn, blnRecurseIn)

  folderLevel = folderLevel + 1
  strXML = "<a:searchrequest xmlns:a=""DAV:""><a:sql>" & _
     "SELECT" & _
     " ""DAV:displayname""" & _
     " FROM scope('shallow traversal of """ & strFolderIn & """')" & _
     " WHERE ""DAV:isfolder""=True" & _
     " AND ""DAV:ishidden""=False" & _
     " AND ""DAV:contentclass""='urn:content-classes:contactfolder'" & _
     " ORDER BY ""DAV:displayname""" & _
   "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strFolderIn, strXML, "")
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "displayname")
    locArrT2 = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      For locN = LBound(locArrT) To UBound(locArrT)
        locT = locArrT2(locN)
        rw "<option value=""" & displayEncodeI0(locT) & """"
        If locT = al0 Then rw " selected"
        rw ">"
        For locN2 = 1 to folderLevel - 1 : rw "&nbsp;" : Next
        rwc displayEncodeI0(locArrT(locN)) & "</option>"
        If blnRecurseIn Then doFolders locT, True
      Next
    End If
  End If
  folderLevel = folderLevel - 1

End Sub

Sub getFirstAndLastContact

  maxMessages = intAddressesPerPage
  If mn0 > numberOfMessages Then mn0 = ((Int(numberOfMessages / maxMessages)) * maxMessages) + 1
  If mn0 < 1 Then mn0 = 1
  firstMessage = mn0
  If mn0 + (maxMessages - 1) > numberOfMessages Then
    lastMessage = numberOfMessages
  Else
    lastMessage = mn0 + (maxMessages - 1)
  End If

End Sub

'Function getNumberOfAddresses
'
'  Dim arrAddressBookRoots, n, objCmd, objConn, objRs, p, strAddressListsRoot, strSearch, v
'  Set objConn = Server.CreateObject("ADODB.Connection")
'  objConn.Provider = "ADsDSOObject"
'  objConn.Properties("ADSI Flag") = 33 ' 1
'  If Not CUSTOMFBA Then
'    objConn.Open "ADs Provider"
'  Else
'    objConn.Open "ADs Provider", strLogonUser, strPassword
'  End If
'  Set objCmd = Server.CreateObject("ADODB.Command")
'  objCmd.ActiveConnection = objConn
'  strSQL = "<" & strLDAPProtocol & "CN=Microsoft Exchange,CN=Services," & strLDAPConfigPath & ">;;addressBookRoots"
'  objCmd.CommandText = strSQL
'  Set objRs = objCmd.Execute
'
'  If Not(objRs.BOF And objRs.EOF) Then
'    arrAddressBookRoots = objRs("addressBookRoots")
'    strAddressListsRoot = arrAddressBookRoots(0)
'    p = Instr(strAddressListsRoot, "CN=Address Lists Container")
'    If p <> 0 Then strAddressListsRoot = Right(strAddressListsRoot, Len(strAddressListsRoot) - (p - 2))
'  End If
'
'  strSQL = "<" & strLDAPProtocol & al0 & strAddressListsRoot & ">;;purportedSearch"
'  objCmd.CommandText = strSQL
'  If SHOWERRORS Then On Error Resume Next
'  Set objRs = objCmd.Execute
'  errNumber = Err.Number
'  errDescription = Err.Description
'  If SHOWERRORS Then On Error Goto 0
'
'  If errNumber = 0 Then
'    If Not objRs.EOF Then strSearch = objRs("purportedSearch")
'    strSQL = "<" & strGCProtocol & strLDAPPath & ">;(&"
'    If fl0 <> "" Then
'      Dim strWildCard
'      strWilCard = ""
'      If SUBSTRINGSEARCH Then strWildCard = "*"
'      Select Case fl1
'        Case "name"
'          strSQL = strSQL & "(displayName=" & strWildCard & fl0 & "*)"
'        Case "address"
'          strSQL = strSQL & "(|" & _
'           "(streetAddress=" & strWildCard & fl0 & "*)" & _
'           "(l=" & strWildCard & fl0 & "*)" & _
'           "(st=" & strWildCard & fl0 & "*)" & _
'           "(co=" & strWildCard & fl0 & "*)" & _
'           "(postalCode=" & strWildCard & fl0 & "*)" & _
'           ")"
'        Case "phone"
'          strSQL = strSQL & "(telephoneNumber=" & strWildCard & fl0 & "*)"
'        Case "company"
'          strSQL = strSQL & "(company=" & strWildCard & fl0 & "*)"
'        Case "department"
'          strSQL = strSQL & "(department=" & strWildCard & fl0 & "*)"
'      End Select
'    End If
'    strSQL = strSQL & strSearch & "(!(msExchHideFromAddressLists=TRUE)))"
'    strSQL = strSQL & ";displayName"
'
'    objCmd.CommandText = strSQL
'    objCmd.Properties("Page Size") = 500
'    objCmd.Properties("Time limit") = 5
'    ' Don't use intAddressesPerPage so that we can see if limit would have exceeded
'    objCmd.Properties("Size limit") = -1
'    Set objRs = objCmd.Execute
'    v = objRs.RecordCount
'  Else
'    rwbc displayEncode0(ERRORTEXT)
'    rwbc displayEncode0(errNumber & " - " & errDescription)
'    rwbc displayEncode0(strSQL)
'  End If
'
'  Set objConn = Nothing : Set objCmd = Nothing : Set objRs = Nothing
'
'  getNumberOfAddresses = v
'
'End Function

Function getNumberOfContacts

  locNumberOfMessages = 0
  strXML = "<a:searchrequest xmlns:a=""DAV:""><a:sql>" & _
     "SELECT" & _
     " ""DAV:visiblecount""" & _
     " FROM scope('shallow traversal of """ & al0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False"
  If fl0 <> "" Then
    Select Case fl1
      Case "name"
        strXML = strXML & " AND ""urn:schemas:contacts:cn"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "address"
        strXML = strXML & " AND ""urn:schemas:contacts:mailingpostaladdress"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "phone"
        strXML = strXML & " AND ""urn:schemas:contacts:telephoneNumber"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "company"
        strXML = strXML & " AND ""urn:schemas:contacts:o"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "department"
        strXML = strXML & " AND ""urn:schemas:contacts:department"" LIKE '%" & displayEncodeX0(fl0) & "%'"
    End Select
  End If
  strXML = strXML & " GROUP BY ""DAV:contentclass""" & _
   "</a:sql></a:searchrequest>"

  If strXML <> "" Then
    strStatus = doWinHTTPSearch(al0, strXML, "")
    'rwbc displayEncode0(objWinHTTP.ResponseText)
    If Left(strStatus, 1) = "2" Then
      arrT = getXMLNodes("DAV:", "visiblecount")
      If SHOWERRORS Then On Error Resume Next
      d = UBound(arrT)
      errNumber = Err.Number
      If SHOWERRORS Then On Error Goto 0
      If errNumber = 0 Then
        For i = LBound(arrT) To UBound(arrT)
          locNumberOfMessages = locNumberOfMessages + CLng(arrT(i))
        Next
      End If
    End If
  End If

  getNumberOfContacts = locNumberOfMessages

End Function
%>
