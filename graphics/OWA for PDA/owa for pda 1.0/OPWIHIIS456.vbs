
For i = 1 To WScript.Arguments.Count
  s = WScript.Arguments(i - 1)
  arrS = Split(s, "=")
  If UBound(arrS) = 1 Then
    Select Case(arrS(0))
      Case "/a"
        inApplication = arrS(1)
      Case "/e"
        inExtension = arrS(1)
      Case "/n"
        inName = arrS(1)
      Case "/p"
        inPath = arrS(1)
      Case "/s"
        inServer = arrS(1)
    End Select
  End If
Next

'inApplication = "owa-pda"
'inExtension = ".aspx"
'inName = "owa-pda"
'inPath = "c:\inetpub\wwwroot"
'inServer = "1"

sError = "Usage is OPWIHIIS456.vbs /a=""Application"" /e=""Extension"" /n=""VDirName"" /p=""Path"" /s=""ServerID""" & vbCrLf
If inApplication = "" Then
  MsgBox sError & "Missing /a=""Application"""
  WScript.Quit
End If
If inExtension = "" Then
  MsgBox sError & "Missing /e=""Extension"""
  WScript.Quit
End If
If inName = "" Then
  MsgBox sError & "Missing /n=""VDirName"""
  WScript.Quit
End If
If inPath = "" Then
  MsgBox sError & "Missing /p=""Path"""
  WScript.Quit
End If
If inServer = "" Then
  MsgBox sError & "Missing /s=""ServerID"""
  WScript.Quit
End If

Set objIIS = CreateObject("IISNamespace")
Set objSite = objIIS.GetObject("IISWebService", "127.0.0.1/w3svc")
Set objServer = objSite.GetObject("IISWebServer", inServer)
Set objRoot = objServer.GetObject("IISWebVirtualDir", "Root")

' Delete existing VDir if testing
'objRoot.Delete "IISWebVirtualDir", inname

' See if Virtual Directory already exists
On Error Resume Next
Set objVDir = objRoot.GetObject("IISWebVirtualDir", inName)
e = Err.Number
Err.Clear
On Error Goto 0
If e = 0 Then
  foundVDir = True
  createdVDir = False
Else
  ' If not then create it
  foundVDir = False
  On Error Resume Next
  Set objVDir = objRoot.Create("IISWebVirtualDir", inName)
  e = Err.Number
  Err.Clear
  On Error GoTo 0
  If e = 0 Then
    createdVDir = True
  Else
    createdVDir = False
  End If
End If

If foundVDir Or createdVDir Then
  If createdVDir Then
    objVDir.KeyType = "IISWebVirtualDir"
    objVDir.DefaultDoc = "Default" & inExtension
    objVDir.AuthFlags = 2
    objVDir.AccessFlags = 513 ' AccessRead | AccessScript
    On Error Resume Next
    Set objSystemInfo = CreateObject("WinNTSystemInfo")
    e = Err.Number
    Err.Clear
    On Error GoTo 0
    If e = 0 Then
      strDomain = objSystemInfo.DomainName
      Set objSystemInfo = Nothing
    Else
      strDomain = "\"
    End If
    objVDir.DefaultLogonDomain = strDomain
    On Error Resume Next
    objVDir.AppCreate2 2
    If Err.Number <> 0 Then
      ' Must be IIS4
      objVDir.AppCreate True
    End If
    On Error Goto 0
    objVDir.AppFriendlyName = inName
    objVDir.AspAllowSessionState = True
    On Error Resume Next
    ' Not sure if this exists in IIS4
    objVDir.AspMaxRequestEntityAllowed = &H40000000
    On Error Goto 0
    objVDir.SetInfo
    arrHTTPErrors = objVDir.HttpErrors
    For i = LBound(arrHTTPErrors) To UBound(arrHTTPErrors)
      If Left(arrHTTPErrors(i), 4) = "404," Then
        arrHTTPErrors(i) = "404,*,URL,/" & inName & "/AT2" & inExtension
        Exit For
      End If
    Next
    objVDir.HttpErrors = arrHTTPErrors
    objVDir.SetInfo

    ' Check for E2K/E2K3 Davex.dll script mapping
    On Error Resume Next
    arrScriptMaps = objVDir.ScriptMaps
    e = Err.Number
    Err.Clear
    On Error GoTo 0
    If e = 0 Then
      i = LBound(arrScriptMaps) - 1
      blnChangedScriptMaps = False
      For j = LBound(arrScriptMaps) To UBound(arrScriptMaps)
        strScriptMap = arrScriptMaps(j)
        arrScriptMap = Split(strScriptMap, ",")
        If Not ((Trim(arrScriptMap(0)) = "*") And (InStr(UCase(arrScriptMap(1)), "DAVEX.DLL") <> 0)) Then
          i = i + 1
          ReDim Preserve arrScriptMaps2(i)
          arrScriptMaps2(i) = strScriptMap
        Else
          blnChangedScriptMaps = True
        End If
      Next
      If blnChangedScriptMaps Then
        objVDir.ScriptMaps = arrScriptMaps2
        objVDir.SetInfo
      End If
    End If
  End If
  objVDir.Path = inPath
  objVDir.SetInfo
Else
  MsgBox "Unable to create or locate Virtual Directory " & inName & ".", vbExclamation + vbOKOnly
End If
     
Set objVDir = Nothing
Set objRoot = Nothing
Set objServer = Nothing
Set objSite = Nothing
Set objIIS = Nothing

If foundVDir Or createdVDir Then
  On Error Resume Next
  Set objShell = CreateObject("WScript.Shell")
  objShell.RegWrite "HKLM\SOFTWARE\leederbyshire.com\" & inApplication & "\IISWebServer", inServer, "REG_SZ"
  objShell.RegWrite "HKLM\SOFTWARE\leederbyshire.com\" & inApplication & "\IISWebVirtualDir", inName, "REG_SZ"
  Set objShell = Nothing
  On Error Goto 0
End If
