<%
Dim folderLevel

Sub listAppointments

  Dim d, locArrT, locN
  If validDate(dy0, mo0, yr0) Then
    myStartDate = DateSerial(yr0, mo0, dy0) + TimeSerial(0, 0, 1)
    writeTitle APPOINTMENTSFROMTEXT

    If IsDate(myStartDate) Then
      If SHOWERRORS Then On Error Resume Next
      d = FormatDateTime(myStartDate, vbLongDate)
      If Err.Number <> 0 Then d = FormatDateTime(myStartDate, vbShortDate)
      If SHOWERRORS Then On Error Goto 0
    End If

    'd = WeekDayName(WeekDay(myStartDate), True)
    'd = d & " " & FormatDateTime(myStartDate, vbLongDate)
    'd = FormatDateTime(myStartDate, vbLongDate)
    writeTitle d
    locArrT = listAppointments2

    If mn0 = 1 Then
      noAppointmentsForDay = False
      If numberOfMessages <> 0 Then
        If CDate(locArrT(1, 0)) <> DateSerial(yr0, mo0, dy0) Then
          noAppointmentsForDay = True
        End If
      Else
        noAppointmentsForDay = True
      End If
      If noAppointmentsForDay Then
        writeUnderline
        writeTitle NOAPPOINTMENTSFORSELECTEDDAYTEXT
      End If
    End If

    writeTable ""
      strParams = "MI0,NOBR"
      If em0 <> "" Then strParams = strParams & ",EM0Y"
      For locN = LBound(locArrT, 2) To UBound(locArrT, 2)
        myDate = locArrT(1, locN)

        If IsDate(myDate) Then
          If SHOWERRORS Then On Error Resume Next
          myDate = FormatDateTime(myDate, vbLongDate)
          If Err.Number <> 0 Then myDate = FormatDateTime(myDate, vbShortDate)
          If SHOWERRORS Then On Error Goto 0
        End If

        'If myDate <> "" Then myDate = FormatDateTime(myDate, vbLongDate)
        If myDate <> lastDate Then
          writeTr "" : writeTd ""
          writeUnderline
          writeTitle myDate
          writeTdEnd : writetrEnd
          lastDate = myDate
          strCurrentRowType = ""
        End If
        If strCurrentRowType = "A" Then
          strCurrentRowType = "B"
        Else
          strCurrentRowType = "A"
        End If
        strAttributes = ""
        If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
          strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
        ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
          strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
        End If
        writeTr strAttributes
          writeTd ""
            myTime = locArrT(2, locN)
            If myTime <> "" Then rwc myTime ' FormatDateTime(myTime, vbShortTime)
            display = checkSubject(locArrT(3, locN))
            mi0 = locArrT(0, locN)
            writeLinkWParams "MSR.asp", display, strParams
          writeTdEnd
        writeTrEnd
      Next
    writeTableEnd

  Else
    rwbc displayEncode0(INVALIDDATETEXT)
  End If

End Sub

Function listAppointments2

  Dim arrT, dt, locD, locN, tm
  ReDim locArrOut(3, lastMessage - firstMessage)
  myStartDate = DateSerial(yr0, mo0, dy0) + TimeSerial(0, 0, 1)
  myStartDate2 = getTZFromLocalDateTime(myStartDate)
  myEndDate = DateAdd("yyyy", 1, myStartDate)
  myEndDate2 = getTZFromLocalDateTime(myEndDate)

  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""urn:schemas:calendar:dtstart""" & _
     ",""urn:schemas:httpmail:subject""" & _
     " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False" & _
     " AND ""urn:schemas:calendar:instancetype"" &lt;&gt; 1" & _
     " AND ""urn:schemas:calendar:dtstart"" &lt;= CAST(""" & myEndDate2 & """ AS 'dateTime.tz')" & _
     " AND ""urn:schemas:calendar:dtend"" &gt; CAST(""" & myStartDate2 & """ AS 'dateTime.tz')" & _
     " ORDER BY ""urn:schemas:calendar:dtstart""" & _
   "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      locArrT2 = getXMLNodes("urn:schemas:calendar:", "dtstart")
      locArrT3 = getXMLNodes("urn:schemas:httpmail:", "subject")
      For locN = LBound(locArrT) To locD
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        myDateTime = getDateTimeFromTZ(locArrT2(locN))
        If myDateTime <> "" Then
          arrT = Split(myDateTime, " ")
          dt = arrT(0)
          If UBound(arrT) >= 1 Then
            tm = arrT(1)
          Else
            tm = "00:00:00"
          End If
          If UBound(arrT) >= 2 Then tm = tm & " " & arrT(2)
          locArrOut(1, locN) = dt
          locArrOut(2, locN) = tm
          ' Didn't work with Hebrew characters
          'locArrOut(1, locN) = FormatDateTime(myDateTime, vbShortDate)
          'locArrOut(2, locN) = FormatDateTime(myDateTime, vbShortTime)
        End If
        locArrOut(3, locN) = locArrT3(locN)
      Next
    End If
  End If
  listAppointments2 = locArrOut

End Function

Sub listContacts

  Dim locArrT, locI, locN
  writeTitle FILTERTEXT & COLONSPACETEXT
  rwc "<select name='fl1'>"
    rw "<option value='name'"
    If fl1 = "name" Then rw " selected"
    rwc ">" & displayEncodeI0(NAMETEXT) & "</option>"
    rw "<option value='address'"
    If fl1 = "address" Then rw " selected"
    rwc ">" & displayEncodeI0(BUSINESSADDRESSTEXT) & "</option>"
    rw "<option value='phone'"
    If fl1 = "phone" Then rw " selected"
    rwc ">" & displayEncodeI0(BUSINESSPHONETEXT) & "</option>"
    rw "<option value='company'"
    If fl1 = "company" Then rw " selected"
    rwc ">" & displayEncodeI0(COMPANYTEXT) & "</option>"
    rw "<option value='department'"
    If fl1 = "department" Then rw " selected"
    rwc ">" & displayEncodeI0(DEPARTMENTTEXT) & "</option>"
  rwbc "</select>"
  rwc "<input type=""text"" class=""inputTextFilter"" name='fl0' value='" & displayEncodeI0(fl0) & "'>"
  doInputSubmit "", GOTEXT, True
  doTopPreviousNextLinks(strContainerClass)
  writeUnderline
  locArrT = listContacts2

  locN = UBound(locArrT, 2) + 1
  If locN = 0 Then rwbc NOTFOUNDTEXT
  writeTable ""
    For locI = LBound(locArrT, 2) To locN - 1
      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If
      writeTr strAttributes
        writeTd ""
          display = checkSubject(locArrT(1, locI))
          mi0 = locArrT(0, locI)
          writeLinkWParams "MSR.asp", display, "MI0,NOBR"
        writeTdEnd
      writeTrEnd
    Next
  writeTableEnd

End Sub

Function listContacts2

  Dim locD, locN
  ReDim locArrOut(1, -1)
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
   "SELECT" & _
   " ""urn:schemas:httpmail:subject""" & _
   " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
   " WHERE ""DAV:isfolder"" = False" & _
   " AND ""DAV:ishidden"" = False" & _
   " AND (""DAV:contentclass"" = 'urn:content-classes:person' OR ""DAV:contentclass"" = 'urn:content-classes:group')"
  If fl0 <> "" Then
    Select Case fl1
      Case "name"
        strXML = strXML & " AND ""urn:schemas:contacts:cn"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "address"
        strXML = strXML & " AND ""urn:schemas:contacts:mailingpostaladdress"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "phone"
        strXML = strXML & " AND ""urn:schemas:contacts:telephoneNumber"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "company"
        strXML = strXML & " AND ""urn:schemas:contacts:o"" LIKE '%" & displayEncodeX0(fl0) & "%'"
      Case "department"
        strXML = strXML & " AND ""urn:schemas:contacts:department"" LIKE '%" & displayEncodeX0(fl0) & "%'"
    End Select
  End If
  strXML = strXML & " ORDER BY ""DAV:displayname""" & _
   "</a:sql></a:searchrequest>"

  ' Don't use lastMessage - 1 so that we can see if limit would have exceeded
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      ' Do the ReDim here so we can check intAddressesPerPage when displayed
      ReDim locArrOut(1, locD)
      locArrT2 = getXMLNodes("urn:schemas:httpmail:", "subject")
      For locN = LBound(locArrT) To locD
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
      Next
    End If
  End If
  listContacts2 = locArrOut

End Function

Sub listDeletedItems

  Dim locArrT, locI
  locArrT = listDeletedItems2
  writeTable ""
    For locI = LBound(locArrT, 2) To UBound(locArrT, 2)
      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If
      writeTr strAttributes

        writeTd ""
          rw "<input type='checkbox' name='c" & locI + 1 & "' value='" & locArrT(3, locI) & "'>"
        writeTdEnd

        writeTd ""
          display = checkSubject(locArrT(1, locI))
          display = packText(display, intListingSubjectWidth)
          rwbc displayEncode0(display) & "&nbsp;"
        writeTdEnd
        writeTd ""
          rwbc getDateTimeFromTZ(locArrT(2, locI)) & "&nbsp;"
        writeTdEnd
      writeTrEnd
    Next
  writeTableEnd
  writeUnderLine
  doInputSubmit "action", RECOVERMARKEDTEXT, False

End Sub

Function listDeletedItems2

  ReDim locArrOut(3, lastMessage - firstMessage)
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""urn:schemas:httpmail:subject""" & _
     ",""urn:schemas:httpmail:deletedon""" & _
     " FROM scope('softdeleted traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False" & _
     " ORDER BY ""urn:schemas:httpmail:deletedon"" DESC" & _
   "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      locArrT2 = getXMLNodes("urn:schemas:httpmail:", "subject")
      locArrT3 = getXMLNodes("urn:schemas:httpmail:", "deletedon")
      For locN = LBound(locArrT) To locD
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
        locArrOut(2, locN) = locArrT3(locN)
        locArrOut(3, locN) = Replace(locArrT(locN), strBaseURL & fi0, "")
      Next
    End If
  End If
  listDeletedItems2 = locArrOut

End Function

Sub listMessages

  Dim d, locArrT, locN, p
  locArrT = listMessages2

  writeTable ""

    For locN = LBound(locArrT, 2) To UBound(locArrT, 2)

      If blnShowDatesInFolder Then
        myDate = locArrT(5, locN)
        If IsDate(myDate) Then
          If SHOWERRORS Then On Error Resume Next
          myDate = FormatDateTime(myDate, vbLongDate)
          If Err.Number <> 0 Then myDate = FormatDateTime(myDate, vbShortDate)
          If SHOWERRORS Then On Error Goto 0
        End If
        If myDate <> lastDate Then
          writeTr ""
          intColSpan = 4
          If blnShowTimesInFolder Then intColSpan = intColSpan + 1
          If blnSenderSubjectBreak Then intColSpan = intColSpan - 1
          writeTd "colspan='" & intColSpan & "'"
          writeTitle myDate
          writeTdEnd : writetrEnd
          lastDate = myDate
          strCurrentRowType = ""
        End If
      End If

      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If

      writeTr strAttributes

        writeTd ""
          rw "<input type='checkbox' name='c" & locN + 1 & "' value='" & locArrT(10, locN) & "'>"
        writeTdEnd

        If intShowIcons And OPWICONSMSL Then
          strImage = getImageName(locArrT(1, locN), locArrT(6, locN), locArrT(9, locN), locArrT(8, locN), locArrT(7, locN))
          strAlt = "R" : If Instr(strImage, "MsgU") <> 0 Then strAlt = "U"
          writeTd ""
            rw "<img src='Img/" & strImage & "' width='16' height='16' alt='" & strAlt & "'>&nbsp;"
          writeTdEnd
        End If

        ' Are we in Sent Items?
        locInSentItems = (Left(fi0, Len(strSentItemsURL)) = strSentItemsURL)
        ' Are we in Drafts?
        locInDrafts = (Left(fi0, Len(strDraftsURL)) = strDraftsURL)

        writeTd ""
          If Not (locInSentItems Or locInDrafts Or locInOutbox) Then
            display = locArrT(3, locN)
          Else
            display = locArrT(2, locN)
          End If
          display = packText(display, intListingSenderWidth)
          rwc displayEncode0(display) & "&nbsp;"
        writeTdEnd

        If Not blnSenderSubjectBreak Then
          writeTd ""
            display = checkSubject(locArrT(4, locN))
            display = packText(display, intListingSubjectWidth)
            mi0 = locArrT(0, locN)

            linkParams = "NOBR"
            If locArrT(6, locN) Then linkParams = linkParams & ",BOLD"
            ' If we're not in Drafts or Outbox, then display a link to the Read page
            If Not(locInDrafts Or locInOutbox) Then
              linkParams = linkParams & ",MI0"
              writeLinkWParams "MSR.asp", display, linkParams
            Else
              ' otherwise, display a link to the Edit page
              ci0 = mi0 : mt0 = OPWCME + OPWFRFO
              linkParams = linkParams & ",CI0,MT0"
              writeLinkWParams "MSE.asp", display, linkParams
            End If
            rw "&nbsp;"
          writeTdEnd
        End If

        If blnShowTimesInFolder Then
          If IsDate(locArrT(5, locN)) Then
            display = FormatDateTime(locArrT(5, locN), vbShortTime)
          Else
            display = NONETEXT
          End If
          writeTd ""
            rw displayEncode0(display) & "&nbsp;"
          writeTdEnd
        End If

      writeTrEnd

      If blnSenderSubjectBreak Then
        writeTr strAttributes
          writeTd "": rw "&nbsp;" : writeTdEnd
          intColSpan = 2
          If blnShowTimesInFolder Then intColSpan = intColSpan + 1
          writeTd "colspan='" & intColSpan & "'"
            display = checkSubject(locArrT(4, locN))
            display = packText(display, intListingSubjectWidth)
            mi0 = locArrT(0, locN)

            linkParams = "NOBR"
            If locArrT(6, locN) Then linkParams = linkParams & ",BOLD"
            ' If we're not in Drafts or Outbox, then display a link to the Read page
            If Not(locInDrafts Or locInOutbox) Then
              linkParams = linkParams & ",MI0"
              writeLinkWParams "MSR.asp", display, linkParams
            Else
              ' otherwise, display a link to the Edit page
              ci0 = mi0 : mt0 = OPWCME + OPWFRFO
              linkParams = linkParams & ",CI0,MT0"
              writeLinkWParams "MSE.asp", display, linkParams
            End If
            rw "&nbsp;"
          writeTdEnd
        writeTrEnd
      End If

      If intPreviewSize > 0 Then
        writeTr "bgcolor='white'" : strCurrentRowType = ""
          writeTd "": rw "&nbsp;" : writeTdEnd
          intColSpan = 3
          If blnShowTimesInFolder Then intColSpan = intColSpan + 1
          If blnSenderSubjectBreak Then intColSpan = intColSpan - 1
          writeTd "colspan='" & intColSpan & "'"
            display = locArrT(11, locN)
            display = Trim(display)
            display = Replace(display, vbCrLf, " ")
            display = Replace(display, vbTab, " ")
            Do While Instr(display, "  ") <> 0
              display = Replace(display, "  ", " ")
            Loop
            p = intPreviewSize : q = p
            Do While (Mid(display, p + 1, 1) <> " ") And (p > 0)
              p = p - 1
            Loop
            If p = 0 Then p = q
            display = Left(display, p)
            rw displayEncode0(display)
          writeTdEnd
        writeTrEnd
      End If

    Next

  writeTableEnd
  writeUnderLine

  doInputSubmit "action", DELETEMARKEDTEXT, False
  If blnMultipleMarkUnread Then doInputSelect "action", MARKSELECTEDUNREADTEXT, False
  rwbc ""

  If blnMultipleMove Then
    doInputSubmit "action", MOVEMARKEDTEXT, True
    rwc displayEncode0(TO2TEXT & COLONSPACETEXT)
    rwc "<select name='fi2'>"
      folderLevel = 0
      doFolder("")
    rwc "</select>"
  End If

End Sub

Function listMessages2

  Dim locN, locD, locT

  If Not blnSearchFailed Then

  ReDim locArrOut(11, lastMessage - firstMessage)

  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""http://schemas.microsoft.com/exchange/outlookmessageclass""" & _
     ",""http://schemas.microsoft.com/mapi/proptag/x10900003""" & _
     ",""urn:schemas:httpmail:datereceived""" & _
     ",""urn:schemas:httpmail:displayto""" & _
     ",""urn:schemas:httpmail:hasattachment""" & _
     ",""urn:schemas:httpmail:importance""" & _
     ",""urn:schemas:httpmail:read""" & _
     ",""urn:schemas:httpmail:sendername""" & _
     ",""urn:schemas:httpmail:subject""" & _
     " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False"
  If (fl3 <> "") And (fl2 <> "") Then
    If fl3 = "From" Then
      strXML = strXML & " AND ""urn:schemas:httpmail:sendername"" LIKE '%" & displayEncodeX0(fl2) & "%'"
    ElseIf fl3 = "To" Then
      strXML = strXML & " AND ""urn:schemas:httpmail:displayto"" LIKE '%" & displayEncodeX0(fl2) & "%'"
    ElseIf fl3 = "Subject" Then
      strXML = strXML & " AND ""urn:schemas:httpmail:subject"" LIKE '%" & displayEncodeX0(fl2) & "%'"
    ElseIf fl3 = "Message" Then
      strXML = strXML & " AND ""urn:schemas:httpmail:textdescription"" LIKE '%" & displayEncodeX0(fl2) & "%'"
    End If
  End If
  strXML = strXML & "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)

  If Left(strStatus, 1) = "2" Then

    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then

      locArrT2 = getXMLNodes("http://schemas.microsoft.com/exchange/", "outlookmessageclass")
      locArrT3 = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x10900003")
      locArrT4 = getXMLNodes("urn:schemas:httpmail:", "datereceived")
      locArrT5 = getXMLNodes("urn:schemas:httpmail:", "displayto")
      locArrT6 = getXMLNodes("urn:schemas:httpmail:", "hasattachment")
      locArrT7 = getXMLNodes("urn:schemas:httpmail:", "importance")
      locArrT8 = getXMLNodes("urn:schemas:httpmail:", "read")
      locArrT9 = getXMLNodes("urn:schemas:httpmail:", "sendername")
      locArrT10 = getXMLNodes("urn:schemas:httpmail:", "subject")

      For locN = LBound(locArrT) To locD
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
        locArrOut(2, locN) = locArrT5(locN)
        locArrOut(3, locN) = locArrT9(locN)
        locArrOut(4, locN) = locArrT10(locN)
        locArrOut(5, locN) = getDateTimeFromTZ(locArrT4(locN))
        locArrOut(6, locN) = (locArrT8(locN) = "0")
        locArrOut(7, locN) = locArrT7(locN)
        locArrOut(8, locN) = locArrT3(locN)
        locArrOut(9, locN) = (locArrT6(locN) = "1")
        locArrOut(10, locN) = Replace(locArrT(locN), strBaseURL & fi0, "")
        If intPreviewSize > 0 Then
          strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
             "<j:textdescription/>" & _
           "</a:prop></a:propfind>"
          If Left(doWinHTTPPropfind(locArrT(locN), strXML), 1) = "2" Then
            locT = getXMLNode("urn:schemas:httpmail:", "textdescription")
            locT = Left(locT, 3 * intPreviewSize)
            locArrOut(11, locN) = locT
          End If
        End If
      Next
    End If
  End If

  Else

  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""http://schemas.microsoft.com/exchange/outlookmessageclass""" & _
     ",""http://schemas.microsoft.com/mapi/proptag/x10900003""" & _
     ",""urn:schemas:httpmail:datereceived""" & _
     ",""urn:schemas:httpmail:displayto""" & _
     ",""urn:schemas:httpmail:hasattachment""" & _
     ",""urn:schemas:httpmail:importance""" & _
     ",""urn:schemas:httpmail:read""" & _
     ",""urn:schemas:httpmail:sendername""" & _
     ",""urn:schemas:httpmail:subject"""
  If fl3 = "Message" Then
    strXML = strXML & ",""urn:schemas:httpmail:textdescription"""
  End If
  strXML = strXML & _
     " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False"
  strXML = strXML & "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=0-499")

  If Left(strStatus, 1) = "2" Then

    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then

      locArrT2 = getXMLNodes("http://schemas.microsoft.com/exchange/", "outlookmessageclass")
      locArrT3 = getXMLNodes("http://schemas.microsoft.com/mapi/proptag/", "x10900003")
      locArrT4 = getXMLNodes("urn:schemas:httpmail:", "datereceived")
      locArrT5 = getXMLNodes("urn:schemas:httpmail:", "displayto")
      locArrT6 = getXMLNodes("urn:schemas:httpmail:", "hasattachment")
      locArrT7 = getXMLNodes("urn:schemas:httpmail:", "importance")
      locArrT8 = getXMLNodes("urn:schemas:httpmail:", "read")
      locArrT9 = getXMLNodes("urn:schemas:httpmail:", "sendername")
      locArrT10 = getXMLNodes("urn:schemas:httpmail:", "subject")
      If fl3 = "Message" Then
        locArrT11 = getXMLNodes("urn:schemas:httpmail:", "textdescription")
      End If
      locO = -1
      ReDim locArrOut(11, -1)
      For locN = LBound(locArrT) To locD
        Select Case fl3
          Case "From"
            strT = locArrT9(locN)
          Case "To"
            strT = locArrT5(locN)
          Case "Subject"
            strT = locArrT10(locN)
          Case "Message"
            strT = locArrT11(locN)
        End Select
        If Instr(UCase(strT), UCase(fl2)) <> 0 Then
          locO = locO + 1
          ReDim Preserve locArrOut(11, LocO)
          locArrOut(0, locO) = Replace(locArrT(locN), strBaseURL, "")
          locArrOut(1, locO) = locArrT2(locN)
          locArrOut(2, locO) = locArrT5(locN)
          locArrOut(3, locO) = locArrT9(locN)
          locArrOut(4, locO) = locArrT10(locN)
          locArrOut(5, locO) = getDateTimeFromTZ(locArrT4(locN))
          locArrOut(6, locO) = (locArrT8(locN) = "0")
          locArrOut(7, locO) = locArrT7(locN)
          locArrOut(8, locO) = locArrT3(locN)
          locArrOut(9, locO) = (locArrT6(locN) = "1")
          locArrOut(10, locO) = Replace(locArrT(locN), strBaseURL & fi0, "")
          If intPreviewSize > 0 Then
            strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
               "<j:textdescription/>" & _
             "</a:prop></a:propfind>"
            If Left(doWinHTTPPropfind(locArrT(locN), strXML), 1) = "2" Then
              locT = getXMLNode("urn:schemas:httpmail:", "textdescription")
              locT = Left(locT, 3 * intPreviewSize)
              locArrOut(11, locO) = locT
            End If
          End If
        End If
      Next
    End If
  End If

  End If

  listMessages2 = locArrOut

End Function

Sub doFolder(strFolderIn)

  folderLevel = folderLevel + 1
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
   "SELECT" & _
   " ""DAV:displayname""" & _
   " FROM scope('shallow traversal of """ & strBaseURL & strFolderIn & """')" & _
   " WHERE ""DAV:isfolder""=True" & _
   " AND ""DAV:ishidden""=False" & _
   " ORDER BY ""DAV:displayname""" & _
   "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & strFolderIn, strXML, "")
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "displayname")
    locArrT2 = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      For locN = LBound(locArrT) To UBound(locArrT)
        locT = Replace(locArrT2(locN), strBaseURL, "")
        rw "<option value='" & locT & "'"
        If locT = fi0 Then rw " selected"
        rw ">"
        For locN2 = 1 to folderLevel - 1 : rw "&nbsp;" : Next
        rwc displayEncodeI0(locArrT(locN)) & "</option>"
        doFolder(locT)
      Next
    End If
  End If
  folderLevel = folderLevel - 1

End Sub

Sub listNotes

  Dim locArrT, locI
  locArrT = listNotes2
  writeTable ""
    For locI = LBound(locArrT, 2) To UBound(locArrT, 2)
      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If
      writeTr strAttributes
        writeTd ""
          display = checkSubject(locArrT(1, locI))
          mi0 = locArrT(0, locI)
          writeLinkWParams "MSR.asp", display, "MI0,NOBR"
        writeTdEnd
      writeTrEnd
    Next
  writeTableEnd

End Sub

Function listNotes2

  ReDim locArrOut(1, lastMessage - firstMessage)
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""urn:schemas:httpmail:subject""" & _
     " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False" & _
     " ORDER BY ""DAV:displayname""" & _
   "</a:sql></a:searchrequest>"
  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      locArrT2 = getXMLNodes("urn:schemas:httpmail:", "subject")
      For locN = LBound(locArrT) To UBound(locArrT)
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
      Next
    End If
  End If
  listNotes2 = locArrOut

End Function

Sub listPosts

  Dim locArrT, locN
  locArrT = listPosts2

  writeTable ""

    For locN = LBound(locArrT, 2) To UBound(locArrT, 2)

      If blnShowDatesInFolder Then
        myDate = locArrT(4, locN)
        If IsDate(myDate) Then
          If SHOWERRORS Then On Error Resume Next
          myDate = FormatDateTime(myDate, vbLongDate)
          If Err.Number <> 0 Then myDate = FormatDateTime(myDate, vbShortDate)
          If SHOWERRORS Then On Error Goto 0
        End If
        'If myDate <> "" Then myDate = FormatDateTime(myDate, vbLongDate)
        If myDate <> lastDate Then
          writeTr ""
          intColSpan = 3
          If blnShowTimesInFolder Then intColSpan = intColSpan + 1
          If blnSenderSubjectBreak Then intColSpan = intColSpan - 1
          writeTd "colspan='" & intColSpan & "'"
          'd = WeekDayName(WeekDay(myDate), True)
          'd = d & " " & FormatDateTime(myDate, vbLongDate)
          'writeTitle d
          writeTitle myDate
          writeTdEnd : writetrEnd
          lastDate = myDate
          strCurrentRowType = ""
        End If
      End If

      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If

      writeTr strAttributes

        If intShowIcons And OPWICONSMSL Then
          strImage = getImageName(locArrT(1, locN), locArrT(5, locN), locArrT(6, locN), 0, 1)
          strAlt = "R" : If Instr(strImage, "MsgU") <> 0 Then strAlt = "U"
          writeTd ""
            rw "<img src='Img/" & strImage & "' width='16' height='16' alt='" & strAlt & "'>&nbsp;"
          writeTdEnd
        End If

        display = locArrT(2, locN)
        display = packText(display, intListingSenderWidth)
        writeTd ""
          rwc displayEncode0(display) & "&nbsp;"
        writeTdEnd

        If Not blnSenderSubjectBreak Then
          writeTd ""
            display = checkSubject(locArrT(3, locN))
            display = packText(display, intListingSubjectWidth)
            mi0 = locArrT(0, locN)
            linkParams = "MI0,NOBR"
            If locArrT(5, locN) Then linkParams = linkParams & ",BOLD"
            writeLinkWParams "MSR.asp", display, linkParams
            rw "&nbsp;"
          writeTdEnd
        End If

        If blnShowTimesInFolder Then
          If IsDate(locArrT(4, locN)) Then
            display = FormatDateTime(locArrT(4, locN), vbShortTime)
          Else
            display = NONETEXT
          End If
          writeTd ""
            rw displayEncode0(display) & "&nbsp;"
          writeTdEnd
        End If

      writeTrEnd

      If blnSenderSubjectBreak Then
        writeTr strAttributes
          writeTd "": rw "&nbsp;" : writeTdEnd
          intColSpan = 1
          If blnShowTimesInFolder Then intColSpan = intColSpan + 1
          writeTd "colspan='" & intColSpan & "'"
            display = checkSubject(locArrT(3, locN))
            display = packText(display, intListingSubjectWidth)
            mi0 = locArrT(0, locN)
            linkParams = "MI0,NOBR"
            If locArrT(5, locN) Then linkParams = linkParams & ",BOLD"
            writeLinkWParams "MSR.asp", display, linkParams
            rw "&nbsp;"
          writeTdEnd
        writeTrEnd
      End If

    Next

  writeTableEnd

End Sub

Function listPosts2

  Dim locN
  ReDim locArrOut(6, (lastMessage - firstMessage))
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
     "SELECT" & _
     " ""http://schemas.microsoft.com/exchange/outlookmessageclass""" & _
     ",""urn:schemas:httpmail:date""" & _
     ",""urn:schemas:httpmail:hasattachment""" & _
     ",""urn:schemas:httpmail:read""" & _
     ",""urn:schemas:httpmail:sendername""" & _
     ",""urn:schemas:httpmail:subject""" & _
     " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
     " WHERE ""DAV:isfolder"" = False" & _
     " AND ""DAV:ishidden"" = False" & _
   "</a:sql></a:searchrequest>"

  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then

    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then

      locArrT2 = getXMLNodes("http://schemas.microsoft.com/exchange/", "outlookmessageclass")
      locArrT3 = getXMLNodes("urn:schemas:httpmail:", "date")
      locArrT4 = getXMLNodes("urn:schemas:httpmail:", "hasattachment")
      locArrT5 = getXMLNodes("urn:schemas:httpmail:", "read")
      locArrT6 = getXMLNodes("urn:schemas:httpmail:", "sendername")
      locArrT7 = getXMLNodes("urn:schemas:httpmail:", "subject")

      For locN = LBound(locArrT) To UBound(locArrT)
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
        locArrOut(2, locN) = locArrT6(locN)
        locArrOut(3, locN) = locArrT7(locN)
        If Trim(locArrT3(locN)) <> "" Then
          locArrOut(4, locN) = getDateTimeFromTZ(locArrT3(locN))
        Else
          locArrOut(4, locN) = ""
        End If
        locArrOut(5, locN) = (locArrT5(locN) = "0")
        locArrOut(6, locN) = (locArrT4(locN) = "1")
      Next

    End If
  End If
  listPosts2 = locArrOut

End Function

Sub listTasks

  Dim locArrT, locI
  oldAt0 = at0
  If at0 = "" Then
    at0 = 1
    writeLinkWParams strPageName, LISTALLTASKSTEXT, "AT0"
    blnShowAllTasks = False
  Else
    at0 = ""
    writeLinkWParams strPageName, LISTOPENTASKSTEXT, "AT0"
    blnShowAllTasks = True
  End If
  at0 = oldAt0
  writeUnderline
  locArrT = listTasks2(blnShowAllTasks)
  writeTable ""
    For locI = LBound(locArrT, 2) To UBound(locArrT, 2)
      If strCurrentRowType = "A" Then
        strCurrentRowType = "B"
      Else
        strCurrentRowType = "A"
      End If
      strAttributes = ""
      If (strCurrentRowType = "A") And (strTableRowColourA <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourA & "'"
      ElseIf (strCurrentRowType = "B") And (strTableRowColourB <> "") Then
        strAttributes = strAttributes & " bgcolor='" & strTableRowColourB & "'"
      End If
      writeTr strAttributes
        writeTd ""
          display = checkSubject(locArrT(1, locI))
          mi0 = locArrT(0, locI)
          writeLinkWParams "MSR.asp", display, "MI0,NOBR"
        writeTdEnd
      writeTrEnd
    Next
  writeTableEnd

End Sub

Function listTasks2(inShowAllTasks)

  Dim locD, locN
  ReDim locArrOut(1, lastMessage - firstMessage)
  strXML = "<a:searchrequest xmlns:a='DAV:'><a:sql>" & _
   "SELECT" & _
   " ""urn:schemas:httpmail:subject""" & _
   " FROM scope('shallow traversal of """ & strBaseURL & fi0 & """')" & _
   " WHERE ""DAV:isfolder"" = False" & _
   " AND ""DAV:ishidden"" = False"
  If Not inShowAllTasks Then strXML = strXML & _
   " AND ""http://schemas.microsoft.com/mapi/id/{00062003-0000-0000-C000-000000000046}/0x811C"" = CAST(False AS 'boolean')"
  strXML = strXML & "</a:sql></a:searchrequest>"

  strStatus = doWinHTTPSearch(strBaseURL & fi0, strXML, "rows=" & firstMessage - 1 & "-" & lastMessage - 1)
  If Left(strStatus, 1) = "2" Then
    locArrT = getXMLNodes("DAV:", "href")
    If SHOWERRORS Then On Error Resume Next
    locD = UBound(locArrT)
    errNumber = Err.Number
    If SHOWERRORS Then On Error Goto 0
    If errNumber = 0 Then
      locArrT2 = getXMLNodes("urn:schemas:httpmail:", "subject")
      For locN = LBound(locArrT) To locD
        locArrOut(0, locN) = Replace(locArrT(locN), strBaseURL, "")
        locArrOut(1, locN) = locArrT2(locN)
      Next
    End If
  End If
  listTasks2 = locArrOut

End Function

Function checkSubject(inSubject)

  Dim s
  s = inSubject
  s = Replace(s, vbCr, " ")
  s = Replace(s, vbLf, " ")
  s = Replace(s, vbTab, " ")
  s = Trim(s)
  If s = "" Then s = NONETEXT
  checkSubject = s

End Function

Function getImageName(inMessageClass, inUnread, inHasAttachment, inFollowUpFlag, inImportance)

  If Instr(inMessageClass, "IPM.Schedule.Meeting") <> 0 Then
    strGraphic = "Calendar.gif"
  Else
    strGraphic = "Msg"
    If inUnread Then
      strGraphic = strGraphic & "U"
    Else
      strGraphic = strGraphic & "R"
    End If
    If Not inHasAttachment Then
      strGraphic = strGraphic & "N"
    Else
      strGraphic = strGraphic & "A"
    End If
    ' Check Follow-up flag CdoPR_FLAG_STATUS
    ' 0 = No flag, 1 = White flag, 2 = Red flag 
    Select Case inFollowUpFlag
      Case 0
        strGraphic = strGraphic & "N"
      Case 1
        strGraphic = strGraphic & "W"
      Case 2
        strGraphic = strGraphic & "R"
      Case Else
        strGraphic = strGraphic & "N"
    End Select
    If inImportance <> 2 Then
      strGraphic = strGraphic & "N"
    Else
      strGraphic = strGraphic & "H"
    End If
    strGraphic = strGraphic & ".gif"
  End If
  getImageName = strGraphic

End Function
%>
