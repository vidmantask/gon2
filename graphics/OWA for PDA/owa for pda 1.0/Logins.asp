# <%
# This file contains Login:Mailbox mappings for installations
#  having CONST USELOGINNAME = True in Define.inc .
#
# For any Mailbox Names that are NOT the same as the Login Names
#  add a line beginning with a Login Name followed by a colon (:)
#  and then the Mailbox Name like this:
#
#  admin:johnd
#
# If you want to force the Mailbox Name page for any Login use an
#  asterisk (*) like this:
#
#  admin2:*
#
# Blank lines and lines beginning with a '#' are ignored.
#
# If a Login Name occurs more than once, the last match is the
#  one that will be used.
#
