%systemroot%\system32\inetsrv\appcmd.exe add app /site.name:%1 /path:%2 /physicalPath:%3
%systemroot%\system32\inetsrv\appcmd.exe set config %4 -section:defaultDocument /+files.[@start,value='Default%5']
%systemroot%\system32\inetsrv\appcmd.exe set config %4 -section:defaultDocument /enabled:true
%systemroot%\system32\inetsrv\appcmd.exe unlock config %4 -section:anonymousAuthentication /commit:apphost
%systemroot%\system32\inetsrv\appcmd.exe set config %4 -section:anonymousAuthentication /enabled:false
%systemroot%\system32\inetsrv\appcmd.exe unlock config %4 -section:basicAuthentication /commit:apphost
%systemroot%\system32\inetsrv\appcmd.exe set config %4 -section:basicAuthentication /enabled:true /defaultLogonDomain:\
%systemroot%\system32\inetsrv\appcmd.exe unlock config %4 -section:httpErrors /commit:apphost
%systemroot%\system32\inetsrv\appcmd.exe set config %4 -section:httpErrors /[statusCode='404'].path:%2/AT2%5 /[statusCode='404'].responseMode:ExecuteURL /[statusCode='404'].prefixLanguageFilePath:""
