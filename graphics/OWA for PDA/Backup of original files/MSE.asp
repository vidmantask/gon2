<% @ LANGUAGE = "VBScript" %>
<!--#include file="Define.inc"-->
<!--#include file="Lang.inc"-->
<!--#include file="Include.inc"-->
<!--#include file="ALL.inc"-->
<% writeHeaders %>

<!-- OWA For PDA COPYRIGHT 2001 ~ 2010 LEE DERBYSHIRE -->

<%
If Not SHOWERRORS Then On Error Resume Next

writePageStart ""
login

If errorCode = 0 Then

  Dim strRe0, strType

  writeBody
  setParam "ci1", "IPM.Note"
  writeForm "MSE.asp"

  Select Case mt0 And OPWMTMASK
    Case OPWCMN
      writeTitle NEWMESSAGETEXT
    Case OPWCMR
      writeTitle REPLYTEXT
    Case OPWCMA
      writeTitle REPLYTOALLTEXT
    Case OPWCMF
      writeTitle FORWARDTEXT
    Case OPWCME
      writeTitle EDITMESSAGETEXT
  End Select

  If ci0 = "" Then createMessage

  ' Get any recipient passed in the QueryString
  '  (i.e. from Address Lists display)
  ' In this case it must be a 'To' recipient
  re0 = Request.QueryString("re0")
  If re0 <> "" Then
    strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
       "<j:to/>" & _
     "</a:prop></a:propfind>"
    doWinHTTPPropfind strMailboxURL & ci0, strXML
    Dim strTo1
    strTo1 = getXMLNode("urn:schemas:httpmail:", "to")
    strTo1 = addAddressToString(strTo1, re0, re0)
    strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
       "<k:to>" & displayEncodeX0(strTo1) & "</k:to>" & _
     "</a:prop></a:set></a:propertyupdate>"
    doWinHTTPProppatch strMailboxURL & ci0, strXML
  End If

  If CUSTOM = 1 And ac0 = TO1TEXT Then ac0 = ADDRESSLISTSTEXT

  Dim p
  p = Instr(ac0, " (")
  If p <> 0 Then ac0 = Left(ac0, p - 1)

  If ac0 <> DELETETEXT Then

    If (pg0 = "M") Or (((pg0 = "RB") Or (pg0 = "RC")) And (ac0 = OKTEXT)) Then
      ' We are navigating away from the main page, or returning from one of the recipients pages.
      rs0 = getFormData("rs0")
      Dim strTo2
      strTo2 = checkNames(rs0)
      'Dim strType
      Select Case ry0
        Case 2
          strType = "cc"
        Case 3
          strType = "bcc"
        Case Else
          strType = "to"
      End Select
      strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
         "<k:" & strType & ">" & displayEncodeX0(strTo2) & "</k:" & strType & ">" & _
       "</a:prop></a:set></a:propertyupdate>"
      doWinHTTPProppatch strMailboxURL & ci0, strXML
    End If

    If pg0 = "M" Then
      If ac0 <> CLEARTEXT Then
        Dim strSubject
        strSubject = getFormData("subject")
        Dim strText
        strText = getFormData("text")
        updateMessageTextMSE ci0, strSubject, strText, pg1
        If ac0 = PREVIOUSTEXT Then
          pg1 = pg1 - 1
          setPref "pg1", pg1
        End If
        If ac0 = NEXTTEXT Then
          pg1 = pg1 + 1
          setPref "pg1", pg1
        End If
      Else
        strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
           "<j:subject></j:subject>" & _
           "<j:textdescription></j:textdescription>" & _
         "</a:prop></a:set></a:propertyupdate>"
        strStatus = doWinHTTPProppatch(strMailboxURL & ci0, strXML)
        If Left(strStatus, 1) = "2" Then
          Dim s
          s = getXMLNode("DAV:", "href")
          ci0 = Right(s, Len(s) - Len(strBaseURL))
          setParam "ci0", ci0
        End If
        pg1 = 1
        setPref "pg1", pg1
      End If
    End If

    If ((pg0 = "RB") Or (pg0 = "RC")) And (ac0 = ADDRESSLISTSTEXT) Then
      rs0 = getFormData("rs0")
      setParam "newRs0", rs0
    End If

    If pg0 = "A" Then
      Dim strTo3
      strTo3 = ""
      'Dim strType
      Select Case ry0
        Case 2
          strType = "cc"
        Case 3
          strType = "bcc"
        Case Else
          strType = "to"
      End Select
      strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
         "<j:" & strType & "/>" & _
       "</a:prop></a:propfind>"
      doWinHTTPPropfind strMailboxURL & ci0, strXML
      Dim strTo4
      strTo4 = getXMLNode("urn:schemas:httpmail:", "to")
      Dim arrTo
      arrTo = getAddresses(strTo4)
      Dim i
      For i = 0 To UBound(arrTo, 2)
        If i <> 0 Then strTo3 = strTo3 & ";"
        strTo3 = strTo3 & arrTo(1, i)
      Next
      'Dim strRe0
      For Each strRe0 In Request.Form("re0")
        If strTo3 <> "" Then strTo3 = strTo3 & "; "
        strTo3 = strTo3 & strRe0
      Next
      strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
         "<k:" & strType & ">" & displayEncodeX0(strTo3) & "</k:" & strType & ">" & _
       "</a:prop></a:set></a:propertyupdate>"
      doWinHTTPProppatch strMailboxURL & ci0, strXML
    End If

    If (pg0 = "AB") Or (pg0 = "AC") Then
      Dim strTo5
      strTo5 = getParam("newRs0", False)
      For Each strRe0 In Request.Form("re0")
        If strTo5 <> "" Then strTo5 = strTo5 & "; "
        strTo5 = strTo5 & strRe0
      Next
      setParam "newRs0", strTo5
    End If

    If pg0 = "O" Then
      Dim intRdr
      intRdr = 0 : If getFormData("rdr") <> "" Then intRdr = 1
      Dim intRrr
      intRrr = 0 : If getFormData("rrr") <> "" Then intRrr = 1
      Dim intImp
      intImp = getFormData("imp")
      Dim intSens
      intSens = getFormData("sens")
      strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
         "<d:sensitivity>" & intSens & "</d:sensitivity>" & _
         "<g:x0023000b>" & intRdr & "</g:x0023000b>" & _
         "<g:x0029000b>" & intRrr & "</g:x0029000b>" & _
         "<j:importance>" & intImp & "</j:importance>" & _
       "</a:prop></a:set></a:propertyupdate>"
      doWinHTTPProppatch strMailboxURL & ci0, strXML
    End If

  End If

  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<j:cc/>" & _
     "<j:displaybcc/>" & _
     "<j:to/>" & _
   "</a:prop></a:propfind>"
  doWinHTTPPropfind strMailboxURL & ci0, strXML

  Dim strTo
  strTo = getXMLNode("urn:schemas:httpmail:", "to")
  Dim strCc
  strCc = getXMLNode("urn:schemas:httpmail:", "cc")
  Dim strBcc
  strBcc = getXMLNode("urn:schemas:httpmail:", "displaybcc")

  Dim blnHasRecipients
  blnHasRecipients = False
  If (strTo <> "") Or (strCc <> "") Or (strBcc <> "") Then blnHasRecipients = True

  Select Case ac0
    Case "", "A", CLEARTEXT, "M", NEXTTEXT, PREVIOUSTEXT
      doMessageForm
    Case ADDRESSLISTSTEXT, GOTEXT
      doAddressListsForm
    Case ATTACHMENTSTEXT
      Response.Redirect "MSEA.asp?mb0=" & Server.URLEncode(mb0) & "&si0=" & si0
    Case BCCTEXT, CCTEXT, "RB", "RC"
      doRecipientsForm
    Case DELETETEXT
      doDeleteForm
    Case OKTEXT
      Select Case pg0
        Case "A", "RB", "RC"
          doMessageForm
        Case "AB", "AC"
          doRecipientsForm
      End Select
    Case OPTIONSTEXT
      doOptionsForm
    Case SAVETEXT
      doSaveForm
    Case SENDTEXT
      If blnHasRecipients Then
        doSendForm
      Else
        doMessageForm
      End If
  End Select

  rwc "</form>"

End If

logoff
writePageEnd

Sub createMessage

  Dim strText

  Dim strSig
  strSig = getSig

  If mt0 And OPWCMN Then

    If strSig <> "" Then strText = vbCrLf & vbCrLf & strSig & vbCrLf

    strURL = getNextFreeURL("", strDraftsURL, "", ".EML")
    strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
       "<j:subject></j:subject>" & _
       "<j:textdescription>" & displayEncodeX0(strText) & "</j:textdescription>" & _
     "</a:prop></a:set></a:propertyupdate>"
    doWinHTTPProppatch strMailboxURL & strDraftsURL & strURL, strXML
    ci0 = strDraftsURL & strURL

  Else

    If st0 <> "u" Then

      'ci0 = tryOWA2007ReplyEtc(mi0)

      'If ci0 = "" Then

        ci0 = tryOWA2003ReplyEtc(mi0)
        ' Get the original message addresses and subject
        ' to add to the top of the new message text
        strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
           "<j:date/>" & _
           "<j:displayto/>" & _
           "<j:from/>" & _
           "<j:subject/>" & _
         "</a:prop></a:propfind>"
        strStatus = doWinHTTPPropfind(strMailboxURL & mi0, strXML)
        If Left(strStatus, 1) = "2" Then
          strText = " " & vbCrLf & vbCrLf & ORIGINALMESSAGETEXT & vbCrLf
          strText = strText & FROMTEXT & COLONSPACETEXT & getXMLNode("urn:schemas:httpmail:", "from") & vbCrLf
          strText = strText & SENTTEXT & COLONSPACETEXT & getXMLNode("urn:schemas:httpmail:", "date") & vbCrLf
          strText = strText & TO1TEXT & COLONSPACETEXT & getXMLNode("urn:schemas:httpmail:", "displayto") & vbCrLf
          strText = strText & SUBJECTTEXT & COLONSPACETEXT & getXMLNode("urn:schemas:httpmail:", "subject") & vbCrLf & vbCrLf
        End If

      'End If

      If ci0 = "" Then Response.Write("ci0 is empty") : Response.End

      strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
         "<j:textdescription/>" & _
       "</a:prop></a:propfind>"
      strStatus = doWinHTTPPropfind(strMailboxURL & ci0, strXML)
      If Left(strStatus, 1) = "2" Then
        strText = strText & vbCrLf & vbCrLf & getXMLNode("urn:schemas:httpmail:", "textdescription")
        If strSig <> "" Then strText = vbCrLf & vbCrLf & strSig & vbCrLf & vbCrLf & strText
        strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
           "<j:textdescription>" & displayEncodeX0(strText) & "</j:textdescription>" & _
         "</a:prop></a:set></a:propertyupdate>"
        doWinHTTPProppatch strMailboxURL & ci0, strXML
      End If

    Else

      Dim strCc, strDisplayTo, strFrom
      Dim strSenderName, strSubject
      Dim strTextDescription, strTo

      strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
         "<j:cc/>" & _
         "<j:displayto/>" & _
         "<j:from/>" & _
         "<j:sendername/>" & _
         "<j:subject/>" & _
         "<j:textdescription/>" & _
         "<j:to/>" & _
       "</a:prop></a:propfind>"
      strStatus = doWinHTTPPropfind(strBaseURL & mi0, strXML)
      If Left(strStatus, 1) = "2" Then
        strCc = getXMLNode("urn:schemas:httpmail:", "cc")
        strDisplayTo = getXMLNode("urn:schemas:httpmail:", "displayto")
        strFrom = getXMLNode("urn:schemas:httpmail:", "from")
        strSenderName = getXMLNode("urn:schemas:httpmail:", "sendername")
        strSubject = getXMLNode("urn:schemas:httpmail:", "subject")
        strTextDescription = getXMLNode("urn:schemas:httpmail:", "textdescription")
        strTo = getXMLNode("urn:schemas:httpmail:", "to")
      End If

      Dim strCc2, strSubject2, strTo2

      strSubject2 = strSubject

      If mt0 And OPWCMR Then
        strTo2 = strFrom
        If Left(UCase(strSubject), Len(RETEXT & COLONSPACETEXT)) <> RETEXT & COLONSPACETEXT Then
          strSubject2 = RETEXT & COLONSPACETEXT & strSubject2
        End If
      ElseIf mt0 And OPWCMA Then
        strTo2 = strFrom
        If strTo <> "" Then strTo2 = strTo2 & ", " & strTo
        strCc2 = strCc
        If Left(UCase(strSubject), Len(RETEXT & COLONSPACETEXT)) <> RETEXT & COLONSPACETEXT Then
          strSubject2 = RETEXT & COLONSPACETEXT & strSubject2
        End If
      ElseIf mt0 And OPWCMF Then
        If Left(UCase(strSubject), Len(FWTEXT & COLONSPACETEXT)) <> FWTEXT & COLONSPACETEXT Then
          strSubject2 = FWTEXT & COLONSPACETEXT & strSubject2
        End If
      End If

      strText = strText & vbCrLf & vbCrLf & ORIGINALMESSAGETEXT & vbCrLf
      strText = strText & FROMTEXT & COLONSPACETEXT & strSenderName & vbCrLf
      strText = strText & TO1TEXT & COLONSPACETEXT & strDisplayTO & vbCrLf
      strText = strText & vbCrLf & _
       SUBJECTTEXT & COLONSPACETEXT & strSubject & vbCrLf & _
       vbCrLf
      strText = strText & strTextDescription

      strURL = getNextFreeURL("", strDraftsURL, "", ".EML")
      strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
         "<j:subject>" & displayEncodeX0(strSubject2) & "</j:subject>" & _
         "<j:textdescription>" & displayEncodeX0(strText) & "</j:textdescription>" & _
         "<k:cc>" & displayEncodeX0(strCc2) & "</k:cc>" & _
         "<k:to>" & displayEncodeX0(strTo2) & "</k:to>" & _
       "</a:prop></a:set></a:propertyupdate>"

      doWinHTTPProppatch strMailboxURL & strDraftsURL & strURL, strXML

      ci0 = strDraftsURL & strURL

    End If

  End If

  setParam "ci0", ci0

End Sub

Sub doAddressListsForm

  Select Case pg0
    Case "A", "M"
      pg0 = "A"
    Case "RB"
      pg0 = "AB"
    Case "RC"
      pg0 = "AC"
  End Select
  rwc "<input type=""hidden"" name=""pg0"" value=""" & pg0 & """>"

  Select Case ry0
    Case 1
      writeTitle TO1TEXT & COLONSPACETEXT
    Case 2
      writeTitle CCTEXT & COLONSPACETEXT
    Case 3
      writeTitle BCCTEXT & COLONSPACETEXT
  End Select

  Dim oldAc0
  oldAc0 = ac0
  Select Case ry0
    Case 1
      ac0 = "M"
    Case 2
      ac0 = "RC"
    Case 3
      ac0 = "RB"
  End Select

  writeLinkWParams "MSE.asp", CANCELTEXT, "AC0"
  ac0 = oldAc0
  writeUnderline

  Dim strDisplayName
  If Left(al0, 4) = "http" Then
    strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
       "<a:displayname/>" & _
     "</a:prop></a:propfind>"
    doWinHTTPPropfind al0, strXML
    strDisplayName = getXMLNode("DAV:", "displayname")
    intNumItems = getNumberOfContacts
    getFirstAndLastContact
    If intNumItems > 0 Then
      strDisplayname = strDisplayname & COLONSPACETEXT & intFirstItem & "-" & intLastItem & " " & OFTEXT & " " & intNumItems
    End If
  Else
    If USESPECIFIEDGAL = "" Then
      strDisplayName = al0
      p = Instr(strDisplayName, ",CN=")
      If p <> 0 Then
        strDisplayName = Left(strDisplayName, p - 1)
        strDisplayName = Replace(strDisplayName, "CN=", "")
      End If
    Else
      Dim arrSpecifiedGAL
      arrSpecifiedGAL = Split(USESPECIFIEDGAL, ":")
      If UBound(arrSpecifiedGAL) = 0 Then
        arrSpecifiedGAL = Split(USESPECIFIEDGAL, ",")
        strDisplayName = Replace(arrSpecifiedGAL(0), "CN=", "")
      Else
        strDisplayName = arrSpecifiedGAL(1)
      End If
    End If
    'strDisplayName = al0
    'Dim p
    'p = Instr(strDisplayName, ",CN=")
    'If p <> 0 Then
    '  strDisplayName = Left(strDisplayName, p - 1)
    '  strDisplayName = Replace(strDisplayName, "CN=", "")
    'End If
  End If
  writeTitle strDisplayName

  doAddressLists

  writeTitle FILTERTEXT & COLONSPACETEXT

  rwc "<select name='fl1'>"

  rw "<option value='name'"
  If fl1 = "name" Then rw " selected"
  rwc ">" & displayEncodeI0(NAMETEXT) & "</option>"

  rw "<option value='address'"
  If fl1 = "address" Then rw " selected"
  rwc ">" & displayEncodeI0(BUSINESSADDRESSTEXT) & "</option>"

  rw "<option value='phone'"
  If fl1 = "phone" Then rw " selected"
  rwc ">" & displayEncodeI0(BUSINESSPHONETEXT) & "</option>"

  rw "<option value='company'"
  If fl1 = "company" Then rw " selected"
  rwc ">" & displayEncodeI0(COMPANYTEXT) & "</option>"

  rw "<option value='department'"
  If fl1 = "department" Then rw " selected"
  rwc ">" & displayEncodeI0(DEPARTMENTTEXT) & "</option>"

  rwbc "</select>"

  rwc "<input type=""text"" class=""inputTextFilter"" name='fl0' value='" & displayEncodeI0(fl0) & "'>"

  doInputSubmit "ac0", GOTEXT, True

  If Left(al0, 4) = "http" Then
    doTopPreviousNextLinksContacts
    writeUnderline
    doContacts
  Else
    writeUnderline
    doGAL
  End If

  doInputSubmit "ac0", OKTEXT, True

End Sub

Sub doDeleteForm
  doWinHTTPDelete strBaseURL & ci0, ""
  ci0 = ""
  setParam "ci0", ci0
  If Not STARTININBOX Then
    If mt0 And OPWFRMB Then
      rwc "<meta http-equiv='refresh' content='1; URL=MBX.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRMR Then
      rwc "<meta http-equiv='refresh' content='1; URL=MSR.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRFO Then
      rwc "<meta http-equiv='refresh' content='1; URL=MSL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRAL Then
      rwc "<meta http-equiv='refresh' content='1; URL=ALL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    End If
  Else
    rwc "<meta http-equiv='refresh' content='1; URL=MSL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "&fi0=OPWINBOX'>"
  End If
  If Not STARTININBOX Then
    If mt0 And OPWFRMB Then
      writeLinkWParams "MBX.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRMR Then
      writeLinkWParams "MSR.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRFO Then
      writeLinkWParams "MSL.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRAL Then
      writeLinkWParams "ALL.asp", OKTEXT, ""
    End If
  Else
    Dim oldFi0
    oldFi0 = fi0
    fi0 = "OPWINBOX"
    writeLinkWParams "MSL.asp", OKTEXT, "FI0"
    fi0 = oldFi0
  End If
End Sub

Sub doMessageForm

  rwc "<input type=""hidden"" name=""pg0"" value=""M"">"

  setParam "ry0", 1

  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<j:cc/>" & _
     "<j:displaybcc/>" & _
     "<j:to/>" & _
   "</a:prop></a:propfind>"
  doWinHTTPPropfind strMailboxURL & ci0, strXML

  Dim strTo
  strTo = getXMLNode("urn:schemas:httpmail:", "to")
  Dim strCc
  strCc = getXMLNode("urn:schemas:httpmail:", "cc")
  Dim strBcc
  strBcc = getXMLNode("urn:schemas:httpmail:", "displaybcc")
  strBcc = checkNames(strBcc)

  Dim arrTo
  arrTo = getAddresses(strTo)

  rs0 = ""
  Dim i
  For i = 0 To UBound(arrTo, 2)
    If i <> 0 Then rs0 = rs0 & ";"
    rs0 = rs0 & arrTo(1, i)
  Next

  Dim intNumCc
  intNumCc = UBound(getAddresses(strCc), 2) + 1
  Dim intNumBcc
  intNumBcc = UBound(getAddresses(strBcc), 2) + 1

  If CUSTOM <> 1 Then
    doTopButtons intNumCc, intNumBcc
  Else
    rwc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSESend"" value=""" & SENDTEXT & """>"
    rwc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSESave"" value=""" & SAVETEXT & """>"
    rwbc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSEDelete"" value=""" & DELETETEXT & """>"
    rwbc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSETo"" value=""" & TO1TEXT & """>"
    If Instr(rs0, ";") = 0 Then
      rwbc "<input type=""text"" class=""inputText"" name=""rs0"" value=""" & displayEncodeI0(rs0) & """>"
    Else
      rw "<pre><textarea name=""rs0"""
      rwc ">" & displayEncodeI0(rs0) & "</textarea></pre>"
    End If
    Dim strCaption
    strCaption = CCTEXT
    If intNumCc <> 0 Then strCaption = strCaption & " (" & intNumCc & ")"
    rwc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSECc"" value=""" & strCaption & """>"
    strCaption = BCCTEXT
    If intNumBcc <> 0 Then strCaption = strCaption & " (" & intNumBcc & ")"
    rwbc "<input type=""submit"" name=""ac0"" class=""inputSubmitMSEBcc"" value=""" & strCaption & """>"
  End If

  If CUSTOM <> 1 Then
    writeTitle TO1TEXT & COLONSPACETEXT
    If Instr(rs0, ";") = 0 Then
      rwbc "<input type=""text"" class=""inputText"" name=""rs0"" value=""" & displayEncodeI0(rs0) & """>"
    Else
      rw "<pre><textarea name=""rs0"""
      rwc ">" & displayEncodeI0(rs0) & "</textarea></pre>"
    End If
  End If

  rwc "<input type=""hidden"" name=""pg1"" value=""" & CStr(pg1) & """>"
  doMessageTextMSE ci0, pg1

  doBottomButtons

End Sub

Sub doBottomButtons
  doInputSubmit "ac0", OPTIONSTEXT, False
  If blnCanSendAttachments Then doInputSubmit "ac0", ATTACHMENTSTEXT, False
  rwbc ""
End Sub

Sub doTopButtons(inCc, inBcc)
  Dim strCaption
  doInputSubmit "ac0", SENDTEXT, False
  doInputSubmit "ac0", SAVETEXT, False
  doInputSubmit "ac0", DELETETEXT, True
  doInputSubmit "ac0", ADDRESSLISTSTEXT, False
  strCaption = CCTEXT
  If inCc <> 0 Then strCaption = strCaption & " (" & inCc & ")"
  doInputSubmit "ac0", strCaption, False
  strCaption = BCCTEXT
  If inBcc <> 0 Then strCaption = strCaption & " (" & inBcc & ")"
  doInputSubmit "ac0", strCaption, True
End Sub

Sub doOptionsForm

  rwc "<input type=""hidden"" name=""pg0"" value=""O"">"
  writeLinkWParams "MSE.asp", CANCELTEXT, ""
  writeUnderline

  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<d:sensitivity/>" & _
     "<g:x0023000b/>" & _
     "<g:x0029000b/>" & _
     "<j:importance/>" & _
   "</a:prop></a:propfind>"
  doWinHTTPPropfind strMailboxURL & ci0, strXML

  Dim blnRdr
  blnRdr = getXMLNode("http://schemas.microsoft.com/mapi/proptag/", "x0023000b") = "1"
  Dim blnRrr
  blnRrr = getXMLNode("http://schemas.microsoft.com/mapi/proptag/", "x0029000b") = "1"
  Dim intImp
  intImp = getXMLNode("urn:schemas:httpmail:", "importance")
  Dim intSens
  intSens = getXMLNode("http://schemas.microsoft.com/exchange/", "sensitivity")

  rw "<input type=""checkbox"" name=""rdr"""
  If blnRdr Then rw " checked"
  rwc ">"
  rwbc displayEncode0(REQUESTDELIVERYRECEIPTQTEXT)

  rw "<input type=""checkbox"" name=""rrr"""
  If blnRrr Then rw " checked"
  rwc ">"
  rwbc(displayEncode0(REQUESTREADRECEIPTQTEXT))

  rwc displayEncode0(IMPORTANCETEXT & COLONSPACETEXT)
  rwc "<select name=""imp"">"
  rw "<option value=""0"""
  If intImp = 0 Then rw " selected"
  rwc ">" & displayEncodeI0(LOWTEXT) & "</option>"
  rw "<option value=""1"""
  If intImp = 1 Then rw " selected"
  rwc ">" & displayEncodeI0(NORMALTEXT) & "</option>"
  rw "<option value=""2"""
  If intImp = 2 Then rw " selected"
  rwc ">" & displayEncodeI0(HIGHTEXT) & "</option>"
  rwbc "</select>"

  rwc displayEncode0(SENSITIVITYTEXT & COLONSPACETEXT)
  rwc "<select name=""sens"">"
  rw "<option value=""0"""
  If intSens = 0 Then rw " selected"
  rwc ">" & displayEncodeI0(NORMALTEXT) & "</option>"
  rw "<option value=""1"""
  If intSens = 1 Then rw " selected"
  rwc ">" & displayEncodeI0(PERSONALTEXT) & "</option>"
  rw "<option value=""2"""
  If intSens = 2 Then rw " selected"
  rwc ">" & displayEncodeI0(PRIVATETEXT) & "</option>"
  rw "<option value=""3"""
  If intSens = 3 Then rw " selected"
  rwc ">" & displayEncodeI0(CONFIDENTIALTEXT) & "</option>"
  rwbc "</select>"

  doInputSubmit "", OKTEXT, False

End Sub

Sub doRecipientsForm

  Dim locArrT
  Dim strURL, locN
  Dim locName, locAddress, locT
  Dim strType

  rs0 = ""
  setParam "rs0", rs0

  Select Case ac0
    Case BCCTEXT
      ry0 = 3
      setParam "ry0", ry0
    Case CCTEXT
      ry0 = 2
      setParam "ry0", ry0
  End Select

  Select Case ry0
    Case 3
      writeTitle BCCTEXT & COLONSPACETEXT
      rwc "<input type=""hidden"" name=""pg0"" value=""RB"">"
      strType = "displaybcc"
    Case 2
      writeTitle CCTEXT & COLONSPACETEXT
      rwc "<input type=""hidden"" name=""pg0"" value=""RC"">"
      strType = "cc"
  End Select

  rwbc displayEncode0(RECIPIENTSTEXT)
  writeLinkWParams "MSE.asp", CANCELTEXT, ""

  Select Case pg0
    Case "M"
      strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
       "<j:" & strType & "/>" & _
       "</a:prop></a:propfind>"

      strURL = strMailboxURL & ci0
      strStatus = doWinHTTPPropfind(strURL, strXML)
      If Left(strStatus, 1) = "2" Then
        locT = getXMLNode("urn:schemas:httpmail:", strType)
        If strType = "displaybcc" Then locT = checkNames(locT)
        locArrT = getAddresses(locT)
        'If Not(IsNothing(locArrT)) Then
          For locN = 0 To UBound(locArrT, 2)
            locName = locArrT(0, locN)
            locAddress = locArrT(1, locN)
            If locName = "" Then locName = locAddress
            If locAddress = "" Then locAddress = locName
            If rs0 <> "" Then rs0 = rs0 & "; "
            rs0 = rs0 & locAddress
          Next
        'End If
      End If
      setParam "newRs0", rs0
    Case "AB", "AC"
      rs0 = getParam("newRs0", False)
  End Select
  doTextArea "rs0", rs0

  doInputSubmit "ac0", ADDRESSLISTSTEXT, True
  doInputSubmit "ac0", OKTEXT, True

End Sub

Sub doSaveForm
  ci0 = ""
  setParam "ci0", ci0
  If Not STARTININBOX Then
    If mt0 And OPWFRMB Then
      rwc "<meta http-equiv='refresh' content='1; URL=MBX.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRMR Then
      rwc "<meta http-equiv='refresh' content='1; URL=MSR.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRFO Then
      rwc "<meta http-equiv='refresh' content='1; URL=MSL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    ElseIf mt0 And OPWFRAL Then
      rwc "<meta http-equiv='refresh' content='1; URL=ALL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "'>"
    End If
  Else
    rwc "<meta http-equiv='refresh' content='1; URL=MSL.asp?mb0=" & mb0 & "&amp;" & "si0=" & si0 & "&fi0=OPWINBOX'>"
  End If
  If Not STARTININBOX Then
    If mt0 And OPWFRMB Then
      writeLinkWParams "MBX.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRMR Then
      writeLinkWParams "MSR.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRFO Then
      writeLinkWParams "MSL.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRAL Then
      writeLinkWParams "ALL.asp", OKTEXT, ""
    End If
  Else
    Dim oldFi0
    oldFi0 = fi0
    fi0 = "OPWINBOX"
    writeLinkWParams "MSL.asp", OKTEXT, "FI0"
    fi0 = oldFi0
  End If
End Sub

Sub doContacts

  locArrT = getContacts
  If SHOWERRORS Then On Error Resume Next
  locN = UBound(locArrT, 2) + 1
  If locN = 0 Then rwbc NOTFOUNDTEXT
  errNumber = Err.Number
  If SHOWERRORS Then On Error Goto 0
  If errNumber = 0 Then
    If locN <> 0 Then

      For locL = 0 To locN - 1
        rwc "<input type=""checkbox"" name=""re0"" value=""" & displayEncodeI0(locArrT(1, locL)) & """>"
        rwbc displayEncodeI0(locArrT(0, locL))
      Next

      'rwc "<select name='re0' multiple='yes'>"
      'For locL = LBound(locArrT, 2) To locN - 1
      '  rwc "<option value='" & displayEncodeI0(locArrT(1, locL)) & "'>" & displayEncodeI0(locArrT(0, locL)) & "</option>"
      'Next
      'rwbc "</select>"

    End If
  End If

End Sub

Sub doGAL
  Dim arrT
  arrT = getGAL
  Dim n
  n = UBound(arrT, 2)
  If n = -1 Then rwbc NOTFOUNDTEXT
  For i = 0 To n
    rwc "<input type=""checkbox"" name=""re0"" value=""" & displayEncodeI0(arrT(2, i)) & """>"
    rw displayEncodeI0(arrT(0, i))
    Dim strDepartment, strTitle
    strDepartment = arrT(3, i)
    strTitle = arrT(4, i)
    If strDepartment <> "" Then rw "/" & displayEncode0(strDepartment)
    If strTitle <> "" Then rw "/" & displayEncode0(strTitle)
    rwbc ""
  Next
End Sub

Sub doMessageTextMSE(inItemID, inPage)
  Dim arrS
  arrS = doMessageTextMSE2(inItemID)
  Dim strSubject
  strSubject = arrS(0)
  Dim strText
  strText = arrS(1)
  writeTitle SUBJECTTEXT & COLONSPACETEXT
  rwbc "<input type=""text"" class=""inputText"" name='subject' value=""" & displayEncodeI0(strSubject) & """>"
  Dim l
  l = Len(strText)
  Dim p, p2
  p = 0
  Dim i
  For i = 1 To inPage
    p2 = p + 1
    If p + (intMaxPageSize - 1) < l Then
      p = p2 + (intMaxPageSize - 1)
      Dim b
      b = False
      Dim i2
      For i2 = p To (p2 + 1) Step - 1
        Dim c
        c = Mid(strText, i2, 1)
        If (c = " ") Or (c = vbCrLf) Or (c = vbTab) Then
          b = True
          Exit For
        End If
      Next
      If b Then p = i2
    Else
      p = l
    End If
  Next
  Dim s
  s = Mid(strText, p2, (p - p2) + 1)
  writeTitle MESSAGETEXT & COLONSPACETEXT
  doTextArea "text", s
  doInputSubmit "ac0", CLEARTEXT, False
  If inPage > 1 Then
    doInputSubmit "ac0", PREVIOUSTEXT, False
  End If
  If p < l Then
    doInputSubmit "ac0", NEXTTEXT, False
  End If
  rwbc ""
End Sub

Function doMessageTextMSE2(inItemID)
  Dim arrS(1)
  strXML = "<a:propfind " & strXMLNS & "><a:prop>" & _
     "<j:subject/>" & _
     "<j:textdescription/>" & _
   "</a:prop></a:propfind>"
  doWinHTTPPropfind strMailboxURL & ci0, strXML
  Dim s
  s = getXMLNode("urn:schemas:httpmail:", "subject")
  Dim s2
  s2 = getXMLNode("urn:schemas:httpmail:", "textdescription")
  arrS(0) = s
  arrS(1) = s2
  doMessageTextMSE2 = arrS
End Function

Sub doSendForm

  rwc "Sorry, you are not able to send"
  rwc "a message with the demo version"
  rwbc "of this software."
  doWinHTTPDelete strMailboxURL & ci0, ""
  ci0 = ""
  setParam "ci0", ci0
  If Not STARTININBOX Then
    If mt0 And OPWFRMB Then
      writeLinkWParams "MBX.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRMR Then
      writeLinkWParams "MSR.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRFO Then
      writeLinkWParams "MSL.asp", OKTEXT, ""
    ElseIf mt0 And OPWFRAL Then
      writeLinkWParams "ALL.asp", OKTEXT, ""
    End If
  Else
    Dim oldFi0
    oldFi0 = fi0
    fi0 = "OPWINBOX"
    writeLinkWParams "MSL.asp", OKTEXT, "FI0"
    fi0 = oldFi0
  End If

End Sub

Function tryOWA2003ReplyEtc(inMi0)
  Dim strCi0, strCmd
  If mt0 And OPWCMR Then
    strCmd = "reply"
  ElseIf mt0 And OPWCMA Then
    strCmd = "replyall"
  ElseIf mt0 And OPWCMF Then
    strCmd = "forward"
  End If
  Dim strURL
  strURL = strMailboxURL & inMi0 & "/?Cmd=" & strCmd
  strStatus = doWinHTTPGet(strURL, "")
  If Left(strStatus, 1) <> "2" Then
    rwbc "Failed GET " & strURL
    Response.End
  End If
  Dim s
  s = objWinHTTP.Option(1)
  Dim s2
  s2 = Right(s, Len(s) - Len(strMailboxURL))
  Dim p
  p = InstrRev(UCase(s2), "?CMD=" & UCase(strCMD))
  If p <> 0 Then strCi0 = Left(s2, p - 1)
  tryOWA2003ReplyEtc = strCi0
End Function

Sub updateMessageTextMSE(inItemID, inSubject, inText, inPage)
  Dim arrS
  arrS = doMessageTextMSE2(inItemID)
  Dim strText
  strText = arrS(1)
  Dim l
  l = Len(strText)
  Dim p, p2
  p = 0
  Dim i
  For i = 1 To inPage
    p2 = p + 1
    If p + (intMaxPageSize - 1) < l Then
      p = p2 + (intMaxPageSize - 1)
      Dim b
      b = False
      Dim i2
      For i2 = p To (p2 + 1) Step - 1
        Dim c
        c = Mid(strText, i2, 1)
        If (c = " ") Or (c = vbCrLf) Or (c = vbTab) Then
          b = True
          Exit For
        End If
      Next
      If b Then p = i2
    Else
      p = l
    End If
  Next
  Dim s
  s = "" : If p2 > 1 Then s = Left(strText, p2 - 1)
  Dim s2
  s2 = "" : If p < Len(strText) Then s2 = Right(strText, Len(strText) - p)
  Dim s3
  s3 = s & inText & s2
  updateMessageTextMSE2 inItemID, inSubject, s3
End Sub

Sub updateMessageTextMSE2(inItemID, inSubject, inText)
  strXML = "<a:propertyupdate " & strXMLNS & "><a:set><a:prop>" & _
     "<j:subject>" & displayEncodeX0(inSubject) & "</j:subject>" & _
     "<j:textdescription>" & displayEncodeX0(inText) & "</j:textdescription>" & _
   "</a:prop></a:set></a:propertyupdate>"
  strStatus = doWinHTTPProppatch(strMailboxURL & inItemID, strXML)
  If Left(strStatus, 1) = "2" Then
    Dim s
    s = getXMLNode("DAV:", "href")
    ci0 = Right(s, Len(s) - Len(strBaseURL))
    setParam "ci0", ci0
  End If
End Sub
%>
