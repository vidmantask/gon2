' List here any mandatory user settings
' For SendAttachments, SenderWidth, SubjectWidth, Theme
' Use the format User:Setting:Value
' E.g. JohnDoe:Theme:Olive
' To force a setting for all users, use * for the username
' To make an exceptional setting for one user, it must come after a * setting
' Leave a setting blank to allow a user to choose their own value
' for Theme
' Users can always choose their own value for
' for SenderWidth, ShowDatesAndTimes, SubjectWidth, SubjectBelowSender

*:HideTitles:0
*:SenderWidth:12
*:SendAttachments:False
*:ShowDatesInFolders:True
*:ShowTimesInFolders:
*:SubjectBelowSender:
*:SubjectWidth:12
*:Theme:
