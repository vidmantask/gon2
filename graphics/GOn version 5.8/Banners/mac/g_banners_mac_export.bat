REM this produces banners in lower resolution, especially for mac

REM ---------------------------------------------------------------------
REM Enroll

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3327 -d72 -e g_enroll_banner.png g_banners_mac.svg


REM ---------------------------------------------------------------------
REM Install

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3281 -d72 -e g_install_banner.png g_banners_mac.svg


REM ---------------------------------------------------------------------
REM Login

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3224 -d72 -e g_login_banner.png g_banners_mac.svg


REM ---------------------------------------------------------------------
REM Remove

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3373 -d72 -e g_remove_banner.png g_banners_mac.svg


REM ---------------------------------------------------------------------
REM Uninstall

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3415 -d72 -e g_uninstall_banner.png g_banners_mac.svg


REM ---------------------------------------------------------------------
REM Update

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3457 -d72 -e g_update_banner.png g_banners_mac.svg



REM ---------------------------------------------------------------------
REM CONVERT ALL FILES TO .bmp
REM NOTICE: Conversion to .bmp must be done manually (use Paint.net). When converted with gimp-scripting, the .bmp images doesn't work in the client!!!!!
