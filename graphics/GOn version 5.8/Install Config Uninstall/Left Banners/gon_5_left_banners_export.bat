REM ---------------------------------------------------------------------
REM Installer

"/Program Files/Inkscape/Inkscape.com" --export-id=rect3952 -e gon_5_installer_h.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM Installer Beta

"/Program Files/Inkscape/Inkscape.com" --export-id=rect1477 -e gon_5_installer_h_beta.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM Server Configuration

"/Program Files/Inkscape/Inkscape.com" --export-id=rect3956 -e gon_5_server_config_h.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM Server Configuration Beta

"/Program Files/Inkscape/Inkscape.com" --export-id=rect1479 -e gon_5_server_config_h_beta.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM Uninstaller

"/Program Files/Inkscape/Inkscape.com" --export-id=rect3954 -e gon_5_uninstaller_h.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM Uninstaller Beta

"/Program Files/Inkscape/Inkscape.com" --export-id=rect1529 -e gon_5_uninstaller_h_beta.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM v

"/Program Files/Inkscape/Inkscape.com" --export-id=rect3010 -e gon_5_v.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM v Beta

"/Program Files/Inkscape/Inkscape.com" --export-id=rect4176 -e gon_5_v_beta.png gon_5_left_banners.svg


REM ---------------------------------------------------------------------
REM CONVERT ALL FILES TO .bmp
REM NOTICE: Conversion to .bmp must be done manually (use Paint.net). When converted with gimp-scripting, the .bmp images doesn't work in the client!!!!!
gimp-2.8 -i -b "(excitor-save-as-bmp \"gon_5_installer_h.png\" \"gon_5_installer_h.bmp\")" -b "(excitor-save-as-bmp \"gon_5_installer_h_beta.png\" \"gon_5_installer_h_beta.bmp\")" -b "(excitor-save-as-bmp \"gon_5_server_config_h.png\" \"gon_5_server_config_h.bmp\")" -b "(excitor-save-as-bmp \"gon_5_server_config_h_beta.png\" \"gon_5_server_config_h_beta.bmp\")" -b "(excitor-save-as-bmp \"gon_5_uninstaller_h.png\" \"gon_5_uninstaller_h.bmp\")" -b "(excitor-save-as-bmp \"gon_5_uninstaller_h_beta.png\" \"gon_5_uninstaller_h_beta.bmp\")" -b "(excitor-save-as-bmp \"gon_5_v.png\" \"gon_5_v.bmp\")" -b "(excitor-save-as-bmp \"gon_5_v_beta.png\" \"gon_5_v_beta.bmp\")" -b "(gimp-quit 0)"