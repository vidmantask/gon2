REM ---------------------------------------------------------------------
REM Enroll

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect2465 -e g_enroll_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3327 -d72 -e mac/g_enroll_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Install

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect2465-5 -e g_install_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3281 -d72 -e mac/g_install_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Login

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3783 -e g_login_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3224 -d72 -e mac/g_login_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Remove

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect2465-54 -e g_remove_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3373 -d72 -e mac/g_remove_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Uninstall

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect2465-0 -e g_uninstall_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3415 -d72 -e mac/g_uninstall_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Update

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect2465-6 -e g_update_banner.png g_banners.svg
REM Mac resolution
/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3457 -d72 -e mac/g_update_banner.png g_banners.svg



REM ---------------------------------------------------------------------
REM -- Version 4 --------------------------------------------------------
REM ---------------------------------------------------------------------

REM ---------------------------------------------------------------------
REM Version 4 - Enroll

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3140 -e Version_4/g_enroll_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Version 4 - Install

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3166 -e Version_4/g_install_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Version 4 - Login

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3192 -e Version_4/g_login_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Version 4 - Remove

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3194 -e Version_4/g_remove_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Version 4 - Uninstall

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3218 -e Version_4/g_uninstall_banner.png g_banners.svg


REM ---------------------------------------------------------------------
REM Version 4 - Update

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-id=rect3242 -e Version_4/g_update_banner.png g_banners.svg




REM ---------------------------------------------------------------------
REM CONVERT ALL FILES TO .bmp
REM NOTICE: Conversion to .bmp must be done manually (use Paint.net). When converted with gimp-scripting, the .bmp images doesn't work in the client!!!!!
