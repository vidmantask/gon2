REM ---------------------------------------------------------------------
REM Android

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3074 -w512 -h512 -e Android/dme_icon_android_512x512.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3074 -w72 -h72 -e Android/dme_icon_android_72x72.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3074 -w48 -h48 -e Android/dme_icon_android_48x48.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3074 -w36 -h36 -e Android/dme_icon_android_36x36.png dme_icon.svg


REM ---------------------------------------------------------------------
REM iOS

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3018 -w1024 -h1024 -e iOS/dme_icon_ios_1024x1024.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3018 -w512 -h512 -e iOS/dme_icon_ios_512x512.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3018 -w72 -h72 -e iOS/dme_icon_ios_72x72.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3018 -w48 -h48 -e iOS/dme_icon_ios_48x48.png dme_icon.svg

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect3018 -w36 -h36 -e iOS/dme_icon_ios_36x36.png dme_icon.svg


REM ---------------------------------------------------------------------
REM Marketing

/"Program Files (x86)"/Inkscape/Inkscapec.exe --export-background-opacity=0.0 --export-id=rect19210 -w512 -h512 -e Marketing/dme_icon_ios_marketing_512x512.png dme_icon.svg


