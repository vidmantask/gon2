
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package com.giritech.server_config_ws;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SaveConfigTemplateResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SaveConfigTemplateResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareChangeRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareChangeRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeUpgradeRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeUpgradeRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GOnManagementServiceControlResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GOnManagementServiceControlResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "LoginResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.LoginResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeUpgradeResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeUpgradeResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateDemodataResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateDemodataResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareInstallationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareInstallationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_element".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_element.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "LoginRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.LoginRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetLicenseInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetLicenseInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "ConfigurationTemplateSpec".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_selection".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_selection.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareInstallationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareInstallationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetFirstConfigSpecificationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetFirstConfigSpecificationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "test_config_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Test_config_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateKnownsecretsResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateKnownsecretsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeInstallationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeInstallationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetConfigTemplateResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetConfigTemplateResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetConfigTemplateRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetConfigTemplateRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobInstallServicesResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobInstallServicesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SetLicenseRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SetLicenseRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SetLicenseResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SetLicenseResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "portscan_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Portscan_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SaveConfigTemplateRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SaveConfigTemplateRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetNextConfigSpecificationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetNextConfigSpecificationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateGPMSResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateGPMSResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeChangeRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeChangeRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetJobInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetJobInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "LogoutRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.LogoutRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetStatusRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetStatusRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "LicenseInfoType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.LicenseInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "details".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Details.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GOnManagementServiceControlRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GOnManagementServiceControlRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GOnManagementServiceGetStatusRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GOnManagementServiceGetStatusRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_job".equals(namespaceURI) &&
                  "JobInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_job.JobInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetLicenseInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetLicenseInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SaveConfigSpecificationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SaveConfigSpecificationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateKnownsecretsRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateKnownsecretsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_selection_choice".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_selection_choice.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "SaveConfigSpecificationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.SaveConfigSpecificationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "PingRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.PingRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_job".equals(namespaceURI) &&
                  "job_status_type0".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_job.Job_status_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareUpgradeRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareUpgradeRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "ReloadConfigSpecificationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.ReloadConfigSpecificationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "CancelJobResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.CancelJobResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "ErrorLocationType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.ErrorLocationType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GOnManagementServiceGetStatusResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GOnManagementServiceGetStatusResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeInstallationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeInstallationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetServicesResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetServicesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "ServiceType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.ServiceType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareUpgradeResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareUpgradeResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetGOnSystemsResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetGOnSystemsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "TestConfigSpecificationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.TestConfigSpecificationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "TestConfigSpecificationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.TestConfigSpecificationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "details_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Details_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateGPMSRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateGPMSRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "DeleteConfigTemplateRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.DeleteConfigTemplateRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "CancelJobRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.CancelJobRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "PingResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.PingResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "DateType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.DateType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateSupportPackageResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateSupportPackageResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobFinalizeChangeResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobFinalizeChangeResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetJobInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetJobInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobPrepareChangeResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobPrepareChangeResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetNextConfigSpecificationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetNextConfigSpecificationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "ConfigurationTemplate".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.ConfigurationTemplate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetFirstConfigSpecificationRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetFirstConfigSpecificationRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobInstallServicesRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobInstallServicesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetGOnSystemsRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetGOnSystemsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateSupportPackageRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateSupportPackageRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetStatusResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetStatusResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "LogoutResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.LogoutResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "StartJobGenerateDemodataRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.StartJobGenerateDemodataRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GetServicesRequestType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GetServicesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "ReloadConfigSpecificationResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.ReloadConfigSpecificationResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "DeleteConfigTemplateResponseType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.DeleteConfigTemplateResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/server_config_ws".equals(namespaceURI) &&
                  "GOnSystemType".equals(typeName)){
                   
                            return  com.giritech.server_config_ws.GOnSystemType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    