
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package com.giritech.admin_deploy_ws;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetJobInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetJobInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetGPMCollectionsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetGPMCollectionsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "InstallGPMCollectionRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.InstallGPMCollectionRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_job".equals(namespaceURI) &&
                  "job_status_type0".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_job.Job_status_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetGPMCollectionsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetGPMCollectionsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetTokensResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetTokensResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_gpm".equals(namespaceURI) &&
                  "GPMInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_gpm.GPMInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "InitTokenRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.InitTokenRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "InitTokenResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.InitTokenResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetTokenTypesRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetTokenTypesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "TokenTypeType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.TokenTypeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "InstallGPMCollectionResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.InstallGPMCollectionResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetJobInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetJobInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "DeployTokenRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.DeployTokenRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "DeployTokenResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.DeployTokenResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "LogoutResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.LogoutResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "LogoutRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.LogoutRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_gpm".equals(namespaceURI) &&
                  "GPMCollectionInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_gpm.GPMCollectionInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetRuntimeEnvInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetRuntimeEnvInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetRuntimeEnvInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetRuntimeEnvInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetTokenTypesResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetTokenTypesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "PingRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.PingRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "LoginResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.LoginResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "CancelJobResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.CancelJobResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GPMInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GPMInfoTypeE.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "GetTokensRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.GetTokensRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "LoginRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.LoginRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "PingResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.PingResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws".equals(namespaceURI) &&
                  "CancelJobRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.CancelJobRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_token".equals(namespaceURI) &&
                  "TokenInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_token.TokenInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_job".equals(namespaceURI) &&
                  "JobInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_job.JobInfoType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    