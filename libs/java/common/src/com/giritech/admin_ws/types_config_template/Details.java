
/**
 * Details.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package com.giritech.admin_ws.types_config_template;
            

            /**
            *  Details bean class
            */
        
        public  class Details
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = details
                Namespace URI = http://giritech.com/admin_ws/types_config_template
                Namespace Prefix = ns2
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://giritech.com/admin_ws/types_config_template")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for Internal_name
                        */

                        
                                    protected java.lang.String localInternal_name ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternal_nameTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternal_name(){
                               return localInternal_name;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Internal_name
                               */
                               public void setInternal_name(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localInternal_nameTracker = true;
                                       } else {
                                          localInternal_nameTracker = false;
                                              
                                       }
                                   
                                            this.localInternal_name=param;
                                    

                               }
                            

                        /**
                        * field for Config_edit
                        * This was an Array!
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] localConfig_edit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConfig_editTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[]
                           */
                           public  com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] getConfig_edit(){
                               return localConfig_edit;
                           }

                           
                        


                               
                              /**
                               * validate the array for Config_edit
                               */
                              protected void validateConfig_edit(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Config_edit
                              */
                              public void setConfig_edit(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] param){
                              
                                   validateConfig_edit(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localConfig_editTracker = true;
                                          } else {
                                             localConfig_editTracker = false;
                                                 
                                          }
                                      
                                      this.localConfig_edit=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec
                             */
                             public void addConfig_edit(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec param){
                                   if (localConfig_edit == null){
                                   localConfig_edit = new com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[]{};
                                   }

                            
                                 //update the setting tracker
                                localConfig_editTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localConfig_edit);
                               list.add(param);
                               this.localConfig_edit =
                             (com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[])list.toArray(
                            new com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[list.size()]);

                             }
                             

                        /**
                        * field for Config_new
                        * This was an Array!
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] localConfig_new ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConfig_newTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[]
                           */
                           public  com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] getConfig_new(){
                               return localConfig_new;
                           }

                           
                        


                               
                              /**
                               * validate the array for Config_new
                               */
                              protected void validateConfig_new(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Config_new
                              */
                              public void setConfig_new(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[] param){
                              
                                   validateConfig_new(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localConfig_newTracker = true;
                                          } else {
                                             localConfig_newTracker = false;
                                                 
                                          }
                                      
                                      this.localConfig_new=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec
                             */
                             public void addConfig_new(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec param){
                                   if (localConfig_new == null){
                                   localConfig_new = new com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[]{};
                                   }

                            
                                 //update the setting tracker
                                localConfig_newTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localConfig_new);
                               list.add(param);
                               this.localConfig_new =
                             (com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[])list.toArray(
                            new com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[list.size()]);

                             }
                             

                        /**
                        * field for Details_type
                        * This was an Attribute!
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.Details_type_type0 localDetails_type ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.Details_type_type0
                           */
                           public  com.giritech.admin_ws.types_config_template.Details_type_type0 getDetails_type(){
                               return localDetails_type;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Details_type
                               */
                               public void setDetails_type(com.giritech.admin_ws.types_config_template.Details_type_type0 param){
                            
                                            this.localDetails_type=param;
                                    

                               }
                            

                        /**
                        * field for Edit_enabled
                        * This was an Attribute!
                        */

                        
                                    protected boolean localEdit_enabled =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("true");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEdit_enabled(){
                               return localEdit_enabled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Edit_enabled
                               */
                               public void setEdit_enabled(boolean param){
                            
                                            this.localEdit_enabled=param;
                                    

                               }
                            

                        /**
                        * field for Delete_enabled
                        * This was an Attribute!
                        */

                        
                                    protected boolean localDelete_enabled =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("true");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDelete_enabled(){
                               return localDelete_enabled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Delete_enabled
                               */
                               public void setDelete_enabled(boolean param){
                            
                                            this.localDelete_enabled=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       Details.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://giritech.com/admin_ws/types_config_template");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":details",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "details",
                           xmlWriter);
                   }

               
                   }
               
                                    
                                    if (localDetails_type != null){
                                        writeAttribute("",
                                           "details_type",
                                           localDetails_type.toString(), xmlWriter);
                                    }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "edit_enabled",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEdit_enabled), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "delete_enabled",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDelete_enabled), xmlWriter);

                                            
                                      }
                                    
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"title", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"title");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("title");
                                    }
                                

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localInternal_nameTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"internal_name", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"internal_name");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("internal_name");
                                    }
                                

                                          if (localInternal_name==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("internal_name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localInternal_name);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localConfig_editTracker){
                                       if (localConfig_edit!=null){
                                            for (int i = 0;i < localConfig_edit.length;i++){
                                                if (localConfig_edit[i] != null){
                                                 localConfig_edit[i].serialize(new javax.xml.namespace.QName("","config_edit"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("config_edit cannot be null!!");
                                        
                                    }
                                 } if (localConfig_newTracker){
                                       if (localConfig_new!=null){
                                            for (int i = 0;i < localConfig_new.length;i++){
                                                if (localConfig_new[i] != null){
                                                 localConfig_new[i].serialize(new javax.xml.namespace.QName("","config_new"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("config_new cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                     if (localInternal_nameTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "internal_name"));
                                 
                                        if (localInternal_name != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternal_name));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("internal_name cannot be null!!");
                                        }
                                    } if (localConfig_editTracker){
                             if (localConfig_edit!=null) {
                                 for (int i = 0;i < localConfig_edit.length;i++){

                                    if (localConfig_edit[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("",
                                                                          "config_edit"));
                                         elementList.add(localConfig_edit[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("config_edit cannot be null!!");
                                    
                             }

                        } if (localConfig_newTracker){
                             if (localConfig_new!=null) {
                                 for (int i = 0;i < localConfig_new.length;i++){

                                    if (localConfig_new[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("",
                                                                          "config_new"));
                                         elementList.add(localConfig_new[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("config_new cannot be null!!");
                                    
                             }

                        }
                            attribList.add(
                            new javax.xml.namespace.QName("","details_type"));
                            
                                      attribList.add(localDetails_type.toString());
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","edit_enabled"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEdit_enabled));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","delete_enabled"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDelete_enabled));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Details parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Details object =
                new Details();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"details".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Details)com.giritech.admin_ws.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    // handle attribute "details_type"
                    java.lang.String tempAttribDetails_type =
                        
                                reader.getAttributeValue(null,"details_type");
                            
                   if (tempAttribDetails_type!=null){
                         java.lang.String content = tempAttribDetails_type;
                        
                                                  object.setDetails_type(
                                                        com.giritech.admin_ws.types_config_template.Details_type_type0.Factory.fromString(reader,tempAttribDetails_type));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("details_type");
                    
                    // handle attribute "edit_enabled"
                    java.lang.String tempAttribEdit_enabled =
                        
                                reader.getAttributeValue(null,"edit_enabled");
                            
                   if (tempAttribEdit_enabled!=null){
                         java.lang.String content = tempAttribEdit_enabled;
                        
                                                 object.setEdit_enabled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribEdit_enabled));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("edit_enabled");
                    
                    // handle attribute "delete_enabled"
                    java.lang.String tempAttribDelete_enabled =
                        
                                reader.getAttributeValue(null,"delete_enabled");
                            
                   if (tempAttribDelete_enabled!=null){
                         java.lang.String content = tempAttribDelete_enabled;
                        
                                                 object.setDelete_enabled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribDelete_enabled));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("delete_enabled");
                    
                    
                    reader.next();
                
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","title").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","internal_name").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInternal_name(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","config_edit").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("","config_edit").equals(reader.getName())){
                                                                    list3.add(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setConfig_edit((com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","config_new").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("","config_new").equals(reader.getName())){
                                                                    list4.add(com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setConfig_new((com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          