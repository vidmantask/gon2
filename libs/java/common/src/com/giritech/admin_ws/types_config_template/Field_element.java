
/**
 * Field_element.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package com.giritech.admin_ws.types_config_template;
            

            /**
            *  Field_element bean class
            */
        
        public  class Field_element
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = field_element
                Namespace URI = http://giritech.com/admin_ws/types_config_template
                Namespace Prefix = ns2
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://giritech.com/admin_ws/types_config_template")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Name
                        */

                        
                                    protected java.lang.String localName ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(java.lang.String param){
                            
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for Default_value
                        */

                        
                                    protected java.lang.String localDefault_value ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefault_valueTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDefault_value(){
                               return localDefault_value;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Default_value
                               */
                               public void setDefault_value(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDefault_valueTracker = true;
                                       } else {
                                          localDefault_valueTracker = false;
                                              
                                       }
                                   
                                            this.localDefault_value=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localTitleTracker = true;
                                       } else {
                                          localTitleTracker = false;
                                              
                                       }
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for Tooltip
                        */

                        
                                    protected java.lang.String localTooltip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTooltipTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTooltip(){
                               return localTooltip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tooltip
                               */
                               public void setTooltip(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localTooltipTracker = true;
                                       } else {
                                          localTooltipTracker = false;
                                              
                                       }
                                   
                                            this.localTooltip=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDescriptionTracker = true;
                                       } else {
                                          localDescriptionTracker = false;
                                              
                                       }
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for Section
                        */

                        
                                    protected java.lang.String localSection ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSectionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSection(){
                               return localSection;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Section
                               */
                               public void setSection(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localSectionTracker = true;
                                       } else {
                                          localSectionTracker = false;
                                              
                                       }
                                   
                                            this.localSection=param;
                                    

                               }
                            

                        /**
                        * field for Selection
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.Value_selection localSelection ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSelectionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.Value_selection
                           */
                           public  com.giritech.admin_ws.types_config_template.Value_selection getSelection(){
                               return localSelection;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Selection
                               */
                               public void setSelection(com.giritech.admin_ws.types_config_template.Value_selection param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localSelectionTracker = true;
                                       } else {
                                          localSelectionTracker = false;
                                              
                                       }
                                   
                                            this.localSelection=param;
                                    

                               }
                            

                        /**
                        * field for Action
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.Field_action localAction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActionTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.Field_action
                           */
                           public  com.giritech.admin_ws.types_config_template.Field_action getAction(){
                               return localAction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Action
                               */
                               public void setAction(com.giritech.admin_ws.types_config_template.Field_action param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localActionTracker = true;
                                       } else {
                                          localActionTracker = false;
                                              
                                       }
                                   
                                            this.localAction=param;
                                    

                               }
                            

                        /**
                        * field for Field_type
                        * This was an Attribute!
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.Field_type_type0 localField_type ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.Field_type_type0
                           */
                           public  com.giritech.admin_ws.types_config_template.Field_type_type0 getField_type(){
                               return localField_type;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Field_type
                               */
                               public void setField_type(com.giritech.admin_ws.types_config_template.Field_type_type0 param){
                            
                                            this.localField_type=param;
                                    

                               }
                            

                        /**
                        * field for Value_type
                        * This was an Attribute!
                        */

                        
                                    protected com.giritech.admin_ws.types_config_template.Value_type_type0 localValue_type ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.types_config_template.Value_type_type0
                           */
                           public  com.giritech.admin_ws.types_config_template.Value_type_type0 getValue_type(){
                               return localValue_type;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Value_type
                               */
                               public void setValue_type(com.giritech.admin_ws.types_config_template.Value_type_type0 param){
                            
                                            this.localValue_type=param;
                                    

                               }
                            

                        /**
                        * field for Advanced
                        * This was an Attribute!
                        */

                        
                                    protected boolean localAdvanced =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("false");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAdvanced(){
                               return localAdvanced;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Advanced
                               */
                               public void setAdvanced(boolean param){
                            
                                            this.localAdvanced=param;
                                    

                               }
                            

                        /**
                        * field for Max_length
                        * This was an Attribute!
                        */

                        
                                    protected java.math.BigInteger localMax_length ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getMax_length(){
                               return localMax_length;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Max_length
                               */
                               public void setMax_length(java.math.BigInteger param){
                            
                                            this.localMax_length=param;
                                    

                               }
                            

                        /**
                        * field for Secret
                        * This was an Attribute!
                        */

                        
                                    protected boolean localSecret =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("false");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSecret(){
                               return localSecret;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Secret
                               */
                               public void setSecret(boolean param){
                            
                                            this.localSecret=param;
                                    

                               }
                            

                        /**
                        * field for Read_only
                        * This was an Attribute!
                        */

                        
                                    protected boolean localRead_only =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("false");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getRead_only(){
                               return localRead_only;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Read_only
                               */
                               public void setRead_only(boolean param){
                            
                                            this.localRead_only=param;
                                    

                               }
                            

                        /**
                        * field for Mandatory
                        * This was an Attribute!
                        */

                        
                                    protected boolean localMandatory =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("false");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getMandatory(){
                               return localMandatory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Mandatory
                               */
                               public void setMandatory(boolean param){
                            
                                            this.localMandatory=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       Field_element.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://giritech.com/admin_ws/types_config_template");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":field_element",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "field_element",
                           xmlWriter);
                   }

               
                   }
               
                                    
                                    if (localField_type != null){
                                        writeAttribute("",
                                           "field_type",
                                           localField_type.toString(), xmlWriter);
                                    }
                                    
                                      else {
                                          throw new org.apache.axis2.databinding.ADBException("required attribute localField_type is null");
                                      }
                                    
                                    
                                    if (localValue_type != null){
                                        writeAttribute("",
                                           "value_type",
                                           localValue_type.toString(), xmlWriter);
                                    }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "advanced",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAdvanced), xmlWriter);

                                            
                                      }
                                    
                                            if (localMax_length != null){
                                        
                                                writeAttribute("",
                                                         "max_length",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMax_length), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "secret",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSecret), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "read_only",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRead_only), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "mandatory",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMandatory), xmlWriter);

                                            
                                      }
                                    
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"name", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"name");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("name");
                                    }
                                

                                          if (localName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localDefault_valueTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"default_value", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"default_value");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("default_value");
                                    }
                                

                                          if (localDefault_value==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("default_value cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDefault_value);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTitleTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"title", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"title");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("title");
                                    }
                                

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTooltipTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"tooltip", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"tooltip");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("tooltip");
                                    }
                                

                                          if (localTooltip==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("tooltip cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTooltip);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescriptionTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"description", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"description");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("description");
                                    }
                                

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSectionTracker){
                                    namespace = "";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"section", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"section");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("section");
                                    }
                                

                                          if (localSection==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("section cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSection);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSelectionTracker){
                                            if (localSelection==null){
                                                 throw new org.apache.axis2.databinding.ADBException("selection cannot be null!!");
                                            }
                                           localSelection.serialize(new javax.xml.namespace.QName("","selection"),
                                               factory,xmlWriter);
                                        } if (localActionTracker){
                                            if (localAction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("action cannot be null!!");
                                            }
                                           localAction.serialize(new javax.xml.namespace.QName("","action"),
                                               factory,xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "name"));
                                 
                                        if (localName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        }
                                     if (localDefault_valueTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "default_value"));
                                 
                                        if (localDefault_value != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDefault_value));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("default_value cannot be null!!");
                                        }
                                    } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localTooltipTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "tooltip"));
                                 
                                        if (localTooltip != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTooltip));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("tooltip cannot be null!!");
                                        }
                                    } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localSectionTracker){
                                      elementList.add(new javax.xml.namespace.QName("",
                                                                      "section"));
                                 
                                        if (localSection != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSection));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("section cannot be null!!");
                                        }
                                    } if (localSelectionTracker){
                            elementList.add(new javax.xml.namespace.QName("",
                                                                      "selection"));
                            
                            
                                    if (localSelection==null){
                                         throw new org.apache.axis2.databinding.ADBException("selection cannot be null!!");
                                    }
                                    elementList.add(localSelection);
                                } if (localActionTracker){
                            elementList.add(new javax.xml.namespace.QName("",
                                                                      "action"));
                            
                            
                                    if (localAction==null){
                                         throw new org.apache.axis2.databinding.ADBException("action cannot be null!!");
                                    }
                                    elementList.add(localAction);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","field_type"));
                            
                                      attribList.add(localField_type.toString());
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","value_type"));
                            
                                      attribList.add(localValue_type.toString());
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","advanced"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAdvanced));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","max_length"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMax_length));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","secret"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSecret));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","read_only"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRead_only));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","mandatory"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMandatory));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Field_element parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Field_element object =
                new Field_element();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"field_element".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Field_element)com.giritech.admin_ws.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    // handle attribute "field_type"
                    java.lang.String tempAttribField_type =
                        
                                reader.getAttributeValue(null,"field_type");
                            
                   if (tempAttribField_type!=null){
                         java.lang.String content = tempAttribField_type;
                        
                                                  object.setField_type(
                                                        com.giritech.admin_ws.types_config_template.Field_type_type0.Factory.fromString(reader,tempAttribField_type));
                                            
                    } else {
                       
                               throw new org.apache.axis2.databinding.ADBException("Required attribute field_type is missing");
                           
                    }
                    handledAttributes.add("field_type");
                    
                    // handle attribute "value_type"
                    java.lang.String tempAttribValue_type =
                        
                                reader.getAttributeValue(null,"value_type");
                            
                   if (tempAttribValue_type!=null){
                         java.lang.String content = tempAttribValue_type;
                        
                                                  object.setValue_type(
                                                        com.giritech.admin_ws.types_config_template.Value_type_type0.Factory.fromString(reader,tempAttribValue_type));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("value_type");
                    
                    // handle attribute "advanced"
                    java.lang.String tempAttribAdvanced =
                        
                                reader.getAttributeValue(null,"advanced");
                            
                   if (tempAttribAdvanced!=null){
                         java.lang.String content = tempAttribAdvanced;
                        
                                                 object.setAdvanced(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribAdvanced));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("advanced");
                    
                    // handle attribute "max_length"
                    java.lang.String tempAttribMax_length =
                        
                                reader.getAttributeValue(null,"max_length");
                            
                   if (tempAttribMax_length!=null){
                         java.lang.String content = tempAttribMax_length;
                        
                                                 object.setMax_length(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(tempAttribMax_length));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("max_length");
                    
                    // handle attribute "secret"
                    java.lang.String tempAttribSecret =
                        
                                reader.getAttributeValue(null,"secret");
                            
                   if (tempAttribSecret!=null){
                         java.lang.String content = tempAttribSecret;
                        
                                                 object.setSecret(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribSecret));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("secret");
                    
                    // handle attribute "read_only"
                    java.lang.String tempAttribRead_only =
                        
                                reader.getAttributeValue(null,"read_only");
                            
                   if (tempAttribRead_only!=null){
                         java.lang.String content = tempAttribRead_only;
                        
                                                 object.setRead_only(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribRead_only));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("read_only");
                    
                    // handle attribute "mandatory"
                    java.lang.String tempAttribMandatory =
                        
                                reader.getAttributeValue(null,"mandatory");
                            
                   if (tempAttribMandatory!=null){
                         java.lang.String content = tempAttribMandatory;
                        
                                                 object.setMandatory(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribMandatory));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("mandatory");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","name").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","default_value").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDefault_value(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","title").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","tooltip").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTooltip(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","description").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","section").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSection(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","selection").equals(reader.getName())){
                                
                                                object.setSelection(com.giritech.admin_ws.types_config_template.Value_selection.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("","action").equals(reader.getName())){
                                
                                                object.setAction(com.giritech.admin_ws.types_config_template.Field_action.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          