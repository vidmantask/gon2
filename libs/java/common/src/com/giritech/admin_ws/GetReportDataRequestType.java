
/**
 * GetReportDataRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package com.giritech.admin_ws;
            

            /**
            *  GetReportDataRequestType bean class
            */
        
        public  class GetReportDataRequestType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = GetReportDataRequestType
                Namespace URI = http://giritech.com/admin_ws
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://giritech.com/admin_ws")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Session_id
                        */

                        
                                    protected java.lang.String localSession_id ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSession_id(){
                               return localSession_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Session_id
                               */
                               public void setSession_id(java.lang.String param){
                            
                                            this.localSession_id=param;
                                    

                               }
                            

                        /**
                        * field for Module_name
                        */

                        
                                    protected java.lang.String localModule_name ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getModule_name(){
                               return localModule_name;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Module_name
                               */
                               public void setModule_name(java.lang.String param){
                            
                                            this.localModule_name=param;
                                    

                               }
                            

                        /**
                        * field for Data_id
                        */

                        
                                    protected java.lang.String localData_id ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getData_id(){
                               return localData_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Data_id
                               */
                               public void setData_id(java.lang.String param){
                            
                                            this.localData_id=param;
                                    

                               }
                            

                        /**
                        * field for Arg_int_01
                        */

                        
                                    protected java.math.BigInteger localArg_int_01 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_int_01Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getArg_int_01(){
                               return localArg_int_01;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_int_01
                               */
                               public void setArg_int_01(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_int_01Tracker = true;
                                       } else {
                                          localArg_int_01Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_int_01=param;
                                    

                               }
                            

                        /**
                        * field for Arg_int_02
                        */

                        
                                    protected java.math.BigInteger localArg_int_02 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_int_02Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getArg_int_02(){
                               return localArg_int_02;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_int_02
                               */
                               public void setArg_int_02(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_int_02Tracker = true;
                                       } else {
                                          localArg_int_02Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_int_02=param;
                                    

                               }
                            

                        /**
                        * field for Arg_int_03
                        */

                        
                                    protected java.math.BigInteger localArg_int_03 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_int_03Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getArg_int_03(){
                               return localArg_int_03;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_int_03
                               */
                               public void setArg_int_03(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_int_03Tracker = true;
                                       } else {
                                          localArg_int_03Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_int_03=param;
                                    

                               }
                            

                        /**
                        * field for Arg_int_04
                        */

                        
                                    protected java.math.BigInteger localArg_int_04 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_int_04Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getArg_int_04(){
                               return localArg_int_04;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_int_04
                               */
                               public void setArg_int_04(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_int_04Tracker = true;
                                       } else {
                                          localArg_int_04Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_int_04=param;
                                    

                               }
                            

                        /**
                        * field for Arg_int_05
                        */

                        
                                    protected java.math.BigInteger localArg_int_05 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_int_05Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigInteger
                           */
                           public  java.math.BigInteger getArg_int_05(){
                               return localArg_int_05;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_int_05
                               */
                               public void setArg_int_05(java.math.BigInteger param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_int_05Tracker = true;
                                       } else {
                                          localArg_int_05Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_int_05=param;
                                    

                               }
                            

                        /**
                        * field for Arg_string_01
                        */

                        
                                    protected java.lang.String localArg_string_01 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_string_01Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArg_string_01(){
                               return localArg_string_01;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_string_01
                               */
                               public void setArg_string_01(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_string_01Tracker = true;
                                       } else {
                                          localArg_string_01Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_string_01=param;
                                    

                               }
                            

                        /**
                        * field for Arg_string_02
                        */

                        
                                    protected java.lang.String localArg_string_02 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_string_02Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArg_string_02(){
                               return localArg_string_02;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_string_02
                               */
                               public void setArg_string_02(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_string_02Tracker = true;
                                       } else {
                                          localArg_string_02Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_string_02=param;
                                    

                               }
                            

                        /**
                        * field for Arg_string_03
                        */

                        
                                    protected java.lang.String localArg_string_03 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_string_03Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArg_string_03(){
                               return localArg_string_03;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_string_03
                               */
                               public void setArg_string_03(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_string_03Tracker = true;
                                       } else {
                                          localArg_string_03Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_string_03=param;
                                    

                               }
                            

                        /**
                        * field for Arg_string_04
                        */

                        
                                    protected java.lang.String localArg_string_04 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_string_04Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArg_string_04(){
                               return localArg_string_04;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_string_04
                               */
                               public void setArg_string_04(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_string_04Tracker = true;
                                       } else {
                                          localArg_string_04Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_string_04=param;
                                    

                               }
                            

                        /**
                        * field for Arg_string_05
                        */

                        
                                    protected java.lang.String localArg_string_05 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_string_05Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArg_string_05(){
                               return localArg_string_05;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_string_05
                               */
                               public void setArg_string_05(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localArg_string_05Tracker = true;
                                       } else {
                                          localArg_string_05Tracker = false;
                                              
                                       }
                                   
                                            this.localArg_string_05=param;
                                    

                               }
                            

                        /**
                        * field for Arg_boolean_01
                        */

                        
                                    protected boolean localArg_boolean_01 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_boolean_01Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getArg_boolean_01(){
                               return localArg_boolean_01;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_boolean_01
                               */
                               public void setArg_boolean_01(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (false) {
                                           localArg_boolean_01Tracker = false;
                                              
                                       } else {
                                          localArg_boolean_01Tracker = true;
                                       }
                                   
                                            this.localArg_boolean_01=param;
                                    

                               }
                            

                        /**
                        * field for Arg_boolean_02
                        */

                        
                                    protected boolean localArg_boolean_02 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_boolean_02Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getArg_boolean_02(){
                               return localArg_boolean_02;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_boolean_02
                               */
                               public void setArg_boolean_02(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (false) {
                                           localArg_boolean_02Tracker = false;
                                              
                                       } else {
                                          localArg_boolean_02Tracker = true;
                                       }
                                   
                                            this.localArg_boolean_02=param;
                                    

                               }
                            

                        /**
                        * field for Arg_boolean_03
                        */

                        
                                    protected boolean localArg_boolean_03 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_boolean_03Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getArg_boolean_03(){
                               return localArg_boolean_03;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_boolean_03
                               */
                               public void setArg_boolean_03(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (false) {
                                           localArg_boolean_03Tracker = false;
                                              
                                       } else {
                                          localArg_boolean_03Tracker = true;
                                       }
                                   
                                            this.localArg_boolean_03=param;
                                    

                               }
                            

                        /**
                        * field for Arg_boolean_04
                        */

                        
                                    protected boolean localArg_boolean_04 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_boolean_04Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getArg_boolean_04(){
                               return localArg_boolean_04;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_boolean_04
                               */
                               public void setArg_boolean_04(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (false) {
                                           localArg_boolean_04Tracker = false;
                                              
                                       } else {
                                          localArg_boolean_04Tracker = true;
                                       }
                                   
                                            this.localArg_boolean_04=param;
                                    

                               }
                            

                        /**
                        * field for Arg_boolean_05
                        */

                        
                                    protected boolean localArg_boolean_05 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArg_boolean_05Tracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getArg_boolean_05(){
                               return localArg_boolean_05;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Arg_boolean_05
                               */
                               public void setArg_boolean_05(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (false) {
                                           localArg_boolean_05Tracker = false;
                                              
                                       } else {
                                          localArg_boolean_05Tracker = true;
                                       }
                                   
                                            this.localArg_boolean_05=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       GetReportDataRequestType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://giritech.com/admin_ws");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":GetReportDataRequestType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "GetReportDataRequestType",
                           xmlWriter);
                   }

               
                   }
               
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"session_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"session_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("session_id");
                                    }
                                

                                          if (localSession_id==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("session_id cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSession_id);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"module_name", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"module_name");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("module_name");
                                    }
                                

                                          if (localModule_name==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("module_name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localModule_name);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"data_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"data_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("data_id");
                                    }
                                

                                          if (localData_id==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("data_id cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localData_id);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localArg_int_01Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_int_01", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_int_01");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_int_01");
                                    }
                                

                                          if (localArg_int_01==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_int_01 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_01));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_int_02Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_int_02", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_int_02");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_int_02");
                                    }
                                

                                          if (localArg_int_02==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_int_02 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_02));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_int_03Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_int_03", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_int_03");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_int_03");
                                    }
                                

                                          if (localArg_int_03==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_int_03 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_03));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_int_04Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_int_04", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_int_04");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_int_04");
                                    }
                                

                                          if (localArg_int_04==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_int_04 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_04));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_int_05Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_int_05", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_int_05");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_int_05");
                                    }
                                

                                          if (localArg_int_05==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_int_05 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_05));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_string_01Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_string_01", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_string_01");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_string_01");
                                    }
                                

                                          if (localArg_string_01==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_string_01 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArg_string_01);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_string_02Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_string_02", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_string_02");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_string_02");
                                    }
                                

                                          if (localArg_string_02==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_string_02 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArg_string_02);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_string_03Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_string_03", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_string_03");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_string_03");
                                    }
                                

                                          if (localArg_string_03==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_string_03 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArg_string_03);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_string_04Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_string_04", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_string_04");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_string_04");
                                    }
                                

                                          if (localArg_string_04==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_string_04 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArg_string_04);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_string_05Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_string_05", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_string_05");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_string_05");
                                    }
                                

                                          if (localArg_string_05==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("arg_string_05 cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArg_string_05);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_boolean_01Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_boolean_01", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_boolean_01");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_boolean_01");
                                    }
                                
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("arg_boolean_01 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_01));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_boolean_02Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_boolean_02", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_boolean_02");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_boolean_02");
                                    }
                                
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("arg_boolean_02 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_02));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_boolean_03Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_boolean_03", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_boolean_03");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_boolean_03");
                                    }
                                
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("arg_boolean_03 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_03));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_boolean_04Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_boolean_04", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_boolean_04");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_boolean_04");
                                    }
                                
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("arg_boolean_04 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_04));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArg_boolean_05Tracker){
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"arg_boolean_05", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"arg_boolean_05");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("arg_boolean_05");
                                    }
                                
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("arg_boolean_05 cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_05));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "session_id"));
                                 
                                        if (localSession_id != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSession_id));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("session_id cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "module_name"));
                                 
                                        if (localModule_name != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localModule_name));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("module_name cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "data_id"));
                                 
                                        if (localData_id != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localData_id));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("data_id cannot be null!!");
                                        }
                                     if (localArg_int_01Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_int_01"));
                                 
                                        if (localArg_int_01 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_01));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_int_01 cannot be null!!");
                                        }
                                    } if (localArg_int_02Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_int_02"));
                                 
                                        if (localArg_int_02 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_02));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_int_02 cannot be null!!");
                                        }
                                    } if (localArg_int_03Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_int_03"));
                                 
                                        if (localArg_int_03 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_03));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_int_03 cannot be null!!");
                                        }
                                    } if (localArg_int_04Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_int_04"));
                                 
                                        if (localArg_int_04 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_04));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_int_04 cannot be null!!");
                                        }
                                    } if (localArg_int_05Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_int_05"));
                                 
                                        if (localArg_int_05 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_int_05));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_int_05 cannot be null!!");
                                        }
                                    } if (localArg_string_01Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_string_01"));
                                 
                                        if (localArg_string_01 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_string_01));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_string_01 cannot be null!!");
                                        }
                                    } if (localArg_string_02Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_string_02"));
                                 
                                        if (localArg_string_02 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_string_02));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_string_02 cannot be null!!");
                                        }
                                    } if (localArg_string_03Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_string_03"));
                                 
                                        if (localArg_string_03 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_string_03));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_string_03 cannot be null!!");
                                        }
                                    } if (localArg_string_04Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_string_04"));
                                 
                                        if (localArg_string_04 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_string_04));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_string_04 cannot be null!!");
                                        }
                                    } if (localArg_string_05Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_string_05"));
                                 
                                        if (localArg_string_05 != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_string_05));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("arg_string_05 cannot be null!!");
                                        }
                                    } if (localArg_boolean_01Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_boolean_01"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_01));
                            } if (localArg_boolean_02Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_boolean_02"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_02));
                            } if (localArg_boolean_03Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_boolean_03"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_03));
                            } if (localArg_boolean_04Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_boolean_04"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_04));
                            } if (localArg_boolean_05Tracker){
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "arg_boolean_05"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localArg_boolean_05));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static GetReportDataRequestType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            GetReportDataRequestType object =
                new GetReportDataRequestType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"GetReportDataRequestType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetReportDataRequestType)com.giritech.admin_ws.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","session_id").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSession_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","module_name").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setModule_name(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","data_id").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setData_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_int_01").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_int_01(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_int_02").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_int_02(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_int_03").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_int_03(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_int_04").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_int_04(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_int_05").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_int_05(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInteger(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_string_01").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_string_01(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_string_02").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_string_02(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_string_03").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_string_03(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_string_04").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_string_04(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_string_05").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_string_05(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_boolean_01").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_boolean_01(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_boolean_02").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_boolean_02(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_boolean_03").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_boolean_03(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_boolean_04").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_boolean_04(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","arg_boolean_05").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArg_boolean_05(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          