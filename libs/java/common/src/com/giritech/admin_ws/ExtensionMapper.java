
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */

            package com.giritech.admin_ws;
            /**
            *  ExtensionMapper class
            */
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "SetLicenseRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.SetLicenseRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewayIsUserOnlineRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewayIsUserOnlineRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RespondToEnrollmentRequestRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RespondToEnrollmentRequestRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetAuthRestrictionElementsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetAuthRestrictionElementsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AccessDef".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AccessDef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceStopResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceStopResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ValuesType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ValuesType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetConfigTemplateRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetConfigTemplateRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGetServersResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGetServersResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "ConfigurationTemplateSpec".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRestrictedLaunchTypeCategoriesRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRestrictedLaunchTypeCategoriesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RemoveUserFromLaunchTypeCategoryRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RemoveUserFromLaunchTypeCategoryRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddElementToMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddElementToMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "OpenSessionRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.OpenSessionRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateRuleResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateRuleResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportDataResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportDataResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementSeachCategoriesResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementSeachCategoriesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ElementSpec".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ElementSpec.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeleteConfigElementRowResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeleteConfigElementRowResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "FaultServerOperationNotAllowedElementType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.FaultServerOperationNotAllowedElementType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "EnrollDeviceToUserRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.EnrollDeviceToUserRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceRestartRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceRestartRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "portscan_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Portscan_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "OpenSessionResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.OpenSessionResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetConfigSpecResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetConfigSpecResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "StringMap".equals(typeName)){
                   
                            return  com.giritech.admin_ws.StringMap.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGetServersRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGetServersRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "LicenseCountType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.LicenseCountType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RemoveUserFromLaunchTypeCategoryResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RemoveUserFromLaunchTypeCategoryResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "LoginRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.LoginRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetBasicEntityTypesResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetBasicEntityTypesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_selection_choice".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_selection_choice.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetBasicEntityTypesRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetBasicEntityTypesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GenerateGPMSignatureResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GenerateGPMSignatureResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGatewaySessionsNextRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGatewaySessionsNextRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CreateConfigTemplateRowResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CreateConfigTemplateRowResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesNextResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesNextResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementSeachCategoriesRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementSeachCategoriesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "item_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.Item_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "TemplateRefType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.TemplateRefType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGPMCollectionsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGPMCollectionsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "MoveItemToMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.MoveItemToMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "MoveItemToMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.MoveItemToMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportDataRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportDataRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGetKnownSecretClientResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGetKnownSecretClientResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RuleType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RuleType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ElementType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ElementType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CreateConfigTemplateRowRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CreateConfigTemplateRowRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceGetListResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceGetListResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetTokenInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetTokenInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMStartResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMStartResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "MenuType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.MenuType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewaySessionRecalculateMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewaySessionRecalculateMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "details_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Details_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportSpecResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportSpecResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CloseSessionResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CloseSessionResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetEnrollmentRequestsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetEnrollmentRequestsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMStartRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMStartRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleHeaderElementRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleHeaderElementRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnSessionType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnSessionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "MenuItemRef".equals(typeName)){
                   
                            return  com.giritech.admin_ws.MenuItemRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetMenuItemsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetMenuItemsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddMembersToFirstTimeEnrollersRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddMembersToFirstTimeEnrollersRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetAccessDefintionRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetAccessDefintionRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RuleSpecType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RuleSpecType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsNextResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsNextResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewaySessionCloseRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewaySessionCloseRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "EnrollDeviceToUserResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.EnrollDeviceToUserResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "ConfigurationTemplate".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.ConfigurationTemplate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceGetListRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceGetListRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetAccessDefintionResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetAccessDefintionResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewaySessionRecalculateMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewaySessionRecalculateMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddItemToMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddItemToMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesNextRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesNextRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddElementToMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddElementToMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateConfigTemplateRowRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateConfigTemplateRowRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetMenuItemsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetMenuItemsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RespondToEnrollmentRequestResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RespondToEnrollmentRequestResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementsStopResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementsStopResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CreateRuleRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CreateRuleRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CloseSessionRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CloseSessionRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateConfigTemplateRowResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateConfigTemplateRowResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "SetLicenseResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.SetLicenseResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetConfigSpecRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetConfigSpecRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ReportRefType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ReportRefType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateAuthRestrictionElementsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateAuthRestrictionElementsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGetKnownSecretClientRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGetKnownSecretClientRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetConfigTemplateResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetConfigTemplateResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsFirstRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsFirstRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGenerateKeyPairRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGenerateKeyPairRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_element".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_element.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "field_type_type0".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Field_type_type0.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRestrictedLaunchTypeCategoriesResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRestrictedLaunchTypeCategoriesResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementConfigTemplateResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementConfigTemplateResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ConditionType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ConditionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetLicenseInfoResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetLicenseInfoResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "CreateRuleResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.CreateRuleResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_gpm".equals(namespaceURI) &&
                  "GPMCollectionInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_gpm.GPMCollectionInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "value_selection".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Value_selection.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "test_config_action".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Test_config_action.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "EnrollDeviceRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.EnrollDeviceRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsFirstResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsFirstResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "LoginResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.LoginResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetAuthRestrictionElementsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetAuthRestrictionElementsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RemoveItemFromMenuResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RemoveItemFromMenuResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRuleSpecRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRuleSpecRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeleteRuleRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeleteRuleRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGatewaySessionsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGatewaySessionsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "LicenseInfoTypeManagement".equals(typeName)){
                   
                            return  com.giritech.admin_ws.LicenseInfoTypeManagement.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetLicenseInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetLicenseInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGatewaySessionsNextResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGatewaySessionsNextResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMStopResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMStopResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportSpecRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportSpecRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_config_template".equals(namespaceURI) &&
                  "details".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_config_template.Details.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetEntityTypeForPluginTypeResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetEntityTypeForPluginTypeResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddItemToMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddItemToMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateAuthRestrictionElementsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateAuthRestrictionElementsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetTokenInfoRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetTokenInfoRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleHeaderElementResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleHeaderElementResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetSpecificElementsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetSpecificElementsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws/types_gpm".equals(namespaceURI) &&
                  "GPMInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.types_gpm.GPMInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceStopRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceStopRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGatewaySessionsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGatewaySessionsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeployGenerateKeyPairResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeployGenerateKeyPairResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetGPMCollectionsResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetGPMCollectionsResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesFirstRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesFirstRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementConfigTemplateRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementConfigTemplateRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetElementsStopRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetElementsStopRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetReportsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetReportsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "InternalLoginResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.InternalLoginResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRuleSpecResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRuleSpecResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "ConfigElementType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.ConfigElementType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMGetChunkResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMGetChunkResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetSpecificElementsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetSpecificElementsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMGetChunkRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMGetChunkRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GOnServiceRestartResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GOnServiceRestartResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RemoveItemFromMenuRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RemoveItemFromMenuRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesForElementRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesForElementRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesForElementResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesForElementResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "StingListType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.StingListType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetModuleElementsNextRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetModuleElementsNextRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesFirstResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesFirstResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DownloadGPMStopRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DownloadGPMStopRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "PingRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.PingRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "EnrollmentRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.EnrollmentRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "MenuItemType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.MenuItemType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeleteConfigElementRowRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeleteConfigElementRowRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetEntityTypeForPluginTypeRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetEntityTypeForPluginTypeRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "RuleType1".equals(typeName)){
                   
                            return  com.giritech.admin_ws.RuleType1.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetRulesRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetRulesRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "InternalLoginRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.InternalLoginRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "PortScanResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.PortScanResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewaySessionCloseResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewaySessionCloseResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GenerateGPMSignatureRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GenerateGPMSignatureRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "PingResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.PingResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "DeleteRuleResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.DeleteRuleResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GatewayIsUserOnlineResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GatewayIsUserOnlineResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "AddMembersToFirstTimeEnrollersResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.AddMembersToFirstTimeEnrollersResponseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "UpdateRuleRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.UpdateRuleRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "PortScanRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.PortScanRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "FaultServerElementType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.FaultServerElementType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "GetEnrollmentRequestsRequestType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.GetEnrollmentRequestsRequestType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_deploy_ws/types_token".equals(namespaceURI) &&
                  "TokenInfoType".equals(typeName)){
                   
                            return  com.giritech.admin_deploy_ws.types_token.TokenInfoType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://giritech.com/admin_ws".equals(namespaceURI) &&
                  "EnrollDeviceResponseType".equals(typeName)){
                   
                            return  com.giritech.admin_ws.EnrollDeviceResponseType.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    