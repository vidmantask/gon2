
/**
 * RuleSpecType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:41 LKT)
 */
            
                package com.giritech.admin_ws;
            

            /**
            *  RuleSpecType bean class
            */
        
        public  class RuleSpecType
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = RuleSpecType
                Namespace URI = http://giritech.com/admin_ws
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://giritech.com/admin_ws")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Rule_title
                        */

                        
                                    protected java.lang.String localRule_title ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRule_title(){
                               return localRule_title;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rule_title
                               */
                               public void setRule_title(java.lang.String param){
                            
                                            this.localRule_title=param;
                                    

                               }
                            

                        /**
                        * field for Rule_title_long
                        */

                        
                                    protected java.lang.String localRule_title_long ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRule_title_long(){
                               return localRule_title_long;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Rule_title_long
                               */
                               public void setRule_title_long(java.lang.String param){
                            
                                            this.localRule_title_long=param;
                                    

                               }
                            

                        /**
                        * field for Unique_elements
                        * This was an Array!
                        */

                        
                                    protected java.lang.String[] localUnique_elements ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnique_elementsTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String[]
                           */
                           public  java.lang.String[] getUnique_elements(){
                               return localUnique_elements;
                           }

                           
                        


                               
                              /**
                               * validate the array for Unique_elements
                               */
                              protected void validateUnique_elements(java.lang.String[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Unique_elements
                              */
                              public void setUnique_elements(java.lang.String[] param){
                              
                                   validateUnique_elements(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localUnique_elementsTracker = true;
                                          } else {
                                             localUnique_elementsTracker = false;
                                                 
                                          }
                                      
                                      this.localUnique_elements=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param java.lang.String
                             */
                             public void addUnique_elements(java.lang.String param){
                                   if (localUnique_elements == null){
                                   localUnique_elements = new java.lang.String[]{};
                                   }

                            
                                 //update the setting tracker
                                localUnique_elementsTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localUnique_elements);
                               list.add(param);
                               this.localUnique_elements =
                             (java.lang.String[])list.toArray(
                            new java.lang.String[list.size()]);

                             }
                             

                        /**
                        * field for Mandatory_elements
                        * This was an Array!
                        */

                        
                                    protected com.giritech.admin_ws.StingListType[] localMandatory_elements ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMandatory_elementsTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.StingListType[]
                           */
                           public  com.giritech.admin_ws.StingListType[] getMandatory_elements(){
                               return localMandatory_elements;
                           }

                           
                        


                               
                              /**
                               * validate the array for Mandatory_elements
                               */
                              protected void validateMandatory_elements(com.giritech.admin_ws.StingListType[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Mandatory_elements
                              */
                              public void setMandatory_elements(com.giritech.admin_ws.StingListType[] param){
                              
                                   validateMandatory_elements(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localMandatory_elementsTracker = true;
                                          } else {
                                             localMandatory_elementsTracker = false;
                                                 
                                          }
                                      
                                      this.localMandatory_elements=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.giritech.admin_ws.StingListType
                             */
                             public void addMandatory_elements(com.giritech.admin_ws.StingListType param){
                                   if (localMandatory_elements == null){
                                   localMandatory_elements = new com.giritech.admin_ws.StingListType[]{};
                                   }

                            
                                 //update the setting tracker
                                localMandatory_elementsTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMandatory_elements);
                               list.add(param);
                               this.localMandatory_elements =
                             (com.giritech.admin_ws.StingListType[])list.toArray(
                            new com.giritech.admin_ws.StingListType[list.size()]);

                             }
                             

                        /**
                        * field for Header_rule
                        */

                        
                                    protected com.giritech.admin_ws.RuleType localHeader_rule ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.RuleType
                           */
                           public  com.giritech.admin_ws.RuleType getHeader_rule(){
                               return localHeader_rule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Header_rule
                               */
                               public void setHeader_rule(com.giritech.admin_ws.RuleType param){
                            
                                            this.localHeader_rule=param;
                                    

                               }
                            

                        /**
                        * field for Template_rule
                        */

                        
                                    protected com.giritech.admin_ws.RuleType localTemplate_rule ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.RuleType
                           */
                           public  com.giritech.admin_ws.RuleType getTemplate_rule(){
                               return localTemplate_rule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Template_rule
                               */
                               public void setTemplate_rule(com.giritech.admin_ws.RuleType param){
                            
                                            this.localTemplate_rule=param;
                                    

                               }
                            

                        /**
                        * field for Entity_type_maps
                        * This was an Array!
                        */

                        
                                    protected com.giritech.admin_ws.StringMap[] localEntity_type_maps ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntity_type_mapsTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return com.giritech.admin_ws.StringMap[]
                           */
                           public  com.giritech.admin_ws.StringMap[] getEntity_type_maps(){
                               return localEntity_type_maps;
                           }

                           
                        


                               
                              /**
                               * validate the array for Entity_type_maps
                               */
                              protected void validateEntity_type_maps(com.giritech.admin_ws.StringMap[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Entity_type_maps
                              */
                              public void setEntity_type_maps(com.giritech.admin_ws.StringMap[] param){
                              
                                   validateEntity_type_maps(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             localEntity_type_mapsTracker = true;
                                          } else {
                                             localEntity_type_mapsTracker = false;
                                                 
                                          }
                                      
                                      this.localEntity_type_maps=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.giritech.admin_ws.StringMap
                             */
                             public void addEntity_type_maps(com.giritech.admin_ws.StringMap param){
                                   if (localEntity_type_maps == null){
                                   localEntity_type_maps = new com.giritech.admin_ws.StringMap[]{};
                                   }

                            
                                 //update the setting tracker
                                localEntity_type_mapsTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEntity_type_maps);
                               list.add(param);
                               this.localEntity_type_maps =
                             (com.giritech.admin_ws.StringMap[])list.toArray(
                            new com.giritech.admin_ws.StringMap[list.size()]);

                             }
                             

                        /**
                        * field for Create_enabled
                        * This was an Attribute!
                        */

                        
                                    protected boolean localCreate_enabled =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("true");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getCreate_enabled(){
                               return localCreate_enabled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Create_enabled
                               */
                               public void setCreate_enabled(boolean param){
                            
                                            this.localCreate_enabled=param;
                                    

                               }
                            

                        /**
                        * field for Edit_enabled
                        * This was an Attribute!
                        */

                        
                                    protected boolean localEdit_enabled =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("true");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEdit_enabled(){
                               return localEdit_enabled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Edit_enabled
                               */
                               public void setEdit_enabled(boolean param){
                            
                                            this.localEdit_enabled=param;
                                    

                               }
                            

                        /**
                        * field for Delete_enabled
                        * This was an Attribute!
                        */

                        
                                    protected boolean localDelete_enabled =
                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean("true");
                                

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getDelete_enabled(){
                               return localDelete_enabled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Delete_enabled
                               */
                               public void setDelete_enabled(boolean param){
                            
                                            this.localDelete_enabled=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       RuleSpecType.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://giritech.com/admin_ws");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":RuleSpecType",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "RuleSpecType",
                           xmlWriter);
                   }

               
                   }
               
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "create_enabled",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreate_enabled), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "edit_enabled",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEdit_enabled), xmlWriter);

                                            
                                      }
                                    
                                                   if (true) {
                                               
                                                writeAttribute("",
                                                         "delete_enabled",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDelete_enabled), xmlWriter);

                                            
                                      }
                                    
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"rule_title", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"rule_title");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("rule_title");
                                    }
                                

                                          if (localRule_title==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rule_title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRule_title);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             
                                    namespace = "http://giritech.com/admin_ws";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"rule_title_long", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"rule_title_long");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("rule_title_long");
                                    }
                                

                                          if (localRule_title_long==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("rule_title_long cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRule_title_long);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                              if (localUnique_elementsTracker){
                             if (localUnique_elements!=null) {
                                   namespace = "http://giritech.com/admin_ws";
                                   boolean emptyNamespace = namespace == null || namespace.length() == 0;
                                   prefix =  emptyNamespace ? null : xmlWriter.getPrefix(namespace);
                                   for (int i = 0;i < localUnique_elements.length;i++){
                                        
                                            if (localUnique_elements[i] != null){
                                        
                                                if (!emptyNamespace) {
                                                    if (prefix == null) {
                                                        java.lang.String prefix2 = generatePrefix(namespace);

                                                        xmlWriter.writeStartElement(prefix2,"unique_elements", namespace);
                                                        xmlWriter.writeNamespace(prefix2, namespace);
                                                        xmlWriter.setPrefix(prefix2, namespace);

                                                    } else {
                                                        xmlWriter.writeStartElement(namespace,"unique_elements");
                                                    }

                                                } else {
                                                    xmlWriter.writeStartElement("unique_elements");
                                                }

                                            
                                                        xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnique_elements[i]));
                                                    
                                                xmlWriter.writeEndElement();
                                              
                                                } else {
                                                   
                                                           // we have to do nothing since minOccurs is zero
                                                       
                                                }

                                   }
                             } else {
                                 
                                         throw new org.apache.axis2.databinding.ADBException("unique_elements cannot be null!!");
                                    
                             }

                        } if (localMandatory_elementsTracker){
                                       if (localMandatory_elements!=null){
                                            for (int i = 0;i < localMandatory_elements.length;i++){
                                                if (localMandatory_elements[i] != null){
                                                 localMandatory_elements[i].serialize(new javax.xml.namespace.QName("http://giritech.com/admin_ws","mandatory_elements"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("mandatory_elements cannot be null!!");
                                        
                                    }
                                 }
                                            if (localHeader_rule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("header_rule cannot be null!!");
                                            }
                                           localHeader_rule.serialize(new javax.xml.namespace.QName("http://giritech.com/admin_ws","header_rule"),
                                               factory,xmlWriter);
                                        
                                            if (localTemplate_rule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("template_rule cannot be null!!");
                                            }
                                           localTemplate_rule.serialize(new javax.xml.namespace.QName("http://giritech.com/admin_ws","template_rule"),
                                               factory,xmlWriter);
                                         if (localEntity_type_mapsTracker){
                                       if (localEntity_type_maps!=null){
                                            for (int i = 0;i < localEntity_type_maps.length;i++){
                                                if (localEntity_type_maps[i] != null){
                                                 localEntity_type_maps[i].serialize(new javax.xml.namespace.QName("http://giritech.com/admin_ws","entity_type_maps"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("entity_type_maps cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "rule_title"));
                                 
                                        if (localRule_title != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRule_title));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rule_title cannot be null!!");
                                        }
                                    
                                      elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "rule_title_long"));
                                 
                                        if (localRule_title_long != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRule_title_long));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("rule_title_long cannot be null!!");
                                        }
                                     if (localUnique_elementsTracker){
                            if (localUnique_elements!=null){
                                  for (int i = 0;i < localUnique_elements.length;i++){
                                      
                                         if (localUnique_elements[i] != null){
                                          elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                              "unique_elements"));
                                          elementList.add(
                                          org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUnique_elements[i]));
                                          } else {
                                             
                                                    // have to do nothing
                                                
                                          }
                                      

                                  }
                            } else {
                              
                                    throw new org.apache.axis2.databinding.ADBException("unique_elements cannot be null!!");
                                
                            }

                        } if (localMandatory_elementsTracker){
                             if (localMandatory_elements!=null) {
                                 for (int i = 0;i < localMandatory_elements.length;i++){

                                    if (localMandatory_elements[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                          "mandatory_elements"));
                                         elementList.add(localMandatory_elements[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("mandatory_elements cannot be null!!");
                                    
                             }

                        }
                            elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "header_rule"));
                            
                            
                                    if (localHeader_rule==null){
                                         throw new org.apache.axis2.databinding.ADBException("header_rule cannot be null!!");
                                    }
                                    elementList.add(localHeader_rule);
                                
                            elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                      "template_rule"));
                            
                            
                                    if (localTemplate_rule==null){
                                         throw new org.apache.axis2.databinding.ADBException("template_rule cannot be null!!");
                                    }
                                    elementList.add(localTemplate_rule);
                                 if (localEntity_type_mapsTracker){
                             if (localEntity_type_maps!=null) {
                                 for (int i = 0;i < localEntity_type_maps.length;i++){

                                    if (localEntity_type_maps[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("http://giritech.com/admin_ws",
                                                                          "entity_type_maps"));
                                         elementList.add(localEntity_type_maps[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("entity_type_maps cannot be null!!");
                                    
                             }

                        }
                            attribList.add(
                            new javax.xml.namespace.QName("","create_enabled"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreate_enabled));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","edit_enabled"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEdit_enabled));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","delete_enabled"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDelete_enabled));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static RuleSpecType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            RuleSpecType object =
                new RuleSpecType();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"RuleSpecType".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (RuleSpecType)com.giritech.admin_ws.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    // handle attribute "create_enabled"
                    java.lang.String tempAttribCreate_enabled =
                        
                                reader.getAttributeValue(null,"create_enabled");
                            
                   if (tempAttribCreate_enabled!=null){
                         java.lang.String content = tempAttribCreate_enabled;
                        
                                                 object.setCreate_enabled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribCreate_enabled));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("create_enabled");
                    
                    // handle attribute "edit_enabled"
                    java.lang.String tempAttribEdit_enabled =
                        
                                reader.getAttributeValue(null,"edit_enabled");
                            
                   if (tempAttribEdit_enabled!=null){
                         java.lang.String content = tempAttribEdit_enabled;
                        
                                                 object.setEdit_enabled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribEdit_enabled));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("edit_enabled");
                    
                    // handle attribute "delete_enabled"
                    java.lang.String tempAttribDelete_enabled =
                        
                                reader.getAttributeValue(null,"delete_enabled");
                            
                   if (tempAttribDelete_enabled!=null){
                         java.lang.String content = tempAttribDelete_enabled;
                        
                                                 object.setDelete_enabled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(tempAttribDelete_enabled));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("delete_enabled");
                    
                    
                    reader.next();
                
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","rule_title").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRule_title(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","rule_title_long").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRule_title_long(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","unique_elements").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(reader.getElementText());
                                            
                                            //loop until we find a start element that is not part of this array
                                            boolean loopDone3 = false;
                                            while(!loopDone3){
                                                // Ensure we are at the EndElement
                                                while (!reader.isEndElement()){
                                                    reader.next();
                                                }
                                                // Step out of this element
                                                reader.next();
                                                // Step to next element event.
                                                while (!reader.isStartElement() && !reader.isEndElement())
                                                    reader.next();
                                                if (reader.isEndElement()){
                                                    //two continuous end elements means we are exiting the xml structure
                                                    loopDone3 = true;
                                                } else {
                                                    if (new javax.xml.namespace.QName("http://giritech.com/admin_ws","unique_elements").equals(reader.getName())){
                                                         list3.add(reader.getElementText());
                                                        
                                                    }else{
                                                        loopDone3 = true;
                                                    }
                                                }
                                            }
                                            // call the converter utility  to convert and set the array
                                            
                                                    object.setUnique_elements((java.lang.String[])
                                                        list3.toArray(new java.lang.String[list3.size()]));
                                                
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","mandatory_elements").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.giritech.admin_ws.StingListType.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("http://giritech.com/admin_ws","mandatory_elements").equals(reader.getName())){
                                                                    list4.add(com.giritech.admin_ws.StingListType.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMandatory_elements((com.giritech.admin_ws.StingListType[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.giritech.admin_ws.StingListType.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","header_rule").equals(reader.getName())){
                                
                                                object.setHeader_rule(com.giritech.admin_ws.RuleType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","template_rule").equals(reader.getName())){
                                
                                                object.setTemplate_rule(com.giritech.admin_ws.RuleType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://giritech.com/admin_ws","entity_type_maps").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.giritech.admin_ws.StringMap.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("http://giritech.com/admin_ws","entity_type_maps").equals(reader.getName())){
                                                                    list7.add(com.giritech.admin_ws.StringMap.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEntity_type_maps((com.giritech.admin_ws.StringMap[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.giritech.admin_ws.StringMap.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          