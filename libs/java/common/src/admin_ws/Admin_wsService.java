

/**
 * Admin_wsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package admin_ws;

    /*
     *  Admin_wsService java interface
     */

    public interface Admin_wsService {
          

        /**
          * Auto generated method signature
          * 
                    * @param gatewayIsUserOnline0
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GatewayIsUserOnlineResponse GatewayIsUserOnline(

                        admin_ws.GatewayIsUserOnline gatewayIsUserOnline0)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gatewayIsUserOnline0
            
          */
        public void startGatewayIsUserOnline(

            admin_ws.GatewayIsUserOnline gatewayIsUserOnline0,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createRule2
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.CreateRuleResponse CreateRule(

                        admin_ws.CreateRule createRule2)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createRule2
            
          */
        public void startCreateRule(

            admin_ws.CreateRule createRule2,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getModuleElementsNext4
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetModuleElementsNextResponse GetModuleElementsNext(

                        admin_ws.GetModuleElementsNext getModuleElementsNext4)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getModuleElementsNext4
            
          */
        public void startGetModuleElementsNext(

            admin_ws.GetModuleElementsNext getModuleElementsNext4,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getGatewaySessions6
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetGatewaySessionsResponse GetGatewaySessions(

                        admin_ws.GetGatewaySessions getGatewaySessions6)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getGatewaySessions6
            
          */
        public void startGetGatewaySessions(

            admin_ws.GetGatewaySessions getGatewaySessions6,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getModuleElementsFirst8
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetModuleElementsFirstResponse GetModuleElementsFirst(

                        admin_ws.GetModuleElementsFirst getModuleElementsFirst8)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getModuleElementsFirst8
            
          */
        public void startGetModuleElementsFirst(

            admin_ws.GetModuleElementsFirst getModuleElementsFirst8,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getEntityTypeForPluginType10
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetEntityTypeForPluginTypeResponse GetEntityTypeForPluginType(

                        admin_ws.GetEntityTypeForPluginType getEntityTypeForPluginType10)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getEntityTypeForPluginType10
            
          */
        public void startGetEntityTypeForPluginType(

            admin_ws.GetEntityTypeForPluginType getEntityTypeForPluginType10,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getGPMCollections12
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetGPMCollectionsResponse GetGPMCollections(

                        admin_ws.GetGPMCollections getGPMCollections12)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getGPMCollections12
            
          */
        public void startGetGPMCollections(

            admin_ws.GetGPMCollections getGPMCollections12,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get data for a report
                    * @param getReportData14
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetReportDataResponse GetReportData(

                        admin_ws.GetReportData getReportData14)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get data for a report
                * @param getReportData14
            
          */
        public void startGetReportData(

            admin_ws.GetReportData getReportData14,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTokenInfo16
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetTokenInfoResponse GetTokenInfo(

                        admin_ws.GetTokenInfo getTokenInfo16)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTokenInfo16
            
          */
        public void startGetTokenInfo(

            admin_ws.GetTokenInfo getTokenInfo16,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param generateGPMSignature18
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GenerateGPMSignatureResponse GenerateGPMSignature(

                        admin_ws.GenerateGPMSignature generateGPMSignature18)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param generateGPMSignature18
            
          */
        public void startGenerateGPMSignature(

            admin_ws.GenerateGPMSignature generateGPMSignature18,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param enrollDeviceToUser20
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.EnrollDeviceToUserResponse EnrollDeviceToUser(

                        admin_ws.EnrollDeviceToUser enrollDeviceToUser20)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param enrollDeviceToUser20
            
          */
        public void startEnrollDeviceToUser(

            admin_ws.EnrollDeviceToUser enrollDeviceToUser20,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addItemToMenu22
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.AddItemToMenuResponse AddItemToMenu(

                        admin_ws.AddItemToMenu addItemToMenu22)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addItemToMenu22
            
          */
        public void startAddItemToMenu(

            admin_ws.AddItemToMenu addItemToMenu22,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param openSession24
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.OpenSessionResponse OpenSession(

                        admin_ws.OpenSession openSession24)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param openSession24
            
          */
        public void startOpenSession(

            admin_ws.OpenSession openSession24,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param setLicense26
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.SetLicenseResponse SetLicense(

                        admin_ws.SetLicense setLicense26)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param setLicense26
            
          */
        public void startSetLicense(

            admin_ws.SetLicense setLicense26,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getElementsStop28
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetElementsStopResponse GetElementsStop(

                        admin_ws.GetElementsStop getElementsStop28)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getElementsStop28
            
          */
        public void startGetElementsStop(

            admin_ws.GetElementsStop getElementsStop28,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRulesNext30
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRulesNextResponse GetRulesNext(

                        admin_ws.GetRulesNext getRulesNext30)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRulesNext30
            
          */
        public void startGetRulesNext(

            admin_ws.GetRulesNext getRulesNext30,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get the list of available reports
                    * @param getReports32
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetReportsResponse GetReports(

                        admin_ws.GetReports getReports32)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get the list of available reports
                * @param getReports32
            
          */
        public void startGetReports(

            admin_ws.GetReports getReports32,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gOnServiceRestart34
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GOnServiceRestartResponse GOnServiceRestart(

                        admin_ws.GOnServiceRestart gOnServiceRestart34)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gOnServiceRestart34
            
          */
        public void startGOnServiceRestart(

            admin_ws.GOnServiceRestart gOnServiceRestart34,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param respondToEnrollmentRequest36
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.RespondToEnrollmentRequestResponse RespondToEnrollmentRequest(

                        admin_ws.RespondToEnrollmentRequest respondToEnrollmentRequest36)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param respondToEnrollmentRequest36
            
          */
        public void startRespondToEnrollmentRequest(

            admin_ws.RespondToEnrollmentRequest respondToEnrollmentRequest36,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deployGenerateKeyPair38
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DeployGenerateKeyPairResponse DeployGenerateKeyPair(

                        admin_ws.DeployGenerateKeyPair deployGenerateKeyPair38)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deployGenerateKeyPair38
            
          */
        public void startDeployGenerateKeyPair(

            admin_ws.DeployGenerateKeyPair deployGenerateKeyPair38,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gatewaySessionClose40
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GatewaySessionCloseResponse GatewaySessionClose(

                        admin_ws.GatewaySessionClose gatewaySessionClose40)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gatewaySessionClose40
            
          */
        public void startGatewaySessionClose(

            admin_ws.GatewaySessionClose gatewaySessionClose40,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gOnServiceStop42
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GOnServiceStopResponse GOnServiceStop(

                        admin_ws.GOnServiceStop gOnServiceStop42)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gOnServiceStop42
            
          */
        public void startGOnServiceStop(

            admin_ws.GOnServiceStop gOnServiceStop42,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getAccessDefintion44
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetAccessDefintionResponse GetAccessDefintion(

                        admin_ws.GetAccessDefintion getAccessDefintion44)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getAccessDefintion44
            
          */
        public void startGetAccessDefintion(

            admin_ws.GetAccessDefintion getAccessDefintion44,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param closeSession46
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.CloseSessionResponse CloseSession(

                        admin_ws.CloseSession closeSession46)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param closeSession46
            
          */
        public void startCloseSession(

            admin_ws.CloseSession closeSession46,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deleteRule48
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.DeleteRuleResponse DeleteRule(

                        admin_ws.DeleteRule deleteRule48)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deleteRule48
            
          */
        public void startDeleteRule(

            admin_ws.DeleteRule deleteRule48,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRulesFirst50
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRulesFirstResponse GetRulesFirst(

                        admin_ws.GetRulesFirst getRulesFirst50)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRulesFirst50
            
          */
        public void startGetRulesFirst(

            admin_ws.GetRulesFirst getRulesFirst50,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deleteConfigElementRow52
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.DeleteConfigElementRowResponse DeleteConfigElementRow(

                        admin_ws.DeleteConfigElementRow deleteConfigElementRow52)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deleteConfigElementRow52
            
          */
        public void startDeleteConfigElementRow(

            admin_ws.DeleteConfigElementRow deleteConfigElementRow52,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param moveItemToMenu54
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.MoveItemToMenuResponse MoveItemToMenu(

                        admin_ws.MoveItemToMenu moveItemToMenu54)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param moveItemToMenu54
            
          */
        public void startMoveItemToMenu(

            admin_ws.MoveItemToMenu moveItemToMenu54,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getModuleHeaderElement56
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetModuleHeaderElementResponse GetModuleHeaderElement(

                        admin_ws.GetModuleHeaderElement getModuleHeaderElement56)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getModuleHeaderElement56
            
          */
        public void startGetModuleHeaderElement(

            admin_ws.GetModuleHeaderElement getModuleHeaderElement56,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param updateAuthRestrictionElements58
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.UpdateAuthRestrictionElementsResponse UpdateAuthRestrictionElements(

                        admin_ws.UpdateAuthRestrictionElements updateAuthRestrictionElements58)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param updateAuthRestrictionElements58
            
          */
        public void startUpdateAuthRestrictionElements(

            admin_ws.UpdateAuthRestrictionElements updateAuthRestrictionElements58,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gatewaySessionRecalculateMenu60
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GatewaySessionRecalculateMenuResponse GatewaySessionRecalculateMenu(

                        admin_ws.GatewaySessionRecalculateMenu gatewaySessionRecalculateMenu60)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gatewaySessionRecalculateMenu60
            
          */
        public void startGatewaySessionRecalculateMenu(

            admin_ws.GatewaySessionRecalculateMenu gatewaySessionRecalculateMenu60,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addElementToMenu62
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.AddElementToMenuResponse AddElementToMenu(

                        admin_ws.AddElementToMenu addElementToMenu62)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addElementToMenu62
            
          */
        public void startAddElementToMenu(

            admin_ws.AddElementToMenu addElementToMenu62,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getEnrollmentRequests64
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.GetEnrollmentRequestsResponse GetEnrollmentRequests(

                        admin_ws.GetEnrollmentRequests getEnrollmentRequests64)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getEnrollmentRequests64
            
          */
        public void startGetEnrollmentRequests(

            admin_ws.GetEnrollmentRequests getEnrollmentRequests64,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param getRestrictedLaunchTypeCategories66
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRestrictedLaunchTypeCategoriesResponse GetRestrictedLaunchTypeCategories(

                        admin_ws.GetRestrictedLaunchTypeCategories getRestrictedLaunchTypeCategories66)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param getRestrictedLaunchTypeCategories66
            
          */
        public void startGetRestrictedLaunchTypeCategories(

            admin_ws.GetRestrictedLaunchTypeCategories getRestrictedLaunchTypeCategories66,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addMembersToFirstTimeEnrollers68
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.AddMembersToFirstTimeEnrollersResponse AddMembersToFirstTimeEnrollers(

                        admin_ws.AddMembersToFirstTimeEnrollers addMembersToFirstTimeEnrollers68)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addMembersToFirstTimeEnrollers68
            
          */
        public void startAddMembersToFirstTimeEnrollers(

            admin_ws.AddMembersToFirstTimeEnrollers addMembersToFirstTimeEnrollers68,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param enrollDevice70
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.EnrollDeviceResponse EnrollDevice(

                        admin_ws.EnrollDevice enrollDevice70)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param enrollDevice70
            
          */
        public void startEnrollDevice(

            admin_ws.EnrollDevice enrollDevice70,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get report specification
                    * @param getReportSpec72
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetReportSpecResponse GetReportSpec(

                        admin_ws.GetReportSpec getReportSpec72)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get report specification
                * @param getReportSpec72
            
          */
        public void startGetReportSpec(

            admin_ws.GetReportSpec getReportSpec72,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param getAuthRestrictionElements74
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetAuthRestrictionElementsResponse GetAuthRestrictionElements(

                        admin_ws.GetAuthRestrictionElements getAuthRestrictionElements74)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param getAuthRestrictionElements74
            
          */
        public void startGetAuthRestrictionElements(

            admin_ws.GetAuthRestrictionElements getAuthRestrictionElements74,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deployGetKnownSecretClient76
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DeployGetKnownSecretClientResponse DeployGetKnownSecretClient(

                        admin_ws.DeployGetKnownSecretClient deployGetKnownSecretClient76)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deployGetKnownSecretClient76
            
          */
        public void startDeployGetKnownSecretClient(

            admin_ws.DeployGetKnownSecretClient deployGetKnownSecretClient76,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param getSpecificElements78
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetSpecificElementsResponse GetSpecificElements(

                        admin_ws.GetSpecificElements getSpecificElements78)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param getSpecificElements78
            
          */
        public void startGetSpecificElements(

            admin_ws.GetSpecificElements getSpecificElements78,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRuleSpec80
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRuleSpecResponse GetRuleSpec(

                        admin_ws.GetRuleSpec getRuleSpec80)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRuleSpec80
            
          */
        public void startGetRuleSpec(

            admin_ws.GetRuleSpec getRuleSpec80,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param updateConfigTemplateRow82
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.UpdateConfigTemplateRowResponse UpdateConfigTemplateRow(

                        admin_ws.UpdateConfigTemplateRow updateConfigTemplateRow82)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param updateConfigTemplateRow82
            
          */
        public void startUpdateConfigTemplateRow(

            admin_ws.UpdateConfigTemplateRow updateConfigTemplateRow82,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRules84
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRulesResponse GetRules(

                        admin_ws.GetRules getRules84)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRules84
            
          */
        public void startGetRules(

            admin_ws.GetRules getRules84,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getLicenseInfo86
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetLicenseInfoResponse GetLicenseInfo(

                        admin_ws.GetLicenseInfo getLicenseInfo86)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getLicenseInfo86
            
          */
        public void startGetLicenseInfo(

            admin_ws.GetLicenseInfo getLicenseInfo86,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gOnServiceGetList88
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GOnServiceGetListResponse GOnServiceGetList(

                        admin_ws.GOnServiceGetList gOnServiceGetList88)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gOnServiceGetList88
            
          */
        public void startGOnServiceGetList(

            admin_ws.GOnServiceGetList gOnServiceGetList88,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param downloadGPMStart90
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DownloadGPMStartResponse DownloadGPMStart(

                        admin_ws.DownloadGPMStart downloadGPMStart90)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param downloadGPMStart90
            
          */
        public void startDownloadGPMStart(

            admin_ws.DownloadGPMStart downloadGPMStart90,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deployGetServers92
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DeployGetServersResponse DeployGetServers(

                        admin_ws.DeployGetServers deployGetServers92)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deployGetServers92
            
          */
        public void startDeployGetServers(

            admin_ws.DeployGetServers deployGetServers92,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param getModuleElements94
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetModuleElementsResponse GetModuleElements(

                        admin_ws.GetModuleElements getModuleElements94)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param getModuleElements94
            
          */
        public void startGetModuleElements(

            admin_ws.GetModuleElements getModuleElements94,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getGatewaySessionsNext96
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetGatewaySessionsNextResponse GetGatewaySessionsNext(

                        admin_ws.GetGatewaySessionsNext getGatewaySessionsNext96)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getGatewaySessionsNext96
            
          */
        public void startGetGatewaySessionsNext(

            admin_ws.GetGatewaySessionsNext getGatewaySessionsNext96,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getMenuItems98
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetMenuItemsResponse GetMenuItems(

                        admin_ws.GetMenuItems getMenuItems98)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getMenuItems98
            
          */
        public void startGetMenuItems(

            admin_ws.GetMenuItems getMenuItems98,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param downloadGPMStop100
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DownloadGPMStopResponse DownloadGPMStop(

                        admin_ws.DownloadGPMStop downloadGPMStop100)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param downloadGPMStop100
            
          */
        public void startDownloadGPMStop(

            admin_ws.DownloadGPMStop downloadGPMStop100,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param downloadGPMGetChunk102
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.DownloadGPMGetChunkResponse DownloadGPMGetChunk(

                        admin_ws.DownloadGPMGetChunk downloadGPMGetChunk102)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param downloadGPMGetChunk102
            
          */
        public void startDownloadGPMGetChunk(

            admin_ws.DownloadGPMGetChunk downloadGPMGetChunk102,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param login104
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.LoginResponse Login(

                        admin_ws.Login login104)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param login104
            
          */
        public void startLogin(

            admin_ws.Login login104,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param ping106
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.PingResponse Ping(

                        admin_ws.Ping ping106)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param ping106
            
          */
        public void startPing(

            admin_ws.Ping ping106,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getConfigTemplate108
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetConfigTemplateResponse GetConfigTemplate(

                        admin_ws.GetConfigTemplate getConfigTemplate108)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getConfigTemplate108
            
          */
        public void startGetConfigTemplate(

            admin_ws.GetConfigTemplate getConfigTemplate108,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getBasicEntityTypes110
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetBasicEntityTypesResponse GetBasicEntityTypes(

                        admin_ws.GetBasicEntityTypes getBasicEntityTypes110)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getBasicEntityTypes110
            
          */
        public void startGetBasicEntityTypes(

            admin_ws.GetBasicEntityTypes getBasicEntityTypes110,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param removeItemFromMenu112
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.RemoveItemFromMenuResponse RemoveItemFromMenu(

                        admin_ws.RemoveItemFromMenu removeItemFromMenu112)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param removeItemFromMenu112
            
          */
        public void startRemoveItemFromMenu(

            admin_ws.RemoveItemFromMenu removeItemFromMenu112,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param internalLogin114
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.InternalLoginResponse InternalLogin(

                        admin_ws.InternalLogin internalLogin114)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param internalLogin114
            
          */
        public void startInternalLogin(

            admin_ws.InternalLogin internalLogin114,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getConfigSpec116
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetConfigSpecResponse GetConfigSpec(

                        admin_ws.GetConfigSpec getConfigSpec116)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getConfigSpec116
            
          */
        public void startGetConfigSpec(

            admin_ws.GetConfigSpec getConfigSpec116,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRulesForElement118
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetRulesForElementResponse GetRulesForElement(

                        admin_ws.GetRulesForElement getRulesForElement118)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRulesForElement118
            
          */
        public void startGetRulesForElement(

            admin_ws.GetRulesForElement getRulesForElement118,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param portScan120
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.PortScanResponse PortScan(

                        admin_ws.PortScan portScan120)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param portScan120
            
          */
        public void startPortScan(

            admin_ws.PortScan portScan120,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getElementConfigTemplate122
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetElementConfigTemplateResponse GetElementConfigTemplate(

                        admin_ws.GetElementConfigTemplate getElementConfigTemplate122)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getElementConfigTemplate122
            
          */
        public void startGetElementConfigTemplate(

            admin_ws.GetElementConfigTemplate getElementConfigTemplate122,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * Get all module elements
                    * @param removeUserFromLaunchTypeCategory124
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.RemoveUserFromLaunchTypeCategoryResponse RemoveUserFromLaunchTypeCategory(

                        admin_ws.RemoveUserFromLaunchTypeCategory removeUserFromLaunchTypeCategory124)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * Get all module elements
                * @param removeUserFromLaunchTypeCategory124
            
          */
        public void startRemoveUserFromLaunchTypeCategory(

            admin_ws.RemoveUserFromLaunchTypeCategory removeUserFromLaunchTypeCategory124,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param updateRule126
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.UpdateRuleResponse UpdateRule(

                        admin_ws.UpdateRule updateRule126)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param updateRule126
            
          */
        public void startUpdateRule(

            admin_ws.UpdateRule updateRule126,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getMenu128
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetMenuResponse GetMenu(

                        admin_ws.GetMenu getMenu128)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getMenu128
            
          */
        public void startGetMenu(

            admin_ws.GetMenu getMenu128,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getElementSeachCategories130
                
             * @throws admin_ws.FaultServer : 
         */

         
                     public admin_ws.GetElementSeachCategoriesResponse GetElementSeachCategories(

                        admin_ws.GetElementSeachCategories getElementSeachCategories130)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getElementSeachCategories130
            
          */
        public void startGetElementSeachCategories(

            admin_ws.GetElementSeachCategories getElementSeachCategories130,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createConfigTemplateRow132
                
             * @throws admin_ws.FaultServer : 
             * @throws admin_ws.FaultServerOperationNotAllowed : 
         */

         
                     public admin_ws.CreateConfigTemplateRowResponse CreateConfigTemplateRow(

                        admin_ws.CreateConfigTemplateRow createConfigTemplateRow132)
                        throws java.rmi.RemoteException
             
          ,admin_ws.FaultServer
          ,admin_ws.FaultServerOperationNotAllowed;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createConfigTemplateRow132
            
          */
        public void startCreateConfigTemplateRow(

            admin_ws.CreateConfigTemplateRow createConfigTemplateRow132,

            final admin_ws.Admin_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    