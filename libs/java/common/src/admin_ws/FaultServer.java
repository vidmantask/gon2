
/**
 * FaultServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package admin_ws;

public class FaultServer extends java.lang.Exception{
    
    private com.giritech.admin_ws.FaultServerElement faultMessage;
    
    public FaultServer() {
        super("FaultServer");
    }
           
    public FaultServer(java.lang.String s) {
       super(s);
    }
    
    public FaultServer(java.lang.String s, java.lang.Throwable ex) {
      super(s, ex);
    }
    
    public void setFaultMessage(com.giritech.admin_ws.FaultServerElement msg){
       faultMessage = msg;
    }
    
    public com.giritech.admin_ws.FaultServerElement getFaultMessage(){
       return faultMessage;
    }
}
    