
/**
 * FaultServerOperationNotAllowed.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package admin_ws;

public class FaultServerOperationNotAllowed extends java.lang.Exception{
    
    private com.giritech.admin_ws.FaultServerOperationNotAllowedElement faultMessage;
    
    public FaultServerOperationNotAllowed() {
        super("FaultServerOperationNotAllowed");
    }
           
    public FaultServerOperationNotAllowed(java.lang.String s) {
       super(s);
    }
    
    public FaultServerOperationNotAllowed(java.lang.String s, java.lang.Throwable ex) {
      super(s, ex);
    }
    
    public void setFaultMessage(com.giritech.admin_ws.FaultServerOperationNotAllowedElement msg){
       faultMessage = msg;
    }
    
    public com.giritech.admin_ws.FaultServerOperationNotAllowedElement getFaultMessage(){
       return faultMessage;
    }
}
    