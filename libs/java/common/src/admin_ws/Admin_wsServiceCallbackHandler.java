
/**
 * Admin_wsServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package admin_ws;

    /**
     *  Admin_wsServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class Admin_wsServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public Admin_wsServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public Admin_wsServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for GatewayIsUserOnline method
            * override this method for handling normal response from GatewayIsUserOnline operation
            */
           public void receiveResultGatewayIsUserOnline(
                    admin_ws.GatewayIsUserOnlineResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GatewayIsUserOnline operation
           */
            public void receiveErrorGatewayIsUserOnline(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for CreateRule method
            * override this method for handling normal response from CreateRule operation
            */
           public void receiveResultCreateRule(
                    admin_ws.CreateRuleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from CreateRule operation
           */
            public void receiveErrorCreateRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetModuleElementsNext method
            * override this method for handling normal response from GetModuleElementsNext operation
            */
           public void receiveResultGetModuleElementsNext(
                    admin_ws.GetModuleElementsNextResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetModuleElementsNext operation
           */
            public void receiveErrorGetModuleElementsNext(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetGatewaySessions method
            * override this method for handling normal response from GetGatewaySessions operation
            */
           public void receiveResultGetGatewaySessions(
                    admin_ws.GetGatewaySessionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetGatewaySessions operation
           */
            public void receiveErrorGetGatewaySessions(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetModuleElementsFirst method
            * override this method for handling normal response from GetModuleElementsFirst operation
            */
           public void receiveResultGetModuleElementsFirst(
                    admin_ws.GetModuleElementsFirstResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetModuleElementsFirst operation
           */
            public void receiveErrorGetModuleElementsFirst(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetEntityTypeForPluginType method
            * override this method for handling normal response from GetEntityTypeForPluginType operation
            */
           public void receiveResultGetEntityTypeForPluginType(
                    admin_ws.GetEntityTypeForPluginTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetEntityTypeForPluginType operation
           */
            public void receiveErrorGetEntityTypeForPluginType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetGPMCollections method
            * override this method for handling normal response from GetGPMCollections operation
            */
           public void receiveResultGetGPMCollections(
                    admin_ws.GetGPMCollectionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetGPMCollections operation
           */
            public void receiveErrorGetGPMCollections(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetReportData method
            * override this method for handling normal response from GetReportData operation
            */
           public void receiveResultGetReportData(
                    admin_ws.GetReportDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetReportData operation
           */
            public void receiveErrorGetReportData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetTokenInfo method
            * override this method for handling normal response from GetTokenInfo operation
            */
           public void receiveResultGetTokenInfo(
                    admin_ws.GetTokenInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetTokenInfo operation
           */
            public void receiveErrorGetTokenInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GenerateGPMSignature method
            * override this method for handling normal response from GenerateGPMSignature operation
            */
           public void receiveResultGenerateGPMSignature(
                    admin_ws.GenerateGPMSignatureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GenerateGPMSignature operation
           */
            public void receiveErrorGenerateGPMSignature(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for EnrollDeviceToUser method
            * override this method for handling normal response from EnrollDeviceToUser operation
            */
           public void receiveResultEnrollDeviceToUser(
                    admin_ws.EnrollDeviceToUserResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from EnrollDeviceToUser operation
           */
            public void receiveErrorEnrollDeviceToUser(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for AddItemToMenu method
            * override this method for handling normal response from AddItemToMenu operation
            */
           public void receiveResultAddItemToMenu(
                    admin_ws.AddItemToMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from AddItemToMenu operation
           */
            public void receiveErrorAddItemToMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for OpenSession method
            * override this method for handling normal response from OpenSession operation
            */
           public void receiveResultOpenSession(
                    admin_ws.OpenSessionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from OpenSession operation
           */
            public void receiveErrorOpenSession(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for SetLicense method
            * override this method for handling normal response from SetLicense operation
            */
           public void receiveResultSetLicense(
                    admin_ws.SetLicenseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from SetLicense operation
           */
            public void receiveErrorSetLicense(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetElementsStop method
            * override this method for handling normal response from GetElementsStop operation
            */
           public void receiveResultGetElementsStop(
                    admin_ws.GetElementsStopResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetElementsStop operation
           */
            public void receiveErrorGetElementsStop(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRulesNext method
            * override this method for handling normal response from GetRulesNext operation
            */
           public void receiveResultGetRulesNext(
                    admin_ws.GetRulesNextResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRulesNext operation
           */
            public void receiveErrorGetRulesNext(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetReports method
            * override this method for handling normal response from GetReports operation
            */
           public void receiveResultGetReports(
                    admin_ws.GetReportsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetReports operation
           */
            public void receiveErrorGetReports(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GOnServiceRestart method
            * override this method for handling normal response from GOnServiceRestart operation
            */
           public void receiveResultGOnServiceRestart(
                    admin_ws.GOnServiceRestartResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GOnServiceRestart operation
           */
            public void receiveErrorGOnServiceRestart(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for RespondToEnrollmentRequest method
            * override this method for handling normal response from RespondToEnrollmentRequest operation
            */
           public void receiveResultRespondToEnrollmentRequest(
                    admin_ws.RespondToEnrollmentRequestResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from RespondToEnrollmentRequest operation
           */
            public void receiveErrorRespondToEnrollmentRequest(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeployGenerateKeyPair method
            * override this method for handling normal response from DeployGenerateKeyPair operation
            */
           public void receiveResultDeployGenerateKeyPair(
                    admin_ws.DeployGenerateKeyPairResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeployGenerateKeyPair operation
           */
            public void receiveErrorDeployGenerateKeyPair(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GatewaySessionClose method
            * override this method for handling normal response from GatewaySessionClose operation
            */
           public void receiveResultGatewaySessionClose(
                    admin_ws.GatewaySessionCloseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GatewaySessionClose operation
           */
            public void receiveErrorGatewaySessionClose(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GOnServiceStop method
            * override this method for handling normal response from GOnServiceStop operation
            */
           public void receiveResultGOnServiceStop(
                    admin_ws.GOnServiceStopResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GOnServiceStop operation
           */
            public void receiveErrorGOnServiceStop(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetAccessDefintion method
            * override this method for handling normal response from GetAccessDefintion operation
            */
           public void receiveResultGetAccessDefintion(
                    admin_ws.GetAccessDefintionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetAccessDefintion operation
           */
            public void receiveErrorGetAccessDefintion(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for CloseSession method
            * override this method for handling normal response from CloseSession operation
            */
           public void receiveResultCloseSession(
                    admin_ws.CloseSessionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from CloseSession operation
           */
            public void receiveErrorCloseSession(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeleteRule method
            * override this method for handling normal response from DeleteRule operation
            */
           public void receiveResultDeleteRule(
                    admin_ws.DeleteRuleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeleteRule operation
           */
            public void receiveErrorDeleteRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRulesFirst method
            * override this method for handling normal response from GetRulesFirst operation
            */
           public void receiveResultGetRulesFirst(
                    admin_ws.GetRulesFirstResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRulesFirst operation
           */
            public void receiveErrorGetRulesFirst(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeleteConfigElementRow method
            * override this method for handling normal response from DeleteConfigElementRow operation
            */
           public void receiveResultDeleteConfigElementRow(
                    admin_ws.DeleteConfigElementRowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeleteConfigElementRow operation
           */
            public void receiveErrorDeleteConfigElementRow(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for MoveItemToMenu method
            * override this method for handling normal response from MoveItemToMenu operation
            */
           public void receiveResultMoveItemToMenu(
                    admin_ws.MoveItemToMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from MoveItemToMenu operation
           */
            public void receiveErrorMoveItemToMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetModuleHeaderElement method
            * override this method for handling normal response from GetModuleHeaderElement operation
            */
           public void receiveResultGetModuleHeaderElement(
                    admin_ws.GetModuleHeaderElementResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetModuleHeaderElement operation
           */
            public void receiveErrorGetModuleHeaderElement(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for UpdateAuthRestrictionElements method
            * override this method for handling normal response from UpdateAuthRestrictionElements operation
            */
           public void receiveResultUpdateAuthRestrictionElements(
                    admin_ws.UpdateAuthRestrictionElementsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from UpdateAuthRestrictionElements operation
           */
            public void receiveErrorUpdateAuthRestrictionElements(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GatewaySessionRecalculateMenu method
            * override this method for handling normal response from GatewaySessionRecalculateMenu operation
            */
           public void receiveResultGatewaySessionRecalculateMenu(
                    admin_ws.GatewaySessionRecalculateMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GatewaySessionRecalculateMenu operation
           */
            public void receiveErrorGatewaySessionRecalculateMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for AddElementToMenu method
            * override this method for handling normal response from AddElementToMenu operation
            */
           public void receiveResultAddElementToMenu(
                    admin_ws.AddElementToMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from AddElementToMenu operation
           */
            public void receiveErrorAddElementToMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetEnrollmentRequests method
            * override this method for handling normal response from GetEnrollmentRequests operation
            */
           public void receiveResultGetEnrollmentRequests(
                    admin_ws.GetEnrollmentRequestsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetEnrollmentRequests operation
           */
            public void receiveErrorGetEnrollmentRequests(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRestrictedLaunchTypeCategories method
            * override this method for handling normal response from GetRestrictedLaunchTypeCategories operation
            */
           public void receiveResultGetRestrictedLaunchTypeCategories(
                    admin_ws.GetRestrictedLaunchTypeCategoriesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRestrictedLaunchTypeCategories operation
           */
            public void receiveErrorGetRestrictedLaunchTypeCategories(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for AddMembersToFirstTimeEnrollers method
            * override this method for handling normal response from AddMembersToFirstTimeEnrollers operation
            */
           public void receiveResultAddMembersToFirstTimeEnrollers(
                    admin_ws.AddMembersToFirstTimeEnrollersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from AddMembersToFirstTimeEnrollers operation
           */
            public void receiveErrorAddMembersToFirstTimeEnrollers(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for EnrollDevice method
            * override this method for handling normal response from EnrollDevice operation
            */
           public void receiveResultEnrollDevice(
                    admin_ws.EnrollDeviceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from EnrollDevice operation
           */
            public void receiveErrorEnrollDevice(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetReportSpec method
            * override this method for handling normal response from GetReportSpec operation
            */
           public void receiveResultGetReportSpec(
                    admin_ws.GetReportSpecResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetReportSpec operation
           */
            public void receiveErrorGetReportSpec(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetAuthRestrictionElements method
            * override this method for handling normal response from GetAuthRestrictionElements operation
            */
           public void receiveResultGetAuthRestrictionElements(
                    admin_ws.GetAuthRestrictionElementsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetAuthRestrictionElements operation
           */
            public void receiveErrorGetAuthRestrictionElements(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeployGetKnownSecretClient method
            * override this method for handling normal response from DeployGetKnownSecretClient operation
            */
           public void receiveResultDeployGetKnownSecretClient(
                    admin_ws.DeployGetKnownSecretClientResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeployGetKnownSecretClient operation
           */
            public void receiveErrorDeployGetKnownSecretClient(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetSpecificElements method
            * override this method for handling normal response from GetSpecificElements operation
            */
           public void receiveResultGetSpecificElements(
                    admin_ws.GetSpecificElementsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetSpecificElements operation
           */
            public void receiveErrorGetSpecificElements(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRuleSpec method
            * override this method for handling normal response from GetRuleSpec operation
            */
           public void receiveResultGetRuleSpec(
                    admin_ws.GetRuleSpecResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRuleSpec operation
           */
            public void receiveErrorGetRuleSpec(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for UpdateConfigTemplateRow method
            * override this method for handling normal response from UpdateConfigTemplateRow operation
            */
           public void receiveResultUpdateConfigTemplateRow(
                    admin_ws.UpdateConfigTemplateRowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from UpdateConfigTemplateRow operation
           */
            public void receiveErrorUpdateConfigTemplateRow(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRules method
            * override this method for handling normal response from GetRules operation
            */
           public void receiveResultGetRules(
                    admin_ws.GetRulesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRules operation
           */
            public void receiveErrorGetRules(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetLicenseInfo method
            * override this method for handling normal response from GetLicenseInfo operation
            */
           public void receiveResultGetLicenseInfo(
                    admin_ws.GetLicenseInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetLicenseInfo operation
           */
            public void receiveErrorGetLicenseInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GOnServiceGetList method
            * override this method for handling normal response from GOnServiceGetList operation
            */
           public void receiveResultGOnServiceGetList(
                    admin_ws.GOnServiceGetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GOnServiceGetList operation
           */
            public void receiveErrorGOnServiceGetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DownloadGPMStart method
            * override this method for handling normal response from DownloadGPMStart operation
            */
           public void receiveResultDownloadGPMStart(
                    admin_ws.DownloadGPMStartResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DownloadGPMStart operation
           */
            public void receiveErrorDownloadGPMStart(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeployGetServers method
            * override this method for handling normal response from DeployGetServers operation
            */
           public void receiveResultDeployGetServers(
                    admin_ws.DeployGetServersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeployGetServers operation
           */
            public void receiveErrorDeployGetServers(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetModuleElements method
            * override this method for handling normal response from GetModuleElements operation
            */
           public void receiveResultGetModuleElements(
                    admin_ws.GetModuleElementsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetModuleElements operation
           */
            public void receiveErrorGetModuleElements(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetGatewaySessionsNext method
            * override this method for handling normal response from GetGatewaySessionsNext operation
            */
           public void receiveResultGetGatewaySessionsNext(
                    admin_ws.GetGatewaySessionsNextResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetGatewaySessionsNext operation
           */
            public void receiveErrorGetGatewaySessionsNext(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetMenuItems method
            * override this method for handling normal response from GetMenuItems operation
            */
           public void receiveResultGetMenuItems(
                    admin_ws.GetMenuItemsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetMenuItems operation
           */
            public void receiveErrorGetMenuItems(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DownloadGPMStop method
            * override this method for handling normal response from DownloadGPMStop operation
            */
           public void receiveResultDownloadGPMStop(
                    admin_ws.DownloadGPMStopResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DownloadGPMStop operation
           */
            public void receiveErrorDownloadGPMStop(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DownloadGPMGetChunk method
            * override this method for handling normal response from DownloadGPMGetChunk operation
            */
           public void receiveResultDownloadGPMGetChunk(
                    admin_ws.DownloadGPMGetChunkResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DownloadGPMGetChunk operation
           */
            public void receiveErrorDownloadGPMGetChunk(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Login method
            * override this method for handling normal response from Login operation
            */
           public void receiveResultLogin(
                    admin_ws.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Login operation
           */
            public void receiveErrorLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Ping method
            * override this method for handling normal response from Ping operation
            */
           public void receiveResultPing(
                    admin_ws.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Ping operation
           */
            public void receiveErrorPing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetConfigTemplate method
            * override this method for handling normal response from GetConfigTemplate operation
            */
           public void receiveResultGetConfigTemplate(
                    admin_ws.GetConfigTemplateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetConfigTemplate operation
           */
            public void receiveErrorGetConfigTemplate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetBasicEntityTypes method
            * override this method for handling normal response from GetBasicEntityTypes operation
            */
           public void receiveResultGetBasicEntityTypes(
                    admin_ws.GetBasicEntityTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetBasicEntityTypes operation
           */
            public void receiveErrorGetBasicEntityTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for RemoveItemFromMenu method
            * override this method for handling normal response from RemoveItemFromMenu operation
            */
           public void receiveResultRemoveItemFromMenu(
                    admin_ws.RemoveItemFromMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from RemoveItemFromMenu operation
           */
            public void receiveErrorRemoveItemFromMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for InternalLogin method
            * override this method for handling normal response from InternalLogin operation
            */
           public void receiveResultInternalLogin(
                    admin_ws.InternalLoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from InternalLogin operation
           */
            public void receiveErrorInternalLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetConfigSpec method
            * override this method for handling normal response from GetConfigSpec operation
            */
           public void receiveResultGetConfigSpec(
                    admin_ws.GetConfigSpecResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetConfigSpec operation
           */
            public void receiveErrorGetConfigSpec(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRulesForElement method
            * override this method for handling normal response from GetRulesForElement operation
            */
           public void receiveResultGetRulesForElement(
                    admin_ws.GetRulesForElementResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRulesForElement operation
           */
            public void receiveErrorGetRulesForElement(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for PortScan method
            * override this method for handling normal response from PortScan operation
            */
           public void receiveResultPortScan(
                    admin_ws.PortScanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from PortScan operation
           */
            public void receiveErrorPortScan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetElementConfigTemplate method
            * override this method for handling normal response from GetElementConfigTemplate operation
            */
           public void receiveResultGetElementConfigTemplate(
                    admin_ws.GetElementConfigTemplateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetElementConfigTemplate operation
           */
            public void receiveErrorGetElementConfigTemplate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for RemoveUserFromLaunchTypeCategory method
            * override this method for handling normal response from RemoveUserFromLaunchTypeCategory operation
            */
           public void receiveResultRemoveUserFromLaunchTypeCategory(
                    admin_ws.RemoveUserFromLaunchTypeCategoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from RemoveUserFromLaunchTypeCategory operation
           */
            public void receiveErrorRemoveUserFromLaunchTypeCategory(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for UpdateRule method
            * override this method for handling normal response from UpdateRule operation
            */
           public void receiveResultUpdateRule(
                    admin_ws.UpdateRuleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from UpdateRule operation
           */
            public void receiveErrorUpdateRule(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetMenu method
            * override this method for handling normal response from GetMenu operation
            */
           public void receiveResultGetMenu(
                    admin_ws.GetMenuResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetMenu operation
           */
            public void receiveErrorGetMenu(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetElementSeachCategories method
            * override this method for handling normal response from GetElementSeachCategories operation
            */
           public void receiveResultGetElementSeachCategories(
                    admin_ws.GetElementSeachCategoriesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetElementSeachCategories operation
           */
            public void receiveErrorGetElementSeachCategories(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for CreateConfigTemplateRow method
            * override this method for handling normal response from CreateConfigTemplateRow operation
            */
           public void receiveResultCreateConfigTemplateRow(
                    admin_ws.CreateConfigTemplateRowResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from CreateConfigTemplateRow operation
           */
            public void receiveErrorCreateConfigTemplateRow(java.lang.Exception e) {
            }
                


    }
    