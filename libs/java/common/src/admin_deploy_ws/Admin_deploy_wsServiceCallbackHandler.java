
/**
 * Admin_deploy_wsServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package admin_deploy_ws;

    /**
     *  Admin_deploy_wsServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class Admin_deploy_wsServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public Admin_deploy_wsServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public Admin_deploy_wsServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for GetTokenTypes method
            * override this method for handling normal response from GetTokenTypes operation
            */
           public void receiveResultGetTokenTypes(
                    admin_deploy_ws.GetTokenTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetTokenTypes operation
           */
            public void receiveErrorGetTokenTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetJobInfo method
            * override this method for handling normal response from GetJobInfo operation
            */
           public void receiveResultGetJobInfo(
                    admin_deploy_ws.GetJobInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetJobInfo operation
           */
            public void receiveErrorGetJobInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for CancelJob method
            * override this method for handling normal response from CancelJob operation
            */
           public void receiveResultCancelJob(
                    admin_deploy_ws.CancelJobResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from CancelJob operation
           */
            public void receiveErrorCancelJob(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetGPMCollections method
            * override this method for handling normal response from GetGPMCollections operation
            */
           public void receiveResultGetGPMCollections(
                    admin_deploy_ws.GetGPMCollectionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetGPMCollections operation
           */
            public void receiveErrorGetGPMCollections(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for InstallGPMCollection method
            * override this method for handling normal response from InstallGPMCollection operation
            */
           public void receiveResultInstallGPMCollection(
                    admin_deploy_ws.InstallGPMCollectionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from InstallGPMCollection operation
           */
            public void receiveErrorInstallGPMCollection(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetRuntimeEnvInfo method
            * override this method for handling normal response from GetRuntimeEnvInfo operation
            */
           public void receiveResultGetRuntimeEnvInfo(
                    admin_deploy_ws.GetRuntimeEnvInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetRuntimeEnvInfo operation
           */
            public void receiveErrorGetRuntimeEnvInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Logout method
            * override this method for handling normal response from Logout operation
            */
           public void receiveResultLogout(
                    admin_deploy_ws.LogoutResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Logout operation
           */
            public void receiveErrorLogout(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for InitToken method
            * override this method for handling normal response from InitToken operation
            */
           public void receiveResultInitToken(
                    admin_deploy_ws.InitTokenResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from InitToken operation
           */
            public void receiveErrorInitToken(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Ping method
            * override this method for handling normal response from Ping operation
            */
           public void receiveResultPing(
                    admin_deploy_ws.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Ping operation
           */
            public void receiveErrorPing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeployToken method
            * override this method for handling normal response from DeployToken operation
            */
           public void receiveResultDeployToken(
                    admin_deploy_ws.DeployTokenResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeployToken operation
           */
            public void receiveErrorDeployToken(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Login method
            * override this method for handling normal response from Login operation
            */
           public void receiveResultLogin(
                    admin_deploy_ws.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Login operation
           */
            public void receiveErrorLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetTokens method
            * override this method for handling normal response from GetTokens operation
            */
           public void receiveResultGetTokens(
                    admin_deploy_ws.GetTokensResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetTokens operation
           */
            public void receiveErrorGetTokens(java.lang.Exception e) {
            }
                


    }
    