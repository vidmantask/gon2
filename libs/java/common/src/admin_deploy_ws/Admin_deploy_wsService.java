

/**
 * Admin_deploy_wsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package admin_deploy_ws;

    /*
     *  Admin_deploy_wsService java interface
     */

    public interface Admin_deploy_wsService {
          

        /**
          * Auto generated method signature
          * 
                    * @param getTokenTypes0
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.GetTokenTypesResponse GetTokenTypes(

                        admin_deploy_ws.GetTokenTypes getTokenTypes0)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTokenTypes0
            
          */
        public void startGetTokenTypes(

            admin_deploy_ws.GetTokenTypes getTokenTypes0,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getJobInfo2
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.GetJobInfoResponse GetJobInfo(

                        admin_deploy_ws.GetJobInfo getJobInfo2)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getJobInfo2
            
          */
        public void startGetJobInfo(

            admin_deploy_ws.GetJobInfo getJobInfo2,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param cancelJob4
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.CancelJobResponse CancelJob(

                        admin_deploy_ws.CancelJob cancelJob4)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param cancelJob4
            
          */
        public void startCancelJob(

            admin_deploy_ws.CancelJob cancelJob4,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getGPMCollections6
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.GetGPMCollectionsResponse GetGPMCollections(

                        admin_deploy_ws.GetGPMCollections getGPMCollections6)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getGPMCollections6
            
          */
        public void startGetGPMCollections(

            admin_deploy_ws.GetGPMCollections getGPMCollections6,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param installGPMCollection8
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.InstallGPMCollectionResponse InstallGPMCollection(

                        admin_deploy_ws.InstallGPMCollection installGPMCollection8)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param installGPMCollection8
            
          */
        public void startInstallGPMCollection(

            admin_deploy_ws.InstallGPMCollection installGPMCollection8,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRuntimeEnvInfo10
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.GetRuntimeEnvInfoResponse GetRuntimeEnvInfo(

                        admin_deploy_ws.GetRuntimeEnvInfo getRuntimeEnvInfo10)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRuntimeEnvInfo10
            
          */
        public void startGetRuntimeEnvInfo(

            admin_deploy_ws.GetRuntimeEnvInfo getRuntimeEnvInfo10,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param logout12
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.LogoutResponse Logout(

                        admin_deploy_ws.Logout logout12)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param logout12
            
          */
        public void startLogout(

            admin_deploy_ws.Logout logout12,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param initToken14
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.InitTokenResponse InitToken(

                        admin_deploy_ws.InitToken initToken14)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param initToken14
            
          */
        public void startInitToken(

            admin_deploy_ws.InitToken initToken14,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param ping16
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.PingResponse Ping(

                        admin_deploy_ws.Ping ping16)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param ping16
            
          */
        public void startPing(

            admin_deploy_ws.Ping ping16,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deployToken18
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.DeployTokenResponse DeployToken(

                        admin_deploy_ws.DeployToken deployToken18)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deployToken18
            
          */
        public void startDeployToken(

            admin_deploy_ws.DeployToken deployToken18,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param login20
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.LoginResponse Login(

                        admin_deploy_ws.Login login20)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param login20
            
          */
        public void startLogin(

            admin_deploy_ws.Login login20,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTokens22
                
             * @throws admin_deploy_ws.FaultServer : 
         */

         
                     public admin_deploy_ws.GetTokensResponse GetTokens(

                        admin_deploy_ws.GetTokens getTokens22)
                        throws java.rmi.RemoteException
             
          ,admin_deploy_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTokens22
            
          */
        public void startGetTokens(

            admin_deploy_ws.GetTokens getTokens22,

            final admin_deploy_ws.Admin_deploy_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    