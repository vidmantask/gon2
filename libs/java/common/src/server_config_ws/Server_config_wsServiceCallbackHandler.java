
/**
 * Server_config_wsServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package server_config_ws;

    /**
     *  Server_config_wsServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class Server_config_wsServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public Server_config_wsServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public Server_config_wsServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for Ping method
            * override this method for handling normal response from Ping operation
            */
           public void receiveResultPing(
                    server_config_ws.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Ping operation
           */
            public void receiveErrorPing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetStatus method
            * override this method for handling normal response from GetStatus operation
            */
           public void receiveResultGetStatus(
                    server_config_ws.GetStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetStatus operation
           */
            public void receiveErrorGetStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobGenerateDemodata method
            * override this method for handling normal response from StartJobGenerateDemodata operation
            */
           public void receiveResultStartJobGenerateDemodata(
                    server_config_ws.StartJobGenerateDemodataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobGenerateDemodata operation
           */
            public void receiveErrorStartJobGenerateDemodata(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetLicenseInfo method
            * override this method for handling normal response from GetLicenseInfo operation
            */
           public void receiveResultGetLicenseInfo(
                    server_config_ws.GetLicenseInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetLicenseInfo operation
           */
            public void receiveErrorGetLicenseInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetGOnSystems method
            * override this method for handling normal response from GetGOnSystems operation
            */
           public void receiveResultGetGOnSystems(
                    server_config_ws.GetGOnSystemsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetGOnSystems operation
           */
            public void receiveErrorGetGOnSystems(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for DeleteConfigTemplate method
            * override this method for handling normal response from DeleteConfigTemplate operation
            */
           public void receiveResultDeleteConfigTemplate(
                    server_config_ws.DeleteConfigTemplateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from DeleteConfigTemplate operation
           */
            public void receiveErrorDeleteConfigTemplate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for TestConfigSpecification method
            * override this method for handling normal response from TestConfigSpecification operation
            */
           public void receiveResultTestConfigSpecification(
                    server_config_ws.TestConfigSpecificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from TestConfigSpecification operation
           */
            public void receiveErrorTestConfigSpecification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for SaveConfigSpecification method
            * override this method for handling normal response from SaveConfigSpecification operation
            */
           public void receiveResultSaveConfigSpecification(
                    server_config_ws.SaveConfigSpecificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from SaveConfigSpecification operation
           */
            public void receiveErrorSaveConfigSpecification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobFinalizeInstallation method
            * override this method for handling normal response from StartJobFinalizeInstallation operation
            */
           public void receiveResultStartJobFinalizeInstallation(
                    server_config_ws.StartJobFinalizeInstallationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobFinalizeInstallation operation
           */
            public void receiveErrorStartJobFinalizeInstallation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetJobInfo method
            * override this method for handling normal response from GetJobInfo operation
            */
           public void receiveResultGetJobInfo(
                    server_config_ws.GetJobInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetJobInfo operation
           */
            public void receiveErrorGetJobInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetServices method
            * override this method for handling normal response from GetServices operation
            */
           public void receiveResultGetServices(
                    server_config_ws.GetServicesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetServices operation
           */
            public void receiveErrorGetServices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobPrepareChange method
            * override this method for handling normal response from StartJobPrepareChange operation
            */
           public void receiveResultStartJobPrepareChange(
                    server_config_ws.StartJobPrepareChangeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobPrepareChange operation
           */
            public void receiveErrorStartJobPrepareChange(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for CancelJob method
            * override this method for handling normal response from CancelJob operation
            */
           public void receiveResultCancelJob(
                    server_config_ws.CancelJobResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from CancelJob operation
           */
            public void receiveErrorCancelJob(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for SetLicense method
            * override this method for handling normal response from SetLicense operation
            */
           public void receiveResultSetLicense(
                    server_config_ws.SetLicenseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from SetLicense operation
           */
            public void receiveErrorSetLicense(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GOnManagementServiceControl method
            * override this method for handling normal response from GOnManagementServiceControl operation
            */
           public void receiveResultGOnManagementServiceControl(
                    server_config_ws.GOnManagementServiceControlResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GOnManagementServiceControl operation
           */
            public void receiveErrorGOnManagementServiceControl(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for SaveConfigTemplate method
            * override this method for handling normal response from SaveConfigTemplate operation
            */
           public void receiveResultSaveConfigTemplate(
                    server_config_ws.SaveConfigTemplateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from SaveConfigTemplate operation
           */
            public void receiveErrorSaveConfigTemplate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetFirstConfigSpecification method
            * override this method for handling normal response from GetFirstConfigSpecification operation
            */
           public void receiveResultGetFirstConfigSpecification(
                    server_config_ws.GetFirstConfigSpecificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetFirstConfigSpecification operation
           */
            public void receiveErrorGetFirstConfigSpecification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Logout method
            * override this method for handling normal response from Logout operation
            */
           public void receiveResultLogout(
                    server_config_ws.LogoutResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Logout operation
           */
            public void receiveErrorLogout(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetNextConfigSpecification method
            * override this method for handling normal response from GetNextConfigSpecification operation
            */
           public void receiveResultGetNextConfigSpecification(
                    server_config_ws.GetNextConfigSpecificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetNextConfigSpecification operation
           */
            public void receiveErrorGetNextConfigSpecification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobPrepareInstallation method
            * override this method for handling normal response from StartJobPrepareInstallation operation
            */
           public void receiveResultStartJobPrepareInstallation(
                    server_config_ws.StartJobPrepareInstallationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobPrepareInstallation operation
           */
            public void receiveErrorStartJobPrepareInstallation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobInstallServices method
            * override this method for handling normal response from StartJobInstallServices operation
            */
           public void receiveResultStartJobInstallServices(
                    server_config_ws.StartJobInstallServicesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobInstallServices operation
           */
            public void receiveErrorStartJobInstallServices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for Login method
            * override this method for handling normal response from Login operation
            */
           public void receiveResultLogin(
                    server_config_ws.LoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from Login operation
           */
            public void receiveErrorLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobPrepareUpgrade method
            * override this method for handling normal response from StartJobPrepareUpgrade operation
            */
           public void receiveResultStartJobPrepareUpgrade(
                    server_config_ws.StartJobPrepareUpgradeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobPrepareUpgrade operation
           */
            public void receiveErrorStartJobPrepareUpgrade(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ReloadConfigSpecification method
            * override this method for handling normal response from ReloadConfigSpecification operation
            */
           public void receiveResultReloadConfigSpecification(
                    server_config_ws.ReloadConfigSpecificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ReloadConfigSpecification operation
           */
            public void receiveErrorReloadConfigSpecification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobGenerateGPMS method
            * override this method for handling normal response from StartJobGenerateGPMS operation
            */
           public void receiveResultStartJobGenerateGPMS(
                    server_config_ws.StartJobGenerateGPMSResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobGenerateGPMS operation
           */
            public void receiveErrorStartJobGenerateGPMS(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GetConfigTemplate method
            * override this method for handling normal response from GetConfigTemplate operation
            */
           public void receiveResultGetConfigTemplate(
                    server_config_ws.GetConfigTemplateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GetConfigTemplate operation
           */
            public void receiveErrorGetConfigTemplate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobGenerateKnownsecrets method
            * override this method for handling normal response from StartJobGenerateKnownsecrets operation
            */
           public void receiveResultStartJobGenerateKnownsecrets(
                    server_config_ws.StartJobGenerateKnownsecretsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobGenerateKnownsecrets operation
           */
            public void receiveErrorStartJobGenerateKnownsecrets(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobFinalizeUpgrade method
            * override this method for handling normal response from StartJobFinalizeUpgrade operation
            */
           public void receiveResultStartJobFinalizeUpgrade(
                    server_config_ws.StartJobFinalizeUpgradeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobFinalizeUpgrade operation
           */
            public void receiveErrorStartJobFinalizeUpgrade(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for GOnManagementServiceGetStatus method
            * override this method for handling normal response from GOnManagementServiceGetStatus operation
            */
           public void receiveResultGOnManagementServiceGetStatus(
                    server_config_ws.GOnManagementServiceGetStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from GOnManagementServiceGetStatus operation
           */
            public void receiveErrorGOnManagementServiceGetStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobFinalizeChange method
            * override this method for handling normal response from StartJobFinalizeChange operation
            */
           public void receiveResultStartJobFinalizeChange(
                    server_config_ws.StartJobFinalizeChangeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobFinalizeChange operation
           */
            public void receiveErrorStartJobFinalizeChange(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for StartJobGenerateSupportPackage method
            * override this method for handling normal response from StartJobGenerateSupportPackage operation
            */
           public void receiveResultStartJobGenerateSupportPackage(
                    server_config_ws.StartJobGenerateSupportPackageResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from StartJobGenerateSupportPackage operation
           */
            public void receiveErrorStartJobGenerateSupportPackage(java.lang.Exception e) {
            }
                


    }
    