

/**
 * Server_config_wsService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package server_config_ws;

    /*
     *  Server_config_wsService java interface
     */

    public interface Server_config_wsService {
          

        /**
          * Auto generated method signature
          * 
                    * @param ping0
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.PingResponse Ping(

                        server_config_ws.Ping ping0)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param ping0
            
          */
        public void startPing(

            server_config_ws.Ping ping0,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getStatus2
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetStatusResponse GetStatus(

                        server_config_ws.GetStatus getStatus2)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getStatus2
            
          */
        public void startGetStatus(

            server_config_ws.GetStatus getStatus2,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobGenerateDemodata4
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobGenerateDemodataResponse StartJobGenerateDemodata(

                        server_config_ws.StartJobGenerateDemodata startJobGenerateDemodata4)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobGenerateDemodata4
            
          */
        public void startStartJobGenerateDemodata(

            server_config_ws.StartJobGenerateDemodata startJobGenerateDemodata4,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getLicenseInfo6
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetLicenseInfoResponse GetLicenseInfo(

                        server_config_ws.GetLicenseInfo getLicenseInfo6)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getLicenseInfo6
            
          */
        public void startGetLicenseInfo(

            server_config_ws.GetLicenseInfo getLicenseInfo6,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getGOnSystems8
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetGOnSystemsResponse GetGOnSystems(

                        server_config_ws.GetGOnSystems getGOnSystems8)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getGOnSystems8
            
          */
        public void startGetGOnSystems(

            server_config_ws.GetGOnSystems getGOnSystems8,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deleteConfigTemplate10
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.DeleteConfigTemplateResponse DeleteConfigTemplate(

                        server_config_ws.DeleteConfigTemplate deleteConfigTemplate10)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deleteConfigTemplate10
            
          */
        public void startDeleteConfigTemplate(

            server_config_ws.DeleteConfigTemplate deleteConfigTemplate10,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param testConfigSpecification12
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.TestConfigSpecificationResponse TestConfigSpecification(

                        server_config_ws.TestConfigSpecification testConfigSpecification12)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param testConfigSpecification12
            
          */
        public void startTestConfigSpecification(

            server_config_ws.TestConfigSpecification testConfigSpecification12,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param saveConfigSpecification14
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.SaveConfigSpecificationResponse SaveConfigSpecification(

                        server_config_ws.SaveConfigSpecification saveConfigSpecification14)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param saveConfigSpecification14
            
          */
        public void startSaveConfigSpecification(

            server_config_ws.SaveConfigSpecification saveConfigSpecification14,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobFinalizeInstallation16
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobFinalizeInstallationResponse StartJobFinalizeInstallation(

                        server_config_ws.StartJobFinalizeInstallation startJobFinalizeInstallation16)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobFinalizeInstallation16
            
          */
        public void startStartJobFinalizeInstallation(

            server_config_ws.StartJobFinalizeInstallation startJobFinalizeInstallation16,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getJobInfo18
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetJobInfoResponse GetJobInfo(

                        server_config_ws.GetJobInfo getJobInfo18)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getJobInfo18
            
          */
        public void startGetJobInfo(

            server_config_ws.GetJobInfo getJobInfo18,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getServices20
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetServicesResponse GetServices(

                        server_config_ws.GetServices getServices20)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getServices20
            
          */
        public void startGetServices(

            server_config_ws.GetServices getServices20,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobPrepareChange22
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobPrepareChangeResponse StartJobPrepareChange(

                        server_config_ws.StartJobPrepareChange startJobPrepareChange22)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobPrepareChange22
            
          */
        public void startStartJobPrepareChange(

            server_config_ws.StartJobPrepareChange startJobPrepareChange22,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param cancelJob24
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.CancelJobResponse CancelJob(

                        server_config_ws.CancelJob cancelJob24)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param cancelJob24
            
          */
        public void startCancelJob(

            server_config_ws.CancelJob cancelJob24,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param setLicense26
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.SetLicenseResponse SetLicense(

                        server_config_ws.SetLicense setLicense26)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param setLicense26
            
          */
        public void startSetLicense(

            server_config_ws.SetLicense setLicense26,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gOnManagementServiceControl28
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GOnManagementServiceControlResponse GOnManagementServiceControl(

                        server_config_ws.GOnManagementServiceControl gOnManagementServiceControl28)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gOnManagementServiceControl28
            
          */
        public void startGOnManagementServiceControl(

            server_config_ws.GOnManagementServiceControl gOnManagementServiceControl28,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param saveConfigTemplate30
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.SaveConfigTemplateResponse SaveConfigTemplate(

                        server_config_ws.SaveConfigTemplate saveConfigTemplate30)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param saveConfigTemplate30
            
          */
        public void startSaveConfigTemplate(

            server_config_ws.SaveConfigTemplate saveConfigTemplate30,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getFirstConfigSpecification32
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetFirstConfigSpecificationResponse GetFirstConfigSpecification(

                        server_config_ws.GetFirstConfigSpecification getFirstConfigSpecification32)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getFirstConfigSpecification32
            
          */
        public void startGetFirstConfigSpecification(

            server_config_ws.GetFirstConfigSpecification getFirstConfigSpecification32,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param logout34
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.LogoutResponse Logout(

                        server_config_ws.Logout logout34)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param logout34
            
          */
        public void startLogout(

            server_config_ws.Logout logout34,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getNextConfigSpecification36
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetNextConfigSpecificationResponse GetNextConfigSpecification(

                        server_config_ws.GetNextConfigSpecification getNextConfigSpecification36)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getNextConfigSpecification36
            
          */
        public void startGetNextConfigSpecification(

            server_config_ws.GetNextConfigSpecification getNextConfigSpecification36,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobPrepareInstallation38
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobPrepareInstallationResponse StartJobPrepareInstallation(

                        server_config_ws.StartJobPrepareInstallation startJobPrepareInstallation38)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobPrepareInstallation38
            
          */
        public void startStartJobPrepareInstallation(

            server_config_ws.StartJobPrepareInstallation startJobPrepareInstallation38,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobInstallServices40
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobInstallServicesResponse StartJobInstallServices(

                        server_config_ws.StartJobInstallServices startJobInstallServices40)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobInstallServices40
            
          */
        public void startStartJobInstallServices(

            server_config_ws.StartJobInstallServices startJobInstallServices40,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param login42
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.LoginResponse Login(

                        server_config_ws.Login login42)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param login42
            
          */
        public void startLogin(

            server_config_ws.Login login42,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobPrepareUpgrade44
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobPrepareUpgradeResponse StartJobPrepareUpgrade(

                        server_config_ws.StartJobPrepareUpgrade startJobPrepareUpgrade44)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobPrepareUpgrade44
            
          */
        public void startStartJobPrepareUpgrade(

            server_config_ws.StartJobPrepareUpgrade startJobPrepareUpgrade44,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param reloadConfigSpecification46
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.ReloadConfigSpecificationResponse ReloadConfigSpecification(

                        server_config_ws.ReloadConfigSpecification reloadConfigSpecification46)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param reloadConfigSpecification46
            
          */
        public void startReloadConfigSpecification(

            server_config_ws.ReloadConfigSpecification reloadConfigSpecification46,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobGenerateGPMS48
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobGenerateGPMSResponse StartJobGenerateGPMS(

                        server_config_ws.StartJobGenerateGPMS startJobGenerateGPMS48)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobGenerateGPMS48
            
          */
        public void startStartJobGenerateGPMS(

            server_config_ws.StartJobGenerateGPMS startJobGenerateGPMS48,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getConfigTemplate50
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GetConfigTemplateResponse GetConfigTemplate(

                        server_config_ws.GetConfigTemplate getConfigTemplate50)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getConfigTemplate50
            
          */
        public void startGetConfigTemplate(

            server_config_ws.GetConfigTemplate getConfigTemplate50,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobGenerateKnownsecrets52
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobGenerateKnownsecretsResponse StartJobGenerateKnownsecrets(

                        server_config_ws.StartJobGenerateKnownsecrets startJobGenerateKnownsecrets52)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobGenerateKnownsecrets52
            
          */
        public void startStartJobGenerateKnownsecrets(

            server_config_ws.StartJobGenerateKnownsecrets startJobGenerateKnownsecrets52,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobFinalizeUpgrade54
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobFinalizeUpgradeResponse StartJobFinalizeUpgrade(

                        server_config_ws.StartJobFinalizeUpgrade startJobFinalizeUpgrade54)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobFinalizeUpgrade54
            
          */
        public void startStartJobFinalizeUpgrade(

            server_config_ws.StartJobFinalizeUpgrade startJobFinalizeUpgrade54,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param gOnManagementServiceGetStatus56
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.GOnManagementServiceGetStatusResponse GOnManagementServiceGetStatus(

                        server_config_ws.GOnManagementServiceGetStatus gOnManagementServiceGetStatus56)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param gOnManagementServiceGetStatus56
            
          */
        public void startGOnManagementServiceGetStatus(

            server_config_ws.GOnManagementServiceGetStatus gOnManagementServiceGetStatus56,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobFinalizeChange58
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobFinalizeChangeResponse StartJobFinalizeChange(

                        server_config_ws.StartJobFinalizeChange startJobFinalizeChange58)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobFinalizeChange58
            
          */
        public void startStartJobFinalizeChange(

            server_config_ws.StartJobFinalizeChange startJobFinalizeChange58,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param startJobGenerateSupportPackage60
                
             * @throws server_config_ws.FaultServer : 
         */

         
                     public server_config_ws.StartJobGenerateSupportPackageResponse StartJobGenerateSupportPackage(

                        server_config_ws.StartJobGenerateSupportPackage startJobGenerateSupportPackage60)
                        throws java.rmi.RemoteException
             
          ,server_config_ws.FaultServer;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param startJobGenerateSupportPackage60
            
          */
        public void startStartJobGenerateSupportPackage(

            server_config_ws.StartJobGenerateSupportPackage startJobGenerateSupportPackage60,

            final server_config_ws.Server_config_wsServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    