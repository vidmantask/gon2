package sample_code;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class SampleEditableTable {

	
	public void createTable1(FormToolkit toolkit, Composite masterSection) {
		final Table table = toolkit.createTable(masterSection, SWT.BORDER | SWT.MULTI);
		table.setLinesVisible (true);
		for (int i=0; i<3; i++) {
			TableColumn column = new TableColumn (table, SWT.NONE);
			column.setWidth(100);
		}
		for (int i=0; i<3; i++) {
			TableItem item = new TableItem (table, SWT.NONE);
			item.setText(new String [] {"" + i, "" + i , "" + i});
		}
		final TableEditor editor = new TableEditor (table);
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
		table.addListener (SWT.MouseDown, new Listener () {
			public void handleEvent (Event event) {
				Rectangle clientArea = table.getClientArea ();
				Point pt = new Point (event.x, event.y);
				int index = table.getTopIndex ();
				while (index < table.getItemCount ()) {
					boolean visible = false;
					final TableItem item = table.getItem (index);
					for (int i=0; i<table.getColumnCount (); i++) {
						Rectangle rect = item.getBounds (i);
						if (rect.contains (pt)) {
							final int column = i;
							final Text text = new Text (table, SWT.NONE);
							Listener textListener = new Listener () {
								public void handleEvent (final Event e) {
									switch (e.type) {
										case SWT.FocusOut:
											item.setText (column, text.getText ());
											text.dispose ();
											break;
										case SWT.Traverse:
											switch (e.detail) {
												case SWT.TRAVERSE_RETURN:
													item.setText (column, text.getText ());
													//FALL THROUGH
												case SWT.TRAVERSE_ESCAPE:
													text.dispose ();
													e.doit = false;
											}
											break;
									}
								}
							};
							text.addListener (SWT.FocusOut, textListener);
							text.addListener (SWT.Traverse, textListener);
							editor.setEditor (text, item, i);
							text.setText (item.getText (i));
							text.selectAll ();
							text.setFocus ();
							return;
						}
						if (!visible && rect.intersects (clientArea)) {
							visible = true;
						}
					}
					if (!visible) return;
					index++;
				}
			}
		});
		
	}

	public void createTable(FormToolkit toolkit, Composite masterSection) {
		final Table table = toolkit.createTable(masterSection, SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		TableColumn[] cols = new TableColumn[3];
		cols[0] = new TableColumn(table, SWT.LEFT);
		cols[1] = new TableColumn(table, SWT.LEFT);
		cols[2] = new TableColumn(table, SWT.LEFT);
		cols[0].setText("Column 1");
		cols[1].setText("Column 2");
		cols[2].setText("Column 3");
		cols[0].pack();
		cols[1].pack();
		cols[2].pack();

		for (int i = 0; i < 10; i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			//item.setText(new String[] {"item " + i, "edit this value", "Yo"});
			item.setText(new String[] {"item " + i, "edit this value", "Yo"});
		}
		

		final TableEditor editor = new TableEditor(table);
		//The editor must have the same size as the cell and must
		//not be any smaller than 50 pixels.
		editor.horizontalAlignment = SWT.LEFT;
		editor.grabHorizontal = true;
		editor.minimumWidth = 50;
		// editing the second column
		
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDown(MouseEvent e) {
				Point pt = new Point(e.x, e.y);
				TableItem item = table.getItem(pt);
				if (item!=null) {
					for(int i=0; i<table.getColumnCount(); i++) {
						if (item.getBounds(i).contains(pt)) {
							final int EDITABLECOLUMN = i;
							// Clean up any previous editor control
							Control oldEditor = editor.getEditor();
							if (oldEditor != null) oldEditor.dispose();
							
							// The control that will be the editor must be a child of the Table
							Text newEditor = new Text(table, SWT.NONE);
							newEditor.setText(item.getText(EDITABLECOLUMN));
							newEditor.addModifyListener(new ModifyListener() {
								public void modifyText(ModifyEvent me) {
									Text text = (Text)editor.getEditor();
									editor.getItem().setText(EDITABLECOLUMN, text.getText());
								}
							});
							newEditor.selectAll();
							newEditor.setFocus();
							editor.setEditor(newEditor, item, EDITABLECOLUMN);
						}
					}
				}
			}
			
		});
		
//		table.addSelectionListener(new SelectionAdapter() {
//
//			public void widgetSelected(SelectionEvent e) {
//				// Clean up any previous editor control
//				Control oldEditor = editor.getEditor();
//				if (oldEditor != null) oldEditor.dispose();
//		
//				// Identify the selected row
//				TableItem item = (TableItem)e.item;
//				if (item == null) return;
//				
//		
//				// The control that will be the editor must be a child of the Table
//				Text newEditor = new Text(table, SWT.NONE);
//				newEditor.setText(item.getText(EDITABLECOLUMN));
//				newEditor.addModifyListener(new ModifyListener() {
//					public void modifyText(ModifyEvent me) {
//						Text text = (Text)editor.getEditor();
//						editor.getItem().setText(EDITABLECOLUMN, text.getText());
//					}
//				});
//				newEditor.selectAll();
//				newEditor.setFocus();
//				editor.setEditor(newEditor, item, EDITABLECOLUMN);
//			}
//		});
		table.pack();
		
	}
	
}
