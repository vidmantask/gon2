package gon_client_management;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.ui.AbstractSourceProvider;
import org.eclipse.ui.IPageListener;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveListener;
import org.eclipse.ui.ISources;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.services.ISourceProviderService;

public class GPerspectiveChangeHandler implements IHandler {

	static ArrayList<IPerspectiveDescriptor> perspectiveStack = new ArrayList<IPerspectiveDescriptor>();
	static int currentIndex = -1;
	static boolean skipUpdateActivated = false;
	
	public static class PerspectiveHistorySourceProvider extends AbstractSourceProvider {

		public final static String BACK_HISTORY_STATE = "giritech.com.back_history_state"; 
		public final static String FORWARD_HISTORY_STATE = "giritech.com.forward_history_state"; 
		
		private final static String EXISTS = "exists";
	    private final static String NOT_EXISTS = "not_exists";
	    
	    Map<String, String> currentState = new HashMap<String, String>(); 

		public void updateState() {
			if (currentIndex > 0)
				currentState.put(BACK_HISTORY_STATE, EXISTS);
			else
				currentState.put(BACK_HISTORY_STATE, NOT_EXISTS);
			
			if (currentIndex < perspectiveStack.size()-1)
				currentState.put(FORWARD_HISTORY_STATE, EXISTS);
			else
				currentState.put(FORWARD_HISTORY_STATE, NOT_EXISTS);

			fireSourceChanged(ISources.WORKBENCH, currentState);
		}

	    
		@Override
		public void dispose() {
		}

		@Override
		public Map<String, String> getCurrentState() {
			return currentState;
		}

		@Override
		public String[] getProvidedSourceNames() {
			return new String[] {BACK_HISTORY_STATE, FORWARD_HISTORY_STATE};		}
		
	}
	
	static void updateSourceProvider() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
        ISourceProviderService service = (ISourceProviderService) window.getService(ISourceProviderService.class);
        PerspectiveHistorySourceProvider sessionSourceProvider = (PerspectiveHistorySourceProvider) service.getSourceProvider(PerspectiveHistorySourceProvider.BACK_HISTORY_STATE);
        sessionSourceProvider.updateState();
		
	}
	
	static void initialize(IWorkbenchWindow activeWorkbenchWindow) {
		if (activeWorkbenchWindow!=null) {
			activeWorkbenchWindow.addPerspectiveListener(new IPerspectiveListener() {
				
				@Override
				public void perspectiveChanged(IWorkbenchPage page, IPerspectiveDescriptor perspective, String changeId) {
					System.out.println("Yo");
				}
				
				@Override
				public void perspectiveActivated(IWorkbenchPage page, IPerspectiveDescriptor perspective) {
					if (skipUpdateActivated) {
						skipUpdateActivated = false;
						return;
					}
					if (perspectiveStack.size()>0) {
						if (perspectiveStack.get(currentIndex).equals(perspective))
							return;
					}
					while (currentIndex < perspectiveStack.size()-1) {
						perspectiveStack.remove(perspectiveStack.size()-1);
					}
					perspectiveStack.add(perspective);
					currentIndex = perspectiveStack.size()-1;
					updateSourceProvider();
					System.out.println("Add perspective " + perspective.getLabel());
					
				}
			});
			if (activeWorkbenchWindow.getActivePage()!=null && activeWorkbenchWindow.getActivePage().getPerspective()!=null) {
				perspectiveStack.add(activeWorkbenchWindow.getActivePage().getPerspective());
				currentIndex = 0;
			}
			
			activeWorkbenchWindow.addPageListener(new IPageListener() {
				
				
				@Override
				public void pageOpened(IWorkbenchPage page) {
				}
				
				@Override
				public void pageClosed(IWorkbenchPage page) {
					perspectiveStack.clear();
					currentIndex = 0;
					updateSourceProvider();
				}
				
				@Override
				public void pageActivated(IWorkbenchPage page) {
				}
			});
			
			
		}
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String commandId = event.getCommand().getId();
		if (commandId.equals("java_gon_client_management.toolbar.command_back")) {
			try {
				if (perspectiveStack.size()>0 && currentIndex>0) {
					currentIndex--;
					IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					skipUpdateActivated = true;
					activePage.setPerspective(perspectiveStack.get(currentIndex));
					updateSourceProvider();
				}
			}
			catch(Throwable t) {
				throw new RuntimeException(t);
			}
		}
		else if (commandId.equals("java_gon_client_management.toolbar.command_forward")) {
			try {
				if (perspectiveStack.size()>0 && currentIndex < perspectiveStack.size()-1) {
					currentIndex++;
					IWorkbenchPage activePage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					skipUpdateActivated = true;
					activePage.setPerspective(perspectiveStack.get(currentIndex));
					updateSourceProvider();
				}
			}
			catch(Throwable t) {
				throw new RuntimeException(t);
			}
		}
		return null;
	}




	@Override
	public boolean isEnabled() {
		return true;
	}




	@Override
	public boolean isHandled() {
		return true;
	}

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {}
	@Override
	public void dispose() {}
	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {}
	


}
