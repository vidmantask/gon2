package gon_client_management.view.report;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import gon_client_management.model.GIReportPane;
	
public class ReportElementContentProvider implements IStructuredContentProvider { 
		
	final GIReportPane reportPane;

	public ReportElementContentProvider(GIReportPane pane) {
		this.reportPane = pane;
	}

	//@Override
	public Object[] getElements(Object inputElement) {
		return reportPane.getReports().toArray();		
	}

	//@Override
	public void dispose() {
	}

	//@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
