package gon_client_management.view.ext;

import gon_client_management.ext.GILogger;
import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.model.ext.GServiceTimeoutException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_job.Job_status_type0;


public class GJobHandler extends Job  {
	private final GIJob installSoftwareJob;
	private final Display display;
	private final String myId;
	private final GILogger logger;

	static int runCount = 0;
	
	GIJobInfo lastProgress = null;
	private int lastProgressCount = 0;
	String statusText = "";
	
	private int wait_millis = 2000;

	private JobUpdateHandler progressBarUpdater = null;
	private boolean finished = false;
	private boolean jobCancelled = false;

	public interface JobUpdateHandler {

		void update(String jobId, GIJobInfo status);

		void finished(String jobId, GIJobInfo lastProgress);
		
	}
	

	public GJobHandler(String name, String JobId, Display display, GIJob installSoftwareJob, GILogger logger) {
		super(name);
		this.installSoftwareJob = installSoftwareJob;
		this.myId = JobId;;
		this.display = display;
		this.logger = logger;
		
		lastProgress = new GIJobInfo() {

			public String getJobHeader() { return "";}

			public String getJobInfo() { return "";}

			public int getJobProgress()  { return 0;}

			public Job_status_type0 getJobStatus()  { return Job_status_type0.running_job_status;}

			public boolean more() { return true; }

			@Override
			public JobInfoType getJobInfoType() { return null; }
			
		};
	}

	public boolean cancelJob() {
		if (!jobCancelled) 
			try {
				jobCancelled = installSoftwareJob.cancel();
			}
			catch(GServiceTimeoutException e) {
				// ignore
			}
		return jobCancelled;
	}

	@Override
	protected void canceling() {
		super.canceling();
		cancelJob();
	}


	public void setJobUpdateHandler(JobUpdateHandler progressBarUpdater) {
		this.progressBarUpdater = progressBarUpdater;
	}

	public void removeJobUpdateHandler() {
		this.progressBarUpdater = null;
	}
	
	public GIJobInfo getProgress() {
		return lastProgress;
	}

	@Override
	public IStatus run(IProgressMonitor monitor) {
		try {
			installSoftwareJob.start();
			monitor.beginTask(getName(), installSoftwareJob.getTotalWork());
			while(lastProgress.more()) {
				if (monitor.isCanceled()) {
					cancelJob();
				}
				try {
					Thread.sleep(wait_millis);
				} catch (InterruptedException e) {
				}
				final GIJobInfo status;
				try {
					status = installSoftwareJob.getStatus();
				}
				catch(GServiceTimeoutException e) {
					// Server busy - let's wait a while before asking again
					try {
						Thread.sleep(10*wait_millis);
					} catch (InterruptedException e1) {
					}
					logger.logError("Server timeout getting job status");
					continue;
				}
				final int jobProgress = status.getJobProgress();
				if (jobProgress >= 0) {
					int worked = jobProgress - lastProgressCount;
					lastProgressCount = status.getJobProgress();
					monitor.worked(worked);
					if (status.getJobHeader().length()>0) {
						if (status.getJobInfo().length()>0) {
							monitor.setTaskName(status.getJobHeader() + ": " + status.getJobInfo() );
						}
						else {
							monitor.setTaskName(status.getJobHeader());
							
						}
					}
				}
				lastProgress = status;
				if (progressBarUpdater!=null && !display.isDisposed()) {
					display.asyncExec (new Runnable() {
						public void run () {
							progressBarUpdater.update(myId, status);
						}
					});
				}
			}
			monitor.done();
			finished = true;
			if (progressBarUpdater!=null && !display.isDisposed()) {
				display.asyncExec (new Runnable() {
					public void run () {
						progressBarUpdater.finished(myId, lastProgress);
					}
				});
			}
		}
		catch(final Exception e) {
			logger.logException(e);
			
			lastProgress = new GIJobInfo() {

				public String getJobHeader() { return "Error";}

				public String getJobInfo() { return e.getMessage();}

				public int getJobProgress()  { return -1;}

				public Job_status_type0 getJobStatus()  { return Job_status_type0.done_error_job_status;}

				public boolean more() { return false; }

				public JobInfoType getJobInfoType() { return null; }
				
			};
			if (progressBarUpdater!=null && !display.isDisposed()) {
				display.asyncExec (new Runnable() {
					public void run () {
						progressBarUpdater.finished(myId, lastProgress);
					}
				});
			}
			
			return Status.CANCEL_STATUS; 
		}
		finally {
			if (lastProgress.more()) {
				
				lastProgress = new GIJobInfo() {

					public String getJobHeader() { return "Unknown";}

					public String getJobInfo() { return "Unknown";}

					public int getJobProgress()  { return -1;}

					public Job_status_type0 getJobStatus()  { return Job_status_type0.running_job_status;}

					public boolean more() { return false; }

					@Override
					public JobInfoType getJobInfoType() { return null; }
					
				};
			}
		}
		
		return Status.OK_STATUS;
	}

	public boolean isFinished() {
		return finished;
	}

	public String getId() {
		return myId;
	}

	public int getWaitMillis() {
		return wait_millis;
	}

	public void setWaitMillis(int wait_millis) {
		this.wait_millis = wait_millis;
	}

	
}
