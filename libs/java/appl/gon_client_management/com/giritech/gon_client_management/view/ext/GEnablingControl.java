/**
 * 
 */
package gon_client_management.view.ext;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class GEnablingControl {
		
		private Composite parent;
		private Button enabler;
		private List<GEnablingControl> subEnablingControls = new ArrayList<GEnablingControl>();
		private List<Control> subControls = new ArrayList<Control>();

		public GEnablingControl(Composite parent, Button enabler) {
			this.parent = parent;
			this.enabler = enabler;
			
			
		
		}
		
		private void setEnabled(Control control, boolean enabled) {
			boolean wasEnabled = control.isEnabled();
			if (wasEnabled!=enabled)
				control.setEnabled(enabled);
		}

		public void setVisibility() {
			boolean enabled = getEnabling();
			List<Control> children = getChildren();
			for (Control control : children) {
				control.setVisible(enabled);
			}
		}
		
		public boolean getEnabling() {
			if (enabler!=null)
				return enabler.getSelection();
			else
				return true;
			
		}
		
		public void setEnabled(boolean enabled) {
			if (!enabled) {
				List<Control> children = getChildren();
				for (Control control : children) {
//					control.setEnabled(false);
					setEnabled(control, false);
				}
			}
			else {
				List<Control> children = getChildren();
				for (Control control : children) {
					setEnabled(control, true);
//					control.setEnabled(true);
				}
				for (GEnablingControl enablingControl : subEnablingControls) {
					enablingControl.setEnabling();
				}
			}
			
			
		}
		
		
		public void setEnabling() {
			boolean enabled = getEnabling();
			setEnabled(enabled);
		}

		
		public List<Control> getChildren() {
//			ArrayList<Control> children = new ArrayList<Control>();
			return subControls;
		}
		
		public void addChild(GEnablingControl enablingControl) {
			subEnablingControls.add(enablingControl);
			List<Control> children = enablingControl.getChildren();
			for (Control control : children) {
				addChild(control);
				
			}
		}

		public void addChild(Control control) {
			subControls.add(control);
			
		}

		public Composite getParent() {
			return parent;
		}
		
	}