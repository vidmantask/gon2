package gon_client_management.view.config;

import gon_client_management.Activator;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.GIGatewaySessionsConfig;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.view.ext.GSafeView;
import gon_client_management.view.preferences.PreferenceConstants;
import gon_client_management.view.util.ElementListingRetriever;
import gon_client_management.view.util.GGuiUtils.GIMultipleJobElementHandler;
import gon_client_management.view.util.GGuiUtils.GMultipleJob;
import gon_client_management.view.util.GGuiUtils.GMultipleJobElementHandlerAdapter;
import gon_client_management.view.util.GIElementListing;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import com.giritech.admin_ws.GOnServiceType;
import com.giritech.admin_ws.GOnSessionType;




public class GGatewayUsersView extends GSafeView implements ISelectionListener, GIElementListing {

	Action stopAction, recalcuateMenuAction, refreshAction;

	public GGatewayUsersView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}
	
	public static final String VIEW_ALL = "All";
	public static final String VIEW_SERVERS = "Selected servers";

	private static final String[] showChoices = { VIEW_ALL, VIEW_SERVERS };

	public static final String ID = "gon_client_management.GatewayUsersView";
	private TableViewer servicesViewer = null;
	protected boolean complete = false;
	private ViewContentProvider provider;
	private GIGatewaySessionsConfig modelAPI = null;
	private Set<String> server_sids = new HashSet<String>();
	private ElementListingRetriever retreiver;
	private boolean shownFirst = false;
	private Section servicesSection;
	private int refreshSeconds = 30;
	private RefreshThread autoUpdateThread;
	private Combo showCombo;

	private Button autoUpdateButton;

	private Text searchField;

	private Form statusForm;

	private boolean autoUpdate;

	private String viewChoice;
	private Action stopFetchingElementsAction;
	private MyViewerComparator comparator;
	
	/**
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	class ViewContentProvider implements IStructuredContentProvider {
		

		ViewContentProvider() {
		}
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			return modelAPI.getSessions().toArray();
		}
	}
	

	class ViewLabelProvider extends LabelProvider implements
			ITableLabelProvider {
		
		
		public String getColumnText(Object obj, int index) {
			GOnSessionType service = (GOnSessionType) obj;
			switch (index) {
			case 0:
				return service.getUser_login();
			case 1:
				return service.getUser_name();
			case 2:
				return service.getServer_title();
			case 3:
				return service.getClient_ip();
			case 4:
				try {
					Date sessionStart = CommonUtils.DateTimeConverter.string2datetime(service.getSession_start());
					return CommonUtils.DateTimeConverter.datetime2local(sessionStart);
				} catch (ParseException e) {
					Activator.getLogger().logException(e);
					return service.getSession_start();
				}
			default:
				return "N/A";
			}
		}




		public Image getColumnImage(Object obj, int index) {
			return null;
		}

	}
	
	
	// This will create the columns for the table
	private void createColumns(TableViewer viewer) {
		Table table = viewer.getTable();
		String[] titles = { "User login", "User name", "Server name", "Client IP", "Session start" };
		int[] bounds = { 100, 100, 100, 100, 100 };

		
		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(bounds[i]);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(true);
			column.getColumn().addSelectionListener(getSelectionAdapter(column.getColumn(), i));
			
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	

	public void createActions() {
		stopAction = new Action("Stop Session") {
			public void run() { 
				stopSession();
			}
		};
//		stopAction.setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_STOP)));
		stopAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_STOP_ACTION_ICON"));

		recalcuateMenuAction = new Action("Recalculate Menu") {
			public void run() {
				recalculateMenu();
			}
		};
//		recalcuateMenuAction.setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_HOME_NAV)));
		recalcuateMenuAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_RECALCULATE_MENU_ACTION_ICON"));

		
		refreshAction = new Action("Refresh") {
			public void run() {
				refresh();
			}
		};
//		refreshAction.setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_SYNCED)));
		refreshAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_REFRESH_ACTION_ICON"));

		stopFetchingElementsAction = new Action("Cancel updating") {
			public void run() {
				modelAPI.stopRetrieve();
			}
		};
//		stopFetchingElementsAction.setImageDescriptor(ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_STOP)));
		stopFetchingElementsAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_STOP_FETCHING_ACTION_ICON"));
		
		servicesViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				updateActionEnablement();
			}
		});
		
	}
	
	/**
	 * Hook global actions
	 */
	private void hookGlobalActions() {
		IActionBars bars = getViewSite().getActionBars();
		bars.setGlobalActionHandler(ActionFactory.REFRESH.getCommandId(), refreshAction);
	}
	
	/**
	 * Create menu.
	 */
	private void createMenu() {
		IMenuManager mgr = getViewSite().getActionBars().getMenuManager();
		mgr.add(refreshAction);
	}
	
	
	@SuppressWarnings("unchecked")
	protected void updateActionEnablement() {
		stopFetchingElementsAction.setEnabled(!modelAPI.ready());
		if (modelAPI.isUpdateEnabled()) {
			StructuredSelection selection = (StructuredSelection) servicesViewer.getSelection();
			Iterator iterator = selection.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof GOnSessionType) {
					stopAction.setEnabled(true);
					recalcuateMenuAction.setEnabled(true);
					return;
				}
			}
		}
		stopAction.setEnabled(false);
		recalcuateMenuAction.setEnabled(false);
		
		
	}
	

	@SuppressWarnings("unchecked")
	protected List<GOnSessionType> getSelectedSessions() {
		ArrayList<GOnSessionType> selectedSessions = new ArrayList<GOnSessionType>();
		StructuredSelection selection = (StructuredSelection) servicesViewer.getSelection();
		Iterator iterator = selection.iterator();
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (next instanceof GOnSessionType) {
				selectedSessions.add((GOnSessionType) next);
			}
		}
		return selectedSessions;
		
		
	}

	protected void recalculateMenu() {
		List<GOnSessionType> selectedSessions = getSelectedSessions();
		GIMultipleJobElementHandler<GOnSessionType> elementHandler = new GMultipleJobElementHandlerAdapter<GOnSessionType>() {

			@Override
			public String handleElement(GOnSessionType gOnSession) {
				boolean ok = modelAPI.recalculateMenuForSession(gOnSession.getServer_sid(), gOnSession.getUnique_session_id());
				if (!ok) {
					return "Unable to send recalculate menu request to session " + gOnSession.getUnique_session_id() + " on server " + gOnSession.getServer_sid();
				}
				return null;
			}

			@Override
			public void updateGUI() {
				setMessage("Menu recalculation requests sent");
			}

			@Override
			public String taskString(GOnSessionType element) {
				return element.getUser_login();
			}
			
			
			
		};
		GMultipleJob<GOnSessionType> job = new GMultipleJob<GOnSessionType>("Stop session", getSite().getShell(), selectedSessions, elementHandler);
		job.schedule();
		
	}

	protected void stopSession() {
		List<GOnSessionType> selectedSessions = getSelectedSessions();
		GIMultipleJobElementHandler<GOnSessionType> elementHandler = new GMultipleJobElementHandlerAdapter<GOnSessionType>() {

			@Override
			public String handleElement(GOnSessionType gOnSession) {
				boolean ok = modelAPI.stopSession(gOnSession.getServer_sid(), gOnSession.getUnique_session_id());
				if (!ok) {
					return "Unable to stop session " + gOnSession.getUnique_session_id() + " on server " + gOnSession.getServer_sid();
				}
				return null;
			}

			@Override
			public void updateGUI() {
				internalRefresh(true);
			}

			@Override
			public String taskString(GOnSessionType element) {
				return element.getUser_login();
			}
			
			
			
		};
		GMultipleJob<GOnSessionType> job = new GMultipleJob<GOnSessionType>("Stop session", getSite().getShell(), selectedSessions, elementHandler);
		job.schedule();
		
		
	}
	
	private void createToolbar() {
		statusForm.getToolBarManager().add(stopAction);
		statusForm.getToolBarManager().add(recalcuateMenuAction);
		statusForm.getToolBarManager().add(refreshAction);
		statusForm.getToolBarManager().add(stopFetchingElementsAction);
		statusForm.updateToolBar();
	}

	/**
	 * Create context menu.
	 */
	private void createContextMenu() {
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});
		
		// Create menu.
		Menu menu = menuMgr.createContextMenu(servicesViewer.getControl());
		servicesViewer.getControl().setMenu(menu);
		
		// Register menu for extension.
		getSite().registerContextMenu(menuMgr, servicesViewer);
	}
	
	private void fillContextMenu(IMenuManager mgr) {
		mgr.add(stopAction);
		mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		mgr.add(recalcuateMenuAction);
		mgr.add(new Separator());
		mgr.add(refreshAction);
	}
	


	@Override
	public void dispose() {
		super.dispose();
		if (autoUpdateThread!=null)
			autoUpdateThread.stopThread();
		saveUserSettings();
	}

	private void saveUserSettings() {
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_SESSION_AUTO_UPDATE, autoUpdate);		
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_SESSION_VIEW_CHOICE, viewChoice);		
	}

	private void loadUserSettings() {
		autoUpdate = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.G_SESSION_AUTO_UPDATE);
		viewChoice = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_SESSION_VIEW_CHOICE);
	}
	
	


	private void createStatusView(final Composite parent) {
		
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		statusForm = toolkit.createForm(parent);
		statusForm.setText("Gateway Sessions");
		Font verdanaFont = new Font(parent.getDisplay(), "Verdana", 12, SWT.NORMAL);
		statusForm.setFont(verdanaFont);
		statusForm.updateToolBar();
		toolkit.decorateFormHeading(statusForm);
		GridLayout layout = new GridLayout(); 
		statusForm.getBody().setLayout(layout);
		
		loadUserSettings();
		
		Composite buttonsContainer = toolkit.createComposite(statusForm.getBody(), SWT.None);
		GridLayout controlLayout = new GridLayout(3, false);
		buttonsContainer.setLayout(controlLayout);
		GridData controlLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		controlLayoutData.grabExcessHorizontalSpace = true;
		controlLayoutData.horizontalIndent = 10;
		buttonsContainer.setLayoutData(controlLayoutData);

		autoUpdateButton = new Button(buttonsContainer, SWT.CHECK);
		autoUpdateButton.setText("Automatic update");
		autoUpdateButton.setSelection(autoUpdate);
		autoUpdateButton.setLayoutData(new GridData(SWT.CENTER));
		autoUpdateButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				setAutoUpdate(autoUpdateButton.getSelection());
			}
			
		});
		
		Composite showButtonContainer = toolkit.createComposite(buttonsContainer, SWT.None);
		showButtonContainer.setLayout(new GridLayout(2, false));
		showButtonContainer.setLayoutData(new GridData());
		showButtonContainer.setToolTipText("Yo");
		
		final Label servicePackLabel = new Label(showButtonContainer, SWT.NONE);
		servicePackLabel.setText("Show : ");
		
		showCombo = new Combo(showButtonContainer, SWT.DROP_DOWN);
		showCombo.setItems(showChoices);
		showCombo.setText(viewChoice);
		showCombo.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				setViewChoice(showCombo.getText());
			}
			
		});
//		showCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		

//		new Label(buttonsContainer, SWT.NONE).setText(" | ");
		
		
		Composite searchContainer = toolkit.createComposite(buttonsContainer, SWT.RIGHT);
		searchContainer.setLayout(new GridLayout(3, false));
		searchContainer.setLayoutData(new GridData(GridData.END));

		Label label = new Label(searchContainer, SWT.NONE);
		label.setText("User Search : ");
		searchField = new Text(searchContainer, SWT.BORDER);
		GridData inputStringFieldGridData = new GridData();
		inputStringFieldGridData.widthHint = 200;
		searchField.setLayoutData(inputStringFieldGridData);
		Button searchButton = new Button(searchContainer, SWT.PUSH);
		searchButton.setText("Search");
		SelectionListener searchSelectionListener = new SelectionListener() {
			
			private void selected(SelectionEvent e) {
				modelAPI.setSearchFilter(searchField.getText());
				refresh();
				
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				selected(e);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				selected(e);
			}
			
		};
		searchButton.addSelectionListener(searchSelectionListener);
		searchField.addSelectionListener(searchSelectionListener);
		

		/** Create a section for the services table. */
		servicesSection = toolkit.createSection(statusForm.getBody(), Section.TITLE_BAR);
//		servicesSection.setText("Gateway Sessions");

		label.setBackground(servicesSection.getBackground());
		servicePackLabel.setBackground(servicesSection.getBackground());
		autoUpdateButton.setBackground(servicesSection.getBackground());
		
		/** Create a layout for this section. */
		///GridData servicesSectionGridData = new GridData(GridData.FILL_HORIZONTAL);
		//servicesSectionGridData.grabExcessHorizontalSpace = true;
		servicesSection.setLayout(new GridLayout());
		//servicesSection.setLayoutData(servicesSectionGridData);
		servicesSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
//		servicesSection.setLayoutData(new GridData(GridData.BEGINNING));
		
		Composite servicesContainer = toolkit.createComposite(servicesSection, SWT.None);
		servicesSection.setClient(servicesContainer);
		servicesContainer.setLayout(new GridLayout());
		servicesContainer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));


		
		/** Add some description for this section. */
//		Label label3 = toolkit.createLabel(servicesContainer, "", SWT.WRAP);
//		label3.setText("All services listed below should be running for full functionality of the G/On " +
//				"server. Please use the Microsoft Services tool to start any services that is not listed " +
//				"as running.");
//		label3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		
		/** Create a table with info on services. */
		servicesViewer = new TableViewer(servicesContainer, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		createColumns(servicesViewer);
		provider = new ViewContentProvider();
		servicesViewer.setContentProvider(provider);
		//servicesViewer.setContentProvider(new ViewContentProvider());
		servicesViewer.setLabelProvider(new ViewLabelProvider());
		servicesViewer.setInput(provider);
		
		servicesViewer.getTable().setLayout(new GridLayout());
		GridData ruleTableViewGridData = new GridData(GridData.FILL_BOTH);
		ruleTableViewGridData.grabExcessVerticalSpace = true;
		servicesViewer.getTable().setLayoutData(ruleTableViewGridData);
		
		comparator = new MyViewerComparator();
		servicesViewer.setComparator(comparator);

		
		createActions();
		createMenu();
		createToolbar();
		createContextMenu();
		hookGlobalActions();
		updateActionEnablement();
		
		getViewSite().getPage().addSelectionListener(this);
		
		retreiver = new ElementListingRetriever(this, modelAPI, "Gateway Session");
		retreiver.get(null);
		
		setAutoUpdate(autoUpdate);
		

	}

	protected void setViewChoice(String text) {
		boolean change = !text.equals(viewChoice);
		viewChoice = text;
		if (change) {
			refresh();
		}
		
	}



	protected void setAutoUpdate(boolean value) {
		autoUpdate = value;
		if (autoUpdateThread!=null) {
			autoUpdateThread.stopThread();
		}
		if (autoUpdate) {
			autoUpdateThread = new RefreshThread(refreshSeconds);
			autoUpdateThread.start();
		}
	}
	

	@Override
	public void createView(Composite parent) {
		modelAPI = GModelAPIFactory.getModelAPI().getGatewaySessionConfig();
		createStatusView(parent);
		refreshData(false);
		
		
	}

	@Override
	public void setViewFocus() {
		if (servicesViewer!=null) {
			servicesViewer.getControl().setFocus();
			
			
		}
		
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			boolean GOnServiceTypeFound = false;
			ArrayList<String> serverSids = new ArrayList<String>();   
			for (Object object : ((IStructuredSelection)selection).toList()) {
				if (object instanceof GOnServiceType) {
					serverSids.add(((GOnServiceType) object).getSid());
					GOnServiceTypeFound = true;
				}
			}
			if (GOnServiceTypeFound) {
				HashSet<String> s = new HashSet<String>(serverSids);
				if (!s.equals(server_sids)) {
					server_sids = s;
					if (viewChoice.equals(VIEW_SERVERS))
						refresh();
					
				}
			}
		}
		
	}
	



	private void refreshData(boolean background) {
		if (viewChoice.equals(VIEW_SERVERS)) {
			String[] tmpArray = new String[server_sids.size()];
			modelAPI.setServerSids(server_sids.toArray(tmpArray));
		}
		else {
			modelAPI.setServerSids(null);
			
		}
		modelAPI.refreshData(background);
		updateActionEnablement();
		
	}
	
	private void internalRefresh(boolean background) {
		refreshData(background);
		retreiver.get(null);
	}

	private void refresh() {
		shownFirst  = false;
		internalRefresh(false);
		
	}
	
	private void updateTable() {
		if (!servicesViewer.getTable().isDisposed()) {
			int[] indices = servicesViewer.getTable().getSelectionIndices();
			String tmp ="";
			for(int i=0; i<indices.length; i++)
				tmp += indices[i];
	//		MessageDialog.openInformation(null, "yo", tmp);
			servicesViewer.refresh();
			servicesViewer.getTable().setSelection(indices);
			updateActionEnablement();
		}
	}

	@Override
	public void setErrorMessage(String msg) {
		IStatusLineManager statusLineManager = getViewSite().getActionBars().getStatusLineManager();
		statusLineManager.setErrorMessage(msg);
		
		
	}

	@Override
	public void setMessage(String msg) {
		IStatusLineManager statusLineManager = getViewSite().getActionBars().getStatusLineManager();
		statusLineManager.setMessage(msg);
	}

	@Override
	public void setSelection(String selectedElementId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show(boolean done) {
		if (!shownFirst || done) {
			shownFirst = true;
			updateTable();
		}
		if (!servicesSection.isDisposed()) {
//			servicesSection.setText("Gateway Sessions" + " (Showing " + servicesViewer.getTable().getItemCount() + " of " + modelAPI.getElementCount() + ")");
			servicesSection.setText("Showing " + servicesViewer.getTable().getItemCount() + " of " + modelAPI.getElementCount());
			servicesSection.layout();
		}
		
	}
	
	
	private SelectionAdapter getSelectionAdapter(final TableColumn column, final int index) {
		SelectionAdapter selectionAdapter = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				comparator.setColumn(index);
				int dir = servicesViewer.getTable().getSortDirection();
				if (servicesViewer.getTable().getSortColumn() == column) {
					dir = dir == SWT.UP ? SWT.DOWN : SWT.UP;
				} else {

					dir = SWT.DOWN;
				}
				servicesViewer.getTable().setSortDirection(dir);
				servicesViewer.getTable().setSortColumn(column);
				servicesViewer.refresh();
			}
		};
		return selectionAdapter;
	}

	
	class MyViewerComparator extends ViewerComparator {
		private int propertyIndex;
		private static final int DESCENDING = 1;
		private int direction = DESCENDING;

		public MyViewerComparator() {
			this.propertyIndex = 0;
			direction = DESCENDING;
		}

		public void setColumn(int column) {
			if (column == this.propertyIndex) {
				// Same column as last sort; toggle the direction
				direction = 1 - direction;
			} else {
				// New column; do an ascending sort
				this.propertyIndex = column;
				direction = DESCENDING;
			}
		}
		
		private int compareStrings(String s1, String s2) {
			if (s1==null)
				s1 = "";
			if (s2==null)
				s2 = "";
			return s1.compareToIgnoreCase(s2);
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			if (e1 instanceof GOnSessionType && e2 instanceof GOnSessionType) {
				GOnSessionType s1 = (GOnSessionType) e1;
				GOnSessionType s2 = (GOnSessionType) e2;
				int rc = 0;
				switch (propertyIndex) {
				case 0:
					rc = compareStrings(s1.getUser_login(), s2.getUser_login());
					break;
				case 1:
					rc = compareStrings(s1.getUser_name(), s2.getUser_name());
					break;
				case 2:
					rc = compareStrings(s1.getServer_title(), s2.getServer_title());
					break;
				case 3:
					rc = compareStrings(s1.getClient_ip(), s2.getClient_ip());
					break;
				case 4:
					try {
						Date sessionStart1 = CommonUtils.DateTimeConverter.string2datetime(s1.getSession_start());
						Date sessionStart2 = CommonUtils.DateTimeConverter.string2datetime(s2.getSession_start());
						rc = sessionStart1.compareTo(sessionStart2);
					} catch (ParseException e) {
						rc = compareStrings(s1.getSession_start(), s1.getSession_start());
					}
					break;
					
				default:
					rc = 0;
				}
				if (direction == DESCENDING) {
					rc = -rc;
				}
				return rc;
				
			}
			return 0;
		}
	}
	
	
	class RefreshThread extends ThreadWithStopFlag {

		private int waitSeconds;

		RefreshThread(int waitSeconds) {
			this.waitSeconds = waitSeconds;
		}
		
		@Override
		public void run() {
			while (true) {
				if (this.stopThread)
					break;
				synchronized(this) {
					try {
						wait(waitSeconds*1000);
						Display display = GGatewayUsersView.this.getSite().getShell().getDisplay();
						display.asyncExec(new Runnable() {
						
							@Override
							public void run() {
								if (!servicesViewer.getTable().isDisposed())
									if (autoUpdate && modelAPI.ready()) {
										try {
											internalRefresh(true);
										} catch (Throwable t) {
											//ignore
										}
										
									}
							}
							
						});
					} catch (Throwable e) {
					}
						
				}
				
			}
		}
		
	}
	
}
