package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class ManagementAccessPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.ADMIN_ACCESS_TYPE;
		String [] defaultElements = {GGlobalDefinitions.USER_TYPE, GGlobalDefinitions.USER_GROUP_TYPE, 
				GGlobalDefinitions.ADMIN_ACCESS_TYPE};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  null, 
												  ruleViewId, 
												  defaultElements);

	}

}
