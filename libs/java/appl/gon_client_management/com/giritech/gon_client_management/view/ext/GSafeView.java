package gon_client_management.view.ext;

import gon_client_management.ext.GILogger;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;

public abstract class GSafeView extends ViewPart {

	private Throwable initializationFailure = null;
	public Throwable getInitializationFailure() {
		return initializationFailure;
	}

	private boolean createPartControlCalled = false;
	private GILogger logger;
	private String contextId;
	
	public GSafeView(GILogger logger, String contextId) {
		this.logger = logger;
		this.contextId = contextId;
	}

	@Override
	final public void createPartControl(final Composite parent) {
		/* Check that this hasn't been called before to avoid infinite loop if implementation class call this from "createView" */
		if (createPartControlCalled) {
			logger.log(IStatus.ERROR, "createPartControl called more than once for " + this.toString());
			return;
		}
		createPartControlCalled = true;
		if (initializationFailure!=null)
			createErrorView(parent, initializationFailure);
		else {
			try {
				createView(parent);
			}
			catch(final Throwable t) {
				logger.logException(t);
				parent.getDisplay().asyncExec(new Runnable() {

					public void run() {
						MessageDialog.openError(parent.getShell(), getErrorHeader(), getErrorMessage(t));
						
					}
					
				});
			}
		}

	}
	
	

	@Override
	final public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		try {
			initView(site);
			loadState(memento);
		}
		catch(Throwable t) {
			setInitializationFailure(t);
		}
	}






	protected void loadState(IMemento memento) {
		
	}

	@Override
	final public void init(IViewSite site) throws PartInitException {
		super.init(site);
		try {
			initView(site);
		}
		catch(Throwable t) {
			setInitializationFailure(t);
		}
	}



	private void createErrorView(Composite parent, Throwable t) {
		
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form ruleHandlingForm = toolkit.createForm(parent);
		
		/* Create layout for this form.  */
		GridLayout formGridLayout = new GridLayout(); 
		formGridLayout.numColumns = 2;
		ruleHandlingForm.getBody().setLayout(formGridLayout);
		GridData formLayoutGridData = new GridData(GridData.FILL_VERTICAL);
		formLayoutGridData.grabExcessVerticalSpace = true;
		ruleHandlingForm.getBody().setLayoutData(formLayoutGridData);
		
		Section ruleViewSection = toolkit.createSection(ruleHandlingForm.getBody(), Section.NO_TITLE);
		ruleViewSection.setLayout(new GridLayout());
		GridData ruleViewSectionGridData = new GridData(GridData.FILL_BOTH);
		ruleViewSectionGridData.grabExcessVerticalSpace = true;
		ruleViewSection.setLayoutData(ruleViewSectionGridData);
		
		Composite ruleViewContainer = toolkit.createComposite(ruleViewSection);
		ruleViewSection.setClient(ruleViewContainer);
		GridLayout ruleViewContainerGridLayout = new GridLayout();
		ruleViewContainerGridLayout.numColumns = 2;
		ruleViewContainer.setLayout(ruleViewContainerGridLayout);
		GridData ruleViewContainerGridData = new GridData(GridData.FILL_BOTH);
		ruleViewContainerGridData.grabExcessVerticalSpace = true;
		ruleViewContainer.setLayoutData(ruleViewContainerGridData);
		
		Composite master = ruleViewContainer;
		Label label = toolkit.createLabel(master, getErrorHeader(), SWT.NONE);
		label.setForeground(master.getDisplay().getSystemColor(SWT.COLOR_RED));
		GridData gridData = new GridData(GridData.BEGINNING);
		gridData.verticalAlignment = SWT.BEGINNING;
		label.setLayoutData(gridData);
		
		StyledText inputTextField = new StyledText(master, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP  | SWT.READ_ONLY);
		toolkit.adapt(inputTextField, true, true);
		GridData inputTextFieldGridData = new GridData(GridData.FILL_BOTH);
		inputTextFieldGridData.heightHint = 20;
		inputTextFieldGridData.widthHint = 250;
		inputTextField.setLayoutData(inputTextFieldGridData);
		inputTextField.setForeground(master.getDisplay().getSystemColor(SWT.COLOR_RED));
		inputTextField.setText(getErrorMessage(t));
		
		
		
		
	}
	
	private String getErrorMessage(Throwable t) {
		String message = t.getLocalizedMessage();
		if (message==null)
			return t.toString();
		return message;
	}
	
	private String getErrorHeader() {
		return "Error loading view"; 
	}

	@Override
	final public void setFocus() {
		try {
			if (contextId!=null) {
				IContextService contextService = (IContextService) getSite().getService(IContextService.class);
				contextService.activateContext(contextId);
			}
			setViewFocus();
		}
		catch(Throwable t) {
			// ignore
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart#setInitializationData(org.eclipse.core.runtime.IConfigurationElement, java.lang.String, java.lang.Object)
	 */
	@Override
	final public void setInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		super.setInitializationData(cfig, propertyName, data);
		try {
			setViewInitializationData(cfig, propertyName, data);
		}
		catch(Throwable t) {
			setInitializationFailure(t);
		}
	}


	/**
	 * @param initializationFailure the initializationFailure to set
	 */
	protected void setInitializationFailure(Throwable initializationFailure) {
		logger.logException(initializationFailure);
		this.initializationFailure = initializationFailure;
	}
	
	
	 public abstract void createView(Composite parent); 

	 public abstract void setViewFocus(); 

	 public void setViewInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		 // To be overridden for setInitializationData calls
	 }

	 public void initView(IViewSite site) {
		 // To be overridden for Init calls
	 }
	 
}
