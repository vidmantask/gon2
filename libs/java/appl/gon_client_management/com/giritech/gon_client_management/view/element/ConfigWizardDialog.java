/**
 * 
 */
package gon_client_management.view.element;

import gon_client_management.Activator;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

public class ConfigWizardDialog extends WizardDialog {

	private IDialogSettings dialogSettings = null;


	public ConfigWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
	}
	
	public ConfigWizardDialog(Shell parentShell, IWizard newWizard, IDialogSettings dialogSettings) {
		super(parentShell, newWizard);
		this.dialogSettings  = dialogSettings;
	}
	

	@Override
	protected IDialogSettings getDialogBoundsSettings() {
		if (dialogSettings!=null) {
			return dialogSettings;
		}
		else {
			return Activator.getDefault().getDialogSettings();
		}
	}
}