package gon_client_management.view.rule.editor;

import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;

import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerDropAdapter;
	
/**
 * Supports dropping elements into a rule viewer.
 */
public class RuleEditorDropAdapter extends ViewerDropAdapter {

	public RuleEditor editor = null;
	
	/**
	 * Setup drop possibilities for elements into a rule editor.
	 * @param editor
	 */
	public RuleEditorDropAdapter(RuleEditor editor) {
		super(editor.viewer);
		this.editor = editor;
	}

	@Override
	public void drop(DropTargetEvent event) {
		if (event.data instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) event.data).getFirstElement();
			if (element instanceof GIElement) {
				editor.contentProvider.addElementToRule((GIElement) element);
				getViewer().refresh();
				GIRule rule = editor.contentProvider.getRule();
				editor.updateButtonEnabling(rule);
			}
		}
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean validateDrop(Object target, int op, TransferData type) {
		return LocalSelectionTransfer.getTransfer().isSupportedType(type);
	}

	@Override
	public boolean performDrop(Object data) { return false;	} /* Not used */

}	
