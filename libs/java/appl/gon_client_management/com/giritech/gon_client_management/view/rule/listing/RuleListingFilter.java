package gon_client_management.view.rule.listing;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.themes.IThemeManager;

	/**
	 * Class for filtering table content.
	 * 
	 * @author Giritech, cso
	 *
	 */
public class RuleListingFilter {

	public RuleListing listing = null;
	public Text filterTextInput;
	private RuleFilter ruleTextFilter = null;
	protected long eventCount = 0;

	public RuleListingFilter(RuleListing listing) {

		this.listing = listing;
	
		/* Create a filter for rule viewing. */
		this.filterTextInput = listing.parent.toolkit.createText(listing.ruleViewContainer, "", SWT.SEARCH | SWT.ICON_SEARCH);
		filterTextInput.setMessage("Search                          ");
		filterTextInput.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		filterTextInput.addListener(SWT.KeyUp, filterListener);
		GridData filterInputGridData = new GridData();
		filterInputGridData.grabExcessHorizontalSpace = true;
		filterInputGridData.horizontalAlignment = GridData.END;
		filterTextInput.setLayoutData(filterInputGridData);
		Button filterClearButton = listing.parent.toolkit.createButton(listing.ruleViewContainer, "Clear", SWT.PUSH);
		filterClearButton.addListener(SWT.Selection, filterClearListener);

		/* Create the filter and connect it to a text field. */
		this.ruleTextFilter = new RuleFilter(filterTextInput);

		/* Theme. */
		if (!listing.parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager().getCurrentTheme().getId().equals("org.eclipse.ui.defaultTheme")) {
			IThemeManager themeManager = listing.parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager();
			filterTextInput.setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.filter.background_color")); 
		}
	}
	
	/**
	 * Get the filter so that it can be connected to a view.
	 * @return an element filter
	 */
	public RuleFilter getFilter() {
		return this.ruleTextFilter;
	}
	
	/**
	 * A listener for input in the filter text field.
	 * Refreshing the view activates filters and sorting.
	 */
	public Listener filterListener = new Listener() {

		String lastText = "";
		
		public void handleEvent(Event event) {
			if (!lastText.equals(filterTextInput.getText())) {
				eventCount++;
				startPostponedFilter(eventCount);
			}
			lastText = filterTextInput.getText();
		}
	};

	/**
	 * A listener for clearing the filter.
	 */
	public Listener filterClearListener = new Listener() {
		public void handleEvent(Event event) {
			clearFilter();
		}
	};
	
	/**
	 * Clear the filter.
	 */
	public void clearFilter() {
		filterTextInput.setText("");
		refreshFilter(eventCount);
	}

	protected void startPostponedFilter(final long eventCount2) {
		Thread t = new Thread() {
			 public void run() {
				try {
			        Thread.sleep(1000);
			      } catch (InterruptedException e) {
			     }
			      Display display = Display.getCurrent();
			      //may be null if outside the UI thread
			      if (display == null)
			         display = Display.getDefault();
			     display.asyncExec(new Runnable() {
			          public void run() {
					     refreshFilter(eventCount2);
			          }
			      });			      
				 
			 }
		};
		t.start();
     }

	public void refreshFilter(long eventCount2) {
		if (eventCount2==eventCount) {
			listing.show(true);
		}
	}
	
}
