package gon_client_management.view.administrator;


import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class GLicenseManagementPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		layout.setFixed(true);
		layout.addStandaloneView("gon_client_management.LicenseView",  false, IPageLayout.LEFT, 0.4f, editorArea);
		
		IFolderLayout paletteLayout = layout.createFolder("paletteLayoutLicense", IPageLayout.RIGHT, 0.60f, editorArea);
		paletteLayout.addView("gon_client_management.LicensedUserView");
		paletteLayout.addView("gon_client_management.KeyView");
		paletteLayout.addView("gon_client_management.LicensedProgramView");
		
//		layout.addStandaloneView("gon_client_management.LicensedUserView",  false, IPageLayout.LEFT, 0.3f, editorArea);
//		layout.addStandaloneView("gon_client_management.KeyView",  false, IPageLayout.LEFT, 0.3f, editorArea);
//		layout.addStandaloneView("gon_client_management.ProgramView",  false, IPageLayout.RIGHT, 0.2f, editorArea);

	}

}
