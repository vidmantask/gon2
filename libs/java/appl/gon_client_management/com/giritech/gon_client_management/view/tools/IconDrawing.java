package gon_client_management.view.tools;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.OwnerDrawLabelProvider;
import org.eclipse.swt.graphics.Image;
//import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class IconDrawing extends OwnerDrawLabelProvider {

	private static final String IMPLIES_BUTTON = "IMPLIES_BUTTON";
	private static final String EDIT_BUTTON = "EDIT_BUTTON";
	private static final String DELETE_BUTTON = "DELETE_BUTTON";
	
	private Image image = null;
	
	//public int HEIGHT = 40;
	public int WIDTH = 40;
	
	/**
	 * Creates an ownerdrawn icon for inserting 
	 * into tableviewer cells. This way we can 
	 * fully control the position of the icon 
	 * (ie. center it vertically and horizontally).
	 * 
	 * Button types are:
	 * 	implies, edit, delete
	 * 
	 * @author Giritech, cso
	 *
	 */
	public IconDrawing(String icontype) {
		
		if (icontype.equals("implies")) {
			if (JFaceResources.getImageRegistry().getDescriptor(IMPLIES_BUTTON) == null)
				JFaceResources.getImageRegistry().put(IMPLIES_BUTTON, 
						PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_FORWARD)); 
			this.image = JFaceResources.getImageRegistry().getDescriptor(IMPLIES_BUTTON).createImage();
		}
		else if (icontype.equals("edit")) {
			if (JFaceResources.getImageRegistry().getDescriptor(EDIT_BUTTON) == null)
				JFaceResources.getImageRegistry().put(EDIT_BUTTON, 
						PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_PASTE));
			this.image = JFaceResources.getImageRegistry().getDescriptor(EDIT_BUTTON).createImage();
		}
		else if (icontype.equals("delete")) {
			if (JFaceResources.getImageRegistry().getDescriptor(DELETE_BUTTON) == null)
				JFaceResources.getImageRegistry().put(DELETE_BUTTON, 
						PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_TOOL_DELETE));
			this.image = JFaceResources.getImageRegistry().getDescriptor(DELETE_BUTTON).createImage();
		}
		else {
			System.out.println("IconDrawing needs an icontype: [implies|edit|delete]");
		}
		
	}

//	@Override
//	protected void measure(Event event, Object obj) {
//		event.setBounds(new Rectangle(event.x, event.y, WIDTH, HEIGHT));
//	}

	
	
	@Override
	protected void paint(Event event, Object element) {
//		int xPos = event.x + (WIDTH/2) - image.getBounds().width/2;  
//		int yPos = event.y + (HEIGHT/2) - image.getBounds().height/2;
//		event.gc.drawImage(this.image, xPos, yPos);
		event.gc.drawImage(this.image, event.x +2, event.y + 2);
	}

	@Override
	protected void measure(Event event, Object element) {
		// TODO Auto-generated method stub
		
	}

}
