package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.GIElementPane;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIElement;
import gon_client_management.provider.GLabelProvider;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.ext.ConfigWizard;
import gon_client_management.view.ext.GListChooser;

import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ProgramView extends WizardEditorElementView {

	
	public ProgramView() {
		super();
	}

	@Override
	protected GIConfig createConfig() {
		return GModelAPIFactory.getModelAPI().getConfig(GGlobalDefinitions.LAUNCH_SPEC_TYPE);
	}

	


	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.element.view_default.enabled");
		if (createEnabled())
			contextService.activateContext("com.giritech.element.launch_spec");
	}


	public void openDefaultEditor(GIElement element) {
		GIConfigPane configPane = myConfig.getDefaultConfigPane(element);
		ConfigWizard configWizard = new ConfigWizard(configPane, Activator.getLogger(), getDialogSettings(), true);
		final WizardDialog dialog = new ConfigWizardDialog(getViewSite().getShell(), configWizard, getDialogSettings());
		dialog.open();
	}
	
	protected Action showElementEditorAction = new Action() {

		@Override
		public String getText() {
			if (myConfig.isEditEnabled()) return "Add a new " + modelAPI.getColumnLabel().toLowerCase();
			else return "";
		}

		@Override
		public boolean isEnabled() {
			return createEnabled();
		}

		@Override
		public String getId() {
			return "ADD_ELEMENT";
		}

		@Override
		public void run() {
			IWizard wizard = new ConfigWizardChooser(myConfig);
			
			// Instantiates the wizard container with the wizard and opens it
			WizardDialog dialog = new ConfigWizardDialog(getViewSite().getShell(), wizard, getDialogSettings());
		    dialog.create();
		    dialog.open();
		}

		@Override
		public ImageDescriptor getImageDescriptor() {
			if (JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON") == null) {
				JFaceResources.getImageRegistry().put("ADD_BUTTON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_add.png").createImage());
			}
			return JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON");
		}
	};
	
	public Action getShowElementEditorAction() {
		return showElementEditorAction;
	}

	
	public void addZoneRestrictions(GIElement element) {
		final List<GIElement> existingRestrictions = myConfig.getRestrictions(element, "Zone");
		GIElementPane elementPane = GModelAPIFactory.getModelAPI()
				.getElementPane("Zone");
		elementPane.refreshData();
		while (!elementPane.ready()) {
			synchronized (this) {
				try {
					wait(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println(elementPane.getElementCount());
		GLabelProvider gLabelProvider = new GLabelProvider("Zone");
		GListChooser listChooserDialog = new GListChooser(getSite().getShell(),
														  gLabelProvider, 
														  elementPane.getElements(),
														  existingRestrictions);
		int dialogExitCode = listChooserDialog.open();
		if (dialogExitCode==Window.OK) {
			List<GIElement> chosenElements = listChooserDialog.getChosenElements();
			myConfig.updateRestrictions(element, "Zone", chosenElements);
			GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated(element.getEntityType(), element.getElementId());			
		}
		

	}	
	
	
	
}




