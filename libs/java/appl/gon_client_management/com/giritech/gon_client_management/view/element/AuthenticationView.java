package gon_client_management.view.element;

import org.eclipse.ui.contexts.IContextService;

public class AuthenticationView extends ElementView {

	public AuthenticationView() {
		super();
	}

	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.element.authentication_strength");
		
	}
}
