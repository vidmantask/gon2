package gon_client_management.view.rule.listing;

import gon_client_management.model.ext.GIElement;

import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.TransferData;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerDropAdapter;

/**
 * Supports dropping elements into a rule listing viewer.
 */
public class RuleListingDropAdapter extends ViewerDropAdapter { 

	public RuleListing listing = null;
	
	public RuleListingDropAdapter(RuleListing listing) {
		super(listing.checkBoxRuleTableView);
		this.listing = listing; 
	}

	@Override
	public void drop(DropTargetEvent event) {
		if (event.data instanceof IStructuredSelection) {
			Object element = ((IStructuredSelection) event.data).getFirstElement();
			if (element instanceof GIElement) {
				listing.parent.startEditing((GIElement) element);
			}
		}
	}

	/**
	 * Method declared on ViewerDropAdapter
	 */
	public boolean validateDrop(Object target, int op, TransferData type) {
		return LocalSelectionTransfer.getTransfer().isSupportedType(type);
	}

	@Override
	public boolean performDrop(Object data) { return false;	} /* Not used */

}	
