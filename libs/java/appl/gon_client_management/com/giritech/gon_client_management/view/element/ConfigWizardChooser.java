package gon_client_management.view.element;

import gon_client_management.model.ext.GIConfig;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

public class ConfigWizardChooser extends Wizard {

	private GIConfig myConfig;

	public ConfigWizardChooser(GIConfig myConfig) {
		setForcePreviousAndNextButtons(true);
		
		this.myConfig = myConfig;
	}

	@Override
	public void addPages() {
		IWizardPage launchWizardPage = new ConfigSelectionPage("", myConfig);
		addPage(launchWizardPage);
	}
	
	@Override
	public boolean performFinish() {
		// TODO Auto-generated method stub
		return true;
	}

}
