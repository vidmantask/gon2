package gon_client_management.view.ext;

import gon_client_management.model.ext.GIJob;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.widgets.Composite;

public interface GIConfigPaneContainer extends GIMessageHandler {

	void fieldJobFinished(Composite parent, GIJob job, int fieldIndex);

	void runFieldJob(GJobHandler runnable);
	
	boolean isReadOnly();
	
	

	
}
