package gon_client_management.view.report;

import gon_client_management.model.GIReportPane;
import gon_client_management.model.GModelAPIFactory;

/**
 * Views are created in MANIFEST.MF as extensions, and
 * added in a perspective. The UserView displays the 
 * G elements from the user element model. 
 * 
 * @author Giritech
 *
 */
public class ReportView extends ReportElementView {

	public ReportView() {
	}	
	
	@Override
	protected GIReportPane createModelAPI() {
		return GModelAPIFactory.getModelAPI().getReportPane();
	}
	

	@Override
	public void setViewFocus() {
		super.setViewFocus();
	}
	
}


