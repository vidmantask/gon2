package gon_client_management.view.preferences;

/**
 * Constant definitions for plug-in preferences
 * 
 * EXAMPLES:
 * public static final String P_PATH = "pathPreference";
 *	public static final String P_BOOLEAN = "booleanPreference";
 *	public static final String P_CHOICE = "choicePreference";
 *	public static final String P_STRING = "stringPreference";
 *
 */
public class PreferenceConstants {
	
	public static final String G_MANAGEMENT_SERVER_HOST = "stringPreference1";
	public static final String G_MANAGEMENT_SERVER_PORT = "stringPreference2";
	public static final String G_MANAGEMENT_SERVICE_SERVER_HOST = "stringPreference3";
	public static final String G_MANAGEMENT_SERVICE_SERVER_PORT = "stringPreference4";
	public static final String G_MANAGEMENT_SERVICE_EXECUTABLE = "stringPreference5";
	
	public static final String G_LAST_USER_NAME = "gon_user_name";
	public static final String G_SKIP_LOGIN = "gon_skip_login";
	
	public static final String G_SERVICE_AUTO_UPDATE = "gon_service_auto_update";
	public static final String G_SESSION_AUTO_UPDATE = "gon_session_auto_update";
	public static final String G_SESSION_VIEW_CHOICE = "gon_session_view_choice";
	
	public static final String G_USER_FILTER = "gon_user_filter";
	public static final String G_REGISTERED_USER_FILTER = "gon_registered_user_filter";
	public static final String G_ELEMENT_FILTER = "gon_element_filter";
	public static final String G_CSV_DELIMITER = "gon_csv_delimiter";
	
}
