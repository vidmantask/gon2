package gon_client_management.view.rule.listing;

import gon_client_management.model.GIRule;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

	/**
	 * class for sorting columns increasing or decreasing.
	 * 
	 * @author Giritech
	 *
	 */
public	class RuleListingSorter extends ViewerComparator {

	public int index;
	private int direction;

	public RuleListingSorter() {
		this.index = 0;
		this.direction = 1;
	}

	/**
	 * Compare labels for two elements.
	 * In case index is larger than the number of elements in a rule.  It 
	 * is assumed that sorting is done on the result column. 
	 */
	@Override
	public int compare(Viewer viewer, Object e1, Object e2) {
		GIRule t1 = (GIRule) e1;
		GIRule t2 = (GIRule) e2;

		/* In case of result column */
		if (index > t1.getRuleElementCount()) { 
			return super.compare(viewer, t1.getResultElement().getLabel().toLowerCase(), t2.getResultElement().getLabel().toLowerCase()) * direction;
		}
		/* In case of the rule activation column. */
		else if (index == t1.getRuleElementCount()) {
			return super.compare(viewer, t1.isActive(), t2.isActive()) * direction;
		}
		/* in case of element column */
		else { 
			return super.compare(viewer, t1.getElement(index).getLabel().toLowerCase(), t2.getElement(index).getLabel().toLowerCase()) * direction;
		}
	}

	/**
	 * Set the column index for sorting the table. 
	 * 
	 * @param index
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * Set the sort direction for the comparator.
	 * 
	 * @param direction
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}
}
