package gon_client_management.view.ext;

import gon_client_management.ext.GILogger;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizardContainer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

public class ConfigWizard extends Wizard {
	

	private final GIConfigPane configPane;
	private final boolean readOnly;
	private GILogger logger;

	public ConfigWizard(GIConfigPane configPane, GILogger logger, IDialogSettings dialogSettings) {
		this(configPane, logger, dialogSettings, configPane.isReadOnly());
	}

	public ConfigWizard(GIConfigPane configPane, GILogger logger, IDialogSettings dialogSettings, boolean readOnly) {
		this.configPane = configPane;
		this.readOnly = readOnly;
		this.logger = logger;
		setDialogSettings(dialogSettings);
		setWindowTitle(configPane.getTitle());
	}
	
	@Override
	public IWizardPage getStartingPage() {
		return super.getStartingPage();
	}


	

	@Override
	public void addPages() {
		List<GIConfigPanePage> configPages = configPane.getConfigPages();
		for (GIConfigPanePage configPanePage : configPages) {
			ConfigWizardPage launchWizardPage = new ConfigWizardPage(configPanePage, logger, getDialogSettings(), readOnly);
			addPage(launchWizardPage);
			
		} 
		//LaunchWizardPage launchWizardPage1 = new LaunchWizardPage("page2", "PageTitle2", null);
		//addPage(launchWizardPage1);
	}

	@Override
	public boolean performFinish() {
		IWizardContainer container = getContainer();
		if (!readOnly && container!=null) {
			IWizardPage currentPage = getContainer().getCurrentPage();
			for (IWizardPage page : getPages()) {
				if (page instanceof ConfigWizardPage) {
					((ConfigWizardPage) page).setValues();
				}
			}
			try {
				configPane.save();
			} catch (GOperationNotAllowedException e) {
				((ConfigWizardPage) currentPage).setErrorMessage("Save failed : " + e.getMessage());
				return false;
			}
			
		}
		return true;
			
	}

	@Override
	public boolean needsProgressMonitor() {
		return true;
	}

}
