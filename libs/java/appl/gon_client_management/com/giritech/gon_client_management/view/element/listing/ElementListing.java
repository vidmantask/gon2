package gon_client_management.view.element.listing;

//import gon_client_management.provider.ElementColumnLabelProvider;
import gon_client_management.model.ext.GIElement;
import gon_client_management.provider.GContentProvider;
import gon_client_management.provider.GLabelProvider;
import gon_client_management.view.element.DragListener;
import gon_client_management.view.element.ElementView;
import gon_client_management.view.ext.IElementEditor;
import gon_client_management.view.util.ElementListingRetriever;
import gon_client_management.view.util.GIElementListing;

import java.util.ArrayList;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.forms.widgets.Section;

public class ElementListing implements GIElementListing {

	public ElementView parent = null;
	private Section elementViewSection = null;
	public Composite elementViewContainer = null;
	public TableViewer elementViewer = null;
	private Label waitingForElementsLabel = null;
	private boolean showingwaitingForElementsLabel = false;

	public ElementListingFilter filter = null;
	private ElementSorter sorter = null;
	private ElementListingRetriever retreiver = null;

	final int editColumnIndex = 1;
	final int deleteColumnIndex = 2;
	
	private boolean shownFirst = false;
	public boolean limitResult = true;
	
	

	/**
	 * Add an element listing to the given view.
	 * 
	 * @param parentView in which to insert the listing
	 */
	public ElementListing(final ElementView parent) {

		this.parent = parent;
		
		/**
		 * Create a section for viewing existing elements. 
		 */
		this.elementViewSection = parent.toolkit.createSection(parent.elementHandlingForm.getBody(), Section.TITLE_BAR);
		elementViewSection.setText(parent.modelAPI.getViewHeadline());
		elementViewSection.setLayout(new GridLayout());
		GridData elementViewSectionGridData = new GridData(GridData.FILL_BOTH);
		elementViewSectionGridData.grabExcessVerticalSpace = true;
		elementViewSection.setLayoutData(elementViewSectionGridData);
		
		/* Create a container for the parts in this section. */
		this.elementViewContainer = parent.toolkit.createComposite(elementViewSection);
		elementViewSection.setClient(elementViewContainer);
		GridLayout elementViewContainerGridLayout = new GridLayout();
		elementViewContainerGridLayout.numColumns = 2;
		elementViewContainer.setLayout(elementViewContainerGridLayout);
		GridData elementViewContainerGridData = new GridData(GridData.FILL_BOTH);
		elementViewContainerGridData.grabExcessVerticalSpace = true;
		elementViewContainerGridData.grabExcessHorizontalSpace = true;
		elementViewContainer.setLayoutData(elementViewContainerGridData);
		
		// Create a filter placed on top of the listing.
		this.filter = new ElementListingFilter(this);
		
		// Create a label that shows that we are waiting for data from the server.
		waitingForElementsLabel = parent.toolkit.createLabel(elementViewContainer, "", SWT.NONE);
		waitingForElementsLabel.setText("Retrieving elements...");
		GridData test1 = new GridData();
		test1.grabExcessVerticalSpace = true;
		test1.verticalAlignment = SWT.CENTER;
		test1.horizontalAlignment = SWT.CENTER;
		waitingForElementsLabel.setLayoutData(test1);
		
		// Create the element view for displaying after elements are fetched.
		elementViewer = new TableViewer(elementViewContainer, SWT.VIRTUAL | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.NO_MERGE_PAINTS| SWT.BORDER | SWT.MULTI);
		elementViewer.getTable().setVisible(false);
		GridData elementViewerGridData = new GridData(GridData.FILL_BOTH);
		elementViewerGridData.horizontalSpan = 2;
		elementViewerGridData.grabExcessHorizontalSpace = true;
		elementViewerGridData.grabExcessVerticalSpace = true;
		elementViewerGridData.exclude = true;
		elementViewer.getTable().setLayoutData(elementViewerGridData);
		
		elementViewer.getTable().setHeaderVisible(true);
		TableViewerColumn elementColumn = new TableViewerColumn(elementViewer, SWT.NONE);
		elementColumn.getColumn().setText(parent.modelAPI.getColumnLabel());

		// Enable tool tip support for the individual elements.
		ColumnViewerToolTipSupport.enableFor(elementViewer);
		
		// Set how the elements are displayed in the view.
		//elementColumn.setLabelProvider(new ElementColumnLabelProvider(parent.modelAPI.getColumnType()));
		elementColumn.setLabelProvider(new GLabelProvider(parent.modelAPI.getColumnType()));		
		
		// Setup tab order
//		elementViewContainer.setTabList(new Control[] {filter.inputField, filter.clearButton, elementViewer.getTable()});
		
		// Setup some listeners for user actions.
		elementViewer.getTable().addListener(SWT.MouseUp, rowSelectedListener);
		elementViewer.addDoubleClickListener(elementDoubleClickListener);

		elementViewContainer.addControlListener(containerResizeListener);
		
		/* Setup context menu for the element listing. */
	    MenuManager menuMgr = new MenuManager();
		this.parent.getSite().registerContextMenu("rule_listing_popup", menuMgr, elementViewer);
		Control control = elementViewer.getControl();
		Menu menu = menuMgr.createContextMenu(control);
	    control.setMenu(menu);

		
		
		
		
		// Setup sorting via the column header.
		this.sorter = new ElementSorter();
		elementViewer.getTable().setSortColumn(elementViewer.getTable().getColumn(0));
		elementViewer.setComparator(sorter.getIncreasingSorter());
		elementColumn.getColumn().addListener(SWT.Selection, columnSortListener);

		// Setup filtering.
		elementViewer.addFilter(filter.getFilter());
		
		
		// Setup drag support.
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer()};
		int operations = DND.DROP_COPY | DND.DROP_MOVE;
		elementViewer.addDragSupport(operations, transfers, new DragListener(elementViewer)); 
	
		retreiver = new ElementListingRetriever(this, parent.modelAPI, parent.modelAPI.getColumnLabel().toLowerCase());
		retreiveElements(null);
		
		
	}
	
	private void setWaitingForElementsLabelVisible(boolean visible) {
		waitingForElementsLabel.setVisible(visible);
		showingwaitingForElementsLabel = visible;
	}
	
	/**
	 * Start the process of retrieving elements.
	 * When all elements are retrieved the show method is called.
	 */
	public void retreiveElements(String SelectedElementId) {
		shownFirst = false;
		
		/* Hide the table */
		GridData tmp = (GridData) elementViewer.getTable().getLayoutData();
		tmp.exclude = true;
		elementViewer.getTable().setVisible(false);
		/* Show the sign */
		GridData tmp2 = (GridData) waitingForElementsLabel.getLayoutData();
		tmp2.exclude = false;
		setWaitingForElementsLabelVisible(true);
		/* Retrieve elements */
		elementViewSection.layout();
		retreiver.get(SelectedElementId);
	}
	

	/**
	 * This is called when we want to show the freshly retrieved
	 * data from the model/server. So this removes the waiting sign 
	 * and displays the elements.
	 */
	public void show(boolean done) {
		/* Update content in the element table. */
		if (!waitingForElementsLabel.isDisposed() && showingwaitingForElementsLabel) {
			elementViewer.setContentProvider(new GContentProvider(parent.modelAPI));
//			elementViewer.setItemCount(29);
			elementViewer.setInput(parent.getViewSite());

			/* Hide the 'out for lunch' sign. */
			setWaitingForElementsLabelVisible(false);
			GridData tmp1 = (GridData) waitingForElementsLabel.getLayoutData();
			tmp1.exclude = true;
			/* Show the table */
			GridData tmp2 = (GridData) elementViewer.getTable().getLayoutData();
			tmp2.exclude = false;
			elementViewer.getTable().setVisible(true);
			elementViewer.refresh();

			elementViewSection.layout();
			for (int i=0; i<elementViewer.getTable().getColumnCount(); i++)
				elementViewer.getTable().getColumn(i).pack();
			layout();
		}
		
		updateResultText();
		
	}
	
	void updateResultText() {
		if (elementViewContainer.isDisposed())
			return;
		boolean done = parent.modelAPI.hasFetchedAll();
		if (!limitResult || (limitResult && !shownFirst) || done) {
			shownFirst = true;
			elementViewer.refresh();
		}
		if (parent.modelAPI.resultTruncated()) {
			elementViewSection.setText("Number of " + parent.modelAPI.getColumnLabelPlural() + " exceeds view limit (Showing " +
					(elementViewer.getTable().getItemCount()-1) + ")");
		}
		else {
			if (parent.modelAPI.searchAvailable()) {
				elementViewSection.setText(parent.modelAPI.getViewHeadline() + 
				" (Found " + elementViewer.getTable().getItemCount() + ")");
			}
			else {
				int itemCount = elementViewer.getTable().getItemCount();
				int modelItemCount = parent.modelAPI.getElementCount();
				elementViewSection.setText(parent.modelAPI.getViewHeadline() + 
						" (Showing " + itemCount + " of " + modelItemCount + ")");
			}
		}
	    parent.getStopFetchingElementsAction().setEnabled(!parent.modelAPI.ready());
		elementViewSection.layout(true);
		
	}
	
	/**
	 * Make sure that we get max. value for our real estate.
	 */
	public void layout() {
		/* Find the usable width of the inside of the table. */
		int fullWidth = elementViewer.getTable().getClientArea().width;

		/* Set the column widths. */
		elementViewer.getTable().getColumn(0).setWidth(fullWidth);
		elementViewer.getTable().layout();
	}
	
	/**
	 * Could not find any built in row locator, so I 
	 * rolled my own here.
	 * (Copied from rule view and changed a bit) 
	 * 
	 * @param element
	 * @return index for the row or -1 if not found
	 */
	public TableItem getElementRowIndex(GIElement element) {

		for (int i=0; i<elementViewer.getTable().getItemCount(); i++) {
			TableItem item = elementViewer.getTable().getItem(i);
			if (item.getData() instanceof GIElement) {
				GIElement tmp = (GIElement) item.getData();
				if (tmp.getElementId().equals(element.getElementId())) {
					return item;
				}
			}
		}
		return null;
	}

	/**
	 * Find the specified element in the listing and set
	 * the selection to it. 
	 * Save the selected element in a variable so that it
	 * is possible to set it again in case of a listing
	 * retrieval.
	 * 
	 * @param element to be selected
	 */
	public void setSelection(GIElement element) {

		if (element != null) {
			TableItem elementTableItem = getElementRowIndex(element);
			if (elementTableItem != null) {
				elementViewer.getTable().setSelection(elementTableItem);
				elementViewer.getTable().showSelection();
			}
		}
	}

	public void setSelection(GIElement[] elements) {

		if (elements != null) {
			TableItem[] tableItems = new TableItem[elements.length];
			for(int i=0; i<elements.length; i++) {
				tableItems[i] = getElementRowIndex(elements[i]);
			}
			elementViewer.getTable().setSelection(tableItems);
			elementViewer.getTable().showSelection();
		}
	}
	
	/**
	 * Find the element with the specified ID in the listing and set
	 * the selection to it. 
	 * Save the selected element in a variable so that it
	 * is possible to set it again in case of a listing
	 * retrieving.
	 * 
	 * @param element to be selected
	 */
	public void setSelection(String id) {
		setSelection(parent.modelAPI.getElement(id)); 
	}
	
	/**
	 * Delete an element after consulting the user.
	 * @param element that may get deleted
	 */
	public void deleteElement(GIElement element) {
		parent.deleteElement(element);
	}

	/**
	 * find the index in the selected row where the mouse was clicked.
	 * (Copied from rule view)
	 */
	public int GetSelectedColumnIndex(TableViewer viewer, Event event) {
		final Rectangle[] selectionBounds = new Rectangle[1];
		TableItem item = viewer.getTable().getItem(new Point(event.x,event.y));
		if( item != null ) {
			int count = viewer.getTable().getColumnCount();
			if( count == 0 ) {
				selectionBounds[0] = item.getBounds();
			}
			for( int i = 0; i < count; i++ ) {
				if( item.getBounds(i).contains(event.x,event.y) ) {
					selectionBounds[0] = item.getBounds(i);
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Listener for double clicks on an element (edits).
	 */
	public IDoubleClickListener elementDoubleClickListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			IStructuredSelection selection = (IStructuredSelection) elementViewer.getSelection();
			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof GIElement) {
				GIElement element = (GIElement) selection.getFirstElement();
				showElementEditor(element);
			}
		}
	};
	
	/**
	 * A listener for when the size of the listing container changes.
	 * This will result in resizing the label column accordingly. 
	 */
	public ControlListener containerResizeListener = new ControlListener() {
		public void controlMoved(ControlEvent e) { 
			layout();
		}
		public void controlResized(ControlEvent e) { 
			layout();
		}
	};
	
	public void showElementEditor(GIElement element) {
		if (!parent.editPossible())
			return;
		IElementEditor editorShowing = parent.getEditorShowing();
		if (editorShowing!=null) {
			editorShowing.hide();
		}
		/** Make something is selected, then try to edit. */
		if (element != null) {
			IElementEditor editor  = parent.createEditor(element);
			editor.modify(element);
		}
	}
	
	/**
	 * A listener for when a row is selected. 
	 * Can result in editing or deleting an element. 
	 */
	public Listener rowSelectedListener = new Listener() {
		public void handleEvent(Event event) {
			int columnIndex = GetSelectedColumnIndex(elementViewer, event);
			IStructuredSelection selection = (IStructuredSelection) elementViewer.getSelection();
			
			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof GIElement) {
				GIElement element = (GIElement) selection.getFirstElement();
				
				if (columnIndex == editColumnIndex) {
					showElementEditor(element);
				}
				else if(columnIndex == deleteColumnIndex) 
					parent.deleteElement(element);
			}
			
		}
	};
	
	/**
	 * A listener for when the user clicks columns headers.
	 * Results in sorting or change in the sorting order.
	 */
	public Listener columnSortListener = new Listener() {
		public void handleEvent(Event event) {
			if(elementViewer.getTable().getSortDirection() == SWT.DOWN) {
				elementViewer.getTable().setSortDirection(SWT.UP);	
				elementViewer.setComparator(sorter.getIncreasingSorter());
			}
			else {
				elementViewer.getTable().setSortDirection(SWT.DOWN);
				elementViewer.setComparator(sorter.getDecreasingSorter());
			}
		}
	};

	public void updateView(String selectedElementId) {
		elementViewer.refresh();
		
	}

	@Override
	public void setMessage(String msg) {
		if (parent.statusline!=null)
			parent.statusline.setMessage(msg);
		
	}
	
	@Override
	public void setErrorMessage(String msg) {
		if (parent.statusline!=null)
			parent.statusline.setErrorMessage(msg);
		
	}
	
	public int getSelectionCount() {
		return elementViewer.getTable().getSelectionCount();
	}

	public ArrayList<GIElement> getSelection() {
		IStructuredSelection selection = (IStructuredSelection) elementViewer.getSelection();
		ArrayList<GIElement> elements = new ArrayList<GIElement>(); 
		for (Object object : selection.toArray()) {
			if (object instanceof GIElement)
				elements.add((GIElement) object);
			
		}
		return elements;
		
	}

	public void selectAll() {
		elementViewer.getTable().selectAll();		
	}
	
}
