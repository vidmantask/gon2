package gon_client_management.view.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

public final class GGuiUtils {
	
	public static void setTabList(Composite container, Widget[] widgetList) {
		List<Control> controlList = new ArrayList<Control>();
		for(Widget widget : widgetList) {
			if (widget instanceof Control) {
				controlList.add((Control) widget);
			}
		}
		Control[] controls = new Control[controlList.size()];
		container.setTabList(controlList.toArray(controls));
	}

	public static void createLine(Composite parent, int ncol) 
	{
		Label line = new Label(parent, SWT.SEPARATOR|SWT.HORIZONTAL|SWT.BOLD);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = ncol;
		line.setLayoutData(gridData);
	}	
	
	public static boolean isTextNonEmpty(Text t)
	{
		String s = t.getText();
		if ((s!=null) && (s.trim().length() >0)) return true;
		return false;
	}	
	public static void showMultipleErrors(ArrayList<String> errorMessages) {
		showMultipleErrors(getDisplay().getActiveShell(), errorMessages);
		
	}
	
	public static void showMultipleErrors(Shell shell, ArrayList<String> errorMessages) {
		if (errorMessages.size()>0) {
			String errorMessage = "";
			if (errorMessages.size()>1)
				errorMessage += "Errors occurred during the operation :\n\n";
			else
				errorMessage += "An error occurred during the operation :\n\n";
			int count = 0;
			for (String string : errorMessages) {
				if (count > 9) {
					int rest = errorMessages.size() - count; 
					errorMessage += "... (" + rest + " more)";
					break;
				}
				errorMessage += string + "\n";
				count++;
			}
				
			MessageDialog.openError(shell, "Error", errorMessage);
		}
	}
	
	public static void setTextSafe(Label label, String text) {
		if (text==null)
			text = "";
		label.setText(text);
	}
	
	public static void setTextSafe(Text label, String text) {
		if (text==null)
			text = "";
		label.setText(text);
	}
	
	public static Composite createNoMarginsComposite(Composite parent, int noOfColumns) {
		final Composite compositeBody = new Composite(parent, SWT.None);
		GridLayout gridLayout = new GridLayout(noOfColumns, false);
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.marginBottom = 0;
		gridLayout.marginLeft = 0;
		gridLayout.marginRight = 0;
		gridLayout.marginWidth = 0;
		gridLayout.horizontalSpacing = 0;
		compositeBody.setLayout(gridLayout);
		GridData contentContainerGridData = new GridData();
		compositeBody.setLayoutData(contentContainerGridData);
		return compositeBody;
		
	}

	public static Display getDisplay() {
		Display display = Display.getCurrent();
		// may be null if outside the UI thread
		if (display == null)
			display = Display.getDefault();
		return display;
	}
	
	public static interface GIMultipleJobElementHandler<E> {
		
		String handleElement(E element);
		
		void updateGUI();
		
		String taskString(E element);
	}

	public abstract static class GMultipleJobElementHandlerAdapter<E>  implements GIMultipleJobElementHandler<E> {
		
		public String taskString(E element) {
			return element.toString();
		}
		
	}

	public static class GMultipleJob<E> extends Job {
		
		final List<E> elementList; 
		final GIMultipleJobElementHandler<E> elementHandler;
		final ArrayList<String> errorMessages = new ArrayList<String>();
		private Shell shell;

		public ArrayList<String> getErrorMessages() {
			return errorMessages;
		}

		public GMultipleJob(String name, Shell shell, List<E> elementList, GIMultipleJobElementHandler<E> elementHandler) {
			super(name);
			this.shell = shell;
			if (elementList.size()>1)
				setUser(true);
			this.elementList = elementList;
			this.elementHandler = elementHandler;
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			monitor.beginTask(this.getName(), elementList.size());
			for (E element : elementList) {
				if (monitor.isCanceled())
					break;
				monitor.subTask(elementHandler.taskString(element));
				String error = elementHandler.handleElement(element);
				if (error!=null) {
					errorMessages.add(error);
				}
				
				monitor.worked(1);
			}
			monitor.done();

			GGuiUtils.getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					elementHandler.updateGUI();
					GGuiUtils.showMultipleErrors(shell, errorMessages);
				}
				
			});
			
			return Status.OK_STATUS;
		}
		
	}
	
}
