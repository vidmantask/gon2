package gon_client_management.view.util;

public interface GIElementListing {

	void show(boolean done);

	void setMessage(String msg);

	void setErrorMessage(String msg);

	void setSelection(String selectedElementId);

}
