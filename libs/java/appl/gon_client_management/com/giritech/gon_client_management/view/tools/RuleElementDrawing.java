package gon_client_management.view.tools;

import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;

import org.eclipse.swt.widgets.Event;

/**
 * Class for displaying rule elements.
 * This provides the glue between drawing basic elements and
 * elements that are wrapped into a rule. Basically we would like 
 * to use the methods in elementdrawing, but need to cast some
 * variables to make this possible.
 * 
 * @author Giritech, cso
 *
 */
public class RuleElementDrawing extends ElementDrawing {

	public int position; 
	
	/**
	 * Set which position the element has in the rule.
	 * -1 is the result column. All others starts from position 0. 
	 * 
	 * @param position 
	 */
	public RuleElementDrawing(int position) {
		super();
		this.position = position;
	}
	
	/**
	 * Get the element at position from a rule. Position is set 
	 * in class initialization, so it is fixed. 
	 * 
	 * @param rule
	 * @return element
	 */
	private GIElement getRuleElement(GIRule rule) {
		if (position == -1)
			return (GIElement) rule.getResultElement();
		else
			return (GIElement) rule.getElement(position);
	}
	
	@Override
	protected void measure(Event event, Object obj) {
		super.measure(event, getRuleElement((GIRule) obj));
	}

	@Override
	protected void paint(Event event, Object obj) {
		drawElement(event, getRuleElement((GIRule) obj));
	}

	@Override
	public String getToolTipText(Object obj) {
		return super.getToolTipText(getRuleElement((GIRule) obj));
	}

}
