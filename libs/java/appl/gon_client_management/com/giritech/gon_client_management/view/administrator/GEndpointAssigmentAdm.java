package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class GEndpointAssigmentAdm implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.ENDPOINT_ASSIGNMENT_TYPE;
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.ENDPOINT_ASSIGNMENT_TYPE;
		String [] defaultElements = {GGlobalDefinitions.USER_TYPE,
									 GGlobalDefinitions.ENDPOINT_TYPE,
									 GGlobalDefinitions.ENDPOINT_ASSIGNMENT_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);
	}


}
