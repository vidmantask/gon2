package gon_client_management.view.software;

import gon_client_management.Activator;
import gon_client_management.model.GIDeployment;
import gon_client_management.model.GIToken;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.GISoftwareDeployment.GICollection;
import gon_client_management.model.GISoftwareDeployment.GIPackage;
import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.view.ext.GJobHandler;
import gon_client_management.view.ext.GSafeView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

public class SoftwareView extends GSafeView {


	/** Model side for locating tokens. */
	private GIDeployment deployment;
	

	/** Elements for selection of tokens. */
	private FormToolkit toolkit;
	public Combo currentTokenList = null;
	public Text statusText = null; 
	public int selectedTokenIndex = 0;
	/** Table for displaying packages. */
	Table packageTable = null;
	/** General area for package handling. */
	private Form packageHandlingForm;
	/** Area for selecting which token we are working with. */
	private Section tokenSelectionSection;
	private Composite tokenSelectionContainer;	
	/** Area for selecting which packages we want to adjust. */
	private Section packageSelectionSection; 
	private Composite packageSelectionContainer;
	/** Area for describing packages. */
	private Section packageDescriptionSection; 
	private ScrolledForm packageDescriptionContainer;
	/** Area for package actions (Install, etc.). */
	private Section packageActionSection;
	private Composite packageActionContainer;
	/** Font definitions. */
	private Font boldFont;
	/** Elements for displaying installation action info. */
	private ProgressBar progressBar;
	private Label progressBarHeaderLabel;
	private Label progressBarInfoLabel;
	private Button addSelectedButton;
	private Button cancelButton;
	private Button okButton;
	private Text installInfoBox;
	/** Layout for specific elements. */
	private GridData progressBarHeaderLabelGridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
	private GridData progressBarGridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
	private GridData progressBarInfoLabelGridData = new GridData(SWT.BEGINNING, SWT.CENTER, true, false);
	private GridData addSelectedButtonGridData = new GridData(SWT.END, SWT.CENTER, true, true);
	private GridData okButtonGridData = new GridData(SWT.END, SWT.CENTER, true, true);
	private GridData cancelButtonGridData = new GridData(SWT.END, SWT.CENTER, true, true);
	private GridData installInfoBoxGridData = new GridData(SWT.FILL, SWT.FILL, true, true);

	
	/**
	 * Class initialization
	 */
	public SoftwareView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}
	
	

	@Override
	public void setViewInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		this.deployment = GModelAPIFactory.getModelAPI().getDeployment();
	}



	/**
	 * Creates all gui elements used in this view.
	 */
	@Override
	public void createView(Composite parent) {

		/** Create a form for the toolkit elements. */
		this.toolkit = new FormToolkit(parent.getDisplay());
		this.packageHandlingForm = toolkit.createForm(parent);
		packageHandlingForm.setText("Token Software Management");
		packageHandlingForm.updateToolBar();
		
		/** Makes sure the font is nice and anti-aliased */
		Font verdanaFont = new Font(parent.getDisplay(), "Verdana", 12, SWT.NORMAL);
		packageHandlingForm.setFont(verdanaFont);
		toolkit.decorateFormHeading(packageHandlingForm);
		

		/** Create layout for this form.  */
		GridLayout layout = new GridLayout(); 
		packageHandlingForm.getBody().setLayout(layout);
		layout.numColumns = 2;
		
		/** Create a bold font for minor highlighting. */
		Font initialFont = packageHandlingForm.getFont();
		FontData[] fontData = initialFont.getFontData();
		for (int i = 0; i < fontData.length; i++) { 
			fontData[i].setStyle(SWT.BOLD);
			fontData[i].setHeight(9);
		}
		this.boldFont = new Font(PlatformUI.getWorkbench().getDisplay(), fontData);

		/**
		 * Create a section with a container for selecting an inserted token.
		 * 
		 * -----------
		 * |   |     |
		 * |   |-----| 
		 * | X |     |
		 * |   |-----|
		 * |   |     |
		 * -----------
		 */
		this.tokenSelectionSection = toolkit.createSection(packageHandlingForm.getBody(), Section.TITLE_BAR);
		tokenSelectionSection.setText("Token selection");
		GridData tokenSelectionSectionGridData = new GridData(GridData.FILL_VERTICAL);
		tokenSelectionSectionGridData.verticalSpan = 3;
		tokenSelectionSection.setLayoutData(tokenSelectionSectionGridData);
		this.tokenSelectionContainer = toolkit.createComposite(tokenSelectionSection);
		tokenSelectionSection.setClient(tokenSelectionContainer);
		GridLayout tokenContainerLayout = new GridLayout(); 
		tokenContainerLayout.numColumns = 3;
		tokenSelectionContainer.setLayout(tokenContainerLayout);

		/** Elements for selecting which token we are operating on. */ 
		toolkit.createLabel(tokenSelectionContainer, "Source: ");
		this.currentTokenList = new Combo(tokenSelectionContainer, SWT.DROP_DOWN);
		currentTokenList.addSelectionListener(currentTokenSelectionListener);
		GridData currentTokenListGridData = new GridData();
		currentTokenListGridData.widthHint = 150;
		currentTokenList.setLayoutData(currentTokenListGridData);
		
		Button refreshButton = toolkit.createButton(tokenSelectionContainer, "Refresh", SWT.PUSH);
		refreshButton.addSelectionListener(refreshCurrentTokenListListener);

		/** Elements for indicating whether the token is enrolled. */
		toolkit.createLabel(tokenSelectionContainer, "Status: ");
		this.statusText = toolkit.createText(tokenSelectionContainer, "unknown");
		statusText.setEnabled(false);
		GridData tokenStatusGridData = new GridData(GridData.FILL_HORIZONTAL);
		tokenStatusGridData.horizontalSpan = 2;
		statusText.setLayoutData(tokenStatusGridData);
		
		/** 
		 * Create a section with a container for selecting software packages. 
 		 * -----------
		 * |   |  X  |
		 * |   |-----| 
		 * |   |     |
		 * |   |-----|
		 * |   |     |
		 * -----------
		 */
		this.packageSelectionSection = toolkit.createSection(packageHandlingForm.getBody(), Section.TITLE_BAR);
		packageSelectionSection.setText("Package collections                  ");
		GridData reportViewingSectionGridData = new GridData(GridData.FILL_BOTH);
		reportViewingSectionGridData.grabExcessHorizontalSpace = true;
		packageSelectionSection.setLayoutData(reportViewingSectionGridData);
		this.packageSelectionContainer = toolkit.createComposite(packageSelectionSection);
		packageSelectionSection.setClient(packageSelectionContainer);
		packageSelectionContainer.setLayout(new GridLayout());
		packageSelectionContainer.addControlListener(containerResizeListener);
		packageSelectionContainer.addPaintListener(containerPaintListener);
		
		/** Create a table for listing packages. */
		this.packageTable = toolkit.createTable(packageSelectionContainer, SWT.BORDER | SWT.FULL_SELECTION | SWT.FILL);
		packageTable.setHeaderVisible(true);
		TableColumn availablePackageColumn = new TableColumn(packageTable, SWT.FILL);
		availablePackageColumn.setText("Collection");
		GridData tableLayout = new GridData(GridData.FILL_BOTH);
		tableLayout.grabExcessHorizontalSpace = true;
		packageTable.setLayoutData(tableLayout);
		packageTable.addSelectionListener(packageSelectionListener);

		/**
		 * Create a section with a container for displaying package or selection information.
		 *
		 * ----------
		 * |   |     |
		 * |   |-----| 
		 * |   |  X  |
		 * |   |-----|
		 * |   |     |
		 * -----------
		 */
		this.packageDescriptionSection = toolkit.createSection(packageHandlingForm.getBody(), Section.TITLE_BAR);
		packageDescriptionSection.setText("Description");
		
		GridData packageDescriptionSectionGridData = new GridData(GridData.FILL_BOTH);
		packageDescriptionSectionGridData.grabExcessHorizontalSpace = true;
		packageDescriptionSection.setLayoutData(packageDescriptionSectionGridData);

		this.packageDescriptionContainer = toolkit.createScrolledForm(packageDescriptionSection);
		packageDescriptionSection.setClient(packageDescriptionContainer);
		
		GridLayout packageDescriptionContainerGridLayout = new GridLayout();
		packageDescriptionContainerGridLayout.numColumns = 2;
		packageDescriptionContainer.getBody().setLayout(packageDescriptionContainerGridLayout);
		
		/**
		 * Create a section with a container for package actions (install, etc.)
		 * 
		 * -----------
		 * |   |     |
		 * |   |-----| 
		 * |   |     |
		 * |   |-----|
		 * |   |  X  |
		 * -----------
		 */
		this.packageActionSection = toolkit.createSection(packageHandlingForm.getBody(), Section.TITLE_BAR | Section.DESCRIPTION);
		packageActionSection.setText("Action");
		GridData packageActionGridData = new GridData(GridData.FILL_BOTH);
		packageActionGridData.grabExcessHorizontalSpace = true;
		packageActionSection.setLayoutData(packageActionGridData);
		this.packageActionContainer = toolkit.createComposite(packageActionSection);
		packageActionSection.setClient(packageActionContainer);
		packageActionContainer.setLayout(new GridLayout());

		/** Create a progress bar that will work for individual tokens. */
		this.progressBarHeaderLabel = toolkit.createLabel(packageActionContainer, "Progress bar headline");
		progressBarHeaderLabel.setFont(boldFont);
		this.progressBar = new ProgressBar(packageActionContainer, SWT.HORIZONTAL );
		this.progressBarInfoLabel = toolkit.createLabel(packageActionContainer, "Progress bar subtext");

		/** Create a text area for install status description after the act. */
		this.installInfoBox = toolkit.createText(packageActionContainer, "TEST", SWT.BORDER);
		installInfoBox.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND));
		installInfoBox.setEditable(false);
		
		/** Create a form for buttons. */
		Composite buttons = toolkit.createComposite(packageActionContainer);
		//buttons.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_GREEN));
		buttons.setLayout(new GridLayout(2, false));
		GridData buttonsGridData = new GridData(SWT.END, SWT.CENTER, true, false);
		buttons.setLayoutData(buttonsGridData);
		
		/** Set the headline for the section (should we remove this?). */
		packageActionSection.setDescription("Removes all installed packages from the token and installs the selected package collection.");

		/** Create buttons for installing, cancellation and an OK (for resetting mode) */
		this.cancelButton = toolkit.createButton(buttons, "Cancel", SWT.PUSH);
		cancelButton.addSelectionListener(cancelInstallationListener);
		cancelButton.setEnabled(false);
		cancelButton.setVisible(true);
		this.addSelectedButton = toolkit.createButton(buttons, "Install", SWT.PUSH);
		addSelectedButton.addSelectionListener(addPackagesListener);
		addSelectedButton.setEnabled(false);
		this.okButton = toolkit.createButton(buttons, "OK", SWT.PUSH);
		okButton.addSelectionListener(okButtonListener);
	
		/** set layout and initial state for the action area elements. */
		progressBarHeaderLabel.setLayoutData(this.progressBarHeaderLabelGridData);
		progressBar.setLayoutData(this.progressBarGridData);
		progressBarInfoLabel.setLayoutData(this.progressBarInfoLabelGridData);
		addSelectedButton.setLayoutData(this.addSelectedButtonGridData);
		cancelButton.setLayoutData(cancelButtonGridData);
		okButton.setLayoutData(okButtonGridData);
		installInfoBox.setLayoutData(this.installInfoBoxGridData);
		
		progressBarGridData.exclude = true;
		progressBarHeaderLabelGridData.exclude = true;
		progressBarInfoLabelGridData.exclude = true;
		installInfoBoxGridData.exclude = true;
		okButtonGridData.exclude = true;

		
		getViewSite().getActionBars().setGlobalActionHandler("refresh", new Action() {

			@Override
			public void runWithEvent(Event event) {
				Display display = getViewSite().getShell().getDisplay();
				display.asyncExec (new Runnable () {
					public void run () {
						refreshView();
					}
				});
			}

			
		});
		
		/** Do layout so elements are placed correctly. */
		layout();

		/** Get information for the parts. */
		refreshView();
	}

	protected void refreshView() {
		deployment.refresh();
		getAvailableTokenInfo(0);
		setActionDescription(0);
		setPackageList();
	}

	/**
	 * Refresh the layout.
	 * This is called if the window size changes or similar.
	 */
	public void layout() {
		int fullWidth = packageTable.getClientArea().width;
		packageTable.getColumn(0).setWidth(fullWidth);
		packageDescriptionContainer.layout();
	}
	
	/**
	 * Set the list of packages or collections.
	 * 
	 * I have called this setPackageList because future versions 
	 * will display packages here. But when the state is collection
	 * we will want a collection list.
	 * 
	 * The package / collection list does not depend on which token
	 * is selected. If the token has a runtime_environment_id defined,
	 * installation is possible.
	 */
	public void setPackageList() {

		List<GICollection> collections = deployment.getCollections();
		/** Clear the table before insertion. */
		packageTable.removeAll();
		
		/** Add the packages to the table. */
		for(int i=0; i<collections.size(); i++) {
			TableItem item  = new TableItem(packageTable, SWT.FILL);
			GICollection collection = collections.get(i);
			item.setData(collection.getId());
			item.setText(collection.getTitle());
		}
		/** Make sure some proper content is displayed in the other containers. */
		packageTable.setSelection(0);
		if (collections.size() > 0)
			setPackageDescription(collections.get(0).getId());
	}
	
	/**
	 * Selection listener for user selections in the package table.
	 */
	SelectionListener packageSelectionListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			setPackageDescription(e.item.getData().toString());
		}
	};
	
	/**
	 * Show the description for a selected package or collection.
	 * @param id identifier for the selected package.
	 */
	public void setPackageDescription(String id) {
		/** Remove existing description content. */
		Control[] children = packageDescriptionContainer.getBody().getChildren();
		for (int i=0; i<children.length; i++) children[i].dispose();

		/** Add a title for the collection / package. */
		Label title = toolkit.createLabel(packageDescriptionContainer.getBody(), "Collection");
		title.setFont(this.boldFont);
		GICollection collection = deployment.getCollection(id);
		if (collection!=null) {
			toolkit.createLabel(packageDescriptionContainer.getBody(), collection.getTitle());

			/** Add a description for the selected collection/package. */
			Label description = toolkit.createLabel(packageDescriptionContainer.getBody(), "Description");
			description.setFont(this.boldFont);
			
			Label hej = toolkit.createLabel(packageDescriptionContainer.getBody(), collection.getDescription(), SWT.WRAP);
			hej.setText(collection.getDescription());
			hej.setBounds(packageDescriptionContainer.getBody().getBounds());
			
			/** Add a content list for the selected collection. */
			Label content = toolkit.createLabel(packageDescriptionContainer.getBody(), "Content");
			content.setFont(boldFont);
			List<GIPackage> packages = collection.getPackages();
			for (int i=0; i<packages.size(); i++) {
				GIPackage _package = packages.get(i);
				String contentString = _package.getTitle() + " [version " + _package.getVersion() + ", platform " + _package.getPlatform() + "]";
				toolkit.createLabel(packageDescriptionContainer.getBody(), contentString);
				toolkit.createLabel(packageDescriptionContainer.getBody(), "");
			}
		}
		packageDescriptionContainer.reflow(true);
	}

	/**
	 * Sets the action area information for the token
	 * specified by tokenIndex. 
	 * 
	 * @param Identifier for the selected token.
	 */
	public void setActionDescription(int tokenIndex) {
		List<GIToken> tokens = deployment.getTokens();
		if (tokens.size()==0 || tokenIndex<0 || tokenIndex>=tokens.size()) {
			addSelectedButton.setEnabled(false);
			cancelButton.setEnabled(true);
			return;
		}
		GIToken token = deployment.getTokens().get(tokenIndex);
		String tokenId = token.getId();
		
		final GJobHandler installPackageJob = currentJobs.get(tokenId);
		if (installPackageJob!=null) {
			GIJobInfo progress = installPackageJob.getProgress();
			if (!progress.more()) {
				
				/** Set progress area not visible. */
				progressBarHeaderLabel.setVisible(false);
				progressBar.setVisible(false);
				progressBarInfoLabel.setVisible(false);
				installInfoBox.setVisible(true);
				installInfoBox.setText(progress.getJobInfo());
				installInfoBox.update();
				progressBarGridData.exclude = true;
				progressBarHeaderLabelGridData.exclude = true;
				progressBarInfoLabelGridData.exclude = true;
				installInfoBoxGridData.exclude = false;
				/** Set button states. */
				addSelectedButton.setVisible(false);
				cancelButton.setVisible(false);
				okButton.setVisible(true);
				addSelectedButtonGridData.exclude = true;
				cancelButtonGridData.exclude = true;
				okButtonGridData.exclude = false;
			}
			else {

				/** Set progress area visible. */
				progressBarHeaderLabel.setVisible(true);
				progressBar.setVisible(true);
				progressBarInfoLabel.setVisible(true);
				
				progressBarGridData.exclude = false;
				progressBarHeaderLabelGridData.exclude = false;
				progressBarInfoLabelGridData.exclude = false;
				installInfoBoxGridData.exclude = true;
				
				/** Set button states. */
				addSelectedButton.setVisible(true);
				cancelButton.setVisible(true);
				okButton.setVisible(false);
				addSelectedButton.setEnabled(false);
				cancelButton.setEnabled(true);
				addSelectedButtonGridData.exclude = false;
				cancelButtonGridData.exclude = false;
				okButtonGridData.exclude = true;
				
				System.out.println("Switch to " + tokenId);
				progessBarUpdater.update(tokenId, progress);
				
			}
		}
		else {
			/** Set progress area not visible. */
			progressBarHeaderLabel.setVisible(false);
			progressBar.setVisible(false);
			progressBarInfoLabel.setVisible(false);
			/** Set the install info to not visible. */
			installInfoBox.setVisible(false);
			
			progressBarGridData.exclude = true;
			progressBarHeaderLabelGridData.exclude = true;
			progressBarInfoLabelGridData.exclude = true;
			installInfoBoxGridData.exclude = true;
			
			/** Set button states. */
			addSelectedButton.setVisible(true);
			cancelButton.setVisible(true);
			okButton.setVisible(false);
			addSelectedButton.setEnabled(token.canInstallSoftware());
			cancelButton.setEnabled(false);
			addSelectedButtonGridData.exclude = false;
			cancelButtonGridData.exclude = false;
			okButtonGridData.exclude = true;
			
		}
		
		
		packageActionSection.layout();
		packageHandlingForm.layout();
	}
	
	
	
	/**
	 * View information for the selected token.
	 * For now only status (enrolled, not enrolled, etc.) is shown. 
	 * 
	 * @param index in the getToken - token-list.
	 */
	private void setSelectedTokenDescription(int index) {
		statusText.setText(deployment.getTokens().get(index).getStatus());
	}
	
	
	/**
	 * Fetch information for currently inserted tokens.
	 * The information is inserted into an array.
	 * If the token is not seen before it is added to the 
	 * token description lists.
	 * 
	 * @param selectionIndex for selection in selection box
	 */
	private void getAvailableTokenInfo(int selectionIndex) {
		/** Make sure the model side is updated. */
		deployment.refreshTokenList();
		/** Clear the selection box. */
		this.currentTokenList.removeAll();
		/** Refresh the selection box with the found tokens. */		
		for (int tIndex=0; tIndex<deployment.getTokenCount(); tIndex++) {
			this.currentTokenList.add(deployment.getTokens().get(tIndex).getTitle());
		}
		selectedTokenIndex = selectionIndex;
		if (this.currentTokenList.getItemCount()> selectionIndex) {
			this.currentTokenList.select(selectionIndex);
			setSelectedTokenDescription(selectionIndex);
		}
	}

	Map<String, GJobHandler> currentJobs = new HashMap<String, GJobHandler>();
	/**
	 * Add packages or a collection to a token.
	 * This should start a job that is connected to the selected token, so
	 * that when the token is selected again we can show how much of the
	 * installation is done.
	 * 
	 * @param tokenIndex
	 */
	public void addPackages(final int tokenIndex) {
		
		int selectionIndex = packageTable.getSelectionIndex();
		if (selectionIndex<0)
			return;
		
		TableItem item = packageTable.getItem(selectionIndex);
		String collectionId = item.getData().toString();
		final GIToken token = getCurrentToken();
		final GIJob installSoftwareJob = token.createInstallSoftwareJob(collectionId);
		
		/* Start a job that can update the action info when its token is selected. */
		final GJobHandler job = new GJobHandler(token.getTitle(), token.getId(), getViewSite().getShell().getDisplay(), installSoftwareJob, Activator.getLogger());
		job.setJobUpdateHandler(progessBarUpdater);
		currentJobs.put(token.getId(), job);
		job.schedule();
		
	}
	
	/**
	 * A listener for when the container has been resized. This
	 * should let the listing columns resize as well.
	 */
	public ControlListener containerResizeListener = new ControlListener() {
		public void controlMoved(ControlEvent e) {
			layout();
		}
		public void controlResized(ControlEvent e) {
			layout();
		}
	};
	
	/**
	 * A listener for laying out the content of the container on the first paint.
	 * If we do not use this it is all packed to the left until somebody 
	 * activates the view.
	 */
	public PaintListener containerPaintListener = new PaintListener() {
		public void paintControl(PaintEvent e) {
			layout();
		}
	};
	
	/**
	 * A listener for the refresh button. 
	 * Results in refreshing the list of currently available tokens.
	 */
	SelectionListener refreshCurrentTokenListListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			selectedTokenIndex = 0;
			getAvailableTokenInfo(0);
			setActionDescription(selectedTokenIndex);
		}
	};
	
	/**
	 * A listener for the selection of a token.
	 * When a token is selected we should display its status
	 * and which packages or collections can be added to it.  
	 */
	SelectionListener currentTokenSelectionListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			Combo tokenList = (Combo) e.widget;
			selectedTokenIndex = tokenList.getSelectionIndex();
			setSelectedTokenDescription(selectedTokenIndex);
			setActionDescription(selectedTokenIndex);
		}
	};
	
	/**
	 * Listener for when to add selected packages to a selected token. 
	 */
	SelectionListener addPackagesListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			addPackages(selectedTokenIndex);
			setActionDescription(selectedTokenIndex);
		}
	};
	
	/**
	 * Listener for the OK button that is displayed after installation.
	 * When this is pushed the mode should return to "READY" for the 
	 * current token.
	 */
	SelectionListener okButtonListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			currentJobs.remove(getCurrentToken().getId());
			setActionDescription(selectedTokenIndex);
		}
	};
	
	/**
	 * Listener for the cancel button this is displayed during installation.
	 * When this is returned the mode should go to "FINISH" for the current
	 * token.
	 */
	SelectionListener cancelInstallationListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			if (selectedTokenIndex >= 0) {
				String tokenId = getCurrentToken().getId();
				GJobHandler job = currentJobs.get(tokenId);
				if (job!=null) {
					job.cancelJob();
				}
			}

			
			// TODO: This should be replaced by a model method.
			// TODO: CancelInstallation(id) -> String
			// TODO: Returns a string describing the consequences of cancelling.
			
		}
	};
	
	/**
	 * Override the standard set focus implementation.
	 * This is used for setting up special menu items 
	 * or focus.
	 */
	@Override
	public void setViewFocus() {
		getViewSite().getActionBars().updateActionBars();
	}

	final ProgressBarUpdater progessBarUpdater = new ProgressBarUpdater();
	
	private final class ProgressBarUpdater implements GJobHandler.JobUpdateHandler {
		
		public void update(String jobId, GIJobInfo info) {
			if (isCurrentToken(jobId)) {
				//System.out.println("Update progress "+info.getJobProgress());
				if (!progressBar.isDisposed()) {
					progressBar.setSelection(info.getJobProgress());
				}
				if (!progressBarInfoLabel.isDisposed()) {
					progressBarInfoLabel.setText(info.getJobInfo());
					progressBarInfoLabel.pack();
				}
				if (!progressBarHeaderLabel.isDisposed()) {
					progressBarHeaderLabel.setText(info.getJobHeader());
					progressBarHeaderLabel.pack();
				}
			}
		}
		

		public void finished(String jobId, GIJobInfo lastProgress) {
			if (isCurrentToken(jobId)) {
				setActionDescription(selectedTokenIndex);
			}
		}

		
		
	}
	
	public boolean isCurrentToken(String tokenId) {
		return deployment.getTokens().get(selectedTokenIndex).getId().equals(tokenId);
	}
	
	public GIToken getCurrentToken() {
		return deployment.getTokens().get(selectedTokenIndex);
	}
	
}

