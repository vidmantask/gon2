package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIElement;
import gon_client_management.view.ext.ConfigWizard;
import gon_client_management.view.ext.IElementEditor;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;

public class WizardEditorElementView extends ElementView {
	
	/**
	 * An attempt of making a non modal wizard out of a modal one.
	 * Seems to work fine. If not please just revert to the old wizard
	 * in createEditor.
	 * 
	 * Stole code from here:
	 *   http://markmail.org/message/zvqbd5fmw55qwrds
	 * 
	 * @author cso
	 */
	protected class NonmodalWizardDialog extends ConfigWizardDialog{
		public NonmodalWizardDialog(Shell parentShell, IWizard newWizard, IDialogSettings dialogSettings) {
			super(parentShell, newWizard, dialogSettings);
			setShellStyle(SWT.SHELL_TRIM);
		}

		NonmodalWizardDialog(Shell parentShell, IWizard newWizard){
			super(parentShell, newWizard);
			setShellStyle(SWT.SHELL_TRIM);
		}
	}
	
	/* (non-Javadoc)
	 * @see gon_client_management.view.element.ElementView#getEditor()
	 */
	@Override
	public IElementEditor createEditor(GIElement element) {
		return createEditor(element, false);
	}

	public IElementEditor createEditor(GIElement element, boolean copy) {
		GIConfigPane configPane;
		if (element!=null)
			configPane = myConfig.getConfigPane(element, copy);
		else
			configPane = myConfig.getCreateConfigPane();

		ConfigWizard configWizard = new ConfigWizard(configPane, Activator.getLogger(), Activator.getDefault().getDialogSettings());
		//final WizardDialog dialog = new WizardDialog(getViewSite().getShell(), configWizard);
		final WizardDialog dialog = new NonmodalWizardDialog(getViewSite().getShell(), configWizard, getDialogSettings());
		
		return new IElementEditor() {

			public void hide() {
				dialog.close();
			}

			public void modify(GIElement element) {
				dialog.open();
			}

			public void show() {
				dialog.open();
			}
		};
	}
	

}
