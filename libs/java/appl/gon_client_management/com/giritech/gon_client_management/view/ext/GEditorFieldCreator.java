package gon_client_management.view.ext;


import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GISelectionEntry;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class GEditorFieldCreator {
	
	@SuppressWarnings("unused")
	private final GIMessageHandler messageHandler;
	
	public GEditorFieldCreator(GIMessageHandler messageHandler) {
		this.messageHandler = messageHandler;
	}

	
	public Widget createLabelAndEditor(GIConfigColumn column, FormToolkit toolkit, Composite master, SelectionListener selectionListener) {
		createLabel(column, toolkit, master);
		return createEditor(column, toolkit, master, selectionListener);
	}

	public Widget createReadOnlyLabelAndEditor(GIConfigColumn column, FormToolkit toolkit, Composite master) {
		createLabel(column, toolkit, master);
		return createReadOnlyEditor(column, toolkit, master);
	}

	
	public Label createLabel(GIConfigColumn column, FormToolkit toolkit, Composite master) {
		Label label = toolkit.createLabel(master, column.getLabel(), SWT.NONE);
		label.setToolTipText(column.getToolTip());
		GridData gridData = new GridData(GridData.BEGINNING);
		gridData.verticalAlignment = SWT.BEGINNING;
		label.setLayoutData(gridData);
		return label;
	}
	public Widget createReadOnlyEditor(GIConfigColumn column, FormToolkit toolkit, Composite master) {
		return createEditor(column, toolkit, master, null, true);
	}

	public Widget createEditor(GIConfigColumn column, FormToolkit toolkit, Composite master, SelectionListener selectionListener) {
		return createEditor(column, toolkit, master, selectionListener, false);
	}
	
	private Widget createEditor(GIConfigColumn column, FormToolkit toolkit, Composite master, SelectionListener selectionListener, boolean readOnly) {
		/* Create an input field depending on type. */

		final Widget configurationField;
		
		/* General type settings */
		readOnly = readOnly || column.isReadOnly();
		int styleFlag = SWT.NONE;
		if (readOnly) {
			styleFlag |= SWT.READ_ONLY;
			
		}
		if (column.isSecret())
			styleFlag |= SWT.PASSWORD;
		
		if (column.getSelection()!=null) {
			styleFlag |= SWT.READ_ONLY;
			Combo comboField = new Combo(master,styleFlag);
			
			for(GISelectionEntry selectionChoice : column.getSelection().getSelectionChoices()) {
				comboField.add(selectionChoice.getTitle());
			}
			GridData inputStringFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
			comboField.setLayoutData(inputStringFieldGridData);
			if (readOnly)
				comboField.setEnabled(false);
			comboField.setToolTipText(column.getToolTip());
			configurationField = comboField;
		}
		else { 
		
		switch (column.getType()) {
		case TYPE_INTEGER:
			Text inputIntField = toolkit.createText(master, "", SWT.BORDER | styleFlag);
			GridData inputIntFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
			inputIntField.setLayoutData(inputIntFieldGridData);
//			inputIntField.addVerifyListener(verifyIntegerListener);
			if (selectionListener!=null)
				inputIntField.addSelectionListener(selectionListener);
			inputIntField.setToolTipText(column.getToolTip());
			configurationField = inputIntField;
			break;
		case TYPE_STRING:
			Text inputStringField = toolkit.createText(master, "", SWT.BORDER | styleFlag);
			if (column.getTextLimit()>0)
				inputStringField.setTextLimit(column.getTextLimit());
			GridData inputStringFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
			inputStringField.setLayoutData(inputStringFieldGridData);
			
			if (selectionListener!=null)
				inputStringField.addSelectionListener(selectionListener);
			inputStringField.setToolTipText(column.getToolTip());
			configurationField = inputStringField;
			break;
		case TYPE_BOOLEAN:
			styleFlag  = SWT.NONE;
			Button inputBooleanField = toolkit.createButton(master, "", SWT.CHECK | styleFlag);
			inputBooleanField.setEnabled(!readOnly);
			if (selectionListener!=null)
				inputBooleanField.addSelectionListener(selectionListener);
			inputBooleanField.setToolTipText(column.getToolTip());
			configurationField = inputBooleanField;
			break;
		case TYPE_TEXT:
			StyledText inputTextField = new StyledText(master, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP  | styleFlag);
			toolkit.adapt(inputTextField, true, true);
			GridData inputTextFieldGridData = new GridData(GridData.FILL_BOTH);
			if (column.getTextLimit()>0)
				inputTextField.setTextLimit(column.getTextLimit());
			inputTextFieldGridData.heightHint = 50;
			inputTextFieldGridData.widthHint = 350;
			inputTextField.setLayoutData(inputTextFieldGridData);
			inputTextField.setToolTipText(column.getToolTip());
			configurationField = inputTextField;
			break;
		case TYPE_DATETIME:
			Composite dateTimeField;
			if ((styleFlag & SWT.READ_ONLY) != 0) 
				dateTimeField = new MyReadOnlyDateTimeWidget(master, SWT.DATE | SWT.TIME);
			else
				dateTimeField = new MyDateTimeWidget(master, SWT.DATE | SWT.TIME);
			dateTimeField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			dateTimeField.setToolTipText(column.getToolTip());
			configurationField = dateTimeField;
			break;
		case TYPE_TIME:
			MyDateTimeWidget timeField = new MyDateTimeWidget(master, SWT.TIME);
			timeField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			timeField.setToolTipText(column.getToolTip());
			configurationField = timeField;
			break;
		case TYPE_DATE:
			MyDateTimeWidget dateField = new MyDateTimeWidget(master, SWT.DATE);
			dateField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
			dateField.setToolTipText(column.getToolTip());
			configurationField = dateField;
			break;
			
		default:
			throw new RuntimeException("Error in GEditorFieldCreator : Unknown config type " + column.getType().toString());
		}
		}
		if (readOnly && configurationField instanceof Control)
			((Control) configurationField).setBackground(master.getDisplay().getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
			
		return configurationField;
		
	}
	
	public static void setWidgetValue(Widget widget, GIConfigRowValue configRowValue) {
		if (widget instanceof Text) {
			final String value;
			if (configRowValue.isNull())
				value = "";
			else {
				value = configRowValue.toString();
			}
			((Text) widget).setText(value);
		}
		else if (widget instanceof StyledText) {
			if (configRowValue.getType() != GIConfigColumn.Type.TYPE_TEXT) {
				throw new RuntimeException("Error in GEditorFieldCreator : Wrong type for StyledText input field: " + configRowValue.getType().toString());
			}
			if (configRowValue.isNull())
				((StyledText) widget).setText("");
			else
				((StyledText) widget).setText(configRowValue.getValueString());
		}
		else if (widget instanceof Button) {
			if (configRowValue.getType() != GIConfigColumn.Type.TYPE_BOOLEAN) {
				throw new RuntimeException("Error in GEditorFieldCreator : Wrong type for Button input field: " + configRowValue.getType().toString());
			}
			if (configRowValue.isNull())
				((Button) widget).setSelection(false);
			else
				((Button) widget).setSelection(configRowValue.getValueBoolean());
		}
		else if (widget instanceof Combo) {
			Combo combo = (Combo) widget;
			int selectionValue = configRowValue.getSelectionValue();
			String valueString = configRowValue.getValueString();
			if (selectionValue<0 && valueString!=null)
				combo.setText(valueString);
			else
				combo.select(configRowValue.getSelectionValue());
		}
		else if (widget instanceof DateTime) {
			Date value = configRowValue.getValueDateTime();
			if (value!=null) {
				Calendar cl = Calendar.getInstance();
				cl.setTime(value);
				int year = cl.get(Calendar.YEAR);
				int month = cl.get(Calendar.MONTH);
				int day = cl.get(Calendar.DATE);
				int hours = cl.get(Calendar.HOUR_OF_DAY);
				int minutes = cl.get(Calendar.MINUTE);
				int seconds = cl.get(Calendar.SECOND);
				
				((DateTime) widget).setDate(year, month, day);
				((DateTime) widget).setTime(hours, minutes, seconds);
			}
			
		}
		else if (widget instanceof MyDateTimeWidgetBase) {
			Date value = configRowValue.getValueDateTime();
			((MyDateTimeWidgetBase) widget).setValue(value);
			
		}
		else {
			throw new RuntimeException("Error in GEditorFieldCreator : Unknown widget type: " + widget.toString());
			
		}
		
	}

	public static void getWidgetValue(Widget widget, GIConfigRowValue configRowValue) {
		if (widget instanceof Text) {
			final Text tmp = (Text) widget;
			final String value = tmp.getText();
			switch (configRowValue.getType()) {
			case TYPE_INTEGER:
			case TYPE_STRING:
			case TYPE_TEXT:
				configRowValue.setValue(value);
				break;
			default:
				throw new RuntimeException("Error in GEditorFieldCreator : Wrong type for Text input field: " + configRowValue.getType().toString());
			}
		}
		else if (widget instanceof StyledText) {
			if (configRowValue.getType() != GIConfigColumn.Type.TYPE_TEXT) {
				throw new RuntimeException("Error in GEditorFieldCreator : Wrong type for StyledText input field: " + configRowValue.getType().toString());
			}
			StyledText tmp = (StyledText) widget;
			configRowValue.setValue(tmp.getText());
		}
		else if (widget instanceof Button) {
			if (configRowValue.getType() != GIConfigColumn.Type.TYPE_BOOLEAN) {
				throw new RuntimeException("Error in GEditorFieldCreator : Wrong type for Button input field: " + configRowValue.getType().toString());
			}
			Button tmp2 = (Button) widget;
			configRowValue.setValue(tmp2.getSelection());
		}
		else if (widget instanceof Combo) {
			Combo combo = (Combo) widget;
			int selectionIndex = combo.getSelectionIndex();
			String text = combo.getText();
			if (selectionIndex<0 && !"".equals(text)) 
				configRowValue.setValue(text);
			else
				configRowValue.setSelectionValue(selectionIndex);
	
		}
		else if (widget instanceof DateTime) {
			DateTime dt = (DateTime) widget;
			
			Calendar cl = Calendar.getInstance();
			cl.set(dt.getYear(), dt.getMonth(), dt.getDay(), dt.getHours(), dt.getMinutes(), dt.getSeconds());
			configRowValue.setValue(cl.getTime());
	
		}
		else if (widget instanceof MyDateTimeWidgetBase) {
			MyDateTimeWidgetBase dt = (MyDateTimeWidgetBase) widget;
			
			configRowValue.setValue(dt.getValue());
	
		}
		else {
			throw new RuntimeException("Error in GEditorFieldCreator : Unknown widget type: " + widget.toString());
			
		}
	}
	
	abstract class MyDateTimeWidgetBase extends Composite {

		public MyDateTimeWidgetBase(Composite parent, int style) {
			super(parent, style);
		}

		abstract public void setValue(Date value);

		abstract public Date getValue();
		
	}
	
	
	class MyDateTimeWidget extends MyDateTimeWidgetBase {


		private Button isSetButton;
		private DateTime dateField = null;
		private DateTime timeField = null;
		private GEnablingControl enablingControl;

		public MyDateTimeWidget(Composite parent, int style) {
			super(parent, style);
			
			int columnCount = 1;
			boolean addDate = false;
			boolean addTime = false;
			
			if ((style & SWT.DATE) != 0) {
				addDate = true;
				columnCount++;
			}
			
			if ((style & SWT.TIME) != 0) {
				addTime = true;
				columnCount++;
			}
			if (columnCount==1)
				throw new RuntimeException("Internal Error: Invalid style for MyDateTimeWidget: " + style);
			
			GridLayout gridLayout = new GridLayout(columnCount, false);
//			gridLayout.horizontalSpacing = 0;
			gridLayout.verticalSpacing = 0;
			gridLayout.marginHeight = 0;
			gridLayout.marginBottom = 0;
			gridLayout.marginLeft = 0;
			gridLayout.marginRight = 0;
			gridLayout.marginWidth = 0;
			setLayout(gridLayout);
			
			isSetButton = new Button(this, SWT.CHECK);
			enablingControl = new GEnablingControl(this, isSetButton);
			isSetButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					enablingControl.setVisibility();
				}
				
			});
			if (addDate) {
				dateField = new DateTime(this, SWT.DATE );
				dateField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				enablingControl.addChild(dateField);
			}
			if (addTime) {
				timeField  = new DateTime(this, SWT.TIME | SWT.SHORT);
				timeField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				enablingControl.addChild(timeField);
			}
			
			
		}

		public void setValue(Date value) {
			if (value==null) {
				isSetButton.setSelection(false);
			}
			else {
				isSetButton.setSelection(true);
				Calendar cl = Calendar.getInstance();
				cl.setTime(value);
				if (dateField!=null) {
					int year = cl.get(Calendar.YEAR);
					int month = cl.get(Calendar.MONTH);
					int day = cl.get(Calendar.DATE);
					dateField.setDate(year, month, day);
				}
				if (timeField!=null) {
					int hours = cl.get(Calendar.HOUR_OF_DAY);
					int minutes = cl.get(Calendar.MINUTE);
					int seconds = cl.get(Calendar.SECOND);
					timeField.setTime(hours, minutes, seconds);
				}
				
			}
			enablingControl.setVisibility();
			
		}

		public Date getValue() {
			boolean isSet = isSetButton.getSelection();
			if (isSet) {
				Calendar cl = Calendar.getInstance();
				DateTime dateContent = null;
				DateTime timeContent = null;
				if (dateField!=null) 
					dateContent = dateField;
				else
					dateContent = timeField;
				
				if (timeField!=null) 
					timeContent = timeField;
				
				if (timeContent != null) 
					cl.set(dateContent.getYear(), dateContent.getMonth(), dateContent.getDay(), 
							timeContent.getHours(), timeContent.getMinutes(), timeContent.getSeconds());
				else
					cl.set(dateContent.getYear(), dateContent.getMonth(), dateContent.getDay());

				return cl.getTime();
				
			}
			else {
				return null;
			}
		}

		@Override
		public void setToolTipText(String string) {
			super.setToolTipText(string);
			isSetButton.setToolTipText(string);
			if (dateField!=null)
				dateField.setToolTipText(string);
			if (timeField!=null)
				timeField.setToolTipText(string);
		}

		
		
	}
	class MyReadOnlyDateTimeWidget1 extends MyDateTimeWidgetBase {

		boolean isNull = false;
		private DateTime dateField = null;
		private DateTime timeField = null;

		public MyReadOnlyDateTimeWidget1(Composite parent, int style) {
			super(parent, style);
			
			int columnCount = 0;
			boolean addDate = false;
			boolean addTime = false;
			
			if ((style & SWT.DATE) != 0) {
				addDate = true;
				columnCount++;
			}
			
			if ((style & SWT.TIME) != 0) {
				addTime = true;
				columnCount++;
			}
			if (columnCount==0)
				throw new RuntimeException("Internal Error: Invalid style for MyDateTimeWidget: " + style);
			
			GridLayout gridLayout = new GridLayout(columnCount, false);
//			gridLayout.horizontalSpacing = 0;
			gridLayout.verticalSpacing = 0;
			gridLayout.marginHeight = 0;
			gridLayout.marginBottom = 0;
			gridLayout.marginLeft = 0;
			gridLayout.marginRight = 0;
			gridLayout.marginWidth = 0;
			setLayout(gridLayout);
			
			if (addDate) {
				dateField = new DateTime(this, SWT.DATE);
				dateField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				dateField.setEnabled(false);
			}
			if (addTime) {
				timeField  = new DateTime(this, SWT.TIME | SWT.SHORT);
				timeField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				timeField.setEnabled(false);
			}
			
			
		}

		public void setValue(Date value) {
			if (value==null) {
				isNull = true;
				dateField.setVisible(false);
				timeField.setVisible(false);
			}
			else {
				Calendar cl = Calendar.getInstance();
				cl.setTime(value);
				if (dateField!=null) {
					int year = cl.get(Calendar.YEAR);
					int month = cl.get(Calendar.MONTH);
					int day = cl.get(Calendar.DATE);
					dateField.setDate(year, month, day);
				}
				if (timeField!=null) {
					int hours = cl.get(Calendar.HOUR_OF_DAY);
					int minutes = cl.get(Calendar.MINUTE);
					int seconds = cl.get(Calendar.SECOND);
					timeField.setTime(hours, minutes, seconds);
				}
				
			}
			
		}

		public Date getValue() {
			if (!isNull) {
				Calendar cl = Calendar.getInstance();
				DateTime dateContent = null;
				DateTime timeContent = null;
				if (dateField!=null) 
					dateContent = dateField;
				else
					dateContent = timeField;
				
				if (timeField!=null) 
					timeContent = timeField;
				
				if (timeContent != null) 
					cl.set(dateContent.getYear(), dateContent.getMonth(), dateContent.getDay(), 
							timeContent.getHours(), timeContent.getMinutes(), timeContent.getSeconds());
				else
					cl.set(dateContent.getYear(), dateContent.getMonth(), dateContent.getDay());

				return cl.getTime();
				
			}
			else {
				return null;
			}
		}

		@Override
		public void setToolTipText(String string) {
			super.setToolTipText(string);
			if (dateField!=null)
				dateField.setToolTipText(string);
			if (timeField!=null)
				timeField.setToolTipText(string);
		}

		
		
	}
	
	class MyReadOnlyDateTimeWidget extends MyDateTimeWidgetBase {

		private Date dateTimeValue = null;
		private Text dateField = null;
		private boolean showDate = false;
		private boolean showTime = false;

		public MyReadOnlyDateTimeWidget(Composite parent, int style) {
			super(parent, style);
			
			if ((style & SWT.DATE) != 0) {
				showDate = true;
			}
			
			if ((style & SWT.TIME) != 0) {
				showTime = true;
			}
			if (!showDate && !showTime)
				throw new RuntimeException("Internal Error: Invalid style for MyDateTimeWidget: " + style);
			
			GridLayout gridLayout = new GridLayout(1, false);
//			gridLayout.horizontalSpacing = 0;
			gridLayout.verticalSpacing = 0;
			gridLayout.marginHeight = 0;
			gridLayout.marginBottom = 0;
			gridLayout.marginLeft = 0;
			gridLayout.marginRight = 0;
			gridLayout.marginWidth = 0;
			setLayout(gridLayout);
			dateField = new Text(this, SWT.BORDER | SWT.READ_ONLY);
			GridData inputIntFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
			dateField.setLayoutData(inputIntFieldGridData);
			
		}

		public void setValue(Date value) {
			dateTimeValue = value;
			String dateTimeString = "";
			if (dateTimeValue==null) {
			}
			else {
				if (showDate && showTime)
					dateTimeString = DateFormat.getDateTimeInstance().format(value);
				else if (showDate) 
					dateTimeString = DateFormat.getDateInstance().format(value);
				else
					dateTimeString = DateFormat.getTimeInstance().format(value);
			}
			dateField.setText(dateTimeString);
			
		}

		public Date getValue() {
			return dateTimeValue;
		}

		@Override
		public void setToolTipText(String string) {
			super.setToolTipText(string);
			dateField.setToolTipText(string);
		}

		
		
	}
}
