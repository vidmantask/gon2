package gon_client_management.view.administrator;


import gon_client_management.view.config.GConfigView;
import gon_client_management.view.config.GGatewayUsersView;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class GGatewayConfigPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(false);
		layout.setFixed(true);
		layout.addStandaloneView(GConfigView.ID,  false, IPageLayout.LEFT, 0.3f, editorArea);
		layout.addStandaloneView(GGatewayUsersView.ID,  false, IPageLayout.RIGHT, 0.7f, editorArea);

	}

}
