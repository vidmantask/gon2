package gon_client_management.view.ext;

import gon_client_management.ext.GILogger;
import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.model.ext.GIConfigColumn.GIAction;
import gon_client_management.view.ext.GJobHandler.JobUpdateHandler;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

public class GConfigTemplateViewUtil implements GIMessageHandler {

	private GIConfigPaneContainer configContainer;
	private final boolean readOnly;
	List<Widget> configurationFields = new ArrayList<Widget>();
	List<GIConfigRowValue> configurationValues = new ArrayList<GIConfigRowValue>();
	private GILogger logger;
	private GIConfigPanePage myConfigPane;
	private IDialogSettings dialogSettings;

	public GConfigTemplateViewUtil(GIConfigPaneContainer container, GIConfigPanePage myConfigPane, GILogger logger, IDialogSettings dialogSettings) {
		this.configContainer = container;
		this.logger = logger;
		this.myConfigPane = myConfigPane;
		this.dialogSettings = dialogSettings;
		readOnly = container.isReadOnly();
	}
	
	@Override
	public void setMessage(String message) {
		configContainer.setMessage(message);
	}

	@Override
	public void setMessage(String message, int newType) {
		configContainer.setMessage(message, newType);
	}
	
	public Composite createConfigPaneContent(Composite parent) {

		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		ScrolledForm dialogForm = toolkit.createScrolledForm(parent);

		final GIConfigPanePage advancedPane = myConfigPane.getAdvancedPane();
		
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		dialogForm.getBody().setLayout(layout);

		//GridData gridData = new GridData(GridData.FILL_VERTICAL);
		//GridData gridData = new GridData(GridData.FILL_VERTICAL | GridData.FILL_HORIZONTAL);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		//gridData.grabExcessVerticalSpace = true;
		//gridData.grabExcessHorizontalSpace = true;
		//gridData.verticalAlignment = SWT.FILL;
		dialogForm.getBody().setLayoutData(gridData);
		
		
		

//		Section masterSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR);
//		masterSection.setText("Specification settings");
		final Composite master = createFieldSection(dialogForm.getBody(), toolkit, myConfigPane.getTitle(), false);
		
		populateFieldSection(master, toolkit, myConfigPane);
		if (advancedPane!=null) {
			final Composite advancedSection = createFieldSection(dialogForm.getBody(), toolkit, "Advanced", true);
			populateFieldSection(advancedSection, toolkit, advancedPane);
		}
		return dialogForm;
	}
	
	

	public void populateFieldSection(final Composite master, FormToolkit toolkit, GIConfigPanePage configPane) {
	
		
		/* Create layout for the container. */
		GridLayout masterContainerGridLayout = new GridLayout();
		masterContainerGridLayout.numColumns = 2;
		master.setLayout(masterContainerGridLayout);
		//GridData masterContainerGridData = new GridData(GridData.FILL_BOTH);
		GridData masterContainerGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		//masterContainerGridData.widthHint = 100;
		master.setLayoutData(masterContainerGridData);

		
		/**
		 * Create a configuration editing area. 
		 */

		createConfigFields(toolkit, master, configPane);
		
	}

	public Composite createFieldSection(Composite parent, FormToolkit toolkit, String title, boolean advanced) {

		final Section masterSection;
		if (!advanced) 
			masterSection = toolkit.createSection(parent, Section.TITLE_BAR);
		else {
			masterSection = toolkit.createSection(parent, Section.TITLE_BAR | Section.TWISTIE );
			if (dialogSettings!=null) {
				boolean expanded = dialogSettings.getBoolean("ADVANCED_EXPANDED");
				masterSection.setExpanded(expanded);
				masterSection.addExpansionListener(new ExpansionAdapter() {
					
					@Override
					public void expansionStateChanged(ExpansionEvent e) {
						dialogSettings.put("ADVANCED_EXPANDED", masterSection.isExpanded());
						
					}
				});
			}
		}
		masterSection.setText(title);
		masterSection.setLayout(new GridLayout());
		//GridData masterSectionGridData = new GridData(GridData.FILL_BOTH);
		GridData masterSectionGridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		if (advanced)
			masterSectionGridData.verticalAlignment = SWT.END;
		masterSection.setLayoutData(masterSectionGridData);
		

		/* Create a container for parts in this section. */
		final Composite master = toolkit.createComposite(masterSection);
		masterSection.setClient(master);
		return master;
	}

	private void createConfigFields(FormToolkit toolkit, final Composite parent, GIConfigPanePage configPane) {
		
		GEditorFieldCreator editorFieldCreator = new GEditorFieldCreator(this);
		final int columnCount = configPane.getColumnCount();
		for (int i=0; i<columnCount; i++) {

			final GIConfigColumn column = configPane.getColumn(i);
			final int index = i;
			
			
			final GIAction action = column.getAction();
			
			if (!readOnly && action!=null) {

				editorFieldCreator.createLabel(column, toolkit, parent);

				/* create Composite for field + button */
				Composite composite = createFieldButtonComposite(toolkit, parent);
				
				final Widget widget = editorFieldCreator.createEditor(column, toolkit, composite, null);
				GEditorFieldCreator.setWidgetValue(widget, configPane.getValue(i));
				configurationFields.add(widget);
				
				/* Create button and action */
				final GJobHandler runnable = createAction(parent, action, index);
				
				SelectionListener selectionListener = createButtonSelectionListener(parent, runnable);

				Button button = toolkit.createButton(composite, action.getTitle(), SWT.PUSH);
				button.addSelectionListener(selectionListener);
				
				
			}
			else {
				/* Create a label and field. */
				final Widget widget;
				if (!readOnly)
					widget = editorFieldCreator.createLabelAndEditor(column, toolkit, parent, null);
				else
					widget = editorFieldCreator.createReadOnlyLabelAndEditor(column, toolkit, parent);
				GEditorFieldCreator.setWidgetValue(widget, configPane.getValue(i));
				configurationFields.add(widget);
			}
			configurationValues.add(configPane.getValue(i));
			
	
		}
	}

	private SelectionListener createButtonSelectionListener(final Composite master, final GJobHandler runnable) {
		SelectionListener selectionListener = new SelectionAdapter() {

			public void widgetSelected(SelectionEvent e) {
				setValues();
				configContainer.runFieldJob(runnable);
			}
			
		};
		return selectionListener;
	}

	private Composite createFieldButtonComposite(FormToolkit toolkit,
			final Composite parent) {
		Composite composite = toolkit.createComposite(parent);
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.verticalSpacing = 0;
		gridLayout.marginHeight = 0;
		gridLayout.marginWidth = 0;
		composite.setLayout(gridLayout);
		GridData inputIntFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
		composite.setLayoutData(inputIntFieldGridData);
		return composite;
	}

	private GJobHandler createAction(final Composite parent, final GIAction action, final int fieldIndex) {
		final GIJob job = action.getJob();
		final GJobHandler jobHandler = new GJobHandler(action.getTitle(), "FieldAction" + fieldIndex, parent.getShell().getDisplay(), job, logger);
		jobHandler.setJobUpdateHandler(new JobUpdateHandler() {

			@Override
			public void finished(String jobId, GIJobInfo lastProgress) {
				configContainer.fieldJobFinished(parent, job, fieldIndex);
				
			}

			@Override
			public void update(String jobId, GIJobInfo status) {
			}
			
		});
		
		return jobHandler;
	}
	
	public void setValues() {
		if (readOnly)
			return;
		for (int i=0; i<configurationFields.size(); i++) {
			GEditorFieldCreator.getWidgetValue(configurationFields.get(i), configurationValues.get(i));
		}
		
	}

}
