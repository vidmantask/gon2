package gon_client_management.view.preferences;

import gon_client_management.Activator;

import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class GAdminPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private StringFieldEditor gManagementServerHostField = null;
	private StringFieldEditor gManagementServerPortField = null;
	private StringFieldEditor gManagementServiceServerHostField = null;
	private StringFieldEditor gManagementServiceServerPortField = null;
	private StringFieldEditor gManagementServiceExecutableField = null;
	
	
	public GAdminPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Setup the G/On Management Server and service\n");
	}

//	@Override
//	public void propertyChange(PropertyChangeEvent event) {
//		if (!isValid()) {
//			getApplyButton().setEnabled(false);
//			super.setValid(false);
//		}
//		else {
//			getApplyButton().setEnabled(true);
//			super.setValid(true);
//		}
//		super.propertyChange(event);
//	}
//
//	/**
//	 * See that the fields have valid input. If not the
//	 * buttons used for saving the preference data should
//	 * be disabled.
//	 */
//	@Override
//	public boolean isValid() {
//		String serverUrl = gServerUrlField.getStringValue();
//		String serviceUrl = gAdminServiceUrlField.getStringValue();
//		if (serverUrl.startsWith("http://") && serviceUrl.startsWith("http://")) 
//			return true;
//		else 
//			return false;
//
//	}

	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 * 
	 * EXAMPLES: 
	 *	addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, "&Directory preference:", getFieldEditorParent()));
	 * addField(new BooleanFieldEditor(PreferenceConstants.P_BOOLEAN, "&An example of a boolean preference", getFieldEditorParent()));
	 * 	addField(new RadioGroupFieldEditor(PreferenceConstants.P_CHOICE, "An example of a multiple-choice preference", 1, new String[][] { { "&Choice 1", "choice1" }, {"C&hoice 2", "choice2" }}, getFieldEditorParent()));
	 *	addField(new StringFieldEditor(PreferenceConstants.G_SERVER_URL, "G/On Administration Server:", getFieldEditorParent()));
	 * 
	 */
	public void createFieldEditors() {
		gManagementServerHostField = new StringFieldEditor(PreferenceConstants.G_MANAGEMENT_SERVER_HOST, "G/On Management Server Host:", getFieldEditorParent());
		addField(gManagementServerHostField);

		gManagementServerPortField = new StringFieldEditor(PreferenceConstants.G_MANAGEMENT_SERVER_PORT, "G/On Management Server Port:", getFieldEditorParent()); 
		addField(gManagementServerPortField);
		
		gManagementServiceExecutableField = new StringFieldEditor(PreferenceConstants.G_MANAGEMENT_SERVICE_EXECUTABLE, "Service Executable:", getFieldEditorParent());
		addField(gManagementServiceExecutableField);

		gManagementServiceServerHostField = new StringFieldEditor(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_HOST, "Service Host:", getFieldEditorParent());
		addField(gManagementServiceServerHostField);

		gManagementServiceServerPortField = new StringFieldEditor(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_PORT, "Service Port:", getFieldEditorParent());
		addField(gManagementServiceServerPortField);
		
		ComboFieldEditor comboFieldEditor = new ComboFieldEditor(PreferenceConstants.G_SKIP_LOGIN, 
																  "Skip login", 
																  new String[][] { 
																					{ "Always", MessageDialogWithToggle.ALWAYS }, 
																					{ "Default", "noprompt" }, 
																					{ "Prompt", MessageDialogWithToggle.PROMPT }, 
																					{ "Never", MessageDialogWithToggle.NEVER }, 
																  				}, 
																   getFieldEditorParent());
		
		StringFieldEditor csvDelimiterEditor = new StringFieldEditor(PreferenceConstants.G_CSV_DELIMITER, "CSV File Delimiter", getFieldEditorParent());
		csvDelimiterEditor.setTextLimit(1);
		addField(csvDelimiterEditor);
		
		addField(comboFieldEditor);
		
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
}