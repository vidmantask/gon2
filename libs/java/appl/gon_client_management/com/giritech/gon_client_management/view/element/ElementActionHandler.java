package gon_client_management.view.element;

import gon_client_management.model.ext.GIElement;
import gon_client_management.view.rule.RuleView;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISources;
import org.eclipse.ui.handlers.HandlerUtil;

public class ElementActionHandler implements IHandler {
	
	public void addHandlerListener(IHandlerListener handlerListener) {}
	public void removeHandlerListener(IHandlerListener handlerListener) {}
	public void dispose() {}

	
	private GIElement[] getSelectedElements(Object[] selectedElements) {
		ArrayList<GIElement> ElementList = new ArrayList<GIElement>();
		if (selectedElements!=null)
			for (Object obj : selectedElements) {
				
				if (obj instanceof GIElement) {
					ElementList.add((GIElement) obj);
				}
			}
		GIElement[] Elements = new GIElement[ElementList.size()];
		return ElementList.toArray(Elements);
	}
	
	/**
	 * Execute actions for the context menu. 
	 * 
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String commandId = event.getCommand().getId();
		ElementView currentView = (ElementView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		IStructuredSelection selection;
		GIElement element = null;
		Object[] selectedElements = null;
		
		// Get an element to perform the action on.
		if (event.getApplicationContext() instanceof EvaluationContext) {
 			EvaluationContext context = (EvaluationContext) event.getApplicationContext();
 			if (context.getVariable(ISources.ACTIVE_MENU_SELECTION_NAME) instanceof IStructuredSelection) {
 				selection = (IStructuredSelection) context.getVariable(ISources.ACTIVE_MENU_SELECTION_NAME);
 			}
 			else {
 				selection = (IStructuredSelection) currentView.listing.elementViewer.getSelection();
 			}
 			Object firstElement = selection.getFirstElement();
 			if (firstElement instanceof GIElement)
 				element = (GIElement) selection.getFirstElement();
 			selectedElements = selection.toArray();
		}
		
		// Perform the selected action.
		if (commandId.equals("EDIT_ELEMENT") && element != null) {
			currentView.listing.showElementEditor(element);
		}
		else if (commandId.equals("DELETE_ELEMENT") && element != null) {
 			currentView.deleteElements(getSelectedElements(selectedElements));
		}
		else if (commandId.equals("NEW_ELEMENT")) {
			Action showElementEditorAction = currentView.getShowElementEditorAction();
			if (showElementEditorAction.isEnabled())
				showElementEditorAction.run();
		}
		else if (commandId.equals("ADD_ELEMENT_TO_RULE_EDITOR") && element != null) {
			RuleView currentRuleView = currentView.getCurrentRuleView();
			if (currentRuleView!=null)
				currentRuleView.startEditing(element);
		}
		else if (commandId.equals("VIEW_ELEMENT_DEFAULT") && element != null) {
			currentView.openDefaultEditor(element);
		}
		else if (commandId.equals("ADD_ELEMENT_COPY") && element != null) {
			currentView.showElementCopyEditor(element);
		}
		else if (commandId.equals("REFRESH_ELEMENTS")) {
			currentView.refresh();
			RuleView ruleView = currentView.getCurrentRuleView();
			if (ruleView!=null)
				ruleView.refresh();
		}
		else if (commandId.equals("ADD_TO_ONE_TIME_ENROLLERS")) {
			if (element!=null)
				currentView.modelAPI.addMembersToOneTimeEnrollers(element);
		}
		else if (commandId.equals("java_gon_client_management.REGISTER_USER")) {
			if (element!=null && currentView instanceof UserView) {
				((UserView) currentView).changeUserRegistration(getSelectedElements(selectedElements));
			}
		}
		else if (commandId.equals("ADD_ZONE_RESTRICTION") && element != null) {
			 currentView.addZoneRestrictions(element);
		}
		else if (commandId.equals("SELECT_ALL_ELEMENTS")) {
			 currentView.listing.selectAll();
		}
		else {
			System.out.println("Uncaught commandID: " + commandId);
		}
		return null;
	}

	/**
	 * Enabled status is set by the handler in plugin.xml. 
	 * The context for the current view is set in the ElementView
	 * implementation. Each handler can ask for a specific context
	 * string. 
	 * Therefore - this is not used. 
	 */
	public boolean isEnabled() {
		return true;
	}

	/**
	 * This is set to true to say that we will handle
	 * execution ourselves. This makes sure that execute
	 * is called when the handler is selected.
	 */
	public boolean isHandled() {
		return true;
	}
}
