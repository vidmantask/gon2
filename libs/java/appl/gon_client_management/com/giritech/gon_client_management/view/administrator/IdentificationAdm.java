package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class IdentificationAdm implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.KEY_ASSIGNMENT_TYPE;
		String ruleViewId = "gon_client_management.KeyAssignmentView";
		String [] defaultElements = {GGlobalDefinitions.USER_TYPE,
									GGlobalDefinitions.TOKEN_TYPE,
//									GGlobalDefinitions.KEY_ASSIGNMENT_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
//												  ruleViewName,
												  null,
												  ruleViewId, 
												  defaultElements);
	}
}
