package gon_client_management.view.ext;

import gon_client_management.model.ext.GIConfig;

import java.util.List;

public interface GIConfigWizardSet {
	
	public List<GIWizardExtension> getExtensions();
	
	public void init(GIConfig config);
}
