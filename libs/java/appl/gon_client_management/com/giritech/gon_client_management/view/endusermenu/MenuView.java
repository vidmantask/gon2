package gon_client_management.view.endusermenu;

import gon_client_management.Activator;
import gon_client_management.model.GIMenuItemHandling;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.GIMenuItemHandling.GIMenuItem;
import gon_client_management.model.GIMenuItemHandling.ItemType;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.administrator.GElementUpdateHandler.GIElementListener;
import gon_client_management.view.ext.GSafeView;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;


public class MenuView extends GSafeView implements GIElementListener {

	/** Elements for selection of tokens. */
	private FormToolkit toolkit;
	private Form menuHandlingForm;
	private GIMenuItemHandling menuitems;
	
	
	private Composite treecontainer = null;
	private TreeViewer tree = null;
	
	public MenuView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}

	
	
	@Override
	public void initView(IViewSite site) {
		super.initView(site);
		menuitems = GModelAPIFactory.getModelAPI().getMenuHandlingAPI();
	}

	public void refresh() {
		//System.out.println("MenuView.update() called");
		menuitems.refresh();
		tree.refresh();
		//System.out.println("MenuView.update() finished");
	}
	
	@Override
	public void createView(Composite parent) {
		
		/** Create a form for the toolkit elements. */
		this.toolkit = new FormToolkit(parent.getDisplay());
		this.menuHandlingForm = toolkit.createForm(parent);
		menuHandlingForm.setText("Menu Structure Management");
		menuHandlingForm.updateToolBar();
		
		Font verdanaFont = new Font(parent.getDisplay(), "Verdana", 12, SWT.NORMAL);
		menuHandlingForm.setFont(verdanaFont);

		toolkit.decorateFormHeading(menuHandlingForm);	
		
		/** Set the context for this view. */
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.endusermenu");
		
		/** Create layout for this form.  */
		GridLayout layout = new GridLayout();
		menuHandlingForm.getBody().setLayout(new FillLayout());
		
		SashForm splitform = new SashForm(menuHandlingForm.getBody(), SWT.HORIZONTAL);
		splitform.setLayout(new FillLayout());
		
		/**
		 * ---------
		 * |   |   |
		 * | X |   |
		 * |   |   | 
		 * --------- 
		 */
		this.treecontainer = toolkit.createComposite(splitform, SWT.NONE);
		treecontainer.setLayout(layout);
		GridData TreeGridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		treecontainer.setLayoutData(TreeGridData);

		this.tree = new TreeViewer(treecontainer, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FILL);
		tree.getTree().setLayout(layout);
		tree.getTree().setLayoutData(TreeGridData);

		/** Set providers. */
		tree.setContentProvider(menuContentProvider());
		tree.setInput( menuitems.getTopMenu() );
		tree.setLabelProvider(menuLabelProvider());
		getSite().setSelectionProvider(tree);
		
		/** Add drag support. */
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer()};
		int operations = DND.DROP_MOVE | DND.DROP_COPY;
		tree.addDragSupport(operations, transfers, new MenuDragListener(tree)); 
	
		/** Add drop support. */
		tree.addDropSupport(operations, transfers, new MenuDropListener(tree));
		
		/** Set a comparator for ordering elements in folders. */
		//tree.setComparator(new menuSorter());

		/** Make the folder structure all visible. */
		tree.expandAll();
		//tree.setSelection(new StructuredSelection(menuitems.getTopMenu()));
		
		List<String> subscriptionTypes = menuitems.getSubscriptionTypes();
		GElementUpdateHandler.getElementUpdateHandler().addElementListener(subscriptionTypes, this);
		
	    MenuManager menuMgr = new MenuManager();
	    menuMgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	    getSite().registerContextMenu(menuMgr, tree);
	    Control control = tree.getControl();
	    Menu menu = menuMgr.createContextMenu(control);
	    menuMgr.add(new Action("Remove") {

			@Override
			public void run() {
				removeItem();
			}
	    	
	    });
	    control.setMenu(menu);
	}		
		
	
	/**
	 * Remember to check that the folder to be removed is empty (display alert).
	 * Also check that the top folder is never removed (done in extensions).
	 */
	public void removeFolder() {
		ISelection selection = tree.getSelection();
		
		if (!(selection instanceof TreeSelection))
			return;
		
		GIMenuItem tmp = (GIMenuItem) ((IStructuredSelection) selection).getFirstElement(); 

//		if (tmp.getChildren().size() > 0) { 
//			MessageDialog.openInformation(getViewSite().getShell(), "Information", 
//			"You can not delete a non empty folder. Please move all menu actions to another folder before deleting.");
//		}
// 		else {
// 			tree.setSelection(new StructuredSelection(tmp.getParent()), true);
 			
 			removeItem(tmp, selection);
// 		}
	}

	private void removeItem(GIMenuItem item, ISelection selection) {
		TreeSelection treeSelection = (TreeSelection) selection;
		GIMenuItem parent = getParent(item, treeSelection);
		
		if ( ((GIMenuItem) ((IStructuredSelection) selection).getFirstElement()).getType().toString() == "AUTO_ITEM") {
			MessageDialog.openError(getViewSite().getShell(), "Error", "Automatically generated menu items can't be removed.");
			return;
		}
		if (parent==null)
			return;
		try {
			menuitems.removeItem(item, parent);
		} catch (GOperationNotAllowedException e) {
			handleError(e);
		}
		tree.refresh();
	}

	private void handleError(GOperationNotAllowedException e) {
		MessageDialog.openError(getViewSite().getShell(), "Error", e.getLocalizedMessage());
	}
	
	/**
	 * Remove an item from the tree. 
	 */
	public void removeItem() {
		ISelection selection = tree.getSelection(); 
		GIMenuItem tmp = (GIMenuItem) ((IStructuredSelection) selection).getFirstElement(); 
		removeItem(tmp, selection);
	}
	
	
	private GIMenuItem getParent(GIMenuItem dropee, TreeSelection selection) {
		TreePath[] treePath = selection.getPathsFor(dropee);
		if (treePath.length!=1 || treePath[0].getParentPath()==null) {
			return null;
		}
		TreePath parentPath = treePath[0];
		GIMenuItem parent = (GIMenuItem) parentPath.getParentPath().getLastSegment();
		return parent;
	} 
	
	@Override
	public void setViewFocus() {
	}
	
//	private class menuSorter extends ViewerComparator {
//
//		@Override
//		public int compare(Viewer viewer, Object e1, Object e2) {
//			
//			GIMenuItem item1 = (GIMenuItem) e1;
//			GIMenuItem item2 = (GIMenuItem) e2;
//
//			/** Direction according to folders sort order. */
//			int direction = 1;
//			if (item1.getParent().getSortOrder() == Order.ORDER_DESCENDING)
//				direction = -1;
//			
//			/** Always folders before items. */
//			if (item1.getType() == ItemType.MENU_FOLDER && item2.getType() == ItemType.MENU_ITEM)
//				return -1;
//			if (item1.getType() == ItemType.MENU_ITEM && item2.getType() == ItemType.MENU_FOLDER)
//				return 1;
//			if (item1.getType() == ItemType.MENU_ITEM && item2.getType() == ItemType.MENU_ITEM)
//				return item1.getLabel().compareToIgnoreCase(item2.getLabel()) * direction;
//			if (item1.getType() == ItemType.MENU_FOLDER && item2.getType() == ItemType.MENU_FOLDER)
//				return item1.getLabel().compareToIgnoreCase(item2.getLabel()) * direction;
//			return 0;
//		}
//	}
	
	private class MenuDropListener extends ViewerDropAdapter {

		public MenuDropListener(TreeViewer viewer) {
			super(viewer);
			setFeedbackEnabled(false);
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean performDrop(Object data) { 
			
			if (data instanceof TreeSelection) {
				TreeSelection selection = (TreeSelection) data;
				GIMenuItem dropee = (GIMenuItem) selection.getFirstElement();
	
				GIMenuItem parent = getParent(dropee, selection);
				if (parent==null)
					return false;
				
			
				//GIMenuItem dropee = (GIMenuItem) getSelectedObject();
				GIMenuItem target = (GIMenuItem) getCurrentTarget();
				TreeViewer viewer = tree;
				
				if (dropee != null && target != null) {
					/**
					 * If 'copy' is selected we should make a clone
					 * of the selected item. If we do not the tree 
					 * gets inconsistent when we delete an item.
					 */
					if (getCurrentOperation() == DND.DROP_COPY) {
						try {
							menuitems.copyItem(dropee, target);
						} catch (GOperationNotAllowedException e) {
							handleError(e);
						}
						viewer.expandToLevel(target, 1);
					}
					
					/**
					 * If 'move' is selected we should remove the selected 
					 * item. But then we have to select something else first.
					 * Select the target folder as it is highlighted anyway.
					 */
					if (getCurrentOperation() == DND.DROP_MOVE) {
						try {
							menuitems.moveItem(dropee, parent, target);
						} catch (GOperationNotAllowedException e) {
							handleError(e);
						}
						//tree.setInput( menuitems.getTopMenu() );
						
						viewer.setSelection(new StructuredSelection(target), true);
						viewer.expandToLevel(target, 1);
					}
					viewer.refresh();
					return true;
				}
				else
					return false;
			}
			else if (data instanceof StructuredSelection) {
				StructuredSelection selection = (StructuredSelection) data;
				GIMenuItem target = (GIMenuItem) getCurrentTarget();
				Iterator iterator = selection.iterator();
				while(iterator.hasNext()) {
					Object obj = iterator.next();
					if (obj instanceof GIElement) {
						try {
							menuitems.addElement((GIElement) obj, target);
						} catch (GOperationNotAllowedException e) {
							handleError(e);
						}
					}
				}
				tree.setSelection(new StructuredSelection(target), true);
				tree.expandToLevel(target, 1);
				tree.refresh();
				
			}
			return false;
		}

		
//		/**
//		 * Copy the contents of a folder from source to target.
//		 */
//		private void copyFolder(GIMenuItem source, GIMenuItem target) {
//			ArrayList<GIMenuItem> list = source.getChildren();
//			
//			for (int i=0; i<list.size(); i++) {
//				System.out.println("COPY: " + list.get(i).getLabel() + " to " + target.getLabel());
//				target.addItem(list.get(i).getIdentifier(), list.get(i).getType(), list.get(i).getLabel());
//				tree.expandToLevel(target, 1);
//				if (list.get(i).getType() == ItemType.MENU_FOLDER) {
//					copyFolder(list.get(i), target.getItem(list.get(i).getIdentifier())); 
//				}
//			}
//		}
		
		
		
		/**
		 * Things we should check for:
		 * - You should not be able to drop a parent into a child.
		 * - You should only be allowed to drop on folders.
		 * - 
		 */
		@SuppressWarnings("unchecked")
		@Override
		public boolean validateDrop(Object target, int operation, TransferData transferType) {
			//System.out.println("validate drop: ");
			
			ISelection selection = LocalSelectionTransfer.getTransfer().getSelection();
			
			/**
			 * Only allow dropping into valid targets.
			 */
			if (target == null || !(target instanceof GIMenuItem)) {
				return false;
			}
			
			
			if (selection instanceof StructuredSelection) {
				StructuredSelection selection1 = (StructuredSelection) selection;
				Iterator iterator = selection1.iterator();
				while(iterator.hasNext()) {
					Object obj = iterator.next();
					if (obj==target || 
						!menuitems.dropAllowed(obj, (GIMenuItem) target) ||
						!LocalSelectionTransfer.getTransfer().isSupportedType(transferType))
						return false;
				}
				return true;
				
			}
			return false;
		}

//		/**
//		 * See if the source already exists in the target folder. 
//		 * @param source
//		 * @param target
//		 * @return
//		 */
//		public boolean isInFolder(GIMenuItem source, GIMenuItem target) {
//			ArrayList<GIMenuItem> items = target.getChildren();
//			if (items != null) {
//				boolean found = false;
//				for (int index=0; index<items.size(); index++) {
//					if (((GIMenuItem)items.get(index)).getIdentifier().equals(source.getIdentifier())) {
//						found = true;
//						break;
//					}
//				}
//				return found;
//			}
//			else
//				return false;
//		}
//		
//		/**
//		 * See if the target is a sub folder of the source.
//		 * 
//		 * @param source Folder to be dropped.
//		 * @param target Folder to drop into.
//		 * @return Boolean indicating if the target is a sub folder of the source.
//		 */
//		public boolean isSubfolder(GIMenuItem source, GIMenuItem target) {
//			boolean match = false;
//			
//			if (source.getIdentifier() == target.getIdentifier()) {
//				match = true;
//			}
//			else {
//				if (target.getParent() != null) { 
//					boolean tmp = isSubfolder(source, target.getParent());
//					if (tmp)
//						match = true;
//				}
//			}
//			return match;
//		}	
	}
	
	private class MenuDragListener extends DragSourceAdapter {
		
		private TreeViewer viewer;
		
		/**
		 * Initialization
		 */
		public MenuDragListener(TreeViewer viewer) {
			this.viewer = viewer;
		}

		/**
		 * Method declared on DragSourceListener
		 * Called when dragging is started.
		 */
		public void dragStart(DragSourceEvent event) {
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			if (selection.size()==1) {
				Object obj = selection.getFirstElement();
				if (obj instanceof GIMenuItem && ((GIMenuItem) obj).getType()!=GIMenuItemHandling.ItemType.AUTO_ITEM) {
					LocalSelectionTransfer.getTransfer().setSelection(viewer.getSelection());
					return;
				}		
			}
			event.doit = false;
		}

		/**
		 * Method declared in DragSourceListener.
		 * Called when dragging has finished (drop).
		 */
		public void dragFinished(DragSourceEvent event) {
		}
	}
	
	private static ILabelProvider menuLabelProvider(){
		return new ILabelProvider() {
			public Image getImage(Object element) {
				
				GIMenuItem tmp = (GIMenuItem) element;
				
				if (tmp!=null) {
	
					if (tmp.getType() == ItemType.MENU_FOLDER)
						return JFaceResources.getImageRegistry().get("G_FOLDER_ICON");
					else if(tmp.getType() == ItemType.AUTO_MENU)
						return JFaceResources.getImageRegistry().get("G_AUTO_FOLDER_ICON");
					else if(tmp.getType() == ItemType.MENU_ITEM)
						return JFaceResources.getImageRegistry().get("G_MENU_ACTION_ICON");
				}
				return null;
			}
			
			public String getText(Object element) {
				if (element instanceof GIMenuItem) {
					GIMenuItem gmi = (GIMenuItem) element;
					return gmi.getLabel();
				}
				return "?";
			}
			public void addListener(ILabelProviderListener listener) {}
			public void dispose() {}
			public boolean isLabelProperty(Object element, String property) {return false;	}
			public void removeListener(ILabelProviderListener listener) {}
		};
	}


	private static ITreeContentProvider menuContentProvider(){
		return new ITreeContentProvider(){
			
			public Object[] getChildren(Object parentElement) {
				if (parentElement instanceof GIMenuItem) {
					GIMenuItem tmp = (GIMenuItem) parentElement;
					return tmp.getChildren().toArray();
				}
				return null;
			}

			public Object getParent(Object element) {return null;}
			
			public boolean hasChildren(Object element) {
				if (element instanceof GIMenuItem) {
					GIMenuItem tmp = (GIMenuItem) element;
					if (tmp.getChildren() != null && !tmp.getChildren().isEmpty()) {
						return true;
					}
				}
				return false;
			}

			public Object[] getElements(Object inputElement) {
				if (inputElement instanceof GIMenuItem) {
					GIMenuItem tmp = (GIMenuItem) inputElement;
					return tmp.getChildren().toArray();
				}
				return null;
			}

			public void dispose() {}

			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}
			
		};
	}



	@Override
	public void elementChanged(String id) {
		refresh();
	}



	@Override
	public void elementCreated(String id) {
		refresh();
		
	}



	@Override
	public void elementDeleted(String id) {
		refresh();
		
	}
}
