package gon_client_management.view.element;

import gon_client_management.model.GILicenseConfig;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIElement;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.element.listing.ElementListingFilter;
import gon_client_management.view.util.GGuiUtils;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;


public class GLicensedUserView extends UserView {

	
	private static final String LICENSED_USERS = "Licensed Users";
	
	private String currentFilter = LICENSED_USERS;
	private int currentFilterIndex = 0;

	private GILicenseConfig licenseConfig;

	private String[] restrictedLaunchTypeCategories;

	private Combo comboField = null;
	
	
	
	public GLicensedUserView() {
		super();
		licenseConfig = GModelAPIFactory.getModelAPI().getLicenseConfig();	
		restrictedLaunchTypeCategories = licenseConfig.GetRestrictedLaunchTypeCategories();

		//		restrictedLaunchTypeCategories = new String[0]; // For test
	}

	@Override
	public boolean showGOnUserToggleButton() {
		return false;
	}
	
	@Override
	public void createCustomFilter(final ElementListingFilter elementListingFilter, Composite parent) {


		if (restrictedLaunchTypeCategories.length > 0) {
			final Composite compositeBody = GGuiUtils.createNoMarginsComposite(parent, 2);
			
			
			new Label(compositeBody, SWT.LEFT).setText("Show:  ");
			comboField  = new Combo(compositeBody, SWT.READ_ONLY);
			comboField.add(LICENSED_USERS);
			for(String category: restrictedLaunchTypeCategories) {
				comboField.add(category + " Users");
				
			}
	//		comboField.setText("Licensed Users");
			GridData buttonGridData = new GridData();
//			buttonGridData.horizontalSpan = 2;
			elementListingFilter.setDoSearch(false);
			comboField.setLayoutData(buttonGridData);
			comboField.addSelectionListener(new SelectionListener() {
	
				public void handleEvent(SelectionEvent e) {
					doFilterSearch();
				}
	
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					handleEvent(e);
				}
	
				@Override
				public void widgetSelected(SelectionEvent e) {
					handleEvent(e);
					
				}
			});
			comboField.select(0);
		}
		else {
			new Label(parent, SWT.NONE);
		}
	}
	
	private boolean removeActionShown = false;
	
	@Override
	public void createView(Composite parent) {
		super.createView(parent);
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		// Create menu.
		Menu menu = menuMgr.createContextMenu(listing.elementViewer.getControl());
		listing.elementViewer.getControl().setMenu(menu);
		
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				if (removeActionShown) {
					if (currentFilter.equals(LICENSED_USERS) || listing.getSelectionCount()==0) {
						mgr.remove(removeUserFromLaunchCategoryAction.getId());
						removeActionShown = false;
					}
					
				}
				else {
					if (!currentFilter.equals(LICENSED_USERS) && listing.getSelectionCount()>0) {
						mgr.add(removeUserFromLaunchCategoryAction);
						removeActionShown = true;	
					}
				}
			}
		});
		
		// Register menu for extension.
		getSite().registerContextMenu("rule_listing_popup", menuMgr, listing.elementViewer);
	}
	
	protected Action removeUserFromLaunchCategoryAction = new Action() {

		@Override
		public String getText() {
			return "Remove from " + currentFilter;
		}

		@Override
		public boolean isEnabled() {
			return !currentFilter.equals(LICENSED_USERS) && editEnabled();
		}

		@Override
		public String getId() {
			return "REMOVE_USERS_FROM_LAUNCH_TYPE_CATEGORY";
		}

		
		@Override
		public void run() {
			ArrayList<GIElement> selection = listing.getSelection();
			if (selection.size()>0) {
				if (selection.size()==1) {
					licenseConfig.removeUserFromLaunchTypeCategory(selection.get(0), restrictedLaunchTypeCategories[currentFilterIndex-1]);
				}
				else {
					statusline.setMessage("Deleting the selected element(s)");
					ArrayList<String> errorMessages = new ArrayList<String>();
					for (GIElement element : selection) {
						boolean ok = licenseConfig.removeUserFromLaunchTypeCategory(element, restrictedLaunchTypeCategories[currentFilterIndex-1]);
						if (!ok) 
							errorMessages.add(element.getLabel() + " : was not removed" );
					}
					GGuiUtils.showMultipleErrors(errorMessages);
				}
				refresh();
			}
			
			
			
		}


		@Override
		public ImageDescriptor getImageDescriptor() {
			return null;
		}
	};

	private void refresh1() {
		super.refresh();
		
	}
	
	public void refresh() {
		GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
			
			@Override
			public void run() {
				refresh1();
			}
			
		});
	}
	
	

	public void addCustomHeaderActions(IToolBarManager toolBarManager) {
	}

	public void updateFilter(String category) {
		if (comboField!=null) {
			String newText = category + " Users";
			String[] items = comboField.getItems();
			boolean found = false;
			for (String itemText : items) {
				if (itemText.equalsIgnoreCase(newText)) {
					found = true;
					newText = itemText;
					break;
				}
			}
			if (!found) {
				newText = LICENSED_USERS;
			}
			String currentText = comboField.getText();
			if (!currentText.equals(newText)) {
				comboField.setText(newText);
				doFilterSearch();
			}
			
		}
	}

	private void doFilterSearch() {
		currentFilter = comboField.getText();
		currentFilterIndex = comboField.getSelectionIndex();
		
		if (currentFilter.equals(LICENSED_USERS)) {
//						elementListingFilter.doSearch = false;
			modelAPI.search("__REGISTERED_USERS__");
			refresh();
		}
		else {
			int categoryIndex = currentFilterIndex-1;
			if (categoryIndex>=0 && categoryIndex < restrictedLaunchTypeCategories.length) {
//							elementListingFilter.doSearch = false;
				modelAPI.search("__REGISTERED_USERS__" + restrictedLaunchTypeCategories[categoryIndex]);
				refresh();
			}
		}
		removeUserFromLaunchCategoryAction.setText(currentFilter);
		removeUserFromLaunchCategoryAction.setEnabled(currentFilterIndex>0);
	}

	protected String getStartingFilter() {
		return "__REGISTERED_USERS__";
	}
	
}
