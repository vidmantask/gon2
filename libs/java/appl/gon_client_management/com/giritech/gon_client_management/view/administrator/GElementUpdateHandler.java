package gon_client_management.view.administrator;

import gon_client_management.model.GIModelElement;
import gon_client_management.model.server.GServerInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.widgets.Display;

public class GElementUpdateHandler {
	
	final public static String USER_RULE = "__USER_RULE__"; 
	
	public interface GIElementListener {

		public void elementCreated(String id);
		
		public void elementChanged(String id);

		public void elementDeleted(String id);
		
	}
	
	private HashMap<String, GIModelElement> fetchedElements = new HashMap<String, GIModelElement>();
	
	public static Display getDisplay() {
	      Display display = Display.getCurrent();
	      //may be null if outside the UI thread
	      if (display == null)
	         display = Display.getDefault();
	      return display;		
	   }


	private static GElementUpdateHandler handler = null;
	private Map<String, List<GIElementListener> > listeners = new HashMap<String, List<GIElementListener> >();

	private List<GIElementListener> getListenersForType(String entityType) {
		List<GIElementListener> list = listeners.get(entityType);
		if (list==null) {
			list = new ArrayList<GIElementListener>();
			listeners.put(entityType, list);
		}
		return list;
	}

	public void addElementListener(List<String> entityTypes, GIElementListener listener) {
		
		for (String entityType : entityTypes) {
			getListenersForType(entityType).add(listener);
		}
	}
	
	public void addElementListener(String entityType, GIElementListener listener) {
		getListenersForType(entityType).add(listener);

	}
	
	public void removeElementListener(List<String> entityTypes, GIElementListener listener) {
		for (String entityType : entityTypes) {
			getListenersForType(entityType).remove(listener);
		}
	}
	
	public void removeElementListener(String entityType, GIElementListener listener) {
		getListenersForType(entityType).remove(listener);

	}

	private GElementUpdateHandler() {
		
	}
	
	public static GElementUpdateHandler getElementUpdateHandler() {
		if (handler==null) {
			handler = new GElementUpdateHandler();
		}
		return handler;
	}
	
	public void fireElementCreated(String entityType, String id) {
		if (id!=null) {
			String elementId = entityType + "."  + id; 
			elementCreated(entityType, elementId);
		}
		else
			elementCreated(entityType, null);
	}
	private void elementCreated(String entityType, final String elementId) {
		List<GIElementListener> listeners = getListenersForType(entityType);
		for (final GIElementListener l : listeners)
			getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					l.elementCreated(elementId);
				}
				
			});
		
	}
	
	public GIModelElement getElement(String selectedElementId) {
		if (selectedElementId==null)
			return null;
		GIModelElement element = fetchedElements.get(selectedElementId);
		if (element!=null)
			return element;
		else {
			String[] elementIdAndEntityType = selectedElementId.split("\\.",2);
			String elementId = elementIdAndEntityType[1];
			String entityType = elementIdAndEntityType[0];
			HashMap<String, Set<String>> elementMap = new HashMap<String, Set<String>>();
			Set<String> elementIdSet = new HashSet<String>();
			elementIdSet.add(elementId);
			elementMap.put(entityType, elementIdSet);
			List<GIModelElement> specificElements = GServerInterface.getServer().getSpecificElements(elementMap);
			if (specificElements.size()==1) {
				GIModelElement newElement = specificElements.get(0);
				fetchedElements.put(selectedElementId, newElement);
				return newElement;
			}
			else if (specificElements.size()==0) 
				return null;
			else
				throw new RuntimeException("Found multiple elements with id=" + selectedElementId);
		}
		
	}

	public void fireElementUpdated(String entityType, final String id) {
		fetchedElements.remove(id);
		List<GIElementListener> listeners = getListenersForType(entityType);
		for (final GIElementListener l : listeners)
			getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					l.elementChanged(id);
				}
				
			});
		
	}
	
	public void fireElementDeleted(String entityType, final String id) {
		fetchedElements.remove(id);
		List<GIElementListener> listeners = getListenersForType(entityType);
		for (final GIElementListener l : listeners)
			getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					l.elementDeleted(id);
				}
				
			});
		
	}
	
	

}
