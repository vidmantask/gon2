package gon_client_management.view.rule.editor;

import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.provider.GRuleActiveCheckBoxStateProvider;
import gon_client_management.view.rule.RuleView;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.forms.widgets.Section;

/**
 * A generic rule editor for rules specified by the current model API.
 * 
 * @author Giritech, cso
 *
 */
public class RuleEditor {
	
	public RuleView parent = null;
	public Section section = null;
	public CheckboxTableViewer viewer = null;
	public Button discardChangesButton = null;
	public Button addRuleButton = null;
	public Button addAndCloseButton = null;
	
	public RuleEditorContentProvider contentProvider = null;
	
	private GIRule currentEditingRule = null;
	private TableCursor cursor = null;
	
//	private static final String RULE_EDITOR_CONTEXT = "com.giritech.ruleeditor.context";
	
	/**
	 * Create a rule editor for rules specified by the GIRulePane.
	 * 
	 * @param parentView
	 * @param modelAPI
	 */
	public RuleEditor(final RuleView parent) {

		this.parent = parent;
		this.contentProvider = new RuleEditorContentProvider(parent.modelAPI);
		
		/* Create an area for the editor. */
		this.section = parent.toolkit.createSection(parent.ruleHandlingForm.getBody(), Section.TITLE_BAR);
		section.setText("Edit rule");
		section.setLayout(new GridLayout());
		GridData ruleEditorSectionGridData = new GridData(GridData.FILL_HORIZONTAL);
		section.setLayoutData(ruleEditorSectionGridData);

		/* Set the area to be initially invisible. */
		section.setVisible(false);
		ruleEditorSectionGridData.exclude = true;
		
		/* Create a container for the rule editor. */
		Composite ruleEditorContainer = parent.toolkit.createComposite(section);
		section.setClient(ruleEditorContainer);
		GridLayout ruleEditorContainerGridLayout = new GridLayout();
		ruleEditorContainerGridLayout.numColumns = 3;
		ruleEditorContainer.setLayout(ruleEditorContainerGridLayout);
		GridData ruleEditorContainerGridData = new GridData(GridData.FILL_BOTH);
		ruleEditorContainer.setLayoutData(ruleEditorContainerGridData);

		ruleEditorContainer.addControlListener(containerResizeListener);
		
		// Create a view for editing a rule.
		Table table = new Table(ruleEditorContainer, SWT.BORDER | SWT.FILL); //| SWT.CHECK);
//		this.viewer = new TableViewer(ruleEditorContainer, SWT.FULL_SELECTION | SWT.BORDER | SWT.FILL | SWT.CHECK);
		viewer = new CheckboxTableViewer(table);
		viewer.setContentProvider(contentProvider);
		viewer.setCheckStateProvider(new GRuleActiveCheckBoxStateProvider());
		
		parent.toolkit.adapt(viewer.getTable(), true, true);
		RuleEditorLabelProvider editorLabels = new RuleEditorLabelProvider(parent.modelAPI);
		editorLabels.createColumns(viewer);
		viewer.getTable().setHeaderVisible(true);
		viewer.setInput(parent.getViewSite());
		viewer.getTable().setLayout(new GridLayout());
		GridData ruleEditorViewGridData = new GridData(GridData.FILL_BOTH);
		ruleEditorViewGridData.horizontalSpan = 3;
		ruleEditorViewGridData.heightHint = 50;
		viewer.getTable().setLayoutData(ruleEditorViewGridData);

		/* Create a button that will discard changes made in the editing area. */
		this.discardChangesButton = parent.toolkit.createButton(ruleEditorContainer, "Close", SWT.PUSH);
		discardChangesButton.addListener(SWT.Selection, discardChangesListener);
		GridData discardChangesButtonGridData = new GridData();
		discardChangesButtonGridData.horizontalAlignment = SWT.RIGHT;
		discardChangesButtonGridData.grabExcessHorizontalSpace = true;
		discardChangesButton.setLayoutData(discardChangesButtonGridData);
		
		/* Create a button that will save and close. */
		this.addAndCloseButton = parent.toolkit.createButton(ruleEditorContainer, "Save and Close", SWT.PUSH);
		addAndCloseButton.addListener(SWT.Selection, AddRuleAndCloseListener);
		GridData addAndCloseButtonGridData = new GridData();
		addAndCloseButtonGridData.horizontalAlignment = SWT.RIGHT;
		addAndCloseButton.setLayoutData(addAndCloseButtonGridData);
		
		/* Create a button that will add the rule to the model. */
		this.addRuleButton = parent.toolkit.createButton(ruleEditorContainer, "Save", SWT.PUSH);
		addRuleButton.addListener(SWT.Selection, addRuleListener);
		GridData addRuleButtonGridData  = new GridData();
		addRuleButtonGridData.horizontalAlignment = SWT.RIGHT;
		addRuleButton.setLayoutData(addRuleButtonGridData);

		/* Add element drop support. */
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer()};		
		viewer.addDropSupport(DND.DROP_COPY | DND.DROP_MOVE, transfers, new RuleEditorDropAdapter(this));
		
		/* Setup context menu. */
	    MenuManager menuMgr = new MenuManager();
		this.parent.getSite().registerContextMenu("rule_editor_popup", menuMgr, viewer);
		Control control = viewer.getControl();
		Menu menu = menuMgr.createContextMenu(control);
	    control.setMenu(menu);
	    
	    /**
	     * Functionality for removing a single element in the rule.
	     */
	    this.cursor = new TableCursor(viewer.getTable(), SWT.NONE);
	    final ControlEditor editor = new ControlEditor(cursor);
	    editor.grabHorizontal = true;
        editor.grabVertical = true;
	    
	    cursor.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				/**
				 * If we can change it - open the drop-down...
				 */
				if (cursor.getColumn() != contentProvider.getRule().getRuleElementCount()) {
					
					/**
					 * Create a drop-down for selecting the empty element instead of the current. 
					 */
					final Combo choice = new Combo(cursor, SWT.DROP_DOWN);
					choice.setLayoutData(new GridData());
					choice.setText(cursor.getRow().getText(cursor.getColumn()));
					choice.add("<remove>");
					editor.setEditor(choice);
					choice.setFocus();
					
					/**
					 * Do something when an element in the drop-down is selected.
					 */
					choice.addSelectionListener(new SelectionAdapter() {
	
						@Override
						public void widgetSelected(SelectionEvent e) {

							if (cursor.getColumn() < contentProvider.getRule().getRuleElementCount())
								contentProvider.removeElementFromRule(contentProvider.getRule().getElement(cursor.getColumn()));
							else 
								contentProvider.removeElementFromRule(contentProvider.getRule().getResultElement());
							choice.dispose();
							
							viewer.refresh();
							updateButtonEnabling(contentProvider.getRule());
						}
	
						@Override
						public void widgetDefaultSelected(SelectionEvent e) {
							choice.dispose();
						}
					});
					/**
					 * Clean up when the user looses interest...
					 */
					choice.addFocusListener(new FocusAdapter() {
	
						@Override
						public void focusLost(FocusEvent e) {
							choice.dispose();
						}
					
					});
				}
			}
		});
	    
		viewer.addCheckStateListener(new ICheckStateListener() {

				public void checkStateChanged(CheckStateChangedEvent event) {
					Object element = event.getElement();
					if (element instanceof GIRule) {
						contentProvider.setRuleActive(event.getChecked());
						updateButtonEnabling((GIRule) element);
					}
				}
			}
		);
	}

	/**
	 * Activate or de-activate a rule depending on current state.
	 * @param rule that may get activated or de-activated.
	 */
	public void toggleRuleActivation(GIRule rule) {
		if (rule==null)
			return;
		if (rule.editEnabled()) {
			if (rule.isActive())
				parent.statusline.setMessage("Deactivate the selected rule");
			else
				parent.statusline.setMessage("Activate the selected rule");

			contentProvider.setRuleActive(!rule.isActive());
			updateButtonEnabling(rule);
			viewer.refresh();
		}
		else {
			parent.statusline.setMessage("The selected rule can not be edited");
		}
	}
	
	/**
	 * Start editing a selected rule.
	 * @param rule
	 */
	public void edit(GIRule rule) {
		/* Set the rule to be edited. */
		currentEditingRule = rule;
		parent.statusline.setMessage("");
		if (rule != null) {
			if (rule.editEnabled()) {
				section.setText("Edit Rule");
				contentProvider.setRuleEditorContent(rule);
				viewer.refresh();
			}
			else {
				parent.statusline.setMessage("The selected rule can not be edited");
			}
		}
		else {
			section.setText("Add Rule");
			clear();
		}
		
		/* Select the arrow to avoid a box on top of the 
		 * first column (Weird!).
		 */
		cursor.setSelection(0, contentProvider.getRule().getRuleElementCount());
		
		section.layout();
		show();
	}
	
	/**
	 * Pack the elements in the table, so that they are all visible.
	 */
	public void layout() {
		int fullWidth = viewer.getTable().getClientArea().width;
		int featuresWidth = 20; // Three times icons width.
		int colWidth = (fullWidth - featuresWidth) / (viewer.getTable().getColumnCount() - 1);
		
		for (int i=0; i<viewer.getTable().getColumnCount(); i++) {
			if (i !=viewer.getTable().getColumnCount()-2) {
				viewer.getTable().getColumn(i).setWidth(colWidth);
			}
		}
	}
	
	/**
	 * Called when an element added from the element view.
	 */
	public void add(GIElement element) {
		show();
		contentProvider.addElementToRule(element);
		viewer.refresh();
		GIRule rule = contentProvider.getRule();
		updateButtonEnabling(rule);
		/* Select the arrow to avoid a box on top of the 
		 * first column (Weird!).
		 */
		cursor.setSelection(0, rule.getRuleElementCount());
	}

	void updateButtonEnabling(GIRule rule) {
		boolean validRule = parent.modelAPI.isValidRule(rule);
		addRuleButton.setEnabled(validRule);
		addAndCloseButton.setEnabled(validRule);
	}

	public boolean isShowing() {
		return section.getVisible();
	}
	
	/**
	 * Show the editor. 
	 *  If not already visible - clear and display the editor.
	 *   
	 */
	private void show() {
		if (!isShowing()) {
			addRuleButton.setEnabled(false);
			addAndCloseButton.setEnabled(false);
			section.setVisible(true);
			GridData tmp = (GridData) section.getLayoutData();
			tmp.exclude = false;
			parent.ruleHandlingForm.getBody().layout();
			parent.statusline.setMessage("");
			layout();
		}
	}

	private void hide() {
		clear();
		GridData tmp = (GridData) section.getLayoutData();
		tmp.exclude = true;
		section.setVisible(false);			
		parent.ruleHandlingForm.getBody().layout(false);
	}
	
	/**
	 * Clear the editor.
	 */
	private void clear() {
		contentProvider.clearRuleEditor();		
		viewer.refresh();
	}

	/**
	 * A listener for when the size of the listing container changes.
	 * This will result in resizing the label column accordingly. 
	 */
	public ControlListener containerResizeListener = new ControlListener() {
		public void controlMoved(ControlEvent e) { 
			layout();
		}
		public void controlResized(ControlEvent e) { 
			layout();
		}
	};
	
	/**
	 * Listener for a button that will discard changes in the
	 * editing area. 
	 */
	public Listener discardChangesListener = new Listener() {
		public void handleEvent(Event event) {
			parent.statusline.setMessage("Discarded changes");			
			hide();
		}
	};

	/**
	 * Listener for a button that will add a rule to the model.
	 */
	public Listener addRuleListener = new Listener() {
		
		public void handleEvent(Event event) {

			int selectionIndex;
			
			final GIRule savedRule;
			try {
				savedRule = contentProvider.saveRule(currentEditingRule);
			} catch (GOperationNotAllowedException e) {
				MessageDialog.openError(null, "Error", e.getMessage());
				return;
			}
			/* See if the rule should replace an existing rule. */
			if (currentEditingRule!=null) {
				parent.statusline.setMessage("Replaced rule");				
				currentEditingRule = savedRule;

			}
			else {
				parent.statusline.setMessage("Saved rule");		
				parent.listing.show(true);
			}

			/* Setup the display for viewing existing rules. */
			parent.listing.checkBoxRuleTableView.refresh();

			/* Select the rule that was just created. */
			selectionIndex = parent.getRuleRowIndex(savedRule);
			parent.listing.checkBoxRuleTableView.getTable().setSelection(selectionIndex);
			parent.listing.checkBoxRuleTableView.getTable().showSelection();
			
			
		}
	};

	/**
	 * Listener for a button that will add a rule and then close the 
	 * editor. 
	 */
	public Listener AddRuleAndCloseListener = new Listener() {
		public void handleEvent(Event event) {
			addRuleListener.handleEvent(event);
			hide();
		}
	};
}
