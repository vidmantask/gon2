package gon_client_management.view;

import gon_client_management.Activator;
import gon_client_management.GViewLicenceInfoHandler;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.GILicenseConfig;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.administrator.GElementUpdateHandler.GIElementListener;
import gon_client_management.view.element.GLicensedUserView;
import gon_client_management.view.ext.GSafeView;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import com.giritech.admin_ws.LicenseCountType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;

public class GLicenseView extends GSafeView implements GIElementListener {

	private Label licenseNumberText;
	private Label licenseExpirationText;
	private Label licenseOwnerText;
	private Table table;
	int count = 0;
	
	private String[] titles = { "License restriction type", "No. Licensed", "No. Used"};
	private GILicenseConfig licenseConfig;
	private LicenseInfoTypeManagement licenseInfo;
	private Text licenseText;;
	
	public GLicenseView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}

	
	@Override
	public void initView(IViewSite site) {
		super.initView(site);
		licenseConfig = GModelAPIFactory.getModelAPI().getLicenseConfig();
		licenseInfo = GViewLicenceInfoHandler.getLicenseInfo();
	}


	@Override
	public void createView(Composite parent) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		Form dialogForm = toolkit.createForm(parent);
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		dialogForm.getBody().setLayout(layout);
		/* Setup the header for this dialog */
		dialogForm.setText("License Information");
		toolkit.decorateFormHeading(dialogForm);
	
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
		dialogForm.setLayoutData(data);

		Composite licensesContainer = toolkit.createComposite(dialogForm.getBody());
		licensesContainer.setLayout(new GridLayout(2, false));
		
		toolkit.createLabel(licensesContainer, "License Number");
		licenseNumberText = toolkit.createLabel(licensesContainer, "a");
		
		toolkit.createLabel(licensesContainer, "License Expiration Date");
		licenseExpirationText = toolkit.createLabel(licensesContainer, "");

		toolkit.createLabel(licensesContainer, "Licensed To");
		licenseOwnerText = toolkit.createLabel(licensesContainer, "");

//		toolkit.createLabel(licensesContainer, "Maintenance Expiration Date");
//		toolkit.createLabel(licensesContainer, getDateString(licenseInfo.getLicense_expires()));

//		licensesContainer.setLayoutData(gridData);
		Label separator = toolkit.createSeparator(licensesContainer, SWT.HORIZONTAL);
		{
			GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
			gridData.horizontalSpan = 2;
			gridData.grabExcessHorizontalSpace = true;
			separator.setLayoutData(gridData);
		}

		table = toolkit.createTable(licensesContainer, SWT.NONE);
		table.setHeaderVisible(true);
		table.setLayout(new GridLayout());
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.horizontalSpan = 2;
//		gridData.grabExcessHorizontalSpace = true;
		table.setLayoutData(gridData);
	    for (int i = 0; i < titles.length; i++) {
	      TableColumn column = new TableColumn(table, SWT.NONE);
//	      column.setWidth(50);
	      column.setText(titles[i]);
	    }
        for (int i=0; i<titles.length; i++) {
            table.getColumn (i).pack ();
        }     
	    
//		Button viewLicenseButton = toolkit.createButton(dialogForm.getBody(), "View License File", SWT.PUSH);
//		viewLicenseButton.setBounds(0, 0, 100, 100);
//		viewLicenseButton.addSelectionListener(new SelectionAdapter() {
//
//			
//			@Override
//			public void widgetSelected(SelectionEvent event) {
//			}
//			
//			
//		});

        
        if (licenseInfo.getUpdate_license_allowed()) {
        
//		toolkit.createLabel(licensesContainer, "Update License File");
			Button updateLicenseButton = toolkit.createButton(dialogForm.getBody(), "Update License File", SWT.PUSH);
			updateLicenseButton.setBounds(0, 0, 100, 100);
			updateLicenseButton.addSelectionListener(new SelectionAdapter() {

				String [] extensions = {"gon_license.lic", "*.lic", "*.*"};
				
				@Override
				public void widgetSelected(SelectionEvent event) {
					FileDialog fileChooser = new FileDialog(getViewSite().getShell(), SWT.OPEN);
					fileChooser.setFileName("gon_license.lic");
					fileChooser.setFilterExtensions(extensions);
					String filePath = fileChooser.open();
					if (filePath==null)
						return;
					String content;
					try {
						content = CommonUtils.convertFileToString(filePath, null, "UTF-8");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
					boolean licenseSet = licenseConfig.setLicense(content);
					if (licenseSet) {
						GViewLicenceInfoHandler.updateLicenseInfo();
						refresh();
					}
					else {
						MessageDialog.openError(getViewSite().getShell(), "Error", "Error updating license");
					}
				}
				
				
			});
        }
        String licenseFileContent = licenseInfo.getLicense_file_content();
        if (licenseFileContent!=null) {
	        Section licenseSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR | Section.TWISTIE);
	        licenseSection.setText("License File");
	        licenseSection.setLayout(new GridLayout());
			GridData masterSectionGridData = new GridData(GridData.FILL_BOTH);
			masterSectionGridData.grabExcessVerticalSpace = true;
//			masterSectionGridData.verticalAlignment = SWT.END;
			licenseSection.setLayoutData(masterSectionGridData);
			final Composite licenseSectionBody = toolkit.createComposite(licenseSection);
			licenseSection.setClient(licenseSectionBody);
			licenseSectionBody.setLayout(new GridLayout());
			GridData contentContainerGridData = new GridData(GridData.FILL_BOTH);
			contentContainerGridData.grabExcessVerticalSpace = true;
			contentContainerGridData.grabExcessHorizontalSpace = true;
			licenseSectionBody.setLayoutData(contentContainerGridData);
	        
	        licenseText = toolkit.createText(licenseSectionBody, licenseFileContent, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP | SWT.READ_ONLY);
			GridData licenseTextGridData = new GridData(GridData.FILL_HORIZONTAL | GridData.FILL_VERTICAL);
	//		licenseTextGridData.horizontalSpan = 2;
			licenseTextGridData.grabExcessHorizontalSpace = true;
			licenseText.setLayoutData(licenseTextGridData);
	        
			licenseSection.layout(true);
        }
        
		dialogForm.getToolBarManager().add(refreshAction);
//		parent.addCustomHeaderActions(parent.elementHandlingForm.getToolBarManager());
		dialogForm.updateToolBar();
        
		List<String> subscriptionTypes = GServerInterface.getServer().getBasicEntityTypes("User");
		subscriptionTypes.addAll(GServerInterface.getServer().getBasicEntityTypes("Token"));
		subscriptionTypes.addAll(GServerInterface.getServer().getBasicEntityTypes("ProgramAccess"));
        GElementUpdateHandler.getElementUpdateHandler().addElementListener(subscriptionTypes, this);
		refresh();
		
		table.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				widgetSelected(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem[] selection = table.getSelection();
				if (selection.length==1) {
					TableItem item = selection[0];
					String text = item.getText().toLowerCase();
					String viewId = null;
					if (text.contains("user")) {
						viewId = "gon_client_management.LicensedUserView";
					}
					else if (text.contains("token")) {
						viewId = "gon_client_management.KeyView";
					}
					else if (text.contains("menu action")) {
						viewId = "gon_client_management.LicensedProgramView";
					}
					if (viewId != null) {
						IViewPart view = getSite().getWorkbenchWindow().getActivePage().findView(viewId);
						if (view!=null) {
							getSite().getWorkbenchWindow().getActivePage().bringToTop(view);
							if (view instanceof GLicensedUserView) {
								((GLicensedUserView) view).updateFilter(item.getText().replace("Max users of ", ""));
							}
						}
						
					}
				}
				
			}
			
		});
		
	}
	
//	IViewPart view = getSite().getWorkbenchWindow().getActivePage().findView("gon_client_management.UserView");
//	if (view!=null)
//	getSite().getWorkbenchWindow().getActivePage().bringToTop(view);
	
	
	
	private TableItem createTableItem(Table table, String title) {
		TableItem item = new TableItem(table, SWT.NONE);
        item.setText(0, title);
        return item;
	}
	
	void setTableItemText(int index, String str1, String str2) {
		TableItem item = table.getItem(index);
		item.setText(1, str1);
		item.setText(2, str2);
	}
	
	private String getDateString(String serverString) {
		if (serverString != null) {
			Date string2date;
			try {
				string2date = CommonUtils.DateTimeConverter.string2date(serverString);
				return CommonUtils.DateTimeConverter.datetime2local(string2date);
			} catch (ParseException e) {
				return "Error parsing date";
			}
			
		}
		else {
			return "N/A";
		}
	}
	
	private IAction refreshAction = new Action("Refresh", ImageDescriptor.createFromImage(JFaceResources.getImage("G_REFRESH_ACTION_ICON"))) {
		public void run() { 
			refresh();
		}
	};

	protected void refresh() {
		GViewLicenceInfoHandler.updateLicenseInfo();
		LicenseInfoTypeManagement licenseInfo2 = GViewLicenceInfoHandler.getLicenseInfo();
		if (licenseInfo2==null) {
			MessageDialog.openError(null, "License Error", "Your G/On License is invalid");
			return;
		}
		licenseInfo = licenseInfo2;
		table.removeAll();
		licenseNumberText.setText(licenseInfo.getLicense_number());
		licenseOwnerText.setText(licenseInfo.getLicensed_to());
		licenseExpirationText.setText(getDateString(licenseInfo.getLicense_expires()));
		
		int i = 0;
		for (LicenseCountType licenseCountType : licenseInfo.getCount_type_license()) {
	        TableItem item = createTableItem(table, licenseCountType.getItem_name());
			item.setText(1, licenseCountType.getLicensed_items().toString());
			item.setText(2, licenseCountType.getActual_items().toString());
			if (licenseCountType.getActual_items().intValue() > licenseCountType.getLicensed_items().intValue())
				item.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_RED));
			else
				item.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_WHITE));
				
	        
			setTableItemText(i, licenseCountType.getLicensed_items().toString(), licenseCountType.getActual_items().toString());
	        i++;
	        
		}
		if (licenseText!=null)
			licenseText.setText(licenseInfo.getLicense_file_content());
		
		count++;
	}


	@Override
	public void setViewFocus() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void elementChanged(String id) {
		refresh();
	}


	@Override
	public void elementCreated(String id) {
		refresh();
		
	}


	@Override
	public void elementDeleted(String id) {
		refresh();
		
	}



}
