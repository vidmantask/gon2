package gon_client_management.view.element;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.view.element.editor.TokenEditorDialog;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * Views are created in MANIFEST.MF as extensions, and
 * added in a perspective. The UserView displays the 
 * G elements from the user element model.
 */ 
public class KeyView extends ElementView {

	public TokenEditorDialog dialog = null; 
	private Composite parent;
	
	public KeyView() {
		super();
	}
	
	@Override
	protected GIConfig createConfig() {
		return GModelAPIFactory.getModelAPI().getConfig(GGlobalDefinitions.TOKEN_TYPE);
	}
	

	/**
	 * Overriding the editor has to happen here to avoid 
	 * getting 'null' failures. First dispose of the old editor
	 * that was created on instantiation. Then create the new 
	 * one, that inherits from the default editor.
	 */
	@Override
	public void createView(Composite parent) {
		super.createView(parent);
		this.parent = parent;
	}

	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.element.token");
	}
	


	
	private TokenEditorDialog getTokenEditorDialog() {
		return new TokenEditorDialog(parent.getShell(), this);			
		
	}

	/**
	 * Create an action for creating new rules.
	 */
	private Action showElementCreationAction = new Action("Add a new token") {

		@Override
		public void run() {
			TokenEditorDialog dialog = getTokenEditorDialog();
			dialog.open();
		}

		@Override
		public ImageDescriptor getImageDescriptor() {
			if (JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON") == null) {
				JFaceResources.getImageRegistry().put("ADD_BUTTON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_add.png").createImage());
			}
			return JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON");
		}

		@Override
		public boolean isEnabled() {
			return createEnabled();
		}
		
		
	};

	/* (non-Javadoc)
	 * @see gon_client_management.view.element.ElementView#getShowElementEditorAction()
	 */
	@Override
	public Action getShowElementEditorAction() {
		return showElementCreationAction;
	}



}
	