package gon_client_management.view.rule;

import gon_client_management.Activator;
import gon_client_management.model.GIRule;
import gon_client_management.model.GIRulePane;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIElement;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.administrator.GElementUpdateHandler.GIElementListener;
import gon_client_management.view.element.ElementView;
import gon_client_management.view.ext.GSafeView;
import gon_client_management.view.rule.editor.RuleEditor;
import gon_client_management.view.rule.heading.RuleHeading;
import gon_client_management.view.rule.listing.RuleListing;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.themes.IThemeManager;

/**
 * A generic viewer for rules and rule creation.
 * 
 * @author Giritech
 *
 */
public class RuleView extends GSafeView implements GIElementListener {

	public FormToolkit toolkit;
	public Form ruleHandlingForm;
	public IStatusLineManager statusline;
	
	public RuleHeading heading; 
	public RuleListing listing;
	public RuleEditor editor;

	public List<Integer> columnWidths = new ArrayList<Integer>();;

	public GIRulePane modelAPI;
	
	/** Contexts for enabling context menu items. Defined in extensions. */
	private static final String RULE_DELETE_ENABLED = "com.giritech.rule.delete.enabled";
	private static final String RULE_EDIT_ENABLED = "com.giritech.rule.edit.enabled";
	private static final String RULE_CREATE_ENABLED = "com.giritech.rule.create.enabled";
	private static final String RULE_CONTEXT = "com.giritech.rule.context";
	
	
	/**
	 * Initialization.
	 */
	public RuleView() {		
		super(Activator.getLogger(), "com.giritech.management.client");
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart#setInitializationData(org.eclipse.core.runtime.IConfigurationElement, java.lang.String, java.lang.Object)
	 */
	@Override
	public void setViewInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		if (data instanceof String) {
			initializeModelAPI(data);
		}
	}
	
	@Override
	public void initView(IViewSite site) {
		super.initView(site);
		if (site.getSecondaryId()!=null && modelAPI==null) {
			initializeModelAPI(site.getSecondaryId());
		}
	}
	
	private void initializeModelAPI(Object data) {
		modelAPI = GModelAPIFactory.getModelAPI().getRulePane((String) data);
		
		GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
		
							@Override
							public void run() {
								modelAPI.refreshData();
							}
							
						});
	}
	

	/**
	 * These contexts are set to allow certain actions to be enabled. For instance
	 * the ELEMENT_ADD_ENABLED is always allowed for elements in an ElementView.
	 * But we do not want it to be allowed for a rule view.
	 */
	public void activateContext() {
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		if (modelAPI.isDeleteEnabled())
			contextService.activateContext(RULE_DELETE_ENABLED);
		if (modelAPI.isCreateEnabled())
			contextService.activateContext(RULE_CREATE_ENABLED);
		if (modelAPI.isEditEnabled())
			contextService.activateContext(RULE_EDIT_ENABLED);
		contextService.activateContext(RULE_CONTEXT);
	}

	
	/**
	 * Called when an element is double clicked in an element view.
	 */
	public void startEditing(GIElement element) {
		if (!editor.isShowing())
			editor.edit(null);
		editor.add(element);
	}

	public void deleteRules(GIRule[] rules) {
		listing.deleteRules(rules);
	}
	
	/**
	 * Could not find any built in row locator, so I 
	 * rolled my own here. 
	 * 
	 * @param rule
	 * @return index for the row or -1 if not found
	 */
	public int getRuleRowIndex(GIRule rule) {
		for (int i=0; i<listing.checkBoxRuleTableView.getTable().getItemCount(); i++) {			
			if (rule==listing.checkBoxRuleTableView.getElementAt(i))
				return i;
		}
		return -1;
	}
	
	/**
	 * Creating the controls for this view part. 
	 */
	public void createView(Composite parent) {
		
		/* Create a master form for all content in this view. */
		this.toolkit = new FormToolkit(parent.getDisplay());
		this.ruleHandlingForm = toolkit.createForm(parent);
		
		/* Create layout for this form.  */
		GridLayout formGridLayout = new GridLayout(); 
		formGridLayout.numColumns = 1;
		ruleHandlingForm.getBody().setLayout(formGridLayout);
		GridData formLayoutGridData = new GridData(GridData.FILL_VERTICAL);
		formLayoutGridData.grabExcessVerticalSpace = true;
		ruleHandlingForm.getBody().setLayoutData(formLayoutGridData);
		
		/* Create a sections */
		this.heading = new RuleHeading(this);
		this.listing = new RuleListing(this);
		this.editor = new RuleEditor(this);
		
		/* Setup general status line access. */
		this.statusline = (IStatusLineManager) getViewSite().getActionBars().getStatusLineManager();

		List<String> subscriptionTypes = modelAPI.getSubscriptionTypes();
		GElementUpdateHandler.getElementUpdateHandler().addElementListener(subscriptionTypes, this);

	    activateContext();

	    stopFetchingRulesAction.setEnabled(!modelAPI.ready());
	    
	    getSite().setSelectionProvider(listing.checkBoxRuleTableView);
	    
	    
	    /* Theme for widgets. */
		if (!getSite().getWorkbenchWindow().getWorkbench().getThemeManager().getCurrentTheme().getId().equals("org.eclipse.ui.defaultTheme")) {
			IThemeManager themeManager = getSite().getWorkbenchWindow().getWorkbench().getThemeManager();
			this.ruleHandlingForm.setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.background.color"));
		}
	}

	public void refreshCurrentElementViews() {
		/*
		 * Temporary refresher for element-views.
		 * Hopefully we can remove this when restructuring the manager.
		 * Seems to work nicely though...
		 *  
		 * */
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewReference[] views = page.getViewReferences();
		
		for(int j=0; j<views.length; j++) {
			if(views[j].getPart(false) != null) {
				if (views[j].getPart(false) instanceof ElementView) {
					ElementView tmp = (ElementView) views[j].getPart(false);
					tmp.refresh();
				}
			}
		}
	}
	
	/**
	 * Refresh the element listing.
	 */
	public void refresh() {
//		GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
//
//			@Override
//			public void run() {
//				modelAPI.refreshData();
//				listing.refresh();
//			}
//			
//		});
		modelAPI.refreshData();
	    stopFetchingRulesAction.setEnabled(!modelAPI.ready());
		listing.refresh();
	}
	
	private void refresh(String id) {
		modelAPI.refreshData(id);
		listing.refresh();
	}
	
	
	/**
	 * Action taken when the 'refresh' menu point is chosen.
	 * Makes sure that the displayed elements reflects what
	 * is on the server.
	 */
	public Action reloadRulesAction = new Action() {
		public void runWithEvent(Event event) {
			Display display = getViewSite().getShell().getDisplay();
			display.asyncExec (new Runnable () {
				public void run () {
					refresh();
				}
			});
		}
	};

	
	

	@Override
	protected void loadState(IMemento memento) {
		if (memento==null)
			return;
		super.loadState(memento);
		int i = 0;
		while (true) {
			String key = "ColumnWidth" + i;
			Integer width = memento.getInteger(key);
			if (width != null)
				columnWidths.add(width);
			else
				break;
			i++;
		}
	}

	@Override
	public void saveState(IMemento memento) {
		TableColumn[] columns = listing.checkBoxRuleTableView.getTable().getColumns();
		for (int i=0; i<columns.length; i++) {
			int width = columns[i].getWidth();
			String key = "ColumnWidth" + i;
			memento.putInteger(key, width);
		}
	}

	public void setViewFocus() {
		getViewSite().getActionBars().updateActionBars();
		listing.layout();
	}
	
	public boolean isShowing() {
		return editor.isShowing();
	}

	@Override
	public void elementChanged(String id) {
		refresh(id);
	}


	@Override
	public void elementCreated(String id) {
	}

	@Override
	public void elementDeleted(String id) {
		if (modelAPI.elementUsed(id)) {
			modelAPI.refreshData();
			listing.refresh();
		}
	}

	//private IAction stopFetchingRulesAction = new Action("Stop", ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_STOP))) {
	private IAction stopFetchingRulesAction = new Action("Stop", ImageDescriptor.createFromImage(JFaceResources.getImage("G_STOP_FETCHING_ACTION_ICON"))) {
		public void run() { 
			modelAPI.stopFetchingElements();
		}
	};
	
	
	public IAction getStopFetchingRulesAction() {
		return stopFetchingRulesAction;
	}

	
	private IAction refreshAction = new Action("Refresh", ImageDescriptor.createFromImage(JFaceResources.getImage("G_REFRESH_ACTION_ICON"))) {
		public void run() { 
			refresh();
		}
	};

	public IAction getRefreshAction() {
		return refreshAction;
	}
	
	
}