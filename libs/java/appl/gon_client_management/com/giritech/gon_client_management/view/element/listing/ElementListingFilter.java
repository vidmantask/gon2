package gon_client_management.view.element.listing;

import gon_client_management.view.util.GGuiUtils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

public class ElementListingFilter {
	
	private ElementListing listing = null;
	public Text inputField = null;
	public Button clearButton = null;
	public String searchCategory;
	private ElementFilter elementTextFilter = null;
	private boolean doSearch = true;
	
	private long eventCount = 0;
	
	/**
	 * Creates a filter that will filter on the input from a
	 * Text widget. The filter works on the given ElementListing.
	 * 
	 * This class is used for setting up all elements needed for
	 * filtering elements in an element list. The filter itself
	 * is placed in the ElementFilter.java class.
	 * 
	 * @param listing on which the filter should work
	 */
	public ElementListingFilter(final ElementListing listing) {
	
		this.listing = listing;

		
		final Composite compositeBody = GGuiUtils.createNoMarginsComposite(listing.elementViewContainer, 4);
		GridData compositeGridData = new GridData(GridData.FILL_HORIZONTAL);
		compositeGridData.horizontalSpan = 2;
		compositeBody.setLayoutData(compositeGridData);

		listing.parent.createCustomFilter(this, compositeBody);
		
		/* Create a filter input text field. */
		this.inputField = listing.parent.toolkit.createText(compositeBody, "", SWT.SEARCH);
		inputField.setMessage("Search                          ");
		inputField.setBackground(PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_INFO_BACKGROUND)); 
		inputField.addListener(SWT.KeyUp, filterListener);
//		GridData filterInputGridData = new GridData();
		GridData filterInputGridData = new GridData(GridData.FILL_HORIZONTAL);
		filterInputGridData.grabExcessHorizontalSpace = true;
		filterInputGridData.horizontalAlignment = GridData.END;
		inputField.setLayoutData(filterInputGridData);

		/* Create a 'clear' button for the filter input. */
		this.clearButton = listing.parent.toolkit.createButton(compositeBody, "Clear", SWT.PUSH);
		clearButton.addListener(SWT.Selection, filterClearListener);

//		listing.parent.createCustomFilter(this, listing);

		/* Create the filter and connect it to the input text field. */
		elementTextFilter = new ElementFilter(inputField);
		
	}
	
	/**
	 * Get the filter so that it can be connected to a view.
	 * @return an element filter
	 */
	public ElementFilter getFilter() {
		return this.elementTextFilter;
	}
	
	/**
	 * A listener for input in the filter text field.
	 * Refreshing the view activates filters and sorting.
	 */
	private Listener filterListener = new Listener() {
		
		String lastText = "";
		
		public void handleEvent(Event event) {
			if (!lastText.equals(inputField.getText())) {
				eventCount++;
				startPostponedFilter(eventCount);
			}
			lastText = inputField.getText();
			
		}
	};
	
	/**
	 * A listener for clearing the filter.
	 */
	private Listener filterClearListener = new Listener() {
		public void handleEvent(Event event) {
			clearFilter();
		}
	};
	
	/**
	 * Clear the filter if nothing is entered or the clear
	 * button is pushed.
	 */
	public void clearFilter() {
		inputField.setText("");
		refreshFilter(eventCount);
	}

	protected void startPostponedFilter(final long eventCount2) {
		Thread t = new Thread() {
			 public void run() {
				try {
			        Thread.sleep(1000);
			      } catch (InterruptedException e) {
			     }
			      Display display = Display.getCurrent();
			      //may be null if outside the UI thread
			      if (display == null)
			         display = Display.getDefault();
			     display.asyncExec(new Runnable() {
			          public void run() {
					     refreshFilter(eventCount2);
			          }
			      });			      
				 
			 }
		};
		t.start();
     }
	
	public static String getServerFilter(String text, String searchCategory) {
		if (searchCategory==null || searchCategory=="")
			return text;
		return "__CATEGORY_SEARCH__" + searchCategory + ":" + text;
	}
	

	public void refreshFilter(long eventCount2) {
		if (eventCount2==eventCount) {
			if (doSearch && listing.parent.modelAPI.searchBeforeFilter()) {
				listing.parent.modelAPI.search(getServerFilter(inputField.getText(), searchCategory));
				listing.parent.refresh();
			}
			listing.updateResultText();
		}
	}

	public boolean isDoSearch() {
		return doSearch;
	}

	public void setDoSearch(boolean doSearch) {
		this.doSearch = doSearch;
	}

	public long getEventCount() {
		return eventCount;
	}
}
