package gon_client_management.view.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IRenderTask;
import org.eclipse.birt.report.engine.api.IReportDocument;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunTask;
import org.eclipse.birt.report.engine.api.PDFRenderOption;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

/**
 * Viewer for reporting. 
 * Establish a connection between a web service and a built in browser.
 *  
 * 
 * @author Giritech, cso
 *
 */
public class PreviewReportAction implements IWorkbenchWindowActionDelegate {
	
	private File temporaryFileDesign = null;
	private File temporaryFileDocument = null;
	private EngineConfig config = null;
    private IReportEngineFactory factory = null;
    private IReportEngine engine = null;
    private ByteArrayOutputStream bos = null;
    private String currentReportExportFilename = "";
	
	/**
	 * The constructor.
	 * The temporary file can be create once and for all.
	 */
	public PreviewReportAction() {
		try {
			this.temporaryFileDesign = File.createTempFile("rep", ".rptdesign");
			this.temporaryFileDesign.deleteOnExit();
			this.temporaryFileDocument = File.createTempFile("rep", ".rptdoc");
			this.temporaryFileDocument.deleteOnExit();
			this.config = new EngineConfig();
		    this.factory = (IReportEngineFactory) Platform.createFactoryObject( IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY );
		    this.engine = factory.createReportEngine( config );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Run the report given by the report specification into the browser.
	 * Write the specification into the temporary file and load it into
	 * the browser. 
	 */
	public void run(Browser browser, Action exportAction, String title, String spec, Map<String, String> reportParameters) {
		final String inSpec = spec;
		final Browser inBrowser = browser;
		final Action inExportAction = exportAction;
		final Map<String, String> inParameters = reportParameters;
		final String inTitle = title;
		
		final Job job = new Job("Processing report: " + inTitle) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					Display display = PlatformUI.getWorkbench().getDisplay();
					/* Remove the browser from the workbench. */
					display.asyncExec(new Runnable() {
						public void run () {
							inBrowser.setVisible(false);
							inExportAction.setEnabled(false);
						}
					});
					/* Write specification to temporary file. */
					FileWriter writer = new FileWriter(temporaryFileDesign);
					writer.write(inSpec);
					writer.close();
					
					/* Setup run and render options and parameters */
					IReportRunnable design = engine.openReportDesign(temporaryFileDesign.getAbsolutePath());
					IRunTask task = engine.createRunTask(design);
					
					/* Add parameters to task */
					
				    Iterator<Map.Entry<String,String>> parametersIterator = inParameters.entrySet().iterator();
				    while (parametersIterator.hasNext()) {
				        Entry<String, String> parameter = parametersIterator.next();
						task.setParameterValue(parameter.getKey(), parameter.getValue());
				    }

					/* Run the report and save result in report document file, for later use */
					task.run(temporaryFileDocument.getAbsolutePath());
					task.close();
					
					/* 
					 * Open the report document, and run
					 */
					IReportDocument reportDocument = engine.openReportDocument(temporaryFileDocument.getAbsolutePath());
					IRenderTask taskRender = engine.createRenderTask(reportDocument);
					HTMLRenderOption taskRenderOptions = new HTMLRenderOption();
					taskRenderOptions.setOutputFormat("html");
					taskRenderOptions.setEnableAgentStyleEngine( true );
					
					bos = new ByteArrayOutputStream();
					taskRenderOptions.setOutputStream(bos);
					taskRender.setRenderOption(taskRenderOptions);
					taskRender.render();
					taskRender.close();
					reportDocument.close();

					
					/* Display the report in a browser. */
					display.asyncExec(new Runnable() {
						public void run () {
							inBrowser.setVisible(true);
							try {
								inBrowser.setText(bos.toString("UTF-8"));
							} catch (UnsupportedEncodingException e) {
								inBrowser.setText(bos.toString());
							}
							inExportAction.setEnabled(true);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					monitor.done();
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}		
			
	/**
	 * Test method to if it is possible to do some exporting. 
	 */
	public void exportToPortableDocFormat() {

		final Job job = new Job("Exporting report to pdf") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					/* Open report document */
					IReportDocument reportDocument = engine.openReportDocument(temporaryFileDocument.getAbsolutePath());
					IRenderTask taskRender = engine.createRenderTask(reportDocument);
					PDFRenderOption taskRenderOptions = new PDFRenderOption();
					taskRenderOptions.setOutputFormat(HTMLRenderOption.OUTPUT_FORMAT_PDF);

					/* Get a path to put the exported file into. Use sync (not async) to make export wait for a filename. */
					Display display = PlatformUI.getWorkbench().getDisplay();
					display.syncExec(new Runnable() {
						public void run () {
							Shell[] tmp = PlatformUI.getWorkbench().getDisplay().getShells();
							FileDialog fileSelector = new FileDialog(tmp[0], SWT.SAVE);
							fileSelector.setFilterExtensions(new String[] {"*.pdf"});
							fileSelector.setFileName("Export.pdf");
							fileSelector.setOverwrite(true);
							currentReportExportFilename = fileSelector.open();
						}
					});

					if (currentReportExportFilename != null) { 
						String reportExportFilename = currentReportExportFilename;
						if(!reportExportFilename.endsWith(".pdf"))
							reportExportFilename = reportExportFilename.concat(".pdf");
						taskRenderOptions.setOutputFileName(reportExportFilename);
						taskRender.setRenderOption(taskRenderOptions);
						taskRender.render();
						taskRender.close();
					}
					reportDocument.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					monitor.done();
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}
	
	public void run(IAction action) { /* Not used */ }
	public void init(IWorkbenchWindow window) { /* Not used */	}
	public void selectionChanged(IAction action, ISelection selection) { /* Not used*/ }
	public void dispose() { /* Not used*/ }
}
