package gon_client_management.view.element;

import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.view.ext.GEditorFieldCreator;
import gon_client_management.view.ext.GEnablingControl;
import gon_client_management.view.ext.IElementEditor;
import gon_client_management.view.util.GGuiUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;

public class LoginIntervalView extends WizardEditorElementView {

	
	public LoginIntervalView() {
	}

	
	/* (non-Javadoc)
	 * @see gon_client_management.view.element.ElementView#getEditor()
	 */
	@Override
	public IElementEditor createEditor(GIElement element) {
		return createEditor(element, false);
	}

	public IElementEditor createEditor(GIElement element, boolean copy) {
		GIConfigPane configPane;
		boolean readOnly; 
		if (element!=null) {
			configPane = myConfig.getConfigPane(element, copy);
			readOnly = !editEnabled();
		}
		else {
			configPane = myConfig.getCreateConfigPane();
			readOnly = false;
		}

		//final WizardDialog dialog = new WizardDialog(getViewSite().getShell(), configWizard);
		IWizard wizard = createWizard(configPane, readOnly);
		final WizardDialog dialog = new NonmodalWizardDialog(getViewSite().getShell(), wizard, getDialogSettings());
		
		return new IElementEditor() {

			public void hide() {
				dialog.close();
			}

			public void modify(GIElement element) {
				dialog.open();
			}

			public void show() {
				dialog.open();
			}
		};
	}
	
	
	private class LoginIntervalWizard extends Wizard {
	
		GIConfigPane configPane;
		private Map<Widget, GIConfigRowValue> widget2Fields = new HashMap<Widget, GIConfigRowValue>();
		private boolean readOnly;

		public LoginIntervalWizard(GIConfigPane configPane, boolean readOnly) {
			this.configPane = configPane;
			this.readOnly = readOnly;
		}


		private void createCompositeLayout(Composite planeComposite, int ncol, int style) {
			GridLayout g2 = new GridLayout();
			g2.numColumns = ncol;
			planeComposite.setLayout(g2);
			planeComposite.setLayoutData(new GridData(style));
		}
		
		private class StartPage extends WizardPage implements Listener {
	
			private Text inputStringField;
	
			protected StartPage() {
				super("Start");
				setTitle("Login Interval");
	//			setDescription("General");
			}
	
			public Label createLabel(Composite master, String text) {
				Label label = new Label(master, SWT.NONE);
				label.setText(text);
				GridData gridData = new GridData(GridData.BEGINNING);
				gridData.verticalAlignment = SWT.BEGINNING;
				label.setLayoutData(gridData);
				return label;
			}
			
			@Override
			public void createControl(Composite parent) {
				Composite composite =  new Composite(parent, SWT.NULL);
				GridLayout gl = new GridLayout();
				int ncol = 2;
				gl.numColumns = ncol;
				composite.setLayout(gl);
				final GEnablingControl mainEnablingControl = new GEnablingControl(composite, null);
	
				mainEnablingControl.addChild(createLabel(composite, "Name"));
				inputStringField = new Text(composite, SWT.BORDER);
				GridData inputStringFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
				inputStringField.setLayoutData(inputStringFieldGridData);
				widget2Fields.put(inputStringField, configPane.getValue("name"));
				mainEnablingControl.addChild(inputStringField);
				
				mainEnablingControl.addChild(createLabel(composite, "Description"));
				StyledText inputTextField = new StyledText(composite, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP );
				GridData inputTextFieldGridData = new GridData(GridData.FILL_BOTH);
				inputTextFieldGridData.heightHint = 50;
				inputTextFieldGridData.widthHint = 350;
				inputTextField.setLayoutData(inputTextFieldGridData);
				widget2Fields.put(inputTextField, configPane.getValue("description"));
				mainEnablingControl.addChild(inputTextField);
				inputStringField.addListener(SWT.KeyUp, this);
				
				GGuiUtils.createLine(composite, 2);
				
				Composite carContent = new Composite(composite, SWT.NULL);
				createCompositeLayout(carContent, 2, GridData.FILL_HORIZONTAL);
				GridData gridData = (GridData) carContent.getLayoutData();
				gridData.horizontalSpan = 2;
				
				final GEnablingControl carControl = new GEnablingControl(carContent, null);
				
				
				final String[] weekDays = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
				
				for (int i=0; i<weekDays.length; i++) {
					String day = weekDays[i];					
					Label dayLabel = new Label(carContent, SWT.NONE);
					mainEnablingControl.addChild(dayLabel);
					dayLabel.setText(day);
					carControl.addChild(dayLabel);
					createHoursControls(carContent, mainEnablingControl, i);
					
				}
				
				
				setControl(composite);	
				setWidgetValues();
				
				if (readOnly)
					mainEnablingControl.setEnabled(false);
				else
					mainEnablingControl.setEnabling();
				
			}
	
			
			private void createHoursControls(Composite planeContent, final GEnablingControl planeControl, int dayNo) {
	
				Composite composite =  new Composite(planeContent, SWT.NULL);
				createCompositeLayout(composite, 6, GridData.FILL_HORIZONTAL);
				
				Button alldayButton = new Button(composite, SWT.RADIO);
				alldayButton.setText("All");
				alldayButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				planeControl.addChild(alldayButton);
				widget2Fields.put(alldayButton, configPane.getValue("day" + dayNo + "_allday"));

				Button noneButton = new Button(composite, SWT.RADIO);
				noneButton.setText("None");
				noneButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				planeControl.addChild(noneButton);
				widget2Fields.put(noneButton, configPane.getValue("day" + dayNo + "_active"));
				
				Button hoursButton = new Button(composite, SWT.RADIO);
				hoursButton.setText("Time");
				hoursButton.setLayoutData(new GridData(GridData.END));
				planeControl.addChild(hoursButton);
				widget2Fields.put(hoursButton, configPane.getValue("day" + dayNo + "_time"));
				
				final GEnablingControl hoursControl = new GEnablingControl(composite, hoursButton);
				hoursButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						hoursControl.setEnabling();
					}
					
				});
				
				
				final DateTime fromTime = new DateTime(composite, SWT.TIME | SWT.SHORT);
				fromTime.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				hoursControl.addChild(fromTime);
				widget2Fields.put(fromTime, configPane.getValue("day" + dayNo + "_start"));
				
	
				Label toLabel = new Label(composite, SWT.NONE);
				toLabel.setText(" - ");
				hoursControl.addChild(toLabel);
				
				final DateTime toTime = new DateTime(composite, SWT.TIME | SWT.SHORT);
				toTime.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				hoursControl.addChild(toTime);
				widget2Fields.put(toTime, configPane.getValue("day" + dayNo + "_end"));
	
				planeControl.addChild(hoursControl);
			}
			
	
			/**
			 * @see Listener#handleEvent(Event)
			 */
			public void handleEvent(Event event) {
			    // Initialize a variable with the no error status
				String currentError;
				if (GGuiUtils.isTextNonEmpty(inputStringField)) {
					currentError = null;
				}
				else {
					currentError = "Name cannot be empty";
				}
				setErrorMessage(currentError);
				getWizard().getContainer().updateButtons();
			}
	
	
			
			
			
			
			@Override
			public boolean isPageComplete() {
				if (GGuiUtils.isTextNonEmpty(inputStringField)) {
					return super.isPageComplete();
				}
				else {
					return false;
				}
			}
	
	
	//		@Override
	//		public boolean canFlipToNextPage() {
	//			if (isTextNonEmpty(inputStringField.getText())) {
	//				return super.canFlipToNextPage();
	//			}
	//			else {
	//				return false;
	//			}
	//		}
	
			
			
		}
	
		void setWidgetValues() {
			Set<Entry<Widget, GIConfigRowValue>> entrySet = widget2Fields.entrySet();
			for (Entry<Widget, GIConfigRowValue> entry : entrySet) {
				GEditorFieldCreator.setWidgetValue(entry.getKey(), entry.getValue());
			}
			
		}
		
		void getWidgetValues() {
			Set<Entry<Widget, GIConfigRowValue>> entrySet = widget2Fields.entrySet();
			for (Entry<Widget, GIConfigRowValue> entry : entrySet) {
				GEditorFieldCreator.getWidgetValue(entry.getKey(), entry.getValue());
			}
			
		}
	
	



		@Override
		public boolean performFinish() {
			if (readOnly)
				return true;
			
			getWidgetValues();
			try {
				configPane.save();
			} catch (GOperationNotAllowedException e) {
				MessageDialog.openError(this.getShell(), "Error Saving Element", e.getMessage());
				return false;
			}
			return true;
		}

		
		public void addPages()
		{
//			LoginHourPage loginHourPage = new LoginHourPage();
//			HolidayMainPage holidayMainPage = new HolidayMainPage();
			StartPage startPage = new StartPage();
			
			addPage(startPage);
//			addPage(holidayMainPage);
//			addPage(loginHourPage);
		}
		
		
	}
	
	private IWizard createWizard(GIConfigPane configPane, boolean readOnly) {
		return new LoginIntervalWizard(configPane, readOnly);
	}
	
	
}
