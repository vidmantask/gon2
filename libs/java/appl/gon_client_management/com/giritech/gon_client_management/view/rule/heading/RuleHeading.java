package gon_client_management.view.rule.heading;

import gon_client_management.view.rule.RuleView;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class RuleHeading {

	public RuleView parent = null;
	
	public RuleHeading(RuleView parent) {
		this.parent = parent;
		parent.ruleHandlingForm.setText(parent.modelAPI.getLongViewHeadline()); 
		parent.toolkit.decorateFormHeading(parent.ruleHandlingForm);
		/* Create a button in the header for activating rule creation. */
		parent.ruleHandlingForm.getToolBarManager().add(showRuleEditorAction);
		parent.ruleHandlingForm.getToolBarManager().add(parent.getStopFetchingRulesAction());		
		parent.ruleHandlingForm.getToolBarManager().add(parent.getRefreshAction());		
		parent.ruleHandlingForm.updateToolBar();
		
		// TODO: Should be corrected back to Theme sometime. 4 lines below just makes sure that rule headline has anti-alias 
		Display display = parent.getViewSite().getShell().getDisplay();
		Font verdanaFont = new Font(display, "Verdana", 12, SWT.NORMAL);
		parent.ruleHandlingForm.setFont(verdanaFont);
		parent.toolkit.decorateFormHeading(parent.ruleHandlingForm);
		
		/* Theme for heading widgets. */
		//System.out.println("Theme: " + parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager().getCurrentTheme().getId());
		// TODO: Should be corrected back to Theme sometime:
		/*if (!parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager().getCurrentTheme().getId().equals("org.eclipse.ui.defaultTheme")) {

			IThemeManager themeManager = parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager();
			parent.ruleHandlingForm.setFont(themeManager.getCurrentTheme().getFontRegistry().get("gon_client_management.ruleview.heading.font"));
			parent.ruleHandlingForm.setForeground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.heading.foreground_color"));
			parent.ruleHandlingForm.setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.heading.background_color"));
			parent.ruleHandlingForm.setTextBackground(
					new Color[] {themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.heading.background_color_1"),
							themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.heading.background_color_2")},
							new int[] {themeManager.getCurrentTheme().getInt("gon_client_management.ruleview.heading.background_gradient_partition")}, 
							true);
			parent.ruleHandlingForm.setSeparatorVisible(true);
			parent.ruleHandlingForm.setHeadColor(IFormColors.H_BOTTOM_KEYLINE1, themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.heading.seperator_color"));
		}*/
	}

	/**
	 * Create an action for creating new rules.
	 */
	Action showRuleEditorAction = new Action("Add a new rule") {
		@Override
		public void run() {
			parent.editor.edit(null);
		}

		@Override
		public boolean isEnabled() {
			return parent.modelAPI.isCreateEnabled();
		}

		@Override
		public ImageDescriptor getImageDescriptor() {
			if (JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON") == null) {
				JFaceResources.getImageRegistry().put("ADD_BUTTON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_add.png").createImage());
			}
			return JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON");
		}
	};
}
