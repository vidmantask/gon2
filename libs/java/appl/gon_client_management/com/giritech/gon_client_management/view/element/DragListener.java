package gon_client_management.view.element;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;

/**
 * Support for dragging elements from the element viewers.
 * 
 * @author Giritech
 *
 */
public class DragListener extends DragSourceAdapter {
	private TableViewer viewer;

	/**
	 * Initialization
	 */
	public DragListener(TableViewer viewer) {
		this.viewer = viewer;
	}

	/**
	 * Method declared on DragSourceListener
	 * Called when dragging is started.
	 */
	public void dragStart(DragSourceEvent event) {
		LocalSelectionTransfer.getTransfer().setSelection(viewer.getSelection());
	}

	/**
	 * Method declared in DragSourceListener.
	 * Called when dragging has finished (drop).
	 */
	public void dragFinished(DragSourceEvent event) {
	}
	
}
