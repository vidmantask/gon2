package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class TokenGroupAdm implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE;
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE;
		String [] defaultElements = {GGlobalDefinitions.TOKEN_TYPE,
									 GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);

	}

}
