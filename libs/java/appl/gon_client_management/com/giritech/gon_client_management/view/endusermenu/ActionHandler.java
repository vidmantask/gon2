package gon_client_management.view.endusermenu;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.ui.handlers.HandlerUtil;

public class ActionHandler implements IHandler {

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {}

	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {}

	@Override
	public void dispose() {}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		String commandId = event.getCommand().getId();

		if (commandId.equals("REMOVE_MENU_FOLDER")) {
			MenuView currentView = (MenuView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
			currentView.removeFolder();
		}
		else if (commandId.equals("REMOVE_MENU_ITEM")) {
			MenuView currentView = (MenuView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
			currentView.removeItem();
		}
		else if (commandId.equals("REFRESH_MENU_TREE")) {
			//System.out.println("ActionHandler.execute() called with REFRESH_MENU_TREE");
			MenuView currentView = (MenuView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
			currentView.refresh();
		}
		
		return null;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isHandled() {
		// TODO Auto-generated method stub
		return true;
	}


}
