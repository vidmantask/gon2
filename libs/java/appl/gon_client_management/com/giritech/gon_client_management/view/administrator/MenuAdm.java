package gon_client_management.view.administrator;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class MenuAdm implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {

		String editorArea = layout.getEditorArea();
	    IFolderLayout paletteLayout = layout.createFolder("paletteLayout", IPageLayout.LEFT, 0.30f, editorArea);
	    
	    final String tagViewId = "gon_client_management.TagView";
	    paletteLayout.addView(tagViewId);
	    layout.getViewLayout(tagViewId).setCloseable(false);
	    
	    final String menuItemViewId = "gon_client_management.MenuItemView";
//	    final String menuItemViewId = "gon_client_management.ProgramView";
	    paletteLayout.addView(menuItemViewId);
	    layout.getViewLayout(menuItemViewId).setCloseable(false);
	    
		layout.addStandaloneView("gon_client_management.MenuView", false, IPageLayout.RIGHT, 1, IPageLayout.ID_EDITOR_AREA);
		layout.setEditorAreaVisible(false);
	}

}
