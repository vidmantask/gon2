package gon_client_management.view.report;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.GIModelReport;
import gon_client_management.model.GIReportPane;
import gon_client_management.model.server.GIServer;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.provider.ReportElementColumnLabelProvider;
import gon_client_management.view.ext.GSafeView;
import gon_client_management.view.preferences.PreferenceConstants;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

/**
 * A generic element viewer for other specific element
 * views to inherit from.
 *  
 * @author Giritech
 *
 */
public abstract class ReportElementView extends GSafeView {
	
	private GIReportPane modelAPI;
	public TableViewer elementViewer = null;
	
	private ElementSorter elementComparatorInc = new ElementSorter(1);
	private ElementSorter elementComparatorDec = new ElementSorter(-1);
	private Browser browser;
	private PreviewReportAction preview = null;
	
	private Composite reportSelectionContainer;
	private Form reportSelectionForm;
	private Section reportSelectionSection;
	private Section reportViewingSection; 
	private Composite reportViewingContainer;
	private FormToolkit toolkit;
	
	public GIModelReport selectedReport;
	
	
	public ReportElementView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}

	
	@Override
	public void setViewInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		super.setViewInitializationData(cfig, propertyName, data);
		this.modelAPI = createModelAPI();
		this.preview = new PreviewReportAction();
	}


	protected abstract GIReportPane createModelAPI();
	
	public void retreiveElements() {
		Display display = getViewSite().getShell().getDisplay();
		display.asyncExec (new Runnable () {
		      public void run () {
		    	elementViewer.setContentProvider(new ReportElementContentProvider(modelAPI));
				elementViewer.setInput(getViewSite());
				elementViewer.getTable().setVisible(true);
				pack();
		      }
		   });
	}

	/**
	 * A listener for when the user clicks columns headers.
	 */
	public Listener columnSortListener = new Listener() {
		public void handleEvent(Event e) {
			if(elementViewer.getTable().getSortDirection() == SWT.DOWN) {
				elementViewer.getTable().setSortDirection(SWT.UP);	
				elementViewer.setComparator(elementComparatorInc);
			}
			else {
				elementViewer.getTable().setSortDirection(SWT.DOWN);
				elementViewer.setComparator(elementComparatorDec);
			}
		}
	};

	/**
	 * A location listener for the browser, notified when clicking on links
	 */
	private static String ReportParmKeyReport = "__report";
	private static String ReportParmKeyReportDesign = "__report_design";
	private static String ReportParmKeyReportGiritechSessionId = "giritech_session_id";
	private static String ReportParmKeyReportGiritechWSURL = "giritech_admin_ws_url";

	public LocationListener browserLocationListener = new LocationListener() {
		public void changing(LocationEvent event) {
			Map<String, String> reportParameters = decodeReportURL(event.location);
			if (reportParameters.containsKey(ReportParmKeyReportDesign)) {
				String report_specification_filename = reportParameters.get(ReportParmKeyReportDesign);
				
				GIServer server = GServerInterface.getServer();
				String reportSpecification = server.getReportSpecificationFromFilename(report_specification_filename);
				
				runReportAndShowInBrowser("Inline", reportSpecification, reportParameters);
				event.doit = false;
			}
		}

		public void changed(LocationEvent event) {
		}

		private Map<String, String> decodeReportURL(String reportURLString) {
			Map<String, String> reportParameters = new HashMap<String, String>();
			try {
				URL reportURL = new URL(reportURLString.replace("about:run", "https://127.0.0.1:18072"));
		        String reportURLQuery = reportURL.getQuery();
			    for (String param : reportURLQuery.split("&")) {
			        String[] pair = param.split("=");
			        String key = URLDecoder.decode(pair[0], "UTF-8");
			        String value = URLDecoder.decode(pair[1], "UTF-8");
			        
			        // twa
			        // Due to at bug in birt, all parameter values are enclosed in brackets.
			        if(value.startsWith("[")) {
			        	value = value.substring(1);
			        }
			        if(value.endsWith("]")) {
			        	value = value.substring(0, value.length()-1);
			        }
			        reportParameters.put(key, value);
			    }
			    if(reportParameters.containsKey(ReportParmKeyReport)) {
			    	String reportFilename = reportParameters.get(ReportParmKeyReport);
			    	File file = new File(reportFilename);
			    	reportParameters.put(ReportParmKeyReportDesign, file.getName());
			    }
			} catch (MalformedURLException e) {
			} catch (UnsupportedEncodingException e) {
			}
		    return reportParameters;
		}
	};	

	public String getGiritechWSURL() {
		return "https://" + 
		  Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_HOST) +
		  ":" +
		  Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_PORT);
	}
	
	public void runReportAndShowInBrowser(String reportTitle, String reportSpecification) {
		runReportAndShowInBrowser(reportTitle, reportSpecification, new HashMap<String, String>());
	}

	public void runReportAndShowInBrowser(String reportTitle, String reportSpecification, Map<String, String> reportParameters) {
		reportParameters.put(ReportParmKeyReportGiritechSessionId, modelAPI.getSessionID());
		reportParameters.put(ReportParmKeyReportGiritechWSURL, this.getGiritechWSURL());
		preview.run(browser, exportAction, reportTitle, reportSpecification, reportParameters);							
		reportViewingSection.setText("Report");
	}
	
	/**
	 * Displays the selected report.
	 */
	public IDoubleClickListener sendElementListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			IStructuredSelection selection = (IStructuredSelection) elementViewer.getSelection();
			GIModelReport report = (GIModelReport) selection.getFirstElement();
			runReportAndShowInBrowser(report.getTitle(), report.getSpecification());
		}
	};
	
	/**
	 * Setup an area for viewing existing elements.
	 */
	public void createElementViewArea() {
		elementViewer = new TableViewer(reportSelectionContainer, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.NO_MERGE_PAINTS | SWT.BORDER | SWT.FILL);
		//elementViewer.getTable().setHeaderVisible(true);
		elementViewer.getTable().setLinesVisible(false);
		
		/* Set layout for the element viewer. */
		elementViewer.getTable().setLayout(new GridLayout());
		GridData elementListGridData = new GridData(GridData.FILL_VERTICAL);
		elementListGridData.grabExcessVerticalSpace = true;
		elementViewer.getTable().setLayoutData(elementListGridData);

		/* Create a column for report elements. */
		TableViewerColumn elementColumn = new TableViewerColumn(elementViewer, SWT.NONE);

		/* Set specifics for this column. */
		elementColumn.getColumn().setText(modelAPI.getColumnLabel());

		/* Set how the elements are displayed in the view. */
		elementColumn.setLabelProvider(new ReportElementColumnLabelProvider(GGlobalDefinitions.REPORT_TYPE));
		
		/* Setup tooltip support for the report listing. */
		ColumnViewerToolTipSupport.enableFor(elementViewer);
		
		// Setup some listeners for user actions.
		elementViewer.addDoubleClickListener(sendElementListener);

		// Setup sorting via the column header.
		elementViewer.getTable().setSortColumn(elementViewer.getTable().getColumn(0));
		elementViewer.setComparator(elementComparatorInc);
		elementColumn.getColumn().addListener(SWT.Selection, columnSortListener);

		//browser = new Browser( viewerGroup, SWT.NONE );
		browser = new Browser( reportViewingContainer, SWT.BORDER);
		org.eclipse.birt.report.viewer.utilities.WebViewer.startup();
		//WebViewer.startup( browser );
		GridData reportBrowserGridData = new GridData(GridData.FILL_BOTH);
		reportBrowserGridData.grabExcessHorizontalSpace = true;
		browser.setLayoutData(reportBrowserGridData);
		browser.addLocationListener(browserLocationListener);
	}

	/**
	 * Action for exporting the currently selected report
	 * to different formats.
	 * Currently only PDF.
	 */
	Action exportAction = new Action("Export") {
		public void run() { preview.exportToPortableDocFormat(); }
	};
	
	/**
	 * Implements WorkbenchPart.createPartControl
	 * 
	 * Using gridlayout for proper control with the layout. 
	 * TableWrapLayout will only do layout horizontally. Which is probably nice
	 * for webpage style  layouts. But do not give us sufficient control here.
	 * All Layouts seem to enherit from 
	 * 
	 */
	@Override
	public void createView(Composite parent) {
		
		/* Create a form for the toolkit elements. */
		this.toolkit = new FormToolkit(parent.getDisplay());
		this.reportSelectionForm = toolkit.createForm(parent);
		reportSelectionForm.getToolBarManager().add(exportAction);
		reportSelectionForm.setText("Reporting");
		reportSelectionForm.updateToolBar();
		Font verdanaFont = new Font(parent.getDisplay(), "Verdana", 12, SWT.NORMAL);
		reportSelectionForm.setFont(verdanaFont);
		toolkit.decorateFormHeading(reportSelectionForm);	

		/* Create layout for this form.  */
		GridLayout layout = new GridLayout(); 
		reportSelectionForm.getBody().setLayout(layout);
		layout.numColumns = 2;
		GridData formLayoutWrapData = new GridData();
		reportSelectionForm.getBody().setLayoutData(formLayoutWrapData);
		
		/* 
		 * Create a section for selection of reports 
		 */
		this.reportSelectionSection = toolkit.createSection(reportSelectionForm.getBody(), Section.TITLE_BAR);
		reportSelectionSection.setText("Report selection");
		GridData reportSelectionSectionWrapData = new GridData(GridData.FILL_VERTICAL);
		reportSelectionSection.setLayoutData(reportSelectionSectionWrapData);
		
		/* create a container for the elements in this section. */
		this.reportSelectionContainer = toolkit.createComposite(reportSelectionSection);
		reportSelectionSection.setClient(reportSelectionContainer);
		reportSelectionContainer.setLayout(new GridLayout());
		
		/* 
		 * Create a section for viewing of reports. 
		 */
		this.reportViewingSection = toolkit.createSection(reportSelectionForm.getBody(), Section.TITLE_BAR);
		reportViewingSection.setText("Report                                  "); // Added extra blanks to let the title be changed by previewReportAction
		GridData reportViewingSectionGridData = new GridData(GridData.FILL_BOTH);
		reportViewingSectionGridData.grabExcessHorizontalSpace = true;
		reportViewingSection.setLayoutData(reportViewingSectionGridData);
		
		/* Create a container for elements in this section. */
		this.reportViewingContainer = toolkit.createComposite(reportViewingSection);
		reportViewingSection.setClient(reportViewingContainer);
		reportViewingContainer.setLayout(new GridLayout());

		/* Setup list content and browser. */
		createElementViewArea();
		retreiveElements();
	}

	/**
	 * Pack the elements relative to the table column
	 * used for listing reports. 
	 */
	public void pack() {
		/* Set the width of the first column.*/
		Rectangle clientRect = elementViewer.getTable().getClientArea();
		elementViewer.getTable().getColumn(0).setWidth(clientRect.width + 145);
		/* Adjust layout to fit the column. */
		reportSelectionForm.layout();
	}
	
	/**
	 * Implements WorkbenchPart.setFocus
	 */
	@Override
	public void setViewFocus() {
		getViewSite().getActionBars().updateActionBars();
		elementViewer.refresh();
	}

	/**
	 * class for sorting columns increasing or decreasing.
	 * 
	 * @author Giritech
	 *
	 */
	class ElementSorter extends ViewerComparator {

		private int direction;
		
		public ElementSorter(int direction) {
			this.direction = direction;
		}
		
		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			GIModelReport t1 = (GIModelReport) e1;
			GIModelReport t2 = (GIModelReport) e2;
			return super.compare(viewer, t1.getTitle(), t2.getTitle()) * direction;
		}
	}
	
}