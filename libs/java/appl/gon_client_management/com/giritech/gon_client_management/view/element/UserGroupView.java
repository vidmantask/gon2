package gon_client_management.view.element;

import org.eclipse.ui.contexts.IContextService;

public class UserGroupView extends ElementView {

	public UserGroupView() {
		super();
	}

	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.element.group");
		contextService.deactivateContext(editEnabledContext);
		
	}
	
	@Override
	public boolean createEnabled() {
		return false;
	}
	
	protected boolean limitResultInView() {
		return false;
	}

}
