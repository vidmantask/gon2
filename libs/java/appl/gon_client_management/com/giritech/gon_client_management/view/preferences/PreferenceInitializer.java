package gon_client_management.view.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.preference.IPreferenceStore;


import gon_client_management.Activator;

/**
 * Class used to initialize default preference values.
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.G_MANAGEMENT_SERVER_HOST, "127.0.0.1");
		store.setDefault(PreferenceConstants.G_MANAGEMENT_SERVER_PORT, "8072");
		store.setDefault(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_HOST, "127.0.0.1");
		store.setDefault(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_PORT, "8073");
		store.setDefault(PreferenceConstants.G_MANAGEMENT_SERVICE_EXECUTABLE, "..\\..\\gon_client_management_service\\win\\gon_client_management_service.exe");
		store.setDefault(PreferenceConstants.G_SKIP_LOGIN, MessageDialogWithToggle.PROMPT);
		store.setDefault(PreferenceConstants.G_CSV_DELIMITER, ",");
		
		store.setDefault(PreferenceConstants.G_SERVICE_AUTO_UPDATE, true);
		store.setDefault(PreferenceConstants.G_SESSION_AUTO_UPDATE, true);
		store.setDefault(PreferenceConstants.G_SESSION_VIEW_CHOICE, "All");

	}

}
