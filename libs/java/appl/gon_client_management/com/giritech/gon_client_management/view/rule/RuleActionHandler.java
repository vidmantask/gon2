package gon_client_management.view.rule;

import gon_client_management.model.GIRule;

import java.util.ArrayList;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.core.expressions.EvaluationContext;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISources;
import org.eclipse.ui.handlers.HandlerUtil;

public class RuleActionHandler implements IHandler {
	
	public void addHandlerListener(IHandlerListener handlerListener) {}
	public void removeHandlerListener(IHandlerListener handlerListener) {}
	public void dispose() {}
	
	
	private GIRule[] getSelectedRules(Object[] selectedElements) {
		ArrayList<GIRule> ruleList = new ArrayList<GIRule>();
		if (selectedElements!=null)
			for (Object obj : selectedElements) {
				
				if (obj instanceof GIRule) {
					ruleList.add((GIRule) obj);
				}
			}
		GIRule[] rules = new GIRule[0];
		return ruleList.toArray(rules);
	}

	/**
	 * Execute actions for the context menu. 
	 * 
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		String commandId = event.getCommand().getId();
		RuleView currentView = (RuleView) HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().getActivePart();
		IStructuredSelection selection;
		GIRule rule = null;
		Object[] selectedElements = null;
		
		
		// Get an element to perform the action on.
		if (event.getApplicationContext() instanceof EvaluationContext) {
 			EvaluationContext context = (EvaluationContext) event.getApplicationContext();
 			if (context.getVariable(ISources.ACTIVE_MENU_SELECTION_NAME) instanceof IStructuredSelection) {
 				selection = (IStructuredSelection) context.getVariable(ISources.ACTIVE_MENU_SELECTION_NAME);
 			}
 			else {
 				selection = (IStructuredSelection) currentView.listing.checkBoxRuleTableView.getSelection();
 			}
 			rule = (GIRule) selection.getFirstElement();
 			selectedElements = selection.toArray();

		}
		
		/** Perform the selected action. */
		if (commandId.equals("EDIT_RULE") && rule != null) {
			/** Go through listing so selection can be set
			 * properly after editing. This way we use the same
			 * method for editing no matter where the call came 
			 * from.
			 **/
			currentView.editor.edit(rule);
		}
		else if (commandId.equals("DELETE_RULE") && rule != null) {
			currentView.deleteRules(getSelectedRules(selectedElements));
		}
		else if (commandId.equals("NEW_RULE")) {
			currentView.editor.edit(null);
		}
		else if (commandId.equals("REFRESH_RULES")) {
			currentView.refresh();
			currentView.refreshCurrentElementViews();
		}
		else if (commandId.equals("TOGGLE_RULE_ACTIVATION")) {
			currentView.listing.toggleRuleActivation(getSelectedRules(selectedElements));
		}
		else if (commandId.equals("TOGGLE_EDITOR_RULE_ACTIVATION")) {
			currentView.editor.toggleRuleActivation(rule);
		}
		else if (commandId.equals("EXPORT_RULES_TO_CSV")) {
			currentView.listing.exportRulesToCSV();
		}
		else if (commandId.equals("SELECT_ALL_RULES")) {
			currentView.listing.selectAll();
		}
		return null;
	}

	/**
	 * Enabled status is set by the handler in plugin.xml. 
	 * The context for the current view is set in the ElementView
	 * implementation. Each handler can ask for a specific context
	 * string. 
	 * Therefore - this is not used. 
	 */
	public boolean isEnabled() {
		return true;
	}

	/**
	 * This is set to true to say that we will handle
	 * execution ourselves. This makes sure that execute
	 * is called when the handler is selected.
	 */
	public boolean isHandled() {
		return true;
	}
}
