package gon_client_management.view.element.editor;

import gon_client_management.Activator;
import gon_client_management.model.GIDeployment;
import gon_client_management.model.GIToken;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.view.element.KeyView;
import gon_client_management.view.util.GGuiUtils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.FormDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class TokenEditorDialog extends FormDialog {

	public String deploymentExecutable = null;
	GIDeployment deployment;
	public ScrolledForm parentForm;
	public Form dialogForm = null;
	public Combo currentTokenList = null;
	public Label statusLabel = null;
	public Label tokenSerialLabel  = null;
	public Text newSerialText = null;
	public Text tokenDescriptionText = null;
	public Text tokenCasingText = null;
	private Text tokenNameText;
	public int selectedTokenIndex = 0;
	static KeyView parentView;
	
	/**
	 * Init
	 * 
	 * @param parentShell
	 * @param parent
	 */
	public TokenEditorDialog(Shell parentShell, KeyView parent) {
		super(parentShell);
		parentView = parent;
		deployment = GModelAPIFactory.getModelAPI().getDeployment();
	}
	
	void setEnrollButtoneEnabled(boolean enabled) {
    	if (getButton(100) != null) { 
    		getButton(100).setEnabled(enabled); 
    	}
		
	}
	

	/**
	 * Overrides the standard OK/Cancel button combination, so that 
	 * we have enroll and close buttons.
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		/* Setup layout. */
		GridData rightAligned = new GridData(GridData.HORIZONTAL_ALIGN_END);
		parent.setLayoutData(rightAligned);
		
		/* Create the buttons we need. */
		Button enrollButton = createButton(parent, 100, "Enroll", false);
		enrollButton.addSelectionListener(enrollSelectionListener);
		Button closeButton = createButton(parent, 101, "Close", false);
		closeButton.addSelectionListener(closeButtonListener);
		
		
		/* If we can not select a token, we can not enroll. */
		refreshSelectedTokenInfo(selectedTokenIndex);
		parent.update();
	}

	/**
	 * Listener for closing the Token editor dialog.
	 */
	SelectionListener closeButtonListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) { 
			close();
		}
	};

	/**
	 * This adds all parts in the dialog form area.
	 */
	@Override
	protected void createFormContent(IManagedForm mform) {
		super.createFormContent(mform);
		this.parentForm = mform.getForm();
		this.dialogForm = (Form) mform.getForm().getContent();
		FormToolkit toolkit = mform.getToolkit();
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		dialogForm.getBody().setLayout(layout);
		/* Setup the header for this dialog */
		dialogForm.setText("Add a new token");
		if (JFaceResources.getImageRegistry().getDescriptor("TOKEN") == null) 
			JFaceResources.getImageRegistry().put("TOKEN", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_token_man_icon_16x16.png").createImage());
		dialogForm.setImage(JFaceResources.getImageRegistry().get("TOKEN"));
		toolkit.decorateFormHeading(dialogForm);
		
		/**
		 *  Section for token selection
		 */

		/* Create a section for selection */
		Section selectionSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR);
		selectionSection.setText("Token selection");
		selectionSection.setLayout(new GridLayout());
		GridData selectionSectionGridData = new GridData(GridData.FILL_BOTH);
		selectionSection.setLayoutData(selectionSectionGridData);
		
		/* Create a container for inserting parts for this section. */
		Composite selectionContainer = toolkit.createComposite(selectionSection);
		selectionSection.setClient(selectionContainer);
		
		/* Create layout for the container. */
		GridLayout selectionContainerGridLayout = new GridLayout();
		selectionContainerGridLayout.numColumns = 3;
		selectionContainer.setLayout(selectionContainerGridLayout);
		GridData selectionContainerGridData = new GridData(GridData.FILL_BOTH);
		selectionContainer.setLayoutData(selectionContainerGridData);
		
		/* Parts for selecting which token we are operating on. */ 
		toolkit.createLabel(selectionContainer, "Source: ");
		this.currentTokenList = new Combo(selectionContainer, SWT.DROP_DOWN);
		currentTokenList.addSelectionListener(currentTokenSelectionListener);
		GridData currentTokenListGridData = new GridData(GridData.FILL_HORIZONTAL);
		currentTokenList.setLayoutData(currentTokenListGridData);
		
		Button refreshButton = toolkit.createButton(selectionContainer, "Refresh", SWT.PUSH);
		refreshButton.addSelectionListener(refreshCurrentTokenListListener);
		/* Area to indicate whether the token is previously deployed. */
		toolkit.createLabel(selectionContainer, "Status: ");
		this.statusLabel = toolkit.createLabel(selectionContainer, "unknown");
		GridData statusTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		statusTextGridData.horizontalSpan = 2;
		statusLabel.setLayoutData(statusTextGridData);
		
		/**
		 * Section for initialization settings
		 */

		/* Create a section for initialization settings. */
		Section initializationSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR);
		initializationSection.setText("Token initialisation settings");
		initializationSection.setLayout(new GridLayout());
		GridData initializationSectionGridData = new GridData(GridData.FILL_BOTH);
		initializationSection.setLayoutData(initializationSectionGridData);

		/* Create a container for inserting parts for this section. */
		Composite initializationContainer = toolkit.createComposite(initializationSection);
		initializationSection.setClient(initializationContainer);
		
		/* Create layout for the container. */
		GridLayout initializationContainerGridLayout = new GridLayout();
		initializationContainerGridLayout.numColumns = 2;
		initializationContainer.setLayout(initializationContainerGridLayout);
		GridData initializationContainerGridData = new GridData(GridData.FILL_BOTH);
		initializationContainer.setLayoutData(initializationContainerGridData);
		
		/* Area for displaying current tokens existing serial. */
		toolkit.createLabel(initializationContainer, "Serial: ");
		this.tokenSerialLabel = toolkit.createLabel(initializationContainer, "unknown");
		GridData tokenSerialTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		tokenSerialLabel.setLayoutData(tokenSerialTextGridData);
		
		/**
		 * Section for token description
		 */
		/* Create a section for descriptions. */
		Section descriptionSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR);
		descriptionSection.setText("Token description settings");
		descriptionSection.setLayout(new GridLayout());
		GridData descriptionSectionGridData = new GridData(GridData.FILL_BOTH);
		descriptionSection.setLayoutData(descriptionSectionGridData);

		/* Create  a container for the parts in this section. */
		Composite descriptionContainer = toolkit.createComposite(descriptionSection);
		descriptionSection.setClient(descriptionContainer);
		
		/* Create layout for the section */
		GridLayout descriptionContainerGridLayout = new GridLayout();
		descriptionContainerGridLayout.numColumns = 2;
		descriptionContainer.setLayout(descriptionContainerGridLayout);
		GridData descriptionContainerGridData = new GridData(GridData.FILL_BOTH);
		descriptionContainer.setLayoutData(descriptionContainerGridData);

		/* Area for displaying and  changing current tokens description and casing info. */
		toolkit.createLabel(descriptionContainer, "Name");
		this.tokenNameText = toolkit.createText(descriptionContainer, "",  SWT.BORDER);
		GridData tokenNameTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		tokenNameText.setLayoutData(tokenNameTextGridData);
		tokenNameText.setEnabled(false);
		
		/* Area for displaying and  changing current tokens description and casing info. */
		toolkit.createLabel(descriptionContainer, "Description");
		this.tokenDescriptionText = toolkit.createText(descriptionContainer, "",  SWT.BORDER);
		GridData tokenDescriptionTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		tokenDescriptionText.setLayoutData(tokenDescriptionTextGridData);
		tokenDescriptionText.addSelectionListener(enrollSelectionListener);
		//tokenDescriptionText.setText("Hej"); // TEST
		
		toolkit.createLabel(descriptionContainer, "Casing #");
		this.tokenCasingText = toolkit.createText(descriptionContainer, "", SWT.BORDER);
		GridData tokenCasingTextGridData = new GridData(GridData.FILL_HORIZONTAL);
		tokenCasingText.setLayoutData(tokenCasingTextGridData);
		tokenCasingText.addSelectionListener(enrollSelectionListener);
		
		updateAvailableTokenInfo(0);
	}
	


	/**
	 * Refresh all parts that depend on the selection of an actual token.
	 * 
	 * @param index
	 */
	private void refreshSelectedTokenInfo(int index) {
		if (index<0)
			return;
		
		selectedTokenIndex = index;

		// Demonstrates a model side problem when two tokens are inserted.
		// They return the same serial (and I think they shouldn't).
		//System.out.println("TEST: " + deployment.getTokens().get(0).getSerial());
		//System.out.println("TEST: " + deployment.getTokens().get(1).getSerial());
		
		boolean enrollable = false;
        if (index < deployment.getTokenCount()) {
        	/* Enable the possibility of enrollment. */
        	GIToken token = deployment.getTokens().get(index);

        	enrollable = token.isEnrollable();

        	/* Set deployment status info. */
        	GGuiUtils.setTextSafe(statusLabel, token.getStatus());
			/* Set serial info */
			//System.out.println("Attempt to set token serial: " + token.getSerial());
        	GGuiUtils.setTextSafe(tokenSerialLabel, token.getSerial());
			/* Set the token description */
			//System.out.println("Attempt to set token description: " + token.getDescription());
        	GGuiUtils.setTextSafe(tokenNameText, token.getName());
        	GGuiUtils.setTextSafe(tokenDescriptionText, token.getDescription());
			//tokenDescriptionText.setText("description");
			/* Set the token casing text */
			//System.out.println("Attempt to set token casing: " + token.getCasing());
        	GGuiUtils.setTextSafe(tokenCasingText, token.getCasing());
			
        }
        else {
            this.statusLabel.setText("Unknown");
        }
    	tokenDescriptionText.setEnabled(enrollable);
    	tokenCasingText.setEnabled(enrollable);
    	setEnrollButtoneEnabled(enrollable);
        
	}

	/**
	 * Enroll the currently selected token.
	 *
	 * 1: Create an entrance in the DB
	 * 2: Initialize the token with the serial from DB using gon_client_deploy.exe
	 * 3: Deploy using gon_client_deploy.exe - returns a public key public key for the token
	 * 4: Add the public key to the entrance in the DB
	 */
	private void enrollToken() {
		
		/* Show the user that we are really busy. */
		parentForm.setBusy(true);
		parentForm.update();
    	setEnrollButtoneEnabled(false);
		
		/* Start  enrolling the token. */
		final Job job = new Job("Enrolling new token") {
			
			final String description = tokenDescriptionText.getText();
			final String casing = tokenCasingText.getText();
			@Override
			protected IStatus run(IProgressMonitor monitor) {
					
				final GIToken token = deployment.getTokens().get(selectedTokenIndex);
				token.setDescription(description);
				token.setCasing(casing);
				//token.setDescription(tokenDescriptionText.getText());
				//token.setCasing(tokenCasingText.getText());				
				String errorMessageTmp = null;
				String tokenId = null;
				try {
					tokenId = deployment.enrollToken(token);
				} catch (GOperationNotAllowedException e) {
					errorMessageTmp = e.getMessage();
				} catch (RuntimeException e) {
					Activator.getLogger().logException(e);
					errorMessageTmp = e.getMessage();
					// Unfortunately, some exceptions (e.g. NullPointer) has no message...
					if (errorMessageTmp==null)
						errorMessageTmp = e.toString();
				}
				
				final String errorMessage = errorMessageTmp;
				final String elementId = tokenId; 

				
				/* Leave the GUI in a proper state. */
				Display display = PlatformUI.getWorkbench().getDisplay();
				display.syncExec(new Runnable() {
					public void run () {
						/* Show that we are no longer busy. */
						if (!parentForm.isDisposed()) {
							
							parentForm.setBusy(false);
							if (errorMessage!=null)
								dialogForm.setMessage(errorMessage, IMessageProvider.ERROR);
							else
								dialogForm.setMessage(null);
							dialogForm.update();
						
							/* Refresh the token info. */
							refreshSelectedTokenInfo(selectedTokenIndex);
						
						}
						if (errorMessage==null) {
							
							/* Refresh the token listing in parents view. */
							if (parentView != null) { 
								parentView.refresh(elementId);
							}
							if (elementId==null) {
								updateAvailableTokenInfo(selectedTokenIndex);
							}
					    	setEnrollButtoneEnabled(false);
						}
						
					}
				});
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}




	
	private void updateAvailableTokenInfo(int index) {
		/* Refresh the selection box with the found tokens. */
		deployment.refreshTokenList();
		this.currentTokenList.removeAll();
		for (int k=0; k<deployment.getTokenCount(); k++) {
			this.currentTokenList.add(deployment.getTokens().get(k).getTitle());
		}
		this.currentTokenList.select(index);
		refreshSelectedTokenInfo(index);
		selectedTokenIndex = index;
	}
	
	/**
	 * Listener for the refresh button. Results in refreshing the
	 * list of currently available tokens.
	 */
	private SelectionListener refreshCurrentTokenListListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			updateAvailableTokenInfo(0); 
		}
	};

	/**
	 * Listener for the selection box. Results in refreshing all parts that depend
	 * on the selection.
	 */
	private SelectionListener currentTokenSelectionListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) { /* Not used */ }
		public void widgetSelected(SelectionEvent e) {
			Combo tmp = (Combo) e.widget;
			//System.out.println("New token selected: " + tmp.getSelectionIndex());
			refreshSelectedTokenInfo(tmp.getSelectionIndex()); 
		}
	};

	/**
	 * Listener for enrolling tokens.
	 */
	private SelectionListener enrollSelectionListener = new SelectionListener() {
		public void widgetDefaultSelected(SelectionEvent e) {
			enrollToken();
		}
		public void widgetSelected(SelectionEvent e) {
			enrollToken();
		}
	};
}
