package gon_client_management.view.tools;

import gon_client_management.model.GIModelReport;

import org.eclipse.swt.widgets.Event;

public class ReportElementDrawing extends ElementDrawing {

	public void drawReportElement(Event event, GIModelReport reportElement) {
		draw(event, reportElement.getTitle(), "");
	}
	
	@Override
	protected void measure(Event event, Object obj) {
		super.measure(event, null);
	}

	@Override
	protected void paint(Event event, Object obj) {
		drawReportElement(event, (GIModelReport) obj);
	}

	@Override
	public String getToolTipText(Object obj) {
		return "Double click to view";
	}
}
