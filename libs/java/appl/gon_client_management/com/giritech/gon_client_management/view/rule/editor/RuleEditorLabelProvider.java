package gon_client_management.view.rule.editor;

import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;

import org.eclipse.swt.SWT;

import gon_client_management.model.GIRulePane;
import gon_client_management.provider.GLabelProvider;
import gon_client_management.provider.IconColumnLabelProvider;

public class RuleEditorLabelProvider {
	
	final GIRulePane modelAPI;
	
	/**
	 * Draw parts in a rule view. 
	 * 
	 * @param modelAPI A panel of GIRules consisting of GIElements. 
	 */
	public RuleEditorLabelProvider(GIRulePane modelAPI) {
		this.modelAPI = modelAPI;
	}
		
	/**
	 * Create the columns for the table that holds the rules.
	 * 
	 * @param viewer The table viewer that displays the rules.
	 */
	public void createColumns(TableViewer viewer) {

		/* Create columns for rule elements. */
		for (int i=0; i<modelAPI.getRuleElementCount(); i++) { 
			TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
			//column.setLabelProvider(new RuleElementColumnLabelProvider(modelAPI.getColumnType(i), i));
			column.setLabelProvider(new GLabelProvider(modelAPI.getColumnType(i), i));
			column.getColumn().setText(modelAPI.getColumnLabel(i));
			column.getColumn().setMoveable(false);
			column.getColumn().setResizable(true);
		}
		
		/* Create a column for an implication arrow. */
		TableViewerColumn arrowColumn = new TableViewerColumn(viewer, SWT.NONE);
		arrowColumn.setLabelProvider(new IconColumnLabelProvider("Implies"));
		arrowColumn.getColumn().setText("");
		arrowColumn.getColumn().setWidth(20);
		arrowColumn.getColumn().setMoveable(false);
		arrowColumn.getColumn().setResizable(false);
		
		/* Create  a column for the result element. */ 
		TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
		//column.setLabelProvider(new RuleElementColumnLabelProvider(modelAPI.getColumnLabel(-1), -1));
		column.setLabelProvider(new GLabelProvider(modelAPI.getColumnLabel(-1), -1));
		column.getColumn().setText(modelAPI.getColumnLabel(-1));
		column.getColumn().setMoveable(false);
		column.getColumn().setResizable(true);

		/* Setup tool tips for elements. */
		ColumnViewerToolTipSupport.enableFor(viewer);
	}
}
