package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class AuthenticationPolicyPerspective implements IPerspectiveFactory {

	//@Override
	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE;
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE;
		String [] defaultElements = {GGlobalDefinitions.USER_GROUP_TYPE,
									 GGlobalDefinitions.TOKEN_GROUP_TYPE,
									 GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);

	}

}
