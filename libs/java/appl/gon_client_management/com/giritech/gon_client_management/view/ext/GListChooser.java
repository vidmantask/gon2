package gon_client_management.view.ext;

import gon_client_management.model.ext.GIElement;

import java.util.List;

import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.FormDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

public class GListChooser extends FormDialog {

	private String fromTitle = "Add elemenents";
	private CellLabelProvider labelProvider;
	private IStructuredContentProvider sourceContentProvider;
	private Shell shell;
	private MyContentProvider destinationContentProvider;
	private TableViewer sourceTable;
	private TableViewer destinationTable;

	public GListChooser(Shell shell, 
						CellLabelProvider labelProvider, 
						List<GIElement> sourceElements, 
						List<GIElement> destinationElements) {
		super(shell);
		this.shell = shell;
		this.labelProvider = labelProvider;
		this.sourceContentProvider = new MyContentProvider(sourceElements, true);
		this.destinationContentProvider = new MyContentProvider(destinationElements, false);
	}
	
	

	@Override
	protected void createFormContent(IManagedForm mform) {
		super.createFormContent(mform);
		Form dialogForm = (Form) mform.getForm().getContent();
		FormToolkit toolkit = mform.getToolkit();
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		dialogForm.getBody().setLayout(layout);
		/* Setup the header for this dialog */
		dialogForm.setText("Add/Remove Zone restrictions");
		toolkit.decorateFormHeading(dialogForm);
		
		/**
		 * Create a section for viewing existing elements. 
		 */
		Section elementViewSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR);
		elementViewSection.setText(fromTitle);
		elementViewSection.setLayout(new GridLayout());
		GridData elementViewSectionGridData = new GridData(GridData.FILL_BOTH);
		elementViewSectionGridData.grabExcessVerticalSpace = true;
		elementViewSection.setLayoutData(elementViewSectionGridData);
		
		/* Create a container for the parts in this section. */
		Composite contentContainer = toolkit.createComposite(elementViewSection);
		elementViewSection.setClient(contentContainer);
		GridLayout contentContainerGridLayout = new GridLayout();
		contentContainerGridLayout.numColumns = 3;
		contentContainer.setLayout(contentContainerGridLayout);
		GridData contentContainerGridData = new GridData(GridData.FILL_BOTH);
		contentContainerGridData.grabExcessVerticalSpace = true;
		contentContainerGridData.grabExcessHorizontalSpace = true;
		contentContainer.setLayoutData(contentContainerGridData);
		

		sourceTable = addList(toolkit, "Zones", contentContainer, sourceContentProvider);
		
		Composite buttonContainer = toolkit.createComposite(contentContainer);
		GridLayout buttonContainerGridLayout = new GridLayout();
//		elementViewContainerGridLayout.numColumns = 2;
		buttonContainer.setLayout(buttonContainerGridLayout);
		GridData buttonContainerGridData = new GridData(GridData.FILL_BOTH);
		buttonContainerGridData.grabExcessVerticalSpace = true;
		buttonContainerGridData.grabExcessHorizontalSpace = true;
		buttonContainer.setLayoutData(buttonContainerGridData);
		
		Button addButton = toolkit.createButton(buttonContainer, ">", SWT.PUSH | SWT.CENTER);
		addButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection selection = (IStructuredSelection) sourceTable.getSelection();
				destinationContentProvider.addElements(selection.toArray());
				destinationTable.refresh();
			}
		});
		Button removeButton = toolkit.createButton(buttonContainer, "<", SWT.PUSH | SWT.CENTER);
		removeButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				IStructuredSelection selection = (IStructuredSelection) destinationTable.getSelection();
				destinationContentProvider.removeElements(selection.toArray());
				destinationTable.refresh();
			}
		});
		
//		addButton.addSelectionListener(listener)

		destinationTable = addList(toolkit, "Restrictions", contentContainer, destinationContentProvider);
		
		elementViewSection.layout(true);
		
		
	}

	private TableViewer addList(FormToolkit toolkit,
						 String title,
						 Composite contentContainer,
						 IContentProvider  contentProvider) {
		/* Create a container for the parts in this section. */
		Composite elementViewContainer = toolkit.createComposite(contentContainer);
		GridLayout elementViewContainerGridLayout = new GridLayout();
//		elementViewContainerGridLayout.numColumns = 2;
		elementViewContainer.setLayout(elementViewContainerGridLayout);
		GridData elementViewContainerGridData = new GridData(GridData.FILL_BOTH);
		elementViewContainerGridData.grabExcessVerticalSpace = true;
		elementViewContainerGridData.grabExcessHorizontalSpace = true;
		elementViewContainer.setLayoutData(elementViewContainerGridData);
		
		// Create a filter placed on top of the listing.
//		this.filter = new ElementListingFilter(this);
		
		
		// Create the element view for displaying after elements are fetched.
		TableViewer elementViewer = new TableViewer(elementViewContainer, SWT.VIRTUAL | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.NO_MERGE_PAINTS| SWT.BORDER | SWT.MULTI);
		GridData elementViewerGridData = new GridData(GridData.FILL_BOTH);
//		elementViewerGridData.horizontalSpan = 2;
		elementViewerGridData.grabExcessHorizontalSpace = true;
		elementViewerGridData.grabExcessVerticalSpace = true;
		elementViewer.getTable().setLayoutData(elementViewerGridData);

		elementViewer.getTable().setLayout(new GridLayout());
		
		TableViewerColumn elementColumn = new TableViewerColumn(elementViewer, SWT.NONE);
		elementColumn.getColumn().setText(title);
		elementColumn.getColumn().setResizable(true);
		elementColumn.getColumn().setWidth(150);

		// Enable tool tip support for the individual elements.
		ColumnViewerToolTipSupport.enableFor(elementViewer);
		
		// Set how the elements are displayed in the view.
		//elementColumn.setLabelProvider(new ElementColumnLabelProvider(parent.modelAPI.getColumnType()));
		elementColumn.setLabelProvider(labelProvider);
		elementColumn.getColumn().setText(title);
		
		elementViewer.getTable().setHeaderVisible(true);
		elementViewer.setContentProvider(contentProvider);
		elementViewer.setInput(shell);
		elementViewer.getTable().setVisible(true);
		elementViewer.refresh();

		
		// Setup some listeners for user actions.
		elementViewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
			}
		});
		
		return elementViewer;
	}
	
	class MySelectionAdapter extends SelectionAdapter {
		

		MySelectionAdapter() {
			
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			IStructuredSelection selection = (IStructuredSelection) sourceTable.getSelection();
			destinationContentProvider.addElements(selection.toArray());
			destinationTable.refresh();
		}
		
	}

	class MyContentProvider implements IStructuredContentProvider {
		
		private List<GIElement> elements;
		private boolean fixed;

		MyContentProvider(List<GIElement> elements, boolean fixed) {
			this.elements = elements;
			this.fixed = fixed;
		}
		
		public void addElements(Object[] objects) {
			if (!fixed) {
				for (Object obj : objects) {
					GIElement element = findElement(obj);
					if (element==null)
						elements.add((GIElement) obj);
					
				}
			}
		}

		public void removeElements(Object[] objects) {
			if (!fixed) {
				for (Object obj : objects) {
					GIElement element = findElement(obj);
					if (element!=null)
						elements.remove(element);
					
				}
			}
		}

		
		private GIElement findElement(Object obj) {
			if (obj instanceof GIElement) {
				for(int i=0; i<elements.size(); i++) {
					GIElement element = (GIElement) obj;
					if (elements.get(i).getElementId().equals(element.getElementId()))
						return element;
				}
			}
			return null;
		}

		@Override
		public Object[] getElements(Object inputElement) {
			return elements.toArray();
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public List<GIElement> getChosenElements() {
		return destinationContentProvider.elements;
	}

}
