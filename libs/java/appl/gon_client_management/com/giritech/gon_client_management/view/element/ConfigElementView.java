package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.view.ext.GSafeView;

import org.eclipse.core.runtime.IConfigurationElement;

public abstract class ConfigElementView extends GSafeView {

	protected GIConfig myConfig;	
	
	public ConfigElementView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}
	
	
	
	@Override
	public void setViewInitializationData(IConfigurationElement cfig,String propertyName, Object data) {
		super.setViewInitializationData(cfig, propertyName, data);
		this.myConfig = createConfig();
		
	}



	protected abstract GIConfig createConfig();



	protected void refreshView(String selectedElementId) {
		if (myConfig!=null)
			myConfig.refresh();
	}

	

}
