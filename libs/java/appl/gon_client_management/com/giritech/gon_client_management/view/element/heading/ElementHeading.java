package gon_client_management.view.element.heading;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import gon_client_management.view.element.ElementView;

public class ElementHeading {

	private ElementView parent = null;
	
	/**
	 * Create a heading for an element view.
	 * 
	 * @param parentView
	 */
	public ElementHeading(ElementView parent) {
		this.parent = parent;
		
		/* Create a nice header title */
		this.parent.elementHandlingForm.setText(parent.modelAPI.getViewHeadline());
		Display display = parent.getViewSite().getShell().getDisplay();
		Font verdanaFont = new Font(display, "Verdana", 12, SWT.NORMAL);
		parent.elementHandlingForm.setFont(verdanaFont);
		parent.toolkit.decorateFormHeading(parent.elementHandlingForm);

		/* Create a button for activating element creation */
		parent.elementHandlingForm.getToolBarManager().add(parent.getShowElementEditorAction());
		parent.elementHandlingForm.getToolBarManager().add(parent.getStopFetchingElementsAction());
		parent.elementHandlingForm.getToolBarManager().add(parent.getRefreshAction());
		parent.addCustomHeaderActions(parent.elementHandlingForm.getToolBarManager());
		parent.elementHandlingForm.updateToolBar();
	}


}
