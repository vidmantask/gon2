package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class SecurityAdm implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.LAUNCH_SPEC_TYPE;
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.LAUNCH_SPEC_TYPE;
		String [] defaultElements = {GGlobalDefinitions.USER_GROUP_TYPE,
									 GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE,
									 GGlobalDefinitions.LAUNCH_SPEC_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);
	}
}
