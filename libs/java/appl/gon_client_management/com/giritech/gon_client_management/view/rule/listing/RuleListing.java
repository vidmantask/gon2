package gon_client_management.view.rule.listing;

import gon_client_management.Activator;
import gon_client_management.GViewLicenceInfoHandler;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.provider.GContentProvider;
import gon_client_management.provider.GLabelProvider;
import gon_client_management.provider.GRuleActiveCheckBoxStateProvider;
import gon_client_management.provider.IconColumnLabelProvider;
import gon_client_management.view.preferences.PreferenceConstants;
import gon_client_management.view.rule.RuleView;
import gon_client_management.view.util.ElementListingRetriever;
import gon_client_management.view.util.GGuiUtils.GIMultipleJobElementHandler;
import gon_client_management.view.util.GGuiUtils.GMultipleJob;
import gon_client_management.view.util.GGuiUtils.GMultipleJobElementHandlerAdapter;
import gon_client_management.view.util.GIElementListing;

import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.themes.IThemeManager;

import com.csvreader.CsvWriter;

public class RuleListing implements GIElementListing {

	public RuleView parent = null;
	public Section ruleViewSection = null;
	public Composite ruleViewContainer = null;
	public RuleListingSorter sorter = null;
	public CheckboxTableViewer checkBoxRuleTableView = null;
	private ElementListingRetriever retreiver = null;
	private boolean shownFirst = false;

	/**
	 * Creates a section for listing the already existing rules.
	 * 
	 * @param parent
	 */
	public RuleListing(RuleView parent) {
		
		this.parent = parent;
		
		/* Create a section for viewing existing rules.  */
		this.ruleViewSection = parent.toolkit.createSection(parent.ruleHandlingForm.getBody(), SWT.NONE);
		ruleViewSection.setText(parent.modelAPI.getLongViewHeadline() + " (Fetching rules...)");
		ruleViewSection.setLayout(new GridLayout());
		GridData ruleViewSectionGridData = new GridData(GridData.FILL_BOTH);
		ruleViewSectionGridData.grabExcessVerticalSpace = true;
		ruleViewSection.setLayoutData(ruleViewSectionGridData);
		
		/* create a container for the parts in this section. */
		this.ruleViewContainer = parent.toolkit.createComposite(ruleViewSection);
		ruleViewSection.setClient(ruleViewContainer);
		GridLayout ruleViewContainerGridLayout = new GridLayout();
		ruleViewContainerGridLayout.numColumns = 2;
		ruleViewContainerGridLayout.marginWidth = 0;
		ruleViewContainer.setLayout(ruleViewContainerGridLayout);
		GridData ruleViewContainerGridData = new GridData(GridData.FILL_BOTH);
		ruleViewContainerGridData.grabExcessVerticalSpace = true;
		ruleViewContainer.setLayoutData(ruleViewContainerGridData);
		ruleViewContainer.addControlListener(containerResizeListener);
		ruleViewContainer.addPaintListener(containerPaintListener);

		/* Create a filter for rule viewing. */
		RuleListingFilter filter = new RuleListingFilter(this);
		
		/* Setup the rule table view. */
		/* Add this when we want to enable/disable individual rules. */
		Table table = new Table(ruleViewContainer, SWT.READ_ONLY | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER |  SWT.MULTI); //| SWT.CHECK);
//		Table table = new Table(ruleViewContainer, SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER | SWT.FILL);
		this.checkBoxRuleTableView = new CheckboxTableViewer(table);
		checkBoxRuleTableView.setContentProvider(new GContentProvider(parent.modelAPI));
		checkBoxRuleTableView.setCheckStateProvider(new GRuleActiveCheckBoxStateProvider());
		
		/* Gets nagging image */
		if (!GViewLicenceInfoHandler.isLicenseOk())
			checkBoxRuleTableView.getTable().setBackgroundImage(Activator.getImageDescriptor("icons/nag_screen_man.png").createImage());
		
		/* Create element columns */
		final ArrayList<TableViewerColumn> conditionColumns = new ArrayList<TableViewerColumn>();
		for (int i=0; i<parent.modelAPI.getRuleElementCount(); i++) { 
			TableViewerColumn column = new TableViewerColumn(checkBoxRuleTableView, SWT.NONE);
			column.setLabelProvider(new GLabelProvider(parent.modelAPI.getColumnType(i), i));
			column.getColumn().setText(parent.modelAPI.getColumnLabel(i));
			column.getColumn().setMoveable(false);
			column.getColumn().setResizable(true);
			conditionColumns.add(column);
		}
		
		/* Create a column to display an arrow in. */
		TableViewerColumn impliescolumn = new TableViewerColumn(checkBoxRuleTableView, SWT.NONE);
		impliescolumn.setLabelProvider(new IconColumnLabelProvider("Implies"));
		//impliescolumn.getColumn().setText("");
		impliescolumn.getColumn().setImage(JFaceResources.getImageRegistry().get("G_IMPLIES_ICON"));
		impliescolumn.getColumn().setWidth(30);
		impliescolumn.getColumn().setMoveable(false);
		impliescolumn.getColumn().setResizable(true);
		
		impliescolumn.getColumn().setAlignment(SWT.RIGHT);
		
		/* Create a column for the result predicate. */
		final TableViewerColumn resultcolumn = new TableViewerColumn(checkBoxRuleTableView, SWT.NONE);
		resultcolumn.setLabelProvider(new GLabelProvider(parent.modelAPI.getColumnLabel(-1), -1));
		resultcolumn.getColumn().setText(parent.modelAPI.getColumnLabel(-1));
		resultcolumn.getColumn().setMoveable(false);
		resultcolumn.getColumn().setResizable(true);
		
		checkBoxRuleTableView.getTable().setHeaderVisible(true);
		checkBoxRuleTableView.setInput(parent.getViewSite());
		checkBoxRuleTableView.addFilter(filter.getFilter());
		checkBoxRuleTableView.getTable().setLinesVisible(false);
		parent.toolkit.adapt(checkBoxRuleTableView.getTable(), true, true);
		
		/* Setup support for tool tips. */
		ColumnViewerToolTipSupport.enableFor(checkBoxRuleTableView);

		/* Setup layout for the table of rules. */
		checkBoxRuleTableView.getTable().setLayout(new GridLayout());
		GridData ruleTableViewGridData = new GridData(GridData.FILL_BOTH);
		ruleTableViewGridData.horizontalSpan = 2;
		ruleTableViewGridData.grabExcessVerticalSpace = true;
		checkBoxRuleTableView.getTable().setLayoutData(ruleTableViewGridData);
		
		/* Setup sorting by table headers. */
		this.sorter = new RuleListingSorter();
		//checkBoxRuleTableView.getTable().setSortColumn(ruleTableView.getTable().getColumn(0));
		checkBoxRuleTableView.getTable().setSortColumn(checkBoxRuleTableView.getTable().getColumn(0));
		checkBoxRuleTableView.setComparator(sorter);
		for (int colIndex=0; colIndex<parent.modelAPI.getRuleElementCount() + 2; colIndex++) {
			//if (colIndex != parent.modelAPI.getRuleElementCount())
			checkBoxRuleTableView.getTable().getColumn(colIndex).addListener(SWT.Selection, tableColumnSortListener);
		}
		
		/* Setup a context menu for the rule list*/
	    MenuManager menuMgr = new MenuManager();
		this.parent.getSite().registerContextMenu("rule_listing_popup", menuMgr, checkBoxRuleTableView);
		Control control = checkBoxRuleTableView.getControl();
		Menu menu = menuMgr.createContextMenu(control);
	    control.setMenu(menu);

	    
		ruleViewContainer.addControlListener(new ControlAdapter() {
			private int minWidth = 10;
			
			public void controlResized(ControlEvent e) {
				Composite comp = ruleViewContainer;
				Table table = checkBoxRuleTableView.getTable();
				Rectangle area = comp.getClientArea();
				Point oldSize = table.getSize();
				int delta = area.width - oldSize.x;
				
				if (delta != 0) {
					int accWidth = 0;
					for (TableViewerColumn column : conditionColumns) {
						accWidth += column.getColumn().getWidth();
					}
					accWidth += resultcolumn.getColumn().getWidth();

					int usedWidth = 0;
					for (TableViewerColumn column : conditionColumns) {
						usedWidth += updateColumnWidth(delta, accWidth, column);

					}
					int colWidth = resultcolumn.getColumn().getWidth();
					colWidth += (delta-usedWidth);
					setColumnWidth(resultcolumn, colWidth);
					
					table.setSize(area.width, area.height);
				}

			}

			private int updateColumnWidth(int delta, int accWidth, TableViewerColumn column) {
				int colWidth = column.getColumn().getWidth();
				int columnDelta = Math.round(((float) delta*colWidth)/ (float) accWidth);
				colWidth += columnDelta;
				if (colWidth <= 0) {
					columnDelta -= (colWidth-1); 
					colWidth = 1;
					
				}
				setColumnWidth(column, colWidth);
				return columnDelta;
			}

			private void setColumnWidth(TableViewerColumn column, int colWidth) {
				column.getColumn().setWidth(Math.max(minWidth, colWidth));
			}
		});
	    
		/* Setup listeners for user actions. */
		checkBoxRuleTableView.addDoubleClickListener(ruleDoubleClickListener);
		checkBoxRuleTableView.addCheckStateListener(checkStateListener);
		
		/* Add element drop support. */
		Transfer[] transfers = new Transfer[] { LocalSelectionTransfer.getTransfer()};		
		checkBoxRuleTableView.addDropSupport(DND.DROP_COPY | DND.DROP_MOVE, transfers, new RuleListingDropAdapter(this));

		
        TableColumn[] columns = checkBoxRuleTableView.getTable().getColumns();
        if (columns.length == parent.columnWidths.size()) {
        	int w =0;
			for (int i=0; i<columns.length; i++) {
				Integer integer = parent.columnWidths.get(i);
				w += integer;
				columns[i].setWidth(parent.columnWidths.get(i));
			}
			checkBoxRuleTableView.getTable().setSize(w+10, 500);
        }
        else {
			for (TableColumn tableColumn : columns) {
				if (tableColumn.getWidth()==0)
					tableColumn.pack();
			}
			checkBoxRuleTableView.getTable().pack();
        }
		
		/* Theme management. */
		if (!parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager().getCurrentTheme().getId().equals("org.eclipse.ui.defaultTheme")) {
			IThemeManager themeManager = parent.getSite().getWorkbenchWindow().getWorkbench().getThemeManager();
			ruleViewSection.setFont(themeManager.getCurrentTheme().getFontRegistry().get("gon_client_management.ruleview.listing.headline_font"));
			ruleViewSection.setForeground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.listing.headline_color"));
			ruleViewSection.setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.listing.section_background_color"));
			ruleViewContainer.setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.listing.section_background_color"));
			checkBoxRuleTableView.getTable().setBackground(themeManager.getCurrentTheme().getColorRegistry().get("gon_client_management.ruleview.listing.background_color"));
			checkBoxRuleTableView.getTable().setFont(themeManager.getCurrentTheme().getFontRegistry().get("gon_client_management.ruleview.listing.headline_font"));
		}
		
		retreiver = new ElementListingRetriever(this, parent.modelAPI, parent.modelAPI.getViewHeadline());
		retreiver.get(null);
		
	}
	
	public void layout() {
		
	}
	
	/**
	 * Listener for when the checked state of a rule is set or reset.
	 */
	public ICheckStateListener checkStateListener = new ICheckStateListener() {

		@Override
		public void checkStateChanged(CheckStateChangedEvent event) {
//			GIRule changedrule = (GIRule) event.getElement();
			//System.out.println("RuleListing.checkStateListener.checkStateChanged called on rule with result: " + changedrule.getResultElement().getLabel());
			//System.out.println("Needs to be connected to the modelAPI (PWL).");
		}
	};
	
	/**
	 * A listener for when the container has been resized. This
	 * should let the listing columns resize as well.
	 */
	public ControlListener containerResizeListener = new ControlListener() {
		public void controlMoved(ControlEvent e) {
			layout();
		}
		public void controlResized(ControlEvent e) {
			layout();
		}
	};

	/**
	 * A listener for laying out the content of the container on the first paint.
	 * If we do not use this it is all packed to the left until somebody 
	 * activates the view.
	 */
	public PaintListener containerPaintListener = new PaintListener() {
		public void paintControl(PaintEvent e) {
			layout();
		}
	};
	
	/**
	 * Listener for double clicks on an element (edits).
	 */
	public IDoubleClickListener ruleDoubleClickListener = new IDoubleClickListener() {
		public void doubleClick(DoubleClickEvent event) {
			IStructuredSelection selection = (IStructuredSelection) checkBoxRuleTableView.getSelection();
			GIRule rule = (GIRule) selection.getFirstElement();
			parent.editor.edit(rule);
		}
	};
	
	private String ruleToString(GIRule rule) {
		String ruleString = "Rule ";
		ArrayList<String> conditions = new ArrayList<String>(); 
		for(int i=0; i<rule.getElements().size(); i++) {
			GIElement e = rule.getElements().get(i);
			String label = e.getLabel();
			if (label.length()>0) {
				conditions.add(label);
			}
		}
		for(int i=0; i<conditions.size(); i++) {
			String label = conditions.get(i);
			ruleString += label;
			if (i<conditions.size()-1)
				ruleString += " + ";
			else
				ruleString += " -> ";
		}
		ruleString += rule.getResultElement().getLabel();
	
		return ruleString;
	}
	
	private void deleteRule(GIRule rule) throws GOperationNotAllowedException {
		if (!rule.deleteEnabled()) 
			throw new GOperationNotAllowedException("The selected rule can not be deleted");
		parent.modelAPI.deleteRule(rule);
	}
	
	/**
	 * Delete a rule after consulting the user
	 * @param rule that may get deleted.
	 */
	public void deleteRules(GIRule[] rules) {
		if (rules.length==0)
			return;
		Boolean areYouSure;
		boolean multipleRules = rules.length > 1;
		if (!multipleRules) {
			areYouSure = MessageDialog.openQuestion(null, "Confirm", "Are you sure you want to delete this rule?");
			if (areYouSure) {
				GIRule rule = rules[0];
				try {
					deleteRule(rule);
				} catch (GOperationNotAllowedException e) {
					MessageDialog.openError(null, "Error", e.getMessage());
				}
			}
			refresh();
		}
		else {
			areYouSure = MessageDialog.openQuestion(null, "Confirm", "Are you sure you want to delete these rules?");
			if (areYouSure) {
				parent.statusline.setMessage("Deleting the selected rules");
				GIMultipleJobElementHandler<GIRule> elementHandler = new GMultipleJobElementHandlerAdapter<GIRule>() {

					@Override
					public String handleElement(GIRule rule) {
						try {
							deleteRule(rule);
						} catch (GOperationNotAllowedException e) {
							return e.getMessage();
						}
						return null;
					}

					@Override
					public void updateGUI() {
						parent.statusline.setMessage("Done");
						refresh();
						
					}
					
				};
				GMultipleJob<GIRule> job = new GMultipleJob<GIRule>("Deleting rules", parent.getSite().getShell(), CommonUtils.convertToList(rules), elementHandler); 
				job.schedule();
			}
		}
	}



	/**
	 * Activate or de-activate a rule depending on current state.
	 * @param rule that may get activated or de-activated.
	 */
	public void toggleRuleActivation(GIRule[] rules) {
		if (rules==null)
			return;
		
		GIMultipleJobElementHandler<GIRule> elementHandler = new GMultipleJobElementHandlerAdapter<GIRule>() {

			@Override
			public String handleElement(GIRule rule) {
				if (rule.editEnabled()) {
					try {
						parent.modelAPI.toggleActivation(rule);
					} catch (GOperationNotAllowedException e) {
						return ruleToString(rule) + " : " + e.getMessage();
					}
				}
				else {
					return ruleToString(rule) + " : This rule can not be edited";
				}
				return null;
			}

			@Override
			public void updateGUI() {
				parent.statusline.setMessage("Done");
				refresh();
				
			}
			
		};
		GMultipleJob<GIRule> job = new GMultipleJob<GIRule>("Changing rule activation", parent.getSite().getShell(), CommonUtils.convertToList(rules), elementHandler);
		job.schedule();
		
//		boolean multipleRules = rules.length > 1;
//		ArrayList<String> errorMessages = new ArrayList<String>(); 
//		for (GIRule rule : rules) {
//			
//			if (rule.editEnabled()) {
//				if (rule.isActive())
//					parent.statusline.setMessage("Deactivate the selected rule");
//				else
//					parent.statusline.setMessage("Activate the selected rule");
//				try {
//					parent.modelAPI.toggleActivation(rule);
//				} catch (GOperationNotAllowedException e) {
//					if (multipleRules) {
//						errorMessages.add(ruleToString(rule) + " : " + e.getMessage());
//					}
//					else {
//						MessageDialog.openError(null, "Error", e.getMessage());
//						return;
//					}
//				}
//				//this.parent.editor.viewer.refresh();
//			}
//			else {
//				parent.statusline.setMessage("The selected rule can not be edited");
//			}
//		}
//		GGuiUtils.showMultipleErrors(errorMessages);
//		refresh();
	}
	
	/**
	 * A listener for when the user clicks columns headers.
	 * 
	 * A bit hard coded to get sorting on the result column
	 * to play along. Something clever could probably be done 
	 * to make the column indices more dynamic. 
	 * 
	 */
	public Listener tableColumnSortListener = new Listener() {
		
		public void handleEvent(Event e) {

			/* Find an index for the clicked column header. */ 
			int columnIndex = 0;
			for (int i=0; i<parent.modelAPI.getRuleElementCount() + 2; i++) {
				if (e.widget == checkBoxRuleTableView.getTable().getColumn(i)) {
					columnIndex = i;
				}
			}

			RuleListingSorter sorter = (RuleListingSorter) checkBoxRuleTableView.getComparator();
			
			/* If the column has not been selected before -> select and sort ascending */
			if (sorter.index != columnIndex) {
				checkBoxRuleTableView.getTable().setSortColumn(checkBoxRuleTableView.getTable().getColumn(columnIndex));
				checkBoxRuleTableView.getTable().setSortDirection(SWT.UP);
				sorter.setIndex(columnIndex);
				sorter.setDirection(1);
			}
			/* If it was selected before -> change sort direction. */
			else {
				if(checkBoxRuleTableView.getTable().getSortDirection() == SWT.DOWN) {
					checkBoxRuleTableView.getTable().setSortDirection(SWT.UP);
					sorter.setDirection(1);
				}
				else {
					checkBoxRuleTableView.getTable().setSortDirection(SWT.DOWN);
					sorter.setDirection(-1);
				}
			}
			refresh();
		}
	};

	public void refresh() {
//		checkBoxRuleTableView.setComparator(null);
		shownFirst = false;
		retreiver.get(null);
		updateRuleListing();
		
	}
	
	public void exportRulesToCSV() {
		FileDialog fileChooser = new FileDialog(parent.getViewSite().getShell(), SWT.SAVE);
		String title = parent.modelAPI.getLongViewHeadline();
		title = title.replace("/", "_");
		String [] extensions = {title + ".csv", "*.csv", "*.*"};
		fileChooser.setFileName(title + ".csv");
		fileChooser.setFilterExtensions(extensions);
                fileChooser.setOverwrite(true);                
		String filePath = fileChooser.open();
		if (filePath==null)
			return;
		try {
			exportToCSV(filePath);
		} catch (IOException e) {
			MessageDialog.openError(parent.getViewSite().getShell(), "Error", "Error during export: " + e.getLocalizedMessage());
		}
		
		
	}

	private void exportToCSV(String filePath) throws IOException {
		IStructuredSelection selection = (IStructuredSelection) checkBoxRuleTableView.getSelection();
		TableColumn[] columns = checkBoxRuleTableView.getTable().getColumns();
		boolean empty = selection.isEmpty();
		if (empty) {
			checkBoxRuleTableView.getTable().selectAll();
			selection = (IStructuredSelection) checkBoxRuleTableView.getSelection();
		}
		String delimiterString = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_CSV_DELIMITER);
		char delimiter = ',';
		if (delimiterString.length()==1)
			delimiter = delimiterString.charAt(0);
			
		CsvWriter csvWriter = new CsvWriter(filePath, delimiter, null);
		for (TableColumn col : columns) {
			if (col.getImage()!=null && col.getText().equals(""))
				csvWriter.write("-->");
			else
				csvWriter.write(col.getText());
		}
		csvWriter.endRecord();
		
		Object[] array = selection.toArray();
		for (Object object : array) {
			if (object instanceof GIRule) {
				GIRule rule = (GIRule) object;
				for (GIElement element : rule.getElements()) {
					csvWriter.write(element.getLabel());
				}
				if (rule.isActive())
					csvWriter.write("-->");
				else
					csvWriter.write("-/->");
				csvWriter.write(rule.getResultElement().getLabel());
				csvWriter.endRecord();
			}
		}
		
		csvWriter.close();
		
		if (empty) {
			checkBoxRuleTableView.getTable().deselectAll();
		}
		
	}

	private void updateRuleListing() {
		if (!checkBoxRuleTableView.getTable().isDisposed()) {
			int rowIndex = -1;
			if (checkBoxRuleTableView.getTable().getSelectionCount()==1)
				rowIndex = checkBoxRuleTableView.getTable().getSelectionIndex();
			if (checkBoxRuleTableView.getTable().getSelectionCount()==1)
				rowIndex = checkBoxRuleTableView.getTable().getSelectionIndex();
			
			GIRule rule = null;
			try {
				TableItem item = checkBoxRuleTableView.getTable().getItem(rowIndex);
				Object data = item.getData();
				if (data instanceof GIRule) {
					rule = (GIRule) data;
				}
			}
			catch(Throwable t) {
				
			}
			
			checkBoxRuleTableView.refresh();
			if (rule!=null) {
				rowIndex = parent.getRuleRowIndex(rule);
				
			}
			
			layout();
			checkBoxRuleTableView.getTable().setSelection(rowIndex);
			
		}
	}

	@Override
	public void setMessage(String msg) {
		if (parent.statusline!=null)
			parent.statusline.setMessage(msg);
		
	}

	@Override
	public void setErrorMessage(String msg) {
		if (parent.statusline!=null)
			parent.statusline.setErrorMessage(msg);
		
	}

	@Override
	public void setSelection(String selectedElementId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show(boolean done) {
		if (!ruleViewSection.isDisposed()) {
			if (!shownFirst || done) {
				shownFirst = true;
				updateRuleListing();
			}
		    parent.getStopFetchingRulesAction().setEnabled(!parent.modelAPI.ready());
			ruleViewSection.setText(parent.modelAPI.getLongViewHeadline() + " (Showing " + checkBoxRuleTableView.getTable().getItemCount() + " of " + parent.modelAPI.getRuleCount() + ")");
			ruleViewSection.layout();
		
			if (done) {
				TableColumn[] columns = checkBoxRuleTableView.getTable().getColumns();
				for (TableColumn tableColumn : columns) {
					if (tableColumn.getWidth()==0)
						tableColumn.pack();
			}
		}
//			checkBoxRuleTableView.setComparator(sorter);
			
		}
		
	}

	public void selectAll() {
		checkBoxRuleTableView.getTable().selectAll();		
	}


}

