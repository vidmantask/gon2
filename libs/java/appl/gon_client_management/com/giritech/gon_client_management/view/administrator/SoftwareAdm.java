package gon_client_management.view.administrator;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class SoftwareAdm implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		layout.addStandaloneView("gon_client_management.SoftwareView", false, IPageLayout.RIGHT, 1, IPageLayout.ID_EDITOR_AREA);
		layout.setEditorAreaVisible(false);
	}
}
