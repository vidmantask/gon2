package gon_client_management.view.rule;

import org.eclipse.ui.contexts.IContextService;

/**
 * Views are created in MANIFEST.MF as extensions, and
 * added in a perspective. The KeyAssignmentView displays the 
 * rules defining key elements assigned to user elements. 
 * 
 * @author Giritech
 *
 */
public class KeyAssignmentView extends RuleView {
	
	public KeyAssignmentView() {
		super();
	}
	
	public void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.rule.authentication_strength");
	}
}
