package gon_client_management.view.ext;

import gon_client_management.ext.GILogger;
import gon_client_management.model.ext.GIConfigPanePage;
import gon_client_management.model.ext.GIGetSelectionVauesJob;
import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GIJobInfo;
import gon_client_management.model.ext.GISelectionEntry;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.dialogs.ListDialog;

public class ConfigWizardPage extends WizardPage implements GIConfigPaneContainer {

	
	protected GIConfigPanePage myConfigPane;
//	List<Widget> configurationFields = new ArrayList<Widget>();
//	private List<GIConfigRowValue> configurationValues = new ArrayList<GIConfigRowValue>();
	protected final boolean readOnly;
	protected GConfigTemplateViewUtil configTemplateUtil;


	public ConfigWizardPage(GIConfigPanePage configPanePage, GILogger logger, IDialogSettings dialogSettings, boolean readOnly) {
		super("");
		setTitle(configPanePage.getTitle());
		myConfigPane = configPanePage;
		this.readOnly = readOnly;
		this.configTemplateUtil = new GConfigTemplateViewUtil(this, myConfigPane, logger, dialogSettings);
	}
	
	public void setValues() {
		configTemplateUtil.setValues();
	}
	


	public void createControl(Composite parent) {
		/**
		 * Section for all content parts.
		 */
		Control dialogForm = configTemplateUtil.createConfigPaneContent(parent);
		
		setControl(dialogForm);
	}


	public void fieldJobFinished(final Composite parent, final GIJob job, final int fieldIndex) {
		
		final GIJobInfo status = job.getStatus();
		if (job instanceof GIGetSelectionVauesJob) {
			final List<GISelectionEntry> entries = ((GIGetSelectionVauesJob) job).getSelectionChoices();
			if (entries.size()>0) {
				
				ListDialog listDialog = new ListDialog(parent.getShell());
				listDialog.setContentProvider(new IStructuredContentProvider() {
	
					public Object[] getElements(Object inputElement) {
						return entries.toArray();
					}
	
					public void dispose() {}
	
					public void inputChanged(Viewer viewer,Object oldInput, Object newInput) {}
					
				});
				listDialog.setLabelProvider(new LabelProvider());
				listDialog.setInput(listDialog);
				listDialog.setTitle(status.getJobHeader());
				listDialog.setMessage(status.getJobInfo());
				listDialog.open();
				
				Object[] result = listDialog.getResult();
				if (result!=null && result.length>0) {
					GISelectionEntry entry = (GISelectionEntry) result[0];
					Widget widget = configTemplateUtil.configurationFields.get(fieldIndex);
					GEditorFieldCreator.setWidgetValue(widget, entry.getValue());
				}
			}
			else {
				MessageDialog.openInformation(parent.getShell(), status.getJobHeader(), status.getJobInfo());
			}
		}
		else {
			MessageDialog.openInformation(parent.getShell(), status.getJobHeader(), status.getJobInfo());
		}
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}

	@Override
	public void runFieldJob(final GJobHandler job) {
		
		final IRunnableWithProgress runnable = new IRunnableWithProgress() {

			public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
				job.run(monitor);
			}
		};
		
		try {
			getContainer().run(true, true, runnable);
		} catch (InvocationTargetException e1) {
			Throwable e2 = e1.getTargetException();
			String message = (e2!=null) ? e2.getLocalizedMessage() : e1.getLocalizedMessage();
			MessageDialog.openError(getShell(), "Error", message);
		} catch (InterruptedException e1) {
			MessageDialog.openError(getShell(), "Error", e1.getLocalizedMessage());
		}
	}
		
	


}
