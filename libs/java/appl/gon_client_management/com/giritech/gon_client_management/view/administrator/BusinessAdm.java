package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class BusinessAdm implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.GONUSERGROUP_TYPE;
		//String ruleViewId = "gon_client_management.AccessRuleView";
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.GONUSERGROUP_TYPE;
		String [] defaultElements = {GGlobalDefinitions.USER_TYPE,
									 GGlobalDefinitions.GROUP_TYPE,
									 GGlobalDefinitions.GONUSERGROUP_TYPE,
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);
	}
}
