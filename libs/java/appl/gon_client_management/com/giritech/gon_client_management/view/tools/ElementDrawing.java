package gon_client_management.view.tools;

import gon_client_management.model.ext.GIElement;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.OwnerDrawLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.graphics.TextLayout;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;

public class ElementDrawing extends OwnerDrawLabelProvider {

	public int HEIGHT = 40;
	public int WIDTH = 145;
	
	@Override
	protected void measure(Event event, Object obj) {
		event.setBounds(new Rectangle(event.x, event.y, WIDTH, HEIGHT));
	}
	
	@Override
	protected void paint(Event event, Object obj) {
		GIElement element = (GIElement) obj;
		drawElement(event, element);
	}

	/**
	 * Draw the content of an element.
	 * 
	 * @param event
	 * @param element to draw.
	 */
	public void drawElement(Event event, GIElement element) {
		draw(event, element.getLabel(), element.getInfo());
	}
	
	public void draw(Event event, String title, String subtitle) {
	
		if (!title.isEmpty()) {
			event.gc.setForeground(event.display.getSystemColor(SWT.COLOR_BLACK)); // Avoid color inversion on XP
			event.gc.drawRoundRectangle(event.x+1, event.y+1, WIDTH - 1, HEIGHT - 4, 5, 5); 
			event.gc.setBackground(event.display.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
			event.gc.fillRoundRectangle(event.x+2, event.y+2, WIDTH - 2, HEIGHT - 5, 5, 5); 
		}
		int maxTextLength = 20;
		String elementLabel = "";
		String elementInfo = "";
		int xval = event.x + 10;
		int yval = 0;
		
		// Make sure the label text fits in to the box.
		if (title.length() > maxTextLength) {
			elementLabel = title.substring(0, maxTextLength).concat("...");
		}
		else {
			elementLabel = title;
		}
		
		// Make sure the extra info fits into the box.
		if (subtitle.length() > maxTextLength) {
			elementInfo = subtitle.substring(0, maxTextLength).concat("...");
		}
		else {
			elementInfo = subtitle;
		}
		
		// Draw the element content.
		if (subtitle.isEmpty()) {
			// Center the main label - since it is all we have.
			yval = event.y + HEIGHT/2 - 8;
			event.gc.setForeground(event.display.getSystemColor(SWT.COLOR_BLACK)); // Avoid color inversion on XP
			event.gc.drawText(title, xval, yval, SWT.DRAW_TRANSPARENT);
		}
		else {
			// Make room for two lines.
			yval = event.y + 2;
			event.gc.setForeground(event.display.getSystemColor(SWT.COLOR_BLACK)); // Avoid color inversion on XP
			event.gc.drawText(elementLabel, xval, yval, SWT.DRAW_TRANSPARENT);
		
			// Print extra information
			Display display = event.item.getDisplay();
			TextLayout layout = new TextLayout(display);
			layout.setText(elementInfo);
			TextStyle italic = new TextStyle(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT), 
					display.getSystemColor(SWT.COLOR_GRAY), 
					display.getSystemColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
			layout.setStyle(italic, 0, elementInfo.length());
			layout.draw(event.gc, xval, yval+15);
		}	
	}
	
	@Override
	public String getToolTipText(Object obj) {
		GIElement element = (GIElement) obj;
		if (element.getInfo().isEmpty())
			return element.getLabel().toString();
		else
			return element.getLabel().toString() + "\n[" + element.getInfo().toString() + "]";
	}
}
