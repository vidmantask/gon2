package gon_client_management.view.element.listing;

import gon_client_management.model.ext.GIElement;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;


/**
 * class for sorting columns increasing or decreasing.
 * 
 * @author Giritech
 *
 */

public class ElementSorter {
	
	private ElementComparator elementComparatorInc;
	private ElementComparator elementComparatorDec;

	/**
	 * For sorting elements in an element listing.
	 * 
	 */
	public ElementSorter() {
		this.elementComparatorInc = new ElementComparator(1);
		this.elementComparatorDec = new ElementComparator(-1);
	}
	
	/**
	 * 
	 * @return Element sorter for decreasing sort
	 */
	public ElementComparator getDecreasingSorter() {
		return this.elementComparatorDec;
	}

	/**
	 * 
	 * @return Element sorter for increasing sort
	 */
	public ElementComparator getIncreasingSorter() {
		return this.elementComparatorInc;
	}
	
	/**
	 * Bidirectional comparator for comparing elements. 
	 * 
	 * @author Giritech
	 *
	 */
	private class ElementComparator extends ViewerComparator {

		private int direction;

		public ElementComparator(int direction) {
			this.direction = direction;
		}

		@Override
		public int compare(Viewer viewer, Object e1, Object e2) {
			if (e1 instanceof GIElement && e2 instanceof GIElement) {
				GIElement t1 = (GIElement) e1;
				GIElement t2 = (GIElement) e2;
				return super.compare(viewer, t1.getLabel().toLowerCase(), t2.getLabel().toLowerCase()) * direction;
			}
			else if (e1 instanceof GIElement) {
				return -direction;
			}
			else {
				return direction;
			}
		}
	}

}