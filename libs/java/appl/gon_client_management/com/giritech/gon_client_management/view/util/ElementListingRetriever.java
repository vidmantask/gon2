package gon_client_management.view.util;

import gon_client_management.model.GIObservableListPane;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.util.GIElementListing;

import java.util.Observable;
import java.util.Observer;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.swt.widgets.Display;


public class ElementListingRetriever {

	private GIElementListing listing = null;
	private GIObservableListPane modelAPI;
	private String selectedElementId = null;
	private String elementName;
	
	/**
	 * Fetch elements from models server side.
	 * This may take a while, so a progress bar may get displayed.
	 * 
	 * @param listing to insert the retrieve  elements into.
	 */
	public ElementListingRetriever(GIElementListing listing, GIObservableListPane modelAPI, String elementName) {
		this.listing = listing; 
		this.modelAPI = modelAPI;
		this.elementName = elementName;
	}
	
	/**
	 * Job for setting up a progress monitor and getting 
	 * the elements from the server.
	 */
	public void get(final String selectedElementId) {
		this.selectedElementId = selectedElementId;
		
		final Job job = new Job("Retreiving " + elementName + " data...") {
			
			private boolean startMonitorIf(IProgressMonitor monitor) {
				int maxCount = modelAPI.getMaxCount();
//				System.out.println(elementName + " Max count :" + maxCount);
				if (maxCount>10) {
					monitor.beginTask("Fetching data", maxCount);
					return true;
				}
				return false;
			}
			
			protected IStatus run(IProgressMonitor monitor) {
				try {
					boolean monitorStarted = startMonitorIf(monitor);
					int count = 0;
					
					while(true) {
						if (modelAPI.ready()) 
							break;
						if (monitorStarted) {
							final int n = modelAPI.getElementCount();
							final int worked = n-count;
							count = n;
							monitor.worked(worked);
						}
						else {
							monitorStarted = startMonitorIf(monitor);
						}
						synchronized (this) {
							try {
								wait(500);
							} catch (InterruptedException e) {
								// Ignore
							}
						}
					}
//					monitor.done();
					if (modelAPI.hasFetchedAll())
						return Status.OK_STATUS;
					else
						return Status.CANCEL_STATUS;
				}
				finally {
//					System.out.println(elementName + " monitor done");
					monitor.done();
				}
			}
		};
		
		/**
		 * Setup an observer that can be notified of changes
		 * in the retrieving of elements. This in turn notifies
		 * the running job.
		 */
		final Observer observer = new Observer() {
			public void update(Observable o, Object arg) {
//				System.out.println(arg.toString());
//				synchronized (job) {
					GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
						
						@Override
						public void run() {
							listing.show(false);
						}
						
					});
//					job.notify();
//				}
			}
		};		
		modelAPI.addObserver(observer);
		
		listing.setMessage("");
		listing.setErrorMessage(null);
		job.addJobChangeListener(new RetreiverJobChangeListener(observer));
		job.schedule();
	}
	
	/**
	 * Observer for the job of fetching elements from the server.
	 * 
	 * @author Giritech
	 *
	 */
	class RetreiverJobChangeListener extends JobChangeAdapter {
		
		private Observer observer;
		
		public RetreiverJobChangeListener(Observer observer) {
			this.observer = observer;
		}
		
		public void running(IJobChangeEvent event) {
			super.running(event);
		}

		public void scheduled(IJobChangeEvent event) {
			super.scheduled(event);
		}

		public void done(final IJobChangeEvent event) {
			modelAPI.deleteObserver(observer);
			Display display = GElementUpdateHandler.getDisplay();
			display.asyncExec (new Runnable () {
				public void run () {
					if (event.getResult().isOK())
						listing.setMessage("All " + elementName + " data received succesfully");
					else {
						String errorMesssage = modelAPI.getFetchElementErrorMesssage();
						if (errorMesssage==null)
							listing.setErrorMessage("Error fetching " + elementName + " data, some were not received");
						else
							listing.setErrorMessage("Error fetching " + elementName + " data: " + errorMesssage);
					}
					listing.show(true);
					if (selectedElementId!=null)
						listing.setSelection(selectedElementId);
				}
			});
		}
	}
	
}
