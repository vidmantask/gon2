package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.element.listing.ElementListingFilter;
import gon_client_management.view.preferences.PreferenceConstants;
import gon_client_management.view.util.GGuiUtils;
import gon_client_management.view.util.GGuiUtils.GIMultipleJobElementHandler;
import gon_client_management.view.util.GGuiUtils.GMultipleJob;
import gon_client_management.view.util.GGuiUtils.GMultipleJobElementHandlerAdapter;

import java.util.Comparator;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.contexts.IContextService;

import com.giritech.admin_ws.types_config_template.Value_selection_choice;

/**
 * Views are created in MANIFEST.MF as extensions, and
 * added in a perspective. The UserView displays the 
 * G elements from the user element model. 
 * 
 * @author Giritech
 *
 */
public class UserView extends ElementView {

	
	private static final String ALL_USERS = "All Directories";
	private static final String LICENSED_USERS = "Licensed only";
	protected String currentSearchCategory;
	private String[] searchCategoryMap;
	private Combo comboField;

	public UserView() {
		super();
		super.showElementEditorAction.setEnabled(false);
	}
	
	@Override
	protected GIConfig createConfig() {
		return GModelAPIFactory.getModelAPI().getConfig(GGlobalDefinitions.USER_TYPE);
	}
	
	
	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		contextService.activateContext("com.giritech.element.user");
		String[] yo = { GElementUpdateHandler.USER_RULE };
		List<String > subscriptionTypes = CommonUtils.convertToList(yo);
		GElementUpdateHandler.getElementUpdateHandler().addElementListener(subscriptionTypes, this);		
	}


	protected boolean limitResultInView() {
		return false;
	}

	@Override
	public void elementDeleted(String id) {
	}

	public void changeUserRegistration(final GIElement[] selectedElements) {
		if (selectedElements.length == 0)
			return;
		if (selectedElements.length == 1) {
			try {
				GIElement element = selectedElements[0];
				modelAPI.changeUserRegistration(element, true);
//				currentView.refresh(element.getElementId());
				listing.elementViewer.refresh();
				listing.setSelection(element.getElementId());
			} catch (GOperationNotAllowedException e) {
				MessageDialog.openError(null, "Error", e.getMessage());
			}
		}
		else {
			statusline.setMessage("Updating selected users");
			GIMultipleJobElementHandler<GIElement> elementHandler = new GMultipleJobElementHandlerAdapter<GIElement>() {

				@Override
				public String handleElement(GIElement element) {
					if (element.editEnabled()) {
						try {
							modelAPI.changeUserRegistration(element, false);
						} catch (Throwable e) {
							return element.getLabel() + " : " + e.getMessage();
						}
					}
					else {
						return element.getLabel() + " cannot be updated";
					}
					return null;
				}

				@Override
				public void updateGUI() {
					listing.elementViewer.refresh();
					listing.setSelection(selectedElements);
					statusline.setMessage("Done");
					
				}

			};
			GMultipleJob<GIElement> job2 = new GMultipleJob<GIElement>("Updating selected users", getSite().getShell(), CommonUtils.convertToList(selectedElements), elementHandler); 
					
			job2.schedule();
			
		}
	}
		
		
	public boolean showGOnUserToggleButton() {
		return true;
	}

	@Override
	public void createCustomFilter(final ElementListingFilter elementListingFilter, Composite parent) {
		Value_selection_choice[] searchCategories = modelAPI.getSearchCategories(GGlobalDefinitions.USER_TYPE);
		searchCategories = CommonUtils.selectionSort(searchCategories, new Comparator<Value_selection_choice>() {

			@Override
			public int compare(Value_selection_choice o1, Value_selection_choice o2) {
				return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
			}
		});
		
		final Composite compositeBody = GGuiUtils.createNoMarginsComposite(parent, 2);
		
		
		new Label(compositeBody, SWT.LEFT).setText("Show:  ");
		comboField = new Combo(compositeBody, SWT.READ_ONLY);

		final int noOfSearchCategories = searchCategories.length > 1 && searchCategories.length < 25 ? searchCategories.length : 0;
		searchCategoryMap = new String[2 + noOfSearchCategories];
		if (searchCategories.length == 1) 
			comboField.add(searchCategories[0].getTitle());
		else 
			comboField.add(ALL_USERS);
		searchCategoryMap[0] = null;
		comboField.add(LICENSED_USERS);
		searchCategoryMap[1] = "__REGISTERED_USERS__";
		if (noOfSearchCategories > 0) {
			int index = 2;
			for(Value_selection_choice category: searchCategories) {
				comboField.add(category.getTitle());
				searchCategoryMap[index] = category.getValue();
				index++;
			}
		}
//		comboField.setText("Licensed Users");
		GridData buttonGridData = new GridData();
//		buttonGridData.horizontalSpan = 2;
//		elementListingFilter.setDoSearch(false);
		comboField.setLayoutData(buttonGridData);
		comboField.addSelectionListener(new SelectionListener() {

			public void handleEvent(SelectionEvent e) {
				int selectionIndex = comboField.getSelectionIndex();
				setSelection(selectionIndex, elementListingFilter, searchCategoryMap);				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				handleEvent(e);
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				handleEvent(e);
				
			}
		});
		

	}


	protected String getStartingFilter() {
		
		String selected = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_USER_FILTER);
		int startIndex = 0;
		if (selected != "") {
			for(int i=1; i<searchCategoryMap.length; i++) {
				if (selected.equals(searchCategoryMap[i])) {
					startIndex = i;
					break;
				}
			}
		}
		String newSearchCategory = searchCategoryMap[startIndex];
		comboField.select(startIndex);

		
		if (newSearchCategory==null || !newSearchCategory.equals("__REGISTERED_USERS__")) {
			newSearchCategory = ElementListingFilter.getServerFilter("", newSearchCategory);
		}
		currentSearchCategory = newSearchCategory;
		return newSearchCategory;
		
		
	}

	private void setSelection(int selectionIndex, final ElementListingFilter elementListingFilter, final String[] searchCategoryMap) {

		if (selectionIndex>=0 && selectionIndex<searchCategoryMap.length) {
			String newSearchCategory = searchCategoryMap[selectionIndex];
			if (newSearchCategory != currentSearchCategory) {
				currentSearchCategory = newSearchCategory;
				if (newSearchCategory!=null && newSearchCategory.equals("__REGISTERED_USERS__")) {
					elementListingFilter.searchCategory = "";
					elementListingFilter.setDoSearch(false);
					modelAPI.search("__REGISTERED_USERS__");
					refresh();
					
				}
				else {
					elementListingFilter.setDoSearch(true);
					elementListingFilter.searchCategory = currentSearchCategory;
					elementListingFilter.refreshFilter(elementListingFilter.getEventCount());
				}
			}
		}
		String savedSearchCategory = currentSearchCategory == null ? "" : currentSearchCategory;
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_USER_FILTER, savedSearchCategory);
	}
	

}
	