package gon_client_management.view.element.listing;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.ext.GIElement;

public class ElementPropertyTester extends org.eclipse.core.expressions.PropertyTester {
	
	private int isRegisteredUser(Object receiver) {
		if (receiver instanceof GIElement) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals("User"))
				if (element.checkProperty(GGlobalDefinitions.PROPERTY_REGISTERED_USER))
					return 1;
				else if (element.checkProperty(GGlobalDefinitions.PROPERTY_UNMATCHED_REGISTERED_USER))
					return 1;
				else 
					return 0;
		}
		return -1;
		
	}

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {

		if (property.equals("hasRuleView")) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals("MenuAction")) {
				return false;
			}
			else if (element.getEntityType().equals("Tag")) {
				return false;
			}
		}
		else if (property.equals("isLaunchSpec")) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals(GGlobalDefinitions.LAUNCH_SPEC_TYPE))
				return true;
			else
				return false;
		}
		else if (property.equals("canCreateCopy")) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals(GGlobalDefinitions.ADMIN_ACCESS_TYPE) ||
				element.getEntityType().equals(GGlobalDefinitions.LAUNCH_SPEC_TYPE))
				return true;
			else
				return false;
		}
		else if (property.equals("isGroup")) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals("Group"))
				return true;
			else
				return false;
		}
		else if (property.equals("isUser")) {
			GIElement element = (GIElement) receiver;
			if (element.getEntityType().equals("User"))
				return true;
			else
				return false;
		}
		else if (property.equals("isRegisteredUser")) {
			int registeredUser = isRegisteredUser(receiver);
			return registeredUser==1;
		}
		else if (property.equals("isUnRegisteredUser")) {
			int registeredUser = isRegisteredUser(receiver);
			return registeredUser==0;
		}
		
		return true;
	}
}
