package gon_client_management.view.rule.listing;

import gon_client_management.model.GIRule;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.widgets.Text;

public class RuleFilter extends ViewerFilter {

	private Text filterInput = null; 
	
	public RuleFilter(Text filterInput) {
		this.filterInput = filterInput;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		boolean selectThis = true;
		if (filterInput.getText().length() > 0) {
			selectThis = false;
			GIRule rule = (GIRule) element;
			for (int i=0; i<rule.getRuleElementCount(); i++) {
				if(rule.getElements().get(i).getLabel().toLowerCase().contains(filterInput.getText().toLowerCase()) || 
						rule.getElements().get(i).getInfo().toLowerCase().contains(filterInput.getText().toLowerCase())) {
					selectThis = true;
				}
				if (rule.getResultElement().getLabel().toLowerCase().contains(filterInput.getText().toLowerCase()) || 
						rule.getResultElement().getInfo().toLowerCase().contains(filterInput.getText().toLowerCase())) {
					selectThis = true;
				}
			}
		}
		return selectThis;
	}
}
