package gon_client_management.view.config;

import gon_client_management.Activator;
import gon_client_management.model.GIGatewayServiceConfig;
import gon_client_management.model.GIGuiCallback;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.ext.GSafeView;
import gon_client_management.view.preferences.PreferenceConstants;

import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import com.giritech.admin_ws.GOnServiceType;




public class GConfigView extends GSafeView {

	Action stopAction, restartAction, stopWhenNoneAction, restartWhenNoneAction, refreshAction;

	public GConfigView() {
		super(Activator.getLogger(), "com.giritech.management.client");
	}

	public static final String ID = "gon_client_management.GatewayServerView";
	private TableViewer servicesViewer = null;
	private Form statusForm;
	protected boolean complete = false;
	private MyViewContentProvider provider;
	private GIGatewayServiceConfig modelAPI = null;
	private int refreshSeconds = 30;
	private Button autoUpdateButton;
	protected boolean autoUpdate = true;
	private RefreshThread autoUpdateThread;
	
	/**
	 * The content provider class is responsible for providing objects to the
	 * view. It can wrap existing objects in adapters or simply return objects
	 * as-is. These objects may be sensitive to the current input of the view,
	 * or ignore it and always show the same content (like Task List, for
	 * example).
	 */
	class MyViewContentProvider implements IStructuredContentProvider {
		

		MyViewContentProvider() {
		}
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}

		public void dispose() {
		}

		public Object[] getElements(Object parent) {
			try {
				return modelAPI.getServices();
			}
			catch (Throwable t) {
				IStatusLineManager statusLineManager = getViewSite().getActionBars().getStatusLineManager();
				statusLineManager.setErrorMessage("Error fetching services : " + t.getMessage());
				return new Object[0];
			}
		}
	}
	

	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		
		
		
		@SuppressWarnings("static-access")
		public String getColumnText(Object obj, int index) {
			GOnServiceType service = (GOnServiceType) obj;
			switch (index) {
			case 0:
				return service.getTitle();
			case 1:
				return service.getIp();
			case 2:
				return service.getPort().toString();
			case 3:
				return service.getSession_count().toString();
			case 4:
				String status = service.getStatus();
				if (status.equals(modelAPI.STATE_RUNNING)) {
					return "Running";
				}
				else if (status.equals(modelAPI.STATE_RESTART_WHEN)) {
					return "Restart when ready";
				}
				else if (status.equals(modelAPI.STATE_RESTARTING)) {
					return "Restarting";
				}
				else if (status.equals(modelAPI.STATE_STOP_WHEN)) {
					return "Stop when ready";
				}
				else if (status.equals(modelAPI.STATE_STOPPING)) {
					return "Stopping";
				}
				else {
					return "Unknown";
				}
			default:
				return "N/A";
			}
		}




		@SuppressWarnings("static-access")
		public Image getColumnImage(Object obj, int index) {
			if (index==4) {
				GOnServiceType service = (GOnServiceType) obj;
				String status = service.getStatus();
				if (status.equals(modelAPI.STATE_RUNNING)) {
					return JFaceResources.getImageRegistry().get("G_RUNNING_STATE_ICON");
				}
				else if (status.equals(modelAPI.STATE_RESTART_WHEN)) {
					return JFaceResources.getImageRegistry().get("G_RESTART_WHEN_NONE_STATE_ICON");
				}
				else if (status.equals(modelAPI.STATE_RESTARTING)) {
					return JFaceResources.getImageRegistry().get("G_RESTART_STATE_ICON");
				}
				else if (status.equals(modelAPI.STATE_STOP_WHEN)) {
					return JFaceResources.getImageRegistry().get("G_STOP_WHEN_NONE_STATE_ICON");
				}
				else if (status.equals(modelAPI.STATE_STOPPING)) {
					return JFaceResources.getImageRegistry().get("G_STOP_STATE_ICON");
				}
				else {
					return JFaceResources.getImageRegistry().get("G_UNKNOWN_STATE_ICON");
				}
			}
			return null;
		}

	}
	
	
	// This will create the columns for the table
	private void createColumns(TableViewer viewer) {
		Table table = viewer.getTable();
		String[] titles = { "Server name ", "IP", "Port", "Sessions", "Status" };
		int[] bounds = { 125, 60, 50, 80 , 80 };

		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(bounds[i]);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(true);
		}
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	

	public void createActions() {
		stopAction = new Action("Stop") {
			public void run() { 
				stopServices(false);
			}
		};
		stopAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_STOP_ACTION_ICON"));
		
		restartAction = new Action("Restart") {
			public void run() {
				restartServices(false);
			}
		};
		restartAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_RESTART_ACTION_ICON"));

		stopWhenNoneAction = new Action("Stop when no users") {
			public void run() {
				stopServices(true);
			}
		};
		stopWhenNoneAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_STOP_WHEN_NONE_ACTION_ICON"));
		
		restartWhenNoneAction = new Action("Restart when no users") {
			public void run() {
				restartServices(true);
			}
		};
		restartWhenNoneAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_RESTART_WHEN_NONE_ACTION_ICON"));
		
		refreshAction = new Action("Refresh") {
			public void run() {
				refresh();
			}
		};
		refreshAction.setImageDescriptor(JFaceResources.getImageRegistry().getDescriptor("G_REFRESH_ACTION_ICON"));
		
		servicesViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				updateActionEnablement();
			}
		});
		
//		// Add selection listener.
//		servicesViewer.getTable().addSelectionListener(new SelectionListener() {
//
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//				updateActionEnablement();
//				
//			}
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
	}
	
	@SuppressWarnings("unchecked")
	protected void restartServices(boolean whenNoUsers) {
		StructuredSelection selection = (StructuredSelection) servicesViewer.getSelection();
		Iterator iterator = selection.iterator();
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (next instanceof GOnServiceType) {
				GOnServiceType serviceType = (GOnServiceType) next;
				if (serviceType.getStatus().equals("running")) {
					modelAPI.restartService(serviceType.getSid(), whenNoUsers);
				}
			}
		}
		refresh();
		
	}

	@SuppressWarnings("unchecked")
	protected void stopServices(boolean whenNoUsers) {
		StructuredSelection selection = (StructuredSelection) servicesViewer.getSelection();
		Iterator iterator = selection.iterator();
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (next instanceof GOnServiceType) {
				GOnServiceType serviceType = (GOnServiceType) next;
				if (serviceType.getStatus().equals("running")) {
					modelAPI.stopService(serviceType.getSid(), whenNoUsers);
				}
			}
		}
		refresh();
		
	}

	@SuppressWarnings("unchecked")
	protected void updateActionEnablement() {
		if (modelAPI.isUpdateEnabled()) {
			StructuredSelection selection = (StructuredSelection) servicesViewer.getSelection();
			Iterator iterator = selection.iterator();
			while (iterator.hasNext()) {
				Object next = iterator.next();
				if (next instanceof GOnServiceType) {
					GOnServiceType serviceType = (GOnServiceType) next;
					if (serviceType.getStatus().equals("running")) {
						stopAction.setEnabled(true);
						stopWhenNoneAction.setEnabled(true);
						restartAction.setEnabled(true);
						restartWhenNoneAction.setEnabled(true);
						return;
					}
				}
			}
		}
		stopAction.setEnabled(false);
		stopWhenNoneAction.setEnabled(false);
		restartAction.setEnabled(false);
		restartWhenNoneAction.setEnabled(false);
	}

	private void createToolbar() {
		statusForm.getToolBarManager().add(stopAction);
		statusForm.getToolBarManager().add(restartAction);
		statusForm.getToolBarManager().add(stopWhenNoneAction);
		statusForm.getToolBarManager().add(restartWhenNoneAction);
		statusForm.getToolBarManager().add(refreshAction);
		statusForm.updateToolBar();
	}

	/**
	 * Create context menu.
	 */
	private void createContextMenu() {
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});
		
		// Create menu.
		Menu menu = menuMgr.createContextMenu(servicesViewer.getControl());
		servicesViewer.getControl().setMenu(menu);
		
		// Register menu for extension.
		getSite().registerContextMenu(menuMgr, servicesViewer);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		saveUserSettings();
	}

	private void saveUserSettings() {
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_SERVICE_AUTO_UPDATE, autoUpdate);		
	}

	private void loadUserSettings() {
		autoUpdate = Activator.getDefault().getPreferenceStore().getBoolean(PreferenceConstants.G_SERVICE_AUTO_UPDATE);
	}
	

	private void fillContextMenu(IMenuManager mgr) {
		mgr.add(stopAction);
		mgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		mgr.add(restartAction);
		mgr.add(stopWhenNoneAction);
		mgr.add(restartWhenNoneAction);
		
		mgr.add(new Separator());
		mgr.add(refreshAction);
	}
	

	private void createStatusView(final Composite parent) {
		
		
		/** Create a nice full width header. */
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		statusForm = toolkit.createForm(parent);
		statusForm.setText("Gateway Servers");
		statusForm.updateToolBar();
		Font verdanaFont = new Font(parent.getDisplay(), "Verdana", 12, SWT.NORMAL);
		statusForm.setFont(verdanaFont);		
		toolkit.decorateFormHeading(statusForm);
		
		/** Create layout for the main form. */
		GridLayout layout = new GridLayout(); 
		statusForm.getBody().setLayout(layout);

		loadUserSettings();
		
		Composite buttonsContainer = toolkit.createComposite(statusForm.getBody(), SWT.None);
		GridLayout controlLayout = new GridLayout(1, false);
		buttonsContainer.setLayout(controlLayout);
		GridData controlLayoutData = new GridData(GridData.FILL_HORIZONTAL);
		controlLayoutData.grabExcessHorizontalSpace = true;
		controlLayoutData.horizontalIndent = 10;
		buttonsContainer.setLayoutData(controlLayoutData);
		
		autoUpdateButton = new Button(buttonsContainer, SWT.CHECK);
		autoUpdateButton.setText("Automatic update");
		autoUpdateButton.setSelection(autoUpdate);
		autoUpdateButton.setLayoutData(new GridData(SWT.CENTER));
		autoUpdateButton.addSelectionListener(new SelectionAdapter() {
			
			public void widgetSelected(SelectionEvent e) {
				setAutoUpdate(autoUpdateButton.getSelection());
			}
			
		});
		
		/** Create a section for the services table. */
		Section servicesSection = toolkit.createSection(statusForm.getBody(), Section.NO_TITLE);
//		servicesSection.setText("Gateway Servers");
		autoUpdateButton.setBackground(servicesSection.getBackground());
		
		/** Create a layout for this section. */
		servicesSection.setLayout(new GridLayout());
		servicesSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		Composite servicesContainer = toolkit.createComposite(servicesSection);
		servicesSection.setClient(servicesContainer);
		servicesContainer.setLayout(new GridLayout());

		
		/** Add some description for this section. */
//		Label label3 = toolkit.createLabel(servicesContainer, "", SWT.WRAP);
//		label3.setText("All services listed below should be running for full functionality of the G/On " +
//				"server. Please use the Microsoft Services tool to start any services that is not listed " +
//				"as running.");
//		label3.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		
		/** Create a table with info on services. */
		servicesViewer = new TableViewer(servicesContainer, SWT.BORDER | SWT.FULL_SELECTION | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		createColumns(servicesViewer);
		provider = new MyViewContentProvider();
		servicesViewer.setContentProvider(provider);
		//servicesViewer.setContentProvider(new ViewContentProvider());
		servicesViewer.setLabelProvider(new ViewLabelProvider());
		servicesViewer.setInput(servicesContainer);
		
		servicesViewer.getTable().setLayout(new GridLayout());
		GridData ruleTableViewGridData = new GridData(GridData.FILL_BOTH);
		ruleTableViewGridData.grabExcessVerticalSpace = true;
		servicesViewer.getTable().setLayoutData(ruleTableViewGridData);
		
		/** Create a layout for the table. */
//		servicesViewer.getTable().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		
		createActions();
//		createMenu();
		createToolbar();
		createContextMenu();
//		hookGlobalActions();
		updateActionEnablement();
		
		getSite().setSelectionProvider(servicesViewer);
		setAutoUpdate(autoUpdate);
		refresh();

	}
	
	protected void setAutoUpdate(boolean value) {
		autoUpdate = value;
		if (autoUpdateThread!=null) {
			autoUpdateThread.stopThread();
		}
		if (autoUpdate) {
			autoUpdateThread = new RefreshThread(refreshSeconds);
			autoUpdateThread.start();
		}
	}

	class RefreshThread extends ThreadWithStopFlag {

		private int waitSeconds;

		RefreshThread(int waitSeconds) {
			this.waitSeconds = waitSeconds;
		}
		
		@Override
		public void run() {
			while (true) {
				if (stopThread)
					break;
				synchronized(this) {
					try {
						wait(waitSeconds*1000);
						Display display = GConfigView.this.getSite().getShell().getDisplay();
						display.asyncExec(new Runnable() {
						
							@Override
							public void run() {
								if (!servicesViewer.getTable().isDisposed() && autoUpdate)
									refresh();
							}
							
						});
					} catch (Throwable e) {
					}
						
				}
				
			}
		}
		
	}
	

	@Override
	public void createView(Composite parent) {
		modelAPI = GModelAPIFactory.getModelAPI().getGatewayServiceConfig();
		createStatusView(parent);
		
	}

	@Override
	public void setViewFocus() {
		if (servicesViewer!=null)
			servicesViewer.getControl().setFocus();
		
	}

	private void refresh() {
		GIGuiCallback callback = new GIGuiCallback() {
			
			@Override
			public void done() {
				GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {

					@Override
					public void run() {
						if (!servicesViewer.getTable().isDisposed()) {
							int[] indices = servicesViewer.getTable().getSelectionIndices();
							servicesViewer.refresh();
							servicesViewer.getTable().setSelection(indices);
							updateActionEnablement();
						}
						
					}
					
				});
				
			}
		};
		modelAPI.refresh(callback);
	}
}
