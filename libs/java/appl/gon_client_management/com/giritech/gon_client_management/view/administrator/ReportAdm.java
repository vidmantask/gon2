package gon_client_management.view.administrator;

//import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class ReportAdm implements IPerspectiveFactory {

	public void createInitialLayout(IPageLayout layout) {
		//String editorArea = layout.getEditorArea();
		//IFolderLayout paletteLayout = layout.createFolder("paletteLayout", IPageLayout.LEFT, 1f, editorArea);
		//paletteLayout.addView("gon_client_management.ReportView");
		//layout.addStandaloneView("org.eclipsercp.hyperbola.views.progress", false, IPageLayout.BOTTOM, 0.22f, layout.getEditorArea());
		
		layout.addStandaloneView("gon_client_management.ReportView", false, IPageLayout.RIGHT, 1, IPageLayout.ID_EDITOR_AREA);
		layout.setEditorAreaVisible(false);
		
	}
	
	
}
