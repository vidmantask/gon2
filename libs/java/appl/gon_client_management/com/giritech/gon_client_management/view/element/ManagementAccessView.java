package gon_client_management.view.element;

import org.eclipse.ui.contexts.IContextService;

public class ManagementAccessView extends WizardEditorElementView {


	@Override
	protected void activateContext() {
		super.activateContext();
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		if (createEnabled())
			contextService.activateContext("com.giritech.element.launch_spec");
	}

}
