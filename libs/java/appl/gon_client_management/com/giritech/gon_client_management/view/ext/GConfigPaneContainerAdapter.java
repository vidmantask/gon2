package gon_client_management.view.ext;

import gon_client_management.model.ext.GIJob;

import org.eclipse.swt.widgets.Composite;

public class GConfigPaneContainerAdapter implements GIConfigPaneContainer {

	boolean readOnly = false;
	
	@Override
	public void fieldJobFinished(Composite parent, GIJob job, int fieldIndex) {
	}

	@Override
	public boolean isReadOnly() {
		return readOnly;
	}
	
	public void setReadOnly(boolean b) {
		readOnly = b;
	}

	@Override
	public void runFieldJob(GJobHandler runnable) {
	}

	@Override
	public void setMessage(String message) {
	}

	@Override
	public void setMessage(String message, int newType) {
	}

}
