package gon_client_management.view.element;

import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GISelectionEntry;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.ext.GIConfigColumn.GISelection;
import gon_client_management.view.ext.GEditorFieldCreator;
import gon_client_management.view.ext.GEnablingControl;
import gon_client_management.view.ext.IElementEditor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.jface.dialogs.IDialogPage;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;



public class OsSecurityStatus extends WizardEditorElementView {

	
	public OsSecurityStatus() {
		
	}

	/* (non-Javadoc)
	 * @see gon_client_management.view.element.ElementView#getEditor()
	 */
	@Override
	public IElementEditor createEditor(GIElement element) {
		return createEditor(element, false);
	}

	public IElementEditor createEditor(GIElement element, boolean copy) {
		GIConfigPane configPane;
		boolean readOnly; 
		if (element!=null) {
			configPane = myConfig.getConfigPane(element, copy);
			readOnly = !editEnabled();
		}
		else {
			configPane = myConfig.getCreateConfigPane();
			readOnly = false;
		}

		//final WizardDialog dialog = new WizardDialog(getViewSite().getShell(), configWizard);
		IWizard wizard = createWizard(configPane, readOnly);
		final WizardDialog dialog = new NonmodalWizardDialog(getViewSite().getShell(), wizard, getDialogSettings());
		
		return new IElementEditor() {

			public void hide() {
				dialog.close();
			}

			public void modify(GIElement element) {
				dialog.open();
			}

			public void show() {
				dialog.open();
			}
		};
	}
	
	public abstract static class OsPage extends WizardPage
	{

		GEnablingControl mainEnablingControl;
		Button mainButton;
		
		protected OsPage(String pageName) {
			super(pageName);
			setDescription(pageName);
			setTitle("Operating System Status");
			
		}
		
		/**
		 * @see IWizardPage#canFlipToNextPage()
		 */
		public boolean canFlipToNextPage()
		{
			
			return super.canFlipToNextPage();
		}
		
		

		void createLine(Composite parent, int ncol) 
		{
			Label line = new Label(parent, SWT.SEPARATOR|SWT.HORIZONTAL|SWT.BOLD);
			GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
			gridData.horizontalSpan = ncol;
			line.setLayoutData(gridData);
		}	
		

	}

	
	private static boolean isTextNonEmpty(String s)
	{
		if ((s!=null) && (s.trim().length() >0)) return true;
		return false;
	}	
	
	
	
	private class OsConfig {
		
		private String osName;
		private GIConfigColumn allowedField;
		private GIConfigColumn versionCheckField;
		private GIConfigColumn securityCheckField;
		private Map<String, CheckField> versionCheckFieldMap = new LinkedHashMap<String, CheckField>();
		private Map<String, CheckField> securityCheckFieldMap = new LinkedHashMap<String, CheckField>();
		private Map<Widget, GIConfigRowValue> widget2Fields;
		private GIConfigPane configPane;
		private boolean readOnly;

		class CheckField {
			
			GIConfigColumn mainCheckField;
			GIConfigColumn subCheckField;
			
		}
		
		OsConfig(String osName, GIConfigPane configPane, Map<Widget, GIConfigRowValue> widget2Fields, boolean readOnly) {
			this.osName = osName;
			this.widget2Fields = widget2Fields;
			this.configPane = configPane;
			this.readOnly = readOnly;
		}
		
		void addField(GIConfigColumn field) {
			String fieldName = field.getName().substring(osName.length()+1);
			if (fieldName.equals("allowed")) {
				this.allowedField = field;
			}
			else if (fieldName.equals("version_check")) {
				this.versionCheckField = field;
			}
			else if (fieldName.equals("security_check")) {
				this.securityCheckField = field;
			}
			else if (fieldName.startsWith("version_check_field")) {
				if (fieldName.startsWith("version_check_field_main_")) {
					String versionName = fieldName.substring("version_check_field_main_".length());
					CheckField checkField = versionCheckFieldMap.get(versionName);
					if (checkField==null) {
						checkField = new CheckField();
						versionCheckFieldMap.put(versionName, checkField);
					}
					checkField.mainCheckField = field;
				}
				else if (fieldName.startsWith("version_check_field_sub_")) {
					String versionName = fieldName.substring("version_check_field_sub_".length());
					CheckField checkField = versionCheckFieldMap.get(versionName);
					if (checkField==null) {
						checkField = new CheckField();
						versionCheckFieldMap.put(versionName, checkField);
					}
					checkField.subCheckField = field;
				}
			}
			else if (fieldName.startsWith("security_check_field")) {
				if (fieldName.startsWith("security_check_field_active_")) {
					String securityName = fieldName.substring("security_check_field_active_".length());
					CheckField checkField = securityCheckFieldMap.get(securityName);
					if (checkField==null) {
						checkField = new CheckField();
						securityCheckFieldMap.put(securityName, checkField);
					}
					checkField.mainCheckField = field;
				}
				else if (fieldName.startsWith("security_check_field_value_")) {
					String securityName = fieldName.substring("security_check_field_value_".length());
					CheckField checkField = securityCheckFieldMap.get(securityName);
					if (checkField==null) {
						checkField = new CheckField();
						securityCheckFieldMap.put(securityName, checkField);
					}
					checkField.subCheckField = field;
				}
			}
		}
		
		OsSetupPage getPage() {
			if (versionCheckField!=null || securityCheckField!=null) {
				return new OsSetupPage(readOnly);
			}
			return null;
		}
		
		public GIConfigColumn getAllowedField() {
			return allowedField;
		}
		
		
		public class OsSetupPage extends OsPage
		{


			// widgets on this page 
			Button versionButton;
			Button securityButton;
			private boolean readOnly;
			
				

			
			
			/**
			 * Constructor for HolidayMainPage.
			 */
			public OsSetupPage(boolean readOnly) {
				super(osName);
				this.readOnly = readOnly;
			}
			

			/**
			 * @see IDialogPage#createControl(Composite)
			 */
			public void createControl(Composite parent) {

			    // create the composite to hold the widgets
				Composite composite =  new Composite(parent, SWT.NULL);
//				
//				mainButton = new Button(composite, SWT.CHECK);
//				mainButton.setText(allowedField.getLabel());
//				mainButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
//				
//				mainEnablingControl = new GEnablingControl(composite, mainButton);
//				mainButton.addSelectionListener(new SelectionAdapter() {
//					public void widgetSelected(SelectionEvent e) {
//						mainEnablingControl.setEnabling();
//					}
//					
//				});
//				widget2Fields.put(mainButton, configPane.getValue(allowedField.getName()));
				mainEnablingControl = new GEnablingControl(composite, null);

			    // create the desired layout for this wizard page
				GridLayout gl = new GridLayout();
				int ncol = 1;
				gl.numColumns = ncol;
				composite.setLayout(gl);
//				createLine(composite, ncol);
				
				if (versionCheckField != null)
				{
					GridData gd;
					versionButton = new Button(composite, SWT.CHECK);
					versionButton.setText("Check Version");
					gd = new GridData(GridData.FILL_HORIZONTAL);
					gd.horizontalSpan = ncol;
					versionButton.setLayoutData(gd);
					mainEnablingControl.addChild(versionButton);
					widget2Fields.put(versionButton, configPane.getValue(versionCheckField.getName()));
					
					final Composite versionComposite =  new Composite(composite, SWT.NULL);
					GridLayout g2 = new GridLayout();
					int ncol2 = 4;
					g2.numColumns = ncol2;
					versionComposite.setLayout(g2);
					versionComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					
					final GEnablingControl osEnablingControl = new GEnablingControl(versionComposite, versionButton);
					Collection<CheckField> versionCheckFields = versionCheckFieldMap.values();
					for (CheckField checkField : versionCheckFields) {
						addChoices(osEnablingControl, checkField);
					}
					
					versionButton.addSelectionListener(new SelectionAdapter() {
						public void widgetSelected(SelectionEvent e) {
							osEnablingControl.setEnabling();
		//					versionComposite.setEnabled(versionButton.getSelection());
						}
						
					});
					mainEnablingControl.addChild(osEnablingControl);

				}
				
				if (securityCheckField != null)
				{
					if (versionCheckField != null)
						createLine(composite, ncol);
					
					GridData gd;
					securityButton = new Button(composite, SWT.CHECK);
					securityButton.setText("Check Security Status");
					gd = new GridData(GridData.FILL_HORIZONTAL);
					gd.horizontalSpan = ncol;
					securityButton.setLayoutData(gd);
					mainEnablingControl.addChild(securityButton);
					widget2Fields.put(securityButton, configPane.getValue(securityCheckField.getName()));
					
					
					final Composite versionComposite =  new Composite(composite, SWT.NULL);
					GridLayout g2 = new GridLayout();
					int ncol2 = 4;
					g2.numColumns = ncol2;
					versionComposite.setLayout(g2);
					versionComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
					
					final GEnablingControl securityEnablingControl = new GEnablingControl(versionComposite, securityButton);
					Collection<CheckField> securityCheckFields = securityCheckFieldMap.values();
					for (CheckField checkField : securityCheckFields) {
						addChoices(securityEnablingControl, checkField);
					}
					
					mainEnablingControl.addChild(securityEnablingControl);
					
					securityButton.addSelectionListener(new SelectionAdapter() {
						public void widgetSelected(SelectionEvent e) {
							securityEnablingControl.setEnabling();
						}
						
					});
					
					((ConfigWizard) getWizard()).setWidgetValues();
					
				}
				if (!readOnly)
					mainEnablingControl.setEnabling();
				else
					mainEnablingControl.setEnabled(false);
			    // set the composite as the control for this page
				setControl(composite);		
				addListeners();
			}


			private GEnablingControl addChoices(GEnablingControl mainEnablingControl, CheckField checkField) {
				if (checkField.mainCheckField==null)
					return null;
				GridData gd;
				Composite versionComposite = mainEnablingControl.getParent();
				Label label = new Label (versionComposite, SWT.NONE);
				label.setText(checkField.mainCheckField.getLabel());						
				gd = new GridData();
				gd.horizontalAlignment = GridData.BEGINNING;	
				final Button checkButton = new Button(versionComposite, SWT.CHECK);
				checkButton.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
				final GEnablingControl enablingControl = new GEnablingControl(versionComposite, checkButton);
				widget2Fields.put(checkButton, configPane.getValue(checkField.mainCheckField.getName()));
				
				boolean extraFields = false;
				if (checkField.subCheckField!=null) {
					GISelection selection = checkField.subCheckField.getSelection();
					if (selection!=null) {
						extraFields = true;
						final Label servicePackLabel = new Label(versionComposite, SWT.NONE);
						servicePackLabel.setText(checkField.subCheckField.getLabel());
						final Combo servicePackCombo = new Combo(versionComposite, SWT.BORDER);
						servicePackCombo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
						List<GISelectionEntry> selectionChoices = selection.getSelectionChoices();
						for (GISelectionEntry selectionEntry : selectionChoices) {
							servicePackCombo.add(selectionEntry.getTitle());
						}
						enablingControl.addChild(servicePackLabel);
						enablingControl.addChild(servicePackCombo);
						widget2Fields.put(servicePackCombo, configPane.getValue(checkField.subCheckField.getName()));
					}
				}
				if (!extraFields) {
					GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
					gridData.horizontalSpan = 3;
					checkButton.setLayoutData(gridData);
					
				}

				mainEnablingControl.addChild(label);
				mainEnablingControl.addChild(checkButton);
				mainEnablingControl.addChild(enablingControl);
				
				checkButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						enablingControl.setEnabling();
					}
					
				});
				return enablingControl;
			}
			
			private void addListeners()
			{
			}

			


		}
		
	}
	
	private class ConfigWizard extends Wizard {

		private StartPage startPage;
		private GIConfigPane configPane;
		private Map<String, OsConfig.OsSetupPage> osPages = new LinkedHashMap<String, OsConfig.OsSetupPage>();
		
		private List<OsConfig.OsSetupPage> activeOsPages = new ArrayList<OsConfig.OsSetupPage>();
		private HashMap<String, OsConfig> osConfigMap;
		
		private Map<Widget, GIConfigRowValue> widget2Fields = new HashMap<Widget, GIConfigRowValue>();
		private boolean readOnly; 
		
		

		public ConfigWizard(GIConfigPane configPane, boolean readOnly) {
			this.configPane = configPane;
			this.readOnly = readOnly;
			osConfigMap = new HashMap<String, OsConfig>();
			for(int i=0; i<configPane.getColumnCount(); i++) {
				GIConfigColumn column = configPane.getColumn(i);
				String os = column.getSection();
				if (os != null) {
					OsConfig osConfig = osConfigMap.get(os);
					if (osConfig==null) {
						osConfig = new OsConfig(os, configPane, widget2Fields, readOnly);
						osConfigMap.put(os, osConfig);
					}
					osConfig.addField(column);
				}
			}
			Set<Entry<String, OsConfig>> values = osConfigMap.entrySet();
			for (Entry<String, OsConfig> entry : values) {
				OsConfig.OsSetupPage page = entry.getValue().getPage();
				if (page!=null) {
					osPages.put(entry.getKey(), page);
				}
				
			}
		}
		

		@Override
		public boolean performFinish() {
			if (readOnly)
				return true;
			getWidgetValues();
			try {
				configPane.save();
			} catch (GOperationNotAllowedException e) {
				MessageDialog.openError(this.getShell(), "Error Saving Element", e.getMessage());
				return false;
			}
			// TODO Auto-generated method stub
			return true;
		}
		
		void setWidgetValues() {
			Set<Entry<Widget, GIConfigRowValue>> entrySet = widget2Fields.entrySet();
			for (Entry<Widget, GIConfigRowValue> entry : entrySet) {
				GEditorFieldCreator.setWidgetValue(entry.getKey(), entry.getValue());
			}
			
		}
		
		void getWidgetValues() {
			Set<Entry<Widget, GIConfigRowValue>> entrySet = widget2Fields.entrySet();
			for (Entry<Widget, GIConfigRowValue> entry : entrySet) {
				GEditorFieldCreator.getWidgetValue(entry.getKey(), entry.getValue());
			}
			
		}

		
		public void addPages()
		{
			startPage = new StartPage(readOnly);
			addPage(startPage);
			for (OsConfig.OsSetupPage page : osPages.values()) {
				addPage(page);
			}
//			holidayPage = new WindowsPage();
//			addPage(holidayPage);
//			linuxPage = new LinuxPage();
//			addPage(linuxPage);
//			macPage = new MacPage();
//			addPage(macPage);
		}
		
		private class StartPage extends WizardPage implements Listener, SelectionListener {

			private Text inputStringField;
			private boolean readOnly;

			protected StartPage(boolean readOnly) {
				super("Start");
				setTitle("Operating System Status");
				setDescription("General");
				this.readOnly = readOnly;
			}

			public Label createLabel(Composite master, String text) {
				Label label = new Label(master, SWT.NONE);
				label.setText(text);
				GridData gridData = new GridData(GridData.BEGINNING);
				gridData.verticalAlignment = SWT.BEGINNING;
				label.setLayoutData(gridData);
				return label;
			}
			
			@Override
			public void createControl(Composite parent) {
				Composite composite =  new Composite(parent, SWT.NULL);
				GridLayout gl = new GridLayout();
				int ncol = 2;
				gl.numColumns = ncol;
				composite.setLayout(gl);

				createLabel(composite, "Name");
				inputStringField = new Text(composite, SWT.BORDER);
				GridData inputStringFieldGridData = new GridData(GridData.FILL_HORIZONTAL);
				inputStringField.setLayoutData(inputStringFieldGridData);
				widget2Fields.put(inputStringField, configPane.getValue("name"));
				
				
				createLabel(composite, "Description");
				StyledText inputTextField = new StyledText(composite, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP );
				GridData inputTextFieldGridData = new GridData(GridData.FILL_BOTH);
				inputTextFieldGridData.heightHint = 50;
				inputTextFieldGridData.widthHint = 350;
				inputTextField.setLayoutData(inputTextFieldGridData);
				widget2Fields.put(inputTextField, configPane.getValue("description"));
				
				
				for(OsConfig osConfig : osConfigMap.values()) {
					GIConfigColumn allowedField = osConfig.getAllowedField();
					if (allowedField!=null) {
						Button osAllowedButton = new Button(composite, SWT.CHECK);
						osAllowedButton.setText(allowedField.getLabel());
						widget2Fields.put(osAllowedButton, configPane.getValue(allowedField.getName()));
						GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
						gridData.horizontalSpan = ncol;
						osAllowedButton.setLayoutData(gridData);
						osAllowedButton.addSelectionListener(this);
						
					}
				}
				
				inputStringField.addListener(SWT.KeyUp, this);
				
				setControl(composite);	
				setWidgetValues();
				calculateOSPages();
				if (readOnly) {
					Control[] children = composite.getChildren();
					for (Control control : children) {
						control.setEnabled(false);
					}
				}
			}

			private void calculateOSPages() {
				getWidgetValues();
				activeOsPages.clear();
				Set<Entry<String, OsConfig.OsSetupPage>> entrySet = osPages.entrySet();
				for (Entry<String, OsConfig.OsSetupPage> entry : entrySet) {
					String fieldName = osConfigMap.get(entry.getKey()).getAllowedField().getName();
					GIConfigRowValue value = configPane.getValue(fieldName);
					if (value.getValueBoolean()) {
						activeOsPages.add(entry.getValue());
					}
				}
				
			}

			/**
			 * @see Listener#handleEvent(Event)
			 */
			public void handleEvent(Event event) {
			    // Initialize a variable with the no error status
				String currentError;
				if (isTextNonEmpty(inputStringField.getText())) {
					currentError = null;
				}
				else {
					currentError = "Name cannot be empty";
				}
				setErrorMessage(currentError);
				getWizard().getContainer().updateButtons();
			}


			
			
			@Override
			public IWizardPage getNextPage() {
				getWidgetValues();
				if (activeOsPages.size()>0) {
					return activeOsPages.get(0);
				}
				else {
					return null;
				}
			}

			
			
			@Override
			public boolean isPageComplete() {
				if (isTextNonEmpty(inputStringField.getText())) {
					return super.isPageComplete();
				}
				else {
					return false;
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				calculateOSPages();
				getWizard().getContainer().updateButtons();
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				calculateOSPages();
				getWizard().getContainer().updateButtons();
			}

//			@Override
//			public boolean canFlipToNextPage() {
//				if (isTextNonEmpty(inputStringField.getText())) {
//					return super.canFlipToNextPage();
//				}
//				else {
//					return false;
//				}
//			}

			
			
		}
		
	}

	private IWizard createWizard(GIConfigPane configPane, boolean readOnly) {
		return new ConfigWizard(configPane, readOnly);
	}
	
	
}
