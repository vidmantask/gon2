package gon_client_management.view.administrator;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.server.GIRuleSpec;
import gon_client_management.model.server.GServerInterface;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;

@SuppressWarnings("serial")
class GRuleAdmLayoutCreator {
	

	private final static class DefaultProperties extends Properties {

		public DefaultProperties() {
			super();
			setProperty(GGlobalDefinitions.USER_TYPE, "gon_client_management.UserView");
			setProperty(GGlobalDefinitions.GROUP_TYPE, "gon_client_management.UserGroupView");
			setProperty(GGlobalDefinitions.TOKEN_TYPE, "gon_client_management.KeyView");

			setProperty(GGlobalDefinitions.KEY_ASSIGNMENT_TYPE, "gon_client_management.AuthenticationView");
			setProperty(GGlobalDefinitions.USER_GROUP_TYPE,"gon_client_management.ApplicationTypeView");
			setProperty(GGlobalDefinitions.LAUNCH_SPEC_TYPE, "gon_client_management.ProgramView");

			setProperty(GGlobalDefinitions.USER_GROUP_TYPE, "gon_client_management.AllUserGroupView");
			setProperty(GGlobalDefinitions.GONUSERGROUP_TYPE, "gon_client_management.ApplicationTypeView");
			setProperty(GGlobalDefinitions.TOKEN_GROUP_TYPE, "gon_client_management.TokenGroupView");
			setProperty(GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE, "gon_client_management.TokenGroupMembershipView");
			setProperty(GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE, "gon_client_management.AuthenticationStatusView");

			setProperty(GGlobalDefinitions.ENDPOINT_GROUP_MEMBERSHIP_TYPE, "gon_client_management.EndpointGroupMembershipView");
			setProperty(GGlobalDefinitions.ENDPOINT_GROUP_TYPE, "gon_client_management.EndpointGroupView");
			setProperty(GGlobalDefinitions.ENDPOINT_TYPE, "gon_client_management.EndpointView");
			setProperty(GGlobalDefinitions.ENDPOINT_ASSIGNMENT_TYPE, "gon_client_management.PersonalDeviceView");

			setProperty(GGlobalDefinitions.ADMIN_ACCESS_TYPE, "gon_client_management.AdminAccessView");

			setProperty(GGlobalDefinitions.SECURITY_STATUS_TYPE, "gon_client_management.SecurityStatusView");
			setProperty(GGlobalDefinitions.LOGIN_INTERVAL_TYPE, "gon_client_management.LoginIntervalView");

			setProperty(GGlobalDefinitions.ZONE_TYPE, "gon_client_management.ZoneView");
			setProperty(GGlobalDefinitions.IP_RANGE_TYPE, "gon_client_management.IpRangeView");

			setProperty(GGlobalDefinitions.DEVICE_GROUP_MEMBERSHIP_TYPE, "gon_client_management.DeviceGroupView");
			setProperty(GGlobalDefinitions.DEVICE_GROUP_TYPE, "gon_client_management.DeviceGroupView");
			setProperty(GGlobalDefinitions.DEVICE_TYPE, "gon_client_management.DeviceView");
			
		}

	};
	
	private static Properties viewProperties = null;
	
	private static Properties getViewProperties() {
		if (viewProperties==null) {
			viewProperties = new Properties(new DefaultProperties());
			InputStream propertiesFile = null;
			try {
				propertiesFile = Activator.openResource("rule_customization.properties");
				if (propertiesFile!=null) {
					viewProperties.load(propertiesFile);
				}
			} catch (IOException e) {
				Activator.getLogger().logException(e);
			}
		}
		return viewProperties;
	}
	
	static LinkedHashSet<String> getRuleElementViewIds(String[] elementNames) {

		final Properties viewProperties = getViewProperties();
		LinkedHashSet<String> viewIds = new LinkedHashSet<String>();
		for(String elementName : elementNames) {
			String viewId = viewProperties.getProperty(elementName);
			if (viewId==null) {
				Activator.getLogger().logError("Unable to find view id for element '" + elementName +"'");
				viewId = "gon_client_management.ElementView:" + elementName;
			}
			viewIds.add(viewId);
		}
		return viewIds;
		
	}
	
	
	private static String getElementType(String entityType) {
		return entityType.split("\\.",2)[0];		
	}
	
	static void createInitialLayout(IPageLayout layout, 
									String ruleViewName,
									String ruleViewId,
									String [] defaultElements) {
		String editorArea = layout.getEditorArea();
		IFolderLayout paletteLayout = layout.createFolder("paletteLayout", IPageLayout.LEFT, 0.30f, editorArea);
		
		String[] viewElements = null;
		if (ruleViewName!=null)
			try {
				GIRuleSpec ruleSpec = GServerInterface.getServer().getRuleSpec(ruleViewName);
				List<GIElement> elements = ruleSpec.getTemplateRule().getElements();
				viewElements = new String[elements.size()+1];
				for(int i=0; i<elements.size(); i++) {
					GIElement element = elements.get(i);
					viewElements[i] = getElementType(element.getEntityType());
				}
				viewElements[viewElements.length-1] = getElementType(ruleSpec.getTemplateRule().getResultElement().getEntityType()); 
			}
			catch(Throwable t) {
				Activator.getLogger().logException(t);
			}
		if (viewElements==null)
			viewElements = defaultElements;
		LinkedHashSet<String> elementViewIds = getRuleElementViewIds(viewElements);
		
		for (String elementViewId : elementViewIds) {
			paletteLayout.addView(elementViewId);
			layout.getViewLayout(elementViewId).setCloseable(false);
		}
	
		//layout.addView("gon_client_management.SecurityRuleView", IPageLayout.RIGHT, 0.70f, IPageLayout.ID_EDITOR_AREA);
		layout.addStandaloneView(ruleViewId, false, IPageLayout.RIGHT, 0.70f, IPageLayout.ID_EDITOR_AREA);
		layout.getViewLayout(ruleViewId).setCloseable(false);
		
		layout.setEditorAreaVisible(false);
	}
	

}
