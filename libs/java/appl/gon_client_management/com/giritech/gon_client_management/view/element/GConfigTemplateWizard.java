package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.view.ext.ConfigWizard;
import gon_client_management.view.ext.GIConfigWizardSet;
import gon_client_management.view.ext.GIWizardExtension;

import java.util.List;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.swt.graphics.Point;

import com.giritech.admin_ws.TemplateRefType;

public class GConfigTemplateWizard implements GIConfigWizardSet {

	private GIConfig config;
	private GIWizardExtension[] elements;

	private class WizardNode implements GIWizardExtension {
		

	    protected IWizard wizard;
		private TemplateRefType template;
		
		public WizardNode(TemplateRefType templateRefType) {
			this.template = templateRefType;
			
		}

		public void dispose() {
		}

		public Point getExtent() {
	        return new Point(-1, -1);
		}

		public IWizard getWizard() {
	        if (wizard != null) {
				return wizard; // we've already created it
			}
	        GIConfigPane configPane;
	        configPane = config.getConfigPane(template.getEntity_type(), template.getTemplate_name());
	        /*
	        if (template.getTemplate_type().intValue()==0)
	        	configPane = config.getConfigPane(template.getEntity_type(), template.getTemplate_name());
	        else
	        	configPane = config.getConfigPane(template.getEntity_type());
	        	*/
	        wizard = new ConfigWizard(configPane, Activator.getLogger(), Activator.getDefault().getDialogSettings());
	        return wizard;
		}

		public boolean isContentCreated() {
	        return wizard != null;
		}
		
		String titleStr = null;
		@Override
		public String toString() {
			if (titleStr==null) {
				String description = template.getTemplate_description();
				if (description!=null)
					titleStr = template.getTemplate_title() + " ... " + description + "";
				else
					titleStr = template.getTemplate_title();
			}
			return titleStr;
		}
		
	}
	
	public List<GIWizardExtension> getExtensions() {
		return CommonUtils.convertToList(elements);
	}

	public void init(GIConfig config) {
		this.config = config;
		List<TemplateRefType> configTemplates = config.getConfigTemplates();
		elements = new WizardNode[configTemplates.size()]; 
        for(int i=0; i<configTemplates.size(); i++) {
        	elements[i] = new WizardNode(configTemplates.get(i));
        }
		
		

	}

}
