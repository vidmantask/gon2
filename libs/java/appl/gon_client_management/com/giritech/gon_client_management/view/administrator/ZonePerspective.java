package gon_client_management.view.administrator;

import gon_client_management.GGlobalDefinitions;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class ZonePerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		String ruleViewName = GGlobalDefinitions.ZONE_TYPE;
		String ruleViewId = "gon_client_management.RuleView:" + GGlobalDefinitions.ZONE_TYPE;
		String [] defaultElements = {GGlobalDefinitions.IP_RANGE_TYPE, 
									 GGlobalDefinitions.SECURITY_STATUS_TYPE,
									 GGlobalDefinitions.LOGIN_INTERVAL_TYPE,
									 GGlobalDefinitions.ZONE_TYPE
		};
		GRuleAdmLayoutCreator.createInitialLayout(layout, 
												  ruleViewName, 
												  ruleViewId, 
												  defaultElements);

	}

}
