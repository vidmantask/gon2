package gon_client_management.view.element.listing;

import gon_client_management.model.ext.GIElement;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.widgets.Text;

/**
 * Class for filtering table content.
 * 
 * @author Giritech
 *
 */
class ElementFilter extends ViewerFilter {
	
	private Text filterInput = null; 
	
	public ElementFilter(Text filterInput) {
		this.filterInput = filterInput;
	}
	
	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		if (filterInput.getText().length() > 0 && element instanceof GIElement) {
			GIElement filterElement = (GIElement) element;
			if (filterElement.getLabel().toLowerCase().contains(filterInput.getText().toLowerCase()) ||
				filterElement.getInfo().toLowerCase().contains(filterInput.getText().toLowerCase())) {
				return true;				
			} 
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}
}
