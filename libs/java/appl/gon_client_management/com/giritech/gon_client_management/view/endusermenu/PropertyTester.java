package gon_client_management.view.endusermenu;

import gon_client_management.model.GIMenuItemHandling.GIMenuItem;
import gon_client_management.model.GIMenuItemHandling.ItemType;

public class PropertyTester extends org.eclipse.core.expressions.PropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		
		if (receiver instanceof GIMenuItem && property.equals("isTopFolder")) {
			GIMenuItem item = (GIMenuItem) receiver;
            return (item.isRoot());
		}
		else if (receiver instanceof GIMenuItem && property.equals("isFolder")) {
            GIMenuItem item = (GIMenuItem) receiver;
            return (item.getType() == ItemType.MENU_FOLDER);
		}
		else if (receiver instanceof GIMenuItem && property.equals("isItem")) {
            GIMenuItem item = (GIMenuItem) receiver;
            return (item.getType() == ItemType.MENU_ITEM);
		}
		else if (receiver instanceof GIMenuItem && property.equals("isDeletable")) {
			GIMenuItem item = (GIMenuItem) receiver;
			return item.deleteEnabled();
		}		
		else {
			return false;
		}
	}
}
