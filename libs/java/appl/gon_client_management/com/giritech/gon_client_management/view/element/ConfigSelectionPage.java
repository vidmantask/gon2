package gon_client_management.view.element;

import gon_client_management.model.ext.GIConfig;
import gon_client_management.view.ext.GIConfigWizardSet;
import gon_client_management.view.ext.GIWizardExtension;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.IWizardNode;
import org.eclipse.jface.wizard.WizardSelectionPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.FilteredList;


public class ConfigSelectionPage extends WizardSelectionPage {

	private GIConfig config;

	protected ConfigSelectionPage(String pageName, GIConfig myConfig) {
		super(pageName);
		config = myConfig;
		setTitle("Launch Specification Wizards");
	}
	

	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gl = new GridLayout();
	    int ncol = 1;
	    gl.numColumns = ncol;
	    composite.setLayout(gl);		
	    // create the widgets  and their grid data objects 
	    // Date of travel
	    new Label (composite, SWT.NONE).setText("Choose template:");

	    // Filter
	    final Text text = new Text(composite,SWT.None);
		GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
		gridData.grabExcessHorizontalSpace = true;
		text.setLayoutData(gridData);
		text.setMessage("Filter");
	    
        int flags = SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.SINGLE;
        final FilteredList list = new FilteredList(composite, flags, new LabelProvider(), true, false, true);
	    text.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				list.setFilter("*" + text.getText());
				
			}
		});
        //list.setSize(0, 5);
        //list.setBounds(0, 0, 50, 50);
//        final org.eclipse.swt.widgets.List list = new org.eclipse.swt.widgets.List(composite, flags);
        
        GridData data = new GridData();
        data.widthHint = convertWidthInCharsToPixels(50);
        data.heightHint = convertHeightInCharsToPixels(50);
        data.grabExcessVerticalSpace = true;
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        data.verticalAlignment = GridData.FILL;
        list.setLayoutData(data);
        list.setFont(parent.getFont());
        //list.setFilter(""); //$NON-NLS-1$		

        List<IWizardNode> wizards = new ArrayList<IWizardNode>();
        IExtensionRegistry reg = Platform.getExtensionRegistry();
        IConfigurationElement[] extensions = reg.getConfigurationElementsFor("gon_client_management.config_wizard_set");
        for (int i = 0; i < extensions.length; i++) {
			IConfigurationElement element = extensions[i];
			try {
				Object o = element.createExecutableExtension("config_wizard_set");
				if (o instanceof GIConfigWizardSet) {
					GIConfigWizardSet wizardSet = (GIConfigWizardSet) o;
					wizardSet.init(config);
					List<GIWizardExtension> wizardList = wizardSet.getExtensions();
					wizards.addAll(wizardList);
				}
			} catch (CoreException e1) {
				e1.printStackTrace();
			}

		}
//        Comparator<IWizardNode> comparator = new Comparator<IWizardNode>() {
//
//			@Override
//			public int compare(IWizardNode o1, IWizardNode o2) {
//				String s1 = o1.toString();
//				String s2 = o2.toString();
//				return s1.compareTo(s2);
//			}
//		};
//        Collections.sort(wizards, comparator);
//	    for (IWizardNode node : wizards) {
//	    	list.add(node.toString());
//			
//		}
	    list.setElements(wizards.toArray());
        
        list.addSelectionListener(new SelectionListener() {
        	
        	IWizardNode getSelectedNode() {
        		Object[] selection = list.getSelection();
        		if (selection.length>0)
        			return (IWizardNode) selection[0];
        		return null;
        	}
            //handleDefaultSelected();
        	
            void handleSelected() {
            	IWizardNode selectedNode = getSelectedNode();
                if (selectedNode!=null)
                	setSelectedNode(selectedNode);
            }

            public void widgetDefaultSelected(SelectionEvent e) {
            	handleSelected();
            	getContainer().showPage(getNextPage());
            }
            
            public void widgetSelected(SelectionEvent e) {
            	handleSelected();

            }
        });

        List<String> templateErrors = config.getTemplateErrors();
        if (templateErrors.size()>0) {
        	String errorMessage = "Error in template file(s): ";
        	for(int i=0; i<templateErrors.size()-1; i++)
        		errorMessage += templateErrors.get(i) + ", ";
        	errorMessage += templateErrors.get(templateErrors.size()-1);
        	setErrorMessage(errorMessage);
        }
        list.setFocus();
	    setControl(composite);
	    

	}

}
