package gon_client_management.view.ext;


import gon_client_management.model.ext.GIElement;

public interface IElementEditor {

	/**
	 * Show the element editor.
	 */
	public void show();

	/**
	 * Edit a selected element.
	 * 
	 * @param the element to edit
	 */
	public void modify(GIElement element);

	/**
	 * Hide the element editor.
	 */
	public void hide();

}