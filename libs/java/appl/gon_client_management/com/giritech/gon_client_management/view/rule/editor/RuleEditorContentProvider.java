package gon_client_management.view.rule.editor;

import java.util.ArrayList;

import gon_client_management.model.GIRule;
import gon_client_management.model.GIRulePane;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class RuleEditorContentProvider implements IStructuredContentProvider {

	final GIRulePane modelAPI;
	private ArrayList<GIRule> ruleElementList;	
	private GIRule newRule = null;
	
	public RuleEditorContentProvider(GIRulePane pane) {
		this.modelAPI = pane;
		this.ruleElementList = new ArrayList<GIRule>();
		this.newRule = modelAPI.getNewEmptyRule();
		this.ruleElementList.add(newRule);
	}
	
	/**
	 * Add an element in the correct position onto the rule
	 * that is being edited.
	 * 
	 * @param element
	 */
	public void addElementToRule(GIElement element) {
		newRule = modelAPI.updateRule(newRule, element);
	}

	/**
	 * remove an element from the rule that is being edited.
	 * 
	 * @param element
	 */
	public void removeElementFromRule(GIElement element) {
		newRule = modelAPI.removeCondition(newRule, element);
	}
	
	/**
	 * Add the new rule to the model.
	 * @param selectedRule! 
	 * @throws GOperationNotAllowedException 
	 */
	public GIRule saveRule(GIRule selectedRule) throws GOperationNotAllowedException {
		if (selectedRule!=null)
			return modelAPI.replaceRule(selectedRule, newRule);
		else
			return modelAPI.addRule(newRule);
	}
	
	/**
	 * Set the content of the editor to whatever
	 * was selected in the rule table. for editing 
	 * already existing rules.
	 */
	public void setRuleEditorContent(GIRule rule) {
		this.ruleElementList.clear();
		this.newRule = modelAPI.getEditableRuleCopy(rule);
		this.ruleElementList.add(this.newRule);
	}
	
	/**
	 * Clear the rule for an empty display in the editor area.
	 */
	public void clearRuleEditor() {
		this.ruleElementList.clear();
		this.newRule = modelAPI.getNewEmptyRule();
		this.ruleElementList.add(this.newRule);
	}
	
	/**
	 * Get the rule that is in the editor.
	 * (Nothing is checked)
	 * 
	 * @return Rule from the editor.
	 */
	public GIRule getRule() {
		return this.newRule;
	}
	
	//@Override
	public Object[] getElements(Object inputElement) {
		return ruleElementList.toArray(); 
	}

	//@Override
	public void dispose() {
	}

	//@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		//System.out.println("inputChanged called");
	}

	public void setRuleActive(boolean active) {
		newRule = modelAPI.setRuleActive(newRule, active);
		
	}
	
}
