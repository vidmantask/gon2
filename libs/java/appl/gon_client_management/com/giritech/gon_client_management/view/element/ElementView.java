package gon_client_management.view.element;

import gon_client_management.Activator;
import gon_client_management.ext.CommonUtils;
import gon_client_management.model.GIElementPane;
import gon_client_management.model.GModelAPIFactory;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.provider.GLabelProvider;
import gon_client_management.view.administrator.GElementUpdateHandler;
import gon_client_management.view.administrator.GElementUpdateHandler.GIElementListener;
import gon_client_management.view.element.heading.ElementHeading;
import gon_client_management.view.element.listing.ElementListing;
import gon_client_management.view.element.listing.ElementListingFilter;
import gon_client_management.view.endusermenu.MenuView;
import gon_client_management.view.ext.GConfigPaneContainerAdapter;
import gon_client_management.view.ext.GConfigTemplateViewUtil;
import gon_client_management.view.ext.IElementEditor;
import gon_client_management.view.rule.RuleView;
import gon_client_management.view.util.GGuiUtils.GIMultipleJobElementHandler;
import gon_client_management.view.util.GGuiUtils.GMultipleJob;
import gon_client_management.view.util.GGuiUtils.GMultipleJobElementHandlerAdapter;

import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.contexts.IContextActivation;
import org.eclipse.ui.contexts.IContextService;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.plugin.AbstractUIPlugin;

/**
 * A generic element viewer for other specific element
 * views to inherit from.
 *  
 * @author Giritech
 *
 */
public class ElementView extends ConfigElementView implements GIElementListener {

	public Form elementHandlingForm;
	protected ElementListing listing = null;	
	public GIElementPane modelAPI = null;
	public FormToolkit toolkit;
	public IStatusLineManager statusline;
	
	private ElementEditor editor = null;
	
	/** Contexts defined in extensions. */
	protected static final String ELEMENT_DELETE_ENABLED = "com.giritech.element.delete.enabled";
	protected static final String ELEMENT_EDIT_ENABLED = "com.giritech.element.edit.enabled";
	protected static final String ELEMENT_EDIT_POSSIBLE = "com.giritech.element.edit.possible";
	protected static final String ELEMENT_CREATE_ENABLED = "com.giritech.element.create.enabled";
	protected static final String ELEMENT_ADD_ENABLED = "com.giritech.element.add.enabled";
	protected static final String ELEMENT_CONTEXT = "com.giritech.element.context";
	protected IContextActivation editEnabledContext;
	
	/**
	 * Creates a new element view.
	 * The parts (header/listing/editor) in this view 
	 * is created in createPartsControl. 
	 */
	public ElementView() {
		super();

	}
	
	/**
	 * These contexts are set to allow certain actions to be enabled. For instance
	 * the ELEMENT_ADD_ENABLED is always allowed for elements in an ElementView.
	 * But we do not want it to be allowed for a rule view.
	 */
	protected void activateContext() {
		IContextService contextService = (IContextService) getSite().getService(IContextService.class);
		if (deleteEnabled()) 
			contextService.activateContext(ELEMENT_DELETE_ENABLED);
		if (myConfig.isEditEnabled())
			editEnabledContext = contextService.activateContext(ELEMENT_EDIT_ENABLED);
		if (myConfig.isEditPossible())
			editEnabledContext = contextService.activateContext(ELEMENT_EDIT_POSSIBLE);
		if (createEnabled())
			contextService.activateContext(ELEMENT_CREATE_ENABLED);
		
		contextService.activateContext(ELEMENT_ADD_ENABLED);
		contextService.activateContext(ELEMENT_CONTEXT);
			
	}
	
	public IElementEditor createEditor(GIElement element) {

		ElementEditor editor = new ElementEditor(this, element);
		/* Create a tab order. */
		//elementHandlingForm.getBody().setTabList(new Control[] {listing.elementViewSection, editor.section});
		return editor;
	}

	public IElementEditor createEditor(GIElement element, boolean copy) {

		ElementEditor editor = new ElementEditor(this, element, copy);
		/* Create a tab order. */
		//elementHandlingForm.getBody().setTabList(new Control[] {listing.elementViewSection, editor.section});
		return editor;
	}
	
	/**
	 * Method that is mostly here for the possibility of
	 * over riding in inherited classes. Decides if it is
	 * possible to delete elements.
	 * 
	 * @return whether deleting is enabled
	 */
	public boolean deleteEnabled() {
		return myConfig.isDeleteEnabled();
	}

	/**
	 * Method that is mostly here for the possibility of
	 * over riding in inherited classes. Decides if it is
	 * possible to edit elements.
	 * 
	 * @return whether editing is enabled
	 */
	public boolean editEnabled() {
		return myConfig.isEditEnabled();
	}

	/**
	 * Method that is mostly here for the possibility of
	 * over riding in inherited classes. Decides if it is
	 * possible to create new elements.
	 * 
	 * @return whether creating new elements is enabled.
	 */
	public boolean createEnabled() {
		return myConfig.isCreateEnabled();
	}
	
	/**
	 * Delete a specific element from the server.
	 * This needs to be in the top/parent view to allow convenient override.
	 * 
	 * @param element to delete
	 */
	public void deleteElement(GIElement element) {
		Boolean delete = MessageDialog.openConfirm(null, "Confirm", "Are you sure you want to delete this element?");
		if (delete) {
			try {
				myConfig.deleteRow(element);
			} catch (GOperationNotAllowedException e) {
				MessageDialog.openError(null, "Error", e.getMessage());
				return;
			}
			statusline.setMessage("Deleted the selected element");
			GElementUpdateHandler.getElementUpdateHandler().fireElementDeleted(element.getEntityType(), element.getElementId());
		}
		else {
			statusline.setMessage("");
		}
	}
	
	public void deleteElements(final GIElement[] selectedElements) {
		if (selectedElements.length > 0) {
			if (selectedElements.length == 1) {
				deleteElement(selectedElements[0]);
			}
			else {
				Boolean delete = MessageDialog.openConfirm(getSite().getShell(), "Confirm", "Are you sure you want to delete these elements?");
				if (delete) {
					
					statusline.setMessage("Deleting selected elements");
					GIMultipleJobElementHandler<GIElement> elementHandler = new GMultipleJobElementHandlerAdapter<GIElement>() {

						@Override
						public String handleElement(GIElement element) {
							if (element.deleteEnabled()) {
								try {
									myConfig.deleteRow(element);
//									GElementUpdateHandler.getElementUpdateHandler().fireElementDeleted(element.getEntityType(), element.getElementId());
								} catch (GOperationNotAllowedException e) {
									return element.getLabel() + " : " + e.getMessage();
								}
							}
							else {
								return element.getLabel() + " cannot be deleted";
							}
							return null;
						}

						@Override
						public void updateGUI() {
							refresh();
							statusline.setMessage("Done");
							
						}

					};
					GMultipleJob<GIElement> job2 = new GMultipleJob<GIElement>("Deleting elements", getSite().getShell(), CommonUtils.convertToList(selectedElements), elementHandler); 
					job2.schedule();
					
				}
				
			}
		}
		
	}

	
	
	/**
	 * Setup a link to the view that displays rules.
	 * The link is discovered by finding the one that inherits from
	 * ruleview. Although this works, it is potentially error prone.
	 * There may be a smarter way of connecting the views, but
	 * this will do for now.
	 */
	public RuleView getCurrentRuleView() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewReference[] views = page.getViewReferences();
		
		for(int j=0; j<views.length; j++) {
			if(views[j].getPart(false) != null) {
				if (views[j].getPart(false) instanceof RuleView) {
					return (RuleView) views[j].getPart(false); 
					
				}
			}
		}
		return null;
	}

	/**
	 * Similar to getCurrentRuleView. 
	 * Should be redone shortly.
	 * TODO: Do this the proper way.
	 * 
	 * @return 
	 */
	public MenuView getCurrentMenuView() {
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		IViewReference[] views = page.getViewReferences();
		
		for(int j=0; j<views.length; j++) {
			if(views[j].getPart(false) != null) {
				if (views[j].getPart(false) instanceof MenuView) {
					return (MenuView) views[j].getPart(false); 
				}
			}
		}
		return null;
	}
	
	
	/**
	 * Implements WorkbenchPart.createPartControl
	 */
	@Override
	public void createView(Composite parent) {
		
		String id = this.getViewSite().getId();
		System.out.println(id);
		

		/* Create a master form for all content in this view. */
		this.toolkit = new FormToolkit(parent.getDisplay());
		this.elementHandlingForm = toolkit.createForm(parent);
		
		/* Create layout for this form. */
		GridLayout formGridLayout = new GridLayout(); 
		formGridLayout.numColumns = 1;
		elementHandlingForm.getBody().setLayout(formGridLayout);
		GridData formLayoutGridData = new GridData(GridData.FILL_VERTICAL);
		formLayoutGridData.grabExcessVerticalSpace = true;
		elementHandlingForm.getBody().setLayoutData(formLayoutGridData);
		/* Setup element sections. */
		new ElementHeading(this);
		listing = new ElementListing(this);
		listing.limitResult = limitResultInView();

		/* Setup general status line access. */
		this.statusline = (IStatusLineManager) getViewSite().getActionBars().getStatusLineManager();
		/* Setup a 'refresh' menu item for the element viewer. */
		//getViewSite().getActionBars().setGlobalActionHandler("refresh", reloadElementsAction);
		List<String> subscriptionTypes = modelAPI.getSubscriptionTypes();
		GElementUpdateHandler.getElementUpdateHandler().addElementListener(subscriptionTypes, this);
		
		/* Setup context menu. */
	    MenuManager menuMgr = new MenuManager();
	    menuMgr.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
	    /**
	     * The next two has to match in order to be able to use 'selection'
	     * in the extensions menu, command visibleWhen settings.
	     */
	    getSite().registerContextMenu(menuMgr, listing.elementViewer);
	    getSite().setSelectionProvider(listing.elementViewer);  
	    activateContext();
	    
	    stopFetchingElementsAction.setEnabled(!modelAPI.ready());
	    
	    getSite().setSelectionProvider(listing.elementViewer);
	    
//	    // MOVED TO elementListing
//	    Control control = listing.elementViewer.getControl();
//	    Menu menu = menuMgr.createContextMenu(control);
//	    control.setMenu(menu);
	}

	/** 
	 * (non-Javadoc)
	 * @see org.eclipse.ui.part.ViewPart#setInitializationData(org.eclipse.core.runtime.IConfigurationElement, java.lang.String, java.lang.Object)
	 */
	@Override
	public void setViewInitializationData(IConfigurationElement cfig, String propertyName, Object data) {
		if (data instanceof String) {
			elementType = (String) data;
			initializeModelAPI(data);
			
		}
		super.setViewInitializationData(cfig, propertyName, data);
	}

	protected String getStartingFilter() {
		return "";
	}

	protected void initializeModelAPI(Object data) {
		modelAPI = GModelAPIFactory.getModelAPI().getElementPane((String) data);
		
		GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
		
							@Override
							public void run() {
								modelAPI.search(getStartingFilter());
//								modelAPI.refreshData();
							}
							
						});
	}

	
	@Override
	public void initView(IViewSite site) {
		super.initView(site);
		if (site.getSecondaryId()!=null) {
			elementType = site.getSecondaryId();
			this.myConfig = createConfig();
			initializeModelAPI(site.getSecondaryId());
			setTitleImage(null);
			
		}
		if (modelAPI==null) {
			Throwable initializationFailure = getInitializationFailure();
			if (initializationFailure!=null)
				throw new RuntimeException(initializationFailure.getMessage());
			else
				throw new RuntimeException("Unknown error loading view");
		}
		this.setPartName(modelAPI.getColumnLabel());
	}

	/**
	 * Implements WorkbenchPart.setFocus
	 * Update the action bars to get the 'refresh' menu item enabled.
	 */
	@Override
	public void setViewFocus() {
		getViewSite().getActionBars().updateActionBars();
		listing.layout(); 
		if (getCurrentRuleView()!=null) { 
			IContextService contextService = (IContextService) getSite().getService(IContextService.class);
			contextService.activateContext("com.giritech.rule_element.context");
		}
		
	}
	
	public void refresh() {
		refreshView(null);
	}
	
	public void updateView(String selectedElementId) {
		listing.updateView(selectedElementId);
		
	}

	public void refresh(String selectedElementId) {
		if (selectedElementId!=null)
			refreshView(selectedElementId);
		else 
			refresh();
	}

	protected void refreshView(String selectedElementId) {
		modelAPI.refreshData(selectedElementId);
	    stopFetchingElementsAction.setEnabled(!modelAPI.ready());
		listing.retreiveElements(selectedElementId);
		super.refreshView(selectedElementId);
	}
	
	
//	/**
//	 * Action taken when the 'refresh' menu point is chosen.
//	 * Makes sure that the displayed elements reflects what
//	 * is on the server.
//	 */
//	public Action reloadElementsAction = new Action() {
//		public void runWithEvent(Event event) {
//			Display display = getViewSite().getShell().getDisplay();
//			display.asyncExec (new Runnable () {
//				public void run () {
//					refresh();
//				}
//			});
//		}
//	};
//	
	
	/**
	 * Create an action for creating new rules.
	 */
	protected Action showElementEditorAction = new Action() {

		@Override
		public String getText() {
			if (myConfig.isEditEnabled()) return "Add a new " + modelAPI.getColumnLabel().toLowerCase();
			else return "";
		}

		@Override
		public boolean isEnabled() {
			return createEnabled();
		}

		@Override
		public String getId() {
			return "ADD_ELEMENT";
		}

		@Override
		public void run() {
			IElementEditor editorShowing = getEditorShowing();
			if (editorShowing!=null) {
				editorShowing.hide();
			}
			IElementEditor editor = createEditor(null);
			editor.show();
		}

		@Override
		public ImageDescriptor getImageDescriptor() {
			if (JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON") == null) {
				JFaceResources.getImageRegistry().put("ADD_BUTTON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_add.png").createImage());
			}
			return JFaceResources.getImageRegistry().getDescriptor("ADD_BUTTON");
		}
	};
	private String elementType = null;
	
	//private IAction stopFetchingElementsAction = new Action("Stop", ImageDescriptor.createFromImage(PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ELCL_STOP))) {
	private IAction stopFetchingElementsAction = new Action("Stop", ImageDescriptor.createFromImage(JFaceResources.getImage("G_STOP_FETCHING_ACTION_ICON"))) {
				public void run() { 
			modelAPI.stopFetchingElements();
		}
	};

	private IAction refreshAction = new Action("Refresh", ImageDescriptor.createFromImage(JFaceResources.getImage("G_REFRESH_ACTION_ICON"))) {
		public void run() { 
			refresh();
		}
	};
	
	public Action getShowElementEditorAction() {
		return showElementEditorAction;
	}

	public IAction getStopFetchingElementsAction() {
		return stopFetchingElementsAction;
	}

	public IAction getRefreshAction() {
		return refreshAction;
	}
	

	public void setEditorShowing(ElementEditor editor) {
		this.editor = editor;
	}


	public IElementEditor getEditorShowing() {
		return editor;
	}
	
	
	protected class ElementEditor implements IElementEditor {

		public Section section = null; 
		
		protected Composite elementEditorContainer = null;
		protected Composite buttonContainer = null;
		protected Composite inputContainer; 
		
		protected Text elementLabelField = null;
		protected int height = 150;

		/** Set when an element is selected for editing */
		protected GIElement selectedElement = null;

		
		private Button addElementButton = null;
		private Button discardElementButton = null;
		@SuppressWarnings("unused")
		private Label elementLabel = null;

		private GIConfigPane configPane = null;
		
		boolean copy = false;

		private boolean isReadOnly = false;

		
		
		/**
		 * An editor for elements that is inserted into the parent view. 
		 * @param parentView
		 */
		public ElementEditor(ElementView parent, GIElement element) {
			
			init(parent, element);
		}

		public ElementEditor(ElementView parent, GIElement element, boolean copy) {
			this.copy = copy;
			init(parent, element);
		}
		

		public void init(ElementView parent, GIElement element) {
			
			
			/* Create an area for the editor */
			this.section = parent.toolkit.createSection(parent.elementHandlingForm.getBody(), Section.TITLE_BAR);
			if (element!=null)
				((Section) section).setText("Edit " + element.getLabel());
			else
				((Section) section).setText("Add new " + modelAPI.getColumnLabel());
			section.setLayout(new GridLayout());
			GridData elementEditorSectionGridData = new GridData(GridData.FILL_HORIZONTAL);
			elementEditorSectionGridData.heightHint =height;
			section.setLayoutData(elementEditorSectionGridData);

			
			/* Create a container for the element editor */
			this.elementEditorContainer = parent.toolkit.createComposite(section);
			((Section) section).setClient(elementEditorContainer);
			GridLayout elementEditorContainerGridLayout = new GridLayout();
			//elementEditorContainerGridLayout.numColumns = 2;
			elementEditorContainer.setLayout(elementEditorContainerGridLayout);
			GridData ruleEditorContainerGridData = new GridData(GridData.FILL_BOTH);
			elementEditorContainer.setLayoutData(ruleEditorContainerGridData);
			
			/* Set the area to be initially hidden */
			section.setVisible(false);
			elementEditorSectionGridData.exclude = true;
			
			final Listener saveElementListener;
			assert (myConfig!=null);
			this.inputContainer = parent.toolkit.createComposite(elementEditorContainer);
			
			GConfigPaneContainerAdapter container = new GConfigPaneContainerAdapter();
			if (element==null)
				configPane = myConfig.getCreateConfigPane();
			else
				configPane  = myConfig.getConfigPane(element, copy);

			isReadOnly  = element!=null && (!editEnabled() || configPane.isReadOnly());
			container.setReadOnly(isReadOnly);

			final GConfigTemplateViewUtil configTemplateViewUtil = new GConfigTemplateViewUtil(container, configPane, Activator.getLogger(), null);
			configTemplateViewUtil.populateFieldSection(inputContainer, toolkit, configPane);
			
			saveElementListener = new Listener() {
				public void handleEvent(Event event) {
					configTemplateViewUtil.setValues();
					save();
				}
			};

		
			final SelectionListener selectionListener = new SelectionListener() {

				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					if (!isReadOnly) {
						configTemplateViewUtil.setValues();
						save();
					}
					else
						hide();

				}

				@Override
				public void widgetSelected(SelectionEvent e) {}
				
			};
			
			Control[] children = inputContainer.getChildren();
			for (Control control : children) {
				if (control instanceof Text) {
					((Text) control).addSelectionListener(selectionListener);
				}
				
			}

			/* Create a container for keeping the buttons at the bottom */
			this.buttonContainer = parent.toolkit.createComposite(elementEditorContainer);
			GridLayout buttonContainerGridLayout = new GridLayout(); 
			buttonContainerGridLayout.numColumns = 2;
			buttonContainer.setLayout(buttonContainerGridLayout);
			GridData buttonContainerGridData = new GridData(GridData.FILL_HORIZONTAL);
			buttonContainer.setLayoutData(buttonContainerGridData);
			
			/* Create a button that will discard changes made in the editing area. */
			this.discardElementButton = parent.toolkit.createButton(buttonContainer, "Close", SWT.PUSH);
			discardElementButton.addListener(SWT.Selection, discardElementListener);
			GridData discardElementButtonGridData = new GridData(GridData.GRAB_VERTICAL);
			discardElementButtonGridData.grabExcessHorizontalSpace = true;
			discardElementButtonGridData.horizontalAlignment = SWT.RIGHT;
			//discardElementButtonGridData.exclude = true;
			discardElementButton.setLayoutData(discardElementButtonGridData);
			
			/* Create a button that will add the element to the model. */
			if (!isReadOnly) {
				this.addElementButton = parent.toolkit.createButton(buttonContainer, "Save and Close", SWT.PUSH);
				addElementButton.addListener(SWT.Selection, saveElementListener);
			}

			/* Setup a tab order */
//				elementEditorContainer.setTabList(new Control[] {inputContainer, buttonContainer});
//				inputContainer.setTabList(new Control[] {elementLabelField});
//				buttonContainer.setTabList(new Control[] {discardElementButton, addElementButton});
				
			
		}

		/**
		 * Remove all parts in the editor.
		 * This is useful if an overwrite is made and
		 * we want to make sure what we are using.
		 * 
		 * TODO: Dispose of the rest of the editor...
		 */
		public void dispose() {
			//createElementButton.setVisible(false);
			//createElementButton.removeListener(SWT.Selection, createElementListener);
			//createElementButton.dispose();
		}
		
		public void reset() {
			selectedElement = null;
			elementLabelField.setText("");
			
		}
		
		/**
		 * Save the element currently in the editor to the model.
		 * If saving is successful the editor is closed. 
		 * The only check performed is to see if the label text is
		 * non empty.
		 */
		public void save() {
			if (configPane!=null) {
				try {
					configPane.save();
					hide();
				} catch (GOperationNotAllowedException e) {
					statusline.setErrorMessage(e.getLocalizedMessage());
				}
			}
		}
		
		/**
		 * Show the element editor.
		 */
		public void show() {
			
			section.setVisible(true);
			GridData tmp = (GridData) section.getLayoutData();
			tmp.exclude = false;
			elementHandlingForm.layout();
			statusline.setMessage("");
			inputContainer.setFocus();
			setEditorShowing(this);

		}
		
		/**
		 * Hide the element editor.
		 */
		public void hide() {
			section.setVisible(false);
			GridData tmp = (GridData) section.getLayoutData();
			tmp.exclude = true;
			statusline.setMessage("");
			elementHandlingForm.layout();
			setEditorShowing(null);
		}
		
		/**
		 * Edit a selected element.
		 * 
		 * @param the element to edit
		 */
		public void modify(GIElement element) {
			selectedElement = element;
			show();
			//this.elementLabelField.setText(element.getLabel());
		}
		
		
		/**
		 * Needed here for allowing enter in the text field to save new elements.
		 */
		public SelectionListener saveElementSelectionListener = new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				ElementEditor.this.save();
			}

			/** Must implement this - not used */
			public void widgetSelected(SelectionEvent e) { }
		};
		

		/**
		 * Listener that will close the editing area without saving.
		 */
		private Listener discardElementListener = new Listener() {
			public void handleEvent(Event event) {
				hide();
				statusline.setMessage("Discarded changes");
			}
		};
		
	}


	public void elementChanged(String id) {
		refresh(id);
	}


	public void elementCreated(String id) {
		refresh(id);
	}
	
	public void elementDeleted(String id) {
		refresh(id);
		
	}
	

	public void openDefaultEditor(GIElement element) {
	}

	@Override
	protected GIConfig createConfig() {
		if (elementType !=null)
			return GModelAPIFactory.getModelAPI().getConfig(elementType);
		else
			return null;
	}

	@Override
	/**
	 * Temporary fix for setting icons for element panes.
	 */
	public Image getTitleImage() {
		//System.out.println("Gettitleimage called: " + modelAPI.getColumnType());
		if (modelAPI==null)
			return JFaceResources.getImageRegistry().get("G_GIRITECH_ICON");
		GLabelProvider labelProvider = new GLabelProvider(modelAPI.getColumnType());
		return labelProvider.getImage(null);
	}


	public String getElementType() {
		return elementType;
	}
	
	protected boolean limitResultInView() {
		return true;
	}

	public void showElementCopyEditor(GIElement element) {
		if (!createEnabled())
			return;
		IElementEditor editorShowing = getEditorShowing();
		if (editorShowing!=null) {
			editorShowing.hide();
		}
		/** Make something is selected, then try to edit. */
		if (element != null) {
			IElementEditor editor  = createEditor(element, true);
			editor.modify(element);
		}
	}

	public void addZoneRestrictions(GIElement element) {
		throw new RuntimeException("Error: This element type does not support zone restrictions");
	}
	public IDialogSettings getDialogSettings() {
		return getDialogSettings(null);
	}


	public IDialogSettings getDialogSettings(String elementId) {
		String viewId = getViewSite().getId();
		if (elementId!=null) {
			viewId += "." + elementId;
		}
		IDialogSettings section = Activator.getDefault().getDialogSettings().getSection(viewId);
		if (section==null) {
			section = Activator.getDefault().getDialogSettings().addNewSection(viewId);
		}
		return section;
		
	}

	public boolean editPossible() {
		return myConfig.isEditPossible();
	}
	
	public boolean showGOnUserToggleButton() {
		return false;
	}


	public void addCustomHeaderActions(IToolBarManager toolBarManager) {
	}

	public void createCustomFilter(ElementListingFilter elementListingFilter, Composite parent) {
		new Label(parent, SWT.NONE);
	}
	
	
}
