package gon_client_management;

import gon_client_management.ext.LaunchFailureDialog;
import gon_client_management.view.preferences.PreferenceConstants;

import org.eclipse.swt.widgets.Shell;

public class GManagementLaunchFailureDialog extends LaunchFailureDialog {

	
	public GManagementLaunchFailureDialog(Shell shell) {
		super(shell, "G/On Management");
	}


	@Override
	protected String getServerHost() {
		return Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_HOST);
	}

	@Override
	protected String getServerPort() {
		return Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_PORT);
	}

	@Override
	protected void setServerHost(String host) {
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_MANAGEMENT_SERVER_HOST, host);
	}

	@Override
	protected void setServerPort(String port) {
		Activator.getDefault().getPreferenceStore().setValue(PreferenceConstants.G_MANAGEMENT_SERVER_PORT, port);
	}
}
