package gon_client_management;

import gon_client_management.model.GModelAPIFactory;
import gon_client_management.view.preferences.PreferenceConstants;

import javax.security.auth.login.LoginException;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.FormDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

public class GLoginDialog extends FormDialog {

	private final static int F_LABEL_HORIZONTAL_INDENT = 0;
	private final static int F_TEXT_WIDTH_HINT = 160;
	private final static int F_COLUMN_COUNT = 3;
	public static String userName = null;
	private Text fTextUsername;
	private Text fTextPassword;
	private Button fButtonSkip;
	private String gon_skip_login;
	private Form dialogForm;
	private boolean alwaysLogin;
	
	public GLoginDialog(Shell shell, boolean alwaysLogin) {
		super(shell);
		this.alwaysLogin = alwaysLogin;
		gon_skip_login = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_SKIP_LOGIN);
		/**
		 * Should we display this dialog or not.
		 */
	}
	
	@Override
	public int open() {
		if (!alwaysLogin) {
			if (gon_skip_login.equals(MessageDialogWithToggle.ALWAYS)) {
				userName = "";
				return Window.OK;
			}
		}
		return super.open();
	}

	@Override
	protected void createFormContent(IManagedForm mform) {
		super.createFormContent(mform);

		this.dialogForm = (Form) mform.getForm().getContent();
		dialogForm.getShell().setText("G/On Management");
		FormToolkit toolkit = mform.getToolkit();
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		dialogForm.getBody().setLayout(layout);
		/* Setup the header for this dialog */
		dialogForm.setText("Login");
		toolkit.decorateFormHeading(dialogForm);
	
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
		data.horizontalSpan = F_COLUMN_COUNT;
		dialogForm.setLayoutData(data);
	
		/* Create a container for inserting parts for this section. */
		Composite selectionContainer = toolkit.createComposite(dialogForm.getBody());
		
		/* Create layout for the container. */
		GridLayout selectionContainerGridLayout = new GridLayout();
		selectionContainerGridLayout.numColumns = F_COLUMN_COUNT;
		selectionContainer.setLayout(selectionContainerGridLayout);
		GridData selectionContainerGridData = new GridData(GridData.FILL_BOTH);
		selectionContainer.setLayoutData(selectionContainerGridData);

		/**
		 * Label for user-name
		 */
		Label usernamelabel = toolkit.createLabel(selectionContainer, "&User Name:");
		{
			GridData labeldata = new GridData();
			labeldata.horizontalIndent = F_LABEL_HORIZONTAL_INDENT;
			usernamelabel.setLayoutData(labeldata);
		}
		
		/**
		 * Text field for user-name
		 */
		fTextUsername = toolkit.createText(selectionContainer, "", SWT.BORDER);
		String userName = PlatformUI.getPreferenceStore().getString(PreferenceConstants.G_LAST_USER_NAME);
		if (userName!=null) {
			fTextUsername.setText(userName);
		}
		GridData fielddata = new GridData(SWT.NONE, SWT.NONE, false, false);
		fielddata.widthHint = F_TEXT_WIDTH_HINT;
		fielddata.horizontalSpan = 2;
		fTextUsername.setLayoutData(fielddata);		

		/**
		 * Label for password
		 */
		Label passwordlabel = toolkit.createLabel(selectionContainer, "&Password:");
		{
			GridData labeldata = new GridData();
			labeldata.horizontalIndent = F_LABEL_HORIZONTAL_INDENT;
			passwordlabel.setLayoutData(labeldata);
		}

		/**
		 * Text field for password
		 */
		fTextPassword = toolkit.createText(selectionContainer, "", SWT.PASSWORD | SWT.BORDER);
		fTextPassword.setLayoutData(fielddata);		

		
		/**
		 * Check button for skipping this login
		 */
		if (!gon_skip_login.equals(MessageDialogWithToggle.NEVER)) {

			fButtonSkip = toolkit.createButton(selectionContainer, "Skip login", SWT.CHECK | SWT.COLOR_WHITE);
			if (gon_skip_login.equals("noprompt")) {
				fButtonSkip.setSelection(true);
			}
			GridData buttondata = new GridData(SWT.NONE, SWT.NONE, false, false);
			buttondata.verticalIndent = 5;
			fButtonSkip.setLayoutData(buttondata);
			fButtonSkip.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					boolean selected = fButtonSkip.getSelection();
					fTextPassword.setEnabled(!selected);
					fTextUsername.setEnabled(!selected);
				}
			});
		}
		dialogForm.getShell().setMinimumSize(400, 400);
	}

	private void setAuthenticated(String name) {
		userName = name;
	}
	
	@Override
	protected void cancelPressed() {
		super.cancelPressed();
		System.exit(0);
	}

	@Override
	protected void okPressed() {
		if (fButtonSkip!=null && fButtonSkip.getSelection()) {
			try {
				GModelAPIFactory.getModelAPI().login();
			} catch (Throwable t) {
				setAuthenticated(null);
				return;
			}
			setAuthenticated("");
			super.okPressed();
		}
		else {
		
			String username = fTextUsername.getText();
			String password = fTextPassword.getText();
			/**
			 * Authentication is successful if a user provides any user-name and
			 * any password
			 */
			if ((username.length() > 0) &&
					(password.length() > 0)) {
				try {
					GModelAPIFactory.getModelAPI().login(username, password);
					setAuthenticated(username);
					super.okPressed();
				} catch (LoginException e) {
					MessageDialog.openError(this.getShell(), "Authentication Failed", e.getMessage());
				} catch (Throwable t) {
					MessageDialog.openError(this.getShell(), "Error during login", t.getMessage()); 
				}
			} 
			else {
				MessageDialogWithToggle start = MessageDialogWithToggle.openYesNoQuestion(this.getShell(), 
						"Invalid Login",
						"A username and password must be specified to login.\n" +
						   "Do you want to open G/On Management without logging in?", 
						"Always open G/On Management without a login", 
						false, 
						Activator.getDefault().getPreferenceStore(), 
						"gon_skip_login");
				
				if (start.getReturnCode()==IDialogConstants.YES_ID) {
					setAuthenticated("");
					super.okPressed();
				}
			}
		}
	}
}
