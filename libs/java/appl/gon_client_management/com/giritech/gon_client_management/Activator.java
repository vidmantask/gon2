package gon_client_management;


import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.GILogger;
import gon_client_management.model.localservice.GLocalService;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "gon_client_management";

	// The shared instance
	private static Activator plugin;
	
	/**
	 * The constructor
	 */
	public Activator() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
		GLocalService.stopLocalService();
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	public static ILog getClientLog() {
		plugin.getBundle().getSymbolicName();
		return plugin.getLog();
	}
	
	public static String getPluginId() {
		return plugin.getBundle().getSymbolicName();
	}
	
	private static GLogger logger = null; 

	public static GILogger getLogger() {
		if (logger==null) {
			logger = new GLogger();
		}
		return logger;
	}
	
	private static class GLogger implements GILogger {

		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#log(int, java.lang.String)
		 */
		public void log(int severity, String message) {
			Status msg = new Status(severity, getPluginId(), message);
			plugin.getLog().log(msg);
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logError(java.lang.String)
		 */
		public void logError(String message) {
			log(IStatus.ERROR, message);
		}
	
	
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logInfo(java.lang.String)
		 */
		public void logInfo(String message) {
			log(IStatus.INFO, message);
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.GILogger#logException(java.lang.Throwable)
		 */
		public void logException(Throwable t) {
			logError(CommonUtils.getStackTrace(t));
		}
	}
	
	public static InputStream openResource(String pathName) throws IOException {
		IPath path = Path.fromOSString(pathName);
		URL url = FileLocator.find(plugin.getBundle(), path, null);
		if (url!=null) {
			return url.openStream();
		}
		return null;
		
	}

	
}
