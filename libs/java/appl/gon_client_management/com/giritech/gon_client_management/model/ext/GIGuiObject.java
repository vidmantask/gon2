package gon_client_management.model.ext;

public interface GIGuiObject {

	public boolean deleteEnabled();
	public boolean editEnabled();

}
