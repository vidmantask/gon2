package gon_client_management.model;

import java.util.List;

import com.giritech.admin_ws.GOnSessionType;

public interface GIGatewaySessionsConfig extends GIObservableListPane {

	List<GOnSessionType> getSessions();

	void stopRetrieve();
	
	void setServerSids(String [] serverSids);
	
	void setSearchFilter(String filter);
	
	boolean stopSession(String serverSid, String uniqueSessionId);
	
	boolean recalculateMenuForSession(String serverSid, String uniqueSessionId);
	
	public void refreshData(boolean background);
	
	public boolean isUpdateEnabled();
	
	

}
