package gon_client_management.model.server;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.GIDeployment.KeyPair;
import junit.framework.TestCase;

public class GServerTest extends TestCase {
	
	private GIServer server;

	public GServerTest() {
		server = GServer.getTestServer();
	}

	public void testGetRuleElementHeader() {
		server.getRuleElementHeader(GGlobalDefinitions.USER_TYPE);
	}
	
	public void testDeployGetKnownSecretClient() {
		String secretClient = server.deployGetKnownSecretClient();
		System.out.println(secretClient);
	}
	
	public void testDeployGenerateKeyPair() {
		KeyPair keyPair = server.deployGenerateKeyPair();
		System.out.println(keyPair.getPrivateKey());
		System.out.println(keyPair.getPublicKey());
	}

}
