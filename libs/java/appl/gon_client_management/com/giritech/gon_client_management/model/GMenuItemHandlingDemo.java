package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GMenuItemHandlingDemo implements GIMenuItemHandling {

	private GMenuItem topmenu;
	private HashMap<String, GMenuItem> items;

	
	@SuppressWarnings("serial")
	private class CycleDetectedException extends Exception {
		
	}
	
	/**
	 * Temporary implementation of the G menu for menu handling.
	 * 
	 * @author cso
	 *
	 */
	public class GMenuItem implements GIMenuItem {


		private String identifier = null;
		private ItemType type = null;
		private String label = null;
		private ArrayList<GIMenuItem> items = null;
		private boolean deletable = true;
		
		
		
		public GMenuItem(String identifier, ItemType type, String label) {
			this.identifier = identifier;
			this.type = type;
			this.label = label;
			if (type == ItemType.MENU_FOLDER)
				this.items = new ArrayList<GIMenuItem>(); // Create this always to avoid refresh problems.
			GMenuItemHandlingDemo.this.items.put(identifier, this);
		}
		
		public GMenuItem(GMenuItem oldRoot) {
			this(oldRoot.identifier, oldRoot.type, oldRoot.label);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof GMenuItem) {
				return identifier.equals(((GMenuItem) obj).identifier);
			}
			return super.equals(obj);
		}

		public ItemType getType() {
			return type;
		}

		public String getIdentifier() {
			return identifier;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;

		}


		public void addItem(String identifier, ItemType type, String label) {
			if (items == null) {
				this.items = new ArrayList<GIMenuItem>();
			}
			this.items.add(new GMenuItem(identifier, type, label));
		}


		

		public ArrayList<GIMenuItem> getChildren() {
			return items;
		}

		@Override
		public boolean deleteEnabled() {
			return deletable;
		}


		public void copy(GMenuItem oldItem) {
			if (oldItem.items==null)
				return;
			for (int i=0; i<oldItem.items.size(); i++) {
				GMenuItem item = (GMenuItem) oldItem.items.get(i);
				GMenuItem newItem = getItem(item.identifier);
				if (newItem==null) {
					newItem = new GMenuItem(item);
					newItem.copy(item);
				}
				this.items.add(newItem);
			}
			
		}

		public void checkCycles(GMenuItem dropee, GMenuItem target, boolean found) throws CycleDetectedException {
			if (!found && identifier.equals(dropee.identifier)) 
				found = true;
			if (found && identifier.equals(target.identifier))
				throw new CycleDetectedException();
			if (items!=null) {
				for(int i=0; i < items.size(); i++) {
					GMenuItem item = (GMenuItem) items.get(i);
					item.checkCycles(dropee, target, found);
				}
			}
			
			
			
		}

		public void checkCycles(GIMenuItem item, GIMenuItem toItem) throws CycleDetectedException {
			checkCycles((GMenuItem) item, (GMenuItem) toItem, false);
		}

		@Override
		public boolean isRoot() {
			return false;
		}

	}
	
	/**
	 * Constructor for the menu handler.
	 * Contains demo data for now.
	 */
	public GMenuItemHandlingDemo() {
		 this.items = new HashMap<String,GMenuItem>();
		 this.topmenu = new GMenuItem("-1", ItemType.MENU_ITEM, "container");
		 topmenu.addItem("0", ItemType.MENU_FOLDER, "G/On Menu");
		 getItem("0").addItem("1", ItemType.MENU_ITEM, "Item 1");
		 getItem("0").addItem("2", ItemType.MENU_ITEM, "Item 2");
		 getItem("0").addItem("3", ItemType.MENU_ITEM, "Item 3");
		 getItem("0").addItem("4", ItemType.MENU_FOLDER, "Folder 1");
		 getItem("4").addItem("5", ItemType.MENU_FOLDER, "Folder 2");
		 getItem("5").addItem("6", ItemType.MENU_ITEM, "Item 4");
		 getItem("5").addItem("7", ItemType.MENU_FOLDER, "Folder 7");
		 getItem("0").addItem("8", ItemType.MENU_FOLDER, "Folder 8");
		 getItem("0").addItem("9", ItemType.MENU_FOLDER, "Folder 9");
		 
		 
	}
	
	private GMenuItem getItem(String string) {
		return items.get(string);
	}

	public GIMenuItem getTopMenu() {
		return this.topmenu;
	}

	@Override
	public void removeItem(GIMenuItem item, GIMenuItem fromItem) {
		//System.out.println("Attempting to remove: " + item.getLabel() + " from " + item.getParent().getLabel());
		((GMenuItem)fromItem).items.remove(item);
	}

	@Override
	public void addElement(GIElement element, GIMenuItem item) throws GOperationNotAllowedException {
		String elementId = element.getElementId();
		GMenuItem elementItem = getItem(elementId);
		if (elementItem!=null) {
			copyItem(elementItem, item);
		}
		else {
			((GMenuItem)item).addItem(elementId, ItemType.MENU_ITEM, element.getLabel());
		}
	}
	
	@Override
	public void copyItem(GIMenuItem item, GIMenuItem toItem)
			throws GOperationNotAllowedException {
		
		try {
			topmenu.checkCycles(item, toItem);
		} catch (CycleDetectedException e) {
			throw new GOperationNotAllowedException("Menu cannot appear in itself");
		}
		GMenuItem menuItem = (GMenuItem) item;
		GMenuItem toMenuItem = (GMenuItem) toItem;
		
		toMenuItem.items.add(menuItem);
		
	}

	@Override
	public void moveItem(GIMenuItem item, GIMenuItem fromItem, GIMenuItem toItem) throws GOperationNotAllowedException {
		try {
			topmenu.checkCycles(item, toItem);
		} catch (CycleDetectedException e) {
			throw new GOperationNotAllowedException("Menu cannot appear in itself");
		}
		copyItem(item, toItem);
		removeItem(item, fromItem);
		
	}

	@Override
	public boolean dropAllowed(Object obj, GIMenuItem target) {
		if (obj instanceof GIMenuItem) {
			GIMenuItem dropee = (GIMenuItem) obj;
			if (dropee.getType()==ItemType.MENU_FOLDER && target.getType()==ItemType.MENU_FOLDER) {
				// Check cycles
				try {
					topmenu.checkCycles((GMenuItem) dropee, (GMenuItem) target, false);
				} catch (CycleDetectedException e) {
					System.out.println("Cycle detected!");
					return false;
				}
			}
		}
		return false;
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getSubscriptionTypes() {
		return new ArrayList<String>();
	}

}
