package gon_client_management.model;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.giritech.admin_ws.Item_type_type0;
import com.giritech.admin_ws.MenuItemRef;
import com.giritech.admin_ws.MenuItemType;
import com.giritech.admin_ws.MenuType;

public class GMenuItemHandling implements GIMenuItemHandling {

	private GMenuItem topmenu;
	private HashMap<String, GMenuItem> items;
	private GMenuItem rootMenuItem = null;

	
	@SuppressWarnings("serial")
	private class CycleDetectedException extends Exception {
		
	}
	
	/**
	 * Temporary implementation of the G menu for menu handling.
	 * 
	 * @author cso
	 *
	 */
	public class GMenuItem implements GIMenuItem {


		private String id = null;
		private ItemType type = null;
		private String label = null;
		private ArrayList<GIMenuItem> items = new ArrayList<GIMenuItem>();
		private boolean deletable = true;
		private MenuItemType menuItemType;
		private int maxItems = -1;
		
		
		
		public GMenuItem(String identifier, ItemType type, String label) {
			this.id = identifier;
			this.type = type;
			this.label = label;
			if (type == ItemType.MENU_FOLDER)
				this.items = new ArrayList<GIMenuItem>(); // Create this always to avoid refresh problems.
			GMenuItemHandling.this.items.put(identifier, this);
		}
		
		public GMenuItem(GMenuItem oldRoot) {
			this(oldRoot.id, oldRoot.type, oldRoot.label);
		}
		

		public GMenuItem(MenuItemType menuItemType) {
			id = menuItemType.getId();
			update(menuItemType);
		}
		
		public void update(MenuItemType menuItemType) {
			label = menuItemType.getLabel();
			setType(menuItemType.getItem_type()); 
			this.menuItemType = menuItemType;
			setMaxItems(menuItemType.getMax_items());
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof GMenuItem) {
				return id.equals(((GMenuItem) obj).id);
			}
			return super.equals(obj);
		}

		public ItemType getType() {
			return type;
		}

		public String getId() {
			return id;
		}

		public String getLabel() {
			return label;
		}

		public int getMaxItems() {
			return maxItems ;
		}

		public ArrayList<GIMenuItem> getChildren() {
			return items;
		}

		@Override
		public boolean deleteEnabled() {
			return deletable;
		}



		public void checkCycles(GMenuItem dropee, GMenuItem target, boolean found) throws CycleDetectedException {
			if (!found && id.equals(dropee.id)) 
				found = true;
			if (found && id.equals(target.id))
				throw new CycleDetectedException();
			if (items!=null) {
				for(int i=0; i < items.size(); i++) {
					GMenuItem item = (GMenuItem) items.get(i);
					item.checkCycles(dropee, target, found);
				}
			}
			
			
			
		}

		public void checkCycles(GIMenuItem item, GIMenuItem toItem) throws CycleDetectedException {
			checkCycles((GMenuItem) item, (GMenuItem) toItem, false);
		}

		public void setMaxItems(BigInteger max_items) {
			if (max_items != null)
				maxItems = menuItemType.getMax_items().intValue();
			else
				maxItems = -1;
			
		}

		public void setType(Item_type_type0 item_type) {
			if (item_type == Item_type_type0.menu_item) 
				type = ItemType.MENU_ITEM;
			else if (item_type == Item_type_type0.menu_folder) 
				type = ItemType.MENU_FOLDER;
			else if (item_type == Item_type_type0.auto_menu) 
				type = ItemType.AUTO_MENU;
			else if (item_type == Item_type_type0.auto_item) 
				type = ItemType.AUTO_ITEM;
			else 
				throw new RuntimeException("Unknown item type : " + item_type);
			
		}

		@Override
		public boolean isRoot() {
			return this==rootMenuItem;
		}

	}
	
	/**
	 * Constructor for the menu handler.
	 * Contains demo data for now.
	 */
	public GMenuItemHandling() {
		this.items = new HashMap<String,GMenuItem>();
		MenuItemType[] menuItems = GServerInterface.getServer().getMenuItems();
		for (MenuItemType menuItemType : menuItems) {
			GMenuItem menuItem = new GMenuItem(menuItemType);
			items.put(menuItem.getId(), menuItem);
		}
		MenuType menu = GServerInterface.getServer().getMenu();
		this.topmenu = new GMenuItem("", ItemType.MENU_ITEM, "container");
		
		buildMenu(menu);
		 
		 
	}

	private void buildMenu(MenuType menu) {
		rootMenuItem = getItem(menu.getRoot_id());
		topmenu.items.add(rootMenuItem);
		MenuItemRef[] menu_items = menu.getMenu_items();
		for (MenuItemRef menuItemRef : menu_items) {
			GMenuItem item = getItem(menuItemRef.getId());
			String[] children = menuItemRef.getChildren();
			if (children!=null)
				for (String childId : children) {
					GMenuItem child = getItem(childId);
					if (child==null) {
						Activator.getLogger().logError("Unable to find sub menu item '" + childId + "' for menu folder '" + menuItemRef.getId() + "'");
					}
					else {
						item.items.add(child);
					}
				}
			
		}
	}
	
	private GMenuItem getItem(String string) {
		return items.get(string);
	}

	public GIMenuItem getTopMenu() {
		return this.topmenu;
	}

	@Override
	public void removeItem(GIMenuItem item, GIMenuItem fromItem) {
		MenuItemType menuItemType = ((GMenuItem) item).menuItemType;
		MenuItemType fromMenuItemType = ((GMenuItem) fromItem).menuItemType;
		MenuType newMenu = GServerInterface.getServer().removeItemFromMenu(menuItemType, fromMenuItemType);
		updateMenu(newMenu);
		
	}

	private void updateMenu(MenuType newMenu) {
		String newRootId = newMenu.getRoot_id();
		if (rootMenuItem==null || rootMenuItem.getId()==newRootId) {
			// Menu changed completely
			//topmenu.items.clear();
		}
		Collection<GMenuItem> values = items.values();
		for (GMenuItem menuItem : values) {
			menuItem.items.clear();
		}
		buildMenu(newMenu);

		
	}

	
    @Override
	public void addElement(GIElement element, GIMenuItem item) throws GOperationNotAllowedException {
		String elementId = ((GIModelElement) element).getId();
		GMenuItem elementItem = getItem(elementId);
		if (elementItem!=null) {
			copyItem(elementItem, item);
		}
		else {
			Pair<MenuItemType, MenuType> pair = GServerInterface.getServer().addElementToMenu(element, ((GMenuItem) item).menuItemType);
			GMenuItem menuItem = new GMenuItem(pair.getFirst());
			items.put(menuItem.getId(), menuItem);
			updateMenu(pair.getSecond());
		}
	}
	
	@Override
	public void copyItem(GIMenuItem item, GIMenuItem toItem)
			throws GOperationNotAllowedException {
		
		try {
			topmenu.checkCycles(item, toItem);
		} catch (CycleDetectedException e) {
			throw new GOperationNotAllowedException("Menu cannot appear in itself");
		}
		GMenuItem menuItem = (GMenuItem) item;
		GMenuItem toMenuItem = (GMenuItem) toItem;
		
		MenuType menu = GServerInterface.getServer().addItemToMenu(menuItem.menuItemType, toMenuItem.menuItemType);
		updateMenu(menu);
		
	}

	@Override
	public void moveItem(GIMenuItem item, GIMenuItem fromItem, GIMenuItem toItem) throws GOperationNotAllowedException {
		try {
			topmenu.checkCycles(item, toItem);
		} catch (CycleDetectedException e) {
			throw new GOperationNotAllowedException("Menu cannot appear in itself");
		}
		GMenuItem menuItem = (GMenuItem) item;
		GMenuItem toMenuItem = (GMenuItem) toItem;
		GMenuItem fromMenuItem = (GMenuItem) fromItem;
		
		MenuType menu = GServerInterface.getServer().
							moveItemToMenu(menuItem.menuItemType, 
									       fromMenuItem.menuItemType, 
									       toMenuItem.menuItemType);
		updateMenu(menu);
		
	}

	@Override
	public boolean dropAllowed(Object obj, GIMenuItem target) {
		if (!(target.getType()==ItemType.MENU_ITEM)) {
			if (obj instanceof GIElement) {
				GIElement element = (GIElement) obj;
				String elementId = ((GIModelElement) element).getId();
				if (element.getEntityType().equals("Tag")) {
					GMenuItem existingItem = getItem(elementId);
					if (existingItem==null)
						return target.getType()==ItemType.MENU_FOLDER;
					else
						obj = existingItem;
				}
				else if (element.getEntityType().equals(GGlobalDefinitions.MENU_ACTION_TYPE) ||
						 element.getEntityType().equals(GGlobalDefinitions.LAUNCH_SPEC_TYPE)) {
					GMenuItem existingItem = getItem(elementId);
					if (existingItem==null)
						return true;
					else
						obj = existingItem;
				}
				else
					return false;
			}
			
			if (obj instanceof GIMenuItem) {
				GIMenuItem dropee = (GIMenuItem) obj;
				if (dropee.getType()==ItemType.MENU_FOLDER ||
					dropee.getType()==ItemType.AUTO_MENU) {
					if (//target==rootMenuItem || 
						target.getChildren().contains(dropee))
						return false;
					// Check cycles
					try {
						topmenu.checkCycles((GMenuItem) dropee, (GMenuItem) target, false);
					} catch (CycleDetectedException e) {
						return false;
					}
					return true;
				}
				else if (dropee.getType()==ItemType.MENU_ITEM) {

					if (target.getType()==ItemType.MENU_FOLDER &&
						!target.getChildren().contains(dropee))
						return true;
					
				}
			}
		}
		return false;
	}

	@Override
	public void refresh() {
		MenuItemType[] menuItems = GServerInterface.getServer().getMenuItems();
		for (MenuItemType menuItemType : menuItems) {
			if (!items.containsKey(menuItemType.getId())) {
				GMenuItem menuItem = new GMenuItem(menuItemType);
				items.put(menuItem.getId(), menuItem);
			}
			else {
				GMenuItem menuItem = items.get(menuItemType.getId());
				menuItem.update(menuItemType);
			}
		}
		MenuType menu = GServerInterface.getServer().getMenu();
		updateMenu(menu);
		
	}

	static String[] entityTypes = { "Tag", "ProgramAccess" };

	@Override
	public List<String> getSubscriptionTypes() {
		return CommonUtils.convertToList(entityTypes);
	}

}
