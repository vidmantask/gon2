package gon_client_management.model.server;

public interface GIServerFactory {

	public abstract GIServer getServer();

}