package gon_client_management.model.ext;

@SuppressWarnings("serial")
public class GServiceTimeoutException extends RuntimeException {
	static String errorMsg = "Server connection timed out";

	public GServiceTimeoutException() {
		super(errorMsg);
	}

	public GServiceTimeoutException(String arg0) {
		super(arg0);
	}

	public GServiceTimeoutException(Throwable arg0) {
		super(errorMsg);
	}

}
