package gon_client_management.model.ext;



public interface GIJob {
	
	public void start();
	
	public boolean cancel();
	
	public GIJobInfo getStatus();
	
	public int getTotalWork();


}
