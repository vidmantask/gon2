package gon_client_management.model.server;

import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.GIDeployment;
import gon_client_management.model.GIModelElement;
import gon_client_management.model.GIModelReport;
import gon_client_management.model.GIModelRule;
import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;

import admin_ws.CreateConfigTemplateRowResponse;
import admin_ws.GetConfigSpecResponse;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.AccessDef;
import com.giritech.admin_ws.GOnServiceType;
import com.giritech.admin_ws.GOnSessionType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;
import com.giritech.admin_ws.MenuItemType;
import com.giritech.admin_ws.MenuType;
import com.giritech.admin_ws.RuleType1;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.admin_ws.types_config_template.Value_selection_choice;

public interface GIServer {

	List<GIModelElement> getRuleElements(String element_type, String filter, boolean refresh);

	GIModelElement getRuleElementHeader(String element_type);
	
	GIRuleElementFetcher<GIModelElement> getRuleElementFetcher(String element_type, String filter, boolean refresh);
	
	List<GIModelRule> getRules(String string, boolean refresh);
	
	public GIRuleSpec getRuleSpec(String className);	

	GIModelRule addRule(String ruleClassName, GIRule rule) throws GOperationNotAllowedException;
	
	void deleteRule(String ruleClassName, GIModelRule rule) throws GOperationNotAllowedException;

	GIModelRule saveRule(String ruleClassName, GIModelRule oldRule, GIModelRule newRule) throws GOperationNotAllowedException;

	List<GIModelReport> getReports();

	String getReportSpecification(String module_name, String report_id);

	String getReportSpecificationFromFilename(String specification_filename);

	boolean deleteConfigRow(GIElement element) throws GOperationNotAllowedException;

	CreateConfigTemplateRowResponse createConfigRow(String entityType, ConfigurationTemplate template) throws GOperationNotAllowedException;	

	ConfigurationTemplate updateConfigRow(GIElement element, ConfigurationTemplate template) throws GOperationNotAllowedException;
	
	String getAdmin_ws_url();

	String getSessionID();
	
	public String deployGetKnownSecretClient();
	
	public GIDeployment.KeyPair deployGenerateKeyPair();

	GetConfigSpecResponse getConfigSpec(String name);

	ConfigurationTemplate getConfigurationTemplate(String entityType, String templateName);

	ConfigurationTemplate getConfigurationTemplate(GIElement element, boolean bypassCustomTemplate, boolean refresh);
	
	public Value_selection portScan(String name, List<String> portNumbers);	

	public MenuType getMenu();
	
	public MenuItemType[] getMenuItems();
	
	public MenuType removeItemFromMenu(MenuItemType menuItemType, MenuItemType fromMenuItemType);
	
	public MenuType moveItemToMenu(MenuItemType item, MenuItemType fromItem, MenuItemType toItem);
	
	public MenuType addItemToMenu(MenuItemType item, MenuItemType fromItem);
	
	public Pair<MenuItemType, MenuType> addElementToMenu(GIElement element, MenuItemType fromItem);
		
	public String getEntityTypeForPluginType(String pluginName, String elementType);

	public List<String> getBasicEntityTypes(String entityType);

	public void addMembersToOneTimeEnrollers(GIElement element);

	public void enrollDevice(ConfigurationTemplate template, boolean enrollAsEndpoint) throws GOperationNotAllowedException;

	public TokenInfoType[] getTokenInfo(TokenInfoType[] tokenArray);

	public RuleType1[] GetRulesForElement(GIElement element, RuleType1[] knownRules);

	public void changeUserRegistration(GIElement element) throws GOperationNotAllowedException;

	public List<GIModelElement> getSpecificElements(Map<String, Set<String>> elementMap);

	public GIRuleElementFetcher<GIModelRule> getRuleFetcher(String externalRuleClassName, String filter);

	public AccessDef[] GetAccessDefinitions();

	void Login(String username, String password) throws LoginException;

	void CloseSession();

	boolean Ping();

	public Pair<GOnServiceType[], Boolean> GetServiceList();

	void StopGatewayService(String sid, boolean whenNoUsers);

	void RestartGatewayService(String sid, boolean whenNoUsers);

	GIRuleElementFetcher<GOnSessionType> getGOnSessionsFetcher(String[] serverSids, String searchFilter);

	boolean closeGatewaySession(String serverSid, String uniqueSessionId);

	boolean recalculateMenuForGatewaySession(String serverSid, String uniqueSessionId);

	boolean getGatewaySessionsUpdateEnabled();

	List<GIModelElement> getRestrictions(GIElement element, String restrictionType);

	void updateRestrictions(GIElement element, String restrictionType, List<GIElement> chosenElements);

	LicenseInfoTypeManagement getLicenseInfo();

	boolean setLicense(String content);

	boolean removeUserFromLaunchTypeCategory(GIElement element, String launchTypeCategory);

	String[] GetRestrictedLaunchTypeCategories();

	public Value_selection_choice[] getSearchCategories(String element_type);


}

