package gon_client_management.model.ext;


import java.util.List;




/**
 * Interface for plugin cofiguration 
 *
 */
public interface GIConfigPane extends GIConfigPanePage {

	public GIConfigColumn getColumn(String name) throws UnknownColumnException;
	public GIConfigRowValue getValue(String name);
	
	public void save() throws GOperationNotAllowedException;
	
	public List<GIConfigPanePage> getConfigPages();
	public boolean isReadOnly();
	
	
}
