package gon_client_management.model;

import java.util.List;

import gon_client_management.model.server.GServerInterface;

public class GReportPane implements GIReportPane {

	public List<GIModelReport> getReports() {
		return GServerInterface.getServer().getReports();
	}
	
	public String getViewHeadline() {
		return "Reports";
	}
	
	public String getColumnLabel() {
		return "Name";
	}
	
	/*
	 * 
	 * 
		public int getElementCount() {
		synchronized (elementList) {
			return elementList.size();
		}
	}

	public int getMaxCount() {
		return maxCount;
	}

	public boolean ready() {
		synchronized (hasFetchedElements) {
			return hasFetchedElements;
		}
	}

	public boolean hasFetchedAll() {
		synchronized (elementsFetched) {
			return elementsFetched;
		}
	}
	 */
	public String getAdmin_ws_url() {
		return GServerInterface.getServer().getAdmin_ws_url();
	}
	public String getSessionID() {
		return GServerInterface.getServer().getSessionID();
	}

}
