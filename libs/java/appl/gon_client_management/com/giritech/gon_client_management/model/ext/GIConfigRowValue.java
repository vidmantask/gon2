package gon_client_management.model.ext;

import java.util.Date;


public interface GIConfigRowValue {

	public GIConfigColumn.Type getType();
	
	public String getValueString();

	public String getValueInteger();

	public Boolean getValueBoolean();

	public Date getValueDateTime();
	
	public void setValue(String value);
	
	public void setValueInteger(String value);

	public void setValue(Boolean value);

	public void setValue(Date date);
	
	public void setSelectionValue(int index);	

	public int getSelectionValue();	
	
	public boolean isNull();

	public String encode();

	public void setValue(GIConfigRowValue rowValue);

}
