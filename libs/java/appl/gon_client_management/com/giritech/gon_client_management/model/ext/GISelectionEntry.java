/**
 * 
 */
package gon_client_management.model.ext;

public interface GISelectionEntry {
	
	public String getTitle();
	
	public GIConfigRowValue getValue();
}