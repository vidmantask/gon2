package gon_client_management.model;

import gon_client_management.model.ext.GIElement;

abstract public class GAbstractModelElement implements GIModelElement {

	private boolean editEnabled = true;
	private boolean deleteEnabled = true;
	
	public String getElementId() {
		return getEntityType() + "." + getId();
	}

	public boolean deleteEnabled() {
		return deleteEnabled;
	}
	public boolean editEnabled() {
		return editEnabled;
	}
	static int count = 0;
	
	public boolean checkProperty(String propertyName) {
		return false;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GIElement) {
			return isSameElement(this, (GIElement) obj);
		}
		return super.equals(obj);
	}

	private boolean isSameElement(GIElement element1, GIElement element2) {
		String[] entityTypes1 = element1.getEntityType().split("\\.");
		String[] entityTypes2 = element2.getEntityType().split("\\.");

		String entityType1 = entityTypes1[entityTypes1.length-1];
		String entityType2 = entityTypes2[entityTypes2.length-1];

		if (!entityType1.equals(entityType2)) 
			return false;
		
		String[] elementIds1 = element1.getElementId().split("\\.");
		String[] elementIds2 = element2.getElementId().split("\\.");

		String elementId1 = elementIds1[elementIds1.length-1];
		String elementId2 = elementIds2[elementIds2.length-1];
		
		if (!elementId1.equals(elementId2)) 
			return false;
		
		return true;

	}
	
	
}
