package gon_client_management.model;

import javax.security.auth.login.LoginException;

import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.server.GServerInterface;



public class GModelAPI implements GIModelAPI {
	
	public void login(String username, String password) throws LoginException {
		GServerInterface.getServer().Login(username, password);
	}

	public void login() {
		GServerInterface.getServer().Ping();
	}
	
	public void close() {
		GServerInterface.getServer().CloseSession();
	}

	public GIElementPane getElementPane(String name) {
		return new GElementPane(name);
	}

	public GIRulePane getRulePane(String name) {
		return GRulePaneFactory.get(name);
	}

	public GIReportPane getReportPane() {
		return new GReportPane();
	}

	public GIConfig getConfig(String name) {
		return new GConfig(name);	
	}
	
	public GIDeployment getDeployment() {
		return new GDeployment();
	}

	public GISoftwareDeployment getSoftwareDeployment() {
		//return new GTestSoftwareDeployment();
		return new GSoftwareDeployment();
	}

	@Override
	public GIMenuItemHandling getMenuHandlingAPI() {
		return new GMenuItemHandling();
	}
	
	public GIGatewayServiceConfig getGatewayServiceConfig() {
		return new GGatewayConfig();
	}

	@Override
	public GIGatewaySessionsConfig getGatewaySessionConfig() {
		return new GGatewaySessionsConfig();
	}

	@Override
	public GILicenseConfig getLicenseConfig() {
		return new GLicenseConfig();
	}

}
