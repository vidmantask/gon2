package gon_client_management.model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

class GRulePaneFactory {
	
	enum RulePane {
		KeyAssignment(KeyAssignmentRulePane.class); 
		
		
		@SuppressWarnings("unchecked")
		private final Class _class;
		
		@SuppressWarnings("unchecked")
		RulePane(Class _class) {
			this._class = _class;
		}
		
	}
	

	@SuppressWarnings("unchecked")
	static GIRulePane get(String name) {
		final RulePane rulePane;
		try {
			rulePane = RulePane.valueOf(name);
		} catch (IllegalArgumentException e) {
			return new GRulePane(name);
		}
		Class partypes[] = new Class[1];
		partypes[0] = String.class;
		try {
			Constructor constructor = rulePane._class.getConstructor(partypes);
			Object arglist[] = new Object[1];
            arglist[0] = name;
            Object retobj = constructor.newInstance(arglist);
            return (GIRulePane) retobj;
			
		} catch (SecurityException e) {
			handleException(rulePane._class, e);
		} catch (NoSuchMethodException e) {
			handleException(rulePane._class, e);
		} catch (IllegalArgumentException e) {
			handleException(rulePane._class, e);
		} catch (InstantiationException e) {
			handleException(rulePane._class, e);
		} catch (IllegalAccessException e) {
			handleException(rulePane._class, e);
		} catch (InvocationTargetException e) {
			Throwable cause = e.getCause();
			if (cause!=null) 
				throw new Error(cause.getMessage());
			else
				handleException(rulePane._class, e);
		} 
		return null;
	}


	@SuppressWarnings("unchecked")
	private static void handleException(Class _class, Exception e) {
		throw new Error("Unable to construct object of class " + _class + " Reason:" + e.getMessage());
	}

}
