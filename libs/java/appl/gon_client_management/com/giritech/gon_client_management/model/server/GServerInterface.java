package gon_client_management.model.server;

import gon_client_management.Activator;



public class GServerInterface {
	
	private static int server_type = 1;
	private static GIServerFactory factory = null;
	private static GIServer server = null;
	
	public static GIServer getServer() {
		try {
			if (server!=null)
				return server;
			if (factory==null) {
				switch (server_type) {
				case 0:
					factory = new GDemoServerFactory();
					break;
	
				case 1:
					factory = new GRemoteServerFactory();
					break;
	
				default:
					break;
				}
				
			}
			return factory.getServer();
		}
		catch (Throwable t) {
			Activator.getLogger().logException(t);
			throw new RuntimeException("Unable to connect to server\nSee log file for details");
		}
	}

	/**
	 * @param server the server to set
	 */
	public static void setServer(GIServer server) {
		GServerInterface.server = server;
	}

}
