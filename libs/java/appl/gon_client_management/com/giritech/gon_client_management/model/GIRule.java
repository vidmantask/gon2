package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GIGuiObject;

import java.util.List;



public interface GIRule extends GIGuiObject {
	
	public int getRuleElementCount();
	public GIElement getElement(int index);
	public List<GIElement> getElements();
	
	public boolean multipleElementsAllowed(int index);

	//public void update()
	
	public GIElement getResultElement();
	
	public boolean isActive();
	
	
}
