package gon_client_management.model.server;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.GIModelConfigColumn;
import gon_client_management.model.GIModelElement;
import gon_client_management.model.GIModelReport;
import gon_client_management.model.GIModelRule;
import gon_client_management.model.GIRule;
import gon_client_management.model.GIDeployment.KeyPair;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.security.auth.login.LoginException;

import admin_ws.CreateConfigTemplateRowResponse;
import admin_ws.GetConfigSpecResponse;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.AccessDef;
import com.giritech.admin_ws.GOnServiceType;
import com.giritech.admin_ws.GOnSessionType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;
import com.giritech.admin_ws.MenuItemType;
import com.giritech.admin_ws.MenuType;
import com.giritech.admin_ws.RuleType1;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.admin_ws.types_config_template.Value_selection_choice;

public class GDemoServerFactory implements GIServerFactory {
	
	private DemoServer server = null;

	public GIServer getServer() {
		if (server == null)
			server =  new DemoServer();
		return server;
	}
	
	private class DemoServer implements GIServer {

		
		protected class DemoRule implements GIModelRule {
			
			private final GIModelRule modelRule;
			private int id = -1;
				
			DemoRule(GIModelRule rule) {
				this.modelRule = rule;
				
			}
			
			public int getId() {
				if (id<0)
					return modelRule.getId();
				else
					return id;
			}

			public boolean deleteEnabled() {
				return modelRule.deleteEnabled();
			}

			public boolean editEnabled() {
				return modelRule.editEnabled();
			}

			public GIElement getElement(int index) {
				return modelRule.getElement(index);
			}

			public List<GIElement> getElements() {
				return modelRule.getElements();
			}

			public GIElement getResultElement() {
				return modelRule.getResultElement();
			}

			public int getRuleElementCount() {
				return modelRule.getRuleElementCount();
			}

			public boolean multipleElementsAllowed(int index) {
				return modelRule.multipleElementsAllowed(index);
			}

			@Override
			public boolean isActive() {
				return true;
			}

			@Override
			public void replaceElement(int elementIndex, GIElement element) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setResultElement(GIElement element) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		
		private List<GIModelRule> rules = new ArrayList<GIModelRule>();
		
		DemoServer() {
			final List<GIElement> list  = new ArrayList<GIElement>(); 
			list.add(createElement(0, "Bo",GGlobalDefinitions.USER_TYPE));
			list.add(createElement(0, "Key0",GGlobalDefinitions.TOKEN_TYPE));
			GIModelRule rule = new GIModelRule() {
				
				
				public boolean deleteEnabled() {
					return true;
				}

				public boolean editEnabled() {
					return true;
				}

				public GIElement getElement(int index) {
					return list.get(index);
				}

				public List<GIElement> getElements() {
					return list;
				}

				public boolean isActive() {
					return true;
				}
				
				public GIElement getResultElement() {
					return new GIElement(){

						public String getEntityType() {
							return GGlobalDefinitions.KEY_ASSIGNMENT_TYPE;
						}

						public String getInfo() {
							return "";
						}

						public String getLabel() {
							return "Assigned";
						}
						
						public void setLabel(String label) {}

						public String getElementId() {
							return null;
						}

						@Override
						public boolean deleteEnabled() {
							return true;
						}

						@Override
						public boolean editEnabled() {
							return true;
						}

						@Override
						public boolean checkProperty(String propertyName) {
							// TODO Auto-generated method stub
							return false;
						}
						
					};
					
					
				}

				public int getRuleElementCount() {
					return list.size();
				}

				public boolean multipleElementsAllowed(int index) {
					return false;
				}

				public int getId() {
					return 0;
				}

				@Override
				public void replaceElement(int elementIndex, GIElement element) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void setResultElement(GIElement element) {
					// TODO Auto-generated method stub
					
				}
				
			};
			this.rules.add(rule);
		}
		
		private GServerElement createElement(int id, String name, String type) {
			GServerElement e = new GServerElement();
			e.setId(Integer.toString(id));
			e.setLabel(name);
			e.setInfo("Info for " + name);
			e.setEntityType(type);
			e.setElementType("class");
			return e;
			
		}
		
		private Map<String, List<GIModelElement> > elementMap = new HashMap<String, List<GIModelElement>>();
		
		

		public List<GIModelElement> getRuleElements(String element_type, String filter, boolean refresh) {
			List<GIModelElement> list = elementMap.get(element_type);
			if (list==null) {
				Random random = new Random();
				int n = 1 + random.nextInt(100);
				System.out.println(n);
				list = new ArrayList<GIModelElement>();
				for(int i=0; i<n; i++) {
					list.add(createElement(i, element_type + "(" + i + ")", element_type));
				}
				elementMap.put(element_type, list);
			}
			return new ArrayList<GIModelElement>(list);
		}


		public List<GIModelRule> getRules(String string, boolean refresh) {
			return new ArrayList<GIModelRule>(rules);
		}


		public GIModelRule addRule(String ruleClassName, GIRule rule) {
			DemoRule demoRule = new DemoRule((GIModelRule) rule);
			rules.add(demoRule);
			demoRule.id = rules.size()-1;
			return demoRule;
		}


		public void deleteRule(String ruleClassName, GIModelRule rule) {
			rules.remove(rule);
		}

		public GIModelRule saveRule(String ruleClassName, GIModelRule oldRule, GIModelRule newRule) {
			rules.set(newRule.getId(), newRule);
			return newRule;
		}

		public GIModelElement createRuleElement(GIModelElement element) {
			List<GIModelElement> list = elementMap.get(element.getEntityType());
			if (list!=null) {
				GServerElement newElement = createElement(element);
				list.add(newElement);
				newElement.setId(Integer.toString(list.size()-1));
				return newElement;
			}
			return element;
		}

		private GServerElement createElement(GIModelElement element) {
			GServerElement e = new GServerElement();
			e.setId(element.getId());
			e.setLabel(element.getLabel());
			e.setInfo("");
			e.setEntityType(element.getEntityType());
			e.setElementType("class");
			return e;
		}

		public boolean deleteRuleElement(GIModelElement element) {
			List<GIModelElement> list = elementMap.get(element.getEntityType());
			if (list!=null)
				list.remove(element);
			return true;
			
		}

		public GIModelElement updateRuleElement(GIModelElement element) {
			List<GIModelElement> list = elementMap.get(element.getEntityType());
			if (list!=null) {
				list.set(Integer.parseInt(element.getId()), element);
			}
			return element;
		}

		public String getReportSpecification(String module_name, String report_id) {
			return "";
		}

		public String getReportSpecificationFromFilename(String specification_filename) {
			return "";
		}

		public List<GIModelReport> getReports() {
			ArrayList<GIModelReport> resultList = new ArrayList<GIModelReport>();
			resultList.add(new GServerReport("my_module", "1", "My first demo report"));
			return resultList;
		}

		public GIRuleElementFetcher getRuleElementFetcher(final String element_type, boolean refresh) {
			return new GIRuleElementFetcher() {

				List<GIModelElement> elements = getRuleElements(element_type, null, false);
				
				public int getCountMax() {
					return elements.size();
				}

				public List<GIModelElement> getElements(int count) {
					return elements;
				}

				public boolean hasFetchedAll() {
					return true;
				}

				@Override
				public boolean isResultTruncated() {
					return false;
				}

				@Override
				public void stop() {
					// TODO Auto-generated method stub
					
				}
				
			};
		}

		public GIModelElement getRuleElementHeader(String element_type) {
			return createElement(-1, element_type, element_type);
		}


		public boolean deleteConfigRow(GIElement element) {
			return true;
		}

		public List<GIModelConfigColumn> getConfigColumns(String entityType) {
			/*
			GConfigColumn column = new GConfigColumn("my_id_1", GIConfigColumns.Type.TYPE_STRING, "My first column");
			columns.add("my_id_1", GIConfigColumns.Type.TYPE_STRING, "My first column");
			columns.add("my_id_2", GIConfigColumns.Type.TYPE_STRING, "My second column");
			*/
			return null;
		}

		public Map<String, String> getConfigRow(GIElement element) {
			/*
			GConfigRow row = new GConfigRow(getConfigColumns(element));
			
			GConfigRowValue row_value = new GConfigRowValue();
			row_value.setValue("Hej med dig");
			row.addValue(row_value);

			row_value = new GConfigRowValue();
			row_value.setValue("Hej med dig, nu med mere tekst");
			row.addValue(row_value);
			return row;
			*/
			return null;
		}
		
		
		public GetConfigSpecResponse getConfigSpec(String name) {
			// TODO Auto-generated method stub
			return null;
		}

		public Map<String,String> updateConfigRow(String entityType, Map<String, GIConfigRowValue> values) throws GOperationNotAllowedException {
			// TODO Auto-generated method stub
			return null;
		}
		

		public Map<String,String> createConfigRow(String entityType, Map<String, GIConfigRowValue> values) throws GOperationNotAllowedException {
			// TODO Auto-generated method stub
			return null;
		}

		public String getAdmin_ws_url() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getSessionID() {
			// TODO Auto-generated method stub
			return null;
		}

		public String deployGetKnownSecretClient() {
			// TODO Auto-generated method stub
			return null;
		}

		public KeyPair deployGenerateKeyPair() {
			// TODO Auto-generated method stub
			return null;
		}

		public GIRuleSpec getRuleSpec(String className) {
			// TODO Auto-generated method stub
			return null;
		}

		public ConfigurationTemplate getConfigurationTemplate(
				String entityType, String templateName) {
			// TODO Auto-generated method stub
			return null;
		}

		public CreateConfigTemplateRowResponse createConfigRow(String entityType,
				ConfigurationTemplate template)
				throws GOperationNotAllowedException {
			return null;
			
		}

		public ConfigurationTemplate updateConfigRow(GIElement element,
				ConfigurationTemplate template)
				throws GOperationNotAllowedException {
			return null;
			
		}

		public ConfigurationTemplate getConfigurationTemplate(GIElement element, boolean bypassCustomTemplate, boolean refresh) {
			// TODO Auto-generated method stub
			return null;
		}

		public Value_selection portScan(String name,
				List<String> portNumbers) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public MenuType getMenu() {
			// TODO Auto-generated method stub
			return null;
			
		}
		public MenuItemType[] getMenuItems() {
			return null;
		}

		@Override
		public Pair<MenuItemType, MenuType> addElementToMenu(GIElement element,
				MenuItemType fromItem) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public MenuType addItemToMenu(MenuItemType item, MenuItemType fromItem) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public MenuType moveItemToMenu(MenuItemType item,
				MenuItemType fromItem, MenuItemType toItem) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public MenuType removeItemFromMenu(MenuItemType menuItemType,
				MenuItemType fromMenuItemType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getEntityTypeForPluginType(String pluginName,
				String elementType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public List<String> getBasicEntityTypes(String entityType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public GIRuleElementFetcher getRuleElementFetcher(String elementType,
				String filter, boolean refresh) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void addMembersToOneTimeEnrollers(GIElement element) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void enrollDevice(ConfigurationTemplate template,
				boolean enrollAsEndpoint) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public TokenInfoType[] getTokenInfo(TokenInfoType[] tokenArray) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public RuleType1[] GetRulesForElement(GIElement element,
				RuleType1[] knownRules) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void changeUserRegistration(GIElement element) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public List<GIModelElement> getSpecificElements(
				Map<String, Set<String>> elementMap) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public GIRuleElementFetcher<GIModelRule> getRuleFetcher(
				String externalRuleClassName, String filter) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public AccessDef[] GetAccessDefinitions() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void CloseSession() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void Login(String username, String password)
				throws LoginException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean Ping() {
			// TODO Auto-generated method stub
			return true;
			
		}

		@Override
		public Pair<GOnServiceType[], Boolean> GetServiceList() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void StopGatewayService(String sid, boolean whenNoUsers) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void RestartGatewayService(String sid, boolean whenNoUsers) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public GIRuleElementFetcher<GOnSessionType> getGOnSessionsFetcher(
				String[] serverSids, String searchFilter) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean closeGatewaySession(String serverSid,
				String uniqueSessionId) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean recalculateMenuForGatewaySession(String serverSid,
				String uniqueSessionId) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean getGatewaySessionsUpdateEnabled() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public List<GIModelElement> getRestrictions(GIElement element,
				String restrictionType) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void updateRestrictions(GIElement element,
				String restrictionType, List<GIElement> chosenElements) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public LicenseInfoTypeManagement getLicenseInfo() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean setLicense(String content) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean removeUserFromLaunchTypeCategory(GIElement element,
				String launchTypeCategory) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public String[] GetRestrictedLaunchTypeCategories() {
			// TODO Auto-generated method stub
			return null;
		}

		public Value_selection_choice[] getSearchCategories(String element_type) {
			return null;
		}
		

	}

}
