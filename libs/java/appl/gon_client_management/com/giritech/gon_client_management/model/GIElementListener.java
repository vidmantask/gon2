package gon_client_management.model;

public interface GIElementListener {
	
	public void elementCreated(String id);
	
	public void elementChanged(String id);

}
