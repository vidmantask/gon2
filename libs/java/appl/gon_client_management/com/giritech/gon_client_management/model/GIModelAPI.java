package gon_client_management.model;

import javax.security.auth.login.LoginException;

import gon_client_management.model.ext.GIConfig;

public interface GIModelAPI {
	
	public void login(String username, String password) throws LoginException;
	
	public void close();
	
	public GIElementPane getElementPane(String name);
	
	public GIRulePane getRulePane(String name);

	public GIReportPane getReportPane();

	public GIConfig getConfig(String name);
	
	public GIDeployment getDeployment();
	
	public GISoftwareDeployment getSoftwareDeployment();

	public GIMenuItemHandling getMenuHandlingAPI();

	public void login();
	
	public GIGatewayServiceConfig getGatewayServiceConfig();

	public GIGatewaySessionsConfig getGatewaySessionConfig();
	
	public GILicenseConfig getLicenseConfig();

}
