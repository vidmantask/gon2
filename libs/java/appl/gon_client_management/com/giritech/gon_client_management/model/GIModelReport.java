package gon_client_management.model;


/**
 *
 */
public interface GIModelReport {

	public String getId();
	public String getTitle();

	public String getSpecification();
}
