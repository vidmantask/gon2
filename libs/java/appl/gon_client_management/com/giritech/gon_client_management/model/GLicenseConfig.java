package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.view.administrator.GElementUpdateHandler;

import com.giritech.admin_ws.LicenseInfoTypeManagement;

public class GLicenseConfig implements GILicenseConfig {

	@Override
	public LicenseInfoTypeManagement getLicenseInfo() {
		return GServerInterface.getServer().getLicenseInfo();
	}

	@Override
	public boolean setLicense(String content) {
		return GServerInterface.getServer().setLicense(content);
	}

	@Override
	public boolean removeUserFromLaunchTypeCategory(GIElement element,String launchTypeCategory) {
		boolean ok = GServerInterface.getServer().removeUserFromLaunchTypeCategory(element, launchTypeCategory);
		GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated("User", element.getElementId());
		return ok;
		
	}


	@Override
	public String[] GetRestrictedLaunchTypeCategories() {
		return GServerInterface.getServer().GetRestrictedLaunchTypeCategories();
		
	}

}
