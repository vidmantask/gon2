package gon_client_management.model;

import gon_client_management.model.ext.GIElement;

public abstract class GAbstractRule implements GIModelRule {


	public boolean isSameElement(GIElement element1, GIElement element2) {
		String[] entityTypes1 = element1.getEntityType().split("\\.");
		String[] entityTypes2 = element2.getEntityType().split("\\.");

		String entityType1 = entityTypes1[entityTypes1.length-1];
		String entityType2 = entityTypes2[entityTypes2.length-1];

		if (!entityType1.equals(entityType2)) 
			return false;
		
		String[] elementIds1 = element1.getElementId().split("\\.");
		String[] elementIds2 = element2.getElementId().split("\\.");

		String elementId1 = elementIds1[elementIds1.length-1];
		String elementId2 = elementIds2[elementIds2.length-1];
		
		if (!elementId1.equals(elementId2)) 
			return false;
		
		return true;

	}

	@Override
	public void replaceElement(int elementIndex, GIElement element) {
		if (elementIndex<0) {
			GIElement resultElement = getResultElement();
			if (isSameElement(resultElement, element)) {
				setResultElement(element);
			}
		}
		else {
			GIElement condition = getElement(elementIndex);
			if (isSameElement(condition, element)) {
				setCondition(elementIndex, element);
			}
			
			
		}
		
	}

	abstract protected void setCondition(int elementIndex, GIElement element);
	

}
