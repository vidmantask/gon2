package gon_client_management.model.localservice;

import gon_client_management.model.GIModelToken;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.types_gpm.GPMCollectionInfoType;

public interface GILocalService {

	/* (non-Javadoc)
	 * @see gon_client_management.model.server.GIServer#getRuleElementHeader(java.lang.String)
	 */
	public GPMCollectionInfoType[] getPackageCollections();

	public String deployToken(GIModelToken token);
	
	public String initToken(GIModelToken token);
	
	public TokenInfoType[] getTokenInfo();
	
	public int installGPMCollection(String collectionId, String runtimeEnvId);
	
	public JobInfoType getJobInfo(int jobId);
	
	public boolean cancelJob(int jobId);

	public void stop();

}