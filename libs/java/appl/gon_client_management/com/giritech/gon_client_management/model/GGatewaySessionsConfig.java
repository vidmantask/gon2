package gon_client_management.model;

import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.model.server.GIRuleElementFetcher;
import gon_client_management.model.server.GServerInterface;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import com.giritech.admin_ws.GOnSessionType;

public class GGatewaySessionsConfig extends Observable implements GIGatewaySessionsConfig {

	protected volatile List<GOnSessionType> sessions = new ArrayList<GOnSessionType>();

	private volatile Boolean elementsFetched = false;
	private volatile Boolean hasFetchedElements = false;
	private String filter = null;
	private ThreadWithStopFlag fetchElementsThread = null;

	protected volatile String fetchElementErrorMesssage;

	protected String[] serverSids = null; 
	
	@Override
	public List<GOnSessionType> getSessions() {
		synchronized (sessions) {
			return sessions;
		}
	}

	@Override
	public int getElementCount() {
		synchronized (sessions) {
			return sessions.size();
		}
	}

	@Override
	public String getFetchElementErrorMesssage() {
		return fetchElementErrorMesssage;
	}

	@Override
	public int getMaxCount() {
		return -1;
	}

	@Override
	public boolean hasFetchedAll() {
		return elementsFetched;
	}

	@Override
	public boolean ready() {
		return hasFetchedElements;
	}

	@Override
	public void refreshData() {
		refreshData(false);
		
	}

	@Override
	public void refreshData(boolean background) {
		stopRetrieve();
		if (background)
			getElementsFromServerInBackground();
		else
			getElementsFromServer();
	}
	
	@Override
	public void stopRetrieve() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			fetchElementsThread.stopThread();
			try {
				fetchElementsThread.join();
			} catch (InterruptedException e) {
			}
		}
		
	}
	

	void getElementsFromServer() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			return;
		}
		hasFetchedElements = false;
		fetchElementsThread = new ThreadWithStopFlag() {

			/* (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				try {
					elementsFetched = false;
//					System.out.println(externalRuleClassName + " clear");
					synchronized (sessions) {
						sessions.clear();
					}
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STARTED);
					GIRuleElementFetcher<GOnSessionType> gOnSessionsFetcher = GServerInterface.getServer().getGOnSessionsFetcher(serverSids, filter);
					while(!gOnSessionsFetcher.hasFetchedAll()) {
						if (stopThread) {
							gOnSessionsFetcher.stop();
							break;
						}
						List<GOnSessionType> elements = gOnSessionsFetcher.getElements(50);
						
						synchronized (sessions) {
							sessions.addAll(elements);
						}
						GGatewaySessionsConfig.this.setChanged();
						notifyObservers(NotificationType.SOME_ELEMENTS_FETCHED);
						/*
						try {
							sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						*/
					}
					elementsFetched = true;
				}
				finally {
					hasFetchedElements = true;
					GGatewaySessionsConfig.this.setChanged();
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STOPPED);
				}
			}
			
		};
		fetchElementErrorMesssage = null;
		fetchElementsThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, final Throwable e) {
				fetchElementErrorMesssage  = e.getMessage();
			}
		});
		
		fetchElementsThread.start();
		
	}

	void getElementsFromServerInBackground() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			return;
		}
		hasFetchedElements = false;

		fetchElementsThread = new ThreadWithStopFlag() {

			List<GOnSessionType> tmpSessions = new ArrayList<GOnSessionType>();
			/* (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				try {
					try {
						sleep(2000);
					} catch (InterruptedException e) {
						// Ignore
					}
					
					elementsFetched = false;
					GIRuleElementFetcher<GOnSessionType> gOnSessionsFetcher = GServerInterface.getServer().getGOnSessionsFetcher(serverSids, filter);
					while(!gOnSessionsFetcher.hasFetchedAll()) {
						if (stopThread) {
							gOnSessionsFetcher.stop();
							break;
						}
						List<GOnSessionType> elements = gOnSessionsFetcher.getElements(50);
						
						tmpSessions.addAll(elements);
					}
					elementsFetched = true;
				}
				finally {
					synchronized (sessions) {
						sessions = tmpSessions;
					}
					hasFetchedElements = true;
					GGatewaySessionsConfig.this.setChanged();
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STOPPED);
				}
			}
			
		};
		fetchElementErrorMesssage = null;
		fetchElementsThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, final Throwable e) {
				fetchElementErrorMesssage  = e.getMessage();
			}
		});
		
		fetchElementsThread.start();
		
	}
	
	@Override
	public void setSearchFilter(String filter) {
		this.filter = filter;
	}

	@Override
	public void setServerSids(String[] serverSids) {
		this.serverSids = serverSids;
	}

	@Override
	public boolean recalculateMenuForSession(String serverSid, String uniqueSessionId) {
		return GServerInterface.getServer().recalculateMenuForGatewaySession(serverSid, uniqueSessionId);
	}

	@Override
	public boolean stopSession(String serverSid, String uniqueSessionId) {
		return GServerInterface.getServer().closeGatewaySession(serverSid, uniqueSessionId);
	}

	@Override
	public void stopFetchingElements() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {		
			fetchElementsThread.stopThread();
		}
	}

	@Override
	public boolean isUpdateEnabled() {
		try {
			return GServerInterface.getServer().getGatewaySessionsUpdateEnabled();
		}
		catch (Throwable t) {
			return false;
		}
	}
	
}
