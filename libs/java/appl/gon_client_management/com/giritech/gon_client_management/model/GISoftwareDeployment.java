package gon_client_management.model;

import java.util.List;

public interface GISoftwareDeployment {
	
	
	public interface GIPackage {

		public String getId();
		
		public String getTitle();
		
		public String getDescription();
		
		public String getVersion();
		
		public String getPlatform();
	}
	
	public interface GICollection {
		
		public String getId();
		
		public String getTitle();
		
		public String getDescription();
		
		public List<GIPackage> getPackages();
		
	}
	
	public List<GICollection> getCollections();
	
	public void refresh();
	
	public GICollection getCollection(String id);
	
	public void installCollection();

}
