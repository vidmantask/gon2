package gon_client_management.model.ext;



import java.util.List;

public interface GIConfigColumn {

	enum Type {TYPE_STRING, TYPE_INTEGER, TYPE_BOOLEAN, TYPE_TEXT, TYPE_DATE, TYPE_DATETIME, TYPE_TIME};
	
	public Type getType();

	public String getLabel();

	public String getName();
	
	public String getToolTip();
	
	public boolean isReadOnly();

	public boolean isSecret();
	
	public int getTextLimit();
	
	public GISelection getSelection(); 
	
	public interface GISelection {
		
		public List<GISelectionEntry> getSelectionChoices();
		
		public GIConfigRowValue getValue(int index);
		
		public boolean isEditAllowed();
		
		
	}
	
	public interface GIAction {
		
		public String getTitle();
		
		public GIJob getJob();
		
		
	}
	
	public GIAction getAction();

	public String getSection();

}
