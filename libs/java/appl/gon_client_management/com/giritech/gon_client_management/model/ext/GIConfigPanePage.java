package gon_client_management.model.ext;

import java.util.List;



public interface GIConfigPanePage {

	public int getColumnCount();
	public GIConfigColumn getColumn(int index);
	public GIConfigRowValue getValue(int index);
	
	public GIConfigPanePage getAdvancedPane();
	
	public String getTitle();
	List<GIDetails> getDetails();
	
	
}
