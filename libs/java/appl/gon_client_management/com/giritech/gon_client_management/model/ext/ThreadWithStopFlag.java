package gon_client_management.model.ext;

public abstract class ThreadWithStopFlag extends Thread {
	
	protected volatile boolean stopThread = false;
	
	synchronized public void stopThread() {
		stopThread = true;
	}
	

}
