package gon_client_management.model.ext;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public interface GIJobInfo {

	/**
	 * Auto generated getter method
	 * @return java.math.BigInteger
	 */
	public Job_status_type0 getJobStatus();

	/**
	 * Auto generated getter method
	 * @return java.lang.String
	 */
	public String getJobHeader();

	/**
	 * Auto generated getter method
	 * @return java.lang.String
	 */
	public String getJobInfo();

	/**
	 * Auto generated getter method
	 * @return java.math.BigInteger
	 */
	public int getJobProgress();
	
	/**
	 * Return true if job is not finished
	 * @return java.math.BigInteger
	 */
	public boolean more();
	
	
	public JobInfoType getJobInfoType();

}