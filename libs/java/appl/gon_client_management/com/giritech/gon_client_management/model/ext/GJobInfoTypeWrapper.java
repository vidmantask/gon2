package gon_client_management.model.ext;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_job.Job_status_type0;

public class GJobInfoTypeWrapper implements GIJobInfo {

	private JobInfoType jobInfoType;

	public GJobInfoTypeWrapper(JobInfoType jobInfoType) {
		this.jobInfoType = jobInfoType;
	}
	
	@Override
	public String getJobHeader() {
		return jobInfoType.getJob_header();
	}

	@Override
	public String getJobInfo() {
		return jobInfoType.getJob_sub_header();
	}

	@Override
	public int getJobProgress() {
		return jobInfoType.getJob_progress().intValue();
	}

	@Override
	public Job_status_type0 getJobStatus() {
		return jobInfoType.getJob_status();
	}

	@Override
	public boolean more() {
		return jobInfoType.getJob_more();
	}

	@Override
	public JobInfoType getJobInfoType() {
		return jobInfoType;
	}

}
