package gon_client_management.model.server;


import java.util.List;

public interface GIRuleElementFetcher<E> {

	public List<E> getElements(int count);

	/**
	 * @return the countMax
	 */
	public int getCountMax();

	/**
	 * @return the fetchedAll
	 */
	public boolean hasFetchedAll();
	
	public boolean isResultTruncated();

	public void stop();

	

}