package gon_client_management.model;

import java.util.Observer;

public interface GIObservableListPane {
	
	public int getElementCount();
	
	
	/**
	 * @return whether all elements has been fetched
	 */
	public boolean hasFetchedAll();

	/**
	 * @return an indication of how many elements can be fetched. Returns -1 if not known
	 */
	public int getMaxCount();

	/**
	 * @return whether the model has finished fetching elements (which could be due to an error)
	 */
	public boolean ready();

	/**
	 * fetches all elements from server again
	 */
	public void refreshData();
	
	/**
	 * Adds an observer for observing the state of fetching elements. The notifications will contain an element of type NotificationType   
	 */
	public void addObserver(Observer o);

	public void deleteObserver(Observer observer);
	
	public enum NotificationType {
		FETCHING_ELEMENTS_STARTED,
		SOME_ELEMENTS_FETCHED,
		FETCHING_ELEMENTS_STOPPED
	}

	public String getFetchElementErrorMesssage();
	
	public void stopFetchingElements();
	
}
