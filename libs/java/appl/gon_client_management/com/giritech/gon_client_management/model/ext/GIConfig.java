package gon_client_management.model.ext;

import gon_client_management.model.GIGetConfigListener;

import java.util.List;

import com.giritech.admin_ws.TemplateRefType;

public interface GIConfig {
	
	public GIConfigPane getConfigPane(GIElement element);
	public GIConfigPane getConfigPane(GIElement element, boolean copy);

	public GIConfigPane getDefaultConfigPane(GIElement element);
	
	public GIConfigPane getConfigPane(String name, String templateName);

	public GIConfigPane getCreateConfigPane();
	
	public boolean deleteRow(GIElement element) throws GOperationNotAllowedException;
	
	public List<TemplateRefType> getConfigTemplates();
	
	public List<String> getTemplateErrors();
	
	
	public void addGetConfigListener(GIGetConfigListener listener);
	public void removeGetConfigListener(GIGetConfigListener listener);

	public void refresh();
	
	public boolean isCreateEnabled();

	public boolean isEditEnabled();

	public boolean isEditPossible();
	
	public boolean isDeleteEnabled();
	
	public List<GIElement> getRestrictions(GIElement element, String string);
	public void updateRestrictions(GIElement element, String string,List<GIElement> chosenElements);

	
	


}
