package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;

import com.giritech.admin_ws.types_config_template.Value_selection_choice;

/**
 * @author pwl
 *
 */
public interface GIElementPane extends GIObservableListPane {
	
	public List<GIElement> getElements();
	
	/**
	 * Get a headline for the view.
	 * 
	 * @return A string that can be used as a headline for the view.
	 */
	public abstract String getViewHeadline();
	
	/**
	 * Each pane has a header which can be used fx. for
	 * sorting the content of the pane. This header should 
	 * have a label.
	 * 
	 * @return A string label describing the column.
	 */
	public abstract String getColumnLabel();

	/**
	 * 
	 * @return The column label in plural form.
	 */
	public abstract String getColumnLabelPlural();

	/**
	 * Get type for this element. Used for Icons and tooltip text.
	 * 
	 * @param index - if index < 0 the result column is assumed.
	 * @return A single string label naming the column at index. 
	 */
	public abstract String getColumnType();
	
	

	
	/**
	 * @return whether not all elements were fetched, i.e. if a max limit for number of elements were reached  
	 */
	public boolean resultTruncated();
	

	/**
	 * @return whether it is possible to search for elements  
	 */
	public boolean searchAvailable();


	/**
	 * @return whether it is possible to search for elements  
	 */
	public boolean searchBeforeFilter();

	/**
	 * @return whether it is possible to search for elements  
	 */
	public void search(String searchStr);
	

	public GIElement getElement(String elementId);

	public List<String> getSubscriptionTypes();

	public void addMembersToOneTimeEnrollers(GIElement element);

	public void changeUserRegistration(GIElement element, boolean fireUpdate) throws GOperationNotAllowedException;

	public void refreshData(String selectedElementId);

	public void excludeElementType(String string);

	public Value_selection_choice[] getSearchCategories(String elementType);




}
