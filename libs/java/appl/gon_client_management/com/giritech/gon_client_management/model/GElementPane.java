package gon_client_management.model;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.model.server.GIRuleElementFetcher;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.view.administrator.GElementUpdateHandler;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import com.giritech.admin_ws.types_config_template.Value_selection_choice;

class GElementPane extends Observable implements GIElementPane {

	volatile List<GIModelElement> elementList = new ArrayList<GIModelElement>();
	final GIModelElement headerElement;
	private final String elementType;
	private volatile Boolean elementsFetched = false;
	private volatile Boolean hasFetchedElements = false;
	private volatile Boolean lastResultTruncated = false;
	private String filter = null;
	private int maxCount = -1;
	private ThreadWithStopFlag fetchElementsThread = null;
	protected String fetchElementErrorMesssage = null;
	
	public static int ELEMENT_FETCH_COUNT = 50;
	Value_selection_choice[] searchCategories;
	
	public String getViewHeadline() {
		return headerElement.getInfo();
	}
	
	private void updateElementList(List<GIModelElement> elements) {
		if (excludedElementTypes.size()==0)
			elementList.addAll(elements);
		else {
			for (GIModelElement element : elements) {
				if (checkElementType(element))
					elementList.add(element);
				
			}
		}
		this.setChanged();
		notifyObservers(NotificationType.SOME_ELEMENTS_FETCHED);
		
	}

	private boolean checkElementType(GIModelElement element) {
		if (excludedElementTypes.size()==0)
			return true;
		String[] typeParts = element.getElementType().split("\\.");
		if (typeParts.length>0) {
			if (excludedElementTypes.contains(typeParts[0]))
				return false;
		}
		return true;
		
	}

	public List<GIElement> getElements() {
//		synchronized (elementList) {
			return new ArrayList<GIElement>(elementList);
//		}
	}
	
	public void refreshData() {
		getElementsFromServer(true);
		
	}
	
	private int findElement(String id) {
		for (int i=0; i<elementList.size(); i++) {
			GIModelElement guiElement = elementList.get(i);
			if (guiElement.getId().equals(id)) {
				return i;
			}
		}
		return -1;
	}

	public void refreshData(String selectedElementId) {
		if (selectedElementId==null)
			getElementsFromServer(true);
		else {
			GIModelElement element = GElementUpdateHandler.getElementUpdateHandler().getElement(selectedElementId);
			if (element != null && checkElementType(element)) {
				int index = findElement(element.getId());
				if (index<0) {
					elementList.add(element);
				}
				else {
					elementList.set(index, element);
				}
			}
			else {
				String[] elementIds = selectedElementId.split("\\.",2);
				String elementId = elementIds[elementIds.length-1];
				int index = findElement(elementId);
				if (index>=0) {
					elementList.remove(index);
				}
				
				
				
			}
		}
		
	}
	
	void getElementsFromServer(final boolean refresh) {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			return;
		}
		setHasFetchedElements(false);
		fetchElementsThread = new ThreadWithStopFlag() {

			/* (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				try {
					setElementsFetched(false);
//					System.out.println(elementType + " elementList.clear()");
//					synchronized (elementList) {
						elementList.clear();
//					}
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STARTED);
					GIRuleElementFetcher<GIModelElement> ruleElementFetcher = GServerInterface.getServer().getRuleElementFetcher(elementType, filter, refresh);
					while(!ruleElementFetcher.hasFetchedAll()) {
						if (stopThread) {
							ruleElementFetcher.stop();
							break;
						}
						List<GIModelElement> elements = ruleElementFetcher.getElements(ELEMENT_FETCH_COUNT);
						maxCount = ruleElementFetcher.getCountMax();
						lastResultTruncated = ruleElementFetcher.isResultTruncated();

						GElementPane.this.updateElementList(elements);
					}
					setElementsFetched(true);
				}
				finally {
//					synchronized (hasFetchedElements) {
						hasFetchedElements = true;
//						System.out.println(elementType + " done");
						GElementPane.this.setChanged();
						notifyObservers(NotificationType.FETCHING_ELEMENTS_STOPPED);
//					}
				}
			}
			
		};
		fetchElementErrorMesssage = null;
		fetchElementsThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, final Throwable e) {
				fetchElementErrorMesssage  = e.getMessage();
			}
		});

		fetchElementsThread.start();
		
	}

	
	public String getFetchElementErrorMesssage() {
		return fetchElementErrorMesssage;
	}

	public String getColumnLabel() {
		return headerElement.getLabel();
	}
	
	

	public String getColumnLabelPlural() {
		return headerElement.getInfo();
	}
	
	public String getColumnType() {
		return elementType;
	}


	public GElementPane(String elementType) {
		headerElement = GServerInterface.getServer().getRuleElementHeader(elementType);
		this.elementType = elementType;
		
	}

	public Value_selection_choice[] getSearchCategories(String elementType) {
		if (searchCategories==null)
			searchCategories = GServerInterface.getServer().getSearchCategories(elementType);
		return searchCategories;
	}
	

	public int getElementCount() {
		return elementList.size();
	}

	public int getMaxCount() {
		return maxCount;
	}

	public boolean ready() {
//		synchronized (hasFetchedElements) {
			return hasFetchedElements;
//		}
	}

	public boolean hasFetchedAll() {
//		synchronized (elementsFetched) {
			return elementsFetched;
//		}
	}

	/**
	 * @param elementsFetched the elementsFetched to set
	 */
	void setElementsFetched(Boolean elementsFetched) {
//		synchronized (elementsFetched) {
			this.elementsFetched = elementsFetched;
//		}
	}

	/**
	 * @param hasFetchedElements the hasFetchedElements to set
	 */
	void setHasFetchedElements(Boolean hasFetchedElements) {
//		synchronized (hasFetchedElements) {
			this.hasFetchedElements = hasFetchedElements;
//		}
	}

	private Map<String, GIElement> elementIdMap = new HashMap<String, GIElement>();
	private List<String> subscriptionTypes = null;
	private List<String> excludedElementTypes = new ArrayList<String>();
	
	public GIElement getElement(String elementId) {
		GIElement element = elementIdMap.get(elementId);
		if (element == null) {
			for(GIModelElement e : elementList) {
				elementIdMap.put(e.getElementId(), e);
				if (e.getElementId().equals(elementId))
					return e;
			}
			return null;
		}
		return element;
	}

	@Override
	public List<String> getSubscriptionTypes() {
		if (subscriptionTypes==null) {
			subscriptionTypes = GServerInterface.getServer().getBasicEntityTypes(elementType);
		}
		return subscriptionTypes;
	}

	@Override
	public boolean resultTruncated() {
		return lastResultTruncated;
	}

	@Override
	public void search(String searchStr) {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			fetchElementsThread.stopThread();
			try {
				fetchElementsThread.join();
			} catch (InterruptedException e) {
			}
		}
		filter = searchStr.length()>0 ? searchStr : null;
		refreshData();
		
	}

	@Override
	public boolean searchAvailable() {
		List<String> elementTypes = getSubscriptionTypes();
		for (String elementType : elementTypes) {
			if (elementType.equalsIgnoreCase("group") || elementType.equalsIgnoreCase("user"))
				return true;
			
		}
		return false;
	}

	@Override
	public boolean searchBeforeFilter() {
		return searchAvailable();
	}

	@Override
	public void addMembersToOneTimeEnrollers(final GIElement element) {
		Job job = new Job("Add members to One-Time Enrollers") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				GServerInterface.getServer().addMembersToOneTimeEnrollers(element);
				GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated("GOnUserGroup", null);
				return Status.OK_STATUS;
			}
			
		};
		job.schedule();
	}

	@Override
	public void changeUserRegistration(GIElement element, boolean fireUpdate) throws GOperationNotAllowedException {
		GServerInterface.getServer().changeUserRegistration(element);
		if (fireUpdate) {
			if (element.checkProperty(GGlobalDefinitions.PROPERTY_REGISTERED_USER)) {
				GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated(element.getEntityType(), element.getElementId());
			}
			else {
				GElementUpdateHandler.getElementUpdateHandler().fireElementDeleted(element.getEntityType(), element.getElementId());
			}
		}
		
	}

	@Override
	public void stopFetchingElements() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {		
			fetchElementsThread.stopThread();
		}
		
	}

	@Override
	public void excludeElementType(String elementType) {
		excludedElementTypes .add(elementType);
		
	}
	
	

}
