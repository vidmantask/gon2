package gon_client_management.model;

import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.model.server.GTestServer;
import gon_client_management.model.server.GTestServer.RuleElement;
import junit.framework.TestCase;

public class UnitTest_GConfigPane extends TestCase {

	private static GTestServer testServer = new GTestServer();
	
	public UnitTest_GConfigPane(String name) {
		super(name);
		GServerInterface.setServer(testServer);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetColumn() {
		final int noOfTypes = GIConfigColumn.Type.values().length;
		testServer.createConfigPane(getName(), 20, noOfTypes, 2);
		GConfigTemplatePane configPane = new GConfigTemplatePane(getName(), null);
		assertEquals(18, configPane.getColumnCount());
		for(int i=0; i<noOfTypes; i++) {
			GIConfigColumn column = configPane.getColumn(i);
			int index = i+1;
			assertEquals("Header" + index, column.getLabel());
			assertEquals(GIConfigColumn.Type.values()[index%noOfTypes], column.getType());
		}

		testServer.createConfigPane( "empty", 0, noOfTypes, 2);
		GConfigTemplatePane configPane1 = new GConfigTemplatePane("empty", null);
		assertEquals(0, configPane1.getColumnCount());
		
	}

	public void testGetValue() {
		testServer.createConfigPane(getName(), 20, -1, 2);
		RuleElement element = new GTestServer.RuleElement(getName(), "ElementLabel", "ElementInfo");
		GConfigTemplatePane configPane = new GConfigTemplatePane(element, null);
		
		for(int i=0; i<configPane.getColumnCount(); i++) {
			GIConfigColumn col = configPane.getColumn(i);
			GIConfigRowValue value = configPane.getValue(i);
			assertEquals(col.getType(), value.getType());
			switch (col.getType()) {
			case TYPE_BOOLEAN:
				Boolean valueBoolean = value.getValueBoolean();
				assertNotNull(valueBoolean);
				break;
			case TYPE_STRING:
			case TYPE_TEXT:
				String valueString = value.getValueString();
				assertEquals(col.getName()+(i+1), valueString);
				break;
			case TYPE_INTEGER:
				Integer valueInteger = Integer.parseInt(value.getValueInteger());
				assertEquals(i+1, valueInteger.intValue());
				break;

			default:
				break;
			}
		}
	}

	public void testSave() {
		testServer.createConfigPane(getName(), 10, -1, 0);
		RuleElement element = new GTestServer.RuleElement(getName(), "ElementLabel", "ElementInfo");
		GConfigTemplatePane configPane = new GConfigTemplatePane(element, null);
		for(int i=0; i<configPane.getColumnCount(); i++) {
			GIConfigColumn col = configPane.getColumn(i);
			GIConfigRowValue value = configPane.getValue(i);
			switch (col.getType()) {
			case TYPE_BOOLEAN:
				value.setValue(false);
				break;
			case TYPE_STRING:
			case TYPE_TEXT:
				value.setValue("Yo" + i);
				break;
			case TYPE_INTEGER:
				value.setValue("" + -i);
				break;

			default:
				break;
			}

		}
		try {
			configPane.save();
		} catch (GOperationNotAllowedException e) {
			fail(e.getMessage());
		}
		for(int i=0; i<configPane.getColumnCount(); i++) {
			GIConfigColumn col = configPane.getColumn(i);
			GIConfigRowValue value = configPane.getValue(i);
			switch (col.getType()) {
			case TYPE_BOOLEAN:
				Boolean valueBoolean = value.getValueBoolean();
				assertFalse(valueBoolean);
				break;
			case TYPE_STRING:
			case TYPE_TEXT:
				String valueString = value.getValueString();
				assertEquals("Yo" + i, valueString);
				break;
			case TYPE_INTEGER:
				Integer valueInteger = Integer.parseInt(value.getValueInteger());
				assertEquals(-i, valueInteger.intValue());
				break;

			default:
				break;
			}
		}
			
	}

	public void testCreate() {
		testServer.createConfigPane(getName(), 20, -1, 2);
		GConfigTemplatePane configPane = new GConfigTemplatePane(getName(), null);
		for(int i=0; i<configPane.getColumnCount(); i++) {
			GIConfigColumn col = configPane.getColumn(i);
			GIConfigRowValue value = configPane.getValue(i);
			switch (col.getType()) {
			case TYPE_BOOLEAN:
				value.setValue(false);
				break;
			case TYPE_STRING:
			case TYPE_TEXT:
				value.setValue("Yo" + i);
				break;
			case TYPE_INTEGER:
				value.setValue("" + -i);
				break;

			default:
				break;
			}

		}
		try {
			configPane.save();
		} catch (GOperationNotAllowedException e) {
			fail(e.getMessage());
		}
		for(int i=0; i<configPane.getColumnCount(); i++) {
			GIConfigColumn col = configPane.getColumn(i);
			GIConfigRowValue value = configPane.getValue(i);
			switch (col.getType()) {
			case TYPE_BOOLEAN:
				Boolean valueBoolean = value.getValueBoolean();
				assertFalse(valueBoolean);
				break;
			case TYPE_STRING:
			case TYPE_TEXT:
				String valueString = value.getValueString();
				assertEquals("Yo" + i, valueString);
				break;
			case TYPE_INTEGER:
				Integer valueInteger = Integer.parseInt(value.getValueInteger());
				assertEquals(-i, valueInteger.intValue());
				break;

			default:
				break;
			}
		}
		
		
	}


}
