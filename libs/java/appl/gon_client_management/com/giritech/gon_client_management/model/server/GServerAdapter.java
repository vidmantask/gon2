package gon_client_management.model.server;

import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.GIModelConfigColumn;
import gon_client_management.model.GIModelElement;
import gon_client_management.model.GIModelReport;
import gon_client_management.model.GIModelRule;
import gon_client_management.model.GIRule;
import gon_client_management.model.GIDeployment.KeyPair;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;

import admin_ws.CreateConfigTemplateRowResponse;
import admin_ws.GetConfigSpecResponse;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.AccessDef;
import com.giritech.admin_ws.GOnServiceType;
import com.giritech.admin_ws.GOnSessionType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;
import com.giritech.admin_ws.MenuItemType;
import com.giritech.admin_ws.MenuType;
import com.giritech.admin_ws.RuleType1;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.admin_ws.types_config_template.Value_selection_choice;

/**
 * @author pwl
 *
 * Class for generating GServer api's without implementing all methods. Subclasses can 
 * implement the methods which are needed in the context 
 */
public class GServerAdapter implements GIServer {

	public String getAdmin_ws_url() {
		throw new UnsupportedOperationException();
	}

	public String getSessionID() {
		throw new UnsupportedOperationException();
	}


	public GIModelRule addRule(String ruleClassName, GIRule rule)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}


	public GIModelElement createRuleElement(GIModelElement element)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}

	public boolean deleteConfigRow(GIElement element)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
	}

	public void deleteRule(String ruleClassName, GIModelRule rule)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();

	}

	public boolean deleteRuleElement(GIModelElement modelElement)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
	}

	public KeyPair deployGenerateKeyPair() {
		throw new UnsupportedOperationException();
		
	}

	public String deployGetKnownSecretClient() {
		throw new UnsupportedOperationException();
		
	}


	public String getReportSpecification(String module_name,
			String report_id) {
		throw new UnsupportedOperationException();
	}

	public String getReportSpecificationFromFilename(String specification_filename) {
		throw new UnsupportedOperationException();
	}

	public List<GIModelReport> getReports() {
		throw new UnsupportedOperationException();
		
	}

	@SuppressWarnings("unchecked")
	public GIRuleElementFetcher getRuleElementFetcher(String element_type,
			boolean refresh) {
		throw new UnsupportedOperationException();
		
	}

	public GIModelElement getRuleElementHeader(String element_type) {
		throw new UnsupportedOperationException();
		
	}

	public List<GIModelElement> getRuleElements(String element_type,
			String filter, boolean refresh) {
		throw new UnsupportedOperationException();
		
	}

	public List<GIModelRule> getRules(String string, boolean refresh) {
		throw new UnsupportedOperationException();
		
	}

	public GIModelRule saveRule(String ruleClassName, GIModelRule oldRule, GIModelRule newRule)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}


	public GIModelElement updateRuleElement(GIModelElement modelElement)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}

	public GIRuleSpec getRuleSpec(String className) {
		throw new UnsupportedOperationException();
	}

	public List<GIModelConfigColumn> getConfigColumns(String entityType) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public Map<String, String> getConfigRow(GIElement element) {
		throw new UnsupportedOperationException();
	}

	public GetConfigSpecResponse getConfigSpec(String name) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public Map<String, String> updateConfigRow(String entityType,
			Map<String, GIConfigRowValue> values)
			throws GOperationNotAllowedException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public Map<String, String> createConfigRow(String entityType,
			Map<String, GIConfigRowValue> values)
			throws GOperationNotAllowedException {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}


	public ConfigurationTemplate getConfigurationTemplate(String entityType,
			String templateName) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}

	public CreateConfigTemplateRowResponse createConfigRow(String entityType,
			ConfigurationTemplate template)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}

	public ConfigurationTemplate updateConfigRow(GIElement element,
			ConfigurationTemplate template)
			throws GOperationNotAllowedException {
		throw new UnsupportedOperationException();
		
	}

	public ConfigurationTemplate getConfigurationTemplate(GIElement element, boolean bypassCustomTemplate, boolean refresh) {
		throw new UnsupportedOperationException();
	}

	public Value_selection portScan(String name, List<String> portNumbers) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MenuType getMenu() {
		throw new UnsupportedOperationException();
		
	}

	public MenuItemType[] getMenuItems() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public Pair<MenuItemType, MenuType> addElementToMenu(GIElement element, MenuItemType fromItem) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MenuType addItemToMenu(MenuItemType item, MenuItemType fromItem) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MenuType moveItemToMenu(MenuItemType item, MenuItemType fromItem,
			MenuItemType toItem) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MenuType removeItemFromMenu(MenuItemType menuItemType,
			MenuItemType fromMenuItemType) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getEntityTypeForPluginType(String pluginName,
			String elementType) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> getBasicEntityTypes(String entityType) {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	@Override
	public GIRuleElementFetcher getRuleElementFetcher(String elementType,
			String filter, boolean refresh) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addMembersToOneTimeEnrollers(GIElement element) {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void enrollDevice(ConfigurationTemplate template,
			boolean enrollAsEndpoint) {
		throw new UnsupportedOperationException();
	}

	@Override
	public TokenInfoType[] getTokenInfo(TokenInfoType[] tokenArray) {
		throw new UnsupportedOperationException();
	}

	@Override
	public RuleType1[] GetRulesForElement(GIElement element,
			RuleType1[] knownRules) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeUserRegistration(GIElement element) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<GIModelElement> getSpecificElements(
			Map<String, Set<String>> elementMap) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GIRuleElementFetcher<GIModelRule> getRuleFetcher(
			String externalRuleClassName, String filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccessDef[] GetAccessDefinitions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void CloseSession() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Login(String username, String password) throws LoginException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean Ping() {
		// TODO Auto-generated method stub
		return true;
		
	}

	@Override
	public Pair<GOnServiceType[], Boolean> GetServiceList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void StopGatewayService(String sid, boolean whenNoUsers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void RestartGatewayService(String sid, boolean whenNoUsers) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public GIRuleElementFetcher<GOnSessionType> getGOnSessionsFetcher(
			String[] serverSids, String searchFilter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean closeGatewaySession(String serverSid, String uniqueSessionId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean recalculateMenuForGatewaySession(String serverSid,
			String uniqueSessionId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getGatewaySessionsUpdateEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<GIModelElement> getRestrictions(GIElement element,
			String restrictionType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateRestrictions(GIElement element, String restrictionType,
			List<GIElement> chosenElements) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public LicenseInfoTypeManagement getLicenseInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean setLicense(String content) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeUserFromLaunchTypeCategory(GIElement element,
			String launchTypeCategory) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String[] GetRestrictedLaunchTypeCategories() {
		// TODO Auto-generated method stub
		return null;
	}

	public Value_selection_choice[] getSearchCategories(String element_type) {
		return null;
	}


}
