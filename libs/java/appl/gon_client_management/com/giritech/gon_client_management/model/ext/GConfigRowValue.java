package gon_client_management.model.ext;

import gon_client_management.ext.CommonUtils;
import gon_client_management.model.ext.GIConfigColumn.GISelection;
import gon_client_management.model.ext.GIConfigColumn.Type;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


public class GConfigRowValue implements GIConfigRowValue {

	
	private Type type;
	private String value_string;
	private Boolean value_boolean;
	private Date value_datetime;
	private GISelection selection;
	
	public GConfigRowValue(Type type) {
		this.type = type;
	}

	public GConfigRowValue(Type type, String value) {
		this.type = type;
		setValue(value);
	}

	public GConfigRowValue(Boolean value) {
		type = Type.TYPE_BOOLEAN;
		setValue(value);
	}

	public Type getType() {
		return type;
	}

	public Boolean getValueBoolean() {
		assert(type == Type.TYPE_BOOLEAN);
		return value_boolean;
	}

	public String getValueInteger() {
		assert(type == Type.TYPE_INTEGER);
		normaliseIntegerString();
		return value_string;
	}

	public String getValueString() {
		assert(type == Type.TYPE_STRING);
		return value_string;
	}

	public String getValueText() {
		assert(type == Type.TYPE_TEXT);
		return value_string;
	}
	
	public Date getValueDate() {
		assert(type == Type.TYPE_DATE);
		return value_datetime;
	}

	public Date getValueDateTime() {
		assert(type == Type.TYPE_DATETIME);
		return value_datetime;
	}

	public Date getValueTime() {
		assert(type == Type.TYPE_TIME);
		return value_datetime;
	}
	
	public void setValue(Boolean value) {
		value_boolean = value;
	}

	public void setValue(String value) {
		value_string = value;
	}
	
	public void setValue(Date value) {
		value_datetime = value;
	}

	public void setValueInteger(String value) {
		value_string = value;
	}

	public void setSelectionValue(int index) {
		if (selection!=null) {
			GIConfigRowValue value = selection.getValue(index);
			setValue(value);
		}
		
	}
	
	public int getSelectionValue() {
		if (selection!=null) {
			List<GISelectionEntry> selectionChoices = selection.getSelectionChoices();
			for(int i=0; i<selectionChoices.size(); i++) {
				GISelectionEntry selectionEntry = selectionChoices.get(i);
				if (hasSameValue(selectionEntry.getValue()))
					return i;
			}
		}
		return -1;
	}
	
	public void setValue(GIConfigRowValue rowValue) {
		if (rowValue==null || rowValue.isNull()) {
			setNull();
		}
		else {
			switch (rowValue.getType()) {
			case TYPE_STRING:
			case TYPE_TEXT:
				setValue(rowValue.getValueString());
				break;
			case TYPE_INTEGER:
				setValue(rowValue.getValueInteger());
				break;
			case TYPE_BOOLEAN:
				setValue(rowValue.getValueBoolean());
				break;
			case TYPE_DATE:
			case TYPE_DATETIME:
			case TYPE_TIME:
				setValue(rowValue.getValueDateTime());
				break;
			
			default:
				throw new RuntimeException("Unknown config type : " + type);
			
			}
		}
	}
	

	public void dump() {
		switch(type) {
		case TYPE_STRING:
			System.out.println("STRING : " + value_string);
			break;
		case TYPE_INTEGER:
			System.out.println("INTEGER : " + value_string);
			break;
		case TYPE_BOOLEAN:
			if (value_boolean) {
				System.out.println("BOOLEAN : True");
			}
			else {
				System.out.println("BOOLEAN : False");
			}
			break;
		case TYPE_TEXT:
			System.out.println("TEXT : " + value_string);
			break;
		case TYPE_DATE:
			System.out.println("DATE : " + value_datetime);
			break;
		case TYPE_DATETIME:
			System.out.println("DATETIME : " + value_datetime);
			break;
		case TYPE_TIME:
			System.out.println("TIME : " + value_datetime);
			break;
		default:
			throw new RuntimeException("Unknown config type : " + type);
		}
		
	}
	
	public String encode() {
		if (isNull())
			return null;
		switch(type) {
		case TYPE_STRING:
			return value_string;
		case TYPE_INTEGER:
			normaliseIntegerString();
			return value_string;
		case TYPE_BOOLEAN:
			if (value_boolean) {
				return "1";
			}
			return "0";
		case TYPE_TEXT:
			return value_string;
		case TYPE_DATE:
			return CommonUtils.DateTimeConverter.date2string(value_datetime);
		case TYPE_DATETIME:
			return CommonUtils.DateTimeConverter.datetime2string(value_datetime);
		case TYPE_TIME:
			return CommonUtils.DateTimeConverter.time2string(value_datetime);
		default:
			throw new RuntimeException("Unable to encode because the type is not implemented yet");
		}
	}

	public static GConfigRowValue factory(Type row_type, String value) {
		GConfigRowValue row_value = new GConfigRowValue(row_type);
		try {
		
			if (value!=null) {
				switch(row_type) {
				case TYPE_STRING:
				case TYPE_TEXT:
					row_value.setValue(value);
					break;
				case TYPE_INTEGER:
					row_value.setValueInteger(value);
					break;
				case TYPE_BOOLEAN:
					row_value.setValue(value.equals("1"));
					break;
				case TYPE_DATE:
					row_value.setValue(CommonUtils.DateTimeConverter.string2date(value));
					break;
				case TYPE_DATETIME:
					row_value.setValue(CommonUtils.DateTimeConverter.string2datetime(value));
					break;
				case TYPE_TIME:
					row_value.setValue(CommonUtils.DateTimeConverter.string2time(value));
					break;
				default:
					throw new Error("Type of recived row value not implemented yet");
				}
			}
		} catch (ParseException e) {
			throw new RuntimeException("Error parsing '" + value + "'");
		}
		return row_value;
	}

	public boolean isNull() {
		switch(type) {
		case TYPE_STRING:
		case TYPE_TEXT:
			return value_string==null;
		case TYPE_INTEGER:
			return value_string==null || value_string=="";
		case TYPE_BOOLEAN:
			return value_boolean==null;
		case TYPE_DATE:
		case TYPE_DATETIME:
		case TYPE_TIME:
			return value_datetime==null;
		default:
			throw new RuntimeException("Unable to tell Null value from unknown type : " + type.toString() );
		}
	}
	
	private void setNull() {
		switch(type) {
		case TYPE_STRING:
		case TYPE_TEXT:
			value_string=null;
			break;
		case TYPE_INTEGER:
			value_string=null;
			break;
		case TYPE_BOOLEAN:
			value_boolean=null;
			break;
		case TYPE_DATE:
		case TYPE_DATETIME:
		case TYPE_TIME:
			value_datetime=null;
			break;
		default:
			throw new RuntimeException("Unable to set Null value for unknown type : " + type.toString() );
		}
		
	}

	@Override
	public String toString() {
		if (isNull())
			return "";
		switch(type) {
		case TYPE_STRING:
		case TYPE_TEXT:
			return value_string;
		case TYPE_INTEGER:
			normaliseIntegerString();
			return value_string;
		case TYPE_BOOLEAN:
			return value_boolean.toString();
		case TYPE_DATE:
		case TYPE_DATETIME:
		case TYPE_TIME:
			return value_datetime.toString();
		default:
			return "Value of unknown type";
		}
	}

	private void normaliseIntegerString() {
		if (value_string == "")
			value_string = null;
		else {
			try {
				Integer intValue = Integer.valueOf(value_string);
				value_string = intValue.toString();
			}
			catch(NumberFormatException e) {
				// Okay - could be template value
			}
		}
	}

	public void setSelectionValue(GISelection selection) {
		this.selection = selection;
		
	}

	private boolean hasSameValue(GIConfigRowValue value) {
		if (value.getType()==type) {
			if (value.isNull() && isNull())
				return true;
			else if (value.isNull()==isNull() && value.toString().equals(toString()))
				return true;
		}
		return false;
		
	}
	

	



}
