/**
 * 
 */
package gon_client_management.model.ext;

import java.util.List;

public interface GIGetSelectionVauesJob extends GIJob {
	
	public List<GISelectionEntry> getSelectionChoices();
	
}