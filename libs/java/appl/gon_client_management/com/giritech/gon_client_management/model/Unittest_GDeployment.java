package gon_client_management.model;

import gon_client_management.model.ext.GIConfigColumn;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.localservice.GLocalServerInterface;
import gon_client_management.model.localservice.GLocalServiceAdapter;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.model.server.GTestServer;
import gon_client_management.model.server.GTestServer.ConfigColumn;

import java.util.List;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;


import junit.framework.TestCase;

public class Unittest_GDeployment extends TestCase {

	private static GTestServer testServer = new GTestServer();
	
	private GTestServer.ConfigColumn createColumn(String name) {
		ConfigColumn configColumn = new GTestServer.ConfigColumn();
		configColumn.type = GIConfigColumn.Type.TYPE_STRING;
		configColumn.name = name;
		return configColumn;
	}

	public Unittest_GDeployment(String name) {
		super(name);
		GServerInterface.setServer(testServer);
		TestConfigPane configPane = new TestConfigPane(getName());
		configPane.columns.add(createColumn("id"));
		configPane.columns.add(createColumn("plugin_type"));
		configPane.columns.add(createColumn("description"));
		configPane.columns.add(createColumn("casing_number"));
		configPane.columns.add(createColumn("serial"));
		configPane.columns.add(createColumn("public_key"));
		testServer.configPanes.put("plugin_type_0", configPane);
	}
	
	
	
	private class TestConfigPane extends GTestServer.ConfigPane {

		public TestConfigPane(String name) {
			super(name);
		}
	}
	
	private class TestService extends GLocalServiceAdapter {
	
	    String[] TOKEN_STATUS_TEXTS = {"Initialized", "Deployed", "Undeployed", "Burned"};
		
	    TokenInfoType[] tokens;
		
		public boolean failInit = false;
		public boolean failDeploy = false;
		
		TestService(final int noOfTokens) {
			tokens = new TokenInfoType[noOfTokens]; 
			for(int i=0; i<noOfTokens; i++) {
				TokenInfoType tokenType = new TokenInfoType();
				tokens[i] = tokenType;
				String status = TOKEN_STATUS_TEXTS[i%TOKEN_STATUS_TEXTS.length];
				String serial = status.equals("Deployed") ? "serial_" + i : "";
				tokenType.setToken_id("token_id_" + i);
				tokenType.setToken_label("token_title_" + i);
				tokenType.setToken_type_id("plugin_type_" + i%3);
				tokenType.setToken_status(status);
				tokenType.setToken_serial(serial);
			}
		}
		
		@Override
		public String deployToken(GIModelToken token)  {
			if (failDeploy)
				throw new RuntimeException("Some error");
            if (token.getId().length()>0) {
            	checkTokenId(token);
            	return "public_key_" + token.getId();
            }
            else
            	fail("Missing or wrong token id " + token.getId());
            return null;
		}

		@Override
		public TokenInfoType[] getTokenInfo() {
			return tokens;
		}
		
		private void checkTokenId(GIModelToken token) {
        	String[] split = token.getId().split("\\.");
        	assertEquals(2, split.length);
        	assertEquals(token.getType(), split[0]);
            assertTrue(split[1].startsWith("token_id"));
		}

		@Override
		public String initToken(GIModelToken token) {
			if (failInit)
            	throw new RuntimeException("Some error");
            if (token.getId().length()>0 && token.getSerial().length()>0) {
            	checkTokenId(token);
            }
            else 
            	fail("Missing or wrong token id " + token.getId());
            return token.getSerial();
		}
		
		
	}

	public void testEnrollToken() {
		GLocalServerInterface.setService(new TestService(3));
		GDeployment deployment = new GDeployment();
		assertEquals(0, deployment.getTokenCount());
		deployment.refreshTokenList();
		assertEquals(3, deployment.getTokenCount());
		List<GIToken> tokens = deployment.getTokens();
		assertTrue(tokens.size()>0);
		try {
			deployment.enrollToken(tokens.get(0));
		} catch (GOperationNotAllowedException e) {
			fail(e.getMessage());
		}
	}


	public void testGetTokens() {
		GLocalServerInterface.setService(new TestService(0));
		GDeployment deployment = new GDeployment();
		deployment.refreshTokenList();
		assertEquals(0, deployment.getTokenCount());
		GLocalServerInterface.setService(new TestService(200));
		deployment = new GDeployment();
		deployment.refreshTokenList();
		assertEquals(200, deployment.getTokenCount());
	}


	public void testEnrollToken1() {
		TestService testService = new TestService(3);
		testService.failInit = true;
		GLocalServerInterface.setService(testService);
		GDeployment deployment = new GDeployment();
		deployment.refreshTokenList();
		assertEquals(3, deployment.getTokenCount());
		List<GIToken> tokens = deployment.getTokens();
		assertTrue(tokens.size()>0);
		GIToken token = tokens.get(0);
		try {
			deployment.initToken(token);
			fail("Expected exception to be thrown");
		} catch(RuntimeException e) {
			assertEquals("Some error", e.getMessage());
		}
	}

	public void testEnrollToken2() {
		TestService testService = new TestService(3);
		testService.failDeploy = true;
		GLocalServerInterface.setService(testService);
		GDeployment deployment = new GDeployment();
		deployment.refreshTokenList();
		assertEquals(3, deployment.getTokenCount());
		List<GIToken> tokens = deployment.getTokens();
		assertTrue(tokens.size()>0);
		GIToken token = tokens.get(0);
		try {
			deployment.enrollToken(token);
			fail("Expected exception to be thrown");
		} catch(RuntimeException e) {
			assertEquals("Some error", e.getMessage());
		} catch (GOperationNotAllowedException e) {
			fail("Wrong type exception thrown");
		}
	}
	
	
	
	public static void main(String args[]) {
		for(String s: args)
			System.out.println(s);
		System.out.println();
		
	}

}

