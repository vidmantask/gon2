package gon_client_management.model.localservice;


import gon_client_management.Activator;
import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.CommonUtils.ProcessHandler;
import gon_client_management.model.GIModelToken;
import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.view.preferences.PreferenceConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.rmi.RemoteException;

import org.apache.axiom.soap.SOAPFaultDetail;
import org.apache.axis2.AxisFault;

import admin_deploy_ws.Admin_deploy_wsServiceStub;
import admin_deploy_ws.FaultServer;
import admin_deploy_ws.GetJobInfoResponse;
import admin_deploy_ws.GetTokensResponse;
import admin_deploy_ws.InitTokenResponse;
import admin_deploy_ws.InstallGPMCollectionResponse;

import com.giritech.admin_deploy_ws.FaultServerElementType;
import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.types_gpm.GPMCollectionInfoType;


public class GLocalService implements GILocalService {

	private String admin_deploy_ws_url;
	private long ws_timeout_min;
	private String sessionID;
	private Admin_deploy_wsServiceStub service;
	private ThreadWithStopFlag keepAliveThread;

	private GLocalService(String admin_deploy_ws_url, long ws_timeout_min) {
		this.admin_deploy_ws_url = admin_deploy_ws_url;
		this.ws_timeout_min = ws_timeout_min;
		connect();

		keepAliveThread = new ThreadWithStopFlag() {
        	
			@Override
			public void run() {
				while (!stopThread) {
					try {
						sleep(1000*60);
						ping();
					} catch (InterruptedException e) {
					}
				}
			}
        	
		};
		keepAliveThread.start();
		
	}
	
	@SuppressWarnings("serial")
	private class ErrorConnectionException extends RuntimeException {
		
		ErrorConnectionException(RemoteException e) {
			super("Error connecting to local service - please try restarting the Management client");
			Activator.getLogger().logException(e);
			if (e instanceof AxisFault) {
				SOAPFaultDetail faultDetailElement = ((AxisFault) e).getFaultDetailElement();
				if (faultDetailElement!=null) {
					String string = faultDetailElement.toString();
					Activator.getLogger().logError(string);
				}
			}
		}
	}
	
	private void connect() {
		
    	Admin_deploy_wsServiceStub admin_deploy_ws_service = null;
		try {
			admin_deploy_ws_service = new Admin_deploy_wsServiceStub(admin_deploy_ws_url);
		} catch (AxisFault e) {
			throw new RuntimeException(e);
		}

        /*
         * This is needed because the py-soap server need content length of requests,
         * and this is enabled if CHUNKED is disabled.
         */
        admin_deploy_ws_service._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, "false");
        
        /*
         * Increase timeout
         * 
         */
        long timeout_ms = ws_timeout_min * 60 * 1000; 
        admin_deploy_ws_service._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout_ms);
        
        
        admin_deploy_ws.Login login_request = new admin_deploy_ws.Login();
        com.giritech.admin_deploy_ws.LoginRequestType login_request_content = new com.giritech.admin_deploy_ws.LoginRequestType();
        login_request_content.setPassword("Hej");
        login_request_content.setUsername("Davsxxx");
        login_request.setContent(login_request_content);
        

        admin_deploy_ws.LoginResponse login_response;
		try {
			login_response = admin_deploy_ws_service.Login(login_request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
        this.sessionID = login_response.getContent().getSession_id();
        this.service = admin_deploy_ws_service;
        
	}
	
	
	private void handleFaultServer(FaultServer e) {
		Activator.getLogger().logException(e);
		final FaultServerElementType faultMessage = e.getFaultMessage();
		if (faultMessage!=null)
			throw new RuntimeException(faultMessage.getFaultServerElementType());
		else 
			throw new RuntimeException(e);
	}
	

	private static GILocalService server_instance = null;
	private static ProcessHandler processHandler = null;
	/**
	 * The default server string is "https://127.0.0.1:8080"
	 * 
	 * @return A new GIServer instance
	 */
	public synchronized static GILocalService getLocalService() {
		String serverUrl = "https://" +
        	Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_HOST) +
        	":" +
        	Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_PORT);
		
		if (server_instance == null) {
			try {
				server_instance = new GLocalService(serverUrl, 5);
			}
			catch(RuntimeException t) {
				// ignore
			}
		}
		if (server_instance != null) {
			if (!((GLocalService) server_instance).ping())
				server_instance = null;
		}
		if (server_instance == null) {
			startLocalService();
			//String serverUrl = "https://127.0.0.1:8082";
			int connect_attempts = 0;
			RuntimeException error = null;
			while(connect_attempts<3) {
				myWait(100);
				try {
					server_instance = new GLocalService(serverUrl, 5);
					if (((GLocalService) server_instance).ping())
						return server_instance;
				}
				catch(RuntimeException t) {
					error = t;
				}
				connect_attempts++;
			}
			if (error!=null)
				throw error;
			else
				throw new RuntimeException("Could not contact server");
		}
		return server_instance;
	}

	
	
	private synchronized static void myWait(int timeout) {
		try {
			Thread.sleep(timeout);
		} catch (InterruptedException e) {
		}
		
	}


	@SuppressWarnings("unused")
	private static void printChildOutput(Process child) {
		InputStream out = child.getInputStream();
		InputStreamReader gcdReader = new InputStreamReader(out);
		BufferedReader in = new BufferedReader(gcdReader);
		String line;
		try {
			while ((line = in.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
		}
		
	}
	

	public synchronized static void stopLocalService()  {
		if (server_instance!=null) {
			server_instance.stop();
		}
		if (processHandler!=null) {
			processHandler.stop();
		}
	}
	
	private static void startLocalService() {
		String command = Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVICE_EXECUTABLE) + 
						 " --serve " +
						 " --ip " + Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_HOST) +
						 " --port " + Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVICE_SERVER_PORT) +
						 " --management_server_ip " + Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_HOST) +
						 " --management_server_port " + Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_PORT);
		if (processHandler==null) {
			processHandler = new CommonUtils.ProcessHandler(command, null, true, Activator.getLogger());
		}
		else {
			processHandler.setCommand(command);
		}
		processHandler.start();

	}

	private boolean ping() {
		admin_deploy_ws.Ping request = new admin_deploy_ws.Ping();
		com.giritech.admin_deploy_ws.PingRequestType requestContent = new com.giritech.admin_deploy_ws.PingRequestType();
		requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        admin_deploy_ws.PingResponse response;
        try {
			response = this.service.Ping(request);
		} catch (RemoteException e) {
			return false;
		} catch (FaultServer e) {
			return false;
		}
		return response.getContent().getRc();
		
	}
	

	/* (non-Javadoc)
	 * @see gon_client_management.model.localservice.GILocalService#getPackageCollections()
	 */
	public GPMCollectionInfoType[] getPackageCollections() {
		admin_deploy_ws.GetGPMCollections request = new admin_deploy_ws.GetGPMCollections();
		com.giritech.admin_deploy_ws.GetGPMCollectionsRequestType requestContent = new com.giritech.admin_deploy_ws.GetGPMCollectionsRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        admin_deploy_ws.GetGPMCollectionsResponse response;
        try {
			response = this.service.GetGPMCollections(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		GPMCollectionInfoType[] gpm_collections = response.getContent().getGpm_collections();
		if (gpm_collections!=null) {
			return gpm_collections;
		}
		else
			return new GPMCollectionInfoType[0]; 
		
	}
	
	public String deployToken(GIModelToken token) {
		admin_deploy_ws.DeployToken request = new admin_deploy_ws.DeployToken();
		com.giritech.admin_deploy_ws.DeployTokenRequestType requestContent = new com.giritech.admin_deploy_ws.DeployTokenRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setToken_id(token.getTypeSpecificId());
        requestContent.setToken_type_id(token.getType());
        requestContent.setGenerate_keypair(true);
        request.setContent(requestContent);
        admin_deploy_ws.DeployTokenResponse response;
        try {
			response = this.service.DeployToken(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getPublic_key();
	}

	public TokenInfoType[] getTokenInfo() {
		admin_deploy_ws.GetTokens request = new admin_deploy_ws.GetTokens();
		com.giritech.admin_deploy_ws.GetTokensRequestType requestContent = new com.giritech.admin_deploy_ws.GetTokensRequestType();
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetTokensResponse response;
        try {
			response = this.service.GetTokens(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		TokenInfoType[] tokens = response.getContent().getTokens();
		if (tokens==null)
			tokens = new TokenInfoType[0]; 
		return tokens;
	}
	
	public String initToken(GIModelToken token) {
		admin_deploy_ws.InitToken request = new admin_deploy_ws.InitToken();
		com.giritech.admin_deploy_ws.InitTokenRequestType requestContent = new com.giritech.admin_deploy_ws.InitTokenRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setToken_id(token.getTypeSpecificId());
        requestContent.setToken_type_id(token.getType());
        request.setContent(requestContent);
        InitTokenResponse response;
        try {
			response = this.service.InitToken(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getToken_serial();
		
	}
	
	
	public int installGPMCollection(String collectionId, String runtimeEnvId) {
		admin_deploy_ws.InstallGPMCollection request = new admin_deploy_ws.InstallGPMCollection();
		com.giritech.admin_deploy_ws.InstallGPMCollectionRequestType requestContent = new com.giritech.admin_deploy_ws.InstallGPMCollectionRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setGpm_collection_id(collectionId);
        requestContent.setRuntime_env_id(runtimeEnvId);
        request.setContent(requestContent);
        InstallGPMCollectionResponse response;
        try {
			response = this.service.InstallGPMCollection(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_id().intValue();
		
	}
	
	
	public JobInfoType getJobInfo(int jobId) {
		admin_deploy_ws.GetJobInfo request = new admin_deploy_ws.GetJobInfo();
		com.giritech.admin_deploy_ws.GetJobInfoRequestType requestContent = new com.giritech.admin_deploy_ws.GetJobInfoRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setJob_id(BigInteger.valueOf(jobId));
        request.setContent(requestContent);
        GetJobInfoResponse response;
        try {
			response = this.service.GetJobInfo(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getJob_info();
		
	}
	
	public boolean cancelJob(int jobId) {
		admin_deploy_ws.CancelJob request = new admin_deploy_ws.CancelJob();
		com.giritech.admin_deploy_ws.CancelJobRequestType requestContent = new com.giritech.admin_deploy_ws.CancelJobRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setJob_id(BigInteger.valueOf(jobId));
        request.setContent(requestContent);
        admin_deploy_ws.CancelJobResponse response;
        try {
			response = this.service.CancelJob(request);
		} catch (RemoteException e) {
			throw new ErrorConnectionException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
		
	}


	@Override
	public void stop() {
		if (keepAliveThread!=null)
			keepAliveThread.stopThread();
	}
	
}
