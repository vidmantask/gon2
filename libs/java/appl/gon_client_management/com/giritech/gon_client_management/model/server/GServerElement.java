package gon_client_management.model.server;

import gon_client_management.model.GAbstractModelElement;
import gon_client_management.model.ext.GConfigTemplateUtil;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIConfigColumn.Type;

import com.giritech.admin_ws.ConfigElementType;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;

public class GServerElement extends GAbstractModelElement  {
	
	private String id = "";
	private String entityType= "";
	private String info= "";
	private String label= "";
	private String elementType = null;
	private GConfigTemplateUtil gConfigTemplateUtil = null;
	
	GServerElement() {
		id = "";
		entityType = "";
		info = "";
		label = "";
		
	}
	
	GServerElement(com.giritech.admin_ws.ElementType from) {
		setId(from.getId());
		setEntityType(from.getEntity_type());
		setInfo(from.getInfo());
		setLabel(from.getLabel());
		setElementType(from.getElement_type());
		if (from instanceof ConfigElementType) {
			ConfigElementType configElement = (ConfigElementType) from; 
			setConfigTemplate(configElement.getConfig_template());
		}
	}

	public String getEntityType() {
		return entityType;
	}

	public String getInfo() {
		return info;
	}

	public String getLabel() {
		return label;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	void setInfo(String info) {
		if (info!=null)
			this.info = info;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getElementType() {
		return elementType;
	}

	void setElementType(String elementType) {
		this.elementType = elementType;
	}

	public GConfigTemplateUtil getgConfigTemplateUtil() {
		return gConfigTemplateUtil;
	}

	public void setgConfigTemplateUtil(GConfigTemplateUtil gConfigTemplateUtil) {
		this.gConfigTemplateUtil = gConfigTemplateUtil;
	}


	@Override
	public boolean checkProperty(String propertyName) {
		if (gConfigTemplateUtil!=null) {
			GIConfigRowValue value = gConfigTemplateUtil.values.get(propertyName);
			if (value!=null && value.getType()==Type.TYPE_BOOLEAN) {
				return value.getValueBoolean();
			}
		}
		else {
			String[] typeParts = getElementType().split("\\.");
			if (typeParts.length>0)
				return typeParts[typeParts.length-1].equals(propertyName); 
		}

		return false;
	}

	public void setConfigTemplate(ConfigurationTemplate configTemplate) {
		if (configTemplate!=null) {
			gConfigTemplateUtil = new GConfigTemplateUtil(configTemplate, null);
			gConfigTemplateUtil.setValuesFromTemplate();
		}
		
	}

	@Override
	public String toString() {
		return getLabel();
	}
	
	

}
