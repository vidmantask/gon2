package gon_client_management.model;

import gon_client_management.model.ext.GIJob;

public interface GIToken {

	public String getId();

	public String getTitle();

	public String getType();
	
	public String getSerial();
	
	public void setSerial(String serial);

	public String getPublic_key();

	public void setPublic_key(String key);

	public String getCasing();

	public void setCasing(String casing);

	public String getStatus();

	public String getDescription();
	
	public String getTypeLabel();

	public void setDescription(String description);

	public boolean canInstallSoftware();
	
	public GIJob createInstallSoftwareJob(String collectionId);
	
	public String getName();

	public void setName(String tokenName);

	public boolean isEnrollable();

	public String getInternalType();

	
	

}