package gon_client_management.model.localservice;

public interface GILocalServiceFactory {
	
	public GILocalService getLocalService();

}