package gon_client_management.model;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.ext.ThreadWithStopFlag;
import gon_client_management.model.server.GIRuleElementFetcher;
import gon_client_management.model.server.GIRuleSpec;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.view.administrator.GElementUpdateHandler;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Set;
import java.util.Map.Entry;


class GRulePane extends Observable implements GIRulePane {
	

	protected static final int RULE_FETCH_COUNT = 50;
	
	protected List<GIModelRule> rules = new ArrayList<GIModelRule>();
	protected final String externalRuleClassName;
	protected final GIRule templateRule;
	protected final GIRule headerRule;
	protected final GIRuleSpec ruleSpec;
	protected final Set<Integer> uniqueIndices = new HashSet<Integer>(); 
	protected final List<Set<Integer>> mandatoryIndices = new ArrayList<Set<Integer>>(); 
	protected final Map<String, List<String>> entityTypeMap;
	protected List<String> subscriptionTypes = null;
	
	private Boolean elementsFetched = false;
	private Boolean hasFetchedElements = false;
	private Boolean lastResultTruncated = false;
	@SuppressWarnings("unused")
	private Boolean fullResultTruncated = false;
	private String filter = null;
	private int maxCount = -1;
	private ThreadWithStopFlag fetchElementsThread = null;

	protected String fetchElementErrorMesssage;
	
	
	public GRulePane(final String name) {
		this.externalRuleClassName = name;
		
		ruleSpec = GServerInterface.getServer().getRuleSpec(name);
		this.headerRule = ruleSpec.getHeaderRule();
		this.templateRule = ruleSpec.getTemplateRule();
		
//		rules = GServerInterface.getServer().getRules(name, false);
//		rules = new ArrayList<GIModelRule>();
//		Thread fetchRuleTread = new Thread() {
//
//			@Override
//			public void run() {
//				GServerInterface.getServer().getRules(name, false);
//				GElementUpdateHandler.getDisplay().asyncExec(new Runnable() {
//
//					@Override
//					public void run() {
//						List<GIModelRule> rules2 = GServerInterface.getServer().getRules(name, false);
//						rules.addAll(rules2);
//					}
//					
//				});
//				
//			}
//			
//		};
//		fetchRuleTread.start();
		
		for (String elementType : ruleSpec.getUniqueelementTypes()) {
			final int elementIndex = findIndexOfConditionElementType(elementType);
			if (elementIndex>=0)
				uniqueIndices.add(elementIndex);
		}
		
		for(List<String> mandatoryElementTypeGroup : ruleSpec.getMandatoryElementTypes()) {
			Set<Integer> mandatoryIndicesGroup = new HashSet<Integer>();
			for (String elementType : mandatoryElementTypeGroup) {
				final int elementIndex = findIndexOfConditionElementType(elementType);
				if (elementIndex>=0)
					mandatoryIndicesGroup.add(elementIndex);
			}
			if (!mandatoryIndicesGroup.isEmpty())
				mandatoryIndices.add(mandatoryIndicesGroup);
			
		}
		
		if (ruleSpec.getEntityTypeMap() != null) {
			entityTypeMap = ruleSpec.getEntityTypeMap();
		}
		else {
			entityTypeMap = new HashMap<String, List<String>>();			
		}
			
		
	}
	
	
	
	private int findIndexOfConditionElementType(String elementType) {
		for (int i=0; i<templateRule.getRuleElementCount(); i++) {
			GIElement e = templateRule.getElement(i);
			if (e.getEntityType().equalsIgnoreCase(elementType)) {
				return i;
			}
		}
		return -1;
		
	}
	
	private void fireUserUpdateIf(GIRule rule) {
		List<GIElement> elements = rule.getElements();
		for (GIElement element : elements) {
			if (element.getEntityType().equals("User") && !element.checkProperty(GGlobalDefinitions.PROPERTY_REGISTERED_USER)) {
//				GElementUpdateHandler.getElementUpdateHandler().fireElementCreated(GElementUpdateHandler.USER_RULE, null);
				GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated(element.getEntityType(), element.getElementId());
				return;
			}
				
			
		}
	}

	public GIRule addRule(GIRule rule) throws GOperationNotAllowedException {
		checkRule(null, rule);
		GIModelRule newRule = GServerInterface.getServer().addRule(getRuleClassName(), rule);
//		synchronized (rules) {
			rules.add(newRule);
//		}
		fireUserUpdateIf(rule);
		
		return newRule;
	}

	public void deleteRule(GIRule rule) throws GOperationNotAllowedException {
		GServerInterface.getServer().deleteRule(getRuleClassName(), (GIModelRule) rule);
//		synchronized (rules) {
			rules.remove(rule);
//		}
	}
	


	public GIRule getRule(int index) {
		return rules.get(index);
	}

	public int getRuleCount() {
		return rules.size();
	}

	public List<GIRule> getRules() {
		return new ArrayList<GIRule>(rules);
	}

	public GIRule replaceRule(GIRule oldRule, GIRule newRule) throws GOperationNotAllowedException {
		checkRule(oldRule, newRule);
		final int ruleIndex = rules.indexOf(oldRule);
		GIModelRule updatedRule = GServerInterface.getServer().saveRule(getRuleClassName(), (GIModelRule) oldRule, (GIModelRule) newRule);
		if (ruleIndex>=0)
//			synchronized(rules) {
				rules.set(ruleIndex, updatedRule);
//			}
		fireUserUpdateIf(newRule);
		return updatedRule;
	}
	
	public GIRule toggleActivation(GIRule rule) throws GOperationNotAllowedException {
		final int ruleIndex = rules.indexOf(rule);
		GIRule newRule = getEditableRuleCopy(rule);
		newRule = setRuleActive(newRule, !rule.isActive());
		GIModelRule updatedRule = GServerInterface.getServer().saveRule(getRuleClassName(), (GIModelRule) rule, (GIModelRule) newRule);
		if (ruleIndex>=0)
//			synchronized(rules) {
				rules.set(ruleIndex, updatedRule);
//			}
		return updatedRule;
		
	}
	

	public GIRule getEditableRuleCopy(GIRule rule) {
		return new GTempRule(rule);
	}
	
	static class GTempRule extends GAbstractRule {
		
		private GIElement resultElement;
		private List<GIElement> conditions;
		private int id = -1;
		private boolean active;
		

		GTempRule(GIRule rule) {
			resultElement = rule.getResultElement();
			conditions = new ArrayList<GIElement>(rule.getElements());
			if (rule instanceof GIModelRule) 
				id = ((GIModelRule) rule).getId();
			this.active = rule.isActive();
			
		}
		
		GTempRule(GIElement result, List<GIElement> conditions) {
			resultElement = result;
			this.conditions = conditions;
			
		}

		public void clear() {
		}

		@Override
		public boolean isActive() {
			return active;
		}
		
		public boolean deleteEnabled() {
			return true;
		}

		public boolean editEnabled() {
			return true;
		}

		public GIElement getElement(int index) {
			return conditions.get(index);
		}
		
		public void setElement(int index, GIElement element) {
			conditions.set(index, element);
		}

		public GIElement getResultElement() {
			return resultElement;
		}

		public int getRuleElementCount() {
			return conditions.size();
		}

		public boolean multipleElementsAllowed(int index) {
			return false;
		}

		public List<GIElement> getElements() {
			return conditions;
		}

		public int getId() {
			return id;
		}

		public void setResultElement(GIElement e) {
			resultElement = e;
		}

		@Override
		protected void setCondition(int elementIndex, GIElement element) {
			conditions.set(elementIndex, element);
		}


		
	}

	public String getViewHeadline() {
		return ruleSpec.getTitle();
	}
	
	
	

	/* (non-Javadoc)
	 * @see gon_client_management.model.GIRulePane#getLongViewHeadline()
	 */
	public String getLongViewHeadline() {
		return ruleSpec.getTitleLong();
	}
	
	


	/* (non-Javadoc)
	 * @see gon_client_management.model.GIRulePane#refreshData()
	 */
	public void refreshData() {
		getElementsFromServer();
	}

	@Override
	public void refreshData(String elementId) {
		GIModelElement element = GElementUpdateHandler.getElementUpdateHandler().getElement(elementId);
		if (element==null)
			refreshData();
		else {
			String entityType = element.getEntityType().split("\\.",2)[0];
			if (entityType.equals("ProgramAccess")) {
				refreshData();
			}
			else {
				int elementIndex = getRuleElementIndex(element);
				if (elementIndex>-2) {
					for (GIModelRule rule : rules) {
						rule.replaceElement(elementIndex, element);
					}
				}
			}
		}
		
	}



	private int getRuleElementIndex(GIElement element) {
		int elementIndex = -2;
		if (isSameType(templateRule.getResultElement(), element)) {
			elementIndex = -1;
		}
		else {
			for(int i=0; i<templateRule.getElements().size(); i++) {
				if (isSameTypeGroup(templateRule.getElement(i), element)) {
					elementIndex=i;
					break;
				}
			}
		}
		return elementIndex;
	}
	

	public GIRule getNewEmptyRule() {
		
		return new GTempRule(templateRule);
		
	}

	public String getColumnLabel(int index) {
		if (index<0)
			return headerRule.getResultElement().getLabel();
		else
			return headerRule.getElement(index).getLabel();
	}
	
	
	public String getColumnType(int index) {
		if (index<0)
			return headerRule.getResultElement().getEntityType();
		else
			return headerRule.getElement(index).getEntityType();
	}
	
	
	public String getLongColumnLabel(int index) {
		if (index<0)
			return headerRule.getResultElement().getInfo();
		else
			return headerRule.getElement(index).getInfo();
	}


	public int getRuleElementCount() {
		return this.templateRule.getRuleElementCount();
	}

	/*
	public void addRule(GIRule rule) {
		//assignmentList.add((Assignment) rule);
	}

	public void deleteRule(GIRule rule) {
		//assignmentList.remove(rule);
	}
	*/
	
	private boolean isSameType(GIElement e1, GIElement e2) {
		String entityType1 = e1.getEntityType().split("\\.",2)[0];
		String entityType2 = e2.getEntityType().split("\\.",2)[0];
		return entityType1.equals(entityType2);
	}

	private boolean isSameTypeGroup(GIElement e1, GIElement e2) {
		if (isSameType(e1, e2))
			return true;
		String entityType1 = e1.getEntityType().split("\\.",2)[0];
		String entityType2 = e2.getEntityType().split("\\.",2)[0];
		Set<Entry<String, List<String>>> entrySet = entityTypeMap.entrySet();
		for (Entry<String, List<String>> entry : entrySet) {
			if ((entry.getKey().equals(entityType1) || entry.getValue().contains(entityType1)) &&
			    (entry.getKey().equals(entityType2) || entry.getValue().contains(entityType2))) {
				return true;
			}
				
		}
		return false;
	}
	
	public GIRule setRuleActive(GIRule rule, boolean active) {
		final GTempRule tempRule;
		
		if (!(rule instanceof GTempRule))
			tempRule = new GTempRule(rule);
		else
			tempRule = (GTempRule) rule;
		
		tempRule.active = active;
		
		return tempRule;
	}

	@Override
	public GIRule removeCondition(GIRule rule, GIElement e) {
		final GTempRule tempRule;
		
		if (!(rule instanceof GTempRule))
			tempRule = new GTempRule(rule);
		else
			tempRule = (GTempRule) rule;
		
		if (isSameType(templateRule.getResultElement(),e)) {
			tempRule.setResultElement(new GTempElement(templateRule.getResultElement()));
			return tempRule;
			
		}
		

		for (int i=0; i<templateRule.getRuleElementCount(); i++) {
			if (isSameTypeGroup(templateRule.getElement(i), e)) {
				tempRule.setElement(i, new GTempElement(templateRule.getElement(i)));
				return tempRule;
			}
		}
		
		throw new Error("Unknown element type " + e.getEntityType());
		
	}

	public GIRule updateRule(GIRule rule, GIElement e) {
		final GTempRule tempRule;
		
		if (!(rule instanceof GTempRule))
			tempRule = new GTempRule(rule);
		else
			tempRule = (GTempRule) rule;			
		
		if (isSameType(templateRule.getResultElement(),e)) {
			tempRule.setResultElement(e);
			return tempRule;
			
		}
		
		int index = -1;
		for (int i=0; i<templateRule.getRuleElementCount(); i++) {
			if (isSameTypeGroup(templateRule.getElement(i), e)) {
				index = i;
				break;
			}
		}
		if (index>=0) {
			tempRule.setElement(index, e);
			return tempRule;
		} 
		else {
			throw new Error("Unknown element type " + e.getEntityType());
		}
			
	}

	protected String getRuleClassName() {
		return externalRuleClassName;
	}
	
	protected final boolean isEmptyElement(GIElement e) {
		if (e!=null && e instanceof GIModelElement) {
			String id = ((GIModelElement) e).getId();
			if (id!=null && id!="")
				return false;
		}
		return true;
		
	}

	public boolean isValidRule(GIRule rule) {
		if (isEmptyElement(rule.getResultElement()))
			return false;
		if (!checkMandatoryConditions(rule))
			return false;
		return true;
	}

	private boolean checkMandatoryConditions(GIRule rule) {
		if (mandatoryIndices.size()==0)
			return true;
		for(Set<Integer> indicesGroup : mandatoryIndices) {
			boolean missingElement = false;
			for(int index: indicesGroup) {
				if (isEmptyElement(rule.getElement(index))) {
					missingElement = true;
					break;
				}
			}
			if (!missingElement)
				return true;
		}
		return false;
	}

	void checkRule(GIRule oldRule, GIRule newRule) throws GOperationNotAllowedException {
		if (!isValidRule(newRule)) {
			throw new GOperationNotAllowedException("Rule is not valid");
		}
		checkUniqueConditions(oldRule, newRule);
		
	}
	
	void checkUniqueConditions(GIRule oldRule, GIRule newRule) throws GOperationNotAllowedException {
		//if (keyIndex>=0) {
		for(int index : uniqueIndices) {
			final String newId = ((GIModelElement) newRule.getElement(index)).getId();
			final String entityType = newRule.getElement(index).getEntityType();
			for(GIModelRule rule : rules) {
				if (rule==oldRule)
					continue;
				if (((GIModelElement) rule.getElement(index)).getId().equals(newId) &&
					rule.getElement(index).getEntityType().equals(entityType))
				{
					final String name = headerRule.getElement(index).getLabel();
					throw new GOperationNotAllowedException(name + " has already been used in another rule");
				}
			}
		}
	}

	@Override
	public List<String> getSubscriptionTypes() {
		if (subscriptionTypes==null) {
			Set<String> stringSet = new HashSet<String>();
			List<String> basicEntityTypes = GServerInterface.getServer().getBasicEntityTypes(templateRule.getResultElement().getEntityType());
			stringSet.addAll(basicEntityTypes);
			for (GIElement element : templateRule.getElements()) {
				basicEntityTypes = GServerInterface.getServer().getBasicEntityTypes(element.getEntityType());
				stringSet.addAll(basicEntityTypes);
				
			}
			subscriptionTypes = new ArrayList<String>(stringSet);
		}
		return subscriptionTypes;
	}


	void getElementsFromServer() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {
			return;
		}
		hasFetchedElements = false;
		fetchElementsThread = new ThreadWithStopFlag() {

			/* (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				try {
					elementsFetched = false;
//					System.out.println(externalRuleClassName + " clear");
					synchronized (rules) {
						rules.clear();
					}
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STARTED);
					GIRuleElementFetcher<GIModelRule> ruleElementFetcher = GServerInterface.getServer().getRuleFetcher(externalRuleClassName, filter);
					while(!ruleElementFetcher.hasFetchedAll()) {
						if (stopThread) {
							ruleElementFetcher.stop();
							break;
						}
						List<GIModelRule> elements = ruleElementFetcher.getElements(RULE_FETCH_COUNT);
						maxCount = ruleElementFetcher.getCountMax();
						lastResultTruncated = ruleElementFetcher.isResultTruncated();
						if (filter==null)
							fullResultTruncated = lastResultTruncated;

//						System.out.println(externalRuleClassName + " updateElementList");
						
						synchronized (rules) {
							rules.addAll(elements);
						}
						GRulePane.this.setChanged();
						notifyObservers(NotificationType.SOME_ELEMENTS_FETCHED);
						/*
						try {
							sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						*/
					}
					elementsFetched = true;
				}
				finally {
//					System.out.println(externalRuleClassName + " done");
					hasFetchedElements = true;
					GRulePane.this.setChanged();
					notifyObservers(NotificationType.FETCHING_ELEMENTS_STOPPED);
				}
			}
			
		};
		fetchElementErrorMesssage = null;
		fetchElementsThread.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, final Throwable e) {
				fetchElementErrorMesssage  = e.getMessage();
			}
		});
		
		fetchElementsThread.start();
		
	}

	@Override
	public String getFetchElementErrorMesssage() {
		return fetchElementErrorMesssage;
	}


	@Override
	public int getElementCount() {
		return rules.size();
	}



	@Override
	public int getMaxCount() {
		return maxCount;
	}



	@Override
	public boolean hasFetchedAll() {
		return elementsFetched;
	}



	@Override
	public boolean ready() {
		return hasFetchedElements;
	}

	private GIElement getRuleElement(GIRule rule, int index) {
		if (index<0) 
			return rule.getResultElement();
		else
			return rule.getElement(index);
	}


	@Override
	public boolean elementUsed(String id) {
		String[] strings = id.split("\\.", 2);
//		final String elementId = strings[strings.length-1];
//		String entityType = strings[0];
//		for(int i=1; i<strings.length-1; i++) {
//			entityType += "." + strings[i];
//		}
		String elementId = strings[1];
		String entityType = strings[0];
		GTempElement element = new GTempElement();
		element.entityType = entityType;
		element.id = elementId;
		int elementIndex = getRuleElementIndex(element);
		if (elementIndex > -2) {
			for (GIModelRule rule : rules) {
				GIElement ruleElement = getRuleElement(rule, elementIndex);
				if (ruleElement.equals(element))
					return true;
				
			}
		}
		return false;
	}

	private class GTempElement extends GAbstractModelElement {

		
		String id;
		String entityType;
		
		GTempElement() {
			
		}

		GTempElement(GIElement element) {
			this.id = "";
			this.entityType = element.getEntityType();
		}
		
		@Override
		public String getId() {
			return id;
		}

		@Override
		public String getEntityType() {
			return entityType;
		}

		@Override
		public String getElementType() {
			return "";
		}



		@Override
		public String getInfo() {
			return "";
		}

		@Override
		public String getLabel() {
			return "";
		}

		@Override
		public void setLabel(String label) {
			
		}

	}

	@Override
	public boolean isCreateEnabled() {
		return ruleSpec.isCreateEnabled();
	}



	@Override
	public boolean isDeleteEnabled() {
		return ruleSpec.isDeleteEnabled();
	}



	@Override
	public boolean isEditEnabled() {
		return ruleSpec.isEditEnabled();
	}



	@Override
	public void stopFetchingElements() {
		if (fetchElementsThread!=null && fetchElementsThread.isAlive()) {		
			fetchElementsThread.stopThread();
		}
	}
	
}
