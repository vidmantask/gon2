package gon_client_management.model.server;

import gon_client_management.model.GAbstractRule;
import gon_client_management.model.ext.GIElement;

import java.util.ArrayList;
import java.util.List;

public class GServerRuleElement extends GAbstractRule {
	
	private GIElement result_element;
	private final int rule_id;
	private final List<GIElement> rule_elements;
	private boolean active;
	
	GServerRuleElement(com.giritech.admin_ws.RuleType rule) {
		this.rule_id = rule.getId().intValue();
		this.result_element = new GServerElement(rule.getResult_element());
		this.rule_elements = new ArrayList<GIElement>();
		for (int i=0; i < rule.getCondition_elements().length; i++) {
			rule_elements.add(new GServerElement(rule.getCondition_elements()[i]));
		}
		this.active = rule.getActive();
	}

	public void clear() {
	}

	public boolean deleteEnabled() {
		return true;
	}

	public boolean editEnabled() {
		return true;
	}

	public GIElement getElement(int index) {
		return rule_elements.get(index);
	}

	public GIElement getResultElement() {
		return result_element;
	}

	public int getRuleElementCount() {
		return rule_elements.size();
	}

	public boolean multipleElementsAllowed(int index) {
		return false;
	}

	public List<GIElement> getElements() {
		return rule_elements;
	}

	public int getId() {
		return rule_id;
	}

	@Override
	public boolean isActive() {
		return active;
	}


	@Override
	public void setResultElement(GIElement element) {
		this.result_element = element;
		
	}

	@Override
	protected void setCondition(int elementIndex, GIElement element) {
		rule_elements.set(elementIndex, element);
	}
	
	private String elementToString(GIElement element) {
		if (element==null || element.getLabel().isEmpty()) 
			return "<empty>";
		else
			return element.getLabel();
		
	}

	@Override
	public String toString() {
		String repr = "";
		if (rule_elements.size()>0) {
			for(int i=0; i<rule_elements.size()-1; i++) {
				repr += elementToString(rule_elements.get(i)) + " + ";
			}
			repr += elementToString(rule_elements.get(rule_elements.size()-1));
		}
		repr += " -> " + elementToString(result_element);
		
		return repr;
	}

	
}
