package gon_client_management.model;


import java.util.ArrayList;
import java.util.List;

public class GTestSoftwareDeployment implements GISoftwareDeployment {
	
	private List<GICollection> collections;

	public GTestSoftwareDeployment() {
		collections = new ArrayList<GICollection>();
		collections.add(new GCollection("0", "Basic Windows client package collection", "Description for Basic Windows client package collection (Remember wrap)"));
		collections.add(new GCollection("1", "Basic Mac client package collection", "Description for Basic Mac client package collection")); 
		collections.add(new GCollection("2", "Basic Linux client package collection", "Description for Basic Linux client package collection"));
		collections.add(new GCollection("3", "Full cross platform client package collection", "Description for Full cross platform client package collection"));

		final String [][] packageContent = {{"0", "G/On client for windows", "5.2", "13 MB"},
				{"0", "Remote Desktop", "7.4", "75 MB"},
				{"1", "G/On client for mac", "5.2", "45 MB"}, 
				{"1", "Remote desktop for mac", "73.3", "15 MB"},
				{"2", "G/On client for linux", "5.2", "45 MB"},
				{"2", "Remote Desktop for linux", "6.3", "742 MB"},
				{"3", "G/On client for windows", "5.2", "13 MB"},
				{"3", "Remote Desktop", "7.4", "75 MB"},
				{"3", "G/On client for mac", "5.2", "45 MB"}, 
				{"3", "Remote desktop for mac", "73.3", "15 MB"},
				{"3", "G/On client for linux", "5.2", "45 MB"},
				{"3", "Remote Desktop for linux", "6.3", "742 MB"},
			   };

		for (int i=0; i<packageContent.length; i++) {
			addPackage(packageContent[i][0], packageContent[i][1], packageContent[i][2], packageContent[i][3]);
		}
		
	}

	public List<GICollection> getCollections() {
		return collections;
	}

	public void addPackage(String id, String title, String version, String size) {
		GPackage package1 = new GPackage(title, version, size);
		for (GICollection collection : collections) {
			if (collection.getId().equals(id)) {
				((GCollection) collection).packages.add(package1);
				break;
			}
			
			
		}
	}
	
	public void installCollection() {

	}
	
	static class GCollection implements GICollection {
		
		public GCollection(String id, String title, String description) {
			super();
			packages = new ArrayList<GIPackage>();
			this.title = title;
			this.id = id;
			this.description = description;
		}

		public String title;
		public List<GIPackage> packages;
		private String description;
		private String id;

		public String getDescription() {
			// TODO Auto-generated method stub
			return description;
		}

		public List<GIPackage> getPackages() {
			return packages;
		}

		public String getTitle() {
			return title;
		}

		public String getId() {
			return id;
		}
		
		public void addPackage(String id, String title, String version, String size) {
		}
		
	}
	
	static class GPackage implements GIPackage {

		public GPackage(String title, String version, String size) {
			super();
			this.title = title;
			this.version = version;
			this.size = size;
		}

		private String title;
		private String version;
		private String size;

		public String getTitle() {
			return title;
		}

		public String getVersion() {
			return version;
		}

		public String getDescription() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getId() {
			// TODO Auto-generated method stub
			return null;
		}

		public String getPlatform() {
			return size;
		}
		
	}

	public GICollection getCollection(String id) {
		for (GICollection collection : collections) {
			if (collection.getId().equals(id)) {
				return collection;
			}
		}
		return null;
	}

	public void refresh() {
		// TODO Auto-generated method stub
		
	}

}
