package gon_client_management.model.ext;


import java.util.ArrayList;
import java.util.List;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Field_element;
import com.giritech.admin_ws.types_config_template.Field_type_type0;


public abstract class GConfigPane extends GAbstractConfigPane implements GIConfigPane {

	protected GIElement configElement = null;

	
	protected String entityType;
	private GConfigPanePage configPage;
	protected GConfigTemplateUtil configTemplateUtil;

	private class GConfigPanePage extends GConfigTemplateUtil.GConfigPanePage {
		


		public GConfigPanePage(GConfigTemplateUtil configTemplateUtil) {
			configTemplateUtil.super();
		}

		public GConfigPanePage(boolean advanced) {
			configTemplateUtil.super(advanced);
		}

		protected void addField(final Field_element field) {
			
			
			if (field.getField_type() == Field_type_type0.hidden) {
				GIConfigColumn pageColumn = createConfigColumn(field);
				setDefaultValue(field, pageColumn);
				
			}
			else {
				super.addField(field);
				
			}
			
		}

		protected GConfigPanePage createAdvancedPane() {
			return new GConfigPanePage(true);
		}
		
		

		public String getTitle() {
			if (configElement!=null)
				return configElement.getLabel();
			else
				return configTemplateUtil.template.getTitle();
		}
		
	}
	
	public GConfigPane(String entityType, ConfigurationTemplate template) {
		init(entityType, template);
	}
	

	public GConfigPane(GIElement element, ConfigurationTemplate template) {
		this.configElement  = element;
		init(element.getEntityType(), template);
	}
	
	


	public GIConfigPanePage getAdvancedPane() {
		return configPage.getAdvancedPane();
	}
	
	
	
	

	protected void init(String entityType, ConfigurationTemplate template) {
		this.entityType = entityType;
		if (template==null)
			throw new RuntimeException("No template for creating object of type " + entityType);
		configTemplateUtil = new GConfigTemplateUtil(template, getFieldAction()); 
		configPage = new GConfigPanePage(configTemplateUtil);
		
	}




	abstract protected GIFieldAction getFieldAction();


	public GIConfigColumn getColumn(String name) throws UnknownColumnException {
		return null;
	}


	public List<GIConfigPanePage> getConfigPages() {
		ArrayList<GIConfigPanePage> list = new ArrayList<GIConfigPanePage>();
		list.add(configPage);
		return list;
	}


	public GIConfigRowValue getValue(String name) {
		return configTemplateUtil.values.get(name);
	}


	abstract public void save() throws GOperationNotAllowedException;
	
	
	public GIConfigColumn getColumn(int index) {
		return configPage.getColumn(index);
	}


	public int getColumnCount() {
		return configPage.getColumnCount();
	}


	public GIConfigRowValue getValue(int index) {
		return configPage.getValue(index);
	}


	public String getTitle() {
		return configTemplateUtil.template.getTitle();
	}


}
