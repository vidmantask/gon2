package gon_client_management.model;

import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.server.GServerInterface;

import java.lang.Thread.UncaughtExceptionHandler;
import java.math.BigInteger;

import com.giritech.admin_ws.GOnServiceType;

public class GGatewayConfig implements GIGatewayServiceConfig {

private volatile boolean updateEnabled = false;
private volatile GOnServiceType[] serviceList = new GOnServiceType[0];
protected String fetchElementErrorMesssage; 


	//	@Override
	public GOnServiceType[] getServices1() {
		GOnServiceType gOnServiceType = createService(0);
		Pair<GOnServiceType[], Boolean> getServiceListPair = GServerInterface.getServer().GetServiceList();
		GOnServiceType[] getServiceList = getServiceListPair.getFirst();
		updateEnabled = getServiceListPair.getSecond().booleanValue();
		GOnServiceType[] array = new GOnServiceType[getServiceList.length+1];
		array[0] = gOnServiceType;
		for(int i=0; i<getServiceList.length; i++)
			array[i+1] = getServiceList[i];
		return array;
	}
	
	
	
	@Override
	public GOnServiceType[] getServices() {
		return serviceList;
	}

//	@Override
	public GOnServiceType[] getServices2() {
		GOnServiceType[] array = new GOnServiceType[4];
		for(int i=0; i<4; i++) {
			GOnServiceType gOnServiceType = createService(i+1);
			array[i] = gOnServiceType;
		}
		return array;
	}



	private GOnServiceType createService(int index) {
		GOnServiceType gOnServiceType = new GOnServiceType();
		gOnServiceType.setIp("1.1.1.1");
		gOnServiceType.setPort(BigInteger.valueOf(80));
		gOnServiceType.setSid("" + index);
		gOnServiceType.setStatus("running");
		return gOnServiceType;
	}
	
	@Override
	public void restartService(String sid, boolean whenNoUsers) {
		GServerInterface.getServer().RestartGatewayService(sid, whenNoUsers);
		
	}

	@Override
	public void stopService(String sid, boolean whenNoUsers) {
		GServerInterface.getServer().StopGatewayService(sid, whenNoUsers);
		
	}


	@Override
	public boolean isUpdateEnabled() {
		return updateEnabled;
	}


	@Override
	public void refresh(final GIGuiCallback callback) {
		Thread t = new Thread() {

			@Override
			public void run() {
				Pair<GOnServiceType[], Boolean> serviceListPair = GServerInterface.getServer().GetServiceList();
				serviceList = serviceListPair.getFirst();
				updateEnabled = serviceListPair.getSecond().booleanValue();
				callback.done();
			}
			
		};
		
		t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
			
			@Override
			public void uncaughtException(Thread t, final Throwable e) {
				fetchElementErrorMesssage  = e.getMessage();
			}
		});
		
		t.start();
		
	}


}
