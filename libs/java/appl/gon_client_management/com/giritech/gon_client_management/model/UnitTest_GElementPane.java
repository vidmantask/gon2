package gon_client_management.model;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.model.server.GTestServer;
import junit.framework.TestCase;

public class UnitTest_GElementPane extends TestCase {

	//private final GElementPane elementPane;
	private static GTestServer testServer = new GTestServer();
	
	private static String CLASS_TEST = "ClassElements";
	private static String PLUGIN_TEST = "PluginElements";
	
	private static int CLASS_TEST_COUNT = GElementPane.ELEMENT_FETCH_COUNT/4;
	private static int PLUGIN_TEST_COUNT = GElementPane.ELEMENT_FETCH_COUNT*2+1;
	
	
	public UnitTest_GElementPane(String name) {
		super(name);
		GServerInterface.setServer(testServer);
		testServer.createElementPane(CLASS_TEST, "class", CLASS_TEST_COUNT);
		testServer.createElementPane(PLUGIN_TEST, "plugin", PLUGIN_TEST_COUNT);
	}

	private void waitForElements(final GIElementPane elementPane) {
		while (!elementPane.ready()) {
			synchronized (this) {
				try {
					wait(500);
				} catch (InterruptedException e) {
				}
			}
			
		}
	}
	
	
	public void testGetElements() {
		final GElementPane elementPane = new GElementPane(PLUGIN_TEST);
		waitForElements(elementPane);
		elementPane.addObserver(new Observer() {
			
			int count = 0;

			public void update(Observable o, Object arg) {
				if (arg instanceof GIElementPane.NotificationType &&
					((GIElementPane.NotificationType) arg)==GIElementPane.NotificationType.SOME_ELEMENTS_FETCHED) {
					count++;
					int n = elementPane.getElements().size();
					if (n<PLUGIN_TEST_COUNT)
						assertEquals(count*GElementPane.ELEMENT_FETCH_COUNT, n);
					
				}
			}
			
		});
		elementPane.refreshData();
		waitForElements(elementPane);
		List<GIElement> elements = elementPane.getElements();
		assertEquals(PLUGIN_TEST_COUNT, elements.size());
	}



	public void testGetViewHeadline() {
		final GIElementPane elementPane = new GElementPane(CLASS_TEST);
		assertEquals("Info",elementPane.getViewHeadline());
		
	}

	public void testGetColumnLabel() {
		final GIElementPane elementPane = new GElementPane(CLASS_TEST);
		assertEquals("Header",elementPane.getColumnLabel());
	}

	public void testGetColumnLabelPlural() {
		final GIElementPane elementPane = new GElementPane(CLASS_TEST);
		assertEquals("Info",elementPane.getColumnLabelPlural());
	}







}
