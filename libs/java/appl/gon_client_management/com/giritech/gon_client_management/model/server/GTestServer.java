/**
 * 
 */
package gon_client_management.model.server;

import gon_client_management.model.GAbstractModelElement;
import gon_client_management.model.GIModelConfigColumn;
import gon_client_management.model.GIModelElement;
import gon_client_management.model.GIModelRule;
import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.ext.GIConfigColumn.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import admin_ws.GetConfigSpecResponse;


public class GTestServer extends GServerAdapter {


	public Map<String, RulePane> panes = new HashMap<String, RulePane>();
	private Map<String, ElementPane> elementMap = new HashMap<String, ElementPane>();
	public Map<String, ConfigPane> configPanes = new HashMap<String, ConfigPane>();
	private static int gRuleIdCount=0; 
	private static int gRuleElementIdCount=0; 
	
	public class RulePane {
		private final ArrayList<GIModelRule> rules = new ArrayList<GIModelRule>();
		private final RuleSpec ruleSpec = new RuleSpec();
		
		public GIModelRule addRule(GIRule rule) {
			Rule newRule = new Rule(rule);
			rules.add(newRule);
			return newRule;
		}
		
		
	}
	
	public class RuleSpec implements GIRuleSpec {
		public String title = "";
		public String titleLong = "";
		
		public List<String> uniqueelementTypes = new ArrayList<String>();
		public List<List<String>> mandatoryElementTypes = new ArrayList<List<String>>();

		private final Map<String, List<String>> entityTypeMap = new HashMap<String, List<String>>();
		
		public Rule templateRule;
		public Rule headerRule;
		
		
		public GIRule getHeaderRule() {
			return headerRule;
		}
		public List<List<String>> getMandatoryElementTypes() {
			return mandatoryElementTypes;
		}
		public GIRule getTemplateRule() {
			return templateRule;
		}
		public String getTitle() {
			return title;
		}
		public String getTitleLong() {
			return titleLong;
		}
		public List<String> getUniqueelementTypes() {
			return uniqueelementTypes;
		}
		@Override
		public Map<String, List<String>> getEntityTypeMap() {
			return entityTypeMap;
		}
		@Override
		public boolean isCreateEnabled() {
			return true;
		}
		@Override
		public boolean isDeleteEnabled() {
			return true;
		}
		@Override
		public boolean isEditEnabled() {
			return true;
		}
		
	}
	
	
	public static class RuleElement extends GAbstractModelElement {
		
		public String type;
		public String id;
		public String label;
		public String info;
		public String elementType;


		public RuleElement(String type, String label, String info) {
			super();
			this.type = type;
			this.id = "" + ++gRuleElementIdCount;
			this.label = label;
			this.info = info;
			this.elementType = "class";
		}
		
		public RuleElement(GIElement template, String label) {
			this(template.getEntityType(), label, "");
			this.label = label + id;
			this.info = "info" + id;
		}
		
		public String getEntityType() {
			return this.type;
		}

		public String getId() {
			return id;
		}

		public String getInfo() {
			return info;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getElementType() {
			return elementType;
		}
		
	}
	
	public static class Rule implements GIModelRule {
		
		private int id;
		private boolean deleteEnabled = true;
		private boolean editEnabled = true;
		private List<GIElement> elements = new ArrayList<GIElement>();
		private GIElement resultElement;

		Rule(GIElement resultElement) {
			super();
			this.id = ++gRuleIdCount;
			this.resultElement = resultElement;
		}

		Rule(GIRule rule) {
			super();
			this.id = ++gRuleIdCount;
			this.resultElement = rule.getResultElement();
			this.elements = rule.getElements();
			this.editEnabled = rule.deleteEnabled();
			this.deleteEnabled = rule.deleteEnabled();
		}
		
		public Rule(GIElement resultElement, List<GIElement> list) {
			this(resultElement);
			this.elements = list;
		}

		public int getId() {
			return id;
		}

		public boolean deleteEnabled() {
			return deleteEnabled;
		}

		public boolean editEnabled() {
			return editEnabled;
		}

		public GIElement getElement(int index) {
			return elements.get(index);
		}

		public List<GIElement> getElements() {
			return elements;
		}

		public GIElement getResultElement() {
			return resultElement;
		}

		public int getRuleElementCount() {
			return elements.size();
		}

		public boolean multipleElementsAllowed(int index) {
			return false;
		}

		@Override
		public boolean isActive() {
			return true;
		}
		
		void setDeleteEnabled(boolean deleteEnabled) {
			this.deleteEnabled = deleteEnabled;
		}

		void setEditEnabled(boolean editEnabled) {
			this.editEnabled = editEnabled;
		}

		public void setResultElement(GIElement resultElement) {
			this.resultElement = resultElement;
		}
		
		void addCondition(GIElement element) {
			this.elements.add(element);
		}

		@Override
		public void replaceElement(int elementIndex, GIElement element) {
			// TODO Auto-generated method stub
			
		}

		
	}
	
	
	public class ElementPane {
		private final ArrayList<GIModelElement> elements = new ArrayList<GIModelElement>();
		private final GIModelElement headerElement;
		
		public ElementPane(String entityType, String elementType, int elementCount) {
			RuleElement element = new RuleElement(entityType, "Header", "Info");
			element.elementType = elementType;
			this.headerElement = element; 
			
			for(int i=0; i<elementCount; i++) {
				elements.add(new RuleElement(headerElement, "Label"));
				
			}
			
		}
		
	}
	
	
	public static class ConfigColumn implements GIModelConfigColumn {
		public Type type;
		public String label= "";
		public String name;
		public String tooltip = null;
		public boolean readOnly = false;
		public boolean hidden = false;
		public boolean secret= false;
		public boolean isId = false;
		
		public Type getType() {
			return type;
		}
		public String getLabel() {
			return label;
		}
		public String getName() {
			return name;
		}
		public boolean isReadOnly() {
			return readOnly;
		}
		public boolean isHidden() {
			return hidden;
		}
		public boolean isSecret() {
			return secret;
		}
		public String getToolTip() {
			return tooltip;
		}
		public boolean isId() {
			return isId;
		}
		public GISelection getSelection() {
			return null;
		}
		public GIAction getAction() {
			return null;
		}
		public int getTextLimit() {
			return -1;
		}
		@Override
		public String getSection() {
			return "";
		}
		
	}
	
	
	public static class ConfigPane {
		
		public final List<ConfigColumn> columns = new ArrayList<ConfigColumn>();
		public String name;
		private Map<String, String> values = null;
		
		public ConfigPane(String name) {
			this.name = name;
		}

		public Map<String, String> getRow() {
			if (values==null) {
				values = new HashMap<String, String>();
				for(int index = 0; index<columns.size(); index++) {
					ConfigColumn col = columns.get(index);
					String value = col.type.toString() + index;
					switch (col.type) {
					case TYPE_BOOLEAN:
						value = "" + index%2;
						break;
					case TYPE_STRING:
					case TYPE_TEXT:
						value = col.name + index;
						break;
					case TYPE_INTEGER:
						value = "" + index;
						break;
					default:
						value = "_Unknown";
						break;
					}
					values.put(col.name, value);
				}
			}
			return values;
		}
		
	}

	public class RuleElementFetcher implements GIRuleElementFetcher {
		
		

		@SuppressWarnings("unused")
		private final boolean refresh;
		private final ElementPane elementPane;
		private int fetched;
		private boolean hasFetchedAll;

		public RuleElementFetcher(String element_type, boolean refresh) {
			this.elementPane = elementMap.get(element_type);
			this.refresh = refresh;
			this.fetched = 0;
			this.hasFetchedAll = false;
		}

		public int getCountMax() {
			return -1;
		}

		public List<GIModelElement> getElements(int count) {
			if (hasFetchedAll)
				return null;
			
			int maxIndex = fetched+count;
			if (maxIndex>elementPane.elements.size())
				maxIndex = elementPane.elements.size();
			List<GIModelElement> elements = new ArrayList<GIModelElement>();
			for(int i=fetched; i<maxIndex; i++) {
				elements.add(elementPane.elements.get(i));
			}
			fetched = maxIndex;
			if (fetched==elementPane.elements.size())
				hasFetchedAll = true;
			return elements;
		}

		public boolean hasFetchedAll() {
			return hasFetchedAll;
		}

		@Override
		public boolean isResultTruncated() {
			return false;
		}

		@Override
		public void stop() {
			// TODO Auto-generated method stub
			
		}

	}
	
	public GTestServer() {
		/*
		RuleElement resultTemplateElement = new RuleElement("result", "", "Result");
		this.templateRule = new Rule(-1, resultTemplateElement);
		RuleElement conditionElement1 = new RuleElement("condition1", "", "Condition1");
		templateRule.addCondition(conditionElement1);
		RuleElement conditionElement2 = new RuleElement("condition2", "", "Condition2");
		templateRule.addCondition(conditionElement2);
		RuleElement conditionElement3 = new RuleElement("condition3", "", "Condition3");
		templateRule.addCondition(conditionElement3);
		
		for(int i=0; i<10; i++) {
			Rule rule = new Rule(i, new RuleElement(resultTemplateElement, Integer.toString(i)));
			for(int j=0; j<templateRule.getRuleElementCount(); j++) {
				GIElement element = templateRule.getElement(j);
				rule.addCondition(new RuleElement(element, "Condition" + Integer.toString(10*i+j)));
			}
			rules.add(rule);
			
		}
		rules.add(templateRule);
		*/
	}
	
	public Rule createRuleTemplate(final int conditionCount) {
		RuleElement resultTemplateElement = new RuleElement("result", "", "Result");
		Rule rule = new Rule(resultTemplateElement);
		for(int i=0; i<conditionCount; i++) {
			RuleElement conditionElement = new RuleElement("condition" + i, "", "Condition" + i);
			rule.addCondition(conditionElement);
		}
		return rule;
		
	}

	
	public Rule createRuleHeader(final int conditionCount) {
		RuleElement resultTemplateElement = new RuleElement("result", "HeaderLabel", "HeaderInfo");
		Rule rule = new Rule(resultTemplateElement);
		for(int i=0; i<conditionCount; i++) {
			RuleElement conditionElement = new RuleElement("condition" + i, "ConditionLabel" + i, "HeaderConditionInfo" + i);
			rule.addCondition(conditionElement);
		}
		return rule;
		
	}
	
	public void createRulePane(String name, final int conditionCount, final int ruleCount) {
		RulePane rulePane = new RulePane();
		rulePane.ruleSpec.templateRule = createRuleTemplate(conditionCount);
		rulePane.ruleSpec.headerRule = createRuleHeader(conditionCount);
		rulePane.ruleSpec.title = name + "Title";
		rulePane.ruleSpec.titleLong = name + "TitleLong";
		this.panes.put(name, rulePane);
		
		for(int i=0; i<ruleCount; i++) {
			Rule rule = createRule(name); 
			rulePane.rules.add(rule);
		}
		
	}
	
	public void createElementPane(String name, String elementType, int elementCount) {
		ElementPane elementPane = new ElementPane(name, elementType, elementCount);
		elementMap.put(name, elementPane);
	}
	
	
	
	public Rule createRule(String name) {
		RulePane rulePane = panes.get(name);
		Rule rule = new Rule(new RuleElement(rulePane.ruleSpec.templateRule.resultElement, "Result"));
		int rule_id = rule.getId();
		for(int j=0; j<rulePane.ruleSpec.templateRule.getRuleElementCount(); j++) {
			GIElement element = rulePane.ruleSpec.templateRule.getElement(j);
			rule.addCondition(new RuleElement(element, "Condition" + rule_id + j));
		}
		
		return rule;
	}
	
	
	public void createConfigPane(String name, int noOfColumns, int noOfTypes, int noOfHidden) {
		ConfigPane configPane = new ConfigPane(name);
		if (noOfColumns>0) {
			if (noOfTypes<0) 
				noOfTypes = Type.values().length;
			for(int i=0; i<noOfColumns; i++) {
				final int typeIndex = i%noOfTypes;
				Type type = Type.values()[typeIndex];
				ConfigColumn configColumn = new ConfigColumn();
				configColumn.label = "Header" + i;
				configColumn.type = type;
				configColumn.name = "Column" + i;
				configPane.columns.add(configColumn);
			}
			for(int i=0; i<noOfHidden/2; i++) {
				configPane.columns.get(i).hidden = true;
				configPane.columns.get(configPane.columns.size()-(i+1)).hidden = true;
			}
		}
		this.configPanes.put(name, configPane);
	}

	@Override
	public GIModelRule addRule(String ruleClassName, GIRule rule)
			throws GOperationNotAllowedException {
		RulePane rulePane = panes.get(ruleClassName);
		return rulePane.addRule(rule);
	}

	@Override
	public void deleteRule(String ruleClassName, GIModelRule rule)
			throws GOperationNotAllowedException {
		RulePane rulePane = panes.get(ruleClassName);
		rulePane.rules.remove(rule);
	}



	@Override
	public List<GIModelRule> getRules(String ruleClassName, boolean refresh) {
		RulePane rulePane = panes.get(ruleClassName);
		return new ArrayList<GIModelRule>(rulePane.rules);
	}

	@Override
	public GIModelRule saveRule(String ruleClassName, GIModelRule oldRule, GIModelRule newRule)
			throws GOperationNotAllowedException {
		return newRule;
	}

	@Override
	public GIRuleSpec getRuleSpec(String className) {
		RulePane rulePane = panes.get(className);
		return rulePane.ruleSpec;
	}

	@Override
	public GIModelElement createRuleElement(GIModelElement element) throws GOperationNotAllowedException {
		String entityType = element.getEntityType();
		ElementPane elementPane = elementMap.get(entityType);
		RuleElement newElement = new RuleElement(element, "");
		newElement.label = element.getLabel();
		elementPane.elements.add(newElement);
		return newElement;
	}

	@Override
	public boolean deleteRuleElement(GIModelElement modelElement) throws GOperationNotAllowedException {
		String entityType = modelElement.getEntityType();
		ElementPane elementPane = elementMap.get(entityType);
		return elementPane.elements.remove(modelElement);
	}

	@Override
	public GIRuleElementFetcher getRuleElementFetcher(String element_type, boolean refresh) {
		return new RuleElementFetcher(element_type, refresh);
	}

	@Override
	public GIModelElement getRuleElementHeader(String element_type) {
		ElementPane elementPane = elementMap.get(element_type);
		return elementPane.headerElement;
	}


	@Override
	public GIModelElement updateRuleElement(GIModelElement modelElement)
			throws GOperationNotAllowedException {
		return modelElement;
	}

	@Override
	public Map<String, String> createConfigRow(String entityType, Map<String, GIConfigRowValue> values) throws GOperationNotAllowedException {
		ConfigPane configPane = getConfigPane(entityType);
		if (configPane!=null) {
			configPane.getRow();
			for(String name: values.keySet()) {
				GIConfigRowValue value = values.get(name);
				if (!value.isNull())
					configPane.values.put(name, value.encode());
			}
			return configPane.values;
		}
		else
			return null;
	}

	@Override
	public boolean deleteConfigRow(GIElement element)
			throws GOperationNotAllowedException {
		// TODO Auto-generated method stub
		return super.deleteConfigRow(element);
	}

	private String rsplit(String s, String sep) {
		String[] split = s.split(sep);
		if (split.length>0)
			return split[split.length-1];
		else
			return "";
		
	}
	
	ConfigPane getConfigPane(String entityType) {
		entityType = rsplit(entityType, "\\.");
		return configPanes.get(entityType);
		
	}
	
	@Override
	public List<GIModelConfigColumn> getConfigColumns(String entityType) {
		ConfigPane configPane = getConfigPane(entityType);
		if (configPane!=null)
			return new ArrayList<GIModelConfigColumn>(configPane.columns);
		else
			return null;
	}

	@Override
	public Map<String, String> getConfigRow(GIElement element) {
		ConfigPane configPane = getConfigPane(element.getEntityType());
		if (configPane!=null)
			return configPane.getRow();
		else
			return null;
	}

	@Override
	public GetConfigSpecResponse getConfigSpec(String name) {
		// TODO Auto-generated method stub
		return super.getConfigSpec(name);
	}

	@Override
	public Map<String, String> updateConfigRow(String entityType,Map<String, GIConfigRowValue> values) throws GOperationNotAllowedException {
		ConfigPane configPane = getConfigPane(entityType);
		if (configPane!=null) {
			for(String name: values.keySet()) {
				GIConfigRowValue value = values.get(name);
				configPane.values.put(name, value.encode());
			}
			return configPane.values;
		}
		else
			return null;
	}
	
	

	
	
}
