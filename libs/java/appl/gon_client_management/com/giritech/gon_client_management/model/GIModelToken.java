package gon_client_management.model;

public interface GIModelToken extends GIToken {

	public String getTypeSpecificId();
	
}
