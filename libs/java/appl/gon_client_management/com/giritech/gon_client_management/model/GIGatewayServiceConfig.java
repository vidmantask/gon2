package gon_client_management.model;

import com.giritech.admin_ws.GOnServiceType;

public interface GIGatewayServiceConfig {
	
	public static String STATE_RUNNING = "running";
	public static String STATE_RESTART_WHEN = "restart_when_no_sessions";
	public static String STATE_STOP_WHEN = "stop_when_no_sessions";
	public static String STATE_STOPPING = "stopping";
	public static String STATE_RESTARTING = "restarting";

	
	GOnServiceType[] getServices();

	
	void stopService(String sid, boolean whenNoUsers);

	void restartService(String sid, boolean whenNoUsers);
	
	public boolean isUpdateEnabled();


	void refresh(GIGuiCallback callback);
	
}
