package gon_client_management.model.localservice;


public class GLocalServerInterface {
	private static int server_type = 1;
	private static GILocalServiceFactory factory = null;
	private static GILocalService service = null;
	
	public static GILocalService getLocalService() {
		try {
			if (service!=null)
				return service;
			if (factory==null) {
				switch (server_type) {
				case 0:
					factory = null;
					break;
	
				case 1:
					factory = new GLocalServiceFactory();
					break;
	
				default:
					break;
				}
				
			}
			return factory.getLocalService();
		}
		catch (Throwable t) {
			throw new RuntimeException("Unable to connect to local service");
		}
	}

	/**
	 * @param server the server to set
	 */
	public static void setService(GILocalService service) {
		GLocalServerInterface.service = service;
	}

}
