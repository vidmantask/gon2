package gon_client_management.model;

import gon_client_management.model.ext.GIElement;

public interface GIModelRule extends GIRule {
	
	public int getId();

	public void setResultElement(GIElement element);

	public void replaceElement(int elementIndex, GIElement element);

}
