package gon_client_management.model;

import gon_client_management.model.ext.GIConfigPane;

public interface GIGetConfigListener {
	
	public void ConfigFetched(GIConfigPane configPane);

}
