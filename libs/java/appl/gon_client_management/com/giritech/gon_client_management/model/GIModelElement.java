package gon_client_management.model;

import gon_client_management.model.ext.GIElement;

public interface GIModelElement extends GIElement {

	public String getId();

	public String getElementType();
	
}
