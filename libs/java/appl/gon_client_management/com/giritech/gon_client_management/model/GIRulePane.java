package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;


public interface GIRulePane extends GIObservableListPane {
	
	public int getRuleCount();
	public GIRule getRule(int index);
	
	public int getRuleElementCount();
	
	public void deleteRule(GIRule rule) throws GOperationNotAllowedException;
	public GIRule updateRule(GIRule rule, GIElement e);
	public GIRule removeCondition(GIRule rule, GIElement e);
	public GIRule addRule(GIRule rule) throws GOperationNotAllowedException;
	public GIRule replaceRule(GIRule selectedRule, GIRule newRule) throws GOperationNotAllowedException;
	public GIRule toggleActivation(GIRule rule) throws GOperationNotAllowedException;
	public List<GIRule> getRules();
	
	/**
	 * Get a name to display as a headline for the view.
	 * 
	 * @return A string with a headline for the view. 
	 */
	public abstract String getViewHeadline();

	/**
	 * Get a name to display as a headline for the view.
	 * 
	 * @return A string with a headline for the view. 
	 */
	public abstract String getLongViewHeadline();

	/**
	 * Get names for column headers.
	 * 
	 * @param index - if index < 0 the result column is assumed.
	 * @return A single string label naming the column at index.
	 */
	public abstract String getColumnLabel(int index);

	/**
	 * Get long names for column headers.
	 * 
	 * @param index - if index < 0 the result column is assumed.
	 * @return A single string label naming the column at index. 
	 */
	public abstract String getLongColumnLabel(int index);

	/**
	 * Get type for this column. Used for Icons and tooltip text.
	 * 
	 * @param index - if index < 0 the result column is assumed.
	 * @return A single string label naming the column at index. 
	 */
	public String getColumnType(int index);
	
	
	/**
	 * When trying to create a new rule. We need
	 * to be able to see the structure. So getting
	 * a fresh and empty rule to put elements into
	 * would be nice.
	 * 
	 * @return A fresh and empty GIRule
	 */
	public abstract GIRule getNewEmptyRule();

	/**
	 * 
	 * @param rule
	 * @return
	 */
	public abstract GIRule getEditableRuleCopy(GIRule rule);
	
	
	public boolean isValidRule(GIRule rule);
	
	/**
	 * fetches all rules from server again
	 */
	public void refreshData();
	public void refreshData(String elementId);
	
	public List<String> getSubscriptionTypes();
	
	public GIRule setRuleActive(GIRule rule, boolean active);
	public boolean elementUsed(String id);
	
	public boolean isCreateEnabled();
	public boolean isEditEnabled();
	public boolean isDeleteEnabled();
	
	
	
}
