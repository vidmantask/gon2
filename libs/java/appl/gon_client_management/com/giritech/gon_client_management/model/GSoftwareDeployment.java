package gon_client_management.model;

import gon_client_management.model.localservice.GLocalServerInterface;

import java.util.ArrayList;
import java.util.List;

import com.giritech.admin_ws.types_gpm.GPMCollectionInfoType;
import com.giritech.admin_ws.types_gpm.GPMInfoType;

public class GSoftwareDeployment implements GISoftwareDeployment {

	private List<GICollection> collections;
	
	public GSoftwareDeployment() {
		super();
		collections = null;
	}

	public List<GICollection> getCollections() {
		if (collections==null) {
			collections = new ArrayList<GICollection>();
			GPMCollectionInfoType[] packageCollections = GLocalServerInterface.getLocalService().getPackageCollections();
			for(GPMCollectionInfoType gpm : packageCollections) {
				GCollection collection = new GCollection(gpm.getGpm_collection_id(),
														 gpm.getGpm_collection_summary(), 
														 gpm.getGpm_collection_description());
				GPMInfoType[] gpm_packages = gpm.getGpm_collection_gpms();
				if (gpm_packages!=null) {
					for(GPMInfoType gpm_package : gpm_packages) {
						GPackage package_ = new GPackage(gpm_package.getGpm_id());
						package_.setTitle(gpm_package.getGpm_summary());
						package_.setPlatform(gpm_package.getGpm_arch());
						package_.setDescription(gpm_package.getGpm_description());
						package_.setVersion(gpm_package.getGpm_version());
						collection.addPackage(package_);
					}
				}
				collections.add(collection);
			}
		}
		return collections;
	}
	
	public void refresh() {
		collections = null;
	}
	

	public void installCollection() {
		// TODO Auto-generated method stub

	}
	static class GCollection implements GICollection {
		
		public GCollection(String id, String title, String description) {
			super();
			packages = new ArrayList<GIPackage>();
			this.title = title;
			this.id = id;
			this.description = description;
		}

		private String title;
		private List<GIPackage> packages;
		private String description;
		private String id;

		public String getDescription() {
			return description;
		}

		public List<GIPackage> getPackages() {
			return packages;
		}

		public String getTitle() {
			return title;
		}

		public String getId() {
			return id;
		}
		
		public void addPackage(GIPackage package_) {
			packages.add(package_);
		}
		
	}
	
	static class GPackage implements GIPackage {


		public GPackage(String id) {
			super();
			this.id = id;
		}

		private String title;
		private String version;
		private String platform;
		private String description;
		private final String id;

		public String getPlatform() {
			return platform;
		}

		public String getTitle() {
			return title;
		}

		public String getVersion() {
			return version;
		}

		public String getDescription() {
			return description;
		}

		public String getId() {
			return id;
		}
		void setTitle(String title) {
			this.title = title;
		}

		void setVersion(String version) {
			this.version = version;
		}

		void setPlatform(String platform) {
			this.platform = platform;
		}

		void setDescription(String description) {
			this.description = description;
		}
		
	}

	public GICollection getCollection(String id) {
		for (GICollection collection : collections) {
			if (collection.getId().equals(id)) {
				return collection;
			}
		}
		return null;
	}

}
