package gon_client_management.model;

import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.List;

public interface GIDeployment extends GISoftwareDeployment {


	
	public interface KeyPair {
		
		public String getPrivateKey();
		
		public String getPublicKey();
	}
	
	public int getTokenCount();
	
	public List<GIToken> getTokens();

	public void refreshTokenList();

	public String enrollToken(GIToken token) throws GOperationNotAllowedException;

	

}
