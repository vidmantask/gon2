package gon_client_management.model.ext;


import com.giritech.admin_ws.types_config_template.ConfigurationTemplateSpec;

public interface GIDetails {

	public String getName();

	public String getTitle();

	public ConfigurationTemplateSpec[] getConfigSpecs();
	
	public boolean isCreateEnabled();

	public boolean isUpdateEnabled();

	public boolean isDeleteEnabled();

	public void delete(int selectionIndex);

	GIConfigPage getConfigPage(int index);

	boolean isSingleton();

	ConfigurationTemplateSpec[] getCreateSpecs();
	
	
	
}
