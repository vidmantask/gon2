package gon_client_management.model;

import gon_client_management.model.ext.GIConfigColumn;

public interface GIModelConfigColumn extends GIConfigColumn {
	
	public boolean isHidden();

	public boolean isId();

	
}
