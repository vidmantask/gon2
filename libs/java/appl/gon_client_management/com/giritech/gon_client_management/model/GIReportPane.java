package gon_client_management.model;

import java.util.List;
//import java.util.Observer;

/**
 *
 */
public interface GIReportPane {
	
	/**
	 * 
	 * @return a list of available reports
	 */
	public List<GIModelReport> getReports();
	
	/**
	 * Get a headline for the view.
	 * 
	 * @return A string that can be used as a headline for the view.
	 */
	public abstract String getViewHeadline();
	
	/**
	 * Each pane has a header which can be used fx. for
	 * sorting the content of the pane. This header should 
	 * have a label.
	 * 
	 * @return A string label describing the column.
	 */
	public abstract String getColumnLabel();
	
	//public int getElementCount();

	//public boolean hasFetchedAll();

	//public int getMaxCount();
	
	//public void addObserver(Observer o);

	//public boolean ready();

	String getAdmin_ws_url();
	String getSessionID();

}
