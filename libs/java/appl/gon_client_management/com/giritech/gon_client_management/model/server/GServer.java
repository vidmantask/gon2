package gon_client_management.model.server;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.CommonUtils.Pair;
import gon_client_management.model.GIDeployment;
import gon_client_management.model.GIModelElement;
import gon_client_management.model.GIModelReport;
import gon_client_management.model.GIModelRule;
import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GConfigTemplateUtil;
import gon_client_management.model.ext.GIConfigRowValue;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.view.preferences.PreferenceConstants;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.security.auth.login.LoginException;

import org.apache.axiom.soap.SOAPFaultDetail;
import org.apache.axis2.AxisFault;

import admin_ws.AddElementToMenu;
import admin_ws.AddItemToMenu;
import admin_ws.Admin_wsServiceStub;
import admin_ws.CreateConfigTemplateRowResponse;
import admin_ws.DeployGenerateKeyPairResponse;
import admin_ws.DeployGetKnownSecretClientResponse;
import admin_ws.FaultServer;
import admin_ws.FaultServerOperationNotAllowed;
import admin_ws.GOnServiceGetList;
import admin_ws.GOnServiceGetListResponse;
import admin_ws.GOnServiceRestart;
import admin_ws.GOnServiceRestartResponse;
import admin_ws.GOnServiceStop;
import admin_ws.GOnServiceStopResponse;
import admin_ws.GatewaySessionClose;
import admin_ws.GatewaySessionCloseResponse;
import admin_ws.GatewaySessionRecalculateMenu;
import admin_ws.GatewaySessionRecalculateMenuResponse;
import admin_ws.GetAccessDefintion;
import admin_ws.GetAccessDefintionResponse;
import admin_ws.GetAuthRestrictionElements;
import admin_ws.GetAuthRestrictionElementsResponse;
import admin_ws.GetBasicEntityTypes;
import admin_ws.GetConfigSpecResponse;
import admin_ws.GetConfigTemplateResponse;
import admin_ws.GetElementConfigTemplateResponse;
import admin_ws.GetElementSeachCategories;
import admin_ws.GetElementsStop;
import admin_ws.GetElementsStopResponse;
import admin_ws.GetEntityTypeForPluginType;
import admin_ws.GetGatewaySessions;
import admin_ws.GetGatewaySessionsNext;
import admin_ws.GetGatewaySessionsNextResponse;
import admin_ws.GetGatewaySessionsResponse;
import admin_ws.GetLicenseInfo;
import admin_ws.GetLicenseInfoResponse;
import admin_ws.GetMenu;
import admin_ws.GetMenuItems;
import admin_ws.GetRestrictedLaunchTypeCategories;
import admin_ws.GetRestrictedLaunchTypeCategoriesResponse;
import admin_ws.GetRulesForElement;
import admin_ws.GetRulesForElementResponse;
import admin_ws.GetTokenInfo;
import admin_ws.GetTokenInfoResponse;
import admin_ws.MoveItemToMenu;
import admin_ws.Ping;
import admin_ws.PingResponse;
import admin_ws.RemoveItemFromMenu;
import admin_ws.RemoveUserFromLaunchTypeCategory;
import admin_ws.RemoveUserFromLaunchTypeCategoryResponse;
import admin_ws.SetLicense;
import admin_ws.SetLicenseResponse;
import admin_ws.UpdateAuthRestrictionElements;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.AccessDef;
import com.giritech.admin_ws.AddElementToMenuRequestType;
import com.giritech.admin_ws.AddItemToMenuRequestType;
import com.giritech.admin_ws.ConfigElementType;
import com.giritech.admin_ws.ElementSpec;
import com.giritech.admin_ws.ElementType;
import com.giritech.admin_ws.FaultServerElement;
import com.giritech.admin_ws.FaultServerOperationNotAllowedElement;
import com.giritech.admin_ws.GOnServiceGetListRequestType;
import com.giritech.admin_ws.GOnServiceRestartRequestType;
import com.giritech.admin_ws.GOnServiceStopRequestType;
import com.giritech.admin_ws.GOnServiceType;
import com.giritech.admin_ws.GOnSessionType;
import com.giritech.admin_ws.GatewaySessionCloseRequestType;
import com.giritech.admin_ws.GatewaySessionRecalculateMenuRequestType;
import com.giritech.admin_ws.GetAccessDefintionRequestType;
import com.giritech.admin_ws.GetAuthRestrictionElementsRequestType;
import com.giritech.admin_ws.GetBasicEntityTypesRequestType;
import com.giritech.admin_ws.GetElementsStopRequestType;
import com.giritech.admin_ws.GetEntityTypeForPluginTypeRequestType;
import com.giritech.admin_ws.GetGatewaySessionsNextRequestType;
import com.giritech.admin_ws.GetGatewaySessionsRequestType;
import com.giritech.admin_ws.GetLicenseInfoRequestType;
import com.giritech.admin_ws.GetMenuItemsRequestType;
import com.giritech.admin_ws.GetMenuRequestType;
import com.giritech.admin_ws.GetRestrictedLaunchTypeCategoriesRequestType;
import com.giritech.admin_ws.GetRulesForElementRequestType;
import com.giritech.admin_ws.GetTokenInfoRequestType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;
import com.giritech.admin_ws.MenuItemType;
import com.giritech.admin_ws.MenuType;
import com.giritech.admin_ws.MoveItemToMenuRequestType;
import com.giritech.admin_ws.PingRequestType;
import com.giritech.admin_ws.RemoveItemFromMenuRequestType;
import com.giritech.admin_ws.RemoveUserFromLaunchTypeCategoryRequestType;
import com.giritech.admin_ws.RuleSpecType;
import com.giritech.admin_ws.RuleType;
import com.giritech.admin_ws.RuleType1;
import com.giritech.admin_ws.SetLicenseRequestType;
import com.giritech.admin_ws.StingListType;
import com.giritech.admin_ws.StringMap;
import com.giritech.admin_ws.UpdateAuthRestrictionElementsRequestType;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.admin_ws.types_config_template.Value_selection_choice;

class GServer implements GIServer {

	private static final int SERVER_GENERAL_ERROR = -1;
	private static final int SERVER_SESSION_LOST = 0;
	private static final int SERVER_ACCESS_DENIED = 1;

	private static final int RECONNECT_COUNT = 1;
	private static volatile GServer server_instance = null;
	String sessionID;
	volatile Admin_wsServiceStub service;
	private String admin_ws_url;
	private long ws_timeout_min;
	private String username = null;
	private String password = null;

	private boolean disconnected = false;
	public boolean gatewaySessionsUpdateEnabled = false;
	

	
	private GServer(String admin_ws_url, long ws_timeout_min) {
		this.admin_ws_url = admin_ws_url;
		this.ws_timeout_min = ws_timeout_min;
		
		connect();
	}
	private void reconnect() {
		reconnect(0);
	}
	
	private void reconnect(int reconnectCount) {
		if (service==null || reconnectCount >= RECONNECT_COUNT) { 
			disconnected = true;
			return;
		}
		
		Activator.getLogger().logInfo("Trying server reconnect");
		String oldSessionId = this.sessionID;
		try {
			openSession(service, oldSessionId);
		} catch (RemoteException e1) {
			reconnect(reconnectCount+1);
			return;
		} catch (FaultServer e1) {
			Activator.getLogger().logException(e1);
			disconnected = true;
			return;
		}
		if (oldSessionId.equals(sessionID)) {
			Activator.getLogger().logInfo("Connecting to same session");
		}
		else {
			Activator.getLogger().logInfo("Starting new session");
			if (username!=null && password!=null)
				try {
					getServer().Login(username, password);
				} catch (LoginException e) {
					// Ignore
					Activator.getLogger().logInfo("Login to new session failed : " + e.getMessage());
				}
		}
		disconnected = false;
		
	}	
	
	private void connect() {
		
    	admin_ws.Admin_wsServiceStub admin_ws_service = null;
		try {
			admin_ws_service = new Admin_wsServiceStub(admin_ws_url);
		} catch (AxisFault e) {
			throw new RuntimeException(e);
		}

        /*
         * This is needed because the py-soap server need content length of requests,
         * and this is enabled if CHUNKED is disabled.
         */
        admin_ws_service._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, "false");
        
        /*
         * Increase timeout
         * 
         */
        long timeout_ms = ws_timeout_min * 60 * 1000; 
        admin_ws_service._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout_ms);
        
        try {
        	openSession(admin_ws_service, null);
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
        
        this.service = admin_ws_service;
        
        Activator.getLogger().logInfo("Connected to server");
        
	}

	private void openSession(admin_ws.Admin_wsServiceStub admin_ws_service, String oldSessionId) throws RemoteException, FaultServer {
		admin_ws.OpenSession OpenSession_request = new admin_ws.OpenSession();
        com.giritech.admin_ws.OpenSessionRequestType OpenSession_request_content = new com.giritech.admin_ws.OpenSessionRequestType();
        if (oldSessionId!=null)
        	OpenSession_request_content.setOld_session_id(oldSessionId);
        OpenSession_request.setContent(OpenSession_request_content);
        
        admin_ws.OpenSessionResponse response;
		response = admin_ws_service.OpenSession(OpenSession_request);
        this.sessionID = response.getContent().getSession_id();
	}
	
	public String getAdmin_ws_url() {
		return admin_ws_url;
	}
	
	public String getSessionID() {
		return this.sessionID;
	}

	
	@SuppressWarnings("unused")
	private class ServerCall<Request, Response> {
		
		private final Method method;
		private final Request request;
		private int callCount;
		
		ServerCall(String methodName, Request request)  {
			try {
				this.method = GServer.this.service.getClass().getMethod(methodName, request.getClass());
			} catch (NoSuchMethodException e) {
				throw new RuntimeException(e);
			}
			this.request = request;
			callCount = 0;
		}
		
		Response callWithReconnect() {
			try {
				return callWithReconnect1();
			} catch (GOperationNotAllowedException e) {
				throw new RuntimeException(e);
			}
			
		}

		@SuppressWarnings("unchecked")
		Response callWithReconnect1() throws GOperationNotAllowedException {
			try {
				return (Response) method.invoke(GServer.this.service, request);
					
			} catch (InvocationTargetException e) {
				Throwable cause = e.getCause();
				if (cause!=null) {
					if (cause instanceof FaultServer) {
						FaultServer ex = (FaultServer) cause;
						boolean sessionLost = ex.getFaultMessage().getFaultServerElement().getError_message().contains("Server sesssion lost or expired");
						if (sessionLost) {
							callCount++;
							if (callCount < 2) {
								try {
									Method getContentMethod = request.getClass().getMethod("getContent");
									Object content = getContentMethod.invoke(request);
									Method setSessionMethod = content.getClass().getMethod("setSession_id", String.class);
									connect();
									setSessionMethod.invoke(content, GServer.this.sessionID);
									return callWithReconnect1();
								} catch (Exception e1) {
								}
							}
						}
						handleFaultServer((FaultServer) cause);
					}
					if (cause instanceof FaultServerOperationNotAllowed) {
						handleFaultServerOperationNotAllowed((FaultServerOperationNotAllowed) cause);
					}
					throw new RuntimeException(cause);
				}
				else
					throw new RuntimeException(e);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
		}

		
	}
	
	private class GServerRuleSpec implements GIRuleSpec {
		
		private final String title;
		private final String titleLong;
		
		private final List<String> uniqueelementTypes = new ArrayList<String>();
		private final List<List<String>> mandatoryElementTypes = new ArrayList<List<String>>();

		private final Map<String, List<String>> entityTypeMap = new HashMap<String, List<String>>();
		
		private final GIRule templateRule;
		private final GIRule headerRule;
		private boolean deleteEnabled;
		private boolean createEnabled;
		private boolean editEnabled;

		GServerRuleSpec(RuleSpecType spec) {
			this.title = spec.getRule_title();
			this.titleLong = spec.getRule_title_long();
			
			this.templateRule = new GServerRuleElement(spec.getTemplate_rule());
			this.headerRule = new GServerRuleElement(spec.getHeader_rule());
			
			this.editEnabled = spec.getEdit_enabled();
			this.createEnabled = spec.getCreate_enabled();
			this.deleteEnabled = spec.getDelete_enabled();
			
			String[] uniqueElements = spec.getUnique_elements();
			if (uniqueElements!=null)
				for(int i=0; i<uniqueElements.length; i++)
					uniqueelementTypes.add(uniqueElements[i]);
			
			StingListType[] mandatoryElements = spec.getMandatory_elements();
			if (mandatoryElements!=null)
				for(int i=0; i<mandatoryElements.length; i++) {
					ArrayList<String> elementList = new ArrayList<String>();
					String[] elements = mandatoryElements[i].getElements();
					for(int j=0; j<elements.length; j++)
						elementList.add(elements[j]);
					this.mandatoryElementTypes.add(elementList);
					
				}
			
			StringMap[] entityTypeMaps = spec.getEntity_type_maps();
			if (entityTypeMaps != null) {
				for (StringMap stringMap : entityTypeMaps) {
					List<String> values = CommonUtils.convertToList(stringMap.getValues());
					entityTypeMap.put(stringMap.getKey(), values);
					
				}
			}
			
		}
		
		public GIRule getHeaderRule() {
			return headerRule;
		}

		public List<List<String>> getMandatoryElementTypes() {
			return mandatoryElementTypes;
		}

		public GIRule getTemplateRule() {
			return templateRule;
		}

		public String getTitle() {
			return title;
		}

		public String getTitleLong() {
			return titleLong;
		}

		public List<String> getUniqueelementTypes() {
			return uniqueelementTypes;
		}

		@Override
		public Map<String, List<String>> getEntityTypeMap() {
			return entityTypeMap;
		}

		@Override
		public boolean isCreateEnabled() {
			return createEnabled;
		}

		@Override
		public boolean isDeleteEnabled() {
			return deleteEnabled;
		}

		@Override
		public boolean isEditEnabled() {
			return editEnabled;
		}
		
	}
	
	void handleFaultServer(FaultServer e) {
		final FaultServerElement faultMessage = e.getFaultMessage();
		String errorMessage;
		int errorCode = SERVER_GENERAL_ERROR;
		if (faultMessage!=null) {
			errorMessage = faultMessage.getFaultServerElement().getError_message();
			errorCode = faultMessage.getFaultServerElement().getError_code().intValue();
		}
		else  
			errorMessage = e.getMessage();
		
		if (errorCode == SERVER_ACCESS_DENIED) {
			handleFaultServerOperationNotAuthorized(errorMessage);
		}
		Activator.getLogger().logError(errorMessage);
		// Check to see if server session has timed out
		if (errorCode==SERVER_SESSION_LOST) {
			reconnect(0);
			if (!disconnected)
				throw new RuntimeException("Error connecting to server:\n" + errorMessage + "\nConnection to server has been restored.\nPlease try again");
			else
				throw new RuntimeException(errorMessage);
		}
		else {
			throw new RuntimeException(errorMessage);
			
		}
	}
	
	
	void handleRemoteException(RemoteException e) {
		if (e instanceof AxisFault) {
			AxisFault e1 = (AxisFault) e;
			SOAPFaultDetail faultDetailElement = e1.getFaultDetailElement();
			if (faultDetailElement!=null) {
				String text3 = faultDetailElement.toString();
				Activator.getLogger().logError(text3);
			}
		}
		Activator.getLogger().logException(e);
		reconnect(0);
		if (disconnected) {
			throw new RuntimeException("Error connecting to server\nUnable to restore connection\nSee log file for details");
		}
		else
			throw new RuntimeException("Error connecting to server\nThe connection has been restored\nPlease try again");
		
	}
	
	private void handleFaultServerOperationNotAllowed(FaultServerOperationNotAllowed e) throws GOperationNotAllowedException {
		final FaultServerOperationNotAllowedElement faultMessage = e.getFaultMessage();
		if (faultMessage!=null)
			throw new GOperationNotAllowedException(faultMessage.getFaultServerOperationNotAllowedElement().getError_message());
		else 
			throw new GOperationNotAllowedException(e);
	}
	
	private void handleFaultServerOperationNotAuthorized(String errorMessage) {
		throw new RuntimeException(errorMessage);
		
	}
	
	/**
	 * The default server string is "https://127.0.0.1:8080"
	 * 
	 * @return A new GIServer instance
	 */
	public synchronized static GIServer getServer() {
		if (server_instance == null) {
			String serverUrl = "https://" + 
			                   Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_HOST) +
			                   ":" +
			                   Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.G_MANAGEMENT_SERVER_PORT)
			                   ;
			server_instance = new GServer(serverUrl, 60);
		}
		try {
			boolean ping = server_instance.Ping();
			if (!ping)
				server_instance.reconnect();
		} catch(Throwable t) {
			// Ignore
		}
		if (server_instance.disconnected) {
			server_instance.reconnect(0);
			if (server_instance.disconnected) {
				throw new RuntimeException("Error: Disconnected from server");
			}
		}
		return server_instance;
	}

	/**
	 * The default server string is "https://127.0.0.1:8080"
	 * 
	 * @return A new GIServer instance
	 */
	synchronized static GIServer getTestServer() {
		if (server_instance == null) {
			server_instance = new GServer("https://127.0.0.1:8080", 5);
		}
		return server_instance;
	}
	
	interface GIFetchResult<E> {
		BigInteger getCount_max();
		List<E> getElements();
		String getNext_session_id();
		boolean getResult_truncated();
		
	}
	

	private abstract class ElementFetcher<E> implements GIRuleElementFetcher<E> {
		
		final String element_type;
		final boolean refresh;
		final String filter;
		String nextSessionID = null;
		boolean fetchedAll = false;
		int countMax = -1;
		boolean resultTruncated = false;
		


		ElementFetcher(String element_type, String filter, boolean refresh) {
			this.element_type = element_type;
			this.refresh = refresh;
			this.filter = filter;
		}


		/* (non-Javadoc)
		 * @see gon_client_management.model.server.GIRuleElementFetcher#getElements(int)
		 */
		public List<E> getElements(int count) {
			if (fetchedAll) {
				return null;
			}
			if (nextSessionID==null) {
				GIFetchResult<E> response = getFirstElements(count);
				
				BigInteger count_max = response.getCount_max();
				this.countMax  = count_max != null ? count_max.intValue() : -1;
				nextSessionID = response.getNext_session_id();
				if (nextSessionID == null) 
					this.fetchedAll = true;
				
				resultTruncated = response.getResult_truncated();
				
				List<E> resultList = response.getElements();
				return resultList;
			}
			else {
				GIFetchResult<E> responseNext = getNextElements(count);
				nextSessionID = responseNext.getNext_session_id();
				if (nextSessionID == null) 
					this.fetchedAll = true;

				resultTruncated = responseNext.getResult_truncated();
				
				List<E> resultList = responseNext.getElements();
				
				return resultList;
				
			}
		}
		
		public void stop() {
			if (nextSessionID != null) {
				try {
					GServer.this.stopFetchingElements(nextSessionID);
				} catch(Throwable t) {
					// Ignore
				}
			}
			
			
		}
		

		abstract protected GIFetchResult<E> getFirstElements(int count);
		abstract protected GIFetchResult<E> getNextElements(int count);


		/* (non-Javadoc)
		 * @see gon_client_management.model.server.GIRuleElementFetcher#getCountMax()
		 */
		public int getCountMax() {
			return countMax;
		}

		/* (non-Javadoc)
		 * @see gon_client_management.model.server.GIRuleElementFetcher#hasFetchedAll()
		 */
		public boolean hasFetchedAll() {
			return fetchedAll;
		}

		public boolean isResultTruncated() {
			return resultTruncated;
		}
		
	}
	

	private class RuleElementFetcher extends ElementFetcher<GIModelElement> {
		

		RuleElementFetcher(String element_type, String filter, boolean refresh) {
			super(element_type, filter, refresh);
		}



		@Override
		protected GIFetchResult<GIModelElement> getFirstElements(int count) {
	        admin_ws.GetModuleElementsFirst request = new admin_ws.GetModuleElementsFirst();
	        com.giritech.admin_ws.GetModuleElementsFirstRequestType requestContent = new com.giritech.admin_ws.GetModuleElementsFirstRequestType();
	        requestContent.setSession_id(GServer.this.sessionID);
	        requestContent.setElement_type(element_type);
	        requestContent.setRefresh_cache(refresh);
	        requestContent.setCount(BigInteger.valueOf(count));
	        requestContent.setSearch_filter(filter);
	        request.setContent(requestContent);
	        final admin_ws.GetModuleElementsFirstResponse response;
	        try {
	            synchronized(service) {
	            	response = GServer.this.service.GetModuleElementsFirst(request);
	            }
			} catch (RemoteException e) {
				handleRemoteException(e);
				throw new RuntimeException(e);
			} catch (FaultServer e) {
				handleFaultServer(e);
				throw new RuntimeException(e);
			}
			
			final List<GIModelElement> resultList = createClientElements(response.getContent().getElements());
			
			return new GIFetchResult<GIModelElement>() {

				@Override
				public BigInteger getCount_max() {
					return response.getContent().getCount_max();
				}

				@Override
				public List<GIModelElement> getElements() {
					return resultList;
				}

				@Override
				public String getNext_session_id() {
					return response.getContent().getNext_session_id();
				}

				@Override
				public boolean getResult_truncated() {
					return response.getContent().getResult_truncated();
				}
			};
			
		}





		@Override
		protected GIFetchResult<GIModelElement> getNextElements(int count) {
	        admin_ws.GetModuleElementsNext requestNext = new admin_ws.GetModuleElementsNext();
	        com.giritech.admin_ws.GetModuleElementsNextRequestType requestNextContent = new com.giritech.admin_ws.GetModuleElementsNextRequestType();
	        requestNextContent.setSession_id(GServer.this.sessionID);
	        requestNextContent.setElement_type(element_type);
	        requestNextContent.setNext_session_id(nextSessionID);
	        requestNextContent.setCount(BigInteger.valueOf(count));
	        requestNext.setContent(requestNextContent);
	        final admin_ws.GetModuleElementsNextResponse response;
	        try {
	            synchronized(service) {
	            	response = GServer.this.service.GetModuleElementsNext(requestNext);
	            }
			} catch (RemoteException e) {
				handleRemoteException(e);
				throw new RuntimeException(e);
			} catch (FaultServer e) {
				handleFaultServer(e);
				throw new RuntimeException(e);
			}
			final List<GIModelElement> resultList = createClientElements(response.getContent().getElements());
			
			return new GIFetchResult<GIModelElement>() {

				@Override
				public BigInteger getCount_max() {
					return response.getContent().getCount_max();
				}

				@Override
				public List<GIModelElement> getElements() {
					return resultList;
				}

				@Override
				public String getNext_session_id() {
					return response.getContent().getNext_session_id();
				}

				@Override
				public boolean getResult_truncated() {
					return response.getContent().getResult_truncated();
				}
			};
		}
		
	}


	private class RuleFetcher extends ElementFetcher<GIModelRule> {
		

		RuleFetcher(String rule_type, String filter) {
			super(rule_type, filter, false);
		}



		@Override
		protected GIFetchResult<GIModelRule> getFirstElements(int count) {
	        admin_ws.GetRulesFirst request = new admin_ws.GetRulesFirst();
	        com.giritech.admin_ws.GetRulesFirstRequestType requestContent = new com.giritech.admin_ws.GetRulesFirstRequestType();
	        requestContent.setSession_id(GServer.this.sessionID);
	        requestContent.setRule_type(element_type);
	        requestContent.setCount(BigInteger.valueOf(count));
	        requestContent.setSearch_filter(filter);
	        request.setContent(requestContent);
	        final admin_ws.GetRulesFirstResponse response;
	        try {
	            synchronized(service) {
	            	response = GServer.this.service.GetRulesFirst(request);
	            }
			} catch (RemoteException e) {
				handleRemoteException(e);
				throw new RuntimeException(e);
			} catch (FaultServer e) {
				handleFaultServer(e);
				throw new RuntimeException(e);
			}
			
			RuleType[] elements = response.getContent().getRules();
			if (elements == null)
				elements = new RuleType[0]; 
			
			final ArrayList<GIModelRule> resultList = new ArrayList<GIModelRule>();
			for (int i=0; i<elements.length; i++) {
				RuleType from = elements[i];
				GServerRuleElement to = new GServerRuleElement(from);
				resultList.add(to);
			}
			
			return new GIFetchResult<GIModelRule>() {

				@Override
				public BigInteger getCount_max() {
					return response.getContent().getCount_max();
				}

				@Override
				public List<GIModelRule> getElements() {
					return resultList;
				}

				@Override
				public String getNext_session_id() {
					return response.getContent().getNext_session_id();
				}

				@Override
				public boolean getResult_truncated() {
					return response.getContent().getResult_truncated();
				}
			};
			
		}


		@Override
		protected GIFetchResult<GIModelRule> getNextElements(int count) {
	        admin_ws.GetRulesNext requestNext = new admin_ws.GetRulesNext();
	        com.giritech.admin_ws.GetRulesNextRequestType requestNextContent = new com.giritech.admin_ws.GetRulesNextRequestType();
	        requestNextContent.setSession_id(GServer.this.sessionID);
	        requestNextContent.setRule_type(element_type);
	        requestNextContent.setNext_session_id(nextSessionID);
	        requestNextContent.setCount(BigInteger.valueOf(count));
	        requestNext.setContent(requestNextContent);
	        final admin_ws.GetRulesNextResponse response;
	        try {
	            synchronized(service) {
	        	response = GServer.this.service.GetRulesNext(requestNext);
	            }
			} catch (RemoteException e) {
				handleRemoteException(e);
				throw new RuntimeException(e);
			} catch (FaultServer e) {
				handleFaultServer(e);
				throw new RuntimeException(e);
			}
			RuleType[] elements = response.getContent().getRules();
			if (elements == null)
				elements = new RuleType[0]; 
			
			final ArrayList<GIModelRule> resultList = new ArrayList<GIModelRule>();
			for (int i=0; i<elements.length; i++) {
				RuleType from = elements[i];
				GServerRuleElement to = new GServerRuleElement(from);
				resultList.add(to);
			}
			
			return new GIFetchResult<GIModelRule>() {

				@Override
				public BigInteger getCount_max() {
					return response.getContent().getCount_max();
				}

				@Override
				public List<GIModelRule> getElements() {
					return resultList;
				}

				@Override
				public String getNext_session_id() {
					return response.getContent().getNext_session_id();
				}

				@Override
				public boolean getResult_truncated() {
					return response.getContent().getResult_truncated();
				}
			};
		}
		
	}

	/* (non-Javadoc)
	 * @see gon_client_management.model.server.GIServer#getRuleElementFetcher(java.lang.String)
	 */
	public GIRuleElementFetcher<GIModelElement> getRuleElementFetcher(String element_type, String filter, boolean refresh) {
		return new RuleElementFetcher(element_type, filter, refresh);
	}

	/* (non-Javadoc)
	 * @see gon_client_management.model.server.GIServer#getRuleElementFetcher(java.lang.String)
	 */
	public GIRuleElementFetcher<GIModelRule> getRuleFetcher(String element_type, String filter) {
		return new RuleFetcher(element_type, filter);
	}
	
	public Value_selection_choice[] getSearchCategories(String element_type) {
		GetElementSeachCategories request = new GetElementSeachCategories();
        com.giritech.admin_ws.GetElementSeachCategoriesRequestType requestContent = new com.giritech.admin_ws.GetElementSeachCategoriesRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement_type(element_type);
        request.setContent(requestContent);
        admin_ws.GetElementSeachCategoriesResponse response;
        try {
            synchronized(service) {
            	response = this.service.GetElementSeachCategories(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		Value_selection_choice[] searchCategories = response.getContent().getSearch_categories();
		if (searchCategories==null)
			searchCategories = new Value_selection_choice[0];
		return searchCategories;
		
	}
	
	

	/* (non-Javadoc)
	 * @see gon_client_management.model.server.GIServer#getRuleElementHeader(java.lang.String)
	 */
	public GIModelElement getRuleElementHeader(String element_type) {
		admin_ws.GetModuleHeaderElement request = new admin_ws.GetModuleHeaderElement();
        com.giritech.admin_ws.GetModuleHeaderElementRequestType requestContent = new com.giritech.admin_ws.GetModuleHeaderElementRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement_type(element_type);
        request.setContent(requestContent);
        admin_ws.GetModuleHeaderElementResponse response;
        try {
            synchronized(service) {
			response = this.service.GetModuleHeaderElement(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		ElementType element = response.getContent().getElement();
		
		return new GServerElement(element);
	}

	
	public List<GIModelElement> getRuleElements(String element_type, String filter, boolean refresh) {
        admin_ws.GetModuleElements request = new admin_ws.GetModuleElements();
        com.giritech.admin_ws.GetModuleElementsRequestType requestContent = new com.giritech.admin_ws.GetModuleElementsRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement_type(element_type);
        requestContent.setRefresh_cache(refresh);
        requestContent.setSearch_filter(filter);
        request.setContent(requestContent);
        admin_ws.GetModuleElementsResponse response;
        try {
            synchronized(service) {
			response = this.service.GetModuleElements(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return createClientElements(response.getContent().getElements());
	}
	
	private List<GIModelElement> createClientElements(ElementType[] elements) {
		
		ArrayList<GIModelElement> resultList = new ArrayList<GIModelElement>();
		if (elements != null) {
			for (int i=0; i<elements.length; i++) {
				ElementType from = elements[i];
				GServerElement to = new GServerElement(from);
				resultList.add(to);
				
			}
		}
		return resultList;
	}
	
	@Override
	public List<GIModelElement> getSpecificElements(Map<String, Set<String>> elementMap) {
        admin_ws.GetSpecificElements request = new admin_ws.GetSpecificElements();
        com.giritech.admin_ws.GetSpecificElementsRequestType requestContent = new com.giritech.admin_ws.GetSpecificElementsRequestType();
        requestContent.setSession_id(this.sessionID);

//        Set<Entry<String, Set<String>>> entrySet = elementMap.entrySet();
////        ElementSpec[] elementSpecs = new ElementSpec[0];
//        List<ElementSpec> elementSpecs = new ArrayList<ElementSpec>();
//        for (Entry<String, Set<String>> entry : entrySet) {
//        	final String entityType = entry.getKey();
//        	for (String elementId : entry.getValue()) {
//        		ElementSpec elementSpec = new ElementSpec();
//        		elementSpec.setEntity_type(entityType);
//        		elementSpec.setId(elementId);
//        		elementSpecs.add(elementSpec);
//			}
//		}
//        requestContent.setElement_specs(elementSpecs.toArray(new ElementSpec[0]));

        ElementSpec[] elementSpecs = new ElementSpec[elementMap.size()];
        Set<Entry<String, Set<String>>> entrySet = elementMap.entrySet();
        int count = 0;
	    for (Entry<String, Set<String>> entry : entrySet) {
	    	ElementSpec elementSpec = new ElementSpec();
	    	elementSpec.setEntity_type(entry.getKey());
	    	elementSpec.setIds(entry.getValue().toArray(new String[0]));
	    	elementSpecs[count++] = elementSpec;
		}
	    requestContent.setElement_specs(elementSpecs);
        request.setContent(requestContent);
        admin_ws.GetSpecificElementsResponse response;
        try {
            synchronized(service) {
			response = this.service.GetSpecificElements(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		ConfigElementType[] elements = response.getContent().getElements();
		if (elements == null)
			elements = new ConfigElementType[0]; 
		
		ArrayList<GIModelElement> resultList = new ArrayList<GIModelElement>();
		for (int i=0; i<elements.length; i++) {
			ElementType from = elements[i];
			GServerElement to = new GServerElement(from);
			resultList.add(to);
			
		}
		
		return resultList;
	}
	

	public GIRuleSpec getRuleSpec(String className) {
        com.giritech.admin_ws.GetRuleSpecRequestType requestContent = new com.giritech.admin_ws.GetRuleSpecRequestType();
        admin_ws.GetRuleSpec request = new admin_ws.GetRuleSpec();
        requestContent.setSession_id(this.sessionID);
        requestContent.setClass_name(className);
        request.setContent(requestContent);
        admin_ws.GetRuleSpecResponse response;
        try {
            synchronized(service) {
			response = this.service.GetRuleSpec(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		RuleSpecType rule_spec = response.getContent().getRule_spec();
		return new GServerRuleSpec(rule_spec);
		
	}
	
	public List<GIModelRule> getRules(String class_name, boolean refresh) {
        com.giritech.admin_ws.GetRulesRequestType requestContent = new com.giritech.admin_ws.GetRulesRequestType();
        admin_ws.GetRules request = new admin_ws.GetRules();
        requestContent.setSession_id(this.sessionID);
        requestContent.setClass_name(class_name);
        requestContent.setRefresh_cache(refresh);
        request.setContent(requestContent);
        admin_ws.GetRulesResponse response;
        try {
            synchronized(service) {
			response = this.service.GetRules(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		RuleType[] rules = response.getContent().getRules();
		if (rules==null)
			rules = new RuleType[0];
		ArrayList<GIModelRule> resultList = new ArrayList<GIModelRule>();
		for(int i=0; i<rules.length; i++)
			resultList.add(new GServerRuleElement(rules[i]));
		return resultList;
		
	}
	
	public GIModelRule addRule(String class_name, GIRule rule) throws GOperationNotAllowedException {
		com.giritech.admin_ws.CreateRuleRequestType requestContent = new com.giritech.admin_ws.CreateRuleRequestType(); 
        admin_ws.CreateRule request = new admin_ws.CreateRule();
        requestContent.setSession_id(this.sessionID);
        RuleType server_rule = createServerRule(rule);
        requestContent.setRule(server_rule);
        requestContent.setClass_name(class_name);
        request.setContent(requestContent);
        admin_ws.CreateRuleResponse response = null;
        try {
            synchronized(service) {
			response = this.service.CreateRule(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		RuleType newRule = response.getContent().getRule();
		
		return new GServerRuleElement(newRule);
	}
	
	public void deleteRule(String class_name, GIModelRule rule) throws GOperationNotAllowedException {
		com.giritech.admin_ws.DeleteRuleRequestType requestContent = new com.giritech.admin_ws.DeleteRuleRequestType(); 
        admin_ws.DeleteRule request = new admin_ws.DeleteRule();
        requestContent.setSession_id(this.sessionID);
        RuleType server_rule = createServerRule(rule);
        requestContent.setRule(server_rule);
        requestContent.setClass_name(class_name);
        request.setContent(requestContent);
        try {
            synchronized(service) {
			this.service.DeleteRule(request);
            }
        } catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		
	}
	
	public GIModelRule saveRule(String class_name, GIModelRule oldRule, GIModelRule newRule) throws GOperationNotAllowedException {
		com.giritech.admin_ws.UpdateRuleRequestType requestContent = new com.giritech.admin_ws.UpdateRuleRequestType(); 
        admin_ws.UpdateRule request = new admin_ws.UpdateRule();
        requestContent.setSession_id(this.sessionID);
        RuleType old_server_rule = createServerRule(oldRule);
        requestContent.setOld_rule(old_server_rule);
        RuleType new_server_rule = createServerRule(newRule);
        requestContent.setNew_rule(new_server_rule);
        requestContent.setClass_name(class_name);
        request.setContent(requestContent);

        admin_ws.UpdateRuleResponse response = null;
        try {
            synchronized(service) {
			response = this.service.UpdateRule(request);
            }
        } catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		RuleType updatedRule = response.getContent().getRule();
		
		return new GServerRuleElement(updatedRule);
	}

	private RuleType createServerRule(GIRule rule) {
		RuleType server_rule = new RuleType();
		if (rule instanceof GIModelRule)
			server_rule.setId(BigInteger.valueOf(((GIModelRule) rule).getId()));
		else
			server_rule.setId(BigInteger.valueOf(-1));
        server_rule.setResult_element(createElementType(rule.getResultElement()));
        server_rule.setCondition_elements(createElementTypes(rule.getElements()));
        server_rule.setActive(rule.isActive());
		return server_rule;
	}
	

	private ElementType[] createElementTypes(List<GIElement> elementList) {
		ElementType[] array = new ElementType[elementList.size()];
		for (int i=0; i<elementList.size(); i++) 
			array[i] = createElementType(elementList.get(i));
		return array;
	}

	private ElementType createElementType(GIElement fromElement) {
		ElementType element = new ElementType();
		element.setEntity_type(fromElement.getEntityType());
		element.setInfo(fromElement.getInfo());
		element.setLabel(fromElement.getLabel());
		if (fromElement instanceof GIModelElement) {
			GIModelElement modelElement = (GIModelElement) fromElement;
			element.setId(modelElement.getId());
			element.setElement_type(modelElement.getElementType());
		}
		else {
			element.setId("");
		}
		
		return element;
	}

	public List<GIModelReport> getReports() {
		com.giritech.admin_ws.GetReportsRequestType requestContent = new com.giritech.admin_ws.GetReportsRequestType();
		admin_ws.GetReports request = new admin_ws.GetReports();
		requestContent.setSession_id(this.sessionID);
		request.setContent(requestContent);
		
		admin_ws.GetReportsResponse response = null;
		try {
            synchronized(service) {
			response = this.service.GetReports(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}

		com.giritech.admin_ws.ReportRefType[] reports = response.getContent().getReports();
		if (reports == null)
			reports = new com.giritech.admin_ws.ReportRefType[0];

		ArrayList<GIModelReport> resultList = new ArrayList<GIModelReport>();
		for (int i = 0; i < reports.length; i++) {
			GServerReport to = new GServerReport(reports[i]);
			resultList.add(to);
		}
		return resultList;
	}

	public String getReportSpecificationFromFilename(String specification_filename) {
		return getReportSpecification("", "", specification_filename);
	}

	public String getReportSpecification(String module_name, String report_id) {
		return getReportSpecification(module_name, report_id, "");
	}

	private String getReportSpecification(String module_name, String report_id, String specification_filename) {
		com.giritech.admin_ws.GetReportSpecRequestType requestContent = new com.giritech.admin_ws.GetReportSpecRequestType();
		admin_ws.GetReportSpec request = new admin_ws.GetReportSpec();
		requestContent.setSession_id(this.sessionID);
		requestContent.setModule_name(module_name);
		requestContent.setReport_id(report_id);
		requestContent.setSpecification_filename(specification_filename);
		request.setContent(requestContent);
		
		admin_ws.GetReportSpecResponse response = null;
		try {
            synchronized(service) {
			response = this.service.GetReportSpec(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getReport_spec();
	}
	
	

	public boolean deleteConfigRow(GIElement element) throws GOperationNotAllowedException {
		com.giritech.admin_ws.DeleteConfigElementRowRequestType requestContent = new com.giritech.admin_ws.DeleteConfigElementRowRequestType();
		admin_ws.DeleteConfigElementRow request = new admin_ws.DeleteConfigElementRow();
		requestContent.setSession_id(this.sessionID);
		requestContent.setEntity_type(element.getEntityType());
		requestContent.setElement_id(((GIModelElement) element).getId());
		request.setContent(requestContent);

		admin_ws.DeleteConfigElementRowResponse response = null;
		try {
            synchronized(service) {
			response = this.service.DeleteConfigElementRow(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		return response.getContent().getRc();
	}

	public GetConfigSpecResponse getConfigSpec(String name) {
		com.giritech.admin_ws.GetConfigSpecRequestType requestContent = new com.giritech.admin_ws.GetConfigSpecRequestType();
		admin_ws.GetConfigSpec request = new admin_ws.GetConfigSpec();
		requestContent.setSession_id(this.sessionID);
		requestContent.setEntity_type(name);
		request.setContent(requestContent);

		admin_ws.GetConfigSpecResponse response = null;
		try {
            synchronized(service) {
			response = this.service.GetConfigSpec(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		

		return response;
		
		
	}
	

	
	public String deployGetKnownSecretClient() {
		com.giritech.admin_ws.DeployGetKnownSecretClientRequestType requestContent = new com.giritech.admin_ws.DeployGetKnownSecretClientRequestType();
		admin_ws.DeployGetKnownSecretClient request = new admin_ws.DeployGetKnownSecretClient();
		requestContent.setSession_id(this.sessionID);
		request.setContent(requestContent);
		final DeployGetKnownSecretClientResponse response; 
		try {
            synchronized(service) {
			response = this.service.DeployGetKnownSecretClient(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getKnownsecret_client();
		
	}
	
	public GIDeployment.KeyPair deployGenerateKeyPair() {
		com.giritech.admin_ws.DeployGenerateKeyPairRequestType requestContent = new com.giritech.admin_ws.DeployGenerateKeyPairRequestType();
		admin_ws.DeployGenerateKeyPair request = new admin_ws.DeployGenerateKeyPair();
		requestContent.setSession_id(this.sessionID);
		request.setContent(requestContent);
		final DeployGenerateKeyPairResponse response; 
		try {
            synchronized(service) {
            	response = this.service.DeployGenerateKeyPair(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		final String privateKey = response.getContent().getPrivate_key();
		final String publicKey = response.getContent().getPublic_key();
		return new GIDeployment.KeyPair() {

			public String getPrivateKey() {
				return privateKey;
			}

			public String getPublicKey() {
				return publicKey;
			}
			
		};
		
	}

	public ConfigurationTemplate getConfigurationTemplate(String entityType, String templateName) {
		com.giritech.admin_ws.GetConfigTemplateRequestType requestContent = new com.giritech.admin_ws.GetConfigTemplateRequestType();
		admin_ws.GetConfigTemplate request = new admin_ws.GetConfigTemplate();
		requestContent.setSession_id(this.sessionID);
		requestContent.setEntity_type(entityType);
		requestContent.setTemplate_name(templateName);
		request.setContent(requestContent);
		final GetConfigTemplateResponse response; 
		try {
            synchronized(service) {
			response = this.service.GetConfigTemplate(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_template();
	}

	public ConfigurationTemplate getConfigurationTemplate(GIElement element, boolean bypassCustomTemplate, boolean refresh) {
		com.giritech.admin_ws.GetElementConfigTemplateRequestType requestContent = new com.giritech.admin_ws.GetElementConfigTemplateRequestType();
		admin_ws.GetElementConfigTemplate request = new admin_ws.GetElementConfigTemplate();
		requestContent.setSession_id(this.sessionID);
		requestContent.setElement(createElementType(element));
		requestContent.setRefresh_cache(refresh);
		requestContent.setBypass_custom_template(bypassCustomTemplate);
		request.setContent(requestContent);
		final GetElementConfigTemplateResponse response; 
		try {
            synchronized(service) {
			response = this.service.GetElementConfigTemplate(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getConfig_template();
	}
	
	public CreateConfigTemplateRowResponse createConfigRow(String entityType, ConfigurationTemplate template) throws GOperationNotAllowedException {
		com.giritech.admin_ws.CreateConfigTemplateRowRequestType requestContent = new com.giritech.admin_ws.CreateConfigTemplateRowRequestType();
		admin_ws.CreateConfigTemplateRow request = new admin_ws.CreateConfigTemplateRow();
		requestContent.setSession_id(this.sessionID);
		requestContent.setEntity_type(entityType);
		requestContent.setTemplate(template);
		request.setContent(requestContent);
		
		admin_ws.CreateConfigTemplateRowResponse response = null;
		try {
            synchronized(service) {
			response = this.service.CreateConfigTemplateRow(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		return response;
		
	}

	public ConfigurationTemplate updateConfigRow(GIElement element, ConfigurationTemplate template) throws GOperationNotAllowedException {
		com.giritech.admin_ws.UpdateConfigTemplateRowRequestType requestContent = new com.giritech.admin_ws.UpdateConfigTemplateRowRequestType();
		admin_ws.UpdateConfigTemplateRow request = new admin_ws.UpdateConfigTemplateRow();
		requestContent.setSession_id(this.sessionID);
		requestContent.setEntity_type(element.getEntityType());
		requestContent.setElement_id(((GIModelElement) element).getId());
		requestContent.setTemplate(template);
		request.setContent(requestContent);
		
		admin_ws.UpdateConfigTemplateRowResponse response = null;
		try {
            synchronized(service) {
			response = this.service.UpdateConfigTemplateRow(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		return response.getContent().getTemplate();
		
	}

	public Value_selection portScan(String name, List<String> portNumbers){
		com.giritech.admin_ws.PortScanRequestType requestContent = new com.giritech.admin_ws.PortScanRequestType();
		admin_ws.PortScan request = new admin_ws.PortScan();
		requestContent.setSession_id(this.sessionID);
		if (name!=null)
			requestContent.setName(name);
		String [] tmp = new String[portNumbers.size()];
		int i = 0;
		for (String string : portNumbers) {
			tmp[i] = string;
			i++;
		}
		requestContent.setPort_numbers(tmp);
		request.setContent(requestContent);
		
		admin_ws.PortScanResponse response = null;
		try {
            synchronized(service) {
			response = this.service.PortScan(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getSelection_values();
		
	}
	
	
	public MenuType getMenu() {
		GetMenuRequestType requestContent = new GetMenuRequestType();
		GetMenu request = new GetMenu();
		requestContent.setSession_id(sessionID);

		request.setContent(requestContent);

		admin_ws.GetMenuResponse response = null;
		try {
            synchronized(service) {
			response = this.service.GetMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getMenu();
		
	}

	public MenuItemType[] getMenuItems() {
		GetMenuItemsRequestType requestContent = new GetMenuItemsRequestType();
		GetMenuItems request = new GetMenuItems();
		requestContent.setSession_id(sessionID);

		request.setContent(requestContent);

		admin_ws.GetMenuItemsResponse response = null;
		try {
            synchronized(service) {
			response = this.service.GetMenuItems(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getItems();
		
	}
	
	
	public MenuType removeItemFromMenu(MenuItemType item, MenuItemType fromItem) {
		RemoveItemFromMenuRequestType requestContent = new RemoveItemFromMenuRequestType();
		RemoveItemFromMenu request = new RemoveItemFromMenu();
		requestContent.setSession_id(sessionID);
		requestContent.setItem(item);
		requestContent.setMenu_item(fromItem);

		request.setContent(requestContent);

		admin_ws.RemoveItemFromMenuResponse response = null;
		try {
            synchronized(service) {
			response = this.service.RemoveItemFromMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getMenu();
		
	}

	public MenuType moveItemToMenu(MenuItemType item, MenuItemType fromItem, MenuItemType toItem) {
		MoveItemToMenuRequestType requestContent = new MoveItemToMenuRequestType();
		MoveItemToMenu request = new MoveItemToMenu();
		requestContent.setSession_id(sessionID);
		requestContent.setItem(item);
		requestContent.setFrom_menu_item(fromItem);
		requestContent.setTo_menu_item(toItem);

		request.setContent(requestContent);

		admin_ws.MoveItemToMenuResponse response = null;
		try {
            synchronized(service) {
			response = this.service.MoveItemToMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getMenu();
		
	}

	public MenuType addItemToMenu(MenuItemType item, MenuItemType fromItem) {
		AddItemToMenuRequestType requestContent = new AddItemToMenuRequestType();
		AddItemToMenu request = new AddItemToMenu();
		requestContent.setSession_id(sessionID);
		requestContent.setItem(item);
		requestContent.setMenu_item(fromItem);

		request.setContent(requestContent);

		admin_ws.AddItemToMenuResponse response = null;
		try {
            synchronized(service) {
			response = this.service.AddItemToMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getMenu();
		
	}

	public Pair<MenuItemType, MenuType> addElementToMenu(GIElement element, MenuItemType fromItem) {
		AddElementToMenuRequestType requestContent = new AddElementToMenuRequestType();
		AddElementToMenu request = new AddElementToMenu();
		requestContent.setSession_id(sessionID);
		requestContent.setElement(createElementType(element));
		requestContent.setMenu_item(fromItem);

		request.setContent(requestContent);

		admin_ws.AddElementToMenuResponse response = null;
		try {
            synchronized(service) {
			response = this.service.AddElementToMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return new Pair<MenuItemType, MenuType>(response.getContent().getNew_item(), response.getContent().getMenu());
		
	}

	@Override
	public String getEntityTypeForPluginType(String pluginName, String elementType) {
		GetEntityTypeForPluginTypeRequestType requestContent = new GetEntityTypeForPluginTypeRequestType();
		GetEntityTypeForPluginType request = new GetEntityTypeForPluginType();
		requestContent.setSession_id(sessionID);
		requestContent.setPlugin_name(pluginName);
//		if (elementType!=null)
			requestContent.setElement_type(elementType);

		request.setContent(requestContent);
		admin_ws.GetEntityTypeForPluginTypeResponse response = null;
		try {
            synchronized(service) {
            	response = this.service.GetEntityTypeForPluginType(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getEntity_type();
	}
	
	@Override
	public List<String> getBasicEntityTypes(String entityType) {
		GetBasicEntityTypesRequestType requestContent = new GetBasicEntityTypesRequestType();
		GetBasicEntityTypes request = new GetBasicEntityTypes();
		requestContent.setSession_id(sessionID);
		requestContent.setEntity_type(entityType);

		request.setContent(requestContent);
		admin_ws.GetBasicEntityTypesResponse response = null;
		try {
            synchronized(service) {
            	response = this.service.GetBasicEntityTypes(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return CommonUtils.convertToList(response.getContent().getEntity_types());
	}
	
	
	@Override
	public void enrollDevice(ConfigurationTemplate template, boolean enrollAsEndpoint) throws GOperationNotAllowedException {
        admin_ws.EnrollDeviceToUser request = new admin_ws.EnrollDeviceToUser();
        com.giritech.admin_ws.EnrollDeviceToUserRequestType requestContent = new com.giritech.admin_ws.EnrollDeviceToUserRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setExternal_user_id("");
        requestContent.setActivate_rule(false);
        requestContent.setUser_plugin("");
        requestContent.setDevice_template(template);
        requestContent.setEnroll_as_endpoint(enrollAsEndpoint);
        if (enrollAsEndpoint)
        	requestContent.setUnique_session_id(GGlobalDefinitions.GATEWAY_SESSION_ID);
        
        request.setContent(requestContent);
        admin_ws.EnrollDeviceToUserResponse response = null;
        try {
            synchronized(service) {
            	response = this.service.EnrollDeviceToUser(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			handleFaultServerOperationNotAllowed(e);
		}
		boolean returnCode = response.getContent().getRc();
		if (!returnCode) {
			throw new RuntimeException(response.getContent().getMessage());
		}
		
	}

	@Override
	public void addMembersToOneTimeEnrollers(GIElement element) {
        admin_ws.AddMembersToFirstTimeEnrollers request = new admin_ws.AddMembersToFirstTimeEnrollers();
        com.giritech.admin_ws.AddMembersToFirstTimeEnrollersRequestType requestContent = new com.giritech.admin_ws.AddMembersToFirstTimeEnrollersRequestType();
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement(createElementType(element));
        request.setContent(requestContent);
        admin_ws.AddMembersToFirstTimeEnrollersResponse response = null;
        try {
            synchronized(service) {
            	response = this.service.AddMembersToFirstTimeEnrollers(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		} catch (FaultServerOperationNotAllowed e) {
			Activator.getLogger().logException(e);
			throw new RuntimeException(e);
		}
		@SuppressWarnings("unused")
		boolean returnCode = response.getContent().getRc();
		
	}

	@Override
	public TokenInfoType[] getTokenInfo(TokenInfoType[] tokenArray) {
		GetTokenInfo request = new GetTokenInfo();
		GetTokenInfoRequestType requestContent = new GetTokenInfoRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setTokens(tokenArray);
        request.setContent(requestContent);
        GetTokenInfoResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GetTokenInfo(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		TokenInfoType[] tokens = response.getContent().getTokens();
		if (tokens==null)
			tokens = new TokenInfoType[0];
		return tokens;
	}
	
	@Override
	public RuleType1[] GetRulesForElement(GIElement element, RuleType1[] knownRules) {
		GetRulesForElement request = new GetRulesForElement();
		GetRulesForElementRequestType requestContent = new GetRulesForElementRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setKnown_rules(new RuleType1[0]);
        requestContent.setElement_id(((GIModelElement) element).getId());
        requestContent.setEntity_type(element.getEntityType());
        request.setContent(requestContent);
        GetRulesForElementResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GetRulesForElement(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		@SuppressWarnings("unused")
		RuleType1[] newRules = response.getContent().getNew_rules();
		return response.getContent().getNew_rules();
	}

	
	@Override
	public void Login(String username, String password) throws LoginException {
	    admin_ws.Login login_request = new admin_ws.Login();
	    com.giritech.admin_ws.LoginRequestType login_request_content = new com.giritech.admin_ws.LoginRequestType();
	    login_request_content.setSession_id(sessionID);
	    login_request_content.setPassword(password);
	    login_request_content.setUsername(username);
	    login_request.setContent(login_request_content);
	    admin_ws.LoginResponse login_response;
		try {
            synchronized(service) {
			login_response = this.service.Login(login_request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		BigInteger errorCode = login_response.getContent().getError_code();
		if (errorCode.intValue()!=0) {
			throw new LoginException(login_response.getContent().getError_msg());
		}
		this.username = username;
		this.password = password;
	}

	@Override
	public void CloseSession() {
	
	    admin_ws.CloseSession CloseSession_request = new admin_ws.CloseSession();
	    com.giritech.admin_ws.CloseSessionRequestType CloseSession_request_content = new com.giritech.admin_ws.CloseSessionRequestType();
	    CloseSession_request.setContent(CloseSession_request_content);
	    CloseSession_request_content.setSession_id(sessionID);
		try {
            synchronized(service) {
            	this.service.CloseSession(CloseSession_request);
            }
		} catch (RemoteException e) {
			// ignore
		} catch (FaultServer e) {
			// ignore
		}
	}
	
	@Override
	public AccessDef[] GetAccessDefinitions() {
		GetAccessDefintion request = new GetAccessDefintion();
		GetAccessDefintionRequestType requestContent = new GetAccessDefintionRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetAccessDefintionResponse response = null;
        try {
            synchronized(service) {
            	response = this.service.GetAccessDefintion(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getAccess_defs();
	}

	
	@Override
	public CommonUtils.Pair<GOnServiceType[], Boolean> GetServiceList() {
		GOnServiceGetList request = new GOnServiceGetList();
		GOnServiceGetListRequestType requestContent = new GOnServiceGetListRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GOnServiceGetListResponse response = null;
        try {
            synchronized(service) {
				response = this.service.GOnServiceGetList(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		
		GOnServiceType[] gonServices = response.getContent().getGon_services();
		if (gonServices==null)
			gonServices = new GOnServiceType[0]; 
		return new CommonUtils.Pair<GOnServiceType[], Boolean>(gonServices, response.getContent().getUpdate_enabled());
	}

	@Override
	public void StopGatewayService(String sid, boolean whenNoUsers) {
		GOnServiceStop request = new GOnServiceStop();
		GOnServiceStopRequestType requestContent = new GOnServiceStopRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setSid(sid);
        requestContent.setWhen_no_sessions(whenNoUsers);
        request.setContent(requestContent);
        @SuppressWarnings("unused")
		GOnServiceStopResponse response = null;
        try {
            synchronized(service) {
            	response = this.service.GOnServiceStop(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void RestartGatewayService(String sid, boolean whenNoUsers) {
		GOnServiceRestart request = new GOnServiceRestart();
		GOnServiceRestartRequestType requestContent = new GOnServiceRestartRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setSid(sid);
        requestContent.setWhen_no_sessions(whenNoUsers);
        request.setContent(requestContent);
        @SuppressWarnings("unused")
		GOnServiceRestartResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GOnServiceRestart(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
	}

	private class GatewaySessionsFetcher extends ElementFetcher<GOnSessionType> {
		

		private final class SessionTypeFetchResult implements
				GIFetchResult<GOnSessionType> {
			private final String nextSessionId;
			private final List<GOnSessionType> resultList;

			private SessionTypeFetchResult(String nextSessionId,
					List<GOnSessionType> resultList) {
				this.nextSessionId = nextSessionId;
				this.resultList = resultList;
			}

			@Override
			public BigInteger getCount_max() {
				return BigInteger.valueOf(-1);
			}

			@Override
			public List<GOnSessionType> getElements() {
				return resultList;
			}

			@Override
			public String getNext_session_id() {
				return nextSessionId;
			}

			@Override
			public boolean getResult_truncated() {
				return nextSessionId != null;
			}
		}


		private String[] serverSids;


		GatewaySessionsFetcher(String[] serverSids, String searchFilter) {
			super("", searchFilter, false);
			this.serverSids = serverSids;
		}



		@Override
		protected GIFetchResult<GOnSessionType> getFirstElements(int count) {
			GOnSessionType[] gonSessions = null;
			String nextSessionId = null;
			if (serverSids==null || serverSids.length>0) {
				GetGatewaySessions request = new GetGatewaySessions();
				GetGatewaySessionsRequestType requestContent = new GetGatewaySessionsRequestType(); 
		        requestContent.setSession_id(GServer.this.sessionID);
		        requestContent.setSearch_filter(filter);
		        requestContent.setServer_sid(serverSids);
		        request.setContent(requestContent);
		        final GetGatewaySessionsResponse response;
		        try {
		            synchronized(service) {
						response = GServer.this.service.GetGatewaySessions(request);
		            }
				} catch (RemoteException e) {
					handleRemoteException(e);
					throw new RuntimeException(e);
				} catch (FaultServer e) {
					handleFaultServer(e);
					throw new RuntimeException(e);
				}
				gonSessions = response.getContent().getGon_sessions();
				nextSessionId = response.getContent().getNext_session_id();
				gatewaySessionsUpdateEnabled = response.getContent().getUpdate_enabled();
			}
			if (gonSessions==null)
				gonSessions = new GOnSessionType[0]; 
			
			final List<GOnSessionType> resultList = CommonUtils.convertToList(gonSessions);
			return new SessionTypeFetchResult(nextSessionId, resultList);
			
		}
		

		@Override
		protected GIFetchResult<GOnSessionType> getNextElements(int count) {
			GetGatewaySessionsNext request = new GetGatewaySessionsNext();
			GetGatewaySessionsNextRequestType requestContent = new GetGatewaySessionsNextRequestType(); 
	        requestContent.setSession_id(GServer.this.sessionID);
	        requestContent.setNext_session_id(nextSessionID);
	        request.setContent(requestContent);
	        final GetGatewaySessionsNextResponse response;
	        try {
	            synchronized(service) {
					response = GServer.this.service.GetGatewaySessionsNext(request);
	            }
			} catch (RemoteException e) {
				handleRemoteException(e);
				throw new RuntimeException(e);
			} catch (FaultServer e) {
				handleFaultServer(e);
				throw new RuntimeException(e);
			}
			GOnSessionType[] gonSessions = response.getContent().getGon_sessions();
			if (gonSessions==null)
				gonSessions = new GOnSessionType[0]; 
			
			final List<GOnSessionType> resultList = CommonUtils.convertToList(gonSessions);
			
			final String nextSessionId = response.getContent().getNext_session_id();
			return new SessionTypeFetchResult(nextSessionId, resultList);
		}
		
	}

	@Override
	public boolean getGatewaySessionsUpdateEnabled() {
		return gatewaySessionsUpdateEnabled;
	}
	
	@Override
	public GIRuleElementFetcher<GOnSessionType> getGOnSessionsFetcher(String[] serverSids, String searchFilter) {
		return new GatewaySessionsFetcher(serverSids, searchFilter);
	}
	
	public boolean stopFetchingElements(String nextSessionId) {
		GetElementsStop request = new GetElementsStop();
		GetElementsStopRequestType requestContent = new GetElementsStopRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setNext_session_id(nextSessionId);
        request.setContent(requestContent);
        GetElementsStopResponse response = null;
        try {
            synchronized(service) {
				response = this.service.GetElementsStop(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
	}
	
	
	@Override
	public void changeUserRegistration(GIElement element) throws GOperationNotAllowedException {
		if (element instanceof GServerElement) {
			GServerElement e = (GServerElement) element;
			GConfigTemplateUtil templateUtil = e.getgConfigTemplateUtil();
			if (templateUtil==null) {
				ConfigurationTemplate configurationTemplate = getConfigurationTemplate(element, false, false);
				templateUtil = new GConfigTemplateUtil(configurationTemplate, null);
				templateUtil.setValuesFromTemplate();
			}
			GIConfigRowValue value = templateUtil.values.get(GGlobalDefinitions.PROPERTY_REGISTERED_USER);
			Boolean currentValue = value.getValueBoolean();
			try {
				value.setValue(!currentValue);
				templateUtil.updateTemplateValues();
				ConfigurationTemplate updateConfigRow = updateConfigRow(element, templateUtil.template);
				((GServerElement) element).setConfigTemplate(updateConfigRow);
			} catch (GOperationNotAllowedException t) {
				value.setValue(currentValue);
				templateUtil.updateTemplateValues();
				throw t;
				
			}
		}
		else
			throw new RuntimeException("Internal error : element is not the right type"); 
		
	}

	@Override
	public boolean closeGatewaySession(String serverSid, String uniqueSessionId) {
		GatewaySessionClose request = new GatewaySessionClose();
		GatewaySessionCloseRequestType requestContent = new GatewaySessionCloseRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setSid(serverSid);
        requestContent.setUnique_session_id(uniqueSessionId);
        request.setContent(requestContent);
        GatewaySessionCloseResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GatewaySessionClose(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
	}
	
	@Override
	public boolean recalculateMenuForGatewaySession(String serverSid, String uniqueSessionId) {
		GatewaySessionRecalculateMenu request = new GatewaySessionRecalculateMenu();
		GatewaySessionRecalculateMenuRequestType requestContent = new GatewaySessionRecalculateMenuRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setSid(serverSid);
        requestContent.setUnique_session_id(uniqueSessionId);
        request.setContent(requestContent);
        GatewaySessionRecalculateMenuResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GatewaySessionRecalculateMenu(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
	}

	@Override
	public LicenseInfoTypeManagement getLicenseInfo() {
		GetLicenseInfo request = new GetLicenseInfo();
		GetLicenseInfoRequestType requestContent = new GetLicenseInfoRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        GetLicenseInfoResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GetLicenseInfo(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getLicense_info();
	}

	@Override
	public boolean setLicense(String content) {
		SetLicense request = new SetLicense();
		SetLicenseRequestType requestContent = new SetLicenseRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setLicense(content);
        request.setContent(requestContent);
        SetLicenseResponse response = null;
        try {
            synchronized(service) {
			response = this.service.SetLicense(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return response.getContent().getRc();
	}
	
	@Override
	public boolean Ping() {
		Ping request = new Ping();
		PingRequestType requestContent = new PingRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
		PingResponse response = null;
        try {
            synchronized(service) {
			response = this.service.Ping(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		
		return response.getContent().getRc();
	}
	
	@Override
	public List<GIModelElement> getRestrictions(GIElement element, String restrictionType) {
		GetAuthRestrictionElements request = new GetAuthRestrictionElements();
		GetAuthRestrictionElementsRequestType requestContent = new GetAuthRestrictionElementsRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement(createElementType(element));
        requestContent.setRestriction_type(restrictionType);
        request.setContent(requestContent);
        GetAuthRestrictionElementsResponse response = null;
        try {
            synchronized(service) {
			response = this.service.GetAuthRestrictionElements(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		return createClientElements(response.getContent().getElements());
	}
	
	@Override
	public void updateRestrictions(GIElement element, String restrictionType, List<GIElement> chosenElements) {
		UpdateAuthRestrictionElements request = new UpdateAuthRestrictionElements();
		UpdateAuthRestrictionElementsRequestType requestContent = new UpdateAuthRestrictionElementsRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement(createElementType(element));
        requestContent.setRestriction_elements(createElementTypes(chosenElements));
        requestContent.setRestriction_type(restrictionType);
        request.setContent(requestContent);
        try {
            synchronized(service) {
			this.service.UpdateAuthRestrictionElements(request);
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		
	}

	
	@Override
	public boolean removeUserFromLaunchTypeCategory(GIElement element, String launchTypeCategory) {
		RemoveUserFromLaunchTypeCategory request = new RemoveUserFromLaunchTypeCategory();
		RemoveUserFromLaunchTypeCategoryRequestType requestContent = new RemoveUserFromLaunchTypeCategoryRequestType(); 
        requestContent.setSession_id(this.sessionID);
        requestContent.setElement(createElementType(element));
        requestContent.setLaunch_type_category(launchTypeCategory);
        request.setContent(requestContent);
        try {
            synchronized(service) {
				RemoveUserFromLaunchTypeCategoryResponse removeUserFromLaunchTypeCategory = this.service.RemoveUserFromLaunchTypeCategory(request);
				return removeUserFromLaunchTypeCategory.getContent().getRc();
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		
	}

	@Override
	public String[] GetRestrictedLaunchTypeCategories() {
		GetRestrictedLaunchTypeCategories request = new GetRestrictedLaunchTypeCategories();
		GetRestrictedLaunchTypeCategoriesRequestType requestContent = new GetRestrictedLaunchTypeCategoriesRequestType(); 
        requestContent.setSession_id(this.sessionID);
        request.setContent(requestContent);
        try {
            synchronized(service) {
				GetRestrictedLaunchTypeCategoriesResponse GetRestrictedLaunchTypeCategories = this.service.GetRestrictedLaunchTypeCategories(request);
				String[] launchTypeCategories = GetRestrictedLaunchTypeCategories.getContent().getLaunch_type_category();
				if (launchTypeCategories==null)
					launchTypeCategories = new String[0];
				return launchTypeCategories;
            }
		} catch (RemoteException e) {
			handleRemoteException(e);
			throw new RuntimeException(e);
		} catch (FaultServer e) {
			handleFaultServer(e);
			throw new RuntimeException(e);
		}
		
	}
	
}
