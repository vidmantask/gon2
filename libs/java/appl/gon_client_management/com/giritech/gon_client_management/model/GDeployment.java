package gon_client_management.model;

import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.ext.GConfigTemplateUtil;
import gon_client_management.model.ext.GIJob;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.localservice.GLocalServerInterface;
import gon_client_management.model.localservice.GLocalServiceJob;
import gon_client_management.model.server.GServerInterface;

import java.util.ArrayList;
import java.util.List;

import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;

class GDeployment extends GSoftwareDeployment implements GIDeployment {
	
	List<GIToken> tokenList;
	
	public GDeployment() {
		this.tokenList = new ArrayList<GIToken>();
	}

	private class GToken implements GIModelToken {
		
		String id = "";
		String title = "";
		String serial = "";
		String type = "";
		String public_key = "";
		String casing = "";
		String status = "";
		String description = "";
		String runtimeEnvId = null;
		String typeLabel = "";
		String name = "";
		boolean isEnrollable = true;
		private String internal_type;
		


		@SuppressWarnings("unused")
		GToken() {
			
		}

		GToken(TokenInfoType tokenInfo) {
			id = tokenInfo.getToken_id();
			title = tokenInfo.getToken_label();
			serial = tokenInfo.getToken_serial();
			status = tokenInfo.getToken_status();
			type = tokenInfo.getToken_type_id();
			internal_type = tokenInfo.getToken_internal_type();
			runtimeEnvId = tokenInfo.getRuntime_env_id();
			typeLabel = tokenInfo.getToken_type_label();

			casing = tokenInfo.getCasing();
			description = tokenInfo.getDescription();
			name = tokenInfo.getName();
			isEnrollable = tokenInfo.getEnrollable();
			
			
		}
		
		
		@SuppressWarnings("unused")
		public TokenInfoType getServerTypeToken() {
			TokenInfoType tokenInfo = new TokenInfoType();
			
			tokenInfo.setToken_id(id);
			tokenInfo.setToken_label(title);
			tokenInfo.setToken_serial(serial);
			tokenInfo.setToken_status(status);
			tokenInfo.setToken_type_id(type);
			tokenInfo.setToken_internal_type(internal_type);
			tokenInfo.setRuntime_env_id(runtimeEnvId);
			tokenInfo.setToken_type_label(typeLabel);
			tokenInfo.setCasing(casing);
			tokenInfo.setDescription(description);
			tokenInfo.setName(name);
			tokenInfo.setEnrollable(isEnrollable);
			
			return tokenInfo;
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getId()
		 */
		public String getId() {
			return type+"."+id;
		}


		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getTitle()
		 */
		public String getTitle() {
			return title;
		}


		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getSerial()
		 */
		public String getSerial() {
			return serial;
		}

		public void setSerial(String serial) {
			this.serial = serial;
		}

		public String getTypeSpecificId() {
			return id;
		}
		
		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getType()
		 */
		public String getType() {
			return type;
		}

		@SuppressWarnings("unused")
		void setType(String type) {
			this.type = type;
		}

		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getPublic_key()
		 */
		public String getPublic_key() {
			return public_key;
		}

		public void setPublic_key(String public_key) {
			this.public_key = public_key;
		}

		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#getCasing()
		 */
		public String getCasing() {
			return casing;
		}

		/* (non-Javadoc)
		 * @see gon_client_management.model.GIToken#setCasing(java.lang.String)
		 */
		public void setCasing(String casing) {
			this.casing = casing;
		}


		public String getStatus() {
			return status;
		}
		
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@SuppressWarnings("unused")
		void setRuntimeEnvId(String runtimeEnvId) {
			this.runtimeEnvId = runtimeEnvId;
		}

		@SuppressWarnings("unused")
		public String getRuntimeEnvId() {
			return runtimeEnvId;
		}
		
		public boolean canInstallSoftware() {
			return runtimeEnvId!=null;
		}

		public String getTypeLabel() {
			return typeLabel;
		}

		public String getName() {
			return name;
		}
		
		public boolean isEnrollable() {
			return isEnrollable;
		}
		
		public GIJob createInstallSoftwareJob(final String collectionId) {
			if (runtimeEnvId!=null)
				return new GLocalServiceJob(new GLocalServiceJob.GIJobCaller() {
					public int callJob() {
						return GLocalServerInterface.getLocalService().installGPMCollection(collectionId, runtimeEnvId);
					}
				});
			throw new RuntimeException("Cannot install on this token");
			
		}
		
		

		@Override
		public void setName(String tokenName) {
			this.name = tokenName;
		}

		@Override
		public String getInternalType() {
			return internal_type;
		}

		
	}
	
	
	
	/**
	 * Fetch information for currently inserted tokens.
	 * The information is inserted into an array.
	 * @param tokenElements 
	 */
	private void updateAvailableTokenInfo() {
		
		synchronized (tokenList) {
			/* Get token information and insert it into a structure. */
			tokenList.clear();
			
			TokenInfoType[] tokenInfoList = GLocalServerInterface.getLocalService().getTokenInfo();
			
			tokenInfoList = GServerInterface.getServer().getTokenInfo(tokenInfoList);
			
			for(TokenInfoType tokenInfo : tokenInfoList) {
				GToken token = new GToken(tokenInfo);
				
				if (token.type.equals("endpoint_token") && GGlobalDefinitions.GATEWAY_SESSION_ID==null)
					token.isEnrollable = false;
				
				tokenList.add(token);
			}
		}
	}

	public String enrollToken(GIToken token) throws GOperationNotAllowedException {
		/* Enrollment on the servers side.*/ 
		//final String entityType = entityTypeMap.get(token.getType());
		final String entityType = GServerInterface.getServer().getEntityTypeForPluginType(token.getType(), null);
		final ConfigurationTemplate template = GServerInterface.getServer().getConfigurationTemplate(entityType, null);
			
		GConfigTemplateUtil templateUtil = new GConfigTemplateUtil(template, null);
		templateUtil.setValuesFromTemplate();
		String serial = initToken(token);
		if (serial==null) {
			throwError("Unknown error during deployment - initialization of token failed");
		}
		templateUtil.values.get("serial").setValue(serial);

		/* Enrollment on the token side - returns a public key.*/
		String publicKey = GLocalServerInterface.getLocalService().deployToken((GIModelToken) token);
		if (publicKey==null) 
			throwError("Unknown error during deployment - no public key returned");
		templateUtil.values.get("public_key").setValue(publicKey);
		
		templateUtil.values.get("description").setValue(token.getDescription());
		templateUtil.values.get("casing_number").setValue(token.getCasing());
		templateUtil.values.get("name").setValue(token.getTypeLabel());
		templateUtil.values.get("plugin_type").setValue(token.getType());
		templateUtil.values.get("internal_type").setValue(token.getInternalType());
		boolean enrollAsEndpoint = entityType.equals("EndpointToken");
		templateUtil.values.get("endpoint").setValue(enrollAsEndpoint);
		
		templateUtil.updateTemplateValues();
		GServerInterface.getServer().enrollDevice(template, enrollAsEndpoint);

		updateAvailableTokenInfo();
//			updateTokenInfo(token);
		
		if (enrollAsEndpoint)
			return null;
		else
			return entityType + "." + token.getType() + "." + serial;
		
	}

//	private void updateTokenInfo(GIToken token) {
//		GToken updatedToken = ((GToken) token);
//		TokenInfoType serverTypeToken = updatedToken.getServerTypeToken();
//		TokenInfoType[] tokenList = new TokenInfoType[1];
//		tokenList[0] = serverTypeToken;
//		TokenInfoType[] updatedTokenInfoArray = GServerInterface.getServer().getTokenInfo(tokenList);
//		
//		if (updatedTokenInfoArray.length>0) {
//			TokenInfoType updatedTokenInfo = updatedTokenInfoArray[0];
//			
//			updatedToken.isEnrollable = updatedTokenInfo.getEnrollable();
//			updatedToken.name = updatedTokenInfo.getName();
//			updatedToken.description = updatedTokenInfo.getDescription();
//			updatedToken.casing = updatedTokenInfo.getCasing();
//		}
//	}

	public int getTokenCount() {
		synchronized (tokenList) {
			return tokenList.size();
		}
	}

	public List<GIToken> getTokens() {
		synchronized (tokenList) {
			return tokenList;
		}
	}

	public void refreshTokenList() {
		updateAvailableTokenInfo();		
	}
	
	
	/**
	 * Do token initialization to set the tokens serial.
	 * Remember that init removed the deployment on the token.
	 * @return 
	 */
	public String initToken(GIToken token) {
		return GLocalServerInterface.getLocalService().initToken((GIModelToken) token);
	}
	
	
	private void throwError(String line) {
		throw new RuntimeException(line);
		
	}

}
