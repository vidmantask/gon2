package gon_client_management.model.ext;

import gon_client_management.model.ext.GIConfigColumn.GIAction;
import gon_client_management.model.ext.GIConfigColumn.GISelection;
import gon_client_management.model.ext.GIConfigColumn.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import server_config_ws.TestConfigSpecificationResponse;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_job.Job_status_type0;
import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Field_action;
import com.giritech.admin_ws.types_config_template.Field_element;
import com.giritech.admin_ws.types_config_template.Field_type_type0;
import com.giritech.admin_ws.types_config_template.Portscan_action;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.admin_ws.types_config_template.Value_selection;
import com.giritech.admin_ws.types_config_template.Value_selection_choice;
import com.giritech.admin_ws.types_config_template.Value_type_type0;

public class GConfigTemplateUtil {
	
	public Map<String, GIConfigRowValue> values = new HashMap<String, GIConfigRowValue>();
	public List<GIDetails> details = null;
	private final GIFieldAction fieldActions;
	
	public static Type convertType(Value_type_type0 value_type) {
		if (value_type==Value_type_type0.string_value_type)
			return Type.TYPE_STRING;
		else if (value_type==Value_type_type0.integer_value_type)
			return Type.TYPE_INTEGER;
		else if (value_type==Value_type_type0.boolean_value_type)
			return Type.TYPE_BOOLEAN;
		else if (value_type==Value_type_type0.text_value_type)
			return Type.TYPE_TEXT;
		else if (value_type==Value_type_type0.date_value_type)
			return Type.TYPE_DATE;
		else if (value_type==Value_type_type0.datetime_value_type)
			return Type.TYPE_DATETIME;
		else if (value_type==Value_type_type0.time_value_type)
			return Type.TYPE_TIME;
		else {
			//TODO: Log error
			return Type.TYPE_STRING;
		}
	}
	
	public GConfigTemplateUtil(ConfigurationTemplate template, GIFieldAction fieldActions) {
		this.template = template;
		this.fieldActions = fieldActions;
	}

	public GConfigTemplateUtil(ConfigurationTemplate template, GIFieldAction fieldActions, List<GIDetails> details) {
		this(template, fieldActions);
		this.details = details;
	}
	
	
	public ConfigurationTemplate template;
	
	public void updateTemplateValues() {
		Field_element[] fields = template.getField();
		for(Field_element field : fields) {
			if (values.containsKey(field.getName())) {
				GIConfigRowValue value = values.get(field.getName());
				if (value!=null && !value.isNull()) 
					field.setDefault_value(value.encode());
				else
					field.setDefault_value(null);
			}
		}
	}

	public GIConfigPanePage getConfigPanePage() {
		return new GConfigPanePage();
	}

	public List<GIDetails> getDetails() {
		return new ArrayList<GIDetails>(details);
	}
	
	public GConfigRowValue getDefaultValue(final Field_element field) {
		Type column_type = convertType(field.getValue_type());
		String value = field.getDefault_value();
		GConfigRowValue configRowValue;
		if (value!=null) {
			configRowValue = GConfigRowValue.factory(column_type, value);
		}
		else {
			configRowValue = new GConfigRowValue(column_type);
		}
		return configRowValue;
	}
	
	public void setValuesFromTemplate() {
		for (Field_element field : template.getField()) {
			GConfigRowValue configRowValue = getDefaultValue(field);
			values.put(field.getName(), configRowValue);
		}
	}

	public class GConfigPanePage implements GIConfigPanePage {
		
		protected List<GIConfigColumn> columns = new ArrayList<GIConfigColumn>();
		protected GConfigPanePage advancedPane = null;
		protected boolean advanced;
		
		protected GConfigPanePage(boolean advanced) {
			this.advanced = advanced;
		}
		
		public GConfigPanePage() {
			Field_element[] fields = template.getField();
			for(Field_element field : fields) {
				addColumn(field);
			}
			
		}

		private void addColumn(final Field_element field) {
			if (field.getField_type() == Field_type_type0.hidden) {
				addField(field);
			}
			else if (advanced) {
				if (field.getAdvanced()) {
					addField(field);
				}
			}
			else {
				if (!field.getAdvanced()) { 
					addField(field);
				}
				else {
					if (advancedPane==null) {
						advancedPane = createAdvancedPane();
					}
					advancedPane.addColumn(field);
				}
			}
				
		}
		
		

		protected GConfigPanePage createAdvancedPane() {
			return new GConfigPanePage(true);
		}

		protected void addField(final Field_element field) {
			
			if (field.getField_type() != Field_type_type0.hidden) {
				
				GIConfigColumn pageColumn = createConfigColumn(field);
				columns.add(pageColumn);
				setDefaultValue(field, pageColumn);
			}
			
		}
		
		@Override
		public List<GIDetails> getDetails() {
			return GConfigTemplateUtil.this.getDetails();
		}

		public GIConfigPanePage getAdvancedPane() {
			return advancedPane;
		}

		public GIConfigColumn getColumn(int index) {
			return columns.get(index);
		}


		public int getColumnCount() {
			return columns.size();
		}


		public GIConfigRowValue getValue(int index) {
			String name = columns.get(index).getName();
			return values.get(name);
		}

		public String getTitle() {
			return template.getTitle();
		}
		
		public GIConfigColumn createConfigColumn(final Field_element field) {
			
			final Field_action fieldAction = field.getAction();
			
			GIConfigColumn pageColumn = new GIConfigColumn() {
				
				Type column_type = convertType(field.getValue_type());
				
				final GISelection selection = createSelection(field, this);
				
				public String getLabel() { return field.getTitle(); }
		
				public String getName() { return field.getName(); }
		
				public Type getType() { return column_type; }
		
				public boolean isReadOnly() { return field.getRead_only(); }
		
				public boolean isSecret() { return field.getSecret(); }
		
				public String getToolTip() { return field.getTooltip(); }

				public String getSection() { return field.getSection(); }
				
				public GISelection getSelection() { return selection; }
		
				public GIAction getAction() { return createAction(fieldAction, this); }
		
				public int getTextLimit() { return (field.getMax_length()!=null) ? field.getMax_length().intValue() : -1; }
				
			};
			return pageColumn;
		}
		
		protected GIAction createAction(Field_action fieldAction, GIConfigColumn configColumn) {
			if (fieldAction!=null) {
				if (fieldAction.getPortscan()!=null) {
					return createScanAction(configColumn, fieldAction.getPortscan());
				}
				if (fieldAction.getTest_config()!=null) {
					return createTestConfigAction(configColumn, fieldAction.getTest_config());
					
				}
			}
			return null;
		}


		private GIAction createTestConfigAction(GIConfigColumn configColumn, final Test_config_action testConfig) {
			final GIAction action;
			final GFieldActionJobInfo jobInfo = new GFieldActionJobInfo();
			

			action = new GIAction() {

				public GIJob getJob() {
					
					return new GIJob() {
						
						Thread t = null;
						private TestConfigSpecificationResponse testConfigSpecResponse;

						public boolean cancel() {
							return false;
						}

						public GIJobInfo getStatus() {
							if (t!=null && !t.isAlive()) {
								jobInfo.more = false;
							}
							return jobInfo;
						}

						public int getTotalWork() {
							return -1;
						}

						public void start() {
							
							jobInfo.header = testConfig.getTitle();
							jobInfo.more = true;
							
							
							t = new Thread() {

								@Override
								public void run() {
									updateTemplateValues();
									testConfigSpecResponse = fieldActions.testConfigSpec(template, testConfig);
									boolean rc = testConfigSpecResponse.getContent().getRc();
									if (rc) {
										jobInfo.jobStatus = Job_status_type0.done_ok_job_status;
									}
									else {
										jobInfo.jobStatus = Job_status_type0.done_error_job_status;
										
									}
									jobInfo.info = testConfigSpecResponse.getContent().getMessage();
									
								}
								
							};
							t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

								@Override
								public void uncaughtException(Thread t, Throwable e) {
									jobInfo.header = "Error";
									jobInfo.info = e.getMessage();
									
								}
								
							});
							t.start();
						}


						
					};
				}

				public String getTitle() {
					return testConfig.getTitle();
				}
				
			};
			return action;
		}

		private class GFieldActionJobInfo implements GIJobInfo {
			
			String header = "";
			String info = "";
			boolean more = true;
			Job_status_type0 jobStatus = Job_status_type0.running_job_status;

			public String getJobHeader() {
				return header;
			}

			public String getJobInfo() {
				return info;
			}

			public int getJobProgress() {
				return 0;
			}

			public Job_status_type0 getJobStatus() {
				return jobStatus;
			}

			public boolean more() {
				return more;
			}

			@Override
			public JobInfoType getJobInfoType() {
				return null;
			}
			
		}
		
		private GIAction createScanAction(final GIConfigColumn column, final Portscan_action portscanAction) {
			final GIAction action;
			final GFieldActionJobInfo jobInfo = new GFieldActionJobInfo();
			

			action = new GIAction() {

				public GIJob getJob() {
					
					return new GIGetSelectionVauesJob() {
						
						Thread t = null;
						private Value_selection portScan = null;

						public boolean cancel() {
							return false;
						}

						public GIJobInfo getStatus() {
							if (t!=null && !t.isAlive()) {
								jobInfo.more = false;
							}
							return jobInfo;
						}

						public int getTotalWork() {
							return -1;
						}

						public void start() {
							
							jobInfo.header = "Scanning port ";
							jobInfo.more = true;
							
							final GIConfigRowValue configRowValue = values.get(portscanAction.getPort_field());
							
							if (configRowValue==null || configRowValue.toString().length()==0) {
								jobInfo.header = "Error";
								jobInfo.info = "Port number not set";
								jobInfo.more = false;
								return;
							}
							else {
								try {
									Integer.valueOf(configRowValue.toString());
								}
								catch (NumberFormatException e)  {
									jobInfo.header = "Error";
									jobInfo.info = "Port number '" + configRowValue + "' is invalid";
									jobInfo.more = false;
									return;
								}
								jobInfo.info = configRowValue.toString();
							}
							t = new Thread() {


								@Override
								public void run() {
									ArrayList<String> portNumbers = new ArrayList<String>();
									String name = values.get(column.getName()).toString();
									portNumbers.add(configRowValue.toString());
									portScan = fieldActions.portScan(name, portNumbers);
									if (portScan.getSelection_choice()!=null && portScan.getSelection_choice().length>0) {
										jobInfo.header = "Scan Result";
										jobInfo.info = "Choose a server";
									}
									else {
										jobInfo.header = "Scan Result";
										jobInfo.info = "No servers found";
										
									}
									
								}
								
							};
							t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

								@Override
								public void uncaughtException(Thread t, Throwable e) {
									jobInfo.header = "Error";
									jobInfo.info = "Error during scan: " + e.getMessage();
									
								}
								
							});
							t.start();
						}

						public List<GISelectionEntry> getSelectionChoices() {
							if (portScan!=null) {
								synchronized(portScan) {
									return new ArrayList<GISelectionEntry>(createSelectionList(portScan, column));
								}
							}
							return new ArrayList<GISelectionEntry>();
						}
						
					};
				}

				public String getTitle() {
					return portscanAction.getTitle();
				}
				
			};
			return action;
		}
		

		public GConfigRowValue getDefaultValue(final Field_element field, final GIConfigColumn column) {
			String value = field.getDefault_value();
			GConfigRowValue configRowValue;
			if (value!=null) {
				configRowValue = GConfigRowValue.factory(column.getType(), value);
			}
			else {
				configRowValue = new GConfigRowValue(column.getType());
			}
			configRowValue.setSelectionValue(column.getSelection());
			return configRowValue;
		}
		
		
		public void setDefaultValue(final Field_element field, final GIConfigColumn column) {
			GConfigRowValue configRowValue = getDefaultValue(field, column);
			values.put(field.getName(), configRowValue);
		}
		

		
		protected GISelection createSelection(Field_element field, GIConfigColumn configColumn) {
			if (field.getSelection()!=null) {
				Value_selection selection = field.getSelection();
				if (selection==null) {
					selection = new Value_selection();
					selection.setSelection_choice(new Value_selection_choice[0]);
				}
				return new GModelSelection(selection, configColumn);
			}
			return null;
		}
		

		class GModelSelection implements GISelection {
			private boolean editAllowed;
			
			private List<SelectionEntry> values;

			public GModelSelection(Value_selection selection, GIConfigColumn configColumn) {
				this.values = createSelectionList(selection, configColumn);
			}




			public List<GISelectionEntry> getSelectionChoices() {
				return new ArrayList<GISelectionEntry>(values);
			}


			public GIConfigRowValue getValue(int index) {
				if (index>=0 && index<values.size())
					return values.get(index).getValue();
				return null;
			}


			public boolean isEditAllowed() {
				return editAllowed;
			}

			public void setEditAllowed(boolean editAllowed) {
				this.editAllowed = editAllowed;
			}

			
		}

		private class SelectionEntry implements GISelectionEntry {
			public String title;
			public GIConfigRowValue value;
			
		
			SelectionEntry(String title, GIConfigRowValue value) {
				this.title = title;
				this.value = value;
			}
			
			public String getTitle() {
				return title;
			}
		
			public GIConfigRowValue getValue() {
				return value;
			}

			@Override
			public String toString() {
				return getTitle();
			}
			
		}

		protected List<SelectionEntry> createSelectionList(Value_selection selection, GIConfigColumn configColumn) {
			List<SelectionEntry> values = new ArrayList<SelectionEntry>();
			
			if (selection.getSelection_choice()!=null) {
				for(Value_selection_choice selection_choice : selection.getSelection_choice()) {
					String guiString = selection_choice.getTitle();
					GConfigRowValue value = GConfigRowValue.factory(configColumn.getType(), selection_choice.getValue());
					if (guiString==null)
						guiString = value.toString();
					values.add(new SelectionEntry(guiString, value));
				}
			}
			return values;
		}
		
		
	}
	

	public void setTemplate(ConfigurationTemplate newTemplate) {
		this.template = newTemplate;
		setValuesFromTemplate();
		details = null;
		
		
	}
	
}
