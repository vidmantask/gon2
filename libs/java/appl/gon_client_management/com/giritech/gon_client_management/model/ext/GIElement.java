package gon_client_management.model.ext;


public interface GIElement extends GIGuiObject{

	/**
	 * 
	 * @return The type of this element.
	 */
	public abstract String getEntityType();

	/**
	 * 
	 * @return The label that is the main descriptor of this predicate.
	 */
	public abstract String getLabel();

	/**
	 * 
	 * @return Extra information that describes this predicate.
	 */
	public abstract String getInfo();

	/**
	 * Get a unique identifier for this element.
	 * @return ID as a string
	 */
	public String getElementId();
	
	public abstract void setLabel(String label);
	
	public boolean checkProperty(String propertyName);
	
}