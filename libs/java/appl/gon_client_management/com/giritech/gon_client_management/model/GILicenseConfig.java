package gon_client_management.model;

import gon_client_management.model.ext.GIElement;

import com.giritech.admin_ws.LicenseInfoTypeManagement;

public interface GILicenseConfig {
	
	public LicenseInfoTypeManagement getLicenseInfo();

	public boolean setLicense(String content);

	public boolean removeUserFromLaunchTypeCategory(GIElement element, String launchTypeCategory);

	public String[] GetRestrictedLaunchTypeCategories();
	

}
