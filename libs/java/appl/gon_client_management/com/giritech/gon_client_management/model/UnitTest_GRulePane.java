package gon_client_management.model;

import java.util.ArrayList;
import java.util.List;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.model.server.GTestServer;
import gon_client_management.model.server.GTestServer.RuleElement;
import junit.framework.TestCase;

public class UnitTest_GRulePane extends TestCase {
	
	
	private final GRulePane rulePane;
	private static GRulePaneTestServer testServer = new GRulePaneTestServer();
	final int conditionCount = 10;
	final int ruleCount = 5;

	
	private static final class GRulePaneTestServer extends GTestServer {
		
	}
	
	public UnitTest_GRulePane(String name) {
		super(name);
		//testServer = new GRulePaneTestServer();
		testServer.createRulePane(name, conditionCount, ruleCount);
		GServerInterface.setServer(testServer);
		rulePane = new GRulePane(name);
	}

	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testAddRule() throws GOperationNotAllowedException {
		assertTrue(rulePane.getRuleCount()>0);
		int ruleCount = rulePane.getRuleCount();
		GIElement resultElement = rulePane.getRule(0).getResultElement();
		List<GIElement> list = new ArrayList<GIElement>();
		rulePane.addRule(new GTestServer.Rule(resultElement, list));
		assertEquals(ruleCount+1, rulePane.getRuleCount());
	}

	public void testDeleteRule() throws GOperationNotAllowedException {
		assertTrue(rulePane.getRuleCount()>0);
		int ruleCount = rulePane.getRuleCount();
		rulePane.deleteRule(rulePane.getRule(0));
		assertEquals(ruleCount-1, rulePane.getRuleCount());
	}

	public void testReplaceRule() {
		assertTrue(rulePane.getRuleCount()>0);
		GIRule oldRule = rulePane.getRule(0);
		GIRule newRule = testServer.createRule(getName());
		try {
			rulePane.replaceRule(oldRule, newRule);
		} catch (GOperationNotAllowedException e) {
			fail(e.getMessage());
		}
		final String oldLabel = oldRule.getResultElement().getLabel();
		final String newLabel = newRule.getResultElement().getLabel();
		assertFalse(oldLabel.equals(newLabel));
		GIRule replacedRule = rulePane.getRule(0);
		assertEquals(newLabel, replacedRule.getResultElement().getLabel());
	}

	public void testGetViewHeadline() {
		assertEquals(getName() + "Title", rulePane.getViewHeadline());
	}

	public void testGetNewEmptyRule() {
		GIRule newEmptyRule = rulePane.getNewEmptyRule();
		assertEquals(conditionCount, newEmptyRule.getRuleElementCount());
		assertEquals("", newEmptyRule.getResultElement().getLabel());
		assertEquals("", newEmptyRule.getElement(conditionCount-1).getLabel());
		assertEquals("result", newEmptyRule.getResultElement().getEntityType());
		assertEquals("condition" + (conditionCount-1), newEmptyRule.getElement(conditionCount-1).getEntityType());
		
	}

	public void testGetColumnLabel() {
		final int index = conditionCount/2;
		assertEquals("ConditionLabel" + index, rulePane.getColumnLabel(index));
	}

	public void testGetResultColumnLabel() {
		assertEquals("HeaderLabel", rulePane.getColumnLabel(-1));
	}

	public void testUpdateRule() {
		final int ruleIndex = ruleCount/2;
		GIRule rule = rulePane.getRule(ruleIndex);
		final int index = conditionCount/3;
		GIElement element = rule.getElement(index);
		RuleElement ruleElement = new GTestServer.RuleElement(element, "Yo");
		GIRule updatedRule = rulePane.updateRule(rule, ruleElement);
		assertEquals("Yo" + ruleElement.getId(), updatedRule.getElement(index).getLabel());

		ruleElement = new GTestServer.RuleElement(updatedRule.getResultElement(), "Result");
		updatedRule = rulePane.updateRule(rule, ruleElement);
		assertEquals("Result" + ruleElement.getId(), updatedRule.getResultElement().getLabel());
		
		//rulePane.updateRule(rule, e);
	}

}
