package gon_client_management.model.localservice;

import gon_client_management.model.GIModelToken;

import com.giritech.admin_deploy_ws.types_job.JobInfoType;
import com.giritech.admin_deploy_ws.types_token.TokenInfoType;
import com.giritech.admin_ws.types_gpm.GPMCollectionInfoType;

public class GLocalServiceAdapter implements GILocalService {

	public boolean cancelJob(int jobId) {
		throw new UnsupportedOperationException();
	}

	public String deployToken(GIModelToken token) {
		throw new UnsupportedOperationException();
	}

	public JobInfoType getJobInfo(int jobId) {
		throw new UnsupportedOperationException();
	}

	public GPMCollectionInfoType[] getPackageCollections() {
		throw new UnsupportedOperationException();
	}

	public TokenInfoType[] getTokenInfo() {
		throw new UnsupportedOperationException();
	}

	public String initToken(GIModelToken token) {
		throw new UnsupportedOperationException();
	}

	public int installGPMCollection(String collectionId, String runtimeEnvId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}
