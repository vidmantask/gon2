package gon_client_management.model;

import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;

import java.util.ArrayList;
import java.util.List;

public interface GIMenuItemHandling {

	/**
	 * Get a new identifier that is unique among 
	 * menu items. This should probably be smarter
	 * than my example implementation :o)
	 * 
	 * @return a new unique identifier for menu items.
	 */
	
	enum ItemType {MENU_ITEM, MENU_FOLDER, AUTO_MENU, AUTO_ITEM};
	
	/**
	 * It is assumed that menu items are instantiated with
	 * an identifier, a type, a platform and a label.
	 * 
	 * @author cso
	 *
	 */
	public interface GIMenuItem {
		
		/**
		 * @return Id for the item.
		 */
		//public abstract String getIdentifier();
		
		/**
		 * @return type of the menu item (item or folder).
		 */
		public abstract ItemType getType();
		
		/**
		 * @return Label for the menu.
		 */
		public abstract String getLabel(); 
		
		
		
		/**
		 * @return Array list of menu items and folders.
		 */
		public abstract ArrayList<GIMenuItem> getChildren();
		
		/**
		 * This could be used to make sure that the last copy of 
		 * an item is not deleted. It is possible to copy items, 
		 * but it should not be possible to remove all of them.
		 * 
		 * @return value indicating if this item can be deleted.
		 */
		public abstract boolean deleteEnabled();

		public boolean isRoot();
		
		
	}
	
	public void moveItem(GIMenuItem item, GIMenuItem fromItem, GIMenuItem toItem) throws GOperationNotAllowedException;

	public void copyItem(GIMenuItem item, GIMenuItem toItem) throws GOperationNotAllowedException;

	/**
	 */
	public abstract void addElement(GIElement element, GIMenuItem item) throws GOperationNotAllowedException;
	

	/**
	 */
	public abstract void removeItem(GIMenuItem item, GIMenuItem fromItem) throws GOperationNotAllowedException;
	
	/**
	 * @return The top menu.
	 */
	public abstract GIMenuItem getTopMenu();

	public boolean dropAllowed(Object dropee, GIMenuItem target);

	public void refresh();

	public List<String> getSubscriptionTypes();
	
}
