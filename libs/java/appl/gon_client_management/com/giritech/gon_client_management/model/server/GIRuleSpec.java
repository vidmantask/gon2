package gon_client_management.model.server;

import gon_client_management.model.GIRule;

import java.util.List;
import java.util.Map;

public interface GIRuleSpec {
	
	public String getTitle();
	public String getTitleLong();
	
	public List<String> getUniqueelementTypes();
	public List<List<String>> getMandatoryElementTypes();
	
	public Map<String, List<String>> getEntityTypeMap();
	
	public GIRule getTemplateRule();
	public GIRule getHeaderRule();
	
	public boolean isCreateEnabled();
	public boolean isEditEnabled();
	public boolean isDeleteEnabled();

}
