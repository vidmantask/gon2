package gon_client_management.model.ext;

import gon_client_management.model.ext.GIConfigPanePage;

public interface GIConfigPage  {
	
	public GIConfigPanePage getPage();
	
	public String savePage();
	
	public boolean isFinished();

	public String getType();

}
