/**
 * 
 */
package gon_client_management.model.server;

import gon_client_management.model.GIModelReport;

import com.giritech.admin_ws.ReportRefType;

/**
 *
 */
public class GServerReport implements GIModelReport {

	private final String module_name_;
	private final String report_id_;
	private final String report_title_;
	
	public GServerReport(ReportRefType from) {
		module_name_ = from.getModule_name();
		report_id_ = from.getReport_id();
		report_title_ = from.getReport_title();
	}

	public GServerReport(String module_name, String report_id, String report_title) {
		module_name_ = module_name;
		report_id_ = report_id;
		report_title_ = report_title;
	}

	public String getId() {
		return report_id_;
	}

	public String getSpecification() {
		return GServerInterface.getServer().getReportSpecification(module_name_, report_id_);
	}

	public String getTitle() {
		return report_title_;
	}

}
