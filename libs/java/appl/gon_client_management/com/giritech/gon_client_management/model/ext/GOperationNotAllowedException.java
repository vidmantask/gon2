package gon_client_management.model.ext;

public class GOperationNotAllowedException extends Exception {

	public GOperationNotAllowedException() {
		super();
	}

	public GOperationNotAllowedException(String message, Throwable cause) {
		super(message, cause);
	}

	public GOperationNotAllowedException(Throwable cause) {
		super(cause);
	}

	public GOperationNotAllowedException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
