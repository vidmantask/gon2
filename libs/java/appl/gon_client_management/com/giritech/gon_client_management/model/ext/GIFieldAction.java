package gon_client_management.model.ext;

import java.util.List;

import server_config_ws.TestConfigSpecificationResponse;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.admin_ws.types_config_template.Value_selection;

public interface GIFieldAction {

	public Value_selection portScan(String name, List<String> portNumbers);
	
	public TestConfigSpecificationResponse testConfigSpec(ConfigurationTemplate template, Test_config_action action);
	
}
