package gon_client_management.model;



import gon_client_management.ext.CommonUtils;
import gon_client_management.model.ext.GIConfig;
import gon_client_management.model.ext.GIConfigPane;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;

import java.util.ArrayList;
import java.util.List;

import admin_ws.GetConfigSpecResponse;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.TemplateRefType;

public class GConfig implements GIConfig {

	private List<TemplateRefType> configTemplates;
	private final String name;
	private boolean refresh = true;

	private final boolean createEnabled;
	private final boolean editEnabled;
	private final boolean deleteEnabled;
	private final boolean editPossible;
	private String[] errorTemplates;

	public GConfig(String name) {
		this.name = name;
		GetConfigSpecResponse configSpec = GServerInterface.getServer().getConfigSpec(name);
		configTemplates = CommonUtils.convertToList(configSpec.getContent().getConfig_templates());
		errorTemplates = configSpec.getContent().getError_templates();
		createEnabled = configSpec.getContent().getCreate_enabled();
		editEnabled = configSpec.getContent().getEdit_enabled();
		deleteEnabled = configSpec.getContent().getDelete_enabled();
		editPossible = configSpec.getContent().getEdit_possible();
	}
	

	public boolean deleteRow(GIElement element) throws GOperationNotAllowedException {
		return GServerInterface.getServer().deleteConfigRow(element);
	}

	
	public GIConfigPane getConfigPane(GIElement element) {
		return getConfigPane(element, false);
	}

	public GIConfigPane getConfigPane(GIElement element, boolean copy) {
		ConfigurationTemplate template = GServerInterface.getServer().getConfigurationTemplate(element, false, refresh);
		if (template==null) {
			throw new RuntimeException("Unable to get properties from server");
		}
		refresh = false;
		final GIConfigPane pane;
		if (copy)
			pane = new GConfigTemplatePane(element.getEntityType(), template);
		else {
			pane = new GConfigTemplatePane(element, template);
		}
		configFetched(pane);
		return pane;
	}

	public GIConfigPane getDefaultConfigPane(GIElement element) {
		ConfigurationTemplate template = GServerInterface.getServer().getConfigurationTemplate(element, true, false);
		final GIConfigPane pane = new GConfigTemplatePane(element, template);
		configFetched(pane);
		return pane;
	}

	public GIConfigPane getCreateConfigPane() {
		getConfigTemplates();
		if (configTemplates.size()==0)
			throw new RuntimeException("Cannot create element of type " + this.name);
		TemplateRefType templateRefType = configTemplates.get(0);
		String entityType = templateRefType.getEntity_type();
		ConfigurationTemplate template = GServerInterface.getServer().getConfigurationTemplate(entityType, null);
		final GIConfigPane pane = new GConfigTemplatePane(entityType,template);
		configFetched(pane);
		return pane;
	}
	

	public GIConfigPane getConfigPane(String entityType, String templateName) {
		ConfigurationTemplate template = GServerInterface.getServer().getConfigurationTemplate(entityType, templateName);
		final GIConfigPane pane = new GConfigTemplatePane(entityType,template);
		configFetched(pane);
		return pane;
	}
	
	public List<TemplateRefType> getConfigTemplates() {
		if (configTemplates==null) {
			GetConfigSpecResponse configSpec = GServerInterface.getServer().getConfigSpec(name);
			configTemplates = CommonUtils.convertToList(configSpec.getContent().getConfig_templates());
			errorTemplates = configSpec.getContent().getError_templates();
		}
		
		return new ArrayList<TemplateRefType>(configTemplates);
	}
	
	@Override
	public List<String> getTemplateErrors() {
		getConfigTemplates();
		return CommonUtils.convertToList(errorTemplates);
	}

	
	public void refresh() {
		this.configTemplates = null;
		this.refresh = true;
	}

	private List<GIGetConfigListener> listeners = new ArrayList<GIGetConfigListener>();


	public void addGetConfigListener(GIGetConfigListener listener) {
		listeners.add(listener);
	}

	public void removeGetConfigListener(GIGetConfigListener listener) {
		listeners.remove(listener);
	}


	private void configFetched(GIConfigPane pane) {
		for (GIGetConfigListener l : listeners)
			l.ConfigFetched(pane);
	}

	public boolean isCreateEnabled() {
		return createEnabled;
	}


	public boolean isEditEnabled() {
		return editEnabled;
	}
	
	@Override
	public boolean isEditPossible() {
		return editPossible;
	}
	
	
	public boolean isDeleteEnabled() {
		return deleteEnabled;
	}


	@Override
	public List<GIElement> getRestrictions(GIElement element, String restrictionType) {
		List<GIModelElement> restrictions = GServerInterface.getServer().getRestrictions(element, restrictionType);
		return new ArrayList<GIElement>(restrictions);
	}


	@Override
	public void updateRestrictions(GIElement element, String restrictionType, List<GIElement> chosenElements) {
		GServerInterface.getServer().updateRestrictions(element, restrictionType, chosenElements);
	}



}
