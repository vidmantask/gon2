package gon_client_management.model;

import gon_client_management.model.ext.GConfigPane;
import gon_client_management.model.ext.GIDetails;
import gon_client_management.model.ext.GIElement;
import gon_client_management.model.ext.GIFieldAction;
import gon_client_management.model.ext.GOperationNotAllowedException;
import gon_client_management.model.server.GServerInterface;
import gon_client_management.view.administrator.GElementUpdateHandler;

import java.util.List;

import server_config_ws.TestConfigSpecificationResponse;
import admin_ws.CreateConfigTemplateRowResponse;

import com.giritech.admin_ws.types_config_template.ConfigurationTemplate;
import com.giritech.admin_ws.types_config_template.Test_config_action;
import com.giritech.admin_ws.types_config_template.Value_selection;


public class GConfigTemplatePane extends GConfigPane {


	private boolean readOnly;



	public GConfigTemplatePane(String entityType, ConfigurationTemplate template) {
		super(entityType, template);
		readOnly = false;
	}



	public GConfigTemplatePane(GIElement element, ConfigurationTemplate template) {
		super(element, template);
		readOnly = template.getRead_only();
	}



	private class GFieldAction implements GIFieldAction {

		@Override
		public Value_selection portScan(String name, List<String> portNumbers) {
			return GServerInterface.getServer().portScan(name, portNumbers);
		}

		@Override
		public TestConfigSpecificationResponse testConfigSpec(
				ConfigurationTemplate template, Test_config_action action) {
			throw new RuntimeException("Not implemented");
		}
		
		
	}
	
	

	public void save() throws GOperationNotAllowedException {
		configTemplateUtil.updateTemplateValues();
		if (configElement!=null) {
			ConfigurationTemplate newTemplate = GServerInterface.getServer().updateConfigRow(configElement, configTemplateUtil.template);
			this.init(configElement.getEntityType(), newTemplate);
			GElementUpdateHandler.getElementUpdateHandler().fireElementUpdated(entityType, configElement.getElementId());
			
		}
		else {
			CreateConfigTemplateRowResponse createConfigRowResponse = GServerInterface.getServer().createConfigRow(entityType, configTemplateUtil.template);
			final String id = createConfigRowResponse.getContent().getElement_id();
			configElement = new GAbstractModelElement() {

				public String getEntityType() { return entityType; }

				public String getInfo() { return ""; }

				public String getLabel() { return "";}

				public void setLabel(String label) {}

				public String getElementType() { return "";}

				public String getId() { return id; }
				
			};
			this.init(configElement.getEntityType(), createConfigRowResponse.getContent().getTemplate());
			GElementUpdateHandler.getElementUpdateHandler().fireElementCreated(entityType, id);
		}
		
	}



	@Override
	protected GIFieldAction getFieldAction() {
		return new GFieldAction();
	}



	@Override
	public boolean isReadOnly() {
		return readOnly;
	}



	@Override
	public List<GIDetails> getDetails() {
		return configTemplateUtil.getDetails();
	}


}
