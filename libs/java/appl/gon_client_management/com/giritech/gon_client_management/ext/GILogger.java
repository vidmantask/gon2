package gon_client_management.ext;

public interface GILogger {

	public void log(int severity, String message);

	public void logError(String message);

	public void logInfo(String message);

	public void logException(Throwable t);

}