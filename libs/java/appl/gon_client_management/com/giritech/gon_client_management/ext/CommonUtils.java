package gon_client_management.ext;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

public class CommonUtils {
	
	
	public static <E> List<E> convertToList(E[] array) {
		List<E> list = new ArrayList<E>();
		if (array!=null)
			for(E e : array) 
				list.add(e);
		return list;
		
	}
	
	
	public static String[] convertToArray(List<String> list) {
		String [] cmdArray = new String[list.size()];
		for(int i=0; i<list.size(); i++) {
			cmdArray[i] = list.get(i);
		}
		return cmdArray;
	}

	public static String getStackTrace(Throwable aThrowable) {
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    aThrowable.printStackTrace(printWriter);
	    return result.toString();
	  }

	// Pair class - stolen from
	// http://www.factsandpeople.com/facts-mainmenu-5/8-java/10-java-pair-class?start=1
	public static class Pair<A, B> {

		public A fst;
		public B snd;

		public Pair(A fst, B snd) {
			this.fst = fst;
			this.snd = snd;
		}

		public A getFirst() {
			return fst;
		}

		public B getSecond() {
			return snd;
		}

		public void setFirst(A v) {
			fst = v;
		}

		public void setSecond(B v) {
			snd = v;
		}

		public String toString() {
			return "Pair[" + fst + "," + snd + "]";
		}

		private static boolean equals(Object x, Object y) {
			return (x == null && y == null) || (x != null && x.equals(y));
		}

		@SuppressWarnings("unchecked")
		public boolean equals(Object other) {
			return other instanceof Pair && 
			       equals(fst, ((Pair) other).fst) &&
			       equals(snd, ((Pair) other).snd);
		}

		public int hashCode() {
			if (fst == null)
				return (snd == null) ? 0 : snd.hashCode() + 1;
			else if (snd == null)
				return fst.hashCode() + 2;
			else
				return fst.hashCode() * 17 + snd.hashCode();
		}

		public static <A, B> Pair<A, B> of(A a, B b) {
			return new Pair<A, B>(a, b);
		}
	}
	
	
	public static class ProcessHandler {
		private Process child = null;
		private boolean continuously;
		private String dir;
		private String cmd;
		private Thread t = null;
		private boolean stop;
		private GILogger logger;
		private String[] cmdArray;
		
		public ProcessHandler(final String cmd, final String dir, boolean runContinuously, GILogger logger) {
			this.continuously = runContinuously;
			this.cmd = cmd;
			this.dir = dir;
			this.stop = false;
			this.logger = logger;
		}

		public ProcessHandler(final String[] cmdArray, final String dir, boolean runContinuously, GILogger logger) {
			this.continuously = runContinuously;
			this.cmdArray = cmdArray;
			this.dir = dir;
			this.stop = false;
			this.logger = logger;
		}

		public void stop() {
			if (child!=null) {
				this.continuously = false;
				this.stop = true;
				child.destroy();
			}
		}

		public void start() {
			// First check whether the process is alredy running
			if (t!=null && t.isAlive())
				return;
			t = new Thread() {

				@Override
				public void run() {
					final File fileDir = (dir==null) ? null : new File(dir);
					int failExecCount = 0;
					// Try starting the process - if it fails more than 3 times we give up
					while(!stop) {
						try {
							if (cmd!=null)
								child = Runtime.getRuntime().exec(cmd, null, fileDir);
							else
								child = Runtime.getRuntime().exec(cmdArray, null, fileDir);
							break;
						} catch (IOException e) {
							failExecCount++;
							logger.logException(e);
							if (failExecCount > 3)
								return;
							synchronized(e) {
								try {
									e.wait(500);
								} catch (InterruptedException e1) {
								}
							}
						}
					}
					// Process has now been started - now we should wait for it to finish
					while(!stop) {
						try {
							child.waitFor();
							// Check exit value and log potential error code 
							final int errorCode = child.exitValue();
							if (errorCode!=0) {
								logger.logError("Command '" + cmd + "' failed with error code " + errorCode);
							}
							break;
							
						} catch (InterruptedException e) {
							// Ignore - just continue waiting if we have not been told to stop 
						}
					}
					// Process has stopped - restart it if we should
					if (continuously)
						throw new RuntimeException("Restarting " + cmd);
				}
				
			};
			// Set exception handler to log the exception and then restart the process if it should run continuously 
			UncaughtExceptionHandler exceptionHandler = new Thread.UncaughtExceptionHandler() {

				public void uncaughtException(Thread t, Throwable e) {
					logger.logException(e);
					if (continuously) {
						ProcessHandler.this.t = null;
						start();
					}
				}
				
			};
			t.setUncaughtExceptionHandler(exceptionHandler);
			t.start();
		}

		public void setCommand(String cmd) {
			this.cmd = cmd;
			
		}
		
	}
	
	

	public static String convertFileToString(String fileName, String filePath) throws IOException {
		File file;
		if (filePath==null)
			file = new File(fileName);
		else
			file = new File(filePath, fileName);
		FileReader fileReader = new FileReader(file);
		return convertToString(fileReader);
//		return "";
		
	}


	private static String convertToString(Reader fileReader)
			throws IOException {
		char[] charBuf = new char[1];
		StringBuffer strBuf = new StringBuffer();
		int read;
		do {
			read = fileReader.read(charBuf);
			if (read>0)
				strBuf.append(charBuf);
		} while (read>0);
		
		return strBuf.toString();
	}

	public static String convertFileToString(String fileName, String filePath, String encoding) throws IOException {
		File file;
		if (filePath==null)
			file = new File(fileName);
		else
			file = new File(filePath, fileName);
		FileInputStream fileInputStream = new FileInputStream(file);
		InputStreamReader fileReader = new InputStreamReader(fileInputStream, encoding);
		return convertToString(fileReader);
		
	}

	public static class DateTimeConverter {
		static String dtformat = "yyyy-MM-dd'T'HH:mm:ss";
		static String tformat = "HH:mm:ss";
		static String dformat = "yyyy-MM-dd";
		
		public static String date2string(Date value_datetime) {
			DateFormat dateInstance = new SimpleDateFormat(dformat);
			return dateInstance.format(value_datetime);
		}

		public static String datetime2string(Date value_datetime) {
			DateFormat datetimeInstance = new SimpleDateFormat(dtformat);
			return datetimeInstance.format(value_datetime);
		}

		public static String time2string(Date value_datetime) {
			DateFormat timeInstance = new SimpleDateFormat(tformat);
			return timeInstance.format(value_datetime);
		}

		public static Date string2date(String value) throws ParseException {
			DateFormat dateInstance = new SimpleDateFormat(dformat);
			return dateInstance.parse(value);
		}

		public static Date string2datetime(String value) throws ParseException {
			DateFormat dateInstance = new SimpleDateFormat(dtformat);
			return dateInstance.parse(value);
		}

		public static Date string2time(String value) throws ParseException {
			DateFormat dateInstance = new SimpleDateFormat(tformat);
			return dateInstance.parse(value);
		}
		
		public static String datetime2local(Date value_datetime) {
			DateFormat dateInstance = new SimpleDateFormat();
			return dateInstance.format(value_datetime);
			
		}
		
		
	};

	/*
	 * Class for disable https client side hostname checking, because fetching data
	 * for birt reports faile with the error:
	 *    java.security.cert.CertificateException: No subject alternative names present
	 * 
	 * Taken from http://jafag.blogspot.com/2008/12/java-ssl-no-subject-alternative-matched.html
	 */
	private static class CustomizedHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}
	
	private static boolean trySetSSLProperties(String path, String cacertsFileName) {
		File cacerts = new File(path, cacertsFileName);
		if (cacerts.exists()) {
			System.setProperty("javax.net.ssl.trustStore", cacerts.getAbsolutePath());
			System.setProperty("javax.net.ssl.trustStorePassword", "Giritech");
			HttpsURLConnection.setDefaultHostnameVerifier(new CustomizedHostnameVerifier());
			System.out.print("TWA, trySetSSLProperties ok "+cacertsFileName );
			return true;
		}
		return false;
		
	}
	
	public static void setSSLProperties(String cacertsFileName) {
		boolean ok = trySetSSLProperties(System.getProperty("user.dir"), cacertsFileName);
		if (!ok) {
			try {
				File executable = new File(System.getProperty("eclipse.launcher"));
				String absolutePath = executable.getParentFile().getAbsolutePath();
				ok = trySetSSLProperties(absolutePath, cacertsFileName);
			}
			catch (Throwable t) {
				// ignore
			}
		}
		if (!ok) {
			trySetSSLProperties(System.getProperty("osgi.instance.area"), cacertsFileName);
		}
	}
	
	
	

	public static <E> E[] selectionSort(E[] array, Comparator<E> comparator) {
		List<E> tmpList = new ArrayList<E>();
		for(int i=0; i<array.length-1; i++) {
			for(int j=i+1; j<array.length; j++) {
				int result = comparator.compare(array[i], array[j]);
				if (result>0) {
					tmpList.add(array[i]);
					array[i] = array[j];
					array[j] = tmpList.remove(0);
				}
			}
		}
		return array;
		
	}

}
