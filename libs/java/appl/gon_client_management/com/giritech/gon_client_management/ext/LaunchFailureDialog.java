package gon_client_management.ext;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.FormDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

public abstract class LaunchFailureDialog extends FormDialog {

	public ScrolledForm parentForm;
	public Form dialogForm = null;
	Text serverHostname = null;
	Text serverPort = null;
	String title;
	
	public LaunchFailureDialog(Shell shell, String title) {
		super(shell);
		this.title = title;
	}

	@Override
	protected void createFormContent(IManagedForm mform) {
		this.parentForm = mform.getForm();
		parentForm.getShell().setText(title);
		
		this.dialogForm = (Form) mform.getForm().getContent();
		FormToolkit toolkit = mform.getToolkit();
		
		/* Setup layout for the main form. */
		GridLayout layout = new GridLayout();
		dialogForm.getBody().setLayout(layout);
		/* Setup the header for this dialog */
		dialogForm.setText("Server Connection Failed");
		toolkit.decorateFormHeading(dialogForm);
	
		/* Create a section for selection */
		Section selectionSection = toolkit.createSection(dialogForm.getBody(), Section.TITLE_BAR );
		selectionSection.setText("Server Address and Port");
		selectionSection.setLayout(new GridLayout());
		GridData selectionSectionGridData = new GridData(GridData.FILL_BOTH);
		selectionSection.setLayoutData(selectionSectionGridData);
	
		/* Create a container for inserting parts for this section. */
		Composite selectionContainer = toolkit.createComposite(selectionSection);
		selectionSection.setClient(selectionContainer);
		
		/* Create layout for the container. */
		GridLayout selectionContainerGridLayout = new GridLayout();
		selectionContainerGridLayout.numColumns = 2;
		selectionContainer.setLayout(selectionContainerGridLayout);
		GridData selectionContainerGridData = new GridData(GridData.FILL_BOTH);
		selectionContainer.setLayoutData(selectionContainerGridData);

		/* Description of the action to take. */
		Label desc = toolkit.createLabel(selectionContainer, "Adjust server address and port settings and press OK to restart.");
		GridData descrdata = new GridData(SWT.NONE, SWT.NONE, false, false);
		descrdata.horizontalSpan = 2;
		desc.setLayoutData(descrdata);		
		
		/* Create fields for changing the server name and port. */
		toolkit.createLabel(selectionContainer, "Server Address: ");
		this.serverHostname = toolkit.createText(selectionContainer, getServerHost(), SWT.BORDER);
		toolkit.createLabel(selectionContainer, "Server Port: ");
		this.serverPort = toolkit.createText(selectionContainer, getServerPort(), SWT.BORDER);

		/* Layout the fields. */
		GridData fielddata = new GridData(SWT.FILL, SWT.NONE, true, false);
		serverHostname.setLayoutData(fielddata);		
		serverPort.setLayoutData(fielddata);
	}

	@Override
	protected void okPressed() {
		setServerHost(serverHostname.getText());
		setServerPort(serverPort.getText());
		super.okPressed();
	}
	
	abstract protected String getServerHost();
	abstract protected void setServerHost(String host);

	abstract protected String getServerPort();
	abstract protected void setServerPort(String port);

}
