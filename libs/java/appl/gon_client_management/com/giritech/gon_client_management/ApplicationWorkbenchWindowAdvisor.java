package gon_client_management;

import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	private IWorkbenchWindowConfigurer configurer;
	
    public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
    	super(configurer);
    }

    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }
    
    public void postWindowOpen() {
		IStatusLineManager statusline = getWindowConfigurer().getActionBarConfigurer().getStatusLineManager();
		statusline.setMessage(null, "Ready");
    	super.postWindowOpen();
    	
    	IWorkbenchWindowConfigurer configurer = getWindowConfigurer();

    	if (GLoginDialog.userName!=null && GLoginDialog.userName.length()>0) 
			configurer.setTitle("G/On Management (" + GLoginDialog.userName + ")");
		else
			configurer.setTitle("G/On Management");

    }

    public void preWindowOpen() {
		configurer = getWindowConfigurer();
    	configurer.setInitialSize(new Point(800, 600));
    	configurer.setShowStatusLine(true);
    	configurer.setShowProgressIndicator(true);
		configurer.setShowPerspectiveBar(true);
		configurer.setShowCoolBar(true);
	}
}
