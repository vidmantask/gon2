package gon_client_management;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

public class ApplicationActionBarAdvisor extends ActionBarAdvisor {
	
	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
    }

	/**
	 * This fix is needed until rework of the command framework is
	 * completed by the friendly eclipse contributers.
	 * See: http://dev.eclipse.org/newslists/news.eclipse.platform.rcp/msg33818.html
	 */
	protected void makeActions(final IWorkbenchWindow window) {
		IWorkbenchAction resetPerspectiveAction = ActionFactory.RESET_PERSPECTIVE.create(window);
		register(resetPerspectiveAction);
		IWorkbenchAction action = ActionFactory.REFRESH.create(window);
		action.setEnabled(true);
		register(action);
	}

}
