package gon_client_management;

import gon_client_management.ext.CommonUtils;
import gon_client_management.model.server.GServerInterface;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.forms.FormDialog;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.giritech.admin_ws.LicenseCountType;
import com.giritech.admin_ws.LicenseInfoTypeManagement;

public class GViewLicenceInfoHandler implements IHandler {
	
	private static LicenseInfoTypeManagement licenseInfo = null;

	public static LicenseInfoTypeManagement getLicenseInfo() {
		return licenseInfo;
	}

	
	public static boolean isLicenseOk() {
		if (licenseInfo!=null) {
			return licenseInfo.getLicense_status_ok();
		}
		return false;
	}

	public static boolean isLicenseValid() {
		return (licenseInfo!=null);
	}
	
	public static boolean updateLicenseInfo() {
		try{
			licenseInfo = GServerInterface.getServer().getLicenseInfo();
		} 
		catch(Throwable t) {
			Activator.getLogger().logException(t);
			MessageDialog.openError(null, "Error getting licence info", t.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public void addHandlerListener(IHandlerListener handlerListener) {}
	@Override
	public void dispose() {}
	@Override
	public void removeHandlerListener(IHandlerListener handlerListener) {}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateLicenseInfo();
		LicenseDialog licenseDialog = new LicenseDialog(null);
		licenseDialog.open();
		return null;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isHandled() {
		return true;
	}
	
	
	public static class LicenseDialog extends FormDialog {

		private Label licenseNumberText;
		private Label licenseExpirationText;
		private Label licenseOwnerText;
		private Table table;
		int count = 0;
		
		private String[] titles = { "License restriction type", "No. Licensed", "No. Used"};;
		
		
		public LicenseDialog(Shell shell) {
			super(shell);
		}
		
		private String getDateString(String serverString) {
			if (serverString != null) {
				Date string2date;
				try {
					string2date = CommonUtils.DateTimeConverter.string2date(serverString);
					return CommonUtils.DateTimeConverter.datetime2local(string2date);
				} catch (ParseException e) {
					return "Error parsing date";
				}
				
			}
			else {
				return "N/A";
			}
		}

		@Override
		protected void createFormContent(IManagedForm mform) {
			super.createFormContent(mform);

			Form dialogForm = (Form) mform.getForm().getContent();
			FormToolkit toolkit = mform.getToolkit();
			
			/* Setup layout for the main form. */
			GridLayout layout = new GridLayout();
			dialogForm.getBody().setLayout(layout);
			/* Setup the header for this dialog */
			dialogForm.setText("License Information");
			toolkit.decorateFormHeading(dialogForm);
		
			GridData data = new GridData(SWT.FILL, SWT.FILL, true, false);
			dialogForm.setLayoutData(data);

			Composite licensesContainer = toolkit.createComposite(dialogForm.getBody());
			licensesContainer.setLayout(new GridLayout(2, false));
			
			toolkit.createLabel(licensesContainer, "License Number");
			licenseNumberText = toolkit.createLabel(licensesContainer, "a");
			
			toolkit.createLabel(licensesContainer, "License Expiration Date");
			licenseExpirationText = toolkit.createLabel(licensesContainer, "");

			toolkit.createLabel(licensesContainer, "Licensed To");
			licenseOwnerText = toolkit.createLabel(licensesContainer, "");

//			toolkit.createLabel(licensesContainer, "Maintenance Expiration Date");
//			toolkit.createLabel(licensesContainer, getDateString(licenseInfo.getLicense_expires()));

//			licensesContainer.setLayoutData(gridData);
			Label separator = toolkit.createSeparator(licensesContainer, SWT.HORIZONTAL);
			{
				GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
				gridData.horizontalSpan = 2;
				gridData.grabExcessHorizontalSpace = true;
				separator.setLayoutData(gridData);
			}

			table = toolkit.createTable(licensesContainer, SWT.NONE);
			table.setHeaderVisible(true);
			table.setLayout(new GridLayout());
			GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
			gridData.horizontalSpan = 2;
//			gridData.grabExcessHorizontalSpace = true;
			table.setLayoutData(gridData);
		    for (int i = 0; i < titles.length; i++) {
		      TableColumn column = new TableColumn(table, SWT.NONE);
//		      column.setWidth(50);
		      column.setText(titles[i]);
		    }
	        for (int i=0; i<titles.length; i++) {
	            table.getColumn (i).pack ();
	        }     
		    
	        
	        if (licenseInfo.getUpdate_license_allowed()) {
	        
//			toolkit.createLabel(licensesContainer, "Update License File");
				Button updateLicenseButton = toolkit.createButton(dialogForm.getBody(), "Update License File", SWT.PUSH);
				updateLicenseButton.setBounds(0, 0, 100, 100);
				updateLicenseButton.addSelectionListener(new SelectionAdapter() {
	
					String [] extensions = {"gon_license.lic", "*.lic", "*.*"};
					
					@Override
					public void widgetSelected(SelectionEvent event) {
						FileDialog fileChooser = new FileDialog(getParentShell(), SWT.OPEN);
						fileChooser.setFileName("gon_license.lic");
						fileChooser.setFilterExtensions(extensions);
						String filePath = fileChooser.open();
						if (filePath==null)
							return;
						String content;
						try {
							content = CommonUtils.convertFileToString(filePath, null, "UTF-8");
						} catch (IOException e) {
							throw new RuntimeException(e);
						}
						boolean licenseSet = GServerInterface.getServer().setLicense(content);
						if (licenseSet) {
							updateLicenseInfo();
							refresh();
						}
						else {
							MessageDialog.openError(getParentShell(), "Error", "Error updating license");
						}
					}
					
					
				});
	        }
			refresh();
			
		}

		private void createTableItem(Table table, String title) {
			TableItem item = new TableItem(table, SWT.NONE);
	        item.setText(0, title);
		}
		
		void setTableItemText(int index, String str1, String str2) {
			TableItem item = table.getItem(index);
			item.setText(1, str1);
			item.setText(2, str2);
		}
		
		protected void refresh() {
			table.removeAll();
			licenseNumberText.setText(licenseInfo.getLicense_number());
			licenseOwnerText.setText(licenseInfo.getLicensed_to());
			licenseExpirationText.setText(getDateString(licenseInfo.getLicense_expires()));
			
			int i = 0;
			for (LicenseCountType licenseCountType : licenseInfo.getCount_type_license()) {
		        createTableItem(table, licenseCountType.getItem_name());
				setTableItemText(i, licenseCountType.getLicensed_items().toString(), licenseCountType.getActual_items().toString());
		        i++;
			}
			count++;
//	        createTableItem(table, "Users");
//	        createTableItem(table, "Tokens");
//	        createTableItem(table, "Menu Actions");
	        
////	        table.setSize(table.computeSize(200, 100));
//			
//			setTableItemText(0, licenseInfo.getLicensed_users().toString(), licenseInfo.getActual_users().toString());
//			setTableItemText(1, licenseInfo.getLicensed_tokens().toString(), licenseInfo.getActual_tokens().toString());
//			setTableItemText(2, licenseInfo.getLicensed_menu_items().toString(), licenseInfo.getActual_menu_items().toString());

		}
		
	}
	


}
