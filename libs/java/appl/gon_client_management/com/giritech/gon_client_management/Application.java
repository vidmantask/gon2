package gon_client_management;

import gon_client_management.ext.CommonUtils;
import gon_client_management.ext.GILogger;
import gon_client_management.model.server.GServerInterface;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication {

	public static final String PLUGIN_ID = "gon_client_management";

	
	private static final String ARG_GATEWAY_SERVER_SESSION_ID = "-gateway_server_session_id";
	private static final String ARG_LOGIN = "-login";
	
	
	private final class GApplicationWorkbenchAdvisor extends ApplicationWorkbenchAdvisor {
		
		
		public GApplicationWorkbenchAdvisor(String pluginId) {
			super();
		}

		/* (non-Javadoc)
		 * @see org.eclipse.ui.application.WorkbenchAdvisor#eventLoopException(java.lang.Throwable)
		 */
		@Override
		public void eventLoopException(Throwable exception) {
			super.eventLoopException(exception);
			GUncaughtExceptionHandler.handleException(exception, PLUGIN_ID);
			//Application.this.stop();
		}
		
		

		@Override
		public void postStartup() {
			super.postStartup();
			
			IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			GPerspectiveChangeHandler.initialize(activeWorkbenchWindow);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		PlatformUI.getPreferenceStore().setDefault(IWorkbenchPreferenceConstants.KEY_CONFIGURATION_ID, "gon_client_management.scheme");
		Thread.setDefaultUncaughtExceptionHandler(new GUncaughtExceptionHandler(PLUGIN_ID));
		CommonUtils.setSSLProperties("gon_management_client.cacerts");
		Display display = PlatformUI.createDisplay();
		String[] args = (String[]) context.getArguments().get(IApplicationContext.APPLICATION_ARGS);
		GILogger logger = Activator.getLogger();
		
		logger.logInfo("SSL cert file: " + 	System.getProperty("javax.net.ssl.trustStore"));

		// TODO: To be removed before release
//		Properties properties = System.getProperties();
//		Set<Entry<Object, Object>> entrySet = properties.entrySet();
//		for (Entry<Object, Object> entry : entrySet) {
//			logger.logInfo(entry.getKey().toString() + ": " + entry.getValue().toString());
//		}
		
		int argIndex = getArgumentIndex(ARG_GATEWAY_SERVER_SESSION_ID, args)+1;
		if (argIndex>0 && argIndex<args.length) {
			GGlobalDefinitions.GATEWAY_SESSION_ID = args[argIndex];
			logger.logInfo("gateway_server_session_id=" + GGlobalDefinitions.GATEWAY_SESSION_ID);
		}
		else {
			logger.logInfo("gateway_server_session_id not specified");
			
		}
		
		boolean serverConnected = false;
		while(!serverConnected) {
			try {
				serverConnected = GServerInterface.getServer().Ping();
			}
			catch (Throwable t) {
				GManagementLaunchFailureDialog lfDialog = new GManagementLaunchFailureDialog(null);
				int rc = lfDialog.open();
				if (rc == Window.CANCEL) {
					System.exit(0);
				}
			}
		}

		/**
		 * Login if all seems good
		 */
		boolean alwaysLogin = false;
		argIndex = getArgumentIndex(ARG_LOGIN, args);
		if (argIndex >= 0){
			alwaysLogin = true;
		}
		GLoginDialog logindialog = new GLoginDialog(null, alwaysLogin);
		logindialog.open();
		
		boolean ok = GViewLicenceInfoHandler.updateLicenseInfo();
		if (!ok) {
			System.exit(1);
		}
		if (!GViewLicenceInfoHandler.isLicenseValid()) {
			MessageDialog.openError(null, "License Error", "Your G/On License is invalid");
			System.exit(1);
		}
		
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display, new GApplicationWorkbenchAdvisor(PLUGIN_ID));
			if (returnCode == PlatformUI.RETURN_RESTART)
				return IApplication.EXIT_RESTART;
			else
				return IApplication.EXIT_OK;

		} finally {
			display.dispose();
		}
		
	}
	
	


	private int getArgumentIndex(String argName, String[] args) {
		for (int i=0; i<args.length; i++) {
			String arg = args[i];
			if (arg.equals(argName)) {
				return i;
			}
		}
		return -1;
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}








}
