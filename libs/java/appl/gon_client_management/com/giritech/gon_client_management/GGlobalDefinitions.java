package gon_client_management;

public final class GGlobalDefinitions {

	public final static String GROUP_TYPE = "Group";
	public final static String USER_GROUP_TYPE = "UserGroup";
	public final static String GONUSERGROUP_TYPE = "GOnUserGroup";
	public final static String USER_TYPE = "User";
	public final static String TAG_TYPE = "Tag";
	public final static String TOKEN_TYPE = "Token";
	public final static String SOFTTOKEN_TOKEN_TYPE = "SoftToken"; 
	public final static String MICROSMART_TOKEN_TYPE = "MicroSmart";
	public final static String MICROSMARTSWISS_TOKEN_TYPE = "MicroSmartSwiss";
	public final static String MICROSMARTSWISS_2_TOKEN_TYPE = "MicroSmartSwissbit2";
	public final static String MICROSMARTSWISS_PE_TOKEN_TYPE = "MicroSmartSwissbitPE";
	public final static String KEY_ASSIGNMENT_TYPE = "PersonalTokenAssignment";
	public final static String TOKEN_GROUP_TYPE = "TokenGroup";
	public final static String TOKEN_GROUP_MEMBERSHIP_TYPE = "TokenGroupMembership";
	public final static String AUTHENTICATION_STATUS_TYPE = "AuthenticationStatus";
	public final static String REPORT_TYPE = "Report";
	public final static String LAUNCH_SPEC_TYPE = "ProgramAccess";
	public final static String MENU_ACTION_TYPE = "MenuAction";
	public static final String ENDPOINT_ASSIGNMENT_TYPE = "PersonalEndpointAssignment";
	public static final String ENDPOINT_TYPE = "Endpoint";
	public final static String ENDPOINT_GROUP_MEMBERSHIP_TYPE = "EndpointGroupMembership";
	public final static String ENDPOINT_GROUP_TYPE = "EndpointGroup";
	public final static String HAGIWARA_TOKEN_TYPE = "Hagiwara";
	public final static String MOBILE_TOKEN_TYPE = "Mobile";
	public static final String ANDROID_TOKEN_TYPE = "Android";
	public final static String ZONE_TYPE = "Zone";
	public final static String IP_RANGE_TYPE = "IpRange";
	public final static String ADMIN_ACCESS_TYPE = "AdminAccess";
	public final static String USER_OR_GROUP_TYPE = "UserOrGroup";
	public final static String SECURITY_STATUS_TYPE = "SecurityStatus";
	public final static String LOGIN_INTERVAL_TYPE = "LoginInterval";
	public static final String DEVICE_GROUP_MEMBERSHIP_TYPE = "DeviceGroupMembership";
	public static final String DEVICE_TYPE = "Device";
	public static final String DEVICE_GROUP_TYPE = "DeviceGroup";
	public static final String DME_TOKEN_TYPE = "DMEToken";

	public final static String PROPERTY_REGISTERED_USER = "registered_user";
	public final static String PROPERTY_UNMATCHED_REGISTERED_USER = "unmatched_registered_user";
	public final static String PROPERTY_RESTRICTED_ACTION = "restricted_action";
	
	public static String GATEWAY_SESSION_ID = null;
	
	
}
