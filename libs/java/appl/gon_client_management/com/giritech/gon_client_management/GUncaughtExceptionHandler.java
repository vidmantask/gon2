package gon_client_management;

import gon_client_management.view.administrator.GElementUpdateHandler;

import java.lang.Thread.UncaughtExceptionHandler;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class GUncaughtExceptionHandler implements UncaughtExceptionHandler {

	private final String pluginId;
	
	public GUncaughtExceptionHandler(String pluginId) {
		this.pluginId = pluginId;
	}
	
	public static void handleException(final Throwable e, final String pluginId) {
		Display display = GElementUpdateHandler.getDisplay();
		display.asyncExec(new Runnable() {
			
			@Override
			public void run() {
				Activator.getLogger().logException(e);
				String localizedMessage = e.getLocalizedMessage();
				if (localizedMessage==null)
					localizedMessage = e.toString();
				MessageDialog.openError(null, "Error", localizedMessage);	
				
			}
		});
	}

	public void uncaughtException(Thread t, Throwable e) {
		handleException(e, pluginId);
	}

}
