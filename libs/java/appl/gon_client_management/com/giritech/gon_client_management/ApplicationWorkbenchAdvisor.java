package gon_client_management;

import gon_client_management.model.GModelAPIFactory;
import gon_client_management.view.preferences.PreferenceConstants;

import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private static final String PERSPECTIVE_ID = "gon_client_management.perspective";

    public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        return new ApplicationWorkbenchWindowAdvisor(configurer);
    }

	public String getInitialWindowPerspectiveId() {
		IPerspectiveDescriptor defaultPerspective = PlatformUI.getWorkbench().getPerspectiveRegistry().findPerspectiveWithId(PERSPECTIVE_ID);
		if (defaultPerspective!=null) {
			return PERSPECTIVE_ID;
		}
		IPerspectiveDescriptor[] perspectives = PlatformUI.getWorkbench().getPerspectiveRegistry().getPerspectives();
		if (perspectives.length > 0) {
			return perspectives[0].getId();
		}
		return null;
//		return PERSPECTIVE_ID;
	}

	
	/**
	 * Change default settings to the ones we like.
	 */
	public void initialize(IWorkbenchConfigurer configurer) {
		super.initialize(configurer);
		/** Set the theme. The theme settings are located in MANIFEST.MF/extensions. */
		//PlatformUI.getWorkbench().getThemeManager().setCurrentTheme("gon_client_management.default_theme");
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.SHOW_TRADITIONAL_STYLE_TABS, false);
		PlatformUI.getPreferenceStore().setValue(IWorkbenchPreferenceConstants.PERSPECTIVE_BAR_EXTRAS,
			"gon_client_management.perspective2,gon_client_management.perspective,gon_client_management.perspective3");
		//"gon_client_management.perspective3,gon_client_management.AuthenticationPolicyPerspective,gon_client_management.perspective,gon_client_management.perspective5");
		// The key configuration setting is in the Application.start() method.  
		//PlatformUI.getPreferenceStore().setDefault(IWorkbenchPreferenceConstants.KEY_CONFIGURATION_ID, "gon_client_management.scheme");
		/** Save and restore the setup as the user left it, if it is the same user*/
		if (GLoginDialog.userName!=null) {
			String userName = PlatformUI.getPreferenceStore().getString(PreferenceConstants.G_LAST_USER_NAME);
			boolean restoreSettings = userName.equalsIgnoreCase(GLoginDialog.userName);
			configurer.setSaveAndRestore(restoreSettings);
		}
		else 
			configurer.setSaveAndRestore(false);
		/** Add shared images to the workbench register. */
		registerWorkBenchImages();
	}

	@Override
	public void postStartup() {
		super.postStartup();
		if (GLoginDialog.userName!=null) {
			PlatformUI.getPreferenceStore().setValue(PreferenceConstants.G_LAST_USER_NAME, GLoginDialog.userName);
			getWorkbenchConfigurer().setSaveAndRestore(true);
		}

		/**
		 * Remove unwanted preference pages.
		 */
		PreferenceManager pm = PlatformUI.getWorkbench().getPreferenceManager();
		pm.remove("org.eclipse.birt.report.designer.ui.preferences");
		pm.remove("org.eclipse.datatools.connectivity.ui.preferences.dataNode");
		/* Use this code to show the preferences (Keep for future reference). */
		//for (IPreferenceNode node : pm.getRootSubNodes()) {
		//	System.out.println(node.getId());
		//}
	}

	@Override
	public void postShutdown() {
		super.postShutdown();
		GModelAPIFactory.getModelAPI().close();
	}

	@Override
	public boolean preShutdown() {
		// TODO Auto-generated method stub
		return super.preShutdown();
	}




	/**
	 * Add images to the shared register.
	 */
	private void registerWorkBenchImages() {
//		IViewDescriptor[] views = PlatformUI.getWorkbench().getViewRegistry().getViews();
//		for (IViewDescriptor viewDescriptor : views) {
//			ImageDescriptor imageDescriptor = viewDescriptor.getImageDescriptor();
//			String id = viewDescriptor.getId();
//			String yo = "yo";
//		}
		
		/** G/On User Group */
		JFaceResources.getImageRegistry().put("G_GON_USER_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_user_group_gon_man_icon_16x16.png").createImage());
		/** User Group */
		JFaceResources.getImageRegistry().put("G_USER_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_user_group_man_icon_16x16.png").createImage());
		/** Users */
		JFaceResources.getImageRegistry().put("G_USER_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_user_man_icon_16x16.png").createImage());
		/** G/On Users */
		JFaceResources.getImageRegistry().put("G_GON_USER_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_user_gon_man_icon_16x16.png").createImage());
		/** G/On Users */
		JFaceResources.getImageRegistry().put("G_UNMATCHED_GON_USER_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_user_missing_man_icon_16x16.png").createImage());
		
		
		/** Tokens */
		JFaceResources.getImageRegistry().put("G_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_token_man_icon_16x16.png").createImage());
		/** Soft Tokens */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_SOFT_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_soft_token_man_icon_16x16.png").createImage());
		/** Personal Tokens */
		JFaceResources.getImageRegistry().put("G_PERSONAL_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_personal_token_man_icon_16x16.png").createImage());
		
		/** GD Token */
		JFaceResources.getImageRegistry().put("G_GD_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_gd_token_man_icon_16x16.png").createImage());
		/** Swissbit Token */
		JFaceResources.getImageRegistry().put("G_SWISSBIT_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_swissbit_micro_smart_man_16x16.png").createImage());
		/** Swissbit 2 Token */
		JFaceResources.getImageRegistry().put("G_SWISSBIT_2_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_swissbit_2_micro_smart_man_16x16.png").createImage());
		/** Swissbit  PE Token */
		JFaceResources.getImageRegistry().put("G_SWISSBIT_PE_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_swissbit_pe_micro_smart_man_16x16.png").createImage());
		/** Smartcard Token */
		JFaceResources.getImageRegistry().put("G_SMARTCARD_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_smartcard_token_man_icon_16x16.png").createImage());
		/** Hagiwara Token */
		JFaceResources.getImageRegistry().put("G_HAGIWARA_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_hagi_token_man_icon_16x16.png").createImage());
		/** Mobile(iPad/iPhone) Token */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_MOBILE_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_mobile_iphone_16x16.png").createImage());

		JFaceResources.getImageRegistry().put("G_ANDROID_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_mobile_android_16x16.png").createImage());
		
		/** Endpoint */
		JFaceResources.getImageRegistry().put("G_ENDPOINT_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_desktop_token_man_icon_16x16.png").createImage());
		/** Endpoint Group */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_ENDPOINT_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_desktop_token_man_icon_16x16.png").createImage());
		/** Personal Device */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_PERSONAL_DEVICE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_personal_device.ico").createImage());
		
		/** Reports */
		JFaceResources.getImageRegistry().put("G_REPORT_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_report_man_icon_16x16.png").createImage());
		/** Menu Action */
		JFaceResources.getImageRegistry().put("G_MENU_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_menu_action_man_icon_16x16.png").createImage());
		/** Restricted Menu Action */
		JFaceResources.getImageRegistry().put("G_RESTRICTED_MENU_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_restricted_menu_action_man_icon_16x16.png").createImage());
		/** Menu Actions - multiple */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_MENU_ACTION_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_no_icon.ico").createImage());
		/** Authentication Status */
		JFaceResources.getImageRegistry().put("G_AUTHENTICATION_STATUS_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_authn_status_man_icon_16x16.png").createImage());
		/** Tags */
		JFaceResources.getImageRegistry().put("G_TAG_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_tag_man_icon_16x16.png").createImage());
		/** Token Group */
		JFaceResources.getImageRegistry().put("G_TOKEN_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_token_group_man_icon_16x16.png").createImage());
		/** Folder */
		JFaceResources.getImageRegistry().put("G_FOLDER_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_folder_man_icon_16x16.png").createImage());		
		/** Auto folder */
		JFaceResources.getImageRegistry().put("G_AUTO_FOLDER_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_folder_all_man_icon_16x16.png").createImage());		
		/** Implies arrow */
		JFaceResources.getImageRegistry().put("G_IMPLIES_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_implies_arrow_16x16.png").createImage());
		/** Implies not arrow */
		JFaceResources.getImageRegistry().put("G_DOES_NOT_IMPLY_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_implies_arrow_inactive_16x16.png").createImage());

		/** DME Token Group */
		JFaceResources.getImageRegistry().put("G_DME_TOKEN_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_dme_man_icon_16x16.png").createImage());

		/** Access Definition **/
		JFaceResources.getImageRegistry().put("G_ADMIN_ACCESS_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_access_definition_man_icon_16x16.png").createImage());
		/** Operating System State **/
		JFaceResources.getImageRegistry().put("G_SECURITY_STATUS_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_oss_man_icon_16x16.png").createImage());
		/** Login Interval **/
		JFaceResources.getImageRegistry().put("G_LOGIN_INTERVAL_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_login_interval_man_icon_16x16.png").createImage());
		/** IP Range **/
		JFaceResources.getImageRegistry().put("G_IP_RANGE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_ip_man_icon_16x16.png").createImage());
		/** Zone **/
		JFaceResources.getImageRegistry().put("G_ZONE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_zone_man_icon_16x16.png").createImage());
		
		/** DEVICE */
		JFaceResources.getImageRegistry().put("G_DEVICE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_device_token_man_icon_16x16.png").createImage());
		/** DEVICE Group */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_DEVICE_GROUP_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_device_token_group_man_icon_16x16.png").createImage());
		
		/** Stop Action **/
		JFaceResources.getImageRegistry().put("G_STOP_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_stop_action_man_icon_16x16.png").createImage());
		/** Stop When None Action **/
		JFaceResources.getImageRegistry().put("G_STOP_WHEN_NONE_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_stop_when_none_action_man_icon_16x16.png").createImage());
		/** Restart Action **/
		JFaceResources.getImageRegistry().put("G_RESTART_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_restart_action_man_icon_16x16.png").createImage());
		/** Restart When None Action **/
		JFaceResources.getImageRegistry().put("G_RESTART_WHEN_NONE_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_restart_when_none_action_man_icon_16x16.png").createImage());
		/** Refresh Action **/
		JFaceResources.getImageRegistry().put("G_REFRESH_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_refresh_action_man_icon_16x16.png").createImage());
		/** Recalculate Menu Action **/
		JFaceResources.getImageRegistry().put("G_RECALCULATE_MENU_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_recalculate_menu_action_man_icon_16x16.png").createImage());
		/** Stop Fetching Elements Action **/
		JFaceResources.getImageRegistry().put("G_STOP_FETCHING_ACTION_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_stop_fetching_action_man_icon_16x16.png").createImage());
		
		/** Running State **/
		JFaceResources.getImageRegistry().put("G_RUNNING_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_running_state_man_icon_16x16.png").createImage());
		/** Stop State **/
		JFaceResources.getImageRegistry().put("G_STOP_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_stop_state_man_icon_16x16.png").createImage());
		/** Stop When None State **/
		JFaceResources.getImageRegistry().put("G_STOP_WHEN_NONE_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_stop_when_none_state_man_icon_16x16.png").createImage());
		/** Restart State **/
		JFaceResources.getImageRegistry().put("G_RESTART_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_restart_state_man_icon_16x16.png").createImage());
		/** Restart When None State **/
		JFaceResources.getImageRegistry().put("G_RESTART_WHEN_NONE_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_restart_when_none_state_man_icon_16x16.png").createImage());
		/** Unknown State **/
		JFaceResources.getImageRegistry().put("G_UNKNOWN_STATE_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_unknown_state_man_icon_16x16.png").createImage());
		
		
		
		/** No icon for missing icon items. */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_GIRITECH_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/giritech_icon_32bit_16x16.gif").createImage());
		
		/** No icon for missing icon items. */
		/* emo: NOT FIXED */
		JFaceResources.getImageRegistry().put("G_NO_ICON", AbstractUIPlugin.imageDescriptorFromPlugin("gon_client_management", "icons/g_no_icon.ico").createImage());
		
				
	}
}
