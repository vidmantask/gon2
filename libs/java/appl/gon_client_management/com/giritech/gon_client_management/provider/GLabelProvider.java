package gon_client_management.provider;

import gon_client_management.Activator;
import gon_client_management.GGlobalDefinitions;
import gon_client_management.model.GIRule;
import gon_client_management.model.ext.GIElement;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;

public class GLabelProvider extends ColumnLabelProvider {

	private int position;
	private boolean isRuleBased = false;
	private boolean isElementBased = false;
	private String rText;
	private final String defaultType;
	
	/**
	 * Initialize for use with elements from a rule.
	 * 
	 * @param type
	 * @param position
	 */
	public GLabelProvider(String type, int position) {
		super();
		this.position = position;
		this.isRuleBased = true;
		defaultType = type;
	}
	
	/**
	 * Initialize for use with single elements.
	 * 
	 * @param type
	 */
	public GLabelProvider(String type) {
		super();
		this.isElementBased = true;
		defaultType = type;
	}
	
	/**
	 * Find the type of each individual element. 
	 * This means that we can have different types in one column
	 * and use the icons to show that they are different.
	 * 
	 * @param element
	 * @return
	 */
	private String getElementType(Object element) {
		String elementType = defaultType;
		if (element != null) {
			if (this.isElementBased) {
				if (element instanceof GIElement)
					elementType = ((GIElement) element).getEntityType();
				else
					elementType = null;
			}
			else if (this.isRuleBased) {	
				if (position < 0)
					elementType = ((GIRule) element).getResultElement().getEntityType();
				else
					elementType = ((GIRule) element).getElement(position).getEntityType();
			}
		}

		return elementType;
	}
	/**
	 * Get the element this is called for.
	 * 
	 * @param obj
	 * @return
	 */
	private GIElement getElement(Object obj) {
		if (this.isElementBased && obj instanceof GIElement) {
			return ((GIElement) obj);
		}
		else if (this.isRuleBased && obj instanceof GIRule) {	
			if (position < 0)
				return ((GIRule) obj).getResultElement();
			else
				return ((GIRule) obj).getElement(position);
		}
		return null;
	}
	
	/**
	 * Get an image to put next to the label.
	 * Images are added to the image register in the 
	 * ApplicationWorkBenchAdvisor.
	 * 
	 * Note : element can be null
	 */
	@Override
	public Image getImage(Object element) {
		String elementType = getElementType(element);
		
		if (elementType==null)
			return null;

		/** If there is no text - there should be no image..*/
		if ( isRuleBased) { 
			GIElement ruleElement = getElement(element);
			if (ruleElement==null || ruleElement.getLabel().isEmpty())
				return null;
		}

		/** Get most precise element type **/
		if(elementType.contains("."))
			elementType = elementType.split("\\.",2)[1];
		
		/** Users and user groups */
		if (elementType.equals(GGlobalDefinitions.GONUSERGROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_GON_USER_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.GROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_USER_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.USER_GROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_USER_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.USER_TYPE)) {
			if (isRuleBased)
				return JFaceResources.getImageRegistry().get("G_GON_USER_ICON");
			if (element==null)
				return JFaceResources.getImageRegistry().get("G_USER_ICON");
			GIElement giElement = getElement(element);
			if (giElement.checkProperty(GGlobalDefinitions.PROPERTY_REGISTERED_USER))
				return JFaceResources.getImageRegistry().get("G_GON_USER_ICON");
			else if (giElement.checkProperty(GGlobalDefinitions.PROPERTY_UNMATCHED_REGISTERED_USER))
				return JFaceResources.getImageRegistry().get("G_UNMATCHED_GON_USER_ICON");
			else
				return JFaceResources.getImageRegistry().get("G_USER_ICON");
		}
		
		/** Tokens and token groups */
		else if (elementType.equals(GGlobalDefinitions.TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.SOFTTOKEN_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_SOFT_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.MICROSMART_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_GD_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.MICROSMARTSWISS_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_SWISSBIT_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.MICROSMARTSWISS_2_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_SWISSBIT_2_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.MICROSMARTSWISS_PE_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_SWISSBIT_PE_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.HAGIWARA_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_HAGIWARA_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.MOBILE_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_MOBILE_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.ANDROID_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_ANDROID_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.KEY_ASSIGNMENT_TYPE))
			return JFaceResources.getImageRegistry().get("G_PERSONAL_TOKEN_ICON");
		else if (elementType.equals(GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE))
			return JFaceResources.getImageRegistry().get("G_AUTHENTICATION_STATUS_ICON");
		else if (elementType.equals(GGlobalDefinitions.TOKEN_GROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_TOKEN_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE))
			return JFaceResources.getImageRegistry().get("G_TOKEN_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.DME_TOKEN_TYPE))
			return JFaceResources.getImageRegistry().get("G_DME_TOKEN_ICON");

		
		/** Computer user token */
		else if (elementType.equals(GGlobalDefinitions.ENDPOINT_TYPE))
			return JFaceResources.getImageRegistry().get("G_ENDPOINT_ICON");
		else if (elementType.equals(GGlobalDefinitions.ENDPOINT_ASSIGNMENT_TYPE))
			return JFaceResources.getImageRegistry().get("G_PERSONAL_DEVICE_ICON");
		else if (elementType.equals(GGlobalDefinitions.ENDPOINT_GROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_ENDPOINT_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.ENDPOINT_GROUP_MEMBERSHIP_TYPE))
			return JFaceResources.getImageRegistry().get("G_ENDPOINT_GROUP_ICON");
		
		/** Application and app groups */
		else if (elementType.equals( GGlobalDefinitions.MENU_ACTION_TYPE) ||
				 elementType.equals( GGlobalDefinitions.LAUNCH_SPEC_TYPE)) {
			GIElement giElement = getElement(element);
			if (element==null)
				return JFaceResources.getImageRegistry().get("G_MENU_ACTION_ICON");
			if (giElement.checkProperty(GGlobalDefinitions.PROPERTY_RESTRICTED_ACTION))
				return JFaceResources.getImageRegistry().get("G_RESTRICTED_MENU_ACTION_ICON");
			else
				return JFaceResources.getImageRegistry().get("G_MENU_ACTION_ICON");
		}
		
		/** Folders and tags */
		else if (elementType.equals( GGlobalDefinitions.TAG_TYPE))
			return JFaceResources.getImageRegistry().get("G_TAG_ICON");

		else if (elementType.equals( GGlobalDefinitions.ADMIN_ACCESS_TYPE))
			return JFaceResources.getImageRegistry().get("G_ADMIN_ACCESS_ICON");
		else if (elementType.equals( GGlobalDefinitions.SECURITY_STATUS_TYPE))
			return JFaceResources.getImageRegistry().get("G_SECURITY_STATUS_ICON");
		else if (elementType.equals( GGlobalDefinitions.LOGIN_INTERVAL_TYPE))
			return JFaceResources.getImageRegistry().get("G_LOGIN_INTERVAL_ICON");
		else if (elementType.equals( GGlobalDefinitions.ZONE_TYPE))
			return JFaceResources.getImageRegistry().get("G_ZONE_ICON");
		else if (elementType.equals( GGlobalDefinitions.IP_RANGE_TYPE))
			return JFaceResources.getImageRegistry().get("G_IP_RANGE_ICON");

		/** Computer token */
		else if (elementType.equals(GGlobalDefinitions.DEVICE_TYPE))
			return JFaceResources.getImageRegistry().get("G_DEVICE_ICON");
		else if (elementType.equals(GGlobalDefinitions.DEVICE_GROUP_TYPE))
			return JFaceResources.getImageRegistry().get("G_DEVICE_GROUP_ICON");
		else if (elementType.equals(GGlobalDefinitions.DEVICE_GROUP_MEMBERSHIP_TYPE))
			return JFaceResources.getImageRegistry().get("G_DEVICE_GROUP_ICON");
		
		else {
			Activator.getLogger().log(Status.WARNING, "Uncaught type: " + elementType);
			return JFaceResources.getImageRegistry().get("G_GIRITECH_ICON");
		}
	}

	/**
	 * Get text for the label depending on whether the 
	 * provider is rule based or element based.
	 */
	@Override
	public String getText(Object element) {
		if (this.isElementBased) {
			if (element instanceof GIElement) 
				rText = ((GIElement) element).getLabel();
			else
				rText = element.toString();
		}
		else if (this.isRuleBased) {	
			if (position < 0)
				rText = ((GIRule) element).getResultElement().getLabel();
			else
				rText = ((GIRule) element).getElement(position).getLabel();
		}
		else rText = "<empty>";
		return rText;
	}

	@Override
	public Image getToolTipImage(Object object) {
		return getImage(object);
	}

/*	@Override
	public Color getBackground(Object element) {
		if (!((GIRule) element).isActive() && position < 0)
			return PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_GRAY);			
		else
			return super.getBackground(element);
	}
*/
	
	@Override
	public Color getForeground(Object element) {
		if (this.isRuleBased) {	
			if (!((GIRule) element).isActive())
				return PlatformUI.getWorkbench().getDisplay().getSystemColor(SWT.COLOR_RED);			
			else
				return super.getBackground(element);
		}
		else {
			return super.getBackground(element);
		}
	}
	
	@Override
	public String getToolTipText(Object obj) {
		
		GIElement element = getElement(obj);
		if (element==null)
			return null;

		String infotext = element.getInfo();
		if (!infotext.isEmpty())
			return infotext; 
		
		String elementType = getElementType(obj);
		String text = "";
		
		if (elementType==null)
			return null;

		/** Get most precise element type **/
		if(elementType.contains("."))
			elementType = elementType.split("\\.",2)[1];
		
		/** If there is no text - there should be no type description. */
		if (getElement(obj).getInfo().isEmpty())
			text = "";
		/** Users and user groups */
		else if (elementType.equals(GGlobalDefinitions.GONUSERGROUP_TYPE))
			text = new String("G/On User Group\n"); 
		else if (elementType.equals(GGlobalDefinitions.GROUP_TYPE))
			text = new String("User Group\n");
		else if (elementType.equals(GGlobalDefinitions.USER_GROUP_TYPE))
			text = new String("User Group\n");
		else if (elementType.equals(GGlobalDefinitions.USER_TYPE))
			text = new String("Single User\n");
		/** Tokens and token groups */
		else if (elementType.equals(GGlobalDefinitions.SOFTTOKEN_TOKEN_TYPE))
			text = new String("SoftToken\n");
		else if (elementType.equals(GGlobalDefinitions.MICROSMART_TOKEN_TYPE))
			text = new String("MicroSmart\n");
		else if (elementType.equals(GGlobalDefinitions.KEY_ASSIGNMENT_TYPE))
			text = new String("Personal Token Status (Special Token Group)\n");
		else if (elementType.equals(GGlobalDefinitions.AUTHENTICATION_STATUS_TYPE))
			text = new String("Authentication Status\n");
		else if (elementType.equals(GGlobalDefinitions.TOKEN_GROUP_MEMBERSHIP_TYPE))
			text = new String("Token Group\n");

		/** Application and app groups */
		else if (elementType.equals( GGlobalDefinitions.MENU_ACTION_TYPE))
			text = new String("Menu Action\n");
		else if (elementType.equals( GGlobalDefinitions.LAUNCH_SPEC_TYPE))
			text = new String("Menu Action\n");
		
		/** Folders and tags */
		else if (elementType.equals( GGlobalDefinitions.TAG_TYPE))
			text = new String("Tag\n");
		
		String subtext = getElement(obj).getLabel();
		if (subtext.isEmpty())
			subtext = new String("Always true");
//		String infotext = getElement(obj).getInfo();
//		if (!infotext.isEmpty())
//			infotext = "\n" + infotext; 
		
		/** Add the elements label */
		text = text + subtext + infotext;
		return text;
	}

}
