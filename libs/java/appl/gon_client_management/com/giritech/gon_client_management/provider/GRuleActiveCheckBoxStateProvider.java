package gon_client_management.provider;

import gon_client_management.model.GIRule;

import org.eclipse.jface.viewers.ICheckStateProvider;

public class GRuleActiveCheckBoxStateProvider implements ICheckStateProvider {

	public GRuleActiveCheckBoxStateProvider() {
		super();
		//System.out.println("provider.GCheckBoxStateProvider: This should be hooked up the modelAPI. Need PWL's help here. ");
	}

	@Override
	public boolean isChecked(Object element) {
		if (element instanceof GIRule) {
			return ((GIRule) element).isActive();
			
		}
		return true;
	}

	@Override
	public boolean isGrayed(Object element) {
		return false;
	}
}
