package gon_client_management.provider;

import gon_client_management.model.server.GServerInterface;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.ui.AbstractSourceProvider;

import com.giritech.admin_ws.AccessDef;

public class MySourceProvider extends AbstractSourceProvider {

	public final static String MENU_ACTION = "menuAction1";
	public final static String ENABLED = "ENABLED";
	public final static String DISABLED = "DISABLED";
	public static final String VIEW_LiCENSE = "java_gon_client_management.Standard.GatewayConfig";
	public static boolean menActionEnabled = true;
	
	private Map<String, String> stateMap = null;

	public MySourceProvider() {
		super();
	}
	

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	private void createMap() {
		if (stateMap==null) {
			stateMap = new HashMap<String, String>();
//			stateMap.put("Report", ENABLED);
			AccessDef[] getAccessDefinitions = new AccessDef[0];
			try {
				getAccessDefinitions = GServerInterface.getServer().GetAccessDefinitions();
			}
			catch (Throwable t) {
			}
			for (AccessDef accessDef : getAccessDefinitions) {
				System.out.println(accessDef.getName() + ": " + accessDef.getRead_access());
				stateMap.put(accessDef.getName(), accessDef.getRead_access() ? ENABLED : DISABLED);
			}
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map getCurrentState() {
		createMap();
		return stateMap;
	}

	@Override
	public String[] getProvidedSourceNames() {
		createMap();
		Set<String> keySet = stateMap.keySet();
		String[] array = new String[keySet.size()];
		int i = 0;
		for (String string : keySet) {
			array[i] = string;
		}
		return array;
//		return new String[] {MENU_ACTION};

//		return new String[0];
	}
	
	public void refresh() {
		System.out.println("Yo there");
	}

    public void setVariableState(boolean flag) {
//        if(this.enabled == flag)
//            return; // no change 
//       this.enabled = flag;
//       String currentState =  enabled ? ENABLED : DISABLED;
//       fireSourceChanged(ISources.WORKBENCH, VARIABLE,
//               currentState);
   }
	
}
