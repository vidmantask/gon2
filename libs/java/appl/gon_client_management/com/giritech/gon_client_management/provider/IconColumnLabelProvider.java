package gon_client_management.provider;

import gon_client_management.model.GIRule;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

public class IconColumnLabelProvider extends ColumnLabelProvider {

	private String type = null;

	public IconColumnLabelProvider(String type) {
		super();
		this.type = type;
	}	

	@Override
	public Image getImage(Object element) {
		if (type.equals("Implies")) {
			if (((GIRule) element).isActive()) 
				return JFaceResources.getImageRegistry().get("G_IMPLIES_ICON");
			else
				return JFaceResources.getImageRegistry().get("G_DOES_NOT_IMPLY_ICON");
		}
		else
			return null;
	}

	@Override
	public String getText(Object element) {
		return "";
	}

	@Override
	public String getToolTipText(Object element) {
		if (type.equals( "Implies") && ((GIRule) element).isActive())
			return "This rule is active\n\n" +
					"If all premises to the left of this arrow are true,\n" +
					"the rule expresses that the result to the right is also true.\n\n" +
					"Right click to toggle the activation status.";
		else 
			return "This rule is NOT active\n\n" +
			       "When a rule is inactive, it will not be used to conclude\n" +
				   "anything about the result to the right of the arrow.\n\n" +
			       "Right click to toggle the activation status.";
	}

}
