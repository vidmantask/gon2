package gon_client_management.provider;

import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

import gon_client_management.model.GIModelReport;

public class ReportElementColumnLabelProvider extends ColumnLabelProvider {

	public ReportElementColumnLabelProvider(String type) {
	}

	@Override
	public String getText(Object element) {
		if (element instanceof GIModelReport) {
			GIModelReport report = (GIModelReport) element;
			return report.getTitle();
		}
		return null;
	}

	@Override
	public Image getImage(Object element) {
		return JFaceResources.getImageRegistry().get("G_REPORT_ICON");
	}
	
}
