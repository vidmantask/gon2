package gon_client_management.provider;

import gon_client_management.model.GIElementPane;
import gon_client_management.model.GIRulePane;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class GContentProvider implements IStructuredContentProvider { 
		
	final GIElementPane elements;
	final GIRulePane rules;

	/**
	 * Initialize the content provider for
	 * providing rules.
	 * @param pane with a number of rules.
	 */
	public GContentProvider(GIRulePane pane) {
		this.elements = null;
		this.rules = pane;
	}

	/**
	 * Initialize the content provider for 
	 * providing elements.
	 * 
	 * @param pane with a number of elements.f
	 */
	public GContentProvider(GIElementPane pane) {
		this.elements = pane;
		this.rules = null;
	}
	
	/**
	 * Get the elements depending on whether the provider
	 * has elements or rules.
	 */
	public Object[] getElements(Object inputElement) {
		if (this.elements != null) {
			Object[] array = elements.getElements().toArray();
			if (elements.resultTruncated()) {
				Object[] extendedArray = new Object[array.length+1];
				for(int i=0; i<array.length; i++)
					extendedArray[i] = array[i];
				extendedArray[array.length] = "...";
				return extendedArray;
			}
			return array;
		}
		else if (this.rules != null)
			return rules.getRules().toArray();
		else {
			System.out.println("Error found in GContentProvider.getElements()");
			return null;
		}
	}

	public void dispose() {
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

}
