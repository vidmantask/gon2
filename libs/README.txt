See http://giriwiki/Build_Process

Release
=============================
1) create folder e:\gon_5\build_store\5.6.0-7 on dev-build-host-1
2) Build and distribute platform specific versions (see below)



Release instructions windows:
=============================
1) Start dev-build-win-5-6
2) Start cmd on desktop
3) cd release_5_6
4) python config.py --cmd_release all
5) python config.py --cmd_release distribute


Release instructions fedora:
=============================
1) Start dev-build-win-5-6
2) Start cmd on desktop
3) cd release_5_6
4) python config.py --cmd_release all
5) python config.py --cmd_release distribute


