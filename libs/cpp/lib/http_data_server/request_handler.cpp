//
// request_handler.cpp
// ~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "request_handler.hpp"
#include <fstream>
#include <sstream>
#include <string>
#include <boost/lexical_cast.hpp>
#include "mime_types.hpp"
#include "reply.hpp"
#include "request.hpp"

namespace Giritech {
namespace HTTPDataServer {

request_handler::request_handler(
		  const Utility::CheckpointHandler::APtr& checkpoint_handler,
		  const std::string& data,
		  const std::string& data_key,
		  const std::string& data_content_type,
		  const std::string& url_filename)
  	  : data_(data),
  	    data_key_(data_key),
  	    data_content_type_(data_content_type),
  	    url_filename_(url_filename) {
}

void request_handler::handle_request(const request& req, reply& rep) {
	std::string request_path;
	if (!url_decode(req.uri, request_path))	{
		rep = reply::stock_reply(reply::forbidden);
		return;
	}

	std::string request_path_facit = "/" + data_key_ + "/" + url_filename_;
	if(request_path != request_path_facit) {
		rep = reply::stock_reply(reply::forbidden);
		return;
	}

	// Determine the file extension.
	std::size_t last_slash_pos = request_path_facit.find_last_of("/");
	std::size_t last_dot_pos = request_path_facit.find_last_of(".");
	std::string extension;
	if (last_dot_pos != std::string::npos && last_dot_pos > last_slash_pos) {
		extension = request_path_facit.substr(last_dot_pos + 1);
	}

	// Fill out the reply to be sent to the client.
	rep.status = reply::ok;
	rep.content.append(data_);
	rep.headers.resize(2);
	rep.headers[0].name = "Content-Length";
	rep.headers[0].value = boost::lexical_cast<std::string>(data_.size());
	rep.headers[1].name = "Content-Type";
	if(data_content_type_ == "") {
		rep.headers[1].value = mime_types::extension_to_type(extension);
	}
	else {
		rep.headers[1].value = data_content_type_;
	}

}

bool request_handler::url_decode(const std::string& in, std::string& out) {
	out.clear();
	out.reserve(in.size());
	for (std::size_t i = 0; i < in.size(); ++i) {
		if (in[i] == '%') {
			if (i + 3 <= in.size()) {
				int value = 0;
				std::istringstream is(in.substr(i + 1, 2));
				if (is >> std::hex >> value) {
					out += static_cast<char>(value);
					i += 2;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		else if (in[i] == '+') {
			out += ' ';
		}
		else {
			out += in[i];
		}
	}
	return true;
}

}
}
