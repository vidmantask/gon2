//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//


#include "server.hpp"

#include "lib/utility/UY_Checkpoint.hxx"
#include "HTTPDataServer_CheckpointAttr.hxx"

#include <boost/bind.hpp>
#include <iostream>


using namespace Giritech::Utility;

namespace Giritech {
namespace HTTPDataServer {

server::server(
		 const Utility::CheckpointHandler::APtr& checkpoint_handler,
		 const std::string& listen_host,
		 const unsigned long& listen_port,
		 const std::string& data,
		 const std::string& data_content_type,
		 const std::string& data_key,
		 const std::string& url_filename,
		 const boost::posix_time::time_duration& timeout):
		 data_key_(data_key),
		 url_filename_(url_filename),
		 io_service_(),
		 acceptor_(io_service_),
		 connection_manager_(),
		 request_handler_(checkpoint_handler, data, data_key_, data_content_type, url_filename),
		 checkpoint_handler_(checkpoint_handler),
		 state_(State_Initializing),
		 timeout_(timeout),
		 timeout_timer_(io_service_),
		 accept_pending_(false),
		 timeout_pending_(false) {

	// Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
	boost::asio::ip::tcp::endpoint endpoint;
	if (listen_host.empty()) {
		endpoint = boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::loopback(), listen_port);
	} else {
		endpoint = boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(listen_host), listen_port);
	}
	acceptor_.open(endpoint.protocol());
	acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	acceptor_.bind(endpoint);
	acceptor_.listen();

	start_accept();

	timeout_timer_.expires_from_now(timeout_);
	timeout_timer_.async_wait(boost::bind(&server::handle_accept_timeout, this, boost::asio::placeholders::error));
	timeout_pending_ = true;
}

server::APtr server::create(
		  const Utility::CheckpointHandler::APtr& checkpoint_handler,
		  const std::string& listen_host,
		  const unsigned long& listen_port,
		  const std::string& data,
		  const std::string& data_content_type,
		  const std::string& data_key,
		  const std::string& url_filename,
		  const boost::posix_time::time_duration& timeout) {
	return APtr(new server(checkpoint_handler, listen_host, listen_port, data, data_content_type, data_key, url_filename, timeout));
}

void server::run() {
	Checkpoint cp1(*checkpoint_handler_, "server::run.begin", Attr_HTTPDataServer(), CpAttr_debug());
	// The io_service::run() call will block until all asynchronous operations
	// have finished. While the server is running, there is always at least one
	// asynchronous operation outstanding: the asynchronous accept call waiting
	// for new incoming connections.
	io_service_.run();
	state_ = State_Done;
	Checkpoint cp2(*checkpoint_handler_, "server::run.end", Attr_HTTPDataServer(), CpAttr_debug());
}

void server::start_accept() {
	new_connection_ = connection_ptr(new connection(io_service_, connection_manager_, request_handler_));
	acceptor_.async_accept(new_connection_->socket(), boost::bind(&server::handle_accept, this, boost::asio::placeholders::error));
	state_ = State_Serving;
	accept_pending_ = true;
}

void server::stop() {
	// Post a call to the stop function so that server::stop() is safe to call
	// from any thread.
	io_service_.post(boost::bind(&server::handle_stop, this));
}

void server::handle_accept_timeout(const boost::system::error_code& e) {
	Checkpoint cp(*checkpoint_handler_, "server::handle_accept_timeout", Attr_HTTPDataServer(), CpAttr_debug());
	timeout_pending_ = false;
	if(accept_pending_) {
		acceptor_.cancel();
		return;
	}
	handle_stop();
}

void server::handle_accept(const boost::system::error_code& e) {
	Checkpoint cp(*checkpoint_handler_, "server::handle_accept", Attr_HTTPDataServer(), CpAttr_debug());
	accept_pending_ = false;
	if (!e) {
		connection_manager_.start(new_connection_);
		start_accept();
		return;
	}
	if(timeout_pending_) {
		timeout_timer_.expires_from_now(boost::posix_time::seconds(0));
		return;
	}
	handle_stop();
}

void server::handle_stop() {
	Checkpoint cp(*checkpoint_handler_, "server::handle_stop", Attr_HTTPDataServer(), CpAttr_debug());
	// The server is stopped by cancelling all outstanding asynchronous
	// operations. Once all operations have finished the io_service::run() call
	// will exit.
	acceptor_.close();
	connection_manager_.stop_all();
}

std::string server::get_url(void) {
	std::stringstream ss;
	ss << "http://";
	ss << acceptor_.local_endpoint().address().to_string() << ":" << acceptor_.local_endpoint().port();
	ss << "/" << data_key_;
	ss << "/" << url_filename_;
	return ss.str();
}

bool server::is_serving(void) {
	return state_ == State_Serving;
}

bool server::is_done(void) {
	return state_ == State_Done && !timeout_pending_;
}

}
}
