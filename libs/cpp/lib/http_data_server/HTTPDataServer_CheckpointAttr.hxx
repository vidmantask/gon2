/*! \file HTTPDataServer_CheckpointAttr.hxx
 \brief This file contains the global singelton for the module attribute HTTPDataServer
 */
#ifndef HTTPDataServer_CHECKPOINTATTR_HXX
#define HTTPDataServer_CHECKPOINTATTR_HXX

#include <lib/utility/UY_Checkpoint.hxx>

namespace Giritech {
namespace HTTPDataServer {

Utility::CheckpointAttr_Module::APtr Attr_HTTPDataServer(void);

}
}
#endif
