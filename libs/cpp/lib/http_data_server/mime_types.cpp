//
// mime_types.cpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "mime_types.hpp"

namespace Giritech {
namespace HTTPDataServer {
namespace mime_types {

struct mapping
{
  const char* extension;
  const char* mime_type;
} mappings[] =
{
  { "htm", "text/html; charset=utf-8" },
  { "html", "text/html; charset=utf-8" },
  { "txt", "text/plain; charset=utf-8" },
  { "gif", "image/gif" },
  { "jpg", "image/jpeg" },
  { "png", "image/png" },
  { "rdp", "application/x-rdp;  charset=utf-8"},
  { "ica", "application/x-ica;  charset=utf-8"},
  { "pdf", "application/pdf" },
  { 0, 0 } // Marks end of list.
};

std::string extension_to_type(const std::string& extension)
{
  for (mapping* m = mappings; m->extension; ++m)
  {
    if (m->extension == extension)
    {
      return m->mime_type;
    }
  }

  return "text/plain";
}

} // namespace mime_types
}
}
