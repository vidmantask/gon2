//
// server.hpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_SERVER_HPP
#define HTTP_SERVER_HPP

#include <boost/asio.hpp>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>

#include "connection.hpp"
#include "connection_manager.hpp"
#include "request_handler.hpp"

namespace Giritech {
namespace HTTPDataServer {

/// The top-level class of the HTTP server.
class server: private boost::noncopyable {
public:

    typedef boost::shared_ptr<server> APtr;

	  enum State {
		  State_Initializing,
		  State_Serving,
		  State_Closeing,
		  State_Done
	  };



  static APtr create(
		  const Utility::CheckpointHandler::APtr& checkpoint_handler,
		  const std::string& listen_host,
		  const unsigned long& listen_port,
		  const std::string& data,
		  const std::string& data_content_type,
		  const std::string& data_key,
		  const std::string& url_filename,
		  const boost::posix_time::time_duration& timeout);

  /// Run the server's io_service loop.
  void run();

  /// Stop the server.
  void stop();


  std::string get_url(void);

  bool is_serving(void);
  bool is_done(void);

private:
  /// Construct the server to listen on the specified TCP address and port, and
  /// serve up files from the given directory.
  server(
	  const Utility::CheckpointHandler::APtr& checkpoint_handler,
	  const std::string& listen_host,
	  const unsigned long& listen_port,
	  const std::string& data,
	  const std::string& data_content_type,
	  const std::string& data_key,
	  const std::string& url_filename,
	  const boost::posix_time::time_duration& timeout);

  /// Handle completion of an asynchronous accept operation.
  void handle_accept(const boost::system::error_code& e);
  void handle_accept_timeout(const boost::system::error_code& e);

  /// Handle a request to stop the server.
  void handle_stop();
  void start_accept();


  std::string data_key_;
  std::string url_filename_;

  /// The io_service used to perform asynchronous operations.
  boost::asio::io_service io_service_;

  /// Acceptor used to listen for incoming connections.
  boost::asio::ip::tcp::acceptor acceptor_;

  /// The connection manager which owns all live connections.
  connection_manager connection_manager_;

  /// The next connection to be accepted.
  connection_ptr new_connection_;

  /// The handler for all incoming requests.
  request_handler request_handler_;

  Utility::CheckpointHandler::APtr checkpoint_handler_;

  State state_;

  boost::posix_time::time_duration timeout_;
  boost::asio::deadline_timer timeout_timer_;
  bool accept_pending_;
  bool timeout_pending_;

};

}
}

#endif // HTTP_SERVER_HPP
