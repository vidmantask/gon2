/*! \file HTTPDataServer_CheckpointAttr.cxx
 \brief This file contains the global singelton for the module attribute communication
 */

#include "HTTPDataServer_CheckpointAttr.hxx"

using namespace Giritech;
using namespace Giritech::Utility;

static CheckpointAttr_Module::APtr componentConnection_;

CheckpointAttr_Module::APtr Giritech::HTTPDataServer::Attr_HTTPDataServer(void) {
    if (!componentConnection_) {
        componentConnection_ = CheckpointAttr_Module::create("HTTPDataServer");
    }
    return componentConnection_;
}
