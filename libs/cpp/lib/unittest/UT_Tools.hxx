/*! \file UT_Tools.hxx
 */
#ifndef UT_Tools_HXX
#define UT_Tools_HXX

#include <boost/thread.hpp>
#include <boost/filesystem.hpp>


namespace Giritech {
namespace Unittest {


template <class Predicate>
void wait_for(
		Predicate predicate,
		const int& timeout_sec=40) {

	while(!predicate()) {
    	boost::this_thread::sleep(boost::posix_time::seconds(1));
	}
}


boost::filesystem::path get_unittest_data_root(void) {
	boost::filesystem::path hg_root(boost::filesystem::path(__FILE__).parent_path() / ".." / ".." / "..");
	return boost::filesystem::absolute(hg_root) / "setup" / "unittest";
}


}
}
#endif
