/*! \file GPM_Spec.cxx
 \brief This file contains the implementaion of the Gpm class
 */
#include <sstream>
#include <iostream>
#include <string>

#include <lib/gpm/GPM_Spec.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Gpm;

/*
 ------------------------------------------------------------------
 GpmSpec
 ------------------------------------------------------------------
 */
GpmSpec::GpmSpec(void) {
}

GpmSpec::~GpmSpec() {
}

GpmSpec::APtr GpmSpec::create(void) {
    return GpmSpec::APtr(new GpmSpec());
}

void GpmSpec::load_from_file(const std::string& filename) {
    bool rc = gpm_spec_doc_.LoadFile(filename.c_str());
    if (!rc) {
        throw Exception_GpmSpec(gpm_spec_doc_.ErrorStr());
    }
}

void GpmSpec::load_from_string(const std::string& xml_document) {
    gpm_spec_doc_.Parse(xml_document.c_str());
    if (gpm_spec_doc_.Error()) {
        throw Exception_GpmSpec(gpm_spec_doc_.ErrorStr());
    }
}

void GpmSpec::get_ro_files(std::list<GpmSpec::GpmSpecFile>& files) {
    tinyxml2::XMLHandle doc_handler( &gpm_spec_doc_ );
    tinyxml2::XMLElement* gpm_spec_file = doc_handler.FirstChildElement("gpm").FirstChildElement("files_ro").FirstChildElement("file").ToElement();
    get_files_(files, gpm_spec_file);
}

void GpmSpec::get_rw_files(std::list<GpmSpec::GpmSpecFile>& files) {
    tinyxml2::XMLHandle doc_handler( &gpm_spec_doc_ );
    tinyxml2::XMLElement* gpm_spec_file = doc_handler.FirstChildElement("gpm").FirstChildElement("files_rw").FirstChildElement("file").ToElement();
    get_files_(files, gpm_spec_file);
}


void GpmSpec::get_files_(std::list<GpmSpec::GpmSpecFile>& files, tinyxml2::XMLElement* gpm_spec_file) {
    while(gpm_spec_file != NULL) {
        GpmSpecFile spec_file;
        spec_file.checksum = gpm_spec_file->Attribute("checksum");
        spec_file.dest = gpm_spec_file->Attribute("dest");
        gpm_spec_file->QueryDoubleAttribute("size", &(spec_file.size));
        files.push_back(spec_file);
        gpm_spec_file = gpm_spec_file->NextSiblingElement("file");
    }
}

