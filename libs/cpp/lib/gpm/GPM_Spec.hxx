/*! \file GPM_Spec.hxx
 \brief This file contains gpm sepecification lib
 */
#ifndef GPM_Spec_HXX
#define GPM_Spec_HXX

#include <string>
#include <list>
#include <boost/shared_ptr.hpp>

#include <tinyxml2.h>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

namespace Giritech {
namespace Gpm {

/*! \brief This class represent Gpm specification
 */
class GpmSpec {
public:
    typedef boost::shared_ptr<GpmSpec> APtr;

    class Exception_GpmSpec: public Giritech::Utility::Exception {
    public:
        Exception_GpmSpec(const std::string& message) :
            Exception(message) {
        }
    };

    struct GpmSpecFile {
        std::string dest;
        std::string checksum;
        double size;
    };


    /*! \brief Destructor
     */
    ~GpmSpec();

    /*! \brief Load document from file or string.
     *
     * If an error is found the exception Exception_GpmSpec is thrown
     */
    void load_from_file(const std::string& filename);
    void load_from_string(const std::string& xml_document);


    /*! \brief Get a list of ro-file/rw-file information found in the document
     *
     * If an error is found the exception Exception_GpmSpec is thrown
     */
    void get_ro_files(std::list<GpmSpecFile>& files);
    void get_rw_files(std::list<GpmSpecFile>& files);

    /*! \brief Create instance
     */
    static GpmSpec::APtr create(void);


private:
    GpmSpec(void);

    void get_files_(std::list<GpmSpecFile>& files, tinyxml2::XMLElement* gpm_spec_file);
    tinyxml2::XMLDocument gpm_spec_doc_;
};

}
}
#endif
