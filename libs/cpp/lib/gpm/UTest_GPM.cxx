/*! \file UTest_GPM.cxx
 \brief This file contains unittest suite for the Gpm functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>
#include <lib/gpm/GPM_Spec.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Gpm;

/*! /brief Unittest for the the basis specification gpm
 */
BOOST_AUTO_TEST_CASE( test_gpm_parse_spec ) {
    const char gpm_spec_001_data[] = "<?xml version='1.0'?>"
            "<gpm>"
            "<header name='gon_client'>"
            "  <version bugfix='1' build_id='1' main='5' minor='1' /><version_release main='1' />"
            "  <arch>win</arch>"
            "  <description lang='en' summary='G/On Client'>This package contains the G/On Client for the Windows platform.</description>"
            "</header>"
            "<files_ro size='15163053'>"
            "  <file checksum='20272c4dd8c0c7416b0e72a51d3fa195' dest='/gon_client/win/select.pyd' size='7680' />"
            "  <file checksum='0d4dacb5bc3d710be9d1b06effe704f3' dest='/gon_client/win/plugin_modules/client_ok/__init__.py' size='153' />"
            "  <folder dest='/gon_client/win/plugin_modules/hagiwara/client_runtime'/>"
            "</files_ro>"
            "</gpm>";

    GpmSpec::APtr gpm_spec_001(GpmSpec::create());
    gpm_spec_001->load_from_string(gpm_spec_001_data);

    std::list<GpmSpec::GpmSpecFile> ro_files;
    gpm_spec_001->get_ro_files(ro_files);
    BOOST_CHECK( ro_files.size() == 2);
    BOOST_CHECK( ro_files.front().dest == "/gon_client/win/select.pyd");
    BOOST_CHECK( ro_files.back().checksum == "0d4dacb5bc3d710be9d1b06effe704f3");

    std::list<GpmSpec::GpmSpecFile> rw_files;
    gpm_spec_001->get_rw_files(rw_files);
    BOOST_CHECK( rw_files.size() == 0);
}

BOOST_AUTO_TEST_CASE( test_gpm_parse_spec_error ) {
    const char gpm_spec_001_data[] = "<?xml version='1.0'?>"
            "<gpmxxx>"
            "</gpm>";

    GpmSpec::APtr gpm_spec_001(GpmSpec::create());
    BOOST_CHECK_THROW(gpm_spec_001->load_from_string(gpm_spec_001_data), GpmSpec::Exception_GpmSpec);
}



boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
