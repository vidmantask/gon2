/*! \file CF_KeyStore.cxx
    \brief This file contains the implementation for handling key generation and storage of keys
*/
#include <sstream>
#include <iostream>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_KeyStore.hxx>
#include <lib/utility/UY_String.hxx>

#include <osrng.h>
#include <oids.h>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

using namespace CryptoPP;


/* 
   ------------------------------------------------------------------ 
   KeyStore implementation
   ------------------------------------------------------------------ 
*/
KeyStore::KeyStore(boost::asio::io_service& io, const CryptFactory::APtr& cryptFactory) :
		cryptFactory_(cryptFactory),
		sessionKeyPairGenerated_(false),
		 mutex_(Mutex::create(io, "KeyStore")) {
}

KeyStore::~KeyStore(void) {
	burn();
}

void KeyStore::generateSessionKeypair(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) {
	try {
		MutexScopeLockAnonyme mutex_lock(mutex_);
		RandomNumberGenerator rng;

		PKCryptosystem::PrivateKey privateSessionKey;
		PKCryptosystem::PublicKey  publicSessionKey;
		privateSessionKey.GenerateRandomWithKeySize(rng, sessionKeySize);
		publicSessionKey.AssignFrom(privateSessionKey);

		sessionKeyPublic = saveKey<PKCryptosystem::PublicKey>(publicSessionKey);
		sessionKeyPrivate = saveKey<PKCryptosystem::PrivateKey>(privateSessionKey);
	}
	catch(const CryptoPP::Exception& e) {
		/*! \fips_stf Exception_SelftestFailed("Crypto++ selftest failed.", <Error message from Crypto++>) */
		throw CryptFacilityService::Exception_SelftestFailed("Crypto++ selftest failed.", e.what());
	}
}

void KeyStore::resetSessionKeypair(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(sessionKeyPublic_.get() != NULL) {
		sessionKeyPublic_->burn();
	}
	if(sessionKeyPublic_.get() != NULL) {
		sessionKeyPublic_->burn();
	}
	sessionKeyPairGenerated_ = false;
}

void KeyStore::setSessionKeypair(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(sessionKeyPublic.get() && (sessionKeyPublic->getSize() > 0) && sessionKeyPrivate.get() && (sessionKeyPrivate->getSize() > 0) ) {
		sessionKeyPublic_ = sessionKeyPublic;
		sessionKeyPrivate_ = sessionKeyPrivate;
		sessionKeyPairGenerated_ = true;
	}
}

DataBufferManaged::APtr KeyStore::getSessionKeyPublic(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(!sessionKeyPairGenerated_) {
		generateSessionKeypair(sessionKeyPublic_, sessionKeyPrivate_);
		sessionKeyPairGenerated_ = true;
	}
	return sessionKeyPublic_;
}

DataBufferManaged::APtr KeyStore::getSessionKeyPrivate(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(!sessionKeyPairGenerated_) {
		generateSessionKeypair(sessionKeyPublic_, sessionKeyPrivate_);
		sessionKeyPairGenerated_ = true;
	}
	return sessionKeyPrivate_;
}

void KeyStore::generateEncrypterKey(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	encrypterKey_ = cryptFactory_->generateRandom(symetricKeySize);
	encrypterIV_  = cryptFactory_->generateRandom(symetricKeySize);
}

void KeyStore::generateDecrypterKey(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	decryptionKey_ = cryptFactory_->generateRandom(symetricKeySize);
	decryptionIV_ = cryptFactory_->generateRandom(symetricKeySize);
}

void KeyStore::getEncrypterKey(Utility::DataBufferManaged::APtr& key, Utility::DataBufferManaged::APtr& iv) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	generateEncrypterKey();
	key = encrypterKey_;
	iv = encrypterIV_;
}

void KeyStore::getDecrypterKey(Utility::DataBufferManaged::APtr& key, Utility::DataBufferManaged::APtr& iv) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	generateDecrypterKey();
	key = decryptionKey_;
	iv = decryptionIV_;
}

KeyStore::APtr KeyStore::create(boost::asio::io_service& io, const CryptFactory::APtr& cryptFactory) {
	return APtr(new KeyStore(io, cryptFactory));
}

void KeyStore::burn(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
    if (decryptionKey_.get() != NULL) {
    	decryptionKey_->burn();
    }
    if (decryptionIV_.get() != NULL) {
    	decryptionIV_->burn();
    }
    if (encrypterKey_.get() != NULL) {
    	encrypterKey_->burn();
    }
    if (encrypterIV_.get() != NULL) {
    	encrypterIV_->burn();
    }
    if (sessionKeyPublic_.get() != NULL) {
    	sessionKeyPublic_->burn();
    }
    if (sessionKeyPrivate_.get() != NULL) {
    	sessionKeyPrivate_->burn();
    }
}
