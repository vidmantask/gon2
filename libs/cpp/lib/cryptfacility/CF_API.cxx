/*! \file CF_APIt.cxx
 \brief This file contains the implementation of a API wrapper for CryptFacility
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>
#include <lib/cryptfacility/CF_KE_SessionKeyExchange.hxx>
#include <lib/cryptfacility/CF_API.hxx>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#else
#  include <cryptopp/modes.h>
#  include <cryptopp/aes.h>
#  include <cryptopp/eccrypto.h>
#  include <cryptopp/rsa.h>
#  include <cryptopp/osrng.h>
#endif


using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace std;

boost::python::tuple Giritech::CryptFacility::APICryptFacility_pk_generate_keys(void) {
	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
	CryptFactory::PKPair key_pair(factory->pk_generate_keys());
	return boost::python::make_tuple(key_pair.first->encodeHex()->toString(), key_pair.second->encodeHex()->toString());
}
boost::python::tuple Giritech::CryptFacility::APICryptFacility_pk_generate_keys_rsa(void) {
	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
	CryptFactory::PKPair key_pair(factory->pk_generate_keys_rsa());
	return boost::python::make_tuple(key_pair.first->encodeHex()->toString(), key_pair.second->encodeHex()->toString());
}


string Giritech::CryptFacility::APICryptFacility_pk_sign_challenge(
		const std::string& private_key,
		const std::string& challenge) {
	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
	DataBufferManaged::APtr buf_private_key(DataBufferManaged::create(private_key)->decodeHex());
	DataBufferManaged::APtr buf_challenge(DataBufferManaged::create(challenge));
	DataBufferManaged::APtr buf_challenge_signature(factory->pk_sign_challenge(buf_private_key, buf_challenge));
	return buf_challenge_signature->encodeHex()->toString();
}

string Giritech::CryptFacility::APICryptFacility_pk_sign_challenge_rsa(
		const std::string& private_key,
		const std::string& challenge) {
	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
	DataBufferManaged::APtr buf_private_key(DataBufferManaged::create(private_key)->decodeHex());
	DataBufferManaged::APtr buf_challenge(DataBufferManaged::create(challenge));
	DataBufferManaged::APtr buf_challenge_signature(factory->pk_sign_challenge_rsa(buf_private_key, buf_challenge));
	return buf_challenge_signature->encodeHex()->toString();
}


bool Giritech::CryptFacility::APICryptFacility_pk_verify_challenge(
		const string& public_key,
		const string& challenge, const string& signature_of_challenge) {
	bool verified = false;
	try {
		boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
		DataBufferManaged::APtr buf_public_key(DataBufferManaged::create(public_key)->decodeHex());
		DataBufferManaged::APtr buf_challenge(DataBufferManaged::create(challenge));
		DataBufferManaged::APtr buf_signature_of_challenge(DataBufferManaged::create(signature_of_challenge)->decodeHex());
		verified = factory->pk_verify_challange(buf_public_key, buf_challenge, buf_signature_of_challenge);
	}
	catch (...) {
	}
	return verified;
}

bool Giritech::CryptFacility::APICryptFacility_pk_verify_challenge_rsa(
		const string& public_key,
		const string& challenge, const string& signature_of_challenge) {
	bool verified = false;
	try {
		boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
		DataBufferManaged::APtr buf_public_key(DataBufferManaged::create(public_key)->decodeHex());
		DataBufferManaged::APtr buf_challenge(DataBufferManaged::create(challenge));
		DataBufferManaged::APtr buf_signature_of_challenge(DataBufferManaged::create(signature_of_challenge)->decodeHex());
		verified = factory->pk_verify_challange_rsa(buf_public_key, buf_challenge, buf_signature_of_challenge);
	}
	catch (...) {
	}
	return verified;
}

boost::python::tuple Giritech::CryptFacility::APICryptFacility_generate_secrets(void) {
	DataBufferManaged::APtr clientKnownSecret;
	DataBufferManaged::APtr serverKnownSecret;
	KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);
	return boost::python::make_tuple(clientKnownSecret->encodeHex()->toString(), serverKnownSecret->encodeHex()->toString());
}

bool Giritech::CryptFacility::APICryptFacility_verify_public_key_signature(const string& public_key, const string& exp_s, const string& challenge, const string& challenge_signature) {
    CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Verifier verifier;
    CryptoPP::Integer m;
    m.Decode((const unsigned char*)public_key.c_str(), public_key.size());
    verifier.AccessKey().SetModulus(m);
    CryptoPP::Integer e;
    e.Decode((const unsigned char*)exp_s.c_str(), exp_s.size());
    verifier.AccessKey().SetPublicExponent(e);
    size_t signature_length = verifier.SignatureLength();
    return verifier.VerifyMessage(
            (const unsigned char*)(challenge.c_str()), challenge.size(),
            (const unsigned char*)(challenge_signature.c_str()), challenge_signature.size());
}
