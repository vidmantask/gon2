/*! \file CF_Hash_CryptoPP_UTest.cxx
 \brief This file contains unittest suite for the interface classe to Crypto++ library class hash functions
 */
#include <functional>
#include <time.h>
#include <iomanip>

#include <boost/test/unit_test.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
# include <lib/cryptfacility/CryptFacilityDLL.hxx>
#else
# include <lib/utility/UY_Exception.hxx>
# include <lib/utility/UY_DataBuffer.hxx>
# include <lib/cryptfacility/CF_Crypt.hxx>
# include <lib/cryptfacility/CF_CryptFactory.hxx>
# include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>
# include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#endif

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

BOOST_AUTO_TEST_CASE( sha1_block_hashing )
{
	Hash_CryptoPP_SHA1::APtr hashSHA1(Hash_CryptoPP_SHA1::create());
	BOOST_CHECK( hashSHA1->digestSize() == 20 );
}

BOOST_AUTO_TEST_CASE( sha1_calculate_and_verify_digest )
{
	Hash_CryptoPP_SHA1::APtr hashSHA1(Hash_CryptoPP_SHA1::create());
	DataBufferManaged::APtr buffer(DataBufferManaged::create("This buffer is filled with text"));
	DataBufferManaged::APtr digest(hashSHA1->calculateDigest(buffer));
	BOOST_CHECK(hashSHA1->verifyDigest(digest, buffer) );
}

BOOST_AUTO_TEST_CASE( sha1_calculate_and_verify_digest_for_file )
{
//    Hash_CryptoPP_SHA1::APtr hashSHA1(Hash_CryptoPP_SHA1::create("sha1"));
//    DataBufferManaged::APtr digest(hashSHA1->calculateDigestForFile("c:\\tmp\\xx.log"));
//    cout << digest->toString() << endl;
}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
	CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
	CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
	return 0;
}
