/*! \file CF_Crypt_CryptoPP_UTest.cxx
 \brief This file contains unittest suite for the interface classe to Crypto++ library class
 */
#include <functional>
#include <time.h>
#include <iomanip>
#include <iostream>

#include <boost/test/unit_test.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
# include <lib/cryptfacility/CryptFacilityDLL.hxx>
#else
# include <lib/utility/UY_Exception.hxx>
# include <lib/utility/UY_DataBuffer.hxx>
# include <lib/cryptfacility/CF_Crypt.hxx>
# include <lib/cryptfacility/CF_CryptFactory.hxx>
# include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
# include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#endif

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

BOOST_AUTO_TEST_CASE( AES_creation_destruction )
{
    Crypt_CryptoPP_Block<CryptoPP::AES>::APtr cryptAES(Crypt_CryptoPP_Block<CryptoPP::AES>::create(16));
    BOOST_CHECK( cryptAES->getKeyLen() == 16 );
}

BOOST_AUTO_TEST_CASE( AES_initialize_key_and_iv_from_random )
{
    RandomNumberGenerator rng;
    DataBufferManaged::APtr key(DataBufferManaged::create(500));
    DataBufferManaged::APtr iv(DataBufferManaged::create(500));

    rng.GenerateBlock(key->data(), key->getSize());
    rng.GenerateBlock(iv->data(), iv->getSize());
    BOOST_CHECK( memcmp(key->data(), iv->data(), key->getSize()) != 0);

    Crypt_CryptoPP_Block<CryptoPP::AES>::APtr cryptAES(Crypt_CryptoPP_Block<CryptoPP::AES>::create(32));
    cryptAES->setKeyWithIV(key->data(), iv->data());

    DataBufferManaged::APtr message(DataBufferManaged::create("12345678901234567890123456789012"));
    DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(32));
    DataBufferManaged::APtr messageDecrypted(DataBufferManaged::create(32));

    cryptAES->encrypt(message->data(), 32, messageEncrypted->data());
    cryptAES->decrypt(messageEncrypted->data(), 32, messageDecrypted->data());
    BOOST_CHECK( memcmp(message->data(), messageEncrypted->data(), 32) != 0);
    BOOST_CHECK( memcmp(message->data(), messageDecrypted->data(), 32) == 0);
}

BOOST_AUTO_TEST_CASE( AES_initialize_encrypt_decrypt )
{

    const byte_t key[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45,
        0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89,
        0xab, 0xcd, 0xef};
    const byte_t iv[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01,
        0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
        0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd,
        0xef};
    const byte_t plaintext[] = {0x4e, 0x6f, 0x77, 0x20, 0x69, 0x73, 0x20,
        0x74, 0x68, 0x65, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x20, 0x66, 0x6f,
        0x72, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x66, 0x6f, 0x72, 0x20,
        0x61, 0x6c, 0x6c, 0x20};
    byte_t ciphertext[32];
    byte_t decrypted[32];
    Crypt_CryptoPP_Block<CryptoPP::AES>::APtr cryptAES(
            Crypt_CryptoPP_Block<CryptoPP::AES>::create(32));

    cryptAES->setKeyWithIV(key, iv);
    cryptAES->encrypt(plaintext, 32, ciphertext);
    cryptAES->decrypt(ciphertext, 32, decrypted);
    BOOST_CHECK(memcmp(plaintext, ciphertext, 32) != 0);
    BOOST_CHECK(memcmp(plaintext, decrypted, 32) == 0);
}

BOOST_AUTO_TEST_CASE( AES_ivalid_blocksize )
{
    const byte_t key[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45,
        0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89,
        0xab, 0xcd, 0xef};
    const byte_t iv[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01,
        0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
        0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd,
        0xef};
    const byte_t plaintext[] = {0x4e, 0x6f, 0x77, 0x20, 0x69, 0x73, 0x20,
        0x74, 0x68, 0x65, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x20, 0x66, 0x6f,
        0x72, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x66, 0x6f, 0x72, 0x20,
        0x61, 0x6c, 0x6c, 0x20};
    byte_t ciphertext[24];
    byte_t decrypted[24];

    Crypt_CryptoPP_Block<CryptoPP::AES>::APtr crypt(
            Crypt_CryptoPP_Block<CryptoPP::AES>::create(32));

    crypt->setKeyWithIV(key, iv);

    BOOST_CHECK_THROW(crypt->encrypt(plaintext, 24, ciphertext),
            Exception_CF_CryptoPP);
    BOOST_CHECK_THROW(crypt->decrypt(ciphertext, 24, decrypted),
            Exception_CF_CryptoPP);
}

BOOST_AUTO_TEST_CASE( stream_AES_initialize_encrypt_decrypt )
{

    const byte_t key[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45,
        0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89,
        0xab, 0xcd, 0xef};
    const byte_t iv[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01,
        0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67,
        0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd,
        0xef};
    const byte_t plaintext[] = {0x4e, 0x6f, 0x77, 0x20, 0x69, 0x73, 0x20,
        0x74, 0x68, 0x65, 0x20, 0x74, 0x69, 0x6d, 0x65, 0x20, 0x66, 0x6f,
        0x72, 0x20, 0x61, 0x6c, 0x6c, 0x20, 0x66, 0x6f, 0x72, 0x20,
        0x61};
    byte_t ciphertext[29];
    byte_t decrypted[29];
    Crypt_CryptoPP_Stream<CryptoPP::AES>::APtr crypt_stream_aes(
            Crypt_CryptoPP_Stream<CryptoPP::AES>::create(32));

    crypt_stream_aes->setKeyWithIV(key, iv);
    crypt_stream_aes->encrypt(plaintext, 29, ciphertext);
    crypt_stream_aes->decrypt(ciphertext, 29, decrypted);
    BOOST_CHECK(memcmp(plaintext, ciphertext, 29) != 0);
    BOOST_CHECK(memcmp(plaintext, decrypted, 29) == 0);
}

BOOST_AUTO_TEST_CASE( PKAES_creation_encrypt_decrypt )
{
    try {
        long keySize = 1024;
        RandomNumberGenerator rng;
        PKCryptosystem::PrivateKey privateKey;
        PKCryptosystem::PublicKey publicKey;

        privateKey.GenerateRandomWithKeySize(rng, keySize);
        publicKey.AssignFrom(privateKey);

        cout << "generate keys" << endl;
        Crypt_CryptoPP_PKAES::APtr encrypterPKAES(Crypt_CryptoPP_PKAES::create(keySize));
        Crypt_CryptoPP_PKAES::APtr decrypterPKAES(Crypt_CryptoPP_PKAES::create(keySize));
        cout << "save private keys" << endl;
        DataBuffer::APtr privateKeyRaw(saveKey<PKCryptosystem::PrivateKey>(privateKey));
        cout << "save public keys" << endl;
        DataBuffer::APtr publicKeyRaw(saveKey<PKCryptosystem::PublicKey>(publicKey));

        cout << "set keys privateKeySize:" << privateKeyRaw->getSize() << endl;
        decrypterPKAES->setDecryptionKey(privateKeyRaw);

        cout << "set keys publicKeySize:" << publicKeyRaw->getSize() << endl;
        encrypterPKAES->setEncryptionKey(publicKeyRaw);

        DataBufferManaged::APtr message(DataBufferManaged::create("Don't show this to anyone"));
        message->pkcsAdd(encrypterPKAES->getBlockSize());

        cout << "encrypting" << endl;
        DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(encrypterPKAES->getCipherBufferSize(message->getSize())));
        encrypterPKAES->encrypt(message->data(),message->getSize(), messageEncrypted->data());

        cout << "decrypting" << endl;
        DataBufferManaged::APtr messageDecrypted(DataBufferManaged::create(decrypterPKAES->getPlainBufferSize(message->getSize())));
        decrypterPKAES->decrypt(messageEncrypted->data(), messageEncrypted->getSize(), messageDecrypted->data());
        messageDecrypted->pkcsRemove(decrypterPKAES->getBlockSize());

        BOOST_CHECK( memcmp(message->data(), messageDecrypted->data(), message->getSize()) == 0);
        BOOST_CHECK( memcmp(message->data(), messageEncrypted->data(), message->getSize()) != 0);
    }
    catch (std::exception& e) {
        cout << e.what() << "\n";
        BOOST_FAIL(e.what());
    }
}
    
BOOST_AUTO_TEST_CASE( pk_signing_and_validation )
{
    try {
    	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());
    	
    	CryptFactory::PKPair key_pair_1(factory->pk_generate_keys());
    	CryptFactory::PKPair key_pair_2(factory->pk_generate_keys());

    	DataBufferManaged::APtr challenge_1(DataBufferManaged::create("My first challenge"));
    	DataBufferManaged::APtr challenge_2(DataBufferManaged::create("My second challenge"));

    	DataBufferManaged::APtr challenge_1_signature(factory->pk_sign_challenge(key_pair_1.second, challenge_1));
    	DataBufferManaged::APtr challenge_2_signature(factory->pk_sign_challenge(key_pair_2.second, challenge_2));

        BOOST_CHECK( factory->pk_verify_challange(key_pair_1.first, challenge_1, challenge_1_signature) == true);
        BOOST_CHECK( factory->pk_verify_challange(key_pair_2.first, challenge_2, challenge_2_signature) == true);
        BOOST_CHECK( factory->pk_verify_challange(key_pair_1.first, challenge_2, challenge_2_signature) == false);
        BOOST_CHECK( factory->pk_verify_challange(key_pair_1.first, challenge_1, challenge_2_signature) == false);
        BOOST_CHECK( factory->pk_verify_challange(key_pair_1.first, challenge_2, challenge_1_signature) == false);
    }
    catch (std::exception& e) {
        cout << e.what() << "\n";
        BOOST_FAIL(e.what());
    }
}


BOOST_AUTO_TEST_CASE( pk_signing_and_validation_rsa )
{
    try {
    	boost::shared_ptr<CryptFactory> factory(CryptFacilityService::getInstance().getCryptFactory());

    	CryptFactory::PKPair key_pair_1(factory->pk_generate_keys_rsa());
    	CryptFactory::PKPair key_pair_2(factory->pk_generate_keys_rsa());

    	DataBufferManaged::APtr challenge_1(DataBufferManaged::create("My first challenge"));
    	DataBufferManaged::APtr challenge_2(DataBufferManaged::create("My second challenge"));

    	DataBufferManaged::APtr challenge_1_signature(factory->pk_sign_challenge_rsa(key_pair_1.second, challenge_1));
    	DataBufferManaged::APtr challenge_2_signature(factory->pk_sign_challenge_rsa(key_pair_2.second, challenge_2));

        BOOST_CHECK( factory->pk_verify_challange_rsa(key_pair_1.first, challenge_1, challenge_1_signature) == true);
        BOOST_CHECK( factory->pk_verify_challange_rsa(key_pair_2.first, challenge_2, challenge_2_signature) == true);
        BOOST_CHECK( factory->pk_verify_challange_rsa(key_pair_1.first, challenge_2, challenge_2_signature) == false);
        BOOST_CHECK( factory->pk_verify_challange_rsa(key_pair_1.first, challenge_1, challenge_2_signature) == false);
        BOOST_CHECK( factory->pk_verify_challange_rsa(key_pair_1.first, challenge_2, challenge_1_signature) == false);

    }
    catch (std::exception& e) {
        cout << e.what() << "\n";
        BOOST_FAIL(e.what());
    }
}


boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
