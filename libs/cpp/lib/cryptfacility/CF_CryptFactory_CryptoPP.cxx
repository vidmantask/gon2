/*! \file CF_CryptFactory_CryptoPP.cxx
 \brief This file contains the implementation of the CryptFactory_CryptoPP class.
 */
#include <sstream>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
# include <cryptopp/dll.h>
# include <cryptopp/fips140.h>
# include <cryptopp/oids.h>
#else
# include <cryptopp/rsa.h>
# include <cryptopp/eccrypto.h>
# include <cryptopp/skipjack.h>
# include <cryptopp/osrng.h>
# include <cryptopp/oids.h>
#endif

#include <lib/utility/UY_String.hxx>

#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>
#include <lib/cryptfacility/CF_CryptFactory_CryptoPP.hxx>

using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace std;
using namespace CryptoPP;

/*
 ------------------------------------------------------------------
 CryptFactory_CryptoPP implementation
 ------------------------------------------------------------------
 */
CryptFactory_CryptoPP::CryptFactory_CryptoPP(void)
	:  CryptFactory("Crypto++") {
}

CryptFactory_CryptoPP::~CryptFactory_CryptoPP(void) {
}

CryptFactory_CryptoPP::APtr CryptFactory_CryptoPP::create(void) {
    return CryptFactory_CryptoPP::APtr(new CryptFactory_CryptoPP());
}

Crypt::APtr CryptFactory_CryptoPP::getCryptBlockAES256(void) const {
	return Crypt_CryptoPP_Block<CryptoPP::AES>::create(32);
}

Crypt::APtr CryptFactory_CryptoPP::getCryptStreamAES256(void) const {
	return Crypt_CryptoPP_Stream<CryptoPP::AES>::create(32);
}

Hash::APtr CryptFactory_CryptoPP::getHashSHA1(void) const {
	return Hash_CryptoPP_SHA1::create();
}


DataBufferManaged::APtr CryptFactory_CryptoPP::generateRandom(const long byteSize) {
    try {
        RandomNumberGenerator rng;
        DataBufferManaged::APtr result(DataBufferManaged::create(byteSize));
        rng.GenerateBlock(result->data(), result->getSize());
        return result;
    }
    catch(const CryptoPP::Exception& e) {
        /*! \fips_stf Exception_SelftestFailed("Crypto++ selftest failed.", e.what()) */
        throw CryptFacilityService::Exception_SelftestFailed("Crypto++ selftest failed.", e.what());
    }
}

std::pair<DataBufferManaged::APtr, DataBufferManaged::APtr> CryptFactory_CryptoPP::pk_generate_keys(void) {
    AutoSeededRandomPool rng;

    PKSignatureScheme::PrivateKey private_key;
    PKSignatureScheme::PublicKey public_key;

    private_key.Initialize(rng, ASN1::sect233k1());
    private_key.MakePublicKey(public_key);

/*
 *  Seems  to cause c++ unhandled exception when using the crypto++ fips dll
 */
#ifndef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
    public_key.AccessGroupParameters().SetPointCompression(true);
    public_key.AccessGroupParameters().SetEncodeAsOID(true);
    private_key.AccessGroupParameters().SetPointCompression(true);
    private_key.AccessGroupParameters().SetEncodeAsOID(true);
#endif

    string public_key_string;
	CryptoPP::StringSink public_key_string_sink(public_key_string);
	public_key.Save(public_key_string_sink);
	public_key_string_sink.MessageEnd();

	string private_key_string;
	CryptoPP::StringSink private_key_string_sink(private_key_string);
	private_key.Save(private_key_string_sink);
	private_key_string_sink.MessageEnd();

    std::pair<DataBufferManaged::APtr, Utility::DataBufferManaged::APtr> result = make_pair(DataBufferManaged::create(public_key_string), DataBufferManaged::create(private_key_string));
    return result;
}

DataBufferManaged::APtr CryptFactory_CryptoPP::pk_sign_challenge(const DataBufferManaged::APtr& private_key_buffer, const DataBufferManaged::APtr& challenge) {
	PKSignatureScheme::PrivateKey private_key(loadKey<PKSignatureScheme::PrivateKey>(private_key_buffer));
	PKSignatureScheme::Signer signer(private_key);
	assert(signer.SignatureLength() != 0);

	DataBufferManaged::APtr signature(DataBufferManaged::create(signer.SignatureLength()));

	RandomNumberGenerator rng;
	signer.SignMessage(rng, challenge->data(), challenge->getSize(), signature->data());
	return signature;
}

bool CryptFactory_CryptoPP::pk_verify_challange(const DataBufferManaged::APtr& public_key_buffer, const DataBufferManaged::APtr& challenge, const DataBufferManaged::APtr& signature_of_challenge) {
	PKSignatureScheme::PublicKey public_key(loadKey<PKSignatureScheme::PublicKey>(public_key_buffer));
	PKSignatureScheme::Verifier verifier(public_key);
	return verifier.VerifyMessage(challenge->data(), challenge->getSize(), signature_of_challenge->data(), signature_of_challenge->getSize());
}


std::pair<DataBufferManaged::APtr, DataBufferManaged::APtr> CryptFactory_CryptoPP::pk_generate_keys_rsa(void) {
	AutoSeededRandomPool rng;

	InvertibleRSAFunction parameters;
	parameters.GenerateRandomWithKeySize(rng, 3072);

	PKSignatureSchemeRSA::PrivateKey private_key(parameters);
	PKSignatureSchemeRSA::PublicKey public_key(parameters);

    string public_key_string;
	CryptoPP::StringSink public_key_string_sink(public_key_string);
	public_key.Save(public_key_string_sink);
	public_key_string_sink.MessageEnd();

	string private_key_string;
	CryptoPP::StringSink private_key_string_sink(private_key_string);
	private_key.Save(private_key_string_sink);
	private_key_string_sink.MessageEnd();

    std::pair<DataBufferManaged::APtr, Utility::DataBufferManaged::APtr> result = make_pair(DataBufferManaged::create(public_key_string), DataBufferManaged::create(private_key_string));
    return result;
}

DataBufferManaged::APtr CryptFactory_CryptoPP::pk_sign_challenge_rsa(const DataBufferManaged::APtr& private_key_buffer, const DataBufferManaged::APtr& challenge) {
	PKSignatureSchemeRSA::PrivateKey private_key(loadKey<PKSignatureSchemeRSA::PrivateKey>(private_key_buffer));
	PKSignatureSchemeRSA::Signer signer(private_key);
	assert(signer.SignatureLength() != 0);

	DataBufferManaged::APtr signature(DataBufferManaged::create(signer.SignatureLength()));

	RandomNumberGenerator rng;
	signer.SignMessage(rng, challenge->data(), challenge->getSize(), signature->data());
	return signature;
}

bool CryptFactory_CryptoPP::pk_verify_challange_rsa(const DataBufferManaged::APtr& public_key_buffer, const DataBufferManaged::APtr& challenge, const DataBufferManaged::APtr& signature_of_challenge) {
	PKSignatureSchemeRSA::PublicKey public_key(loadKey<PKSignatureSchemeRSA::PublicKey>(public_key_buffer));
	PKSignatureSchemeRSA::Verifier verifier(public_key);
	return verifier.VerifyMessage(challenge->data(), challenge->getSize(), signature_of_challenge->data(), signature_of_challenge->getSize());
}
