/*! \file CF_KE_CIFExchange.hxx
    \brief This file contains classes for handling the CIF(Client-Identification-Facility) state of the key exchange
*/
#ifndef CF_KE_CIFEXCHANGE_HXX
#define CF_KE_CIFEXCHANGE_HXX

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_Crypter.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>

#include <lib/cryptfacility/CF_KE_SessionKeyExchange.hxx>


namespace Giritech {
namespace CryptFacility {


  /*! \brief This is a abstrac class representing information that identify a client.

  This class is implemented outside cryptfacility.
  */
  class CRYPTFACILITY_DLL ClientIdentification : boost::noncopyable {
  public:
    typedef boost::shared_ptr<ClientIdentification> APtr;

    /*! \brief Destructor
    */
    virtual ~ClientIdentification(void) {
    }

    /*! \brief Initialize client identificaiton
        \param dataBuffer Raw databuffer with client identification
    */
    virtual void init(const Utility::DataBuffer::APtr& dataBuffer) = 0;
    

    /*! \brief Returns a raw data buffer with client identification
    */
    virtual Utility::DataBuffer::APtr getDataBuffer(void) const = 0;
  };



  /*! \brief An abstract class which represent and handles the CIF(Client-Identity-Facility) state of the key exchange.

  The CIF state exchange client identification information and client facility information, and the iv/key to use for the upstream cipher.
  The facility information contains a list of available ciperid/keylength's and hashid's. 
  The identification information contains G/On-usb-key id, and other information to identify the client, e.g. OS info and mac/ip addresses.
  Notice that the identification information is only seen as data, and are not used for anything inside %CryptFacility.

  This is what happends:
  -# The client generates iv/key for symmetric SessionUpstreamKey for the upstream.
  -# The client sends a CIF message containing the TemporaryClientKey.public, 
     client identity/facility information blackbox and iv/key for the SessionUpstreamKey, 
     and the sign of the CIF information using InstallationKey.public.
     The whole message is encrypted with hardcoded cipher using the TemporaryServerKey.public.
  -# The server decrypts the CIF message received from the client using the TemporaryServerKey.private.
  -# The server verify the sign of the message using the InstallationKey.private.
  -# Now the server know the SessionUpstreamKey(iv/key).

  \dot
  digraph graph_session_key_exchange {
    rankdir=TB;
    ranksep=0.5; 
    center=true;
    edge [minlen=1.5, fontsize=8, fontname="Helvetica" ];
    node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];

    SKE_Client_SKE               [shape=octagon, style=filled, fillcolor=2, label="KeyExchange_SessionKeyExchange"];
    CIF_Client_create            [shape=box,     style=filled, fillcolor=2, label="create"];
    CIF_Client_generateMessage   [shape=box,     style=filled, fillcolor=2, label="generateMessage"];
    CIF_message                  [shape=ellipse, style=filled, fillcolor=1  label="CIF datagram"];
    CIF_Server_createFromMessage [shape=box,     style=filled, fillcolor=3, label="createFromMessage"];
    CIF_Client_CIF               [shape=ellipse, style=filled, fillcolor=2, label="Client Identification\nClient Facility"];
    CIF_Server_CIF               [shape=ellipse, style=filled, fillcolor=3, label="Client Identification\nClient Facility"];
    SKE_Server_SKE               [shape=octagon, style=filled, fillcolor=3, label="KeyExchange_SessionKeyExchange"];

    SKE_Client_SKE               -> CIF_Client_generateMessage;
    CIF_Client_create            -> CIF_Client_CIF;
    CIF_Client_CIF               -> CIF_Client_generateMessage;
    CIF_Client_generateMessage   -> CIF_message;
    CIF_message                  -> CIF_Server_createFromMessage;
    CIF_Server_createFromMessage -> CIF_Server_CIF;
    CIF_Server_createFromMessage -> SKE_Server_SKE;
  }
  \enddot


  The CIF datagram generated on the client and send to the server looks like this:
  \image html drawing_protocol_cif.png "CIF Payload"
  \image latex drawing_protocol_cif.png "CIF Payload"

  */
  class CRYPTFACILITY_DLL KeyExchange_CIFExchange : boost::noncopyable {
  public:

    /*! \brief This exception is used as base for alle exceptions thrown during cif-exchange state
     */
    class Exception_CF_KE_CIFExchange : public Exception_CF {
    public:
      Exception_CF_KE_CIFExchange(const std::string& message) 
        : Exception_CF(message) {
      }
    };

    typedef boost::shared_ptr<KeyExchange_CIFExchange> APtr;
 
    /*! \brief Destructor
    */
    virtual ~KeyExchange_CIFExchange(void) {
    }

  protected:
    /*! \brief Constructor
    */
    KeyExchange_CIFExchange(void);

  private:
    /* \brief Burns intenal values before destruction
    */
    void burn(void);

  };



  /*! \brief Represent and handles the client side of the CIF-exchange state of the key exchange.

  */
  class CRYPTFACILITY_DLL KeyExchange_CIFExchange_Client : public virtual KeyExchange_CIFExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_CIFExchange_Client> APtr;
 

    /*! \brief Destructor
    */
    virtual ~KeyExchange_CIFExchange_Client(void);
    

    /*! \brief Generate the CIF message for the server
    
    The CIF message contains client identification, client facility information, cipher key/iv to use for the upstream. 
    The message is encrypted using the key-exchange cipher, and the remote session-public-key, recived during the session-key-exchange state. 
    */
    Utility::DataBufferManaged::APtr generateMessage(const KeyExchange_SessionKeyExchange::APtr& ske,
                                                     const Crypter::APtr& crypter,
                                                     const ClientIdentification::APtr& clientIdentification) const;


    /*! \brief Create an client instance
        \return Auto pointer to generated class instance

    Generate iv and key for the upstream. 
    Notice that the cipher to use for the upstream has not yet been chosen, and thus we actualy don't know the key length yet.
    */
    static APtr create(const CryptFactory& cryptFactory);

  private:
    /*! \brief Constructor
    */
    KeyExchange_CIFExchange_Client(void);

    /*! \brief Burns intenal values before destruction
    */
    void burn(void);
  };



  /*! \brief Represent and handles the server side of the CIF-exchange state of the key exchange.

  */
  class CRYPTFACILITY_DLL KeyExchange_CIFExchange_Server : public virtual KeyExchange_CIFExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_CIFExchange_Server> APtr;


    /*! \brief Destructor
    */
    virtual ~KeyExchange_CIFExchange_Server(void);


    /*! \brief Parse CIF information and create an instance
        \return Auto pointer to generated class instance

    Update the crypter with upstream key/iv.
    */
    static APtr createFromMessage(const KeyExchange_SessionKeyExchange::APtr& ske,
                                  const ClientIdentification::APtr& clientIdentification,
                                  const Crypter::APtr& crypter,
                                  const Utility::DataBuffer::APtr& dataBuffer);

  private:
    /*! \brief Constructor
    */
    KeyExchange_CIFExchange_Server(void);

    /*! \brief Burns intenal values before destruction
    */
    void burn(void);
  };
 

}
}

#endif
