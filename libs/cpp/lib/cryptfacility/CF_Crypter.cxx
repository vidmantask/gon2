/*! \file CF_Crypter.cxx
 \brief This file contains the implementation for handling the encryption
 */
#include <sstream>
#include <iostream>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Crypter.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

Crypter::Crypter(const CryptFactory::APtr& cryptFactory) :
    cryptFactory_(cryptFactory) {
}

Crypter::APtr Crypter::create(const CryptFactory::APtr& cryptFactory) {
    return Crypter::APtr(new Crypter(cryptFactory));
}

void Crypter::initHash(const Hash::APtr& hash) {
    hash_ = hash;
}

void Crypter::initEncrypter(const Crypt::APtr& encrypter) {
    assert(encrypter.get() != NULL);
    assert(encrypterKey_.get() != NULL);
    assert(encrypterIV_.get() != NULL);
    encrypter_ = encrypter;

    if (encrypterIV_->getSize() == 0) {
        encrypter_->setEncryptionKey(encrypterKey_);
    } else {
        encrypter_->setKeyWithIV(encrypterKey_->data(), encrypterIV_->data());
    }

}
void Crypter::initEncrypter(const Crypt::APtr& encrypter,
                            const Utility::DataBuffer::APtr& key,
                            const Utility::DataBuffer::APtr& iv) {
    setEncrypterKey(key, iv);
    initEncrypter(encrypter);
}
void Crypter::initEncrypter(const Crypt::APtr& encrypter, const Utility::DataBuffer::APtr& key) {
    setEncrypterKey(key);
    initEncrypter(encrypter);
}

void Crypter::initDecrypter(const Crypt::APtr& decrypter) {
    assert(decrypter.get() != NULL);
    assert(decrypterKey_.get() != NULL);
    assert(decrypterIV_.get() != NULL);
    decrypter_ = decrypter;

    if (decrypterIV_->getSize() == 0) {
        decrypter_->setDecryptionKey(decrypterKey_);

    } else {
        decrypter_->setKeyWithIV(decrypterKey_->data(), decrypterIV_->data());
    }
}

void Crypter::initDecrypter(const Crypt::APtr& decrypter,
                            const Utility::DataBuffer::APtr& key,
                            const Utility::DataBuffer::APtr& iv) {
    setDecrypterKey(key, iv);
    initDecrypter(decrypter);
}
void Crypter::initDecrypter(const Crypt::APtr& decrypter, const Utility::DataBuffer::APtr& key) {
    setDecrypterKey(key);
    initDecrypter(decrypter);
}

void Crypter::setEncrypterKey(const Utility::DataBuffer::APtr& key,
                              const Utility::DataBuffer::APtr& iv) {
    encrypterKey_ = key;
    encrypterIV_ = iv;
}
void Crypter::setEncrypterKey(const Utility::DataBuffer::APtr& key) {
    encrypterKey_ = key;
    encrypterIV_ = Utility::DataBufferManaged::create(0);
}

void Crypter::setDecrypterKey(const Utility::DataBuffer::APtr& key,
                              const Utility::DataBuffer::APtr& iv) {
    decrypterKey_ = key;
    decrypterIV_ = iv;
}
void Crypter::setDecrypterKey(const Utility::DataBuffer::APtr& key) {
    decrypterKey_ = key;
    decrypterIV_ = Utility::DataBufferManaged::create(0);
}

void Crypter::knownAnswerTestDecrypter(void) {
    assert(decrypter_.get() != NULL);
    if (CryptFacilityService::getInstance().isModeOfOperationFIPS()) {
        if (!decrypter_->consistencyTest()) {
            /*! \fips_stf Exception_SelftestFailed("Pairwise consistency test failed for generated key/iv") */
            throw CryptFacilityService::Exception_SelftestFailed("Pairwise consistency test failed for generated key/iv");
        }
    }
}
void Crypter::knownAnswerTestEncrypter(void) {
    assert(encrypter_.get() != NULL);
    if (CryptFacilityService::getInstance().isModeOfOperationFIPS()) {
        if (!encrypter_->consistencyTest()) {
            /*! \fips_stf Exception_SelftestFailed("Pairwise consistency test failed for generated key/iv") */
            throw CryptFacilityService::Exception_SelftestFailed("Pairwise consistency test failed for generated key/iv");
        }
    }
}

Hash::APtr Crypter::getHash(void) const {
    assert(hash_.get() != NULL);
    return hash_;
}

Crypt::APtr Crypter::getEncrypter(void) const {
    assert(encrypter_.get() != NULL);
    return encrypter_;
}

Crypt::APtr Crypter::getDecrypter(void) const {
    assert(decrypter_.get() != NULL);
    return decrypter_;
}

void Crypter::set_keys_from(const Crypter::APtr& other_crypter) {
    assert(other_crypter.get() != NULL);
    encrypterKey_ = other_crypter->encrypterKey_;
    encrypterIV_ = other_crypter->encrypterIV_;
    decrypterKey_ = other_crypter->decrypterKey_;
    decrypterIV_ = other_crypter->decrypterIV_;
}

void Crypter::burn(void) {
	// Keys are not burned here, because they are handled by keystore
}
