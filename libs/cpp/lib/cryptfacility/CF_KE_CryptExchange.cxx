/*! \file CF_KE_CryptExchange.cxx
    \brief This file contains the implementation for handling the crypt-exchange stats of the key exchange mecanisme
*/
#include <sstream>
#include <iostream>

#include <lib/utility/UY_String.hxx>
#include <lib/cryptfacility/CF_KE_CryptExchange.hxx>

#include "lib/cryptfacility/CF_Hash_CryptoPP.hxx"

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;


/* 
   ------------------------------------------------------------------ 
   KeyExchange_CryptExchange implementation
   ------------------------------------------------------------------ 
*/
const char* KeyExchange_CryptExchange::skpType_0 = "0";

KeyExchange_CryptExchange::KeyExchange_CryptExchange(void) {
}

void KeyExchange_CryptExchange::calculateReceipt(const Utility::DataBuffer::APtr& serverKeyMessage) {
  assert(serverKeyMessage.get()!=NULL);
  Hash_CryptoPP_SHA1::APtr hashSHA1(Hash_CryptoPP_SHA1::create());
  receipt_ = hashSHA1->calculateDigest(serverKeyMessage);
  assert(receipt_.get()!=NULL);
}

void KeyExchange_CryptExchange::burn(void) {
  receipt_->burn();
}


Utility::DataBuffer::APtr KeyExchange_CryptExchange::getReceipt(void) const {
  return receipt_;
}

Crypter::APtr KeyExchange_CryptExchange::create_crypter_stream(const Crypter::APtr& crypter, const CryptFactory::APtr& crypt_factory) {
    assert(crypter.get() != NULL);
    assert(crypt_factory != NULL);

    Crypter::APtr crypter_stream(Crypter::create(crypt_factory));
    crypter_stream->set_keys_from(crypter);

    /* Initialice crypter */
    crypter_stream->initEncrypter(crypt_factory->getCryptStreamAES256());
    crypter_stream->initDecrypter(crypt_factory->getCryptStreamAES256());
    crypter_stream->initHash(crypt_factory->getHashSHA1());
    return crypter_stream;
}




/* 
   ------------------------------------------------------------------ 
   KeyExchange_CryptExchange_Client implementation
   ------------------------------------------------------------------ 
*/
KeyExchange_CryptExchange_Client::KeyExchange_CryptExchange_Client(void)
  : KeyExchange_CryptExchange() {
}


KeyExchange_CryptExchange_Client::APtr KeyExchange_CryptExchange_Client::createFromMessage(const KeyExchange_SessionKeyExchange& ske,
                                                                                           Crypter& crypter, 
                                                                                           const CryptFactory& cryptFactory,
                                                                                           const Utility::DataBuffer& message) {

  if(message.getSize() == 0) {
    throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message length");
  }

  try {
    /* Decrypt and unpad buffer using the session encryption */
    Crypt::APtr sessionCrypter(ske.getSessionCrypter()->getDecrypter());
    long messageDecryptedSize = sessionCrypter->getPlainBufferSizeAfter(message.getSize());
    DataBufferManaged::APtr messageDecrypted(DataBufferManaged::create(messageDecryptedSize));
    sessionCrypter->decrypt(message.data(), message.getSize(), messageDecrypted->data());
    messageDecrypted->pkcsRemove(sessionCrypter->getBlockSize());

    /* Pars elements of message */
    long pos = 0;
    DataBufferManaged::APtr skpTypeBuffer;
    pos = messageDecrypted->parse(",", skpTypeBuffer);
    if(skpTypeBuffer.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing skp-type");
    }
    string skpType = skpTypeBuffer->toString();
    if(skpType != skpType_0) {
      DataBufferManaged::APtr additionalDataBuffer;
       messageDecrypted->parse(additionalDataBuffer, pos);
      if(additionalDataBuffer.get()==NULL) {
        throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing aditional data for skp-type " + skpType + " message");
      }

      stringstream skpTypeSS(skpType);
      int skpTypeInt;
      skpTypeSS >> skpTypeInt;
      if(!skpTypeSS.fail()) {
        throw Exception_CF_KE_CryptExchange_Special_SPK(skpTypeInt, additionalDataBuffer->decodeHex());
      }
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, the skpType '" + skpType + "' is invalid");
    }

    DataBufferManaged::APtr cipherIdEncryptionBuffer;
    pos = messageDecrypted->parse(",", cipherIdEncryptionBuffer, pos);
    if(cipherIdEncryptionBuffer.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing id for encryption cipher");
    }
    string cipherIdEncryption = cipherIdEncryptionBuffer->toString();

    DataBufferManaged::APtr cipherIdDecryptionBuffer;
    pos = messageDecrypted->parse(",", cipherIdDecryptionBuffer, pos);
    if(cipherIdDecryptionBuffer.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing id for decryption cipher");
    }
    string cipherIdDecryption = cipherIdDecryptionBuffer->toString();

    DataBufferManaged::APtr hashIdBuffer;
    pos = messageDecrypted->parse(",", hashIdBuffer, pos);
    if(hashIdBuffer.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing id for hash");
    }
    string hashId = hashIdBuffer->toString();

    DataBufferManaged::APtr downstreamKey;
    pos = messageDecrypted->parse(",", downstreamKey, pos);
    if(downstreamKey.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing downstream key");
    }
    downstreamKey = downstreamKey->decodeHex();

    DataBufferManaged::APtr downstreamIV;
    messageDecrypted->parse(downstreamIV, pos);
    if(downstreamIV.get()==NULL) {
      throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message, missing downstream iv");
    }
    downstreamIV = downstreamIV->decodeHex();

    /* create instance */
    KeyExchange_CryptExchange_Client::APtr ce(new KeyExchange_CryptExchange_Client());

    /* Calculate recipt */
    ce->calculateReceipt(messageDecrypted);

    /* Initialice crypter */
    crypter.initDecrypter(cryptFactory.getCryptBlockAES256(), downstreamKey, downstreamIV);
    crypter.initHash(cryptFactory.getHashSHA1());

    return ce;
  }
  catch(DataBuffer::Exception_Seperator_not_found&) {
    throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message");
  }
  catch(DataBuffer::Exception_Out_of_bound&) {
    throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message");
  }
  catch(DataBuffer::Exception_decoding&) {
    throw Exception_CF_KE_CryptExchange("Invalid crypt-exchange-message");
  }
}

void KeyExchange_CryptExchange_Client::burn(void) {
  /* Nothing to burn */
}




/*
  ------------------------------------------------------------------ 
  KeyExchange_CryptExchange_Server implementation
  ------------------------------------------------------------------ 
*/
KeyExchange_CryptExchange_Server::KeyExchange_CryptExchange_Server(void)
  : KeyExchange_CryptExchange() {
}

KeyExchange_CryptExchange_Server::APtr KeyExchange_CryptExchange_Server::create(const KeyExchange_CIFExchange& cif) {
  return KeyExchange_CryptExchange_Server::APtr(new KeyExchange_CryptExchange_Server());
}

DataBufferManaged::APtr KeyExchange_CryptExchange_Server::generateMessage(const KeyExchange_SessionKeyExchange& ske,
                                                                          Crypter& crypter,
                                                                          const CryptFactory& cryptFactory) {

  /* Generate iv/key for the downstream */
  DataBufferManaged::APtr downstreamKey;
  DataBufferManaged::APtr downstreamIV;
  ske.getKeyStore()->getEncrypterKey(downstreamKey, downstreamIV);
  crypter.initEncrypter(cryptFactory.getCryptBlockAES256(), downstreamKey, downstreamIV);
  crypter.initHash(cryptFactory.getHashSHA1());

  /* Select client ciphers, no longer used, this values are expected by old clients, but not used by new clients */
  string clientCipherIdEncryption = "1-32";
  string clientCipherIdDecryption = "1-32";
  string clientHashId             = "3";

  /* Generate message */
  DataBufferManaged::APtr message(DataBufferManaged::create(0));
  message->append(skpType_0);
  message->append(",");
  message->append(clientCipherIdEncryption);
  message->append(",");
  message->append(clientCipherIdDecryption);
  message->append(",");
  message->append(clientHashId);
  message->append(",");
  message->append(downstreamKey->encodeHex());
  message->append(",");
  message->append(downstreamIV->encodeHex());

  /* Calculate recipt */
  calculateReceipt(message);

  /* Pad buffer, and encrypt using the session encryption */
  Crypt::APtr sessionCrypter(ske.getSessionCrypter()->getEncrypter());
  message->pkcsAdd(sessionCrypter->getBlockSize());

  DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(sessionCrypter->getCipherBufferSize(message->getSize())));
  sessionCrypter->encrypt(message->data(), message->getSize(), messageEncrypted->data());
  return messageEncrypted;
}

DataBufferManaged::APtr KeyExchange_CryptExchange_Server::generateMessage(const KeyExchange_SessionKeyExchange& ske, 
                                                                          const unsigned int& skpType,
                                                                          const Utility::DataBuffer::APtr& additionalData) {

  stringstream skpTypeSS;
  skpTypeSS << skpType;
  if(skpType == 0) {
    throw Exception_CF_KE_CryptExchange("The skpType '" + skpTypeSS.str() + "' is invalid");
  }

  /* Generate message */
  DataBufferManaged::APtr message(DataBufferManaged::create(0));
  message->append(skpTypeSS.str());
  message->append(",");
  message->append(additionalData->encodeHex());

  /* Calculate recipt */
  calculateReceipt(message);

  /* Pad buffer, and encrypt using the session encryption */
  Crypt::APtr sessionCrypter(ske.getSessionCrypter()->getEncrypter());
  message->pkcsAdd(sessionCrypter->getBlockSize());

  DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(sessionCrypter->getCipherBufferSize(message->getSize())));
  sessionCrypter->encrypt(message->data(), message->getSize(), messageEncrypted->data());
  return messageEncrypted;
}


void KeyExchange_CryptExchange_Server::burn(void) {
  /* Nothing to burn */
}
