/*! \file CF_Crypt_CryptoPP.hxx
 \brief This file contains the interface classes to en-/decryption algorithm in the Crypto++ library.
 */
#ifndef CF_CRYPT_CRYPTOPP_HXX
#define CF_CRYPT_CRYPTOPP_HXX

#include <sstream>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
# include <cryptopp/dll.h>
# include <cryptopp/fips140.h>
#else
# include <cryptopp/modes.h>
# include <cryptopp/aes.h>
# include <cryptopp/eccrypto.h>
# include <cryptopp/rsa.h>
# include <cryptopp/osrng.h>
#endif

#include <lib/utility/UY_Types.hxx>

#include <lib/cryptfacility/CF_Crypt.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>

namespace Giritech {
namespace CryptFacility {

typedef CryptoPP::ECDSA< CryptoPP::EC2N, CryptoPP::SHA1> PKSignatureScheme;
typedef CryptoPP::RSAES< CryptoPP::OAEP <CryptoPP::SHA1> > PKCryptosystem;
typedef CryptoPP::AutoSeededX917RNG<CryptoPP::AES> RandomNumberGenerator;


typedef CryptoPP::RSASS< CryptoPP::PKCS1v15, CryptoPP::SHA1> PKSignatureSchemeRSA;



/*! \brief This exception is ecapsuling exctiptions thrown by CryptoPP
 */
class Exception_CF_CryptoPP : public Exception_CF {
public:
    Exception_CF_CryptoPP(const std::string& message) :
        Exception_CF(message) {
    }
};

/*! \brief Class representing a base classe for where common functionality for Crypto++ Symmetric block chipers and Symetric Stream chipers are implemented
 */
template<class EncryptionMode, class T> class Crypt_CryptoPP_Base : public virtual Crypt {
protected:
    typedef T CipherType;

    typename EncryptionMode::Encryption encryption_;
    typename EncryptionMode::Decryption decryption_;

public:
    /*! \brief Destructor

     On destruction all internal state variables are burned, \see burn
     */
    virtual ~Crypt_CryptoPP_Base(void) {
    }

    /*! \brief Set key and initialization vector
     */
    void setKeyWithIV(const Utility::byte_t* key, const Utility::byte_t* iv) {
        try {
            encryption_.SetKeyWithIV(key, getKeyLen(), iv);
            decryption_.SetKeyWithIV(key, getKeyLen(), iv);
            setInitialized();
        }
        catch(const CryptoPP::Exception& e) {
            throw Exception_CF_CryptoPP(e.what());
        }
    }
    /*! \brief Return the length of the plain text buffer after encryption and decrytion

     This function can be used to resize the plaintext buffer.
     AES is a block cipher an thus return the (bufferlen  mod buffersize) + bufferlen
     */
    long getPlainBufferSize(const long plainBufferSize) const {
        int blockSize = encryption_.MandatoryBlockSize();
        return plainBufferSize + (plainBufferSize % blockSize);
    }

    /*! \brief Return the length of the cipher text buffer after encryption.

     This is a block cipher an thus return the (bufferlen  mod buffersize) + bufferlen
     */
    long getCipherBufferSize(const long plainBufferSize) const {
        return getPlainBufferSize(plainBufferSize);
    }

    /*! \brief Return the length of the plain text buffer after encryption

     This is a block cipher an thus return the (bufferlen  mod buffersize) + bufferlen
     */
    long getPlainBufferSizeAfter(const long cipherBufferSize) const {
        return getPlainBufferSize(cipherBufferSize);
    }

    /*! \brief Return the block size
     */
    long getBlockSize(void) const {
        return encryption_.MandatoryBlockSize();
    }

    /*! \brief Encrypt a buffer of bytes
     */
    void encrypt(const Utility::byte_t* plainBuffer,
                 const int bufferLen,
                 Utility::byte_t* cipherBuffer) {
        readyToEncrypt(bufferLen);
        checkBufferLen(bufferLen);
        try {
            encryption_.ProcessString(cipherBuffer, plainBuffer, bufferLen);
        }
        catch(const CryptoPP::Exception& e) {
            throw Exception_CF_CryptoPP(e.what());
        }
    }

    /*! \brief Decrypt a buffer of bytes
     */
    void decrypt(const Utility::byte_t* cipherBuffer,
                 const int bufferLen,
                 Utility::byte_t* plainBuffer) {
        readyToDecrypt(bufferLen);
        checkBufferLen(bufferLen);
        try {
            decryption_.ProcessString(plainBuffer, cipherBuffer, bufferLen);
        }
        catch(const CryptoPP::Exception& e) {
            throw Exception_CF_CryptoPP(e.what());
        }
    }

    /*! \brief Return the name of the crypt algorithm
     */
    std::string getName(void) const {
        return encryption_.AlgorithmName();
    }

protected:
    /*! \brief Constructor
     \param _keyLen Keylen to be used by this instance. The keylen is in bits.
     */
    Crypt_CryptoPP_Base(const int keyLen) :
        Crypt(keyLen) {
    }

    virtual void burn(void) = 0;
    virtual void checkBufferLen(const int bufferLen) const = 0;
};

/*! \brief Class representing the interface to Crypto++ Symmetric block chipers
 */
template<class T> class Crypt_CryptoPP_Block :
    public virtual Crypt_CryptoPP_Base< CryptoPP::CBC_Mode<T>, T > {
public:
    typedef boost::shared_ptr< Crypt_CryptoPP_Block<T> > APtr;

    /*! \brief Destructor

     On destruction all internal state variables are burned, \see burn
     */
    virtual ~Crypt_CryptoPP_Block(void) {
        burn();
    }

    /*! \brief Clone current instance
     */
    Crypt::APtr clone(void) const {
        return APtr(new Crypt_CryptoPP_Block(*this));
    }

    /*! \brief Create an instance
     */
    static APtr create(const int keyLen) {
        return APtr(new Crypt_CryptoPP_Block(keyLen));
    }

private:
    /*! \brief Constructor
     \param _keyLen Keylen to be used by this instance. The keylen is in bits.
     */
    Crypt_CryptoPP_Block(const int keyLen) :
        Crypt(keyLen), Crypt_CryptoPP_Base<CryptoPP::CBC_Mode<T>, T>(keyLen) {
    }
    void burn(void) {
        // Nothing to do, and is used to keep Crypt_CryptPP_Base abstract
    }

    void checkBufferLen(const int bufferLen) const {
        int blockSize = this->encryption_.MandatoryBlockSize();
        if ( (bufferLen % blockSize) != 0) {
            std::stringstream ss;
            ss << "Invalid size of buffer. Buffer size is " << bufferLen
                    << ", but the size is incompatible with current used block size " << blockSize;
            throw Exception_CF_CryptoPP(ss.str());
        }
    }
};

/*! \brief Class representing the interface to Crypto++ Symmetric stream chipers
 */
template<class T> class Crypt_CryptoPP_Stream :
    public virtual Crypt_CryptoPP_Base< CryptoPP::CTR_Mode<T>, T > {
public:
    typedef boost::shared_ptr< Crypt_CryptoPP_Stream<T> > APtr;

    /*! \brief Destructor

     On destruction all internal state variables are burned, \see burn

     \todo Ther is something strange about this constructor that need to be invistageted regarding base-class instantiation,
     the Crypt-constructor need to be calle explicit.
     */
    virtual ~Crypt_CryptoPP_Stream(void) {
        burn();
    }

    /*! \brief Clone current instance
     */
    Crypt::APtr clone(void) const {
        return APtr(new Crypt_CryptoPP_Stream(*this));
    }

    /*! \brief Create an instance
     */
    static APtr create(const int keyLen) {
        return APtr(new Crypt_CryptoPP_Stream(keyLen));
    }

private:
    /*! \brief Constructor
     \param _keyLen Keylen to be used by this instance. The keylen is in bits.

     \todo Ther is something strange about this constructor that need to be invistageted regarding base-class instantiation,
     the Crypt-constructor need to be calle explicit.
     */
    Crypt_CryptoPP_Stream(const int keyLen) :
        Crypt(keyLen), Crypt_CryptoPP_Base<CryptoPP::CTR_Mode<T>, T>(keyLen) {
    }
    void burn(void) {
        // Nothing to do, and is used to keep Crypt_CryptPP_Base abstract
    }

    void checkBufferLen(const int bufferLen) const {
    }

};

/*! \brief This class interface to Crypto++ Public key Cryptosystem RSAES<OAEP<SHA1> > en-/decryption algorithm
 */
class CRYPTFACILITY_DLL Crypt_CryptoPP_PKAES : public virtual Crypt {
public:
    typedef boost::shared_ptr<Crypt_CryptoPP_PKAES> APtr;

    /*! \brief Destructor

     On destruction all internal state variables are burned, \see burn
     */
    ~Crypt_CryptoPP_PKAES(void) {
        burn();
    }

    /*! \brief Clone current instance
     */
    Crypt::APtr clone(void) const;

    /*! \brief Set key and initialization vector
     */
    void setKeyWithIV(const Utility::byte_t* key, const Utility::byte_t* iv);

    /*! \brief Set encryption key
     */
    void setEncryptionKey(const Giritech::Utility::DataBuffer::APtr& key);

    /*! \brief Set decryption key
     */
    void setDecryptionKey(const Giritech::Utility::DataBuffer::APtr& key);

    /*! \brief Return the length of the plain text buffer after encryption and decrytion

     This function can be used to resize the plaintext buffer.
     */
    long getPlainBufferSize(const long plainBufferSize) const;

    /*! \brief Return the length of the cipher text buffer after encryption.
     */
    long getCipherBufferSize(const long plainBufferSize) const;

    /*! \brief Return the length of the plain text buffer after encryption
     */
    long getPlainBufferSizeAfter(const long cipherBufferSize) const;

    /*! \brief Return the block size
     */
    long getBlockSize(void) const;

    /*! \brief Encrypt a buffer of bytes
     */
    void encrypt(const Utility::byte_t* plainBuffer,
                 const int bufferLen,
                 Utility::byte_t* cipherBuffer);

    /*! \brief Decrypt a buffer of bytes
     */
    void decrypt(const Utility::byte_t* cipherBuffer,
                 const int bufferLen,
                 Utility::byte_t* plainBuffer);

    /*! \brief Return the name of the crypt algorithm
     */
    std::string getName(void) const;

    /*! \brief Create an instance
     */
    static APtr create(const int keyLen);

private:
    /*! \brief Constructor
     */
    Crypt_CryptoPP_PKAES(const int keyLen);

    /*! \brief Burn all internal buffers.
     */
    void burn(void);

    void checkBufferLen(const int bufferLen) const;

    PKCryptosystem::Encryptor encryptor_;
    PKCryptosystem::Decryptor decryptor_;
};

/*! \brief Template for loading key from raw buffer
 */
template<class T> T loadKey(const Utility::DataBuffer::APtr& keyRaw) {
    T key;
    assert(keyRaw.get() != NULL);
    CryptoPP::StringSource keySource(keyRaw->data(), keyRaw->getSize(), true);
    key.Load(keySource);
    return key;
}

/*! \brief Template for loading key from raw buffer
 */
template<class T> T loadKey(const Utility::byte_t* buffer, const long size) {
    T key;
    CryptoPP::StringSource keySource(buffer, size, true);
    key.Load(keySource);
    return key;
}

/*! \brief Template for saving key into raw buffer
 */
template<class T> Utility::DataBufferManaged::APtr saveKey(const T& key) {
    boost::shared_ptr<std::string> publicKeyString(new std::string);
    CryptoPP::StringSink publicKeySink(*(publicKeyString.get()));
    key.Save(publicKeySink);
    return Utility::DataBufferManaged::create(*(publicKeyString.get()));
}

}
}
#endif
