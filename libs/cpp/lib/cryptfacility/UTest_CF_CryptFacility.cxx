/*! \file CF_CryptFacility_UTest.cxx
 \brief This file contains unittest suite for the cryptfacility module
 */

#include <boost/test/unit_test.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#endif

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
# include <lib/cryptfacility/CryptFacilityDLL.hxx>
#else
# include <lib/utility/UY_DataBuffer.hxx>
# include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#endif

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

BOOST_AUTO_TEST_CASE( cryptfacility_powerup_forced_failed_selftest_own )
{
	CryptFacilityService& cryptFacility(CryptFacilityService::getInstance());

	cryptFacility.enableFakeSelftestError();
	BOOST_CHECK_THROW(cryptFacility.initialize(CryptFacilityService::modeofoperation_unknown), CryptFacilityService::Exception_SelftestFailed);
}

BOOST_AUTO_TEST_CASE( cryptfacility_powerup_forced_failed_selftest )
{
	CryptFacilityService& cryptFacility(CryptFacilityService::getInstance());
	cryptFacility.initialize(CryptFacilityService::modeofoperation_unknown);
	BOOST_CHECK(cryptFacility.isReady() );
}

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
BOOST_AUTO_TEST_CASE( cryptfacility_fipsmode )
{
	CryptFacilityService& cryptFacility(CryptFacilityService::getInstance());

	cryptFacility.initialize(CryptFacilityService::modeofoperation_fips);
	BOOST_CHECK( cryptFacility.isModeOfOperationFIPS() );
}
#endif

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
	return 0;
}
