/*! \file CF_KE_CryptStreamValidator.cxx
    \brief This file contains the implementation for classes for valdating the upstream and downstream cipher
*/
#include <sstream>
#include <iostream>
#include <boost/utility.hpp>

#include <lib/utility/UY_String.hxx>

#include <lib/cryptfacility/CF_KE_CryptStreamValidator.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;


/* 
   ------------------------------------------------------------------ 
   KeyExchange_CryptStreamValidator
   ------------------------------------------------------------------ 
*/
DataBufferManaged::APtr KeyExchange_CryptStreamValidator::generateMessage(const Crypter& crypter, const DataBuffer& receipt) {
  Crypt::APtr encrypter(crypter.getEncrypter());

  DataBufferManaged::APtr receiptPadded(receipt.clone());
  receiptPadded->pkcsAdd(encrypter->getBlockSize());

  long messageEncryptedSize = encrypter->getCipherBufferSize(receiptPadded->getSize());
  DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(messageEncryptedSize));
  encrypter->encrypt(receiptPadded->data(), receiptPadded->getSize(), messageEncrypted->data());
  return messageEncrypted;
}

   
void KeyExchange_CryptStreamValidator::validateMessage(const Crypter& crypter, const DataBuffer& receipt, const DataBuffer& message){
  Crypt::APtr decrypter(crypter.getDecrypter());
  long messageDecryptedSize = decrypter->getPlainBufferSizeAfter(message.getSize());
  DataBufferManaged::APtr messageDecrypted(DataBufferManaged::create(messageDecryptedSize));
  decrypter->decrypt(message.data(), message.getSize(), messageDecrypted->data());

  messageDecrypted->pkcsRemove(decrypter->getBlockSize());
  if(receipt != (*messageDecrypted) ) {
    throw Exception_Invalid("Invalid reciept recived");
  }
}
