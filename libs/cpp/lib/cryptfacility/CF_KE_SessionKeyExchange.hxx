/*! \file CF_KE_SessionKeyExchange.hxx
    \brief This file contains classes for handling the session key exchange stats of the key exchange mecanisme
*/
#ifndef CF_KE_SESSIONKEYEXCHANGE_HXX
#define CF_KE_SESSIONKEYEXCHANGE_HXX

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#else
#  include <cryptopp/modes.h>
#  include <cryptopp/aes.h>
#  include <cryptopp/eccrypto.h>
#endif

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypt.hxx>
#include <lib/cryptfacility/CF_Crypter.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_KeyStore.hxx>


namespace Giritech {
namespace CryptFacility {


  /*! \brief An abstract class representing and handling the session-key-exchange phase of the key exchange.

  The client and the server generate a asymmetric temporary key pair and distribute its public key to the other part.
  The temporary keys are only used until symmetric keys has been established.
  The session-key-exchange phase exchange the public key of the asymmetric server temporary key pair using a SPK(Server-Public-Key) datagram.

  This is what happends:
  -# The server generates a TemporaryServerKey(asymmetric).
  -# The server sends a SPK datagram containing the TemporaryServerKey.public signed with the InstallationKey.private(to prove the identity of the server).
  -# The client generates a TemporaryClientKey(asymmetric).
  -# The client parses the SPK datagram received from server containing the TemporaryServerKey.public and its sign.
  -# The client verify the signature using the InstallationKey.public and now knows the TemporaryServerKey.public.
  Notice that TemporaryClientKey and TemporaryServerKey are only used until symmetric keys established

  \dot
  digraph graph_session_key_exchange {
    rankdir=TB;
    ranksep=0.5;
    center=true;
    edge [minlen=1.5, fontsize=8, fontname="Helvetica" ];
    node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];

    SKE_signing_key              [shape=ellipse, style=filled, fillcolor=3, label="InstallationKey.private\n(see note 1)"];
    SKE_Server_create            [shape=box,     style=filled, fillcolor=3, label="create"];
    SKE_Server_generateMessage   [shape=box,     style=filled, fillcolor=3, label="generateMessage"];
    SKE_Server_SKE               [shape=ellipse, style=filled, fillcolor=3, label="TemporaryServerKey.private\nTemporaryServerKey.public\nTemporaryClientKey.public\n(see note 2)"];
    SKE_message                  [shape=ellipse, style=filled, fillcolor=1, label="SPK Datagram"];
    SKE_validation_key           [shape=ellipse, style=filled, fillcolor=2, label="InstallationKey.public\n(see note 1)"];
    SKE_Client_createFromMessage [shape=box,     style=filled, fillcolor=2, label="createFromMessage"];
    SKE_Client_SKE               [shape=ellipse, style=filled, fillcolor=2, label="TemporaryClientKey.private\nTemporaryClientKey.public\nTemporaryClientKey.private"];

    SKE_signing_key              -> SKE_Server_create;
    SKE_Server_create            -> SKE_Server_SKE;
    SKE_Server_SKE               -> SKE_Server_generateMessage;
    SKE_Server_generateMessage   -> SKE_message;
    SKE_message                  -> SKE_Client_createFromMessage;
    SKE_validation_key           -> SKE_Client_createFromMessage
    SKE_Client_createFromMessage -> SKE_Client_SKE;
    }
  \enddot
  Notes to figure:
  -# The InstallationKey used for signing and validation are generated during server installation.
  -# The TemporaryClientKey.public on the server side is transfered from the client to the server during the CIF Exchange phase.


  The SPK diagram generated on the server and send to the client looks like this:
  \image html drawing_protocol_spk.png "SPK Payload"
  \image latex drawing_protocol_spk.png "SPK Payload"
  */
  class CRYPTFACILITY_DLL KeyExchange_SessionKeyExchange : boost::noncopyable {
  public:
    typedef KeyExchange_SessionKeyExchange* Ptr;
    typedef boost::shared_ptr<KeyExchange_SessionKeyExchange> APtr;


    /*! \brief This exception is used as base for alle exceptions thrown during session-key-exchange state
     */
    class CRYPTFACILITY_DLL Exception_CF_KE_SessionKeyExchange : public Exception_CF {
    public:
      Exception_CF_KE_SessionKeyExchange(const std::string& message)
        : Exception_CF(message) {
      }
    };


    /*! \brief Constructor
        \param remotePublicSesionKey The remote public part of a session key pair

    During construction a private/public session-key pair is generated.
    \see generatePublicPrivateKeys
    */
    KeyExchange_SessionKeyExchange(const CryptFactory::APtr& cryptFactory,
                                   const Utility::DataBufferManaged::APtr& knownSecret,
                                   const KeyStore::APtr& keyStore);
    KeyExchange_SessionKeyExchange(const CryptFactory::APtr& cryptFactory,
                                   const Utility::DataBufferManaged::APtr& knownSecret,
                                   const Utility::DataBuffer::APtr& remotePublicSesionKey,
                                   const KeyStore::APtr& keyStore);


    /*! \brief Destructor
    */
    virtual ~KeyExchange_SessionKeyExchange(void) {
      burn();
    };

    /*! \brief Get public key
    */
    Utility::DataBufferManaged::APtr getPublicKey(void) const;

    /*! \brief Return session crypter
    */
    Crypter::APtr getSessionCrypter(void) const {
      return sessionCrypter_;
    };


    /*! \brief Set the remote public session key
    */
    void setRemotePublicSessionKey(const Utility::DataBuffer::APtr& remotePublicSessionKey);


    /*! \brief Sign buffer
     */
    Utility::DataBuffer::APtr signBuffer(const Utility::DataBuffer::APtr& buffer) const;


    /*! \brief Verify signed buffer
     */
    bool verifySignedBuffer(const Utility::DataBuffer::APtr& buffer, const Utility::DataBuffer::APtr& signitureBuffer) const;


    /*! \brief Generate a key pair for use at client and server side for signing and verifying
    */
    static void generateClientServerKnownSecrect(Utility::DataBufferManaged::APtr& clientKnownSecret,
                                                 Utility::DataBufferManaged::APtr& serverKnownSecret);

    /* \brief Generate a signing/validation key pair
    */
    static void generateSigningVerificationKeys(Utility::DataBufferManaged::APtr& signingKey,
                                                Utility::DataBufferManaged::APtr& validationKey);

    /*! \brief Parse and set signing and validation keys from known secret
     */
    static void setSigningValidationKeyFromKnownSecret(const Utility::DataBufferManaged::APtr& knownSecret,
                                                       Utility::DataBufferManaged::APtr& signingKey,
                                                       Utility::DataBufferManaged::APtr& validationKey);

    /* \brief Burns intenal values before destruction

    Notice that this is not a virtual method because it is called during destruction.
    */
    void burn(void);

    /* \brief Get used key store
    */
    KeyStore::APtr getKeyStore(void) const;

    /* \brief Get crypt factory
    */
    CryptFactory::APtr getCryptFactory(void);

  private:
    /* \brief Generate  public and private keys

    Generate a private/public session-key pair using Crypto++ Public Key Cryptosystems RSAES<OAEP<SHA1> >.
    */
    void initPublicPrivateKeys(void);


  protected:
    CryptFactory::APtr cryptFactory_;
    Crypter::APtr sessionCrypter_;

    Utility::DataBufferManaged::APtr signingKey_;
    Utility::DataBufferManaged::APtr validationKey_;

    KeyStore::APtr keyStore_;

  private:
    static const long keySize = 2048;
  };



  /*! \brief Represent and handles the client side of the session-key-exchange state of the key exchange.

  */
  class CRYPTFACILITY_DLL KeyExchange_SessionKeyExchange_Client : public virtual KeyExchange_SessionKeyExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_SessionKeyExchange_Client> APtr;


    /*! \brief Destructor
    */
    virtual ~KeyExchange_SessionKeyExchange_Client(void) {
      burn();
    }


    /*! \brief Create an client instance from buffer recived from server
        \param validationKey This is the public part of the ????????????????
        \param message       Message recived from server holding the session-public-key and a signiture
        \return Auto pointer to generated class instance

    The signiture of the skp-datagram is verified using using the validation-key,
    and create an instance of the class using the remote session-public-key.

    */
//    static KeyExchange_SessionKeyExchange::APtr createFromMessage(const CryptFactory::APtr& cryptFactory,
//                                                                  const Utility::DataBufferManaged::APtr& knownSecret,
//                                                                  const Utility::DataBuffer::APtr& message);

    static KeyExchange_SessionKeyExchange::APtr createFromMessage(const CryptFactory::APtr& cryptFactory,
                                                                  const Utility::DataBufferManaged::APtr& knownSecret,
                                                                  const Utility::DataBuffer::APtr& message,
                                                                  const KeyStore::APtr& keyStore);



  private:
    /*! \brief Constructor
        \param publicSesionKey This is the public part of the session key pair
        \param validationKey   Known secret generated during server installation

    Notice that an instance of this class only can be created using one of the create methods.
    \see createFromMessage
    */
    KeyExchange_SessionKeyExchange_Client(const CryptFactory::APtr& cryptFactory,
                                          const Utility::DataBufferManaged::APtr& knownSecret,
                                          const Utility::DataBuffer::APtr& remotePublicSessionKey,
                                          const KeyStore::APtr& keyStore)
      : KeyExchange_SessionKeyExchange(cryptFactory, knownSecret, remotePublicSessionKey, keyStore) {
    }


    /* \brief Burns intenal values before destruction

    Notice that this is not a virtual method because it is calle during destruction.
    */
    void burn(void);
  };



  /*! \brief Represent and handles the server side of the session-key-exchange state of the key exchange.

  */
  class CRYPTFACILITY_DLL KeyExchange_SessionKeyExchange_Server : public virtual KeyExchange_SessionKeyExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_SessionKeyExchange_Server> APtr;


    /*! \brief Destructor
    */
    virtual ~KeyExchange_SessionKeyExchange_Server(void) {
      burn();
    }


    /*! \brief Generate a signiture and pack it with public-session-key into a message for the client

    Create a signature of the public-session-key using the signing-key,
    and pack signature and public-session-key into a spk-datagram.
    */
    Utility::DataBufferManaged::APtr generateMessage(void) const;


    /*! \brief Create a server instance
        \param signingKey  Known secret generated during server installation
        \return Auto pointer to generated class instance

    Then create a instance of the class.
    */
    static KeyExchange_SessionKeyExchange_Server::APtr create(
    		boost::asio::io_service& io,
    		const CryptFactory::APtr& cryptFactory,
    		const Utility::DataBufferManaged::APtr& knownSecret);


  private:
    /*! \brief Constructor

    Notice that an instance of this class only can be created using one of the create methods.
    */
    KeyExchange_SessionKeyExchange_Server(const CryptFactory::APtr& cryptFactory,
                                          const Utility::DataBufferManaged::APtr& knownSecret,
                                          const KeyStore::APtr& keyStore);


    /* \brief Burns intenal values before destruction

    Notice that this is not a virtual method because it is called during destruction.
    */
    void burn(void);
  };


}
}

#endif
