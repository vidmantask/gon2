/*! \file CF_Hash_CryptoPP.hxx
 \brief This file contains the interface classes to hash algorithm in the Crypto++ library.
 */
#ifndef CF_HASH_CRYPTOPP_HXX
#define CF_HASH_CRYPTOPP_HXX

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#else
#  include <cryptopp/modes.h>
#  include <cryptopp/aes.h>
#  include <cryptopp/eccrypto.h>
#  include <cryptopp/rsa.h>
#  include <cryptopp/osrng.h>
#  include <cryptopp/files.h>
#  include <cryptopp/hex.h>
#endif

#include <lib/cryptfacility/CF_Hash.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>

namespace Giritech {
namespace CryptFacility {

/*! \brief This class interface to Crypto++ Hash function SHA1
 */
class CRYPTFACILITY_DLL Hash_CryptoPP_SHA1: public virtual Hash {
public:
    typedef boost::shared_ptr<Hash_CryptoPP_SHA1> APtr;

    /*! \brief Destructor

     On destruction all internal state variables are burned, \see burn
     */
    ~Hash_CryptoPP_SHA1(void);

    /*! \brief Return the name of the hash algorithm
     */
    virtual std::string getName(void) const;

    /*! \brief Returns the size in byte of the digest
     */
    virtual long digestSize(void) const;

    /*! \brief Calculate the digest
     */
    virtual Utility::DataBufferManaged::APtr calculateDigest(const Utility::DataBuffer::APtr& input);

    /*! \brief Verify the digest
     */
    virtual bool verifyDigest(const Utility::DataBuffer::APtr& digest, const Utility::DataBuffer::APtr& input);

    /*! \brief Calculate the digest for the given file
     */
    virtual Utility::DataBufferManaged::APtr calculateDigestForFile(const std::string& filename_abs);

    /*! \brief Create instance
     */
    static APtr create(void);

private:
    /*! \brief Constructor
     */
    Hash_CryptoPP_SHA1(void);

    /*! \brief Burn all internal buffers.
     */
    void burn(void);

    CryptoPP::SHA1 hash_;
};

}
}
#endif
