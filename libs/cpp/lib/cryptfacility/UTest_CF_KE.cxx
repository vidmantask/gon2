/*! \file CF_KE_UTest.cxx
 \brief This file contains unittest suite for the cryptfacility key-exchange
 */
#include <iostream>
#include <boost/test/unit_test.hpp>

#include <string>
#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#  include <cryptopp/oids.h>
#else
#  include <cryptopp/osrng.h>
#  include <cryptopp/oids.h>
#endif

#include <lib/cryptfacility/CF_ALL.hxx>

using namespace std;
using namespace CryptoPP;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

class TestClientIdentification : public ClientIdentification {
public:
	typedef boost::shared_ptr<TestClientIdentification> APtr;

	class MyException {
		string x;
	};

	TestClientIdentification(const std::string& idA, const std::string& idB) :
		idA_(idA), idB_(idB) {
	}
	TestClientIdentification(void) :
		idA_("Unitialized"), idB_("Unitialized") {
	}

	~TestClientIdentification(void) {
	}

	void init(const DataBuffer::APtr& dataBuffer) {
		long pos;

		DataBufferManaged::APtr idABuffer;
		pos = dataBuffer->parse(";", idABuffer);
		if (idABuffer.get() == NULL) {
			throw "Error paring client info";
		}
		idA_ = idABuffer->toString();

		DataBufferManaged::APtr idBBuffer;
		dataBuffer->parse(idBBuffer, pos);
		if (idBBuffer.get() == NULL) {
			throw "Error paring client info";
		}

		idB_ = idBBuffer->toString();
		if (idB_ == "throw") {
			throw MyException();
		}
	}

	DataBuffer::APtr getDataBuffer(void) const {
		DataBufferManaged::APtr buffer(DataBufferManaged::create(0));
		buffer->append(idA_);
		buffer->append(";");
		buffer->append(idB_);
		return buffer;
	}

	string getIdA(void) const {
		return idA_;
	}
	string getIdB(void) const {
		return idB_;
	}

private:
	std::string idA_;
	std::string idB_;
};

BOOST_AUTO_TEST_CASE( ske_server_create_fail)
{
	boost::asio::io_service io;
	try {
		DataBufferManaged::APtr clientKnownSecret;
		DataBufferManaged::APtr serverKnownSecret;
		KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);

		CryptFactory::APtr factory( CryptFacilityService::getInstance().getCryptFactory());
		DataBufferManaged::APtr serverKnownSecret_empty(DataBufferManaged::create(0));
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret_empty),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

		DataBufferManaged::APtr serverKnownSecret_invalid(DataBufferManaged::create("Invalid signing key"));
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret_invalid),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

		DataBufferManaged::APtr serverKnownSecret_tampered( serverKnownSecret->clone());
		(*serverKnownSecret_tampered)[0] = 't';
		(*serverKnownSecret_tampered)[1] = 't';
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret_tampered),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);
	}
	catch (std::exception& e) {
		cout << e.what() << "\n";
		BOOST_FAIL("Uncauht excetption");
	}
}

BOOST_AUTO_TEST_CASE( ske_client_create_fail)
{
	boost::asio::io_service io;
	try {
		DataBufferManaged::APtr clientKnownSecret;
		DataBufferManaged::APtr serverKnownSecret;
		KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);

		CryptFactory::APtr factory( CryptFacilityService::getInstance().getCryptFactory());
		KeyExchange_SessionKeyExchange_Server::APtr ske_server(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret));
		DataBuffer::APtr message( ske_server->generateMessage() );

		DataBufferManaged::APtr message_empty(DataBufferManaged::create(0));
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory, clientKnownSecret, message_empty, KeyStore::create(io, factory)),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

		DataBufferManaged::APtr message_invalid(DataBufferManaged::create("Garbage message"));
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory, clientKnownSecret, message_invalid, KeyStore::create(io, factory)),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

		DataBuffer::APtr message_tampered_publickey(message->clone());
		(*message_tampered_publickey)[2] = 't';
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory, clientKnownSecret, message_tampered_publickey, KeyStore::create(io, factory)),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

		DataBuffer::APtr message_tampered_signature(message->clone());
		(*message_tampered_signature)[message_tampered_signature->getSize()-1] = 't';
		BOOST_CHECK_THROW(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory, clientKnownSecret, message_tampered_signature, KeyStore::create(io, factory)),
				KeyExchange_SessionKeyExchange::Exception_CF_KE_SessionKeyExchange);

	}
	catch (std::exception& e) {
		cout << e.what() << "\n";
		BOOST_FAIL("Uncauht excetption");
	}
}

BOOST_AUTO_TEST_CASE( cif_server_fail)
{
	boost::asio::io_service io;
	try {
		DataBufferManaged::APtr clientKnownSecret;
		DataBufferManaged::APtr serverKnownSecret;
		KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);

		CryptFactory::APtr factory( CryptFacilityService::getInstance().getCryptFactory());
		Crypter::APtr crypter_client(Crypter::create(factory));
		Crypter::APtr crypter_server(Crypter::create(factory));

		/* session-key-exchage */
		KeyExchange_SessionKeyExchange_Server::APtr ske_server(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret));
		DataBuffer::APtr message_ske( ske_server->generateMessage() );
		KeyExchange_SessionKeyExchange::APtr ske_client(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory,
						clientKnownSecret,
						message_ske, KeyStore::create(io, factory)));

		/* cif exchange */
		TestClientIdentification::APtr testClientInfo_client(new TestClientIdentification("Ib", "throw"));
		KeyExchange_CIFExchange_Client::APtr cif_client(KeyExchange_CIFExchange_Client::create(*factory));
		DataBuffer::APtr message_cif(cif_client->generateMessage(ske_client, crypter_client, testClientInfo_client));

		TestClientIdentification::APtr testClientInfo_server(new TestClientIdentification);

		DataBuffer::APtr message_cif_empty(DataBufferManaged::create(0));
		BOOST_CHECK_THROW( KeyExchange_CIFExchange_Server::createFromMessage(ske_server,
						testClientInfo_server,
						crypter_server,
						message_cif_empty),
				KeyExchange_CIFExchange::Exception_CF_KE_CIFExchange);

		/* This test will stop to fail when the message is encrypted */
		DataBuffer::APtr message_cif_tampered_001(DataBufferManaged::create(message_cif));
		message_cif_tampered_001->data()[0] = 'x';
		BOOST_CHECK_THROW( KeyExchange_CIFExchange_Server::createFromMessage(ske_server,
						testClientInfo_server,
						crypter_server,
						message_cif_tampered_001),
				KeyExchange_CIFExchange::Exception_CF_KE_CIFExchange);

		BOOST_CHECK_THROW( KeyExchange_CIFExchange_Server::createFromMessage(ske_server,
						testClientInfo_server,
						crypter_server,
						message_cif),
				TestClientIdentification::MyException);

	}
	catch (std::exception& e) {
		cout << e.what() << "\n";
		BOOST_FAIL("Uncauht excetption");
	}
}

BOOST_AUTO_TEST_CASE( skp__x )
{
	boost::asio::io_service io;
	try {
		DataBufferManaged::APtr clientKnownSecret;
		DataBufferManaged::APtr serverKnownSecret;
		KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);

		CryptFactory::APtr factory( CryptFacilityService::getInstance().getCryptFactory());

		/* session-key-exchage */
		KeyExchange_SessionKeyExchange_Server::APtr ske_server(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret));
		DataBuffer::APtr message_ske( ske_server->generateMessage() );
		KeyExchange_SessionKeyExchange::APtr ske_client(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory,
						clientKnownSecret,
						message_ske, KeyStore::create(io, factory)));

		/* cif exchange */
		Crypter::APtr crypter_client(Crypter::create(factory));

		TestClientIdentification::APtr testClientInfo_client(new TestClientIdentification("Ib", "Hansen"));
		KeyExchange_CIFExchange_Client::APtr cif_client(KeyExchange_CIFExchange_Client::create(*factory));
		DataBuffer::APtr message_cif(cif_client->generateMessage(ske_client, crypter_client, testClientInfo_client));

		Crypter::APtr crypter_server(Crypter::create(factory));
		TestClientIdentification::APtr testClientInfo_server(new TestClientIdentification);
		KeyExchange_CIFExchange_Server::APtr cif_server(KeyExchange_CIFExchange_Server::createFromMessage(ske_server,
						testClientInfo_server,
						crypter_server,
						message_cif));
		BOOST_CHECK( testClientInfo_server->getIdA() == testClientInfo_client->getIdA() );
		BOOST_CHECK( testClientInfo_server->getIdB() == testClientInfo_client->getIdB() );

		/* skp-x exchange */
		int skpType = 1;
		DataBufferManaged::APtr additionalData(DataBufferManaged::create("Hello"));

		KeyExchange_CryptExchange_Server::APtr ce_server(KeyExchange_CryptExchange_Server::create(*cif_server));
		DataBuffer::APtr message_ce(ce_server->generateMessage(*ske_server, skpType, additionalData));

		try {
			KeyExchange_CryptExchange_Client::APtr ce_client(KeyExchange_CryptExchange_Client::createFromMessage(*ske_client, *crypter_client, *factory, *message_ce));
		}
		catch(KeyExchange_CryptExchange_Client::Exception_CF_KE_CryptExchange_Special_SPK& e) {
			BOOST_CHECK( e.getSKPType()== skpType );
			BOOST_CHECK( *(e.getAdditionalData()) == (*additionalData) );
		}

	}
	catch (std::exception& e) {
		cout << e.what() << "\n";
		BOOST_FAIL("Uncauht excetption");
	}
}

BOOST_AUTO_TEST_CASE( ke_complete_key_exchange)
{
	boost::asio::io_service io;
	try {
		DataBufferManaged::APtr clientKnownSecret;
		DataBufferManaged::APtr serverKnownSecret;
		KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(clientKnownSecret, serverKnownSecret);

		CryptFactory::APtr factory( CryptFacilityService::getInstance().getCryptFactory());

		/* session-key-exchage */
		KeyExchange_SessionKeyExchange_Server::APtr ske_server(KeyExchange_SessionKeyExchange_Server::create(io, factory, serverKnownSecret));
		DataBuffer::APtr message_ske( ske_server->generateMessage() );
		KeyExchange_SessionKeyExchange::APtr ske_client(KeyExchange_SessionKeyExchange_Client::createFromMessage(factory,
						clientKnownSecret,
						message_ske, KeyStore::create(io, factory)));

		/* cif exchange */
		Crypter::APtr crypter_client(Crypter::create(factory));

		TestClientIdentification::APtr testClientInfo_client(new TestClientIdentification("Ib", "Hansen"));
		KeyExchange_CIFExchange_Client::APtr cif_client(KeyExchange_CIFExchange_Client::create(*factory));
		DataBuffer::APtr message_cif(cif_client->generateMessage(ske_client, crypter_client, testClientInfo_client));

		Crypter::APtr crypter_server(Crypter::create(factory));
		TestClientIdentification::APtr testClientInfo_server(new TestClientIdentification);
		KeyExchange_CIFExchange_Server::APtr cif_server(KeyExchange_CIFExchange_Server::createFromMessage(ske_server,
						testClientInfo_server,
						crypter_server,
						message_cif));
		BOOST_CHECK( testClientInfo_server->getIdA() == testClientInfo_client->getIdA() );
		BOOST_CHECK( testClientInfo_server->getIdB() == testClientInfo_client->getIdB() );

		/* crypt exchange */
		KeyExchange_CryptExchange_Server::APtr ce_server(KeyExchange_CryptExchange_Server::create(*cif_server));
		DataBuffer::APtr message_ce(ce_server->generateMessage(*ske_server, *crypter_server, *factory));

		KeyExchange_CryptExchange_Client::APtr ce_client(KeyExchange_CryptExchange_Client::createFromMessage(*ske_client, *crypter_client, *factory, *message_ce));

		Crypt::APtr encrypter_client(crypter_client->getEncrypter());
		Crypt::APtr decrypter_client(crypter_client->getDecrypter());
		Hash::APtr hash_client(crypter_client->getHash());

		Crypt::APtr encrypter_server(crypter_server->getEncrypter());
		Crypt::APtr decrypter_server(crypter_server->getDecrypter());
		Hash::APtr hash_server(crypter_server->getHash());

		BOOST_CHECK( encrypter_client->getName() == encrypter_server->getName() );
		BOOST_CHECK( decrypter_client->getName() == decrypter_server->getName() );

		/* upstream validation */
		DataBuffer::APtr message_up(KeyExchange_CryptStreamValidator::generateMessage(*crypter_client, *(ce_client->getReceipt())));
		KeyExchange_CryptStreamValidator::validateMessage(*crypter_server, *(ce_server->getReceipt()), *message_up);

		/* Downstream validation */
		DataBuffer::APtr message_down(KeyExchange_CryptStreamValidator::generateMessage(*crypter_server, *(ce_server->getReceipt())));
		KeyExchange_CryptStreamValidator::validateMessage(*crypter_client, *(ce_client->getReceipt()), *message_down);

	}
	catch (std::exception& e) {
		cout << e.what() << "\n";
		BOOST_FAIL("Uncauht excetption");
	}
}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
	CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
	CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
	return 0;
}
