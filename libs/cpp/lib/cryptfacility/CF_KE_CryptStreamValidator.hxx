/*! \file CF_KE_CryptStreamValidator.hxx
    \brief This file contains classes for valdating the upstream and downstream cipher
*/
#ifndef CF_KE_CRYPTSTREAMVALIDATOR_HXX
#define CF_KE_CRYPTSTREAMVALIDATOR_HXX

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypter.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>


namespace Giritech {
namespace CryptFacility {


  /*! \brief Trait class containing methods to validate the upstream and downstream ciphers

  */
  class CRYPTFACILITY_DLL KeyExchange_CryptStreamValidator : boost::noncopyable {
  public:

    /*! \brief This exception is used if the validation of a message fails
     */
    class Exception_Invalid : public Exception_CF {
    public:
      Exception_Invalid(const std::string& message) 
        : Exception_CF(message) {
      }
    };

    /*! \brief Encrypt the receipt into a message using the encrypter
    */
    static Utility::DataBufferManaged::APtr generateMessage(const Crypter& crypter, const Utility::DataBuffer& receipt);

   
    /*! \brief Decrypts the receipt using the decrypter, and compare it to the given reciept. 

    If the decrypted receipt do not match an exception is thrown.
    */
    static void validateMessage(const Crypter& crypter, 
                                const Utility::DataBuffer& receipt, 
                                const Utility::DataBuffer& message);    

  };
}
}

#endif
