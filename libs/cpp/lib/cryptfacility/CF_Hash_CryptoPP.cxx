/*! \file CF_Hash_CryptoPP.cxx
    \brief This file contains the implementation of then interface classes to hash algorithm in the Crypto++ library.
*/
#include <sstream>
#include <iostream>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>

using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace std;

using namespace CryptoPP;


/*
  ------------------------------------------------------------------
  Hash_CryptoPP_SHA1 implementation
  ------------------------------------------------------------------
*/
Hash_CryptoPP_SHA1::Hash_CryptoPP_SHA1(void) :
    Hash() {
}

Hash_CryptoPP_SHA1::~Hash_CryptoPP_SHA1(void) {
    burn();
}

Hash_CryptoPP_SHA1::APtr Hash_CryptoPP_SHA1::create(void) {
    return APtr(new Hash_CryptoPP_SHA1());
}

long Hash_CryptoPP_SHA1::digestSize(void) const {
  return hash_.DigestSize();
}

std::string Hash_CryptoPP_SHA1::getName(void) const {
  return hash_.AlgorithmName();
}

DataBufferManaged::APtr Hash_CryptoPP_SHA1::calculateDigest(const Utility::DataBuffer::APtr& input) {
  assert(input.get() != NULL);
  DataBufferManaged::APtr digest(DataBufferManaged::create(digestSize()));
  hash_.CalculateDigest(digest->data(), input->data(), input->getSize());
  return digest;
}

bool Hash_CryptoPP_SHA1::verifyDigest(const Utility::DataBuffer::APtr& digest, const Utility::DataBuffer::APtr& input) {
  assert(input.get() != NULL);
  assert(digest.get() != NULL);
  return hash_.VerifyDigest(digest->data(), input->data(), input->getSize());
}

Utility::DataBufferManaged::APtr Hash_CryptoPP_SHA1::calculateDigestForFile(const std::string& filename_abs) {
    string hash;
    CryptoPP::FileSource(filename_abs.c_str(), true, new CryptoPP::HashFilter(hash_, new CryptoPP::HexEncoder(new CryptoPP::StringSink(hash))));
    return DataBufferManaged::create(hash);
}

void Hash_CryptoPP_SHA1::burn(void) {
  /* Nothing to burn */
}
