/*! \file CF_CryptFactory.cxx
 \brief This file contains the implementation of the CryptFactory class.
 */
#include <sstream>
#include <lib/cryptfacility/CF_CryptFactory.hxx>

using namespace Giritech::CryptFacility;
using namespace std;

/*
 ------------------------------------------------------------------ 
 CryptFactory implementation
 ------------------------------------------------------------------ 
 */
CryptFactory::CryptFactory(const std::string& factoryName)
	: factoryName_(factoryName) {
}

CryptFactory::~CryptFactory() {
}

std::string CryptFactory::getFactoryName(void) const {
    return factoryName_;
}

