/*! \file CF_KE_SessionKeyExchange.cxx
    \brief This file contains the implementation for handling the session key exchange stats of the key exchange mecanisme
*/
#include <sstream>
#include <iostream>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_KE_SessionKeyExchange.hxx>
#include <lib/utility/UY_String.hxx>

#include <osrng.h>
#include <oids.h>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

using namespace CryptoPP;


/* 
   ------------------------------------------------------------------ 
   KeyExchange_SessionKeyExchange implementation
   ------------------------------------------------------------------ 
*/
KeyExchange_SessionKeyExchange::KeyExchange_SessionKeyExchange(const CryptFactory::APtr& cryptFactory,
                                                               const Utility::DataBufferManaged::APtr& knownSecret,
                                                               const KeyStore::APtr& keyStore)
  :  cryptFactory_(cryptFactory), sessionCrypter_(Crypter::create(cryptFactory)), keyStore_(keyStore) {
  setSigningValidationKeyFromKnownSecret(knownSecret, signingKey_, validationKey_);
  initPublicPrivateKeys();
}

KeyExchange_SessionKeyExchange::KeyExchange_SessionKeyExchange(const CryptFactory::APtr& cryptFactory, 
                                                               const Utility::DataBufferManaged::APtr& knownSecret,
                                                               const Utility::DataBuffer::APtr& remotePublicSessionKey,
                                                               const KeyStore::APtr& keyStore)
  :  cryptFactory_(cryptFactory), sessionCrypter_(Crypter::create(cryptFactory)), keyStore_(keyStore) {
  assert(knownSecret.get() != NULL);
  assert(remotePublicSessionKey.get() != NULL);
  setSigningValidationKeyFromKnownSecret(knownSecret, signingKey_, validationKey_);
  initPublicPrivateKeys();
  setRemotePublicSessionKey(remotePublicSessionKey);
}

void KeyExchange_SessionKeyExchange::initPublicPrivateKeys(void) {
	assert(sessionCrypter_.get() != NULL);
	sessionCrypter_->initDecrypter(Crypt_CryptoPP_PKAES::create(keySize), keyStore_->getSessionKeyPrivate());
}

void KeyExchange_SessionKeyExchange::setRemotePublicSessionKey(const Utility::DataBuffer::APtr& remotePublicSessionKeyRaw) {
  sessionCrypter_->initEncrypter(Crypt_CryptoPP_PKAES::create(keySize), remotePublicSessionKeyRaw);
}

Utility::DataBufferManaged::APtr KeyExchange_SessionKeyExchange::getPublicKey(void) const {
	return keyStore_->getSessionKeyPublic();
}

bool KeyExchange_SessionKeyExchange::verifySignedBuffer(const Utility::DataBuffer::APtr& buffer, 
                                                        const Utility::DataBuffer::APtr& signitureBuffer) const {
  PKSignatureScheme::PublicKey validationKey(loadKey<PKSignatureScheme::PublicKey>(validationKey_));
  PKSignatureScheme::Verifier verifier(validationKey);
  return verifier.VerifyMessage(buffer->data(), buffer->getSize(), signitureBuffer->data(), signitureBuffer->getSize());
}


void KeyExchange_SessionKeyExchange::burn(void) {
  if(signingKey_.get() != NULL) {
    signingKey_->burn();
  }
  if(validationKey_.get() != NULL) {
    validationKey_->burn();
  }
}

KeyStore::APtr KeyExchange_SessionKeyExchange::getKeyStore(void) const {
	return keyStore_;
}

CryptFactory::APtr KeyExchange_SessionKeyExchange::getCryptFactory(void) {
	return cryptFactory_;
}

DataBuffer::APtr KeyExchange_SessionKeyExchange::signBuffer(const Utility::DataBuffer::APtr& buffer) const {
  try {
    PKSignatureScheme::PrivateKey signingKey(loadKey<PKSignatureScheme::PrivateKey>(signingKey_));
    PKSignatureScheme::Signer signer(signingKey);
    assert(signer.SignatureLength() != 0);
    DataBufferManaged::APtr signatureBuffer(DataBufferManaged::create(signer.SignatureLength()));
    RandomNumberGenerator rng;
    signer.SignMessage(rng, buffer->data(), buffer->getSize(), signatureBuffer->data());
    return signatureBuffer;
  }
  catch(const CryptoPP::Exception& e) {
    /*! \fips_stf Exception_SelftestFailed("Crypto++ selftest failed.", <Error message from Crypto++>) */
    throw CryptFacilityService::Exception_SelftestFailed("Crypto++ selftest failed.", e.what());
  }
}


void KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(Utility::DataBufferManaged::APtr& clientKnownSecret, 
                                                                      Utility::DataBufferManaged::APtr& serverKnownSecret) {
  DataBufferManaged::APtr downSigningKey;
  DataBufferManaged::APtr downVerficationKey;

  generateSigningVerificationKeys(downSigningKey, downVerficationKey);
  downSigningKey->autoBurn();
  downVerficationKey->autoBurn();

  DataBufferManaged::APtr upSigningKey;
  DataBufferManaged::APtr upVerficationKey;
  generateSigningVerificationKeys(upSigningKey, upVerficationKey);
  upSigningKey->autoBurn();
  upVerficationKey->autoBurn();

  clientKnownSecret = DataBufferManaged::create(0);
  clientKnownSecret->appendWithLen(downVerficationKey, 2);
  clientKnownSecret->append(upSigningKey);

  serverKnownSecret = DataBufferManaged::create(0);
  serverKnownSecret->appendWithLen(upVerficationKey, 2);
  serverKnownSecret->append(downSigningKey);
}

void KeyExchange_SessionKeyExchange::generateSigningVerificationKeys(Utility::DataBufferManaged::APtr& signingKeyBuffer, 
                                                                     Utility::DataBufferManaged::APtr& validationKeyBuffer) {
  try {
    RandomNumberGenerator rng;
    PKSignatureScheme::PrivateKey signingKey;
    PKSignatureScheme::PublicKey  validationKey;

    signingKey.Initialize(rng, ASN1::sect233k1());
    signingKey.MakePublicKey(validationKey);

    boost::shared_ptr<string> validationKeyString(new string);
    CryptoPP::StringSink validationKeySink(*(validationKeyString.get()));
    validationKey.Save(validationKeySink);
    validationKeySink.MessageEnd();

    boost::shared_ptr<string> signingKeyString(new string);
    CryptoPP::StringSink signingKeySink(*(signingKeyString.get()));
    signingKey.Save(signingKeySink);
    signingKeySink.MessageEnd();
  
    signingKeyBuffer    = DataBufferManaged::create(*(signingKeyString.get()));
    validationKeyBuffer = DataBufferManaged::create(*(validationKeyString.get()));
    String::burnString(*(signingKeyString.get()));
    String::burnString(*(validationKeyString.get()));
    
  }
  catch(const CryptoPP::Exception& e) {
    /*! \fips_stf Exception_SelftestFailed("Crypto++ selftest failed.", <Error message from Crypto++>) */
    throw CryptFacilityService::Exception_SelftestFailed("Crypto++ selftest failed.", e.what());
  }
}


void KeyExchange_SessionKeyExchange::setSigningValidationKeyFromKnownSecret(const Utility::DataBufferManaged::APtr& knownSecret,
                                                                            Utility::DataBufferManaged::APtr& signingKey,
                                                                            Utility::DataBufferManaged::APtr& validationKey) {

  assert(knownSecret.get() != NULL);
  long pos = 0;
  try {
    pos = knownSecret->parseWithLen(validationKey, 2);
    if(validationKey.get()==NULL || knownSecret->getSize() == 0) {
      throw Exception_CF_KE_SessionKeyExchange("Invalid known secret, unable to get validation key");
    }
    knownSecret->parse(signingKey, pos);
    if(signingKey.get()==NULL) {
      throw Exception_CF_KE_SessionKeyExchange("Invalid known secret, unable to get signing key");
    }
  }
  catch(DataBuffer::Exception_Out_of_bound& e) {
    throw Exception_CF_KE_SessionKeyExchange("Invalid known secret. " + string(e.what()));
  }
  catch(DataBuffer::Exception_decoding& e) {
    throw Exception_CF_KE_SessionKeyExchange("Invalid known secret. " + string(e.what()));
  }

}

/* 
   ------------------------------------------------------------------ 
   KeyExchange_SessionKeyExchange_Client implementation
   ------------------------------------------------------------------ 
*/
//KeyExchange_SessionKeyExchange::APtr KeyExchange_SessionKeyExchange_Client::createFromMessage(const CryptFactory::APtr& cryptFactory,
//                                                                                              const Utility::DataBufferManaged::APtr& knownSecret,
//                                                                                              const Utility::DataBuffer::APtr& message) {
//
//	return createFromMessage(cryptFactory, knownSecret, message, KeyStore::create(cryptFactory));
//}

KeyExchange_SessionKeyExchange::APtr KeyExchange_SessionKeyExchange_Client::createFromMessage(const CryptFactory::APtr& cryptFactory,
                                                                                              const Utility::DataBufferManaged::APtr& knownSecret,
                                                                                              const Utility::DataBuffer::APtr& message,
                                                                                              const KeyStore::APtr& keyStore) {
  assert(knownSecret.get() != NULL);
  assert(message.get() != NULL);

  try {
    /* Parse message */
    long pos = 0;
    DataBufferManaged::APtr remotePublicKeyBuffer;
    pos = message->parseWithLen(remotePublicKeyBuffer, 2);
    if(remotePublicKeyBuffer.get() == NULL || remotePublicKeyBuffer->getSize() == 0 ) {
      throw Exception_CF_KE_SessionKeyExchange("Invalid size of public key");
    }

    DataBufferManaged::APtr signatureBuffer;
    message->parse(signatureBuffer, pos);
    if(signatureBuffer.get()==NULL) {
      throw Exception_CF_KE_SessionKeyExchange("Invalid size of signature");
    }

    /* Create instance */
    APtr ske(new KeyExchange_SessionKeyExchange_Client(cryptFactory, knownSecret, remotePublicKeyBuffer, keyStore));

    /* Validate signature of public key */
    if(!ske->verifySignedBuffer(remotePublicKeyBuffer, signatureBuffer)) {
      throw Exception_CF_KE_SessionKeyExchange("Signature could not be verified");
    }

    assert(ske.get() != NULL);
    return ske;
  }
  catch(DataBuffer::Exception_Seperator_not_found&) {
    throw Exception_CF_KE_SessionKeyExchange("Invalid message skp datagram");
  }
  catch(DataBuffer::Exception_Out_of_bound&) {
    throw Exception_CF_KE_SessionKeyExchange("Invalid skp datagram");
  }
  catch(CryptoPP::Exception& e) {
    throw Exception_CF_KE_SessionKeyExchange(e.what());
  }
}

void KeyExchange_SessionKeyExchange_Client::burn(void) {
  /* Nothing to burn */
}





/* 
   ------------------------------------------------------------------ 
   KeyExchange_SessionKeyExchange_Server implementation
   ------------------------------------------------------------------ 
*/
KeyExchange_SessionKeyExchange_Server::KeyExchange_SessionKeyExchange_Server(const CryptFactory::APtr& cryptFactory,
                                                                             const Utility::DataBufferManaged::APtr& knownSecret,
                                                                             const KeyStore::APtr& keyStore)
  : KeyExchange_SessionKeyExchange(cryptFactory, knownSecret, keyStore) {
}

DataBufferManaged::APtr KeyExchange_SessionKeyExchange_Server::generateMessage(void) const {
  try {

    /* Generate message */
    DataBufferManaged::APtr message(DataBufferManaged::create(0));
    message->appendWithLen(keyStore_->getSessionKeyPublic(), 2);
    message->append(signBuffer(keyStore_->getSessionKeyPublic()));

    return message;
  }
  catch(CryptoPP::Exception& e) {
    throw Exception_CF_KE_SessionKeyExchange(e.what());
  }
}

KeyExchange_SessionKeyExchange_Server::APtr KeyExchange_SessionKeyExchange_Server::create(
		boost::asio::io_service& io,
		const CryptFactory::APtr& cryptFactory,
		const Utility::DataBufferManaged::APtr& knownSecret) {
  try {
    KeyExchange_SessionKeyExchange_Server::APtr ske( new KeyExchange_SessionKeyExchange_Server(cryptFactory, knownSecret, KeyStore::create(io, cryptFactory)) );
    assert(ske.get() != NULL);
    return ske;                                                
  }
  catch(CryptoPP::Exception& e) {
    throw Exception_CF_KE_SessionKeyExchange(e.what());
  }
}



void KeyExchange_SessionKeyExchange_Server::burn(void) {
  /* Nothing to burn */
}
