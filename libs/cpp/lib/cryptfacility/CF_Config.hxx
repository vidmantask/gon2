/*! \file CF_Config.hxx
    \brief Cryptfacility lib configurations
*/
#ifndef CF_CONFIG_HXX
#define CF_CONFIG_HXX


#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC

# ifdef GOPTION_CRYPTFACILITY_DLL_IMPORT
#   define CRYPTFACILITY_DLL __declspec(dllimport)
# endif
# ifdef GOPTION_CRYPTFACILITY_DLL_EXPORT
#   define CRYPTFACILITY_DLL __declspec(dllexport)
# endif

#else

# define CRYPTFACILITY_DLL

#endif

#endif
