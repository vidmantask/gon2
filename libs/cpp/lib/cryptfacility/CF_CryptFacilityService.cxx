/*! \file CF_CryptFacilityService.cxx
    \brief This file contains the implementation of the CryptFacilityService class.
*/
#include <sstream>
#include <fstream>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#  include <cryptopp/hmac.h>
#endif

#include <lib/version/Version.hxx>
#include <lib/utility/UY_Types.hxx>
#include <lib/version/Version.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>
#include <lib/cryptfacility/CF_CryptFactory_CryptoPP.hxx>


using namespace Giritech::CryptFacility;
using namespace Giritech::Utility;
using namespace std;



/*
  ------------------------------------------------------------------
  CryptFacilityService implementation
  ------------------------------------------------------------------
*/
CryptFacilityService::APtr CryptFacilityService::globalInstance;

CryptFacilityService& CryptFacilityService::getInstance(void) {
  if(globalInstance.get() == NULL) {
    globalInstance = CryptFacilityService::APtr( new CryptFacilityService() );
  }
  return (*globalInstance);
}


void CryptFacilityService::initialize(const ModeOfOperation modeOfOperation) {

#ifndef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
  if(modeOfOperation == modeofoperation_fips) {
    /*! \fips_stf Exception_SelftestFailed("FIPS mode of operation is not available in this build") */
    throw Exception_SelftestFailed("FIPS mode of operation is not available in this build");
  }
#endif
  modeOfOperation_ = modeOfOperation;

  /* Do power up selftest */
  if(state_ == state_selftest_TOFAIL) {
    /*! \fips_stf Exception_SelftestFailed("CryptFacility PowerUp selftest failed") */
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
  try {
    CryptoPP::DoDllPowerUpSelfTest();
    if(CryptoPP::GetPowerUpSelfTestStatus() != CryptoPP::POWER_UP_SELF_TEST_PASSED) {
      /*! \fips_stf Exception_SelftestFailed("Crypto++ PowerUp selftest failed.", "PowerUp test has not passed.") */
      throw Exception_SelftestFailed("Crypto++ PowerUp selftest failed.", "PowerUp test has not passed.");
    }
  }
  catch(const CryptoPP::Exception& e) {
    /*! \fips_stf Exception_SelftestFailed("Crypto++ selftest failed.", <Error message from Crypto++>) */
    throw Exception_SelftestFailed("Crypto++ PowerUp selftest failed.", e.what());
  }
#endif
  state_ = state_selftest_OK;

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
  if(modeOfOperation_ == modeofoperation_fips && !(isModeOfOperationFIPS())) {
    /*! \fips_stf Exception_SelftestFailed("CryptFacility and CryptPP disagree on FIPS mode selection")  */
    throw Exception_SelftestFailed("CryptFacility and CryptPP disagree on FIPS mode selection");
  }
#endif
}


bool CryptFacilityService::isModeOfOperationFIPS(void) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
  return (modeOfOperation_ == modeofoperation_fips && CryptoPP::FIPS_140_2_ComplianceEnabled());
#endif
#ifndef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
  return false;
#endif
}

void CryptFacilityService::getVersionInfo(std::string& buildDate, int& versionMajor, int& versionMinor, std::string& versionRevision) const {
  if(!isReady()) {
    /*! \fips_stf Exception_SelftestFailed("CryptFacility PowerUp selftest failed") */
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }
  Giritech::Version::Version::APtr version(Giritech::Version::Version::create_current());

  buildDate       = version->get_build_date();
  versionMajor    = version->get_version_major();
  versionMinor    = version->get_version_minor();
  versionRevision = version->get_version_build_id();
}



bool CryptFacilityService::isReady(void) const {
  return (state_ == state_selftest_OK);
}

void CryptFacilityService::enablePowerOff(void) {
  state_ = state_poweroff;
}


void CryptFacilityService::enableFakeSelftestError(void) {
  state_ = state_selftest_TOFAIL;
}


#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
static HMODULE dllModuleHandlerDLLMain;
void CryptFacilityService::getIntegrityHash(std::string& hmacString) {
  if(!isReady()) {
    /*! \fips_stf Exception_SelftestFailed("CryptFacility PowerUp selftest failed") */
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }

  byte_t hmacKey[] = {0x45, 0x1E, 0x32, 0x96, 0x65, 0xB1, 0x6F, 0xED, 0x0B, 0xF8, 0x6B, 0xFD, 0x02, 0x65, 0x07, 0xCC};

  /* Get filename of dll */
  HMODULE dllModuleHandler = GetModuleHandle("CryptFacilityDLL");
  if(dllModuleHandler!=dllModuleHandlerDLLMain) {
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }

  char moduleFilename[MAX_PATH];
  DWORD rc = GetModuleFileNameA(dllModuleHandler, moduleFilename, sizeof(moduleFilename));
  if(rc==NULL) {
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }

  /* Calculate hash */
  std::auto_ptr<CryptoPP::MessageAuthenticationCode> hmac(new CryptoPP::HMAC<CryptoPP::SHA1>(hmacKey, sizeof(hmacKey)));
  string* dllHMACSink = new string;
  CryptoPP::FileSource(moduleFilename, true, new CryptoPP::HashFilter(*hmac, new CryptoPP::HexEncoder(new CryptoPP::StringSink(*dllHMACSink))));
  hmacString = (*dllHMACSink);
}

void CryptFacilityService::setDLLModuleHandler(HMODULE dllModuleHandler) {
  dllModuleHandlerDLLMain = dllModuleHandler;
}
#endif

#ifndef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
void CryptFacilityService::getIntegrityHash(std::string& hmacString) {
  hmacString = "Valid Hash not avaiable, because the module is not compiled in FIPS mode";
}
#endif

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
void CryptFacilityService::getCryptoPPModuleFilename(std::string& module_filename) {
	/* Get filename of cryptopp */
	HMODULE dllModuleHandler = GetModuleHandle("cryptopp");

	char moduleFilename[MAX_PATH];
	DWORD rc = GetModuleFileNameA(dllModuleHandler, moduleFilename, sizeof(moduleFilename));
	if(rc==NULL) {
		throw Exception_SelftestFailed("Crypt++ module filename not available");
	}
	module_filename = moduleFilename;
}
#else
void CryptFacilityService::getCryptoPPModuleFilename(std::string& module_filename) {
	module_filename = "Crypto++ module filename is not available, because this program is not compiled in FIPS mode";
}
#endif


CryptFactory::APtr CryptFacilityService::getCryptFactory(void) {
  if(!isReady()) {
    /*! \fips_stf Exception_SelftestFailed("CryptFacility PowerUp selftest failed") */
    throw Exception_SelftestFailed("CryptFacility PowerUp selftest failed");
  }
  return CryptFactory_CryptoPP::create();
}
