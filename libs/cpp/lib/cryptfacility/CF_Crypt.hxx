/*! \file CF_Crypt.hxx
    \brief This file contains the abstrac class representing an en-/decryption algorithm
*/
#ifndef CF_CRYPT_HXX
#define CF_CRYPT_HXX

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <lib/version/Version.hxx>
#include <lib/utility/UY_Types.hxx>
#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>


namespace Giritech {
namespace CryptFacility {

  /*! \brief This exception is thrown if a ivalid state is encountered.
  */
  class Exception_CF_Invalid_State : public Exception_CF {
  public:
    Exception_CF_Invalid_State(const std::string& message) 
      : Exception_CF(message) {
    }
  };


  /*! \brief This class abstract class represent an en-/decryption algorithm
  */
  class CRYPTFACILITY_DLL Crypt {
  public:
    typedef boost::shared_ptr<Crypt> APtr;
    typedef Crypt* Ptr;

    enum State {state_unknown, state_burned, state_keyInitialized};
 
    /*! \brief Destructor
      
      On destruction all internal state variables are burned, \see burn
    */
    virtual ~Crypt(void){
      burn();
    }

    /*! \brief Clone current instance
    */
    virtual APtr clone(void) const = 0;


    /*! \brief Set key an initialization vector
    */
    virtual void setKeyWithIV(const Utility::byte_t* key, const Utility::byte_t* iv);

    /*! \brief Set encryption key
    */
    virtual void setEncryptionKey(const Giritech::Utility::DataBuffer::APtr& buffer) {
    };

    /*! \brief Set decryption key
    */
    virtual void setDecryptionKey(const Giritech::Utility::DataBuffer::APtr& buffer) {
    };


    /*! \brief Encrypt a buffer of bytes
    */
    virtual void encrypt(const Utility::byte_t* plainBuffer, const int bufferLen, Utility::byte_t* cipherBuffer) = 0;


    /*! \brief Decrypt a buffer of bytes
    */
    virtual void decrypt(const Utility::byte_t* cipherBuffer, const int bufferLen, Utility::byte_t* plainBuffer) = 0;
    
    /*! \brief Return the key length used by this instance.
    */
    int getKeyLen(void) const;

    /*! \brief Return the name of the crypt algorithm
    */
    virtual std::string getName(void) const = 0;
    
    /*! \brief Return the length of the plain text buffer after encryption and decrytion

    This function can be used to resize the plaintext buffer.
    */
    virtual long getPlainBufferSize(const long plainBufferSize) const = 0;


    /*! \brief Return the length of the plain text buffer after encryption

    This function can be used to resize the plaintext buffer.
    */
    virtual long getPlainBufferSizeAfter(const long cipherBufferSize) const = 0;


    /*! \brief Return the length of the cipher text buffer after encryption.
    */
    virtual long getCipherBufferSize(const long plainBufferSize) const = 0;


    /*! \brief Return the block size
    */
    virtual long getBlockSize(void) const = 0;


    /*! \brief Asymmetric PWC-Test (Pairwise Consistency test of generated key pair)
    */
    bool pairwiseConsistencyTest_Asymetric( const Utility::DataBuffer::APtr& publicKey, 
                                            const Utility::DataBuffer::APtr& privateKey);

    /*! \brief Symmetric PWC-Test (Pairwise Consistency test of generated key/iv)
    */
    bool knownAnswerTest_Symetric( const Utility::DataBuffer::APtr& key, 
                                   const Utility::DataBuffer::APtr& iv);
    /*! \brief Performing the actual consisten test(Pairwise and known-ansewer test
    */
    bool consistencyTest(void);

  private:
    /*! \brief Burn all internal buffers. 

    After calling this method this instance can no longer be used and will throw exceptions.
    */
    void burn(void); 

  protected:
    /*! \brief Constructor
        \param _keyLen Keylen to be used by this instance. The keylen is in bits.
    */
    Crypt(const int keyLen)
      : keyLen_(keyLen), state_(state_unknown) {
    }

    virtual void readyToEncrypt(const int bufferLen);
    virtual void readyToDecrypt(const int bufferLen);
    virtual void setInitialized(void);
    virtual void setBurned(void);

  private:
    int         keyLen_;
    enum State  state_;
  };

}
}

#endif
