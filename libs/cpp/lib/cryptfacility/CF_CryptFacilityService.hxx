/*! \file CF_CryptFacilityService.hxx
 \brief This file contains general services for CryptFacility
 */
#ifndef CF_CRYPTFACILITYSERVICE_HXX
#define CF_CRYPTFACILITYSERVICE_HXX

#include <string>
#include <vector>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
# define WIN32_LEAN_AND_MEAN   // Exclude rarely-used stuff from Windows headers
# include <windows.h>
#endif

#include <lib/utility/UY_Exception.hxx>
#include <lib/cryptfacility/CF_Config.hxx>

namespace Giritech {
namespace CryptFacility {

class CryptFactory;

/*! \brief This exception is used as base for alle exceptions thrown by the module CryptFacility
 */
class Exception_CF : public Giritech::Utility::Exception {
public:
    Exception_CF(const std::string& message) :
        Exception(message) {
    }
    virtual ~Exception_CF(void) throw() {
    }
};

/*! \brief This class holds the state of the CryptFacility module

 Only one instance of CryptFacility class exists, and is only avaiable through the method getInstance.
 */
class CRYPTFACILITY_DLL CryptFacilityService : boost::noncopyable {
public:

    /*! \brief This exception is thrown if a selftest fails
     */
    class Exception_SelftestFailed : public Exception_CF {
public:
        Exception_SelftestFailed(const std::string& message) :
            Exception_CF(message) {
            CryptFacilityService::getInstance().enablePowerOff();
        }
        Exception_SelftestFailed(const std::string& message, const std::string& additionalMessage) :
            Exception_CF(message + " " + additionalMessage) {
            CryptFacilityService::getInstance().enablePowerOff();
        }
    };

    enum ModeOfOperation {modeofoperation_unknown, /*!< Unspecified mode of operation */
        modeofoperation_fips /*!< FIPS mode of operation */
    };

    /*! \brief Initialize the module and set the mode of operation
     */
    void initialize(const ModeOfOperation modeOfOperation);

    /*! \brief Returns version information of the module
     */
    void getVersionInfo(std::string& buildDate,
                        int& versionMajor,
                        int& versionMinor,
                        std::string& revision) const;

    /*! \brief Returns true if the FIPS mode of operation is enabled
     */
    bool isModeOfOperationFIPS(void);

    /*! \brief Returns true if module has been selftested and no errors detected.
     */
    bool isReady(void) const;

    /*! \brief Bring the module to the power-off state.

     In power-off state the CryptFacility module will throw exceptions when used because the isReady() functions
     wille fail. 
     The module can be brought into operation state by calling the method initialize().

     This method is automaticaly called every time the exception CryptFacilityService::Exception_SelftestFailed is thrown.
     */
    void enablePowerOff(void);

    /*! \brief Set the state of the module as if it the selftest has failed.
     */
    void enableFakeSelftestError(void);

    /*! \brief Return the avaiable crypt factory
     */
    boost::shared_ptr<CryptFactory> getCryptFactory(void);

    /*! \brief Returns the global instance

     Only one instance of CryptFacilityService class exists, and is only avaiable through this method.
     */
    static CryptFacilityService& getInstance(void);

    /*! \brief Build hash of dll file

     Through the dll-handler the dll-filename is avaiable, and used calculate a HMAC<SHA1> digest of the dll-module-file.
     An internal key is used when calculating he digest.

     The valid digest is available in the the file CryptFacilityDLL.digest and will be avaiable in this module specification at final release.
     */
    void getIntegrityHash(std::string& hmacString);

    /*! \brief Get used filename of crypto++ module
     */
    void getCryptoPPModuleFilename(std::string& module_filename);

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    /*! \brief Set the dll module handler to be used when calculating integrity hash.
     */
    static void setDLLModuleHandler(HMODULE dllModuleHandlerDLLMain);

#endif

private:
    /*! \brief Constructor
     */
    CryptFacilityService(void) :
        state_(state_unknown), modeOfOperation_(modeofoperation_unknown) {
    }

private:
    typedef boost::shared_ptr<CryptFacilityService> APtr;

    enum State {state_unknown, state_selftest_OK, state_selftest_FAILED, state_selftest_TOFAIL, state_poweroff};
    State state_;

    ModeOfOperation modeOfOperation_;

    static APtr globalInstance;
};
}
}

#endif
