/*! \file CF_KE_CIFExchange.cxx
    \brief This file contains the impplementation for handling the CIF(Client-Identification-Facility) state of the key exchange
*/
#include <sstream>
#include <iostream>

#include <lib/utility/UY_String.hxx>
#include <lib/cryptfacility/CF_KE_CIFExchange.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;

using namespace CryptoPP;


/* 
   ------------------------------------------------------------------ 
   KeyExchange_CIFExchange implementation
   ------------------------------------------------------------------ 
*/
KeyExchange_CIFExchange::KeyExchange_CIFExchange(void) {
}

void KeyExchange_CIFExchange::burn(void) {
  /* nothing to burn */
}



/* 
   ------------------------------------------------------------------ 
   KeyExchange_CIFExchange_Client implementation
   ------------------------------------------------------------------ 
*/
KeyExchange_CIFExchange_Client::KeyExchange_CIFExchange_Client(void)
	: KeyExchange_CIFExchange() {
}


KeyExchange_CIFExchange_Client::APtr KeyExchange_CIFExchange_Client::create(const CryptFactory& cryptFactory) {
  return APtr(new KeyExchange_CIFExchange_Client());
}


KeyExchange_CIFExchange_Client::~KeyExchange_CIFExchange_Client(void) {
  burn();
}


void KeyExchange_CIFExchange_Client::burn(void) {
  /* nothing to burn */
}



DataBufferManaged::APtr KeyExchange_CIFExchange_Client::generateMessage(const KeyExchange_SessionKeyExchange::APtr& ske,
                                                                        const Crypter::APtr& crypter,
                                                                        const ClientIdentification::APtr& clientIdentification) const {
  assert(crypter.get() != NULL);
  assert(clientIdentification.get() != NULL);
  
  /* Generate iv/key for the upstream */
  DataBufferManaged::APtr upstreamKey;
  DataBufferManaged::APtr upstreamIV;
  ske->getKeyStore()->getEncrypterKey(upstreamKey, upstreamIV);
  crypter->initEncrypter(ske->getCryptFactory()->getCryptBlockAES256(), upstreamKey, upstreamIV);

  /* create ciper/hash ids, no longer used */
  DataBufferManaged::APtr cipherIds(DataBufferManaged::create(0));
  DataBufferManaged::APtr hashIds(DataBufferManaged::create(0));

  /* Build cif buffer */
  DataBufferManaged::APtr message(DataBufferManaged::create(0));
  message->append(ske->getPublicKey()->encodeHex());
  message->append(",");
  message->append(cipherIds);
  message->append(",");
  message->append(hashIds);
  message->append(",");
  message->append(upstreamKey->encodeHex());
  message->append(",");
  message->append(upstreamIV->encodeHex());
  message->append(",");
  message->append(clientIdentification->getDataBuffer()->encodeHex());

  /* Add len and signiture */
  DataBufferManaged::APtr messageCIF(DataBufferManaged::create(0));
  messageCIF->appendWithLen(message, 2);
  messageCIF->append(ske->signBuffer(message));

  /* Pad buffer, and encrypt using the session encryption */
  Crypt::APtr sessionCrypter(ske->getSessionCrypter()->getEncrypter());
  messageCIF->pkcsAdd(sessionCrypter->getBlockSize());
  DataBufferManaged::APtr messageEncrypted(DataBufferManaged::create(sessionCrypter->getCipherBufferSize(messageCIF->getSize())));
  sessionCrypter->encrypt(messageCIF->data(), messageCIF->getSize(), messageEncrypted->data());
  return messageEncrypted;
}




/*
  ------------------------------------------------------------------ 
  KeyExchange_CIFExchange_Server implementation
  ------------------------------------------------------------------ 
*/
KeyExchange_CIFExchange_Server::KeyExchange_CIFExchange_Server(void)
  : KeyExchange_CIFExchange() {
}

KeyExchange_CIFExchange_Server::~KeyExchange_CIFExchange_Server(void) {
  burn();
}

void KeyExchange_CIFExchange_Server::burn(void) {
  /* nothing to burn */
}

KeyExchange_CIFExchange_Server::APtr KeyExchange_CIFExchange_Server::createFromMessage(const KeyExchange_SessionKeyExchange::APtr& ske,
                                                                                       const ClientIdentification::APtr& clientIdentification,
                                                                                       const Crypter::APtr& crypter,
                                                                                       const Utility::DataBuffer::APtr& message) {
  if(message->getSize() == 0) {
    throw Exception_CF_KE_CIFExchange("Invalid cif-message length");
  }
  
  try {
      /* Decrypt and unpad buffer using the session encryption */
    Crypt::APtr sessionCrypter(ske->getSessionCrypter()->getDecrypter());
    long messageDecryptedSize = sessionCrypter->getPlainBufferSizeAfter(message->getSize());

    DataBufferManaged::APtr messageDecrypted(DataBufferManaged::create(messageDecryptedSize));
    sessionCrypter->decrypt(message->data(), message->getSize(), messageDecrypted->data());
    messageDecrypted->pkcsRemove(sessionCrypter->getBlockSize());


    long pos = 0;
    DataBufferManaged::APtr cifDatagramBuffer;
    pos = messageDecrypted->parseWithLen(cifDatagramBuffer, 2);
    if(cifDatagramBuffer.get() == NULL || cifDatagramBuffer->getSize() == 0) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing cif-datagram");
    }
  
    DataBufferManaged::APtr cifDatagramSignitureBuffer;
    messageDecrypted->parse(cifDatagramSignitureBuffer, pos);
    if(cifDatagramSignitureBuffer.get() == NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing signiture of cif-datagram");
    }

    /* Verify signiture */
    if(!ske->verifySignedBuffer(cifDatagramBuffer, cifDatagramSignitureBuffer)) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, signiture of cif-datagram is not valid");
    }


    /* Parse cif-datagram */
    pos = 0;
    DataBufferManaged::APtr remotePublicKeyBuffer;
    pos = cifDatagramBuffer->parse(",", remotePublicKeyBuffer);
    if(remotePublicKeyBuffer.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing remote public key");
    }
    remotePublicKeyBuffer = remotePublicKeyBuffer->decodeHex();

    DataBufferManaged::APtr clientCipherIdsBuffer; // Ignored
    pos = cifDatagramBuffer->parse(",", clientCipherIdsBuffer, pos);
    if(clientCipherIdsBuffer.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing client cipher ids");
    }

    DataBufferManaged::APtr clientHashIdsBuffer; //Ignored
    pos = cifDatagramBuffer->parse(",", clientHashIdsBuffer, pos);
    if(clientHashIdsBuffer.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing client hash ids");
    }
        
    DataBufferManaged::APtr upstreamKey;
    pos = cifDatagramBuffer->parse(",", upstreamKey, pos);
    if(upstreamKey.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing upstream key");
    }
    upstreamKey = upstreamKey->decodeHex();

    DataBufferManaged::APtr upstreamIV;
    pos = cifDatagramBuffer->parse(",", upstreamIV, pos);
    if(upstreamIV.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing upstream IV");
    }
    upstreamIV = upstreamIV->decodeHex();

    DataBufferManaged::APtr clientIdentificationBuffer;
    cifDatagramBuffer->parse(clientIdentificationBuffer, pos);
    if(clientIdentificationBuffer.get()==NULL) {
      throw Exception_CF_KE_CIFExchange("Invalid cif-message, missing client information");
    }
    clientIdentificationBuffer = clientIdentificationBuffer->decodeHex();
    clientIdentification->init(clientIdentificationBuffer);
    
    /* Create instance */
    KeyExchange_CIFExchange_Server::APtr cif(new KeyExchange_CIFExchange_Server());

    /* Initialise decrypter */
    crypter->initDecrypter(ske->getCryptFactory()->getCryptBlockAES256(), upstreamKey, upstreamIV);

    /* Set remote public key */
    ske->setRemotePublicSessionKey(remotePublicKeyBuffer);

    return cif;  
  }
  catch(DataBuffer::Exception_Seperator_not_found&) {
    throw Exception_CF_KE_CIFExchange("Invalid cif-message");
  }
  catch(DataBuffer::Exception_Out_of_bound&) {
    throw Exception_CF_KE_CIFExchange("Invalid cif-message");
  }
  catch(DataBuffer::Exception_decoding&) {
    throw Exception_CF_KE_CIFExchange("Invalid cif-message");
  }
}
