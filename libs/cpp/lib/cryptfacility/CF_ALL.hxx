/*! \file CF_ALL.hxx
    \brief Include all header files
    */
#ifndef CF_ALL_HXX
#define CF_ALL_HXX

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
# include <lib/cryptfacility/CryptFacilityDLL.hxx>
#else
# include <lib/utility/UY_DataBuffer.hxx>
# include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
# include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>
# include <lib/cryptfacility/CF_CryptFacilityService.hxx>
# include <lib/cryptfacility/CF_KE_SessionKeyExchange.hxx>
# include <lib/cryptfacility/CF_KE_CIFExchange.hxx>
# include <lib/cryptfacility/CF_KE_CryptExchange.hxx>
# include <lib/cryptfacility/CF_KE_CryptStreamValidator.hxx>
# include <lib/cryptfacility/CF_KeyStore.hxx>
#endif

#endif
