/*! \file CF_Hash.hxx
    \brief This file contains the abstrac class representing a hash algorithm
*/
#ifndef CF_HASH_HXX
#define CF_HASH_HXX

#include <string>
#include <vector>
#include <istream>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Config.hxx>


namespace Giritech {
namespace CryptFacility {


  /*! \brief This class abstract class represent a hash algorithm
  */
  class CRYPTFACILITY_DLL Hash : boost::noncopyable {
  public:
    typedef boost::shared_ptr<Hash> APtr;

    /*! \brief Destructor

      On destruction all internal state variables are burned, \see burn
    */
    ~Hash(void);

    /*! \brief Return the name of the hash algorithm
    */
    virtual std::string getName(void) const = 0;

    /*! \brief Returns the size in byte of the digest
    */
    virtual long digestSize(void) const = 0;


    /*! \brief Calculate the digest
    */
    virtual Utility::DataBufferManaged::APtr calculateDigest(const Utility::DataBuffer::APtr& input) = 0;

    /*! \brief Calculate the digest for the given file
    */
    virtual Utility::DataBufferManaged::APtr calculateDigestForFile(const std::string& filename_abs) = 0;

    /*! \brief Verify the digest for the file
    */
    virtual bool verifyDigest(const Utility::DataBuffer::APtr& digest, const Utility::DataBuffer::APtr& input) = 0;


  protected:
    /*! \brief Constructor
      \param _keyLen Keylen to be used by this instance. The keylen is in bits.
    */
    Hash(void);

  private:
    /*! \brief Burn all internal buffers.
    */
    void burn(void);
  };

}
}

#endif
