/*! \file CF_KE_CryptExchange.hxx
    \brief This file contains classes for handling the crypt-exchange stats of the key exchange mecanisme
*/
#ifndef CF_KE_CRYPTEXCHANGE_HXX
#define CF_KE_CRYPTEXCHANGE_HXX

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/utility.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_Crypter.hxx>

#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_KE_CIFExchange.hxx>


namespace Giritech {
namespace CryptFacility {


  /*! \brief Abstract class representing and handling the crypt-exchange state of the key exchange.

  The crypt-exchange state exchange the SKP(Server-Key-Package) datagram. 
  The datagram type 0(normal flow of key exchange) contains the cipher to use for the upstream and downstream, 
  and the symmetric key for the downstream.

  The state also generate and holds a hash(SHA1) of the SKP datagram which are to be used when testing the upstream and downstream ciphers.

  This is what happends:
  -# The server generate symmetric SessionDownstreamKey(iv/key) for the downstream.
  -# The server selects the SessionDownstreamCipher and SessionUpstreamCipher to use for the up and down stream based on the client's facility information.
  -# The send a SKP message(always type 0) containing the SessionDownstreamKey(iv/key) and the selected ciphers.
     The message is encrypted with the TemporaryClientKey.public.
  -# The server calculates and store a hash of the send SKP message, known as the receipt.
  -# The client decrypts the SKP message using the TemporaryClientKey.private.

  During special flow of key exchange the type N SKP datagram is used. 
  This datagram do not exchange key information and are to be use to escape key exchange and tell the client to do somthing special.
  \dot
  digraph graph_session_key_exchange {
    rankdir=TB;
    ranksep=0.5; 
    center=true;
    edge [minlen=1.5, fontsize=8, fontname="Helvetica" ];
    node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];

    CRYPTER_Server               [shape=octagon, style=filled, fillcolor=3, label="Crypter"];
    SKE_Server_SKE               [shape=octagon, style=filled, fillcolor=3, label="KeyExchange_SessionKeyExchange"];
    CE_Server_create             [shape=box,     style=filled, fillcolor=3, label="create"];
    CE_Server_generateMessage    [shape=box,     style=filled, fillcolor=3, label="generateMessage"];
    CE_Server                    [shape=ellipse, style=filled, fillcolor=3, label="SessionDownstreamCipher\nSessionUpstreamCipher\nreceipt"];
    CE_Message                   [shape=ellipse, style=filled, fillcolor=1, label="SKP Datagram" ];
    CE_Client                    [shape=ellipse, style=filled, fillcolor=2, label="SessionDownstreamCipher\nSessionUpstreamCipher\nreceipt"];
    CE_Client_createFromMessage  [shape=box,     style=filled, fillcolor=2, label="createFromMessage"];
    SKE_Client_SKE               [shape=octagon, style=filled, fillcolor=2, label="KeyExchange_SessionKeyExchange"];
    CRYPTER_Client               [shape=octagon, style=filled, fillcolor=2, label="Crypter"];

    CE_Server_generateMessage   -> CRYPTER_Server
    CE_Server_create            -> CE_Server;
    CE_Server                   -> CE_Server_generateMessage;
    CE_Server_generateMessage   -> CE_Server;
    SKE_Server_SKE              -> CE_Server_generateMessage;
    CE_Server_generateMessage   -> CE_Message;
    CE_Message                  -> CE_Client_createFromMessage;
    SKE_Client_SKE              -> CE_Client_createFromMessage;
    CE_Client_createFromMessage -> CRYPTER_Client
    CE_Client_createFromMessage -> CE_Client;
  }
  \enddot


  The SKP Datagram type 0 generated as part of the normal flow of key exchang looks like this:
  \image html drawing_protocol_skp.png "SKP Payload"
  \image latex drawing_protocol_skp.png "SKP Payload"

  The SKP Datagram type n generated as in case of alternative flow looks like this:
  \dot
    digraph graph_skp {
      rankdir=TB;
      ranksep=0.5; 
      center=true;
      edge [fontsize=8, fontname="Helvetica" ];
      node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
      skp [
        shape=record, stype=filled, fillcolor=1, 
        label="skptype\nN|comma|databuffer\n(hex encoded)"
      ]
  }
  \enddot

  */
  class CRYPTFACILITY_DLL KeyExchange_CryptExchange : boost::noncopyable {
  public:
    typedef boost::shared_ptr<KeyExchange_CryptExchange> APtr;
 

    /*! \brief This exception is used as base for alle exceptions thrown during crypt-exchange state
     */
    class Exception_CF_KE_CryptExchange : public Exception_CF {
    public:
      Exception_CF_KE_CryptExchange(const std::string& message) 
        : Exception_CF(message) {
      }
      virtual ~Exception_CF_KE_CryptExchange(void) throw () {
      }
    };


    /*! \brief Destructor
    */
    virtual ~KeyExchange_CryptExchange(void) {
      burn();
    }


    /* \brief Calculate and set the receipt, which is a SHA1 of the server-key message sendt
    */
    void calculateReceipt(const Utility::DataBuffer::APtr& serverKeyMessage);


    /* \brief Returns the reciept
    */
    Utility::DataBuffer::APtr getReceipt(void) const;

    
    /* \brief Create a stream crypter using same keys as the given crypter
    */
    Crypter::APtr create_crypter_stream(const Crypter::APtr& crypter, const CryptFactory::APtr& crypt_factory);
    
    
  protected:
    /*! \brief Constructor
    */
    KeyExchange_CryptExchange(void);

  private:
    /* \brief Burns intenal values before destruction
     */
    void burn(void);


  protected:
    Utility::DataBufferManaged::APtr receipt_;
    static const char* skpType_0;
  };



  /*! \brief Represent and handles the client side of the crypt-exchange state of the key exchange.
  */
  class CRYPTFACILITY_DLL KeyExchange_CryptExchange_Client : public virtual KeyExchange_CryptExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_CryptExchange_Client> APtr;
 

    /*! \brief This exception is used as base for alle exceptions thrown during crypt-exchange state
     */
    class Exception_CF_KE_CryptExchange_Special_SPK : public Exception_CF_KE_CryptExchange {
    public:
      Exception_CF_KE_CryptExchange_Special_SPK(const unsigned int& skpType, const Utility::DataBufferManaged::APtr& additionalDataBuffer) 
        : Exception_CF_KE_CryptExchange("Special SPK recived"), 
          skpType_(skpType), 
          additionalDataBuffer_(additionalDataBuffer) {
      }
      virtual ~Exception_CF_KE_CryptExchange_Special_SPK(void) throw() {
      }

      unsigned int getSKPType(void) {
        return skpType_;
      }
      Utility::DataBufferManaged::APtr getAdditionalData(void) {
        return additionalDataBuffer_;
      }
    private:
      unsigned int skpType_;
      Utility::DataBufferManaged::APtr additionalDataBuffer_;
    };


    /*! \brief Destructor
    */
    virtual ~KeyExchange_CryptExchange_Client(void) {
      burn();
    }


    /*! \brief Create an client instance from server-key message recived from server
    */
    static APtr createFromMessage(const KeyExchange_SessionKeyExchange& ske,
                                  Crypter& crypter,
                                  const CryptFactory& cryptFactory,
                                  const Utility::DataBuffer& serverMessage);

  private:
    /*! \brief Constructor
     */
    KeyExchange_CryptExchange_Client(void);

    /*! \brief Burns intenal values before destruction
    */
    void burn(void);
  };



  /*! \brief Represent and handles the server side of the crypt-exchange state of the key exchange.
  */
  class CRYPTFACILITY_DLL KeyExchange_CryptExchange_Server : public virtual KeyExchange_CryptExchange {
  public:
    typedef boost::shared_ptr<KeyExchange_CryptExchange_Server> APtr;


    /*! \brief Destructor
    */
    virtual ~KeyExchange_CryptExchange_Server(void) {
      burn();
    }
 

    /*! \brief Generate the crypt-key message for the client
    
    The server-key message contains downstream/upstream cipher identification to use for the communication, 
    and the key/iv to use for the downstream. 
    The message is encrypted using the key-exchange cipher, and the remote session-public-key recived during the session-key-exchange state. 
    Calculate the reciept(a SHA1 hash) of the server-key message.

    This message has skp type 0.
    */
    Utility::DataBufferManaged::APtr generateMessage(const KeyExchange_SessionKeyExchange& ske, Crypter& crypter, const CryptFactory& cryptFactory);


    /*! \brief Generate a special skp-datagram indicating some kind of break of key exchange
    
    This kind of message is currently not used, by can be used to break the key exchange.
    The skpType must be greater than 1.
    */
    Utility::DataBufferManaged::APtr generateMessage(const KeyExchange_SessionKeyExchange& ske, 
                                                     const unsigned int& skpType,
                                                     const Utility::DataBuffer::APtr& skpAdditionalData);


    /*! \brief Create a server instance
    Initialize the downstream cipher.   
    */
    static APtr create(const KeyExchange_CIFExchange& cif);


  private:
    /*! \brief Constructor
    */
    KeyExchange_CryptExchange_Server(void);


    /*! \brief Burns intenal values before destruction
    */
    void burn(void);
  };
 

}
}

#endif
