/*! \file CF_KeyStore.hxx
    \brief This file contains classes for handling key generation and key storage
*/
#ifndef CF_KEYSTORE_HXX
#define CF_KEYSTORE_HXX

#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
#  include <cryptopp/dll.h>
#  include <cryptopp/fips140.h>
#else
#  include <cryptopp/modes.h>
#  include <cryptopp/aes.h>
#  include <cryptopp/eccrypto.h>
#endif

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypt.hxx>
#include <lib/cryptfacility/CF_Crypter.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>


namespace Giritech {
namespace CryptFacility {

  class CRYPTFACILITY_DLL KeyStore : boost::noncopyable {
  public:
    typedef boost::shared_ptr<KeyStore> APtr;

    virtual ~KeyStore(void);

    /*! \brief Geters
    */
    virtual Utility::DataBufferManaged::APtr getSessionKeyPublic(void);
    virtual Utility::DataBufferManaged::APtr getSessionKeyPrivate(void);

    /*! \brief Get key and IV for the encrypter/decrypter
     */
    virtual void getEncrypterKey(Utility::DataBufferManaged::APtr& key, Utility::DataBufferManaged::APtr& iv);
    virtual void getDecrypterKey(Utility::DataBufferManaged::APtr& key, Utility::DataBufferManaged::APtr& iv);

    void setSessionKeypair(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate);

    /*
     * Reset session key pair
     */
    virtual void resetSessionKeypair(void);


    static APtr create(boost::asio::io_service& io, const CryptFactory::APtr& cryptFactory);

  protected:
    /* \brief Burns internal values before destruction
     */
    void burn(void);

    KeyStore(boost::asio::io_service& io, const CryptFactory::APtr& cryptFactory);


    /* \brief Generate session keypair
     *
    Generate a private/public session-key pair using Crypto++ Public Key Cryptosystems RSAES<OAEP<SHA1> >.
    */
    void generateSessionKeypair(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate);

    /*! \brief Generate key and IV for the encrypter/decrypter
     */
    void generateEncrypterKey(void);
    void generateDecrypterKey(void);


    CryptFactory::APtr cryptFactory_;

    Utility::DataBufferManaged::APtr sessionKeyPublic_;
    Utility::DataBufferManaged::APtr sessionKeyPrivate_;

    Utility::DataBufferManaged::APtr encrypterKey_;
    Utility::DataBufferManaged::APtr encrypterIV_;

    Utility::DataBufferManaged::APtr decryptionKey_;
    Utility::DataBufferManaged::APtr decryptionIV_;

    static const long sessionKeySize = 2048;
    static const long symetricKeySize = 255;

    Utility::Mutex::APtr mutex_;

  private:
    bool sessionKeyPairGenerated_;

};

}
}

#endif
