/*! \file CF_Crypter.hxx
 \brief This file contains classes for handling the encryption
 */
#ifndef CF_CRYPTER_HXX
#define CF_CRYPTER_HXX

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_Config.hxx>
#include <lib/cryptfacility/CF_Crypt.hxx>
#include <lib/cryptfacility/CF_Hash.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>

namespace Giritech {
namespace CryptFacility {

/*! \brief Handles encryption and decryption .

 */
class CRYPTFACILITY_DLL Crypter : boost::noncopyable {
public:
    typedef boost::shared_ptr<Crypter> APtr;

    /*! \brief Destructor
     */
    virtual ~Crypter(void) {
        burn();
    }

    /*! \brief Initialize the hash
     */
    void initHash(const Hash::APtr& hash);

    /*! \brief Initialize the encrypter
     */
    void initEncrypter(const Crypt::APtr& encrypter);
    void initEncrypter(const Crypt::APtr& encrypter, const Utility::DataBuffer::APtr& key);
    void initEncrypter(const Crypt::APtr& encrypter,
                       const Utility::DataBuffer::APtr& key,
                       const Utility::DataBuffer::APtr& iv);

    /*! \brief Initialize the decrypter
     */
    void initDecrypter(const Crypt::APtr& decrypter);
    void initDecrypter(const Crypt::APtr& decrypter, const Utility::DataBuffer::APtr& key);
    void initDecrypter(const Crypt::APtr& decrypter,
                       const Utility::DataBuffer::APtr& key,
                       const Utility::DataBuffer::APtr& iv);

    /*! \brief Set key and IV for the encrypter
     */
    void setEncrypterKey(const Utility::DataBuffer::APtr& key, const Utility::DataBuffer::APtr& iv);
    void setEncrypterKey(const Utility::DataBuffer::APtr& key);

    /*! \brief Set key and IV for the decrypter
     */
    void setDecrypterKey(const Utility::DataBuffer::APtr& key, const Utility::DataBuffer::APtr& iv);
    void setDecrypterKey(const Utility::DataBuffer::APtr& key);

    /*! \brief Perform Known answer test of generated key/iv
     
     Use the encrypter for encryption and the decryptor for decryptrion.
     */
    void knownAnswerTestDecrypter(void);
    void knownAnswerTestEncrypter(void);

    /*! \brief Returns the encrypter
     */
    Crypt::APtr getEncrypter(void) const;

    /*! \brief Returns the decrypter
     */
    Crypt::APtr getDecrypter(void) const;

    /*! \brief Returns the hash
     */
    Hash::APtr getHash(void) const;

    /*! \brief Create instance of Crypter

     The Crypt Factory are use for random number generation
     */
    static APtr create(const CryptFactory::APtr& cryptFactory);

    /*! \brief Copy keys from another crypter
     */
    void set_keys_from(const APtr& crypter);

private:
    /*! \brief Constructor
     */
    Crypter(const CryptFactory::APtr& cryptFactory);

    /* \brief Burns intenal values before destruction
     */
    virtual void burn(void);

private:
    /*! \brief Size of generated key/iv for symmetric cipher */
    static const long KeySize_ = 255;

    CryptFactory::APtr cryptFactory_;

    Hash::APtr hash_;

    Crypt::APtr encrypter_;
    Crypt::APtr decrypter_;

    Utility::DataBuffer::APtr encrypterKey_;
    Utility::DataBuffer::APtr encrypterIV_;
    Utility::DataBuffer::APtr decrypterKey_;
    Utility::DataBuffer::APtr decrypterIV_;

};

}
}

#endif
