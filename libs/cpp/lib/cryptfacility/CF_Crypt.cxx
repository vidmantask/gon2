/*! \file CF_Crypt.cxx
    \brief This file contains the implementation of the Crypt class.
*/
#include <sstream>

//#include "base64.h"
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_Crypt.hxx>


using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace std;

/*
  ------------------------------------------------------------------
  Crypt implementation
  ------------------------------------------------------------------
*/
void Crypt::burn(void) {
  keyLen_ = 0;
  state_  = state_burned;
}

void Crypt::setInitialized(void) {
  state_ = state_keyInitialized;
}

void Crypt::setBurned(void) {
  state_ = state_burned;
}

int Crypt::getKeyLen(void) const {
    return keyLen_;
}

void Crypt::setKeyWithIV(const byte_t* key, const byte_t* iv) {
  switch(state_) {
  case state_burned:
    throw Exception_CF_Invalid_State("Key can not be set because the crypt instance has been burned.");
  default:
    setInitialized();
  }
}

void Crypt::readyToEncrypt(const int bufferLen) {
  switch(state_) {
  case state_keyInitialized:
    return;
  case state_burned:
    throw Exception_CF_Invalid_State("Encryption not available because the instance has been burned.");
  default:
    throw Exception_CF_Invalid_State("Encryption not available because key has not been initialized.");
  }
}

void Crypt::readyToDecrypt(const int bufferLen) {
  switch(state_) {
  case state_keyInitialized:
    return;
  case state_burned:
    throw Exception_CF_Invalid_State("Decryption not available because the instance has been burned.");
  default:
      throw Exception_CF_Invalid_State("Encryption not available because key has not been initialized.");
  }
}

bool Crypt::knownAnswerTest_Symetric( const Utility::DataBuffer::APtr& key,
                                      const Utility::DataBuffer::APtr& iv) {
  setKeyWithIV(key->data(), iv->data());
  return consistencyTest();
}

bool Crypt::pairwiseConsistencyTest_Asymetric( const Utility::DataBuffer::APtr& pwcPublicKey,
                                               const Utility::DataBuffer::APtr& pwcPrivateKey) {
  setEncryptionKey(pwcPublicKey);
  setDecryptionKey(pwcPrivateKey);

  return consistencyTest();
}

bool Crypt::consistencyTest(void) {
  DataBufferManaged::APtr knownBuffer(DataBufferManaged::create("initilized"));
  knownBuffer->pkcsAdd(getBlockSize());

  long knownBufferEncryptedSize = getCipherBufferSize(knownBuffer->getSize());
  DataBufferManaged::APtr knownBufferEncrypted(DataBufferManaged::create(knownBufferEncryptedSize));
  encrypt(knownBuffer->data(), knownBuffer->getSize(), knownBufferEncrypted->data());

  long knownBufferDecryptedSize = getPlainBufferSizeAfter(knownBufferEncrypted->getSize());
  DataBufferManaged::APtr knownBufferDecrypted(DataBufferManaged::create(knownBufferDecryptedSize));
  decrypt(knownBufferEncrypted->data(), knownBufferEncrypted->getSize(), knownBufferDecrypted->data());
  knownBufferDecrypted->pkcsRemove(getBlockSize());

  return !( ((*knownBuffer) == (*knownBufferDecrypted)) && ((*knownBuffer) != (*knownBufferEncrypted)) );

}
