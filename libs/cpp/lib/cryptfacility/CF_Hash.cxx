/*! \file CF_Hash.cxx
    \brief This file contains the implementation of the Hash class.
*/
#include <lib/cryptfacility/CF_Hash.hxx>

#include <sstream>

using namespace Giritech::CryptFacility;
using namespace std;

/*
  ------------------------------------------------------------------ 
  Hash implementation
  ------------------------------------------------------------------ 
*/
Hash::Hash(void) {
}

Hash::~Hash(void) {
  burn();
}

void Hash::burn(void) {
  /* nothing to burn */
}

