/*! \file CF_Crypt_CryptoPP.cxx
    \brief This file contains the implementation of then interface classes to en-/decryption algorithm in the Crypto++ library.
*/
#include <sstream>
#include <iostream>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/utility/UY_Types.hxx>

using namespace Giritech;
using namespace Giritech::CryptFacility;
using namespace std;

using namespace CryptoPP;


/*
  ------------------------------------------------------------------
  Crypt_CryptoPP_PKAES implementation
  ------------------------------------------------------------------
*/
Crypt_CryptoPP_PKAES::Crypt_CryptoPP_PKAES(const int keyLen)
  : Crypt(keyLen) {
}

Crypt_CryptoPP_PKAES::APtr Crypt_CryptoPP_PKAES::create(const int keyLen) {
  PKCryptosystem::PublicKey tempKey;
  try {
    Crypt_CryptoPP_PKAES::APtr crypt(new Crypt_CryptoPP_PKAES(keyLen));
    return crypt;
  }
  catch(const Exception& e) {
    throw Exception_CF_CryptoPP(e.what());
   }
}

Crypt::APtr Crypt_CryptoPP_PKAES::clone(void) const {
  return Crypt_CryptoPP_PKAES::APtr(new Crypt_CryptoPP_PKAES(*this));
}

void Crypt_CryptoPP_PKAES::setKeyWithIV(const Utility::byte_t* key, const Utility::byte_t* iv) {
  PKCryptosystem::PrivateKey privateKey(loadKey<PKCryptosystem::PrivateKey>(key, getKeyLen()));
  PKCryptosystem::PublicKey  publicKey(loadKey<PKCryptosystem::PublicKey>(key, getKeyLen()));

  decryptor_.AccessPrivateKey().AssignFrom(privateKey);
  encryptor_.AccessPublicKey().AssignFrom(publicKey);
  setInitialized();
}
void Crypt_CryptoPP_PKAES::setEncryptionKey(const Giritech::Utility::DataBuffer::APtr& key) {
  PKCryptosystem::PublicKey  publicKey(loadKey<PKCryptosystem::PublicKey>(key));
  encryptor_.AccessPublicKey().AssignFrom(publicKey);
  setInitialized();
}
void Crypt_CryptoPP_PKAES::setDecryptionKey(const Giritech::Utility::DataBuffer::APtr& key) {
  PKCryptosystem::PrivateKey privateKey(loadKey<PKCryptosystem::PrivateKey>(key));
  decryptor_.AccessPrivateKey().AssignFrom(privateKey);
  setInitialized();
}

std::string Crypt_CryptoPP_PKAES::getName(void) const {
  return encryptor_.AlgorithmName();
}

long Crypt_CryptoPP_PKAES::getPlainBufferSize(const long plainBufferSize) const {
  return plainBufferSize + (plainBufferSize % getBlockSize());
}

long Crypt_CryptoPP_PKAES::getCipherBufferSize(const long plainBufferSize) const {
  long ptBlockSize = getBlockSize();
  long ctBlockSize = encryptor_.CiphertextLength(ptBlockSize);
  long factor      = plainBufferSize / ptBlockSize;
  return factor * ctBlockSize;
}
long Crypt_CryptoPP_PKAES::getPlainBufferSizeAfter(const long cipherBufferSize) const {
  long ptBlockSize = getBlockSize();
  long ctBlockSize = decryptor_.CiphertextLength(ptBlockSize);
  long factor      = cipherBufferSize / ctBlockSize;
  return factor * ptBlockSize;
}

long Crypt_CryptoPP_PKAES::getBlockSize(void) const {
  long blockSize = max(encryptor_.FixedMaxPlaintextLength(), decryptor_.FixedMaxPlaintextLength());
  return blockSize;
}

void Crypt_CryptoPP_PKAES::checkBufferLen(const int bufferLen) const {
  if( (bufferLen % getBlockSize()) != 0) {
    stringstream ss;
    ss << "Invalid size of buffer. Buffer size is "
       << bufferLen
       << ", but the size is incompatible with current used block size "
       << getBlockSize() << ".";
    throw Exception_CF_CryptoPP(ss.str());
  }
}

void Crypt_CryptoPP_PKAES::encrypt(const Utility::byte_t* plainBuffer, const int bufferLen, Utility::byte_t* cipherBuffer) {
  readyToEncrypt(bufferLen);
  checkBufferLen(bufferLen);
  try {
    RandomNumberGenerator rng;
    long ptBlockSize = encryptor_.FixedMaxPlaintextLength();
    long ctBlockSize = encryptor_.CiphertextLength(ptBlockSize);
    long blocks = bufferLen / ptBlockSize;
    for(long i=0; i<blocks; ++i) {
      encryptor_.Encrypt(rng, &(plainBuffer[ptBlockSize * i]), ptBlockSize, &(cipherBuffer[ctBlockSize * i]));
    }

  }
  catch(const Exception& e) {
    throw Exception_CF_CryptoPP(e.what());
  }
}

void Crypt_CryptoPP_PKAES::decrypt(const Utility::byte_t* cipherBuffer, const int bufferLen, Utility::byte_t* plainBuffer) {
  readyToDecrypt(bufferLen);
  try {
    RandomNumberGenerator rng;
    long ptBlockSize = decryptor_.FixedMaxPlaintextLength();
    long ctBlockSize = decryptor_.CiphertextLength(ptBlockSize);
    long blocks = bufferLen / ctBlockSize;
    for(long i=0; i<blocks; i++) {
      decryptor_.Decrypt(rng, &(cipherBuffer[ctBlockSize * i]), ctBlockSize, &(plainBuffer[ptBlockSize * i]));
    }
  }
  catch(const Exception& e) {
    throw Exception_CF_CryptoPP(e.what());
  }
}

void Crypt_CryptoPP_PKAES::burn(void) {
  /* Nothing to burn */
}
