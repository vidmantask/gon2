/*! \file CF_Crypt_UTest.cxx
    \brief This file contains unittest suite for the Crypt class
*/

#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
# include <lib/cryptfacility/CryptFacilityDLL.hxx>
#else
# include "CryptFacility/CF_Crypt.hxx"
# include "CryptFacility/CF_CryptFactory.hxx"
#endif

#include <lib/utility/UY_Types.hxx>
#include "UnitTest/UT_UnitTest.hxx"

using namespace std;
using namespace Giritech::CryptFacility;
using namespace Giritech::Utility;


namespace {


/*! \brief Testclass needed by testsute implementing a Crypt algoritme
*/
class CryptTest : public Crypt {
public:
  typedef boost::shared_ptr<Crypt> APtr;

  static CryptTest::APtr create(const int keyLen, const string& gid) {
    return CryptTest::APtr(new CryptTest(keyLen, gid));
  }

  CryptTest(const int keyLen, const string& gid) 
    : Crypt(keyLen, gid) {
  }


  Crypt::APtr clone(void) const {
    return APtr(new CryptTest(*this));
  }

  void setKey(const Giritech::Utility::DataBuffer::APtr& buffer) {
  }

  void encrypt(const byte_t* plainBuffer,  const int bufferLen, byte_t* cipherBuffer) {
    readyToEncrypt(bufferLen);
    for(int i=0; i< bufferLen; ++i) {
      cipherBuffer[i] = plainBuffer[i] + 1;
    }
  }
  void decrypt(const byte_t* cipherBuffer, const int bufferLen, byte_t* plainBuffer) {
    readyToDecrypt(bufferLen);
    for(int i=0; i< bufferLen; ++i) {
      plainBuffer[i] = cipherBuffer[i] - 1;
    }
  }

  long getPlainBufferSize(const long plainBufferSize) const {
    return plainBufferSize;
  }

  long getCipherBufferSize(const long plainBufferSize) const {
    return plainBufferSize;
  }

  long getPlainBufferSizeAfter(const long cipherBufferSize) const {
     return cipherBufferSize;
  }

  long getBlockSize(void) const {
     return 32;
  }

  std::string getName(void) const {
    return "CryptTest";
  }

};



/*! /breif Unittest for Crypt
*/
class UnitTest_Crypt : public Test::Suite {
public:
  UnitTest_Crypt(void) { 
    TEST_ADD(UnitTest_Crypt::test_simple_get_set); 
    TEST_ADD(UnitTest_Crypt::test_simple_encrypt_decrypt); 
    TEST_ADD(UnitTest_Crypt::test_exception_key_unitialized);
    TEST_ADD(UnitTest_Crypt::test_creator); 
  }

private:

    void test_simple_get_set(void) {
      CryptTest::APtr cryptTest(new CryptTest(128, "ct"));
      TEST_ASSERT( cryptTest->getKeyLen() == 128 );      
    }

    void test_simple_encrypt_decrypt(void) {
      CryptTest::APtr cryptTest(new CryptTest(3, "ct"));
      byte_t key[] = "abc";
      byte_t iv[]  = "123";
      byte_t plainText1[] = "12345678901234567890";
      byte_t plainText2[] = "                    ";
      byte_t ciphetText[] = "                    ";
      int  bufferLen = 10;

      cryptTest->setKeyWithIV(key,iv);
      cryptTest->encrypt(plainText1, bufferLen, ciphetText);
      cryptTest->decrypt(ciphetText, bufferLen, plainText2);

      TEST_ASSERT( memcmp(plainText1, ciphetText, bufferLen) != 0  );      
      TEST_ASSERT( memcmp(plainText1, plainText2, bufferLen) == 0  );      
    }

    void test_exception_key_unitialized(void) {
      CryptTest::APtr cryptTest(new CryptTest(3, "ct"));
      byte_t plainText1[] = "12345678901234567890";
      byte_t plainText2[] = "                    ";
      byte_t ciphetText[] = "                    ";
      int  bufferLen = 10;

      TEST_THROWS(cryptTest->encrypt(plainText1, bufferLen, ciphetText), Exception_CF_Invalid_State);
      TEST_THROWS(cryptTest->decrypt(ciphetText, bufferLen, plainText2), Exception_CF_Invalid_State);
    }


    void test_creator(void) {
      static const string name("CryptTest");
      static const string gid("42");
      CryptCreator<CryptTest> creatorCryptTest_128(name, gid, 128);
      CryptCreator<CryptTest> creatorCryptTest_255(name, gid, 255);
      Crypt::APtr cryptTest_128(creatorCryptTest_128());
      Crypt::APtr cryptTest_255(creatorCryptTest_255());

      TEST_ASSERT( creatorCryptTest_128.getAlgoName() == "CryptTest");      
      TEST_ASSERT( creatorCryptTest_128.getAlgoGId()  == "42");
      TEST_ASSERT( creatorCryptTest_128.getGId()      == "42-128");
      
      TEST_ASSERT( creatorCryptTest_255.getAlgoName() == "CryptTest");      
      TEST_ASSERT( creatorCryptTest_255.getAlgoGId()  == "42");
      TEST_ASSERT( creatorCryptTest_255.getGId()      == "42-255");
      
      TEST_ASSERT( cryptTest_128->getKeyLen() == 128);      
      TEST_ASSERT( cryptTest_255->getKeyLen() == 255);
    }

  };
}


int main(int argc, char **argv ) {
  Giritech::UnitTest::UnitTest uTest(argc, argv, "CryptFacility, Crypt" );

  uTest.add(auto_ptr<Test::Suite>(new UnitTest_Crypt));
  return uTest.run() ? 0 : 1;
}




