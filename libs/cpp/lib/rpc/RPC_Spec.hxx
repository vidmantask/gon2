/*! \file RPC_Spec.hxx
 \brief This file contains Remote Procedure Call sepecification lib
 */
#ifndef RPC_Spec_HXX
#define RPC_Spec_HXX

#include <string>
#include <list>
#include <map>
#include <boost/shared_ptr.hpp>

#include <tinyxml2.h>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

namespace Giritech {
namespace RPC {


/*! \brief Exception used for errors when handling RPC
 */
class Exception_RPC: public Giritech::Utility::Exception {
public:
	Exception_RPC(const std::string& message) :
        Exception(message) {
    }
};


/*! \brief This class represent RPC value
 */
class RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValue> APtr;

    ~RPCSpecValue();

    static APtr create_from_parent(tinyxml2::XMLElement* arg);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent) = 0;
};


class RPCSpecValueNone : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueNone> APtr;

    ~RPCSpecValueNone();

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueNone::APtr create(void);

private:
    RPCSpecValueNone(void);
};


class RPCSpecValueString : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueString> APtr;

    ~RPCSpecValueString();

    std::string get_value(void);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueString::APtr create(const std::string& value);
    static RPCSpecValueString::APtr create_from_content(const std::string& content);

private:
    RPCSpecValueString(const std::string& value);

    std::string value_;
};

class RPCSpecValueInteger : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueInteger> APtr;

    ~RPCSpecValueInteger();

    long get_value(void);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueInteger::APtr create(const long& value);
    static RPCSpecValueInteger::APtr create_from_content(const std::string& content);

private:
    RPCSpecValueInteger(const long& value);

    long value_;
};

class RPCSpecValueList : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueList> APtr;

    ~RPCSpecValueList();

    void push_back(RPCSpecValue::APtr key_value);


    std::vector<RPCSpecValue::APtr>& get_value(void);
    std::vector<std::string> get_as_list_of_strings(void);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueList::APtr create(void);
    static RPCSpecValueList::APtr create_from_strings(const std::vector<std::string>& strings);
    static RPCSpecValueList::APtr create_from_parent(tinyxml2::XMLElement* rpc_call);



private:
    RPCSpecValueList(void);

    std::vector<RPCSpecValue::APtr> value_;
};


class RPCSpecValueDict : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueDict> APtr;

    ~RPCSpecValueDict();

    void update(const std::string& key, RPCSpecValue::APtr key_value);

    RPCSpecValue::APtr get(const std::string& key);

    std::string get_as_string(const std::string& key);
    std::string get_as_string(const std::string& key, const std::string& default_value);
    long get_as_integer(const std::string& key);
    long get_as_integer(const std::string& key, const long default_value);
    std::vector<RPCSpecValue::APtr> get_as_list(const std::string& key);
    RPCSpecValueList::APtr get_as_valuelist(const std::string& key);
    RPCSpecValueDict::APtr get_as_dict(const std::string& key);

    std::map<std::string, RPCSpecValue::APtr>& get_value(void);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueDict::APtr create(void);
    static RPCSpecValueDict::APtr create_from_content(const std::string& content);
    static RPCSpecValueDict::APtr create_from_parent(tinyxml2::XMLElement* rpc_call);

private:
    RPCSpecValueDict(void);

    std::map<std::string, RPCSpecValue::APtr> value_;
};




class RPCSpecValueTuple : public RPCSpecValue {
public:
    typedef boost::shared_ptr<RPCSpecValueTuple> APtr;

    ~RPCSpecValueTuple();

    void push_back(RPCSpecValue::APtr key_value);


    std::vector<RPCSpecValue::APtr>& get_value(void);

	virtual tinyxml2::XMLElement* to_xml(tinyxml2::XMLElement* parent);

    static RPCSpecValueTuple::APtr create(void);
    static RPCSpecValueTuple::APtr create_from_parent(tinyxml2::XMLElement* rpc_call);

private:
    RPCSpecValueTuple(void);
    std::vector<RPCSpecValue::APtr> value_;
};




/*! \brief This class represent RPC Call
 */
class RPCSpecCall {
public:
    typedef boost::shared_ptr<RPCSpecCall> APtr;

    /*! \brief Destructor
     */
    ~RPCSpecCall();

    /*! \brief Getters
     */
    std::string get_function_name(void);
    RPCSpecValueDict::APtr get_args(void);

    std::string to_xml_doc_string(void);

    /*! \brief Create instance
     *
     * If an error is found the exception Exception_RPCS is thrown
     */
    static RPCSpecCall::APtr create_from_string(const std::string& xml_document);
    static RPCSpecCall::APtr create(const std::string function_name, RPCSpecValueDict::APtr& args);


private:
    RPCSpecCall(const std::string function_name, RPCSpecValueDict::APtr& args);

    std::string function_name_;
    RPCSpecValueDict::APtr args_;
};


/*
 * Converters
 */
RPCSpecValueDict::APtr value_to_dict(const RPCSpecValue::APtr& value);

}
}
#endif
