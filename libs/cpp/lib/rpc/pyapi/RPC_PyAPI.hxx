/*! \file RPC_PyAPI.hxx
 \brief This file contains Remote Procedure Call python API
 */
#ifndef RPC_PyAPI_HXX
#define RPC_PyAPI_HXX

#include <string>
#include <list>
#include <boost/python.hpp>

#include <lib/rpc/RPC_Spec.hxx>


namespace Giritech {
namespace RPC {

/*
 * Encode a python tuple (method_name<string>, args<dict>) into xml as a rpc-call
 */
std::string rpc_xml_encode(const std::string& function_name, const boost::python::dict& values);

/*
 * Decode a xml rpc-call into a python tuple (rc<bool>, error_message<string>, method_name<string>, args<dict>)
 */
boost::python::tuple rpc_xml_decode(const std::string& xml_doc);

}
}
#endif
