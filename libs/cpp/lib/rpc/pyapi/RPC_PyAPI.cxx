/*! \file PC_PyAPI.cxx
 \brief This file contains the implementaion of the Gpm class
 */
// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <sstream>
#include <string>


#include <component/communication/pyapi/PYAPI_Utility.hxx>
#include <lib/rpc/pyapi/RPC_PyAPI.hxx>
#include <lib/rpc/RPC_Spec.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::RPC;
using namespace boost::python;


boost::python::object convert_to_py(const RPCSpecValue::APtr& value);
RPCSpecValueList::APtr convert_from_py_list(const boost::python::list& value);
RPCSpecValueDict::APtr convert_from_py_dict(const boost::python::dict& value);



/*
 *
 */
RPCSpecValue::APtr convert_from_py(const boost::python::object& value) {
	if(value.ptr() == Py_None) {
		return RPCSpecValueNone::create();
	}
	extract<const char*> get_char_prt(value);
	if (get_char_prt.check()) {
		return RPCSpecValueString::create(get_char_prt());
	}
	extract<const long> get_long(value);
	if (get_long.check()) {
		return RPCSpecValueInteger::create(get_long());
	}
	extract<boost::python::dict> get_dict(value);
	if (get_dict.check()) {
		return convert_from_py_dict(get_dict());
	}
	extract<boost::python::list> get_list(value);
	if (get_list.check()) {
		return convert_from_py_list(get_list());
	}
	return RPCSpecValueString::create("ERROR: Missing from_py converter");
}

RPCSpecValueDict::APtr convert_from_py_dict(const boost::python::dict& value) {
	RPCSpecValueDict::APtr result_value(RPCSpecValueDict::create());
	boost::python::object objectKey;
	boost::python::object objectValue;
	const boost::python::object objectKeys(value.iterkeys());
	const boost::python::object objectValues(value.itervalues());
	unsigned long count = boost::python::extract<unsigned long>(value.attr("__len__")());
	for (unsigned long i = 0; i < count; i++) {
		objectKey = objectKeys.attr("next")();
		objectValue = objectValues.attr("next")();
		std::string key = boost::python::extract<const char*>(objectKey)();
		result_value->update(key, convert_from_py(objectValue));
	}
	return result_value;
}

RPCSpecValueList::APtr convert_from_py_list(const boost::python::list& value) {
	RPCSpecValueList::APtr result_value(RPCSpecValueList::create());

	boost::python::ssize_t n = boost::python::len(value);
	for(boost::python::ssize_t i=0;i<n;i++) {
		result_value->push_back(convert_from_py(value[i]));
	}
	return result_value;
}

std::string Giritech::RPC::rpc_xml_encode(const std::string& function_name, const boost::python::dict& args) {
    Giritech::Communication::PYAPI::ThreadLockNoLog python_thread_lock;
	RPCSpecValueDict::APtr converted_args(convert_from_py_dict(args));
	RPCSpecCall::APtr rpm_call(RPCSpecCall::create(function_name, converted_args));
	return rpm_call->to_xml_doc_string();
}

/*
 *
 */
boost::python::str convert_to_py_string(RPCSpecValueString* value_ptr) {
	boost::python::str result_value;
	return boost::python::str(value_ptr->get_value());
}

boost::python::long_ convert_to_py_int(RPCSpecValueInteger* value_ptr) {
	boost::python::long_ result_value;
	return boost::python::long_(value_ptr->get_value());
}


boost::python::dict convert_to_py_dict(RPCSpecValueDict* value) {
	boost::python::dict result_value;
	std::map<std::string, RPCSpecValue::APtr>::const_iterator i(value->get_value().begin());
	while( i != value->get_value().end()) {
		result_value[i->first] = convert_to_py(i->second);
		i++;
	}
	return result_value;
}

boost::python::list convert_to_py_list(RPCSpecValueList* value) {
	boost::python::list result_value;
    std::vector<RPCSpecValue::APtr>::const_iterator i(value->get_value().begin());
	while(i != value->get_value().end()) {
		result_value.append(convert_to_py(*i));
		i++;
	}
	return result_value;
}

boost::python::tuple convert_to_py_tuple(RPCSpecValueTuple* value) {
	boost::python::list result_value;
    std::vector<RPCSpecValue::APtr>::const_iterator i(value->get_value().begin());
	while(i != value->get_value().end()) {
		result_value.append(convert_to_py(*i));
		i++;
	}
	return boost::python::tuple(result_value);
}

boost::python::object convert_to_py(const RPCSpecValue::APtr& value) {
	if (RPCSpecValueNone* value_as_string_ptr = dynamic_cast<RPCSpecValueNone*>(value.get())) {
		return boost::python::object();
	}
	if (RPCSpecValueString* value_as_string_ptr = dynamic_cast<RPCSpecValueString*>(value.get())) {
		return convert_to_py_string(value_as_string_ptr);
	}
	if (RPCSpecValueInteger* value_as_int_ptr = dynamic_cast<RPCSpecValueInteger*>(value.get())) {
		return convert_to_py_int(value_as_int_ptr);
	}
	if (RPCSpecValueList* value_as_list_ptr = dynamic_cast<RPCSpecValueList*>(value.get())) {
		return convert_to_py_list(value_as_list_ptr);
	}
	if (RPCSpecValueDict* value_as_dict_ptr = dynamic_cast<RPCSpecValueDict*>(value.get())) {
		return convert_to_py_dict(value_as_dict_ptr);
	}
	if (RPCSpecValueTuple* value_as_tuple_ptr = dynamic_cast<RPCSpecValueTuple*>(value.get())) {
		return convert_to_py_tuple(value_as_tuple_ptr);
	}
	boost::python::str result_value("Type of rpc-value not supported");
	return result_value;
}


boost::python::tuple Giritech::RPC::rpc_xml_decode(const std::string& xml_doc) {
	Giritech::Communication::PYAPI::ThreadLockNoLog python_thread_lock;
	RPCSpecCall::APtr rpm_call(RPCSpecCall::create_from_string(xml_doc));
    boost::python::dict args = convert_to_py_dict(rpm_call->get_args().get());
    return boost::python::make_tuple(true, "OK", rpm_call->get_function_name(), args);
}

//void CPyFormat::Add( boost::python::dict& rdictValues )
//{
//   gd_std::wstring stringKey, stringValue;
//   boost::python::object objectKey, objectValue;
//   const boost::python::object objectKeys = rdictValues.iterkeys();
//   const boost::python::object objectValues = rdictValues.itervalues();
//   unsigned long ulCount = boost::python::extract<unsigned long>(rdictValues.attr("__len__")());
//   for( unsigned long u = 0; u < ulCount; u++ )
//   {
//      objectKey = objectKeys.attr( "next" )();
//      objectValue = objectValues.attr( "next" )();
//
//      char chCheckKey = objectKey.ptr()->ob_type->tp_name[0];
//      // simple check
//      if( chCheckKey != 's' && chCheckKey != 'u' ) throw std::runtime_error("Unknown key type" );
//
//      if( chCheckKey == 's' ) stringKey = boost::python::extract<>(objectKey);
//      else stringKey = boost::python::extract<std::wstring>(objectKey);
//
//      char chCheckValue = objectValue.ptr()->ob_type->tp_name[0];
//      // simple check
//      if( chCheckValue != 's' && chCheckValue != 'u' ) throw std::runtime_error( "Unknown value type" );
//
//      if( chCheckValue == 's' ) stringValue = boost::python::extract<const char*>(objectValue);
//      else stringValue = boost::python::extract<std::wstring>(objectValue);
//
//
//      m_vectorProperties.push_back( std::pair<std::wstring,std::wstring>(stringKey.c_str(), stringValue.c_str() ) );
//   }
//}


