/*! \file GPM_Spec.cxx
 \brief This file contains the implementaion of the Gpm class
 */
#include <sstream>
#include <iostream>
#include <string>

#include <boost/lexical_cast.hpp>

#include <lib/rpc/RPC_Spec.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::RPC;


/*
   base64.cpp and base64.h

   Copyright (C) 2004-2008 René Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   René Nyffenegger rene.nyffenegger@adp-gmbh.ch

*/
static const std::string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
  std::string ret;
  int i = 0;
  int j = 0;
  unsigned char char_array_3[3];
  unsigned char char_array_4[4];

  while (in_len--) {
    char_array_3[i++] = *(bytes_to_encode++);
    if (i == 3) {
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        ret += base64_chars[char_array_4[i]];
      i = 0;
    }
  }

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret += base64_chars[char_array_4[j]];

    while((i++ < 3))
      ret += '=';

  }

  return ret;

}

std::string base64_decode(std::string const& encoded_string) {
  int in_len = encoded_string.size();
  int i = 0;
  int j = 0;
  int in_ = 0;
  unsigned char char_array_4[4], char_array_3[3];
  std::string ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4) {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += char_array_3[i];
      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
  }

  return ret;
}

/*
 * ------------------------------------------------------------------
 * RPCSpecValue
 * ------------------------------------------------------------------
 */
RPCSpecValue::~RPCSpecValue() {
}

RPCSpecValue::APtr RPCSpecValue::create_from_parent(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLHandle doc_handler(parent);
	tinyxml2::XMLElement* rpc_value = doc_handler.FirstChildElement("rpc_value_string").ToElement();
	if(rpc_value != NULL) {
		const char* rpc_value_content(rpc_value->GetText());
		if (rpc_value_content == NULL) {
			return RPCSpecValueString::create_from_content("");
		}
		return RPCSpecValueString::create_from_content(rpc_value->GetText());
	}
	rpc_value = doc_handler.FirstChildElement("rpc_value_integer").ToElement();
	if(rpc_value != NULL) {
		return RPCSpecValueInteger::create_from_content(rpc_value->GetText());
	}
	rpc_value = doc_handler.FirstChildElement("rpc_value_none").ToElement();
	if(rpc_value != NULL) {
		return RPCSpecValueNone::create();
	}
	rpc_value = doc_handler.FirstChildElement("rpc_value_list").ToElement();
	if(rpc_value != NULL) {
		return RPCSpecValueList::create_from_parent(parent);
	}
	rpc_value = doc_handler.FirstChildElement("rpc_value_dict").ToElement();
	if(rpc_value != NULL) {
		return RPCSpecValueDict::create_from_parent(parent);
	}
	rpc_value = doc_handler.FirstChildElement("rpc_value_tuple").ToElement();
	if(rpc_value != NULL) {
		return RPCSpecValueTuple::create_from_parent(parent);
	}
	return RPCSpecValueString::create("ERROR");
}

/*
 * ------------------------------------------------------------------
 * RPCSpecValueNone
 * ------------------------------------------------------------------
 */
RPCSpecValueNone::RPCSpecValueNone(void) {
}

RPCSpecValueNone::~RPCSpecValueNone() {
}

RPCSpecValueNone::APtr RPCSpecValueNone::create(void) {
	return APtr(new RPCSpecValueNone());
}

tinyxml2::XMLElement* RPCSpecValueNone::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_none");
    parent->InsertEndChild(element);
	return element;
}




/*
 * ------------------------------------------------------------------
 * RPCSpecValueString
 * ------------------------------------------------------------------
 */
RPCSpecValueString::RPCSpecValueString(const std::string& value) : value_(value){
}

RPCSpecValueString::~RPCSpecValueString() {
}

std::string RPCSpecValueString::get_value(void) {
	return value_;
}


RPCSpecValueString::APtr RPCSpecValueString::create(const std::string& value) {
	return APtr(new RPCSpecValueString(value));
}

RPCSpecValueString::APtr RPCSpecValueString::create_from_content(const std::string& content) {
	return APtr(new RPCSpecValueString(base64_decode(content)));
}


tinyxml2::XMLElement* RPCSpecValueString::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_string");
    string value_base64(base64_encode(reinterpret_cast<const unsigned char*>(value_.c_str()), value_.length()));
	tinyxml2::XMLText* content = parent->GetDocument()->NewText(value_base64.c_str());
    element->InsertEndChild(content);
	parent->InsertEndChild(element);
	return element;
}



/*
 * ------------------------------------------------------------------
 * RPCSpecValueInteger
 * ------------------------------------------------------------------
 */
RPCSpecValueInteger::RPCSpecValueInteger(const long& value) : value_(value) {
}

RPCSpecValueInteger::~RPCSpecValueInteger() {
}
long RPCSpecValueInteger::get_value(void) {
	return value_;
}

RPCSpecValueInteger::APtr RPCSpecValueInteger::create(const long& value) {
	return APtr(new RPCSpecValueInteger(value));
}

RPCSpecValueInteger::APtr RPCSpecValueInteger::create_from_content(const std::string& content) {
	return APtr(new RPCSpecValueInteger(boost::lexical_cast<long>(content)));
}

tinyxml2::XMLElement* RPCSpecValueInteger::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_integer");
	tinyxml2::XMLText* content = parent->GetDocument()->NewText(boost::lexical_cast<std::string>(value_).c_str());
	element->InsertEndChild(content);
    parent->InsertEndChild(element);
	return element;
}

/*
 * ------------------------------------------------------------------
 * RPCSpecValueDict
 * ------------------------------------------------------------------
 */
RPCSpecValueDict::RPCSpecValueDict(void) {
}

RPCSpecValueDict::~RPCSpecValueDict() {
}

RPCSpecValueDict::APtr RPCSpecValueDict::create(void) {
	return APtr(new RPCSpecValueDict());
}

void RPCSpecValueDict::update(const std::string& key, RPCSpecValue::APtr key_value) {
	value_[key] = key_value;
}

std::map<std::string, RPCSpecValue::APtr>& RPCSpecValueDict::get_value(void)  {
	return value_;
}


RPCSpecValue::APtr RPCSpecValueDict::get(const std::string& key) {
	std::map<std::string, RPCSpecValue::APtr>::const_iterator i(value_.find(key));
	if(i != value_.end()) {
		return i->second;
	}
	stringstream ss;
	ss << "key '" << key << "' not found";
	throw Exception_RPC(ss.str());
}

std::string RPCSpecValueDict::get_as_string(const std::string& key) {
	RPCSpecValue::APtr value(get(key));
	if (RPCSpecValueString* value_as_string_ptr = dynamic_cast<RPCSpecValueString*>(value.get())) {
		return value_as_string_ptr->get_value();
	}
	stringstream ss;
	ss << "key '" << key << "' has not type string";
	throw Exception_RPC(ss.str());
}
std::string RPCSpecValueDict::get_as_string(const std::string& key, const std::string& default_value) {
	try {
		RPCSpecValue::APtr value(get(key));
		if (RPCSpecValueString* value_as_string_ptr = dynamic_cast<RPCSpecValueString*>(value.get())) {
			return value_as_string_ptr->get_value();
		}
	}
	catch(...) {
	}
	return default_value;
}

long RPCSpecValueDict::get_as_integer(const std::string& key, const long default_value) {
	try {
		RPCSpecValue::APtr value(get(key));
		if (RPCSpecValueInteger* value_as_int_ptr = dynamic_cast<RPCSpecValueInteger*>(value.get())) {
			return value_as_int_ptr->get_value();
		}
	}
	catch(...) {
	}
	return default_value;
}

long RPCSpecValueDict::get_as_integer(const std::string& key) {
	RPCSpecValue::APtr value(get(key));
	if (RPCSpecValueInteger* value_as_int_ptr = dynamic_cast<RPCSpecValueInteger*>(value.get())) {
		return value_as_int_ptr->get_value();
	}
	stringstream ss;
	ss << "key '" << key << "' has not type integer";
	throw Exception_RPC(ss.str());
}

std::vector<RPCSpecValue::APtr> RPCSpecValueDict::get_as_list(const std::string& key) {
	RPCSpecValue::APtr value(get(key));
	if (RPCSpecValueList* value_as_list_ptr = dynamic_cast<RPCSpecValueList*>(value.get())) {
		return value_as_list_ptr->get_value();
	}
	stringstream ss;
	ss << "key '" << key << "' has not type list";
	throw Exception_RPC(ss.str());
}

RPCSpecValueList::APtr RPCSpecValueDict::get_as_valuelist(const std::string& key) {
	RPCSpecValue::APtr value(get(key));
	RPCSpecValueList::APtr value_as_list = boost::dynamic_pointer_cast<RPCSpecValueList>(value);
	if (value_as_list.get() != NULL) {
		return value_as_list;
	}
	stringstream ss;
	ss << "key '" << key << "' has not type list";
	throw Exception_RPC(ss.str());
}


RPCSpecValueDict::APtr RPCSpecValueDict::get_as_dict(const std::string& key) {
	return value_to_dict(get(key));
}

RPCSpecValueDict::APtr RPCSpecValueDict::create_from_parent(tinyxml2::XMLElement* rpc_call) {
	RPCSpecValueDict::APtr result_dict(RPCSpecValueDict::create());

	tinyxml2::XMLHandle doc_handler(rpc_call);
	tinyxml2::XMLElement* dict_element = doc_handler.FirstChildElement("rpc_value_dict").FirstChildElement("rpc_value_dict_element").ToElement();
	while(dict_element) {
		const char* dict_element_key;
	    if (dict_element->QueryStringAttribute("name", &dict_element_key)!=tinyxml2::XML_SUCCESS) {
	        throw Exception_RPC("rpc_value_dict_element' node did not contain the attribute 'name'");
	    }
	    result_dict->update(std::string(dict_element_key), RPCSpecValue::create_from_parent(dict_element));
	    dict_element = dict_element->NextSiblingElement("rpc_value_dict_element");
	}
	return result_dict;
}


tinyxml2::XMLElement* RPCSpecValueDict::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_dict");
	std::map<std::string, RPCSpecValue::APtr>::const_iterator i(value_.begin());
	while( i != value_.end()) {
		tinyxml2::XMLElement* dict_element = parent->GetDocument()->NewElement("rpc_value_dict_element");
		dict_element->SetAttribute("name", i->first.c_str());
        i->second->to_xml(dict_element);
        element->InsertEndChild(dict_element);
		i++;
	}
    parent->InsertEndChild(element);
	return element;
}



/*
 * ------------------------------------------------------------------
 * RPCSpecValueList
 * ------------------------------------------------------------------
 */
RPCSpecValueList::RPCSpecValueList(void) {
}

RPCSpecValueList::~RPCSpecValueList() {
}

RPCSpecValueList::APtr RPCSpecValueList::create(void) {
	return APtr(new RPCSpecValueList());
}

RPCSpecValueList::APtr RPCSpecValueList::create_from_strings(const std::vector<std::string>& strings) {
	RPCSpecValueList::APtr result(RPCSpecValueList::create());
	std::vector<std::string>::const_iterator i(strings.begin());
	while( i != strings.end()) {
		result->push_back(RPCSpecValueString::create(*i));
		++i;
	}
	return result;
}

void RPCSpecValueList::push_back(RPCSpecValue::APtr element) {
	value_.push_back(element);
}

std::vector<RPCSpecValue::APtr>& RPCSpecValueList::get_value(void) {
	return value_;
}

std::vector<std::string> RPCSpecValueList::get_as_list_of_strings(void) {
	std::vector<std::string> result;
	std::vector<RPCSpecValue::APtr>::const_iterator i(value_.begin());
	while( i != value_.end()) {
		try {
			if (RPCSpecValueString* value_as_string_ptr = dynamic_cast<RPCSpecValueString*>(i->get())) {
				result.push_back( value_as_string_ptr->get_value());
			}
			else {
				result.push_back( "NOT ABLE TO CONVERT TO STRING 1");
			}
		}
		catch(...) {
			result.push_back( "NOT ABLE TO CONVERT TO STRING 2");
		}
		i++;
	}
	return result;
}

RPCSpecValueList::APtr RPCSpecValueList::create_from_parent(tinyxml2::XMLElement* rpc_call) {
	RPCSpecValueList::APtr result_list(RPCSpecValueList::create());
	tinyxml2::XMLHandle doc_handler(rpc_call);
	tinyxml2::XMLElement* dict_element = doc_handler.FirstChildElement("rpc_value_list").FirstChildElement("rpc_value_list_element").ToElement();
	while(dict_element) {
		result_list->push_back(RPCSpecValue::create_from_parent(dict_element));
	    dict_element = dict_element->NextSiblingElement("rpc_value_list_element");
	}
	return result_list;
}

tinyxml2::XMLElement* RPCSpecValueList::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_list");
	std::vector<RPCSpecValue::APtr>::const_iterator i(value_.begin());
	while( i != value_.end()) {
		tinyxml2::XMLElement* dict_element = parent->GetDocument()->NewElement("rpc_value_list_element");
        (*i)->to_xml(dict_element);
        element->InsertEndChild(dict_element);
		i++;
	}
    parent->InsertEndChild(element);
	return element;
}

/*
 * ------------------------------------------------------------------
 * RPCSpecValueTuple
 * ------------------------------------------------------------------
 */
RPCSpecValueTuple::RPCSpecValueTuple(void) {
}

RPCSpecValueTuple::~RPCSpecValueTuple() {
}

RPCSpecValueTuple::APtr RPCSpecValueTuple::create(void) {
	return APtr(new RPCSpecValueTuple());
}

void RPCSpecValueTuple::push_back(RPCSpecValue::APtr element) {
	value_.push_back(element);
}

std::vector<RPCSpecValue::APtr>& RPCSpecValueTuple::get_value(void) {
	return value_;
}

RPCSpecValueTuple::APtr RPCSpecValueTuple::create_from_parent(tinyxml2::XMLElement* rpc_call) {
	RPCSpecValueTuple::APtr result_list(RPCSpecValueTuple::create());
	tinyxml2::XMLHandle doc_handler(rpc_call);
	tinyxml2::XMLElement* dict_element = doc_handler.FirstChildElement("rpc_value_tuple").FirstChildElement("rpc_value_tuple_element").ToElement();
	while(dict_element) {
		result_list->push_back(RPCSpecValue::create_from_parent(dict_element));
	    dict_element = dict_element->NextSiblingElement("rpc_value_tuple_element");
	}
	return result_list;
}

tinyxml2::XMLElement* RPCSpecValueTuple::to_xml(tinyxml2::XMLElement* parent) {
	tinyxml2::XMLElement* element = parent->GetDocument()->NewElement("rpc_value_tuple");
	std::vector<RPCSpecValue::APtr>::const_iterator i(value_.begin());
	while( i != value_.end()) {
		tinyxml2::XMLElement* dict_element = parent->GetDocument()->NewElement("rpc_value_tuple_element");
        (*i)->to_xml(dict_element);
		element->InsertEndChild(dict_element);
		i++;
	}
    parent->InsertEndChild(element);
	return element;
}




/*
 * ------------------------------------------------------------------
 * RPCSpecCall
 * ------------------------------------------------------------------
 */
RPCSpecCall::RPCSpecCall(const std::string function_name, RPCSpecValueDict::APtr& args)
: function_name_(function_name), args_(args) {
}

RPCSpecCall::~RPCSpecCall() {
}

RPCSpecCall::APtr RPCSpecCall::create(const std::string function_name, RPCSpecValueDict::APtr& args) {
	return APtr(new RPCSpecCall(function_name, args));
}

RPCSpecCall::APtr RPCSpecCall::create_from_string(const std::string& xml_document) {
	tinyxml2::XMLDocument doc;
    tinyxml2::XMLHandle doc_handler(&doc);
    doc.Parse(xml_document.c_str());
    if (doc.Error()) {
        throw Exception_RPC(doc.ErrorStr());
    }
    tinyxml2::XMLElement* rpc_call = doc_handler.FirstChildElement("rpc_call").ToElement();
    if (rpc_call == NULL) {
        throw Exception_RPC("'rpc_call' node not found in xml-document");
    }
	const char* function_name_value;
    if (rpc_call->QueryStringAttribute("name", &function_name_value)!=tinyxml2::XML_SUCCESS) {
        throw Exception_RPC("'rpc_call' node did not contain the attribute 'name'");
    }
    RPCSpecValueDict::APtr args(RPCSpecValueDict::create_from_parent(rpc_call));
    return RPCSpecCall::create(std::string(function_name_value), args);
}

std::string RPCSpecCall::to_xml_doc_string(void) {
	tinyxml2::XMLDocument doc;

	tinyxml2::XMLDeclaration* decl = doc.NewDeclaration("version=\"1.0\"");

	tinyxml2::XMLElement* element = doc.NewElement("rpc_call");
	element->SetAttribute("name", function_name_.c_str());

    args_->to_xml(element);

    doc.InsertEndChild(decl);
    doc.InsertEndChild(element);

	tinyxml2::XMLPrinter printer;
	doc.Accept(&printer);
	return std::string(printer.CStr());
}

RPCSpecValueDict::APtr RPCSpecCall::get_args(void) {
	return args_;
}

std::string RPCSpecCall::get_function_name(void) {
	return function_name_;
}

/*
 * Converters
 */
RPCSpecValueDict::APtr Giritech::RPC::value_to_dict(const RPCSpecValue::APtr& value) {
	RPCSpecValueDict::APtr value_as_dict = boost::dynamic_pointer_cast<RPCSpecValueDict>(value);
	if (value_as_dict.get() != NULL) {
		return value_as_dict;
	}
	stringstream ss;
	ss << "ERROR: value_to_dict did not get dict type";
	throw Exception_RPC(ss.str());
}

