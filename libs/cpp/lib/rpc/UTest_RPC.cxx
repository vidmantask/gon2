/*! \file UTest_RPC.cxx
 \brief This file contains unittest suite for the RPC functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>
#include <lib/rpc/RPC_Spec.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::RPC;

/*! /brief Unittest for the the basis specification
 */
BOOST_AUTO_TEST_CASE( test_rpc_parse_spec ) {
    const char rpm_spec_001_data[] = "<?xml version='1.0'?>"
            "<rpc_call name='my_func'>"
            "  <rpc_value_dict>"
            "    <rpc_value_dict_element name='arg_1'><rpc_value_string>1234</rpc_value_string></rpc_value_dict_element>"
            "    <rpc_value_dict_element name='arg_2'><rpc_value_integer>1234</rpc_value_integer></rpc_value_dict_element>"
            "    <rpc_value_dict_element name='arg_3'><rpc_value_string>efghi</rpc_value_string></rpc_value_dict_element>"
            "    <rpc_value_dict_element name='arg_4'>"
            "        <rpc_value_list>"
    		"           <rpc_value_list_element><rpc_value_string>efghi.1</rpc_value_string></rpc_value_list_element>"
    		"           <rpc_value_list_element><rpc_value_string>efghi.2</rpc_value_string></rpc_value_list_element>"
    		"           <rpc_value_list_element><rpc_value_string>efghi.3</rpc_value_string></rpc_value_list_element>"
            "        </rpc_value_list>"
            "    </rpc_value_dict_element>"
            "    <rpc_value_dict_element name='arg_5'>"
            "      <rpc_value_dict>"
            "        <rpc_value_dict_element name='arg_1'><rpc_value_string>1234</rpc_value_string></rpc_value_dict_element>"
            "        <rpc_value_dict_element name='arg_2'><rpc_value_integer>1234</rpc_value_integer></rpc_value_dict_element>"
            "      </rpc_value_dict>"
            "    </rpc_value_dict_element>"
            "    <rpc_value_dict_element name='arg_6'>"
            "      <rpc_value_tuple>"
            "        <rpc_value_tuple_element><rpc_value_string>1234</rpc_value_string></rpc_value_tuple_element>"
            "        <rpc_value_tuple_element><rpc_value_none/></rpc_value_tuple_element>"
            "      </rpc_value_tuple>"
            "    </rpc_value_dict_element>"
            "  </rpc_value_dict>"
            "</rpc_call>";

    RPCSpecCall::APtr rpm_call(RPCSpecCall::create_from_string(rpm_spec_001_data));
    string rpc_call_string(rpm_call->to_xml_doc_string());
    rpm_call = RPCSpecCall::create_from_string(rpc_call_string);

    cout << rpm_call->to_xml_doc_string() << endl;
}


boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
