/*! \file SWISSBIT_API_Authorization.hxx
 */
#include <boost/python.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <lib/swissbit/SWISSBIT_API.hxx>
#include <lib/swissbit/SWISSBIT_Key.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Swissbit;

#define DEFAULT_RETRY_COUNT 5


static boost::recursive_mutex global_swissbit_mutex;

class AllowPythonThreads {
public:
    AllowPythonThreads(const std::string& id) :
        id_(id) {
        save_ = PyEval_SaveThread();
        global_swissbit_mutex.lock();
    }
    ~AllowPythonThreads() {
    	global_swissbit_mutex.unlock();
        PyEval_RestoreThread(save_);
    }
private:
    std::string id_;
    PyThreadState *save_;
};

boost::python::tuple Giritech::Swissbit::API::key_init(void) {
    AllowPythonThreads allow_python_threads("key_init");
    return boost::python::make_tuple(true, "ok");
}

boost::python::tuple Giritech::Swissbit::API::key_create_challenge_signature(
		const std::string& msc_filename,
		const std::string& challenge) {
    AllowPythonThreads allow_python_threads("key_create_challenge_signature");
	return key_create_challenge_signature_internal(msc_filename, challenge, DEFAULT_RETRY_COUNT);
}

boost::python::tuple Giritech::Swissbit::API::key_create_challenge_signature_internal(
		const std::string& msc_filename,
		const std::string& challenge,
		const unsigned int& retry_count) {
    Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(msc_filename));
    try {
    	key->connect();
    	Utility::DataBufferManaged::APtr signature(key->create_challenge_signature(Utility::DataBufferManaged::create(challenge)));
        return boost::python::make_tuple(true, "ok", signature->toString());
    }
    catch(const Swissbit::SwissbitKey::Exception_Swissbit& e) {
    	if(retry_count <= 0) {
    		return boost::python::make_tuple(false, e.what(), boost::python::object());
    	}
    	return key_create_challenge_signature_internal(msc_filename, challenge, retry_count-1);
    }
    catch(...) {
    }
    return boost::python::make_tuple(false, "unexpected exception", boost::python::object());
}

boost::python::tuple Giritech::Swissbit::API::key_generate_keypair(
		const std::string& msc_filename,
		const unsigned long& keylength) {
    AllowPythonThreads allow_python_threads("key_generate_keypair");
	return key_generate_keypair_internal(msc_filename, keylength, DEFAULT_RETRY_COUNT);
}

boost::python::tuple Giritech::Swissbit::API::key_generate_keypair_internal(
		const std::string& msc_filename,
		const unsigned long& keylength,
		const unsigned int& retry_count) {
    try {
        Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(msc_filename));
    	key->connect();
    	key->generate_keypair();
        return boost::python::make_tuple(true, "ok");
    }
    catch(const Swissbit::SwissbitKey::Exception_Swissbit& e) {
    	if(retry_count <= 0) {
    		return boost::python::make_tuple(false, e.what());
    	}
    	return key_generate_keypair_internal(msc_filename, keylength, retry_count-1);
    }
    catch(...) {
    }
    return boost::python::make_tuple(false, "unexpected exception");
}

boost::python::tuple Giritech::Swissbit::API::key_get_public_key(
		const std::string& msc_filename) {
    AllowPythonThreads allow_python_threads("key_get_public_key");
	return key_get_public_key_internal(msc_filename, DEFAULT_RETRY_COUNT);
}

boost::python::tuple Giritech::Swissbit::API::key_get_public_key_internal(
		const std::string& msc_filename,
		const unsigned int& retry_count) {
    try {
        Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(msc_filename));
    	key->connect();
    	Utility::DataBufferManaged::APtr public_key(key->get_public_key());
        return boost::python::make_tuple(true, "ok", public_key->toString());
    }
    catch(const Swissbit::SwissbitKey::Exception_Swissbit& e) {
    	if(retry_count <= 0) {
    		return boost::python::make_tuple(false, e.what(), boost::python::object());
    	}
    	return key_get_public_key_internal(msc_filename, retry_count-1);
    }
    catch(...) {
    }
    return boost::python::make_tuple(false, "unexpected exception", boost::python::object());
}

boost::python::tuple Giritech::Swissbit::API::key_write_file(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const std::string& data) {
    AllowPythonThreads allow_python_threads("key_write_file");
	return key_write_file_internal(msc_filename, file_id, data, DEFAULT_RETRY_COUNT);
}

boost::python::tuple Giritech::Swissbit::API::key_write_file_internal(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const std::string& data,
		const unsigned int& retry_count) {
    try {
        Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(msc_filename));
    	key->connect();
    	key->write_to_file(file_id, Utility::DataBufferManaged::create(data));
        return boost::python::make_tuple(true, "ok");
    }
    catch(const Swissbit::SwissbitKey::Exception_Swissbit& e) {
    	if(retry_count <= 0) {
    		return boost::python::make_tuple(false, e.what());
    	}
    	return key_write_file_internal(msc_filename, file_id, data, retry_count-1);
    }
    catch(...) {
    }
    return boost::python::make_tuple(false, "unexpected exception");
}

boost::python::tuple Giritech::Swissbit::API::key_read_file(
		const std::string& msc_filename,
		const unsigned char& file_id) {
    AllowPythonThreads allow_python_threads("key_read_file");
	return key_read_file_internal(msc_filename, file_id, DEFAULT_RETRY_COUNT);
}

boost::python::tuple Giritech::Swissbit::API::key_read_file_internal(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const unsigned int& retry_count) {
    try {
        Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(msc_filename));
    	key->connect();
    	Utility::DataBufferManaged::APtr data(key->read_from_file(file_id));
        return boost::python::make_tuple(true, "ok", data->toString());
    }
    catch(const Swissbit::SwissbitKey::Exception_Swissbit& e) {
    	if(retry_count <= 0) {
    		return boost::python::make_tuple(false, e.what(), boost::python::object());
    	}
    	return key_read_file_internal(msc_filename, file_id, retry_count-1);
    }
    catch(...) {
    }
    return boost::python::make_tuple(false, "unexpected exception", boost::python::object());
}
