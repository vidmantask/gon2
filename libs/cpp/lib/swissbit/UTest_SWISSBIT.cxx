/*! \file UTest_SWISSBIT.cxx
 \brief This file contains unittest suite for the Swissbit functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/optional.hpp>

#include "lib/swissbit/SWISSBIT_Key.hxx"
#include "lib/utility/UY_CheckpointHandler.hxx"
#include <lib/cryptfacility/CF_ALL.hxx>
#include "CardReader.h"

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
static CryptoPP::PNew s_pNew = NULL;
static CryptoPP::PDelete s_pDelete = NULL;

extern "C" __declspec(dllexport) void __cdecl SetNewAndDeleteFromCryptoPP(CryptoPP::PNew pNew, CryptoPP::PDelete pDelete,  CryptoPP::PSetNewHandler pSetNewHandler) {
    s_pNew = pNew;
    s_pDelete = pDelete;
}

void * __cdecl operator new (size_t size) {
    return s_pNew(size);
}

void __cdecl operator delete (void * p) {
    s_pDelete(p);
}
#endif



using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;



void append_sub_folders(const boost::filesystem::path& token_root, std::vector<boost::filesystem::path>& token_roots, const bool recursive) {
  cout << "testing " << token_root.string() << endl;
	if (boost::filesystem::is_directory(token_root)) {
		copy(boost::filesystem::directory_iterator(token_root), boost::filesystem::directory_iterator(), std::back_inserter(token_roots));
	}
}

bool find_token_is_token_root(const boost::filesystem::path& token_root) {
	if (boost::filesystem::exists(token_root / "gon_unittest")) {
		return true;
	}
	return false;
}

boost::optional<std::string> find_token(void) {
	std::vector<boost::filesystem::path> token_roots;
  cout << "a" << endl;

#ifdef GIRITECH_COMPILEOPTION_TARGET_LINUX
	const char* p_user;
	p_user = getenv("USER");
	append_sub_folders(boost::filesystem::path("/run/media") / p_user, token_roots, false);
#endif
#ifdef GIRITECH_COMPILEOPTION_TARGET_WIN
//	token_roots.push_back(boost::filesystem::path("d:"));
//	token_roots.push_back(boost::filesystem::path("e:"));
	token_roots.push_back(boost::filesystem::path("f:"));
	token_roots.push_back(boost::filesystem::path("g:"));
	token_roots.push_back(boost::filesystem::path("h:"));
	token_roots.push_back(boost::filesystem::path("i:"));
	token_roots.push_back(boost::filesystem::path("j:"));
	token_roots.push_back(boost::filesystem::path("k:"));
	token_roots.push_back(boost::filesystem::path("l:"));
	token_roots.push_back(boost::filesystem::path("m:"));
	token_roots.push_back(boost::filesystem::path("n:"));
	token_roots.push_back(boost::filesystem::path("o:"));
#endif
#ifdef GIRITECH_COMPILEOPTION_TARGET_MAC
	append_sub_folders(boost::filesystem::path("/Volumes"), token_roots, false);
#endif

	std::vector<boost::filesystem::path>::const_iterator i(token_roots.begin());
	while(i != token_roots.end()) {
    cout << *i << endl;
		if(find_token_is_token_root(*i)) {
			return boost::optional<std::string>(i->string());
		}
		i++;
	}
	return boost::optional<std::string>();
}



BOOST_AUTO_TEST_CASE( test_swissbit_basis )
{

	boost::optional<std::string> token(find_token());
	if(!token) {
		BOOST_FAIL( "No token found");
		return;
	}
  cout << "Token: " << *token << endl;
	Swissbit::SwissbitKey::APtr key_001(Swissbit::SwissbitKey::create((*token).c_str()));

	cout << "connect.begin" << endl;
	try {
		key_001->connect();
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "connect.end" << endl;

	cout << "get_version.begin" << endl;
	try {
		Utility::DataBufferManaged::APtr version(key_001->get_version());
		cout << "  version : " << version->encodeHex()->toString() << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "get_version.end" << endl;

	Utility::DataBufferManaged::APtr public_key(Utility::DataBufferManaged::create(""));
	cout << "get_public_key.begin" << endl;
	try {
		public_key = key_001->get_public_key();
		cout << "  public_key : " << public_key->encodeHex()->toString() << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "get_public_key.end" << endl;

	cout << "generate_keypair.begin" << endl;
	try {
		key_001->generate_keypair();
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "generate_keypair.end" << endl;

	cout << "get_public_key.begin" << endl;
	try {
		Utility::DataBufferManaged::APtr public_key_new(key_001->get_public_key());
		cout << "  public_key : " << public_key_new->encodeHex()->toString() << endl;
		BOOST_CHECK(public_key->encodeHex()->toString() != public_key_new->encodeHex()->toString());
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "get_public_key.end" << endl;

	cout << "create_challenge_signature.begin" << endl;
	try {
		Utility::DataBufferManaged::APtr signature_001(key_001->create_challenge_signature(Utility::DataBufferManaged::create("my challenge_001")));
		cout << "  signature : " << signature_001->encodeHex()->toString() << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "create_challenge_signature.end" << endl;

	cout << "write_to_file.begin" << endl;
	Utility::DataBufferManaged::APtr data_000(Utility::DataBufferManaged::create("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890"));
	try {
		key_001->write_to_file(0, data_000);
		cout << "  write_0_ok" << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "write_to_file.end" << endl;

	cout << "read_from_file.begin" << endl;
	try {
		Utility::DataBufferManaged::APtr data_000_r(key_001->read_from_file(0));
		BOOST_CHECK( data_000_r->toString() == data_000->toString() );
		cout << "  read_0 : " << data_000_r->toString() << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	cout << "read_from_file.end" << endl;

}


BOOST_AUTO_TEST_CASE( test_swissbit_sign )
{
	boost::optional<std::string> token(find_token());
	if(!token) {
		BOOST_FAIL( "No token found");
		return;
	}

	Swissbit::SwissbitKey::APtr key_001(Swissbit::SwissbitKey::create((*token).c_str()));
	try {
		cout << "sign.begin" << endl;
		key_001->connect();
		Utility::DataBufferManaged::APtr challenge(Utility::DataBufferManaged::create("123456"));
		Utility::DataBufferManaged::APtr public_key(key_001->get_public_key());
		Utility::DataBufferManaged::APtr challenge_signature(key_001->create_challenge_signature(challenge));
		cout << "sign.end" << endl;


		cout << "verify.begin" << endl;
	    CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Verifier verifier;

	    CryptoPP::Integer e(65537);
	    verifier.AccessKey().SetPublicExponent(e);

	    CryptoPP::Integer m;
	    m.Decode((const unsigned char*)public_key->data(), public_key->getSize());
	    verifier.AccessKey().SetModulus(m);

	    bool ok = verifier.VerifyMessage(
	            (const unsigned char*)(challenge->data()), challenge->getSize(),
	            (const unsigned char*)(challenge_signature->data()), challenge_signature->getSize());
        BOOST_CHECK( ok );
        cout << "verify.begin ok:" << ok << endl;
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
}

BOOST_AUTO_TEST_CASE( test_swissbit_raw)
{
	static unsigned char cmdSelectApplet[] = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0xD2, 0x76, 0x00, 0x01, 0x62, 0x4D, 0x69, 0x6E, 0x69, 0x41, 0x75, 0x74, 0x68, 0x01 };
	static unsigned char cmdVerifyPin[] = { 0xB0, 0x10, 0x00, 0x00, 0x06, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
	static unsigned char cmdRequestKey[] = { 0xB0, 0x32, 0x00, 0x01, 0x00 };
	static unsigned char cmdCreateDataFile[] = { 0xB0, 0x20, 0x00, 0x00, 0x02, 0x20, 0x00 };
	static unsigned char cmdSelectDataFile[] = { 0xB0, 0x22, 0x00, 0x00, 0x00 };
	static unsigned char cmdReadDataFile[] = { 0xB0, 0x23, 0x00, 0x00, 0x00 };

	boost::optional<std::string> token(find_token());
	if(!token) {
		BOOST_FAIL( "No token found");
		return;
	}

	boost::shared_ptr<sfc::CardReader> card_reader(boost::shared_ptr<sfc::CardReader>(new sfc::CardReader((*token).c_str())));

	// Connect to card and get ATR
	unsigned char recvAPDU[FSI_MAX_RESPONSE_SIZE];
	unsigned int recvAPDULen(FSI_MAX_RESPONSE_SIZE);

	cout << "run.1" << endl;
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->connect(SWISSBIT_TIMEOUT, recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->disconnect(SWISSBIT_TIMEOUT);
	cout << "run.1.done" << endl;

//	boost::this_thread::sleep(boost::posix_time::seconds(5));

	cout << "run.2" << endl;
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->connect(SWISSBIT_TIMEOUT, recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->disconnect(SWISSBIT_TIMEOUT);
	cout << "run.2.done" << endl;

//	boost::this_thread::sleep(boost::posix_time::seconds(5));

	cout << "run.3" << endl;
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->connect(SWISSBIT_TIMEOUT, recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->disconnect(SWISSBIT_TIMEOUT);
	cout << "run.3.done" << endl;

//	boost::this_thread::sleep(boost::posix_time::seconds(5));

	cout << "run.4" << endl;
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->connect(SWISSBIT_TIMEOUT, recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->transmit(SWISSBIT_TIMEOUT, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU, &recvAPDULen);
  recvAPDULen = FSI_MAX_RESPONSE_SIZE;
	card_reader->disconnect(SWISSBIT_TIMEOUT);
	cout << "run.4.done" << endl;

}


boost::unit_test::test_suite* init_unit_test_suite(int argc, char* argv[]) {
    return 0;
}
