/*! \file SWISSBIT_Key.hxx
 \brief This file contains Swisssbit lib
 */
#ifndef SWISSBIT_Key_HXX
#define SWISSBIT_Key_HXX

#include <sstream>
#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include "sfcconstants.h"

#define SWISSBIT_TIMEOUT 60000

namespace sfc {
	class CardReader;
};


namespace Giritech {
namespace Swissbit {

/*! \brief This class holds represent a Swissbit key
 */
class SwissbitKey {
public:
    typedef boost::shared_ptr<SwissbitKey> APtr;

    /*! \brief This exception is used as base for alle exceptions thrown by the module CryptFacility
     */
    class Exception_Swissbit:
    	public Giritech::Utility::Exception {
    public:
    	Exception_Swissbit(const std::string& message) :
            Exception(message) {
        }
    };

    ~SwissbitKey();

    static SwissbitKey::APtr create(
    		const boost::filesystem::path& com_filename);

    void connect(void);
    void disconnect(void);

    void generate_keypair(void);

    Utility::DataBufferManaged::APtr get_public_key(void);

    Utility::DataBufferManaged::APtr get_version(void);

    Utility::DataBufferManaged::APtr create_challenge_signature(
    		const Utility::DataBufferManaged::APtr& challenge);

	void write_to_file(
			const unsigned char fileNumber,
			const Utility::DataBufferManaged::APtr& data);

	Utility::DataBufferManaged::APtr read_from_file(
			const unsigned char fileNumber);

private:
    SwissbitKey(
    		const boost::filesystem::path& com_filename);

    bool check_apdu_error(void);
    bool get_apdu_data(Utility::DataBufferManaged::APtr& data);

    boost::filesystem::path com_filename_;
    boost::shared_ptr<sfc::CardReader> card_reader_;
    unsigned char recvAPDU_[FSI_MAX_RESPONSE_SIZE];
    unsigned int recvAPDULen_;
};

}
}
#endif
