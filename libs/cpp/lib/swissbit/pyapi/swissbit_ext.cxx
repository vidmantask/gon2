/*! \file swissbit_ext.cxx
 * \brief Implementation of python swissbit extenstion
 *
 * The api uses python objects for callback. Because callback is done
 * from another thread we need to aquire the global python interpretenter lock before doing the callback.
 *
 */
#include <boost/python.hpp>  // This should be included first, othervise mac build breaks
#include <string>


#include <lib/swissbit/SWISSBIT_API.hxx>
#include <lib/utility/UY_Exception.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>


using namespace std;
using namespace boost::python;
using namespace Giritech;
using namespace Giritech::Utility;


#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
extern "C" __declspec(dllexport) void __cdecl GetNewAndDeleteForCryptoPP(CryptoPP::PNew &pNew, CryptoPP::PDelete &pDelete) {
   pNew = &operator new;
   pDelete = &operator delete;
}
#endif


/*! \brief This exception is thrown in case of a error ocured during callback
*/
class ExceptionCallback : public Giritech::Utility::Exception {
    public:
        ExceptionCallback(const std::string& message) :
            Giritech::Utility::Exception(message) {
        }
};


class ThreadLock {
public:
    ThreadLock(const std::string& id) :
        id_(id) {
        state_ = PyGILState_Ensure();
    }
    ~ThreadLock() {
        PyGILState_Release(state_);
    }
private:
    PyGILState_STATE state_;
    std::string id_;
};




/*! \brief Initialice swissbit component
 */
void init_swissbit(void) {
    Giritech::CryptFacility::CryptFacilityService::getInstance().initialize(Giritech::CryptFacility::CryptFacilityService::modeofoperation_unknown);
}

/*
 * Define python api
 */
void def_api_lib_swissbit(void) {
    def("init_swissbit", &init_swissbit);

    def("key_create_challenge_signature", &Giritech::Swissbit::API::key_create_challenge_signature);
    def("key_generate_keypair", &Giritech::Swissbit::API::key_generate_keypair);
    def("key_get_public_key", &Giritech::Swissbit::API::key_get_public_key);
    def("key_write_file", &Giritech::Swissbit::API::key_write_file);
    def("key_read_file", &Giritech::Swissbit::API::key_read_file);

}


BOOST_PYTHON_MODULE(swissbit_ext) {
    docstring_options doc_options(true);
    def_api_lib_swissbit();
}
