/*! \file SWISSBIT_API.hxx
 \brief This file contains the api to the SWISSBIT functionality used by python extension.
 */
#ifndef SWISSBIT_API_HXX
#define SWISSBIT_API_HXX

#include <string>
#include <vector>
#include <list>
#include <boost/python.hpp>


#include <lib/utility/UY_API_Checkpoint.hxx>


namespace Giritech {
namespace Swissbit {
namespace API {


/*! \brief Initialization of SWISSBIT token
 * Returns a tuple (bool, message)
 */
boost::python::tuple key_init(void);


/*
 * is_ok, error_message, signature = lib_cpp.swissbit.create_challenge_signature(msc_filename, challenge)
 */
boost::python::tuple key_create_challenge_signature(
		const std::string& msc_filename,
		const std::string& challenge);

boost::python::tuple key_create_challenge_signature_internal(
		const std::string& msc_filename,
		const std::string& challenge,
		const unsigned int& retry_count);


/*
 * is_ok, error_message = lib_cpp.swissbit.key_generate_key_pair(msc_filename, keylength=2048)
 */
boost::python::tuple key_generate_keypair(
		const std::string& msc_filename,
		const unsigned long& keylength);

boost::python::tuple key_generate_keypair_internal(
		const std::string& msc_filename,
		const unsigned long& keylength,
		const unsigned int& retry_count);

/*
 * is_ok, error_message, public_key = lib_cpp.swissbit.key_get_public_key(msc_filename)
 *
 * public_key is a hex incoded string
 */
boost::python::tuple key_get_public_key(
		const std::string& msc_filename);

boost::python::tuple key_get_public_key_internal(
		const std::string& msc_filename,
		const unsigned int& retry_count);

/*
 * is_ok, error_message = lib_cpp.swissbit.key_write_file(msc_filename, CHUNK_ID_DEPLOY, chunk)
 */
boost::python::tuple key_write_file(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const std::string& data);

boost::python::tuple key_write_file_internal(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const std::string& data,
		const unsigned int& retry_count);

/*
 * is_ok, error_message, data = lib_cpp.swissbit.key_read_file(msc_filename, CHUNK_ID_INIT)
 */
boost::python::tuple key_read_file(
		const std::string& msc_filename,
		const unsigned char& file_id);

boost::python::tuple key_read_file_internal(
		const std::string& msc_filename,
		const unsigned char& file_id,
		const unsigned int& retry_count);

}
}
}
#endif
