
//#include "stdafx.h"
#include "excitor_token.h"
#include "CardReader.h"

#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>

using namespace sfc;

//constants
#define DEBUG_MODE 0
#define ERROR_MSG_LENGTH 1024
#define RETURN_APDU_LENGTH 512
#define FILE_I_CHUNK_SIZE 252
#define FILE_O_CHUNK_SIZE 256

//globals
static unsigned char recvAPDU[RETURN_APDU_LENGTH];
static unsigned int recvAPDULen = RETURN_APDU_LENGTH;
static char errorMsg[ERROR_MSG_LENGTH];

static CardReader* reader = NULL;

static unsigned char cmdSelectApplet[] = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0xD2, 0x76, 0x00, 0x01, 0x62, 0x4D, 0x69, 0x6E, 0x69, 0x41, 0x75, 0x74, 0x68, 0x01 };
static unsigned char cmdVerifyPin[] = { 0xB0, 0x10, 0x00, 0x00, 0x06, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
static unsigned char cmdRequestKey[] = { 0xB0, 0x32, 0x00, 0x01, 0x00 };
static unsigned char cmdCreateDataFile[] = { 0xB0, 0x20, 0x00, 0x00, 0x02, 0x20, 0x00 };
static unsigned char cmdSelectDataFile[] = { 0xB0, 0x22, 0x00, 0x00, 0x00 };
static unsigned char cmdReadDataFile[] = { 0xB0, 0x23, 0x00, 0x00, 0x00 };

//helper func
bool isTransmitError();
int ExcTokenGetResponse(unsigned char * resp, unsigned int * size);



//////////////////////////////////////////////////////////////////////////////
// API FUNCTIONS
//////////////////////////////////////////////////////////////////////////////
int ExcTokenConnectAny() {
	int rv = 0;

	try {
		bool isLoaded = false;
		const char drives[] = {'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'};
		for (int i = 0; i < sizeof(drives); i++) {
			try {
				reader = new CardReader((unsigned char)drives[i]);
				isLoaded = true;
				break;
			} catch (...) {}
		}

		if (!isLoaded) {
			strcpy(errorMsg, "No SDCARD found");
			return -1;
		}

		// Connect to card and get ATR
		unsigned char atr[128];
		unsigned int atrLength;
		reader->connect(1000, atr, &atrLength);

		reader->transmit(1000, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Cannot select applet");
			return -1;
		}

		reader->transmit(1000, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Cannot verify pin");
			return -1;
		}
		
	} catch (...) {
		strcpy(errorMsg, "Cannot connect to SDCARD");
		rv = -1;
	}
    
	return rv;
}


int ExcTokenConnect(const char drive) {
	int rv = 0;

	try {
		char drive_letter(drive);
		if(reader == NULL) {
			reader = new CardReader(drive_letter);
		}
		
		// Connect to card and get ATR
		unsigned char atr[128];
		unsigned int atrLength;
		reader->connect(1000, atr, &atrLength);

		reader->transmit(1000, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Cannot select applet");
			return -1;
		}

		reader->transmit(1000, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Cannot verify pin");
			return -1;
		}
		
	} catch(std::runtime_error& e) {
		strcpy(errorMsg, e.what());
		rv = -1;
	} catch (...) {
		strcpy(errorMsg, "Cannot connect to SDCARD. Unknown error.");
		rv = -1;
	}
    
	return rv;
}

int ExcTokenDisconnect() {
	int rv = 0;
	if (reader) {
		try {
			reader->disconnect(1000);
		} catch (...) {
			strcpy(errorMsg, "Cannot disconnect from SDCARD");
			rv = -1;
		}
		delete reader;
		reader = NULL;
	}
	return rv;
}

char* ExcTokenGetErrorText(int rv) {
	//int rv was used for getting errors from GlobalPlatform lib
	//not relevant to Swissbit lib. Remove later if won't be used in future.
	return errorMsg;
}

int ExcTokenGenerateKeyPair() {
	// Dummy function for now. The key auto-generated during applet installation
	return 0;
}

int ExcTokenGetPublicKey(unsigned char * key, unsigned int * size) {
	reader->transmit(1000, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU, &recvAPDULen);
	if (isTransmitError()) {
		strcpy(errorMsg, "Request key failed");
		return -1;
	}
	
	if ( ExcTokenGetResponse(key, size) != 0 ) {
		strcpy(errorMsg, "Cannot get correct public key data");
		return -1;
	}

	return 0;
}

int ExcTokenCreateChallengeSignature(const unsigned char * challenge, const unsigned int challengeLength, 
									  unsigned char * signature, unsigned int * signatureLength) {
	int rv;
	unsigned char* apdu;
	const unsigned char prefix[] = { 0xB0, 0x33, 0x00, 0x00 };

	apdu = new unsigned char(challengeLength + 5);
	memcpy(apdu, prefix, 4);
	apdu[4] = challengeLength;
	memcpy(apdu+5, challenge, challengeLength);

	reader->transmit(1000, apdu, challengeLength + 5, recvAPDU, &recvAPDULen);
	delete apdu;
	if (isTransmitError()) {
		strcpy(errorMsg, "Create Challenge Signature failed");
		return -1;
	}
	return ExcTokenGetResponse(signature, signatureLength);
}

int ExcTokenWriteFile(
		const unsigned char fileNumber,
		const unsigned char* data,
		const unsigned long dataLength) {
	int rv;
	unsigned char* apdu;
	long len;
	unsigned int chunkLen;
	int i = 0;
	unsigned int offset = 0; 

#if defined DEBUG_MODE
	unsigned int zzz;
#endif

	//instatiate applet file IO transfer functionality
	cmdCreateDataFile[2] = fileNumber;
	reader->transmit(1000, cmdCreateDataFile, sizeof(cmdCreateDataFile), recvAPDU, &recvAPDULen);

	cmdSelectDataFile[2] = fileNumber;
	reader->transmit(1000, cmdSelectDataFile, sizeof(cmdSelectDataFile), recvAPDU, &recvAPDULen);
	if (isTransmitError()) {
		strcpy(errorMsg, "Cannot create/open file");
		return -1;
	}
	
	//allocate 5 command bytes + 252 data chunk bytes
	apdu = new unsigned char(FILE_I_CHUNK_SIZE + 5);
	
	apdu[0] = 0xB0;
	apdu[1] = 0x24;
	apdu[2] = fileNumber;

	len = dataLength;
	//cut entire data to chunks and write to sdcard chunk by chunk
	while ( len > 0 ) {

		//chunk number byte
		apdu[3] = i;
		
		//if it is first chunk, write in first 2 bytes the size of entire data
		if (len == dataLength) {
			offset = 2;
			apdu[6] = dataLength;
			apdu[5] = dataLength >> 8;
		} else {
			offset = 0;
		}

		if (len > FILE_I_CHUNK_SIZE - offset) {
			chunkLen = FILE_I_CHUNK_SIZE - offset;
			apdu[4] = FILE_I_CHUNK_SIZE;
		} else {
			chunkLen = len;
			//length of the chunk data
			apdu[4] = chunkLen + offset;
		}
		//calculate length of remaining data for next loop (next chunk)
		len = len - FILE_I_CHUNK_SIZE + offset;
		//fill apdu buffer with the chunk data
		memcpy(apdu + 5 + offset, data + i*FILE_I_CHUNK_SIZE + offset - 2, chunkLen);

		//write chunk to sdcard
		reader->transmit(1000, apdu, chunkLen + 5 + offset, recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Write data failed");
			delete apdu;
			return -1;
		}
		
#if defined DEBUG_MODE
		printf("CHUNK %d\n", i);
		for (zzz = 0; zzz < chunkLen + 5 + offset; zzz++)
			printf("%02X ", apdu[zzz]);
		printf("\n");
#endif
		
		i++;//next chunk.. counter
	}

	delete apdu;
	return 0;
}

int ExcTokenReadFile(
		const unsigned char fileNumber,
		unsigned char* data,
		unsigned long* size) {
	int rv = 0;
	unsigned char* response;
	int chunkNumber = 0;
	int dataSize = 0; //remaining length of entire data needs to be read
	unsigned int chunkLength; //length of the chunk received from SDCARD
	unsigned int bufLength; //length of data from current chunk that will be copied to output buffer
	unsigned int offset = 0; 

#if defined DEBUG_MODE
	unsigned int zzz;
#endif

	//instatiate applet file IO transfer functionality
	cmdSelectDataFile[2] = fileNumber;
	reader->transmit(1000, cmdSelectDataFile, sizeof(cmdSelectDataFile), recvAPDU, &recvAPDULen);
	if (isTransmitError()) {
		strcpy(errorMsg, "Cannot open file");
		return -1;
	}

	response = new unsigned char(FILE_O_CHUNK_SIZE);

	while (chunkNumber >= 0) {
		cmdReadDataFile[2] = fileNumber;
		cmdReadDataFile[3] = chunkNumber;
		reader->transmit(1000, cmdReadDataFile, sizeof(cmdReadDataFile), recvAPDU, &recvAPDULen);
		if (isTransmitError()) {
			strcpy(errorMsg, "Cannot read file");
			break;
		}
		
		rv = ExcTokenGetResponse(response, &chunkLength);
		if ( rv != 0)
			break;

		// if it is first chunk, detect entire data size from first 2 bytes, 
		// and cut them from reading of first chunk response
		if (chunkNumber == 0) {
			offset = 2;//2 bytes to cut when memcpy to output buffer
			dataSize = response[1] | response[0] << 8;
			*size = dataSize;

#if defined DEBUG_MODE
			printf("dataSize: %d\n", dataSize);
			printf("chunkLength: %d\n", chunkLength);

			for (zzz = 0; zzz < chunkLength; zzz++)
				printf("%02X ", response[zzz]);
			printf("\n");
#endif
		} else {
			offset = 0;
		}

		//calculate how much data we will read from entire response
		if (dataSize < FILE_O_CHUNK_SIZE - offset) {
			bufLength = dataSize;
		} else {
			bufLength = FILE_O_CHUNK_SIZE - offset;
		}

		//copy chunk to output buffer
		memcpy(data + FILE_O_CHUNK_SIZE*chunkNumber - 2 + offset, recvAPDU + offset, bufLength);

		//calculate remaining data for next loop
		dataSize -= bufLength;
		if (dataSize <= 0) 
			chunkNumber = -1;  //all data were read, finish loop
		else
			chunkNumber++;
	}

#if defined DEBUG_MODE
	printf("DATA:\n");
	for (zzz = 0; zzz < FILE_O_CHUNK_SIZE - offset; zzz++)
		printf("%02X ", data[zzz]);
	printf("\n");
#endif

	delete response;
	return rv;
}


//////////////////////////////////////////////////////////////////////////////
// HELPERS
//////////////////////////////////////////////////////////////////////////////

bool isTransmitError() {
	if ( recvAPDULen < 2 || !(recvAPDU[recvAPDULen-2] == 0x90 && recvAPDU[recvAPDULen-1] == 0x00)) {
		printf("isTransmitError:\n");
		for (int idx = 0; idx < recvAPDULen; idx++)
			printf("%02X ", recvAPDU[idx]);
		printf("isTransmitError.done\n");
		return -1;
	}
	return 0;
}

int ExcTokenGetResponse(unsigned char * resp, unsigned int * size) {
	if ( recvAPDULen < 2 || !(recvAPDU[recvAPDULen-2] == 0x90 && recvAPDU[recvAPDULen-1] == 0x00)) {
		//if end of response not 9000, copy all response to output and return fail
		memcpy(resp, recvAPDU, recvAPDULen);
		*size = recvAPDULen;
		return -1;
	}

	//data OK, cut from output 9000 return code
	memcpy(resp, recvAPDU, recvAPDULen - 2);
	*size = recvAPDULen - 2;
	return 0;
}








