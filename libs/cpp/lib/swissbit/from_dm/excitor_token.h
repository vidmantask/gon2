
#ifndef EXC_TOKEN_H_
#define EXC_TOKEN_H_

#ifdef __cplusplus
extern "C"
{
#endif

	//service
	int ExcTokenConnectAny();
	int ExcTokenConnect(const char drive);
	int ExcTokenGenerateKeyPair();
	int ExcTokenGetPublicKey(
			unsigned char* key,
			unsigned int* size);
	int ExcTokenCreateChallengeSignature(
			const unsigned char* challenge,
			const unsigned int challengeLength,
			unsigned char* signature,
			unsigned int* signatureLength);

	int ExcTokenWriteFile(
			const unsigned char fileNumber,
			const unsigned char* data,
			const unsigned long dataLength);

	int ExcTokenReadFile(
			const unsigned char fileNumber,
			unsigned char* data,
			unsigned long* size);

	int ExcTokenDisconnect();

	//helpers
	char* ExcTokenGetErrorText(int rv);

#ifdef __cplusplus
}
#endif

#endif /* EXC_TOKEN_H_ */
