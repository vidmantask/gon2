#include <string.h>

//constants
#define ERROR_MSG_LENGTH 1024

//globals
static char errorMsg[ERROR_MSG_LENGTH];


//////////////////////////////////////////////////////////////////////////////
// API FUNCTIONS
//////////////////////////////////////////////////////////////////////////////
int ExcTokenConnectAny() {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}


int ExcTokenConnect(const char* path) {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

int ExcTokenDisconnect() {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

char* ExcTokenGetErrorText(int rv) {
	//int rv was used for getting errors from GlobalPlatform lib
	//not relevant to Swissbit lib. Remove later if won't be used in future.
	return errorMsg;
}

int ExcTokenGenerateKeyPair() {
	// Dummy function for now. The key auto-generated during applet installation
	return 0;
}

int ExcTokenGetPublicKey(unsigned char * key, unsigned int * size) {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

int ExcTokenCreateChallengeSignature(const unsigned char * challenge, const unsigned int challengeLength, 
									  unsigned char * signature, unsigned int * signatureLength) {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

int ExcTokenWriteFile(const unsigned char * data, const unsigned long dataLength, const unsigned int fileNumber) {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

int ExcTokenReadFile(const unsigned int fileNumber, unsigned char * data, unsigned long * size) {
	strcpy(errorMsg, "Platform not supported");
	return -1;
}

