/*! \file SWISSBIT_Key_unsupported.cxx
 */
#include <iostream>
#include <string>

#include <lib/swissbit/SWISSBIT_Key.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Swissbit;


SwissbitKey::SwissbitKey(
		const boost::filesystem::path& com_filename) :
		com_filename_(com_filename) {
}

SwissbitKey::~SwissbitKey() {
}

SwissbitKey::APtr SwissbitKey::create(
		const boost::filesystem::path& com_filename) {
	APtr instance(new SwissbitKey(com_filename));
	return instance;
}

void SwissbitKey::connect(void) {
	throw Exception_Swissbit("Not supported on this platform");
}

void SwissbitKey::disconnect(void) {
}

void SwissbitKey::generate_keypair(void) {
	throw Exception_Swissbit("Not supported on this platform");
}

Utility::DataBufferManaged::APtr SwissbitKey::get_public_key(void) {
	throw Exception_Swissbit("Not supported on this platform");
}

Utility::DataBufferManaged::APtr SwissbitKey::create_challenge_signature(
		const Utility::DataBufferManaged::APtr& challenge) {
		throw Exception_Swissbit("Not supported on this platform");
}

void SwissbitKey::write_to_file(
		const unsigned char fileNumber,
		const Utility::DataBufferManaged::APtr& data_buf) {

	throw Exception_Swissbit("Not supported on this platform");
}

Utility::DataBufferManaged::APtr SwissbitKey::read_from_file(
		const unsigned char fileNumber) {
	throw Exception_Swissbit("Not supported on this platform");
}

bool SwissbitKey::check_apdu_error(void) {
	return false;
}

bool SwissbitKey::get_apdu_data(Utility::DataBufferManaged::APtr& data) {
	return false;
}
