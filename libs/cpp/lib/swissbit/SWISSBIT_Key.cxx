/*! \file SWISSBIT_Key.cxx
 \brief This file contains the implementaion of the Hagi lib
 */
#include <iostream>
#include <string>

#include <lib/swissbit/SWISSBIT_Key.hxx>
#include <lib/cryptfacility/CF_Hash_CryptoPP.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>

#include "CardReader.h"

using namespace std;
using namespace Giritech;
using namespace Giritech::Swissbit;


//constants
//#define SWISSBIT_DEBUG 1
#define ERROR_MSG_LENGTH 1024
#define FILE_I_CHUNK_SIZE 252
#define FILE_O_CHUNK_SIZE 256

static unsigned char cmdSelectApplet[]   = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0xD2, 0x76, 0x00, 0x01, 0x62, 0x4D, 0x69, 0x6E, 0x69, 0x41, 0x75, 0x74, 0x68, 0x01 };
static unsigned char cmdVerifyPin[]      = { 0xB0, 0x10, 0x00, 0x00, 0x06, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
static unsigned char cmdRequestKey[]     = { 0xB0, 0x32, 0x00, 0x01, 0x00 };
static unsigned char cmdCreateDataFile[] = { 0xB0, 0x20, 0x00, 0x00, 0x02, 0x20, 0x00 };
static unsigned char cmdSelectDataFile[] = { 0xB0, 0x22, 0x00, 0x00, 0x00 };
static unsigned char cmdReadDataFile[]   = { 0xB0, 0x23, 0x00, 0x00, 0x00 };
static unsigned char cmdRequestVersion[] = { 0xB0, 0x40, 0x00, 0x00, 0x00 };
static unsigned char cmdGenerateKeys[]   = { 0xB0, 0x30, 0x00, 0x00, 0x00 };


SwissbitKey::SwissbitKey(
		const boost::filesystem::path& com_filename) :
		com_filename_(com_filename),
		recvAPDULen_(0) {
}

SwissbitKey::~SwissbitKey() {
	disconnect();
//	boost::this_thread::sleep(boost::posix_time::seconds(2));
}

SwissbitKey::APtr SwissbitKey::create(
		const boost::filesystem::path& com_filename) {
	APtr instance(new SwissbitKey(com_filename));
	return instance;
}

void SwissbitKey::connect(void) {
	try {
		card_reader_ = boost::shared_ptr<sfc::CardReader>(new sfc::CardReader(com_filename_.string().c_str()));
		if(card_reader_.get() == NULL) {
			throw Exception_Swissbit("Unable to connect to swissbit smartcard");
		}
		// Connect to card and get ATR
		unsigned char atr[FSI_MAX_ATR_SIZE];
		unsigned int atrLength(FSI_MAX_ATR_SIZE);
		card_reader_->connect(3000, atr, &atrLength);
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdSelectApplet, sizeof(cmdSelectApplet), recvAPDU_, &recvAPDULen_);

		if (check_apdu_error()) {
			throw Exception_Swissbit("Cannot select applet");
		}
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdVerifyPin, sizeof(cmdVerifyPin), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Cannot verify pin");
		}
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
		throw Exception_Swissbit("Cannot connect to SDCARD, unexpected error");
	}
}

void SwissbitKey::disconnect(void) {
	try {
		if(card_reader_.get() != NULL) {
			card_reader_->disconnect(0);
		}
	} catch (...) {
		// Ignore error
	}
}

void SwissbitKey::generate_keypair(void) {
	if(card_reader_.get() == NULL) {
		throw Exception_Swissbit("Unable to connect to swissbit smartcard");
	}
	try {
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdGenerateKeys, sizeof(cmdGenerateKeys), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to generate keypair");
		}
		return;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to generate keypair, unexpected error");
}

Utility::DataBufferManaged::APtr SwissbitKey::get_public_key(void) {
	if(card_reader_.get() == NULL) {
		throw Exception_Swissbit("Unable to connect to swissbit smartcard");
	}
	try {
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdRequestKey, sizeof(cmdRequestKey), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to get public key");
		}
		Utility::DataBufferManaged::APtr public_key;
		if(!get_apdu_data(public_key)) {
			throw Exception_Swissbit("Unable to get public key, invalid value");
		}
		return public_key;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to get public key, unexpected error");
}

Utility::DataBufferManaged::APtr SwissbitKey::get_version(void) {
	if(card_reader_.get() == NULL) {
		throw Exception_Swissbit("Unable to connect to swissbit smartcard");
	}
	try {
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdRequestVersion, sizeof(cmdRequestVersion), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to get version");
		}
		Utility::DataBufferManaged::APtr version;
		if(!get_apdu_data(version)) {
			throw Exception_Swissbit("Unable to get version, invalid value");
		}
		return version;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to get version, unexpected error");
}


Utility::DataBufferManaged::APtr SwissbitKey::create_challenge_signature(
		const Utility::DataBufferManaged::APtr& challenge) {
	try {
		CryptFacility::Hash_CryptoPP_SHA1::APtr hash_sha1(CryptFacility::Hash_CryptoPP_SHA1::create());
		Utility::DataBufferManaged::APtr challenge_hash(hash_sha1->calculateDigest(challenge));

//# SmartCard just performs rfc 2313 step 10.1.3 - we must hash and wrap its input with
// # DigestInfo from RFC 2313 "10.1.2 Data encoding"
// #   0   33: SEQUENCE {
// #   2    9:   SEQUENCE {
// #   4    5:     OBJECT IDENTIFIER sha1 (1 3 14 3 2 26)
// #  11    0:     NULL
// #         :     }
// #  13   20:   OCTET STRING 'xxxxxxxxxxxxxxxxxxxx'
// #         :   }
// challenge_hash = hashlib.sha1(challenge).digest()
// assert len(challenge_hash) == 0x14
// challenge_hash_pad = '\x30\x21\x30\x09\x06\x05\x2b\x0e\x03\x02\x1a\x05\x00\x04\x14' + challenge_hash

		const unsigned char prefix[] = {
				0xB0, 0x33, 0x00, 0x00, 0x00,
				0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14 };
		Utility::DataBufferManaged::APtr apdu(Utility::DataBufferManaged::create((const char*)prefix, (const long)sizeof(prefix)));
		apdu->append(challenge_hash);
		apdu->data()[4] = 15 + (unsigned char)challenge_hash->getSize();
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, apdu->data(), apdu->getSize(), recvAPDU_, &recvAPDULen_);

		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to create challenge for signature");
		}
		Utility::DataBufferManaged::APtr signature;
		if(!get_apdu_data(signature)) {
			throw Exception_Swissbit("Unable to create challenge for signature, invalid signature");
		}
		return signature;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to create challenge for signature, unexpected error");
}

void SwissbitKey::write_to_file(
		const unsigned char fileNumber,
		const Utility::DataBufferManaged::APtr& data_buf) {

	try {
		const unsigned char* data(data_buf->data());
		const unsigned long dataLength(data_buf->getSize());

		unsigned int chunkLen;
		unsigned int offset = 0;

		if(card_reader_.get() == NULL) {
			throw Exception_Swissbit("Unable to connect to swissbit smartcard");
		}

		//instatiate applet file IO transfer functionality
		cmdCreateDataFile[2] = fileNumber;
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdCreateDataFile, sizeof(cmdCreateDataFile), recvAPDU_, &recvAPDULen_);

		cmdSelectDataFile[2] = fileNumber;
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdSelectDataFile, sizeof(cmdSelectDataFile), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to create/open file");
		}

		//allocate 5 command bytes + 252 data chunk bytes
		unsigned char apdu[FILE_I_CHUNK_SIZE + 5];

		apdu[0] = 0xB0;
		apdu[1] = 0x24;
		apdu[2] = fileNumber;

		long len(dataLength);
		int i(0);

		//cut entire data to chunks and write to sdcard chunk by chunk
		while ( len > 0 ) {

			//chunk number byte
			apdu[3] = i;

			//if it is first chunk, write in first 2 bytes the size of entire data
			if (len == dataLength) {
				offset = 2;
				apdu[6] = dataLength;
				apdu[5] = dataLength >> 8;
			} else {
				offset = 0;
			}

			if (len > FILE_I_CHUNK_SIZE - offset) {
				chunkLen = FILE_I_CHUNK_SIZE - offset;
				apdu[4] = FILE_I_CHUNK_SIZE;
			} else {
				chunkLen = len;
				//length of the chunk data
				apdu[4] = chunkLen + offset;
			}
			//calculate length of remaining data for next loop (next chunk)
			len = len - FILE_I_CHUNK_SIZE + offset;
			//fill apdu buffer with the chunk data
			memcpy(apdu + 5 + offset, data + i*FILE_I_CHUNK_SIZE + offset - 2, chunkLen);

			//write chunk to sdcard
			recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
			card_reader_->transmit(SWISSBIT_TIMEOUT, apdu, chunkLen + 5 + offset, recvAPDU_, &recvAPDULen_);
			if (check_apdu_error()) {
				throw Exception_Swissbit("Unable to write data");
			}
			i++;//next chunk.. counter
		}
		return;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to write data, unexpected error");
}

Utility::DataBufferManaged::APtr SwissbitKey::read_from_file(
		const unsigned char fileNumber) {
	try {
		Utility::DataBufferManaged::APtr data(Utility::DataBufferManaged::create_empty(0));

		int chunkNumber = 0;
		int dataSize = 0;           //remaining length of entire data needs to be read
		unsigned int bufLength = 0; //length of data from current chunk that will be copied to output buffer
		unsigned int offset = 0;

		if(card_reader_.get() == NULL) {
			throw Exception_Swissbit("Unable to connect to swissbit smartcard");
		}

		//instatiate applet file IO transfer functionality
		cmdSelectDataFile[2] = fileNumber;
		recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
		card_reader_->transmit(SWISSBIT_TIMEOUT, cmdSelectDataFile, sizeof(cmdSelectDataFile), recvAPDU_, &recvAPDULen_);
		if (check_apdu_error()) {
			throw Exception_Swissbit("Unable to open file");
		}

		while (chunkNumber >= 0) {
			cmdReadDataFile[2] = fileNumber;
			cmdReadDataFile[3] = chunkNumber;
			recvAPDULen_ = FSI_MAX_RESPONSE_SIZE;
			card_reader_->transmit(SWISSBIT_TIMEOUT, cmdReadDataFile, sizeof(cmdReadDataFile), recvAPDU_, &recvAPDULen_);
			if(check_apdu_error()) {
				break;
			}

			// if it is first chunk, detect entire data size from first 2 bytes,
			// and cut them from reading of first chunk response
			if (chunkNumber == 0) {
				offset = 2;//2 bytes to cut when memcpy to output buffer
				dataSize = recvAPDU_[1] | recvAPDU_[0] << 8;
			} else {
				offset = 0;
			}

			//calculate how much data we will read from entire response
			if (dataSize < FILE_O_CHUNK_SIZE - offset) {
				bufLength = dataSize;
			} else {
				bufLength = FILE_O_CHUNK_SIZE - offset;
			}

			//copy chunk to output buffer
			data->append(Utility::DataBufferManaged::create(recvAPDU_ + offset, bufLength));

			//calculate remaining data for next loop
			dataSize -= bufLength;
			if (dataSize <= 0)
				chunkNumber = -1;  //all data were read, finish loop
			else
				chunkNumber++;
		}
		return data;
	} catch(sfc::CardReaderException& e) {
		throw Exception_Swissbit(e.what());
	} catch(std::runtime_error& e) {
		throw Exception_Swissbit(e.what());
	} catch (...) {
	}
	throw Exception_Swissbit("Unable to read data, unexpected error");
}

bool SwissbitKey::check_apdu_error(void) {
	if ( (recvAPDULen_ < 2) || !((recvAPDU_[recvAPDULen_-2] == 0x90) && (recvAPDU_[recvAPDULen_-1] == 0x00)) ) {
#ifdef SWISSBIT_DEBUG
		printf("apdu_error:\n");
		for (unsigned int idx = 0; idx < recvAPDULen_; idx++)
			printf("%02X ", recvAPDU_[idx]);
		printf("apdu_error.done\n");
#endif
		return true;
	}
	return false;
}

bool SwissbitKey::get_apdu_data(Utility::DataBufferManaged::APtr& data) {
	if(check_apdu_error()) {
		Utility::DataBufferManaged::APtr datachunk(Utility::DataBufferManaged::create(recvAPDU_, recvAPDULen_));
		data = datachunk;
		return false;
	}
	Utility::DataBufferManaged::APtr datachunk(Utility::DataBufferManaged::create(recvAPDU_, recvAPDULen_-2));
	data = datachunk;
	return true;
}
