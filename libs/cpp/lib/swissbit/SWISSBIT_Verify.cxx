/*! \file SWISSBIT_Verify.cxx
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/optional.hpp>
#include <boost/algorithm/string.hpp>

#include "lib/swissbit/SWISSBIT_Key.hxx"
#include <lib/cryptfacility/CF_ALL.hxx>
#include "CardReader.h"


#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
static CryptoPP::PNew s_pNew = NULL;
static CryptoPP::PDelete s_pDelete = NULL;

extern "C" __declspec(dllexport) void __cdecl SetNewAndDeleteFromCryptoPP(CryptoPP::PNew pNew, CryptoPP::PDelete pDelete,  CryptoPP::PSetNewHandler pSetNewHandler) {
    s_pNew = pNew;
    s_pDelete = pDelete;
}

void * __cdecl operator new (size_t size) {
    return s_pNew(size);
}

void __cdecl operator delete (void * p) {
    s_pDelete(p);
}
#endif



using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;


Utility::DataBufferManaged::APtr get_version_with_retry(
		const Swissbit::SwissbitKey::APtr& key,
		const int retry_count) {
	if(retry_count <= 0) {
		return Utility::DataBufferManaged::create("error");
	}

	try {
		key->connect();
		return key->get_version();
	}
	catch(Swissbit::SwissbitKey::Exception_Swissbit& e) {
		cout << "ERROR: " << e.what() << endl;
	}
	return get_version_with_retry(key, retry_count-1);
}

bool verify(
		const std::string& mount_point,
		const std::string& token_type) {
	Swissbit::SwissbitKey::APtr key(Swissbit::SwissbitKey::create(mount_point.c_str()));

  std::string expected_version = "";
  if(boost::equals(token_type, "swissbit")) {
    expected_version = "0002";
  }
  else if(boost::equals(token_type, "swissbit_2")) {
    expected_version = "0003";
  }
  else if(boost::equals(token_type, "swissbit_pe")) {
    expected_version = "0003";
  }

	Utility::DataBufferManaged::APtr version(get_version_with_retry(key, 5));
	if(boost::equals(expected_version, version->toString())) {
		return true;
	}
	cout << "ERROR invalid version, found version " << version->toString() << " expected version " << expected_version << endl;
	return false;
}

int main(int argc, char* argv[]) {
	if(argc != 3) {
		cout << "Invalid number of arguments" << endl;
		return -1;
	}

	std::string mount_point(argv[1]);
  std::string token_type(argv[2]);
  cout << "mount_point :" << mount_point << endl;
  cout << "token_type  :" << token_type << endl;

	if(verify(mount_point, token_type)) {
		cout << "Verification ok" << endl;
		return 0;
	}
	cout << "Verification failed" << endl;
	return -1;
}
