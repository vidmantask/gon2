/*! \file HAGI_Key.cxx
 \brief This file contains the implementaion of the Hagi lib
 */
#include <iostream>
#include <string>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/python.hpp>

#include <lib/hagi/HAGI_Key.hxx>
#include <lib/Hagi/HAGI_Authentication.hxx>
#include <lib/utility/UY_Convert.hxx>


#include "HscCDW.h" // Hagiware G2 support
#include "HscUDB.h" // Hagiware G2 support
#include "HscEXD.h" // Hagiware G3 support
#include "HscEXDG4.h" // Hagiware G4 support

#include "zlib/zlib.h"

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Hagi;
using namespace Giritech::CryptFacility;

LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/*
 ------------------------------------------------------------------
 HagiKeyMessageWindow
 ------------------------------------------------------------------
 */
class HagiKeyMessageWindow {
public:
    HagiKeyMessageWindow(HagiKey::CreateAndBurnISOCallback* callback_ptr, const int progress_sections) :
        callback_ptr_(callback_ptr), progress_sections_(progress_sections), error_(false), do_run_(true),  started_(false), hwnd_window_(NULL) {
    }
    ~HagiKeyMessageWindow(void) {
        callback_ptr_ = NULL;
    }

    void reset(void) {
        callback_ptr_ = NULL;
    }

    HWND get_hwnd() {
        if (!error_) {
            return hwnd_window_;
        }
        return NULL;
    }

    void run() {
        Py_Initialize();
        PyEval_InitThreads();

        HINSTANCE h_instance = GetModuleHandle(NULL);
        WNDCLASS wc = { 0 };
        wc.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
        wc.hCursor = LoadCursor(h_instance, IDC_ARROW);
        wc.hIcon = LoadIcon(h_instance, IDI_APPLICATION);
        wc.hInstance = h_instance;

        wc.lpfnWndProc = StaticWndProc;
        wc.lpszClassName = TEXT("message_window");
        wc.style = CS_HREDRAW | CS_VREDRAW;

        if (!RegisterClass(&wc)) {
            error_ = true;
        }
        if (!error_) {
            hwnd_window_ = CreateWindow(TEXT("message_window"), TEXT("The window"), WS_OVERLAPPEDWINDOW, 520, 20, 300, 300, NULL, NULL, h_instance, this);
        }
        started_ = true;

#pragma region _REGION_C - message loop
        MSG msg;
        while (do_run_ && GetMessage(&msg, hwnd_window_, 0, 0)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
#pragma endregion

        if (hwnd_window_ != NULL) {
            DestroyWindow(hwnd_window_);
        }
        if (!error_) {
            UnregisterClass(TEXT("message_window"), h_instance);
        }
    }

    bool is_started(void) {
        return started_;
    }

    void stop() {
        do_run_ = false;
        if (hwnd_window_ != NULL) {
            PostMessage(hwnd_window_, WM_CLOSE, 0, 0);
        }
    }

    LRESULT WndProc(UINT uMsg, WPARAM section_progress, LPARAM section) {
        if (uMsg == WM_APP) {
            if (callback_ptr_ != NULL) {
                int progress = (int) ((100/progress_sections_) * section) + ((100.0/progress_sections_/100.0) * section_progress);
                if  (progress < 0) {
                    progress = 0;
                }
                if  (progress > 100) {
                    progress = 100;
                }
                callback_ptr_->progress("Burning", progress);
            }
        }
        return DefWindowProc(hwnd_window_, uMsg, section_progress, section);
    }

private:
    HagiKey::CreateAndBurnISOCallback* callback_ptr_;
    int progress_sections_;
    bool error_;
    bool do_run_;
    HWND hwnd_window_;
    bool started_;
};

LRESULT CALLBACK StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
    HagiKeyMessageWindow* pParent;
    if (uMsg == WM_CREATE) {
        pParent = (HagiKeyMessageWindow*) ((LPCREATESTRUCT) lParam)->lpCreateParams;
        SetWindowLongPtr(hWnd, GWL_USERDATA, (LONG_PTR) pParent);
    } else {
        pParent = (HagiKeyMessageWindow*) GetWindowLongPtr(hWnd, GWL_USERDATA);
        if (!pParent)
            return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
    return pParent->WndProc(uMsg, wParam, lParam);
}

/*
 ------------------------------------------------------------------
 HagiKey
 ------------------------------------------------------------------
 */
boost::filesystem::path get_gon_hagi_folder(const boost::filesystem::path& root) {
    return root / "gon_client" / "gon_hagi";
}
boost::filesystem::path get_gon_hagi_folder(const std::string& root) {
    return get_gon_hagi_folder(boost::filesystem::path(root + ":/" ));
}

HagiKey::HagiKey(void) :
    dll_is_open_(false), hagi_type_(Hagi_Type_Unknown) {
}

HagiKey::~HagiKey(void) {
}

HagiKey::APtr HagiKey::create(void) {
    return HagiKey::APtr(new HagiKey());
}

void HagiKey::open_dll(void) {
    int rc = 0;
    const char* hagi_dll_secret;
    char unique_id_buffer[100];
    memset(unique_id_buffer, 0, 100);

    if (!dll_is_open_) {

        // Trying G4
        try {
            hagi_dll_secret = generate_hagi_dll_secret();
            rc = HscEXDG4_OpenDll(hagi_dll_secret);
            clean_hagi_dll_secret();
            if (rc == HscEXDG4_SUCCESS) {
                rc = HscEXDG4_ReadUniqueID((UCHAR*) unique_id_buffer);
                if (rc == HscEXDG4_SUCCESS) {
                    dll_is_open_ = true;
                    hagi_type_ = Hagi_Type_G4;
                    unique_id_ = DataBufferManaged::create(unique_id_buffer);
                    return;
                }
            }
        }
        catch (const Exception_Hagi& e) {
        }
        hagi_dll_secret = generate_hagi_dll_secret();
        HscEXDG4_CloseDll(hagi_dll_secret);
        clean_hagi_dll_secret();


        // Trying G3
        try {
            hagi_dll_secret = generate_hagi_dll_secret();
            rc = HscEXD_OpenDll(hagi_dll_secret);
            clean_hagi_dll_secret();
            if (rc == HscEXD_SUCCESS) {
                rc = HscEXD_ReadUniqueID((UCHAR*) unique_id_buffer);
                if (rc == HscEXD_SUCCESS) {
                    dll_is_open_ = true;
                    hagi_type_ = Hagi_Type_G3;
                    unique_id_ = DataBufferManaged::create(unique_id_buffer);
                    return;
                }
            }
        }
        catch (const Exception_Hagi& e) {
        }
        hagi_dll_secret = generate_hagi_dll_secret();
        HscEXD_CloseDll(hagi_dll_secret);
        clean_hagi_dll_secret();

        // Trying G2
        hagi_dll_secret = generate_hagi_dll_secret();
        rc = HscUDB_OpenDLL(hagi_dll_secret);
        clean_hagi_dll_secret();
        if (rc == HscUDB_SUCCESS) {
            rc = HscUDB_GetUniqueID((UCHAR*) unique_id_buffer);
            if (rc == HscUDB_SUCCESS) {
                dll_is_open_ = true;
                hagi_type_ = Hagi_Type_G2;
                unique_id_ = DataBufferManaged::create(unique_id_buffer);
                return;
            }
        }
        HscUDB_CloseDLL();
        throw Exception_Hagi("open_dll", error_to_string(rc));
    }
}
void HagiKey::open_dll_g2_cdw(void) {
    const char* hagi_dll_secret;
    DataBufferManaged::APtr unique_id_buffer(DataBufferManaged::create(17, 0));

    hagi_dll_secret = generate_hagi_dll_secret();
    int rc = HscCDW_OpenDLL(hagi_dll_secret);
    clean_hagi_dll_secret();
    if (rc == HscCDW_COMPLETE) {
        return;
    }
    throw Exception_Hagi("open_dll_g2_cdw", error_to_string(rc));
}

void HagiKey::close_dll(void) {
    int rc = 0;
    if (dll_is_open_) {
        const char* hagi_dll_secret = generate_hagi_dll_secret();
        if (hagi_type_ == Hagi_Type_G2) {
            rc = HscUDB_CloseDLL();
            clean_hagi_dll_secret();
            if (rc != HscUDB_SUCCESS) {
                throw Exception_Hagi("close_dll", error_to_string(rc));
            }
        } else if (hagi_type_ == Hagi_Type_G3) {
            rc = HscEXD_CloseDll(hagi_dll_secret);
            clean_hagi_dll_secret();
            if (rc != HscEXD_SUCCESS) {
                throw Exception_Hagi("close_dll", error_to_string(rc));
            }
        } else if (hagi_type_ == Hagi_Type_G4) {
            rc = HscEXDG4_CloseDll(hagi_dll_secret);
            clean_hagi_dll_secret();
            if (rc != HscEXDG4_SUCCESS) {
                throw Exception_Hagi("close_dll", error_to_string(rc));
            }
        }
        dll_is_open_ = false;
    }
}
void HagiKey::close_dll_g2_cdw(void) {
    int rc = 0;
    rc = HscCDW_CloseDLL();
    if (rc != HscCDW_COMPLETE) {
        throw Exception_Hagi("close_dll_g2_cdw", error_to_string(rc));
    }
}
std::string HagiKey::error_to_string(const int& rc) const {
    if (hagi_type_ == Hagi_Type_G2) {
        switch (rc) {
        case HscUDB_SUCCESS:
            return "HscUDB_SUCCESS";
        case HscUDB_ERROR_INVALID_COMPANY_CODE:
            return "HscUDB_ERROR_INVALID_COMPANY_CODE";
        case HscUDB_ERROR_INVALID_ARGUMENT:
            return "HscUDB_ERROR_INVALID_ARGUMENT";
        case HscUDB_ERROR_DEVICE_NOT_FOUND:
            return "HscUDB_ERROR_DEVICE_NOT_FOUND";
        case HscUDB_ERROR_MANY_DEVICE_FOUND:
            return "HscUDB_ERROR_MANY_DEVICE_FOUND";
        case HscUDB_ERROR_DLL_IS_NOT_ENABLE:
            return "HscUDB_ERROR_DLL_IS_NOT_ENABLE";
        case HscUDB_ERROR_COMMAND_FAILED:
            return "HscUDB_ERROR_COMMAND_FAILED";
        case HscUDB_ERROR_MUST_NEED_ADMINISTRATOR:
            return "HscUDB_ERROR_MUST_NEED_ADMINISTRATOR";
        }
    } else if (hagi_type_ == Hagi_Type_G3) {
        switch (rc) {
        case HscEXD_SUCCESS:
            return "HscEXD_SUCCESS";
        case HscEXD_ERROR_INVALID_ARGUMENT:
            return "HscEXD_ERROR_INVALID_ARGUMENT";
        case HscEXD_ERROR_INVALID_COMPANY_CODE:
            return "HscEXD_ERROR_INVALID_COMPANY_CODE";
        case HscEXD_ERROR_DLL_IS_NOT_ENABLE:
            return "HscEXD_ERROR_DLL_IS_NOT_ENABLE";
        case HscEXD_ERROR_DEVICE_NOT_FOUND:
            return "HscEXD_ERROR_DEVICE_NOT_FOUND";
        case HscEXD_ERROR_MANY_DEVICE_FOUND:
            return "HscEXD_ERROR_MANY_DEVICE_FOUND";
        case HscEXD_ERROR_NOT_SUPPORT_FUNCTION:
            return "HscEXD_ERROR_NOT_SUPPORT_FUNCTION";
        case HscEXD_ERROR_DRIVE_NOT_FOUND:
            return "HscEXD_ERROR_DRIVE_NOT_FOUND";
        case HscEXD_ERROR_DEVICE_NOT_READY:
            return "HscEXD_ERROR_DEVICE_NOT_READY";
        case HscEXD_ERROR_COMMAND_FAILED:
            return "HscEXD_ERROR_COMMAND_FAILED";
        case HscEXD_ERROR_ISO_FILE_NOT_FOUND:
            return "HscEXD_ERROR_ISO_FILE_NOT_FOUND";
        case HscEXD_ERROR_HIDDEN_FILE_NOT_FOUND:
            return "HscEXD_ERROR_HIDDEN_FILE_NOT_FOUND";
        case HscEXD_ERROR_SINGLE_DRIVE_MODE:
            return "HscEXD_ERROR_SINGLE_DRIVE_MODE";
        case HscEXD_ERROR_DEVICE_IS_LOCKED:
            return "HscEXD_ERROR_DEVICE_IS_LOCKED";
        case HscEXD_ERROR_EXCEED_DEVICE_CAPACITY:
            return "HscEXD_ERROR_EXCEED_DEVICE_CAPACITY";
        case HscEXD_ERROR_SOURCE_FOLDER_NOT_FOUND:
            return "HscEXD_ERROR_SOURCE_FOLDER_NOT_FOUND";
        case HscEXD_ERROR_FMTCNVEXE_NOT_FOUND:
            return "HscEXD_ERROR_FMTCNVEXE_NOT_FOUND";
        case HscEXD_ERROR_EXCEED_MAXPATH:
            return "HscEXD_ERROR_EXCEED_MAXPATH";
        case HscEXD_ERROR_INVALID_FILENAME:
            return "HscEXD_ERROR_INVALID_FILENAME";
        case HscEXD_ERROR_INVALID_VOLUMELABEL:
            return "HscEXD_ERROR_INVALID_VOLUMELABEL";
        case HscEXD_ERROR_OUTPUT_FOLDER_NOT_FOUND:
            return "HscEXD_ERROR_OUTPUT_FOLDER_NOT_FOUND";
        case HscEXD_ERROR_CREATE_ISO_FILE:
            return "HscEXD_ERROR_CREATE_ISO_FILE";
        case HscEXD_ERROR_LOCK_FUNCTION_NOT_ENABLE:
            return "HscEXD_ERROR_LOCK_FUNCTION_NOT_ENABLE";
        case HscEXD_ERROR_PASSWORD_MISS_COUNT_OVER:
            return "HscEXD_ERROR_PASSWORD_MISS_COUNT_OVER";
        case HscEXD_ERROR_FLUSH_DRIVE:
            return "HscEXD_ERROR_FLUSH_DRIVE";
        case HscEXD_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED:
            return "HscEXD_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED";
        case HscEXD_ERROR_NO_INITIAL_PASSWORD_SET:
            return "HscEXD_ERROR_NO_INITIAL_PASSWORD_SET";
        case HscEXD_ERROR_DEVICE_IS_ALREADY_UNLOCKED:
            return "HscEXD_ERROR_DEVICE_IS_ALREADY_UNLOCKED";
        case HscEXD_ERROR_REMOVABLE_DISK_DOSFORMAT:
            return "HscEXD_ERROR_REMOVABLE_DISK_DOSFORMAT";
        case HscEXD_ERROR_LOCK_COMMAND_FAILED:
            return "HscEXD_ERROR_LOCK_COMMAND_FAILED";
        }
    } else {
        switch (rc) {
        case HscEXDG4_SUCCESS:
            return "HscEXDG4_SUCCESS";
        case HscEXDG4_ERROR_INVALID_ARGUMENT:
            return "HscEXDG4_ERROR_INVALID_ARGUMENT";
        case HscEXDG4_ERROR_INVALID_COMPANY_CODE:
            return "HscEXDG4_ERROR_INVALID_COMPANY_CODE";
        case HscEXDG4_ERROR_DLL_IS_NOT_ENABLE:
            return "HscEXDG4_ERROR_DLL_IS_NOT_ENABLE";
        case HscEXDG4_ERROR_DEVICE_NOT_FOUND:
            return "HscEXDG4_ERROR_DEVICE_NOT_FOUND";
        case HscEXDG4_ERROR_MANY_DEVICE_FOUND:
            return "HscEXDG4_ERROR_MANY_DEVICE_FOUND";
        case HscEXDG4_ERROR_NOT_SUPPORT_FUNCTION:
            return "HscEXDG4_ERROR_NOT_SUPPORT_FUNCTION";
        case HscEXDG4_ERROR_DRIVE_NOT_FOUND:
            return "HscEXDG4_ERROR_DRIVE_NOT_FOUND";
        case HscEXDG4_ERROR_DEVICE_NOT_READY:
            return "HscEXDG4_ERROR_DEVICE_NOT_READY";
        case HscEXDG4_ERROR_COMMAND_FAILED:
            return "HscEXDG4_ERROR_COMMAND_FAILED";
        case HscEXDG4_ERROR_ISO_FILE_NOT_FOUND:
            return "HscEXDG4_ERROR_ISO_FILE_NOT_FOUND";
        case HscEXDG4_ERROR_HIDDEN_FILE_NOT_FOUND:
            return "HscEXDG4_ERROR_HIDDEN_FILE_NOT_FOUND";
        case HscEXDG4_ERROR_SINGLE_DRIVE_MODE:
            return "HscEXDG4_ERROR_SINGLE_DRIVE_MODE";
        case HscEXDG4_ERROR_DEVICE_IS_LOCKED:
            return "HscEXDG4_ERROR_DEVICE_IS_LOCKED";
        case HscEXDG4_ERROR_EXCEED_DEVICE_CAPACITY:
            return "HscEXDG4_ERROR_EXCEED_DEVICE_CAPACITY";
        case HscEXDG4_ERROR_SOURCE_FOLDER_NOT_FOUND:
            return "HscEXDG4_ERROR_SOURCE_FOLDER_NOT_FOUND";
        case HscEXDG4_ERROR_FMTCNVEXE_NOT_FOUND:
            return "HscEXDG4_ERROR_FMTCNVEXE_NOT_FOUND";
        case HscEXDG4_ERROR_EXCEED_MAXPATH:
            return "HscEXDG4_ERROR_EXCEED_MAXPATH";
        case HscEXDG4_ERROR_INVALID_FILENAME:
            return "HscEXDG4_ERROR_INVALID_FILENAME";
        case HscEXDG4_ERROR_INVALID_VOLUMELABEL:
            return "HscEXDG4_ERROR_INVALID_VOLUMELABEL";
        case HscEXDG4_ERROR_OUTPUT_FOLDER_NOT_FOUND:
            return "HscEXDG4_ERROR_OUTPUT_FOLDER_NOT_FOUND";
        case HscEXDG4_ERROR_CREATE_ISO_FILE:
            return "HscEXDG4_ERROR_CREATE_ISO_FILE";
        case HscEXDG4_ERROR_LOCK_FUNCTION_NOT_ENABLE:
            return "HscEXDG4_ERROR_LOCK_FUNCTION_NOT_ENABLE";
        case HscEXDG4_ERROR_PASSWORD_MISS_COUNT_OVER:
            return "HscEXDG4_ERROR_PASSWORD_MISS_COUNT_OVER";
        case HscEXDG4_ERROR_FLUSH_DRIVE:
            return "HscEXDG4_ERROR_FLUSH_DRIVE";
        case HscEXDG4_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED:
            return "HscEXDG4_ERROR_INITIALPASS_IS_ALREADY_CONFIGURED";
        case HscEXDG4_ERROR_NO_INITIAL_PASSWORD_SET:
            return "HscEXDG4_ERROR_NO_INITIAL_PASSWORD_SET";
        case HscEXDG4_ERROR_DEVICE_IS_ALREADY_UNLOCKED:
            return "HscEXDG4_ERROR_DEVICE_IS_ALREADY_UNLOCKED";
        case HscEXDG4_ERROR_REMOVABLE_DISK_DOSFORMAT:
            return "HscEXDG4_ERROR_REMOVABLE_DISK_DOSFORMAT";
        case HscEXDG4_ERROR_LOCK_COMMAND_FAILED:
            return "HscEXDG4_ERROR_LOCK_COMMAND_FAILED";
        }
    }
    stringstream ss;
    ss << "No type, error: " << ios::hex << rc;
    return ss.str();
}

DataBufferManaged::APtr HagiKey::get_unique_id(void) {
    open_dll();
    return unique_id_;
}

std::string HagiKey::get_ro_path(void) {
    return get_ro_path_(10);
}

std::string HagiKey::get_ro_path_(int counter) {
    int rc = 0;
    char ro_path_buffer[10 + 1];
    memset(ro_path_buffer, 0, sizeof(ro_path_buffer)); // Might be removed by optimization

    open_dll();
    if (hagi_type_ == Hagi_Type_G2) {
        rc = HscUDB_GetRomDriveLetter(ro_path_buffer);
    } else if (hagi_type_ == Hagi_Type_G3) {
        rc = HscEXD_GetRomDriveLetter(ro_path_buffer);
    } else if (hagi_type_ == Hagi_Type_G4) {
        rc = HscEXDG4_GetRomDriveLetter(ro_path_buffer);
    }
    if ((rc == HscEXD_ERROR_DEVICE_NOT_READY) && (counter > 0)) {
        boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(1));
        return get_ro_path_(counter--);
    }
    if (rc != HscEXD_SUCCESS) {
        throw Exception_Hagi("get_ro_path_", counter, error_to_string(rc));
    }

    stringstream ss;
    ss << ro_path_buffer;
    return ss.str();
}

std::string HagiKey::get_rw_path(void) {
    return get_rw_path_(10);
}

std::string HagiKey::get_rw_path_(int counter) {
    int rc = 0;
    char rw_path_buffer[10 + 1];
    memset(rw_path_buffer, 0, sizeof(rw_path_buffer)); // Might be removed by optimization

    open_dll();
    if (hagi_type_ == Hagi_Type_G2) {
        rc = HscUDB_GetRemovableDriveLetter(rw_path_buffer);
    } else if (hagi_type_ == Hagi_Type_G3) {
        rc = HscEXD_GetRemovableDriveLetter(rw_path_buffer);
    } else if (hagi_type_ == Hagi_Type_G4) {
        rc = HscEXDG4_GetRemovableDriveLetter(rw_path_buffer);
    }
    if ((rc == HscEXD_ERROR_DEVICE_NOT_READY) && (counter > 0)) {
        boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(1));
        return get_rw_path_(counter--);
    }
    if (rc != HscEXD_SUCCESS) {
        throw Exception_Hagi("get_rw_path_", counter, error_to_string(rc));
    }

    stringstream ss;
    ss << rw_path_buffer;
    return ss.str();
}


string read_file_content(const boost::filesystem::path& filename_path) {
    try {
        if(! boost::filesystem::exists(filename_path)) {
            return "";
        }
    }
    catch(...) {
        return "";
    }
    char buffer_char;
    stringstream save_file_stream;
    ifstream myfile(filename_path.string().c_str());
    if(myfile.is_open()) {
      while(myfile.get(buffer_char)) {
        save_file_stream << buffer_char;
      }
      myfile.close();
    }
    return save_file_stream.str();
}
void write_file_content(const boost::filesystem::path& filename_path, const string& content) {
    ofstream myfile(filename_path.string().c_str());
    myfile << content;
    myfile.close();
}


void HagiKey::create_and_burn_iso(const string& source_root,
                                  const string& temp_root,
                                  const string& volume_label,
                                  const string& knownsecret,
                                  const string& servers,
                                  const bool do_generate_keypair,
                                  HagiKey::CreateAndBurnISOCallback* callback_ptr) {

    HagiKey::create_and_burn_iso_(source_root, temp_root, volume_label, do_generate_keypair, false, knownsecret, servers, callback_ptr);
}

void HagiKey::create_and_burn_iso(const string& source_root,
                                  const string& temp_root,
                                  const string& volume_label,
                                  HagiKey::CreateAndBurnISOCallback* callback_ptr) {
    HagiKey::create_and_burn_iso_(source_root, temp_root, volume_label, false, true, "", "", callback_ptr);
}

void HagiKey::create_and_burn_iso_(const string& source_root,
                                  const string& temp_root,
                                  const string& volume_label,
                                  const bool do_generate_keypair,
                                  const bool keep_knownsecret_and_servers,
                                  const string& knownsecret,
                                  const string& servers,
                                  HagiKey::CreateAndBurnISOCallback* callback_ptr) {

    string knownsecret_to_be_used(knownsecret);
    string servers_to_be_used(servers);

    if(keep_knownsecret_and_servers) {
        std::pair<std::string, std::string> current_knownsecret_and_servers(get_knownsecret_and_servers());
        knownsecret_to_be_used = current_knownsecret_and_servers.first;
        servers_to_be_used = current_knownsecret_and_servers.second;
    }
    boost::filesystem::path gon_hagi_path(get_gon_hagi_folder(boost::filesystem::path(source_root)));
    boost::filesystem::remove_all(gon_hagi_path);
    boost::filesystem::create_directories(gon_hagi_path);
    boost::filesystem::path knowsecret_filename(gon_hagi_path / "gon_client.ks");
    write_file_content(knowsecret_filename, knownsecret_to_be_used);
    boost::filesystem::path servers_filename(gon_hagi_path / "servers");
    write_file_content(servers_filename, servers_to_be_used);

    if(do_generate_keypair) {
        generate_keypair();
    }

    try {
        boost::filesystem::path public_filename(gon_hagi_path / "public");
        write_file_content(public_filename, get_public_key()->encodeHex()->toString());
    }
    catch(...) {
    }

    string iso_image_filename("hagi_burn.iso");
    boost::filesystem::path temp_root_path(temp_root);
    boost::filesystem::path iso_image_filename_path(temp_root_path / iso_image_filename);
    if (callback_ptr!=NULL) {
        callback_ptr->progress("Creating ISO", 0);
    }

    open_dll();
    int rc = HscEXDG4_CreateIsoImage(source_root.c_str(), volume_label.c_str(), iso_image_filename_path.string().c_str());
    close_dll();
    if (rc != HscEXDG4_SUCCESS) {
        throw Exception_Hagi(error_to_string(rc));
    }


    int progress_sections = 1;
    if (hagi_type_ == Hagi_Type_G2) {
        progress_sections = 4;
    } else if (hagi_type_ == Hagi_Type_G3) {
        progress_sections = 7;
    } else if (hagi_type_ == Hagi_Type_G4) {
        progress_sections = 1;
    }
    HagiKeyMessageWindow hagi_key_message_window(callback_ptr, progress_sections);
    boost::thread hagi_key_message_window_thread( boost::bind( &HagiKeyMessageWindow::run, &hagi_key_message_window) );
    int max_wait = 4;
    while(true){
        boost::thread::sleep(boost::get_system_time() + boost::posix_time::milliseconds(300));
        max_wait--;
        if(hagi_key_message_window.is_started() || max_wait < 1) {
            break;
        }
    }
    try {
        if (hagi_type_ == Hagi_Type_G2) {
            open_dll_g2_cdw();
            rc = HscCDW_BurnImage(iso_image_filename_path.string().c_str(), NULL, hagi_key_message_window.get_hwnd(), NULL);
            close_dll_g2_cdw();
        } else if (hagi_type_ == Hagi_Type_G3) {
            open_dll();
            rc = HscEXD_BurnImage(iso_image_filename_path.string().c_str(), NULL, hagi_key_message_window.get_hwnd(), NULL);
            close_dll();
        } else if (hagi_type_ == Hagi_Type_G4) {
            open_dll();
            rc = HscEXDG4_BurnImage(iso_image_filename_path.string().c_str(), NULL, hagi_key_message_window.get_hwnd(), NULL);
            close_dll();
        }
        hagi_key_message_window.reset();
        if (rc != HscEXD_SUCCESS) {
            throw Exception_Hagi(error_to_string(rc));
         }
        hagi_key_message_window.stop();
        hagi_key_message_window_thread.join();
        if (boost::filesystem::exists(iso_image_filename_path)) {
            boost::filesystem::remove(iso_image_filename_path);
        }
    }
    catch (const Exception_Hagi& e) {
        hagi_key_message_window.stop();
        hagi_key_message_window_thread.join();
        throw e;
    }
}

std::pair<std::string, std::string> HagiKey::get_knownsecret_and_servers(void) {
    string knownsecret;
    string servers;
    boost::filesystem::path knowsecret_filename(get_gon_hagi_folder(get_ro_path()) / "gon_client.ks");
    if(boost::filesystem::exists(knowsecret_filename)) {
        knownsecret = read_file_content(knowsecret_filename);
    }
    else {
        throw Exception_Hagi("Unable to find knownsecret");
    }
    boost::filesystem::path servers_filename(get_gon_hagi_folder(get_ro_path()) / "servers");
    if(boost::filesystem::exists(servers_filename)) {
        servers = read_file_content(servers_filename);
    }
    else {
        throw Exception_Hagi("Unable to find servers");
    }
    return make_pair(knownsecret, servers);
}

std::string HagiKey::get_hagi_type(void) {
    open_dll();
    std::string type;
    if (hagi_type_ == Hagi_Type_G2) {
        type = "H2";
    } else if (hagi_type_ == Hagi_Type_G3) {
        type = "H3";
    } else if (hagi_type_ == Hagi_Type_G4) {
        type = "H4";
    } else {
        return "Unknown";
    }
    return type;
}

DataBufferManaged::APtr HagiKey::zip_buffer(const DataBufferManaged::APtr& buffer) {
    DataBufferManaged::APtr buffer_zipped(DataBufferManaged::create(buffer->getSize(), 0));
    unsigned long buffer_zipped_len = buffer_zipped->getSize();
    unsigned long buffer_len = buffer->getSize();
    int err = compress2(&(*buffer_zipped)[0], &buffer_zipped_len, &(*buffer)[0], buffer_len, 9  );
    if (err != Z_OK) {
        throw Exception_Hagi("Error zipping buffer");
    }
    buffer_zipped->resize(buffer_zipped_len);
    return buffer_zipped;
}

DataBufferManaged::APtr HagiKey::unzip_buffer(const DataBufferManaged::APtr& buffer_zipped) {
    DataBufferManaged::APtr buffer(DataBufferManaged::create(buffer_zipped->getSize()*10, 0));
    unsigned long buffer_zipped_len = buffer_zipped->getSize();
    unsigned long buffer_len = buffer->getSize();

    int err = uncompress(&(*buffer)[0], &buffer_len, &(*buffer_zipped)[0], buffer_zipped_len);
    if (err != Z_OK) {
        throw Exception_Hagi("Error unzipping buffer");
    }
    buffer->resize(buffer_len);
    return buffer;
}


void HagiKey::generate_keypair(void) {
    CryptFactory::APtr cryptfacility_factory(CryptFacilityService::getInstance().getCryptFactory());
    CryptFactory::PKPair keypair(cryptfacility_factory->pk_generate_keys());

    DataBufferManaged::APtr keypair_buffer(DataBufferManaged::create(0));

    keypair_buffer->appendWithLen(DataBufferManaged::create(1, 0), 2);
    keypair_buffer->appendWithLen(zip_buffer(keypair.second), 2); // Only private key is saved because of

    try {
        boost::filesystem::path gon_hagi_path(get_gon_hagi_folder(get_rw_path()));
        boost::filesystem::remove_all(gon_hagi_path);
        boost::filesystem::create_directories(gon_hagi_path);
        boost::filesystem::path public_key_filename(gon_hagi_path / "public");
        write_file_content(public_key_filename, keypair.first->encodeHex()->toString());
    }
    catch(...) {
        throw Exception_Hagi("Unexpected hagiwara error, unabel to write public key");
    }

    open_dll();
    if (hagi_type_ == Hagi_Type_G2) {
        ULONG hidden_size_max;
        int rc = HscUDB_GetHiddenTotalByte(&hidden_size_max);
        if (rc != HscUDB_SUCCESS) {
            throw Exception_Hagi("generate_keypair.1", error_to_string(rc));
        }
        if (keypair_buffer->getSize() > static_cast<int> (hidden_size_max)) {
            stringstream ss;
            ss << "G2 The keypair do not fit into the size of the store";
            ss << " key_size:" << keypair_buffer->getSize() << " store_size:" << hidden_size_max;
            throw Exception_Hagi(ss.str());
        }
        rc = HscUDB_WriteHidden(&(*keypair_buffer)[0], 0, keypair_buffer->getSize());
        if (rc != HscUDB_SUCCESS) {
            throw Exception_Hagi("generate_keypair.2", error_to_string(rc));
        }
    } else if (hagi_type_ == Hagi_Type_G3) {
        UINT64 hidden_size_max;
        int rc = HscEXD_ReadFixedHiddenTotalByte(&hidden_size_max);
        if (rc != HscEXD_SUCCESS) {
            throw Exception_Hagi("generate_keypair.3", error_to_string(rc));
        }
        if (keypair_buffer->getSize() > static_cast<int> (hidden_size_max)) {
            stringstream ss;
            ss << "G3 The keypair do not fit into the size of the store";
            ss << " key_size:" << keypair_buffer->getSize() << " store_size:" << hidden_size_max;
            throw Exception_Hagi(ss.str());
        }
        rc = HscEXD_WriteFixedHidden(&(*keypair_buffer)[0], 0, keypair_buffer->getSize());
        if (rc != HscEXD_SUCCESS) {
            throw Exception_Hagi("generate_keypair.4", error_to_string(rc));
        }
    } else if (hagi_type_ == Hagi_Type_G4) {
        UINT64 hidden_size_max;
        int rc = HscEXDG4_ReadFixedHiddenTotalByte(&hidden_size_max);
        if (rc != HscEXD_SUCCESS) {
            throw Exception_Hagi("generate_keypair.6", error_to_string(rc));
        }
        if (keypair_buffer->getSize() > static_cast<int> (hidden_size_max)) {
            stringstream ss;
            ss << "G4 The keypair do not fit into the size of the store";
            ss << " key_size:" << keypair_buffer->getSize() << " store_size:" << hidden_size_max;
            throw Exception_Hagi(ss.str());
        }
        rc = HscEXDG4_WriteFixedHidden(&(*keypair_buffer)[0], 0, keypair_buffer->getSize());
        if (rc != HscEXDG4_SUCCESS) {
            throw Exception_Hagi("generate_keypair.7", error_to_string(rc));
        }
    } else {
        throw Exception_Hagi("Invalid key type");
    }
    close_dll();
}

DataBufferManaged::APtr HagiKey::read_private_key(void) {
    open_dll();
    if (hagi_type_ == Hagi_Type_G2) {
        DataBufferManaged::APtr buffer(DataBufferManaged::create(512, 0));
        int rc = HscUDB_ReadHidden(&(*buffer)[0], 0, buffer->getSize());
        if (rc != HscUDB_SUCCESS) {
            throw Exception_Hagi(error_to_string(rc));
        }
        return read_private_key_parse(buffer);
    } else if (hagi_type_ == Hagi_Type_G3) {
        DataBufferManaged::APtr buffer(DataBufferManaged::create(256, 0));
        int rc = HscEXD_ReadFixedHidden(&(*buffer)[0], 0,  buffer->getSize());
        if (rc != HscEXD_SUCCESS) {
            throw Exception_Hagi(error_to_string(rc));
        }
        return read_private_key_parse(buffer);
    } else if (hagi_type_ == Hagi_Type_G4) {
        DataBufferManaged::APtr buffer(DataBufferManaged::create(1000, 0));
        int rc = HscEXDG4_ReadFixedHidden(&(*buffer)[0], 0,  buffer->getSize());
        if (rc != HscEXDG4_SUCCESS) {
            throw Exception_Hagi(error_to_string(rc));
        }
        return read_private_key_parse(buffer);
    } else {
        throw Exception_Hagi("Invalid key type");
    }
    close_dll();
}

DataBufferManaged::APtr HagiKey::read_private_key_parse(const DataBufferManaged::APtr& buffer) {
    DataBufferManaged::APtr key_private_buffer;
    try {
        DataBufferManaged::APtr protokol_buffer;
        long next_start_pos = buffer->parseWithLen(protokol_buffer, 2);
        next_start_pos = buffer->parseWithLen(key_private_buffer, 2, next_start_pos);
    } catch (const DataBufferManaged::Exception_Out_of_bound&) {
        throw Exception_Hagi("Unable to read data");
    }
    return unzip_buffer(key_private_buffer);
}



DataBufferManaged::APtr HagiKey::get_public_key(void) {
    string public_key;
    boost::filesystem::path gon_hagi_path(get_gon_hagi_folder(get_rw_path()));
    if(boost::filesystem::exists(gon_hagi_path)) {
        public_key = read_file_content(gon_hagi_path / "public");
    }
    else {
        boost::filesystem::path gon_hagi_path(get_gon_hagi_folder(get_ro_path()));
        if(boost::filesystem::exists(gon_hagi_path)) {
            public_key = read_file_content(gon_hagi_path / "public");
        }
    }
    if (public_key.empty()) {
        throw Exception_Hagi("Unable to locate public_key");
    }
    DataBufferManaged::APtr public_key_buffer(DataBufferManaged::create(public_key));
    return public_key_buffer->decodeHex();
}

std::string HagiKey::gon_hagi_authentication(const std::string& challenge) {
    DataBufferManaged::APtr private_key(read_private_key());
    HagiAuthentication::APtr hagi_authentication(HagiAuthentication::create());
    DataBufferManaged::APtr hagi_challenge_signature(hagi_authentication->client_athenticate(private_key, DataBufferManaged::create(challenge)));
    return hagi_challenge_signature->toString();
}

void HagiKey::set_enrolled(void) {
    boost::filesystem::path gon_hagi_path(get_gon_hagi_folder(get_rw_path()));
    boost::filesystem::create_directories(gon_hagi_path);
    boost::filesystem::path gon_hagi_enrolled_path(gon_hagi_path  / "enrolled");
    write_file_content(gon_hagi_enrolled_path, "X");
}

void HagiKey::reset_enrolled(void) {
    boost::filesystem::path gon_hagi_enrolled_path(get_gon_hagi_folder(get_rw_path())  / "enrolled");
    if( boost::filesystem::exists(gon_hagi_enrolled_path)) {
        boost::filesystem::remove(gon_hagi_enrolled_path);
    }
}

bool HagiKey::is_enrolled(void) {
    boost::filesystem::path gon_hagi_enrolled_path(get_gon_hagi_folder(get_rw_path())  / "enrolled");
    return boost::filesystem::exists(gon_hagi_enrolled_path);
}
