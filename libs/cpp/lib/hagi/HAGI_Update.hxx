/*! \file HAGI_Authorization.hxx
 \brief This file contains the functionlaity for updating a Hagi key.
 */
#ifndef HAGI_Update_HXX
#define HAGI_Update_HXX

#include <string>
#include <vector>
#include <list>
#include <utility>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>

namespace Giritech {
namespace Hagi {

/*! \brief This class holds the functionality for authorization of a HagiKey
 */
class HagiUpdate {
public:
    typedef boost::shared_ptr<HagiUpdate> APtr;
    typedef std::pair<Utility::DataBufferManaged::APtr, Utility::DataBufferManaged::APtr> CHALLENGE_AND_SIGN_TYPE;

    /*! \brief Destructor
     */
    ~HagiUpdate();

    /*! \brief Generate update_challenge
     *
     * Generate a updage_challenge and sign it with a private key giving a update_challenge_sig
     */
    CHALLENGE_AND_SIGN_TYPE
            generate_update_challenge_with_signature(const Utility::DataBufferManaged::APtr& client_knownsecret);

    /*! \brief Verify update_challenge using the signature
     *
     */
    bool verify_update_challenge_signature(const Utility::DataBufferManaged::APtr& server_knownsecret,
                                           const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                                           const Utility::DataBufferManaged::APtr& hagi_update_challenge_sign);

    /*! \brief Generate gpms signature
     *
     * Generate a signature of given files and the update_challenge
     */
    Utility::DataBufferManaged::APtr
            generate_gpms_signature(const std::list<std::string>& abs_filenames,
                                    const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                                    const Utility::DataBufferManaged::APtr& site_private_key);
    Utility::DataBufferManaged::APtr
            generate_gpms_signature(const boost::filesystem::path& root,
                                    const std::vector<std::string>& gpm_ids,
                                    const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                                    const Utility::DataBufferManaged::APtr& server_knownsecret);

    /*! \brief Verify gpms signature
     */
    bool verify_gpms_signature(const std::list<std::string>& abs_filenames,
                               const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                               const Utility::DataBufferManaged::APtr& site_public_key,
                               const Utility::DataBufferManaged::APtr& gpms_signature);
    bool verify_gpms_signature(const boost::filesystem::path& root,
                               const std::vector<std::string>& gpm_ids,
                               const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                               const Utility::DataBufferManaged::APtr& client_knownsecret,
                               const Utility::DataBufferManaged::APtr& gpms_signature);

    /*! \brief Return new instance
     */
    static HagiUpdate::APtr create(void);

private:
    HagiUpdate(void);

    CryptFacility::CryptFactory::APtr cryptfacility_factory_;

    void lookup_filenames(const boost::filesystem::path& root, const std::string& file_regex_pattern, std::list<
            std::string>& abs_filenames) const;

    void generate_filenames(const boost::filesystem::path& root, const std::vector<std::string>& gpm_ids, std::list< std::string>& abs_filenames) const;

};

}
}
#endif
