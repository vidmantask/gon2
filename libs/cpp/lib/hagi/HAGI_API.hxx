/*! \file HAGI_API.hxx
 \brief This file contains the api to the Hagi functionality used by python extension.
 */
#ifndef HAGI_API_HXX
#define HAGI_API_HXX

#include <string>
#include <vector>
#include <list>
#include <boost/python.hpp>


#include <lib/utility/UY_API_Checkpoint.hxx>

#include <lib/hagi/HAGI_Key.hxx>

namespace Giritech {
namespace Hagi {
namespace API {


/*! \brief Initialization of hagi token
 * Returns a tuple (bool, message)
 */
boost::python::tuple key_init(void);

/*! \brief Initialization of hagi token
 * Returns a tuple (bool, message)
 */
boost::python::tuple key_discover_device(void);
boost::python::tuple key_get_unique_id(void);

boost::python::tuple key_generate_keypair(void);

boost::python::tuple key_get_public_key(void);

boost::python::tuple key_set_enrolled(void);
boost::python::tuple key_reset_enrolled(void);

boost::python::tuple key_gon_hagi_authentication(const std::string& challenge);

boost::python::tuple key_create_and_burn_iso(const std::string& source_root,
                                             const std::string& temp_root,
                                             const std::string& volume_label,
                                             Giritech::Hagi::HagiKey::CreateAndBurnISOCallback* callback);

boost::python::tuple key_create_and_burn_iso_clean(const std::string& source_root,
                                                   const std::string& temp_root,
                                                   const std::string& volume_label,
                                                   const std::string& knownsecret,
                                                   const std::string& servers,
                                                   const bool do_generate_keypair,
                                                   Giritech::Hagi::HagiKey::CreateAndBurnISOCallback* callback);
boost::python::tuple key_get_knownsecret_and_servers(void);

}
}
}
#endif
