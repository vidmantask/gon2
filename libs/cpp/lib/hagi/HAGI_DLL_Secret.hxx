/*! \file HAGI_DLL_Secret.hxx
 \brief This file contains Hagi lib contains absolutely nothing interesting
 */
#ifndef HAGI_DLL_Secret
#define HAGI_DLL_Secret

const char* generate_hagi_dll_secret(void);
void clean_hagi_dll_secret(void);

#endif
