/*! \file HAGI_DLL_Secret.cxx
 \brief This file contains absolutely nothing interesting
 */

#include <iostream>
#include "HAGI_DLL_Secret_info.hxx"
using namespace std;

const char* generate_hagi_dll_secret(void) {
  return hagi_dll_secret_info;
}

void clean_hagi_dll_secret(void) {
}
