/*! \file HAGI_APIServer.hxx
 \brief This file contains the implementation of the hagiware server side api
 */
#include <lib/hagi/HAGI_APIServer.hxx>
#include <lib/hagi/HAGI_Authentication.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Hagi;

std::string Giritech::Hagi::APIServer::generate_challenge(void) {
    HagiAuthentication::APtr hagi_authentication(HagiAuthentication::create());
    DataBufferManaged::APtr hagi_server_challenge(hagi_authentication->server_generate_hagi_server_challenge());
    return hagi_server_challenge->toString();
}


bool Giritech::Hagi::APIServer::athenticate(const std::string& hagi_public_key_string,
                                            const std::string& hagi_server_challenge_string,
                                            const std::string& hagi_server_challenge_signature_string) {
    HagiAuthentication::APtr hagi_authentication(HagiAuthentication::create());
    DataBufferManaged::APtr hagi_public_key(DataBufferManaged::create(hagi_public_key_string));
    DataBufferManaged::APtr hagi_server_challenge(DataBufferManaged::create(hagi_server_challenge_string));
    DataBufferManaged::APtr hagi_server_challenge_signature(DataBufferManaged::create(hagi_server_challenge_signature_string));
    return hagi_authentication->server_athenticate(hagi_public_key->decodeHex(), hagi_server_challenge, hagi_server_challenge_signature->decodeHex());
}
