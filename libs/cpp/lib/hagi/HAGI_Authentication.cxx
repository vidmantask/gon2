/*! \file HAGI_Authorization.hxx
 \brief This file contains the functionlaity for authorization a Hagi key.
 */
#include <sstream>
#include <string>
#include <iostream>

#include <lib/hagi/HAGI_Authentication.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/cryptfacility/CF_ALL.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Hagi;

/*
 ------------------------------------------------------------------
 HagiAuthentication
 ------------------------------------------------------------------
 */
HagiAuthentication::HagiAuthentication(void) :
    cryptfacility_factory_(CryptFacilityService::getInstance().getCryptFactory()) {
}

HagiAuthentication::~HagiAuthentication(void) {
}

HagiAuthentication::APtr HagiAuthentication::create(void) {
    return HagiAuthentication::APtr(new HagiAuthentication());
}

DataBufferManaged::APtr HagiAuthentication::server_generate_hagi_server_challenge(void) {
    return DataBufferManaged::create(cryptfacility_factory_->generateRandom(100)->encodeHex());
}

DataBufferManaged::APtr HagiAuthentication::client_athenticate(const DataBufferManaged::APtr& hagi_private_key,
                                                               const DataBufferManaged::APtr& hagi_server_challenge) {
    DataBufferManaged::APtr hagi_server_challenge_signature(cryptfacility_factory_->pk_sign_challenge(hagi_private_key, hagi_server_challenge));
    return hagi_server_challenge_signature->encodeHex();
}

bool HagiAuthentication::server_athenticate(const DataBufferManaged::APtr& hagi_public_key,
                                            const DataBufferManaged::APtr& hagi_server_challenge,
                                            const DataBufferManaged::APtr& hagi_server_challenge_signature) {
    return cryptfacility_factory_->pk_verify_challange(hagi_public_key, hagi_server_challenge, hagi_server_challenge_signature);
}
