/*! \file UTest_HAGI.cxx
 \brief This file contains unittest suite for the Hagi functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>

#include "lib/Hagi/HAGI_Key.hxx"
#include "lib/Hagi/HAGI_Authentication.hxx"
#include "lib/Hagi/HAGI_Update.hxx"
#include "lib/utility/UY_CheckpointHandler.hxx"
#include <lib/cryptfacility/CF_ALL.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Hagi;
using namespace Giritech::CryptFacility;


#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
static CryptoPP::PNew s_pNew = NULL;
static CryptoPP::PDelete s_pDelete = NULL;

extern "C" __declspec(dllexport) void __cdecl SetNewAndDeleteFromCryptoPP(CryptoPP::PNew pNew, CryptoPP::PDelete pDelete,  CryptoPP::PSetNewHandler pSetNewHandler) {
    s_pNew = pNew;
    s_pDelete = pDelete;
}

void * __cdecl operator new (size_t size) {
    return s_pNew(size);
}

void __cdecl operator delete (void * p) {
    s_pDelete(p);
}
#endif


/*! /brief Unittest for the the basis functionality of a hagiware-key interface
 */
BOOST_AUTO_TEST_CASE( test_hagiware_key )
{
    CryptFactory::APtr cryptfacility_factory(CryptFacilityService::getInstance().getCryptFactory());

    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        string unique_id(hagi_key->get_unique_id()->toString());
        cout << "hagi_unique_id: " << unique_id << endl;
        BOOST_CHECK( unique_id != "" );

        string ro_path(hagi_key->get_ro_path());
        string rw_path(hagi_key->get_rw_path());
        string hagi_type(hagi_key->get_hagi_type());

        cout << "ro_path: " << ro_path << endl;
        cout << "rw_path: " << rw_path << endl;
        cout << "hagi_type: " << hagi_type << endl;
        BOOST_CHECK( ro_path != rw_path );
        BOOST_CHECK( hagi_type != "" );
    }
    catch (const HagiKey::Exception_Hagi& e) {
        cout << "HagiKey::Exception_Hagi : " << e.what() << endl;
    }
}

/*! /brief Unittest for keypair generation
 */
BOOST_AUTO_TEST_CASE( keypair_generation )
{
    CryptFactory::APtr cryptfacility_factory(CryptFacilityService::getInstance().getCryptFactory());
    try {
        HagiKey::APtr hagi_key(HagiKey::create());

        hagi_key->generate_keypair();

        string public_key(hagi_key->get_public_key()->encodeHex()->toString());
        cout << "public_key: " << public_key << endl;
        BOOST_CHECK( public_key != "" );

        string challenge("hej med dig du");
        cout << "Authenticate:" << endl;
        std::string challenge_signature(hagi_key->gon_hagi_authentication(challenge));
        cout << "challenge_signature :" << challenge_signature << endl;
    }
    catch (const HagiKey::Exception_Hagi& e) {
        cout << "HagiKey::Exception_Hagi : " << e.what() << endl;
    }
}

///*! /brief Unittest for the the hagi authentication functionality
// */
//BOOST_AUTO_TEST_CASE( test_hagiware_authentication )
//{
//    CryptFactory::APtr cryptfacility_factory(CryptFacilityService::getInstance().getCryptFactory());
//
//    CryptFactory::PKPair key_pair(cryptfacility_factory->pk_generate_keys(128));
//    DataBufferManaged::APtr public_key(key_pair.first->encodeHex());
//    DataBufferManaged::APtr private_key(key_pair.second->encodeHex());
//
//    HagiAuthentication::APtr hagi_authentication(HagiAuthentication::create());
//    DataBufferManaged::APtr hagi_challenge(hagi_authentication->server_generate_hagi_server_challenge());
//
//    DataBufferManaged::APtr hagi_challenge_sign(hagi_authentication->client_athenticate(private_key, hagi_challenge));
//    BOOST_CHECK( hagi_authentication->server_athenticate(public_key, hagi_challenge, hagi_challenge_sign) );
//
//    DataBufferManaged::APtr hagi_challenge_tampered(DataBufferManaged::create(hagi_challenge));
//    hagi_challenge_tampered->append("00");
//    BOOST_CHECK( ! hagi_authentication->server_athenticate(public_key, hagi_challenge_tampered, hagi_challenge_sign) );
//}
//
///*! /brief Unittest for the the hagi create_and_burn functionality
// */
//BOOST_AUTO_TEST_CASE( test_create_and_burn_image )
//{
//    HagiKey::APtr hagi_key(HagiKey::create());
//    //    hagi_key->create_iso_image("", "", "x");
//}

///*! /brief Unittest for reading and writing to hiden area of hagiwara device
// */
//BOOST_AUTO_TEST_CASE( test_hidden_read_write )
//{
//    HagiKey::APtr hagi_key(HagiKey::create());
//
//    HagiKey::HiddenData hidden_data;
//    hagi_key->initialize();
//
//    hidden_data.knownsecret = DataBufferManaged::create("This is the known secret");
//    hidden_data.servers = DataBufferManaged::create("This is the servers");
//    hidden_data.private_key = DataBufferManaged::create("This is the private key");
//    hidden_data.public_key = DataBufferManaged::create("This is the public key");
//    hidden_data.update_challenge = DataBufferManaged::create("This is the update challenge");
//
//    bool rc = hagi_key->write_hidden(hidden_data, true);
//    BOOST_CHECK(rc);
//
//    HagiKey::HiddenData hidden_data_get;
//    hagi_key->read_hidden(hidden_data_get);
//    BOOST_CHECK( hidden_data.knownsecret->toString() == hidden_data_get.knownsecret->toString());
//    BOOST_CHECK( hidden_data.servers->toString() == hidden_data_get.servers->toString());
//    BOOST_CHECK( hidden_data.private_key->toString() == hidden_data_get.private_key->toString());
//    BOOST_CHECK( hidden_data.public_key->toString() == hidden_data_get.public_key->toString());
//    BOOST_CHECK( hidden_data.update_challenge->toString() == hidden_data_get.update_challenge->toString());
//
//    DataBufferManaged::APtr garbage(DataBufferManaged::create("This is garbage"));
//    rc = hagi_key->write_hidden_data_buffer(garbage);
//    BOOST_CHECK(rc);
//    BOOST_CHECK_THROW(hagi_key->read_hidden(hidden_data_get), HagiKey::Exception_Hagi);
//}

///*! /brief Unittest for clearing hagiwara device
// */
//BOOST_AUTO_TEST_CASE( test_backup_clean_restore )
//{
//    HagiKey::APtr hagi_key(HagiKey::create());
//
//    HagiKey::HiddenData hidden_data;
//    hagi_key->initialize();
//
//    hidden_data.knownsecret = DataBufferManaged::create("This is the known secret");
//    hidden_data.servers = DataBufferManaged::create("This is the servers");
//    hidden_data.private_key = DataBufferManaged::create("This is the private key");
//    hidden_data.public_key = DataBufferManaged::create("This is the public key");
//    hidden_data.update_challenge = DataBufferManaged::create("This is the update challenge");
//
//    bool rc = hagi_key->write_hidden(hidden_data, true);
//    BOOST_CHECK(rc);
//
//    hagi_key->backup();
//    hagi_key->clear();
//    HagiKey::HiddenData hidden_data_get;
//
//    BOOST_CHECK_THROW(hagi_key->read_hidden(hidden_data_get), HagiKey::Exception_Hagi);
//
//    hagi_key->restore();
//    rc = hagi_key->write_hidden(hidden_data, true);
//    BOOST_CHECK(rc);
//
//    hagi_key->read_hidden(hidden_data_get);
//    BOOST_CHECK( hidden_data.knownsecret->toString() == hidden_data_get.knownsecret->toString());
//    BOOST_CHECK( hidden_data.servers->toString() == hidden_data_get.servers->toString());
//    BOOST_CHECK( hidden_data.private_key->toString() == hidden_data_get.private_key->toString());
//    BOOST_CHECK( hidden_data.public_key->toString() == hidden_data_get.public_key->toString());
//    BOOST_CHECK( hidden_data.update_challenge->toString() == hidden_data_get.update_challenge->toString());
//}

///*! /brief Unittest for reading and writing to hiden area of hagiwara device
// */
//BOOST_AUTO_TEST_CASE( test_hagiwara_update )
//{
//    DataBufferManaged::APtr client_knownsecret;
//    DataBufferManaged::APtr server_knownsecret;
//    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(client_knownsecret, server_knownsecret);
//
//    CryptFactory::APtr cryptfacility_factory(CryptFacilityService::getInstance().getCryptFactory());
//    HagiUpdate::APtr hagi_update(HagiUpdate::create());
//
//    HagiUpdate::CHALLENGE_AND_SIGN_TYPE challenge_and_signature(hagi_update->generate_update_challenge_with_signature(client_knownsecret->encodeHex()));
//
//    bool rc = hagi_update->verify_update_challenge_signature(server_knownsecret->encodeHex(), challenge_and_signature.first, challenge_and_signature.second);
//    BOOST_CHECK(rc);
//
//    boost::filesystem::path root("C:\\tmp\\gpm_meta");
//    std::vector<std::string> gpm_ids;
//    gpm_ids.push_back("app_client_filezilla_2-2.2.29.0-1-win");
//    gpm_ids.push_back("gon_client-5.1.0.3-1-linux");
//    gpm_ids.push_back("gon_client-5.1.0.3-1-mac");
//    DataBufferManaged::APtr gpms_signature(hagi_update->generate_gpms_signature(root, gpm_ids, challenge_and_signature.first, server_knownsecret->encodeHex()));
//
//    rc = hagi_update->verify_gpms_signature(root, gpm_ids, challenge_and_signature.first, client_knownsecret->encodeHex(), gpms_signature);
//    BOOST_CHECK(rc);
//}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
