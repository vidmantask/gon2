/*! \file HAGI_Update.hxx
 \brief This file contains the functionlaity for authorizing updating of a Hagi key.
 */
#include <sstream>
#include <string>
#include <iostream>

//#include <lib/hagi/HAGI_Key.hxx>
#include <lib/hagi/HAGI_Update.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <boost/regex.hpp>
#include <lib/cryptfacility/CF_ALL.hxx>
#include <lib/cryptfacility/CF_KE_SessionKeyExchange.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Hagi;

/*
 ------------------------------------------------------------------
 HagiUpdate
 ------------------------------------------------------------------
 */
HagiUpdate::HagiUpdate(void) :
    cryptfacility_factory_(CryptFacilityService::getInstance().getCryptFactory()) {
}

HagiUpdate::~HagiUpdate() {
}

HagiUpdate::APtr HagiUpdate::create(void) {
    return HagiUpdate::APtr(new HagiUpdate());
}

HagiUpdate::CHALLENGE_AND_SIGN_TYPE HagiUpdate::generate_update_challenge_with_signature(const DataBufferManaged::APtr& client_knownsecret) {
    DataBufferManaged::APtr client_site_private_key;
    DataBufferManaged::APtr server_site_public_key;
    KeyExchange_SessionKeyExchange::setSigningValidationKeyFromKnownSecret(client_knownsecret->decodeHex(), client_site_private_key, server_site_public_key);

    DataBufferManaged::APtr hagi_update_challenge(DataBufferManaged::create(cryptfacility_factory_->generateRandom(100)->encodeHex()));

    DataBufferManaged::APtr hagi_update_challenge_sign(cryptfacility_factory_->pk_sign_challenge(client_site_private_key, hagi_update_challenge->decodeHex()));
    return make_pair(hagi_update_challenge, hagi_update_challenge_sign->encodeHex());
}

bool HagiUpdate::verify_update_challenge_signature(const DataBufferManaged::APtr& server_knownsecret,
                                                   const DataBufferManaged::APtr& hagi_update_challenge,
                                                   const DataBufferManaged::APtr& hagi_update_challenge_sign) {

    DataBufferManaged::APtr server_site_private_key;
    DataBufferManaged::APtr client_site_public_key;
    KeyExchange_SessionKeyExchange::setSigningValidationKeyFromKnownSecret(server_knownsecret->decodeHex(), server_site_private_key, client_site_public_key);
    return cryptfacility_factory_->pk_verify_challange(client_site_public_key,
                                                       hagi_update_challenge->decodeHex(),
                                                       hagi_update_challenge_sign->decodeHex());
}

DataBufferManaged::APtr HagiUpdate::generate_gpms_signature(const std::list<std::string>& abs_filenames,
                                                            const DataBufferManaged::APtr& hagi_update_challenge,
                                                            const DataBufferManaged::APtr& site_private_key) {
	Hash::APtr hash_algo(cryptfacility_factory_->getHashSHA1());
    DataBufferManaged::APtr base_buffer(DataBufferManaged::create(hagi_update_challenge));
    std::list<std::string>::const_iterator i(abs_filenames.begin());
    while (i != abs_filenames.end()) {
    	base_buffer->append(hash_algo->calculateDigestForFile(*i));
        ++i;
    }
    return (cryptfacility_factory_->pk_sign_challenge(site_private_key->decodeHex(), base_buffer->decodeHex()))->encodeHex();
}

void HagiUpdate::lookup_filenames(const boost::filesystem::path& root,
                                  const std::string& file_regex_pattern,
                                  std::list<std::string>& abs_filenames) const {
    boost::regex re;
    re.assign(file_regex_pattern);
    boost::smatch what;

    if (boost::filesystem::exists(root)) {
        boost::filesystem::directory_iterator root_end_itr;
        for ( boost::filesystem::directory_iterator root_itr(root); root_itr != root_end_itr; ++root_itr ) {
            if (boost::filesystem::is_regular_file(root_itr->status())){
                string filename(root_itr->path().string());
                if( boost::regex_search(filename, what, re)) {
                    abs_filenames.push_back(filename);
                }
            }
        }
    }
}
void HagiUpdate::generate_filenames(const boost::filesystem::path& root, const std::vector<std::string>& gpm_ids, std::list< std::string>& abs_filenames) const {
    if (boost::filesystem::exists(root)) {

		vector<string>::const_iterator i(gpm_ids.begin());
		while(i != gpm_ids.end()) {
			string filename = *i + ".gpm.xml";
			boost::filesystem::path abs_filename(root / filename);
			abs_filenames.push_back(abs_filename.string());
			++i;
		}
    }
}


Utility::DataBufferManaged::APtr HagiUpdate::generate_gpms_signature(const boost::filesystem::path& root,
                                                                     const std::vector<string>& gpm_ids,
                                                                     const DataBufferManaged::APtr& hagi_update_challenge,
                                                                     const DataBufferManaged::APtr& server_knownsecret) {
    std::list<std::string> abs_filenames;
    generate_filenames(root, gpm_ids, abs_filenames);

    DataBufferManaged::APtr server_site_private_key;
    DataBufferManaged::APtr client_site_public_key;
    KeyExchange_SessionKeyExchange::setSigningValidationKeyFromKnownSecret(server_knownsecret->decodeHex(), server_site_private_key, client_site_public_key);
    return generate_gpms_signature(abs_filenames, hagi_update_challenge, server_site_private_key->encodeHex());
}

bool HagiUpdate::verify_gpms_signature(const std::list<std::string>& abs_filenames,
                                       const DataBufferManaged::APtr& hagi_update_challenge,
                                       const DataBufferManaged::APtr& site_public_key,
                                       const DataBufferManaged::APtr& gpms_signature) {
    Hash::APtr hash_algo(cryptfacility_factory_->getHashSHA1());
    DataBufferManaged::APtr base_buffer(DataBufferManaged::create(hagi_update_challenge));
    std::list<std::string>::const_iterator i(abs_filenames.begin());
    while (i != abs_filenames.end()) {
        base_buffer->append(hash_algo->calculateDigestForFile(*i));
        ++i;
    }
    return cryptfacility_factory_->pk_verify_challange(site_public_key->decodeHex(),
                                                       base_buffer->decodeHex(),
                                                       gpms_signature->decodeHex());
}

bool HagiUpdate::verify_gpms_signature(const boost::filesystem::path& root,
                                       const std::vector<string>& gpm_ids,
                                       const Utility::DataBufferManaged::APtr& hagi_update_challenge,
                                       const Utility::DataBufferManaged::APtr& client_knownsecret,
                                       const Utility::DataBufferManaged::APtr& gpms_signature) {
    std::list<std::string> abs_filenames;
    generate_filenames(root, gpm_ids, abs_filenames);

    DataBufferManaged::APtr client_site_private_key;
    DataBufferManaged::APtr server_site_public_key;
    KeyExchange_SessionKeyExchange::setSigningValidationKeyFromKnownSecret(client_knownsecret->decodeHex(), client_site_private_key, server_site_public_key);
    return verify_gpms_signature(abs_filenames, hagi_update_challenge, server_site_public_key->encodeHex(), gpms_signature);
}
