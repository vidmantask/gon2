/*! \file HAGI_API_Authorization.hxx
 \brief This file contains the implementation of the api to hagi-authentication functionality used by py-extension.
 */
#include <boost/thread/recursive_mutex.hpp>

#include <lib/hagi/HAGI_API.hxx>
#include <lib/hagi/HAGI_Authentication.hxx>
#include <lib/hagi/HAGI_Update.hxx>
#include <lib/hagi/HAGI_Key.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>

#include <boost/python.hpp>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Hagi;
using namespace Giritech::CryptFacility;



static boost::recursive_mutex global_hagiwara_mutex;

class AllowPythonThreads {
public:
    AllowPythonThreads(const std::string& id) :
        id_(id) {
        save_ = PyEval_SaveThread();
        global_hagiwara_mutex.lock();
    }
    ~AllowPythonThreads() {
        global_hagiwara_mutex.unlock();
        PyEval_RestoreThread(save_);
    }
private:
    std::string id_;
    PyThreadState *save_;
};




boost::python::tuple Giritech::Hagi::API::key_discover_device(void) {
    AllowPythonThreads allow_python_threads("key_discover_device");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        return boost::python::make_tuple(true, "OK", hagi_key->get_hagi_type(), hagi_key->get_ro_path(), hagi_key->get_rw_path(), hagi_key->is_enrolled());
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what(), boost::python::object(), boost::python::object(), boost::python::object(), false);
    }
    return boost::python::make_tuple(false, "Unexpected error", boost::python::object(), boost::python::object(), boost::python::object(), false);
}


boost::python::tuple Giritech::Hagi::API::key_get_unique_id(void) {
    AllowPythonThreads allow_python_threads("key_get_unique_id");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        return boost::python::make_tuple(true, "OK", hagi_key->get_unique_id()->toString());
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what(), boost::python::object());
    }
    return boost::python::make_tuple(false, "Unexpected error", boost::python::object());
}


boost::python::tuple Giritech::Hagi::API::key_generate_keypair(void) {
    AllowPythonThreads allow_python_threads("key_generate_keypair");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        hagi_key->generate_keypair();
        return boost::python::make_tuple(true, "OK");

    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what());
    }
    return boost::python::make_tuple(false, "Unexpected error");
}

boost::python::tuple Giritech::Hagi::API::key_get_public_key(void) {
    AllowPythonThreads allow_python_threads("key_get_public_key");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        return boost::python::make_tuple(true, "OK", hagi_key->get_public_key()->encodeHex()->toString());

    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what(), boost::python::object());
    }
    return boost::python::make_tuple(false, "Unexpected error", boost::python::object());
}


boost::python::tuple Giritech::Hagi::API::key_gon_hagi_authentication(const std::string& challenge) {
    AllowPythonThreads allow_python_threads("key_gon_hagi_authentication");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        return boost::python::make_tuple(true, "OK", hagi_key->gon_hagi_authentication(challenge));
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what(), boost::python::object());
    }
    return boost::python::make_tuple(false, "Unexpected error", boost::python::object());
}


boost::python::tuple Giritech::Hagi::API::key_create_and_burn_iso_clean(const string& source_root,
                                                                        const string& temp_root,
                                                                        const string& volume_label,
                                                                        const string& knownsecret,
                                                                        const string& servers,
                                                                        const bool do_generate_keypair,
                                                                        Giritech::Hagi::HagiKey::CreateAndBurnISOCallback* callback) {
    AllowPythonThreads allow_python_threads("key_create_and_burn_iso");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        hagi_key->create_and_burn_iso(source_root, temp_root, volume_label, knownsecret, servers, do_generate_keypair, callback);
        return boost::python::make_tuple(true, "OK");
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what());
    }
    return boost::python::make_tuple(false, "Unexpected error");
}

boost::python::tuple Giritech::Hagi::API::key_create_and_burn_iso(const string& source_root,
                                                                  const string& temp_root,
                                                                  const string& volume_label,
                                                                  Giritech::Hagi::HagiKey::CreateAndBurnISOCallback* callback) {
    AllowPythonThreads allow_python_threads("key_create_and_burn_iso");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        hagi_key->create_and_burn_iso(source_root, temp_root, volume_label, callback);
        return boost::python::make_tuple(true, "OK");
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what());
    }
    return boost::python::make_tuple(false, "Unexpected error");
}

boost::python::tuple Giritech::Hagi::API::key_get_knownsecret_and_servers(void) {
    AllowPythonThreads allow_python_threads("get_knownsecret_and_servers");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        std::pair<std::string, std::string> knownsecret_and_servers(hagi_key->get_knownsecret_and_servers());
        return boost::python::make_tuple(true, "OK", knownsecret_and_servers.first, knownsecret_and_servers.second);
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what(), boost::python::object(), boost::python::object());
    }
    return boost::python::make_tuple(false, "Unexpected error", boost::python::object(), boost::python::object());
}

boost::python::tuple Giritech::Hagi::API::key_set_enrolled(void) {
    AllowPythonThreads allow_python_threads("key_set_enrolled");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        hagi_key->set_enrolled();
        return boost::python::make_tuple(true, "OK");
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what());
    }
    return boost::python::make_tuple(false, "Unexpected error");
}

boost::python::tuple Giritech::Hagi::API::key_reset_enrolled(void) {
    AllowPythonThreads allow_python_threads("key_set_enrolled");
    try {
        HagiKey::APtr hagi_key(HagiKey::create());
        hagi_key->reset_enrolled();
        return boost::python::make_tuple(true, "OK");
    } catch (const HagiKey::Exception_Hagi& e) {
        return boost::python::make_tuple(false, e.what());
    }
    return boost::python::make_tuple(false, "Unexpected error");
}
