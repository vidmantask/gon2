/*! \file HAGI_Key.hxx
 \brief This file contains Hagi lib
 */
#ifndef HAGI_Key_HXX
#define HAGI_Key_HXX


#include <sstream>
#include <string>

#include <winsock2.h>
#include "windows.h"
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/hagi/HAGI_DLL_Secret.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>

namespace Giritech {
namespace Hagi {

/*! \brief This class holds represent a Hagiware key
 */
class HagiKey {
public:
    typedef boost::shared_ptr<HagiKey> APtr;

    class CreateAndBurnISOCallback {
    public:
        virtual void progress(const std::string& message, const int progress) = 0;
    };

    enum Hagi_Type {
        Hagi_Type_Unknown=0,
        Hagi_Type_G2,
        Hagi_Type_G3,
        Hagi_Type_G4
    };

    /*! \brief This exception is used as base for alle exceptions thrown by the module CryptFacility
     */
    class Exception_Hagi: public Giritech::Utility::Exception {
    public:
        Exception_Hagi(const std::string& message) :
            Exception(message) {
        }
        Exception_Hagi(const std::string& method, const std::string& message)
        : Exception(message) {
            std::stringstream ss;
            ss << method << ": " << message;
            set_message(ss.str());
        }
        Exception_Hagi(const std::string& method, const int counter, const std::string& message)
        : Exception(message) {
            std::stringstream ss;
            ss << method << "(" << counter << ") : " << message;
            set_message(ss.str());
        }
    };

    /*! \brief Destructor
     */
    ~HagiKey();

    /*! \brief Generate a new keypair for the key
    */
   void generate_keypair(void);

   Utility::DataBufferManaged::APtr HagiKey::get_public_key(void);

   std::string gon_hagi_authentication(const std::string& challenge);

   void create_and_burn_iso(const std::string& source_root,
                            const std::string& temp_root,
                            const std::string& volume_label,
                            const std::string& knownsecret,
                            const std::string& servers,
                            const bool generate_keypair,
                            HagiKey::CreateAndBurnISOCallback* callback_ptr);
   void create_and_burn_iso(const std::string& source_root,
                            const std::string& temp_root,
                            const std::string& volume_label,
                            HagiKey::CreateAndBurnISOCallback* callback_ptr);


   std::pair<std::string, std::string> get_knownsecret_and_servers(void);


    /*! \brief Get the unique hagi id
     */
    Utility::DataBufferManaged::APtr get_unique_id(void);

    /*! \brief Get the unique hagi id
     */
    std::string get_ro_path(void);

    /*! \brief Get device letter of ro(cd-rom) drive
     */
    std::string get_rw_path(void);

    /*! \brief Get hagiware type
     */
    std::string get_hagi_type(void);

    /*! \brief Returns device letter of rw drive
     */
    static HagiKey::APtr create(void);


    void set_enrolled(void);
    void reset_enrolled(void);
    bool is_enrolled(void);


private:
    HagiKey(void);

    std::string error_to_string(const int& rc) const;

    std::string HagiKey::get_ro_path_(int counter);
    std::string HagiKey::get_rw_path_(int counter);

    Utility::DataBufferManaged::APtr read_private_key(void);
    Utility::DataBufferManaged::APtr read_private_key_parse(const Utility::DataBufferManaged::APtr& buffer);

    Utility::DataBufferManaged::APtr HagiKey::zip_buffer(const Utility::DataBufferManaged::APtr& buffer);
    Utility::DataBufferManaged::APtr HagiKey::unzip_buffer(const Utility::DataBufferManaged::APtr& zipped_buffer);

    Utility::DataBufferManaged::APtr read_hidden_data_buffer(void);

    void create_and_burn_iso_(const std::string& source_root,
                              const std::string& temp_root,
                              const std::string& volume_label,
                              const bool do_generate_keypair,
                              const bool keep_knownsecret_and_servers,
                              const std::string& knownsecret,
                              const std::string& servers,
                              HagiKey::CreateAndBurnISOCallback* callback_ptr);


    void open_dll(void);
    void open_dll_g2_cdw(void);
    void close_dll(void);
    void close_dll_g2_cdw(void);

    bool dll_is_open_;
    Hagi_Type hagi_type_;
    Utility::DataBufferManaged::APtr unique_id_;
};

}
}
#endif
