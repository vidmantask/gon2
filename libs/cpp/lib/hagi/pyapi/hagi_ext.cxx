/*! \file hagi_ext.cxx
 * \brief Implementation of python hagiwara extenstion
 *
 * The api uses python objects for callback. Because callback is done
 * from another thread we need to aquire the global python interpretenter lock before doing the callback.
 *
 */
#include <string>

#include <boost/python.hpp>

#include <lib/hagi/HAGI_API.hxx>
#include <lib/utility/UY_Exception.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>


using namespace std;
using namespace boost::python;
using namespace Giritech;
using namespace Giritech::Utility;


#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
extern "C" __declspec(dllexport) void __cdecl GetNewAndDeleteForCryptoPP(CryptoPP::PNew &pNew, CryptoPP::PDelete &pDelete) {
   pNew = &operator new;
   pDelete = &operator delete;
}
//static CryptoPP::PNew s_pNew = NULL;
//static CryptoPP::PDelete s_pDelete = NULL;
//
//extern "C" __declspec(dllexport) void __cdecl SetNewAndDeleteFromCryptoPP(CryptoPP::PNew pNew, CryptoPP::PDelete pDelete,  CryptoPP::PSetNewHandler pSetNewHandler) {
//    s_pNew = pNew;
//    s_pDelete = pDelete;
//}
//
//void * __cdecl operator new (size_t size) {
//    return s_pNew(size);
//}
//
//void __cdecl operator delete (void * p) {
//    s_pDelete(p);
//}
#endif


/*! \brief This exception is thrown in case of a error ocured during callback
*/
class ExceptionCallback : public Giritech::Utility::Exception {
    public:
        ExceptionCallback(const std::string& message) :
            Giritech::Utility::Exception(message) {
        }
};


class ThreadLock {
public:
    ThreadLock(const std::string& id) :
        id_(id) {
        state_ = PyGILState_Ensure();
    }
    ~ThreadLock() {
        PyGILState_Release(state_);
    }
private:
    PyGILState_STATE state_;
    std::string id_;
};




class PYAPIKeyCreateAndBurnIsoCallback : public Giritech::Hagi::HagiKey::CreateAndBurnISOCallback {
public:
    PYAPIKeyCreateAndBurnIsoCallback(PyObject* self) :
        self_(self) {
        Py_INCREF(self_);
    }
    ~PYAPIKeyCreateAndBurnIsoCallback(void) {
        Py_DECREF(self_);
    }
    void progress(const std::string& message, const int progress) {
        ThreadLock python_thread_lock("PYAPIKeyCreateAndBurnIsoCallback.progress");
        try {
            call_method<void>(self_, "progress", message, progress);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIKeyCreateAndBurnIsoCallback::progress");
        }
    }
private:
    PyObject* const self_;
};


/*! \brief Initialice hagi component
 */
void init_hagi(void) {
    Giritech::CryptFacility::CryptFacilityService::getInstance().initialize(Giritech::CryptFacility::CryptFacilityService::modeofoperation_unknown);
}

/*
 * Define hagi python api
 */
void def_api_lib_hagi(void) {
    def("init_hagi", &init_hagi);

    def("key_discover_device", &Giritech::Hagi::API::key_discover_device);
    def("key_get_unique_id", &Giritech::Hagi::API::key_get_unique_id);
    def("key_generate_keypair", &Giritech::Hagi::API::key_generate_keypair);
    def("key_get_public_key", &Giritech::Hagi::API::key_get_public_key);
    def("key_gon_hagi_authentication", &Giritech::Hagi::API::key_gon_hagi_authentication);
    def("key_get_knownsecret_and_servers", &Giritech::Hagi::API::key_get_knownsecret_and_servers);

    def("key_set_enrolled", &Giritech::Hagi::API::key_set_enrolled);
    def("key_reset_enrolled", &Giritech::Hagi::API::key_reset_enrolled);

    class_<Giritech::Hagi::HagiKey::CreateAndBurnISOCallback, PYAPIKeyCreateAndBurnIsoCallback, boost::noncopyable> ("CreateAndBurnISOCallback", init<>());
    def("key_create_and_burn_iso_clean", &Giritech::Hagi::API::key_create_and_burn_iso_clean);
    def("key_create_and_burn_iso", &Giritech::Hagi::API::key_create_and_burn_iso);
}


BOOST_PYTHON_MODULE(hagi_ext) {
    docstring_options doc_options(true);
    def_api_lib_hagi();
}
