/*! \file HAGI_APIServer.hxx
 \brief This file contains the api to the Hagi server side functionality
 */
#ifndef HAGI_APIServer_HXX
#define HAGI_APIServer_HXX

#include <string>

namespace Giritech {
namespace Hagi {
namespace APIServer {

/*! \brief Return a new server challenge
 */
std::string generate_challenge(void);

/*! \brief Server side of hagi-key authentication
 */
bool athenticate(const std::string& hagi_public_key_string,
                 const std::string& hagi_server_challenge_string,
                 const std::string& hagi_server_challenge_signature_string);

}
}
}
#endif
