/*! \file HAGI_Authorization.hxx
 \brief This file contains the functionlaity for authorization a Hagi key.
 */
#ifndef HAGI_Authorization_HXX
#define HAGI_Authorization_HXX

#include <string>
#include <utility>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>

namespace Giritech {
namespace Hagi {

/*! \brief This class holds the functionality for authorization of a HagiKey
 */
class HagiAuthentication {
public:
    typedef boost::shared_ptr<HagiAuthentication> APtr;

    /*! \brief Destructor
     */
    ~HagiAuthentication();

    /*! \brief Client side of hagi-key authentication
     *
     * From the hagi_server_challenge the hagi_key_id is calculated and returned together with
     * the hagi_key_id_signature.
     *
     * The hagi_key_id is calculated by appending hagi_secret_poem to hagi_key_unique_id and
     * calculating a sha1 of the result.
     *
     * The hagi_key_id_signature is a signature calculated on the result of appending hagi_key_id with the hagi_server_challenge
     * using the hagi_private_key, which can be verified using the hagi_public_key.
     *
     */
    Utility::DataBufferManaged::APtr
            client_athenticate(const Utility::DataBufferManaged::APtr& hagi_private_key,
                               const Utility::DataBufferManaged::APtr& hagi_server_challenge);


    /*! \brief Server side of hagi-key authentication
     *
     */
    bool server_athenticate(const Utility::DataBufferManaged::APtr& hagi_public_key,
                            const Utility::DataBufferManaged::APtr& hagi_server_challenge,
                            const Utility::DataBufferManaged::APtr& hagi_server_challenge_signature);

    /*! \brief Return a new server challenge
     */
    Utility::DataBufferManaged::APtr server_generate_hagi_server_challenge(void);

    /*! \brief Return new instance
     */
    static HagiAuthentication::APtr create(void);

private:
    HagiAuthentication(void);

    CryptFacility::CryptFactory::APtr cryptfacility_factory_;
};

}
}
#endif
