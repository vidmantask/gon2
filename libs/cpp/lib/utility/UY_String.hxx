/*! \file UY_String.hxx
    \brief This file contains classes for additional string functionality
*/
#ifndef UY_STRING_HXX
#define UY_STRING_HXX

#include <string>
#include <vector>

#include <lib/utility/UY_Config.hxx>


namespace Giritech {
  namespace Utility {
    namespace String {


      /*! \brief Extract a string list from a string with a spcific seperator
          \param seperator Seperator value to search for
          \param value     String to search in
      */
      std::vector<std::string> UTILITY_DLL extractStrings(const std::string& seperator, const std::string& value);


      /*! \brief Zerrorize string
       */
      void UTILITY_DLL burnString(std::string& value);


    } // End namespace, String
  } // End namespace, Utility
} // End Namespace, Giritech
#endif
