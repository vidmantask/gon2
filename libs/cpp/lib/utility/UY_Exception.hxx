/*! \file UY_Exception.hxx
    \brief This file contains exception class to be used as base for alle Giritech exceptions
*/
#ifndef UY_EXCEPTION_HXX
#define UY_EXCEPTION_HXX

#include <string>

namespace Giritech {
  namespace Utility {


/*! \brief Abstract class for representing an envent logger.
*/
class Exception : public std::exception {
public:

  /*! \brief Constructor
  */ 
  Exception(const std::string& message) 
    : message_(message) {
  }
  virtual ~Exception(void) throw() {
  }

  void  set_message(const std::string& message) {
      message_ = message;
  }


  virtual const char* what(void) const throw() {
    return message_.c_str();
  }
private:
  std::string message_;
};


  } // End namespace, Utility
} // End Namespace, Giritech
#endif
