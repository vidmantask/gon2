/*! \file UY_CheckpointFilter.cxx
    \brief This file contains the implementation of classes for checkpoint filtering
*/
#include <functional>
#include <lib/utility/UY_CheckpointFilter.hxx>

using namespace Giritech::Utility;
using namespace std;



/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_true
   ------------------------------------------------------------------ 
*/
CheckpointFilter_true::CheckpointFilter_true(void) {
}
CheckpointFilter::APtr CheckpointFilter_true::create(void) {
  return CheckpointFilter::APtr(new CheckpointFilter_true); 
}

bool CheckpointFilter_true::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return true;
}


/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_false
   ------------------------------------------------------------------ 
*/
CheckpointFilter_false::CheckpointFilter_false(void) {
}
CheckpointFilter::APtr CheckpointFilter_false::create(void) {
  return CheckpointFilter::APtr(new CheckpointFilter_false); 
}

bool CheckpointFilter_false::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return false;
}


/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_or
   ------------------------------------------------------------------ 
*/
CheckpointFilter_or::CheckpointFilter_or(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b) 
  : filter_a_(filter_a), filter_b_(filter_b) {
  assert(filter_a_);
  assert(filter_b_);
}
CheckpointFilter::APtr CheckpointFilter_or::create(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b) {
  return CheckpointFilter::APtr(new CheckpointFilter_or(filter_a, filter_b)); 
}

bool CheckpointFilter_or::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return (*filter_a_)(checkpoint_id, checkpoint_attrs, structure) || (*filter_b_)(checkpoint_id, checkpoint_attrs, structure);
}


/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_and
   ------------------------------------------------------------------ 
*/
CheckpointFilter_and::CheckpointFilter_and(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b) 
  : filter_a_(filter_a), filter_b_(filter_b) {
  assert(filter_a_);
  assert(filter_b_);
}
CheckpointFilter::APtr CheckpointFilter_and::create(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b) {
  return CheckpointFilter::APtr(new CheckpointFilter_and(filter_a, filter_b)); 
}

bool CheckpointFilter_and::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return (*filter_a_)(checkpoint_id, checkpoint_attrs, structure) && (*filter_b_)(checkpoint_id, checkpoint_attrs, structure);
}


/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_not
   ------------------------------------------------------------------ 
*/
CheckpointFilter_not::CheckpointFilter_not(const CheckpointFilter::APtr& filter) 
  : filter_(filter) {
  assert(filter_);
}
CheckpointFilter::APtr CheckpointFilter_not::create(const CheckpointFilter::APtr& filter) {
  return CheckpointFilter::APtr(new CheckpointFilter_not(filter)); 
}

bool CheckpointFilter_not::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return !(*filter_)(checkpoint_id, checkpoint_attrs, structure);
}



/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_checkpoint_ids
   ------------------------------------------------------------------ 
*/
CheckpointFilter_checkpoint_ids::CheckpointFilter_checkpoint_ids(const std::string& id) {
  ids_.insert(id);
}

CheckpointFilter_checkpoint_ids::CheckpointFilter_checkpoint_ids(const std::set<std::string>& ids) {
  ids_.insert(ids.begin(), ids.end());
}

CheckpointFilter::APtr CheckpointFilter_checkpoint_ids::create(const std::string& id) {
  return CheckpointFilter::APtr(new CheckpointFilter_checkpoint_ids(id));
}
CheckpointFilter::APtr CheckpointFilter_checkpoint_ids::create(const std::set<std::string>& ids) {
  return CheckpointFilter::APtr(new CheckpointFilter_checkpoint_ids(ids));
}


bool CheckpointFilter_checkpoint_ids::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  std::set<std::string>::const_iterator i(ids_.find(checkpoint_id));
  return (i != ids_.end());
}



/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_skip_complete
   ------------------------------------------------------------------ 
*/
CheckpointFilter_skip_complete::CheckpointFilter_skip_complete(void) {
}
CheckpointFilter::APtr CheckpointFilter_skip_complete::create(void) {
  return CheckpointFilter::APtr(new CheckpointFilter_skip_complete);
}

bool CheckpointFilter_skip_complete::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  return structure == CheckpointAttr::Structure_begin_end || structure == CheckpointAttr::Structure_begin;  
}



/* 
   ------------------------------------------------------------------ 
   Implementation CheckpointFilter_accept_if_attr_exist
   ------------------------------------------------------------------ 
*/
CheckpointFilter_accept_if_attr_exist::CheckpointFilter_accept_if_attr_exist(const CheckpointAttr::APtr& attr) 
  : attr_(attr) {
  assert(attr);
}
CheckpointFilter::APtr CheckpointFilter_accept_if_attr_exist::create(const CheckpointAttr::APtr& attr) {
  return CheckpointFilter::APtr(new CheckpointFilter_accept_if_attr_exist(attr));
}

bool CheckpointFilter_accept_if_attr_exist::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {   
  CheckpointAttr::APtrs::const_iterator i(checkpoint_attrs.find(attr_->get_id()));
  if(i != checkpoint_attrs.end()) {
    return i->second->equal(attr_);
  }
  return false;
}


