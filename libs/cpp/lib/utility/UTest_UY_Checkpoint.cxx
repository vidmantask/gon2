/*! \file UY_Checkpoint_UTest.cxx
 \brief This file contains unittest suite for the checkpoint functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>

#include "lib/utility/UY_Checkpoint.hxx"
#include "lib/utility/UY_CheckpointHandler.hxx"

using namespace std;
using namespace Giritech::Utility;

/*! /brief Unittest for the the basis functionality of the checkpoint functionality
 */

class CheckpointOutputHandler_Test : public CheckpointOutputHandler {
public:
	typedef boost::shared_ptr< CheckpointOutputHandler_Test > APtr;

	void operator()(const std::string& checkpoint_id,
			const CheckpointAttr::APtrs& event_attrs,
			const CheckpointAttr::Structure structure) {
		if (structure == CheckpointAttr::Structure_end) {
			checkpoint_ids_.insert(checkpoint_id+"_complete");
		} else {
			checkpoint_ids_.insert(checkpoint_id);
		}
	}

	bool got_checkpoint_id(const std::string& checkpoint_id) const {
		std::set< std::string >::const_iterator iend(checkpoint_ids_.end());
		std::set< std::string >::const_iterator
				i(checkpoint_ids_.find(checkpoint_id));
		if (i != iend)
			return true;
		return false;
	}

    void close(void) {
    }

	static APtr create(void) {
		return CheckpointOutputHandler_Test::APtr(new CheckpointOutputHandler_Test);
	}

private:
	std::set< std::string > checkpoint_ids_;
};

void generate_checkpoints(CheckpointHandler& checkpoint_handler) {
	Checkpoint checkpoint_a(checkpoint_handler, "point_a",
			CheckpointAttr_Module::create("TestModule"), CpAttr_error(),
			CheckpointAttrS::create("my_attr_a", "my_value_a"));
	CheckpointScope checkpoint_b(checkpoint_handler, "point_b",
			CheckpointAttr_Module::create("TestModule"), CpAttr_debug(),
			CheckpointAttrS::create("my_attr_b", "my_value_b"));
	Checkpoint checkpoint_c(checkpoint_handler, "point_c",
			CheckpointAttr_Module::create("TestModule"), CpAttr_warning(),
			CheckpointAttrS::create("my_attr_c", "my_value_c"));
}

CheckpointAttr::APtrs generate_attrs(void) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, CheckpointAttrS::create("my_attr_a","my_value_a"));
	Checkpoint::add_attr(attrs, CheckpointAttrS::create("my_attr_b", "my_value_b"));
	Checkpoint::add_attr(attrs, CpAttr_debug());
	return attrs;
}

BOOST_AUTO_TEST_CASE( test_basis )
{
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

BOOST_AUTO_TEST_CASE( test_basis_suggar )
{
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	{
		Checkpoint checkpoint_a(checkpoint_handler, "point_a", CheckpointAttr_Module::create("TestModule"), CpAttr_warning(), CpAttr("my_attr_a", "my_value_a"));
		CheckpointScope checkpoint_b(checkpoint_handler, "point_b", CheckpointAttr_Module::create("TestModule"), CpAttr_debug(), CpAttr("my_attr_b", 42));
		Checkpoint checkpoint_c(checkpoint_handler, "point_c", CheckpointAttr_Module::create("TestModule"), CpAttr_error(),CpAttr("my_attr_c", "my_value_c"));
	}

	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

//void basis_attr_list(void) {
//	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
//	CheckpointOutputHandler::APtr checkpoint_output_handler(
//			CheckpointOutputHandlerTEXT::create(::CheckpointDestinationHandlerCout::create()));
//	CheckpointHandler checkpoint_handler(checkpoint_filter,
//			checkpoint_output_handler);
//	Checkpoint checkpoint_a(checkpoint_handler, "point_a", generate_attrs());
//}

BOOST_AUTO_TEST_CASE( test_filter_checkpoint_ids)
{
	set<string> ids;
	ids.insert("point_a");
	ids.insert("point_b");

	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_checkpoint_ids::create(ids));
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

BOOST_AUTO_TEST_CASE( test_filter_skip_complete )
{
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_skip_complete::create());
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

BOOST_AUTO_TEST_CASE( test_filter_accept_if_attr_exist )
{
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_accept_if_attr_exist::create(CpAttr_error()));
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

BOOST_AUTO_TEST_CASE( test_filter_or )
{
	CheckpointFilter::APtr checkpoint_filter_a(CheckpointFilter_accept_if_attr_exist::create(CpAttr_error()));
	CheckpointFilter::APtr checkpoint_filter_b(CheckpointFilter_accept_if_attr_exist::create(CpAttr_warning()));
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_or::create(checkpoint_filter_a, checkpoint_filter_b));
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

BOOST_AUTO_TEST_CASE( test_filter_not )
{
	CheckpointFilter::APtr checkpoint_filter_a(CheckpointFilter_accept_if_attr_exist::create(CpAttr_warning()));
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_not::create(checkpoint_filter_a));
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_a") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b") );
	BOOST_CHECK( !checkpoint_output_handler->got_checkpoint_id("point_c") );
	BOOST_CHECK( checkpoint_output_handler->got_checkpoint_id("point_b_complete") );
}

//void output_handler_xml(void) {
//	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
//	CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(boost::filesystem::initial_path() / "utest.out"))));
//	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);
//	generate_checkpoints(checkpoint_handler);
//}
//
BOOST_AUTO_TEST_CASE( test_out_text ) {
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
	CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerTEXT::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	generate_checkpoints(checkpoint_handler);
	Checkpoint checkpoint_d(checkpoint_handler, "point_d", CheckpointAttr_Module::create("TestModule"), CpAttr_debug(),	CheckpointAttrS::create("my_attr_d", "my_value_d"));
	Checkpoint checkpoint_e(checkpoint_handler, "point_e", CheckpointAttr_Module::create("TestModule"), CpAttr_debug(),	CheckpointAttrS::create("my_attr_e", "my_value_e"), CheckpointAttrS::create("content", "line a\nline b\nline c"));
}
//
//BOOST_AUTO_TEST_CASE( test_out_folder ) {
//	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
//	CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerFolderXML::create("/tmp/gtest"));
//	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);
//
//	generate_checkpoints(checkpoint_handler);
//	Checkpoint checkpoint_d(checkpoint_handler, "point_d", CheckpointAttr_Module::create("TestModule"), CpAttr_debug(),	CheckpointAttrS::create("content", "my content with < "));
//}

BOOST_AUTO_TEST_CASE( test_is_logging ) {
	CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_accept_if_attr_exist::create(CpAttr_error()));
	CheckpointOutputHandler_Test::APtr checkpoint_output_handler(CheckpointOutputHandler_Test::create());
	CheckpointHandler checkpoint_handler(checkpoint_filter, checkpoint_output_handler);

	CheckpointIsLogging is_logging_001(checkpoint_handler, "point_a", CheckpointAttr_Module::create("TestModule"), CpAttr_error());
	BOOST_CHECK( is_logging_001() );

	CheckpointIsLogging is_logging_002(checkpoint_handler, "point_a", CheckpointAttr_Module::create("TestModule"), CpAttr_info());
	BOOST_CHECK( !is_logging_002() );
}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
	return 0;
}
