/*! \file UY_CheckpointOutputHandler.hxx
    \brief This file contains classes for handling accepted checkpoints in a checkpoint handler
*/
#ifndef UY_CHECKPOINTOUTPUTHANDLER_HXX
#define UY_CHECKPOINTOUTPUTHANDLER_HXX

#include <string>
#include <iostream>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/filesystem.hpp>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointDestinationHandler.hxx>


namespace Giritech {
  namespace Utility {



    /*! \brief Abstract interface class for a handling accepted checkpoints

    A checkpoint output handler is used by a checkpoint handler to accepted checkpoints.
    */
    class CheckpointOutputHandler : boost::noncopyable {
    public:
      typedef boost::shared_ptr<CheckpointOutputHandler> APtr;

      virtual void operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) = 0;

      virtual void close(void) = 0;

      virtual void set_destination_handler(CheckpointDestinationHandler::APtr checkpointDesitnationHandler);
      CheckpointDestinationHandler::APtr get_destination_handler(void);

    protected:
      CheckpointOutputHandler(void);
      CheckpointOutputHandler(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler);

      CheckpointDestinationHandler::APtr checkpointDesitnationHandler_;
    };




    /*! \brief Minimal class for representing accepted checkpoints in a xml format
    */
    class CheckpointOutputHandlerXML : public CheckpointOutputHandler {
    public:
      void operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      void close(void);

      static APtr create(void);
      static APtr create(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler);

    private:
      CheckpointOutputHandlerXML(void);
      CheckpointOutputHandlerXML(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler);

      void print_attr(const CheckpointAttr::APtrs::value_type& attr);

      boost::recursive_timed_mutex mutex_session_instance;
    };



    /*! \brief Minimal class for representing accepted checkpoints in a text format
    */
    class CheckpointOutputHandlerTEXT : public CheckpointOutputHandler {
    public:
      void operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      void close(void);

      static APtr create(void);
      static APtr create(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler);
    private:
      CheckpointOutputHandlerTEXT(void);
      CheckpointOutputHandlerTEXT(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler);

      void print_attr(const CheckpointAttr::APtrs::value_type& attr);

      boost::recursive_timed_mutex mutex_session_instance;
    };

#if !defined(ANDROID)
    /*! \brief Class for writeing each entry as a XML file in in a folder
    */
    class CheckpointOutputHandlerFolderXML : public CheckpointOutputHandler {
    public:
      void operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      void close(void);

      static APtr create(const boost::filesystem::path& folder);

    private:
      CheckpointOutputHandlerFolderXML(void);
      CheckpointOutputHandlerFolderXML(const boost::filesystem::path& folder);

      void print_attr(const CheckpointAttr::APtrs::value_type& attr);

      boost::filesystem::path folder_;
      boost::recursive_timed_mutex mutex_session_instance;
    };
#endif




  } // End namespace, Utility
} // End Namespace, Giritech
#endif
