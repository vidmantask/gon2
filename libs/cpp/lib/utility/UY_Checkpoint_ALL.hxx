/*! \file UY_Checkpoint_ALL.hxx
    \brief This file includes all relevant headerfiles for checkpoint functionality
*/
#ifndef UY_Checkpoint_ALL_HXX
#define UY_Checkpoint_ALL_HXX
# include <lib/utility/UY_Checkpoint.hxx>
# include <lib/utility/UY_CheckpointHandler.hxx>
# include <lib/utility/UY_CheckpointOutputHandler.hxx>
# include <lib/utility/UY_CheckpointDestinationHandler.hxx>
#endif
