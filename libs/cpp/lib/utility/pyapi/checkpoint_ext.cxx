//
// This has been termporary moved to comunication_ext
//


//#include <string>
//
//#include <lib/utility/UY_API_Checkpoint.hxx>
//#include <lib/utility/UY_Checkpoint.hxx>
//#include <lib/utility/UY_CheckpointAttr.hxx>
//#include <lib/utility/UY_CheckpointHandler.hxx>
//#include <lib/utility/UY_CheckpointFilter.hxx>
//#include <lib/utility/UY_CheckpointOutputHandler.hxx>
//
//#include <boost/python.hpp>
//#include <boost/python/stl_iterator.hpp>
//#include <boost/shared_ptr.hpp>
//
//using namespace boost::python;
//using namespace Giritech::Utility;
//
//CheckpointFilter::APtr CheckpointFilter_checkpoint_ids_set_create(list l) {
//    std::set<std::string> ids;
//    for (stl_input_iterator<object> elem(l), end; elem != end; ++elem) {
//        extract<std::string> string_value(*elem);
//        if (string_value.check()) {
//            /* Actually ... we try anyway to get decent error messages: */
//        }
//        ids.insert(string_value());
//    }
//    return CheckpointFilter_checkpoint_ids::create(ids);
//}
//
//CheckpointFilter::APtr CheckpointFilter_accept_if_attr_exist_create(std::string key,
//                                                                    std::string value) {
//    return CheckpointFilter_accept_if_attr_exist::create(CheckpointAttrS::create(key, value));
//}
//
//CheckpointOutputHandler::APtr CheckpointOutputHandlerXML_create_file(std::string filename) {
//    return CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(filename)));
//}
//
//class CheckpointOutputHandler_Test : public CheckpointOutputHandler {
//public:
//    typedef boost::shared_ptr< CheckpointOutputHandler_Test > APtr;
//
//    void operator()(const std::string& checkpoint_id,
//              const CheckpointAttr::APtrs& event_attrs,
//              const CheckpointAttr::Structure structure) {
//        if (structure == CheckpointAttr::Structure_end) {
//            checkpoint_ids_.insert(checkpoint_id+"_complete");
//        } else {
//            checkpoint_ids_.insert(checkpoint_id);
//        }
//    }
//
//    bool got_checkpoint_id(const std::string& checkpoint_id) const {
//        std::set< std::string >::const_iterator iend(checkpoint_ids_.end());
//        std::set< std::string >::const_iterator i(checkpoint_ids_.find(checkpoint_id));
//        if (i != iend)
//            return true;
//        return false;
//    }
//
//    // Py-style method
//    static bool got_checkpoint_id_aptr(APtr &self, const std::string& checkpoint_id) {
//        return self->got_checkpoint_id(checkpoint_id);
//    }
//
//    // Returns APtr which doesn't inherit from base classes APtr...
//    static APtr create(void) {
//        return CheckpointOutputHandler_Test::APtr(new CheckpointOutputHandler_Test);
//    }
//
//    // Return APtr for downcast to base class. Py-style method
//    static CheckpointOutputHandler::APtr get_CheckpointOutputHandler(APtr &self) {
//        return CheckpointOutputHandler::APtr(self);
//    }
//
//private:
//    std::set< std::string > checkpoint_ids_;
//};
//
//BOOST_PYTHON_MODULE(checkpoint_ext) {
//    /* Usage example:
//
//     import checkpoint_ext
//     filter = checkpoint_ext.CheckpointFilter_true()
//     output_handler = checkpoint_ext.CheckpointOutputHandlerTEXT()
//     handler = checkpoint_ext.APICheckpointHandler(filter, output_handler)
//     scope = handler.CheckpointScope('foo', dict(a='asdf', b='bar'))
//     checkpoint_ext.add_complete_attr(scope, dict(xyzzy='1234'))
//     handler.Checkpoint('baz', dict(gog='thin', gokke='fat'))
//     checkpoint_ext.end_scope(scope)
//
//     - but it is meant to be Wrapped further in python to make it more py/oo-ish
//     */
//
//    using namespace Giritech::Utility;
//
//    // CheckpointFilter
//    class_<CheckpointFilter::APtr>("CheckpointFilter") ;
//    def("CheckpointFilter_true", &CheckpointFilter_true::create);
//    def("CheckpointFilter_false", &CheckpointFilter_false::create);
//    def("CheckpointFilter_or", &CheckpointFilter_or::create);
//    def("CheckpointFilter_and", &CheckpointFilter_and::create);
//    def("CheckpointFilter_not", &CheckpointFilter_not::create);
//    CheckpointFilter::APtr
//            (*ids_string)(const std::string& id) = &CheckpointFilter_checkpoint_ids::create;
//    def("CheckpointFilter_checkpoint_ids", ids_string);
//    def("CheckpointFilter_checkpoint_ids", CheckpointFilter_checkpoint_ids_set_create);
//    def("CheckpointFilter_skip_complete", &CheckpointFilter_skip_complete::create);
//    def("CheckpointFilter_accept_if_attr_exist", &CheckpointFilter_accept_if_attr_exist_create);
//
//    // CheckpointFilter
//    class_<CheckpointOutputHandler::APtr>("CheckpointOutputHandler") ;
//    // XML
//    CheckpointOutputHandler::APtr (*out_xml_void)(void) = &CheckpointOutputHandlerXML::create;
//    def("CheckpointOutputHandlerXML", out_xml_void);
//    def("CheckpointOutputHandlerXML", &CheckpointOutputHandlerXML_create_file);
//    // Text
//    CheckpointOutputHandler::APtr (*out_text_void)(void) = &CheckpointOutputHandlerTEXT::create;
//    def("CheckpointOutputHandlerTEXT", out_text_void);
//    CheckpointOutputHandler::APtr
//            (*out_text_dest)(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler) = &CheckpointOutputHandlerTEXT::create;
//    def("CheckpointOutputHandlerTEXT", out_text_dest);
//    // Test
//    def("CheckpointOutputHandler_Test", &CheckpointOutputHandler_Test::create);
//    class_<CheckpointOutputHandler_Test::APtr>("CheckpointOutputHandler_Test_APtr")
//    // APtr methods not possible, use Py-style
//    ;
//    def("got_checkpoint_id", &CheckpointOutputHandler_Test::got_checkpoint_id_aptr);
//    def("get_CheckpointOutputHandler", &CheckpointOutputHandler_Test::get_CheckpointOutputHandler);
//
//    // CheckpointHandler
//    class_<APICheckpointHandler::APtr>("APICheckpointHandler", "Handler for instrumentation")
//    .def("Checkpoint", &APICheckpointHandler::self_createCheckpoint, "Instrumentation logging point")
//    .def("CheckpointScope",
//          &APICheckpointHandler::self_createCheckpointScope,
//         "Instrumentation logging scope");
//    def("APICheckpointHandler_create",
//         &APICheckpointHandler::create);
//
//    // CheckpointScope
//    class_<APICheckpointScope::APtr>("APICheckpointScope")
//    // It would be convenient to have add_complete_attr and __exit__ here - but that's not possible, so we make it py-style...
//    ;
//    def("add_complete_attr", &APICheckpointScope::add_complete_attr_aptr);
//    def("end_scope", &APICheckpointScope::end_scope_aptr);
//}
