/*! \file UY_Checkpoint.cxx
 \brief This file contains the implementation of classes for event handling.
 */

// Includede befor <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>
#include <sstream>

#include <boost/none.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace boost::python;

/*
 * ------------------------------------------------------------------
 * Implementation APICheckpointScope
 * ------------------------------------------------------------------
 */
APICheckpointScope::APICheckpointScope(const CheckpointHandler::APtr& checkpoint_handler_,
                                         const std::string& cp_id,
                                         const CheckpointAttr::APtrs& attrs) {
    checkpoint_handler = checkpoint_handler_; // Keep APtr to handler to ensure lifetime at least as long as the scope which owns a pointer...
    checkpointscope = boost::shared_ptr<CheckpointScope>(new CheckpointScope(*checkpoint_handler, cp_id, attrs));
}
APICheckpointScope::~APICheckpointScope() {
    if (checkpointscope)
        end_scope();
}

APICheckpointScope::APtr APICheckpointScope::create(const CheckpointHandler::APtr& checkpoint_handler,
                                                      const std::string& cp_id,
                                                      const CheckpointAttr::APtrs& attrs) {
    return APtr(new APICheckpointScope(checkpoint_handler, cp_id, attrs)); // Will destruct when we destruct
}

void APICheckpointScope::add_complete_attr_aptr(const APtr& self, const dict& attrs_dict) {
    assert(self->checkpointscope);
    // Don't go here after end_scope!
    for (stl_input_iterator<std::string> key(attrs_dict.keys()), end; key != end; ++key) {
        self->checkpointscope->add_complete_attr(CheckpointAttrS::create(*key,
                                                                         extract<std::string>(attrs_dict[*key])));
    }
}

void APICheckpointScope::end_scope_aptr(const APtr& self) {
    self->end_scope();
}

void APICheckpointScope::end_scope() {
    checkpointscope.reset(); // ~CheckpointScope will use pointer/ref to checkpoint_handler, which thus must be alive
    checkpoint_handler.reset();
}

/*
 * ------------------------------------------------------------------
 * Implementation APICheckpointHandler
 * ------------------------------------------------------------------
 */
APICheckpointHandler::APICheckpointHandler(const CheckpointHandler::APtr& a_checkpoint_handler) :
	checkpoint_handler(a_checkpoint_handler) {
}
APICheckpointHandler::APICheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                                             const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
    checkpoint_handler = CheckpointHandler::APtr(new CheckpointHandler(checkpoint_filter, checkpoint_output_handler));
}
APICheckpointHandler::APICheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                                             const CheckpointOutputHandler::APtr& checkpoint_output_handler,
                                             CheckpointHandler& checkpoint_handler_parent) {
    checkpoint_handler = CheckpointHandler::APtr(new CheckpointHandler(checkpoint_filter, checkpoint_output_handler, checkpoint_handler_parent));
}

CheckpointAttr::APtrs APICheckpointHandler::createCheckPointAttrs(const dict& attrs_dict) {
    CheckpointAttr::APtrs attrs;

    for (stl_input_iterator<std::string> key(attrs_dict.keys()), end; key != end; ++key) {
        object value = attrs_dict[*key];
        extract<int> int_value(value);
        if (int_value.check()) {
            Checkpoint::add_attr(attrs, CheckpointAttrI::create(*key, int_value()));
            continue;
        }
        extract<std::string> string_value(value);
        if (string_value.check()) {
            /* Actually ... we try anyway to get decent error messages: */
        }
        Checkpoint::add_attr(attrs, CheckpointAttrS::create(*key, string_value()));
    }
    return attrs;
}

void APICheckpointHandler::createCheckpoint(const std::string& cp_id, const dict& attrs_dict) {
   Checkpoint(*checkpoint_handler, cp_id, createCheckPointAttrs(attrs_dict));
}

APICheckpointScope::APtr APICheckpointHandler::createCheckpointScope(const std::string& cp_id,
                                                                       const dict& attrs_dict) {
    return APICheckpointScope::create(checkpoint_handler, cp_id, createCheckPointAttrs(attrs_dict));
}

CheckpointHandler::APtr APICheckpointHandler::get_checkpoint_handler(void) const {
    return checkpoint_handler;
}

void APICheckpointHandler::set_output_handler(const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
	checkpoint_handler->set_output_handler(checkpoint_output_handler);
}


APICheckpointHandler::APtr APICheckpointHandler::create_with_handler(const CheckpointHandler::APtr& a_checkpoint_handler) {
    return APtr(new APICheckpointHandler(a_checkpoint_handler));
}

APICheckpointHandler::APtr APICheckpointHandler::create(const CheckpointFilter::APtr& checkpoint_filter,
                                                          const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
    return APtr(new APICheckpointHandler(checkpoint_filter, checkpoint_output_handler));
}

APICheckpointHandler::APtr APICheckpointHandler::create_with_parent(
		const CheckpointFilter::APtr& checkpoint_filter,
		const CheckpointOutputHandler::APtr& checkpoint_output_handler,
		const APICheckpointHandler::APtr& checkpoint_handler_parent) {
	return APtr(new APICheckpointHandler(checkpoint_filter, checkpoint_output_handler, *(checkpoint_handler_parent->get_checkpoint_handler().get())));
}

void APICheckpointHandler::self_createCheckpoint(const APICheckpointHandler::APtr& self,
                                                  const std::string& cp_id,
                                                  const boost::python::dict& attrs_dict) {
    assert(self.get() != NULL);
    self->createCheckpoint(cp_id, attrs_dict);
}

APICheckpointScope::APtr APICheckpointHandler::self_createCheckpointScope(const APICheckpointHandler::APtr& self,
                                                                            const std::string& cp_id,
                                                                            const boost::python::dict& attrs_dict) {
    assert(self.get() != NULL);
    return self->createCheckpointScope(cp_id, attrs_dict);
}

CheckpointHandler::APtr APICheckpointHandler::self_get_checkpoint_handler(const APICheckpointHandler::APtr& self) {
    assert(self.get() != NULL);
    return self->get_checkpoint_handler();
}

void APICheckpointHandler::close(void) {
	checkpoint_handler->close();
}

void APICheckpointHandler::self_close(const APICheckpointHandler::APtr& self) {
    assert(self.get() != NULL);
    self->close();
}

void APICheckpointHandler::self_set_output_handler(const APtr& self, const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
    assert(self.get() != NULL);
    self->set_output_handler(checkpoint_output_handler);
}
