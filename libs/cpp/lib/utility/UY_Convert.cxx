/*! \file UY_Convert.cxx
 \brief This file contains the implementation of functions for convertion between basic types
 */

// #ifdef GIRITECH_COMPILEOPTION_TARGET_WIN
// # include <boost/asio.hpp> 
// #endif
#ifdef GIRITECH_COMPILEOPTION_TARGET_POSIX
# include <netinet/in.h>
#endif

#include <lib/utility/UY_Convert.hxx>

using namespace Giritech::Utility;
using namespace std;

/* 
 Thies definition are found on the good old internel 
 http://www.codeproject.com/cpp/endianness.asp
 */
#define g_ntohll(x) (((boost::uint64_t)(ntohl((boost::uint32_t)((x << 32) >> 32))) << 32) | (boost::uint32_t)ntohl(((boost::uint32_t)(x >> 32))))
#define g_htonll(x) g_ntohll(x)

void Giritech::Utility::convert_to_networkorder_8(const boost::uint8_t& value, byte_t& byte1) {
    byte1 = static_cast<byte_t>(value);
}

void Giritech::Utility::convert_from_networkorder_8(boost::uint8_t& value, const byte_t& byte1) {
    value = static_cast<boost::uint8_t>(byte1);
}

void Giritech::Utility::convert_to_networkorder_16(const boost::uint16_t& value,
                                                   byte_t& byte1,
                                                   byte_t& byte2) {
    boost::uint16_t netWorkByteOrder = 0;
    netWorkByteOrder = static_cast<const boost::uint16_t>(htons(value));
    byte_t* netWorkByteOrderPtr = (byte_t*) &netWorkByteOrder;
    byte1 = netWorkByteOrderPtr[0];
    byte2 = netWorkByteOrderPtr[1];
}
void Giritech::Utility::convert_from_networkorder_16(boost::uint16_t& value,
                                                     const byte_t& byte1,
                                                     const byte_t& byte2) {
    boost::uint16_t netWorkByteOrder = 0;
    byte_t* netWorkByteOrderPtr( (byte_t*)&netWorkByteOrder );
    netWorkByteOrderPtr[0] = byte1;
    netWorkByteOrderPtr[1] = byte2;
    value = ntohs(netWorkByteOrder);
}

#ifndef ANDROID
void Giritech::Utility::convert_to_networkorder_32(const boost::uint32_t& value,
                                                   byte_t& byte1,
                                                   byte_t& byte2,
                                                   byte_t& byte3,
                                                   byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    netWorkByteOrder = htonl(value);
    byte_t* netWorkByteOrderPtr = (byte_t*) &netWorkByteOrder;
    byte1 = netWorkByteOrderPtr[0];
    byte2 = netWorkByteOrderPtr[1];
    byte3 = netWorkByteOrderPtr[2];
    byte4 = netWorkByteOrderPtr[3];
}
void Giritech::Utility::convert_from_networkorder_32(boost::uint32_t& value,
                                                     const byte_t& byte1,
                                                     const byte_t& byte2,
                                                     const byte_t& byte3,
                                                     const byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    byte_t* netWorkByteOrderPtr( (byte_t*)&netWorkByteOrder);
    netWorkByteOrderPtr[0] = byte1;
    netWorkByteOrderPtr[1] = byte2;
    netWorkByteOrderPtr[2] = byte3;
    netWorkByteOrderPtr[3] = byte4;
    value = ntohl(netWorkByteOrder);
}
#endif
void Giritech::Utility::convert_to_networkorder_64(const boost::uint64_t& value,
                                                   byte_t& byte1,
                                                   byte_t& byte2,
                                                   byte_t& byte3,
                                                   byte_t& byte4,
                                                   byte_t& byte5,
                                                   byte_t& byte6,
                                                   byte_t& byte7,
                                                   byte_t& byte8) {
    boost::uint64_t netWorkByteOrder = 0;
    netWorkByteOrder = g_htonll(value);
    byte_t* netWorkByteOrderPtr = (byte_t*) &netWorkByteOrder;
    byte1 = netWorkByteOrderPtr[0];
    byte2 = netWorkByteOrderPtr[1];
    byte3 = netWorkByteOrderPtr[2];
    byte4 = netWorkByteOrderPtr[3];
    byte5 = netWorkByteOrderPtr[4];
    byte6 = netWorkByteOrderPtr[5];
    byte7 = netWorkByteOrderPtr[6];
    byte8 = netWorkByteOrderPtr[7];
}
void Giritech::Utility::convert_from_networkorder_64(boost::uint64_t& value,
                                                     const byte_t& byte1,
                                                     const byte_t& byte2,
                                                     const byte_t& byte3,
                                                     const byte_t& byte4,
                                                     const byte_t& byte5,
                                                     const byte_t& byte6,
                                                     const byte_t& byte7,
                                                     const byte_t& byte8) {
    boost::uint64_t netWorkByteOrder = 0;
    byte_t* netWorkByteOrderPtr( (byte_t*)&netWorkByteOrder);
    netWorkByteOrderPtr[0] = byte1;
    netWorkByteOrderPtr[1] = byte2;
    netWorkByteOrderPtr[2] = byte3;
    netWorkByteOrderPtr[3] = byte4;
    netWorkByteOrderPtr[4] = byte5;
    netWorkByteOrderPtr[5] = byte6;
    netWorkByteOrderPtr[6] = byte7;
    netWorkByteOrderPtr[7] = byte8;
    value = g_ntohll(netWorkByteOrder);
}

boost::uint8_t Giritech::Utility::get_uint_8(const Utility::DataBuffer::APtr& buffer, const long& idx) {
	boost::uint8_t value;
    convert_from_networkorder_8(value, (*buffer)[idx]);
    return value;
}
boost::uint16_t Giritech::Utility::get_uint_16(const Utility::DataBuffer::APtr& buffer, const long& idx) {
	boost::uint16_t value;
    convert_from_networkorder_16(value, (*buffer)[idx], (*buffer)[idx+1]);
    return value;
}
#ifndef ANDROID
boost::uint32_t Giritech::Utility::get_uint_32(const Utility::DataBuffer::APtr& buffer, const long& idx) {
	boost::uint32_t value;
    convert_from_networkorder_32(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3]);
    return value;
}
#endif
boost::uint64_t Giritech::Utility::get_uint_64(const Utility::DataBuffer::APtr& buffer, const long& idx) {
	boost::uint64_t value;
    convert_from_networkorder_64(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3], (*buffer)[idx+4], (*buffer)[idx+5], (*buffer)[idx+6], (*buffer)[idx+7]);
    return value;
}

string Giritech::Utility::get_n_string(const Utility::DataBuffer::APtr& buffer,
                                       const long& idx,
                                       const long& n) {
    string value;
    long i = 0;
    while (i < n) {
        value = value + static_cast<char>((*buffer)[idx+i]);
        i++;
    }
    return value;
}

void Giritech::Utility::set_uint_8(const Utility::DataBuffer::APtr& buffer,
                                   const long& idx,
                                   const boost::uint8_t& value) {
    convert_to_networkorder_8(value, (*buffer)[idx]);
}
void Giritech::Utility::set_uint_16(const Utility::DataBuffer::APtr& buffer,
                                    const long& idx,
                                    const boost::uint16_t& value) {
    convert_to_networkorder_16(value, (*buffer)[idx], (*buffer)[idx+1]);
}
#ifndef ANDROID
void Giritech::Utility::set_uint_32(const Utility::DataBuffer::APtr& buffer,
                                    const long& idx,
                                    const boost::uint32_t& value) {
    convert_to_networkorder_32(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3]);
}
#endif
void Giritech::Utility::set_uint_64(const Utility::DataBuffer::APtr& buffer,
                                    const long& idx,
                                    const boost::uint64_t& value) {
    convert_to_networkorder_64(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3], (*buffer)[idx+4], (*buffer)[idx+5], (*buffer)[idx+6], (*buffer)[idx+7]);
}
void Giritech::Utility::set_n(const Utility::DataBuffer::APtr& buffer,
                              const long& idx,
                              const std::string& value) {
    long i = idx;
    string::const_iterator is(value.begin());
    string::const_iterator isEnd(value.end());
    while (is != isEnd) {
        set_uint_8(buffer, i, static_cast<byte_t>(*is));
        is++;
        i++;
    }
}
