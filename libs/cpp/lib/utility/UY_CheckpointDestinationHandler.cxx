/*! \file UY_CheckpointDestinationHandler.cxx
    \brief This file contains implementation of classes for handling device output
*/
#include <boost/bind.hpp>
#include "boost/filesystem.hpp"

#include <lib/utility/UY_CheckpointDestinationHandler.hxx>

using namespace Giritech::Utility;
using namespace std;


/*
   ------------------------------------------------------------------
   Implementation CheckpointDestinationHandlerCout
   ------------------------------------------------------------------
*/
CheckpointDestinationHandlerCout::CheckpointDestinationHandlerCout(void) {
}
CheckpointDestinationHandler::APtr CheckpointDestinationHandlerCout::create(void) {
  return CheckpointDestinationHandler::APtr(new CheckpointDestinationHandlerCout);
}
std::ostream& CheckpointDestinationHandlerCout::get_os(void) {
  return cout;
}

void CheckpointDestinationHandlerCout::close(void) {
}

void CheckpointDestinationHandlerCout::flush(void) {
    cout.flush();
}

boost::filesystem::path CheckpointDestinationHandlerCout::get_foldername(void) {
	return boost::filesystem::current_path();
}


/*
   ------------------------------------------------------------------
   Implementation CheckpointDestinationHandlerFile
   ------------------------------------------------------------------
*/
#if !defined(ANDROID)
CheckpointDestinationHandlerFile::CheckpointDestinationHandlerFile(const boost::filesystem::path& filepath, const bool append, const bool close_on_flush)
	: filepath_(filepath), append_(append), file_created_(false), file_open_(false), close_on_flush_(close_on_flush) {
}
CheckpointDestinationHandler::APtr CheckpointDestinationHandlerFile::create(const boost::filesystem::path& filepath, const bool append, const bool close_on_flush) {
  return CheckpointDestinationHandler::APtr(new CheckpointDestinationHandlerFile(filepath, append, close_on_flush));
}
std::ostream& CheckpointDestinationHandlerFile::get_os(void) {
	try {
		if(!file_created_) {
		  file_created_ = true;
		  if (append_) {
			  file_ = boost::shared_ptr<boost::filesystem::ofstream>(new boost::filesystem::ofstream(filepath_, std::ios::out|std::ios::app));
		  }
		  else {
			  file_ = boost::shared_ptr<boost::filesystem::ofstream>(new boost::filesystem::ofstream(filepath_, std::ios::out));
		  }
		  file_open_ = true;
	  }
	  if(!file_open_) {
		  if(boost::filesystem::exists(filepath_)) {
			  file_ = boost::shared_ptr<boost::filesystem::ofstream>(new boost::filesystem::ofstream(filepath_, std::ios::out|std::ios::app));
			  file_open_ = true;
		  }
		  else {
			  return cout;
		  }
	  }
	  assert(file_);
	  return *file_;
	}
	catch (...) {
		  return cout;
	}
}

void CheckpointDestinationHandlerFile::close(void) {
	try {
		if(file_open_) {
			file_->close();
			file_open_ = false;
		}
	}
	catch (...) {
	}
}

void CheckpointDestinationHandlerFile::flush(void) {
	try {
		if(file_open_) {
			file_->flush();
			if(close_on_flush_) {
				file_->close();
				file_open_ = false;
			}
		}
	}
	catch (...) {
	}
}

boost::filesystem::path CheckpointDestinationHandlerFile::get_foldername(void) {
	return filepath_.parent_path();
}

#endif
