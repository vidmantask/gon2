/*! \file UY_MutexLock.cxx
 \brief This file contains the implementation of the MutexLock class
 */
#include <stack>
#include <map>

#include <lib/utility/UY_MutexLock.hxx>

#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>



using namespace Giritech::Utility;
using namespace std;

/*
 * ------------------------------------------------------------------
 * Mutex implementation
 * ------------------------------------------------------------------
 */
Mutex::Mutex(boost::asio::io_service& io, const std::string& mutex_id) : mutex_id_(mutex_id), strand_(io), io_(io) {
	// Optimization, will make push_back faster
	mutex_holder_ids_.reserve(15);
}

Mutex::~Mutex(void) {
}

std::string Mutex::get_mutex_id(void) const {
    return mutex_id_;
}

boost::asio::io_service::strand& Mutex::get_strand(void) {
	return strand_;
}

void Mutex::set_mutex_id(const std::string& mutex_id) {
	boost::recursive_mutex::scoped_lock update_mutex_lock(update_mutex_);
	mutex_id_ = mutex_id;
}

void Mutex::set_mutex_holder_id(const std::string& holder_id) {
	boost::recursive_mutex::scoped_lock update_mutex_lock(update_mutex_);
    mutex_holder_ids_.push_back(holder_id);
		mutex_holder_thread_id_ =  boost::lexical_cast<std::string>(boost::this_thread::get_id());;
}

void Mutex::reset_mutex_holder_id(void) {
	boost::recursive_mutex::scoped_lock update_mutex_lock(update_mutex_);
    mutex_holder_ids_.erase(mutex_holder_ids_.end()-1);
}

std::string Mutex::get_mutex_holder_ids(void) {
    stringstream result;

    boost::recursive_mutex::scoped_lock update_mutex_lock(update_mutex_);
	result << "[";
	std::vector<std::string>::const_iterator i(mutex_holder_ids_.begin());
	while(i !=  mutex_holder_ids_.end()) {
		result << (*i);
		result << "<" << mutex_holder_thread_id_ << ">";
		result << ", ";
		++i;
	}
	result << "]";
	return result.str();
}

Mutex::APtr Mutex::create(boost::asio::io_service& io, const std::string& mutex_id) {
    return Mutex::APtr(new Mutex(io, mutex_id));
}

bool Mutex::lock(const unsigned long& minutes) {
  bool rc(mutex_.timed_lock(boost::posix_time::minutes(minutes)));
  return rc;
}

bool Mutex::lock(void) {
  bool rc(mutex_.timed_lock(boost::posix_time::seconds(10)));
  return rc;
}

void Mutex::unlock(void) {
    mutex_.unlock();
}

void Mutex::safe_update(Mutex::APtr& mutex, const Mutex::APtr& mutex_new) {
  bool done(false);

  Mutex::APtr mutex_old(mutex);
  if(mutex_old->lock()) {
    mutex_old->set_mutex_holder_id("safe_update");
    if(mutex_new->lock()) {
      mutex_new->set_mutex_holder_id("safe_update");
      mutex = mutex_new;
      done = true;
      mutex_new->reset_mutex_holder_id();
      mutex_new->unlock();
    }
    mutex_old->reset_mutex_holder_id();
    mutex_old->unlock();
  }
  if(!done) {
    std::cout << "XXXXXXXXXXXXXXXXXXXXXX safe_update failed" << std::endl;
    safe_update(mutex, mutex_new);
  }
}

/*
 * ------------------------------------------------------------------
 * MutexScopeLock implementation
 * ------------------------------------------------------------------
*/
std::map< std::string, std::stack< Mutex::APtr> > global_mutex_map;
boost::recursive_mutex global_mutex_map_update_mutex;

std::string get_thread_id(void) {
    stringstream ss;
		ss << boost::lexical_cast<std::string>(boost::this_thread::get_id());
    return ss.str();
}

void global_mutex_map_push(const Mutex::APtr& mutex) {
	std::string thread_id = get_thread_id();
	global_mutex_map_update_mutex.lock();
	std::map< std::string, std::stack<Mutex::APtr> >::iterator i(global_mutex_map.find(thread_id));
	if(i != global_mutex_map.end()) {
		(i->second).push(mutex);
	}
	else {
		std::stack<Mutex::APtr> mutexes;
		mutexes.push(mutex);
		global_mutex_map.insert(make_pair(thread_id, mutexes));
	}
	global_mutex_map_update_mutex.unlock();
}

void global_mutex_map_pop(const Mutex::APtr& mutex) {
	std::string thread_id = get_thread_id();
	global_mutex_map_update_mutex.lock();
	std::map< std::string, std::stack<Mutex::APtr> >::iterator i(global_mutex_map.find(thread_id));
	if(i != global_mutex_map.end()) {
		(i->second).pop();
	}
	global_mutex_map_update_mutex.unlock();
}

void global_mutex_map_dump(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, const CheckpointAttr_Module::APtr& checkpoint_module) {
	Checkpoint cp(*checkpoint_handler,"MutexLock::mutex_map_header.begin", checkpoint_module, CpAttr_error());
	global_mutex_map_update_mutex.lock();
	std::map< std::string, std::stack<Mutex::APtr> >::const_iterator i(global_mutex_map.begin());
	while(i != global_mutex_map.end()) {
		std::stack<Mutex::APtr> mutexes(i->second);
		while (!mutexes.empty()) {
			Mutex::APtr mutex(mutexes.top());
			Checkpoint cp(*checkpoint_handler,"MutexLock::mutex_map", checkpoint_module, CpAttr_error(), CpAttr("thread_id_element", i->first), CpAttr("mutex_id", mutex->get_mutex_id()));
			mutexes.pop();
		}
		i++;
	}
	global_mutex_map_update_mutex.unlock();
	Checkpoint cp_done(*checkpoint_handler,"MutexLock::mutex_map_header.done", checkpoint_module, CpAttr_error());
}

MutexScopeLock::MutexScopeLock(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, const CheckpointAttr_Module::APtr& checkpoint_module, const Mutex::APtr& mutex, const std::string& id)
    : checkpoint_handler_(checkpoint_handler), checkpoint_module_(checkpoint_module), mutex_(mutex), id_(id), has_lock_(false) {
  //    Checkpoint cp2x(*checkpoint_handler_,"MutexLock::get", checkpoint_module_, CpAttr_debug(), CpAttr("id", id_) );
    if(! mutex->lock()) {
        Checkpoint cp2(*checkpoint_handler_,"MutexLock::lock_timeout", checkpoint_module_, CpAttr_warning(), CpAttr("id", id_), CpAttr("mutex_id", mutex_->get_mutex_id()), CpAttr("mutex_holder_ids", mutex_->get_mutex_holder_ids()) );
        if(! mutex->lock(3)) {
            Checkpoint cp2(*checkpoint_handler_,"MutexLock::lock_timeout.giving_up", checkpoint_module_, CpAttr_critical(), CpAttr("id", id_), CpAttr("mutex_id", mutex_->get_mutex_id()), CpAttr("mutex_holder_ids", mutex_->get_mutex_holder_ids()) );
	    //            global_mutex_map_dump(checkpoint_handler, checkpoint_module);
            throw ExceptionMutexLockTimeout(id_);
        }
        Checkpoint cp3(*checkpoint_handler_,"MutexLock::lock_timeout_1.got_it", checkpoint_module_, CpAttr_warning(), CpAttr("id", id_), CpAttr("mutex_id", mutex_->get_mutex_id()), CpAttr("mutex_holder_ids", mutex_->get_mutex_holder_ids()) );
    }
    //    global_mutex_map_push(mutex);
    mutex_->set_mutex_holder_id(id);
}

MutexScopeLock::~MutexScopeLock(void) {
  //    Checkpoint cp(*checkpoint_handler_,"MutexLock::release", checkpoint_module_, CpAttr_debug(), CpAttr("id", id_) );
    assert(mutex_.get() != NULL);
    global_mutex_map_pop(mutex_);
    mutex_->reset_mutex_holder_id();
    mutex_->unlock();
}


/*
 * ------------------------------------------------------------------
 * MutexScopeLockAnonyme implementation
 * ------------------------------------------------------------------
*/
MutexScopeLockAnonyme::MutexScopeLockAnonyme(const Mutex::APtr& mutex)
	: mutex_(mutex) {
    if(! mutex->lock()) {
        if(! mutex->lock(5)) {
            throw ExceptionMutexLockTimeout("Anonyme");
        }
    }
    mutex_->set_mutex_holder_id("anonyme");
}

MutexScopeLockAnonyme::~MutexScopeLockAnonyme(void) {
    assert(mutex_.get() != NULL);
    mutex_->reset_mutex_holder_id();
    mutex_->unlock();
}
