/*! \file UY_MutexLock.hxx
    \brief This file contains classes for a recursive mutex lock
*/
#ifndef UY_MutexLock_HXX
#define UY_MutexLock_HXX

#include <vector>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/asio.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_Exception.hxx>

namespace Giritech {
  namespace Utility {

  /*! \brief Exception thrown if mutex lock timesout
   */
  class ExceptionMutexLockTimeout : public Giritech::Utility::Exception {
      public:
      ExceptionMutexLockTimeout(const std::string& message) :
              Giritech::Utility::Exception(message) {
          }
  };


  class Mutex : boost::noncopyable {
  public:
      typedef boost::shared_ptr<Mutex> APtr;

      ~Mutex(void);
      static APtr create(boost::asio::io_service& io, const std::string& mutex_id_);

      bool lock(void);
      bool lock(const unsigned long& seconds);
      void unlock(void);

      static void safe_update(Mutex::APtr& mutex, const Mutex::APtr& mutex_new);

      void set_mutex_id(const std::string& mutex_id);
      std::string get_mutex_id(void) const;

      void set_mutex_holder_id(const std::string& holder_id);
      void reset_mutex_holder_id(void);
      std::string get_mutex_holder_ids(void);

      boost::asio::io_service::strand& get_strand(void);

  private:
      Mutex(boost::asio::io_service& io, const std::string& mutex_id_);
      boost::asio::io_service& io_;
      std::string mutex_id_;
      std::vector<std::string> mutex_holder_ids_;
      std::string mutex_holder_thread_id_;
      boost::recursive_timed_mutex mutex_;
      boost::recursive_mutex update_mutex_;
      boost::asio::io_service::strand strand_;

  };


  class MutexScopeLock : boost::noncopyable {
  public:
      MutexScopeLock(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, const Giritech::Utility::CheckpointAttr_Module::APtr& checkpoint_module,
                     const Mutex::APtr& mutex, const std::string& id);
      ~MutexScopeLock(void);
  private:
      Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
      Giritech::Utility::CheckpointAttr_Module::APtr checkpoint_module_;
      Mutex::APtr mutex_;
      std::string id_;
      bool has_lock_;
  };


  class MutexScopeLockAnonyme : boost::noncopyable {
  public:
	  MutexScopeLockAnonyme(const Mutex::APtr& mutex);
      ~MutexScopeLockAnonyme(void);
  private:
      Mutex::APtr mutex_;
      bool has_lock_;
  };


  } // End namespace, Utility
} // End Namespace, Giritech
#endif
