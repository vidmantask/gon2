/*! \file UY_CheckpointOutputHandler.cxx
    \brief This file contains implementation of classes for handling accepted checkpoints in a checkpoint handler
*/
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>

#include <lib/utility/UY_CheckpointOutputHandler.hxx>

using namespace Giritech::Utility;
using namespace std;


/*
   ------------------------------------------------------------------
   Implementation CheckpointOutputHandler
   ------------------------------------------------------------------
*/
CheckpointOutputHandler::CheckpointOutputHandler(void)
: checkpointDesitnationHandler_(CheckpointDestinationHandlerCout::create()) {
}

CheckpointOutputHandler::CheckpointOutputHandler(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler)
: checkpointDesitnationHandler_(checkpointDesitnationHandler) {
}

void CheckpointOutputHandler::set_destination_handler(CheckpointDestinationHandler::APtr checkpointDesitnationHandler) {
	checkpointDesitnationHandler_ = checkpointDesitnationHandler;
}

CheckpointDestinationHandler::APtr CheckpointOutputHandler::get_destination_handler(void) {
	return checkpointDesitnationHandler_;
}



/*
   ------------------------------------------------------------------
   Implementation CheckpointOutputHandlerXML
   ------------------------------------------------------------------
*/
CheckpointOutputHandlerXML::CheckpointOutputHandlerXML(void)
  : CheckpointOutputHandler() {
}
CheckpointOutputHandlerXML::CheckpointOutputHandlerXML(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler)
  : CheckpointOutputHandler(checkpointDesitnationHandler) {
}

void CheckpointOutputHandlerXML::close(void) {
	checkpointDesitnationHandler_->close();
}

CheckpointOutputHandlerXML::APtr CheckpointOutputHandlerXML::create(void) {
  return CheckpointOutputHandlerXML::APtr(new CheckpointOutputHandlerXML);
}
CheckpointOutputHandlerXML::APtr CheckpointOutputHandlerXML::create(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler) {
  return CheckpointOutputHandlerXML::APtr(new CheckpointOutputHandlerXML(checkpointDesitnationHandler));
}

void CheckpointOutputHandlerXML::print_attr(const CheckpointAttr::APtrs::value_type& attr) {
  boost::recursive_timed_mutex::scoped_lock lock(mutex_session_instance);
  if(attr.first !="content") {
	  checkpointDesitnationHandler_->get_os() << " " << attr.first << "=\"" << attr.second->to_string() << "\"";
	  checkpointDesitnationHandler_->flush();
  }
}

void CheckpointOutputHandlerXML::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  boost::recursive_timed_mutex::scoped_lock lock(mutex_session_instance);
  checkpointDesitnationHandler_->get_os() << "<cp checkpoint_id=\"" << checkpoint_id << "\"";
  checkpointDesitnationHandler_->flush();
  for_each(checkpoint_attrs.begin(), checkpoint_attrs.end(), boost::bind(&CheckpointOutputHandlerXML::print_attr, this, _1));
  checkpointDesitnationHandler_->get_os() << ">" << endl;
  checkpointDesitnationHandler_->flush();

  CheckpointAttr::APtrs::const_iterator i_content(checkpoint_attrs.find("content"));
  if(i_content != checkpoint_attrs.end()) {
	  string content((*i_content).second->to_string());
	  boost::replace_first(content, "<", "&lt;");
	  checkpointDesitnationHandler_->get_os() << content;
  }
  checkpointDesitnationHandler_->get_os() << "</cp>" << endl;
  checkpointDesitnationHandler_->flush();
}



/*
   ------------------------------------------------------------------
   Implementation CheckpointOutputHandlerTEXT
   ------------------------------------------------------------------
*/
CheckpointOutputHandlerTEXT::CheckpointOutputHandlerTEXT(void)
  : CheckpointOutputHandler() {
}
CheckpointOutputHandlerTEXT::CheckpointOutputHandlerTEXT(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler)
  : CheckpointOutputHandler(checkpointDesitnationHandler) {
}
void CheckpointOutputHandlerTEXT::close(void) {
	assert(checkpointDesitnationHandler_);
	checkpointDesitnationHandler_->close();
}

CheckpointOutputHandlerTEXT::APtr CheckpointOutputHandlerTEXT::create(void) {
  return CheckpointOutputHandlerTEXT::APtr(new CheckpointOutputHandlerTEXT);
}
CheckpointOutputHandlerTEXT::APtr CheckpointOutputHandlerTEXT::create(const CheckpointDestinationHandler::APtr& checkpointDesitnationHandler) {
  return CheckpointOutputHandlerTEXT::APtr(new CheckpointOutputHandlerTEXT(checkpointDesitnationHandler));
}

void CheckpointOutputHandlerTEXT::print_attr(const CheckpointAttr::APtrs::value_type& attr) {
  boost::recursive_timed_mutex::scoped_lock lock(mutex_session_instance);
  if(attr.first != "type" && attr.first !="ts" && attr.first !="thread_id" && attr.first !="module" && attr.first !="content") {
    checkpointDesitnationHandler_->get_os() << " " << attr.first << "=\"" << attr.second->to_string() << "\"";
  }
}


void CheckpointOutputHandlerTEXT::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  boost::recursive_timed_mutex::scoped_lock lock(mutex_session_instance);
  CheckpointAttr::APtrs::const_iterator i_end(checkpoint_attrs.end());

  stringstream ss;


  /* Get timestamp attiobute */
  CheckpointAttr::APtrs::const_iterator i_ts(checkpoint_attrs.find("ts"));
  if(i_ts != i_end) {
    ss << std::setw(22) << (*i_ts).second->to_string() << " ";
  }

  /* Get thread_id attibute */
  CheckpointAttr::APtrs::const_iterator i_thread_id(checkpoint_attrs.find("thread_id"));
  if(i_thread_id != i_end) {
      ss << std::setw(10) << std::left << (*i_thread_id).second->to_string().substr(0, 9);
  }

  /* Get type attribute */
  CheckpointAttr::APtrs::const_iterator i_type(checkpoint_attrs.find("type"));
  if(i_type != i_end) {
    ss << std::setw(5) << std::left << (*i_type).second->to_string().substr(0,4);
  }
  else {
    ss << std::setw(5) << " ";
  }

  string checkpoint_id_with_module;
  CheckpointAttr::APtrs::const_iterator i_module(checkpoint_attrs.find("module"));
   if(i_module != i_end) {
	   checkpoint_id_with_module = (*i_module).second->to_string() + "(" + checkpoint_id + ")";
   }
   else {
	   checkpoint_id_with_module = checkpoint_id;
   }
   ss << std::setw(50) << std::left << checkpoint_id_with_module << " ";

   checkpointDesitnationHandler_->get_os() << ss.str();
   for_each(checkpoint_attrs.begin(), checkpoint_attrs.end(), boost::bind(&CheckpointOutputHandlerTEXT::print_attr, this, _1));
   checkpointDesitnationHandler_->get_os() << endl;

   CheckpointAttr::APtrs::const_iterator i_content(checkpoint_attrs.find("content"));
   if(i_content != checkpoint_attrs.end()) {
	   string content((*i_content).second->to_string());

	   std::vector<std::string> content_lines;
	   boost::split(content_lines, content, boost::is_any_of("\n"));

	   std::vector<std::string>::const_iterator cl_i(content_lines.begin());
	   while(cl_i != content_lines.end()) {
		   checkpointDesitnationHandler_->get_os() << ss.str() << " " << (*cl_i) << endl;;
		   ++cl_i;
		}
   }
   checkpointDesitnationHandler_->flush();
}


/*
   ------------------------------------------------------------------
   Implementation CheckpointOutputHandlerFolderXML
   ------------------------------------------------------------------
*/
#if !defined(ANDROID)
CheckpointOutputHandlerFolderXML::CheckpointOutputHandlerFolderXML(const boost::filesystem::path& folder)
: CheckpointOutputHandler(), folder_(folder) {
}

void CheckpointOutputHandlerFolderXML::close(void) {
}

CheckpointOutputHandlerFolderXML::APtr CheckpointOutputHandlerFolderXML::create(const boost::filesystem::path& folder) {
  return CheckpointOutputHandlerFolderXML::APtr(new CheckpointOutputHandlerFolderXML(folder));
}

void CheckpointOutputHandlerFolderXML::print_attr(const CheckpointAttr::APtrs::value_type& attr) {
}

void CheckpointOutputHandlerFolderXML::operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
  boost::recursive_timed_mutex::scoped_lock lock(mutex_session_instance);

  /* Get timestamp attribute */
  string timestamp;
  CheckpointAttr::APtrs::const_iterator i_ts(checkpoint_attrs.find("ts"));
  if(i_ts != checkpoint_attrs.end()) {
    timestamp = (*i_ts).second->to_string();
    boost::replace_first(timestamp, ".", "_");
  }
  stringstream ss;
  ss << "cp_" << timestamp << ".xml";
  string cp_filename = ss.str();

  CheckpointDestinationHandler::APtr output_handler(CheckpointDestinationHandlerFile::create(folder_ / cp_filename, false, false));

  output_handler->get_os() << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << endl;;
  output_handler->get_os() << "<cp checkpoint_id=\"" << checkpoint_id << "\"";

  for (const auto& pair : checkpoint_attrs) {
  if (pair.first != "content") {
		  output_handler->get_os() << " " << pair.first << "=\"" << pair.second->to_string() << "\"";
	  }
  }
  output_handler->get_os() << ">" << endl;

  CheckpointAttr::APtrs::const_iterator i_content(checkpoint_attrs.find("content"));
  if(i_content != checkpoint_attrs.end()) {
	  string content((*i_content).second->to_string());
	  boost::replace_first(content, "<", "&lt;");
	  output_handler->get_os() << content;
  }
  output_handler->get_os() << "</cp>" << endl;
  output_handler->close();
}
#endif
