/*! \file UY_DataBuffer.hxx
 \brief This file contains classes for data buffer functionality
 */
#ifndef UY_DATABUFFER_HXX
#define UY_DATABUFFER_HXX

#include <string>
#include <vector>
#include <boost/utility.hpp>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Config.hxx>
#include <lib/utility/UY_Types.hxx>
#include <lib/utility/UY_Exception.hxx>

namespace Giritech {
namespace Utility {

/* Forward declaration */
class DataBufferManaged;
class DataBufferUnmanaged;

/*! \brief Abstract class for representing a data buffer.

 */
class UTILITY_DLL DataBuffer : boost::noncopyable {
public:

    /*! \brief This exception is thrown if a seperator is not found
     */
    class Exception_Seperator_not_found : public Giritech::Utility::Exception {
public:
        Exception_Seperator_not_found(const std::string& message) :
            Exception(message) {
        }
    };

    /*! \brief This exception is thrown if buffer positions are invalid
     */
    class Exception_Out_of_bound : public Giritech::Utility::Exception {
public:
        Exception_Out_of_bound(const std::string& message) :
            Exception(message) {
        }
    };

    /*! \brief This exception is thrown if a buffer is not sutable for decoding
     */
    class Exception_decoding : public Giritech::Utility::Exception {
public:
        Exception_decoding(const std::string& message) :
            Exception(message) {
        }
    };

    /*! \brief This exception is thrown if a buffer is not sutable for encoding
     */
    class Exception_encoding : public Giritech::Utility::Exception {
public:
        Exception_encoding(const std::string& message) :
            Exception(message) {
        }
    };


    typedef boost::shared_ptr<DataBuffer> APtr;

    /*! \brief Destructor
     */
    virtual ~DataBuffer(void) {
    }
    ;

    /*! \brief Get size of allocated buffer
     */
    virtual long getSize(void) const = 0;

    virtual byte_t& operator[](const long idx) = 0;
    virtual const byte_t& operator[](const long idx) const = 0;
    virtual bool operator==(const DataBuffer& other) const;
    virtual bool operator!=(const DataBuffer& other) const;

    /*! \brief Get access to an element inside buffer

     No range checking is done.
     */
    virtual byte_t* data(void) {
        return &(*this)[0];
    }

    virtual const byte_t* data(void) const {
        return &(*this)[0];
    }

    /*! \brief Parse a buffer looking for the seperator
     \param seperator Seperator value to search for
     \param value     Value buffer found
     \param startPos  Where to start looking in the buffer
     \return First position after the found seperator

     When the seperator is found the found value is returned,
     and the first posision after the found seperator is returned.

     If no start position is given then you are looking from the begining.
     If no seperator specified the the remaing buffer is returned.

     If the seperator is not found then the Exception_Seperator_not_found is thrown.
     */
    long parse(const std::string& seperator,
               boost::shared_ptr<DataBufferManaged>& value,
               const long startPos);
    long parse(const std::string& seperator, boost::shared_ptr<DataBufferManaged>& value);
    void parse(boost::shared_ptr<DataBufferManaged>& value);
    void parse(boost::shared_ptr<DataBufferManaged>& value, const long startPos);
    void parse(long& value, const long prefixByteSize);
    void parse(long& value, const long prefixByteSize, const long startPos);
    void parse(bool& value, const long start_pos);

    long parseWithLen(boost::shared_ptr<DataBufferManaged>& value, const long prefixByteSize);
    long parseWithLen(boost::shared_ptr<DataBufferManaged>& value,
                      const long prefixByteSize,
                      const long startPos);

    /*! \brief Hex encode buffer

     Returns a new hex encoded buffer.
     */
    boost::shared_ptr<DataBufferManaged> encodeHex(void) const;

    /*! \brief Hex decode buffer

     Returns a new hex decoded buffer.
     IF the buffer not can be hex decoded then the exception Exception_Invalid_hex_buffer is thrown.
     */
    boost::shared_ptr<DataBufferManaged> decodeHex(void) const;

    /*! \brief Conovert the buffer to a string
     */
    std::string toString(void) const;

    /*! \brief burn

     Zerorize buffer on request. Do not change the buffer len only overwrites all content with zerrors.
     */
    void burn(void);

    /*! \brief Select that the buffer should be burned on destruction
     */
    void autoBurn(void) {
        autoBurn_ = true;
    }

    /*! \brief Clone the buffer
     */
    virtual boost::shared_ptr<DataBufferManaged> clone(void) const = 0;

protected:
    /*! \brief Constructor
     */
    DataBuffer(void) :
        autoBurn_(false) {
    }
    ;

private:
    byte_t decodeHex(const byte_t* buffer) const;

protected:
    bool autoBurn_;
};

/*! \brief A data buffer with managed buffer.

 This class is a managed data buffer, that is it handeles allocation and deallocation of data buffer of the requested size.
 */
class UTILITY_DLL DataBufferManaged : public virtual DataBuffer {
public:
    typedef boost::shared_ptr<DataBufferManaged> APtr;

    /*! \brief Constructor
     */
    DataBufferManaged(const long size) :
        rawBuffer_(size, 0) {
    }
    DataBufferManaged(void) {
    }

    DataBufferManaged(std::istream& is, const size_t& size);

    /*! \brief Destructor
     */
    virtual ~DataBufferManaged(void) {
        if (autoBurn_) {
            burn();
        }
    };

    /*! \brief eatFront
     *
     * Eat a number of bytes from the beginning
     *
     */
    void eatFront(const unsigned long count);


    /*! \brief Get access to an element inside buffer

     No range checking is done.
     */
    virtual const byte_t& operator[](const long idx) const {
        return rawBuffer_[idx];
    }
    virtual byte_t& operator[](const long idx) {
        return rawBuffer_[idx];
    }

    /*! \brief Get size of allocated buffer
     */
    long getSize(void) const {
        return rawBuffer_.size();
    }

    /*! \brief Clear buffer
     */
    void clear(void) {
    	rawBuffer_.clear();
    }

    /*! \brief Resize the buffer

     Resize the buffer.
     If fill is used and the buffer grows the fill is used in new elements, else the zero value is used.
     */
    void resize(const long size, const byte_t fill);
    void resize(const long size);

    /*! \brief Add padding to buffer to match modolu block size.

     Padding is added acording to PKCS (block cipher padding scheme.
     */
    void pkcsRemove(const long blockSize);

    /*! \brief Remove padding from buffer

     Padding is removed acording to PKCS (block cipher padding scheme.
     */
    void pkcsAdd(const long blockSize);

    /*! \brief Clone the buffer

     Makes a deep copy of the databuffer. The new buffer is a managed data buffer.
     */
    DataBufferManaged::APtr clone(void) const;

    /*! \brief Append to the buffer
     */
    void append(const DataBufferManaged::APtr& buffer);
    void append(const bool value);
    void append(const long buffer, const long prefixByteSize);
    void appendInteger(const long value);

    void dump_internal_info(void) const;

    template<class T> void append(const T& buffer) {
        DataBufferManaged::APtr bufferToAppend(DataBufferManaged::create(buffer));
        append(bufferToAppend);
    }

    template<class T> class Appender {
public:
        Appender(DataBufferManaged::APtr& buffer) :
            buffer_(buffer), seperator_(""), containerSize_(0) {
        }
        Appender(DataBufferManaged::APtr& buffer,
                 const int& containerSize,
                 const std::string& seperator) :
            buffer_(buffer), seperator_(seperator), containerSize_(containerSize) {
        }
        void operator()(const T& toAppend) {
            buffer_->append(toAppend);
            --containerSize_;
            if (containerSize_ > 0) {
                buffer_->append(seperator_);
            }
        }
        long getSize(void) {
            return buffer_->getSize();
        }
private:
        DataBufferManaged::APtr& buffer_;
        std::string seperator_;
        int containerSize_;
    };

    /*! \brief Append to the buffer from a container of buffers
     */
    template<class T> void appendContainer(const T& bufferContainer) {
        Appender<typename T::value_type> appender(DataBufferManaged::APtr(this));
        for_each(bufferContainer.begin(), bufferContainer.end(), appender);
    }

    /*! \brief Append with len inditaion in front of the buffer
     \param buffer buffer to append
     \param prefixByteSize Size of prefix in bytes

     If the size of the buffer can not be fitted into the prefix the an exception is thrown
     */
    template<class T> void appendWithLen(const T& buffer, const long prefixByteSize) {
        DataBufferManaged::APtr bufferToAppend(DataBufferManaged::create(buffer));
        long bufferSize = bufferToAppend->getSize();
        append(bufferSize, prefixByteSize);
        append(bufferToAppend);
    }

    /*! \brief Create managed instantce of the managed data buffer
     */
    static APtr create(const long size);
    static APtr create(const char* rawBuffer, const long size);
    static APtr create(const byte_t* rawBuffer, const long size);
    static APtr create(const std::string& stringBuffer);
    static APtr create(const std::string& stringBuffer, const long size);
    static APtr create(const DataBuffer::APtr& buffer);
    static APtr create(const long size, const byte_t fill);
    static APtr create(std::istream& is, const size_t& size);
    static APtr create_empty(const long size);
    static APtr create(const std::vector<byte_t>::const_iterator& i, const unsigned long size);




    /*! \brief Get access to raw buffer
     Please use this with care because it might change.
     It has been used for letting asio-functionlaity creating filling a existing buffer,
     without making DataBuffer dependend of asio.
     */
    std::vector<byte_t>& get_raw_buffer(void) {
        return rawBuffer_;
    }

private:
    std::vector<byte_t> rawBuffer_;
};

/*! \brief A data buffer with unmanged buffer.

 This class is a unmanaged data buffer, that is it the buffers allocation and deallocation is handled by the caller.
 Please only use this class if you are shure of what you are doing.

 The class is for use when calling with buffers over language borders, e.g. delphi/C++,
 where differen memmory managers are used.
 */
class UTILITY_DLL DataBufferUnmanaged : public virtual DataBuffer {
public:
    typedef boost::shared_ptr<DataBufferUnmanaged> APtr;

    /*! \brief Destructor
     */
    virtual ~DataBufferUnmanaged(void) {
        if (autoBurn_) {
            burn();
        }
    }
    ;

    /*! \brief Get access to an element inside buffer

     No range checking is done.
     */
    virtual const byte_t& operator[](const long idx) const {
        return rawBuffer_[idx];
    }
    virtual byte_t& operator[](const long idx) {
        return rawBuffer_[idx];
    }

    /*! \brief Get size of allocated buffer
     */
    long getSize(void) const {
        return size_;
    }
    ;

    /*! \brief Clone the buffer

     Makes a deep copy of the databuffer. The new buffer is a managed data buffer.
     */
    DataBufferManaged::APtr clone(void) const;

    /*! \brief Create managed instantce of the data buffer

     Notice that this does not make the internal data buffer managed.
     */
    static APtr create(const long size, byte_t* rawBuffer);

private:
    /*! \brief Constructor
     */
    DataBufferUnmanaged(const long size, byte_t* rawBuffer) :
        size_(size), rawBuffer_(rawBuffer) {
    }
private:
    long size_;
    byte_t* rawBuffer_;
};

} // End namespace, Utility
} // End Namespace, Giritech
#endif
