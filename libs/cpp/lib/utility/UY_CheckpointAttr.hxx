/*! \file UY_CheckpointAttr.hxx
 \brief This file contains classes for checkpoints attributes
 */
#ifndef UY_CHECKPOINTATTR_HXX
#define UY_CHECKPOINTATTR_HXX

#include <string>
#include <map>
//#include <thread>

#include <boost/shared_ptr.hpp>
#include <boost/utility.hpp>
#include <boost/ref.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>

#include <lib/utility/UY_Exception.hxx>

namespace Giritech {
namespace Utility {

/*! \brief Abstract class a checkpoint attribute
 */
class CheckpointAttr : boost::noncopyable {
public:
    typedef boost::shared_ptr<CheckpointAttr> APtr;
    typedef std::map<std::string, APtr> APtrs;

    enum Structure {
        Structure_begin_end = 0,
        Structure_begin,
        Structure_end
    };

    virtual ~CheckpointAttr(void);

    const std::string& get_id(void) const;
    virtual const std::string to_string(void) const = 0;
    virtual bool equal(const CheckpointAttr::APtr& attr) const = 0;

protected:
    CheckpointAttr(const std::string& id);

private:
    std::string id_;
};

/*! \brief Class representing a checkpoint attribute with a string value
 */
class CheckpointAttrS : public CheckpointAttr {
public:
    const std::string to_string(void) const;
    bool equal(const CheckpointAttr::APtr& attr) const;

    std::string get_value(void) const;

    static APtr create(const std::string& id, const std::string& value);

    static CheckpointAttrS* lookup(const APtrs& attrs, const std::string& attr_id);

protected:
    CheckpointAttrS(const std::string& id, const std::string& value);

private:
    std::string value_;
};



/*! \brief Class representing a checkpoint attribute with a integer value
 */
class CheckpointAttrI : public CheckpointAttr {
public:
    virtual ~CheckpointAttrI(void);

    virtual const std::string to_string(void) const;
    virtual bool equal(const CheckpointAttr::APtr& attr) const;

    long get_value(void) const;

    static CheckpointAttr::APtr create(const std::string& id, const long value);

    static CheckpointAttrI* lookup(const APtrs& attrs, const std::string& attr_id);

protected:
    CheckpointAttrI(const std::string& id, const long value);

private:
    long value_;
};

/*! \brief Class representing a checkpoint attribute with a timestamp
 */
class CheckpointAttrTS : public CheckpointAttr {
public:
    const std::string to_string(void) const;
    bool equal(const CheckpointAttr::APtr& attr) const;

    const boost::posix_time::ptime& get_value(void) const;

    static APtr create(void);

private:
    CheckpointAttrTS(void);
    boost::posix_time::ptime value_;
};

/*! \brief Class representing a checkpoint attribute with a time durration
 */
class CheckpointAttrTD : public CheckpointAttr {
public:
    const std::string to_string(void) const;
    bool equal(const CheckpointAttr::APtr& attr) const;

    static APtr create(const boost::posix_time::time_duration& value);

    const boost::posix_time::time_duration& get_value(void) const;

    static CheckpointAttrTD* lookup(const APtrs& attrs, const std::string& attr_id);
private:
    CheckpointAttrTD(const boost::posix_time::time_duration& value);
    boost::posix_time::time_duration value_;
};

/*! \brief Class representing a checkpoint attribute with the thread_id
 */
class CheckpointAttrThreadId : public CheckpointAttr {
public:
    const std::string to_string(void) const;
    bool equal(const CheckpointAttr::APtr& attr) const;

    std::string get_value(void) const;

    static APtr create(void);

private:
    CheckpointAttrThreadId(void);
    std::string value_;
};

/*! \brief Class representing a scope reference attribute
 */
class CheckpointAttrScope : public CheckpointAttrI {
public:
    static CheckpointAttr::APtr create(const Structure structure, const boost::uint64_t& scope_id);

private:
    CheckpointAttrScope(const Structure structure, const boost::uint64_t& scope_id);
};

/*! \brief Class representing a Module attribute
 */
class CheckpointAttr_Module : public CheckpointAttrS {
public:
    typedef boost::shared_ptr<CheckpointAttr_Module> APtr;
    static APtr create(const std::string& value);

private:
    CheckpointAttr_Module(const std::string& value);
};

/*! \brief Class representing a Type attribute
 */
class CheckpointAttr_Type : public CheckpointAttrS {
public:
    typedef boost::shared_ptr<CheckpointAttr_Type> APtr;
    static APtr create(const std::string& value);

private:
    CheckpointAttr_Type(const std::string& value);
};

/*! \brief Helper factories for easy reading
 */
CheckpointAttr::APtr CpAttr(const std::string& checkpoint_id, const std::string& value);
CheckpointAttr::APtr CpAttr(const std::string& checkpoint_id, const long value);

CheckpointAttr_Type::APtr CpAttr_debug(void);
CheckpointAttr_Type::APtr CpAttr_info(void);
CheckpointAttr_Type::APtr CpAttr_warning(void);
CheckpointAttr_Type::APtr CpAttr_error(void);
CheckpointAttr_Type::APtr CpAttr_critical(void);

CheckpointAttr::APtr CpAttr_message(std::string);

CheckpointAttr_Module::APtr Attr_Utility(void);

} // End namespace, Utility
} // End Namespace, Giritech
#endif
