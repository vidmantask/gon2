/*! \file UY_DataBuffer.cxx
 \brief This file contains the implementation of classes for data buffer handling.
 */
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <math.h>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Convert.hxx>

using namespace Giritech::Utility;
using namespace std;

void convert_from_networkorder_32_xx(boost::uint32_t& value,
                                                     const byte_t& byte1,
                                                     const byte_t& byte2,
                                                     const byte_t& byte3,
                                                     const byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    byte_t* netWorkByteOrderPtr( (byte_t*)&netWorkByteOrder);
    netWorkByteOrderPtr[0] = byte1;
    netWorkByteOrderPtr[1] = byte2;
    netWorkByteOrderPtr[2] = byte3;
    netWorkByteOrderPtr[3] = byte4;
    value = ntohl(netWorkByteOrder);
}

void convert_to_networkorder_32_xx(const boost::uint32_t& value,
                                                   byte_t& byte1,
                                                   byte_t& byte2,
                                                   byte_t& byte3,
                                                   byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    netWorkByteOrder = htonl(value);
    byte_t* netWorkByteOrderPtr = (byte_t*) &netWorkByteOrder;
    byte1 = netWorkByteOrderPtr[0];
    byte2 = netWorkByteOrderPtr[1];
    byte3 = netWorkByteOrderPtr[2];
    byte4 = netWorkByteOrderPtr[3];
}
/*
 ------------------------------------------------------------------
 DataBuffer implementation
 ------------------------------------------------------------------
 */
void Giritech::Utility::DataBuffer::parse(DataBufferManaged::APtr& value, const long startPos) {
    long pos = startPos;
    long posMax = getSize();
    if (pos < 0)
        throw Exception_Out_of_bound("Start position invalid");
    if (pos > posMax)
        throw Exception_Out_of_bound("Start position is larger than buffer size");
    if (pos == posMax) {
        value = DataBufferManaged::create(0);
    } else {
        value = DataBufferManaged::create(&(*this)[pos], posMax - pos);
    }
}

void Giritech::Utility::DataBuffer::parse(DataBufferManaged::APtr& value) {
    parse(value, 0);
}

long Giritech::Utility::DataBuffer::parse(const std::string& seperator,
                                          DataBufferManaged::APtr& value) {
    return parse(seperator, value, 0);
}

long Giritech::Utility::DataBuffer::parse(const std::string& seperator,
                                          DataBufferManaged::APtr& value,
                                          const long startPos) {
    long pos = startPos;
    long posMax = getSize();
    assert(seperator.length() > 0);

    if (pos < 0)
        throw Exception_Out_of_bound("Start position invalid");
    if (pos > posMax)
        throw Exception_Out_of_bound("Start position is larger than buffer size");
    if (posMax==0)
        throw Exception_Out_of_bound("Buffer is empty");

    long seperatorLength = seperator.length();
    while ( (pos + seperatorLength - 1) < posMax) {
        string foundSeperat;
        if (memcmp(&(*this)[pos], seperator.data(), seperatorLength) == 0) {
            value = DataBufferManaged::create(&(*this)[startPos], pos - startPos);
            return pos + seperatorLength;
        }
        ++pos;
    }
    throw Exception_Seperator_not_found("Seperator is not found");
}

void Giritech::Utility::DataBuffer::parse(long& value, const long prefixByteSize) {
    parse(value, prefixByteSize, 0);
}

void Giritech::Utility::DataBuffer::parse(bool& value, const long start_pos) {
    long value_long(0);
    parse(value_long, 1, start_pos);
    value = (value_long == 1);
}

void Giritech::Utility::DataBuffer::parse(long& value,
                                          const long prefixByteSize,
                                          const long startPos) {
    assert(prefixByteSize <= 4 && prefixByteSize >= 1);
    long pos = startPos;
    long posMax = getSize();
    if (pos < 0)
        throw Exception_Out_of_bound("Start position invalid");
    if (pos >= posMax)
        throw Exception_Out_of_bound("Start position is larger than buffer size");
    if (posMax==0)
        throw Exception_Out_of_bound("Buffer is empty");
    if (pos + prefixByteSize > posMax)
        throw Exception_Out_of_bound("Buffer is to small to contain requested prefix value size");

    if (prefixByteSize == 1) {
        boost::uint8_t value_out;
        convert_from_networkorder_8(value_out, (*this)[startPos+0]);
        value = value_out;
    } else if (prefixByteSize == 2) {
        boost::uint16_t value_out;
        convert_from_networkorder_16(value_out, (*this)[startPos+0], (*this)[startPos+1]);
        value = value_out;
    } else {
        boost::uint32_t value_out;
		convert_from_networkorder_32_xx(value_out, (*this)[startPos+0], (*this)[startPos+1], (*this)[startPos+2], (*this)[startPos+3]);
        value = value_out;
    }
}

long Giritech::Utility::DataBuffer::parseWithLen(DataBufferManaged::APtr& value,
                                                 const long prefixByteSize) {
    return parseWithLen(value, prefixByteSize, 0);
}

long Giritech::Utility::DataBuffer::parseWithLen(DataBufferManaged::APtr& value,
                                                 const long prefixByteSize,
                                                 const long startPos) {
    long bufferSize;
    parse(bufferSize, prefixByteSize, startPos);
    if (startPos+prefixByteSize+bufferSize <= getSize()) {
        DataBufferManaged::APtr result(DataBufferManaged::create(bufferSize));
        if (bufferSize > 0) {
            memcpy(result->data(), &(*this)[startPos+prefixByteSize], bufferSize);
        }
        value = result;
    }
    else {
        value = DataBufferManaged::create(0);
    }
    return startPos + bufferSize + prefixByteSize;
}

std::string Giritech::Utility::DataBuffer::toString(void) const {
    if (getSize()>0) {
        return string(reinterpret_cast<const char*>(&(*this)[0]), getSize());
    }
    return "";
}

bool Giritech::Utility::DataBuffer::operator==(const DataBuffer& other) const {
    if(getSize() != other.getSize()) return false;
    if(getSize() == 0) return true;
    return (memcmp(data(), other.data(), getSize()) == 0);
}

bool Giritech::Utility::DataBuffer::operator!=(const DataBuffer& other) const {
    return !((*this) == other);
}

boost::shared_ptr<DataBufferManaged> Giritech::Utility::DataBuffer::encodeHex(void) const {
    long jMax = getSize();
    DataBufferManaged::APtr result(DataBufferManaged::create(jMax*2));
    if (jMax == 0) {
        return result;
    }

    const byte_t* from = data();
    byte_t* to = result->data();
    char hexval[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E',
            'F' };
    for (int j = 0; j < jMax; j++) {
        to[j*2] = hexval[(from[j] >> 4) & 0xF];
        to[(j*2) + 1] = hexval[from[j] & 0x0F];
    }
    return result;
}

byte_t Giritech::Utility::DataBuffer::decodeHex(const byte_t* buffer) const {
    string bufferString;
    bufferString += (char)buffer[0];
    bufferString += (char)buffer[1];

    int result = 0;
    if (EOF == sscanf(bufferString.c_str(), "%x", &result)) {
        throw DataBuffer::Exception_decoding("Error during hex decoding");
    }
    return static_cast<byte_t>(result);
}

boost::shared_ptr<DataBufferManaged> Giritech::Utility::DataBuffer::decodeHex(void) const {
    if ( (getSize() % 2) != 0) {
        throw Exception_decoding("Error during hex decoding");
    }
    long resultSize = getSize() / 2;
    long resultIdx = 0;
    DataBufferManaged::APtr result(DataBufferManaged::create(resultSize));

    long idxMax = getSize();
    long idx = 0;
    while (idx<idxMax) {
        (result->data()[resultIdx]) = decodeHex( &(*this)[idx]);
        resultIdx += 1;
        idx += 2;
    }
    return result;
}

void DataBuffer::burn(void) {
    int iMax = getSize();
    for (int i = 0; i < iMax; ++i) {
        (*this)[i] = 0;
    }
}

/*
 ------------------------------------------------------------------
 DataBufferManaged implementation
 ------------------------------------------------------------------
 */
DataBufferManaged::DataBufferManaged(std::istream& is, const size_t& size) {
    size_t putted = 0;
    istreambuf_iterator<char> isi(is);
    istreambuf_iterator<char> isiEnd;
    while (putted < size && isi != isiEnd) {
        rawBuffer_.push_back(*isi);
        ++isi;
        ++putted;
    }
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create(const long size) {
    return APtr(new DataBufferManaged(size));
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create(const long size,
                                                                     const byte_t fill) {
    APtr buffer(create(0));
    buffer->resize(size, fill);
    return buffer;
}
DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create(const byte_t* rawBuffer,
                                                                     const long size) {
    APtr buffer(create(size));
    if (size > 0) {
        memcpy(&(buffer->rawBuffer_[0]), rawBuffer, size);
    }
    return buffer;
}

DataBufferManaged::APtr DataBufferManaged::create(
		const char* rawBuffer,
		const long size) {
	return DataBufferManaged::create(string(rawBuffer, size));
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create(std::istream& is,
                                                                     const size_t& size) {
    return APtr(new DataBufferManaged(is, size));
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create_empty(const long size) {
    APtr new_buffer(new DataBufferManaged);
    new_buffer->get_raw_buffer().reserve(size);
    return new_buffer;
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::create(const string& stringBuffer) {
    return create((const byte_t*)stringBuffer.data(), (const long)stringBuffer.length());
}
DataBufferManaged::APtr DataBufferManaged::create(const DataBuffer::APtr& buffer) {
    if(buffer->getSize() > 0) {
        return create(buffer->data(), buffer->getSize());
    }
    return create(0);
}

DataBufferManaged::APtr DataBufferManaged::create(const std::vector<byte_t>::const_iterator& i, const unsigned long size) {
    APtr buffer(create(0));
    buffer->get_raw_buffer().insert(buffer->get_raw_buffer().begin(), i, i + size);
    return buffer;
}


void Giritech::Utility::DataBufferManaged::append(const DataBufferManaged::APtr& buffer) {
    rawBuffer_.insert(rawBuffer_.end(), buffer->rawBuffer_.begin(), buffer->rawBuffer_.end());
}

void Giritech::Utility::DataBufferManaged::append(const bool value) {
    long value_long(0);
    if(value) {
        value_long = 1;
    }
    append(value_long, 1);
}

void Giritech::Utility::DataBufferManaged::append(const long buffer, const long prefixByteSize) {
    assert(prefixByteSize <= 4 && prefixByteSize >= 1);
    if (buffer >= pow(2.0, (prefixByteSize * 8.0))) {
        stringstream ss;
        ss << "Buffer can not be encoded in " << prefixByteSize << " number of bytes";
        throw Exception_encoding(ss.str());
    }

    if (prefixByteSize == 1) {
        boost::uint8_t value = static_cast<boost::uint8_t>(buffer);
        byte_t byte1;
        convert_to_networkorder_8(value, byte1);
        rawBuffer_.push_back(byte1);
    } else if (prefixByteSize == 2) {
        boost::uint16_t value = static_cast<boost::uint16_t>(buffer);
        byte_t byte1;
        byte_t byte2;
        convert_to_networkorder_16(value, byte1, byte2);
        rawBuffer_.push_back(byte1);
        rawBuffer_.push_back(byte2);
    } else {
        boost::uint32_t value = static_cast<boost::uint32_t>(buffer);
        byte_t byte1;
        byte_t byte2;
        byte_t byte3;
        byte_t byte4;
		convert_to_networkorder_32_xx(value, byte1, byte2, byte3, byte4);
        rawBuffer_.push_back(byte1);
        rawBuffer_.push_back(byte2);
        rawBuffer_.push_back(byte3);
        rawBuffer_.push_back(byte4);
    }
}

void Giritech::Utility::DataBufferManaged::appendInteger(const long value) {
    stringstream ss;
    ss << value;
    append(ss.str());
}

DataBufferManaged::APtr Giritech::Utility::DataBufferManaged::clone(void) const {
    DataBufferManaged::APtr newDataBuffer(new DataBufferManaged(getSize()));
    newDataBuffer->rawBuffer_.assign(rawBuffer_.begin(), rawBuffer_.end());
    return newDataBuffer;
}

void Giritech::Utility::DataBufferManaged::dump_internal_info(void) const {
    cout << "DataBufferManaged::dump_internal_info size:" << rawBuffer_.size() << " capacity: "
            << rawBuffer_.capacity() << " max_size:" << rawBuffer_.max_size()<< endl;
}

void Giritech::Utility::DataBufferManaged::resize(const long size) {
    resize(size, 0);
}
void Giritech::Utility::DataBufferManaged::resize(const long size, const byte_t fill) {
    assert(size >= 0);
    rawBuffer_.resize(size, fill);
}

void Giritech::Utility::DataBufferManaged::pkcsAdd(const long blockSize) {
    assert(blockSize < 255);
    int padSize = (blockSize - ( (getSize()+1) % blockSize) + 1);
    if (padSize > blockSize) {
        padSize -= blockSize;
    }
    resize(getSize()+padSize, static_cast<byte_t>(padSize));
}

void Giritech::Utility::DataBufferManaged::pkcsRemove(const long blockSize) {
    assert(blockSize < 255);
    int padSize = static_cast<int>(rawBuffer_.back());
    if (padSize > blockSize || padSize > getSize() ) {
        stringstream ss;
        ss << "Error during PKCS decoding, ";
        ss << "pad_size:" << padSize << " ";
        ss << "block_size:" << blockSize << " ";
        ss << "buffer_size:" << getSize() << " ";
        ss << "buffer: " << encodeHex()->toString();
        throw Exception_decoding(ss.str());
    }
    int padStart = getSize()-padSize;
    for (int i=0; i < padSize; ++i) {
        if (rawBuffer_[padStart+i] != static_cast<byte_t>(padSize)) {
            throw Exception_decoding("Error during PKCS decoding");
        }
    }
    resize(getSize()-padSize);
}

void Giritech::Utility::DataBufferManaged::eatFront(const unsigned long count) {
	unsigned long count_to_use = min(count, static_cast<const unsigned long>(getSize()));
	rawBuffer_.erase (rawBuffer_.begin(),rawBuffer_.begin()+count_to_use);
}

/*
 ------------------------------------------------------------------
 DataBufferUnmanaged implementation
 ------------------------------------------------------------------
 */
DataBufferManaged::APtr Giritech::Utility::DataBufferUnmanaged::clone(void) const {
    return DataBufferManaged::create(rawBuffer_, size_);
}

DataBufferUnmanaged::APtr Giritech::Utility::DataBufferUnmanaged::create(const long size,
                                                                         byte_t* rawBuffer) {
    return APtr(new DataBufferUnmanaged(size, rawBuffer));
}
