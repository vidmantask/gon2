/*! \file UY_CheckpointHandler.hxx
 \brief This file contains the class for handling checkpoints
 */
#ifndef UY_CHECKPOINTHANDLER_HXX
#define UY_CHECKPOINTHANDLER_HXX

#include <string>
#include <vector>

#include <boost/utility.hpp>
#include <boost/optional.hpp>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointFilter.hxx>
#include <lib/utility/UY_CheckpointOutputHandler.hxx>

namespace Giritech {
namespace Utility {

/*! \brief Handles checkpoints

 A checkpoint handler is a policy based handling of checkpoints.

 The filter policy is used for filtering the checkpoints,
 and the output policy is handling the use of the checkpoint.

 You will most likely not need to inhiret from this class,
 but instead implement you own policy class.

 A checkpoint handler usaly knows ist parrent, and are thus connected in a chain.
 When a checkpoint is recived by the checkpoint handler, the checkpoint is submittet to the parent.
 */
class CheckpointHandler : boost::noncopyable {
public:
    typedef boost::shared_ptr<CheckpointHandler> APtr;

    CheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                      const CheckpointOutputHandler::APtr& checkpoint_output_handler);
    CheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                      const CheckpointOutputHandler::APtr& checkpoint_output_handler,
                      CheckpointHandler& checkpoint_handler_parent);
    virtual ~CheckpointHandler(void);

    /*! \brief Handle a checkpoint

     The checkpoint is filtered by the checkpoint filter policy.
     If the checkpoint is accepted by the filter, then the checkpoint is send to the output handler.
     Then the checkpoint is submittet to the parent checkpoint handler if one is available.
     */
    void handle_checkpoint(const std::string& checkpoint_id,
                           const CheckpointAttr::APtrs& event_attrs,
                           const CheckpointAttr::Structure structure);

    /* \brief test if logging is enabled
     *
     */
    bool is_logging(const std::string& checkpoint_id,
    				const CheckpointAttr::APtrs& event_attrs,
    				const CheckpointAttr::Structure structure);

    /*! \brief Change the output handler
     */
    void  set_output_handler(const CheckpointOutputHandler::APtr& checkpoint_output_handler);

    /*! \brief Create instance of checkpoint handler
     */
    static APtr create(const CheckpointFilter::APtr& checkpoint_filter,
                       const CheckpointOutputHandler::APtr& checkpoint_output_handler);
    static APtr create(const CheckpointFilter::APtr& checkpoint_filter,
                       const CheckpointOutputHandler::APtr& checkpoint_output_handler,
                       CheckpointHandler& checkpoint_handler_parent);
    static APtr create_cout_all(void);
    static APtr create_cout_all_xml(void);
    static APtr create_ignore(void);

    /*! \brief close the handler
     */
    void close(void);

    APtr create_in_same_folder(const std::string& filename);

private:
    CheckpointFilter::APtr checkpoint_filter_;
    CheckpointOutputHandler::APtr checkpoint_output_handler_;

    typedef boost::optional< CheckpointHandler& > OptionalRef;
    OptionalRef checkpoint_handler_parent_;

    bool closed_;
};

} // End namespace, Utility
} // End Namespace, Giritech
#endif
