/*! \file UY_AsyncMemGuard.cxx
    \brief This file contains implementation for a async memory guard ensuring that object is alive until async callbacks are done
*/
#include <lib/utility/UY_AsyncMemGuard.hxx>
#include <lib/utility/UY_CheckpointAttr.hxx>


using namespace Giritech::Utility;
using namespace std;

/*
 * ------------------------------------------------------------------
 * AsyncMemGuardElement implementation
 * ------------------------------------------------------------------
 */
AsyncMemGuardElement::~AsyncMemGuardElement(void) {
}

/*
 * ------------------------------------------------------------------
 * AsyncMemGuard implementation
 * ------------------------------------------------------------------
 */
static AsyncMemGuard::APtr global_async_guard_;

AsyncMemGuard::AsyncMemGuard(boost::asio::io_service& io)
	: mutex_(Mutex::create(io, "AsyncMemGuard")), io_(io), strand_(io)  {
}

AsyncMemGuard::~AsyncMemGuard(void) {
}

AsyncMemGuard::APtr AsyncMemGuard::create(boost::asio::io_service& io) {
	return APtr(new AsyncMemGuard(io));
}

void AsyncMemGuard::add_element(const AsyncMemGuardElement::APtr& element) {
    strand_.post(boost::bind(&AsyncMemGuard::cleanup, this));
	{
		MutexScopeLockAnonyme mutex_lock(mutex_);
		elements_.insert(element);
	}
}

void AsyncMemGuard::cleanup(void) {
	try {
		std::set<AsyncMemGuardElement::APtr> elements_to_check;
		std::set<AsyncMemGuardElement::APtr> elements_to_be_deleted;
		{
			MutexScopeLockAnonyme mutex_lock(mutex_);
			elements_to_check.insert(elements_.begin(), elements_.end());
		}
		{
			for (const AsyncMemGuardElement::APtr& element : elements_to_check) {
				if (element->async_mem_guard_is_done()) {
					elements_to_be_deleted.insert(element);
				}
			}
		}
		{
			MutexScopeLockAnonyme mutex_lock(mutex_);

			for (const AsyncMemGuardElement::APtr& element : elements_to_be_deleted) {
				elements_.erase(element);
			}
		}
	}
    catch (const std::exception& e) {
    	cout << "AsyncMemGuard::cleanup.unexpected.exception :" << e.what() << endl;
	}
}

std::string AsyncMemGuard::analyze(void)  {
	try {
		std::set<AsyncMemGuardElement::APtr> elements_to_check;
		{
			MutexScopeLockAnonyme mutex_lock(mutex_);
			elements_to_check.insert(elements_.begin(), elements_.end());
		}

		std::map<std::string, long> analyze;

		for (const AsyncMemGuardElement::APtr& element : elements_to_check) {
			std::string name(element->async_mem_guard_get_name());
			std::map<std::string, long>::iterator j(analyze.find(name));
			if (j != analyze.end()) {
				j->second = j->second + 1;
			}
			else {
				analyze.insert(make_pair(name, 1));
			}
		}
		std::stringstream ss;
		ss << "AsyncMemGuard.start" << endl;
		std::map<std::string, long>::iterator k(analyze.begin());
		while(k != analyze.end()) {
	    	ss << k->first << ":" << k->second << endl;
	    	k++;
		}
		ss << "AsyncMemGuard.end" << endl;
		return ss.str();
	}
    catch (const std::exception& e) {
    	cout << "AsyncMemGuard::cleanup.unexpected.exception :" << e.what() << endl;
	}
    return "error";
}

bool AsyncMemGuard::is_empty(void) {
	return elements_.size() == 0;
}

long AsyncMemGuard::get_element_count(void) {
	return elements_.size();
}

boost::asio::io_service& AsyncMemGuard::get_io(void) {
	return io_;
}

void AsyncMemGuard::create_global_guard(boost::asio::io_service& io) {
	if (global_async_guard_.get() == NULL) {
		global_async_guard_ = AsyncMemGuard::create(io);
	}
}

void AsyncMemGuard::destroy_global_guard(boost::asio::io_service& io) {
	if (global_async_guard_.get() != NULL &&
		&global_async_guard_->get_io() == &io) {
		global_async_guard_.reset();
	}
}

AsyncMemGuard::APtr AsyncMemGuard::get_global_guard(void) {
	return global_async_guard_;
}

void AsyncMemGuard::global_add_element(const AsyncMemGuardElement::APtr& element) {
	if(global_async_guard_.get() != NULL) {
		global_async_guard_->add_element(element);
	}
}

void AsyncMemGuard::global_cleanup(void) {
	if(global_async_guard_.get() != NULL) {
		global_async_guard_->cleanup();
	}
}

long AsyncMemGuard::global_get_element_count(void) {
	if(global_async_guard_.get() != NULL) {
		return global_async_guard_->get_element_count();
	}
	return 0;
}

std::string AsyncMemGuard::global_analyze(void) {
	if(global_async_guard_.get() != NULL) {
		return global_async_guard_->analyze();
	}
	return "";
}
