/*! \file UY_API_Checkpoint.hxx
 \brief This file contains api interface for checkpoint classes
 */
#ifndef UY_API_Checkpoint_HXX
#define UY_API_Checkpoint_HXX

#include <string>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointAttr.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_CheckpointFilter.hxx>
#include <lib/utility/UY_CheckpointOutputHandler.hxx>

#include <boost/python.hpp>
#include <boost/python/stl_iterator.hpp>
#include <boost/shared_ptr.hpp>

namespace Giritech {
namespace Utility {

class APICheckpointScope : boost::noncopyable {
private:

    CheckpointHandler::APtr checkpoint_handler; // Keep APtr to handler to ensure lifetime at least as long as the scope
    boost::shared_ptr<CheckpointScope> checkpointscope;

    APICheckpointScope(const CheckpointHandler::APtr& checkpoint_handler_,
                        const std::string& cp_id,
                        const CheckpointAttr::APtrs& attrs);

public:

    typedef boost::shared_ptr<APICheckpointScope> APtr;

    // Consider this a method on CheckpointHandler::APtr ...
    static APtr create(const CheckpointHandler::APtr& checkpoint_handler,
                       const std::string& cp_id,
                       const CheckpointAttr::APtrs& attrs);

    // Consider this a method on APtr
    static void add_complete_attr_aptr(const APtr& self, const boost::python::dict& attrs_dict);

    // Consider this a method on APtr
    static void end_scope_aptr(const APtr& self);

    // Python uses GC, so we can't wait for destruction
    void end_scope();

    ~APICheckpointScope();
};

class APICheckpointHandler : boost::noncopyable {
public:
    typedef boost::shared_ptr<APICheckpointHandler> APtr;

    void createCheckpoint(const std::string& cp_id, const boost::python::dict& attrs_dict);

    APICheckpointScope::APtr createCheckpointScope(const std::string& cp_id,
                                                    const boost::python::dict& attrs_dict);

    CheckpointHandler::APtr get_checkpoint_handler(void) const;

    void close(void);
    static void self_close(const APtr& self);

    void set_output_handler(const CheckpointOutputHandler::APtr& checkpoint_output_handler);


    static APtr create(const CheckpointFilter::APtr& checkpoint_filter, const CheckpointOutputHandler::APtr& checkpoint_output_handler);
    static APtr create_with_handler(const CheckpointHandler::APtr& checkpoint_handler);

    static APtr create_with_parent(
			const CheckpointFilter::APtr& checkpoint_filter,
			const CheckpointOutputHandler::APtr& checkpoint_output_handler,
			const APICheckpointHandler::APtr& checkpoint_handler_parent);

    static void self_createCheckpoint(const APtr& self,
                                      const std::string& cp_id,
                                      const boost::python::dict& attrs_dict);

    static APICheckpointScope::APtr
            self_createCheckpointScope(const APtr& self,
                                       const std::string& cp_id,
                                       const boost::python::dict& attrs_dict);

    static CheckpointHandler::APtr self_get_checkpoint_handler(const APtr& self);

    static void self_set_output_handler(const APtr& self, const CheckpointOutputHandler::APtr& checkpoint_output_handler);

private:
    APICheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                         const CheckpointOutputHandler::APtr& checkpoint_output_handler);

    APICheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter,
                         const CheckpointOutputHandler::APtr& checkpoint_output_handler,
                         CheckpointHandler& checkpoint_handler_parent);

    APICheckpointHandler(const CheckpointHandler::APtr& checkpoint_handler);

    CheckpointHandler::APtr checkpoint_handler;

    // Convert from python dict to CheckpointAttr::APtrs
    CheckpointAttr::APtrs createCheckPointAttrs(const boost::python::dict& attrs_dict);

};

} // End namespace, Utility
} // End Namespace, Giritech
#endif
