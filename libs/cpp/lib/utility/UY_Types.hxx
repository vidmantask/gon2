/*! \file UY_Types.hxx
    \brief This file contains definition for simpel general types
*/
#ifndef UY_TYPES_HXX
#define UY_TYPES_HXX

#include <boost/cstdint.hpp>

namespace Giritech {
  namespace Utility {

    typedef boost::uint8_t byte_t;

  } // End namespace, Utility
} // End Namespace, Giritech
#endif
