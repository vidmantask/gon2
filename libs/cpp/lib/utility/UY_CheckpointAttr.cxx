/*! \file UY_CheckpointAttr.cxx
 \brief This file contains the implementation of classes for checkpoint attributes
 */
#include <iostream>
#include <sstream>

#include <boost/none.hpp>

#include <lib/utility/UY_CheckpointAttr.hxx>

using namespace Giritech::Utility;
using namespace std;

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttr
 ------------------------------------------------------------------
 */
CheckpointAttr::CheckpointAttr(const std::string& id) :
    id_(id) {
}
CheckpointAttr::~CheckpointAttr(void) {
}

const std::string& CheckpointAttr::get_id(void) const {
    return id_;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrS
 ------------------------------------------------------------------
 */
CheckpointAttrS::CheckpointAttrS(const std::string& id, const std::string& value) :
    CheckpointAttr(id), value_(value) {
}
bool CheckpointAttrS::equal(const CheckpointAttr::APtr& attr) const {
    if (const CheckpointAttrS* attrs = dynamic_cast<const CheckpointAttrS*>(attr.get())) {
        return get_id() == attrs->get_id() && value_ == attrs->value_;
    }
    return false;
}

std::string CheckpointAttrS::get_value(void) const {
    return value_;
}

const std::string CheckpointAttrS::to_string(void) const {
    return value_;
}
CheckpointAttr::APtr CheckpointAttrS::create(const std::string& id, const std::string& value) {
    return CheckpointAttr::APtr(new CheckpointAttrS(id, value));
}

CheckpointAttrS* CheckpointAttrS::lookup(const APtrs& attrs, const std::string& attr_id) {
    CheckpointAttr::APtrs::const_iterator i_ts(attrs.find(attr_id));
    if (i_ts != attrs.end()) {
        return dynamic_cast<CheckpointAttrS*>((*i_ts).second.get());
    }
    return NULL;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrI
 ------------------------------------------------------------------
 */
CheckpointAttrI::CheckpointAttrI(const std::string& id, const long value) :
    CheckpointAttr(id), value_(value) {
}
CheckpointAttrI::~CheckpointAttrI(void) {
}
bool CheckpointAttrI::equal(const CheckpointAttr::APtr& attr) const {
    if (const CheckpointAttrI* attrs = dynamic_cast<const CheckpointAttrI*>(attr.get())) {
        return get_id() == attrs->get_id() && value_ == attrs->value_;
    }
    return false;
}
const std::string CheckpointAttrI::to_string(void) const {
    stringstream ss;
    ss << value_;
    return ss.str();
}

long CheckpointAttrI::get_value(void) const {
    return value_;
}

CheckpointAttr::APtr CheckpointAttrI::create(const std::string& id, const long value) {
    return CheckpointAttr::APtr(new CheckpointAttrI(id, value));
}

CheckpointAttrI* CheckpointAttrI::lookup(const APtrs& attrs, const std::string& attr_id) {
    CheckpointAttr::APtrs::const_iterator i_ts(attrs.find(attr_id));
    if (i_ts != attrs.end()) {
        return dynamic_cast<CheckpointAttrI*>((*i_ts).second.get());
    }
    return NULL;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrTS
 ------------------------------------------------------------------
 */
CheckpointAttrTS::CheckpointAttrTS(void) :
    CheckpointAttr("ts") {
    value_ = boost::posix_time::microsec_clock::local_time();
}
bool CheckpointAttrTS::equal(const CheckpointAttr::APtr& attr) const {
    if (const CheckpointAttrTS* attrs = dynamic_cast<const CheckpointAttrTS*>(attr.get())) {
        return get_id() == attrs->get_id() && value_ == attrs->value_;
    }
    return false;
}
const std::string CheckpointAttrTS::to_string(void) const {
    return boost::posix_time::to_iso_string(value_);
}
CheckpointAttr::APtr CheckpointAttrTS::create(void) {
    return CheckpointAttr::APtr(new CheckpointAttrTS);
}
const boost::posix_time::ptime& CheckpointAttrTS::get_value(void) const {
    return value_;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrTD
 ------------------------------------------------------------------
 */
CheckpointAttrTD::CheckpointAttrTD(const boost::posix_time::time_duration& value) :
    CheckpointAttr("duration"), value_(value) {
}
bool CheckpointAttrTD::equal(const CheckpointAttr::APtr& attr) const {
    if (const CheckpointAttrTD* attrs = dynamic_cast<const CheckpointAttrTD*>(attr.get())) {
        return get_id() == attrs->get_id() && value_ == attrs->value_;
    }
    return false;
}
const std::string CheckpointAttrTD::to_string(void) const {
    return boost::posix_time::to_iso_string(value_);
}
CheckpointAttr::APtr CheckpointAttrTD::create(const boost::posix_time::time_duration& value) {
    return CheckpointAttr::APtr(new CheckpointAttrTD(value));
}
const boost::posix_time::time_duration& CheckpointAttrTD::get_value(void) const {
    return value_;
}
CheckpointAttrTD* CheckpointAttrTD::lookup(const APtrs& attrs, const std::string& attr_id) {
    CheckpointAttr::APtrs::const_iterator i_ts(attrs.find(attr_id));
    if (i_ts != attrs.end()) {
        return dynamic_cast<CheckpointAttrTD*>((*i_ts).second.get());
    }
    return NULL;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrThreadId
 ------------------------------------------------------------------
 */

CheckpointAttrThreadId::CheckpointAttrThreadId(void) :
    CheckpointAttr("thread_id") {
    value_ =  boost::lexical_cast<std::string>(boost::this_thread::get_id());;
}
bool CheckpointAttrThreadId::equal(const CheckpointAttr::APtr& attr) const {
    if (const CheckpointAttrThreadId* attrs = dynamic_cast<const CheckpointAttrThreadId*>(attr.get())) {
        return get_id() == attrs->get_id() && value_ == attrs->value_;
    }
    return false;
}
const std::string CheckpointAttrThreadId::to_string(void) const {
    stringstream ss;
    ss << value_;
    return ss.str();
}
CheckpointAttr::APtr CheckpointAttrThreadId::create(void) {
    return CheckpointAttr::APtr(new CheckpointAttrThreadId);
}
std::string CheckpointAttrThreadId::get_value(void) const {
    return value_;
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttrScope
 ------------------------------------------------------------------
 */
CheckpointAttrScope::CheckpointAttrScope(const Structure structure, const boost::uint64_t& scope_id) :
    CheckpointAttrI((structure != Structure_end ? "scope" : "scope_end"), scope_id) {
}
CheckpointAttr::APtr CheckpointAttrScope::create(const Structure structure,
                                                 const boost::uint64_t& scope_id) {
    return CheckpointAttr::APtr(new CheckpointAttrScope(structure, scope_id));
}


/*
 ------------------------------------------------------------------
 Implementation CheckpointAttr_Module
 ------------------------------------------------------------------
 */

CheckpointAttr_Module::CheckpointAttr_Module(const std::string& value) :
    CheckpointAttrS("module", value) {
}
CheckpointAttr_Module::APtr CheckpointAttr_Module::create(const std::string& value) {
    return CheckpointAttr_Module::APtr(new CheckpointAttr_Module(value));
}

/*
 ------------------------------------------------------------------
 Implementation CheckpointAttr_Type
 ------------------------------------------------------------------
 */

CheckpointAttr_Type::CheckpointAttr_Type(const std::string& value) :
    CheckpointAttrS("type", value) {
}
CheckpointAttr_Type::APtr CheckpointAttr_Type::create(const std::string& value) {
    return CheckpointAttr_Type::APtr(new CheckpointAttr_Type(value));
}




/*
 ------------------------------------------------------------------
 Implementation Suggar
 ------------------------------------------------------------------
 */
CheckpointAttr::APtr Giritech::Utility::CpAttr(const std::string& checkpoint_id,
                                               const std::string& value) {
    return CheckpointAttrS::create(checkpoint_id, value);
}
CheckpointAttr::APtr Giritech::Utility::CpAttr(const std::string& checkpoint_id, const long value) {
    return CheckpointAttrI::create(checkpoint_id, value);
}

CheckpointAttr_Type::APtr Giritech::Utility::CpAttr_debug(void) {
    static CheckpointAttr_Type::APtr type_info_;
    if (!type_info_) {
        type_info_ = CheckpointAttr_Type::create("debug");
    }
    return type_info_;
}

CheckpointAttr_Type::APtr Giritech::Utility::CpAttr_info(void) {
    static CheckpointAttr_Type::APtr type_info_;
    if (!type_info_) {
        type_info_ = CheckpointAttr_Type::create("info");
    }
    return type_info_;
}
CheckpointAttr_Type::APtr Giritech::Utility::CpAttr_warning(void) {
    static CheckpointAttr_Type::APtr type_warning_;
    if (!type_warning_) {
        type_warning_ = CheckpointAttr_Type::create("warning");
    }
    return type_warning_;
}
CheckpointAttr_Type::APtr Giritech::Utility::CpAttr_error(void) {
    static CheckpointAttr_Type::APtr type_error_;
    if (!type_error_) {
        type_error_ = CheckpointAttr_Type::create("error");
    }
    return type_error_;
}
CheckpointAttr_Type::APtr Giritech::Utility::CpAttr_critical(void) {
    static CheckpointAttr_Type::APtr type_critical_;
    if (!type_critical_) {
        type_critical_ = CheckpointAttr_Type::create("critical");
    }
    return type_critical_;
}

CheckpointAttr::APtr Giritech::Utility::CpAttr_message(std::string msg) {
    return CheckpointAttrS::create("message", msg);
}



/*
 ------------------------------------------------------------------
 Implementation Attr_Utility
 ------------------------------------------------------------------
 */

static CheckpointAttr_Module::APtr componentUtility_;

CheckpointAttr_Module::APtr Giritech::Utility::Attr_Utility(void) {
    if (!componentUtility_) {
    	componentUtility_ = CheckpointAttr_Module::create("Utility");
    }
    return componentUtility_;
}
