/*! \file UY_Checkpoint.cxx
    \brief This file contains the implementation of classes for event handling.
*/
#include <iostream>
#include <sstream>

#include <boost/none.hpp>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>


using namespace Giritech::Utility;
using namespace std;




/*
   ------------------------------------------------------------------
   Implementation Checkpoint
   ------------------------------------------------------------------
*/
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, attr_5);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, attr_5);
  add_attr(attrs, attr_6);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, attr_5);
  add_attr(attrs, attr_6);
  add_attr(attrs, attr_7);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7, const CheckpointAttr::APtr& attr_8) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, attr_5);
  add_attr(attrs, attr_6);
  add_attr(attrs, attr_7);
  add_attr(attrs, attr_8);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7, const CheckpointAttr::APtr& attr_8, const CheckpointAttr::APtr& attr_9) {
  CheckpointAttr::APtrs attrs;
  add_attr(attrs, attr_1);
  add_attr(attrs, attr_2);
  add_attr(attrs, attr_3);
  add_attr(attrs, attr_4);
  add_attr(attrs, attr_5);
  add_attr(attrs, attr_6);
  add_attr(attrs, attr_7);
  add_attr(attrs, attr_8);
  add_attr(attrs, attr_9);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg) {
  CheckpointAttr::APtrs attrs(attrs_arg);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, CheckpointAttr::Structure_begin_end);
}
Checkpoint::Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg, const CheckpointAttr::Structure structure) {
  CheckpointAttr::APtrs attrs(attrs_arg);
  add_attr(attrs, CheckpointAttrTS::create());
  add_attr(attrs, CheckpointAttrThreadId::create());
  handler.handle_checkpoint(id, attrs, structure);
}
void Checkpoint::add_attr(CheckpointAttr::APtrs& attrs, const CheckpointAttr::APtr& attr) {
  assert(attr);

//
// Removed for optimization
//

//  CheckpointAttr::APtrs::const_iterator i(attrs.find(attr->get_id()));
//  if(i != attrs.end()) {
//    stringstream ss;
//    ss << "Checkpoint '" <<  attr->get_id() << "' already exists";
//    throw Exception_checkpoint(ss.str());
//  }
  attrs.insert(make_pair(attr->get_id(), attr));
}




/*
   ------------------------------------------------------------------
   Implementation CheckpointScope
   ------------------------------------------------------------------
*/
boost::uint64_t CheckpointScope::global_scope_id = 0;
boost::recursive_mutex CheckpointScope::global_scope_id__mutex_;

boost::uint64_t CheckpointScope::get_scope_id_next(void) {
	boost::recursive_mutex::scoped_lock update_mutex_lock(global_scope_id__mutex_);
	return global_scope_id++;
}

CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_));
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_), attr_3);
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_), attr_3, attr_4);
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_), attr_3, attr_4, attr_5);
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_), attr_3, attr_4, attr_5, attr_6);
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7)
  : handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  Checkpoint checkpoint(handler, id, attr_1, attr_2, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_), attr_3, attr_4, attr_5, attr_6, attr_7);
}
CheckpointScope::CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg)
: handler_(handler), id_(id) {
  time_start_ = boost::posix_time::microsec_clock::local_time();
  scope_id_   = get_scope_id_next();
  CheckpointAttr::APtrs attrs(attrs_arg);
  Checkpoint::add_attr(attrs, CheckpointAttrScope::create(CheckpointAttr::Structure_begin, scope_id_));
  Checkpoint checkpoint(handler, id, attrs);
}
CheckpointScope::~CheckpointScope(void) {
  boost::posix_time::ptime time_end = boost::posix_time::microsec_clock::local_time();
  Checkpoint::add_attr(complete_attrs_, CheckpointAttrTD::create(time_end - time_start_));
  Checkpoint::add_attr(complete_attrs_, CheckpointAttrScope::create(CheckpointAttr::Structure_end, scope_id_));
  Checkpoint checkpoint(handler_, id_, complete_attrs_, CheckpointAttr::Structure_end);
}

void CheckpointScope::add_complete_attr(const CheckpointAttr::APtr& attr_1) {
  Checkpoint::add_attr(complete_attrs_, attr_1);
}

boost::uint64_t CheckpointScope::get_scope_id(void) const {
    return scope_id_;
}



/*
   ------------------------------------------------------------------
   Implementation CheckpointIsLogging
   ------------------------------------------------------------------
*/

CheckpointIsLogging::CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, attr_1);
	Checkpoint::add_attr(attrs, attr_2);
	is_logging_ = handler.is_logging(id, attrs, CheckpointAttr::Structure_begin_end);
}

CheckpointIsLogging::CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, attr_1);
	Checkpoint::add_attr(attrs, attr_2);
	Checkpoint::add_attr(attrs, attr_3);
	is_logging_ = handler.is_logging(id, attrs, CheckpointAttr::Structure_begin_end);
}

CheckpointIsLogging::CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, attr_1);
	Checkpoint::add_attr(attrs, attr_2);
	Checkpoint::add_attr(attrs, attr_3);
	Checkpoint::add_attr(attrs, attr_4);
	is_logging_ = handler.is_logging(id, attrs, CheckpointAttr::Structure_begin_end);
}

CheckpointIsLogging::CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, attr_1);
	Checkpoint::add_attr(attrs, attr_2);
	Checkpoint::add_attr(attrs, attr_3);
	Checkpoint::add_attr(attrs, attr_4);
	Checkpoint::add_attr(attrs, attr_5);
	is_logging_ = handler.is_logging(id, attrs, CheckpointAttr::Structure_begin_end);
}

CheckpointIsLogging::CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6) {
	CheckpointAttr::APtrs attrs;
	Checkpoint::add_attr(attrs, attr_1);
	Checkpoint::add_attr(attrs, attr_2);
	Checkpoint::add_attr(attrs, attr_3);
	Checkpoint::add_attr(attrs, attr_4);
	Checkpoint::add_attr(attrs, attr_5);
	Checkpoint::add_attr(attrs, attr_6);
	is_logging_ = handler.is_logging(id, attrs, CheckpointAttr::Structure_begin_end);
}

bool CheckpointIsLogging::operator()(void) {
	return is_logging_;
}
