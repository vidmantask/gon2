/*! \file UY_String.cxx
    \brief This file contains the implementation of additionale string fuctionality
*/
#include <assert.h>
#include <algorithm>
#include <lib/utility/UY_String.hxx>

using namespace Giritech::Utility::String;
using namespace std;


vector<string> Giritech::Utility::String::extractStrings(const string& seperator, const string& value) {
  assert(seperator.length() > 0);
  vector<string> result;
  if(value.empty()) {
    return result;
  }
  
  string::size_type posStart = 0;
  string::size_type pos = value.find(seperator);
  while(pos != string::npos && pos < value.length() ) {
    result.push_back(value.substr(posStart, pos - posStart));
    posStart = pos + seperator.length();
    pos = value.find(seperator, posStart);
  }
  result.push_back(value.substr(posStart));
  return result;
}



void Giritech::Utility::String::burnString(std::string& value) {
  fill(value.begin(), value.end(), 0);
}
