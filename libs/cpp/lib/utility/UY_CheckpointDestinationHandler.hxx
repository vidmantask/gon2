/*! \file UY_CheckpointDestinationHandler.hxx
    \brief This file contains classes for handling device output
*/
#ifndef UY_CHECKPOINTDESTINATIONHANDLER_HXX
#define UY_CHECKPOINTDESTINATIONHANDLER_HXX

#include <string>
#include <iostream>

#include "boost/filesystem/fstream.hpp"

#include <lib/utility/UY_Checkpoint.hxx>


namespace Giritech {
  namespace Utility {



    /*! \brief Abstract interface class for a handling device output
    */
    class CheckpointDestinationHandler : boost::noncopyable {
    public:
      typedef boost::shared_ptr<CheckpointDestinationHandler> APtr;

      virtual std::ostream& get_os(void) = 0;
      virtual void close(void) = 0;
      virtual void flush(void) = 0;

      virtual boost::filesystem::path get_foldername(void) = 0;
    };


    /*! \brief Class for representing stdout
    */
    class CheckpointDestinationHandlerCout : public CheckpointDestinationHandler {
    public:
      virtual std::ostream& get_os(void);
      void close(void);
      void flush(void);

      boost::filesystem::path get_foldername(void);

      static APtr create(void);
    private:
      CheckpointDestinationHandlerCout(void);
    };

#if !defined(ANDROID)
    /*! \brief Class for representing a file
    */
    class CheckpointDestinationHandlerFile : public CheckpointDestinationHandler {
    public:
      virtual std::ostream& get_os(void);
      void close(void);
      void flush(void);

      boost::filesystem::path get_foldername(void);

      static APtr create(const boost::filesystem::path& filepath, const bool append, const bool close_on_flush);
    private:
      CheckpointDestinationHandlerFile(const boost::filesystem::path& filepath, const bool append=false, const bool close_on_flush=false);

      boost::filesystem::path filepath_;
      bool append_;
      bool file_created_;
      bool file_open_;
      bool close_on_flush_;

      boost::shared_ptr<boost::filesystem::ofstream> file_;
    };
#endif

  } // End namespace, Utility
} // End Namespace, Giritech
#endif
