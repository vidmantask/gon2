/*! \file UY_CheckpointFilter.hxx
    \brief This file contains classes for checkpoint filters
*/
#ifndef UY_CHECKPOINTFILTER_HXX
#define UY_CHECKPOINTFILTER_HXX

#include <string>
#include <vector>
#include <set>

#include <boost/utility.hpp>

#include <lib/utility/UY_Checkpoint.hxx>


namespace Giritech {
  namespace Utility {


    /*! \brief Abstract interface class for a checkpoint filter

    A checkpoint filter is used by a checkpoint handler to filter the checkpoints accepted by the handler.
    */
    class CheckpointFilter {
    public:
      typedef boost::shared_ptr<CheckpointFilter> APtr;

      virtual bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) = 0;
    };


    /*! \brief Checkpoint true-filter
    */
    class CheckpointFilter_true : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);
      static APtr create(void);
    private:
      CheckpointFilter_true(void);
    };


    /*! \brief Checkpoint false-filter
    */
    class CheckpointFilter_false : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);
      static APtr create(void);
    private:
      CheckpointFilter_false(void);
    };


    /*! \brief Checkpoint or-filter
    */
    class CheckpointFilter_or : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);
      static APtr create(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b);
    private:
      CheckpointFilter_or(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b);

      CheckpointFilter::APtr filter_a_;
      CheckpointFilter::APtr filter_b_;
    };


    /*! \brief Checkpoint and-filter
    */
    class CheckpointFilter_and : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);
      static APtr create(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b);
    private:
      CheckpointFilter_and(const CheckpointFilter::APtr& filter_a, const CheckpointFilter::APtr& filter_b);

      CheckpointFilter::APtr filter_a_;
      CheckpointFilter::APtr filter_b_;
    };



    /*! \brief Checkpoint not-filter
    */
    class CheckpointFilter_not : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);
      static APtr create(const CheckpointFilter::APtr& filter);
    private:
      CheckpointFilter_not(const CheckpointFilter::APtr& filter);

      CheckpointFilter::APtr filter_;
    };


    /*! \brief Checkpoint filter accepting only specified checkpoint ids
    */
    class CheckpointFilter_checkpoint_ids : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      static APtr create(const std::string& id);
      static APtr create(const std::set<std::string>& ids);

    private:
      CheckpointFilter_checkpoint_ids(const std::string& id);
      CheckpointFilter_checkpoint_ids(const std::set<std::string>& ids);

      std::set<std::string> ids_;
    };


    /*! \brief Checkpoint filter skip complete checkpoints
    */
    class CheckpointFilter_skip_complete : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      static APtr create(void);

    private:
      CheckpointFilter_skip_complete(void);
    };



    /*! \brief Checkpoint filter accepting checkpoint if one of the attributes exists
     */
    class CheckpointFilter_accept_if_attr_exist : public CheckpointFilter {
    public:
      bool operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure);

      static APtr create(const CheckpointAttr::APtr& attr);

    private:
      CheckpointFilter_accept_if_attr_exist(const CheckpointAttr::APtr& attr);

      CheckpointAttr::APtr attr_;
    };



    
  } // End namespace, Utility
} // End Namespace, Giritech
#endif
