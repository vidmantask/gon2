/*! \file UY_Checkpoint.hxx
    \brief This file contains classes for checkpoints
*/
#ifndef UY_CHECKPOINT_HXX
#define UY_CHECKPOINT_HXX

#include <string>
#include <map>

#ifdef ANDROID
namespace std {
typedef int wint_t;
}
#endif

#include <boost/shared_ptr.hpp>
#include <boost/utility.hpp>
#include <boost/ref.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <lib/utility/UY_Exception.hxx>

#include <lib/utility/UY_CheckpointAttr.hxx>


namespace Giritech {
  namespace Utility {


    class CheckpointHandler;




    /*! \brief Class represinting a checkpoint

    When constructing the ckeckpoint, the checkpoint is immediately send to the checkpoint handler.
    By sending the checkpoint from the constructor, a checkpoint is looking and acting like a
    scoped checkpoint.

    Before sending the checkpoint to the checkpoint handler a timestamp attribute is added to the checkpoint.

    Example of adding checkpoint with one attributes:
    \code
    Checkpoint checkpoint(checkpoint_handler, "my_point", CheckpointAttr::type_info, CheckpointAttr("my_attr", "my_value");
    ...
    \endcode
    Will result in the following checkpoint, here represented as XML:
    \code
    <my_point type="info" my_attr="my_value" ts="xxxxx" />
    \endcode


    */
    class Checkpoint : boost::noncopyable {
    public:


      /*! \brief This exception is thrown when ther is a problem with the checkpoint
       */
      class Exception_checkpoint : public Giritech::Utility::Exception {
      public:
        Exception_checkpoint(const std::string& message)
          : Exception(message) {
        }
      };

      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7, const CheckpointAttr::APtr& attr_8);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7, const CheckpointAttr::APtr& attr_8, const CheckpointAttr::APtr& attr_9);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg);
      Checkpoint(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg, const CheckpointAttr::Structure structure);

      static void add_attr(CheckpointAttr::APtrs& attrs_arg, const CheckpointAttr::APtr& attr);
    };



    /*! \brief Class representing a scoped checkpoint

    A scoped checkpoint send a checkpoint to the checkpoint handler ehdn the checkpoint is constructed,
    and when it is destructed.

    The checkpoint send at destruction time contains a durration attribute and the addiational attributes added with the
    add_complete_attr method.

    Example of adding a scoped checkpoint:
    \code
    CheckpointScope checkpoint(checkpoint_handler, "my_point", CheckpointAttr::type_info, CheckpointAttr("my_attr", "my_value"));
    ...
    checkpoint.add_complete_attr(CheckpointAttr("my_complete_attr", "my_complete_value"));
    ...
    \endcode
    Will result in the following checkpoint, here represented as XML:
    \code
    <my_point type="info" my_attr="my_value" ts="xxxxx" />
    ...
    <my_point my_complete_attr="my_complete_value" durration="xxxx" ts="xxxxx" />
    \endcode

    */
    class CheckpointScope : boost::noncopyable {
    public:
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6, const CheckpointAttr::APtr& attr_7);
      CheckpointScope(CheckpointHandler& handler, const std::string& id, const CheckpointAttr::APtrs& attrs_arg);
      ~CheckpointScope(void);

      /*! \brief Add a attribute that will be included in the compleate checkpoint
      */
      void add_complete_attr(const CheckpointAttr::APtr& attr_1);

      /*
       *
       */
      boost::uint64_t get_scope_id(void) const;

    private:
      CheckpointHandler& handler_;
      std::string id_;
      CheckpointAttr::APtrs complete_attrs_;

      boost::posix_time::ptime time_start_;
      boost::uint64_t scope_id_;

      boost::uint64_t get_scope_id_next(void);
      static boost::uint64_t global_scope_id;
      static boost::recursive_mutex global_scope_id__mutex_;

    };



    /*! \brief Class representing a checkpoint test predicate
     \code
     CheckpointIsLogging check_point_is_logging(checkpoint_handler, "my_point", CheckpointAttr::type_info);
     if(check_point_is_logging()) {
     	 ...
     }

    */
    class CheckpointIsLogging : boost::noncopyable {
    public:
      CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2);
      CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3);
      CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4);
      CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5);
      CheckpointIsLogging(CheckpointHandler& handler, const std::string& id, const CheckpointAttr_Module::APtr& attr_1, const CheckpointAttr_Type::APtr& attr_2, const CheckpointAttr::APtr& attr_3, const CheckpointAttr::APtr& attr_4, const CheckpointAttr::APtr& attr_5, const CheckpointAttr::APtr& attr_6);

      bool operator()(void);
    private:
      bool is_logging_;

    };



  } // End namespace, Utility
} // End Namespace, Giritech
#endif
