/*! \file UY_CheckpointHandler.cxx
    \brief This file contains the implementation of class for handling checkpoints
*/

#include <iostream>
#include <boost/none.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>


using namespace Giritech::Utility;
using namespace std;


/*
   ------------------------------------------------------------------
   Implementation CheckpointHandler
   ------------------------------------------------------------------
*/
CheckpointHandler::CheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter, const CheckpointOutputHandler::APtr& checkpoint_output_handler, CheckpointHandler& checkpoint_handler_parent)
  :  closed_(false),
     checkpoint_filter_(checkpoint_filter),
     checkpoint_output_handler_(checkpoint_output_handler),
     checkpoint_handler_parent_(checkpoint_handler_parent) {
  assert(checkpoint_filter_);
  assert(checkpoint_output_handler_);
}
CheckpointHandler::CheckpointHandler(const CheckpointFilter::APtr& checkpoint_filter, const CheckpointOutputHandler::APtr& checkpoint_output_handler)
  : closed_(false),
    checkpoint_filter_(checkpoint_filter),
    checkpoint_output_handler_(checkpoint_output_handler),
    checkpoint_handler_parent_(boost::none) {
  assert(checkpoint_filter_);
  assert(checkpoint_output_handler_);
}
CheckpointHandler::~CheckpointHandler(void) {
}
void CheckpointHandler::close(void) {
	closed_ = true;
	assert(checkpoint_output_handler_);
	checkpoint_output_handler_->close();
}

CheckpointHandler::APtr CheckpointHandler::create(const CheckpointFilter::APtr& checkpoint_filter, const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
  return CheckpointHandler::APtr(new CheckpointHandler(checkpoint_filter, checkpoint_output_handler));
}
CheckpointHandler::APtr CheckpointHandler::create(const CheckpointFilter::APtr& checkpoint_filter, const CheckpointOutputHandler::APtr& checkpoint_output_handler, CheckpointHandler& checkpoint_handler_parent) {
  return CheckpointHandler::APtr(new CheckpointHandler(checkpoint_filter, checkpoint_output_handler, checkpoint_handler_parent));
}
CheckpointHandler::APtr CheckpointHandler::create_cout_all(void) {
  return CheckpointHandler::APtr(new CheckpointHandler(CheckpointFilter_true::create(),CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create())));
}
CheckpointHandler::APtr CheckpointHandler::create_cout_all_xml(void) {
  return CheckpointHandler::APtr(new CheckpointHandler(CheckpointFilter_true::create(),CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerCout::create())));
}
CheckpointHandler::APtr CheckpointHandler::create_ignore(void) {
  return CheckpointHandler::APtr(new CheckpointHandler(CheckpointFilter_false::create(),CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create())));
}


void CheckpointHandler::handle_checkpoint(const std::string& checkpoint_id, const CheckpointAttr::APtrs& event_attrs, const CheckpointAttr::Structure structure) {
  assert(checkpoint_filter_);
  assert(checkpoint_output_handler_);
  if (! closed_) {
	  if( (*checkpoint_filter_)(checkpoint_id, event_attrs, structure) ) {
		(*checkpoint_output_handler_)(checkpoint_id, event_attrs, structure);
	  }
  }
  if(checkpoint_handler_parent_) {
    checkpoint_handler_parent_.get().handle_checkpoint(checkpoint_id, event_attrs, structure);
  }
}

bool CheckpointHandler::is_logging(const std::string& checkpoint_id, const CheckpointAttr::APtrs& event_attrs,	const CheckpointAttr::Structure structure) {
	return (!closed_) && (*checkpoint_filter_)(checkpoint_id, event_attrs, structure);
}

void  CheckpointHandler::set_output_handler(const CheckpointOutputHandler::APtr& checkpoint_output_handler) {
	checkpoint_output_handler_ = checkpoint_output_handler;
}

CheckpointHandler::APtr CheckpointHandler::create_in_same_folder(const std::string& filename) {
	boost::filesystem::path abs_filename = checkpoint_output_handler_->get_destination_handler()->get_foldername() / filename;
	CheckpointOutputHandler::APtr output_handler(CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerFile::create(abs_filename, false, false)));
	return create(CheckpointFilter_true::create(), output_handler);
}
