/*! \file UY_Convert.hxx
    \brief This file contains functions/classes for conversion between simple types
*/
#ifndef UY_CONVERT_HXX
#define UY_CONVERT_HXX

#include <string>
#include <boost/asio.hpp>
#include <boost/cstdint.hpp>

#include <lib/utility/UY_Types.hxx>
#include <lib/utility/UY_DataBuffer.hxx>


namespace Giritech {
  namespace Utility {

    void convert_to_networkorder_8(const boost::uint8_t& value, byte_t& byte1);
    void convert_from_networkorder_8(boost::uint8_t& value, const byte_t& byte1);

    void convert_to_networkorder_16(const boost::uint16_t& value, byte_t& byte1, byte_t& byte2);
    void convert_from_networkorder_16(boost::uint16_t& value, const byte_t& byte1, const byte_t& byte2);

#ifndef ANDROID
    void convert_to_networkorder_32(const boost::uint32_t& value, byte_t& byte1, byte_t& byte2, byte_t& byte3, byte_t& byte4);
    void convert_from_networkorder_32(boost::uint32_t& value, const byte_t& byte1, const byte_t& byte2, const byte_t& byte3, const byte_t& byte4);
#endif

    void convert_to_networkorder_64(const boost::uint64_t& value, 
                                    byte_t& byte1, byte_t& byte2, byte_t& byte3, byte_t& byte4, 
                                    byte_t& byte5, byte_t& byte6, byte_t& byte7, byte_t& byte8);
    void convert_from_networkorder_64(boost::uint64_t& value, 
                                      const byte_t& byte1, const byte_t& byte2, const byte_t& byte3, const byte_t& byte4,
                                      const byte_t& byte5, const byte_t& byte6, const byte_t& byte7, const byte_t& byte8);

    
    /*! \brief Buffer getters
    */
    boost::uint8_t  get_uint_8(const Utility::DataBuffer::APtr& buffer, const long& idx);
    boost::uint16_t get_uint_16(const Utility::DataBuffer::APtr& buffer, const long& idx);
#ifndef ANDROID
    boost::uint32_t get_uint_32(const Utility::DataBuffer::APtr& buffer, const long& idx);
#endif
    boost::uint64_t get_uint_64(const Utility::DataBuffer::APtr& buffer, const long& idx);
    std::string get_n_string(const Utility::DataBuffer::APtr& buffer, const long& idx, const long& n);


    /*! \brief Buffer setters
    */
    void set_uint_8(const Utility::DataBuffer::APtr& buffer, const long& idx, const boost::uint8_t& value);
    void set_uint_16(const Utility::DataBuffer::APtr& buffer, const long& idx, const boost::uint16_t& value);
#ifndef ANDROID
    void set_uint_32(const Utility::DataBuffer::APtr& buffer, const long& idx, const boost::uint32_t& value);
#endif
    void set_uint_64(const Utility::DataBuffer::APtr& buffer, const long& idx, const boost::uint64_t& value);
    void set_n(const Utility::DataBuffer::APtr& buffer, const long& idx, const std::string& value);

    
  } // End namespace, Utility
} // End Namespace, Giritech
#endif
