/*! \file UY_AsyncMemGuard.hxx
    \brief This file contains classes for a async memory guard ensuring that object is alive until async callbacks are done
*/
#ifndef UY_AsyncMemGuard_HXX
#define UY_AsyncMemGuard_HXX

#include <set>

#include <lib/utility/UY_MutexLock.hxx>


namespace Giritech {
  namespace Utility {

  /*
   * Classes that wants to use AyncMemGuard must enhierit from this element class.
   */
  class AsyncMemGuardElement : public boost::noncopyable {
  public:
      typedef boost::shared_ptr<AsyncMemGuardElement> APtr;

      virtual ~AsyncMemGuardElement(void);

      virtual bool async_mem_guard_is_done(void) = 0;
      virtual std::string async_mem_guard_get_name(void) = 0;

  };


  class AsyncMemGuard : public boost::noncopyable {
  public:
      typedef boost::shared_ptr<AsyncMemGuard> APtr;

      ~AsyncMemGuard(void);

      static APtr create(boost::asio::io_service& io);

      /*
       * Add a element to guard
       */
      void add_element(const AsyncMemGuardElement::APtr& element);

      /*
       * Clean up all elements that reports that they are done
       */
      void cleanup(void);

      std::string analyze(void);

      bool is_empty(void);

      long get_element_count(void);

      boost::asio::io_service& get_io();

      static APtr get_global_guard(void);
      static void create_global_guard(boost::asio::io_service& io);
      static void destroy_global_guard(boost::asio::io_service& io);
      static void global_add_element(const AsyncMemGuardElement::APtr& element);
      static void global_cleanup(void);
      static long global_get_element_count(void);
      static std::string global_analyze(void);


  private:
      AsyncMemGuard(boost::asio::io_service& io);

      std::set<AsyncMemGuardElement::APtr> elements_;
      Utility::Mutex::APtr mutex_;
      boost::asio::io_service::strand strand_;
      boost::asio::io_service& io_;
  };

  } // End namespace, Utility
} // End Namespace, Giritech
#endif
