import ironkey

if 0:
    print 'ironkey module constants:'

    for v, k in sorted((v, k) for k, v in ironkey.__dict__.items()):
        try:
            print '%10x %s' % (v, k)
        except: pass

print 'Initialize', ironkey.Initialize()
with ironkey.Context() as ctx:
    print 'Context:',
    print ctx, dir(ctx);

    print 'GetDeviceInfo', repr(ctx.GetDeviceInfo())
    print 'GetDeviceID', repr(ctx.GetDeviceID())
    print 'GetSerialNumber', repr(ctx.GetSerialNumber())
    print 'GetVersion', ctx.GetVersion()
    print 'GetRandomStream', repr(ctx.GetRandomStream(7))

    print 'IsUserLoggedIn', ctx.IsUserLoggedIn()
    if 0:
        print 'IsDevicePasswordSet:'
        b = ctx.IsDevicePasswordSet()
        if b:
            print 'set'
        else:
            print 'SetInitialPassword', ctx.SetInitialPassword(ironkey.IK_SECURE_VOLUME_ID, "ironkey", 10 ); # 0xe8080017L

    b = ctx.IsUserLoggedIn()
    if not b:
        print 'Login', ctx.Login(ironkey.IK_SECURE_VOLUME_ID, "ironkey", False) # 0xe8080018L or 0xe8080017L
        print 'IsUserLoggedIn', ctx.IsUserLoggedIn()
        print 'GetDeviceRetriesLeft', ctx.GetDeviceRetriesLeft()

        if 0:
            print 'ChangePassword', ctx.ChangePassword(ironkey.IK_SECURE_VOLUME_ID, "ironkey1", "ironkey", 10);
            print 'ChangePassword', ctx.ChangePassword(ironkey.IK_SECURE_VOLUME_ID, "ironkey", "ironkey1", 10);

    print 'IsSecureLUN', ctx.IsSecureLUN()
    if 0:
        print 'formatting ... slow ...'
        def formatprogress(p):
            print 'format progress %s' % p
        print 'FormatDevice', ctx.FormatDevice("daslabel", True, formatprogress) # will pause afterwards ...
    if 0:
        print 'GetCDKeyFingerPrint', ctx.GetCDKeyFingerPrint() # FIXME: crashes!!!
    if 0:
        iso = "bum.iso" #"/home/mk/lars/IronKeyCD.iso"
#cp -ar iso iso2
#chmod -R +w iso2
#ISO=/home/mk/lars/larsx.iso
#mkisofs -v -r -J -l -V lars -publisher 3 -o $ISO -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-info-table -boot-load-size 4 iso2
#openssl dgst -sha256 -binary $ISO > iso.hash
##must be done on windows using nonstandard option
#openssl rsautl -sign -pss -in iso.hash -inkey privkey -out $ISO.sig
## standard option in 1.0 doesn't work on linux:
## openssl dgst -sha256 -sign privkey -sigopt rsa_padding_mode:pss -binary $ISO > iso.sig
#cat $ISO $ISO.sig > bum.iso
        def updateprogress(p):
            print 'update progress %s\r' % p,
        def verifyprogress(p):
            print 'verify progress %s\r' % p,
        print 'CDUpdate ...'
        print ctx.CDUpdate(iso, updateprogress, verifyprogress) # can fail with e80100c8

    if 0: # FIXME: FAILS! also in IK sample app!!!
        msg = 'hello'
        print 'msg', repr(msg)
        encrypted = ctx.RSAEncrypt(0, msg, ironkey.kEncryptDecrypt_RSAES_pkcs);
        print 'encrypted', repr(encrypted)
        decrypted = ctx.RSADecrypt(0, encrypted, ironkey.kEncryptDecrypt_RSAES_pkcs);
        print 'decrypted', repr(decrypted)

    rsakey = 0

    if 0:
        msg = 'hello'
        for options in [
                        ironkey.kSignVerify_RSASSA_pkcs1_nohash,
                        ironkey.kSignVerify_RSASSA_pkcs1_sha1,
                        ironkey.kSignVerify_RSASSA_pkcs1_sha256,
                        ironkey.kSignVerify_RSASSA_pss_sha1,
                        ironkey.kSignVerify_RSASSA_pss_sha256]:
            print 'RSASign',
            signature = ctx.RSASign(rsakey, msg, options)
            print len(signature), repr(signature)
            print 'RSAVerify', ctx.RSAVerify(rsakey, msg, signature, options)
            #print 'RSAVerify', ctx.RSAVerify(rsakey, msg+msg, signature, options)


    if 1:
        print 'GetRSAPublicKeyStruct'
        rsa = ctx.GetRSAPublicKeyStruct(rsakey)
        print repr(rsa)
        print 'GetRSAPublicKeyExponent',
        exp = ctx.GetRSAPublicKeyExponent(rsa)
        print repr(exp)
        print 'GetRSAPublicKeyModulus',
        mod = ctx.GetRSAPublicKeyModulus(rsa)
        print repr(mod)
        print 'BuildRSAPublicKeyStruct',
        rsa2 = ironkey.BuildRSAPublicKeyStruct(mod, exp)
        print rsa2 == rsa

        challenge = 'there is no spoon'
        print 'RSASign',
        signature = ctx.RSASign(rsakey, challenge, ironkey.kSignVerify_RSASSA_pkcs1_nohash)
        print repr(signature)

        def i2osp(long_val):
            """
            I2OSP converts a nonnegative integer to an octet string of a specified length.
            http://tools.ietf.org/html/rfc3447#section-4.1
            """
            le_bytes = []
            while long_val or len(le_bytes) & 1: # until all done AND even number
                le_bytes.append(int(long_val & 0xff))
                long_val >>= 8
            return ''.join(chr(x) for x in reversed(le_bytes))

        def os2ip(octetstring):
            """
            OS2IP converts an octet string to a nonnegative integer.
            http://tools.ietf.org/html/rfc3447#section-4.2
            """
            long_val = 0L
            for c in octetstring:
                long_val = (long_val << 8) | ord(c)
            return long_val

        designed = i2osp(long.__pow__(os2ip(signature), os2ip(exp), os2ip(mod)))
        print 'RSA designed:', repr(designed)

        zeropos = designed.find('\0', 2)
        assert designed == '\x00\x01' + '\xff' * (zeropos-2) + '\0' + challenge
        print 'RSA pkcs#1 verified successfully'

    if 0:
        print 'logging out ... slow ...'
        print 'Logout', ctx.Logout()
        print 'IsUserLoggedIn', ctx.IsUserLoggedIn()
