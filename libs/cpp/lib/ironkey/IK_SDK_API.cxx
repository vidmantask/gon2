#include <boost/python.hpp>
using namespace boost::python;

typedef char char_t; // IK header files needs this ...
#define IKSDK_STATIC
#include "IK_API_defines.h"
#include "IK_ApplicationAPI.h"
#include "IK_DeviceAPI.h"


/* raise python exception on IK error */
static void check(IKError e)
{
	if (e != IK_NO_ERROR)
	{
		// e = e & ~0x80000; // It seems like this bit always is set - try to strip if off
		const char * msg =
			e == IK_NO_ERROR ? "IK_NO_ERROR" :
			e == IKSDK_WARNING ? "IKSDK_WARNING" :
			e == IKSDK_INFORMATIONAL ? "IKSDK_INFORMATIONAL" :
			e == SDK_ERROR ? "SDK_ERROR" :
			e == IKSDK_CUSTOMER ? "IKSDK_CUSTOMER" :
			e == IKSDK_COMPONENT ? "IKSDK_COMPONENT" :
			e == IKSDK_ERROR ? "IKSDK_ERROR" :
			e == IKSDK_NULL_PTR_ERROR ? "IKSDK_NULL_PTR_ERROR" :
			e == IKSDK_BAD_PARAM ? "IKSDK_BAD_PARAM" :
			e == IKSDK_GENERAL_FAILURE ? "IKSDK_GENERAL_FAILURE" :
			e == IKSDK_OPTION_NOT_SUPPORTED ? "IKSDK_OPTION_NOT_SUPPORTED" :
			e == IKSDK_PWD_ENCRYPT_ERROR ? "IKSDK_PWD_ENCRYPT_ERROR" :
			e == IKSDK_PWD_UNAVAILABLE_ERROR ? "IKSDK_PWD_UNAVAILABLE_ERROR" :
			e == IKSDK_PWD_MIN_REQUIREMENTS_ERROR ? "IKSDK_PWD_MIN_REQUIREMENTS_ERROR" :
			e == IKSDK_PWD_USER_SET ? "IKSDK_PWD_USER_SET" :
			e == IKSDK_GENERAL_SERVER_ERROR ? "IKSDK_GENERAL_SERVER_ERROR" :
			e == IKSDK_SERVER_RESPONSE_ERROR ? "IKSDK_SERVER_RESPONSE_ERROR" :
			e == IKSDK_PROXY_SERVER_ERROR ? "IKSDK_PROXY_SERVER_ERROR" :
			e == IKSDK_WRITING_ENT_POLICY_ERROR ? "IKSDK_WRITING_ENT_POLICY_ERROR" :
			e == IKSDK_POLICY_ALREADY_EXISTS_ERROR ? "IKSDK_POLICY_ALREADY_EXISTS_ERROR" :
			e == IKSDK_POLICY_NOT_AVAILABLE_ERROR ? "IKSDK_POLICY_NOT_AVAILABLE_ERROR" :
			e == IKSDK_BAD_POLICY ? "IKSDK_BAD_POLICY" :
			e == IKSDK_UNZIP_APP_ERROR ? "IKSDK_UNZIP_APP_ERROR" :
			e == IKSDK_NO_KEY_FOUND ? "IKSDK_NO_KEY_FOUND" :
			"Unknown";
		PyErr_SetObject(PyExc_RuntimeError, boost::python::make_tuple(e, msg).ptr());
		boost::python::throw_error_already_set();
	}
}

static void Initialize()
{
	check(IK_Initialize());
}

static void Terminate()
{
	check(IK_Terminate());
}

// we use global variables for context of unbound C callbacks
static boost::python::object staticPyUpdateProgress;
static boost::python::object staticPyVerifyProgress;
static boost::python::object staticPyFormatProgress;

class Context
{
private:
	IKContext *ikcontext;
	std::string device_path;
public:
	Context(void)
	{
		device_path.clear();
	}

	Context(std::string path)
	{
		device_path = path;
	}

	boost::shared_ptr<Context> __enter__(void)
	{
#if defined(WIN32)
		check(IK_OpenDeviceContext(device_path.empty() ? 0 : device_path.c_str()[0], &ikcontext)); // FIXME: path is ignored???
#else
		check(IK_OpenDeviceContext(device_path.empty() ? NULL : device_path.c_str(), &ikcontext)); // FIXME: path is ignored???
#endif
		return boost::shared_ptr<Context>(this);
	}

	void __exit__(PyObject *type, PyObject *attr_err, PyObject *traceback)
	{
		check(IK_ReleaseDeviceContext(ikcontext));
		ikcontext = NULL;
	}

	// IK_ApplicationAPI.h

	boost::python::tuple GetDeviceInfo(void)
	{
#if defined(WIN32)
		wchar_t cdpath[2] = { 0x00, 0x00 }, preferredpath[2] = { 0x00, 0x00 };
#else
		char cdpath[100] = { 0x00 }, preferredpath[100] = { 0x00 };
#endif
		check(IK_GetDeviceInfo(ikcontext, cdpath, preferredpath));
		return boost::python::make_tuple(cdpath, preferredpath);
	}

	std::string GetDeviceID(void)
	{
		char deviceID[8] = { 0x00 };
		check(IK_GetDeviceID(ikcontext, deviceID, sizeof(deviceID)));
		return deviceID;
	}

	std::string GetSerialNumber(void)
	{
		char deviceSerialNumber[20] = { 0x00 };
		check(IK_GetSerialNumber(ikcontext, deviceSerialNumber, sizeof(deviceSerialNumber)));
		return deviceSerialNumber;
	}

	std::string GetRandomStream(uint8_t numBytes)
	{
		std::string randomString(numBytes, '\0');
		check(IK_GetRandomStream(ikcontext, randomString.length(), (uint8_t*)randomString.c_str()));
		return randomString;
	}

	bool IsUserLoggedIn(void)
	{
		uint8_t bIsLoggedIn;
		check(IK_IsUserLoggedIn(ikcontext, &bIsLoggedIn));
		return bIsLoggedIn;
	}

	boost::python::tuple GetVersion(void)
	{
		uint16_t version = 0;
		uint16_t sdkVersion = 0;
		check(IK_GetVersion(ikcontext, &version, &sdkVersion));
		return boost::python::make_tuple(version, sdkVersion);
	}

	// IK_DeviceAPI.h

	void SetInitialPassword(uint16_t id, const std::string password, uint8_t numRetries = 10)
	{
		check(IK_SetInitialPassword(ikcontext, id, password.c_str(), password.length(), numRetries));
	}

	void ChangePassword(uint16_t id, const std::string newPassword, const std::string originalPassword, uint8_t numRetries = 10)
	{
		check(IK_ChangePassword(ikcontext, id, newPassword.c_str(), newPassword.length(), originalPassword.c_str(), originalPassword.length(), numRetries));
	}

	void Login(uint16_t id, const std::string password, uint16_t suppressVolumeMount = 0)
	{
		check(IK_Login(ikcontext, id, password.c_str(), password.length(), suppressVolumeMount));
	}

	uint16_t GetDeviceRetriesLeft()
	{
		uint16_t retriesLeft;
		check(IK_GetDeviceRetriesLeft(ikcontext, &retriesLeft));
		return retriesLeft;
	}

	void Logout()
	{
		check(IK_Logout(ikcontext));
	}

	std::string ReadBox(uint8_t boxNumber, uint32_t boxOffset, int32_t dataLen)
	{
		std::string data(dataLen, '\0');
		check(IK_ReadBox(ikcontext, boxNumber, boxOffset, (void*)data.c_str(), dataLen));
		return data;
	}

	void WriteBox(uint8_t boxNumber, uint32_t boxOffset, std::string data)
	{
		check(IK_WriteBox(ikcontext, boxNumber, boxOffset, (void*)data.c_str(), data.length()));
	}

//	IK_SDK_API IKError IK_OpenBoxRead( IKContext *context, void *data, int32_t dataLen );
//	IK_SDK_API IKError IK_OpenBoxWrite( IKContext *context, void *data, int32_t dataLen );

//	IK_SDK_API IKError IK_AcquireWhiteList( IKContext *context, IKWhitelist **whitelist );
//	IK_SDK_API IKError IK_ReleaseWhiteList( IKContext *context, IKWhitelist *whitelist );

	std::string RSASign(uint8_t keyID, std::string message, int options )
	{
		uint32_t l;
		check(IK_RSASign(ikcontext, keyID, (void*)message.c_str(), message.length(), NULL, &l, (IKRSACryptoOptions)options));
		std::string signature(l, '\0');
		check(IK_RSASign(ikcontext, keyID, (void*)message.c_str(), message.length(), (void*)signature.c_str(), &l, (IKRSACryptoOptions)options));
		return signature;
	}

	void RSAVerify(uint8_t keyID, std::string message, std::string signature, int options )
	{
		check(IK_RSAVerify(ikcontext, keyID, (void*)message.c_str(), message.length(), (void*)signature.c_str(), signature.length(), (IKRSACryptoOptions)options));
	}

	std::string RSAEncrypt(uint8_t keyID, std::string clearText, int options )
	{
		uint32_t l;
		check(IK_RSAEncrypt(ikcontext, keyID, (void*)clearText.c_str(), clearText.length(), NULL, &l, (IKRSACryptoOptions)options));
		std::string cipherText(l, '\0');
		check(IK_RSAEncrypt(ikcontext, keyID, (void*)clearText.c_str(), clearText.length(), (void*)cipherText.c_str(), &l, (IKRSACryptoOptions)options));
		return cipherText;
	}

	std::string RSADecrypt(uint8_t keyID, std::string cipherText, int options )
	{
		uint32_t l;
		check(IK_RSADecrypt(ikcontext, keyID, (void*)cipherText.c_str(), cipherText.length(), NULL, &l, (IKRSACryptoOptions)options));
		std::string clearText(l, '\0');
		check(IK_RSADecrypt(ikcontext, keyID, (void*)cipherText.c_str(), cipherText.length(), (void*)clearText.c_str(), &l, (IKRSACryptoOptions)options));
		return clearText;
	}

	static std::string BuildRSAPublicKeyStruct(std::string mod, std::string exp)
	{
		std::string keystruct(270, '\0');
		check(IK_BuildRSAPublicKeyStruct((uint8_t *)mod.c_str(), mod.length(), (uint8_t *)exp.c_str(), exp.length(), (uint8_t *) keystruct.c_str(), keystruct.length()));
		return keystruct;
	}

	std::string GetRSAPublicKeyModulus(std::string inbuffer)
	{
		uint8_t *publicmodulus;
		uint32_t length;
		check(IK_GetRSAPublicKeyModulus(ikcontext, (uint8_t *)inbuffer.c_str(), inbuffer.length(), &publicmodulus, &length));
		return std::string((char*)publicmodulus, length);
	}

	std::string GetRSAPublicKeyExponent(std::string inbuffer)
	{
		uint8_t *publicexponent;
		uint32_t length;
		check(IK_GetRSAPublicKeyExponent(ikcontext, (uint8_t *)inbuffer.c_str(), inbuffer.length(), &publicexponent, &length));
		return std::string((char*)publicexponent, length);
	}

	std::string GetRSAPublicKeyStruct(uint8_t keyID)
	{
		std::string keybuffer(270, '\0');
		uint32_t size;
		check(IK_GetRSAPublicKeyStruct(ikcontext, keyID, (uint8_t *)keybuffer.c_str(), &size ));
		return keybuffer.substr(0, size);
	}

//	// SDK SPECIFIC FUNCTIONS
//	IK_SDK_API IKError IK_FlashTrash( IKContext *context );
//	IK_SDK_API IKError IK_SetFeature( IKContext *context, uint32_t featureID, uint8_t *featureValue, uint32_t featureValueSize );
//	IK_SDK_API IKError IK_GetFeature( IKContext *context, uint32_t featureID, uint8_t *featureValue, uint32_t featureValueSize );

	static void FormatProgressCallback( void *data, int32_t percentage)
	{
		if (staticPyFormatProgress)
			staticPyFormatProgress(percentage);
	}
	void FormatDevice( const std::string driveLabel, bool quickFormat, boost::python::object pyFormatProgress)
	{
		staticPyFormatProgress = pyFormatProgress;
		check(IK_FormatDevice(ikcontext, driveLabel.c_str(), quickFormat, FormatProgressCallback));
		staticPyFormatProgress = boost::python::object(); // good? perhaps not - but it prevents later use
	}

	void DismountDevice( const char_t cdDriveLetter)
	{
		check(IK_DismountDevice(cdDriveLetter));
	}

	static void CDUpdateProgressCallback(uint32_t percentage)
	{
		if (staticPyUpdateProgress)
			staticPyUpdateProgress(percentage);
	}
	static void CDVerifyProgressCallback(uint32_t percentage)
	{
		if (staticPyVerifyProgress)
			staticPyVerifyProgress(percentage);
	}
	void CDUpdate( std::string filePath, boost::python::object update, boost::python::object verify)
	{
		staticPyUpdateProgress = update;
		staticPyVerifyProgress = verify;
		check(IK_CDUpdate(ikcontext, (uint8_t *)filePath.c_str(), CDUpdateProgressCallback, CDVerifyProgressCallback));
		staticPyUpdateProgress = boost::python::object(); // good? perhaps not - but it prevents later use
		staticPyVerifyProgress = boost::python::object(); // good? perhaps not - but it prevents later use
	}

	bool IsSecureLUN(void)
	{
		uint8_t bIsSecureLUN;
		check(IK_IsSecureLUN(ikcontext, &bIsSecureLUN));
		return bIsSecureLUN;
	}

	bool IsDevicePasswordSet(void)
	{
		uint8_t bIsDevicePasswordSet;
		check(IK_IsDevicePasswordSet(ikcontext, &bIsDevicePasswordSet));
		return bIsDevicePasswordSet;
	}

	bool IsBoxLoggedIn(uint8_t id)
	{
		uint8_t bIsBoxLoggedIn;
		check(IK_IsBoxLoggedIn(ikcontext, id, &bIsBoxLoggedIn));
		return bIsBoxLoggedIn;
	}

	std::string GetCDKeyFingerPrint(void)
	{
		std::string fingerprint(1000, '\0');
		uint32_t l = fingerprint.length();
		check(IK_GetCDKeyFingerPrint(ikcontext, (uint8_t *)fingerprint.c_str(), &l)); // FIXME: crashes!!!
		return fingerprint;
	}

};


BOOST_PYTHON_MODULE(ironkey) {

	// IK_ApplicationAPI.h

	boost::python::scope().attr("IK_SECURE_VOLUME_ID") = (unsigned)IK_SECURE_VOLUME_ID;

	boost::python::scope().attr("IK_NO_ERROR") = (unsigned)IK_NO_ERROR;
	boost::python::scope().attr("IKSDK_WARNING") = (unsigned)IKSDK_WARNING;
	boost::python::scope().attr("IKSDK_INFORMATIONAL") = (unsigned)IKSDK_INFORMATIONAL;
	boost::python::scope().attr("SDK_ERROR") = (unsigned)SDK_ERROR;
	boost::python::scope().attr("IKSDK_CUSTOMER") = (unsigned)IKSDK_CUSTOMER;
	boost::python::scope().attr("IKSDK_COMPONENT") = (unsigned)IKSDK_COMPONENT;
	boost::python::scope().attr("IKSDK_ERROR") = (unsigned)IKSDK_ERROR;
	boost::python::scope().attr("IKSDK_NULL_PTR_ERROR") = (unsigned)IKSDK_NULL_PTR_ERROR;
	boost::python::scope().attr("IKSDK_BAD_PARAM") = (unsigned)IKSDK_BAD_PARAM;
	boost::python::scope().attr("IKSDK_GENERAL_FAILURE") = (unsigned)IKSDK_GENERAL_FAILURE;
	boost::python::scope().attr("IKSDK_OPTION_NOT_SUPPORTED") = (unsigned)IKSDK_OPTION_NOT_SUPPORTED;
	boost::python::scope().attr("IKSDK_PWD_ENCRYPT_ERROR") = (unsigned)IKSDK_PWD_ENCRYPT_ERROR;
	boost::python::scope().attr("IKSDK_PWD_UNAVAILABLE_ERROR") = (unsigned)IKSDK_PWD_UNAVAILABLE_ERROR;
	boost::python::scope().attr("IKSDK_PWD_MIN_REQUIREMENTS_ERROR") = (unsigned)IKSDK_PWD_MIN_REQUIREMENTS_ERROR;
	boost::python::scope().attr("IKSDK_PWD_USER_SET") = (unsigned)IKSDK_PWD_USER_SET;
	boost::python::scope().attr("IKSDK_GENERAL_SERVER_ERROR") = (unsigned)IKSDK_GENERAL_SERVER_ERROR;
	boost::python::scope().attr("IKSDK_SERVER_RESPONSE_ERROR") = (unsigned)IKSDK_SERVER_RESPONSE_ERROR;
	boost::python::scope().attr("IKSDK_PROXY_SERVER_ERROR") = (unsigned)IKSDK_PROXY_SERVER_ERROR;
	boost::python::scope().attr("IKSDK_WRITING_ENT_POLICY_ERROR") = (unsigned)IKSDK_WRITING_ENT_POLICY_ERROR;
	boost::python::scope().attr("IKSDK_POLICY_ALREADY_EXISTS_ERROR") = (unsigned)IKSDK_POLICY_ALREADY_EXISTS_ERROR;
	boost::python::scope().attr("IKSDK_POLICY_NOT_AVAILABLE_ERROR") = (unsigned)IKSDK_POLICY_NOT_AVAILABLE_ERROR;
	boost::python::scope().attr("IKSDK_BAD_POLICY") = (unsigned)IKSDK_BAD_POLICY;
	boost::python::scope().attr("IKSDK_UNZIP_APP_ERROR") = (unsigned)IKSDK_UNZIP_APP_ERROR;
	boost::python::scope().attr("IKSDK_NO_KEY_FOUND") = (unsigned)IKSDK_NO_KEY_FOUND;

	// IK_DeviceAPI.h

	// IKRSACryptoOptions
	boost::python::scope().attr("kCryptoNoOption") = (int)kCryptoNoOption;
	boost::python::scope().attr("kEncryptDecrypt_RSAES_oaep_sha1") = (int)kEncryptDecrypt_RSAES_oaep_sha1;
	boost::python::scope().attr("kEncryptDecrypt_RSAES_oaep_sha256") = (int)kEncryptDecrypt_RSAES_oaep_sha256;
	boost::python::scope().attr("kEncryptDecrypt_RSAES_pkcs") = (int)kEncryptDecrypt_RSAES_pkcs;
	boost::python::scope().attr("kSignVerify_RSASSA_pkcs1_sha1") = (int)kSignVerify_RSASSA_pkcs1_sha1;
	boost::python::scope().attr("kSignVerify_RSASSA_pkcs1_nohash") = (int)kSignVerify_RSASSA_pkcs1_nohash;
	boost::python::scope().attr("kSignVerify_RSASSA_pkcs1_sha256") = (int)kSignVerify_RSASSA_pkcs1_sha256;
	boost::python::scope().attr("kSignVerify_RSASSA_pss_sha1") = (int)kSignVerify_RSASSA_pss_sha1;
	boost::python::scope().attr("kSignVerify_RSASSA_pss_sha256") = (int)kSignVerify_RSASSA_pss_sha256;

	boost::python::scope().attr("IK_FLASH_TRASH") = IK_FLASH_TRASH;

	boost::python::scope().attr("IK_PUBLIC_BOXID") = IK_PUBLIC_BOXID;
	boost::python::scope().attr("IK_PRIVATE_AUTO_LOGIN_BOXID") = IK_PRIVATE_AUTO_LOGIN_BOXID;
	boost::python::scope().attr("IK_PRIVATE0_BOXID") = IK_PRIVATE0_BOXID;
	boost::python::scope().attr("IK_PRIVATE1_BOXID") = IK_PRIVATE1_BOXID;
	boost::python::scope().attr("IK_PRIVATE2_BOXID") = IK_PRIVATE2_BOXID;

	def("Initialize", &Initialize); // IK_SDK_API IKError	IK_Initialize( void );
	def("Terminate", &Terminate); // IK_SDK_API IKError	IK_Terminate( void );

	class_< Context, boost::shared_ptr<Context>, boost::noncopyable >("Context")

			// IK_ApplicationAPI.h
			.def(init<>())
			.def(init<std::string>())
			.def("__enter__", &Context::__enter__)
			.def("__exit__", &Context::__exit__)
			.def("GetDeviceInfo", &Context::GetDeviceInfo)
			.def("GetDeviceID", &Context::GetDeviceID)
			.def("GetVersion", &Context::GetVersion)
			.def("GetSerialNumber", &Context::GetSerialNumber)
			.def("GetRandomStream", &Context::GetRandomStream)
			.def("IsUserLoggedIn", &Context::IsUserLoggedIn)

			// IK_DeviceAPI.h
			.def("SetInitialPassword", &Context::SetInitialPassword)
			.def("ChangePassword", &Context::ChangePassword)

			.def("Login", &Context::Login)
			.def("GetDeviceRetriesLeft", &Context::GetDeviceRetriesLeft)
			.def("Logout", &Context::Logout)

			.def("ReadBox", &Context::ReadBox)
			.def("WriteBox", &Context::WriteBox)
//			.def("OpenBoxRead", &Context::OpenBoxRead)
//			.def("OpenBoxWrite", &Context::OpenBoxWrite)

//			.def("AcquireWhiteList", &Context::AcquireWhiteList)
//			.def("ReleaseWhiteList", &Context::ReleaseWhiteList)

			// Public Key Operations
			.def("RSASign", &Context::RSASign)
			.def("RSAVerify", &Context::RSAVerify)
			.def("RSAEncrypt", &Context::RSAEncrypt)
			.def("RSADecrypt", &Context::RSADecrypt)

			.def("GetRSAPublicKeyModulus", &Context::GetRSAPublicKeyModulus)
			.def("GetRSAPublicKeyExponent", &Context::GetRSAPublicKeyExponent)
			.def("GetRSAPublicKeyStruct", &Context::GetRSAPublicKeyStruct)

			// SDK SPECIFIC FUNCTIONS
//			.def("FlashTrash", &Context::FlashTrash)
//			.def("SetFeature", &Context::SetFeature)
//			.def("GetFeature", &Context::GetFeature)

			.def("FormatDevice", &Context::FormatDevice)
			.def("DismountDevice", &Context::DismountDevice)

			.def("CDUpdate", &Context::CDUpdate)

			.def("IsSecureLUN", &Context::IsSecureLUN)
			.def("IsDevicePasswordSet", &Context::IsDevicePasswordSet)
			.def("IsBoxLoggedIn", &Context::IsBoxLoggedIn)

			.def("GetCDKeyFingerPrint", &Context::GetCDKeyFingerPrint);

	def("BuildRSAPublicKeyStruct", &Context::BuildRSAPublicKeyStruct);

}
