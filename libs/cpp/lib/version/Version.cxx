/*! \file Version.cxx
 \brief Version information
 */

#include <sstream>

#include <lib/version/Version.include.hxx>
#include <lib/version/Version.hxx>

using namespace Giritech::Version;

/*
 * ------------------------------------------------------------------
 * Version implementation
 * ------------------------------------------------------------------
 */
Version::Version(const unsigned long& major,
                 const unsigned long& minor,
                 const unsigned long& bugfix,
                 const unsigned long& build_id) :
    version_major_(major), version_minor_(minor), version_bugfix_(bugfix), version_build_id_(build_id) {
}

Version::~Version(void) {
}


Version::APtr Version::create_current(void) {
    Version::APtr version(new Version(GVAR_VERSION_MAJOR, GVAR_VERSION_MINOR, GVAR_VERSION_BUGFIX, GVAR_VERSION_BUILD_ID));
    version->set_version_depot(GVAR_VERSION_DEPOT);
    version->set_build_date(GVAR_BUILD_DATE);
    return version;
}


Version::APtr Version::create(const unsigned long& major,
                              const unsigned long& minor,
                              const unsigned long& bugfix,
                              const unsigned long& build_id) {
    Version::APtr version(new Version(major, minor, bugfix, build_id));
    return version;
}

void Version::set_build_date(const std::string& build_date) {
    build_date_ = build_date;
}

void Version::set_version_depot(const std::string& version_depot) {
    version_depot_ = version_depot;
}

unsigned long Version::get_version_major(void) const {
    return version_major_;
}

unsigned long Version::get_version_minor(void) const {
    return version_minor_;
}

unsigned long Version::get_version_bugfix(void) const {
    return version_bugfix_;
}

unsigned long Version::get_version_build_id(void) const {
    return version_build_id_;
}

std::string Version::get_version_depot(void) const {
    return version_depot_;
}

std::string Version::get_build_date(void) const {
    return build_date_;
}

std::string Version::get_version_string(void) const {
    std::stringstream ss;

    ss << get_version_major();
    ss << ".";
    ss << get_version_minor();
    ss << ".";
    ss << get_version_bugfix();
    ss << ".";
    ss << get_version_build_id();
    return ss.str();
}


bool Version::equal(const APtr& other_version) const {
    return get_version_major() == other_version->get_version_major() &&
        get_version_minor() == other_version->get_version_minor() &&
        get_version_bugfix() == other_version->get_version_bugfix() &&
        get_version_build_id() == other_version->get_version_build_id();
}
