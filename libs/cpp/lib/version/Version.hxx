/*! \file Version.hxx
 \brief Version information
 */
#ifndef VERSION_HXX
#define VERSION_HXX

#include <sstream>

#include <boost/shared_ptr.hpp>

namespace Giritech {
namespace Version {


class Version {
public:
    typedef boost::shared_ptr<Version> APtr;

    ~Version(void);

    static APtr create_current(void);
    static APtr create(const unsigned long& major,
                       const unsigned long& minor,
                       const unsigned long& bugfix,
                       const unsigned long& build_id);

    void set_build_date(const std::string& build_date);
    void set_version_depot(const std::string& version_depot);

    unsigned long get_version_major(void) const;
    unsigned long get_version_minor(void) const;
    unsigned long get_version_bugfix(void) const;
    unsigned long get_version_build_id(void) const;

    std::string get_version_string(void) const;

    std::string get_version_depot(void) const;
    std::string get_build_date(void) const;

    bool equal(const APtr& other_version) const;

private:
    Version(const unsigned long& major,
            const unsigned long& minor,
            const unsigned long& bugfix,
            const unsigned long& build_id);

    unsigned long version_major_;
    unsigned long version_minor_;
    unsigned long version_bugfix_;
    unsigned long version_build_id_;

    std::string version_depot_;
    std::string build_date_;
};

}
}

#endif
