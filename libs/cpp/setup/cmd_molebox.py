"""
This file contains a python wrapper for generating and validiting a molebox of a program
"""
import sys
import os
import shutil
import glob
import subprocess
import tempfile

COMMAND_PACK = 0
COMMAND_DEPLOY = 1

def run_command_pack(setup_filename):
    execfile(setup_filename, globals())

    #
    # Create temp-folder
    #
    temp_folder = tempfile.mkdtemp()

    #
    # Copying files
    #
    for abs_filename in setup_files:
        if not os.path.exists(abs_filename):
            abs_filename = os.path.abspath(os.path.join(setup_root, abs_filename))
        shutil.copy(abs_filename, temp_folder)

    #
    # Find molebox-file
    #
    exe_filenames = glob.glob1(temp_folder, '*.mbxcfg')
    if len(exe_filenames) != 1:
        print "Did not find excatly one exe-file for moleboxing", exe_filenames
    (exe_filename, exe_filname_ext) = os.path.splitext(exe_filenames[0])

    #
    # Running moleboxing
    #
    os.chdir(temp_folder)
    args = [setup_molebox_exe, exe_filename]
    process = subprocess.Popen(args=args, stdout=subprocess.PIPE, shell=True)
    process.wait()
    process_result = process.communicate()[0].splitlines()
    if process.returncode != 0 or len(process_result) == 0:
        print '\n'.join(process_result)
        return 1
    process_result_status = process_result[len(process_result)-1]
    if process_result_status != 'SUCCESS':
        print '\n'.join(process_result)
        return 1

    #
    # Find molebox-file
    #
    moleboxed_exe_filenames = glob.glob1(temp_folder, '*.moleboxed.exe')
    if len(moleboxed_exe_filenames) != 1:
        print "Did not find excatly one exe-file after moleboxing", moleboxed_exe_filenames
    
    moleboxed_exe_filename = os.path.abspath(os.path.join(temp_folder, moleboxed_exe_filenames[0]))
    moleboxed_exe_filename_dest = os.path.abspath(os.path.join(setup_dest_root, exe_filename))
    
    if not os.path.exists(setup_dest_root):
        os.makedirs(setup_dest_root)
        
    shutil.copyfile(moleboxed_exe_filename, moleboxed_exe_filename_dest)

    #
    # Copying additional files
    #
    for abs_filename in setup_files_copy:
        if not os.path.exists(abs_filename):
            abs_filename = os.path.abspath(os.path.join(setup_root, abs_filename))
        shutil.copy(abs_filename, setup_dest_root)


    #
    # Cleaning up
    #
    os.chdir(setup_dest_root)
    shutil.rmtree(temp_folder)


def run_command_deploy(setup_filename):
    execfile(setup_filename, globals())
    
    if not os.path.exists(setup_deploy_root):
        os.makedirs(setup_deploy_root)
    
    #
    # Copying files and additionale files
    #
    files = [];
    files.extend(setup_files)
    files.extend(setup_files_copy)
    for abs_filename in files:
        if not os.path.exists(abs_filename):
            abs_filename = os.path.abspath(os.path.join(setup_root, abs_filename))
        shutil.copy(abs_filename, setup_deploy_root)


def main():
    command = COMMAND_PACK
    if len(sys.argv) != 3:
        print "Invalid number of arguments, expected setup_filename, command('pack' | 'deploy')"
        return 1
        
    setup_filename = sys.argv[1]
    if not os.path.exists(setup_filename):
        print "Invalid setup_filename '%s' " % setup_filename
        return 1

    if sys.argv[2] == 'deploy':
        command = COMMAND_DEPLOY

    if command == COMMAND_PACK:
        run_command_pack(setup_filename)
    else:
        run_command_deploy(setup_filename)

if __name__ == '__main__':
    main()
