/*! \file COM_TunnelendpointTCP.cxx
 \brief This file contains the implementation for the tunnelendpoint-tcp class
 */
#include <functional>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointTCP.hxx>
#include <component/communication/COM_TunnelendpointTCPEventhandler.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TunnelendpointTCP implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCP::TunnelendpointTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		                             boost::asio::io_service& io,
			                         const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                                     TunnelendpointTCPControlEventhandler* tcp_ctrl_eventhandler) :
    tunnel_id_(0),
    io_(io),
    tcp_ctrl_eventhandler_(tcp_ctrl_eventhandler),
    TunnelendpointReliableCrypted(io, checkpoint_handler,this),
    tcp_eventhandler_(NULL),
    state_tcp_(StateTCP_Initializing),
    raw_tunnelendpoint_(raw_tunnelendpoint),
    connection_id_(raw_tunnelendpoint->get_connection_id()) {
    raw_tunnelendpoint_->set_eventhandler(this);
    TunnelendpointReliableCrypted::set_mutex(raw_tunnelendpoint->get_mutex());
}

TunnelendpointTCP::~TunnelendpointTCP(void) {
    state_tcp_ = StateTCP_Unknown;
}

TunnelendpointTCP::APtr TunnelendpointTCP::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		                                          boost::asio::io_service& io,
		                                          const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                                                  TunnelendpointTCPControlEventhandler* tcp_ctrl_eventhandler) {
    TunnelendpointTCP::APtr new_tunnelendpoint(new TunnelendpointTCP(checkpoint_handler, io, raw_tunnelendpoint, tcp_ctrl_eventhandler));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

void TunnelendpointTCP::set_tcp_eventhandler(TunnelendpointTCPEventhandler* tcp_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::set_tcp_eventhandler");
    tcp_eventhandler_ = tcp_eventhandler;
}

void TunnelendpointTCP::reset_tcp_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::reset_tcp_eventhandler");
    tcp_eventhandler_ = NULL;
}

void TunnelendpointTCP::reset_tcp_ctrl_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::reset_tcp_ctrl_eventhandler");
    tcp_ctrl_eventhandler_ = NULL;
}

void TunnelendpointTCP::set_mutex(const Utility::Mutex::APtr& mutex) {
    raw_tunnelendpoint_->set_mutex(mutex);
    Tunnelendpoint::set_mutex(mutex);
}

bool TunnelendpointTCP::is_closed(void) {
	return state_tcp_ == StateTCP_Closed;
}

void TunnelendpointTCP::set_tunnel_id(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::set_tunnel_id");
    tunnel_id_ = tunnel_id;
}

void TunnelendpointTCP::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::close");
    switch (state_tcp_) {
    case StateTCP_Initializing:
    case StateTCP_Connecting:
    case StateTCP_Ready:
        break;
    default:
        return;
    }
    state_tcp_ = StateTCP_Closing;

    if(! raw_tunnelendpoint_->is_closed()) {
        raw_tunnelendpoint_->aio_close_start(force);
        return;
    }
    goto_closed_state_();
}

void TunnelendpointTCP::eof(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::eof");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::eof", Attr_Communication(), CpAttr_debug());
    switch (state_tcp_) {
    case StateTCP_Connecting:
    case StateTCP_Ready:
        break;
    case StateTCP_Closed:
    case StateTCP_Closing:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for eof");
    }
    raw_tunnelendpoint_->aio_close_write_start();
}

std::pair<std::string, unsigned long> TunnelendpointTCP::get_ip_local(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::get_ip_local");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::get_ip_local", Attr_Communication(), CpAttr_debug());
    switch (state_tcp_) {
    case StateTCP_Initializing:
    case StateTCP_Connecting:
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for get_ip_local");
    }
    return raw_tunnelendpoint_->get_ip_local();
}

std::pair<std::string, unsigned long> TunnelendpointTCP::get_ip_remote(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::get_ip_remote");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::get_ip_remote", Attr_Communication(), CpAttr_debug());
    switch (state_tcp_) {
    case StateTCP_Initializing:
    case StateTCP_Connecting:
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for get_ip_remote");
    }
    return raw_tunnelendpoint_->get_ip_remote();
}

void TunnelendpointTCP::connecting(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::connecting");
    switch (state_tcp_) {
    case StateTCP_Initializing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for connecting");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::connecting", Attr_Communication(), CpAttr_debug());
    state_tcp_ = StateTCP_Connecting;
}

void TunnelendpointTCP::connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::connected");
    switch (state_tcp_) {
    case StateTCP_Connecting:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for connected");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::connected", Attr_Communication(), CpAttr_debug());
    goto_ready_state_();
}

void TunnelendpointTCP::com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::com_raw_tunnelendpoint_close");
    switch (state_tcp_) {
    case StateTCP_Initializing:
    case StateTCP_Connecting:
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for com_raw_tunnelendpoint_close");
    }

    if(state_tcp_ == StateTCP_Closing) {
    	goto_closed_state_();
    	return;
    }
   close(true);
}

void TunnelendpointTCP::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::com_raw_tunnelendpoint_close_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::com_raw_tunnelendpoint_close_eof.invalid_state", Attr_Communication(), CpAttr_debug(), CpAttr("state_tcp", state_tcp_));
        return;
    }
    if (tcp_ctrl_eventhandler_ != NULL) {
        tcp_ctrl_eventhandler_->tunnelendpoint_tcp_eh_eof(tunnel_id_);
    } else {
        raw_tunnelendpoint_->aio_close_start(false);
    }
}

void TunnelendpointTCP::com_raw_tunnelendpoint_read(const unsigned long& connection_id, const DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::com_raw_tunnelendpoint_read");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    case StateTCP_Closing:
        return; // Data read after close are ignored
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for com_raw_tunnelendpoint_read");
    }
    tunnelendpoint_send(MessagePayload::create(DataBufferManaged::create(data)));
    raw_tunnelendpoint_->aio_read_start();
}

void TunnelendpointTCP::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
    // Ignoring signal
}

void TunnelendpointTCP::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::tunnelendpoint_eh_recieve");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCP::tunnelendpoint_eh_recieve.message_ignored", Attr_Communication(), CpAttr_debug(), CpAttr("state_tcp", state_tcp_));
        return;
    }
    raw_tunnelendpoint_->aio_write_start(message->get_buffer());
}

void TunnelendpointTCP::tunnelendpoint_eh_connected(void) {
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::tunnelendpoint_eh_connected", Attr_Communication(), CpAttr_debug());
}

void TunnelendpointTCP::read_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::read_start");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCP not ready for read_start");
    }
    raw_tunnelendpoint_->aio_read_start();
}

void TunnelendpointTCP::goto_ready_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::goto_ready_state_");
    switch (state_tcp_) {
    case StateTCP_Initializing:
    case StateTCP_Connecting:
        break;
    default:
        return;
    }
    state_tcp_ = StateTCP_Ready;
    if (tcp_eventhandler_ != NULL) {
        tcp_eventhandler_->tunnelendpoint_tcp_eh_ready();
    } else {
        read_start();
    }
}

void TunnelendpointTCP::goto_closed_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::goto_closed_state_");
    switch (state_tcp_) {
    case StateTCP_Closing:
        break;
    default:
        CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::goto_closed_state_.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state", state_));
        return;
    }

    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCP::goto_closed_state_", Attr_Communication(), CpAttr_debug());
    state_tcp_ = StateTCP_Closed;

    if (tcp_eventhandler_ != NULL) {
        TunnelendpointTCPEventhandler* temp_tcp_eventhandler(tcp_eventhandler_);
        reset_tcp_eventhandler();
        temp_tcp_eventhandler->tunnelendpoint_tcp_eh_closed();
    }

    if (tcp_ctrl_eventhandler_ != NULL) {
    	TunnelendpointTCPControlEventhandler* temp_tcp_ctrl_eventhandler(tcp_ctrl_eventhandler_);
    	temp_tcp_ctrl_eventhandler->tunnelendpoint_tcp_eh_closed(tunnel_id_);
        reset_tcp_ctrl_eventhandler();
    }

    raw_tunnelendpoint_->reset_eventhandler();
    TunnelendpointReliableCrypted::close(true);
}

bool TunnelendpointTCP::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCP::async_mem_guard_is_done");
    switch (state_tcp_) {
    case StateTCP_Closed:
    case StateTCP_Unknown:
        break;
    default:
        return false;
    }
    return TunnelendpointReliableCrypted::async_mem_guard_is_done();
}

std::string TunnelendpointTCP::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}


/*
 * ------------------------------------------------------------------
 * TunnelendpointTCPTunnelServerBase implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCPTunnelServerBase::TunnelendpointTCPTunnelServerBase(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const unsigned long id) :
    TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, this),
	tcp_tunnel_eventhandler_(NULL),
    state_tcp_(StateTCP_Initializing),
    id_(id), checkpoint_tunnel_enabled_(false) {
}

TunnelendpointTCPTunnelServerBase::~TunnelendpointTCPTunnelServerBase(void) {
    state_tcp_ = StateTCP_Unknown;
}


void TunnelendpointTCPTunnelServerBase::set_tcp_eventhandler(TunnelendpointTCPTunnelServerEventhandler* tcp_tunnel_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::set_tcp_eventhandler");
    tcp_tunnel_eventhandler_ = tcp_tunnel_eventhandler;
}
void TunnelendpointTCPTunnelServerBase::reset_tcp_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::reset_tcp_eventhandler");
    tcp_tunnel_eventhandler_ = NULL;
}

void TunnelendpointTCPTunnelServerBase::set_mutex(const Utility::Mutex::APtr& mutex) {
    TunnelendpointReliableCryptedTunnel::set_mutex(mutex);
}

void TunnelendpointTCPTunnelServerBase::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::connect");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerBase::connect", Attr_Communication(), CpAttr_debug());
    Tunnelendpoint::connect();
}

void TunnelendpointTCPTunnelServerBase::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::close");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerBase::close", Attr_Communication(), CpAttr_debug());
    switch (state_tcp_) {
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        return;
    case StateTCP_Ready:
        break;
    default:
    	return;
    }
    state_tcp_ = StateTCP_Closing;
    close_down_(force);
}

bool TunnelendpointTCPTunnelServerBase::is_closed() {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::is_closed");
    return state_tcp_ == StateTCP_Closed;
}

void TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_connected");
    switch (state_tcp_) {
    case StateTCP_Initializing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerBase not ready for tunnelendpoint_eh_connected");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_connected",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));
    goto_ready_state_();
}

void TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_recieve");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closed:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerBase not ready for tunnelendpoint_eh_recieve");
    }

    /*
     * Parse message from client
     */
    TunnelendpointTCPMessage::APtr tcp_message(TunnelendpointTCPMessage::create_from_buffer(message->get_buffer()));
    switch (tcp_message->get_message_type()) {
    case TunnelendpointTCPMessage::MessageType_connect:
        tunnelendpoint_recieve_connect(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_eof:
        tunnelendpoint_recieve_eof(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_close:
        tunnelendpoint_recieve_close(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_closed:
        tunnelendpoint_recieve_closed(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_message:
        tunnelendpoint_recieve_message(tcp_message->get_payload());
        break;
    case TunnelendpointTCPMessage::MessageType_checkpoint:
    	tunnelendpoint_recieve_checkpoint(tcp_message->get_tunnel_id());
        break;
    default:
        CheckpointScope cps(
        		*checkpoint_handler_,
    			"TunnelendpointTCPTunnelServerBase::tunnelendpoint_eh_recieve.invalid_message_type",
    			Attr_Communication(), CpAttr_warning(), CpAttr("message_type", tcp_message->get_message_type()));
    }
}

void TunnelendpointTCPTunnelServerBase::tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_recieve_message");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerBase not ready for tunnelendpoint_recieve_message");
    }
    if (tcp_tunnel_eventhandler_ != NULL) {
        tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_recieve(id_, message);
    }
    return;
}

void TunnelendpointTCPTunnelServerBase::tunnelendpoint_recieve_checkpoint(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_recieve_checkpoint");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    case StateTCP_Closed:
    case StateTCP_Closing:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerBase not ready for tunnelendpoint_recieve_checkpoint_response");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_recieve_checkpoint", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    if(!checkpoint_tunnel_enabled_) {
        TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_checkpoint_response, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_error));
        tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
        return;
    }
    checkpoint_tunnel_id_ = tunnel_id;
    add_tunnelendpoint(checkpoint_tunnel_id_, checkpoint_tunnel_);
    checkpoint_tunnel_->connect();
    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_checkpoint_response, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelServerBase::goto_ready_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::goto_ready_state_");
    state_tcp_ = StateTCP_Ready;
    if (tcp_tunnel_eventhandler_ != NULL) {
        tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_ready(id_);
    }
}

void TunnelendpointTCPTunnelServerBase::tunnelendpoint_tcp_send(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_tcp_send");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerBase not ready for tunnelendpoint_tcp_send");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerBase::tunnelendpoint_tcp_send", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_message, 0, TunnelendpointTCPMessage::ReturnCodeType_ok));
    message_tcp->set_payload(message);
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

bool TunnelendpointTCPTunnelServerBase::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::async_mem_guard_is_done");
    switch (state_tcp_) {
    case StateTCP_Closed:
    case StateTCP_Unknown:
        break;
    default:
        return false;
    }
    return TunnelendpointReliableCrypted::async_mem_guard_is_done();
}

std::string TunnelendpointTCPTunnelServerBase::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void TunnelendpointTCPTunnelServerBase::goto_closed_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::goto_ready_state_");
    state_tcp_ = StateTCP_Closed;
    if (tcp_tunnel_eventhandler_ != NULL) {
        TunnelendpointTCPTunnelServerEventhandler* temp_tcp_tunnel_eventhandler(tcp_tunnel_eventhandler_);
        tcp_tunnel_eventhandler_ = NULL;
        temp_tcp_tunnel_eventhandler->tunnelendpoint_tcp_eh_closed(id_);
    }
    TunnelendpointReliableCrypted::close(true);
}

void TunnelendpointTCPTunnelServerBase::enable_checkpoint_tunnel(const boost::filesystem::path& folder) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerBase::enable_checkpoint_tunnel");
    if(checkpoint_tunnel_enabled_) {
    	return;
    }
	checkpoint_tunnel_enabled_ = true;
	checkpoint_tunnel_ = TunnelendpointCheckpointHandlerServer::create(checkpoint_handler_, io_, folder);
}

/*
 * ------------------------------------------------------------------
 * TunnelendpointTCPTunnelServer implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCPTunnelServer::TunnelendpointTCPTunnelServer(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const std::string& server_ip,
		const unsigned long server_port,
		const unsigned long id) :
		TunnelendpointTCPTunnelServerBase(checkpoint_handler, io, id),
		raw_connector_(RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, server_ip, server_port, this)) {
}

TunnelendpointTCPTunnelServer::~TunnelendpointTCPTunnelServer(void) {
    state_tcp_ = StateTCP_Unknown;
}

TunnelendpointTCPTunnelServer::APtr TunnelendpointTCPTunnelServer::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                                                          boost::asio::io_service& io,
                                                                          const std::string& server_ip,
                                                                          const unsigned long server_port,
                                                                          const unsigned long id) {

	TunnelendpointTCPTunnelServer::APtr new_tunnelendpoint(new TunnelendpointTCPTunnelServer(checkpoint_handle, io, server_ip, server_port, id));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

void TunnelendpointTCPTunnelServer::set_mutex(const Utility::Mutex::APtr& mutex) {
	TunnelendpointTCPTunnelServerBase::set_mutex(mutex);
    raw_connector_->set_mutex(mutex);
}

void TunnelendpointTCPTunnelServer::close_down_(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::close_down_");

    raw_connector_->close();

    if (tcp_tunnelendpoints_.empty() && tunnel_id_waiting_for_connections_.empty()) {
        goto_closed_state_();
    } else {
        /*
         * Notice that a call to close on a tcp_tunnelendpoint can cause the erase of the element
         */
        std::set<unsigned long> tcp_tunnelendpoints_to_close;

        for (const auto& pair : tcp_tunnelendpoints_) {
            tcp_tunnelendpoints_to_close.insert(pair.first);
        }

        std::set<unsigned long>::const_iterator j(tcp_tunnelendpoints_to_close.begin());
        while (j != tcp_tunnelendpoints_to_close.end()) {
            auto i = tcp_tunnelendpoints_.find(*j);
            if (i != tcp_tunnelendpoints_.end()) {
                (i->second)->close(false);
            }
            ++j;
        }
    }
}

void TunnelendpointTCPTunnelServer::com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::com_tunnelendpoint_connected");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
        break;
    case StateTCP_Closed:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for com_tunnelendpoint_connected");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::com_tunnelendpoint_connected", Attr_Communication(), CpAttr_debug());
    tunnelendpoint->log_socket_info(cps);

    /*
     * Get one tunnel_id waiting for connections
     */
    unsigned long tunnel_id(tunnel_id_waiting_for_connections_.front());
    tunnel_id_waiting_for_connections_.pop();

    if(state_tcp_ == StateTCP_Closing || state_tcp_ == StateTCP_Closing_no_connection) {
        close_down_(false);
        return;
    }

    /*
     * Create tcp-tunnelendpoint for the new connection
     */
    TunnelendpointTCP::APtr tunnelendpoint_tcp(TunnelendpointTCP::create(checkpoint_handler_, io_, tunnelendpoint, this));
    tunnelendpoint_tcp->set_skip_connection_handshake();
    tunnelendpoint_tcp->set_tunnel_id(tunnel_id);
    tcp_tunnelendpoints_.insert(make_pair(tunnel_id, tunnelendpoint_tcp));

    add_tunnelendpoint(tunnel_id, tunnelendpoint_tcp);
    tunnelendpoint_tcp->connect();

    cps.add_complete_attr(CpAttr("child_abs_id", get_id_abs_to_string(tunnel_id)));

    /*
     * Send connect_response message to client
     */
    TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect_response, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
    tunnelendpoint_tcp->connecting();
    tunnelendpoint_tcp->connected();
}

void TunnelendpointTCPTunnelServer::com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::com_tunnelendpoint_connection_failed_to_connect");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
        break;
    case StateTCP_Closed:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer::com_tunnelendpoint_connection_failed_to_connect");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::com_tunnelendpoint_connection_failed_to_connect", Attr_Communication(), CpAttr_debug(), CpAttr("message", message));

    unsigned long tunnel_id(tunnel_id_waiting_for_connections_.front());
    tunnel_id_waiting_for_connections_.pop();
    cps.add_complete_attr(CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    if(state_tcp_ == StateTCP_Closing || state_tcp_ == StateTCP_Closing_no_connection) {
        close_down_(false);
        return;
    }
    /*
     * Send connect_response message to client
     */
    TunnelendpointTCPMessage::APtr cmessage(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect_response, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_error));
    tunnelendpoint_send(MessagePayload::create(cmessage->create_as_buffer()));
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_connect(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_connect");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_recieve_connect");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_connect",  Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));


    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_connect.tunnel_id_already_exist",  Attr_Communication(), CpAttr_error(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));
        TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect_response, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_error));
        tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
        return;
    }

    tunnel_id_waiting_for_connections_.push(tunnel_id);
    raw_connector_->aio_connect_start();
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_close(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_close");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_recieve_close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_close",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        (i->second)->close(false);
    }
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_recieve_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_eof",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        (i->second)->eof();
    }
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_closed");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_recieve_closed");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_closed",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        (i->second)->close(false);
    }
}

void TunnelendpointTCPTunnelServer::goto_closed_state_(void) {
    raw_connector_->reset_eventhandler();
    TunnelendpointTCPTunnelServerBase::goto_closed_state_();
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_tcp_eh_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_eof",
    		Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(
    		TunnelendpointTCPMessage::MessageType_eof, tunnel_id,
    		TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServer not ready for tunnelendpoint_tcp_eh_closed");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_closed", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)), CpAttr("state_tcp_", state_tcp_));

    std::map<unsigned long, TunnelendpointTCP::APtr>::iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        tcp_tunnelendpoints_.erase(i);
        remove_tunnelendpoint(tunnel_id);
    }

    if (state_tcp_ == StateTCP_Ready) {
        TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_closed, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
        tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
    }

    if (state_tcp_ == StateTCP_Closing || state_tcp_ == StateTCP_Closing_no_connection) {
        if (tcp_tunnelendpoints_.empty()) {
            goto_closed_state_();
        }
    }
}


/*
 * ------------------------------------------------------------------
 * TunnelendpointTCPTunnelServerWithConnectionEngine implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCPTunnelServerWithConnectionEngine::TunnelendpointTCPTunnelServerWithConnectionEngine(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const unsigned long id) :
		TunnelendpointTCPTunnelServerBase(checkpoint_handler, io, id),
		tcp_tunnel_eventhandler_with_connection_engine_(NULL) {
}

TunnelendpointTCPTunnelServerWithConnectionEngine::~TunnelendpointTCPTunnelServerWithConnectionEngine(void) {
}

TunnelendpointTCPTunnelServerWithConnectionEngine::APtr TunnelendpointTCPTunnelServerWithConnectionEngine::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
		boost::asio::io_service& io,
		const unsigned long id) {

    TunnelendpointTCPTunnelServerWithConnectionEngine::APtr new_tunnelendpoint(new TunnelendpointTCPTunnelServerWithConnectionEngine(checkpoint_handle, io, id));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::set_tcp_eventhandler(TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* tcp_eventhandler) {
	TunnelendpointTCPTunnelServerBase::set_tcp_eventhandler(tcp_eventhandler);
	tcp_tunnel_eventhandler_with_connection_engine_ = tcp_eventhandler;
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::reset_tcp_eventhandler(void) {
	TunnelendpointTCPTunnelServerBase::reset_tcp_eventhandler();
	tcp_tunnel_eventhandler_with_connection_engine_ = NULL;
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::close_down_(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::close_down_");
    goto_closed_state_();
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_connect(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_connect");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_recieve_connect");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_connect",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));
    if(tcp_tunnel_eventhandler_with_connection_engine_ != NULL) {
    	tcp_tunnel_eventhandler_with_connection_engine_->tunnelendpoint_tcp_eh_recieve_remote_connect(tunnel_id);
    }
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_close(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_close");
    switch (state_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_recieve_close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_close",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    if(tcp_tunnel_eventhandler_with_connection_engine_ != NULL) {
    	tcp_tunnel_eventhandler_with_connection_engine_->tunnelendpoint_tcp_eh_recieve_remote_close(tunnel_id);
    }
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_recieve_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_eof",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    if(tcp_tunnel_eventhandler_with_connection_engine_ != NULL) {
    	tcp_tunnel_eventhandler_with_connection_engine_->tunnelendpoint_tcp_eh_recieve_remote_eof(tunnel_id);
    }
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServer::tunnelendpoint_recieve_closed");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_recieve_closed");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_recieve_closed",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    if(tcp_tunnel_eventhandler_with_connection_engine_ != NULL) {
    	tcp_tunnel_eventhandler_with_connection_engine_->tunnelendpoint_tcp_eh_recieve_remote_closed(tunnel_id);
    }
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_send_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_eof",
    		Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(
    		TunnelendpointTCPMessage::MessageType_eof, tunnel_id,
    		TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_send_remote_close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close",
    		Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(
    		TunnelendpointTCPMessage::MessageType_close, tunnel_id,
    		TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_closed");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelServerWithConnectionEngine not ready for tunnelendpoint_send_remote_closed");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_closed",
    		Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(
    		TunnelendpointTCPMessage::MessageType_closed, tunnel_id,
    		TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}


void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_ok(const unsigned long tunnel_id) {
    TunnelendpointTCPMessage::APtr message(
    		TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect_response,
    				tunnel_id,
    				TunnelendpointTCPMessage::ReturnCodeType_ok)
    );
    tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
}

void TunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_error(const unsigned long tunnel_id) {
    TunnelendpointTCPMessage::APtr message(
    		TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect_response,
    				tunnel_id,
    				TunnelendpointTCPMessage::ReturnCodeType_error)
    );
    tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
}


/*
 * ------------------------------------------------------------------
 * TunnelendpointTCPTunnelClient implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCPTunnelClient::TunnelendpointTCPTunnelClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                             boost::asio::io_service& io,
                                                             const std::string& listen_ip,
                                                             const unsigned long listen_port,
                                                             const unsigned long id) :
    TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, this),
    tcp_tunnel_eventhandler_(NULL),
    state_tcp_(StateTCP_Initializing),
    raw_acceptor_(RawTunnelendpointAcceptorTCP::create(checkpoint_handler, io, listen_ip, listen_port, this, this)),
    accept_interupted_(false), id_(id), checkpoint_tunnel_enabled_(false) {

	tunnel_id_free_.reserve(254);
	for(unsigned long i=1; i<255; ++i){
    	tunnel_id_free_.push_back(i);
    }
}

TunnelendpointTCPTunnelClient::~TunnelendpointTCPTunnelClient(void) {
    state_tcp_ = StateTCP_Unknown;
}

void TunnelendpointTCPTunnelClient::set_option_reuse_address(const bool& option_reuse_address) {
    raw_acceptor_->set_option_reuse_address(option_reuse_address);
}

void TunnelendpointTCPTunnelClient::set_tcp_eventhandler(TunnelendpointTCPTunnelClientEventhandler* tcp_tunnel_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::set_tcp_eventhandler");
    tcp_tunnel_eventhandler_ = tcp_tunnel_eventhandler;
}
void TunnelendpointTCPTunnelClient::reset_tcp_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::reset_tcp_eventhandler");
    tcp_tunnel_eventhandler_ = NULL;
}

void TunnelendpointTCPTunnelClient::set_mutex(const Utility::Mutex::APtr& mutex) {
    raw_acceptor_->set_mutex(mutex);
    TunnelendpointReliableCryptedTunnel::set_mutex(mutex);

    if(checkpoint_tunnel_enabled_) {
    	checkpoint_tunnel_->set_mutex(mutex);
    }
}

TunnelendpointTCPTunnelClient::APtr TunnelendpointTCPTunnelClient::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
		boost::asio::io_service& io,
		const std::string& server_ip,
		const unsigned long server_port,
		const unsigned long id) {
	TunnelendpointTCPTunnelClient::APtr new_tunnelendpoint(new TunnelendpointTCPTunnelClient(checkpoint_handle, io, server_ip, server_port, id));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

void TunnelendpointTCPTunnelClient::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::connect");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::connect", Attr_Communication(), CpAttr_debug());
    Tunnelendpoint::connect();
}

void TunnelendpointTCPTunnelClient::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::close");
    switch (state_tcp_) {
    case StateTCP_Closed:
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
        return;
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::close", Attr_Communication(),
                        CpAttr_debug(), CpAttr("num_of_tunnelendpoints", tcp_tunnelendpoints_.size()),
                        CpAttr("abs_id", get_id_abs_to_string()));

    state_tcp_ = StateTCP_Closing;
    close_state_all_connection_closed = false;
    close_state_accept_closed = false;
    close_down_(force);
}

void TunnelendpointTCPTunnelClient::close_down_(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::close_down_");
    if(!raw_acceptor_->is_closed()) {
    	raw_acceptor_->aio_close_start();
    }
    if (tcp_tunnelendpoints_.empty()) {
    	close_state_all_connection_closed = true;
        goto_closed_state_();
    } else {
        /*
         * Notice that a call to close on a tcp_tunnelendpoint can cause the erase of the element
         */
        std::set<unsigned long> tcp_tunnelendpoints_to_close;

        for (const auto& pair : tcp_tunnelendpoints_) {
            tcp_tunnelendpoints_to_close.insert(pair.first);
        }
        std::set<unsigned long>::const_iterator j(tcp_tunnelendpoints_to_close.begin());
        while (j != tcp_tunnelendpoints_to_close.end()) {
            auto i = tcp_tunnelendpoints_.find(*j);
            if (i != tcp_tunnelendpoints_.end()) {
            	(i->second)->close(false);
            }
            ++j;
        }
    }
}
bool TunnelendpointTCPTunnelClient::is_closed() {
    return state_tcp_ == StateTCP_Closed;
}

std::pair<std::string, unsigned long> TunnelendpointTCPTunnelClient::get_ip_local(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::get_ip_local");
    return raw_acceptor_->get_ip_local();
}

unsigned long TunnelendpointTCPTunnelClient::get_new_tunnel_id() {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::get_new_tunnel_id");
    unsigned long new_tunnel_id(tunnel_id_free_.front());
    tunnel_id_free_.erase(tunnel_id_free_.begin());
    return new_tunnel_id;
}

void TunnelendpointTCPTunnelClient::free_tunnel_id(const unsigned long& tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::free_tunnel_id");
    Checkpoint(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::free_tunnel_id", Attr_Communication(), CpAttr_debug(), CpAttr("tunnel_id", tunnel_id));
    tunnel_id_free_.push_back(tunnel_id);
}

bool TunnelendpointTCPTunnelClient::room_for_new_tunnel_id() {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::room_for_new_tunnel_id");
    return tunnel_id_free_.size() > 0;
}


void TunnelendpointTCPTunnelClient::com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::com_tunnelendpoint_accepted");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for com_tunnelendpoint_accepted");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::com_tunnelendpoint_accepted", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));
    tunnelendpoint->log_socket_info(cps);

    /*
     * Create and add tcp-tunnelendpoint for received raw-tcp-tunnelendpoint
     */
    TunnelendpointTCP::APtr tunnelendpoint_tcp(TunnelendpointTCP::create(checkpoint_handler_, io_, tunnelendpoint, this));
    tunnelendpoint_tcp->set_skip_connection_handshake();

    bool accept_connection = true;
    if (tcp_tunnel_eventhandler_ != NULL) {
        accept_connection = tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_connected(id_, tunnelendpoint_tcp);
    }
    if (accept_connection) {
        unsigned long new_tunnel_id(get_new_tunnel_id());
        tunnelendpoint_tcp->set_tunnel_id(new_tunnel_id);

        tcp_tunnelendpoints_.insert(make_pair(new_tunnel_id, tunnelendpoint_tcp));
        add_tunnelendpoint(new_tunnel_id, tunnelendpoint_tcp);
        tunnelendpoint_tcp->connect();
        cps.add_complete_attr(CpAttr("child_abs_id", get_id_abs_to_string(new_tunnel_id)));

        /*
         * Send connect message to server
         */
        TunnelendpointTCPMessage::APtr
                message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect, new_tunnel_id,
                                                         TunnelendpointTCPMessage::ReturnCodeType_ok));
        tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
        tunnelendpoint_tcp->connecting();
    }
}

void TunnelendpointTCPTunnelClient::com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::com_tunnelendpoint_acceptor_error");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::com_tunnelendpoint_acceptor_error",
                        Attr_Communication(), CpAttr_error(), CpAttr("message", error_message),
                        CpAttr("abs_id", get_id_abs_to_string()));
    if (tcp_tunnel_eventhandler_ != NULL) {
        tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_acceptor_error(id_, error_message);
    }
    close(true);
}

void TunnelendpointTCPTunnelClient::com_tunnelendpoint_acceptor_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::com_tunnelendpoint_acceptor_closed");
    switch (state_tcp_) {
    case StateTCP_Closing:
        break;
    default:
        state_tcp_ = StateTCP_Closing;
        close_state_all_connection_closed = true;
        close_state_accept_closed = false;
        close_down_(false);
        return;
    }
	close_state_accept_closed = true;
    goto_closed_state_();
}


void TunnelendpointTCPTunnelClient::accept_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::accept_start");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for accept_start");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::accept_start", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));
    raw_acceptor_->aio_accept_start();
    if (raw_acceptor_->is_closed()) {
        close(true);
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_eh_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_eh_connected");
    switch (state_tcp_) {
    case StateTCP_Initializing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_eh_connected");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_eh_connected", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));

    if(checkpoint_tunnel_enabled_) {
    	state_tcp_ = StateTCP_InitCheckpointTunnel;
    	checkpoint_tunnel_id_ = get_new_tunnel_id();

    	add_tunnelendpoint(checkpoint_tunnel_id_, checkpoint_tunnel_);
    	checkpoint_tunnel_->connect();

        TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_checkpoint, checkpoint_tunnel_id_, TunnelendpointTCPMessage::ReturnCodeType_ok));
        tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
    	return;
    }
    goto_ready_state_();
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_eh_recieve");

    switch (state_tcp_) {
    case StateTCP_InitCheckpointTunnel:
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closed:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_eh_recieve");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_eh_recieve", Attr_Communication(), CpAttr_debug());

    /*
     * Parse message from server
     */
    TunnelendpointTCPMessage::APtr tcp_message(TunnelendpointTCPMessage::create_from_buffer(message->get_buffer()));
    cps.add_complete_attr(CpAttr("abs_id", get_id_abs_to_string(tcp_message->get_tunnel_id())));

    switch (tcp_message->get_message_type()) {
    case TunnelendpointTCPMessage::MessageType_connect_response:
        tunnelendpoint_recieve_connect_response(tcp_message->get_tunnel_id(), tcp_message->get_rc());
        break;
    case TunnelendpointTCPMessage::MessageType_eof:
        tunnelendpoint_recieve_eof(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_close:
        tunnelendpoint_recieve_close(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_closed:
        tunnelendpoint_recieve_closed(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_message:
        tunnelendpoint_recieve_message(tcp_message->get_payload());
        break;
    case TunnelendpointTCPMessage::MessageType_checkpoint_response:
        tunnelendpoint_recieve_checkpoint_response(tcp_message->get_tunnel_id(), tcp_message->get_rc());
        break;
    default:
        break;
    }
    return;
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_connect_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_connect_response");
    switch (state_tcp_) {
    case StateTCP_InitCheckpointTunnel:
    case StateTCP_Ready:
        break;
    case StateTCP_Closed:
    case StateTCP_Closing:
        return;
    default:
        Checkpoint cp1(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_connect_response.invalid_state", Attr_Communication(), CpAttr_debug(), CpAttr("state_tcp_", state_tcp_));
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_connect_response");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_connect_response", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (rc == TunnelendpointTCPMessage::ReturnCodeType_ok) {
        if (i != tcp_tunnelendpoints_.end()) {
            (i->second)->connected();
        }
    } else {
        if (i != tcp_tunnelendpoints_.end()) {
            (i->second)->close(true);
        }
        if (tcp_tunnel_eventhandler_ != NULL) {
            tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_acceptor_error(id_, "Server unable to connect");
        }
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_eof");
    switch (state_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_eof",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        (i->second)->eof();
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_close(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_close");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_close",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::const_iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        (i->second)->close(false);
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_closed");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_closed");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_closed",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointTCP::APtr>::iterator i(tcp_tunnelendpoints_.find(tunnel_id));
    if (i != tcp_tunnelendpoints_.end()) {
        tunnel_id_used_waiting_to_close_on_client_.insert(tunnel_id);
        (i->second)->close(false);
    } else {
        /*
         * Tunnel_id can now be reused because we know that it is no longer used at the server (because of this message)
         */
      free_tunnel_id(tunnel_id);
        if (state_tcp_ == StateTCP_Ready) {
            if (accept_interupted_ && room_for_new_tunnel_id()) {
                Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_closed.accept_interupted_restarted", Attr_Communication(), CpAttr_debug());
                accept_interupted_ = false;
                accept_start();
            }
        }
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_message");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_message");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_message",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));
    if (tcp_tunnel_eventhandler_ != NULL) {
        tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_recieve(id_, message);
    }
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_checkpoint_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_checkpoint_response");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    case StateTCP_Closed:
    case StateTCP_Closing:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_recieve_checkpoint_response");
    }
    Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_checkpoint_response", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)), CpAttr("rc", rc));
    goto_ready_state_();
}

bool TunnelendpointTCPTunnelClient::com_accept_continue(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::com_accept_continue");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for com_accept_continue");
    }

    if (state_tcp_ == StateTCP_Ready) {
        if (!room_for_new_tunnel_id()) {
            Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::com_accept_continue.accept_interupted", Attr_Communication(), CpAttr_debug());
            accept_interupted_ = true;
            return false;
        }
        if (tcp_tunnel_eventhandler_ != NULL) {
            return tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_accept_start_continue(id_);
        }
        return true;
    }
    return false;
}

void TunnelendpointTCPTunnelClient::goto_ready_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::goto_ready_state_");
    state_tcp_ = StateTCP_Ready;
    if (tcp_tunnel_eventhandler_ != NULL) {
        tcp_tunnel_eventhandler_->tunnelendpoint_tcp_eh_ready(id_);
    } else {
        accept_start();
    }
}

void TunnelendpointTCPTunnelClient::goto_closed_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::goto_closed_state_");
    state_tcp_ = StateTCP_Closed;
    if (tcp_tunnel_eventhandler_ != NULL) {
        TunnelendpointTCPTunnelClientEventhandler* tcp_tunnel_eventhandler_temp(tcp_tunnel_eventhandler_);
        reset_tcp_eventhandler();
        tcp_tunnel_eventhandler_temp->tunnelendpoint_tcp_eh_closed(id_);
    }
    raw_acceptor_->reset_eventhandler();
    TunnelendpointReliableCrypted::close(true);
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send");
    switch (state_tcp_) {
    case StateTCP_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_tcp_send");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_message, 0,TunnelendpointTCPMessage::ReturnCodeType_ok));
    message_tcp->set_payload(message);
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_eof");
    switch (state_tcp_) {
    case StateTCP_Ready:
    case StateTCP_Closing:
        break;
    case StateTCP_Closing_no_connection:
    case StateTCP_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_tcp_eh_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_eof",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_eof, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_closed(const unsigned long tunnel_id) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_closed");
	switch (state_tcp_) {
	case StateTCP_Ready:
	case StateTCP_Closing:
	case StateTCP_Closing_no_connection:
		break;
	case StateTCP_Closed:
		return;
	default:
		throw ExceptionUnexpected("TunnelendpointTCPTunnelClient not ready for tunnelendpoint_tcp_eh_closed");
	}
	CheckpointScope cps(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_closed",
						Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

	std::map<unsigned long, TunnelendpointTCP::APtr>::iterator i(tcp_tunnelendpoints_.find(tunnel_id));
	if (i != tcp_tunnelendpoints_.end()) {
		tcp_tunnelendpoints_.erase(i);
		remove_tunnelendpoint(tunnel_id);

		std::set<unsigned long>::const_iterator j(tunnel_id_used_waiting_to_close_on_client_.find(tunnel_id));
		if (j != tunnel_id_used_waiting_to_close_on_client_.end()) {
		  free_tunnel_id(*j);
		  tunnel_id_used_waiting_to_close_on_client_.erase(*j);
		}
	}

	if (state_tcp_ != StateTCP_Closing_no_connection) {
		TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_closed, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
		tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
	}

	if (state_tcp_ == StateTCP_Closing || state_tcp_ == StateTCP_Closing_no_connection) {
		if (tcp_tunnelendpoints_.empty()) {
			close_state_all_connection_closed = true;
			goto_closed_state_();
		}
	}
}

bool TunnelendpointTCPTunnelClient::async_mem_guard_is_done(void) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::async_mem_guard_is_done");
    switch (state_tcp_) {
    case StateTCP_Closed:
    case StateTCP_Unknown:
        break;
    default:
        return false;
    }
    if(!close_state_all_connection_closed) {
    	return false;
    }
    if(!close_state_accept_closed) {
    	return false;
    }
    return TunnelendpointReliableCrypted::async_mem_guard_is_done();
}

std::string TunnelendpointTCPTunnelClient::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

Utility::CheckpointHandler::APtr TunnelendpointTCPTunnelClient::enable_checkpoint_tunnel(const Utility::CheckpointFilter::APtr& checkpoint_filter, const Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::enable_checkpoint_tunnel");
    switch (state_tcp_) {
    case StateTCP_Initializing:
        break;
    default:
    	Checkpoint cp(*checkpoint_handler_, "TunnelendpointTCPTunnelClient::enable_checkpoint_tunnel.invalid_state", Attr_Communication(), CpAttr_error());
        return Utility::CheckpointHandler::create_ignore();
    }

    if(checkpoint_tunnel_enabled_) {
    	return checkpoint_tunnel_->get_checkpoint_handler();
    }

	checkpoint_tunnel_enabled_ = true;
	checkpoint_tunnel_ = TunnelendpointCheckpointHandlerClient::create(checkpoint_handler_, io_, checkpoint_handler_, checkpoint_filter, checkpoint_output_handler);
	checkpoint_tunnel_->set_mutex(get_mutex());
	return checkpoint_tunnel_->get_checkpoint_handler();
}
