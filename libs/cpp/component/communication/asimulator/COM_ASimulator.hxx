/*! \file COM_ASimulator.hxx
 \brief This file contains the aplication simulator classes
 */
#ifndef COM_ASimulator_HXX
#define COM_ASimulator_HXX

#include <map>
#include <queue>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_AsyncContinuePolicy.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This exception is thrown in case of an unexpected fatal error
 */
class ASimulationException: public Exception {
public:
    ASimulationException(const std::string& message) :
        Exception(message) {
    }
};

/*! \brief Baseclass for a application simulation connection
 */
class ASimulatorConnection: public RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<ASimulatorConnection> APtr;

    enum SimulationType {
        SimulationType_Unknown = 0,
        SimulationType_ping,
        SimulationType_ping_server_close,
        SimulationType_upload_until_eof,
        SimulationType_download_until_eof
    };

    /*! \brief Destructor
     */
    virtual ~ASimulatorConnection(void);

    /*! \brief Signals that the raw tunnelendpoint has been closed
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id);

    /*! \brief Data written by the simulation
     */
    void simulate_data_write(const Utility::DataBufferManaged::APtr& data);

    /*! \brief Start simulation
     */
    virtual void simulate_start(void) = 0;

    /*! \brief Notification that the simulation has ended
     */
    virtual void simulate_end(void) = 0;

    /*! \brief Indicate that the simulation is done
     */
    bool is_done(void);

    /*! \brief Signals that data has been received
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data) = 0;

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) = 0;

    /*! \brief Signals that the write data buffer is empty
     *
     *  \param connection_id  Connection identifier
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) = 0;

protected:

    /*! \brief Constructor
     */
    ASimulatorConnection(const RawTunnelendpoint::APtr& raw_tunnelendpoint);

    /*! \brief Generates a message of a given size
     */
    Utility::DataBufferManaged::APtr generate_message(const unsigned long message_size) const;

    enum StateType {
        StateType_Initializing, StateType_Running_Simulation, StateType_Done
    };

    std::atomic<StateType> state_;
    std::atomic<SimulationType> simulation_;

    std::atomic<unsigned long> read_count_;
    std::atomic<unsigned long> write_count_;

    std::atomic<unsigned long> repeat_count_;
    std::atomic<unsigned long> repeat_count_done_;

    std::atomic<unsigned long> message_size_;
    Giritech::Utility::DataBufferManaged::APtr read_data_buffer_;
    Giritech::Utility::DataBufferManaged::APtr read_data_buffer_total_;

    RawTunnelendpoint::APtr raw_tunnelendpoint_;
};

/*! \brief Server side of application simulation connection
 */
class ASimulatorConnectionServer: public ASimulatorConnection {
public:
    typedef boost::shared_ptr<ASimulatorConnectionServer> APtr;

    /*! \brief Destructor
     */
    virtual ~ASimulatorConnectionServer(void);

    /*! \brief Start simulation
     */
    virtual void simulate_start(void);

    /*! \brief Notification that the simulation has ended
     */
    virtual void simulate_end(void);

    /*! \brief Signals that data has been received
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);

    /*! \brief Signals that the write data buffer is empty
     *
     *  \param connection_id  Connection identifier
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);

    /*! \brief Data reviced for the simulation
     */
    void simulate_data_read(const Utility::DataBuffer::APtr& data);

    /*! \brief Create instance
     */
    static APtr create(const RawTunnelendpoint::APtr& raw_tunnelendpoint);

protected:
    /*! \brief Constructor
     */
    ASimulatorConnectionServer(const RawTunnelendpoint::APtr& raw_tunnelendpoint);
};

/*! \brief Client side of application simulation connection
 */
class ASimulatorConnectionClient: public ASimulatorConnection {
public:
    typedef boost::shared_ptr<ASimulatorConnectionClient> APtr;

    /*! \brief Destructor
     */
    virtual ~ASimulatorConnectionClient(void);

    /*! \brief Start simulation
     */
    virtual void simulate_start(void);

    /*! \brief Notification that the simulation has ended
     */
    virtual void simulate_end(void);

    /*! \brief Signals that data has been received
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);

    /*! \brief Signals that the write data buffer is empty
     *
     *  \param connection_id  Connection identifier
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);

    /*! \brief Data reviced for the simulation
     */
    void simulate_data_read(const Utility::DataBuffer::APtr& data);

    /*! \brief Create instance
     */
    static APtr create(const RawTunnelendpoint::APtr& raw_tunnelendpoint,
                       const SimulationType simulation,
                       const unsigned long repeat_count,
                       const unsigned long message_size);

protected:
    /*! \brief Constructor
     */
    ASimulatorConnectionClient(const RawTunnelendpoint::APtr& raw_tunnelendpoint,
                               const SimulationType simulation,
                               const unsigned long repeat_count,
                               const unsigned long message_size);
};

/*! \brief Server side application simulation
 */
class ASimulationServer: public RawTunnelendpointAcceptorTCPEventhandler, public AsyncContinuePolicy {
public:
    typedef boost::shared_ptr<ASimulationServer> APtr;

    /*! \brief Destructor
     */
    virtual ~ASimulationServer(void);

    /*! \brief Start server
     */
    virtual void start(void);

    /*! \brief Stop server
     */
    virtual void stop(void);

    /*! \brief Connected signal
     *
     *  Implementation of RawTunnelendpointAcceptorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void com_tunnelendpoint_acceptor_error(const std::string& error_message);

    /*! \brief closed
     *
     *  The acceptor send this signal when everything is closed (as a response to aio_accept_stop).
     */
    virtual void com_tunnelendpoint_acceptor_closed(void);

     /*! \brief Connected acceptting new connections
     *
     * Implementation of AsyncContinuePolicy
     */
    virtual bool com_accept_continue(void);

    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const std::string& server_ip,
                       const unsigned long server_port,
                       const unsigned long connection_count_max);

protected:

    /*! \brief Constructor
     */
    ASimulationServer(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                      boost::asio::io_service& io,
                      const std::string& server_ip,
                      const unsigned long server_port,
                      const unsigned long connection_count_max);

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;

    RawTunnelendpointAcceptorTCP::APtr raw_acceptor_;

    std::atomic<unsigned long> connection_count_;
    std::atomic<unsigned long> connection_count_max_;

    std::map<unsigned long, ASimulatorConnectionServer::APtr> connections_;
    std::atomic<bool> connections_lock_;
};

/*! \brief Client side of application simulation
 */
class ASimulationClient: public RawTunnelendpointConnectorTCPEventhandler {

public:
    typedef boost::shared_ptr<ASimulationClient> APtr;

    /*! \brief Destructor
     */
    virtual ~ASimulationClient(void);

    /*! \brief Start simulation
     */
    virtual void start(void);

    /*! \brief Connected signal
     *
     *  Implementation of RawTunnelendpointConnectorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Failed to connect signal
     *
     *  Implementation of RawTunnelendpointConnectorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_connection_failed_to_connect(const std::string& message);

    /*! \brief Indicate that the simulation is done
     *
     */
    bool is_done(void);

    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
            boost::asio::io_service& io,
            const std::string& server_ip,
            const unsigned long server_port,
            const ASimulatorConnection::SimulationType simulation,
            const unsigned long simulation_repeat_count,
            const unsigned long message_size);

protected:

    /*! \brief Constructor
     */
    ASimulationClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
            boost::asio::io_service& io,
            const std::string& server_ip,
            const unsigned long server_port,
            const ASimulatorConnection::SimulationType simulation,
            const unsigned long simulation_repeat_count,
            const unsigned long message_size);

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;

    RawTunnelendpointConnectorTCP::APtr raw_connector_;
    std::atomic<ASimulatorConnection::SimulationType> simulation_;
    std::atomic<unsigned long> simulation_repeat_count;
    std::atomic<unsigned long> message_size_;

    ASimulatorConnectionClient::APtr connection_;
};

}

}
#endif
