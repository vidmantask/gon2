/*! \file COM_TunnelendpointTCP.cxx
 \brief This file contains the implementation for the tunnelendpoint-tcp class
 */
#include <functional>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/asimulator/COM_ASimulator.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * ASimulatorConnection implementation
 * ------------------------------------------------------------------
 */
ASimulatorConnection::ASimulatorConnection(const RawTunnelendpoint::APtr& raw_tunnelendpoint) :
    state_(StateType_Initializing), simulation_(SimulationType_Unknown), raw_tunnelendpoint_(raw_tunnelendpoint),
            repeat_count_(0), repeat_count_done_(0), read_count_(0), write_count_(0) {
    raw_tunnelendpoint->set_eventhandler(this);
    read_data_buffer_ =  Giritech::Utility::DataBufferManaged::create(0);
    read_data_buffer_total_ =  Giritech::Utility::DataBufferManaged::create(0);
}

ASimulatorConnection::~ASimulatorConnection(void) {
}

void ASimulatorConnection::com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
    simulate_end();
}

void ASimulatorConnection::simulate_data_write(const Utility::DataBufferManaged::APtr& data) {
    write_count_++;
    raw_tunnelendpoint_->aio_write_start(data);
}

DataBufferManaged::APtr ASimulatorConnection::generate_message(const unsigned long message_size) const {
    stringstream ss;
    unsigned long i = 0;
    while (i < message_size) {
        byte_t value = (i % 256);
        ss << value;
        i += 1;
    }
    return DataBufferManaged::create(ss.str());
}

bool ASimulatorConnection::is_done(void) {
    return state_ == StateType_Done;
}

/*
 * ------------------------------------------------------------------
 * ASimulatorConnectionServer implementation
 * ------------------------------------------------------------------
 */
ASimulatorConnectionServer::ASimulatorConnectionServer(const RawTunnelendpoint::APtr& raw_tunnelendpoint) :
    ASimulatorConnection(raw_tunnelendpoint) {
    raw_tunnelendpoint_->aio_read_start();
}

ASimulatorConnectionServer::~ASimulatorConnectionServer(void) {
}

ASimulatorConnectionServer::APtr ASimulatorConnectionServer::create(const RawTunnelendpoint::APtr& raw_tunnelendpoint) {
    return ASimulatorConnectionServer::APtr(new ASimulatorConnectionServer(raw_tunnelendpoint));
}

void ASimulatorConnectionServer::simulate_start(void) {
    cout << "ASimulatorConnectionServer::simulate_start, " << simulation_ << endl;
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
    case SimulationType_upload_until_eof:
        raw_tunnelendpoint_->aio_read_start();
        break;
    case SimulationType_download_until_eof:
        simulate_data_write(generate_message(message_size_ * repeat_count_));
        raw_tunnelendpoint_->aio_close_write_start();
        raw_tunnelendpoint_->aio_read_start();
        break;
    default:
        break;
    }
}

void ASimulatorConnectionServer::com_raw_tunnelendpoint_read(const unsigned long& connection_id,
                                                             const Utility::DataBuffer::APtr& data) {
    switch (state_) {
    case StateType_Initializing: {
        boost::uint8_t simulation_raw = 0;
        convert_from_networkorder_8(simulation_raw, (*data)[0]);
        simulation_ = static_cast<SimulationType> (simulation_raw);
        state_ = StateType_Running_Simulation;

        boost::uint16_t repeat_count_raw = 0;
        convert_from_networkorder_16(repeat_count_raw, (*data)[1], (*data)[2]);
        repeat_count_ = repeat_count_raw;

        boost::uint16_t message_size_raw = 0;
        convert_from_networkorder_16(message_size_raw, (*data)[3], (*data)[4]);
        message_size_ = message_size_raw;

        raw_tunnelendpoint_->aio_write_start(DataBufferManaged::create("ok"));
        simulate_start();
        break;
    }
    case StateType_Running_Simulation: {
        simulate_data_read(data);
    }
    }
}

void ASimulatorConnectionServer::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
    case SimulationType_download_until_eof:
        raw_tunnelendpoint_->aio_close_start(false);
        break;
    case SimulationType_upload_until_eof:
        simulate_data_write(read_data_buffer_total_);
        raw_tunnelendpoint_->aio_close_start(false);
        break;
    default:
        break;
    }
}

void ASimulatorConnectionServer::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id)  {
    // Ignore signal
}


void ASimulatorConnectionServer::simulate_data_read(const Utility::DataBuffer::APtr& data) {
    Utility::DataBufferManaged::APtr new_buffer;
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
        read_data_buffer_->append(data);
        if(read_data_buffer_->getSize() < message_size_) {
            raw_tunnelendpoint_->aio_read_start();
            return;
        }
        new_buffer = DataBufferManaged::create(read_data_buffer_);
        new_buffer->resize(message_size_);
        read_data_buffer_->resize(read_data_buffer_->getSize() - message_size_);
        read_count_++;
    default:
        break;
    }

    switch (simulation_) {
    case SimulationType_ping:
        simulate_data_write( DataBufferManaged::create(new_buffer));
        repeat_count_done_++;
        raw_tunnelendpoint_->aio_read_start();
        break;
    case SimulationType_ping_server_close:
        simulate_data_write( DataBufferManaged::create(new_buffer));
        repeat_count_done_++;
        if (repeat_count_done_ < repeat_count_) {
            raw_tunnelendpoint_->aio_read_start();
        } else {
            raw_tunnelendpoint_->aio_close_start(false);
        }
        break;
    case SimulationType_upload_until_eof:
        read_data_buffer_total_->append(data);
        raw_tunnelendpoint_->aio_read_start();
        break;
    case SimulationType_download_until_eof:
        read_data_buffer_total_->append(data);
        raw_tunnelendpoint_->aio_read_start();
        break;
    default:
        break;
    }
}

void ASimulatorConnectionServer::simulate_end(void) {
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
        if (read_count_ != write_count_) {
            cout << "ASimulatorConnectionServer::read_count_: " << read_count_ << endl;
            cout << "ASimulatorConnectionServer::write_count_: " << write_count_ << endl;
            throw ASimulationException("ASimulatorConnectionServer, unmatch read/write count");
        }
        break;
    case SimulationType_upload_until_eof:
    case SimulationType_download_until_eof:
        if (read_data_buffer_total_->getSize() != (repeat_count_ * message_size_)) {
            cout << "ASimulatorConnectionServer::read_data_buffer_total_.size: " << read_data_buffer_total_->getSize() << endl;
            cout << "ASimulatorConnectionServer::repeat_count_: " << repeat_count_ << endl;
            cout << "ASimulatorConnectionServer::message_size_: " << message_size_ << endl;
            cout << "ASimulatorConnectionServer::expected_size_: " << (repeat_count_ * message_size_) << endl;
            throw ASimulationException("ASimulatorConnectionServer, did not recive all data");
        }
        break;
    default:
        break;
    }
    state_ = StateType_Done;
}

/*
 * ------------------------------------------------------------------
 * ASimulatorConnectionClient implementation
 * ------------------------------------------------------------------
 */
ASimulatorConnectionClient::ASimulatorConnectionClient(const RawTunnelendpoint::APtr& raw_tunnelendpoint,
                                                       const SimulationType simulation,
                                                       const unsigned long repeat_count,
                                                       const unsigned long message_size) :
    ASimulatorConnection(raw_tunnelendpoint) {
    state_ = StateType_Initializing;
    simulation_ = simulation;
    repeat_count_ = repeat_count;
    message_size_ = message_size;
    simulate_start();
}

ASimulatorConnectionClient::~ASimulatorConnectionClient(void) {

}

ASimulatorConnectionClient::APtr ASimulatorConnectionClient::create(const RawTunnelendpoint::APtr& raw_tunnelendpoint,
                                                                    const SimulationType simulation,
                                                                    const unsigned long repeat_count,
                                                                    const unsigned long message_size) {
    return ASimulatorConnectionClient::APtr(
                                            new ASimulatorConnectionClient(raw_tunnelendpoint, simulation, repeat_count, message_size));
}

void ASimulatorConnectionClient::simulate_start(void) {
    Utility::DataBufferManaged::APtr buffer(Utility::DataBufferManaged::create(5));
    set_uint_8(buffer, 0, simulation_);
    set_uint_16(buffer, 1, repeat_count_);
    set_uint_16(buffer, 3, message_size_);
    raw_tunnelendpoint_->aio_write_start(buffer);
    raw_tunnelendpoint_->aio_read_start();
}

void ASimulatorConnectionClient::com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data) {
    switch (state_) {
    case StateType_Initializing: {
        state_ = StateType_Running_Simulation;
        repeat_count_done_++;
        switch (simulation_) {
        case SimulationType_ping:
        case SimulationType_ping_server_close:
            simulate_data_write(generate_message(message_size_));
            raw_tunnelendpoint_->aio_read_start();
            break;
        case SimulationType_upload_until_eof: {
            simulate_data_write(generate_message(message_size_ * repeat_count_));
            raw_tunnelendpoint_->aio_close_write_start();
            raw_tunnelendpoint_->aio_read_start();
            break;
        case SimulationType_download_until_eof:
            if(data->getSize() > 2) {
                Utility::DataBufferManaged::APtr data_minus_ok(DataBufferManaged::create(data->data()+2, data->getSize()-2));
                data_minus_ok->resize(data->getSize()-2);
                simulate_data_read(data_minus_ok);
            }
            else {
                raw_tunnelendpoint_->aio_read_start();
            }
            break;
        }
        default:
            break;
        }
        break;
    }
    case StateType_Running_Simulation: {
        simulate_data_read(data);
    }
    }
}

void ASimulatorConnectionClient::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
    case SimulationType_upload_until_eof:
        raw_tunnelendpoint_->aio_close_start(false);
        break;
    case SimulationType_download_until_eof:
        simulate_data_write(read_data_buffer_total_);
        raw_tunnelendpoint_->aio_close_start(false);
        break;
    default:
        break;
    }
}

void ASimulatorConnectionClient::simulate_data_read(const Utility::DataBuffer::APtr& data) {
    Utility::DataBufferManaged::APtr new_buffer;
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
        read_data_buffer_->append(data);
        if(read_data_buffer_->getSize() < message_size_) {
            raw_tunnelendpoint_->aio_read_start();
            return;
        }
        new_buffer = DataBufferManaged::create(read_data_buffer_);
        new_buffer->resize(message_size_);
        read_data_buffer_->resize(read_data_buffer_->getSize() - message_size_);
        read_count_++;
    default:
        break;
    }

    switch (simulation_) {
    case SimulationType_ping:
        if (repeat_count_done_ < repeat_count_) {
            repeat_count_done_++;
            simulate_data_write(generate_message(message_size_));
            raw_tunnelendpoint_->aio_read_start();
        } else {
            raw_tunnelendpoint_->aio_close_start(false);
        }
        break;
    case SimulationType_ping_server_close:
        if (repeat_count_done_ < repeat_count_) {
            repeat_count_done_++;
            simulate_data_write(generate_message(message_size_));
        }
        raw_tunnelendpoint_->aio_read_start();
        break;
    case SimulationType_upload_until_eof:
    case SimulationType_download_until_eof:
        read_data_buffer_total_->append(data);
        repeat_count_done_++;
        raw_tunnelendpoint_->aio_read_start();
        break;
    default:
        break;
    }
}

void ASimulatorConnectionClient::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id)  {
    // Ignore signal
}

void ASimulatorConnectionClient::simulate_end(void) {
    switch (simulation_) {
    case SimulationType_ping:
    case SimulationType_ping_server_close:
        if (read_count_ != write_count_) {
            cout << "ASimulatorConnectionClient::read_count_: " << read_count_ << endl;
            cout << "ASimulatorConnectionClient::write_count_: " << write_count_ << endl;
            throw ASimulationException("ASimulatorConnectionClient, unmatch read/write count");
        }
        break;
    case SimulationType_upload_until_eof:
    case SimulationType_download_until_eof:
        if (read_data_buffer_total_->getSize() != (repeat_count_ * message_size_)) {
            cout << "ASimulatorConnectionClient::read_data_buffer_total_.size: " << read_data_buffer_total_->getSize() << endl;
            cout << "ASimulatorConnectionClient::repeat_count_: " << repeat_count_ << endl;
            cout << "ASimulatorConnectionClient::message_size_: " << message_size_ << endl;
            cout << "ASimulatorConnectionClient::expected_size_: " << (repeat_count_ * message_size_) << endl;
            throw ASimulationException("ASimulatorConnectionClient, did not recive all data");
        }
    default:
        break;
    }
    state_ = StateType_Done;
}

/*
 * ------------------------------------------------------------------
 * ASimulationServer implementation
 * ------------------------------------------------------------------
 */
ASimulationServer::ASimulationServer(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     boost::asio::io_service& io,
                                     const std::string& server_ip,
                                     const unsigned long server_port,
                                     const unsigned long connection_count_max) :
    checkpoint_handler_(checkpoint_handler), raw_acceptor_(RawTunnelendpointAcceptorTCP::create(checkpoint_handler, io,
                                                                                                server_ip, server_port,
                                                                                                this, this)),
            connection_count_(0), connection_count_max_(connection_count_max) {
	raw_acceptor_->set_option_reuse_address(true);
}


ASimulationServer::~ASimulationServer(void) {
}

void ASimulationServer::start(void) {
    raw_acceptor_->aio_accept_start();
}

void ASimulationServer::stop(void) {
    raw_acceptor_->aio_close_start();
}

void ASimulationServer::com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
    connection_count_++;
    ASimulatorConnectionServer::APtr connection(ASimulatorConnectionServer::create(tunnelendpoint));

    bool expected = false;
    while (!connections_lock_.compare_exchange_weak(expected, true));

    connections_.insert(make_pair(tunnelendpoint->get_connection_id(), connection));

    connections_lock_ = false;
}

void ASimulationServer::com_tunnelendpoint_acceptor_error(const std::string& error_message) {
}

void ASimulationServer::com_tunnelendpoint_acceptor_closed(void) {
}

bool ASimulationServer::com_accept_continue(void) {
    if (connection_count_max_ != 0) {
        return connection_count_ < connection_count_max_;
    }
    return true;
}

ASimulationServer::APtr ASimulationServer::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                  boost::asio::io_service& io,
                                                  const std::string& server_ip,
                                                  const unsigned long server_port,
                                                  const unsigned long connection_count_max) {
    return ASimulationServer::APtr(
                                   new ASimulationServer(checkpoint_handler, io, server_ip, server_port, connection_count_max));
}

/*
 * ------------------------------------------------------------------
 * ASimulationClient implementation
 * ------------------------------------------------------------------
 */
ASimulationClient::ASimulationClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     boost::asio::io_service& io,
                                     const std::string& server_ip,
                                     const unsigned long server_port,
                                     const ASimulatorConnection::SimulationType simulation,
                                     const unsigned long simulation_repeat_count,
                                     const unsigned long message_size) :
    checkpoint_handler_(checkpoint_handler), simulation_(simulation), simulation_repeat_count(simulation_repeat_count),
            raw_connector_(RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, server_ip, server_port, this)),
            message_size_(message_size) {
}

ASimulationClient::~ASimulationClient(void) {
}

void ASimulationClient::start(void) {
    raw_connector_->aio_connect_start();
}

void ASimulationClient::com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
    connection_ = ASimulatorConnectionClient::create(tunnelendpoint, simulation_, simulation_repeat_count, message_size_);
}

void ASimulationClient::com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
}

bool ASimulationClient::is_done(void) {
    if (connection_ != NULL) {
        return connection_->is_done();
    }
    return false;
}

ASimulationClient::APtr ASimulationClient::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                  boost::asio::io_service& io,
                                                  const std::string& server_ip,
                                                  const unsigned long server_port,
                                                  const ASimulatorConnection::SimulationType simulation,
                                                  const unsigned long simulation_repeat_count,
                                                  const unsigned long message_size) {
    return ASimulationClient::APtr(
                                   new ASimulationClient(checkpoint_handler, io, server_ip, server_port, simulation, simulation_repeat_count, message_size));
}

