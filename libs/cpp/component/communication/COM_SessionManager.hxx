/*! \file COM_SessionManager.hxx
 \brief This file contains the SessionManager functionctionlaity
 */
#ifndef COM_SessionManager_HXX
#define COM_SessionManager_HXX

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_SessionManagerConnection.hxx>
#include <component/communication/COM_SessionManagerConnectionEventhandler.hxx>
#include <component/communication/COM_SessionManagerConnectionGroup.hxx>
#include <component/communication/COM_SessionManagerEventhandler.hxx>
#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionMessage.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Client side session manager
 *
 * The client side session manager holds a list of servers.
 * When starting the session manager a radom server is selected from from the server list.
 * If it is not posible to connect the chosen server, another is randomly selected from the list.
 * If non of the servers in the list works the connect fails.
 *
 * The following drawing shows the state-diagram of the session:
 \dot
 digraph diagram_session_manager_client_state {
 rankdir=TB;
 ranksep=0.5;
 center=true;
 size="12,8.5";
 edge [minlen=2, fontsize=8, fontname="Helvetica" ];
 node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
 fontname="Helvetica";

 State_Initializing
 State_Connecting
 State_Connected
 State_Closed
 State_Closing

 Start              -> State_Initializing [label="constructor"];

 State_Initializing -> State_Connecting   [label="connect_start"];
 State_Initializing -> State_Closed       [label="connect_start"];

 State_Connecting   -> State_Closed       [label="connect_start"];

 State_Initializing -> State_Closed       [label="close"];
 State_Connecting   -> State_Closed       [label="close"];
 State_Connected    -> State_Closing      [label="close"];
 State_Closing      -> State_Closed       [label="close"];

 State_Connected    -> State_Closed       [label="session_closed"];
 State_Closing      -> State_Closed       [label="session_closed"];

 State_Connecting   -> State_Connected    [label="com_tunnelendpoint_connected"];
 }
 \enddot
 *
 */
class SessionManagerClient: public SessionManagerConnectionEventhandler, public SessionEventhandler {
public:
    typedef boost::shared_ptr<SessionManagerClient> APtr;

    enum State {
        State_Initializing, State_Connecting, State_Connected, State_Closed, State_Closing, State_Unknown
    };

    /*! \brief Destructor
     */
    ~SessionManagerClient(void);

    /*! \brief Set eventhandler
     */
    void set_eventhandler(SessionManagerEventhandler*);

    /*! \brief Set eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Add servers informations
     */
    void set_servers(const std::string& servers_spec);
    void add_gserver_connection(const std::string& server_ip, const unsigned long server_port);
    void set_knownsecret_and_servers(const Utility::DataBufferManaged::APtr& known_secret, const std::string& servers_spec_string);

    /*! \brief Get session manager mutex
     */
    Utility::Mutex::APtr get_mutex(void);
    Utility::Mutex::APtr get_session_mutex(const unsigned long& connection_id);

    /*! \brief Enable/disable session logging (only to tell remote session).
     */
    void set_config_session_logging_enabled(const bool& session_logging_enabled);

    /*! \brief Start eventloop
     */
    void start(void);

    /*! \brief Close session connector and if connected also the connection
     */
    void close_start(void);

    /*! \brief Return true if the sessionmanager is closed
     */
    bool is_closed(void) const;

    /*! \brief Ask for resolve of connection
     */
    void session_manager_connection_resolve_connection(std::string& type, std::string& host, unsigned long& port, boost::posix_time::time_duration& timeout);

    /*! \brief Signals that a connection has been established
     */
    void session_manager_connection_connected(const unsigned long& id, const SessionCrypter::APtr&);

    /*! \brief Signals that connection failed
     */
    void session_manager_connection_failed(const unsigned long& id);

    /*! \brief Signal from session
     */
    void session_closed(const unsigned long session_id);

    /*! \brief Signal from session
     */
    void session_security_closed(const unsigned long session_id, const std::string& remote_ip);


    void set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type);

    /*! \brief Create instance
     */
    static APtr create(const AsyncService::APtr& async_service,
                       const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const Utility::DataBufferManaged::APtr& known_secret);
    static APtr create(const AsyncService::APtr& async_service,
                       const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const Utility::DataBufferManaged::APtr& known_secret,
                       const CryptFacility::KeyStore::APtr& key_store);

protected:

    /*! \brief Constructor
     */
    SessionManagerClient(const AsyncService::APtr& async_service,
                         const Utility::CheckpointHandler::APtr& checkpoint_handler,
                         const Utility::DataBufferManaged::APtr& known_secret,
                         const CryptFacility::KeyStore::APtr& key_store);

    void select_connection_from_connection_group_start(void);
    void select_connection_from_connection_group(const boost::system::error_code& error);

    void connect_start(void);

    void close(void);

    AsyncService::APtr async_service_;
    State state_;

    std::vector<SessionManagerConnectionGroup::APtr> gserver_connection_groups_;
    std::list<SessionManagerConnectionGroup::APtr> gserver_connection_groups_saved_;


    unsigned long gserver_connection_group_next_;
    SessionManagerConnectionGroup::APtr gserver_connection_group_current_;
    boost::asio::steady_timer gserver_connection_group_current_selection_delay_timer_;
    boost::asio::io_service::strand gserver_connection_group_current_selection_strand_;

    bool session_logging_enabled_;

    SessionManagerEventhandler* eventhandler_;

    Utility::CheckpointHandler::APtr checkpoint_handler_;
    Utility::DataBufferManaged::APtr known_secret_;
    CryptFacility::KeyStore::APtr key_store_;

    unsigned long connection_id_next_;
    std::map<unsigned long, SessionManagerConnection::APtr> session_connections_;

    Session::APtr current_session_;
    Utility::Mutex::APtr mutex_;
    SessionMessage::ApplProtocolType appl_protocol_type_;
};

/*! \brief Server side session manager
 *
 * The server side session manager listen on a port and accept incomming connections
 * trying to create sessions for each connect.
 *
 * A idle-janitor can be configured to handle timout of idle sessions.
 *
 * A log-janitor can be configured to report status.
 *
 * The following drawing shows the state-diagram of the session:
 \dot
 digraph diagram_session_manager_server_state {
 rankdir=TB;
 ranksep=0.5;
 center=true;
 size="12,8.5";
 edge [minlen=2, fontsize=8, fontname="Helvetica" ];
 node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
 fontname="Helvetica";

 State_Initializing
 State_Closed
 State_Closing
 State_Running

 Start              -> State_Initializing [label="constructor"];
 State_Initializing -> State_Running      [label="start"];

 State_Running  -> State_Closing [label="close"];
 State_Closing  -> State_Closed  [label="close"];
 }
 \enddot
 */
class SessionManagerServer: public RawTunnelendpointAcceptorTCPEventhandler,
        public AsyncContinuePolicy,
        public SessionEventhandler {
public:
    typedef boost::shared_ptr<SessionManagerServer> APtr;

    enum State {
        State_Initializing, State_Closed, State_Closing, State_Running, State_Unknown
    };

    /*! \brief Destructor
     */
    ~SessionManagerServer(void);

    /*! \brief Set eventhandler
     */
    void set_eventhandler(SessionManagerEventhandler*);

    /*! \brief Set eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Set durration for session idle timeout
     */
    void set_config_idle_timeout(const boost::posix_time::time_duration& session_idle_timeout, const boost::posix_time::time_duration& keyexchange_phase_one_idle_timeout);

    /*! \brief Set durration log entries
     */
    void set_config_log_interval(const boost::posix_time::time_duration& config_log_interval);

    /*! \brief Close the session manager when no more sessions. Mainly used during testing.
     */
    void set_config_close_when_no_more_sessions(const bool& config_close_when_no_more_sessions);

    /*! \brief Enable/disable session logging (only to tell remote session)COM_SessionManage.
     */
    void set_config_session_logging_enabled(const bool& session_logging_enabled);

    /*! \brief Set reuse of address option for acceptor.
     */
    void set_option_reuse_address(const bool& option_reuse_address);

    /*! \brief Set high and low bounds for identify dos_attack
     */
    void set_config_security_dos_attack(const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips);

    /*! \brief Set https(ssl) configuration
     */
    void set_config_https(
    		const std::string& certificate_path,
    		const bool certificate_verificatrion_enabled,
    		const bool hostname_verification_enabled,
    		const bool sni_enabled,
    		const std::string& method,
    		const unsigned long ssl_options
    		);

    /*! \brief Get session manager mutex
     */
    Utility::Mutex::APtr get_mutex(void);
    Utility::Mutex::APtr get_session_mutex(const unsigned long& connection_id);

    /*! \brief Start eventloop
     */
    void start(void);

    /*! \brief Start accepting connections
     */
    void accept_connections_start(void);

    /*! \brief Stop accepting connection
     */
    void accept_connections_stop(void);

    /*! \brief Close session manager nicely
     *
     * The call do not return until all sessions are discconected.
     */
    void close_start(void);

    /*! \brief Return true if the sessionmanager is closed
     */
    bool is_closed(void) const;

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message);

    /*! \brief closed
     */
    void com_tunnelendpoint_acceptor_closed(void);

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void);

    /*! \brief Signal from session
     */
    void session_state_ready(const unsigned long session_id);

    /*! \brief Signal from session
     */
    bool session_read_continue_state_ready(const unsigned long session_id);

    /*! \brief Signal from session
     */
    void session_closed(const unsigned long session_id);

    /*! \brief Signal from session
     */
    void session_security_closed(const unsigned long session_id, const std::string& remote_ip);


    int get_session_count(void);

    /*! \brief Create instance
     */
    static APtr create(const AsyncService::APtr& async_service,
                       const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const Utility::DataBufferManaged::APtr& known_secret,
                       const std::string& server_ip,
                       const unsigned long& server_port,
                       const std::string& server_sid);

protected:

    /*! \brief Constructor
     */
    SessionManagerServer(const AsyncService::APtr& async_service,
                         const Utility::CheckpointHandler::APtr& checkpoint_handler,
                         const Utility::DataBufferManaged::APtr& known_secret,
                         const std::string& server_ip,
                         const unsigned long& server_port,
                         const std::string& server_sid

    );

    void timeout_sessions(const boost::posix_time::time_duration& timeout, const bool force);
    void timeout_sessions_keyexchange(const boost::posix_time::time_duration& timeout, const bool force, const bool only_if_keyexchange_phase_one);

    void asio_log_janitor_start(void);
    void asio_log_janitor(const boost::system::error_code& error);

    void asio_session_idle_janitor_start(void);
    void asio_session_idle_janitor(const boost::system::error_code& error);

    void close_start_decoupled(void);
    void session_closed_decoupled(const unsigned long session_id);
    void session_security_closed_decoupled(const unsigned long session_id, const std::string& remote_ip);

    void close(void);

    void security_dos_attack_do_keyexchange_fence(const Session::APtr& session);
    void security_dos_attack(const std::string& attack_ip);
    void security_dos_attack_idle_janitor(void);
    std::string security_dos_attack_get_attack_info(void);
    bool security_dos_attack_banned(const Session::APtr& session);
    unsigned long security_dos_attack_get_count_doing_keyexchange_phase_one(void);


    AsyncService::APtr async_service_;
    State state_;
    std::string server_ip_;
    unsigned long server_port_;
    unsigned long next_session_id_;
    std::string server_sid_;
    bool session_logging_enabled_;
    bool idle_timeout_enabled_;

    SessionManagerEventhandler* eventhandler_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    Utility::DataBufferManaged::APtr known_secret_;

    typedef std::map<unsigned long, Session::APtr> SessionsType;
    SessionsType sessions_;

    RawTunnelendpointAcceptorTCP::APtr acceptor_;

    bool config_close_when_no_more_sessions_;
    boost::posix_time::time_duration config_session_idle_timeout_;
    boost::posix_time::time_duration config_keyexchange_idle_timeout_;
    boost::posix_time::time_duration config_log_interval_;

    boost::asio::steady_timer asio_log_janitor_timer_;
    boost::asio::io_service::strand asio_log_janitor_strand_;

    boost::asio::steady_timer asio_idle_janitor_timer_;
    boost::asio::io_service::strand asio_idle_janitor_strand_;

    Utility::Mutex::APtr mutex_;

    bool security_dos_attack_ban_atacker_ips_;

    int security_dos_attack_keyexchange_phase_one_count_high_;
    int security_dos_attack_keyexchange_phase_one_count_low_;

    enum SecurityState {
    	SecurityState_Normal, SecurityState_DoSAttack
    };
    SecurityState security_state_;
    unsigned long security_state_banned_connection_count_;
    unsigned long security_state_failed_keyexchange_count_;

    std::set<std::string> security_state_banned_ips_;

    unsigned long security_dos_attack_interval_security_closed_count_;
    int security_dos_attack_security_closed_count_high_;
    int security_dos_attack_security_closed_count_low_;

    unsigned long security_dos_attack_interval_banned_connection_count_;
    unsigned long security_dos_attack_interval_connection_count_;

};

}
}
#endif
