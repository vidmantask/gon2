/*! \file UTest_COM_Servers.cxx
 \brief This file contains unittest suite for the Servers specification functionality
 */
#include <boost/filesystem/path.hpp>
#include <boost/test/unit_test.hpp>
#include <component/communication/COM_Servers_Spec.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*! /brief Unittest for the the basis specification servers
 */
BOOST_AUTO_TEST_CASE( test_servers_parse_spec )
{
    const char servers_spec_001_data[] = "<?xml version='1.0'?>"
        "<servers>"
        "  <connection_group title='Group_1' selection_delay_sec='1'>"
        "    <connection title='Connection_11' host='127.0.1.41' port='8011' timeout_sec='11' type='direct' />"
        "    <connection title='Connection_12' host='127.0.1.42' port='8012' timeout_sec='12' type='http' />"
        "  </connection_group>"
        "  <connection_group title='Group_2' selection_delay_sec='2'>"
        "    <connection title='Connection_21' host='127.0.2.41' port='8021' timeout_sec='21' type='direct' />"
        "    <connection title='Connection_22' host='127.0.2.42' port='8022' timeout_sec='22' type='http' />"
        "  </connection_group>"
        "</servers>";

    ServersSpec::APtr servers_spec_001(ServersSpec::create());
    servers_spec_001->load_from_string(servers_spec_001_data);

    std::vector<ServersSpec::ServersConnectionGroup> connection_groups;
    servers_spec_001->get_servers_connection_groups(connection_groups);
    BOOST_CHECK( connection_groups.size() == 2);

    BOOST_CHECK( connection_groups.at(0).title == "Group_1");
    BOOST_CHECK( connection_groups.at(0).selection_delay == boost::posix_time::seconds(1));
    BOOST_CHECK( connection_groups.at(0).connections.size() == 2);

    BOOST_CHECK( connection_groups.at(1).title == "Group_2");
    BOOST_CHECK( connection_groups.at(1).selection_delay == boost::posix_time::seconds(2));
    BOOST_CHECK( connection_groups.at(1).connections.size() == 2);

    BOOST_CHECK( connection_groups.at(1).connections.at(1).title == "Connection_22");
    BOOST_CHECK( connection_groups.at(1).connections.at(1).host == "127.0.2.42");
    BOOST_CHECK( connection_groups.at(1).connections.at(1).port == 8022);
    BOOST_CHECK( connection_groups.at(1).connections.at(1).timeout == boost::posix_time::seconds(22));
    BOOST_CHECK( connection_groups.at(1).connections.at(1).type == ServersSpec::ServersConnection::ConnectionType_http);

    const char servers_spec_002_data[] = "<?xml version='1.0'?>"
        "<servers>"
        "</servers>";

    ServersSpec::APtr servers_spec_002(ServersSpec::create());
    servers_spec_002->load_from_string(servers_spec_002_data);
    std::vector<ServersSpec::ServersConnectionGroup> connection_groups_2;
    servers_spec_002->get_servers_connection_groups(connection_groups_2);
    BOOST_CHECK( connection_groups_2.size() == 0);
}

BOOST_AUTO_TEST_CASE( test_servers_parse_spec_error )
{
    const char servers_spec_001_data[] = "<?xml version='1.0'?>"
        "<serversxxx>"
        "</servers>";

    ServersSpec::APtr servers_spec_001(ServersSpec::create());
    BOOST_CHECK_THROW(servers_spec_001->load_from_string(servers_spec_001_data), ServersSpec::Exception_ServersSpec);
}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
