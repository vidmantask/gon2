/*! \file COM_Sessionr.cxx
 \brief This file contains the impplementation for the COM_Session classes
 */
#include <boost/asio.hpp>

#include <sstream>
#include <boost/bind.hpp>
#include <boost/thread.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionMessage.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_TrafficControlManager.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;




/*
 * ------------------------------------------------------------------
 * Session implementation
 * ------------------------------------------------------------------
 */
Session::Session(boost::asio::io_service& asio_io_service,
                 const unsigned long& session_id,
                 const std::string& sid,
                 const bool& session_logging_enabled,
                 const Utility::CheckpointHandler::APtr& checkpoint_handler,
                 const SessionCrypter::APtr& session_crypter,
                 SessionEventhandler* eventhandler,
                 const SessionMessage::ApplProtocolType appl_protocol_type,
                 const SessionType& session_type) :
                	 asio_io_service_(asio_io_service), session_id_(session_id), sid_(sid), eventhandler_(eventhandler),
                	 eventhandler_ready_(NULL), state_(State_Initalizing), checkpoint_handler_(checkpoint_handler),
                	 session_crypter_(session_crypter),
                	 session_tunnel_(SessionTunnelendpointReliableSessionCrypter::create(asio_io_service, checkpoint_handler, this, session_crypter)),
                	 tunnel_(TunnelendpointReliableCryptedTunnel::create(asio_io_service, checkpoint_handler, this, session_crypter->get_mutex())),
                	 session_logging_enabled_(session_logging_enabled),
                	 remote_session_logging_enabled_(false),
                	 keep_alive_ping_interval_(boost::posix_time::seconds(0)),
                	 keep_alive_ping_timer_(asio_io_service),
                	 asio_keep_alive_ping_running_(false),
                	 session_mutex_(session_crypter->get_mutex()),
                	 appl_protocol_type_(appl_protocol_type),
                	 remote_appl_protocol_type_(appl_protocol_type),
                	 session_type_(session_type),
                	 traffic_control_enabled_(true) {
    session_crypter_->set_eventhandler(this);

	tunnel_->set_connector(this);

	session_id_unique_ = calculate_session_id_unique();
    session_mutex_->set_mutex_id(session_id_unique_);
    remote_unique_session_id_ = "";
    registre_activity();


    // If the session crypte is in ready mode it is a special situation
    if (session_crypter->is_ready()) {
        if(eventhandler != NULL) {
            session_crypter_->set_eventhandler(this);
        }
        state_ = State_KeyExchange;
        session_crypter_key_exchange_complete();
    }
}

Session::~Session(void) {
    state_ = State_Unknown;
}


Mutex::APtr Session::get_mutex(void) {
    return session_mutex_;
}

RawTunnelendpointTCP::APtr Session::get_raw_tunnelendpoint(void) {
	return session_crypter_->get_raw_tunnelendpoint();
}

std::string Session::calculate_session_id_unique(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::calculate_session_id_unique");
    stringstream ss;
    ss << boost::posix_time::to_iso_string(boost::posix_time::microsec_clock::local_time());
    ss << "_";
    ss << sid_;
    ss << "_";
    ss << session_id_;
    return ss.str();
}

unsigned long Session::get_session_id(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_session_id");
    default:
        break;
    }
    return session_id_;
}

std::string Session::get_unique_session_id(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_unique_session_id");
    default:
        break;
    }

    /*
     * On client the remote session_id is used as unique, if available
     */
    if(session_type_ == SessionTypeClient) {
        MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::get_unique_session_id");
    	if(remote_unique_session_id_ != "") {
    		return remote_unique_session_id_;
    	}
    }
    return session_id_unique_;
}

std::string Session::get_remote_unique_session_id(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::get_remote_unique_session_id");
	switch (state_) {
	case State_Unknown:
		throw ExceptionUnexpected("Session state not ready for get_remote_unique_session_id");
	default:
		break;
	}
	return remote_unique_session_id_;
}

bool Session::get_remote_session_logging_enabled(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_remote_session_logging_enabled");
    default:
        break;
    }
    return remote_session_logging_enabled_;
}

std::pair<std::string, unsigned long> Session::get_ip_remote(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_ip_remote");
    default:
        break;
    }
    return session_crypter_->get_ip_remote();
}

std::pair<std::string, unsigned long> Session::get_ip_local(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_ip_local");
    default:
        break;
    }
    return session_crypter_->get_ip_local();
}

bool Session::is_idle(const boost::posix_time::time_duration& idle_timeout) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for is_idle");
    default:
        break;
    }
    boost::posix_time::time_duration idle_time(boost::posix_time::microsec_clock::local_time() - last_activity_);
    return idle_time > idle_timeout;
}

boost::posix_time::ptime Session::get_last_activity(void) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_last_activity");
    default:
        break;
    }
    return last_activity_;
}

bool Session::is_closed(void) {
    return state_ == State_Closed || state_ == State_Done;
}

bool Session::async_mem_guard_is_done(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::async_mem_guard_is_done");
//    Checkpoint(*checkpoint_handler_, "Session::async_mem_guard_is_done", Attr_Communication(), CpAttr_debug(), CpAttr("state", state_), CpAttr("asio_keep_alive_ping_running_", asio_keep_alive_ping_running_));
	return (state_ == State_Done) && (!asio_keep_alive_ping_running_);
}

std::string Session::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void Session::set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::set_appl_protocol_type");
    appl_protocol_type_ = appl_protocol_type;
}

SessionMessage::ApplProtocolType Session::get_appl_protocol_type(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::get_appl_protocol_type");
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for get_appl_protocol_type");
    default:
        break;
    }
    if (remote_appl_protocol_type_ == appl_protocol_type_) {
    	return appl_protocol_type_;
    }
    if ((remote_appl_protocol_type_ == SessionMessage::ApplProtocolType_xml) || (appl_protocol_type_ == SessionMessage::ApplProtocolType_xml)) {
    	return SessionMessage::ApplProtocolType_xml;
    }
    return SessionMessage::ApplProtocolType_python;
}


void Session::registre_activity(void) {
    last_activity_ = boost::posix_time::microsec_clock::local_time();
}

void Session::close(const bool force) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::close");
    switch (state_) {
    case State_Ready:
    case State_ReadySessionData:
    case State_KeyExchange:
    case State_Initalizing:
        break;
    case State_Closing:
    case State_Closed:
    case State_Done:
        return;
    default:
        throw ExceptionUnexpected("Session state not ready for close");
    }
    state_ = State_Closing;
    asio_keep_alive_ping_stop();

    tunnel_->close(true);
    session_tunnel_->close(force);

    if(session_crypter_->is_closed()) {
    	close_finale();
    	return;
    }
    session_crypter_->close(force);
}

void Session::close_finale(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::close_finale");
    switch (state_) {
    case State_Closing:
    	break;
    case State_Closed:
    case State_Done:
        return;
    default:
        throw ExceptionUnexpected("Session state not ready for close_finale");
    }

    state_ = State_Closed;
    if (eventhandler_ready_ != NULL) {
        try {
            SessionEventhandlerReady* eventhandler_ready_temp(eventhandler_ready_);
            reset_eventhandler_ready();
            eventhandler_ready_temp->session_state_closed();
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::close_finale.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }
    if (eventhandler_ != NULL) {
        SessionEventhandler* eventhandler_temp(eventhandler_);
    	reset_eventhandler();
    	eventhandler_temp->session_closed(session_id_);
    }
    tunnel_->reset_eventhandler();
    tunnel_->reset_connector();
    session_crypter_->reset_eventhandler();
    session_tunnel_->reset_eventhandler();

    if(traffic_control_session_.get() != NULL) {
    	traffic_control_session_->reset_eventhandler();
    }
    state_ = State_Done;
}

void Session::read_start_state_ready(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::read_start_state_ready");
    switch (state_) {
    case State_Ready:
    case State_ReadySessionData:
        break;
    case State_Closing:
    case State_Closed:
    case State_Done:
        return;
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for read_start_state_ready");
    }
    if (eventhandler_ready_ != NULL) {
        try {
            if (eventhandler_ready_->session_read_continue_state_ready()) {
                session_crypter_->read_start_state_ready();
            }
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::read_start_state_ready.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
            enter_error_mode();
            return;
        }
    }
    else {
        session_crypter_->read_start_state_ready();
    }
}

bool Session::session_tunnelendpoint_read_continue_state_ready(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::session_tunnelendpoint_read_continue_state_ready");
    switch (state_) {
    case State_ReadySessionData:
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Session state not ready for session_tunnelendpoint_read_continue_state_ready");
    }

    if (eventhandler_ready_ != NULL) {
        try {
            return eventhandler_ready_->session_read_continue_state_ready();
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::session_tunnelendpoint_read_continue_state_ready.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
            enter_error_mode();
            return false;
        }
    }
    return false;
}

void Session::set_eventhandler(SessionEventhandler* eventhandler, const bool& move_session_crypter_eventhandler) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::set_eventhandler");
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for set_eventhandler>");
    default:
        break;
    }
    eventhandler_ = eventhandler;
    if (move_session_crypter_eventhandler) {
        if(session_crypter_.get() != NULL) {
            session_crypter_->set_eventhandler(this);
        }
    }
}

void Session::reset_eventhandler(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::reset_eventhandler");
    eventhandler_ = NULL;
}

void Session::set_eventhandler_ready(SessionEventhandlerReady* eventhandler_ready) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::set_eventhandler_ready");
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for set_eventhandler_ready");
    default:
        break;
    }
    eventhandler_ready_ = eventhandler_ready;
}

void Session::reset_eventhandler_ready(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::reset_eventhandler_ready");
    eventhandler_ready_ = NULL;
}

void Session::add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint) {
    switch (state_) {
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for add_tunnelendpoint");
    default:
        break;
    }
    tunnel_->add_tunnelendpoint(child_id, tunnelendpoint);
}

void Session::do_key_exchange(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::do_key_exchange");
	switch (state_) {
    case State_Initalizing:
    case State_Ready:
    case State_ReadySessionData:
    case State_KeyExchange:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
    	return;
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for do_key_exchange");
    }
	session_mutex_->get_strand().post(boost::bind(&Session::do_key_exchange_decoubled, this));
}

void Session::do_key_exchange_decoubled(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::do_key_exchange_decoubled");
	switch (state_) {
    case State_Initalizing:
    case State_Ready:
    case State_ReadySessionData:
    case State_KeyExchange:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
    	return;
    default:
        throw ExceptionUnexpected("Session state not ready for do_key_exchange_decoubled");
    }
    state_ = State_KeyExchange;
    if (eventhandler_ready_ != NULL) {
        try {
            eventhandler_ready_->session_state_key_exchange();
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::do_key_exchange.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
            enter_error_mode();
            return;
        }
    }
    session_crypter_->do_key_exchange();
}

void Session::session_crypter_key_exchange_complete(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::session_crypter_key_exchange_complete");
	switch (state_) {
    case State_KeyExchange:
        break;
    case State_Closing:
    case State_Closed:
    case State_Done:
    	return;
    default :
        throw ExceptionUnexpected("Session state not ready for signal: session_crypter_key_exchange_complete");
    }
    state_ = State_ReadySessionData;
    registre_activity();

    SessionMessage::APtr session_data(SessionMessage::create(get_unique_session_id(), session_logging_enabled_, appl_protocol_type_, traffic_control_enabled_));
    session_tunnel_->session_tunnelendpoint_send(session_data->build_message_com());
    read_start_state_ready();
}

void Session::session_crypter_key_exchange_failed(const std::string& error_message) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::session_crypter_key_exchange_failed");
    switch (state_) {
    case State_KeyExchange:
        break;
    case State_Closing:
    case State_Closed:
    case State_Done:
    	return;
    default :
        throw ExceptionUnexpected("Session state not ready for signal: session_crypter_key_exchange_failed");
    }

    if (eventhandler_ != NULL) {
        Checkpoint cp(*checkpoint_handler_, "Session::session_crypter_key_exchange_failed", Attr_Communication(), CpAttr_error(), CpAttr_message(error_message));
        try {
            eventhandler_->session_security_closed(session_id_, get_ip_remote().first);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::session_crypter_key_exchange_failed.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
            enter_error_mode();
            return;
        }
    }
    close(true);
}

void Session::session_crypter_closed(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::session_crypter_closed");
    CheckpointScope cps(*checkpoint_handler_, "Session::session_crypter_closed", Attr_Communication(), CpAttr_debug(), CpAttr("state", state_));
    switch (state_) {
    case State_Initalizing:
    case State_Ready:
    case State_ReadySessionData:
    case State_KeyExchange:
    case State_Closing:
        break;
    case State_Closed:
    case State_Done:
    	return;
    default:
        throw ExceptionUnexpected("Session state not ready for session_crypter_closed");
    }

    if(state_ == State_Closing) {
        close_finale();
    }
    else {
        close(true);
    }
}

void Session::session_tunnelendpoint_recieve(const MessageCom::APtr& message) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::session_tunnelendpoint_recieve");
    switch (state_) {
    case State_Ready:
    case State_ReadySessionData:
        break;
    case State_Closing:
    case State_Closed:
    case State_Done:
    case State_Error: {
        Checkpoint c1(*checkpoint_handler_, "Session::session_tunnelendpoint_recieve.ignored", Attr_Communication(), CpAttr_warning(), CpAttr("state", state_));
        return;
    }
    default:
        throw ExceptionUnexpected("Session state not ready for session_tunnelendpoint_recieve");
    }

    if(state_ == State_ReadySessionData) {
        tunnel_->set_protocol_version(message->get_protocol_version());
        SessionMessage::APtr session_data(SessionMessage::create(message));
        if (session_data != NULL) {
        	remote_unique_session_id_ = session_data->get_unique_session_id_();
        	remote_session_logging_enabled_ = session_data->get_session_logging_enabled();
        	remote_appl_protocol_type_ = session_data->get_appl_protocol_type();
        	tunnel_->set_appl_protocol_type(get_appl_protocol_type());

        	// mutex_id is used as tc_session id
        	session_mutex_->set_mutex_id(get_unique_session_id());
            Checkpoint cps(*checkpoint_handler_, "Session::session_tunnelendpoint_recieve.remote_info", Attr_Communication(),
                           CpAttr_debug(), CpAttr("remote_unique_session_id", remote_unique_session_id_), CpAttr("remote_session_logging_enabled", remote_session_logging_enabled_), CpAttr("appl_protocol_type", get_appl_protocol_type()));
        }
        state_ = State_Ready;
        if (eventhandler_ready_ != NULL) {
            try {
                tunnel_->connect();
                eventhandler_ready_->session_state_ready();
            } catch (ExceptionCallback& e) {
                Checkpoint cp(*checkpoint_handler_, "Session::session_tunnelendpoint_recieve.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
                enter_error_mode();
                return;
            }
        }

        /*
         * Mark raw tunnelendpoint as a gconnection for use be TC
         */
        session_crypter_->get_raw_tunnelendpoint()->mark_as_gconnection(get_unique_session_id());

	    traffic_control_session_ = TrafficControlManager::get_global(asio_io_service_, checkpoint_handler_)->get_session(get_unique_session_id());
	    traffic_control_session_->set_eventhandler(this);
	    traffic_control_session_->interval_tick_start();

	    bool remote_traffic_control_enabled = session_data->get_traffic_control_enabled();
	    if(!(remote_traffic_control_enabled && traffic_control_enabled_)) {
            Checkpoint cp(*checkpoint_handler_, "Session::traffic_control.disabled", Attr_Communication(), CpAttr_debug());
	    	traffic_control_enabled_ = false;
    		traffic_control_session_->set_traffic_control_enabled(false);
	    }
	    else {
            Checkpoint cp(*checkpoint_handler_, "Session::traffic_control.enabled", Attr_Communication(), CpAttr_debug());
	    }
        asio_keep_alive_ping_start();
    }
    else {
		try {
			if ((message->get_package_type() == MessageCom::PackageType_Data) ||
			    (message->get_package_type() == MessageCom::PackageType_TunnelendpointData) ||
                (message->get_package_type() == MessageCom::PackageType_DataHuge) ) {
			    registre_activity();
                tunnel_->tunnelendpoint_recieve(message);
                return;
            }
			else if(message->get_package_type() == MessageCom::PackageType_KeepAlivePing) {
				// Message is ignored, reciving it has forfulled its current purpose
			}
			else if(message->get_package_type() == MessageCom::PackageType_KeepAlivePingStart) {
				handle_remote_ping_start(message->get_payload());
			}
			else if(message->get_package_type() == MessageCom::PackageType_KeepAlivePingStop) {
				handle_remote_ping_stop();
			}
			else if((message->get_package_type() == MessageCom::PackageType_TC_Ping) ||
					(message->get_package_type() == MessageCom::PackageType_TC_PingResponse)) {
				if(traffic_control_session_.get() != NULL) {
					traffic_control_session_->recieve_message(message);
				}
			}
			else {
				Checkpoint cps(*checkpoint_handler_, "Session::session_tunnelendpoint_recieve.package_ignored", Attr_Communication(), CpAttr_warning(), CpAttr("package_type", message->get_package_type()));
			}
		} catch (const ExceptionInvalidTunnelendpoint& e) {
			Checkpoint cps(*checkpoint_handler_, "Session::session_tunnelendpoint_recieve.error", Attr_Communication(), CpAttr_warning(), CpAttr("message", e.what()));
		}
    }
}

void Session::tunnelendpoint_connector_send(const MessageCom::APtr& message) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::tunnelendpoint_connector_send");
    switch (state_) {
    case State_Ready:
        break;
    case State_ReadySessionData:
    case State_Closing:
    case State_Closed:
    case State_Done:
        return;
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for tunnelendpoint_connector_send");
    }

    registre_activity();
    session_tunnel_->session_tunnelendpoint_send(message);
}

void Session::tunnelendpoint_connector_closed(void) {
    // Ignoring signal
}

void Session::tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::tunnelendpoint_connector_user_signal");
    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Session state not ready for tunnelendpoint_connector_user_signal");
    }
    if (eventhandler_ready_ != NULL) {
        try {
            eventhandler_ready_->session_user_signal(signal_id, message);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "Session::tunnelendpoint_connector_user_signal.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
            enter_error_mode();
            return;
        }
    }
}

std::string Session::tunnelendpoint_connector_get_unique_session_id(void) {
	return get_unique_session_id();
}

void Session::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    // Ignoring signal
}

void Session::tunnelendpoint_eh_connected(void) {
    // Ignoring signal
}

void Session::traffic_controll_session_send_message(const MessageCom::APtr& com_message) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::traffic_controll_session_send_message");
    switch (state_) {
    case State_Ready:
        break;
    case State_ReadySessionData:
    case State_Closing:
    case State_Closed:
    case State_Done:
        return;
    case State_Unknown:
        throw ExceptionUnexpected("Session state not ready for traffic_controll_session_send_message");
    }
    session_tunnel_->session_tunnelendpoint_send(com_message);
}

boost::asio::io_service& Session::get_io_service(void) {
    return asio_io_service_;
}

void Session::asio_keep_alive_ping_start(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::asio_keep_alive_ping_start");
	if (asio_keep_alive_ping_running_) {
		return;
	}
	if(keep_alive_ping_interval_.total_seconds() > 0) {
		asio_keep_alive_ping_running_ = true;
		keep_alive_ping_timer_.expires_after(boost::chrono::seconds(keep_alive_ping_interval_.total_seconds()));
		keep_alive_ping_timer_.async_wait(boost::bind(&Session::asio_keep_alive_ping, this, boost::asio::placeholders::error));
	}
}

void Session::asio_keep_alive_ping_stop(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::asio_keep_alive_ping_stop");
	if(asio_keep_alive_ping_running_) {
		keep_alive_ping_timer_.expires_after(boost::chrono::seconds(0));
	}
}

void Session::asio_keep_alive_ping(const boost::system::error_code& error) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::asio_keep_alive_ping");
    asio_keep_alive_ping_running_ = false;
    switch (state_) {
    case State_Closing:
        close_finale();
    	return;
    case State_Ready:
        break;
    default:
    	return;
    }
    stringstream ss;
    ss << keep_alive_ping_interval_;
    DataBufferManaged::APtr keep_alive_ping_message_payload(DataBufferManaged::create(ss.str()));
	MessageCom::APtr keep_alive_ping_message(MessageCom::create(MessageCom::PackageType_KeepAlivePing, MessagePayload::create(keep_alive_ping_message_payload), 0));
	session_tunnel_->session_tunnelendpoint_send(keep_alive_ping_message);
    asio_keep_alive_ping_start();
}

void Session::set_keep_alive_ping_interval(const boost::posix_time::time_duration& keep_alive_ping_interval) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::set_keep_alive_ping_interval");
    keep_alive_ping_interval_ = keep_alive_ping_interval;

    if(asio_keep_alive_ping_running_) {
    	// This will abort pending callback (boost::asio::error::operation_aborted), and call asio_keep_alive_ping_start with the new value
    	keep_alive_ping_timer_.expires_after(boost::chrono::seconds(keep_alive_ping_interval_.total_seconds()));
    }
    else {
    	asio_keep_alive_ping_start();
    }
}

void Session::set_traffic_control_enabled(const bool value) {
	traffic_control_enabled_ = value;
}

Session::APtr Session::create_client(boost::asio::io_service& asio_io_service,
                                     const unsigned long& session_id,
                                     const std::string& sid,
                                     const bool& session_logging_enabled,
                                     const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                                     const Utility::DataBufferManaged::APtr& known_secret,
                                     const SessionMessage::ApplProtocolType appl_protocol_type) {
    SessionCrypter::APtr session_crypter(SessionCrypterClient::create(checkpoint_handler, raw_tunnelendpoint, known_secret));
    Session::APtr new_session(new Session(asio_io_service, session_id, sid, session_logging_enabled, checkpoint_handler, session_crypter, NULL, appl_protocol_type, Session::SessionTypeClient));
	AsyncMemGuard::global_add_element(new_session);
	return new_session;
}

Session::APtr Session::create_client(boost::asio::io_service& asio_io_service,
                                     const unsigned long& session_id,
                                     const std::string& sid,
                                     const bool& session_logging_enabled,
                                     const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     const SessionCrypter::APtr& session_crypter,
                                     SessionEventhandler* eventhandler,
                                     const SessionMessage::ApplProtocolType appl_protocol_type) {
    Session::APtr new_session(new Session(asio_io_service, session_id, sid, session_logging_enabled, checkpoint_handler, session_crypter, eventhandler, appl_protocol_type, Session::SessionTypeClient));
	AsyncMemGuard::global_add_element(new_session);
	return new_session;
}

Session::APtr Session::create_server(boost::asio::io_service& asio_io_service,
                                     const unsigned long& session_id,
                                     const std::string& sid,
                                     const bool& session_logging_enabled,
                                     const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                                     const Utility::DataBufferManaged::APtr& known_secret) {
    SessionCrypter::APtr session_crypter(SessionCrypterServer::create(checkpoint_handler, raw_tunnelendpoint, known_secret));
    Session::APtr new_session(new Session(asio_io_service, session_id, sid, session_logging_enabled, checkpoint_handler, session_crypter, NULL, SessionMessage::ApplProtocolType_python, Session::SessionTypeServer));
	AsyncMemGuard::global_add_element(new_session);
    return new_session;
}

void Session::enter_error_mode(void) {
    state_ = State_Error;
    close(true);
}

bool Session::doing_keyexchange_phase_one(void) {
    switch (state_) {
    case State_KeyExchange:
        break;
    default:
    	return false;
    }
    return session_crypter_->doing_keyexchange_phase_one();
}

bool Session::doing_keyexchange(void) {
    switch (state_) {
    case State_KeyExchange:
		return true;
    	break;
    default:
    	return false;
    }
}

bool Session::is_connected(void) {
	return traffic_control_session_->is_connected();
}

void Session::handle_remote_ping_start(const MessagePayload::APtr& payload) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::handle_remote_ping_start");
    switch (state_) {
    case State_KeyExchange:
    case State_ReadySessionData:
    case State_Ready:
        break;
    default:
    	return;
    }
    try {
    	stringstream ss(payload->get_buffer()->toString());
    	unsigned long sec;
    	ss >> sec;
        set_keep_alive_ping_interval(boost::posix_time::seconds(sec));
    }
    catch(...) {
    	return;
    }
}

void Session::handle_remote_ping_stop(void) {
    MutexScopeLock session_mutex_lock_(checkpoint_handler_, Attr_Communication(), session_mutex_, "Session::handle_remote_ping_stop");
    switch (state_) {
    case State_KeyExchange:
    case State_ReadySessionData:
    case State_Ready:
        break;
    default:
    	return;
    }
    set_keep_alive_ping_interval(boost::posix_time::seconds(0));
}
