/*! \file COM_API_Tunnelendpoint.hxx
 *  \brief This file contains the a API wrapper for Tunnelendpoints
 */
#ifndef COM_API_Tunnelendpoint_HXX
#define COM_API_Tunnelendpoint_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/version/Version.hxx>
#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_Tunnelendpoint.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler for a tunnelendpoint connector.
 */
class APITunnelendpointEventhandler : public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_recieve(const std::string& message) = 0;

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_connected(void) = 0;

    /*! \brief User defined signal from a child
     */
    virtual void tunnelendpoint_recieve_user_signal(const unsigned long child_id, const unsigned long signal_id, const std::string& message) = 0;
};

/*! \brief Abstract API tunnelendpoint
 */
class APITunnelendpoint : public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<APITunnelendpoint> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpoint(void);

    /*! \brief Set eventhandler
     */
    void set_eventhandler(APITunnelendpointEventhandler* eventhandler);
    void reset_eventhandler(void);

    /*! \brief Returns tunnelendpoint implemntation
     */
    virtual Tunnelendpoint::APtr get_impl(void) const = 0;

    bool is_connected(void) const;

    void set_version(const Version::Version::APtr& version);

    Version::Version::APtr get_version(void) const;

    Version::Version::APtr get_version_remote(void) const;

    std::string get_id_abs() const;

    APIMutex::APtr get_mutex(void);

    unsigned long get_tc_read_delay_ms(void);
    unsigned long get_tc_read_size(void);
    void tc_report_read_begin(void);
    void tc_report_read_end(const unsigned long size);

    void tc_report_push_buffer_size(const unsigned long size);
    void tc_report_pop_buffer_size(const unsigned long size);


    /*! \brief Signals from TunnelendpointEventhandler
     */
    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    SessionMessage::ApplProtocolType get_appl_protocol_type(void);

    boost::asio::io_service& get_io(void);

protected:
    /*! \brief Constructor
     */
    APITunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);
    APITunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, APITunnelendpointEventhandler* eventhandler);

    APITunnelendpointEventhandler* eventhandler_;
    bool is_ready;
    boost::asio::io_service& io_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
};

/*! \brief encrypted and reliable tunnelendpoint
 */
class APITunnelendpointReliableCrypted : public APITunnelendpoint {
public:
    typedef boost::shared_ptr<APITunnelendpointReliableCrypted> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointReliableCrypted(void);

    /*! \brief Returns tunnelendpoint implemntation
     */
    virtual Tunnelendpoint::APtr get_impl(void) const;

    /*! \brief Enable skip of connection handshake (and version exchange).
     */
    void set_skip_connection_handshake(void);

    /*! \brief Create instance
     */
    static APtr create(const APIAsyncService::APtr& async_service, const Utility::APICheckpointHandler::APtr& checkpoint_handler);

    static void self_set_eventhandler(const APITunnelendpointReliableCrypted::APtr& self, APITunnelendpointEventhandler* eventhandler);
    static void self_reset_eventhandler(const APITunnelendpointReliableCrypted::APtr& self);

    static bool self_is_connected(const APITunnelendpointReliableCrypted::APtr& self);
    static void self_set_version(const APITunnelendpointReliableCrypted::APtr& self, const boost::python::tuple& version);
    static void self_set_skip_connection_handshake(const APITunnelendpointReliableCrypted::APtr& self);

    static boost::python::tuple self_get_version(const APITunnelendpointReliableCrypted::APtr& self);
    static boost::python::tuple self_get_version_remote(const APITunnelendpointReliableCrypted::APtr& self);
    static boost::python::str self_get_id_abs(const APITunnelendpointReliableCrypted::APtr& self);
    static APIMutex::APtr self_get_mutex(const APITunnelendpointReliableCrypted::APtr& self);
    static SessionMessage::ApplProtocolType self_get_appl_protocol_type(const APITunnelendpointReliableCrypted::APtr& self);

    static unsigned long self_get_tc_read_delay_ms(const APITunnelendpointReliableCrypted::APtr& self);
    static unsigned long self_get_tc_read_size(const APITunnelendpointReliableCrypted::APtr& self);

    static void self_tc_report_read_begin(const APITunnelendpointReliableCrypted::APtr& self);
    static void self_tc_report_read_end(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size);

    static void self_tc_report_push_buffer_size(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size);
    static void self_tc_report_pop_buffer_size(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size);


    /*
     * Decoubled
     */
    static void self_tunnelendpoint_send(
    		const APITunnelendpointReliableCrypted::APtr self,
    		const std::string& message);
    void tunnelendpoint_send_decoubled(const MessagePayload::APtr message);

    static void self_tunnelendpoint_user_signal(
    		const APITunnelendpointReliableCrypted::APtr self,
    		const unsigned long& signal_id,
    		const std::string& message);
    void tunnelendpoint_user_signal_decoubled(const unsigned long signal_id, const MessagePayload::APtr message);

    static void self_close(const APITunnelendpointReliableCrypted::APtr self);
    void close_decoubled(void);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointReliableCrypted(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);
    APITunnelendpointReliableCrypted(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, APITunnelendpointEventhandler* eventhandler);


    TunnelendpointReliableCrypted::APtr impl_tunnelendpoint_;
};

/*! \brief encrypted and reliable tunnelendpoint tunnel
 */
class APITunnelendpointReliableCryptedTunnel : public APITunnelendpoint {
public:
    typedef boost::shared_ptr<APITunnelendpointReliableCryptedTunnel> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointReliableCryptedTunnel(void);

    /*! \brief Sends a message
     */
    void tunnelendpoint_send(const std::string& message);

    /*! \brief sends a user defined signal to the parent of the tunnel
     */
    void tunnelendpoint_user_signal(const unsigned long& signal_id, const std::string& message);

    /*! \brief Add child tunnelendpoint
     */
    void add_tunnelendpoint(const unsigned long& child_id,
                            const APITunnelendpoint::APtr& tunnelendpoint);
    void add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint);

    /*! \brief Remove child tunnelendpoint
     */
    void remove_tunnelendpoint(const unsigned long& child_id);

    /*! \brief Enable skip of connection handshake (and version exchange).
     */
    void set_skip_connection_handshake(void);

    /*! \brief Returns tunnelendpoint implemntation
     */
    virtual Tunnelendpoint::APtr get_impl(void) const;

    void close(void);



    /*! \brief Create instance
     */
    static APtr create(const APIAsyncService::APtr& async_service, const Utility::APICheckpointHandler::APtr& checkpoint_handler);

    static void self_set_eventhandler(const APITunnelendpointReliableCryptedTunnel::APtr& self, APITunnelendpointEventhandler* eventhandler);
    static void self_reset_eventhandler(const APITunnelendpointReliableCryptedTunnel::APtr& self);

    static void
            self_add_tunnelendpoint_reliable_crypted(const APITunnelendpointReliableCryptedTunnel::APtr& self,
                                                     const unsigned long& child_id,
                                                     const APITunnelendpointReliableCrypted::APtr& tunnelendpoint);
    static void
            self_add_tunnelendpoint_reliable_crypted_tunnel(const APITunnelendpointReliableCryptedTunnel::APtr& self,
                                                            const unsigned long& child_id,
                                                            const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint);


    static bool self_is_connected(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static void self_set_version(const APITunnelendpointReliableCryptedTunnel::APtr& self, const boost::python::tuple& version);
    static boost::python::tuple self_get_version(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static boost::python::tuple self_get_version_remote(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static boost::python::str self_get_id_abs(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static APIMutex::APtr self_get_mutex(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static SessionMessage::ApplProtocolType self_get_appl_protocol_type(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static void self_set_skip_connection_handshake(const APITunnelendpointReliableCryptedTunnel::APtr& self);

    static unsigned long self_get_tc_read_delay_ms(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static unsigned long self_get_tc_read_size(const APITunnelendpointReliableCryptedTunnel::APtr& self);

    static void self_tc_report_read_begin(const APITunnelendpointReliableCryptedTunnel::APtr& self);
    static void self_tc_report_read_end(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size);

    static void self_tc_report_push_buffer_size(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size);
    static void self_tc_report_pop_buffer_size(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size);

    /*
     * Decoubled
     */
    static void self_tunnelendpoint_send(
    		const APITunnelendpointReliableCryptedTunnel::APtr self,
    		const std::string& message);
    void tunnelendpoint_send_decoubled(const MessagePayload::APtr message);

    static void self_tunnelendpoint_user_signal(
    		const APITunnelendpointReliableCryptedTunnel::APtr self,
    		const unsigned long& signal_id,
    		const std::string& message);
    void tunnelendpoint_user_signal_decoubled(const unsigned long signal_id, const MessagePayload::APtr message);

    static void self_remove_tunnelendpoint(
    		const APITunnelendpointReliableCryptedTunnel::APtr self,
    		const unsigned long child_id);
    void remove_tunnelendpoint_decoubled(const unsigned long child_id);

    static void self_close(const APITunnelendpointReliableCryptedTunnel::APtr self);
    void close_decoubled(void);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointReliableCryptedTunnel(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);
    APITunnelendpointReliableCryptedTunnel(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, APITunnelendpointEventhandler* eventhandler);

    TunnelendpointReliableCryptedTunnel::APtr impl_tunnelendpoint_;
};

}
}
#endif
