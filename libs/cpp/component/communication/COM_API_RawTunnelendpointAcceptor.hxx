/*! \file COM_API_RawTunnelendpointAcceptor.hxx
 *  \brief This file contains the a API wrapper for raw-tunnelendpoint acceptor
 */
#ifndef COM_API_RawTunnelendpointAcceptor_HXX
#define COM_API_RawTunnelendpointAcceptor_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_AsyncContinuePolicy.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler for a tcp-tunnelendpoint acceptor
 */
class APIRawTunnelendpointAcceptorTCPEventhandler: public boost::noncopyable {
public:

    /*! \brief Connected signal
     *
     *  \param tunnelendpoint connected raw tunnelendpoint
     *
     *  The acceptor send this signal as a response to call to RawTunnelendpointAcceptorTCP::aio_accept_start().
     */
    virtual void com_tunnelendpoint_accepted(const APIRawTunnelendpointTCP::APtr& tunnelendpoint) = 0;

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void com_tunnelendpoint_acceptor_error(const std::string& error_message) = 0;


    /*! \brief Connected acceptting new connections
     *
     * \return True indicate that the acceptor should continue accepting connections
     */
    virtual bool com_accept_continue(void) = 0;
};

/*! \brief API for RawTunnelendpointAcceptorTCP class see this class for documentation
 */
class APIRawTunnelendpointAcceptorTCP: public RawTunnelendpointAcceptorTCPEventhandler, public AsyncContinuePolicy {
public:
    typedef boost::shared_ptr<APIRawTunnelendpointAcceptorTCP> APtr;

    /*! \brief Destructor
     */
    ~APIRawTunnelendpointAcceptorTCP(void);

    /*! \brief set tcp option result_address
     */
    void set_option_reuse_address(const bool& option_reuse_address);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APIRawTunnelendpointAcceptorTCPEventhandler* api_eventhandler);

    /*! \brief reset eventhandler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief Start accepting asynchron connection
     */
    void aio_accept_start(void);

    /*! \brief Close the acceptor
     */
    void aio_close_start(void);

    /*! \brief Return true if the acceptor has been closed
     */
    bool is_closed(void) const;

    /*! \brief Get instance mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Get ip and port of the socket that the acceptor is listen on
     */
    std::pair<std::string, unsigned long> get_ip_local(void) const;

    /*! \brief Set instance mutex
     */
    void set_mutex(const APIMutex::APtr& api_mutex);

    /*! \brief Create instance
     */
    static APtr create(const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                       const APIAsyncService::APtr& async_service,
                       const std::string& listen_host,
                       const unsigned long& listen_port);

    /*! \brief Implementation of raw-eventhandlers
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& raw_tunnelendpoint);
    void com_tunnelendpoint_acceptor_error(const std::string& error_message);
    void com_tunnelendpoint_acceptor_closed(void);
    bool com_accept_continue(void);


    /*
     * Self methods for easy python-api defintion
     */
    static void self_set_option_reuse_address(const APtr& self, const bool& option_reuse_address);
    static void self_reset_tcp_eventhandler(const APtr& self);
    static void self_set_tcp_eventhandler(const APtr& self, APIRawTunnelendpointAcceptorTCPEventhandler* api_eventhandler);

    static void self_aio_accept_start(const APtr& self);
    static void self_aio_close_start(const APtr& self);
    static bool self_is_closed(const APtr& self);
    static void self_set_mutex(const APtr& self, const APIMutex::APtr& api_mutex);
    static APIMutex::APtr self_get_mutex(const APtr& self);
    static boost::python::tuple self_get_ip_local(const APtr& self);

private:

    APIRawTunnelendpointAcceptorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                    const APIAsyncService::APtr& async_service,
                                    const std::string& listen_host,
                                    const unsigned long& listen_port);

    RawTunnelendpointAcceptorTCP::APtr impl_tunnelendpoint_;
    APIRawTunnelendpointAcceptorTCPEventhandler* api_eventhandler_;
};

}
}
#endif
