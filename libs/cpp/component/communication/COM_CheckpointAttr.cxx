/*! \file COM_CheckpointAttr.cxx
 \brief This file contains the global singelton for the module attribute communication
 */

#include <component/communication//COM_CheckpointAttr.hxx>

using namespace Giritech;
using namespace Giritech::Utility;

static CheckpointAttr_Module::APtr componentConnection_;
static CheckpointAttr_Module::APtr componentCommunicationTC_;

CheckpointAttr_Module::APtr Giritech::Communication::Attr_Communication(void) {
    if (!componentConnection_) {
        componentConnection_ = CheckpointAttr_Module::create("Communication");
    }
    return componentConnection_;
}

CheckpointAttr_Module::APtr Giritech::Communication::Attr_CommunicationTC(void) {
    if (!componentCommunicationTC_) {
    	componentCommunicationTC_ = CheckpointAttr_Module::create("TC");
    }
    return componentCommunicationTC_;
}
