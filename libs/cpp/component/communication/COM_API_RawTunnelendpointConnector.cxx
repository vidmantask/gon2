/*! \file COM_API_RawTunnelendpointConnector.cxx
 *  \brief This file contains the implementation of the API wrapper for raw-tcp-tunnelendpoint connector
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <component/communication/COM_API_RawTunnelendpointConnector.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APIRawTunnelendpointConnectorTCP implementation
 * ------------------------------------------------------------------
 */
APIRawTunnelendpointConnectorTCP::APIRawTunnelendpointConnectorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                   const APIAsyncService::APtr& async_service,
                                                                   const std::string& remote_host,
                                                                   const unsigned long& remote_port) :
    api_eventhandler_(NULL),
            impl_tunnelendpoint_(RawTunnelendpointConnectorTCP::create(checkpoint_handler, async_service->get_io_service(), remote_host, remote_port,
                                                                       this)) {
}

APIRawTunnelendpointConnectorTCP::~APIRawTunnelendpointConnectorTCP(void) {
}

APIRawTunnelendpointConnectorTCP::APtr APIRawTunnelendpointConnectorTCP::create(const Utility::APICheckpointHandler::APtr& wrap_checkpoint_handler,
                                                                                const APIAsyncService::APtr& async_service,
                                                                                const std::string& remote_host,
                                                                                const unsigned long& remote_port) {
    return APIRawTunnelendpointConnectorTCP::APtr(
                                                  new APIRawTunnelendpointConnectorTCP(wrap_checkpoint_handler->get_checkpoint_handler(), async_service, remote_host, remote_port));
}

void APIRawTunnelendpointConnectorTCP::set_tcp_eventhandler(APIRawTunnelendpointConnectorTCPEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
}

void APIRawTunnelendpointConnectorTCP::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_eventhandler();
    api_eventhandler_ = NULL;
}

void APIRawTunnelendpointConnectorTCP::aio_connect_start(void) {
    impl_tunnelendpoint_->aio_connect_start();
}

APIMutex::APtr APIRawTunnelendpointConnectorTCP::get_mutex(void) {
    return APIMutex::create(impl_tunnelendpoint_->get_mutex());
}

void APIRawTunnelendpointConnectorTCP::set_mutex(const APIMutex::APtr& api_mutex) {
    impl_tunnelendpoint_->set_mutex(api_mutex->get_impl());
}

void APIRawTunnelendpointConnectorTCP::set_connect_timeout(const boost::posix_time::time_duration& connect_timeout) {
    impl_tunnelendpoint_->set_connect_timeout(connect_timeout);
}

void APIRawTunnelendpointConnectorTCP::close(void) {
    impl_tunnelendpoint_->close();
}

void APIRawTunnelendpointConnectorTCP::enable_ssl(const bool& disable_certificate_verification) {
    impl_tunnelendpoint_->enable_ssl(disable_certificate_verification);
}

void APIRawTunnelendpointConnectorTCP::self_reset_tcp_eventhandler(const APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

void APIRawTunnelendpointConnectorTCP::self_set_tcp_eventhandler(const APtr& self,
                                                                 APIRawTunnelendpointConnectorTCPEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APIRawTunnelendpointConnectorTCP::self_aio_connect_start(const APtr& self) {
    assert(self.get() != NULL);
    self->aio_connect_start();
}

APIMutex::APtr APIRawTunnelendpointConnectorTCP::self_get_mutex(const APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}

void APIRawTunnelendpointConnectorTCP::self_set_mutex(const APtr& self, const APIMutex::APtr& api_mutex) {
    assert(self.get() != NULL);
    self->set_mutex(api_mutex);
}

void APIRawTunnelendpointConnectorTCP::self_close(const APtr& self) {
    assert(self.get() != NULL);
    self->close();
}

void APIRawTunnelendpointConnectorTCP::self_enable_ssl(const APtr& self, const bool& disable_certificate_verification) {
    assert(self.get() != NULL);
    self->enable_ssl(disable_certificate_verification);
}

void APIRawTunnelendpointConnectorTCP::self_set_connect_timeout_sec(const APtr& self, unsigned int sec) {
    assert(self.get() != NULL);
    self->set_connect_timeout(boost::posix_time::seconds(sec));
}

void APIRawTunnelendpointConnectorTCP::com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& raw_tunnelendpoint) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_tunnelendpoint_connected(APIRawTunnelendpointTCP::create(raw_tunnelendpoint));
    }
}

void APIRawTunnelendpointConnectorTCP::com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_tunnelendpoint_connection_failed_to_connect(message);
    }
}
