/*! \file COM_TunnelEndpointEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from a tunnelendpoint
 */
#ifndef COM_TunnelendpointEventhandler_HXX
#define COM_TunnelendpointEventhandler_HXX

#include <component/communication/COM_Message.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler handling signals from a tunnelendpoint
 */
class TunnelendpointEventhandler : public boost::noncopyable {
public:

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void) = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) = 0;
};
}
}
#endif
