/*! \file COM_SessionTunnelEndpointEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from the session tunnelendpont
 */
#ifndef COM_SessionTunnelEndpointEventhandler_HXX
#define COM_SessionTunnelEndpointEventhandler_HXX

#include <component/communication/COM_Message.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for handling signals from the session tunnelendpoint
 */
class SessionTunnelendpointEventhandler : public boost::noncopyable {
public:

    /*! \brief Signals that data has been recived
     */
    virtual void session_tunnelendpoint_recieve(const MessageCom::APtr&) = 0;

    /*! \brief Ask if async read_start should continue
     */
    virtual bool session_tunnelendpoint_read_continue_state_ready(void) = 0;
};
}
}
#endif
