/* \brief This file contains the implementation for the TrafficControlManager classes
 */
#include <boost/asio.hpp>

#include <component/communication/COM_TrafficControlSession.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TrafficControlSessionPoint implementation
 * ------------------------------------------------------------------
 */
TrafficControlSessionPoint::TrafficControlSessionPoint(
		TrafficControlSessionCallBack* traffic_control_session_callback,
		const unsigned long& id,
		const ConnectionType& connection_type)
	: traffic_control_session_callback_(traffic_control_session_callback),
	  id_(id),
	  connection_type_(connection_type),
	  current_interval_read_count_(0),
	  current_interval_read_size_(0),
	  current_interval_write_size_(0),
	  current_interval_write_count_(0),
	  current_interval_buffer_transit_min_ms_(0),
	  current_interval_buffer_transit_max_ms_(0),
	  history_interval_read_count_(0),
	  history_interval_read_size_(0),
	  history_interval_write_size_(0),
	  history_interval_write_count_(0),
	  history_interval_buffer_size_(0),
	  history_interval_read_computation_delay_ms_(0),
	  history_interval_buffer_transit_min_ms_(0),
	  history_interval_buffer_transit_max_ms_(0),
	  future_read_delay_ms_(10),
	  future_read_bucket_size_(DEFAULT_READ_BUCKET_SIZE),
	  future_read_size_sec_(DEFAULT_READ_SIZE_SEC_DEFAULT),
	  future_read_bucket_count_(0),
	  current_buffer_size_total_(0) {
}

TrafficControlSessionPoint::~TrafficControlSessionPoint(void) {
}

TrafficControlSessionPoint::APtr TrafficControlSessionPoint::create(
		TrafficControlSessionCallBack* traffic_control_session_callback,
		const unsigned long& id,
		const ConnectionType& connection_type) {
	return APtr(new TrafficControlSessionPoint(traffic_control_session_callback, id, connection_type));
}

TrafficControlSessionPoint::ConnectionType TrafficControlSessionPoint::getConnectionType(void) const {
	return connection_type_;
}

void TrafficControlSessionPoint::report_read_begin(void) {
}

void TrafficControlSessionPoint::report_read_end(const unsigned long& size) {
	current_interval_read_count_ += 1;
	current_interval_read_size_ += size;
}


void TrafficControlSessionPoint::report_push_buffer_size(const unsigned long& size) {
	current_buffer_ts_.push_back(boost::posix_time::microsec_clock::local_time());
	current_buffer_size_.push_back(size);
	current_buffer_size_total_ += size;

	unsigned long current_buffer_transit_ms = (boost::posix_time::microsec_clock::local_time() - current_buffer_ts_.front()).total_milliseconds();

	if(current_buffer_transit_ms > BUFFER_TRANSIT_LIMIT_MS) {
		if(connection_type_ != ConnectionType_GConnection) {
			traffic_control_session_callback_->in_buffer_transit_exceded(id_);
		}
		else {
			traffic_control_session_callback_->out_buffer_transit_exceded(id_);
		}
	}
	else {
		if(connection_type_ != ConnectionType_GConnection) {
			traffic_control_session_callback_->in_buffer_transit_ok(id_);
		}
		else {
			traffic_control_session_callback_->out_buffer_transit_ok(id_);
		}
	}
	current_interval_buffer_transit_min_ms_ = min(current_buffer_transit_ms, current_interval_buffer_transit_min_ms_);
	current_interval_buffer_transit_max_ms_ = max(current_buffer_transit_ms, current_interval_buffer_transit_max_ms_);
}

void TrafficControlSessionPoint::report_pop_buffer_size(const unsigned long& size) {
	if(! current_buffer_ts_.empty()) {
		current_buffer_ts_.erase(current_buffer_ts_.begin());
	}
	if(! current_buffer_size_.empty()) {
		current_buffer_size_.erase(current_buffer_size_.begin());
	}
	current_buffer_size_total_ -= size;

	current_interval_write_count_ += 1;
	current_interval_write_size_ += size;
}


unsigned long TrafficControlSessionPoint::get_read_delay_ms(void) const {
	return future_read_delay_ms_;
}

unsigned long TrafficControlSessionPoint::get_read_size(void) const {
	return future_read_bucket_size_;
}

unsigned long TrafficControlSessionPoint::get_write_delay_ms(void) const {
	return 0;
}

void TrafficControlSessionPoint::interval_tick(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) {
	history_interval_read_count_ = current_interval_read_count_;
	history_interval_read_size_ = current_interval_read_size_;
	history_interval_write_size_ = current_interval_write_size_;
	history_interval_write_count_ = current_interval_write_count_;
	history_interval_buffer_size_ = current_buffer_size_total_;

	history_interval_read_computation_delay_ms_ = 0;

	history_interval_buffer_transit_min_ms_ = current_interval_buffer_transit_min_ms_;
	history_interval_buffer_transit_max_ms_ = current_interval_buffer_transit_max_ms_;

	current_interval_read_count_ = 0;
	current_interval_read_size_ = 0;
	current_interval_write_size_ = 0;
	current_interval_write_count_ = 0;

	current_interval_buffer_transit_min_ms_ = 0;
	current_interval_buffer_transit_max_ms_ = 0;
}

void TrafficControlSessionPoint::add_to_session(
		unsigned long& interval_in_read_size,
		unsigned long& interval_in_buffer_size,
		unsigned long& interval_in_write_size,
		unsigned long& interval_in_buffer_transit_min_ms,
		unsigned long& interval_in_buffer_transit_max_ms,
		unsigned long& interval_out_read_size,
		unsigned long& interval_out_buffer_size,
		unsigned long& interval_out_write_size,
		unsigned long& interval_out_buffer_transit_min_ms,
		unsigned long& interval_out_buffer_transit_max_ms
		) {

	if(connection_type_ != ConnectionType_GConnection) {
		interval_in_write_size += history_interval_write_size_;
		interval_in_buffer_size += history_interval_buffer_size_;
		interval_out_read_size += history_interval_read_size_;
		interval_in_buffer_transit_min_ms = min(interval_in_buffer_transit_min_ms, history_interval_buffer_transit_min_ms_);
		interval_in_buffer_transit_max_ms = max(interval_in_buffer_transit_max_ms, history_interval_buffer_transit_max_ms_);
	}
	else {
		interval_in_read_size += history_interval_read_size_;
		interval_out_buffer_size += history_interval_buffer_size_;
		interval_out_write_size += history_interval_write_size_;
		interval_out_buffer_transit_min_ms = min(interval_out_buffer_transit_min_ms, history_interval_buffer_transit_min_ms_);
		interval_out_buffer_transit_max_ms = max(interval_out_buffer_transit_max_ms, history_interval_buffer_transit_max_ms_);
	}
}

void TrafficControlSessionPoint::add_future_to_session(
		unsigned long& future_in_interval_read_size_sec,
		unsigned long& future_in_interval_read_bucket_size,
		unsigned long& future_in_interval_read_bucket_count,
		unsigned long& future_in_interval_read_delay_ms,
		unsigned long& future_out_interval_read_size_sec,
		unsigned long& future_out_interval_read_bucket_size,
		unsigned long& future_out_interval_read_bucket_count,
		unsigned long& future_out_interval_read_delay_ms
		) {

	if(connection_type_ != ConnectionType_GConnection) {
		future_out_interval_read_size_sec += future_read_size_sec_;
		future_out_interval_read_bucket_size += future_read_bucket_size_;
		future_out_interval_read_bucket_count += future_read_bucket_count_;
		future_out_interval_read_delay_ms += future_read_delay_ms_;
	}
	else {
		future_in_interval_read_size_sec += future_read_size_sec_;
		future_in_interval_read_bucket_size += future_read_bucket_size_;
		future_in_interval_read_bucket_count += future_read_bucket_count_;
		future_in_interval_read_delay_ms += future_read_delay_ms_;
	}
}

void TrafficControlSessionPoint::apply_future_restriction(const BandwidthThrottlingState bandwidth_throttling_state) {
	/*
	 * Calculate new bandwidth
	 */
	switch(bandwidth_throttling_state) {
	case BandwidthThrottlingState_Decreasing:
		future_read_size_sec_ *= 0.99;
		break;
	case BandwidthThrottlingState_Neutral:
		break;
	case BandwidthThrottlingState_Increasing:
		future_read_size_sec_ += 1000;
		break;
	}

	// Check for extrems
	if(future_read_size_sec_ < DEFAULT_READ_SIZE_SEC_MIN) {
		future_read_size_sec_ = DEFAULT_READ_SIZE_SEC_MIN;
	}

	if( future_read_size_sec_ > DEFAULT_READ_SIZE_SEC_MAX) {
		future_read_size_sec_ = DEFAULT_READ_SIZE_SEC_MAX;
	}
	calculate_buckets(future_read_size_sec_);
}

void TrafficControlSessionPoint::calculate_buckets(const unsigned long& future_read_size_sec) {
	future_read_delay_ms_ = DEFAULT_READ_DELAY_MS;
	future_read_bucket_count_ = DEFAULT_READ_BUCKETS_COUNT_SEC;
	future_read_bucket_size_ = future_read_size_sec / future_read_bucket_count_;
}


/*
 * ------------------------------------------------------------------
 * TrafficControlSession implementation
 * ------------------------------------------------------------------
 */
TrafficControlSession::TrafficControlSession(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler_details, const std::string& session_id) :
		asio_io_service_(asio_io_service),
		checkpoint_handler_details_(checkpoint_handler_details),
		mutex_(Mutex::create(asio_io_service, "TrafficControlSession")),
		next_id_(0),
		interval_timer_(asio_io_service),
		state_(State_Running),
		session_id_(session_id),
		traffic_control_session_eventhandler_(NULL),
		in_bandwidth_throttling_state_(BandwidthThrottlingState_Neutral),
		out_bandwidth_throttling_state_(BandwidthThrottlingState_Neutral),
		out_last_buffer_transit_exceded_(boost::posix_time::microsec_clock::local_time()),
		in_last_buffer_transit_exceded_(boost::posix_time::microsec_clock::local_time()),
		traffic_control_enabled_(true) {
}

TrafficControlSession::~TrafficControlSession(void) {
}

unsigned long TrafficControlSession::add_endpoint(const TrafficControlSessionPoint::ConnectionType& connection_type) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	unsigned long id = next_id_++;
    points_.insert(make_pair(id,TrafficControlSessionPoint::create(this, id, connection_type)));
    return id;
}

void TrafficControlSession::remove_endpoint(const unsigned long& id) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	points_.erase(id);
}

void TrafficControlSession::set_traffic_control_enabled(const bool value) {
	traffic_control_enabled_ = value;
}

unsigned long TrafficControlSession::get_read_delay_ms(const unsigned long& id) const {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(!traffic_control_enabled_) {
		return 0;
	}

	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		return i->second->get_read_delay_ms();
	}
	return 0;
}

unsigned long TrafficControlSession::get_read_size(const unsigned long& id) const {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(!traffic_control_enabled_) {
		return DEFAULT_READ_SIZE_MAX;
	}

	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		return i->second->get_read_size();
	}
	return DEFAULT_READ_BUCKET_SIZE;
}

unsigned long TrafficControlSession::get_write_delay_ms(const unsigned long& id) const {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	if(!traffic_control_enabled_) {
		return 0;
	}

	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		return i->second->get_write_delay_ms();
	}
	return 0;
}

void TrafficControlSession::report_read_begin(const unsigned long& id) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		i->second->report_read_begin();
	}
}

void TrafficControlSession::report_read_end(const unsigned long& id, const unsigned long& size) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		i->second->report_read_end(size);
	}
}

void TrafficControlSession::report_pop_buffer_size(const unsigned long& id, const unsigned long& size) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		i->second->report_pop_buffer_size(size);
	}
}

void TrafficControlSession::report_push_buffer_size(const unsigned long& id, const unsigned long& size) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	Points::const_iterator i(points_.find(id));
	if(i != points_.end()) {
		i->second->report_push_buffer_size(size);
	}
}

void TrafficControlSession::interval_tick_start(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	switch(state_) {
	case State_Running:
		break;
	default:
		return;
	}
	interval_timer_.expires_after(boost::chrono::seconds(1));
	interval_timer_.async_wait(boost::bind(&TrafficControlSession::interval_tick, this));
}

void TrafficControlSession::interval_tick(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
//
//	unsigned long interval_in_read_size(0);
//	unsigned long interval_in_buffer_size(0);
//	unsigned long interval_in_write_size(0);
//	unsigned long interval_in_buffer_transit_min_ms(0);
//	unsigned long interval_in_buffer_transit_max_ms(0);
//
//	unsigned long interval_out_read_size(0);
//	unsigned long interval_out_buffer_size(0);
//	unsigned long interval_out_write_size(0);
//	unsigned long interval_out_buffer_transit_min_ms(0);
//	unsigned long interval_out_buffer_transit_max_ms(0);
//
//	Points::const_iterator i(points_.begin());
//	while(i != points_.end()) {
//		i->second->interval_tick(checkpoint_handler_details_);
//		i->second->add_to_session(
//				interval_in_read_size,
//				interval_in_buffer_size,
//				interval_in_write_size,
//				interval_in_buffer_transit_min_ms,
//				interval_in_buffer_transit_max_ms,
//				interval_out_read_size,
//				interval_out_buffer_size,
//				interval_out_write_size,
//				interval_out_buffer_transit_min_ms,
//				interval_out_buffer_transit_max_ms
//		);
//		++i;
//	}

	if (state_ == State_Running) {
		interval_tick_start();
	}
	else if (state_ == State_Stopping) {
		state_ = State_Done;
	}
//
//	Checkpoint cp1(*checkpoint_handler_details_,
//			"tick.interval_in",
//			Attr_CommunicationTC(),
//			CpAttr_debug(),
//			CpAttr("session_id", session_id_),
//			CpAttr("read_size", interval_in_read_size),
//			CpAttr("buffer_size", interval_in_buffer_size),
//			CpAttr("write_size", interval_in_write_size),
//			CpAttr("buffer_transit_min_ms", interval_in_buffer_transit_min_ms),
//			CpAttr("buffer_transit_max_ms", interval_in_buffer_transit_max_ms)
//			);
//
//	Checkpoint cp2(*checkpoint_handler_details_,
//			"tick.interval_out",
//			Attr_CommunicationTC(),
//			CpAttr_debug(),
//			CpAttr("session_id", session_id_),
//			CpAttr("read_size", interval_out_read_size),
//			CpAttr("buffer_size", interval_out_buffer_size),
//			CpAttr("write_size", interval_out_write_size),
//			CpAttr("buffer_transit_min_ms", interval_out_buffer_transit_min_ms),
//			CpAttr("buffer_transit_max_ms", interval_out_buffer_transit_max_ms)
//			);
//
//	Checkpoint cp3(*checkpoint_handler_details_,
//			"tick.interval_state",
//			Attr_CommunicationTC(),
//			CpAttr_debug(),
//			CpAttr("session_id", session_id_),
//			CpAttr("in_bandwidth_throttling_state", in_bandwidth_throttling_state_),
//			CpAttr("out_bandwidth_throttling_state", out_bandwidth_throttling_state_)
//			);
//
//
//	unsigned long future_in_interval_read_size_sec(0);
//	unsigned long future_in_interval_read_bucket_size(0);
//	unsigned long future_in_interval_read_bucket_count(0);
//	unsigned long future_in_interval_read_delay_ms(0);
//	unsigned long future_out_interval_read_size_sec(0);
//	unsigned long future_out_interval_read_bucket_size(0);
//	unsigned long future_out_interval_read_bucket_count(0);
//	unsigned long future_out_interval_read_delay_ms(0);
//
//	i = points_.begin();
//	while(i != points_.end()) {
//		i->second->add_future_to_session(
//				future_in_interval_read_size_sec,
//				future_in_interval_read_bucket_size,
//				future_in_interval_read_bucket_count,
//				future_in_interval_read_delay_ms,
//				future_out_interval_read_size_sec,
//				future_out_interval_read_bucket_size,
//				future_out_interval_read_bucket_count,
//				future_out_interval_read_delay_ms
//				);
//		++i;
//	}
//	Checkpoint cp4(*checkpoint_handler_details_,
//			"tick.interval_future",
//			Attr_CommunicationTC(),
//			CpAttr_debug(),
//			CpAttr("session_id", session_id_),
//			CpAttr("in_interval_read_size_sec", future_in_interval_read_size_sec),
//			CpAttr("out_interval_read_size_sec", future_out_interval_read_size_sec)
//			);
//
//	Checkpoint cp5(*checkpoint_handler_details_,
//			"tick.interval_future_bucket",
//			Attr_CommunicationTC(),
//			CpAttr_debug(),
//			CpAttr("session_id", session_id_),
//			CpAttr("in_interval_read_bucket_size", future_in_interval_read_bucket_size),
//			CpAttr("in_interval_read_bucket_count", future_in_interval_read_bucket_count),
//			CpAttr("in_interval_read_delay_ms", future_in_interval_read_delay_ms),
//			CpAttr("out_interval_read_bucket_size", future_out_interval_read_bucket_size),
//			CpAttr("out_interval_read_bucket_count", future_out_interval_read_bucket_count),
//			CpAttr("out_interval_read_delay_ms", future_out_interval_read_delay_ms)
//			);
}

void TrafficControlSession::interval_tick_stop(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	switch(state_) {
	case State_Running:
		break;
	default:
		return;
	}
	state_ = State_Stopping;
}

bool TrafficControlSession::is_active(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	return points_.size() > 0;
}

bool TrafficControlSession::async_mem_guard_is_done(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	return state_ == State_Done;
}

std::string TrafficControlSession::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void TrafficControlSession::set_eventhandler(TrafficControlSessionEventhandler* eventhandler) {
	traffic_control_session_eventhandler_ = eventhandler;
}

void TrafficControlSession::reset_eventhandler(void) {
	traffic_control_session_eventhandler_ = NULL;
}

void TrafficControlSession::recieve_message(const MessageCom::APtr& com_message) {
}

TrafficControlSession::APtr TrafficControlSession::create(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, const std::string& session_id) {
	APtr result(new TrafficControlSession(asio_io_service, checkpoint_handler, session_id));
	AsyncMemGuard::global_add_element(result);
	return result;
}

bool TrafficControlSession::is_connected(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	return true;
}


void TrafficControlSession::in_buffer_transit_ok(const unsigned long& id) {
	boost::posix_time::time_duration buffer_transit_ok = boost::posix_time::microsec_clock::local_time() - in_last_buffer_transit_exceded_;
	if(buffer_transit_ok.total_milliseconds() > BUFFER_TRANSIT_OK_LIMIT_MS) {
		in_bandwidth_throttling_state_ = BandwidthThrottlingState_Increasing;
		in_apply_future_restriction();
	}
}

void TrafficControlSession::in_buffer_transit_exceded(const unsigned long& id) {
	in_last_buffer_transit_exceded_ = boost::posix_time::microsec_clock::local_time();
	in_bandwidth_throttling_state_ = BandwidthThrottlingState_Decreasing;
	in_apply_future_restriction();
}

void TrafficControlSession::out_buffer_transit_ok(const unsigned long& id) {
	boost::posix_time::time_duration buffer_transit_ok = boost::posix_time::microsec_clock::local_time() - out_last_buffer_transit_exceded_;
	if(buffer_transit_ok.total_milliseconds() > BUFFER_TRANSIT_OK_LIMIT_MS) {
		out_bandwidth_throttling_state_ = BandwidthThrottlingState_Increasing;
		out_apply_future_restriction();
	}
}

void TrafficControlSession::out_buffer_transit_exceded(const unsigned long& id) {
	out_last_buffer_transit_exceded_ = boost::posix_time::microsec_clock::local_time();
	out_bandwidth_throttling_state_ = BandwidthThrottlingState_Decreasing;
	out_apply_future_restriction();
}

void TrafficControlSession::in_apply_future_restriction(void) {

	for (const auto& pair : points_) {
		if (pair.second->getConnectionType() == TrafficControlSessionPoint::ConnectionType_GConnection) {
			pair.second->apply_future_restriction(in_bandwidth_throttling_state_);
		}
	}
}

void TrafficControlSession::out_apply_future_restriction(void) {

	for (const auto& pair : points_) {
		if (pair.second->getConnectionType() != TrafficControlSessionPoint::ConnectionType_GConnection) {
			pair.second->apply_future_restriction(out_bandwidth_throttling_state_);
		}
	}
}
