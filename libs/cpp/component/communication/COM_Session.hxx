/*! \file COM_Session.hxx
 *  \brief This file contains the session class
 */
#ifndef COM_Session_HXX
#define COM_Session_HXX

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_SessionMessage.hxx>
#include <component/communication/COM_SessionEventhandler.hxx>
#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_SessionTunnelendpoint.hxx>
#include <component/communication/COM_SessionTunnelendpointEventhandler.hxx>
#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_TunnelendpointConnector.hxx>
#include <component/communication/COM_StreamToMessage.hxx>
#include <component/communication/COM_TrafficControlSessionEventhandler.hxx>


namespace Giritech {
namespace Communication {

/*! \brief Connect the session-tunnelendpoints with tunnelelendpoints
 *
 * A session connect the session-tunnelendpoints with the dynamic created tunnelelendpoints.
 *
 * The following drawing shows the state-diagram of the session:
 \dot
 digraph diagram_session_state {
 rankdir=TB;
 ranksep=0.5;
 center=true;
 size="12,8.5";
 edge [minlen=2, fontsize=8, fontname="Helvetica" ];
 node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
 fontname="Helvetica";

 State_KeyExchange
 State_Initalizing
 State_Ready
 State_Closing
 State_Closed
 State_Error_01    [label="State_Error"];
 State_Error_02    [label="State_Error"];
 State_Error_03    [label="State_Error"];
 State_Error_04    [label="State_Error"];

 Start             -> State_Initalizing   [label="constructor"];

 State_Initalizing -> State_Closing   [label="close"];
 State_KeyExchange -> State_Closing   [label="close"];
 State_Ready       -> State_Closing   [label="close"];

 State_Initalizing -> State_KeyExchange [label="do_key_exchange"];
 State_Ready       -> State_KeyExchange [label="do_key_exchange"];
 State_Initalizing -> State_Error_01    [label="do_key_exchange\non ExceptionCallback", style=dotted];
 State_Ready       -> State_Error_02    [label="do_key_exchange\non ExceptionCallback", style=dotted];
 State_KeyExchange -> State_Error_03    [label="do_key_exchange\non ExceptionCallback", style=dotted];

 State_KeyExchange -> State_Ready    [label="session_crypter_key_exchange_complete"];
 State_KeyExchange -> State_Error_03 [label="session_crypter_key_exchange_complete\non ExceptionCallback", style=dotted];

 State_Initalizing -> State_Closed   [label="session_crypter_closed"];
 State_KeyExchange -> State_Closed   [label="session_crypter_closed"];
 State_Closing     -> State_Closed   [label="session_crypter_closed"];
 State_Ready       -> State_Closed   [label="session_crypter_closed"];
 State_Initalizing -> State_Error_01 [label="session_crypter_closed\non ExceptionCallback", style=dotted];
 State_KeyExchange -> State_Error_03 [label="session_crypter_closed\non ExceptionCallback", style=dotted];
 State_Closing     -> State_Error_04 [label="session_crypter_closed\non ExceptionCallback", style=dotted];
 State_Ready       -> State_Error_02 [label="session_crypter_closed\non ExceptionCallback", style=dotted];

 State_Ready       -> State_Error_02 [label="read_start_state_ready\non ExceptionCallback", style=dotted];
 State_Ready       -> State_Error_02 [label="session_tunnelendpoint_read_continue_state_ready\non ExceptionCallback", style=dotted];
 }
 \enddot
 *
 */
class Session:
	public SessionCrypterEventhandler,
	public SessionTunnelendpointEventhandler,
	public TunnelendpointConnector,
	public TunnelendpointEventhandler,
	public TrafficControlSessionEventhandler,
	public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<Session> APtr;

    enum State {
        State_Unknown = 0,
        State_KeyExchange,
        State_Initalizing,
        State_ReadySessionData,
        State_Ready,
        State_Closing,
        State_Closed,
        State_Done,
        State_Error
    };

    /*! \brief Destructor
     */
    ~Session(void);

    /*! \brief Get session mutex
     */
    Utility::Mutex::APtr get_mutex(void);

    /*! \brief Close session
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
     */
    void close(const bool force);

    /*! \brief Set eventhandler
     */
    void set_eventhandler(SessionEventhandler*, const bool& move_session_crypter_eventhandler = false);

    /*! \brief Reset eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Set eventhandler
     */
    void set_eventhandler_ready(SessionEventhandlerReady*);

    /*! \brief Set keep alive ping interval
     */
    void set_keep_alive_ping_interval(const boost::posix_time::time_duration& keep_alive_ping_interval);

    /*! \brief Set traffic control
     */
    void set_traffic_control_enabled(const bool traffic_control_enabled);

    /*! \brief Reset eventhandler
     */
    void reset_eventhandler_ready(void);

    /*! \brief Start key exchange
     */
    void do_key_exchange(void);
    void do_key_exchange_decoubled(void);

    /*! \brief Add tunnelendpoint
     */
    void add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint);

    /*! \brief Signal from SessionCrypterEventhandler
     */
    void session_crypter_key_exchange_complete(void);

    /*! \brief Signal that the key-exchange failed
     */
    void session_crypter_key_exchange_failed(const std::string& error_message);

    /*! \brief Signal SessionCrypterEventhandler
     */
    void session_crypter_closed(void);

    /*! \brief Signals from SessionTunnelendpointEventhandler
     */
    void session_tunnelendpoint_recieve(const MessageCom::APtr&);

    /*! \brief Ask if async read_start should continue
     */
    bool session_tunnelendpoint_read_continue_state_ready(void);

    /*! \brief Signal from TunnelendpointConnector
     */
    void tunnelendpoint_connector_send(const MessageCom::APtr& message);

    /*! \brief Notify that the tunnelendpoint has been closed
     */
    void tunnelendpoint_connector_closed(void);

    /*! \brief User signal notification to parent from child
     */
    void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message);

    /*! \brief Ask for unique session id
     */
    std::string tunnelendpoint_connector_get_unique_session_id(void);

    /*! \brief Signal from TunnelendpointEventhandle
     */
    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Start read in ready state
     */
    void read_start_state_ready(void);

    /*! \brief Report back if the session has been idle for the idle_timeout period
     */
    bool is_idle(const boost::posix_time::time_duration& idle_timeout);

    /*! \brief Get last activity ts
     */
    boost::posix_time::ptime get_last_activity(void);

    /*! \brief Report back if the session has been closed
     */
    bool is_closed(void);
    bool is_connected(void);

    /*! \brief Return session id
     */
    unsigned long get_session_id(void);

    /*! \brief Return the unique session id
     */
    std::string get_unique_session_id(void);

    /*! \brief Return the remote unique session id
     */
    std::string get_remote_unique_session_id(void);

    /*! \brief Return the if remote session logging has been enabled
     */
    bool get_remote_session_logging_enabled(void);

    /*! \brief Return remote ip and port
     */
    std::pair<std::string, unsigned long> get_ip_remote(void);

    /*! \brief Return local ip and port
     */
    std::pair<std::string, unsigned long> get_ip_local(void);

    /*! \brief Return asio io service
     */
    boost::asio::io_service& get_io_service(void);

    /*! \brief Return raw tunnelendpoint used by session
     */
    RawTunnelendpointTCP::APtr get_raw_tunnelendpoint(void);

    /*! \brief Set/Get application protocol type
     *
     * Send as part of the SessionMessage just after key-exchange
     */
    void set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type);
    SessionMessage::ApplProtocolType get_appl_protocol_type(void);

    /*
     * Returns true if the keyexchange phase_one is ongoing
     */
    bool doing_keyexchange(void);
    bool doing_keyexchange_phase_one(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);



    /*! \brief Implementation of TrafficControlSessionEventhandler methods
     */
    virtual void traffic_controll_session_send_message(const MessageCom::APtr&);



    /*! \brief Create instance
     */
    static APtr create_client(boost::asio::io_service& asio_io_service,
                              const unsigned long& session_id,
                              const std::string& sid,
                              const bool& session_logging_enabled,
                              const Utility::CheckpointHandler::APtr& checkpoint_handler,
                              const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                              const Utility::DataBufferManaged::APtr& known_secret,
                              const SessionMessage::ApplProtocolType appl_protocol_type);

    static APtr create_client(boost::asio::io_service& asio_io_service,
                              const unsigned long& session_id,
                              const std::string& sid,
                              const bool& session_logging_enabled,
                              const Utility::CheckpointHandler::APtr& checkpoint_handler,
                              const SessionCrypter::APtr& session_crypter,
                              SessionEventhandler* eventhandler,
                              const SessionMessage::ApplProtocolType appl_protocol_type);

    static APtr create_server(boost::asio::io_service& asio_io_service,
                              const unsigned long& session_id,
                              const std::string& sid,
                              const bool& session_logging_enabled,
                              const Utility::CheckpointHandler::APtr& checkpoint_handler,
                              const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                              const Utility::DataBufferManaged::APtr& known_secret);


protected:

    enum SessionType {
    	SessionTypeClient = 0,
    	SessionTypeServer
    };

    /*! \brief Constructor
     */
    Session(boost::asio::io_service& asio_io_service,
            const unsigned long& session_id,
            const std::string& sid,
            const bool& session_logging_enabled,
            const Utility::CheckpointHandler::APtr& checkpoint_handler,
            const SessionCrypter::APtr& session_crypter,
            SessionEventhandler* eventhandler,
            const SessionMessage::ApplProtocolType appl_protocol_type,
            const SessionType& session_type);


    boost::asio::io_service& asio_io_service_;
    unsigned long session_id_;
    std::string sid_;

    std::string session_id_unique_;
    std::string remote_unique_session_id_;

    void registre_activity(void);
    std::string calculate_session_id_unique(void);

    void close_finale(void);

    void asio_keep_alive_ping_start(void);
    void asio_keep_alive_ping_stop(void);
    void asio_keep_alive_ping(const boost::system::error_code& error);

    void enter_error_mode(void);

    void handle_remote_ping_start(const MessagePayload::APtr& payload);
    void handle_remote_ping_stop(void);

    SessionEventhandler* eventhandler_;
    SessionEventhandlerReady* eventhandler_ready_;
    std::atomic<State> state_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    SessionCrypter::APtr session_crypter_;
    SessionTunnelendpoint::APtr session_tunnel_;
    TunnelendpointReliableCryptedTunnel::APtr tunnel_;
    std::atomic<boost::posix_time::ptime> last_activity_;

    bool session_logging_enabled_;
    std::atomic<bool> remote_session_logging_enabled_;

    SessionMessage::ApplProtocolType appl_protocol_type_;
    SessionMessage::ApplProtocolType remote_appl_protocol_type_;

    std::atomic<bool> asio_keep_alive_ping_running_;
    boost::posix_time::time_duration keep_alive_ping_interval_;
    boost::asio::steady_timer keep_alive_ping_timer_;

    Utility::Mutex::APtr session_mutex_;

    TrafficControlSession::APtr traffic_control_session_;

    SessionType session_type_;

    bool traffic_control_enabled_;
};

}
}
#endif
