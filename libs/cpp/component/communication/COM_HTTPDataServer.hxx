/*! \file COM_HTTPDataServer.hxx
 *
 * \brief This file contains the http data server functinality
 *
 * Given data and content type the http data server acts as a http server until the data has been read from a specific url.
 *
 */
#ifndef COM_HTTPDataServer_HXX
#define COM_HTTPDataServer_HXX

#include <boost/shared_ptr.hpp>


#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <lib/http_data_server/server.hpp>


#include <component/communication/COM_AsyncService.hxx>


namespace Giritech {
namespace Communication {


/*! \brief http data server
 *
 */
class COMHTTPDataServer : public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<COMHTTPDataServer> APtr;

    /*
     * Destructor
     */
    ~COMHTTPDataServer(void);

    /*
     * Get url where data is served
     */
    std::string get_url(void);

    /*
     * Start server
     */
    void start(void);

    /*
     * Terminate server
     */
    void terminate(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*
     * Create instance
     */
    static APtr create(
    		const AsyncService::APtr& async_service,
            const Utility::CheckpointHandler::APtr& checkpoint_handler,
            const std::string& data,
            const std::string& data_content_type,
            const std::string& url_filename,
            const boost::posix_time::time_duration& timeout,
            const std::string& listen_host,
            const unsigned long listen_port);

private:
    /*
     * Constructor
     */
    COMHTTPDataServer(
    		const AsyncService::APtr& async_service,
            const Utility::CheckpointHandler::APtr& checkpoint_handler,
            const std::string& data,
            const std::string& data_content_type,
            const std::string& url_filename,
            const boost::posix_time::time_duration& timeout,
            const std::string& listen_host,
            const unsigned long listen_port);

    AsyncService::APtr async_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    boost::posix_time::time_duration timeout_;

    Giritech::HTTPDataServer::server::APtr httpDataServer_;
    boost::thread_group httpDataServerThreads_;

};

}
}
#endif
