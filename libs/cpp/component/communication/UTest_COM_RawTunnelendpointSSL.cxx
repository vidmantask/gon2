/*! \file UTest_COM_RawTunnelendpointSSL.cxx
 * \brief This file contains unittest suite for the raw tunnelendpoint functionality
 */

#include <string>

#include <boost/test/unit_test.hpp>
#include <lib/unittest/UT_Tools.hxx>


#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_RawTunnelendpointSSL.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

using namespace std;
using namespace Giritech::Unittest;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*!
 *  Demo raw client
 */
class UTest_COM_RawTunnelendpoint_Client :
    public RawTunnelendpointConnectorTCPEventhandler,
    public RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_RawTunnelendpoint_Client> APtr;
    typedef boost::weak_ptr<UTest_COM_RawTunnelendpoint_Client> WAPtr;


    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        cout << "CONNECTED ----------------------------------" << endl;
        connected_ = true;
        tunnelendpoint_ = tunnelendpoint;
        tunnelendpoint_->set_eventhandler(this);
        tunnelendpoint_->aio_ssl_start("www.google.dk", false);
        tunnelendpoint_->aio_read_start();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& messag) {
        connected_failed_ = true;
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
        closed_ = true;
    }

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     */
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
        tunnelendpoint_->aio_close_start(false);
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const DataBuffer::APtr& data) {
        cout << "----------------------------------" << endl;
    	cout << data->toString() << endl;
        cout << "----------------------------------" << endl;
    	data_recived_ += data->getSize();
    }

    /*! \brief Signals that the write data buffer is empty
     */
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
        // Ignore
    }

    void connect(void) {
        connector_->aio_connect_start();
    }
    void close(void) {
        connector_->close();
    }

    void set_this_weak(const WAPtr& this_weak) {
    	this_weak_ = this_weak;
    }

    static APtr create(
    		const CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		const std::string& host,
    		const unsigned long port) {
    	APtr instance(new UTest_COM_RawTunnelendpoint_Client(checkpoint_handler, io, host, port));
    	instance->set_this_weak(instance);
    	return instance;
    }

    bool connected(void) {
    	return connected_;
    }

    bool connected_failed(void) {
    	return connected_failed_;
    }

    bool closed(void) {
    	cout << "client closed:"  << closed_ << endl;
    	return closed_;
    }

    bool data_recived(void) {
    	return data_recived_ > 0;
    }

    RawTunnelendpointTCP::APtr tunnelendpoint_;

private:
    UTest_COM_RawTunnelendpoint_Client(
    		const CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		const std::string& host,
    		const unsigned long port) :
        connected_(false),
        connected_failed_(false),
        closed_(false),
        data_recived_(0) {
        connector_ = RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, host, port, this);
        connector_->set_connect_timeout(boost::posix_time::seconds(5));
//        connector_->enable_ssl();
    }

    RawTunnelendpointConnectorTCP::APtr connector_;
    bool connected_failed_;
    bool connected_;
    bool closed_;
    long data_recived_;

    WAPtr this_weak_;
};

/* Create checkpoint handler */

CheckpointHandler::APtr checkpoint_handler_ignore(CheckpointHandler::create_ignore());
CheckpointHandler::APtr checkpoint_handler_all(CheckpointHandler::create_cout_all());


BOOST_AUTO_TEST_CASE( rawtunnelendpoint_connect )
{
    boost::asio::io_service ios;

//    AsyncMemGuard::create_global_guard(ios);

    std::string host("www.google.dk");
    unsigned long port = 443;

    RawTunnelendpointTCPSSLContext::get_instance(ios, "", 0)->init(checkpoint_handler_all, "",  false, false, false);
    UTest_COM_RawTunnelendpoint_Client::APtr raw_client(UTest_COM_RawTunnelendpoint_Client::create(
    		checkpoint_handler_all,
    		ios,
    		host,
    		port));

    raw_client->connect();
    boost::thread_group threads;
    threads.create_thread(boost::bind(&boost::asio::io_service::run, &ios));

//    boost::this_thread::sleep(boost::posix_time::seconds(10));
//    raw_client->close();
//    raw_client.reset();
//    AsyncMemGuard::global_cleanup();
//    boost::this_thread::sleep(boost::posix_time::seconds(20));

    wait_for(boost::bind(&UTest_COM_RawTunnelendpoint_Client::connected, raw_client));
    BOOST_CHECK( raw_client->connected() );
//    boost::this_thread::sleep(boost::posix_time::seconds(2));

    stringstream ss;
	ss << "GET / HTTP/1.1\r\n";
	ss << "Host: www.google.dk\r\n";
	ss << "Accept-Encoding: *\r\n";
	ss << "\r\n";

	raw_client->tunnelendpoint_->aio_read_start();
	raw_client->tunnelendpoint_->aio_write_start(DataBufferManaged::create(ss.str()));

    wait_for(boost::bind(&UTest_COM_RawTunnelendpoint_Client::data_recived, raw_client));
    BOOST_CHECK( raw_client->data_recived() );

    ios.stop();
    threads.join_all();
}


boost::unit_test::test_suite* init_unit_test_suite(int argc, char* argv[]) {
    return 0;
}
