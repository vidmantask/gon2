/*! \file UTest_COM_RawTunnelendpoint.cxx
 * \brief This file contains unittest suite for the raw tunnelendpoint functionality
 */

#include <string>

#include <boost/test/unit_test.hpp>

#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*!
 *  Demo raw server
 */
class UTest_COM_RawTunnelendpoint_Server :
    public RawTunnelendpointAcceptorTCPEventhandler,
    public AsyncContinuePolicy, public RawTunnelendpointEventhandler {
public:
    UTest_COM_RawTunnelendpoint_Server(const CheckpointHandler::APtr& checkpoint_handler,
                                       boost::asio::io_service& io,
                                       const std::string& host,
                                       const unsigned long port) :
        accepted_(false), closed_(false), data_recived_(0) {
        acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler,
                                                         io,
                                                         host,
                                                         port,
                                                          this,
                                                          this);
        acceptor_->set_option_reuse_address(true);
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        accepted_ = true;
        tunnelendpoint_ = tunnelendpoint;
        tunnelendpoint_->set_eventhandler(this);
        tunnelendpoint_->aio_read_start();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    }

    void com_tunnelendpoint_acceptor_closed(void) {
    }

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void) {
        return false;
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
        closed_ = true;
    }

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     */
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
        tunnelendpoint_->aio_close_start(false);
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const DataBuffer::APtr& data) {
        data_recived_ += data->getSize();
        tunnelendpoint_->aio_write_start(DataBufferManaged::create("Pong"));
    }

    /*! \brief Signals that the write data buffer is empty
     */
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
        // Ignore
    }

    void listen(void) {
        acceptor_->aio_accept_start();
    }

    bool accepted_;
    bool closed_;
    long data_recived_;
    RawTunnelendpointTCP::APtr tunnelendpoint_;

private:
    RawTunnelendpointAcceptorTCP::APtr acceptor_;
};

/*!
 *  Demo raw client
 */
class UTest_COM_RawTunnelendpoint_Client :
    public RawTunnelendpointConnectorTCPEventhandler,
    public RawTunnelendpointEventhandler {
public:
    UTest_COM_RawTunnelendpoint_Client(const CheckpointHandler::APtr& checkpoint_handler,
                                       boost::asio::io_service& io,
                                       const std::string& host,
                                       const unsigned long port) :
        connected_(false), connected_failed_(false), closed_(false), data_recived_(0) {
        connector_ = RawTunnelendpointConnectorTCP::create(checkpoint_handler,
                                                           io,
                                                           host,
                                                           port,
                                                           this);
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        connected_ = true;
        tunnelendpoint_ = tunnelendpoint;
        tunnelendpoint_->set_eventhandler(this);
        tunnelendpoint_->aio_read_start();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
        connected_failed_ = true;
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
        closed_ = true;
    }

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     */
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
        tunnelendpoint_->aio_close_start(false);
    }

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const DataBuffer::APtr& data) {
        data_recived_ += data->getSize();
    }

    /*! \brief Signals that the write data buffer is empty
     */
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
        // Ignore
    }

    void connect(void) {
        connector_->aio_connect_start();
    }

    bool connected_failed_;
    bool connected_;
    bool closed_;
    long data_recived_;
    RawTunnelendpointTCP::APtr tunnelendpoint_;

private:
    RawTunnelendpointConnectorTCP::APtr connector_;
};

/* Create checkpoint handler */

CheckpointHandler::APtr checkpoint_handler_ignore(CheckpointHandler::create_ignore());
CheckpointHandler::APtr checkpoint_handler_all(CheckpointHandler::create_cout_all());



/*!
 * \test UTest_COM_RawTunnelendpoint: Simpel test of listen and connect
 */
 /*
BOOST_AUTO_TEST_CASE( rawtunnelendpoint_listen_connect )
{
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Server raw_server(checkpoint_handler_all, ios, host, port);
    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_server.listen();
    raw_client.connect();
    ios.run();

    BOOST_CHECK( raw_server.accepted_ );
    BOOST_CHECK( raw_client.connected_ );
}
*/
/*!
 * \test UTest_COM_RawTunnelendpoint: Test the error situation wher a client connect to a port where noone is listing
 */
BOOST_AUTO_TEST_CASE( rawtunnelendpoint_connect_no_listen )
{
    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_client.connect();
    ios.run();

    BOOST_CHECK( !raw_client.connected_ );
}

/*!
 * \test UTest_COM_RawTunnelendpoint: Simpel test close initiated from server
 */
BOOST_AUTO_TEST_CASE( rawtunnelendpoint_server_close )
{
    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Server raw_server(checkpoint_handler_all, ios, host, port);
    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_server.listen();
    raw_client.connect();
    ios.run();

    BOOST_CHECK( raw_server.accepted_ );
    BOOST_CHECK( raw_client.connected_ );

    raw_server.tunnelendpoint_->aio_read_start();
    raw_client.tunnelendpoint_->aio_read_start();

    raw_server.tunnelendpoint_->aio_close_start(false);
    ios.reset();
    ios.run();

    BOOST_CHECK( raw_server.closed_ );
    BOOST_CHECK( raw_client.closed_ );
}

/*!
 * \test UTest_COM_RawTunnelendpoint: Simpel ping-pong test
 */
BOOST_AUTO_TEST_CASE( rawtunnelendpoint_ping_pong )
{
    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Server raw_server(checkpoint_handler_all, ios, host, port);
    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_server.listen();
    raw_client.connect();
    ios.run();

    raw_client.tunnelendpoint_->aio_read_start();
    raw_server.tunnelendpoint_->aio_read_start();
    raw_client.tunnelendpoint_->aio_write_start(DataBufferManaged::create("Pingo"));
    ios.reset();
    ios.run();

    BOOST_CHECK( raw_server.accepted_ );
    BOOST_CHECK( raw_client.connected_ );
    BOOST_CHECK( raw_server.data_recived_ == 5 );
    BOOST_CHECK( raw_client.data_recived_ == 4 );
}


/*!
 * \test UTest_COM_RawTunnelendpoint: Conneting to local-loopback using known port
 */
BOOST_AUTO_TEST_CASE( rawtunnelendpoint_localhost )
{
    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Server raw_server(checkpoint_handler_all, ios, host, port);
    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_server.listen();
    raw_client.connect();
    ios.run();

    BOOST_CHECK( raw_server.accepted_ );
    BOOST_CHECK( raw_client.connected_ );
}


/*!
 * \test UTest_COM_RawTunnelendpoint: Trying to connect to unknown server
 */
BOOST_AUTO_TEST_CASE( unknown_server_when_connecting )
{
    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("this.server.is.unknown.com");
    unsigned long port = 8045;

    UTest_COM_RawTunnelendpoint_Client raw_client(checkpoint_handler_all, ios, host, port);

    raw_client.connect();
    ios.run();

    BOOST_CHECK( raw_client.connected_failed_ );
}


boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
    return 0;
}
