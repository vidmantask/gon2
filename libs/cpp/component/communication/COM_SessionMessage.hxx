/*! \file COM_SessionMessage.hxx
 *  \brief This file contains the session message class
 */
#ifndef COM_SessionMessage_HXX
#define COM_SessionMessage_HXX

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Message.hxx>


namespace Giritech {
namespace Communication {


class SessionMessage: public boost::noncopyable {
public:
	typedef boost::shared_ptr<SessionMessage> APtr;

	enum ApplProtocolType {
    	ApplProtocolType_python = 0,
    	ApplProtocolType_xml = 1
    };

    ~SessionMessage(void);

    MessageCom::APtr build_message_com(void) const;

    std::string get_unique_session_id_(void) const;
    bool get_session_logging_enabled(void) const;
    ApplProtocolType get_appl_protocol_type(void) const;
    bool get_traffic_control_enabled(void) const;

    static APtr create(const std::string& unique_session_id, const bool& session_logging_enabled, const ApplProtocolType appl_protocol_type, const bool& traffic_control_enabled);
    static APtr create(const MessageCom::APtr& message);

private:
    SessionMessage(const std::string& unique_session_id, const bool& session_logging_enabled, const ApplProtocolType appl_protocol_type, const bool& traffic_control_enabled);

    std::string unique_session_id_;
    bool session_logging_enabled_;
    ApplProtocolType appl_protocol_type_;
    bool traffic_control_enabled_;

	static const unsigned long SESSION_DATA_PROTOCOL_VERSION_OFFEST = 0;
	static const unsigned long SESSION_DATA_APPL_PROTOCOL_TYPE_OFFEST = 1;
	static const unsigned long SESSION_DATA_TRAFFIC_CONTROL_ENABLED_OFFEST = 2;

	static const unsigned long SESSION_DATA_PROTOCOL_VERSION_CURRENT = 1;
};


}
}
#endif
