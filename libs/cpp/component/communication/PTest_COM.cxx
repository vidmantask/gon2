/*! \file PTest_COM.cxx
 * \brief This file contains performance test of the communication component.
 */
#include <string>

#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;

enum State {State_init = 0, State_ptest_key_exchange, State_ptest_data_transfer};

/*!
 *  Demo server
 */
class PTest_COM_Server : public RawTunnelendpointAcceptorTCPEventhandler,
    public AsyncContinuePolicy, public SessionEventhandler, public TunnelendpointEventhandler {
public:
    PTest_COM_Server(const CheckpointHandler::APtr& checkpoint_handler,
                     boost::asio::io_service& io,
                     const std::string& host,
                     const unsigned long port,
                     const DataBufferManaged::APtr& known_secret) :
        io_(io), state_(State_init), checkpoint_handler_(checkpoint_handler), known_secret_(known_secret) {
        acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler,
                                                         io,
                                                         host,
                                                         port,
                                                          this,
                                                          this);
    }

    void listen(void) {
        acceptor_->aio_accept_start();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        session_ = Session::create_server(io_, 1, 1, false, checkpoint_handler_, tunnelendpoint, known_secret_);
        session_->set_eventhandler(this);

        tunnelendpoint_ = TunnelendpointReliableCryptedTunnel::create(io_, checkpoint_handler_, this);
        session_->add_tunnelendpoint(1, tunnelendpoint_);
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    }

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void) {
        return false;
    }
    /*! \brief signal from SessionEventhandler
     */
    void session_state_ready(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    bool session_read_continue_state_ready(const unsigned long session_id) {
        switch (state_) {
        case State_ptest_data_transfer:
            return repeat_count_read_ > 0;
        }
        return false;
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_closed(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_security_closed(const unsigned long session_id) {
    }

    /*! \brief signal from TunnelendpointEventhandler
     */
    unsigned long tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        switch (state_) {
        case State_ptest_data_transfer:
            --repeat_count_read_;
            tunnelendpoint_->tunnelendpoint_send(message, false);
        }
        return 0;
    }

    void tunnelendpoint_eh_connected(void) {
    }

    void run_ptest_key_exchange(void) {
        state_ = State_ptest_key_exchange;
        session_->do_key_exchange();
    }

    void run_ptest_data_transfer(const int& repeat_count, const int& package_size) {
        state_ = State_ptest_data_transfer;
        repeat_count_read_ = repeat_count;
        repeat_count_write_ = repeat_count;
        package_size_ = package_size;

        session_->read_start_state_ready();
    }

private:
	boost::asio::io_service& io_;
    State state_;
    int package_size_;
    int repeat_count_read_;
    int repeat_count_write_;

    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointAcceptorTCP::APtr acceptor_;

    Tunnelendpoint::APtr tunnelendpoint_;
    Session::APtr session_;
};

/*!
 *  Demo client
 */
class PTest_COM_Client : public RawTunnelendpointConnectorTCPEventhandler,
    public SessionEventhandler, public TunnelendpointEventhandler {
public:
    PTest_COM_Client(const CheckpointHandler::APtr& checkpoint_handler,
                     boost::asio::io_service& io,
                     const std::string& host,
                     const unsigned long port,
                     const DataBufferManaged::APtr& known_secret) :
        io_(io), state_(State_init), checkpoint_handler_(checkpoint_handler), known_secret_(known_secret) {
        connector_
                = RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, host, port, this);
    }

    void connect(void) {
        connector_->aio_connect_start();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        session_ = Session::create_client(io_, 1, 1, false, checkpoint_handler_, tunnelendpoint, known_secret_);
        session_->set_eventhandler(this);

        tunnelendpoint_ = TunnelendpointReliableCryptedTunnel::create(io_, checkpoint_handler_, this);
        session_->add_tunnelendpoint(1, tunnelendpoint_);
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connection_failed_to_connect(void) {
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_state_ready(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    bool session_read_continue_state_ready(const unsigned long session_id) {
        switch (state_) {
        case State_ptest_data_transfer:
            return repeat_count_read_ > 0;
        }
        return false;
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_closed(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_security_closed(const unsigned long session_id) {
    }

    /*! \brief signal from TunnelendpointEventhandler
     */
    unsigned long tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        --repeat_count_read_;

        if (repeat_count_write_ > 0) {
            send_message();
        }
        return 0;
    }
    void send_message(void) {
        MessagePayload::APtr message(MessagePayload::create(DataBufferManaged::create(package_size_)));
        tunnelendpoint_->tunnelendpoint_send(message, false);
        ++repeat_count_write_;
    }

    void tunnelendpoint_eh_connected(void) {
    }

    void run_ptest_key_exchange(void) {
        state_ = State_ptest_key_exchange;
        session_->do_key_exchange();
    }

    void run_ptest_data_transfer(const int& repeat_count, const int& package_size) {
        state_ = State_ptest_data_transfer;
        repeat_count_read_ = repeat_count;
        repeat_count_write_ = repeat_count;
        package_size_ = package_size;

        send_message();
        session_->read_start_state_ready();
    }

private:
	boost::asio::io_service& io_;
    State state_;
    int package_size_;
    int repeat_count_read_;
    int repeat_count_write_;

    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointConnectorTCP::APtr connector_;

    Tunnelendpoint::APtr tunnelendpoint_;
    Session::APtr session_;
};

/*
 *
 */
class PTest_CheckpointOutputHandler : public CheckpointOutputHandler {
public:
    typedef boost::shared_ptr< PTest_CheckpointOutputHandler > APtr;

    void close(void) {
    }

    void operator()(const std::string& checkpoint_id,
              const CheckpointAttr::APtrs& event_attrs,
              const CheckpointAttr::Structure structure) {
        if (checkpoint_id == "RawTunnelendpointTCP::aio_write_start") {
            CheckpointAttrI* scope_checkpoint_attr = CheckpointAttrI::lookup(event_attrs, "scope");
            CheckpointAttrI* scope_end_checkpoint_attr = CheckpointAttrI::lookup(event_attrs,
                                                                                 "scope_end");
            CheckpointAttrI* size_checkpoint_attr = CheckpointAttrI::lookup(event_attrs, "size");
            CheckpointAttrI* local_port_checkpoint_attr = CheckpointAttrI::lookup(event_attrs,
                                                                                  "local_port");

            if (scope_checkpoint_attr != NULL && size_checkpoint_attr != NULL) {
                current_scope_ = scope_checkpoint_attr->get_value();
                current_write_byte_count_ = size_checkpoint_attr->get_value();
            } else if (scope_end_checkpoint_attr != NULL && local_port_checkpoint_attr != NULL
                    && scope_end_checkpoint_attr->get_value() == current_scope_) {
                if (local_port_checkpoint_attr != NULL && local_port_checkpoint_attr->get_value()
                        == server_port_) {
                    server_write_count_ += 1;
                    server_write_byte_count_ += current_write_byte_count_;
                } else {
                    client_write_count_ += 1;
                    client_write_byte_count_ += current_write_byte_count_;
                }
            } else {
                assert(false);
            }
        } else if (checkpoint_id == "SessionCrypterServer::decrypt_stream") {
            CheckpointAttrTD* attr = CheckpointAttrTD::lookup(event_attrs, "duration");
            if (attr != NULL) {
                ptest_com_duration_crypt_ += attr->get_value();
            }
        } else if (checkpoint_id == "SessionCrypterServer::encrypt_stream") {
            CheckpointAttrTD* attr = CheckpointAttrTD::lookup(event_attrs, "duration");
            if (attr != NULL) {
                ptest_com_duration_crypt_ += attr->get_value();
            }
        } else if (checkpoint_id == "RawTunnelendpointTCP::aio_read") {
            CheckpointAttrTD* attr = CheckpointAttrTD::lookup(event_attrs, "duration");
            if (attr != NULL) {
                ptest_com_duration_aio_read_ += attr->get_value();
            }
        } else if (checkpoint_id == "ptest_com") {
            CheckpointAttrTD* ptest_com_checkpoint = CheckpointAttrTD::lookup(event_attrs,
                                                                              "duration");
            if (ptest_com_checkpoint != NULL) {
                ptest_com_duration_ = ptest_com_checkpoint->get_value();
            }
        }
    }

    void reset(const unsigned long& server_port) {
        server_port_ = server_port;
        server_write_count_ = 0;
        server_write_byte_count_ = 0;
        client_write_count_ = 0;
        client_write_byte_count_ = 0;
        ptest_com_duration_ = boost::posix_time::microseconds(0);
        ptest_com_duration_crypt_ = boost::posix_time::microseconds(0);
        ptest_com_duration_aio_read_ = boost::posix_time::microseconds(0);
    }

    void dump(void) {
        cout << " server_write_count_:" << server_write_count_ << endl;
        cout << " server_write_byte_count_:" << server_write_byte_count_ << endl;
        cout << " ptest_com_duration_:" << boost::posix_time::to_iso_string(ptest_com_duration_)
                << endl;
    }

    static APtr create(void) {
        return PTest_CheckpointOutputHandler::APtr(new PTest_CheckpointOutputHandler);
    }

    unsigned long current_scope_;
    unsigned long current_write_byte_count_;

    unsigned long server_port_;
    unsigned long server_write_count_;
    unsigned long server_write_byte_count_;
    unsigned long client_write_count_;
    unsigned long client_write_byte_count_;
    boost::posix_time::time_duration ptest_com_duration_;
    boost::posix_time::time_duration ptest_com_duration_crypt_;
    boost::posix_time::time_duration ptest_com_duration_aio_read_;
};

/*
 * Create checkpoint handler
 */
CheckpointAttr_Module::APtr checkpoint_module(CheckpointAttr_Module::create("ptest_com"));
CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create()));

PTest_CheckpointOutputHandler::APtr
        ptest_checkpoint_output_handler(PTest_CheckpointOutputHandler::create());
CheckpointHandler::APtr
        checkpoint_handler(CheckpointHandler::create(checkpoint_filter,
                                                     ptest_checkpoint_output_handler));

/*
 * Create asynchron io handler
 */
std::string server_ip("127.0.0.1");
unsigned long server_port = 8045;
boost::asio::io_service ioservice;

void run_ptest_data_transfer(PTest_COM_Client& client,
                             PTest_COM_Server& server,
                             const int repeat_count,
                             const int package_size) {
    {
        ptest_checkpoint_output_handler->reset(server_port);
        CheckpointScope cps(*checkpoint_handler, "ptest_com", checkpoint_module, CpAttr_debug());

        server.run_ptest_data_transfer(repeat_count, package_size);
        client.run_ptest_data_transfer(repeat_count, package_size);
        ioservice.reset();
        ioservice.run();
    }

    boost::posix_time::time_duration package_latency_crypt =
            ptest_checkpoint_output_handler->ptest_com_duration_crypt_ / (repeat_count * 2);

    boost::posix_time::time_duration package_latency =
            ptest_checkpoint_output_handler->ptest_com_duration_ / (repeat_count * 2);

    boost::posix_time::time_duration package_latency_aio_read =
            ptest_checkpoint_output_handler->ptest_com_duration_aio_read_ / (repeat_count * 2);

    long double transfer_rate_b_mics = (package_size * (double)1.0)
            / package_latency_aio_read.total_microseconds();
    long double transfer_rate_kb_s = (transfer_rate_b_mics * 1000000.0) / 1024.0;

    cout << "  package_size: " << std::setw(5) << package_size << "  package_latency(ms): "
            << std::setw(5) << std::setprecision(3) << std::fixed << std::showpoint
            << (package_latency_aio_read.total_microseconds() / 1000.0)
            <<"  package_latency_crypt(ms): " << std::setw(5) << std::setprecision(3) << std::fixed
            << std::showpoint << (package_latency_crypt.total_microseconds() / 1000.0)
            << "  kb/s: " << std::setw(9) << std::setprecision(4) << std::fixed
            << transfer_rate_kb_s << endl;
}

/*
 * Run performance test
 */
void run_ptest() {
    /*
     * Create known secret
     */
    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client,
                                                                     known_secret_server);

    /*
     * Create and connect server and client
     */
    PTest_COM_Server server(checkpoint_handler,
                            ioservice,
                            server_ip,
                            server_port,
                            known_secret_server);
    PTest_COM_Client client(checkpoint_handler,
                            ioservice,
                            server_ip,
                            server_port,
                            known_secret_client);

    ptest_checkpoint_output_handler->reset(server_port);

    server.listen();
    client.connect();
    ioservice.run();

    /*
     * Execute ptest for key_exchange
     */
    int repeat_count = 4;
    cout << "PTest_COM - key_echange:" << endl;
    {
        ptest_checkpoint_output_handler->reset(server_port);
        CheckpointScope cps(*checkpoint_handler, "ptest_com", checkpoint_module, CpAttr_debug());

        for (int idx = 0; idx < repeat_count; ++idx) {
            server.run_ptest_key_exchange();
            client.run_ptest_key_exchange();
            ioservice.reset();
            ioservice.run();
        }
    }
    cout << "  time            (sec)   : "
            << boost::posix_time::to_iso_string(ptest_checkpoint_output_handler->ptest_com_duration_
                    / repeat_count) << endl;
    cout << "  packages s -> c (#)     : " << ptest_checkpoint_output_handler->server_write_count_
            / repeat_count << endl;
    cout << "  data     s -> c (bytes) : "
            << ptest_checkpoint_output_handler->server_write_byte_count_ / repeat_count << endl;
    cout << "  packages c -> s (#)     : " << ptest_checkpoint_output_handler->client_write_count_
            / repeat_count << endl;
    cout << "  data     c -> s (bytes) : "
            << ptest_checkpoint_output_handler->client_write_byte_count_ / repeat_count << endl;
    cout << endl;

    /*
     * Execute ptest for data transfer
     */
    cout << "PTest_COM - data transfer:" << endl;
    run_ptest_data_transfer(client, server, 1000, 10);
    run_ptest_data_transfer(client, server, 1000, 100);
    run_ptest_data_transfer(client, server, 1000, 1000);
    run_ptest_data_transfer(client, server, 1000, 10000);

}

/*
 * main
 */
int main(int, char* []) {
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
    run_ptest();
    return 0;
}

