/*! \file COM_TrafficControlManager.cxx
 \brief This file contains the implementation for the TrafficControlManager classes
 */
#include <boost/asio.hpp>

#include <component/communication/COM_TrafficControlManager.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TrafficControlManager implementation
 * ------------------------------------------------------------------
 */
static TrafficControlManager::APtr global_traffic_control_manager_;

TrafficControlManager::TrafficControlManager(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) :
		asio_io_service_(asio_io_service),
		mutex_(Mutex::create(asio_io_service, "TrafficControlManager")),
		checkpoint_handler_(checkpoint_handler),
//		checkpoint_handler_details_(checkpoint_handler->create_in_same_folder("gon_traffic_control.log")),
		interval_timer_(asio_io_service),
		state_(State_Running) {
	interval_tick_start();
}

TrafficControlManager::~TrafficControlManager(void) {
}

TrafficControlSession::APtr TrafficControlManager::get_session(const std::string& session_id) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	Sessions::const_iterator i(sessions_.find(session_id));
	if(i != sessions_.end()) {
		return i->second;
	}
	TrafficControlSession::APtr traffic_session(TrafficControlSession::create(asio_io_service_, checkpoint_handler_, session_id));
	sessions_.insert(make_pair(session_id, traffic_session));
	return traffic_session;
}

TrafficControlManager::APtr TrafficControlManager::create(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) {
	APtr result(new TrafficControlManager(asio_io_service, checkpoint_handler));
	AsyncMemGuard::global_add_element(result);
	return result;
}

TrafficControlManager::APtr TrafficControlManager::get_global(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) {
	if (global_traffic_control_manager_.get() == NULL) {
		global_traffic_control_manager_ = TrafficControlManager::create(io, checkpoint_handler);
	}
	return global_traffic_control_manager_;
}

void TrafficControlManager::destroy_global() {
	global_traffic_control_manager_.reset();
}

void TrafficControlManager::interval_tick_start(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	switch(state_) {
	case State_Running:
		break;
	default:
		return;
	}
	interval_timer_.expires_after(boost::chrono::seconds(2));
	interval_timer_.async_wait(boost::bind(&TrafficControlManager::interval_tick, this));
}

void TrafficControlManager::interval_tick(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);

	std::vector<std::string> sessions_inactive;

	for (const auto& pair : sessions_) {
		if(!(pair.second->is_active())) {
			pair.second->interval_tick_stop();
			sessions_inactive.push_back(pair.first);
		}
	}

	std::vector<std::string>::const_iterator j(sessions_inactive.begin());
	while(j != sessions_inactive.end()) {
		sessions_.erase(*j);
		++j;
	}

	if (state_ == State_Running) {
		interval_tick_start();
	}
	else if (state_ == State_Stopping) {
		state_ = State_Done;
	}
}

void TrafficControlManager::close(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	switch(state_) {
	case State_Running:
		break;
	default:
		return;
	}
	state_ = State_Stopping;
}

bool TrafficControlManager::async_mem_guard_is_done(void) {
	MutexScopeLockAnonyme mutex_lock(mutex_);
	return state_ == State_Done;
}

std::string TrafficControlManager::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}
