/*! \file COM_Exception.hxx
 \brief This file contains all exceptions thrown from the communication component
 */
#ifndef COM_EXCEPTION_HXX
#define COM_EXCEPTION_HXX

#include <string>
#include <lib/utility/UY_Exception.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Base class for all excaptions thrown in the communication component 
 */
class Exception : public Giritech::Utility::Exception {

    public:
        Exception(const std::string& message) :
            Giritech::Utility::Exception(message) {
        }
};

/*! \brief This exception is thrown in case of an error during key-exchange 
 */
class Exception_Key_Exchange_Failed : public Exception {

    public:
        Exception_Key_Exchange_Failed(const std::string& message) :
            Exception(message) {
        }
};

/*! \brief This exception is thrown in case a client session manager is unable to find a server that i ready for connectionr 
 */
class ExceptionUnableToConnectToServer : public Exception {

    public:
        ExceptionUnableToConnectToServer(const std::string& message) :
            Exception(message) {
        }
};

/*! \brief This exception is thrown in case a message is recived for a invalid tunnelendpoint
 */
class ExceptionInvalidTunnelendpoint : public Exception {

    public:
        ExceptionInvalidTunnelendpoint(const std::string& message) :
            Exception(message) {
        }
};

/*! \brief This exception is thrown in case of a error occured during callback
 */
class ExceptionCallback : public Exception {

    public:
        ExceptionCallback(const std::string& message) :
            Exception(message) {
        }
};

/*! \brief This exception is thrown in case of an unexpected fatal error 
 */
class ExceptionUnexpected : public Exception {

    public:
        ExceptionUnexpected(const std::string& message) :
            Exception(message) {
        }
};

} // End namespace, Communication
} // End Namespace, Giritech
#endif
