/*! \file COM_TunnelendpointMessage.cxx
 \brief This file contains the implementation for the tunnelendpoint-message class
 */
#include <functional>
#include <iostream>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>

#include <component/communication/COM_TunnelendpointMessage.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Version;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TunnelendpointMessage implementation
 * ------------------------------------------------------------------
 */
TunnelendpointMessage::TunnelendpointMessage(const MessageType message_type, const ReturnCodeType rc) :
    message_type_(message_type), rc_(rc) {
}

TunnelendpointMessage::~TunnelendpointMessage(void) {
}

TunnelendpointMessage::APtr TunnelendpointMessage::create(const MessageType message_type, const ReturnCodeType rc) {
    return APtr(new TunnelendpointMessage(message_type, rc));
}

Utility::DataBufferManaged::APtr TunnelendpointMessage::create_as_buffer(void) {
    unsigned long buffer_size = 6;

    Utility::DataBufferManaged::APtr buffer(Utility::DataBufferManaged::create(buffer_size));
    set_uint_8(buffer, 0, message_type_);
    set_uint_8(buffer, 1, rc_);
    set_uint_8(buffer, 2, version_->get_version_major());
    set_uint_8(buffer, 3, version_->get_version_minor());
    set_uint_8(buffer, 4, version_->get_version_bugfix());
    set_uint_8(buffer, 5, version_->get_version_build_id());
    return buffer;
}

TunnelendpointMessage::APtr TunnelendpointMessage::create_from_buffer(const Utility::DataBufferManaged::APtr& buffer) {
    unsigned long buffer_size = 6;

    boost::uint8_t message_type_raw = 0;
    convert_from_networkorder_8(message_type_raw, (*buffer)[0]);

    boost::uint8_t rc_raw = 0;
    convert_from_networkorder_8(rc_raw, (*buffer)[1]);

    boost::uint8_t version_major_raw = 0;
    convert_from_networkorder_8(version_major_raw, (*buffer)[2]);

    boost::uint8_t version_minor_raw = 0;
    convert_from_networkorder_8(version_minor_raw, (*buffer)[3]);

    boost::uint8_t version_bugfix_raw = 0;
    convert_from_networkorder_8(version_bugfix_raw, (*buffer)[4]);

    boost::uint8_t version_buld_id_raw = 0;
    convert_from_networkorder_8(version_buld_id_raw, (*buffer)[5]);

    TunnelendpointMessage::APtr message(TunnelendpointMessage::create(static_cast<MessageType> (message_type_raw),
                                                                      static_cast<ReturnCodeType> (rc_raw)));
    Version::Version::APtr version(Version::Version::create(version_major_raw, version_minor_raw, version_bugfix_raw, version_buld_id_raw));
    message->set_version(version);
    return message;
}

TunnelendpointMessage::MessageType TunnelendpointMessage::get_message_type(void) const {
    return message_type_;
}

TunnelendpointMessage::ReturnCodeType TunnelendpointMessage::get_rc(void) const {
    return rc_;
}

Giritech::Version::Version::APtr TunnelendpointMessage::get_version(void) const {
    return version_;
}

void TunnelendpointMessage::set_version(const Giritech::Version::Version::APtr& version) {
    version_ = version;
}
