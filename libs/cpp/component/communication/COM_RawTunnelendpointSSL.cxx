/*! \file COM_RawTunnelendpointSSL.cxx
 * \brief This file contains the implementation of the COM_RawTunnelendpointSSL abstraction
 */
#include <sstream>
#include <iostream>
#include <fstream>

#include <component/communication/COM_RawTunnelendpointSSL.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 * ------------------------------------------------------------------
 * RawTunnelendpointTCPSSL implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointTCPSSL::RawTunnelendpointTCPSSL(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const unsigned long& connection_id,
		const std::string& host_name,
		const RawTunnelendpointTCPSSLContext::APtr& context) :
	RawTunnelendpointTCP(checkpoint_handler, io, connection_id),
    ssl_socket_(io, context->get_ctx()),
    context_(context),
    host_name_(host_name) {
	if(context_->get_verify_certificate()) {
		ssl_socket_.set_verify_mode(boost::asio::ssl::verify_peer);
	}
	else {
		ssl_socket_.set_verify_mode(boost::asio::ssl::verify_none);
	}
	if(context_->get_verify_hostname()) {
		ssl_socket_.set_verify_callback(boost::asio::ssl::rfc2818_verification(host_name));
	}
	if(context_->get_sni()) {
		SSL_set_tlsext_host_name(ssl_socket_.native_handle(), host_name);
	}
}

RawTunnelendpointTCPSSL::~RawTunnelendpointTCPSSL(void) {
}

RawTunnelendpointTCPSSL::APtr RawTunnelendpointTCPSSL::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const unsigned long& connection_id,
		const std::string& host_name,
		const RawTunnelendpointTCPSSLContext::APtr& context) {
	APtr result(new RawTunnelendpointTCPSSL(checkpoint_handler, io, connection_id, host_name, context));
	AsyncMemGuard::global_add_element(result);
	return result;
}

boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& RawTunnelendpointTCPSSL::get_socket_ssl(void) {
    return ssl_socket_;
}

const boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& RawTunnelendpointTCPSSL::get_socket_ssl(void) const {
    return ssl_socket_;
}

boost::asio::ip::tcp::socket& RawTunnelendpointTCPSSL::get_socket(void) {
    return ssl_socket_.next_layer();
}

const boost::asio::ip::tcp::socket& RawTunnelendpointTCPSSL::get_socket(void) const {
    return ssl_socket_.next_layer();
}

void RawTunnelendpointTCPSSL::socket_operation_async_read(void) {
    boost::asio::async_read(ssl_socket_,
                            boost::asio::buffer(raw_buffer_->get_raw_buffer()),
                            boost::asio::transfer_at_least(1),
                            mutex_->get_strand().wrap(boost::bind(&RawTunnelendpointTCPSSL::aio_read,
                                                     this,
                                                     boost::asio::placeholders::error,
                                                     boost::asio::placeholders::bytes_transferred)));
}

void RawTunnelendpointTCPSSL::socket_operation_async_write(const DataBufferWithTime::APtr& data) {
    boost::asio::async_write(
    		ssl_socket_,
    		boost::asio::buffer(data->getDataBuffer()->data(), data->getDataBuffer()->getSize()),
    		mutex_->get_strand().wrap(boost::bind( &RawTunnelendpointTCP::aio_write_from_buffer, this, boost::asio::placeholders::error, data)));
}

bool RawTunnelendpointTCPSSL::socket_operation_is_eof(const boost::system::error_code& error_code) {
	boost::system::error_code ssl_eof(ERR_PACK(ERR_LIB_SSL, 0, SSL_R_SHORT_READ), boost::asio::error::get_ssl_category());
	return RawTunnelendpointTCP::socket_operation_is_eof(error_code) || (error_code == ssl_eof);
}
