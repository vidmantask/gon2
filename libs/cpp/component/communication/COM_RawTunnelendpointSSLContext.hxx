/*! \file COM_RawTunnelendpointSSLContext.hxx
 * \brief This file contains the RawTunnelendpoint abstraction
 */
#ifndef COM_RawTunnelendpointSSLContext_HXX
#define COM_RawTunnelendpointSSLContext_HXX

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>


#include <lib/utility/UY_CheckpointHandler.hxx>

namespace Giritech {
namespace Communication {


/*! \brief This class represent the ssl context used by every ssl socket
 *
 */
class RawTunnelendpointTCPSSLContext {
public:
    typedef boost::shared_ptr<RawTunnelendpointTCPSSLContext> APtr;

    ~RawTunnelendpointTCPSSLContext(void);

    static APtr create(
    		boost::asio::io_service& io,
    		const std::string& method,
    		const unsigned long ssl_options);

    boost::asio::ssl::context& get_ctx(void);

    static RawTunnelendpointTCPSSLContext::APtr get_instance(
    		boost::asio::io_service& io,
    		const std::string& method,
    		const unsigned long ssl_options);

    void init(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		const std::string& certificate_path,
    		const bool certificate_verificatrion_enabled,
    		const bool hostname_verification_enabled,
    		const bool sni_enabled
    		);

    bool get_verify_certificate(void) const;
    bool get_verify_hostname(void) const;
    bool get_sni(void) const;

private:
     RawTunnelendpointTCPSSLContext(
    		 boost::asio::io_service& io,
    		 const std::string& method,
    			const unsigned long ssl_options);

     boost::asio::ssl::context ctx_;
     bool certificate_verificatrion_enabled_;
     bool hostname_verification_enabled_;
     bool sni_enabled_;

};


}}
#endif
