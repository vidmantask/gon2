/*! \file COM_AsyncContinuePolicy.hxx
 * \brief This file contains abstract interface AsyncContinuePolicy
 */
#ifndef COM_AsyncContinuePolicy_HXX
#define COM_AsyncContinuePolicy_HXX

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface the policys regarding contiunuation of async operations.
 */
class AsyncContinuePolicy : public boost::noncopyable {
public:

    /*! \brief Connected acceptting new connections
     * 
     * \return True indicate that the acceptor should continue accepting connections
     */
    virtual bool com_accept_continue(void) = 0;
};

}
}
#endif
