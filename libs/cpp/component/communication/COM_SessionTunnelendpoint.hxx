/*! \file COM_SessionTunnelendpoint.hxx
 \brief This file contains the session tunnelendpoint classes
 */
#ifndef COM_SessionTunnelendpoint_HXX
#define COM_SessionTunnelendpoint_HXX

#include <vector>
#include <queue>

#include <boost/asio.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/thread/recursive_mutex.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_SessionTunnelendpointEventhandler.hxx>
#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_StreamToMessage.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Represent a tunnelendpoint for packages with a specific quality
 */
class SessionTunnelendpoint : public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<SessionTunnelendpoint> APtr;

    /*! \brief Destructor
     */
    virtual ~SessionTunnelendpoint(void);

    /*! \brief reset enventhandler
     */
    virtual void reset_eventhandler(void);

    /*! \brief Send a package
     */
    virtual void session_tunnelendpoint_send(const MessageCom::APtr&) = 0;

    /*! \brief Close session
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
     */
    virtual void close(const bool force) = 0;

    /*! \brief Report back if the session tunnel endpoint has been closed
     */
    virtual bool is_closed(void) = 0;

    /*! \brief Indicate that the eventhandler is ready to handle messages
     */
    virtual void read_start_ready(void) = 0;

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void) = 0;

protected:

    /*! \brief Constructor
     */
    SessionTunnelendpoint(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, SessionTunnelendpointEventhandler* eventhandler);

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
    SessionTunnelendpointEventhandler* eventhandler_;
};

/*! \brief A reliable session-tunnelendppoint using the session crypter as raw-tunnelendpoint
 *
 * Convert the ready-stream from the session-crypter to packages.
 *
 */
class SessionTunnelendpointReliableSessionCrypter : public SessionTunnelendpoint,
    public SessionCrypterEventhandlerReady {
public:
    typedef boost::shared_ptr<SessionTunnelendpointReliableSessionCrypter> APtr;

    enum State {
        State_Unknown = 0,
        State_Ready,
        State_Closing,
        State_Closed,
    };

    /*! \brief Destructor
     */
    virtual ~SessionTunnelendpointReliableSessionCrypter(void);

    /*! \brief Send a package
     */
    void session_tunnelendpoint_send(const MessageCom::APtr& message);

    /*! \brief Signal from SessionCrypterEventhandlerReady
     */
    void session_crypter_recieve(const Utility::DataBuffer::APtr& data);

    /*! \brief Signal from SessionCrypterEventhandlerReady
     */
    void session_crypter_closed(void);

    /*! \brief Close session
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
     */
    void close(const bool force);

    /*! \brief Report back if the session tunnel endpoint has been closed
     */
    bool is_closed(void);

    /*! \brief Indicate that the eventhandler is ready to handle messages
     */
    void read_start_ready(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& asio_io_service,
    				   const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       SessionTunnelendpointEventhandler* eventhandler,
                       const SessionCrypter::APtr& session_crypter);

protected:

    /*! \brief Constructor
     */
    SessionTunnelendpointReliableSessionCrypter(boost::asio::io_service& asio_io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                SessionTunnelendpointEventhandler* eventhandler,
                                                const SessionCrypter::APtr& session_crypter);

    State state_;
    SessionCrypter::APtr session_crypter_;
    StreamToMessage::APtr session_crypter_stream_to_message_;
    std::queue<MessageCom::APtr> com_message_queue_;

    bool read_message_started_;
};

}
}
#endif
