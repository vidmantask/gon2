/*! \file COM_API_TunnelendpointCheckpointHandler.hxx
 *  \brief This file contains the a API wrapper for checkpoint_handler tunnelendpoints
 */
#ifndef COM_API_TunnelendpointCheckpointHandler_HXX
#define COM_API_TunnelendpointCheckpointHandler_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointCheckpointHandler.hxx>


namespace Giritech {
namespace Communication {


class APITunnelendpointCheckpointHandlerServer : public boost::noncopyable {
public:
    typedef boost::shared_ptr<APITunnelendpointCheckpointHandlerServer> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointCheckpointHandlerServer(void);


    TunnelendpointCheckpointHandlerServer::APtr get_impl(void);

    /*! \brief create instance
     */
    static APtr create(
    		const APIAsyncService::APtr& async_service,
    		const Utility::APICheckpointHandler::APtr& checkpoint_handler,
    		const std::string& folder);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointCheckpointHandlerServer(
    		const APIAsyncService::APtr& async_service,
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		const boost::filesystem::path& folder);

    TunnelendpointCheckpointHandlerServer::APtr impl_tunnelendpoint_;
};


}
}
#endif
