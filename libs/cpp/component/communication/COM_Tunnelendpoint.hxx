/*! \file COM_Tunnelendpoint.hxx
 \brief This file contains the tunnelendpoint clases
 */
#ifndef COM_Tunnelendpoint_HXX
#define COM_Tunnelendpoint_HXX

#include <map>
#include <set>
#include <list>
#include <vector>
#include <queue>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>
#include <lib/version/Version.hxx>

#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_SessionMessage.hxx>
#include <component/communication/COM_TunnelendpointMessage.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_TunnelendpointConnector.hxx>
#include <component/communication/COM_TrafficControlSession.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Abstract tunnelendpoint
 *
 * The basic functionality of the Tunnelendpoint abstraction is to offer a transport service of messages betwen two tunnelendpoints,
 * where one endpoint is on the server and the other endpoint is in a client(GClient).
 *
 * A Tunnelendpoint is offering a send-method that sends data to the
 * Tunnelendpoint's mirror who recives the data through a recive-event.
 *
 *
 * Protocol versions:
 * 0 - Initial protocol used by version < 5.2
 * 1 - Tunnelendpoints enchange version information before data can be send/recived, current.
 *
 */
class Tunnelendpoint: public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<Tunnelendpoint> APtr;

    /*! \brief Destructor
     */
    virtual ~Tunnelendpoint(void);

    	/*! \brief Sends a message
     */
    virtual void tunnelendpoint_send(const MessageCom::APtr& message);
    virtual void tunnelendpoint_send(const MessagePayload::APtr& payload);

    /*! \brief sends a user defined signal to the parent of the tunnel
     */
    virtual void tunnelendpoint_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message);
    virtual void tunnelendpoint_user_signal(const unsigned long signal_id, const std::string& message);

    /*! \brief Recives a message
     */
    virtual void tunnelendpoint_recieve(const MessageCom::APtr& message);

    /*! \brief Reset eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Get the eventhandler
     */
    TunnelendpointEventhandler* get_eventhandler(void);

    /*! \brief Set the connector
     */
    void set_connector(TunnelendpointConnector* connector);

    virtual void set_mutex(const Utility::Mutex::APtr& mutex);
    virtual Utility::Mutex::APtr get_mutex(void);

    /*! \brief Reset the connector
     */
    void reset_connector(void);

    /*! \brief Return the absolute id of the tunnelendpoint
     */
    std::list<unsigned long> get_id_abs(void);

    /*! \brief Set absolute id
     */
    void set_id_abs(const std::list<unsigned long>& id_abs);

    /*! \brief append id to absolute id
     */
    void append_id_to_id_abs(const unsigned long& id);

    /*! \brief Return absolute id as string
     */
    std::string get_id_abs_to_string();
    std::string get_id_abs_to_string(const unsigned long& id);

    /*! \brief Returns true if the tunnelendpoint is connected to a remote tunnelendpoint
     */
    bool is_connected(void);

    void set_version(const Version::Version::APtr& version);
    Version::Version::APtr get_version(void);
    Version::Version::APtr get_version_remote(void);

    /*! \brief Called by parent/owner of tunnelendpoint when connected
     */
    virtual void connect(void);

    /*! \brief Get protocol version
     */
    unsigned char get_protocol_verison(void);

    /*! \brief Force a specific protocol version
     */
    void set_protocol_version(const unsigned char& protocol_verision);

    /*! \brief Enable skip of connection handshake (and version exchange).
     */
    void set_skip_connection_handshake(void);


    virtual void set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type);
    SessionMessage::ApplProtocolType get_appl_protocol_type(void);

    /*
     * Close by strand will call class using strand objecte ensuring that all posted sends are done before close is called
     */
    void close_by_strand(const bool force);
    void close_from_strand(const bool force);

    virtual void close(const bool force);
    virtual bool is_closed(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    unsigned long get_tc_read_delay_ms(void);
    unsigned long get_tc_read_size(void);
    void tc_report_read_begin(void);
    void tc_report_read_end(const unsigned long size);

    void tc_report_push_buffer_size(const unsigned long size);
    void tc_report_pop_buffer_size(const unsigned long size);


protected:

    /*! \brief Constructor
     */
    Tunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler);

    void tunnelendpoint_recieve_tunnelendpoint_data(const TunnelendpointMessage::APtr& message);

    virtual void goto_state_connected(void);

    void close_final(void);

    virtual void set_traffic_control_session(void);

    boost::asio::io_service& io_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;

    TunnelendpointEventhandler* eventhandler_;
    TunnelendpointConnector* connector_;

    std::list<unsigned long> id_abs_;

    unsigned long send_package_id_next_;

    enum State {
        State_Initializing = 0,
        State_Connecting,
        State_Connected,
        State_Closing,
        State_Closed,
        State_Done,
        State_Unknown
    };
    State state_;
    Version::Version::APtr version_;
    Version::Version::APtr version_remote_;

    unsigned char protocol_version_;
    bool skip_connection_handshake_;

    Utility::Mutex::APtr mutex_;

    SessionMessage::ApplProtocolType appl_protocol_type_;

    bool state_connecting_connect_recived_;
    bool state_connecting_connect_response_recived_;

    unsigned long pending_posts_;

    TrafficControlSession::APtr traffic_control_session_;
    unsigned long traffic_control_session_point_id_;

};

/*! \brief Encrypted and reliable tunnelendpoint
 */
class TunnelendpointReliableCrypted: public Tunnelendpoint {
public:
    typedef boost::shared_ptr<TunnelendpointReliableCrypted> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointReliableCrypted(void);

    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler);

protected:

    /*! \brief Constructor
     */
  TunnelendpointReliableCrypted(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler);

};

/*! \brief Connector helper class for tunnelendpoint tunnel
 */
class TunnelendpointReliableCryptedTunnel;
class TunnelendpointReliableCryptedTunnelConnector: public TunnelendpointConnector {
public:
    typedef boost::shared_ptr<TunnelendpointReliableCryptedTunnelConnector> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointReliableCryptedTunnelConnector(void);

    /*! \brief Sends a message
     */
    void tunnelendpoint_connector_send(const MessageCom::APtr&);

    /*! \brief User signal notification to parent from child
     */
    void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message);

    /*! \brief Indicate that the tunnelendpoint has been closed
     */
    void tunnelendpoint_connector_closed(void);

    /*! \brief Ask for unique session id
     */
    std::string tunnelendpoint_connector_get_unique_session_id(void);

    /*! \brief Create instance
     */
    static APtr create(TunnelendpointReliableCryptedTunnel* tunnelendpoint, unsigned long child_id);

private:
    TunnelendpointReliableCryptedTunnelConnector(TunnelendpointReliableCryptedTunnel* tunnelendpoint, unsigned long child_id);

    unsigned long child_id_;
    TunnelendpointReliableCryptedTunnel* tunnelendpoint_;
};



/*! \brief Encrypted and reliable tunnelendpoint tunnel
 *
 * This tunnelendpoint can have child-tunnels.
 */
class TunnelendpointReliableCryptedTunnel: public TunnelendpointReliableCrypted {
public:
    typedef boost::shared_ptr<TunnelendpointReliableCryptedTunnel> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointReliableCryptedTunnel(void);

    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Add child tunnelendpoint
     */
    void add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint);

    /*! \brief Remove child tunnelendpoint
     */
    void remove_tunnelendpoint(const unsigned long& child_id);

    /*! \brief Sends a message
     */
    void tunnelendpoint_send(const MessagePayload::APtr& payload);
    void tunnelendpoint_send(const unsigned long child_id, const MessageCom::APtr& message);
    void tunnelendpoint_send(const MessageCom::APtr& message);

    virtual void tunnelendpoint_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message);
    virtual void tunnelendpoint_user_signal(const unsigned long signal_id, const std::string& message);
    virtual void tunnelendpoint_user_signal(const unsigned long child_id, const unsigned long signal_id, const MessagePayload::APtr& message);

    virtual std::string tunnelendpoint_get_unique_session_id(void);

    /*! \brief Recives a message
     */
    virtual void tunnelendpoint_recieve(const MessageCom::APtr& message);

    /*! \brief Dispatch a recived message to the right child tunnel
     */
    void dispatch_message(const MessageCom::APtr& message);

    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler);
    static APtr create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler, const Utility::Mutex::APtr& mutex);

    /*! \brief Called by parent/owner of tunnelendpoint when connected
     */
    virtual void connect(void);

    virtual void close(const bool force);

    virtual void set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type);

    virtual void tunnelendpoint_child_closed(const unsigned long child_id);

    void tunnelendpoint_child_closed_decoubled(const unsigned long child_id);

    virtual void set_traffic_control_session(void);

protected:

    /*! \brief Constructor
     */
    TunnelendpointReliableCryptedTunnel(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler);
    TunnelendpointReliableCryptedTunnel(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler, const Utility::Mutex::APtr& mutex);

    virtual void goto_state_connected(void);

    std::map<unsigned long, Tunnelendpoint::APtr> tunnelendpoints_;
    std::map<unsigned long, TunnelendpointReliableCryptedTunnelConnector::APtr> tunnelenspoint_connectors_;

    std::vector< std::pair<unsigned long, MessageCom::APtr> > message_buffer_tunnelendpoint_data_;
};

}
}
#endif
