/*! \file COM_TunnelendpointDirectHttpProxy.cxx
 \brief This file contains the implementation
 */
#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointDirectHttpProxy.hxx>
#include <component/communication/COM_TunnelendpointDirectEventhandler.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;




/*
 * ------------------------------------------------------------------
 * TunnelendpointDirectHttpProxy implementation
 * ------------------------------------------------------------------
 */
TunnelendpointDirectHttpProxy::TunnelendpointDirectHttpProxy(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const TunnelendpointDirect::APtr& tunnelendpoint_direct)
: HttpClientCom(tunnelendpoint_direct->get_io_service(), checkpoint_handler),
  checkpoint_handler_(checkpoint_handler),
  tunnelendpoint_direct_(tunnelendpoint_direct) {
	tunnelendpoint_direct->set_direct_eventhandler(this);
}

TunnelendpointDirectHttpProxy::~TunnelendpointDirectHttpProxy(void) {
}

TunnelendpointDirectHttpProxy::APtr TunnelendpointDirectHttpProxy::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const TunnelendpointDirect::APtr& tunnelendpoint_direct) {
	TunnelendpointDirectHttpProxy::APtr new_tunnelendpoint(new TunnelendpointDirectHttpProxy(checkpoint_handler, tunnelendpoint_direct));
	return new_tunnelendpoint;
}

bool TunnelendpointDirectHttpProxy::is_closed(void) {
	return tunnelendpoint_direct_->is_closed();
}

void TunnelendpointDirectHttpProxy::close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), tunnelendpoint_direct_->get_mutex(), "TunnelendpointDirectHttpProxy::close");

    Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirectHttpProxy::close", Attr_Communication(), CpAttr_debug());
	tunnelendpoint_direct_->close(false);
}

void TunnelendpointDirectHttpProxy::tunnelendpoint_direct_eh_closed() {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), tunnelendpoint_direct_->get_mutex(), "TunnelendpointDirectHttpProxy::tunnelendpoint_direct_eh_closed");
	Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirectHttpProxy::tunnelendpoint_direct_eh_closed", Attr_Communication(), CpAttr_debug());

	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_closed();
	}
}

void TunnelendpointDirectHttpProxy::tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), tunnelendpoint_direct_->get_mutex(), "TunnelendpointDirectHttpProxy::tunnelendpoint_direct_eh_recieve");
	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_read_some(message);
	}
}

void TunnelendpointDirectHttpProxy::connect(const std::string& host, const unsigned long port) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), tunnelendpoint_direct_->get_mutex(), "TunnelendpointDirectHttpProxy::connect");

	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_connected();
	}
}

void TunnelendpointDirectHttpProxy::write(const Utility::DataBufferManaged::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), tunnelendpoint_direct_->get_mutex(), "TunnelendpointDirectHttpProxy::write");
	tunnelendpoint_direct_->write(data);
}

