/*! \file COM_RawTunnelendpointSSL.cxx
 * \brief This file contains the implementation of the COM_RawTunnelendpointSSL abstraction
 */
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <component/communication/COM_RawTunnelendpointSSLContext.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


boost::asio::ssl::context_base::method get_method(const std::string& method) {
	if(boost::iequals(method, "sslv23")) {
		return boost::asio::ssl::context::sslv23;
	}
	else if(boost::iequals(method, "sslv2")) {
		return boost::asio::ssl::context::sslv2;
	}
	else if(boost::iequals(method, "sslv3")) {
		return boost::asio::ssl::context::sslv3;
	}
	else if(boost::iequals(method, "tlsv1")) {
		return boost::asio::ssl::context::tlsv1;
	}
	return boost::asio::ssl::context::sslv23;
}

/*
 * ------------------------------------------------------------------
 * RawTunnelendpointTCPSSLContext implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointTCPSSLContext::RawTunnelendpointTCPSSLContext(
		boost::asio::io_service& io,
		const std::string& method,
		const unsigned long ssl_options)
	: ctx_(get_method(method)),
	  certificate_verificatrion_enabled_(true),
	  hostname_verification_enabled_(true),
	  sni_enabled_(false) {
    // Call openssl directly because of a type error(using int for options, should be long) in asio
	SSL_CTX_set_options(ctx_.native_handle(), ssl_options | SSL_OP_ALL);
	// ctx_.set_options(boost::asio::ssl::context::default_workarounds | ssl_options);
}

RawTunnelendpointTCPSSLContext::~RawTunnelendpointTCPSSLContext(void) {
}

RawTunnelendpointTCPSSLContext::APtr RawTunnelendpointTCPSSLContext::create(
		boost::asio::io_service& io,
		const std::string& method,
		const unsigned long ssl_options
		) {
	APtr instance(new RawTunnelendpointTCPSSLContext(io, method, ssl_options));
	return instance;
}

boost::asio::ssl::context& RawTunnelendpointTCPSSLContext::get_ctx(void) {
	return ctx_;
}

RawTunnelendpointTCPSSLContext::APtr RawTunnelendpointTCPSSLContext::get_instance(
		boost::asio::io_service& io,
		const std::string& method,
		const unsigned long ssl_options) {
	static RawTunnelendpointTCPSSLContext::APtr global_context;
    if (!global_context) {
    	global_context = RawTunnelendpointTCPSSLContext::create(io, method, ssl_options);
    }
    return global_context;
}

bool RawTunnelendpointTCPSSLContext::get_verify_certificate(void) const {
	return certificate_verificatrion_enabled_;
}

bool RawTunnelendpointTCPSSLContext::get_verify_hostname(void) const {
	return hostname_verification_enabled_;
}

bool RawTunnelendpointTCPSSLContext::get_sni(void) const {
	return sni_enabled_;
}

void RawTunnelendpointTCPSSLContext::init(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const std::string& certificate_path,
		const bool certificate_verificatrion_enabled,
		const bool hostname_verification_enabled,
		const bool sni_enabled) {

	certificate_verificatrion_enabled_ = certificate_verificatrion_enabled;
	hostname_verification_enabled_ = hostname_verification_enabled;
	sni_enabled_ = sni_enabled;
    Checkpoint(
    		*checkpoint_handler,
    		"ssl",
    		Attr_Communication(),
    		CpAttr_info(),
    		CpAttr("certificate_verificatrion_enabled", certificate_verificatrion_enabled_),
    		CpAttr("hostname_verification_enabled", hostname_verification_enabled_),
    		CpAttr("certificate_path", certificate_path),
    		CpAttr("sni_enabled", sni_enabled)
    		);

	boost::filesystem::path path(certificate_path);

	if(boost::filesystem::exists(path)) {
		boost::filesystem::directory_iterator i(path);
		boost::filesystem::directory_iterator i_end;
		while(i != i_end) {
			if(boost::iequals(i->path().extension().string(), ".pem")) {
				ctx_.load_verify_file(i->path().string());
				Checkpoint(
						*checkpoint_handler,
						"ssl.loading_certificate_file",
						Attr_Communication(),
						CpAttr_info(),
						CpAttr("filename", i->path().string())
						);
			}
			++i;
		}
	}
	else {
		Checkpoint(
				*checkpoint_handler,
				"ssl.loading_certificate_file.path_not_found",
				Attr_Communication(),
				CpAttr_warning(),
				CpAttr("certificate_path", path.string())
				);
	}
}
