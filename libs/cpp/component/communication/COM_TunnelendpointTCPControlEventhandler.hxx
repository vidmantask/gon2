/*! \file COM_TunnelEndpointTCPControlEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling control signals from a tcp-tunnelendpoint
 */
#ifndef COM_TunnelendpointTCPControlEventhandler_HXX
#define COM_TunnelendpointTCPControlEventhandler_HXX

#include <component/communication/COM_TunnelendpointTCP.hxx>

namespace Giritech {
namespace Communication {


/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint connection
 *
 * Used for signal to TunnelendpointTCPTunnel instances for sending events to other side
 */
class TunnelendpointTCPControlEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint has recived a eof indicating that no more data are comming
     */
    virtual void tunnelendpoint_tcp_eh_eof(const unsigned long tunnel_id) = 0;


    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long tunnel_id) = 0;

};

}
}
#endif
