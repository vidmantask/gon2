/*! \file COM_SessionCrypterEventhandler.hxx
 \brief This file contains abstract interfaces for handling signals from the session crypter
 */
#ifndef COM_SessionCrypterEventhandler_HXX
#define COM_SessionCrypterEventhandler_HXX

namespace Giritech {
namespace Communication {

#include <lib/utility/UY_DataBuffer.hxx>

/*! \brief This class define the abstract interface for handling signals from the session crypter when operating in key-exchange-mode
 */
class SessionCrypterEventhandler : public boost::noncopyable {
public:

    /*! \brief Signal that the key-exchange is competed without errors
     */
    virtual void session_crypter_key_exchange_complete(void) = 0;

    /*! \brief Signal that the key-exchange failed
     */
    virtual void session_crypter_key_exchange_failed(const std::string& error_message) = 0;

    /*! \brief Signal that the raw tunnel has been closed
     */
    virtual void session_crypter_closed(void) = 0;

};

/*! \brief This class define the abstract interface for handling signals from the session crypter when operating in ready-mode
 */
class SessionCrypterEventhandlerReady : public boost::noncopyable {
public:

    /*! \brief Connected signal
     */
    virtual void session_crypter_recieve(const Utility::DataBuffer::APtr& data) = 0;

    /*! \brief Signal that the raw tunnel has been closed
     */
    virtual void session_crypter_closed(void) = 0;
};

}
}
#endif
