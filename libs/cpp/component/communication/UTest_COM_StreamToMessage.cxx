/*! \file UTest_COM_StreamToMessage.cxx
 *  \brief This file contains unittest suite for the stream-to-message functionality
 */
#include <string>
#include <iostream>

#include <boost/test/unit_test.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_StreamToMessage.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*!
 * \test Peeking buffer from stream having one chunk
 */
BOOST_AUTO_TEST_CASE( stream_to_message_one_chunk )
{
    DataBufferManaged::APtr data_chunck_01(DataBufferManaged::create("0123456789"));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data(data_chunck_01);

    BOOST_CHECK( stream_to_message->peek_available() == 10 );

    DataBufferManaged::APtr buffer;
    buffer = stream_to_message->peek_buffer(0, 1);
    BOOST_CHECK( buffer->toString() == "0");

    buffer = stream_to_message->peek_buffer(0, 5);
    BOOST_CHECK( buffer->toString() == "01234");

    buffer = stream_to_message->peek_buffer(0, 10);
    BOOST_CHECK( buffer->toString() == "0123456789");

    buffer = stream_to_message->peek_buffer(1, 1);
    BOOST_CHECK( buffer->toString() == "1");

    buffer = stream_to_message->peek_buffer(3, 5);
    BOOST_CHECK( buffer->toString() == "34567");

    buffer = stream_to_message->peek_buffer(9, 1);
    BOOST_CHECK( buffer->toString() == "9");

    BOOST_CHECK_THROW(stream_to_message->peek_buffer(0, 11), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(1, 10), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(9, 2), StreamToMessage::ExceptionPeekError);
}

/*!
 * \test Peeking buffer from stream having two chunks
 */
BOOST_AUTO_TEST_CASE( stream_to_message_two_chunks )
{
    DataBufferManaged::APtr data_chunck_01(DataBufferManaged::create("0123456789"));
    DataBufferManaged::APtr data_chunck_02(DataBufferManaged::create("abcd"));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data(data_chunck_01);
    stream_to_message->push_data(data_chunck_02);

    BOOST_CHECK( stream_to_message->peek_available() == 14 );

    DataBufferManaged::APtr buffer;
    buffer = stream_to_message->peek_buffer(0, 10);
    BOOST_CHECK( buffer->toString() == "0123456789");

    buffer = stream_to_message->peek_buffer(0, 11);
    BOOST_CHECK( buffer->toString() == "0123456789a");

    buffer = stream_to_message->peek_buffer(0, 14);
    BOOST_CHECK( buffer->toString() == "0123456789abcd");

    buffer = stream_to_message->peek_buffer(9, 2);
    BOOST_CHECK( buffer->toString() == "9a");

    buffer = stream_to_message->peek_buffer(9, 5);
    BOOST_CHECK( buffer->toString() == "9abcd");

    buffer = stream_to_message->peek_buffer(10, 4);
    BOOST_CHECK( buffer->toString() == "abcd");

    buffer = stream_to_message->peek_buffer(13, 1);
    BOOST_CHECK( buffer->toString() == "d");

    BOOST_CHECK_THROW(stream_to_message->peek_buffer(0, 15), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(1, 14), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(9, 6), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(10, 5), StreamToMessage::ExceptionPeekError);
}

/*!
 * \test Peeking buffer from stream having many chunks
 */
BOOST_AUTO_TEST_CASE( stream_to_message_many_chunks )
{
    string facit;
    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    for(int idx = 1; idx < 10; ++idx) {
        DataBufferManaged::APtr data_chunck(DataBufferManaged::create(idx, 64+idx));
        facit = facit + data_chunck->toString();
        stream_to_message->push_data(data_chunck);
    }

    BOOST_CHECK( stream_to_message->peek_available() == 45 );

    DataBufferManaged::APtr buffer;
    buffer = stream_to_message->peek_buffer(0, stream_to_message->peek_available());
    BOOST_CHECK( buffer->toString() == facit);

    for(int size = 1; size <= stream_to_message->peek_available(); ++size) {
        for(int idx = 0; idx < stream_to_message->peek_available() - size; ++idx) {
            buffer = stream_to_message->peek_buffer(idx, size);
            BOOST_CHECK( buffer->toString() == facit.substr(idx, size) );
        }
    }

    for(int idx = 0; idx < stream_to_message->peek_available(); ++idx) {
        BOOST_CHECK_THROW(stream_to_message->peek_buffer(idx, stream_to_message->peek_available() - idx + 1), StreamToMessage::ExceptionPeekError);
    }
}

/*!
 * \test Peeking byte from stream
 */
BOOST_AUTO_TEST_CASE( stream_peek_byte )
{
    string facit;
    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    for(int idx = 1; idx < 10; ++idx) {
        DataBufferManaged::APtr data_chunck(DataBufferManaged::create(idx, 64+idx));
        facit = facit + data_chunck->toString();
        stream_to_message->push_data(data_chunck);
    }

    for(int idx = 0; idx < stream_to_message->peek_available(); ++idx) {
        string my_byte(1, stream_to_message->peek_byte(idx));
        BOOST_CHECK( my_byte == facit.substr(idx, 1) );
    }

    BOOST_CHECK_THROW(stream_to_message->peek_byte(stream_to_message->peek_available() + 1), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_byte(stream_to_message->peek_available() + 20), StreamToMessage::ExceptionPeekError);
}

/*!
 * \test Consume simple
 */
BOOST_AUTO_TEST_CASE( consume_simple )
{
    DataBufferManaged::APtr data_chunck_01(DataBufferManaged::create("0123456789"));
    DataBufferManaged::APtr data_chunck_02(DataBufferManaged::create("abcd"));
    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data(data_chunck_01);
    stream_to_message->push_data(data_chunck_02);

    BOOST_CHECK(stream_to_message->peek_available() == 14);
    BOOST_CHECK_THROW(stream_to_message->consume(stream_to_message->peek_available() + 1), StreamToMessage::ExceptionPeekError);

    stream_to_message->consume(1);
    BOOST_CHECK(stream_to_message->peek_available() == 13);
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0) ) == "1");
    BOOST_CHECK( stream_to_message->peek_buffer(0, 13)->toString() == "123456789abcd");

    stream_to_message->consume(8);
    BOOST_CHECK(stream_to_message->peek_available() == 5);
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0) ) == "9");
    BOOST_CHECK( stream_to_message->peek_buffer(0, 5)->toString() == "9abcd");

    stream_to_message->consume(1);
    BOOST_CHECK(stream_to_message->peek_available() == 4);
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0) ) == "a");
    BOOST_CHECK( stream_to_message->peek_buffer(0, 4)->toString() == "abcd");

    stream_to_message->consume(2);
    BOOST_CHECK(stream_to_message->peek_available() == 2);
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0) ) == "c");
    BOOST_CHECK( stream_to_message->peek_buffer(0, 2)->toString() == "cd");

    stream_to_message->consume(2);
    BOOST_CHECK(stream_to_message->peek_available() == 0);
    BOOST_CHECK(stream_to_message->empty());
    BOOST_CHECK_THROW(stream_to_message->peek_byte(0), StreamToMessage::ExceptionPeekError);
    BOOST_CHECK_THROW(stream_to_message->peek_buffer(0,1), StreamToMessage::ExceptionPeekError);
    
    BOOST_CHECK_THROW(stream_to_message->consume(1), StreamToMessage::ExceptionPeekError);
}

/*!
 * \test Less simple consume 
 */
BOOST_AUTO_TEST_CASE( consume_less_simple )
{
    string facit;
    unsigned long facit_consumed = 0;
    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    for(int idx = 1; idx < 10; ++idx) {
        DataBufferManaged::APtr data_chunck(DataBufferManaged::create(idx, 64+idx));
        facit += data_chunck->toString();
        stream_to_message->push_data(data_chunck);
    }

    facit_consumed += 2;
    stream_to_message->consume(2);
    BOOST_CHECK( stream_to_message->peek_buffer(0, stream_to_message->peek_available())->toString() == facit.substr(facit_consumed, stream_to_message->peek_available()) );
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0)) == facit.substr(facit_consumed, 1) );

    facit_consumed += 10;
    stream_to_message->consume(10);
    BOOST_CHECK( stream_to_message->peek_buffer(0, stream_to_message->peek_available())->toString() == facit.substr(facit_consumed, stream_to_message->peek_available()) );
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0)) == facit.substr(facit_consumed, 1) );
    
    facit_consumed += 7;
    stream_to_message->consume(7);
    BOOST_CHECK( stream_to_message->peek_buffer(0, stream_to_message->peek_available())->toString() == facit.substr(facit_consumed, stream_to_message->peek_available()) );
    BOOST_CHECK( string(1, stream_to_message->peek_byte(0)) == facit.substr(facit_consumed, 1) );
}
boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
    return 0;
}
