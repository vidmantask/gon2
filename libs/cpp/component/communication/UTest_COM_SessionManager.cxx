/*! \file UTest_COM_SessionManager.cxx
 * \brief This file contains unittest suite for the session manager functionality
 */
#include <string>

#include <boost/asio.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_SessionManagerEventhandler.hxx>
#include <component/communication/COM_TrafficControlManager.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;

/*!
 *  Demo bussy/server
 */
class BussyServer: public RawTunnelendpointAcceptorTCPEventhandler, public AsyncContinuePolicy {
public:
    typedef boost::shared_ptr<BussyServer> APtr;

    BussyServer(const CheckpointHandler::APtr& checkpoint_handler,
                const AsyncService::APtr& async_service,
                const std::string& host,
                const unsigned long& port,
                const boost::posix_time::time_duration& close_delay) :
        async_service_(async_service), host_(host), port_(port), close_delay_(close_delay), close_timer_(async_service->get_io_service()) {
        acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler, async_service->get_io_service(), host_,
                                                         port_, this, this);
        acceptor_->set_option_reuse_address(true);
        acceptor_->aio_accept_start();
    }

    static APtr create(const CheckpointHandler::APtr& checkpoint_handler,
                       const AsyncService::APtr& async_service,
                       const std::string& host,
                       const unsigned long& port,
                       const boost::posix_time::time_duration& close_delay) {
        return APtr(new BussyServer(checkpoint_handler, async_service, host, port, close_delay));
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        tunnelendpoint_ = tunnelendpoint;
        close_timer_.expires_from_now(close_delay_);
        close_timer_.async_wait(boost::bind(&BussyServer::close_tunnelendpint, this, boost::asio::placeholders::error));
    }

    void close_tunnelendpint(const boost::system::error_code& error) {
        tunnelendpoint_->aio_close_start(true);
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    }
    void com_tunnelendpoint_acceptor_closed(void) {
    }

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void) {
        return false;
    }



    AsyncService::APtr async_service_;
    std::string host_;
    unsigned long port_;

    boost::posix_time::time_duration close_delay_;
    boost::asio::deadline_timer close_timer_;

    RawTunnelendpointAcceptorTCP::APtr acceptor_;
    RawTunnelendpointTCP::APtr tunnelendpoint_;
};

/*!
 *  Demo server
 */
class UTest_COM_InfoCollector {
public:
    typedef boost::shared_ptr<UTest_COM_InfoCollector> APtr;

    UTest_COM_InfoCollector(void) :
        message_recived_(0), message_send_(0), string_lock_(false) {
    }

    static APtr create(void) {
        return APtr(new UTest_COM_InfoCollector);
    }

    void add_message_send(const std::string& message) {
        message_send_++;

        bool expected = false;
        while (!string_lock_.compare_exchange_weak(expected, true));

        message_send_text_ += message;

        string_lock_ = false;
    }

    void add_message_recived(const std::string& message) {
        message_recived_++;

        bool expected = false;
        while (!string_lock_.compare_exchange_weak(expected, true));

        message_recived_text_ += message;

        string_lock_ = false;
    }

    static bool compare(const UTest_COM_InfoCollector::APtr& info_a, const UTest_COM_InfoCollector::APtr& info_b) {
        if ((info_a.get() == NULL) || (info_b.get() == NULL)) {
            return false;
        }
        return (info_a->message_recived_ == info_b->message_send_) && (info_a->message_recived_text_ == info_b->message_send_text_);
    }

    std::atomic<bool> string_lock_;
    std::atomic<unsigned long> message_recived_;
    std::atomic<unsigned long> message_send_;
    std::string message_recived_text_;
    std::string message_send_text_;
};

/*!
 *  Demo server
 */
class UTest_COM_SessionManager_Server: public SessionEventhandlerReady, public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_SessionManager_Server> APtr;

    UTest_COM_SessionManager_Server(boost::asio::io_service& asio_io_service, const CheckpointHandler::APtr& checkpoint_handler, const Session::APtr& session) :
        asio_io_service_(asio_io_service), session_(session) {
        session_->set_eventhandler_ready(this);
        tunnelendpoint_ = TunnelendpointReliableCrypted::create(asio_io_service, checkpoint_handler, this);
        session_->add_tunnelendpoint(1, tunnelendpoint_);
        info_collector_ = UTest_COM_InfoCollector::create();
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_ready(void) {
    }

    bool session_read_continue_state_ready(void) {
        return true;
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_closed(void) {
    }

    /*! \brief User defined signal from a tunnelendpoint
     */
    void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_key_exchange(void) {
    }

    void tunnelendpoint_eh_connected(void) {
    }

    /*! \brief Signals from TunnelendpointEventhandler
     */
    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        info_collector_->add_message_recived(message->get_buffer()->toString());
        if (message->get_buffer()->toString() == "exit") {
            send(MessagePayload::create(DataBufferManaged::create("Godbye")));
            session_->close(false);
        } else {
            send(message);
        }
    }

    void send(const MessagePayload::APtr& message) {
        info_collector_->add_message_send(message->get_buffer()->toString());
        tunnelendpoint_->tunnelendpoint_send(message);
    }

    unsigned long get_message_recived(void) const {
        return info_collector_->message_recived_;
    }

    static APtr create(boost::asio::io_service& asio_io_service, const CheckpointHandler::APtr& checkpoint_handler, const Session::APtr& session) {
        return APtr(new UTest_COM_SessionManager_Server(asio_io_service, checkpoint_handler, session));
    }

    UTest_COM_InfoCollector::APtr info_collector_;
    Session::APtr session_;
    Tunnelendpoint::APtr tunnelendpoint_;
    boost::asio::io_service& asio_io_service_;
};

/*!
 *  Demo server factory
 */
class UTest_COM_SessionManager_ServerFactory: public SessionManagerEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_SessionManager_ServerFactory> APtr;

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    void session_manager_resolve_connection(std::string& type,
                                            std::string& host,
                                            unsigned long& port,
                                            boost::posix_time::time_duration& timeout) {
        // signal ignored
    }
    void session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
        // signal ignored
    }
    void session_manager_connecting_failed(const unsigned long& connection_id) {
        // signal ignored
    }

    void session_manager_failed(const std::string& message) {
        // signal ignored
    }

    /*! \brief Signals from SessionManagerEventhandler
     */
    void session_manager_session_created(const unsigned long& connection_id, Session::APtr& session) {
        UTest_COM_SessionManager_Server::APtr server_session(UTest_COM_SessionManager_Server::create(asio_io_service_, checkpoint_handler_, session));
        session->set_eventhandler_ready(server_session.get());

        bool expected = false;
        while (!session_lock_.compare_exchange_weak(expected, true));

        sessions_.insert(make_pair(session->get_session_id(), server_session));

        session_lock_ = false;
    }

    /*! \brief Signals from SessionManagerEventhandler
     */
    void session_manager_session_closed(const unsigned long session_id) {

        bool expected = false;
        while (!session_lock_.compare_exchange_weak(expected, true));

        session_info_collections_.insert(make_pair(session_id, sessions_[session_id]->info_collector_));
        sessions_.erase(session_id);

        session_lock_ = false;
    }

    void session_manager_closed(void) {
        // signal ignored
    }

    void session_manager_security_dos_attack_start(const std::string& attacker_ips) {
        // signal ignored
    }

    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count)  {
        // signal ignored
    }

    static APtr create(boost::asio::io_service& asio_io_service, const CheckpointHandler::APtr& checkpoint_handler) {
        return APtr(new UTest_COM_SessionManager_ServerFactory(asio_io_service, checkpoint_handler));
    }

    UTest_COM_SessionManager_ServerFactory(boost::asio::io_service& asio_io_service, const CheckpointHandler::APtr& checkpoint_handler) :
        asio_io_service_(asio_io_service), checkpoint_handler_(checkpoint_handler){
    }

    CheckpointHandler::APtr checkpoint_handler_;
    map<unsigned long, UTest_COM_SessionManager_Server::APtr> sessions_;
    map<unsigned long, UTest_COM_InfoCollector::APtr> session_info_collections_;

    boost::asio::io_service& asio_io_service_;

    std::atomic<bool> session_lock_;
};

/*!
 *  Demo server
 */
class UTest_COM_SessionManager_Client: public SessionEventhandlerReady, public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_SessionManager_Client> APtr;

    UTest_COM_SessionManager_Client(boost::asio::io_service& asio_io_service,
                                    const CheckpointHandler::APtr& checkpoint_handler,
                                    const Session::APtr& session,
                                    const unsigned long message_to_send,
                                    const bool dissconect_after_last_message) :
   asio_io_service_(asio_io_service), session_(session), message_to_send_org_(message_to_send), message_to_send_(message_to_send),
                dissconect_after_last_message_(dissconect_after_last_message) {
        session_->set_eventhandler_ready(this);
        tunnelendpoint_ = TunnelendpointReliableCrypted::create(asio_io_service_, checkpoint_handler, this);
        session_->add_tunnelendpoint(1, tunnelendpoint_);
        info_collector_ = UTest_COM_InfoCollector::create();
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_ready(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_closed(void) {
    }

    /*! \brief User defined signal from a tunnelendpoint
     */
    void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_key_exchange(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    bool session_read_continue_state_ready(void) {
        return true;
    }

    void tunnelendpoint_eh_connected(void) {
        send(MessagePayload::create(DataBufferManaged::create("Hi there")));
        message_to_send_--;
        session_->read_start_state_ready();
    }

    /*! \brief Signals from TunnelendpointEventhandler
     */
    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        info_collector_->add_message_recived(message->get_buffer()->toString());

        unsigned long send_value = message_to_send_;

        while (!message_to_send_.compare_exchange_weak(send_value, send_value - 1));

        if (send_value == 1) {
            send(MessagePayload::create(DataBufferManaged::create("exit")));
        } else if (send_value > 0) {
            stringstream ss;
            ss << "Message :" << message_to_send_;
            send(MessagePayload::create(DataBufferManaged::create(ss.str())));
        } else if (dissconect_after_last_message_) {
            session_->close(true);
        }
    }

    void send(const MessagePayload::APtr& message) {
        info_collector_->add_message_send(message->get_buffer()->toString());
        tunnelendpoint_->tunnelendpoint_send(message);
    }

    static APtr create(boost::asio::io_service& asio_io_service,
                       const CheckpointHandler::APtr& checkpoint_handler,
                       const Session::APtr& session,
                       const unsigned long message_to_send,
                       const bool dissconect_after_last_message) {
        return APtr(new UTest_COM_SessionManager_Client(asio_io_service, checkpoint_handler, session, message_to_send,
                                                        dissconect_after_last_message));
    }

    unsigned long get_message_recived(void) const {
        return info_collector_->message_recived_;
    }

    boost::asio::io_service& asio_io_service_;
    Session::APtr session_;
    Tunnelendpoint::APtr tunnelendpoint_;
    unsigned long message_to_send_org_;
    std::atomic<unsigned long> message_to_send_;
    bool dissconect_after_last_message_;
    UTest_COM_InfoCollector::APtr info_collector_;
};

/*!
 *  Demo client factory
 */
class UTest_COM_SessionManager_ClientFactory: public SessionManagerEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_SessionManager_ClientFactory> APtr;

    /*! \brief Signals from SessionManagerEventhandler
     */
    void session_manager_resolve_connection(std::string& type,
                                            std::string& host,
                                            unsigned long& port,
                                            boost::posix_time::time_duration& timeout) {
        // signal ignored
    }


    void session_manager_session_created(const unsigned long& connection_id, Session::APtr& session) {
        UTest_COM_SessionManager_Client::APtr
                client_session(UTest_COM_SessionManager_Client::create(asio_io_service_, checkpoint_handler_, session, message_to_send_,
                                                                       dissconect_after_last_message_));

        session->set_eventhandler_ready(client_session.get());

        bool expected = false;
        while (!session_lock_.compare_exchange_weak(expected, true));

        sessions_.insert(make_pair(session->get_session_id(), client_session));

        session_lock_ = false;
        ++sessions_created_;
    }

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    void session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
        // signal ignored
    }
    void session_manager_connecting_failed(const unsigned long& connection_id) {
        sessions_failed_++;
    }

    void session_manager_failed(const std::string& message) {
        failed_to_connect_ = true;
    }

    /*! \brief Signals from SessionManagerEventhandler
     */
    void session_manager_session_closed(const unsigned long session_id) {

        bool expected = false;
        while (!session_lock_.compare_exchange_weak(expected, true));

        BOOST_CHECK( sessions_[session_id]->get_message_recived() == sessions_[session_id]->message_to_send_org_ );
        session_info_collections_.insert(make_pair(session_id, sessions_[session_id]->info_collector_));
        sessions_.erase(session_id);

        session_lock_ = false;
    }

    void session_manager_closed(void) {
        // signal ignored
    }

    void session_manager_security_dos_attack_start(const std::string& attacker_ips) {
        // signal ignored
    }

    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count)  {
        // signal ignored
    }

    static APtr create(boost::asio::io_service& asio_io_service,
                       const CheckpointHandler::APtr& checkpoint_handler,
                       const unsigned long message_to_send,
                       const bool dissconect_after_last_message) {
        return APtr(new UTest_COM_SessionManager_ClientFactory(asio_io_service, checkpoint_handler, message_to_send,
                                                               dissconect_after_last_message));
    }

    UTest_COM_SessionManager_ClientFactory(boost::asio::io_service& asio_io_service,
                                           const CheckpointHandler::APtr& checkpoint_handler,
                                           const unsigned long message_to_send,
                                           const bool dissconect_after_last_message) :
    asio_io_service_(asio_io_service), checkpoint_handler_(checkpoint_handler), message_to_send_(message_to_send),
                dissconect_after_last_message_(dissconect_after_last_message), failed_to_connect_(false), sessions_created_(0), sessions_failed_(0), session_lock_(false) {

    }
    CheckpointHandler::APtr checkpoint_handler_;

    map<unsigned long, UTest_COM_SessionManager_Client::APtr> sessions_;
    map<unsigned long, UTest_COM_InfoCollector::APtr> session_info_collections_;

    std::atomic<bool> failed_to_connect_;
    const unsigned long message_to_send_;
    const bool dissconect_after_last_message_;

    std::atomic<long> sessions_created_;
    std::atomic<long> sessions_failed_;
    boost::asio::io_service& asio_io_service_;

    std::atomic<bool> session_lock_;
};

void testsuite_compare_sessions(const UTest_COM_SessionManager_ServerFactory& server_factory, const vector< UTest_COM_SessionManager_ClientFactory::APtr>& client_factorys) {

    BOOST_CHECK( server_factory.session_info_collections_.size() == client_factorys.size());

    int idx = 0;

    for (const auto& pair : server_factory.session_info_collections_) {
        BOOST_CHECK(UTest_COM_InfoCollector::compare(pair.second, client_factorys.at(idx)->session_info_collections_[0]));
        ++idx;
    }
}


void testsuite_wait_and_stop(std::vector<AsyncService::APtr>& async_services, SessionManagerServer::APtr& session_manager_server) {
    boost::this_thread::sleep(boost::posix_time::seconds(5));

    while(session_manager_server->get_session_count() > 0) {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
    }

    cout << "Server session count is 0" << endl;
    for (const AsyncService::APtr& service : async_services) {
    	service->stop();
    }
    cout << "joining threads" << endl;
}

/* Create checkpoint handler */
CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
CheckpointHandler::APtr checkpoint_handler_client(CheckpointHandler::create_cout_all());

/*
 *!/brief Session manager timeout test
 *
 * A number of clients are connecting to the same server, each sending 100 messages and recives 100 messages from server.
 * When the last message has been recived by a client just keep the connection open and waits.
 * The server session manger now timeout the clients, and closes whend the last client has been disconnected.
 *
 */
BOOST_AUTO_TEST_CASE( session_manager_timeout_test )
{
    boost::thread_group threads;
    std::vector<AsyncService::APtr> async_services;

    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
    async_services.push_back(async_service);

    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);


    std::string server_ip("127.0.0.1");
    unsigned long server_port = 8045;
    std::string sid("1");

    UTest_COM_SessionManager_ServerFactory server_factory(async_service->get_io_service(), checkpoint_handler);
    SessionManagerServer::APtr session_manager_server(SessionManagerServer::create(async_service, checkpoint_handler,
                                                                                   known_secret_server, server_ip,
                                                                                   server_port, sid));
    session_manager_server->set_eventhandler(&server_factory);
    session_manager_server->set_config_idle_timeout(boost::posix_time::seconds(20), boost::posix_time::seconds(20));
    session_manager_server->set_config_close_when_no_more_sessions(true);
    session_manager_server->set_option_reuse_address(true);
    session_manager_server->start();
    threads.create_thread(boost::bind(&AsyncService::run, async_service));

    vector<UTest_COM_SessionManager_ClientFactory::APtr> client_factorys;
    vector<SessionManagerClient::APtr> client_session_managers;

    for (int idx = 0; idx < 10; ++idx) {
        AsyncService::APtr client_async_service(AsyncService::create(checkpoint_handler));
        async_services.push_back(client_async_service);

        UTest_COM_SessionManager_ClientFactory::APtr
                client_factory(UTest_COM_SessionManager_ClientFactory::create(client_async_service->get_io_service(), checkpoint_handler, 20, false));
        SessionManagerClient::APtr client_session_manager(SessionManagerClient::create(client_async_service,
                                                                                       checkpoint_handler,
                                                                                       known_secret_client));

        client_session_manager->set_eventhandler(client_factory.get());
        client_session_manager->add_gserver_connection(server_ip, server_port);
        client_session_manager->start();
        threads.create_thread(boost::bind(&AsyncService::run, client_async_service));

        client_factorys.push_back(client_factory);
        client_session_managers.push_back(client_session_manager);
    }
    testsuite_wait_and_stop(async_services, session_manager_server);

    threads.join_all();
    testsuite_compare_sessions(server_factory, client_factorys);

    TrafficControlManager::destroy_global();
}

/*
 *!/brief Session manager connect and dissconect
 *
 * Same test as above, but clients disconnect when they have recived the last message.
 *
 */
BOOST_AUTO_TEST_CASE( session_manager_connect_and_disconnect )
{
    boost::thread_group threads;

    std::vector<AsyncService::APtr> async_services;

    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
    async_services.push_back(async_service);

    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);

    std::string server_ip("127.0.0.1");
    unsigned long server_port = 8046;
    std::string sid("1");

    UTest_COM_SessionManager_ServerFactory server_factory(async_service->get_io_service(), checkpoint_handler);
    SessionManagerServer::APtr session_manager_server(SessionManagerServer::create(async_service, checkpoint_handler,
                                                                                   known_secret_server, server_ip,
                                                                                   server_port, sid));
    session_manager_server->set_eventhandler(&server_factory);
    session_manager_server->set_config_idle_timeout(boost::posix_time::seconds(20), boost::posix_time::seconds(20));
    session_manager_server->set_config_close_when_no_more_sessions(true);
    session_manager_server->set_option_reuse_address(true);
    session_manager_server->start();
    threads.create_thread(boost::bind(&AsyncService::run, async_service));

    vector<UTest_COM_SessionManager_ClientFactory::APtr> client_factorys;
    vector<SessionManagerClient::APtr> client_session_managers;

    for (int idx = 0; idx < 10; ++idx) {
        AsyncService::APtr client_async_service(AsyncService::create(checkpoint_handler));
        async_services.push_back(client_async_service);

        UTest_COM_SessionManager_ClientFactory::APtr
                client_factory(UTest_COM_SessionManager_ClientFactory::create(client_async_service->get_io_service(), checkpoint_handler, 20, true));
        SessionManagerClient::APtr client_session_manager(SessionManagerClient::create(client_async_service,
                                                                                       checkpoint_handler_client,
                                                                                       known_secret_client));

        client_session_manager->set_eventhandler(client_factory.get());
        client_session_manager->add_gserver_connection(server_ip, server_port);
        client_session_manager->start();
        threads.create_thread(boost::bind(&AsyncService::run, client_async_service));

        client_factorys.push_back(client_factory);
        client_session_managers.push_back(client_session_manager);
    }

    testsuite_wait_and_stop(async_services, session_manager_server);

    threads.join_all();

    testsuite_compare_sessions(server_factory, client_factorys);

    TrafficControlManager::destroy_global();
}

/*
 *!/brief Client session manager no server test
 *
 */
BOOST_AUTO_TEST_CASE( client_session_manager_no_server_test )
{


	AsyncService::APtr client_async_service(AsyncService::create(checkpoint_handler));
    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);

    UTest_COM_SessionManager_ClientFactory::APtr
            client_factory(UTest_COM_SessionManager_ClientFactory::create(client_async_service->get_io_service(), checkpoint_handler, 20, false));
    SessionManagerClient::APtr client_session_manager(SessionManagerClient::create(client_async_service,
                                                                                   checkpoint_handler,
                                                                                   known_secret_client));

    const char servers_spec[] = "<?xml version='1.0'?>"
        "<servers>"
        "  <connection_group title='Group_1' selection_delay_sec='1'>"
        "    <connection title='Connection_11' host='127.0.0.1' port='8045' timeout_sec='11' type='direct' />"
        "    <connection title='Connection_12' host='127.0.0.2' port='8046' timeout_sec='12' type='direct' />"
        "  </connection_group>"
        "  <connection_group title='Group_2' selection_delay_sec='2'>"
        "    <connection title='Connection_21' host='127.0.2.41' port='8021' timeout_sec='21' type='direct' />"
        "    <connection title='Connection_22' host='127.0.2.42' port='8022' timeout_sec='22' type='http' />"
        "  </connection_group>"
        "</servers>";

    client_session_manager->set_eventhandler(client_factory.get());
    client_session_manager->set_servers(servers_spec);

    client_session_manager->start();

    boost::thread_group threads;
    threads.create_thread(boost::bind(&AsyncService::run, client_async_service));

    boost::this_thread::sleep(boost::posix_time::seconds(2));
    while(client_factory->sessions_failed_ < 4) {
        boost::this_thread::sleep(boost::posix_time::seconds(1));
        cout << "sessions_failed:" << client_factory->sessions_failed_ << endl;
    }
    client_async_service->stop();

    BOOST_CHECK( client_factory->failed_to_connect_ == true);

    TrafficControlManager::destroy_global();
}

/*
 *!/brief Client session manager try to find a server
 *
 */
BOOST_AUTO_TEST_CASE( client_session_manager_find_the_server_test )
{
    boost::thread_group threads;
    std::vector<AsyncService::APtr> async_services;

    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
    async_services.push_back(async_service);

    AsyncService::APtr client_async_service(AsyncService::create(checkpoint_handler));
    async_services.push_back(client_async_service);

    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);

    std::string server_ip("127.0.0.1");
    unsigned long server_port = 8047;
    std::string sid("1");

    UTest_COM_SessionManager_ServerFactory server_factory(async_service->get_io_service(), checkpoint_handler);
    SessionManagerServer::APtr session_manager_server(SessionManagerServer::create(async_service, checkpoint_handler,
                                                                                   known_secret_server, server_ip,
                                                                                   server_port, sid));
    session_manager_server->set_eventhandler(&server_factory);
    session_manager_server->set_config_idle_timeout(boost::posix_time::seconds(20), boost::posix_time::seconds(20));
    session_manager_server->set_config_close_when_no_more_sessions(true);
    session_manager_server->set_option_reuse_address(true);
    session_manager_server->start();


    UTest_COM_SessionManager_ClientFactory::APtr
            client_factory(UTest_COM_SessionManager_ClientFactory::create(async_service->get_io_service(), checkpoint_handler, 20, false));
    SessionManagerClient::APtr client_session_manager(SessionManagerClient::create(async_service,
                                                                                   checkpoint_handler,
                                                                                   known_secret_client));

    BussyServer::APtr bussy_server_01(BussyServer::create(checkpoint_handler, async_service, "127.0.0.1", 8001, boost::posix_time::seconds(20)));
    BussyServer::APtr bussy_server_02(BussyServer::create(checkpoint_handler, async_service, "127.0.0.2", 8002, boost::posix_time::seconds(20)));
    BussyServer::APtr bussy_server_03(BussyServer::create(checkpoint_handler, async_service, "127.0.0.3", 8003, boost::posix_time::seconds(20)));

    const char servers_spec[] = "<?xml version='1.0'?>"
        "<servers>"
        "  <connection_group title='Group_1' selection_delay_sec='2'>"
        "    <connection title='Connection_01' host='127.0.0.1' port='8001' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_02' host='127.0.0.2' port='8002' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_03' host='127.0.0.3' port='8003' timeout_sec='5' type='direct' />"
        "  </connection_group>"
        "  <connection_group title='Group_2' selection_delay_sec='0'>"
        "    <connection title='Connection_04' host='127.0.0.1' port='8047' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_05' host='127.0.0.1' port='8047' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_06' host='127.0.0.1' port='8047' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_07' host='127.0.0.1' port='8047' timeout_sec='5' type='direct' />"
        "    <connection title='Connection_08' host='127.0.0.1' port='8047' timeout_sec='5' type='direct' />"
        "  </connection_group>"
        "</servers>";

    client_session_manager->set_eventhandler(client_factory.get());
    client_session_manager->set_servers(servers_spec);
    client_session_manager->start();

    threads.create_thread(boost::bind(&AsyncService::run, async_service));
    threads.create_thread(boost::bind(&AsyncService::run, client_async_service));

    boost::this_thread::sleep(boost::posix_time::seconds(5));
    testsuite_wait_and_stop(async_services, session_manager_server);

    BOOST_CHECK( client_factory->failed_to_connect_ == false);
    BOOST_CHECK( client_factory->sessions_created_ == 1);

    TrafficControlManager::destroy_global();
}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
