/*! \file COM_API_RawTunnelendpointAcceptor.cxx
 *  \brief This file contains the implementation of the API wrapper for raw-tcp-tunnelendpoint acceptor
 */
// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>


#include <boost/asio.hpp>
#include <component/communication/COM_API_RawTunnelendpointAcceptor.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APIRawTunnelendpointAcceptorTCP implementation
 * ------------------------------------------------------------------
 */
APIRawTunnelendpointAcceptorTCP::APIRawTunnelendpointAcceptorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                 const APIAsyncService::APtr& async_service,
                                                                 const std::string& listen_host,
                                                                 const unsigned long& listen_port) :
    api_eventhandler_(NULL), impl_tunnelendpoint_(RawTunnelendpointAcceptorTCP::create(checkpoint_handler, async_service->get_io_service(),
                                                                                       listen_host, listen_port, this,
                                                                                       this)) {
}

APIRawTunnelendpointAcceptorTCP::~APIRawTunnelendpointAcceptorTCP(void) {
}

APIRawTunnelendpointAcceptorTCP::APtr APIRawTunnelendpointAcceptorTCP::create(const Utility::APICheckpointHandler::APtr& wrap_checkpoint_handler,
                                                                              const APIAsyncService::APtr& async_service,
                                                                              const std::string& listen_host,
                                                                              const unsigned long& listen_port) {
    return APIRawTunnelendpointAcceptorTCP::APtr(new APIRawTunnelendpointAcceptorTCP(wrap_checkpoint_handler->get_checkpoint_handler(), async_service, listen_host, listen_port));
}

void APIRawTunnelendpointAcceptorTCP::set_tcp_eventhandler(APIRawTunnelendpointAcceptorTCPEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
}

void APIRawTunnelendpointAcceptorTCP::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_eventhandler();
    api_eventhandler_ = NULL;
}

void APIRawTunnelendpointAcceptorTCP::set_option_reuse_address(const bool& option_reuse_address) {
    impl_tunnelendpoint_->set_option_reuse_address(option_reuse_address);
}

void APIRawTunnelendpointAcceptorTCP::aio_accept_start(void) {
    impl_tunnelendpoint_->aio_accept_start();
}

void APIRawTunnelendpointAcceptorTCP::aio_close_start(void) {
    impl_tunnelendpoint_->aio_close_start();
}

bool APIRawTunnelendpointAcceptorTCP::is_closed(void) const {
    return impl_tunnelendpoint_->is_closed();
}

APIMutex::APtr APIRawTunnelendpointAcceptorTCP::get_mutex(void) {
    return APIMutex::create(impl_tunnelendpoint_->get_mutex());
}

std::pair<std::string, unsigned long> APIRawTunnelendpointAcceptorTCP::get_ip_local(void) const {
    return impl_tunnelendpoint_->get_ip_local();
}

void APIRawTunnelendpointAcceptorTCP::set_mutex(const APIMutex::APtr& api_mutex) {
    impl_tunnelendpoint_->set_mutex(api_mutex->get_impl());
}

void APIRawTunnelendpointAcceptorTCP::self_set_option_reuse_address(const APtr& self, const bool& option_reuse_address) {
    assert(self.get() != NULL);
    self->set_option_reuse_address(option_reuse_address);
}

void APIRawTunnelendpointAcceptorTCP::self_reset_tcp_eventhandler(const APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

void APIRawTunnelendpointAcceptorTCP::self_set_tcp_eventhandler(const APtr& self, APIRawTunnelendpointAcceptorTCPEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APIRawTunnelendpointAcceptorTCP::self_aio_accept_start(const APtr& self) {
    assert(self.get() != NULL);
    self->aio_accept_start();
}

void APIRawTunnelendpointAcceptorTCP::self_aio_close_start(const APtr& self) {
    assert(self.get() != NULL);
    self->aio_close_start();
}

bool APIRawTunnelendpointAcceptorTCP::self_is_closed(const APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}

APIMutex::APtr APIRawTunnelendpointAcceptorTCP::self_get_mutex(const APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}

void APIRawTunnelendpointAcceptorTCP::self_set_mutex(const APtr& self, const APIMutex::APtr& api_mutex) {
    assert(self.get() != NULL);
    self->set_mutex(api_mutex);
}

void APIRawTunnelendpointAcceptorTCP::com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& raw_tunnelendpoint) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_tunnelendpoint_accepted(APIRawTunnelendpointTCP::create(raw_tunnelendpoint));
    }
}

void APIRawTunnelendpointAcceptorTCP::com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_tunnelendpoint_acceptor_error(error_message);
    }
}
void APIRawTunnelendpointAcceptorTCP::com_tunnelendpoint_acceptor_closed(void) {
	// ignored for now
}

bool APIRawTunnelendpointAcceptorTCP::com_accept_continue(void) {
    if (api_eventhandler_ != NULL) {
        return api_eventhandler_->com_accept_continue();
    }
    return false;
}

boost::python::tuple APIRawTunnelendpointAcceptorTCP::self_get_ip_local(const APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_local());
    return boost::python::make_tuple(ip.first, ip.second);
}
