/*! \file COM_API_TunnelendpointTCP.hxx
 *  \brief This file contains the a API wrapper for tcp-tunnelendpoints
 */
#ifndef COM_API_TunnelendpointTCP_HXX
#define COM_API_TunnelendpointTCP_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointTCP.hxx>
#include <component/communication/COM_TunnelendpointTCPEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler for a tcp-tunnelendpoint connection
 */
class APITunnelendpointTCPEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint has connecde to the 'other' side and is ready to send/receive packages
     */
    virtual void tunnelendpoint_tcp_eh_ready(void) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(void) = 0;
};

/*! \brief TCP tunnelendpoint connection
 */
class APITunnelendpointTCP: public TunnelendpointTCPEventhandler {
public:
    typedef boost::shared_ptr<APITunnelendpointTCP> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointTCP(void);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APITunnelendpointTCPEventhandler* api_eventhandler);

    /*! \brief reset eventhandler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief Close tcp tunnelendpoint
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    void close(const bool force);

    /*! \brief Start reading
     */
    void read_start(void);

    /*! \brief Geters of ip/port of local and remote part of socket
     */
    std::pair<std::string, unsigned long> get_ip_local(void) const;
    std::pair<std::string, unsigned long> get_ip_remote(void) const;

    /*! \brief Implementation TunnelendpointTCPEventhandler event
     */
    virtual void tunnelendpoint_tcp_eh_ready(void);

    /*! \brief Implementation TunnelendpointTCPEventhandler event
     */
    virtual void tunnelendpoint_tcp_eh_closed(void);

    /*! \brief create instance
     */
    static APtr create(const TunnelendpointTCP::APtr& tunnelendpoint);

    static void self_set_tcp_eventhandler(const APITunnelendpointTCP::APtr& self,
                                          APITunnelendpointTCPEventhandler* api_eventhandler);
    static void self_reset_tcp_eventhandler(const APITunnelendpointTCP::APtr& self);
    static void self_close(const APITunnelendpointTCP::APtr& self, const bool& force);
    static void self_read_start(const APITunnelendpointTCP::APtr& self);
    static boost::python::tuple self_get_ip_remote(const APITunnelendpointTCP::APtr& self);
    static boost::python::tuple self_get_ip_local(const APITunnelendpointTCP::APtr& self);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointTCP(const TunnelendpointTCP::APtr& tunnelendpoint);

    TunnelendpointTCP::APtr impl_tunnelendpoint_;
    APITunnelendpointTCPEventhandler* api_eventhandler_;

};


/*! \brief This class define the abstract eventhandler for a client tunnel tcp-tunnelendpoint
 */
class APITunnelendpointTCPTunnelClientEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_tcp_eh_ready(void) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(void) = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_tcp_eh_recieve(const std::string& message) = 0;

    /*! \brief Signals that a connection has been created.
     *
     * If true is returned the connection will be connected to the serverside,
     * else will the connection be closed and ignored
     */
    virtual bool tunnelendpoint_tcp_eh_connected(const APITunnelendpointTCP::APtr& connection) = 0;

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void tunnelendpoint_tcp_eh_acceptor_error(const std::string& error_message) = 0;

    /*! \brief Return true if the accept_start should continue after a connection has been accepted.

     If False the the accept_start should be called again to start accepting connection.
     If no eventhandler is defined then the default value is true.
     */
    virtual bool tunnelendpoint_tcp_eh_accept_start_continue(void) = 0;
};


class APITunnelendpointTCPTunnelClient:	public TunnelendpointTCPTunnelClientEventhandler {
public:
    typedef boost::shared_ptr<APITunnelendpointTCPTunnelClient> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointTCPTunnelClient(void);

    /*! \brief set tcp option result_address
     */
    void set_option_reuse_address(const bool& option_reuse_address);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APITunnelendpointTCPTunnelClientEventhandler* api_eventhandler);

    /*! \brief set eventhandler
     */
    void reset_tcp_eventhandler(void);


    /*! \brief Return true if the tunnelendpoint is closed
     */
    bool is_closed(void) const;

    /*! \brief Return local endpoint info of acceptor
     */
    std::pair<std::string, unsigned long> get_ip_local(void) const;

    /*! \brief Returns tunnelendpoint implemntation
     */
    virtual Tunnelendpoint::APtr get_impl(void);

    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_tcp_eh_ready(const unsigned long id);

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long id);

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message);

    /*! \brief Signals that a connection has been created.
     *
     * If true is returned the connection will be accepted and connected to the serverside,
     * else will the connection be closed and ignored
     */
    virtual bool tunnelendpoint_tcp_eh_connected(const unsigned long id, const TunnelendpointTCP::APtr& connection);

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void tunnelendpoint_tcp_eh_acceptor_error(const unsigned long id, const std::string& error_message);

    /*! \brief Return true if the accept_start should continue after a connection has been accepted.

     If False the the accept_start should be called again to start accepting connection.
     If no eventhandler is defined then the default value is true.
     */
    virtual bool tunnelendpoint_tcp_eh_accept_start_continue(const unsigned long id);

    /*! \brief create instance
     */
    static APtr create(const APIAsyncService::APtr& async_service,
                       const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                       const std::string& listen_ip,
                       const unsigned long& listen_port);

    static void self_set_tcp_eventhandler(const APITunnelendpointTCPTunnelClient::APtr& self,
                                          APITunnelendpointTCPTunnelClientEventhandler* api_eventhandler);
    static void self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelClient::APtr& self);


    static bool self_is_closed(const APITunnelendpointTCPTunnelClient::APtr& self);

    static boost::python::tuple self_get_ip_local(const APITunnelendpointTCPTunnelClient::APtr& self);

    static void self_set_option_reuse_address(const APITunnelendpointTCPTunnelClient::APtr& self, const bool& option_reuse_address);


    /*! \brief Start the accepting connections
     */
    static void self_accept_start(const APITunnelendpointTCPTunnelClient::APtr self);
    void accept_start(void);


    /*! \brief Close tunnelendpoint and all its children connections
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    static void self_close(const APITunnelendpointTCPTunnelClient::APtr self, const bool& force);
    void close_decoubled(const bool force);


    /*! \brief Sends a message
     */
    static void self_tunnelendpoint_send(const APITunnelendpointTCPTunnelClient::APtr self, const std::string& message);
    void tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message);


    boost::asio::io_service& get_io(void);
    boost::asio::io_service::strand& get_strand(void);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointTCPTunnelClient(const APIAsyncService::APtr& async_service,
                                     const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                     const std::string& listen_ip,
                                     const unsigned long& listen_port);

    TunnelendpointTCPTunnelClient::APtr impl_tunnelendpoint_;
    APITunnelendpointTCPTunnelClientEventhandler* api_eventhandler_;
    boost::asio::io_service& io_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
};



/*! \brief This class define the abstract eventhandler for a server tunnel tcp-tunnelendpoint
 */
class APITunnelendpointTCPTunnelServerEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_tcp_eh_ready(void) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(void) = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_tcp_eh_recieve(const std::string& message) = 0;
};

class APITunnelendpointTCPTunnelServer:	public TunnelendpointTCPTunnelServerEventhandler {
public:
    typedef boost::shared_ptr<APITunnelendpointTCPTunnelServer> APtr;

    /*! \brief Destructor
     */
    virtual ~APITunnelendpointTCPTunnelServer(void);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APITunnelendpointTCPTunnelServerEventhandler* api_eventhandler);

    /*! \brief reset eventhandler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief Return true if the tunnelendpoint is closed
     */
    bool is_closed(void) const;

    /*! \brief Returns tunnelendpoint implemntation
     */
    virtual Tunnelendpoint::APtr get_impl(void);

    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_tcp_eh_ready(const unsigned long id);

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long id);

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& payload);

    /*! \brief create instance
     */
    static APtr create(const APIAsyncService::APtr& async_service,
                       const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                       const std::string& server_ip,
                       const unsigned long& server_port);

    static void self_set_tcp_eventhandler(const APITunnelendpointTCPTunnelServer::APtr& self,
                                          APITunnelendpointTCPTunnelServerEventhandler* api_eventhandler);
    static void self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelServer::APtr& self);

    static bool self_is_closed(const APITunnelendpointTCPTunnelServer::APtr& self);



    /*! \brief Sends a message
     */
    static void self_tunnelendpoint_send(const APITunnelendpointTCPTunnelServer::APtr self, const std::string& message);
    void tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message);


    /*! \brief Close tunnelendpoint and all its children connections
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    static void self_close(const APITunnelendpointTCPTunnelServer::APtr self, const bool& force);
    void close_decoubled(const bool force);



    boost::asio::io_service& get_io(void);
    boost::asio::io_service::strand& get_strand(void);

protected:

    /*! \brief Constructor
     */
    APITunnelendpointTCPTunnelServer(const APIAsyncService::APtr& async_service,
                                     const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                     const std::string& server_ip,
                                     const unsigned long& server_port);

    TunnelendpointTCPTunnelServer::APtr impl_tunnelendpoint_;
    APITunnelendpointTCPTunnelServerEventhandler* api_eventhandler_;
    boost::asio::io_service& io_;
    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
};




/*! \brief This class define the abstract eventhandler for a server tunnel tcp-tunnelendpoint
 */
class APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler:
	 public boost::noncopyable {
public:
	/*! \brief Signals that the tunnelendpoint is ready to handle traffic.
	 *
	 * The signal is send when the tunnelendpoint is connected to the 'other' side,
	 * and is ready to handle traffic connections.
	 */
	virtual void tunnelendpoint_tcp_eh_ready(void) = 0;

	/*! \brief Signals that the tunnelendpoint has been closed
	 *
	 * After this call the eventhandler is never called again
	 */
	virtual void tunnelendpoint_tcp_eh_closed(void) = 0;

	/*! \brief Signals that the tunnelendpoint has recieved a message
	 *
	 *  \param message Message recived
	 */
	virtual void tunnelendpoint_tcp_eh_recieve(const std::string& message) = 0;

	/*! \brief Signals that the remote wants to connect to a child.
	 */
	virtual void tunnelendpoint_tcp_eh_recieve_remote_connect(const unsigned long id) = 0;

	/*! \brief Signals that a remote child request close.
	 */
	virtual void tunnelendpoint_tcp_eh_recieve_remote_close(const unsigned long id) = 0;

	/*! \brief Signals that a remote child has recived eof.
	 */
	virtual void tunnelendpoint_tcp_eh_recieve_remote_eof(const unsigned long id) = 0;

	/*! \brief Signals that a remote child has closed.
	 */
	virtual void tunnelendpoint_tcp_eh_recieve_remote_closed(const unsigned long id) = 0;
};


class APITunnelendpointTCPTunnelServerWithConnectionEngine:	public TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler {
public:
	typedef boost::shared_ptr<APITunnelendpointTCPTunnelServerWithConnectionEngine> APtr;

	/*! \brief Destructor
	 */
	virtual ~APITunnelendpointTCPTunnelServerWithConnectionEngine(void);

	/*! \brief set eventhandler
	 */
	void set_tcp_eventhandler(APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* api_eventhandler);

	/*! \brief reset eventhandler
	 */
	void reset_tcp_eventhandler(void);


	/*! \brief Return true if the tunnelendpoint is closed
	 */
	bool is_closed(void) const;

	/*! \brief Returns tunnelendpoint implemntation
	 */
	virtual Tunnelendpoint::APtr get_impl(void);

    /*! \brief Add child tunnelendpoint
     */
    void add_tunnelendpoint(const unsigned long& child_id, const APITunnelendpoint::APtr& tunnelendpoint);
    void add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint);

	/*! Implementation of TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler
	 */
	virtual void tunnelendpoint_tcp_eh_ready(const unsigned long id);
	virtual void tunnelendpoint_tcp_eh_closed(const unsigned long id);
	virtual void tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& payload);
    virtual void tunnelendpoint_tcp_eh_recieve_remote_connect(const unsigned long id);
    virtual void tunnelendpoint_tcp_eh_recieve_remote_close(const unsigned long id);
    virtual void tunnelendpoint_tcp_eh_recieve_remote_eof(const unsigned long id);
    virtual void tunnelendpoint_tcp_eh_recieve_remote_closed(const unsigned long id);

	/*! \brief create instance
	 */
	static APtr create(
			const APIAsyncService::APtr& async_service,
			const Utility::APICheckpointHandler::APtr& checkpoint_handler);

	static void	self_set_tcp_eventhandler(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
			APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* api_eventhandler);

	static void self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self);

	static bool self_is_closed(const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self);

	static void self_add_tunnelendpoint_reliable_crypted(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
			const unsigned long& child_id,
			const APITunnelendpointReliableCrypted::APtr& tunnelendpoint);

	static void self_add_tunnelendpoint_reliable_crypted_tunnel(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
			const unsigned long& child_id,
			const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint);


	/*! \brief Close tunnelendpoint and all its children connections
	 *
	 *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
	 */
	static void self_close(const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self, const bool& force);
	void close_decoubled(const bool force);

	/*! \brief Sends a message
	 */
	static void	self_tunnelendpoint_send(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const std::string& message);
	void tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message);


	/*! \brief Sends eof/close/closed signal to remote child
	 */
	static void self_tunnelendpoint_send_remote_eof(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long tunnel_id);
	void tunnelendpoint_send_remote_eof_decoubled(const unsigned long tunnel_id);

	static void self_tunnelendpoint_send_remote_close(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long tunnel_id);
	void tunnelendpoint_send_remote_close_decoubled(const unsigned long tunnel_id);

	static void self_tunnelendpoint_send_remote_closed(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long tunnel_id);
	void tunnelendpoint_send_remote_closed_decoubled(const unsigned long tunnel_id);


    /*! \brief Remove child tunnelendpoint
     */
	static void self_remove_tunnelendpoint(
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long& child_id);
    void remove_tunnelendpoint_decoubled(const unsigned long child_id);


	/*! \brief Sends recieve_connect responses to remote
	 */
	static void self_tunnelendpoint_send_remote_recieve_connect_ok (
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long tunnel_id);
	virtual void tunnelendpoint_send_remote_recieve_connect_ok_decoubled(const unsigned long tunnel_id);

	static void self_tunnelendpoint_send_remote_recieve_connect_error (
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const unsigned long tunnel_id);
	virtual void tunnelendpoint_send_remote_recieve_connect_error_decoubled(const unsigned long tunnel_id);



	static void self_enable_checkpoint_tunnel (
			const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
			const std::string& folder);
	virtual void enable_checkpoint_tunnel(const boost::filesystem::path& folder);



    boost::asio::io_service& get_io(void);
    boost::asio::io_service::strand& get_strand(void);

protected:

	/*! \brief Constructor
	 */
	APITunnelendpointTCPTunnelServerWithConnectionEngine(
			const APIAsyncService::APtr& async_service,
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);

	TunnelendpointTCPTunnelServerWithConnectionEngine::APtr impl_tunnelendpoint_;
	APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* api_eventhandler_;
    boost::asio::io_service& io_;
    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
};

}
}
#endif
