/*! \file COM_Tunnelendpoint.cxx
 \brief This file contains the impplementation for the tunnelendpoint connector classes
 */
#include <boost/asio.hpp>

#include <functional>
#include <boost/bind.hpp>

#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_TrafficControlManager.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * Tunnelendpoint implementation
 * ------------------------------------------------------------------
 */
Tunnelendpoint::Tunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler) :
    io_(io),
    checkpoint_handler_(checkpoint_handler),
    eventhandler_(eventhandler),
    connector_(NULL),
    send_package_id_next_(0),
    state_(State_Initializing),
    protocol_version_(1),
    skip_connection_handshake_(false),
    mutex_(Mutex::create(io, "Tunnelendpoint")),
    state_connecting_connect_recived_(false),
    state_connecting_connect_response_recived_(false),
    pending_posts_(0) {
    version_ = Version::Version::create_current();
    version_remote_ = Version::Version::APtr(Version::Version::create(0,0,0,0));
}

Tunnelendpoint::~Tunnelendpoint(void) {
    state_ = State_Unknown;
}

void Tunnelendpoint::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
}

Utility::Mutex::APtr Tunnelendpoint::get_mutex(void) {
    return mutex_;
}

void Tunnelendpoint::set_traffic_control_session(void) {
	if(traffic_control_session_.get() == NULL) {
		traffic_control_session_ = TrafficControlManager::get_global(io_, checkpoint_handler_)->get_session(mutex_->get_mutex_id());
		traffic_control_session_point_id_ = traffic_control_session_->add_endpoint(TrafficControlSessionPoint::ConnectionType_Unknown);
	}
}

unsigned long Tunnelendpoint::get_tc_read_delay_ms(void) {
	set_traffic_control_session();
	return traffic_control_session_->get_read_delay_ms(traffic_control_session_point_id_);
}

unsigned long Tunnelendpoint::get_tc_read_size(void) {
	set_traffic_control_session();
	return traffic_control_session_->get_read_size(traffic_control_session_point_id_);
}

void Tunnelendpoint::tc_report_read_begin(void) {
	set_traffic_control_session();
	traffic_control_session_->report_read_begin(traffic_control_session_point_id_);
}

void Tunnelendpoint::tc_report_read_end(const unsigned long size) {
	set_traffic_control_session();
	traffic_control_session_->report_read_end(traffic_control_session_point_id_, size);
}

void Tunnelendpoint::tc_report_push_buffer_size(const unsigned long size) {
	set_traffic_control_session();
	traffic_control_session_->report_push_buffer_size(traffic_control_session_point_id_, size);
}

void Tunnelendpoint::tc_report_pop_buffer_size(const unsigned long size) {
	set_traffic_control_session();
	traffic_control_session_->report_pop_buffer_size(traffic_control_session_point_id_, size);
}


unsigned char Tunnelendpoint::get_protocol_verison(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::get_protocol_verison");
    return protocol_version_;
}

void Tunnelendpoint::set_protocol_version(const unsigned char& protocol_version) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_protocol_version");
    protocol_version_ = protocol_version;
}

void Tunnelendpoint::tunnelendpoint_send(const MessagePayload::APtr& payload) {
    MutexScopeLock mutex_send_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_send.1");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_send.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state_", state_));
        return;
    }
    MessageCom::PackageType message_package_type = MessageCom::PackageType_Data;
    if(payload->get_buffer()->getSize() > 65000) {
        message_package_type = MessageCom::PackageType_DataHuge;
    }
    MessageCom::APtr message(MessageCom::create(message_package_type, payload, send_package_id_next_++, protocol_version_));
    tunnelendpoint_send(message);
}

void Tunnelendpoint::tunnelendpoint_send(const MessageCom::APtr& message) {
    MutexScopeLock mutex_send_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_send");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_send.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state_", state_));
        return;
    }
	if(connector_ != NULL) {
		connector_->tunnelendpoint_connector_send(message);
	}
}

void Tunnelendpoint::tunnelendpoint_user_signal(const unsigned long signal_id, const std::string& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_user_signal");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_user_signal.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    tunnelendpoint_user_signal(signal_id, MessagePayload::create(DataBufferManaged::create(message)));
}

void Tunnelendpoint::tunnelendpoint_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_user_signal");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_user_signal.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    Checkpoint cp1(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_user_signal", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()) );

    if(connector_ != NULL) {
        connector_->tunnelendpoint_connector_user_signal(signal_id, message);
        return;
    }
    Checkpoint cps_ignore(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_user_signal.signal.ignored", Attr_Communication(), CpAttr_warning(), CpAttr("signal_id", signal_id), CpAttr("state_", state_));
}

void Tunnelendpoint::tunnelendpoint_recieve(const MessageCom::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_recieve");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_recieve.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    MessageCom::PackageType message_package_type = message->get_package_type();
    if(protocol_version_ != message->get_protocol_version()) {
        Checkpoint cps_ignore(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_recieve.mitchmatch_protocol_versions", Attr_Communication(), CpAttr_warning(), CpAttr("message_package_type", message_package_type), CpAttr("state_", state_), CpAttr("protocol_version_", protocol_version_), CpAttr("message_protocol_version_", message->get_protocol_version()));
        return;
    }

    if(protocol_version_ == 0) {
        if (message_package_type == MessageCom::PackageType_TunnelendpointData) {
            TunnelendpointMessage::APtr message_tunnelendpoint(TunnelendpointMessage::create_from_buffer(message->get_payload()->get_buffer()));
            tunnelendpoint_recieve_tunnelendpoint_data(message_tunnelendpoint);
            return;
        }
    }
    else if(protocol_version_ == 1) {
        if ( (message_package_type == MessageCom::PackageType_Data) ||
             (message_package_type == MessageCom::PackageType_DataHuge)) {
            if (state_ == State_Connected) {
                if (eventhandler_ != NULL) {
                    eventhandler_->tunnelendpoint_eh_recieve(message->get_payload());
                    return;
                }
            }
        }
        if (message_package_type == MessageCom::PackageType_TunnelendpointData) {
            TunnelendpointMessage::APtr message_tunnelendpoint(TunnelendpointMessage::create_from_buffer(message->get_payload()->get_buffer()));
            tunnelendpoint_recieve_tunnelendpoint_data(message_tunnelendpoint);
            return;
        }
    }
    Checkpoint cps_ignore(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_recieve.ignored.message", Attr_Communication(), CpAttr_warning(), CpAttr("message_package_type", message_package_type), CpAttr("state_", state_), CpAttr("protocol_version_", protocol_version_), CpAttr("id_abs", get_id_abs_to_string()));
    return;
}

void Tunnelendpoint::tunnelendpoint_recieve_tunnelendpoint_data(const TunnelendpointMessage::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_recieve_tunnelendpoint_data");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_recieve_tunnelendpoint_data.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }

    TunnelendpointMessage::MessageType message_type(message->get_message_type());
    if(message->get_message_type() == TunnelendpointMessage::MessageType_connect_response) {
        if (state_ == State_Connecting && message->get_rc() == TunnelendpointMessage::ReturnCodeType_ok) {
            version_remote_ = message->get_version();
            state_connecting_connect_response_recived_ = true;
            goto_state_connected();
            return;
        }
    }
    else if(message->get_message_type() == TunnelendpointMessage::MessageType_connect) {
        if ( (state_ == State_Initializing || state_ == State_Connecting || state_ == State_Connected ) && message->get_rc() == TunnelendpointMessage::ReturnCodeType_ok) {
            TunnelendpointMessage::APtr message(TunnelendpointMessage::create(TunnelendpointMessage::MessageType_connect_response, TunnelendpointMessage::ReturnCodeType_ok));
            message->set_version(version_);
            MessageCom::APtr message_com(MessageCom::create(MessageCom::PackageType_TunnelendpointData, MessagePayload::create(message->create_as_buffer()), send_package_id_next_++, protocol_version_));
            tunnelendpoint_send(message_com);
            state_connecting_connect_recived_ = true;
            goto_state_connected();
            return;
        }
    }
    else if(message->get_message_type() == TunnelendpointMessage::MessageType_disconnect) {
    	if (state_ == State_Connected && message->get_rc() == TunnelendpointMessage::ReturnCodeType_ok) {
    		close(false);
    		return;
    	}
    }
    Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::tunnelendpoint_recieve_tunnelendpoint_data.ignored.message", Attr_Communication(), CpAttr_warning(), CpAttr("message_type", message->get_message_type()), CpAttr("state_", state_), CpAttr("abs_id", get_id_abs_to_string()));
}

void Tunnelendpoint::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::reset_eventhandler");
    eventhandler_ = NULL;
}

TunnelendpointEventhandler* Tunnelendpoint::get_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::get_eventhandler");
    return eventhandler_;
}

void Tunnelendpoint::set_connector(TunnelendpointConnector* connector) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_connector");
    connector_ = connector;
}

void Tunnelendpoint::reset_connector(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::reset_connector");
    connector_ = NULL;
}

void Tunnelendpoint::set_id_abs(const std::list<unsigned long>& id_abs) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_id_abs");
    id_abs_ = id_abs;
}

void Tunnelendpoint::append_id_to_id_abs(const unsigned long& id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::append_id_to_id_abs");
    id_abs_.push_back(id);
}

std::list<unsigned long> Tunnelendpoint::get_id_abs(void) {
    return id_abs_;
}

std::string Tunnelendpoint::get_id_abs_to_string(void) {
    return get_id_abs_to_string(2000);
}
std::string Tunnelendpoint::get_id_abs_to_string(const unsigned long& id) {
    stringstream ss;
    ss << "(";
    std::list<unsigned long>::const_iterator i(id_abs_.begin());
    while( i != id_abs_.end()) {
        ss << *i;
        ++i;
        if (i != id_abs_.end()) {
            ss << ", ";
        }
    }
    ss << ")";
    if (id != 2000) {
        ss << "<" << id << ">";
    }
    return ss.str();
}

void Tunnelendpoint::set_skip_connection_handshake(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_skip_connection_handshake");
    skip_connection_handshake_ = true;
}

bool Tunnelendpoint::is_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::is_connected");
	return state_ == State_Connected;
}

bool Tunnelendpoint::is_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::is_closed");
    switch(state_) {
    case State_Closed:
    case State_Done:
    case State_Unknown:
    	return true;
    }
    return false;
}

void Tunnelendpoint::goto_state_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::goto_state_connected");
    switch(state_) {
    case State_Connecting:
    	break;
    case State_Connected:
    	return;
    case State_Initializing:
    	break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::goto_state_connected.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state_", state_));
        return;
    }
	if (!skip_connection_handshake_) {
		if(!state_connecting_connect_recived_ || !state_connecting_connect_response_recived_) {
			return;
		}
	}
    state_ = State_Connected;
    if (eventhandler_ != NULL) {
        eventhandler_->tunnelendpoint_eh_connected();
    }
}

void Tunnelendpoint::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::connect");
    switch(state_) {
    case State_Connected:
    case State_Connecting:
        return;
    case State_Initializing:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::connect.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state_", state_));
        return;
    }
    if (protocol_version_ == 0) {
    	goto_state_connected();
    }
    else if (protocol_version_ == 1) {
        if (!skip_connection_handshake_) {
            TunnelendpointMessage::APtr message(TunnelendpointMessage::create(TunnelendpointMessage::MessageType_connect, TunnelendpointMessage::ReturnCodeType_ok));
            message->set_version(version_);

            MessageCom::APtr message_com(MessageCom::create(MessageCom::PackageType_TunnelendpointData, MessagePayload::create(message->create_as_buffer()), send_package_id_next_++, protocol_version_));
            state_ = State_Connecting;
            tunnelendpoint_send(message_com);
        }
        else {
        	goto_state_connected();
        }
    }
}

void Tunnelendpoint::close_by_strand(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::close_by_strand");
    switch(state_) {
    case State_Initializing:
    case State_Connected:
    case State_Connecting:
         break;
    case State_Done:
    case State_Closed:
    case State_Closing:
    	return;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::close_by_strand.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    pending_posts_++;
    mutex_->get_strand().post(boost::bind(&Tunnelendpoint::close_from_strand, this, force));
}

void Tunnelendpoint::close_from_strand(const bool force) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::close_from_strand");
	try {
		// Virtual dispatch seems not to work with boost::bind, in the way we use it
		pending_posts_--;
		close(force);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"Tunnelendpoint::close_from_strand.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void Tunnelendpoint::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::close");
    switch(state_) {
    case State_Connected:
    case State_Connecting:
    case State_Initializing:
    	break;
    default:
    	return;
    }
	state_ = State_Closing;
	close_final();
}

void Tunnelendpoint::close_final(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::close_final");
    Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::close_final", Attr_Communication(), CpAttr_debug(), CpAttr("state_", state_), CpAttr("abs_id", get_id_abs_to_string()));
    switch(state_) {
    case State_Closing:
    	break;
    case State_Closed:
    	return;
    default:
        Checkpoint cps(*checkpoint_handler_, "Tunnelendpoint::close_final.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state_", state_));
        return;
    }
    state_ = State_Closed;
	if(connector_ != NULL) {
		connector_->tunnelendpoint_connector_closed();
	}
    reset_connector();
    reset_eventhandler();
    state_ = State_Done;
}

void Tunnelendpoint::set_version(const Version::Version::APtr& version) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_version");
    version_ = version;
}

Version::Version::APtr Tunnelendpoint::get_version(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::get_version");
    return version_;
}

Version::Version::APtr Tunnelendpoint::get_version_remote(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::get_version_remote");
    return version_remote_;
}

bool Tunnelendpoint::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::async_mem_guard_is_done");
	return (state_ == State_Done) && (pending_posts_ == 0);
}

std::string Tunnelendpoint::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void Tunnelendpoint::set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type) {
	appl_protocol_type_ = appl_protocol_type;
}

SessionMessage::ApplProtocolType Tunnelendpoint::get_appl_protocol_type(void) {
	return appl_protocol_type_;
}


/*
 * ------------------------------------------------------------------
 * TunnelendpointReliableCrypted implementation
 * ------------------------------------------------------------------
 */
TunnelendpointReliableCrypted::TunnelendpointReliableCrypted(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler) :
    Tunnelendpoint(io, checkpoint_handler, eventhandler) {
}

TunnelendpointReliableCrypted::~TunnelendpointReliableCrypted(void) {
}

TunnelendpointReliableCrypted::APtr TunnelendpointReliableCrypted::create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                          TunnelendpointEventhandler* eventhandler) {
	APtr new_tunnelendpoint(new TunnelendpointReliableCrypted(io, checkpoint_handler, eventhandler));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

/*
 * ------------------------------------------------------------------
 * TunnelendpointReliableCryptedTunnelConnector implementation
 * ------------------------------------------------------------------
 */
TunnelendpointReliableCryptedTunnelConnector::TunnelendpointReliableCryptedTunnelConnector(TunnelendpointReliableCryptedTunnel* tunnelendpoint,
                                                                                           unsigned long child_id) :
    tunnelendpoint_(tunnelendpoint), child_id_(child_id) {
}

TunnelendpointReliableCryptedTunnelConnector::~TunnelendpointReliableCryptedTunnelConnector(void) {
}

TunnelendpointReliableCryptedTunnelConnector::APtr TunnelendpointReliableCryptedTunnelConnector::create(TunnelendpointReliableCryptedTunnel* tunnelendpoint,
                                                                                                        unsigned long child_id) {
    return APtr(new TunnelendpointReliableCryptedTunnelConnector(tunnelendpoint, child_id));
}

void TunnelendpointReliableCryptedTunnelConnector::tunnelendpoint_connector_send(const MessageCom::APtr& message) {
    assert(tunnelendpoint_);
   	tunnelendpoint_->tunnelendpoint_send(child_id_, message);
}

void TunnelendpointReliableCryptedTunnelConnector::tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    assert(tunnelendpoint_);
    tunnelendpoint_->tunnelendpoint_user_signal(child_id_, signal_id, message);
}


void TunnelendpointReliableCryptedTunnelConnector::tunnelendpoint_connector_closed(void) {
    assert(tunnelendpoint_);
    tunnelendpoint_->tunnelendpoint_child_closed(child_id_);
}

std::string TunnelendpointReliableCryptedTunnelConnector::tunnelendpoint_connector_get_unique_session_id(void) {
    assert(tunnelendpoint_);
    return tunnelendpoint_->tunnelendpoint_get_unique_session_id();
}


/*
 * ------------------------------------------------------------------
 * TunnelendpointReliableCryptedTunnel implementation
 * ------------------------------------------------------------------
 */
TunnelendpointReliableCryptedTunnel::TunnelendpointReliableCryptedTunnel(boost::asio::io_service& io,
                                                                         const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                         TunnelendpointEventhandler* eventhandler) :
    TunnelendpointReliableCrypted(io, checkpoint_handler, eventhandler) {
}
TunnelendpointReliableCryptedTunnel::TunnelendpointReliableCryptedTunnel(boost::asio::io_service& io,
                                                                         const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                         TunnelendpointEventhandler* eventhandler,
                                                                         const Utility::Mutex::APtr& mutex) :
    TunnelendpointReliableCrypted(io, checkpoint_handler, eventhandler) {
  mutex_ = mutex;
}

TunnelendpointReliableCryptedTunnel::~TunnelendpointReliableCryptedTunnel(void) {
}

void TunnelendpointReliableCryptedTunnel::set_mutex(const Utility::Mutex::APtr& mutex) {
    Tunnelendpoint::set_mutex(mutex);
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_mutex");

    for (const auto& pair : tunnelendpoints_) {
        pair.second->set_mutex(mutex);
    }
}

TunnelendpointReliableCryptedTunnel::APtr TunnelendpointReliableCryptedTunnel::create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler) {
	APtr new_tunnelendpoint(new TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, eventhandler));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

TunnelendpointReliableCryptedTunnel::APtr TunnelendpointReliableCryptedTunnel::create(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, TunnelendpointEventhandler* eventhandler, const Utility::Mutex::APtr& mutex) {
  APtr new_tunnelendpoint(new TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, eventhandler, mutex));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

void TunnelendpointReliableCryptedTunnel::add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::add_tunnelendpoint");
    if (child_id == 0) {
        stringstream ss;
        ss << "The tunnelendpoint "<< get_id_abs_to_string(child_id) << " is reserved and can not be used";
        throw ExceptionInvalidTunnelendpoint(ss.str());
    }
    if (child_id > 255) {
        stringstream ss;
        ss << "Support for child_id > 255 has not yet been implemented";
        throw ExceptionInvalidTunnelendpoint(ss.str());
    }

    // Inheret the protocol version
    tunnelendpoint->set_protocol_version(protocol_version_);
    tunnelendpoint->set_appl_protocol_type(appl_protocol_type_);

    std::map<unsigned long, Tunnelendpoint::APtr>::const_iterator  i(tunnelendpoints_.find(child_id));
    if (i != tunnelendpoints_.end()) {
        stringstream ss;
        ss << "The tunnelendpoint "<< get_id_abs_to_string(child_id) << " has already been defined";
        throw ExceptionInvalidTunnelendpoint(ss.str());
    }
    tunnelendpoints_.insert(make_pair(child_id, tunnelendpoint));

    TunnelendpointReliableCryptedTunnelConnector::APtr connector(TunnelendpointReliableCryptedTunnelConnector::create( this, child_id));
    tunnelendpoint->set_connector(connector.get());
    tunnelenspoint_connectors_.insert(make_pair(child_id, connector));

    tunnelendpoint->set_id_abs(get_id_abs());
    tunnelendpoint->append_id_to_id_abs(child_id);
    tunnelendpoint->set_mutex(mutex_);

    if (is_connected()) {
        tunnelendpoint->connect();
    }

    // Send already recived tunnelendpoint data
    std::vector< std::pair<unsigned long, MessageCom::APtr> >::iterator j(message_buffer_tunnelendpoint_data_.begin());
    while(j != message_buffer_tunnelendpoint_data_.end()) {
        unsigned long message_child_id = j->first;
        if(child_id == message_child_id) {
            tunnelendpoint->tunnelendpoint_recieve(j->second);
            j = message_buffer_tunnelendpoint_data_.erase(j);
        }
        else {
            j++;
        }
    }
}

void TunnelendpointReliableCryptedTunnel::remove_tunnelendpoint(const unsigned long& child_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::remove_tunnelendpoint");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::remove_tunnelendpoint", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(child_id)));
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::remove_tunnelendpoint.ignored", Attr_Communication(), CpAttr_debug());
        return;
    }

    std::map<unsigned long, Tunnelendpoint::APtr>::const_iterator i(tunnelendpoints_.find(child_id));
    if (i != tunnelendpoints_.end()) {
    	if(i->second->is_closed()) {
            i->second->reset_connector();
            tunnelendpoints_.erase(child_id);
            tunnelenspoint_connectors_.erase(child_id);
    	}
    	else {
    		i->second->close_by_strand(false);
    	}
    } else {
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::remove_tunnelendpoint.unknown_tunnelendpoint", Attr_Communication(), CpAttr_warning(), CpAttr("abs_id", get_id_abs_to_string(child_id)));
    }
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed(const unsigned long child_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed");
    switch(state_) {
    case State_Initializing:
    case State_Connected:
    case State_Connecting:
    case State_Closing:
         break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    pending_posts_++;
    mutex_->get_strand().post(boost::bind(&TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed_decoubled, this, child_id));
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed_decoubled(const unsigned long child_id) {
	try {
		MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed_decoubled");
		pending_posts_--;

		switch(state_) {
		case State_Initializing:
		case State_Connected:
		case State_Connecting:
		case State_Closing:
			 break;
		case State_Done:
		case State_Closed:
			return;
		default:
			Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed_decoubled.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
			return;
		}
		std::map<unsigned long, Tunnelendpoint::APtr>::const_iterator i(tunnelendpoints_.find(child_id));
		if (i != tunnelendpoints_.end()) {
			tunnelendpoints_.erase(child_id);
			tunnelenspoint_connectors_.erase(child_id);
		} else {
			Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_child_closed.unknown_tunnelendpoint", Attr_Communication(), CpAttr_warning(), CpAttr("abs_id", get_id_abs_to_string()));
		}

		if( (state_==State_Closing) && tunnelendpoints_.empty()) {
			close_final();
		}
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"Tunnelendpoint::tunnelendpoint_child_closed_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_send(const MessagePayload::APtr& payload) {
    MutexScopeLock mutex_send_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.1");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.1.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    MessageCom::PackageType message_package_type = MessageCom::PackageType_Data;
    if(payload->get_buffer()->getSize() > 65000) {
        message_package_type = MessageCom::PackageType_DataHuge;
    }
    tunnelendpoint_send(MessageCom::create(message_package_type, payload, send_package_id_next_++, protocol_version_));
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_send(const MessageCom::APtr& message) {
    MutexScopeLock mutex_send_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.2");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.2.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    message->push_prefix_address_element(MessageCom::MessageComAddressElement(0));
    Tunnelendpoint::tunnelendpoint_send(message);
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_send(const unsigned long child_id, const MessageCom::APtr& message) {
    MutexScopeLock mutex_send_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.3");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.3.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    if(connector_ == NULL) {
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_send.3.ignore.message", Attr_Communication(), CpAttr_debug(), CpAttr("state", state_), CpAttr("abs_id", get_id_abs_to_string()), CpAttr("send_package_id", message->get_package_id()));
        return;
    }
    message->push_prefix_address_element(MessageCom::MessageComAddressElement(child_id));
    Tunnelendpoint::tunnelendpoint_send(message);
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_user_signal");
    Tunnelendpoint::tunnelendpoint_user_signal(signal_id, message);
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal(const unsigned long signal_id, const std::string& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_user_signal");
    Tunnelendpoint::tunnelendpoint_user_signal(signal_id, message);
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal(const unsigned long child_id, const unsigned long signal_id, const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::tunnelendpoint_user_signal");
    Tunnelendpoint::tunnelendpoint_user_signal(signal_id, message);
}

void TunnelendpointReliableCryptedTunnel::tunnelendpoint_recieve(const MessageCom::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_recieve");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_recieve.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    if(protocol_version_ != message->get_protocol_version()) {
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::tunnelendpoint_recieve.mitchmatch_protocol_versions", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_), CpAttr("protocol_version_", protocol_version_), CpAttr("message_protocol_version_", message->get_protocol_version()));
        return;
    }
    dispatch_message(message);
}

void TunnelendpointReliableCryptedTunnel::dispatch_message(const MessageCom::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::dispatch_message");
    switch(state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Connected:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::dispatch_message.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }

    unsigned long child_id  = message->pop_prefix_address_element().get_address();
    if(protocol_version_ == 0) {
        if (child_id == 0) {
            if (eventhandler_ != NULL) {
                eventhandler_->tunnelendpoint_eh_recieve(message->get_payload());
                return;
            }
        } else {
            std::map<unsigned long, Tunnelendpoint::APtr>::const_iterator i(tunnelendpoints_.find(child_id));
            if (i != tunnelendpoints_.end()) {
        		i->second->tunnelendpoint_recieve(message);
                return;
            } else {
                Checkpoint cp_ignore(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::dispatch_message.message_ignored", Attr_Communication(), CpAttr_warning(), CpAttr("id_abs",  get_id_abs_to_string(child_id)));
            }
        }
    }
    else if(protocol_version_ == 1) {
        if (child_id == 0) {
            if ( (message->get_package_type() == MessageCom::PackageType_Data) ||
                 (message->get_package_type() == MessageCom::PackageType_DataHuge)) {
                if (eventhandler_ != NULL) {
                    eventhandler_->tunnelendpoint_eh_recieve(message->get_payload());
                    return;
                }
            }
            else if (message->get_package_type() == MessageCom::PackageType_TunnelendpointData) {
                TunnelendpointMessage::APtr message_tunnelendpoint(TunnelendpointMessage::create_from_buffer(message->get_payload()->get_buffer()));
                tunnelendpoint_recieve_tunnelendpoint_data(message_tunnelendpoint);
                return;
            }
        } else {
            std::map<unsigned long, Tunnelendpoint::APtr>::const_iterator i(tunnelendpoints_.find(child_id));
            if (i != tunnelendpoints_.end()) {
                i->second->tunnelendpoint_recieve(message);
                return;
            }
            if (message->get_package_type() == MessageCom::PackageType_TunnelendpointData) {
                message_buffer_tunnelendpoint_data_.push_back(make_pair(child_id, message));
                return;
            }
            Checkpoint cp_ignore(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::dispatch_message.message_ignored", Attr_Communication(), CpAttr_warning(), CpAttr("id_abs",  get_id_abs_to_string(child_id)));
        }
    }
    else {
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::dispatch_message.invalid_protocol_version", Attr_Communication(), CpAttr_warning(), CpAttr("protocol_version",  protocol_version_));
    }
    return;
}

void TunnelendpointReliableCryptedTunnel::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::connect");
    switch(state_) {
    case State_Connected:
    case State_Connecting:
        return;
    case State_Initializing:
         break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::connect.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
    Tunnelendpoint::connect();

    for (const auto& pair : tunnelendpoints_) {
        pair.second->set_protocol_version(protocol_version_);
        pair.second->set_appl_protocol_type(appl_protocol_type_);
        pair.second->connect();
    }
}

void TunnelendpointReliableCryptedTunnel::goto_state_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::goto_state_connected");
    switch(state_) {
    case State_Connecting:
    case State_Initializing:
    	break;
    case State_Connected:
    	return;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::goto_state_connected.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
	if (!skip_connection_handshake_) {
		if(!state_connecting_connect_recived_ || !state_connecting_connect_response_recived_) {
			return;
		}
	}
    Tunnelendpoint::goto_state_connected();

    for (const auto& pair : tunnelendpoints_) {
        pair.second->set_protocol_version(protocol_version_);
        pair.second->set_appl_protocol_type(appl_protocol_type_);
        pair.second->connect();
    }
}

void TunnelendpointReliableCryptedTunnel::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointReliableCryptedTunnel::close");
    switch(state_) {
    case State_Initializing:
    case State_Connected:
    case State_Connecting:
         break;
    case State_Done:
    case State_Closed:
    case State_Closing:
    	return;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::close.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }
	state_ = State_Closing;

	std::vector<unsigned long> child_ids_to_delete;

    for (const auto& pair : tunnelendpoints_) {
    	if (pair.second->is_closed()) {
    		child_ids_to_delete.push_back(pair.first);
    	}
    	else {
    		pair.second->close_by_strand(false);
    	}
    }

    std::vector<unsigned long>::const_iterator j(child_ids_to_delete.begin());
    while (j != child_ids_to_delete.end()) {
        tunnelendpoints_.erase(*j);
        tunnelenspoint_connectors_.erase(*j);
        ++j;
    }

    if(tunnelendpoints_.empty()) {
    	close_final();
    }
}

void TunnelendpointReliableCryptedTunnel::set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_appl_protocol_type");
    switch(state_) {
    case State_Initializing:
    case State_Connected:
    case State_Connecting:
         break;
    default:
        Checkpoint cps(*checkpoint_handler_, "TunnelendpointReliableCryptedTunnel::set_appl_protocol_type.invalid_state", Attr_Communication(), CpAttr_warning(), CpAttr("state_", state_));
        return;
    }

    Tunnelendpoint::set_appl_protocol_type(appl_protocol_type);

    for (const auto& pair : tunnelendpoints_) {
        pair.second->set_appl_protocol_type(appl_protocol_type);
    }
}

std::string TunnelendpointReliableCryptedTunnel::tunnelendpoint_get_unique_session_id(void) {
    if(connector_ != NULL) {
    	return connector_->tunnelendpoint_connector_get_unique_session_id();
    }
    return "unknown";
}

void TunnelendpointReliableCryptedTunnel::set_traffic_control_session(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "Tunnelendpoint::set_traffic_control_session");
	if(traffic_control_session_.get() == NULL) {
		traffic_control_session_ = TrafficControlManager::get_global(io_, checkpoint_handler_)->get_session(tunnelendpoint_get_unique_session_id());
		traffic_control_session_point_id_ = traffic_control_session_->add_endpoint(TrafficControlSessionPoint::ConnectionType_Unknown);
	}
}
