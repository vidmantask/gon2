/*! \file COM_Message.cxx
 \brief This file contains the message classes
 */
#include <boost/cstdint.hpp>
#include <boost/crc.hpp>

#include <iostream>

#include <lib/utility/UY_Convert.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_StreamToMessage.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


#ifdef ANDROID
#define g_ntohll(x) (((boost::uint64_t)(ntohl((boost::uint_t)((x << 32) >> 32))) << 32) | (boost::uint32_t)ntohl(((boost::uint32_t)(x >> 32))))
#define g_htonll(x) g_ntohll(x)

void convert_from_networkorder_32(boost::uint32_t& value,
                                                     const byte_t& byte1,
                                                     const byte_t& byte2,
                                                     const byte_t& byte3,
                                                     const byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    byte_t* netWorkByteOrderPtr( (byte_t*)&netWorkByteOrder);
    netWorkByteOrderPtr[0] = byte1;
    netWorkByteOrderPtr[1] = byte2;
    netWorkByteOrderPtr[2] = byte3;
    netWorkByteOrderPtr[3] = byte4;
    value = ntohl(netWorkByteOrder);
}

void convert_to_networkorder_32(const boost::uint32_t& value,
                                                   byte_t& byte1,
                                                   byte_t& byte2,
                                                   byte_t& byte3,
                                                   byte_t& byte4) {
    boost::uint32_t netWorkByteOrder = 0;
    netWorkByteOrder = htonl(value);
    byte_t* netWorkByteOrderPtr = (byte_t*) &netWorkByteOrder;
    byte1 = netWorkByteOrderPtr[0];
    byte2 = netWorkByteOrderPtr[1];
    byte3 = netWorkByteOrderPtr[2];
    byte4 = netWorkByteOrderPtr[3];
}

void set_uint_32(const Utility::DataBuffer::APtr& buffer,
                                    const long& idx,
                                    const boost::uint32_t& value) {
    convert_to_networkorder_32(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3]);
}

boost::uint32_t get_uint_32(const Utility::DataBuffer::APtr& buffer, const long& idx) {
	boost::uint32_t value;
    convert_from_networkorder_32(value, (*buffer)[idx], (*buffer)[idx+1], (*buffer)[idx+2], (*buffer)[idx+3]);
    return value;
}

#endif


/*
 * ------------------------------------------------------------------
 * MessagePayload implementation
 * ------------------------------------------------------------------
 */
MessagePayload::MessagePayload(const Utility::DataBufferManaged::APtr& payload) :
    payload_(payload) {
}
MessagePayload::MessagePayload(void) {
}
MessagePayload::~MessagePayload(void) {
}

MessagePayload::APtr MessagePayload::create(const Utility::DataBufferManaged::APtr& payload) {
    return APtr(new MessagePayload(payload));
}

DataBufferManaged::APtr MessagePayload::get_buffer(void) {
    return payload_;
}

/*
 * ------------------------------------------------------------------
 * MessageRaw implementation
 * ------------------------------------------------------------------
 */
MessageRaw::MessageRaw(const PackageType package_type, const MessagePayload::APtr& payload) :
    payload_(payload), protocol_version_(0), package_type_(package_type) {
}

MessageRaw::~MessageRaw(void) {

}
MessagePayload::APtr MessageRaw::get_payload(void) {
    return payload_;
}

MessageRaw::APtr MessageRaw::create(const PackageType package_type,
                                    const MessagePayload::APtr& payload) {
    return APtr(new MessageRaw(package_type, payload));
}

/*
 * * Header:
 *      protocol-version    8 bit       (currently 0)
 *      package type        8 bit       (currently 0)
 *      payload size        16 bit      (in bytes)
 */
DataBufferManaged::APtr MessageRaw::create_as_buffer(void) {
    unsigned long payload_size = payload_->get_buffer()->getSize();
    unsigned long header_size = 4;
    Utility::DataBufferManaged::APtr buffer(Utility::DataBufferManaged::create(header_size + payload_size, 0));

    set_uint_8(buffer, 0, protocol_version_);
    set_uint_8(buffer, 1, package_type_);
    set_uint_16(buffer, 2, payload_size);

    memcpy(buffer->data() + header_size, payload_->get_buffer()->data(), payload_size);
    return buffer;
}

MessageRaw::APtr MessageRaw::create_from_stream(StreamToMessageConsumer& stream) {
    unsigned long header_size = 4;

    /*
     * Optimization introduced to avoid many ExceptionPeekError() exceptions
     */
    if (stream.peek_available() < header_size) {
        return MessageRaw::APtr();
    }

	try {
        boost::uint8_t protocol_version = 0;
        convert_from_networkorder_8(protocol_version, stream.peek_byte(0));
        if(protocol_version != 0) {
        	throw ExceptionInvalidMessage();
        }

        boost::uint8_t package_type = 0;
        convert_from_networkorder_8(package_type, stream.peek_byte(1));
        if(package_type != 0) {
        	throw ExceptionInvalidMessage();
        }

        boost::uint16_t payload_size = 0;
        convert_from_networkorder_16(payload_size, stream.peek_byte(2), stream.peek_byte(3));
        if(payload_size == 0) {
        	throw ExceptionInvalidMessage();
        }

        unsigned long message_size = header_size + payload_size;
        MessagePayload::APtr payload(MessagePayload::create(stream.peek_buffer(header_size,
                                payload_size)));
        if(payload.get() == NULL) {
        	throw ExceptionInvalidMessage();
        }

        MessageRaw::APtr message(new MessageRaw(static_cast<PackageType>(package_type), payload));

        stream.consume(message_size);
        return message;
    }
    catch (StreamToMessageConsumer::ExceptionPeekError) {
    }
    return MessageRaw::APtr();
}

/*
 * ------------------------------------------------------------------
 * MessageCom implementation
 * ------------------------------------------------------------------
 */
MessageCom::MessageCom(const PackageType package_type, const MessagePayload::APtr& payload, const unsigned long& package_id) :
    payload_(payload), protocol_version_(1), package_id_(package_id), package_type_(package_type)  {
}

MessageCom::~MessageCom(void) {

}
MessagePayload::APtr MessageCom::get_payload(void) {
    return payload_;
}

unsigned long MessageCom::get_package_id(void) const {
    return package_id_;
}

MessageCom::PackageType MessageCom::get_package_type(void) const {
    return package_type_;
}


unsigned char MessageCom::get_protocol_version(void) const {
    return protocol_version_;
}

void MessageCom::set_protocol_version(const unsigned char&  protocol_version) {
     protocol_version_ =  protocol_version;
}

MessageCom::APtr MessageCom::create(const PackageType package_type,
                                    const MessagePayload::APtr& payload,
                                    const unsigned long& package_id) {
    return APtr(new MessageCom(package_type, payload, package_id));
}

MessageCom::APtr MessageCom::create(const PackageType package_type,
                                    const MessagePayload::APtr& payload,
                                    const unsigned long& package_id,
                                    const unsigned char& protocol_version) {
    MessageCom::APtr message(MessageCom::create(package_type, payload, package_id));
    message->set_protocol_version(protocol_version);
    return message;
}


void MessageCom::push_prefix_address_element(const MessageComAddressElement& address_element) {
    message_address_elements_.push_back(address_element);
}

std::string MessageCom::get_address_as_string(void) {
	stringstream ss;
    std::vector<MessageComAddressElement>::const_iterator i(message_address_elements_.begin());
    while(i != message_address_elements_.end()) {
    	ss << (*i).get_address() << " ";
    	i++;
    }
    return ss.str();
}


MessageCom::MessageComAddressElement MessageCom::pop_prefix_address_element(void) {
    assert(message_address_elements_.size() != 0);
    MessageComAddressElement address_element(message_address_elements_.back());
    message_address_elements_.pop_back();
    return address_element;
}

/*
 * * Header:
 *      protocol-version    8 bit       (currently 1)
 *      package type        8 bit       (currently 0)
 *      package id          32 bit
 *      header size         16 bit      (in bytes)
 *      payload size        32 bit      (in bytes)
 *      crc                 32 bit      (crc of all fields except crc)
 *      address (repeated until header size):
 *          address-size    8 bit
 */
const int OFFSET_PROTOCOL_VERSION = 0;
const int OFFSET_PACKAGE_TYPE = 1;
const int OFFSET_PACKAGE_ID = 2;
const int OFFSET_HEADER_SIZE = 6;
const int OFFSET_PAYLOAD_SIZE = 8;
const int OFFSET_CRC = 10;
const int OFFSET_ADDRESS = 14;
const int OFFSET_HUGE_CRC = 12;
const int OFFSET_HUGE_ADDRESS = 16;

const int MIN_MESSAGE_SIZE = 14;







DataBufferManaged::APtr MessageCom::create_as_buffer(void) {
    unsigned long payload_size = payload_->get_buffer()->getSize();
    if ((payload_size > 65000) && (package_type_ == PackageType_Data)) {
        package_type_ = PackageType_DataHuge;
    }

    int offset_crc;
    int offset_address;
    if (package_type_ == PackageType_DataHuge) {
        offset_crc = OFFSET_HUGE_CRC;
        offset_address = OFFSET_HUGE_ADDRESS;
    }
    else {
        offset_crc = OFFSET_CRC;
        offset_address = OFFSET_ADDRESS;
    }

    unsigned long header_size = offset_address + message_address_elements_.size();
    Utility::DataBufferManaged::APtr buffer(Utility::DataBufferManaged::create(header_size + payload_size, 0));

    if (package_type_ == PackageType_DataHuge) {
        set_uint_32(buffer, OFFSET_PAYLOAD_SIZE, payload_size);
    }
    else {
        set_uint_16(buffer, OFFSET_PAYLOAD_SIZE, payload_size);
    }
    set_uint_8(buffer,  OFFSET_PROTOCOL_VERSION, protocol_version_);
    set_uint_8(buffer,  OFFSET_PACKAGE_TYPE, package_type_);
    set_uint_32(buffer, OFFSET_PACKAGE_ID, package_id_);
    set_uint_16(buffer, OFFSET_HEADER_SIZE, header_size);

    int idx = offset_address;
    std::vector<MessageComAddressElement>::const_iterator i(message_address_elements_.begin());
    while (i != message_address_elements_.end()) {
        set_uint_8(buffer, idx, (*i).get_address());
        ++idx;
        ++i;
    }
    if(payload_size > 0) {
    	memcpy(buffer->data() + header_size, payload_->get_buffer()->data(), payload_size);
    }
    unsigned long address_and_payload_size = payload_size + (header_size - offset_address);

    boost::crc_32_type crc;
    crc.process_bytes(buffer->data(), offset_crc);
    crc.process_bytes(buffer->data() + offset_address, address_and_payload_size);

    /*
     * Because of an error in 5.3 the crc has to be discorrected
     * This should be fixed when a new protocol version is introduced
     *
     * The correct code is:
     *   set_uint_32(buffer, offset_crc, crc.checksum());
     *
     */
    boost::uint32_t crc_boost_error = crc.checksum();
    boost::uint32_t CRC32_NEGL = 0xffffffffL;
    crc_boost_error ^= CRC32_NEGL;
    convert_to_networkorder_32(crc_boost_error, *(buffer->data()+offset_crc+3), *(buffer->data()+offset_crc+2), *(buffer->data()+offset_crc+1), *(buffer->data()+offset_crc));
    return buffer;
}

MessageCom::APtr MessageCom::create_from_stream(StreamToMessageConsumer& stream) {
    /*
     * Optimization introduced to avoid many ExceptionPeekError() exceptions
     */
    if (stream.peek_available() < MIN_MESSAGE_SIZE) {
        return MessageCom::APtr();
    }

	try {
        boost::uint8_t protocol_version = 0;
        convert_from_networkorder_8(protocol_version, stream.peek_byte(OFFSET_PROTOCOL_VERSION));

        boost::uint8_t package_type = 0;
        convert_from_networkorder_8(package_type, stream.peek_byte(OFFSET_PACKAGE_TYPE));

        boost::uint32_t package_id = 0L;
        convert_from_networkorder_32(package_id, stream.peek_byte(OFFSET_PACKAGE_ID), stream.peek_byte(OFFSET_PACKAGE_ID+1), stream.peek_byte(OFFSET_PACKAGE_ID+2), stream.peek_byte(OFFSET_PACKAGE_ID+3));

        boost::uint16_t header_size = 0;
        convert_from_networkorder_16(header_size, stream.peek_byte(OFFSET_HEADER_SIZE), stream.peek_byte(OFFSET_HEADER_SIZE+1));

        int offset_crc;
        int offset_address;

        boost::uint32_t payload_size = 0;
        if (package_type == PackageType_DataHuge) {
            offset_crc = OFFSET_HUGE_CRC;
            offset_address = OFFSET_HUGE_ADDRESS;
            boost::uint32_t payload_size_huge = 0;
            convert_from_networkorder_32(payload_size_huge, stream.peek_byte(OFFSET_PAYLOAD_SIZE), stream.peek_byte(OFFSET_PAYLOAD_SIZE+1), stream.peek_byte(OFFSET_PAYLOAD_SIZE+2), stream.peek_byte(OFFSET_PAYLOAD_SIZE+3));
            payload_size = payload_size_huge;
        }
        else {
            offset_crc = OFFSET_CRC;
            offset_address = OFFSET_ADDRESS;

            boost::uint16_t payload_size_small = 0;
            convert_from_networkorder_16(payload_size_small, stream.peek_byte(OFFSET_PAYLOAD_SIZE), stream.peek_byte(OFFSET_PAYLOAD_SIZE+1));
            payload_size = payload_size_small;
        }
        boost::uint32_t crc = 0L;
        convert_from_networkorder_32(crc, stream.peek_byte(offset_crc), stream.peek_byte(offset_crc+1), stream.peek_byte(offset_crc+2), stream.peek_byte(offset_crc+3));
        /*
         * Here we calculate a crc that where the error from 5.3 is corrected
         */
        boost::uint32_t crc_with_error = 0L;
        convert_from_networkorder_32(crc_with_error, stream.peek_byte(offset_crc+3), stream.peek_byte(offset_crc+2), stream.peek_byte(offset_crc+1), stream.peek_byte(offset_crc+0));
        boost::uint32_t CRC32_NEGL = 0xffffffffL;
        crc_with_error ^= CRC32_NEGL;


        unsigned long message_size = header_size + payload_size;
        MessagePayload::APtr payload;
        if(payload_size > 0) {
        	payload = MessagePayload::create(stream.peek_buffer(header_size, payload_size));
        }
        else {
        	payload = MessagePayload::create(DataBufferManaged::create_empty(0));
        }
        assert(payload != NULL);

        MessageCom::APtr message(new MessageCom(static_cast<PackageType>(package_type), payload, package_id));
        message->set_protocol_version(protocol_version);

        int idx = offset_address;
        while (idx < header_size) {
            boost::uint8_t address_element = 0;
            convert_from_networkorder_8(address_element, stream.peek_byte(idx));
            message->push_prefix_address_element(MessageComAddressElement(address_element));
            ++idx;
        }

        boost::crc_32_type crc_calc;
        unsigned long address_and_payload_size = payload_size + (header_size - offset_address);
        crc_calc.process_bytes(stream.peek_buffer(0, offset_crc)->data(), offset_crc);
        if(address_and_payload_size > 0) {
        	crc_calc.process_bytes(stream.peek_buffer(offset_address, address_and_payload_size)->data(), address_and_payload_size);
        }
        boost::uint32_t crc_calc_num = crc_calc.checksum();
        if(crc_calc_num != crc && crc_calc_num != crc_with_error) {
        	stringstream ss;
        	ss << "Invalid CRC in COM-Message:" << endl;
        	ss << " protocol_version:" << int(protocol_version) << endl;
        	ss << " package_type:" << int(package_type) << endl;
            ss << " package_id:" << long(package_id) << endl;
        	ss << " header_size:" << long(header_size) << endl;
        	ss << " payload_size:" << long(payload_size) << endl;
        	ss << " message_size:" << long(message_size) << endl;
        	ss << " crc     :" << std::hex << std::uppercase << crc << endl;
        	ss << " crc_calc:" << std::hex << std::uppercase << crc_calc_num << endl;
//        	ss << " payload:" <<  payload->get_buffer()->encodeHex()->toString() << endl;
        	ss << endl;
        	ss << stream.dump_to_str() << endl;
        	ss << endl;
        	ss << message->create_as_buffer()->encodeHex()->toString() << endl;
        	throw ExceptionUnexpected(ss.str());
        }
        stream.consume(message_size);
        return message;
    }
    catch (StreamToMessageConsumer::ExceptionPeekError) {
    }
    return MessageCom::APtr();
}

/*
 * ------------------------------------------------------------------
 * MessageComAddressElement implementation
 * ------------------------------------------------------------------
 */
MessageCom::MessageComAddressElement::MessageComAddressElement(const unsigned long address) :
    address_size_(AddressSize_4bit), address_(address) {
}

unsigned long MessageCom::MessageComAddressElement::get_address(void) const {
    return address_;
}
