/*! \file COM_TunnelEndpointDirectEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from a direct tunnelendpoint
 */
#ifndef COM_TunnelendpointDirectEventhandler_HXX
#define COM_TunnelendpointDirectEventhandler_HXX

#include <component/communication/COM_TunnelendpointDirect.hxx>



namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointDirectEventhandler: public boost::noncopyable {
public:

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_direct_eh_closed() = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message) = 0;
};



/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointDirectTunnelClientEventhandler: public boost::noncopyable {
public:

	/*! \brief Signals that a connection is ready for use.
     */
    virtual void tunnelendpoint_direct_eh_connection_ready(const TunnelendpointDirect::APtr& connection) = 0;

	/*! \brief Signals that a connection is ready for use.
     */
    virtual void tunnelendpoint_direct_eh_connection_failed(const std::string& errorMessage) = 0;

    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_direct_eh_ready(void) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_direct_eh_closed(void) = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message) = 0;

};


}
}
#endif
