/*! \file COM_SessionManagerConnection.hxx
 *  \brief This file contains
 */
#ifndef COM_SessionManagerConnection_HXX
#define COM_SessionManagerConnection_HXX

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>

#include <component/communication/COM_SessionManagerConnectionEventhandler.hxx>

#include <component/communication/COM_Servers_Spec.hxx>
#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_SessionCrypterEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Represent a connection entry
 *
 *   <connection title="", host="192.168.0.1" port="3945" timeout_sec="15" type="direc|http"/>
 *
 */

class SessionManagerConnection: public RawTunnelendpointConnectorTCPEventhandler, public SessionCrypterEventhandler {
public:
    typedef boost::shared_ptr<SessionManagerConnection> APtr;

    enum State {
        State_unknown = 0,
        State_not_connected,
        State_connecting,
        State_tcp_failed_to_connect,
        State_tcp_connect,
        State_secure_failed_to_connect,
        State_secure_connect,
        State_closed,
        State_timeout
    };

    enum Type {
        Type_direct = 0, Type_http
    };

    /*! \brief Implementation of RawTunnelendpointConnectorTCPEventhandler interface
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Implementation of RawTunnelendpointConnectorTCPEventhandler interface
     */
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& message);

    /*! \brief Implementation of SessionCrypterEventhandler interface
     */
    void session_crypter_key_exchange_complete(void);

    /*! \brief Implementation of SessionCrypterEventhandler interface
     */
    void session_crypter_key_exchange_failed(const std::string& error_message);

    /*! \brief Implementation of SessionCrypterEventhandler interface
     */
    void session_crypter_closed(void);

    /*! \brief Start a connection attempt using this connection
     *
     * Result and are reported using the eventhandler
     */
    void connect(void);
    void connect_timeout(const boost::system::error_code& error);

    /*! \brief Destructor
     */
    ~SessionManagerConnection(void);

    void set_eventhandler(SessionManagerConnectionEventhandler*);
    void reset_eventhandler(void);

    /*! \brief Getters
     */
    unsigned long get_id(void) const;
    State get_state(void) const;
    std::string get_title(void) const;

    /*! \brief Returns true if the connection is pending, that is if it still has a change of successfuly connection
     */
    bool is_pending(void) const;

    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& asio_io_service,
                       const unsigned long& id,
                       const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const Utility::DataBufferManaged::APtr& known_secret,
                       const CryptFacility::KeyStore::APtr& key_store,
                       const std::string& title,
                       const std::string& host,
                       const unsigned long& port,
                       const boost::posix_time::time_duration& timeout,
                       const Type& type);

    static APtr create(boost::asio::io_service& asio_io_service,
                       const unsigned long& id,
                       const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const Utility::DataBufferManaged::APtr& known_secret,
                       const CryptFacility::KeyStore::APtr& key_store,
                       const ServersSpec::ServersConnection& servers_connection);

private:
    SessionManagerConnection(boost::asio::io_service& asio_io_service,
                             const unsigned long& id,
                             const Utility::CheckpointHandler::APtr& checkpoint_handler,
                             const Utility::DataBufferManaged::APtr& known_secret,
                             const CryptFacility::KeyStore::APtr& key_store,
                             const std::string& title,
                             const std::string& host,
                             const unsigned long& port,
                             const boost::posix_time::time_duration& timeout,
                             const Type& type);


    void call_session_manager_connection_failed_decoubled(void);

    boost::asio::io_service& asio_io_service_;
    State state_;

    SessionManagerConnectionEventhandler* eventhandler_;

    unsigned long id_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    Utility::DataBufferManaged::APtr known_secret_;
    CryptFacility::KeyStore::APtr key_store_;

    std::string title_;
    std::string host_;
    unsigned long port_;
    boost::posix_time::time_duration timeout_;
    Type type_;

    RawTunnelendpointConnectorTCP::APtr raw_tunnelendpoint_connector_;
    SessionCrypter::APtr session_crypter_;

    boost::asio::steady_timer connect_timeout_timer_;
    Utility::Mutex::APtr mutex_;
};

}
}
#endif
