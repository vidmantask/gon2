/*! \file COM_RawTunnelendpointConnectorEventhandler.hxx
 *  \brief This file contains abstract interface RawTunnelendpointConnectorTCPEventhandler
 */
#ifndef COM_RawTunnelendpointConnectorEventhandler_HXX
#define COM_RawTunnelendpointConnectorEventhandler_HXX

#include <component/communication/COM_RawTunnelendpoint.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler interface for a raw tunnelendpoint connector.
 * 
 * A raw tunnelendpoint connector eventhandler recive signals from a raw tunnelendpoint connector when a connection is established.
 */
class RawTunnelendpointConnectorTCPEventhandler : public boost::noncopyable {
public:

    /*! \brief Connected signal
     * 
     *  \param tunnelendpoint connected raw tunnelendpoint
     * 
     *  The connector send this signal as a response to call to RawTunnelendpointConnectorTCP::aio_connect_start().
     */
    virtual void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) = 0;


    /*! \brief Failed to connect signal
     * 
     *  The connector send this signal it it was not possible to connect, 
     *  as a response to call to RawTunnelendpointConnectorTCP::aio_connect_start().
     */
    virtual void com_tunnelendpoint_connection_failed_to_connect(const std::string& message) = 0;

};

}
}
#endif
