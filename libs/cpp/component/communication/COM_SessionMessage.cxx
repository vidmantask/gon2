/*! \file COM_SessionMessage.cxx
 \brief This file contains the implementation for the COM_SessionMessage class
 */
#include <component/communication/COM_SessionMessage.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


const unsigned long SESSION_DATA_PROTOCOL_VERSION_OFFEST = 0;
const unsigned long SESSION_DATA_APPL_PROTOCOL_TYPE_OFFEST = 1;
const unsigned long SESSION_DATA_PROTOCOL_VERSION_CURRENT = 1;


SessionMessage::SessionMessage(
		const string& unique_session_id,
		const bool& session_logging_enabled,
		const ApplProtocolType appl_protocol_type,
		const bool& traffic_control_enabled) :
	unique_session_id_(unique_session_id),
	session_logging_enabled_(session_logging_enabled),
	appl_protocol_type_(appl_protocol_type),
	traffic_control_enabled_(traffic_control_enabled) {
}

SessionMessage::~SessionMessage(void) {
}

SessionMessage::APtr SessionMessage::create(
		const string& unique_session_id,
		const bool& session_logging_enabled,
		const ApplProtocolType appl_protocol_type,
		const bool& traffic_control_enabled) {
	return APtr(new SessionMessage(unique_session_id, session_logging_enabled, appl_protocol_type, traffic_control_enabled));
}

SessionMessage::APtr SessionMessage::create(const MessageCom::APtr& message) {
	try {
		if (message->get_package_type()==MessageCom::PackageType_SessionData) {
			DataBufferManaged::APtr payload_buffer(message->get_payload()->get_buffer());
			DataBufferManaged::APtr unique_session_id_buffer;
			long next_start_pos = payload_buffer->parseWithLen(unique_session_id_buffer, 2);
			string unique_session_id(unique_session_id_buffer->toString());

			bool session_logging_enabled(false);
			if (next_start_pos <  payload_buffer->getSize()) {
				payload_buffer->parse(session_logging_enabled, next_start_pos);
			}
			next_start_pos++;

			long protocol_version(0);
			if (next_start_pos <  payload_buffer->getSize()) {
				payload_buffer->parse(protocol_version, 1, next_start_pos);
			}
			next_start_pos++;

			ApplProtocolType appl_protocol_type(ApplProtocolType_python);
			if (next_start_pos <  payload_buffer->getSize()) {
				long appl_protocol_type_l(0);
				payload_buffer->parse(appl_protocol_type_l, 1, next_start_pos);
				appl_protocol_type = static_cast<ApplProtocolType>(appl_protocol_type_l);
			}
			next_start_pos++;

			bool traffic_control_enabled(true);
			if (next_start_pos <  payload_buffer->getSize()) {
				payload_buffer->parse(traffic_control_enabled, next_start_pos);
			}

			return APtr(new SessionMessage(unique_session_id, session_logging_enabled, appl_protocol_type, traffic_control_enabled));
		}
	}
	catch (const Giritech::Utility::Exception& e) {
		return APtr();
	}
	return APtr();
}

string SessionMessage::get_unique_session_id_(void) const {
	return unique_session_id_;
}
bool SessionMessage::get_session_logging_enabled(void) const {
	return session_logging_enabled_;
}

bool SessionMessage::get_traffic_control_enabled(void) const {
	return traffic_control_enabled_;
}

SessionMessage::ApplProtocolType SessionMessage::get_appl_protocol_type(void) const {
	return appl_protocol_type_;
}

MessageCom::APtr SessionMessage::build_message_com(void) const {
	DataBufferManaged::APtr payload(DataBufferManaged::create(0));
	payload->appendWithLen(unique_session_id_, 2);
	payload->append(session_logging_enabled_);

	DataBufferManaged::APtr header(DataBufferManaged::create(3));
	set_uint_8(header, SESSION_DATA_PROTOCOL_VERSION_OFFEST, SESSION_DATA_PROTOCOL_VERSION_CURRENT);
	set_uint_8(header, SESSION_DATA_APPL_PROTOCOL_TYPE_OFFEST, appl_protocol_type_);
	set_uint_8(header, SESSION_DATA_TRAFFIC_CONTROL_ENABLED_OFFEST, traffic_control_enabled_);
	payload->append(header);

	MessagePayload::APtr message_com_payload(MessagePayload::create(payload));
	MessageCom::APtr message_com(MessageCom::create(MessageCom::PackageType_SessionData, message_com_payload, 0));
	return message_com;
}


