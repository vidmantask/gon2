/*! \file COM_TunnelendpointTCPMessage.cxx
 \brief This file contains the implementation for the tunnelendpoint-tcp-message class
 */
#include <functional>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>

#include <component/communication/COM_TunnelendpointTCPMessage.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TunnelendpointTCPMessage implementation
 * ------------------------------------------------------------------
 */
TunnelendpointTCPMessage::TunnelendpointTCPMessage(const MessageType message_type,
                                                   const unsigned long tunnel_id,
                                                   const ReturnCodeType rc) :
     message_type_(message_type), tunnel_id_(tunnel_id), rc_(rc) {
}

TunnelendpointTCPMessage::~TunnelendpointTCPMessage(void) {
}

TunnelendpointTCPMessage::APtr TunnelendpointTCPMessage::create(const MessageType message_type,
                                                                const unsigned long tunnel_id,
                                                                const ReturnCodeType rc) {
    return APtr(new TunnelendpointTCPMessage(message_type, tunnel_id, rc));
}

Utility::DataBufferManaged::APtr TunnelendpointTCPMessage::create_as_buffer(void) {

    unsigned long header_size = 4;
    unsigned long payload_size = 0;
    if (payload_ != NULL) {
        payload_size = payload_->getSize();
    }
    unsigned long buffer_size = header_size + payload_size;

    Utility::DataBufferManaged::APtr buffer(Utility::DataBufferManaged::create(buffer_size));
    set_uint_8(buffer, 0, message_type_);
    set_uint_16(buffer, 1, tunnel_id_);
    set_uint_8(buffer, 3, rc_);

    if (payload_size > 0) {
        memcpy(buffer->data() + header_size, payload_->data(), payload_size);
    }
    return buffer;
}

TunnelendpointTCPMessage::APtr TunnelendpointTCPMessage::create_from_buffer(const Utility::DataBufferManaged::APtr& buffer) {
    unsigned long header_size = 4;

    if (buffer->getSize() < header_size) {
        return TunnelendpointTCPMessage::APtr();
    }

    boost::uint8_t message_type_raw = 0;
    convert_from_networkorder_8(message_type_raw, (*buffer)[0]);

    boost::uint16_t tunnel_id_raw = 0;
    convert_from_networkorder_16(tunnel_id_raw, (*buffer)[1], (*buffer)[2]);

    boost::uint8_t rc_raw = 0;
    convert_from_networkorder_8(rc_raw, (*buffer)[3]);

    TunnelendpointTCPMessage::APtr message =
            TunnelendpointTCPMessage::create(static_cast<MessageType> (message_type_raw), tunnel_id_raw,
                                             static_cast<ReturnCodeType> (rc_raw));
    if (buffer->getSize() > header_size) {
        unsigned long payload_size = buffer->getSize() - header_size;
        DataBufferManaged::APtr payload(DataBufferManaged::create(payload_size, 'x'));
        memcpy(payload->data(), buffer->data()+header_size, payload_size);
        message->set_payload(payload);
    }
    return message;
}

TunnelendpointTCPMessage::MessageType TunnelendpointTCPMessage::get_message_type(void) const {
    return message_type_;
}

unsigned long TunnelendpointTCPMessage::get_tunnel_id(void) const {
    return tunnel_id_;
}
TunnelendpointTCPMessage::ReturnCodeType TunnelendpointTCPMessage::get_rc(void) const {
    return rc_;
}

Utility::DataBufferManaged::APtr TunnelendpointTCPMessage::get_payload(void) const {
    return payload_;
}

void TunnelendpointTCPMessage::set_payload(const Utility::DataBufferManaged::APtr& payload) {
    payload_ = payload;
}
