/*! \file COM_SessionCrypter.hxx
 \brief This file contains the session crypter
 */
#ifndef COM_SessionCrypter_HXX
#define COM_SessionCrypter_HXX

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_ALL.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_StreamToMessage.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>
#include <component/communication/COM_SessionCrypterEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Gateway between a session-tunnelendpoint and a raw-tunnelendpoint
 *
 * The session crypter handles key-exchange and offers functionality for stream/block encryption/decryption.
 *
 * The session crypter is operating in two modes key-exchange-mode and ready-mode.
 * In key-exchange-mode the raw-tunnelendpoint is used for key-exchange, and
 * in ready-mode all packages recived is dispatched to the registered ready-eventhandler
 * (normaly a session-tunnelendpoint).
 *
 * In ready-mode the session crypter offers stream/block encryption/decryption
 * using the keys established during key-exchange. Notice that in ready-mode the package
 * recived from the raw-tunnelendpoint is dispatched directly (no decryption).
 *
 * The following drawing shows the state-diagram of the session crypter:
   \dot
   digraph diagram_session_state {
     rankdir=TB;
     ranksep=0.5;
     center=true;
     size="12,8.5";
     edge [minlen=2, fontsize=8, fontname="Helvetica" ];
     node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
     fontname="Helvetica";

     State_KeyExchange
     State_Ready
     State_Closing
     State_Closed
     State_Initalizing

     Start             -> State_Initalizing   [label="constructor"];

     State_KeyExchange -> State_Closing   [label="close"];
     State_Ready       -> State_Closing   [label="close"];
     State_Closed      -> State_Closing   [label="close"];
     State_Error       -> State_Closing   [label="close"];

     State_KeyExchange -> State_Closed   [label="com_raw_tunnelendpoint_close"];
     State_Ready       -> State_Closed   [label="com_raw_tunnelendpoint_close"];
     State_Closing     -> State_Closed   [label="com_raw_tunnelendpoint_close"];
     State_Error       -> State_Closed   [label="com_raw_tunnelendpoint_close"];

     State_Initalizing  -> State_KeyExchange    [label="SessionCrypterServer\ndo_key_exchange"];
     State_Ready        -> State_KeyExchange    [label="SessionCrypterServer\ndo_key_exchange"];
     State_Initalizing  -> State_KeyExchange    [label="SessionCrypterClient\ndo_key_exchange"];
     State_Ready        -> State_KeyExchange    [label="SessionCrypterClient\ndo_key_exchange"];
    }
   \enddot
 *
 */
class SessionCrypter : public RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<SessionCrypter> APtr;

    enum State {
        State_Initalizing,
        State_Unknown,
        State_KeyExchange,
        State_Ready,
        State_Closing,
        State_Closed
    };

    /*! \brief Destructor
     */
    virtual ~SessionCrypter(void);

    /*! \brief Request to close the session crypter
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
     */
    void close(const bool force);

    /*! \brief Report back if the session has been closed
     */
    bool is_closed(void);

    /*! \brief Report back if the session is ready
     */
    bool is_ready(void);

    /*! \brief Geters of ip/port of local and remote part of socket
     */
    std::pair<std::string, unsigned long> get_ip_local(void);
    std::pair<std::string, unsigned long> get_ip_remote(void);

    /*! \brief Set eventhandler
     */
    void set_eventhandler(SessionCrypterEventhandler* eventhandler);

    /*! \brief Get mutex
     */
    Utility::Mutex::APtr get_mutex(void);

    /*! Get raw tunnelendpoint
     */
    RawTunnelendpointTCP::APtr get_raw_tunnelendpoint(void);

    /*! \brief Set mutex
     */
    void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Reset eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Set ready eventhandler
     */
    void set_eventhandler_ready(SessionCrypterEventhandlerReady* eventhandler_ready);

    /*! \brief Reset ready eventhandler
     */
    void reset_eventhandler_ready(void);

    /*! \brief Start a key-exchange
     */
    virtual void do_key_exchange(void) = 0;

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id);

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     */
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);

    /*! \brief Signal from RawTunnelendpointEventhandler
     */
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);

    /*! \brief Send a message through the connection
     */
    void send_state_ready(const Utility::DataBuffer::APtr& data);

    /*! \brief Start a async read ready state
     */
    void read_start_state_ready(void);

    /*! \brief Block encryption
     */
    Utility::DataBuffer::APtr encrypt_block(const Utility::DataBuffer::APtr& data);

    /*! \brief Block decryption
     */
    Utility::DataBuffer::APtr decrypt_block(const Utility::DataBuffer::APtr& data);

    /*! \brief Stream encryption

     Databuffer is reused for encrypted data.
     */
    Utility::DataBuffer::APtr encrypt_stream(const Utility::DataBuffer::APtr& data);

    /*! \brief Stream decryption

     Databuffer is reused for decrypted data.
     */
    Utility::DataBuffer::APtr decrypt_stream(const Utility::DataBuffer::APtr& data);

    /*
     * Expected to return true if the keyexchange phase_one is on going
     */
    virtual bool doing_keyexchange_phase_one(void) = 0;

protected:

    /*! \brief Constructor
     */
    SessionCrypter(const Utility::CheckpointHandler::APtr& checkpoint_handler, const RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Handle the recived message
     */
    virtual void handle_message_state_key_exchange(const Utility::DataBuffer::APtr& message) = 0;

    /*! \brief Handle the recived message
     */
    void handle_message_state_ready(const Utility::DataBuffer::APtr& message);

    /*! \brief Send a message through the connection
     */
    void send_state_key_exchange(const Utility::DataBufferManaged::APtr& data);

    void close_final_(void);

    Utility::CheckpointHandler::APtr checkpoint_handler_;
    State state_;
    RawTunnelendpointTCP::APtr tunnelendpoint_;
    SessionCrypterEventhandler* eventhandler_;
    SessionCrypterEventhandlerReady* eventhandler_ready_;

    CryptFacility::CryptFactory::APtr factory_;
    CryptFacility::Crypter::APtr crypter_stream_;
    CryptFacility::Crypter::APtr crypter_block_;

    StreamToMessage::APtr stream_to_message_key_exchange_;

    Utility::Mutex::APtr mutex_;
    bool read_message_started_;
};

/*! \brief This class defines a server session crypter
 */
class SessionCrypterServer : public SessionCrypter {
public:
    typedef boost::shared_ptr<SessionCrypterServer> APtr;

    enum KEState {
        KEState_Unknown,
        KEState_Init,
        KEState_wait_for_CIF,
        KEState_wait_for_CR,
        KEState_Ready,
        KEState_Closed,
        KEState_Error
    };

    /*! \brief Destructor
     */
    ~SessionCrypterServer(void);

    /*! \brief Start a key-exchange
     */
    void do_key_exchange(void);

    /*
     * Expected to return true if the keyexchange phase_one is on going
     */
    bool doing_keyexchange_phase_one(void);

    /*! \brief Create instance
     */
    static APtr create(const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const RawTunnelendpointTCP::APtr& tunnelendpoint,
                       const Utility::DataBufferManaged::APtr& client_known_secret_);

protected:

    /*! \brief Constructor
     */
    SessionCrypterServer(const Utility::CheckpointHandler::APtr& checkpoint_handler,
                         const RawTunnelendpointTCP::APtr& tunnelendpoint,
                         const Utility::DataBufferManaged::APtr& client_known_secret_);

    /*! \brief Handle the recived message
     */
    void handle_message_state_key_exchange(const Utility::DataBuffer::APtr& message);
    void handle_message_CIF(const Utility::DataBuffer::APtr& message);
    void handle_message_CR(const Utility::DataBuffer::APtr& message);

    void send_message_SPK(void);
    void send_message_SKP(void);
    void send_message_SR(void);

    KEState ke_state_;
    Utility::DataBufferManaged::APtr server_known_secret_;
    CryptFacility::KeyExchange_SessionKeyExchange_Server::APtr ske_;
    CryptFacility::KeyExchange_CIFExchange_Server::APtr cif_;
    CryptFacility::KeyExchange_CryptExchange_Server::APtr ce_;
};



/*! \brief This class defines a client session crypter
 *  */
class SessionCrypterClient : public SessionCrypter {
public:
    typedef boost::shared_ptr<SessionCrypterClient> APtr;

    enum KEState {
        KEState_Unknown,
        KEState_Init,
        KEState_wait_for_SPK,
        KEState_wait_for_SKP,
        KEState_wait_for_SR,
        KEState_Ready,
        KEState_Closed,
        KEState_Error
    };

    /*! \brief Destructor
     */
    ~SessionCrypterClient(void);

    /*! \brief Start a key-exchange
     */
    void do_key_exchange(void);

    /*
     * Expected to return true if the keyexchange phase_one is on going
     */
    bool doing_keyexchange_phase_one(void);

    /*! \brief Create instance
     */
    static APtr create(const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const RawTunnelendpointTCP::APtr& tunnelendpoint,
                       const Utility::DataBufferManaged::APtr& known_secret);
    static APtr create(const Utility::CheckpointHandler::APtr& checkpoint_handler,
                       const RawTunnelendpointTCP::APtr& tunnelendpoint,
                       const Utility::DataBufferManaged::APtr& known_secret,
                       const CryptFacility::KeyStore::APtr& key_store);

protected:

    /*! \brief Constructor
     */
    SessionCrypterClient(const Utility::CheckpointHandler::APtr& checkpoint_handler,
                         const RawTunnelendpointTCP::APtr& tunnelendpoint,
                         const Utility::DataBufferManaged::APtr& known_secret,
                         const CryptFacility::KeyStore::APtr& key_store);

    /*! \brief Handle the recived message
     */
    void handle_message_state_key_exchange(const Utility::DataBuffer::APtr& message);
    void handle_message_SPK(const Utility::DataBuffer::APtr& message);
    void handle_message_SKP(const Utility::DataBuffer::APtr& message);
    void handle_message_SR(const Utility::DataBuffer::APtr& message);

    void send_message_CIF(void);
    void send_message_CR(void);

    CryptFacility::KeyStore::APtr key_store_;
    KEState ke_state_;
    Utility::DataBufferManaged::APtr client_known_secret_;
    CryptFacility::KeyExchange_SessionKeyExchange::APtr ske_;
    CryptFacility::KeyExchange_CIFExchange_Client::APtr cif_;
    CryptFacility::KeyExchange_CryptExchange_Client::APtr ce_;

};

}
}
#endif
