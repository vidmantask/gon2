/*! \file COM_TrafficControlManager.hxx
 *  \brief This file contains TrafficControlManager class
 */
#ifndef COM_TrafficControlManager_HXX
#define COM_TrafficControlManager_HXX

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_TrafficControlSession.hxx>



namespace Giritech {
namespace Communication {

/*! \brief Traffic Control Manager
 *
 */
class TrafficControlManager : public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<TrafficControlManager> APtr;

    ~TrafficControlManager(void);

    TrafficControlSession::APtr get_session(const std::string& session_id);

    void close(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);

    static APtr get_global(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);

    static void destroy_global();

protected:
    TrafficControlManager(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);

    void interval_tick_start(void);
	void interval_tick(void);

    Utility::Mutex::APtr mutex_;
    boost::asio::io_service& asio_io_service_;
    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
//    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_details_;
    boost::asio::steady_timer interval_timer_;

    typedef std::map<std::string, TrafficControlSession::APtr> Sessions;
    Sessions sessions_;

    enum State {
    	State_Running,
    	State_Stopping,
    	State_Done
    };
    State state_;
};

}
}
#endif
