/*! \file COM_RawTunnelendpoint.cxx
 * \brief This file contains the implementation of the COM_RawTunnelendpoint abstraction
 */
#include <sstream>
#include <iostream>
#include <fstream>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <boost/config.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/regex.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_TrafficControlManager.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 * ------------------------------------------------------------------
 * DataBufferWithTime implementation
 * ------------------------------------------------------------------
 */
DataBufferWithTime::DataBufferWithTime(const Utility::DataBuffer::APtr& dataBuffer)
	: dataBuffer_(dataBuffer), create_ts_(boost::posix_time::microsec_clock::local_time()) {
}

DataBufferWithTime::~DataBufferWithTime(void){
}

DataBufferWithTime::APtr DataBufferWithTime::create(const Utility::DataBuffer::APtr& dataBuffer){
	return APtr(new DataBufferWithTime(dataBuffer));
}

Utility::DataBuffer::APtr DataBufferWithTime::getDataBuffer(void) const {
	return dataBuffer_;
}

boost::posix_time::time_duration DataBufferWithTime::getDurationSinceCreation(void) const{
	return boost::posix_time::microsec_clock::local_time() - create_ts_;
}




/*
 * ------------------------------------------------------------------
 * RawTunnelendpoint implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpoint::RawTunnelendpoint(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                     boost::asio::io_service& io,
                                     const unsigned long& connection_id) :
   checkpoint_handler_(checkpoint_handler), io_(io), connection_id_(connection_id),
   eventhandler_(NULL),
   mutex_(Mutex::create(io, "RawTunnelendpoint")),
   traffic_control_session_(TrafficControlManager::get_global(io, checkpoint_handler)->get_session("")) {
	traffic_control_session_id_ = traffic_control_session_->add_endpoint(TrafficControlSessionPoint::ConnectionType_Unknown);
}

RawTunnelendpoint::~RawTunnelendpoint(void) {
    traffic_control_session_->remove_endpoint(traffic_control_session_id_);
}

void RawTunnelendpoint::set_eventhandler(RawTunnelendpointEventhandler* eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpoint::set_eventhandler");
    eventhandler_ = eventhandler;
}

void RawTunnelendpoint::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpoint::reset_eventhandler");
    eventhandler_ = NULL;
}

Utility::Mutex::APtr RawTunnelendpoint::get_mutex(void) {
    return mutex_;
}

void RawTunnelendpoint::set_mutex(const Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
    traffic_control_session_->remove_endpoint(traffic_control_session_id_);
    traffic_control_session_ = TrafficControlManager::get_global(io_, checkpoint_handler_)->get_session(mutex_->get_mutex_id());
    traffic_control_session_id_ = traffic_control_session_->add_endpoint(TrafficControlSessionPoint::ConnectionType_Unknown);
}

unsigned long RawTunnelendpoint::get_connection_id(void) const {
    return connection_id_;
}

boost::asio::io_service& RawTunnelendpoint::get_io_service(void) {
    return io_;
}

void RawTunnelendpoint::mark_as_gconnection(const std::string& session_id) {
    traffic_control_session_->remove_endpoint(traffic_control_session_id_);
    traffic_control_session_ = TrafficControlManager::get_global(io_, checkpoint_handler_)->get_session(session_id);
    traffic_control_session_id_ = traffic_control_session_->add_endpoint(TrafficControlSessionPoint::ConnectionType_GConnection);
}

/*
 * ------------------------------------------------------------------
 * RawTunnelendpointTCP implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointTCP::RawTunnelendpointTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                           boost::asio::io_service& io,
                                           const unsigned long& connection_id) :
    RawTunnelendpoint(checkpoint_handler, io, connection_id),
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
    ssl_context_(RawTunnelendpointTCPSSLContext::get_instance(io_, "sslv23", 0)),
    ssl_socket_(io, ssl_context_->get_ctx()),
#else
    socket_(io),
#endif
    read_count_pending_(0),
    write_count_pending_(0),
    event_count_pending_(0),
    write_queue_total_size_(0),
    state_(State_Ready),
    read_start_delay_timer_(io),
    write_start_delay_timer_(io),
    write_is_running_(false),
    closed_for_read_(false),
    closed_for_write_(false),
    closing_for_write_(false),
    read_last_bytes_transferred_(0),
    ssl_do_read_start_when_connected_(false),
    ssl_disable_certificate_verification_(false),
    ssl_state_(SSLState_Off) {

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	if(ssl_context_->get_verify_certificate()) {
		ssl_socket_.set_verify_mode(boost::asio::ssl::verify_peer);
	}
	else {
		ssl_socket_.set_verify_mode(boost::asio::ssl::verify_none);
	}

	if(ssl_disable_certificate_verification_) {
		ssl_socket_.set_verify_mode(boost::asio::ssl::verify_none);
	}
#endif

}

RawTunnelendpointTCP::~RawTunnelendpointTCP(void) {
    state_ = State_Unknown;
}

RawTunnelendpointTCP::APtr RawTunnelendpointTCP::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const unsigned long& connection_id) {
	APtr result(new RawTunnelendpointTCP(checkpoint_handler, io, connection_id));
	AsyncMemGuard::global_add_element(result);
	return result;
}

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
RawTunnelendpointTCP::APtr RawTunnelendpointTCP::create_ssl(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		const unsigned long& connection_id,
    		const std::string& ssl_host_name,
			const bool& disable_certificate_verification) {
	APtr result(new RawTunnelendpointTCP(checkpoint_handler, io, connection_id));
	result->set_ssl_info(ssl_host_name, disable_certificate_verification);
	AsyncMemGuard::global_add_element(result);
	return result;
}
#endif

void RawTunnelendpointTCP::set_ssl_info(const std::string& ssl_host_name, const bool& disable_certificate_verification) {
	ssl_host_name_ = ssl_host_name;
	ssl_disable_certificate_verification_ = disable_certificate_verification;
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	if(ssl_context_->get_verify_hostname()) {
		ssl_socket_.set_verify_callback(boost::asio::ssl::rfc2818_verification(ssl_host_name_));
	}

	if(ssl_context_->get_sni()) {
		SSL_set_tlsext_host_name(ssl_socket_.native_handle(), ssl_host_name_.c_str());
	}

	if(ssl_disable_certificate_verification_) {
	  Checkpoint cp(*checkpoint_handler_,
			"RawTunnelendpointTCP::set_ssl_info.disable_certificate_verification",
			Attr_Communication(),
			CpAttr_debug());
	  ssl_socket_.set_verify_mode(boost::asio::ssl::verify_none);
	}

	ssl_state_ = SSLState_On;
#else
    Checkpoint cp(*checkpoint_handler_,
                  "RawTunnelendpointTCP::set_ssl_info.ssl_not_available_on_platform",
                  Attr_Communication(),
                  CpAttr_error());
#endif
}

boost::asio::ip::tcp::socket& RawTunnelendpointTCP::get_socket(void) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
    return ssl_socket_.next_layer();
#else
    return socket_;
#endif
}

const boost::asio::ip::tcp::socket& RawTunnelendpointTCP::get_socket(void) const {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
    return ssl_socket_.next_layer();
#else
    return socket_;
#endif
}

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& RawTunnelendpointTCP::get_socket_ssl(void) {
    return ssl_socket_;
}
#endif

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
const boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& RawTunnelendpointTCP::get_socket_ssl(void) const {
    return ssl_socket_;
}
#endif

void RawTunnelendpointTCP::set_option_nodelay(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::set_option_nodelay");
    socket_operation_set_option_nodelay();
}

void RawTunnelendpointTCP::socket_operation_set_option_nodelay(void) {
	if (get_socket().is_open()) {
		boost::asio::ip::tcp::no_delay option(true);
		get_socket().set_option(option);
	}
}

void RawTunnelendpointTCP::aio_read_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_read_start");
	switch (state_) {
    case State_Ready:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
        return;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_read_start");
    }

	if(ssl_state_ == SSLState_Connecting) {
		ssl_do_read_start_when_connected_ = true;
		return;
	}

	if(read_count_pending_ > 0) {
        Checkpoint cp(*checkpoint_handler_,
                      "RawTunnelendpointTCP::aio_read_start.canceled",
                      Attr_Communication(),
                      CpAttr_debug(),
                      CpAttr("read_count_pending_", read_count_pending_));
	    return;
	}


	/*
	 * If tunnel is half-closed(EOF is received from peer) then cancel the read_start
	 */
	if(closed_for_read_) {
        Checkpoint cp(*checkpoint_handler_,
                      "RawTunnelendpointTCP::aio_read_start.canceled.eof",
                      Attr_Communication(),
                      CpAttr_debug());
        return;
	}

    read_count_pending_++;

    const unsigned long read_delay_ms(traffic_control_session_->get_read_delay_ms(traffic_control_session_id_));
    if(read_delay_ms > 0) {
      read_start_delay_timer_.expires_after(boost::chrono::milliseconds(read_delay_ms));
      read_start_delay_timer_.async_wait(boost::bind(&RawTunnelendpointTCP::aio_read_start_action, this));
    }
    else {
    	aio_read_start_action();
    }
}

void RawTunnelendpointTCP::aio_read_start_action(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_read_start_action");
	switch (state_) {
    case State_Ready:
    case State_Closing:
        break;
    case State_Closed:
    case State_Done:
        read_count_pending_--;
        return;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_read_start_action");
    }

	traffic_control_session_->report_read_begin(traffic_control_session_id_);
    raw_buffer_ = get_read_buffer_();

    socket_operation_async_read();
}

void RawTunnelendpointTCP::socket_operation_async_read(void) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	if((ssl_state_ == SSLState_On) || (ssl_state_ == SSLState_Connecting)) {
	    boost::asio::async_read(ssl_socket_,
	                            boost::asio::buffer(raw_buffer_->get_raw_buffer()),
	                            boost::asio::transfer_at_least(1),
	                            mutex_->get_strand().wrap(boost::bind(&RawTunnelendpointTCP::aio_read,
	                                                     this,
	                                                     boost::asio::placeholders::error,
	                                                     boost::asio::placeholders::bytes_transferred)));


		return;
	}
#endif
    boost::asio::async_read(get_socket(),
                            boost::asio::buffer(raw_buffer_->get_raw_buffer()),
                            boost::asio::transfer_at_least(1),
                            mutex_->get_strand().wrap(boost::bind(&RawTunnelendpointTCP::aio_read,
                                                     this,
                                                     boost::asio::placeholders::error,
                                                     boost::asio::placeholders::bytes_transferred)));
}



void RawTunnelendpointTCP::aio_read(const boost::system::error_code& error, const size_t bytes_transferred) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_read");
	read_count_pending_--;
    read_last_bytes_transferred_ = bytes_transferred;

    switch (state_) {
    case State_Ready:
    case State_Closing:
        break;
    case State_Closed:
    case State_Done:
        return; // Ignoring messages received in closed state
    case State_Unknown:
        assert(false);
    default:
    	// Writing to checkpointhandler instad of throwing exception because a exception in the asio-async-loop might cause Segmentation fault on mac and linux.
    	Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_read.unexpected.1", Attr_Communication(), CpAttr_critical(), CpAttr_message("RawTunnelendpointTCP state not ready for aio_read"));
    	return;
    }

    try {
		/*
		 * If we are closing and this is the final read returning,
		 * then call the pending close
		 */
		if(state_ == State_Closing) {
			aio_close_start_pending_();
			return;
		}

		/*
		 * The package must be send to receiver
		 */
		if (!error) {
			raw_buffer_->get_raw_buffer().resize(bytes_transferred);
			if (eventhandler_ != NULL) {
			    eventhandler_->com_raw_tunnelendpoint_read(connection_id_, raw_buffer_);
			}
			else {
				Checkpoint cp(*checkpoint_handler_,
							  "RawTunnelendpointTCP::aio_read.warning",
							  Attr_Communication(),
							  CpAttr_warning(),
							  CpAttr_message("buffer was read but eventhandler was NULL"));
				aio_close_start(true);
			}
			traffic_control_session_->report_read_end(traffic_control_session_id_, bytes_transferred);
			return;
		}

		/*
		 * EOF has been send by peer
		 */
		if(socket_operation_is_eof(error)) {
			Checkpoint cp(*checkpoint_handler_,
						  "RawTunnelendpointTCP::aio_read.eof",
						  Attr_Communication(),
						  CpAttr_debug(),
						  CpAttr_message(error.message()));

			/*
			 * If already closed for write, then close down this connection
			 */
			if(closed_for_write_) {
				aio_close_start(false);
			}
			else {
                closed_for_read_ = true;
			    event_aio_com_raw_tunnelendpoint_close_eof_start_();
			}
			return;
		}

		/*
		 * The socket has been closed by the peer
		 */
		if (error == boost::asio::error::connection_reset ||
			error == boost::asio::error::connection_aborted) {
			Checkpoint cp(*checkpoint_handler_,
						  "RawTunnelendpointTCP::aio_read.closed",
						  Attr_Communication(),
						  CpAttr_debug(),
						  CpAttr_message(error.message()));
			aio_close_start(true);
			return;
		}

		/*
		 * Unexpected error, report and close the socket
		 */
		Checkpoint cp(*checkpoint_handler_,
					  "RawTunnelendpointTCP::aio_read.error",
					  Attr_Communication(),
					  CpAttr_warning(),
					  CpAttr_message(error.message()));
		aio_close_start(true);
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_read.unexpected.2", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
		aio_close_start(true);
    }
}

bool RawTunnelendpointTCP::socket_operation_is_eof(const boost::system::error_code& error_code) {
	return error_code == boost::asio::error::eof;
}

void RawTunnelendpointTCP::aio_write_start(const DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_write_start");
	switch (state_) {
    case State_Ready:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
        return;
    default:
        assert(false);
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_write_start");
    }
	write_queue_.push(DataBufferWithTime::create(data));
	write_queue_total_size_ += data->getSize();
    traffic_control_session_->report_push_buffer_size(traffic_control_session_id_, data->getSize());

    aio_write_from_buffer_start();
}

void RawTunnelendpointTCP::aio_write_from_buffer_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_write_from_buffer_start");
	switch (state_) {
    case State_Ready:
    case State_Closing:
        break;
    default:
    	return;
    }

	if(ssl_state_ == SSLState_Connecting) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_write_from_buffer_start.wait_until_ssl", Attr_Communication(), CpAttr_debug());
		return;
	}


//    unsigned long write_delay_ms = traffic_control_session_->get_write_delay_ms(traffic_control_session_id_);
//    if(write_delay_ms > 0) {
//        write_start_delay_timer_.expires_after(boost::chrono::milliseconds(write_delay_ms));
//        write_start_delay_timer_.async_wait(boost::bind(&RawTunnelendpointTCP::aio_write_from_buffer_start_action, this));
//    }
//    else {
//    	aio_write_from_buffer_start_action();
//    }
	aio_write_from_buffer_start_action();
}

void RawTunnelendpointTCP::aio_write_from_buffer_start_action(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_write_from_buffer_start_action");
	switch (state_) {
    case State_Ready:
    case State_Closing:
        break;
    default:
    	return;
    }

    if(write_is_running_) {
		return;
	}

	if(!write_queue_.empty()) {
    	DataBufferWithTime::APtr data(write_queue_.front());
    	write_queue_.pop();
        write_queue_total_size_ -= data->getDataBuffer()->getSize();
        traffic_control_session_->report_pop_buffer_size(traffic_control_session_id_, data->getDataBuffer()->getSize());
        if(data->getDataBuffer()->getSize() > 0) {
        	write_is_running_ = true;
        	write_count_pending_++;
        	socket_operation_async_write(data);
        }
    }
	else {
		write_is_running_ = false;
		if (eventhandler_ != NULL) {
			try {
				eventhandler_->com_raw_tunnelendpoint_write_buffer_empty(connection_id_);
			}
			catch (const std::exception& e) {
				Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_write_from_buffer_start_action.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
			}
		}
	}
}

void RawTunnelendpointTCP::socket_operation_async_write(const DataBufferWithTime::APtr& data) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	if(ssl_state_ == SSLState_On) {
	    boost::asio::async_write(
	    		ssl_socket_,
	    		boost::asio::buffer(data->getDataBuffer()->data(), data->getDataBuffer()->getSize()),
	    		mutex_->get_strand().wrap(boost::bind( &RawTunnelendpointTCP::aio_write_from_buffer, this, boost::asio::placeholders::error, data)));
	    return;
	}
#endif
    boost::asio::async_write(
    		get_socket(),
    		boost::asio::buffer(data->getDataBuffer()->data(), data->getDataBuffer()->getSize()),
    		mutex_->get_strand().wrap(boost::bind( &RawTunnelendpointTCP::aio_write_from_buffer, this, boost::asio::placeholders::error, data)));
}

void RawTunnelendpointTCP::aio_write_from_buffer(const boost::system::error_code& error, const DataBufferWithTime::APtr data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_write_from_buffer");
    write_count_pending_--;

    switch (state_) {
    case State_Ready:
    case State_Closing:
    	break;
    case State_Closed:
    case State_Done:
        return;
    case State_Unknown:
        assert(false);
    default:
    	// Writing to checkpointhandler instad of throwing exception because a exception in the asio-async-loop might cause Segmentation fault on mac and linux.
    	Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_write_from_buffer.unexpected", Attr_Communication(), CpAttr_error(), CpAttr_message("RawTunnelendpointTCP state not ready for aio_write"));
    	return;
    }

    try {
		/*
		 * Reinvoke close(and half-close) that has been on hold because write buffer was not empty
		 */
		if ((write_count_pending_ <= 0) && write_queue_.empty()) {
			if (state_ == State_Closing) {
				aio_close_start_pending_();
				return;
			}
			if(closing_for_write_) {
				aio_close_write_start_pending_();
				return;
			}
		}

		if (error) {
			Checkpoint cp(*checkpoint_handler_,
						  "RawTunnelendpointTCP::aio_write_from_buffer.error",
						  Attr_Communication(),
						  CpAttr_warning(),
						  CpAttr_message(error.message()));

			if(state_ == State_Closing) {
				aio_close();
			}
			else {
				aio_close_start(true);
			}
			return;
		}

		write_is_running_ = false;
		aio_write_from_buffer_start();
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_write_from_buffer.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
		aio_close_start(true);
    }
}

void RawTunnelendpointTCP::aio_close_write_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_close_write_start");
    switch (state_) {
    case State_Ready:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
        return;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_close_write_start");
    }
    CheckpointScope cps(*checkpoint_handler_,
                        "RawTunnelendpointTCP::aio_close_write_start",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("connection_id", connection_id_));
    socket_operation_log_info(cps);

    closing_for_write_ = true;
    aio_close_write_start_pending_();
}

void RawTunnelendpointTCP::aio_close_write_start_pending_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_close_write_start_pending_");
    switch (state_) {
    case State_Closed:
    case State_Closing:
    case State_Done:
         return;
    case State_Ready:
         break;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_close_write_start_pending_");
    }
    CheckpointScope cps(*checkpoint_handler_,
                        "RawTunnelendpointTCP::aio_close_write_start_pending_",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("connection_id", connection_id_));
    socket_operation_log_info(cps);

    /*
     * Check to see if all writing of buffer are done.
     * aio_close_write_start_pending_ will be reenvoked after writing of buffers
     */
    if ((write_count_pending_ <= 0) && write_queue_.empty()) {
        closed_for_write_ = true;

        if(closed_for_read_) {
            aio_close_start(false);
            return;
        }
        socket_operation_shutdown_send();
    }
}

void RawTunnelendpointTCP::socket_operation_shutdown_send(void) {
    boost::system::error_code error;
    if(get_socket().shutdown(boost::asio::ip::tcp::socket::shutdown_send, error)) {
        Checkpoint cp(*checkpoint_handler_,
                          "RawTunnelendpointTCP::socket_operation_shutdown_send.error",
                          Attr_Communication(),
                          CpAttr_debug(),
                          CpAttr_message(error.message()));
    }
}

void RawTunnelendpointTCP::socket_operation_shutdown_both(void) {
	boost::system::error_code error;
	if(get_socket().shutdown(boost::asio::ip::tcp::socket::shutdown_both, error)) {
		Checkpoint cp(*checkpoint_handler_,
				"RawTunnelendpointTCP::socket_operation_shutdown_both.error",
				Attr_Communication(),
				CpAttr_debug(),
				CpAttr_message(error.message()));
	}
}

void RawTunnelendpointTCP::socket_operation_shutdown_receive(void) {
	boost::system::error_code error;
	if(get_socket().shutdown(boost::asio::ip::tcp::socket::shutdown_receive, error)) {
		Checkpoint cp(*checkpoint_handler_,
				"socket_operation_shutdown_receive.error",
				Attr_Communication(),
				CpAttr_debug(),
				CpAttr_message(error.message()));
	}
}

void RawTunnelendpointTCP::socket_operation_close(void) {
	boost::system::error_code error;
	get_socket().close(error);
	if (error) {
		Checkpoint cp(*checkpoint_handler_,
				"RawTunnelendpointTCP::socket_operation_close.error",
				Attr_Communication(),
				CpAttr_debug(),
				CpAttr_message(error.message()));
	}
}

void RawTunnelendpointTCP::aio_close_start(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_close_start");
    switch (state_) {
    case State_Ready:
        break;
    case State_Closed:
    case State_Closing:
    case State_Done:
        return;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_close_start");
    }
    CheckpointScope cps(*checkpoint_handler_,
                        "RawTunnelendpointTCP::aio_close_start",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("connection_id", connection_id_));
    socket_operation_log_info(cps);

    state_ = State_Closing;
    if (force) {
    	socket_operation_shutdown_both();
        aio_close();
        return;
    }
    aio_close_start_pending_();
}

void RawTunnelendpointTCP::aio_close_start_pending_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_close_start_pending_");
    switch (state_) {
    case State_Closing:
        break;
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_close_start_pending_");
    }
    CheckpointScope cps(*checkpoint_handler_,
                        "RawTunnelendpointTCP::aio_close_start_pending_",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("connection_id", connection_id_),
                        CpAttr("read_count_pending", read_count_pending_),
                        CpAttr("write_count_pending", write_count_pending_),
                        CpAttr("write_queue_size", write_queue_.size())
                        );

    /*
     * Check to see if all writing of buffer is done.
     * aio_close_start_pending_ will be reenvoked after writing of buffers
     */
    if(!closed_for_write_) {
        if ((write_count_pending_ <= 0) && write_queue_.empty()) {
            closed_for_write_ = true;
            socket_operation_shutdown_both();
        }
    }
    else {
        socket_operation_shutdown_receive();
    }
    /*
     * Check to see if all read operation has terminated.
     * aio_close_start_pending_ will be reenvoked when last read is done.
     *
     * The termination of the last async read is terminated by the above shotdown tricked by the empty write-buffer.
     */
    if(!closed_for_read_) {
        if (read_count_pending_ <= 0) {
            closed_for_read_ = true;
        }
    }

    /*
     * If both read and write are closed then close down the connection
     */
    if (closed_for_read_ && closed_for_write_) {
        aio_close();
    }
}


void RawTunnelendpointTCP::aio_close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_close");
    switch (state_) {
    case State_Closed:
    case State_Done:
        return;
    case State_Closing:
        break;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("RawTunnelendpointTCP state not ready for aio_close");
    }

    CheckpointScope cps(*checkpoint_handler_,
                        "RawTunnelendpointTCP::aio_close",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("connection_id", connection_id_));
    socket_operation_log_info(cps);

    state_ = State_Closed;

    /*
     * close down socket
     */
    socket_operation_close();

    try {
        event_aio_com_raw_tunnelendpoint_close_start_();
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::aio_close.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
		aio_close_start(true);
    }
}

bool RawTunnelendpointTCP::is_closed(void) {
    return state_ == State_Closed || state_ == State_Done || state_ == State_Unknown;
}

bool RawTunnelendpointTCP::is_ready(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::is_ready");
    return state_ == State_Ready;
}

void RawTunnelendpointTCP::log_socket_info(Giritech::Utility::CheckpointScope& cps) const {
	socket_operation_log_info(cps);
}

void RawTunnelendpointTCP::socket_operation_log_info(CheckpointScope& cps) const {
    try {
        if (get_socket().is_open()) {
            cps.add_complete_attr(CpAttr("porttype", "tcp"));
            cps.add_complete_attr(CpAttr("local_ip", get_socket().local_endpoint().address().to_string()));
            cps.add_complete_attr(CpAttr("local_port", get_socket().local_endpoint().port()));
            cps.add_complete_attr(CpAttr("remote_ip", get_socket().remote_endpoint().address().to_string()));
            cps.add_complete_attr(CpAttr("remote_port", get_socket().remote_endpoint().port()));
        }
    }
    catch(boost::system::error_code& error) {
        cps.add_complete_attr(CpAttr("error", error.message()));
    }
    catch(boost::system::system_error& error) {
        cps.add_complete_attr(CpAttr("system_error", error.what()));
    }
}


std::pair<std::string, unsigned long> RawTunnelendpointTCP::get_ip_local(void) const {
	string ip;
	unsigned long port(0);
	try {
		if (get_socket().is_open()) {
			ip = get_socket().local_endpoint().address().to_string();
			port = get_socket().local_endpoint().port();
		}
		return make_pair(ip, port);
	}
	catch(...) {
	}
	return make_pair(ip, port);
}

std::pair<std::string, unsigned long> RawTunnelendpointTCP::get_ip_remote(void) const {
	string ip;
	unsigned long port(0);
	try {
		if (get_socket().is_open()) {
			ip = get_socket().remote_endpoint().address().to_string();
			port = get_socket().remote_endpoint().port();
		}
		return make_pair(ip, port);
	}
	catch(...) {
	}
	return make_pair(ip, port);
}

void RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_start_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_start_");
    if (eventhandler_ != NULL) {
    	event_count_pending_++;
        mutex_->get_strand().post(boost::bind(&RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_, this));
    }
}
void RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_");
	Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_", Attr_Communication(), CpAttr_debug());
    event_count_pending_--;
    if (eventhandler_ != NULL) {
        RawTunnelendpointEventhandler* temp_eventhandler(eventhandler_);
        reset_eventhandler();
        try {
        	temp_eventhandler->com_raw_tunnelendpoint_close(connection_id_);
        }
        catch (const std::exception& e) {
    		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
        }
    }
    state_ = State_Done;
}


void RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof_start_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof_start_");
    if (eventhandler_ != NULL) {
        event_count_pending_++;
        mutex_->get_strand().post(boost::bind(&RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof_, this));
    }
}

void RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof_");
    event_count_pending_--;
    if (eventhandler_ != NULL) {
    	try {
    		eventhandler_->com_raw_tunnelendpoint_close_eof(connection_id_);
    	}
        catch (const std::exception& e) {
    		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointTCP::event_aio_com_raw_tunnelendpoint_close_eof.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    	}
    }
}

DataBufferManaged::APtr RawTunnelendpointTCP::get_read_buffer_(void) {
    unsigned long buffer_size_max = traffic_control_session_->get_read_size(traffic_control_session_id_);
	if (get_socket().is_open()) {
		boost::asio::socket_base::bytes_readable command(true);
		get_socket().io_control(command);
		std::size_t bytes_readable = command.get();
	    if ((bytes_readable > 1) && (bytes_readable < buffer_size_max)) {
	    	buffer_size_max = bytes_readable;
	    }
    }
    /*
     * This is actually not needed, but added because Outlook over http can not
     * handle that a small package is cut in two.
     */
    if(buffer_size_max < 500) {
    	buffer_size_max = 500;
    }
    return DataBufferManaged::create(buffer_size_max);
}

bool RawTunnelendpointTCP::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::async_mem_guard_is_done");
	return is_closed() && (read_count_pending_ <=0) && (write_count_pending_ <=0) && (event_count_pending_ <= 0);
}

std::string RawTunnelendpointTCP::async_mem_guard_get_name(void) {
	stringstream ss;
	ss << __FUNCTION__;
	ss << " state:" << state_;
	ss << " is_closed:" << is_closed();
	ss << " read_count_pending_:" << read_count_pending_;
	ss << " write_count_pending_:" << write_count_pending_;
	ss << " event_count_pending_:" << event_count_pending_;
	return ss.str();
}

void RawTunnelendpointTCP::aio_ssl_start(const std::string& ssl_host_name, const bool& disable_certificate_verification) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_ssl_start");

    Checkpoint(
    		*checkpoint_handler_,
    		"RawTunnelendpointTCP::aio_ssl_start",
    		Attr_Communication(),
    		CpAttr_debug(),
    		CpAttr("id", connection_id_));

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	set_ssl_info(ssl_host_name, disable_certificate_verification);
	ssl_state_ = SSLState_Connecting;
	ssl_socket_.async_handshake(
			boost::asio::ssl::stream_base::client,
			boost::bind(&RawTunnelendpointTCP::aio_tls_handle_handshake,
					this,
					boost::asio::placeholders::error));
#else
    Checkpoint(
    		*checkpoint_handler_,
    		"RawTunnelendpointTCP::aio_ssl_start.ssl_not_available_on_platform",
    		Attr_Communication(),
    		CpAttr_error());
#endif
}

void RawTunnelendpointTCP::aio_tls_handle_handshake(const boost::system::error_code& error) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointTCP::aio_tls_handle_handshake");
    switch (state_) {
    case State_Ready:
        break;
    default:
        Checkpoint(*checkpoint_handler_, "RawTunnelendpointTCP::aio_tls_handle_handshake.invalid_state", Attr_Communication(), Utility::CpAttr_warning(), CpAttr("state",state_), CpAttr("id", connection_id_), CpAttr_message(error.message()));
    	return;
    }
    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointTCP::aio_tls_handle_handshake", Attr_Communication(), Utility::CpAttr_debug());

	if (!error) {
		ssl_state_ = SSLState_On;
		aio_write_from_buffer_start();

		if(ssl_do_read_start_when_connected_) {
			aio_read_start();
		}
	}
    else {
		Checkpoint(*checkpoint_handler_, "RawTunnelendpointTCP::aio_tls_handle_handshake.error", Attr_Communication(), CpAttr_error(), CpAttr_message(error.message()));
		aio_close();
    }
}
