/*! \file COM_TunnelendpointTCPMessage.hxx
 *
 * \brief This file contains message classes used by tcp-tunnelendpoints
 *
 */
#ifndef COM_TunnelendpointTCPMessage_HXX
#define COM_TunnelendpointTCPMessage_HXX

#include <map>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

namespace Giritech {
namespace Communication {

/*! \brief General tcp-tunnelendpoint message
 *
 */
class TunnelendpointTCPMessage: public boost::noncopyable {
public:
    typedef boost::shared_ptr<TunnelendpointTCPMessage> APtr;

    enum MessageType {
        MessageType_connect = 0,
        MessageType_connect_response,
        MessageType_close,
        MessageType_eof,
        MessageType_closed,
        MessageType_message,
        MessageType_checkpoint,
        MessageType_checkpoint_response
    };

    enum ReturnCodeType {
        ReturnCodeType_ok = 0, ReturnCodeType_error
    };

    /*
     * Encode the message in a databuffer for communication
     */
    Utility::DataBufferManaged::APtr create_as_buffer(void);

    /*
     * Decode message from a databuffer
     */
    static APtr create_from_buffer(const Utility::DataBufferManaged::APtr& buffer);

    /*
     * Create instance
     */
    static APtr create(const MessageType message_type, const unsigned long tunnel_id, const ReturnCodeType rc);

    /*
     * Setters
     */
    void set_payload(const Utility::DataBufferManaged::APtr& payload);

    /*
     * Getters
     */
    MessageType get_message_type(void) const;
    unsigned long get_tunnel_id(void) const;
    ReturnCodeType get_rc(void) const;
    Utility::DataBufferManaged::APtr get_payload(void) const;

    /*
     * Destructor
     */
    virtual ~TunnelendpointTCPMessage(void);

private:
    /*
     * Constructor
     */
    TunnelendpointTCPMessage(const MessageType message_type, const unsigned long tunnel_id, const ReturnCodeType rc);

    MessageType message_type_;
    unsigned long tunnel_id_;
    ReturnCodeType rc_;
    Utility::DataBufferManaged::APtr payload_;
};

}
}
#endif
