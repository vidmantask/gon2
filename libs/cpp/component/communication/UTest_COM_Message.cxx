/*! \file UTest_COM_Message.cxx
 *  \brief This file contains unittest suite for the message abstraction
 */
#include <string>
#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/crc.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <crc.h>

#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_StreamToMessage.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*!
 * \test Simpel com message dump and parse
 */
BOOST_AUTO_TEST_CASE( message_simple )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("0123456789")));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, payload, 21));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageCom::APtr message_02(stream_to_message->pop_message_com());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
}


/*!
 * \test Simpel com message dump and parse
 */
BOOST_AUTO_TEST_CASE( message_simple_small )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("")));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, payload, 21));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageCom::APtr message_02(stream_to_message->pop_message_com());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
}

/*!
 * \test Huge data payload message dump and parse
 */
BOOST_AUTO_TEST_CASE( message_huge )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create(80000, 'a')));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_DataHuge, payload, 21));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageCom::APtr message_02(stream_to_message->pop_message_com());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
}


/*!
 * \test Com message with one level of address
 */
BOOST_AUTO_TEST_CASE( message_address_one )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("0123456789")));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, payload, 21));
    message_01->push_prefix_address_element(MessageCom::MessageComAddressElement(42));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageCom::APtr message_02(stream_to_message->pop_message_com());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
    BOOST_CHECK( message_02->pop_prefix_address_element().get_address() == 42 );
}

/*!
 * \test Com message multi level of address
 */
BOOST_AUTO_TEST_CASE( message_address_more )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("0123456789")));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, payload, 21));
    message_01->push_prefix_address_element(MessageCom::MessageComAddressElement(40));
    message_01->push_prefix_address_element(MessageCom::MessageComAddressElement(41));
    message_01->push_prefix_address_element(MessageCom::MessageComAddressElement(42));
    message_01->push_prefix_address_element(MessageCom::MessageComAddressElement(43));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageCom::APtr message_02(stream_to_message->pop_message_com());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
    BOOST_CHECK( message_02->pop_prefix_address_element().get_address() == 43 );
    BOOST_CHECK( message_02->pop_prefix_address_element().get_address() == 42 );
    BOOST_CHECK( message_02->pop_prefix_address_element().get_address() == 41 );
    BOOST_CHECK( message_02->pop_prefix_address_element().get_address() == 40 );
}


/*!
 * \test Raw message dump and parse
 */
BOOST_AUTO_TEST_CASE( raw_message_simple )
{
    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("0123456789")));
    MessageRaw::APtr message_01(MessageRaw::create(MessageRaw::PackageType_KeyExchange, payload));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data( message_01->create_as_buffer() );
    MessageRaw::APtr message_02(stream_to_message->pop_message_raw());

    BOOST_CHECK( *message_01->get_payload()->get_buffer() == *message_02->get_payload()->get_buffer() );
}



/*!
 * \test Stream to message simple
 */
BOOST_AUTO_TEST_CASE( stream_to_message_01 )
{
    DataBufferManaged::APtr buffer(DataBufferManaged::create(0));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, MessagePayload::create(DataBufferManaged::create("AAAAA")), 21));
    MessageCom::APtr message_02(MessageCom::create(MessageCom::PackageType_Data, MessagePayload::create(DataBufferManaged::create("BBBBBBBBBB")), 21));

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data(message_01->create_as_buffer());
    stream_to_message->push_data(message_02->create_as_buffer());

    MessageCom::APtr message_01_out(stream_to_message->pop_message_com());
    BOOST_CHECK( message_01_out->get_payload()->get_buffer()->toString() == "AAAAA");

    MessageCom::APtr message_02_out(stream_to_message->pop_message_com());
    BOOST_CHECK( message_02_out->get_payload()->get_buffer()->toString() == "BBBBBBBBBB");

    MessageCom::APtr message_03_out(stream_to_message->pop_message_com());
    BOOST_CHECK( message_03_out.get() == NULL);
}

/*!
 * \test Stream to message multible push
 */
BOOST_AUTO_TEST_CASE( stream_to_message_02 )
{
    DataBufferManaged::APtr buffer(DataBufferManaged::create(0));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, MessagePayload::create(DataBufferManaged::create("AAAAA")), 21));
    MessageCom::APtr message_02(MessageCom::create(MessageCom::PackageType_Data, MessagePayload::create(DataBufferManaged::create("BBBBBBBBBB")), 21));

    buffer->append(message_01->create_as_buffer());
    buffer->append(message_02->create_as_buffer());

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    vector<MessageCom::APtr> messages;
    int idx = 0;
    while(idx < buffer->getSize()) {
        DataBufferManaged::APtr temp_buffer(DataBufferManaged::create(buffer->data()+ idx, 1));
        stream_to_message->push_data(temp_buffer);
        messages.push_back(stream_to_message->pop_message_com());
        ++idx;
    }
    int idxx = 0;

    for (const MessageCom::APtr& message : messages) {
        if (message != NULL) {
            cout << idxx << ": " << message->create_as_buffer()->encodeHex()->toString() << endl;
        }
        else {
            cout << idxx << ": NULL" << endl;
        }
        ++idxx;
    }
    BOOST_CHECK( messages.at(17) == NULL);
    BOOST_CHECK( messages.at(18) != NULL);
    BOOST_CHECK( messages.at(19) == NULL);

    BOOST_CHECK( messages.at(41) == NULL);
    BOOST_CHECK( messages.at(42) != NULL);
}


/*!
 * \test CRC calculation fails
 */
BOOST_AUTO_TEST_CASE( crc_test )
{
  DataBufferManaged::APtr payload(DataBufferManaged::create(""));

  unsigned long idx(0);
  while(idx < 800000) {
    char my_char(idx % 255);
    payload->append(string(&my_char, 1));
    idx++;
  }

  MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_DataHuge, MessagePayload::create(payload), 21));
    DataBufferManaged::APtr message_01_buffer(message_01->create_as_buffer());
    StreamToMessage::APtr stream_to_message(StreamToMessage::create());

    while(message_01_buffer->getSize() > 1920) {
      DataBufferManaged::APtr data_part(DataBufferManaged::create(message_01_buffer->get_raw_buffer().begin(), 1920));
      stream_to_message->push_data(data_part);
      message_01_buffer->eatFront(1920);
    }
    if(message_01_buffer->getSize() > 0) {
      stream_to_message->push_data(message_01_buffer);
    }

    MessageCom::APtr message_01_out(stream_to_message->pop_message_com());
    cout << "xxx size:" << message_01_out->get_payload()->get_buffer()->getSize() << endl;
}

BOOST_AUTO_TEST_CASE( crc_fail )
{
    DataBufferManaged::APtr buffer(DataBufferManaged::create(0));
    MessageCom::APtr message_01(MessageCom::create(MessageCom::PackageType_Data, MessagePayload::create(DataBufferManaged::create("AAAAA")), 21));

    DataBufferManaged::APtr message_01_buffer(message_01->create_as_buffer());
    message_01_buffer->data()[10] = 10;

    StreamToMessage::APtr stream_to_message(StreamToMessage::create());
    stream_to_message->push_data(message_01_buffer);

    BOOST_CHECK_THROW(MessageCom::APtr message_01_out(stream_to_message->pop_message_com()), ExceptionUnexpected);
}


BOOST_AUTO_TEST_CASE( crc_tryouts )
{
    DataBufferManaged::APtr buffer(DataBufferManaged::create("Hej med dig crc"));
    DataBufferManaged::APtr crc_buffer(DataBufferManaged::create(4,0));

    CryptoPP::CRC32 crc;
    crc.Update(buffer->data(), buffer->getSize());
    set_uint_8(crc_buffer, 0, crc.GetCrcByte(0));
    set_uint_8(crc_buffer, 1, crc.GetCrcByte(1));
    set_uint_8(crc_buffer, 2, crc.GetCrcByte(2));
    set_uint_8(crc_buffer, 3, crc.GetCrcByte(3));

    boost::uint32_t crc_error = 0L;
    convert_from_networkorder_32(crc_error, *(crc_buffer->data()), *(crc_buffer->data()+1), *(crc_buffer->data()+2), *(crc_buffer->data()+3));
    cout << "crc_tryouts CrytpoPP(ERROR)    crc:" << std::hex << std::uppercase << crc_error << std::endl;

    boost::uint32_t crc_boost_ok = 0L;
    convert_from_networkorder_32(crc_boost_ok, *(crc_buffer->data()+3), *(crc_buffer->data()+2), *(crc_buffer->data()+1), *(crc_buffer->data()));
    boost::uint32_t CRC32_NEGL = 0xffffffffL;
    crc_boost_ok ^= CRC32_NEGL;
    cout << "crc_tryouts CrytpoPP(CORRECTED)crc:" << std::hex << std::uppercase << crc_boost_ok << std::endl;


    boost::crc_32_type crc32_boost;
    crc32_boost.process_bytes(buffer->data(), buffer->getSize());
    cout << "crc_tryouts boost(CORRECT)     crc:" << std::hex << std::uppercase << crc32_boost.checksum() << std::endl;


    boost::uint32_t crc_boost_error = crc32_boost.checksum();
    crc_boost_error ^= CRC32_NEGL;

    DataBufferManaged::APtr crc_boost_buffer(DataBufferManaged::create(4,0));
//    set_uint_32(crc_boost_buffer, 0, crc32_boost.checksum());
    convert_to_networkorder_32(crc_boost_error, *(crc_boost_buffer->data()+3), *(crc_boost_buffer->data()+2), *(crc_boost_buffer->data()+1), *(crc_boost_buffer->data()));
    cout << "crc_tryouts boost(ERROR)       crc:" << crc_boost_buffer->encodeHex()->toString() << std::endl;

}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
    return 0;
}
