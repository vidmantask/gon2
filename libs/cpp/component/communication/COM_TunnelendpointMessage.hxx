/*! \file COM_TunnelendpointMessage.hxx
 *
 * \brief This file contains message classes used by tunnelendpoints
 *
 */
#ifndef COM_TunnelendpointMessage_HXX
#define COM_TunnelendpointMessage_HXX

#include <map>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/version/Version.hxx>


namespace Giritech {
namespace Communication {

/*! \brief General tunnelendpoint message
 *
 */
class TunnelendpointMessage: public boost::noncopyable {
public:
    typedef boost::shared_ptr<TunnelendpointMessage> APtr;

    enum MessageType {
        MessageType_connect = 0,
        MessageType_connect_response,
        MessageType_disconnect
     };

    enum ReturnCodeType {
        ReturnCodeType_ok = 0, ReturnCodeType_error
    };

    /*
     * Encode the message in a databuffer for communication
     */
    Utility::DataBufferManaged::APtr create_as_buffer(void);

    /*
     * Decode message from a databuffer
     */
    static APtr create_from_buffer(const Utility::DataBufferManaged::APtr& buffer);

    /*
     * Create instance
     */
    static APtr create(const MessageType message_type, const ReturnCodeType rc);

    /*
     * Setters
     */
    void set_version(const Giritech::Version::Version::APtr& version);

    /*
     * Getters
     */
    MessageType get_message_type(void) const;
    ReturnCodeType get_rc(void) const;
    Giritech::Version::Version::APtr get_version(void) const;

    /*
     * Destructor
     */
    virtual ~TunnelendpointMessage(void);

private:
    /*
     * Constructor
     */
    TunnelendpointMessage(const MessageType message_type, const ReturnCodeType rc);

    MessageType message_type_;
    ReturnCodeType rc_;
    Giritech::Version::Version::APtr version_;
};

}
}
#endif
