/*! \file UTest_COM_Session.cxx
 * \brief This file contains unittest suite for the session functionality
 */
#include <string>

#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionMessage.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;

/*!
 *  Demo tunnelendpoint eventhandler
 */
class UTest_COM_Session_TunnelendpointEventhandler : public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<UTest_COM_Session_TunnelendpointEventhandler> APtr;

    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        ++recived_messages_;
        last_message_ = message;
        cout << "tunnelendpoint_recieve" << endl;
    }

    void tunnelendpoint_eh_connected(void) {
        cout << "Connected" << endl;
    }

    unsigned long recived_messages_;
    MessagePayload::APtr last_message_;

    static APtr create(void) {
        return APtr(new UTest_COM_Session_TunnelendpointEventhandler);
    }
private:
    UTest_COM_Session_TunnelendpointEventhandler(void) :
        recived_messages_(0) {
    }
};
void helper_add_tunnelendpoint(boost::asio::io_service& io,
                               const CheckpointHandler::APtr& checkpoint_handler,
                               vector< Tunnelendpoint::APtr >& tunnelendpoints,
                               vector< UTest_COM_Session_TunnelendpointEventhandler::APtr >& tunnelendpoint_eventhandlers,
                               const Session::APtr& session,
                               const unsigned long& child_id) {
    tunnelendpoint_eventhandlers.push_back(UTest_COM_Session_TunnelendpointEventhandler::create());
    tunnelendpoints.push_back(TunnelendpointReliableCryptedTunnel::create(io, checkpoint_handler, tunnelendpoint_eventhandlers.back().get()));
    session->add_tunnelendpoint(child_id, tunnelendpoints.back());
}

/*!
 *  Demo server
 */
class UTest_COM_Session_Server : public RawTunnelendpointAcceptorTCPEventhandler,
    public AsyncContinuePolicy, public SessionEventhandler, public SessionEventhandlerReady {
public:
    UTest_COM_Session_Server(const CheckpointHandler::APtr& checkpoint_handler,
                             boost::asio::io_service& io,
                             const std::string& host,
                             const unsigned long port,
                             const DataBufferManaged::APtr& known_secret) :
                            	 io_(io),
        read_start_count_(0), checkpoint_handler_(checkpoint_handler), known_secret_(known_secret),
                accepted_(false), key_exchange_complete_(false), data_recived_(0) {
        acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler,
                                                         io,
                                                         host,
                                                         port,
                                                          this,
                                                          this);
        acceptor_->set_option_reuse_address(true);
    }

    void listen(void) {
        acceptor_->aio_accept_start();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        accepted_ = true;
        session_ = Session::create_server(io_, 1, "1", false, checkpoint_handler_, tunnelendpoint, known_secret_);

        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 1);
        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 2);
        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 3);

        session_->set_eventhandler(this);
        session_->set_eventhandler_ready(this);
        session_->do_key_exchange();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    }

    void com_tunnelendpoint_acceptor_closed(void) {
    }

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void) {
        return false;
    }
    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_ready(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_closed(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_key_exchange(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    bool session_read_continue_state_ready(void) {
        if (read_start_count_ > 0) {
            --read_start_count_;
            return true;
        }
        return false;
    }
    /*! \brief signal from SessionEventhandler
     */
    void session_closed(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_security_closed(const unsigned long session_id, const std::string& remote_ip) {
    }

    /*! \brief User defined signal from a tunnelendpoint
     */
    void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    }

    bool accepted_;
    bool key_exchange_complete_;
    unsigned long data_recived_;
    Session::APtr session_;
    vector<Tunnelendpoint::APtr> tunnelendpoints_;
    vector<UTest_COM_Session_TunnelendpointEventhandler::APtr> tunnelendpoint_eventhandlers_;
    unsigned char read_start_count_;

private:
	boost::asio::io_service& io_;
    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointAcceptorTCP::APtr acceptor_;
};

/*!
 *  Demo client
 */
class UTest_COM_Session_Client : public RawTunnelendpointConnectorTCPEventhandler,
    public SessionEventhandler, public SessionEventhandlerReady {
public:
    UTest_COM_Session_Client(const CheckpointHandler::APtr& checkpoint_handler,
                             boost::asio::io_service& io,
                             const std::string& host,
                             const unsigned long port,
                             const DataBufferManaged::APtr& known_secret) :
        io_(io), read_start_count_(0), checkpoint_handler_(checkpoint_handler), known_secret_(known_secret),
                connected_(false), key_exchange_complete_(false), data_recived_(0) {
        connector_ = RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, host, port, this);
    }

    void connect(void) {
        connector_->aio_connect_start();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        connected_ = true;
        session_ = Session::create_client(io_, 1, "1", false, checkpoint_handler_, tunnelendpoint, known_secret_, SessionMessage::ApplProtocolType_python);
        session_->set_eventhandler(this);
        session_->set_eventhandler_ready(this);

        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 1);
        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 2);
        helper_add_tunnelendpoint(io_, checkpoint_handler_, tunnelendpoints_, tunnelendpoint_eventhandlers_, session_, 3);
        session_->do_key_exchange();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& messag) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_ready(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_closed(void) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    void session_state_key_exchange(void) {
    }

    /*! \brief User defined signal from a tunnelendpoint
     */
    void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    }

    /*! \brief signal from SessionEventhandlerReady
     */
    bool session_read_continue_state_ready(void) {
        if (read_start_count_ > 0) {
            --read_start_count_;
            return true;
        }
        return false;
    }
    /*! \brief signal from SessionEventhandler
     */
    void session_closed(const unsigned long session_id) {
    }

    /*! \brief signal from SessionEventhandler
     */
    void session_security_closed(const unsigned long session_id, const std::string& remote_ip) {
    }

    bool connected_;
    bool key_exchange_complete_;
    unsigned long data_recived_;
    Session::APtr session_;

    vector<Tunnelendpoint::APtr> tunnelendpoints_;
    vector<UTest_COM_Session_TunnelendpointEventhandler::APtr> tunnelendpoint_eventhandlers_;
    unsigned char read_start_count_;

private:
    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointConnectorTCP::APtr connector_;
    boost::asio::io_service& io_;
};

/* Create checkpoint handler */
CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create()));
CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create(checkpoint_filter,
                                                                     checkpoint_output_handler));
/*
 * Simpel test session
 */
BOOST_AUTO_TEST_CASE( session_test )
{
    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);
    cout << "client :" << known_secret_client->encodeHex()->toString() << endl;
    cout << "server :" << known_secret_server->encodeHex()->toString() << endl;

    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_Session_Server server(checkpoint_handler, ios, host, port, known_secret_server);
    UTest_COM_Session_Client client(checkpoint_handler, ios, host, port, known_secret_client);

    server.read_start_count_ = 8;
    client.read_start_count_ = 8;

    server.listen();
    client.connect();
    ios.run_for(boost::asio::chrono::seconds(1));

    BOOST_CHECK( server.accepted_ );
    BOOST_CHECK( client.connected_ );
    server.read_start_count_ = 1;
    client.read_start_count_ = 2;
    server.session_->read_start_state_ready();
    client.session_->read_start_state_ready();

    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("AAAAAAAAAA")));
    server.tunnelendpoints_.back()->tunnelendpoint_send(payload);

    ios.reset();
    ios.run_for(boost::asio::chrono::seconds(1));

    BOOST_CHECK( client.tunnelendpoint_eventhandlers_.back()->recived_messages_ == 1 );
    BOOST_CHECK( *(client.tunnelendpoint_eventhandlers_.back()->last_message_->get_buffer()) == *(payload->get_buffer()) );
}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
