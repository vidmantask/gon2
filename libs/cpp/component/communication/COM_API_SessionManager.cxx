/*! \file COM_API_SessionManager.cxx
 \brief This file contains the implementation for the COM_API_SessionManager classes
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <component/communication/COM_API_SessionManager.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <boost/thread.hpp>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APISessionManagerServer implementation
 * ------------------------------------------------------------------
 */
APISessionManagerServer::APISessionManagerServer(const APIAsyncService::APtr& api_async_service,
                                                 APISessionManagerEventhandler* session_manager_eventhandler,
                                                 const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                 const std::string& known_secret_str,
                                                 const std::string& server_ip,
                                                 const unsigned long& server_port,
                                                 const std::string& sid) :
    session_manager_eventhandler_(session_manager_eventhandler), checkpoint_handler_(checkpoint_handler) {

    try {
        known_secret_ = DataBufferManaged::create(known_secret_str)->decodeHex();
    } catch (DataBuffer::Exception_decoding& e) {
        known_secret_ = DataBufferManaged::create("");
        Checkpoint cp(*checkpoint_handler, "unabel_to_decode_known_secret", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
    }

    impl_session_manager_ = SessionManagerServer::create(api_async_service->get_impl(), checkpoint_handler, known_secret_, server_ip, server_port, sid);
    impl_session_manager_->set_eventhandler(this);
}

APISessionManagerServer::~APISessionManagerServer(void) {
}

APIMutex::APtr APISessionManagerServer::get_mutex(void) {
    return APIMutex::create(impl_session_manager_->get_mutex());
}

void APISessionManagerServer::set_config_idle_timeout(const boost::posix_time::time_duration& session_idle_timeout, const boost::posix_time::time_duration& keyexchange_idle_timeout) {
    impl_session_manager_->set_config_idle_timeout(session_idle_timeout, keyexchange_idle_timeout);
}

void APISessionManagerServer::set_config_log_interval(const boost::posix_time::time_duration& config_log_interval) {
    impl_session_manager_->set_config_log_interval(config_log_interval);
}

void APISessionManagerServer::set_config_session_logging_enabled(const bool& session_logging_enabled) {
    impl_session_manager_->set_config_session_logging_enabled(session_logging_enabled);
}

void APISessionManagerServer::set_option_reuse_address(const bool& option_reuse_address) {
    impl_session_manager_->set_option_reuse_address(option_reuse_address);
}
void APISessionManagerServer::set_config_security_dos_attack(const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips) {
    impl_session_manager_->set_config_security_dos_attack(keyexchange_phase_one_count_high, keyexchange_phase_one_count_low, security_closed_count_high, security_closed_count_low, ban_atacker_ips);
}

void APISessionManagerServer::set_config_https(
		const std::string& certificate_path,
		const bool certificate_verificatrion_enabled,
		const bool hostname_verification_enabled,
		const bool sni_enabled,
		const std::string& method,
		const unsigned long ssl_options) {
	impl_session_manager_->set_config_https(certificate_path, certificate_verificatrion_enabled, hostname_verification_enabled, sni_enabled, method, ssl_options);
}

void APISessionManagerServer::start(void) {
    impl_session_manager_->start();
}

void APISessionManagerServer::accept_connections_start(void) {
    impl_session_manager_->accept_connections_start();
}

void APISessionManagerServer::accept_connections_stop(void) {
    impl_session_manager_->accept_connections_stop();
}

void APISessionManagerServer::close_start(void) {
    impl_session_manager_->close_start();
}

bool APISessionManagerServer::is_closed(void) const {
    return impl_session_manager_->is_closed();
}

void APISessionManagerServer::session_manager_resolve_connection(std::string& type,
                                                                 std::string& host,
                                                                 unsigned long& port,
                                                                 boost::posix_time::time_duration& timeout) {
    // Signal ignored, only relevant on client side
}

void APISessionManagerServer::session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerServer::session_manager_connecting");
        session_manager_eventhandler_->session_manager_connecting(connection_id, title);
    }
}

void APISessionManagerServer::session_manager_connecting_failed(const unsigned long& connection_id) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerServer::session_manager_connecting");
        session_manager_eventhandler_->session_manager_connecting_failed(connection_id);
    }
}

void APISessionManagerServer::session_manager_failed(const std::string& message) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_failed(message);
    }
}

void APISessionManagerServer::session_manager_session_created(const unsigned long& connection_id, Session::APtr& session) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerServer::session_manager_connecting");
        APISession::APtr wrapped_session(APISession::create(session));
        session_manager_eventhandler_->session_manager_session_created(connection_id, wrapped_session);
    }
}

void APISessionManagerServer::session_manager_session_closed(const unsigned long session_id) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(session_id), "APISessionManagerServer::session_manager_connecting");
        session_manager_eventhandler_->session_manager_session_closed(session_id);
    }
}

void APISessionManagerServer::session_manager_closed(void) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_closed();
        session_manager_eventhandler_ = NULL;
    }
}

void APISessionManagerServer::session_manager_security_dos_attack_start(const std::string& attacker_ips) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_security_dos_attack_start(attacker_ips);
    }
}

void APISessionManagerServer::session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_security_dos_attack_stop(attacker_ips, banned_connection_count, failed_keyexchange_count);
    }
}

APISessionManagerServer::APtr APISessionManagerServer::create(const APIAsyncService::APtr& api_async_service,
                                                              APISessionManagerEventhandler* session_manager_eventhandler,
                                                              const Utility::APICheckpointHandler::APtr& wrap_checkpoint_handler,
                                                              const std::string& known_secret,
                                                              const std::string& server_ip,
                                                              const unsigned long& server_port,
                                                              const std::string& sid) {
    return APtr(new APISessionManagerServer(api_async_service, session_manager_eventhandler,
                                            wrap_checkpoint_handler->get_checkpoint_handler(), known_secret, server_ip, server_port, sid));
}

void APISessionManagerServer::self_start(const APISessionManagerServer::APtr& self) {
    self->start();
}

void APISessionManagerServer::self_accept_connections_stop(const APISessionManagerServer::APtr& self) {
	self->accept_connections_stop();
}

void APISessionManagerServer::self_accept_connections_start(const APISessionManagerServer::APtr& self) {
	self->accept_connections_start();
}

void APISessionManagerServer::self_close_start(const APISessionManagerServer::APtr& self) {
    self->close_start();
}
bool APISessionManagerServer::self_is_closed(const APISessionManagerServer::APtr& self) {
    return self->is_closed();
}
void APISessionManagerServer::self_set_config_idle_timeout_sec(const APISessionManagerServer::APtr& self, const unsigned long& session_timeout_sec, const unsigned long& keyexchange_timeout) {
    self->set_config_idle_timeout(boost::posix_time::seconds(session_timeout_sec), boost::posix_time::seconds(keyexchange_timeout));
}

void APISessionManagerServer::self_set_config_log_interval_sec(const APISessionManagerServer::APtr& self,
                                                               const unsigned long& sec) {
    self->set_config_log_interval(boost::posix_time::seconds(sec));
}
void APISessionManagerServer::self_set_config_log_interval_min(const APISessionManagerServer::APtr& self,
                                                               const unsigned long& min) {
    self->set_config_log_interval(boost::posix_time::minutes(min));
}

void APISessionManagerServer::self_set_config_session_logging_enabled(const APISessionManagerServer::APtr& self, const bool& session_logging_enabled) {
    self->set_config_session_logging_enabled(session_logging_enabled);
}

void APISessionManagerServer::self_set_option_reuse_address(const APISessionManagerServer::APtr& self, const bool& session_logging_enabled) {
    self->set_option_reuse_address(session_logging_enabled);
}

void APISessionManagerServer::self_set_config_security_dos_attack(const APISessionManagerServer::APtr& self,const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips) {
    self->set_config_security_dos_attack(keyexchange_phase_one_count_high, keyexchange_phase_one_count_low, security_closed_count_high, security_closed_count_low, ban_atacker_ips);
}

void APISessionManagerServer::self_set_config_https(
		const APISessionManagerServer::APtr& self,
		const std::string& certificate_path,
		const bool certificate_verificatrion_enabled,
		const bool hostname_verification_enabled,
		const bool sni_enabled,
		const std::string& method,
		const unsigned long ssl_options) {
    self->set_config_https(certificate_path, certificate_verificatrion_enabled, hostname_verification_enabled, sni_enabled, method, ssl_options);
}

APIMutex::APtr APISessionManagerServer::self_get_mutex(const APISessionManagerServer::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}




/*
 * ------------------------------------------------------------------
 * APISessionManagerClient implementation
 * ------------------------------------------------------------------
 */
APISessionManagerClient::APISessionManagerClient(const APIAsyncService::APtr& api_async_service,
                                                 APISessionManagerEventhandler* session_manager_eventhandler,
                                                 const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                 const std::string& known_secret_str,
                                                 const SessionMessage::ApplProtocolType appl_protocol_type) :
    session_manager_eventhandler_(session_manager_eventhandler), checkpoint_handler_(checkpoint_handler) {
    try {
        known_secret_ = DataBufferManaged::create(known_secret_str)->decodeHex();
    } catch (DataBuffer::Exception_decoding& e) {
        known_secret_ = DataBufferManaged::create("");
        Checkpoint cp(*checkpoint_handler, "unabel_to_decode_known_secret", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
    }
    impl_session_manager_ = SessionManagerClient::create(api_async_service->get_impl(), checkpoint_handler, known_secret_);
    impl_session_manager_->set_eventhandler(this);
    impl_session_manager_->set_appl_protocol_type(appl_protocol_type);
}

APISessionManagerClient::~APISessionManagerClient(void) {
}

void APISessionManagerClient::set_eventhandler(APISessionManagerEventhandler* session_manager_eventhandler) {
	session_manager_eventhandler_ = session_manager_eventhandler;
    impl_session_manager_->set_eventhandler(this);
}

APIMutex::APtr APISessionManagerClient::get_mutex(void) {
    return APIMutex::create(impl_session_manager_->get_mutex());
}

void APISessionManagerClient::set_servers(const std::string& servers_spec) {
    impl_session_manager_->set_servers(servers_spec);
}

void APISessionManagerClient::add_server_connection(const std::string& server_ip, const unsigned long server_port) {
    impl_session_manager_->add_gserver_connection(server_ip, server_port);
}

void APISessionManagerClient::set_config_session_logging_enabled(const bool& session_logging_enabled) {
    impl_session_manager_->set_config_session_logging_enabled(session_logging_enabled);
}

void APISessionManagerClient::start(void) {
    impl_session_manager_->start();
}

void APISessionManagerClient::close_start(void) {
    impl_session_manager_->close_start();
}

bool APISessionManagerClient::is_closed(void) const {
    return impl_session_manager_->is_closed();
}

void APISessionManagerClient::session_manager_resolve_connection(std::string& type,
                                                                 std::string& host,
                                                                 unsigned long& port,
                                                                 boost::posix_time::time_duration& timeout) {

    PyGILState_STATE state = PyGILState_Ensure();
	if (session_manager_eventhandler_ != NULL) {
        boost::python::dict resolve_info;
        resolve_info["type"] = type;
        resolve_info["host"] = host;
        resolve_info["port"] = port;
        resolve_info["timeout_sec"] = timeout.total_seconds();

        boost::python::dict resolve_info_result(session_manager_eventhandler_->session_manager_resolve_connection(resolve_info));
		try {
			type = boost::python::extract<string>(resolve_info_result.get("type"));
		} catch(boost::python::error_already_set) {
	    	PyErr_Clear();
		}
		try {
			host = boost::python::extract<string>(resolve_info_result.get("host"));
		} catch(boost::python::error_already_set) {
	    	PyErr_Clear();
		}
		try {
			port = boost::python::extract<unsigned long>(resolve_info_result.get("port"));
		} catch(boost::python::error_already_set) {
	    	PyErr_Clear();
		}
		try {
      unsigned long sec = boost::python::extract<unsigned long>(resolve_info_result.get("timeout_sec"));
			timeout = boost::posix_time::seconds(sec);
		} catch(boost::python::error_already_set) {
	    	PyErr_Clear();
		}
    }
    PyGILState_Release(state);
}

void APISessionManagerClient::session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerClient::session_manager_connecting");
        session_manager_eventhandler_->session_manager_connecting(connection_id, title);
    }
}

void APISessionManagerClient::session_manager_connecting_failed(const unsigned long& connection_id) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerClient::session_manager_connecting");
        session_manager_eventhandler_->session_manager_connecting_failed(connection_id);
    }
}

void APISessionManagerClient::session_manager_failed(const std::string& message) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_failed(message);
    }
}

void APISessionManagerClient::session_manager_session_created(const unsigned long& connection_id, Session::APtr& session) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(connection_id), "APISessionManagerClient::session_manager_connecting");
        APISession::APtr wrapped_session(APISession::create(session));
        session_manager_eventhandler_->session_manager_session_created(connection_id, wrapped_session);
    }
}

void APISessionManagerClient::session_manager_session_closed(const unsigned long session_id) {
    if (session_manager_eventhandler_ != NULL) {
    	// Session lock is taken before entering python (potential takeing pythongil), order of mutexes are  manager -> session -> pygil
        MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), impl_session_manager_->get_session_mutex(session_id), "APISessionManagerClient::session_manager_connecting");
        session_manager_eventhandler_->session_manager_session_closed(session_id);
    }
}

void APISessionManagerClient::session_manager_closed(void) {
    if (session_manager_eventhandler_ != NULL) {
        session_manager_eventhandler_->session_manager_closed();
        session_manager_eventhandler_ = NULL;
    }
}

void APISessionManagerClient::session_manager_security_dos_attack_start(const std::string& attacker_ips) {
	// Ignore signal, not relevant on client
}

void APISessionManagerClient::session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) {
	// Ignore signal, not relevant on client
}

APISessionManagerClient::APtr APISessionManagerClient::create(const APIAsyncService::APtr& api_async_service,
                                                              APISessionManagerEventhandler* session_manager_eventhandler,
                                                              const Utility::APICheckpointHandler::APtr& wrap_checkpoint_handler,
                                                              const std::string& known_secret,
                                                              const SessionMessage::ApplProtocolType appl_protocol_type) {
    return APtr(new APISessionManagerClient(api_async_service, session_manager_eventhandler, wrap_checkpoint_handler->get_checkpoint_handler(), known_secret, appl_protocol_type));
}

void APISessionManagerClient::self_set_eventhandler(const APISessionManagerClient::APtr& self, APISessionManagerEventhandler* session_manager_eventhandler) {
    self->set_eventhandler(session_manager_eventhandler);
}

void APISessionManagerClient::self_start(const APISessionManagerClient::APtr& self) {
    self->start();
}

void APISessionManagerClient::self_close_start(const APISessionManagerClient::APtr& self) {
    self->close_start();
}

bool APISessionManagerClient::self_is_closed(const APISessionManagerClient::APtr& self) {
    return self->is_closed();
}

void APISessionManagerClient::self_set_servers(const APISessionManagerClient::APtr& self,
                                               const std::string& servers_spec) {
    self->set_servers(servers_spec);
}
void APISessionManagerClient::self_add_server_connection(const APISessionManagerClient::APtr& self, const std::string& server_ip, const unsigned long server_port) {
    self->add_server_connection(server_ip, server_port);
}

void APISessionManagerClient::self_set_config_session_logging_enabled(const APISessionManagerClient::APtr& self,
                                                                      const bool& session_logging_enabled) {
    self->set_config_session_logging_enabled(session_logging_enabled);
}

APIMutex::APtr APISessionManagerClient::self_get_mutex(const APISessionManagerClient::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}
