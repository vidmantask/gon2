/*! \file COM_API_RawTunnelendpointConnector.hxx
 *  \brief This file contains the a API wrapper for raw-tunnelendpoint connector
 */
#ifndef COM_API_RawTunnelendpointConnector_HXX
#define COM_API_RawTunnelendpointConnector_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler for a tcp-tunnelendpoint connector
 */
class APIRawTunnelendpointConnectorTCPEventhandler: public boost::noncopyable {
public:
    /*! \brief Connected signal
     *
     *  \param tunnelendpoint connected raw tunnelendpoint
     *
     *  The connector send this signal as a response to call to RawTunnelendpointConnectorTCP::aio_connect_start().
     */
    virtual void com_tunnelendpoint_connected(const APIRawTunnelendpointTCP::APtr& tunnelendpoint) = 0;

    /*! \brief Failed to connect signal
     *
     *  The connector send this signal it it was not possible to connect,
     *  as a response to call to RawTunnelendpointConnectorTCP::aio_connect_start().
     */
    virtual void com_tunnelendpoint_connection_failed_to_connect(const std::string& message) = 0;
};

/*! \brief API for RawTunnelendpointConnectorTCP class see this class for documentation
 */
class APIRawTunnelendpointConnectorTCP: public RawTunnelendpointConnectorTCPEventhandler {
public:
    typedef boost::shared_ptr<APIRawTunnelendpointConnectorTCP> APtr;

    /*! \brief Destructor
     */
    ~APIRawTunnelendpointConnectorTCP(void);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APIRawTunnelendpointConnectorTCPEventhandler* api_eventhandler);

    /*! \brief reset eventhandler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief Ask for a connection to a remote raw endpoint
     */
    void aio_connect_start(void);

    /*! \brief Set connection timeout
     */
    void set_connect_timeout(const boost::posix_time::time_duration& connect_timeout);

    /*Force close of pending connect attempts
     */
    void close(void);

    /*! \brief Enable SSL for connections
     */
    void enable_ssl(const bool& disable_certificate_verification);

    /*! \brief Get instance mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Set instance mutex
     */
    void set_mutex(const APIMutex::APtr& api_mutex);

    /*! \brief Create instance
     */
    static APtr create(const Utility::APICheckpointHandler::APtr& wrap_checkpoint_handler,
                       const APIAsyncService::APtr& async_service,
                       const std::string& remote_host,
                       const unsigned long& remote_port);

    /*
     * Implementation of raw eventhandlers
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint);
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& message);

    /*
     * Self methods for easy python-api defintion
     */
    static void self_reset_tcp_eventhandler(const APtr& self);
    static void self_set_tcp_eventhandler(const APtr& self, APIRawTunnelendpointConnectorTCPEventhandler* api_eventhandler);
    static void self_aio_connect_start(const APtr& self);
    static APIMutex::APtr self_get_mutex(const APtr& self);
    static void self_set_mutex(const APtr& self, const APIMutex::APtr& api_mutex);
    static void self_close(const APtr& self);
    static void self_enable_ssl(const APtr& self, const bool& disable_certificate_verification);

    static void self_set_connect_timeout_sec(const APtr& self, unsigned int sec);

private:
    APIRawTunnelendpointConnectorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                     const APIAsyncService::APtr& async_service,
                                     const std::string& remote_host,
                                     const unsigned long& remote_port);

    RawTunnelendpointConnectorTCP::APtr impl_tunnelendpoint_;
    APIRawTunnelendpointConnectorTCPEventhandler* api_eventhandler_;
};

}
}
#endif
