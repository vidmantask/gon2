/*! \file COM_SessionManagerEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from a session manager
 */
#ifndef COM_SessionManagerEventhandler_HXX
#define COM_SessionManagerEventhandler_HXX

#include <component/communication/COM_Session.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for handling signals from a session manager
 */
class SessionManagerEventhandler: public boost::noncopyable {
public:

    /*! \brief Ask for resolve of connection
     */
    virtual void session_manager_resolve_connection(std::string& type,
                                                    std::string& host,
                                                    unsigned long& port,
                                                    boost::posix_time::time_duration& timeout) = 0;

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    virtual void session_manager_connecting(const unsigned long& connection_id, const std::string& title) = 0;

    /*! \brief Signals that a the session manager failed when trying to connect to server
     */
    virtual void session_manager_connecting_failed(const unsigned long& connection_id) = 0;

    /*! \brief Signals that a the session manager failed in estabblished in connecting og setup of connection
     */
    virtual void session_manager_failed(const std::string& message) = 0;

    /*! \brief Signals that a session has been created
     */
    virtual void session_manager_session_created(const unsigned long& connection_id, Session::APtr& session) = 0;

    /*! \brief Signals that a session has been created
     */
    virtual void session_manager_session_closed(const unsigned long session_id) = 0;

    /*! \brief Signals that the session manager has closed, and this is the last event ever sent.
     */
    virtual void session_manager_closed(void) = 0;

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    virtual void session_manager_security_dos_attack_start(const std::string& attacker_ips) = 0;

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    virtual void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) = 0;

};
}
}
#endif
