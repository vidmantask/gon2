/*! \file Servers_Spec.hxx
 \brief This file contains servers sepecification
 */
#ifndef Servers_Spec_HXX
#define Servers_Spec_HXX

#include <string>
#include <list>

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>

#include <tinyxml2.h>

#include <lib/utility/UY_Exception.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class represent Servers specification
 *
 * <servers>
 *   <connection_group title="" selection_delay_sec="1">
 *     <connection title="" host="192.168.0.1" port="3945" timeout_sec="15" type="direc|http"/>
 *     ...
 *     <connection ... />
 *   </connection_group>
 * ...
 *   <connection_group ...>
 *   ...
 *   </connection_group>
 * </servers>
 *
 *
 */
class ServersSpec {
public:
    typedef boost::shared_ptr<ServersSpec> APtr;

    class Exception_ServersSpec: public Giritech::Utility::Exception {
    public:
        Exception_ServersSpec(const std::string& message) :
            Exception(message) {
        }
    };


    struct ServersConnection {
        enum ServersConnectionType {
            ConnectionType_direct = 0,
            ConnectionType_http
        };

        std::string title;
        std::string host;
        unsigned long port;
        boost::posix_time::time_duration timeout;
        ServersConnectionType type;
    };

    struct ServersConnectionGroup {
        std::string title;
        boost::posix_time::time_duration selection_delay;
        std::vector<ServersConnection> connections;
    };


    /*! \brief Destructor
     */
    ~ServersSpec();

    /*! \brief Load document from file or string.
     *
     * If an error is found the exception Exception_ServersSpec is thrown
     */
    void load_from_file(const std::string& filename);
    void load_from_string(const std::string& xml_document);


    /*! \brief Get a list connection groups found in the document
     *
     * If an error is found the exception Exception_ServersSpec is thrown
     */
    void get_servers_connection_groups(std::vector<ServersConnectionGroup>& connection_groups);

    /*! \brief Get a list connection found in the document
     *
     * If an error is found the exception Exception_ServersSpec is thrown
     */
    void get_servers_connections(std::vector<ServersConnection>& connections);

    /*! \brief Create instance
     */
    static ServersSpec::APtr create(void);


private:
    ServersSpec(void);

    void get_servers_connection_groups(std::vector<ServersConnectionGroup>& connection_groups, tinyxml2::XMLElement* servers_spec_connection_group);
    void get_servers_connection(std::vector<ServersConnection>& connections, tinyxml2::XMLElement* servers_spec_connection);

    tinyxml2::XMLDocument servers_spec_doc_;

};

}
}
#endif
