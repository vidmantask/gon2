/*! \file COM_TunnelendpointDirect.hxx
 \brief This file contains the tunnelendpoint direct clases
 */
#ifndef COM_TunnelendpointDirect_HXX
#define COM_TunnelendpointDirect_HXX

#include <map>
#include <queue>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>


#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointDirectControlEventhandler.hxx>
#include <component/communication/COM_TunnelendpointCheckpointHandler.hxx>



namespace Giritech {
namespace Communication {


/*
 * Forward declarations of eventhandlers
 */
class TunnelendpointDirectEventhandler;
class TunnelendpointDirectTunnelClientEventhandler;


/*! \brief Tunnelendpoint for handling traffic for a direct connection
 *
 * A Direct-Tunnelendpoint handles the traffic of a direct connection using api
 *
 */
class TunnelendpointDirect :
	public TunnelendpointReliableCrypted,
	public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<TunnelendpointDirect> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointDirect(void);


    boost::asio::io_service& get_io_service(void);

    /*! \brief Set event handler
     */
    void set_direct_eventhandler(TunnelendpointDirectEventhandler* direct_eventhandler);
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Set tunnel id
     */
    void set_tunnel_id(const unsigned long tunnel_id);


    void set_unique_session_id(const std::string& unique_session_id);
    std::string get_unique_session_id(void);


    /*! \brief Reset event handler
     */
    void reset_direct_eventhandler(void);
    void reset_direct_ctrl_eventhandler(void);

    /*! \brief Close tunnelendpoint
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    virtual void close(const bool force);
    bool is_closed(void);

    /*! \brief Notification that the tunnelendpoint is trying to connect to its server/client counterpart
     */
    void connecting(void);

    /*! \brief Notification that the tunnelendpoint is connected to its server/client counterpart
     */
    void connected(void);

    /*! \brief Write data to tunnelendpoint
     */
    void write(const Utility::DataBufferManaged::APtr& data);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);


    Giritech::Utility::CheckpointHandler::APtr get_checkpoint_handler(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Create instance
     */
    static APtr create(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		TunnelendpointDirectControlEventhandler* direct_ctrl_eventhandler);

protected:

    /*! \brief Constructor
     */
    TunnelendpointDirect(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		TunnelendpointDirectControlEventhandler* direct_ctrl_eventhandler);

    void goto_ready_state_(void);
    void goto_closed_state_(void);

    boost::asio::io_service& io_;
    TunnelendpointDirectEventhandler* direct_eventhandler_;
    TunnelendpointDirectControlEventhandler* direct_ctrl_eventhandler_;

    enum StateDirect {
        StateDirect_Initializing,
        StateDirect_Connecting,
        StateDirect_Ready,
        StateDirect_Closing,
        StateDirect_Closed,
        StateDirect_Unknown
    };

    StateDirect state_direct_;
    unsigned long tunnel_id_;
    std::string unique_session_id_;
};




/*! \brief Client side tunnelendpoint factoryfor handling Direct connections.
 *TunnelendpointReliableCrypted
 * This client-side factory creates tunnelendpoint for direct-connections using api
 *
 */
class TunnelendpointDirectTunnelClient:
	public TunnelendpointReliableCryptedTunnel,
	public TunnelendpointEventhandler,
	public TunnelendpointDirectControlEventhandler {

public:
    typedef boost::shared_ptr<TunnelendpointDirectTunnelClient> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointDirectTunnelClient(void);

    /*! \brief Set event handler
     */
    void set_direct_eventhandler(TunnelendpointDirectTunnelClientEventhandler* direct_eventhandler);
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Reset event handler
     */
    void reset_direct_eventhandler(void);

    /*! \brief Called by parent/owner of tunnelendpoint
     */
    virtual void connect(void);

    /*! \brief Close tunnelendpoint and all its children connections
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    virtual void close(const bool force);

    /*! \brief Return true if the tunnelendpoint is closed
     */
    bool is_closed();

    /*! \brief Star the accepting connections
     *
     * Request a connection. When a connectin is ready it is returned in callback
     */
    void request_connection(void);


    Giritech::Utility::CheckpointHandler::APtr get_checkpoint_handler(void);
    boost::asio::io_service& get_io_service(void);


    /*! \brief Signals that the tunnelendpoint has recieved a message

     *  Implementation of TunnelendpointEventhandler signal handling
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);


    /*! \brief Sends a message
     */
    virtual void tunnelendpoint_direct_send(const Utility::DataBufferManaged::APtr& message);


    /*! \brief Implementation of TunnelendpointDirectControlEventhandler interface
     */
    virtual void tunnelendpoint_direct_eh_eof(const unsigned long tunnel_id);
    virtual void tunnelendpoint_direct_eh_closed(const unsigned long tunnel_id);

    /*! \brief Enable use of special checkpoint tunnel that logs directly to the server
     */
    Utility::CheckpointHandler::APtr enable_checkpoint_tunnel(const Utility::CheckpointFilter::APtr& checkpoint_filter, const Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler);


    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const unsigned long connection_pool_size);

protected:

    /*! \brief Constructor
     */
    TunnelendpointDirectTunnelClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                  	  boost::asio::io_service& io,
                                  	  const unsigned long connection_pool_size);

    /*! \brief helper function that do the actual close
     */
    void close_down_(const bool force);

    void goto_ready_state_(void);
    void goto_closed_state_(void);


    /*! \brief Generates a new tunnel id
     */
    unsigned long get_new_tunnel_id();
    bool room_for_new_tunnel_id();

    void tunnelendpoint_recieve_connect_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc);
    void tunnelendpoint_recieve_eof(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_close(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_closed(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message);
    void tunnelendpoint_recieve_checkpoint_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc);

    void check_for_connection_request(void);

    void clean_up_tunnelendpoint(const unsigned long tunnel_id);

    TunnelendpointDirectTunnelClientEventhandler* direct_tunnel_eventhandler_;


    enum StateDirect {
        StateDirect_Initializing,
        StateDirect_InitCheckpointTunnel,
        StateDirect_Ready,
        StateDirect_Closing,
        StateDirect_Closing_no_connection,
        StateDirect_Closed,
        StateDirect_Unknown
    };
    StateDirect state_direct_;


    bool close_state_all_connection_closed_;
    unsigned long tunnel_id_next_;
    unsigned long connection_pool_size_;
    unsigned long waiting_for_connection_;


    std::vector<unsigned long> tunnel_ids_free_;
    std::set<unsigned long> tunnel_ids_ready_;
    std::set<unsigned long> tunnel_ids_bussy_;

    std::set<unsigned long> tunnel_ids_tunnelendpoint_recived_closed_;
    std::set<unsigned long> tunnel_ids_tunnelendpoint_direct_eh_close_;

    std::map<unsigned long, TunnelendpointDirect::APtr> direct_tunnelendpoints_;


    bool checkpoint_tunnel_enabled_;
    TunnelendpointCheckpointHandlerClient::APtr checkpoint_tunnel_;
    unsigned long checkpoint_tunnel_id_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_tunnel_checkpoint_handler_;
};

}

}
#endif
