/*! \file COM_RawTunnelendpoint.hxx
 * \brief This file contains the RawTunnelendpoint abstraction
 */
#ifndef COM_RawTunnelendpoint_HXX
#define COM_RawTunnelendpoint_HXX

#include <vector>
#include <queue>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/asio/error.hpp>
#include <boost/date_time.hpp>

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
#include <boost/asio/ssl.hpp>
#endif

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>
#include <component/communication/COM_TrafficControlSession.hxx>

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
#include <component/communication/COM_RawTunnelendpointSSLContext.hxx>
#endif

namespace Giritech {
namespace Communication {



class DataBufferWithTime : public boost::noncopyable {
public:
	typedef boost::shared_ptr<DataBufferWithTime> APtr;

	virtual ~DataBufferWithTime(void);

	boost::posix_time::time_duration getDurationSinceCreation(void) const;
	Utility::DataBuffer::APtr getDataBuffer(void) const;

	static APtr create(const Utility::DataBuffer::APtr& dataBuffer);

private:
	DataBufferWithTime(const Utility::DataBuffer::APtr& dataBuffer);

	Utility::DataBuffer::APtr dataBuffer_;
	boost::posix_time::ptime create_ts_;
};


/*! \brief This class define a abstrac RawTunnelendpoint
 *
 * A RawTunnelendpoint is abstraction of socket functionality e.g. TCP socket or UDP socket.
 */
class RawTunnelendpoint: public Giritech::Utility::AsyncMemGuardElement {
public:
	typedef boost::shared_ptr<RawTunnelendpoint> APtr;

	/*! \brief Destructor
	 */
	virtual ~RawTunnelendpoint(void);

	/*! \brief  Request a asynchron read
	 *
	 *  When the asynchron read returns then the eventhandler is signaled using the signal RawTunnelendpointEventhandler::com_raw_tunnelendpoint_read().
	 *  If no eventhandler is registred, then no signal is send.
	 */
	virtual void aio_read_start(void) = 0;

	/*! \brief Request a asynchron write of a buffer
	 */
	virtual void aio_write_start(const Utility::DataBuffer::APtr&) = 0;

	/*! \brief Request to close the tunnel
	 *
	 *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
	 *
	 *  When the asyncron close returns then the eventhandler is signaled using the signal RawTunnelendpointEventhandler::com_raw_tunnelendpoint_close().
	 *  If no eventhandler is registred, then no signal is send.
	 */
	virtual void aio_close_start(const bool force) = 0;

	/*! \brief Request to half-close the tunnel, by sending eof
	 *
	 *  After this, it not possible to send data, but read is stil working, until aio_close_start is called.
	 */
	virtual void aio_close_write_start(void) = 0;

	/*! \brief Report back if the tunnelendpoint has been closed
	 */
	virtual bool is_closed(void) = 0;

	/*! \brief Report back if the tunnelendpoint is ready
	 */
	virtual bool is_ready(void) = 0;

	/*! \brief Set the event handler
	 */
	void set_eventhandler(RawTunnelendpointEventhandler*);

	/*! \brief Set instance mutex
	 */
	Utility::Mutex::APtr get_mutex(void);

	/*! \brief Set instance mutex
	 */
	void set_mutex(const Utility::Mutex::APtr& mutex);

	/*! \brief Reset event handler
	 */
	void reset_eventhandler(void);

	/*! \brief Get the connection id
	 */
	unsigned long get_connection_id(void) const;

	/*! \brief Get io-service used
	 */
	boost::asio::io_service& get_io_service(void);

	/*! \brief Mark as a gon connection
	 */
	void mark_as_gconnection(const std::string& session_id);

	/*! \brief Implementation of AsyncMemGuardElement virtual methods
	 */
	virtual bool async_mem_guard_is_done(void) = 0;

protected:

	/*! \brief Constructor
	 *
	 * \param connection_id  Connection identifier used when signaling the RawTunnelendpointEventhandler
	 * \param io             The asyncon servicehandler to use
	 */
	RawTunnelendpoint(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			boost::asio::io_service& io,
			const unsigned long& connection_id);

	Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
	boost::asio::io_service& io_;
	unsigned long connection_id_;
	RawTunnelendpointEventhandler* eventhandler_;

	Utility::Mutex::APtr mutex_;
	TrafficControlSession::APtr traffic_control_session_;
	unsigned long traffic_control_session_id_;
};




/*! \brief This class define a RawTunnelendpoint representing a TCP socket.
 */
class RawTunnelendpointTCP: public RawTunnelendpoint {
public:
	typedef boost::shared_ptr<RawTunnelendpointTCP> APtr;

	/*! \brief Destructor
	 */
	~RawTunnelendpointTCP(void);

	/*! \brief  Request a asynchron read
	 */
	void aio_read_start(void);

	/*! \brief Request a asynchron write of a buffer
	 *
	 * Returns size of buffer holding chunks not yet send
	 */
	void aio_write_start(const Utility::DataBuffer::APtr&);

	/*! \brief Request to close the tunnel
	 */
	void aio_close_start(const bool force);

	/*! \brief Request to half-close the tunnel, by sending eof
	 *
	 *  After this, it not possible to send data, but read is stil working, until aio_close_start is called.
	 */
	void aio_close_write_start(void);

	/*! \brief Report back if the tunnelendpoint has been closed
	 */
	bool is_closed(void);

	/*! \brief Report back if the tunnelendpoint is ready
	 */
	bool is_ready(void);

	/*! \brief Add ip attributes to checkpoint
	 */
	void log_socket_info(Giritech::Utility::CheckpointScope& cps) const;

	/*! \brief Add ip attributes to checkpoint
	 */
	void set_option_nodelay(void);

	/*! \brief Geters of ip/port of local and remote part of socket
	 */
	std::pair<std::string, unsigned long> get_ip_local(void) const;
	std::pair<std::string, unsigned long> get_ip_remote(void) const;

	/*! \brief Create instance
	 *
	 * \param checkpoint_handler Checkpoint handler to use.
	 * \param connection_id  Connection identifier used when signaling the RawTunnelendpointEventhandler
	 * \param io             The asyncon servicehandler to use
	 */
	static APtr create(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			boost::asio::io_service& io,
			const unsigned long& connection_id);

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
    static APtr create_ssl(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		const unsigned long& connection_id,
    		const std::string& ssl_host_name,
			const bool& disable_certificate_verification);
#endif


    void set_ssl_info(const std::string& ssl_host_name, const bool& disable_certificate_verification);
    void aio_ssl_start(const std::string& ssl_host_name, const bool& disable_certificate_verification);


	/*! \brief Get associated asio socket
	 */
	virtual boost::asio::ip::tcp::socket& get_socket(void);
	virtual const boost::asio::ip::tcp::socket& get_socket(void) const;

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& get_socket_ssl(void);
	const boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& get_socket_ssl(void) const;
#endif

	/*! \brief Implementation of AsyncMemGuardElement virtual methods
	 */
	virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

	/*! \brief asio write callback
	 */
	void aio_write_from_buffer(const boost::system::error_code& error, const DataBufferWithTime::APtr data);

protected:
	/*! \brief Constructor
	 *
	 * \see create
	 */
	RawTunnelendpointTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			boost::asio::io_service& io,
			const unsigned long& connection_id);

	/*! \brief asio read callback
	 */
	void aio_read_start_action(void);
	void aio_read(const boost::system::error_code& error, const size_t bytes_transferred);

	/*! \brief Request a asynchron write of a buffer
	 */
	void aio_write_from_buffer_start(void);

	/*! \brief asio write callback
	 */
	void aio_write_from_buffer_start_action(void);

	void aio_close_write_start_pending_(void);
	void aio_close_start_pending_(void);

	void event_aio_com_raw_tunnelendpoint_close_start_(void);
	void event_aio_com_raw_tunnelendpoint_close_(void);

	void event_aio_com_raw_tunnelendpoint_close_eof_start_(void);
	void event_aio_com_raw_tunnelendpoint_close_eof_(void);


    void aio_tls_handle_handshake(const boost::system::error_code& error);


    Utility::DataBufferManaged::APtr get_read_buffer_(void);


	/*! \brief Request to close the tunnel
	 */
	void aio_close(void);

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	RawTunnelendpointTCPSSLContext::APtr ssl_context_;
	boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket_;
#else
	boost::asio::ip::tcp::socket socket_;
#endif

	long read_count_pending_;
	long write_count_pending_;
	long event_count_pending_;
	long write_queue_total_size_;

	unsigned long read_last_bytes_transferred_;

	enum State {
		State_Ready = 0,
		State_Closing,
		State_Closed,
		State_Done,
		State_Unknown
	};
	State state_;

	enum SSLState {
		SSLState_Off = 0,
		SSLState_Connecting,
		SSLState_On
	};
	SSLState ssl_state_;
	std::string ssl_host_name_;
	bool ssl_disable_certificate_verification_;
	bool ssl_do_read_start_when_connected_;

	bool write_is_running_;
	bool closing_for_write_;
	bool closed_for_write_;
	bool closed_for_read_;

	std::queue<DataBufferWithTime::APtr> write_queue_;

        boost::asio::steady_timer read_start_delay_timer_;
        boost::asio::steady_timer write_start_delay_timer_;

	Utility::DataBufferManaged::APtr raw_buffer_;

	/*
	 *
	 */
	 virtual void socket_operation_async_read(void);
	virtual void socket_operation_async_write(const DataBufferWithTime::APtr& data);
	virtual void socket_operation_shutdown_send(void);
	virtual void socket_operation_shutdown_both(void);
	virtual void socket_operation_shutdown_receive(void);
	virtual void socket_operation_close(void);
	virtual void socket_operation_log_info(Giritech::Utility::CheckpointScope& cps) const;
	virtual void socket_operation_set_option_nodelay(void);
	virtual bool socket_operation_is_eof(const boost::system::error_code& error_code);
};

}}
#endif
