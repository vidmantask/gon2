/*! \file COM_API_TunnelendpointCheckpointHandler.cxx
 *  \brief This file contains the implementation of the API wrapper for checkpoint_handler tunnelendpoints
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <component/communication/COM_API_TunnelendpointCheckpointHandler.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 * ------------------------------------------------------------------
 * APITunnelendpointCheckpointHandlerServer implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointCheckpointHandlerServer::APITunnelendpointCheckpointHandlerServer(
		const APIAsyncService::APtr& async_service,
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const boost::filesystem::path& folder) {
    impl_tunnelendpoint_ = TunnelendpointCheckpointHandlerServer::create(checkpoint_handler, async_service->get_io_service(), folder);
}

APITunnelendpointCheckpointHandlerServer::~APITunnelendpointCheckpointHandlerServer(void) {
}

APITunnelendpointCheckpointHandlerServer::APtr APITunnelendpointCheckpointHandlerServer::create(
		const APIAsyncService::APtr& async_service,
		const Utility::APICheckpointHandler::APtr& checkpoint_handler,
		const std::string& folder) {
    return APtr(new APITunnelendpointCheckpointHandlerServer(async_service, checkpoint_handler->get_checkpoint_handler(), folder));
}

TunnelendpointCheckpointHandlerServer::APtr APITunnelendpointCheckpointHandlerServer::get_impl(void) {
	return impl_tunnelendpoint_;
}
