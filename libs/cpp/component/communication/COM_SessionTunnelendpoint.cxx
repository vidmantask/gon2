/*! \file COM_SessionTunnelendpoint.cxx
 \brief This file contains the impplementation for the COM_SessionTunnelendpoint's classes
 */

#include <sstream>
#include <iostream>
#include <fstream>

#include <boost/asio.hpp>

#include <boost/config.hpp>
#include <boost/bind.hpp>
#include <boost/regex.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_SessionTunnelendpoint.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * SessionTunnelendpoint implementation
 * ------------------------------------------------------------------
 */
SessionTunnelendpoint::SessionTunnelendpoint(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                             SessionTunnelendpointEventhandler* eventhandler) :
  checkpoint_handler_(checkpoint_handler), eventhandler_(eventhandler) {
}

SessionTunnelendpoint::~SessionTunnelendpoint(void) {
}

void SessionTunnelendpoint::reset_eventhandler(void) {
	eventhandler_ = NULL;
}




/*
 * ------------------------------------------------------------------
 * SessionTunnelendpointSessionCrypter implementation
 * ------------------------------------------------------------------
 */
SessionTunnelendpointReliableSessionCrypter::SessionTunnelendpointReliableSessionCrypter(boost::asio::io_service& asio_io_service,
																						 const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                                         SessionTunnelendpointEventhandler* eventhandler,
                                                                                         const SessionCrypter::APtr& session_crypter) :
    SessionTunnelendpoint(checkpoint_handler, eventhandler), state_(State_Ready),
    session_crypter_(session_crypter),
    session_crypter_stream_to_message_(StreamToMessage::create()),
    read_message_started_(false) {
    session_crypter_->set_eventhandler_ready(this);
}

SessionTunnelendpointReliableSessionCrypter::~SessionTunnelendpointReliableSessionCrypter(void) {
    state_ = State_Unknown;
}

bool SessionTunnelendpointReliableSessionCrypter::async_mem_guard_is_done(void) {
//    Checkpoint(*checkpoint_handler_, "SessionTunnelendpointReliableSessionCrypter::async_mem_guard_is_done", Attr_Communication(), CpAttr_debug(), CpAttr("is_closed", is_closed()), CpAttr("state_", state_));
	return is_closed();
}

std::string SessionTunnelendpointReliableSessionCrypter::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

SessionTunnelendpointReliableSessionCrypter::APtr SessionTunnelendpointReliableSessionCrypter::create(boost::asio::io_service& asio_io_service,
																									  const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                                                      SessionTunnelendpointEventhandler* eventhandler,
                                                                                                      const SessionCrypter::APtr& session_crypter) {
    APtr result(new SessionTunnelendpointReliableSessionCrypter(asio_io_service, checkpoint_handler, eventhandler, session_crypter));
	AsyncMemGuard::global_add_element(result);
	return result;
}

void SessionTunnelendpointReliableSessionCrypter::close(const bool force) {
    switch (state_) {
    case State_Ready:
    	break;
    default:
    	return;
    }
	state_ = State_Closing;
	if(session_crypter_->is_closed()) {
		state_ = State_Closed;
		reset_eventhandler();
		session_crypter_->reset_eventhandler_ready();
	}
}

bool SessionTunnelendpointReliableSessionCrypter::is_closed(void) {
    return session_crypter_->is_closed() && state_ == State_Closed;
}

void SessionTunnelendpointReliableSessionCrypter::session_crypter_closed(void) {
    state_ = State_Closed;
	reset_eventhandler();
	session_crypter_->reset_eventhandler_ready();
}

void SessionTunnelendpointReliableSessionCrypter::session_crypter_recieve(const DataBuffer::APtr& data) {
    assert(eventhandler_ != NULL);
    session_crypter_stream_to_message_->push_data(session_crypter_->decrypt_stream(data));
    MessageCom::APtr message(session_crypter_stream_to_message_->pop_message_com());
    while (message.get() != NULL) {
        try {
            read_message_started_ = false;
            eventhandler_->session_tunnelendpoint_recieve(message);
        }
        catch(ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_,
                    "SessionTunnelendpointReliableSessionCrypter::session_crypter_recieve.callback",
                    Attr_Communication(),
                    CpAttr_error(),
                    CpAttr_message(e.what()));
        }

        /* The above call can terminate the session-tunnelendpoint if it was the last expected message,
         * and thus cause a session close, thus a session erasion, thus a deleation of this object
         */
        if (state_ != State_Ready) {
            break;
        }
        message = session_crypter_stream_to_message_->pop_message_com();
    }
    if (state_ == State_Ready) {
        if (!session_crypter_stream_to_message_->empty()) {
            session_crypter_->read_start_state_ready();
        } else {
            read_start_ready();
        }
    }
}

void SessionTunnelendpointReliableSessionCrypter::read_start_ready(void) {
    if(!read_message_started_) {
        if(eventhandler_ != NULL) {
            try {
                if (eventhandler_->session_tunnelendpoint_read_continue_state_ready() ) {
                	session_crypter_->read_start_state_ready();
                }
            }
            catch(ExceptionCallback& e) {
                Checkpoint cp(*checkpoint_handler_,
                		"SessionTunnelendpointReliableSessionCrypter::read_start_state_ready.callback",
                        Attr_Communication(),
                        CpAttr_error(),
                        CpAttr_message(e.what()));
            }
        }
    }
    read_message_started_ = true;
}

void SessionTunnelendpointReliableSessionCrypter::session_tunnelendpoint_send(const MessageCom::APtr& message) {
  DataBufferManaged::APtr data_unencrypted(message->create_as_buffer());

  /*
   * Split up message in chunks of size max int, because encryptions only handles blocks of this sizes
   */
  unsigned int buffer_size(20000);
  while(data_unencrypted->getSize() > 0) {
    unsigned int buffer_size_used(buffer_size++);
    if(data_unencrypted->getSize() < buffer_size_used) {
      buffer_size_used = data_unencrypted->getSize();
    }
    DataBufferManaged::APtr data_unencrypted_chunck(DataBufferManaged::create(data_unencrypted->get_raw_buffer().begin(), (const unsigned long)buffer_size_used));
    session_crypter_->send_state_ready(session_crypter_->encrypt_stream(data_unencrypted_chunck));
    data_unencrypted->eatFront(buffer_size_used);
  }
}

