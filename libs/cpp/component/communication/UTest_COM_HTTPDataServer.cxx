/*! \file UTest_COM_HTTPDataServer.cxx
 \brief This file contains unittest suite for http data server
 */
#include <vector>
#include <string>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>

#include <lib/utility/UY_AsyncMemGuard.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_HTTPDataServer.hxx>


using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


BOOST_AUTO_TEST_CASE( http_data_server_serve )
{
    try {
        CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
        AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));

        COMHTTPDataServer::APtr httpDataServer(COMHTTPDataServer::create(async_service, checkpoint_handler, "hej", "text/plain", "test.txt", boost::posix_time::seconds(30), "", 0));
    	httpDataServer->start();

    	cout << "URL: "<< httpDataServer->get_url() << endl;

    	while(!httpDataServer->async_mem_guard_is_done()) {
        	cout << "Just waiting" << endl;
    		boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(1));
    	}

    }
    catch (const std::exception& e) {
        cout << e.what() << "\n";
        BOOST_FAIL(e.what());
    }
}







boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
