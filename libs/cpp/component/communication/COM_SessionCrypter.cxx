/*! \file COM_SessionCrypter.cxx
 \brief This file contains the impplementation for the COM_SessionCrypter classes
 */
#include <boost/asio.hpp>

#include <sstream>
#include <iostream>
#include <fstream>

#include <boost/config.hpp>
#include <boost/bind.hpp>
#include <boost/regex.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_Exception.hxx>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * Help classs
 * ------------------------------------------------------------------
 */

class DummyClientIdentification : public CryptFacility::ClientIdentification {
public:
    typedef boost::shared_ptr<DummyClientIdentification> APtr;

    void init(const Utility::DataBuffer::APtr& dataBuffer) {
    }

    static APtr create(void) {
        return APtr(new DummyClientIdentification);
    }

    Utility::DataBuffer::APtr getDataBuffer(void) const {
        /* Due to a bug in DataBufferManaged::parse (part of fips validated code) atleast one byte should be transmitted.
         * the bug has been fixed
         */
        return Utility::DataBufferManaged::create(0);
    }
};

/*
 * ------------------------------------------------------------------
 * SessionCrypter implementation
 * ------------------------------------------------------------------
 */
SessionCrypter::SessionCrypter(const CheckpointHandler::APtr& checkpoint_handler, const RawTunnelendpointTCP::APtr& tunnelendpoint) :
    checkpoint_handler_(checkpoint_handler), state_(State_Initalizing),
            tunnelendpoint_(tunnelendpoint), eventhandler_(NULL), eventhandler_ready_(NULL),
            stream_to_message_key_exchange_(StreamToMessage::create()),
            mutex_(tunnelendpoint_->get_mutex()), read_message_started_(false) {
    tunnelendpoint_->set_eventhandler(this);
}

SessionCrypter::~SessionCrypter(void) {
    state_ = State_Unknown;
}

void SessionCrypter::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::close");
    state_ = State_Closing;
    if(! tunnelendpoint_->is_closed()) {
    	tunnelendpoint_->aio_close_start(force);
    }
    else {
    	close_final_();
    }
}
bool SessionCrypter::is_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::is_closed");
    return tunnelendpoint_->is_closed() && state_ == State_Closed;
}

bool SessionCrypter::is_ready(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::is_ready");
    return state_ == State_Ready;
}

void SessionCrypter::set_eventhandler(SessionCrypterEventhandler* eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::set_eventhandler");
    eventhandler_ = eventhandler;
}
void SessionCrypter::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::reset_eventhandler");
    eventhandler_ = NULL;
}

void SessionCrypter::set_eventhandler_ready(SessionCrypterEventhandlerReady* eventhandler_ready) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::set_eventhandler_ready");
    eventhandler_ready_ = eventhandler_ready;
}
void SessionCrypter::reset_eventhandler_ready(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::reset_eventhandler_ready");
    eventhandler_ready_ = NULL;
}
Utility::Mutex::APtr SessionCrypter::get_mutex(void) {
    return tunnelendpoint_->get_mutex();
}

RawTunnelendpointTCP::APtr SessionCrypter::get_raw_tunnelendpoint(void) {
	return tunnelendpoint_;
}

void SessionCrypter::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::set_mutex");
    tunnelendpoint_->set_mutex(mutex);
}

void SessionCrypter::com_raw_tunnelendpoint_read(const unsigned long& connection_id, const DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::com_raw_tunnelendpoint_read");
    switch (state_) {
    case State_KeyExchange:
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Recived package but is not ready for it");
    }

    if (state_ == State_KeyExchange) {
    	try {
			stream_to_message_key_exchange_->push_data(data);
			MessageRaw::APtr message(stream_to_message_key_exchange_->pop_message_raw());
			while (message.get() != NULL) {
				handle_message_state_key_exchange(message->get_payload()->get_buffer());

				// Handling the message could change the state to ready
				if (state_ == State_KeyExchange) {
					message = stream_to_message_key_exchange_->pop_message_raw();
				}
				else {
					message = MessageRaw::APtr();
				}
			}

			if(!stream_to_message_key_exchange_->empty()) {
				if(state_ == State_KeyExchange) {
					tunnelendpoint_->aio_read_start();
				}
				else if (state_ == State_Ready) {
					handle_message_state_ready(stream_to_message_key_exchange_->get_remaining());
				}
			}
    	}
		catch (const MessageRaw::ExceptionInvalidMessage& e) {
	        if(eventhandler_ready_ != NULL) {
	        	eventhandler_->session_crypter_key_exchange_failed(e.what());
	        }
		}
    } else if (state_ == State_Ready) {
        handle_message_state_ready(data);
    }
}

void SessionCrypter::com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
    CheckpointScope cps(*checkpoint_handler_, "SessionCrypter::com_raw_tunnelendpoint_close", Attr_Communication(), CpAttr_debug());
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::com_raw_tunnelendpoint_close");
    switch (state_) {
    case State_KeyExchange:
    case State_Ready:
    case State_Closing:
        break;
    default:
    	return;
    }
    close_final_();
}

void SessionCrypter::close_final_(void) {
    CheckpointScope cps(*checkpoint_handler_, "SessionCrypter::close_final_", Attr_Communication(), CpAttr_debug());
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::close_final_");
    switch (state_) {
    case State_KeyExchange:
    case State_Ready:
    case State_Closing:
        break;
    default:
    	return;
    }
    state_ = State_Closed;

    if(eventhandler_ready_ != NULL) {
        SessionCrypterEventhandlerReady* eventhandler_ready_temp(eventhandler_ready_);
        reset_eventhandler_ready();
        eventhandler_ready_temp->session_crypter_closed();
    }
    if(eventhandler_ != NULL) {
        SessionCrypterEventhandler* eventhandler_temp(eventhandler_);
        reset_eventhandler();
        eventhandler_temp->session_crypter_closed();
    }
    tunnelendpoint_->reset_eventhandler();
}

void SessionCrypter::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
}

void SessionCrypter::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::com_raw_tunnelendpoint_close_eof");
    tunnelendpoint_->aio_close_start(false);
}

void SessionCrypter::handle_message_state_ready(const Utility::DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::handle_message_state_ready");
    read_message_started_ = false;
    assert(eventhandler_ready_ != NULL);
    eventhandler_ready_->session_crypter_recieve(data);
}

void SessionCrypter::send_state_ready(const Utility::DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::send_state_ready");
    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Trying to send message but session crypter is not ready");
    }
    tunnelendpoint_->aio_write_start(data);
}

void SessionCrypter::send_state_key_exchange(const DataBufferManaged::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::send_state_key_exchange");
    switch (state_) {
    case State_KeyExchange:
        break;
    default:
        throw ExceptionUnexpected("Trying to send message but session crypter is not ready");
    }
    MessageRaw::APtr message(MessageRaw::create(MessageRaw::PackageType_KeyExchange, MessagePayload::create(data)));
    tunnelendpoint_->aio_write_start(message->create_as_buffer());
}

void SessionCrypter::read_start_state_ready(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::read_start_state_ready");
    switch (state_) {
    case State_Ready:
    case State_Closing:
        break;
    default:
        ExceptionUnexpected("Trying to start read but session crypter is not ready");
    }
    if(!read_message_started_) {
        tunnelendpoint_->aio_read_start();
    }
    read_message_started_ = true;
}

DataBuffer::APtr SessionCrypter::encrypt_block(const Utility::DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::encrypt_block");
    CheckpointScope cps(*checkpoint_handler_,
                        "SessionCrypter::encrypt_block",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("state", state_),
                        CpAttr("data_size", data->getSize()) );
    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Trying to encrypt block but session crypter is not ready");
    }

    if(data->getSize() > 65535) {
        throw ExceptionUnexpected("Trying to encrypt to big a block of data");
    }

    DataBufferManaged::APtr buffer(DataBufferManaged::create(data));
    Crypt::APtr encrypter(crypter_block_->getEncrypter());
    buffer->pkcsAdd(encrypter->getBlockSize());
    long bufferSize(buffer->getSize());
    long bufferSizeEncrypted(encrypter->getCipherBufferSize(bufferSize));
    DataBufferManaged::APtr bufferEncrypted(DataBufferManaged::create(bufferSizeEncrypted));
    encrypter->encrypt(buffer->data(), bufferSize, bufferEncrypted->data());
    return bufferEncrypted;

}

DataBuffer::APtr SessionCrypter::decrypt_block(const DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::decrypt_block");
    CheckpointScope cps(*checkpoint_handler_,
                        "SessionCrypter::decrypt_block",
                        Attr_Communication(),
                        CpAttr_debug(),
                        CpAttr("state", state_),
                        CpAttr("data_size", data->getSize()) );

    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Trying to decrypt block but session crypter is not ready");
    }
    Crypt::APtr decrypter(crypter_block_->getDecrypter());
    long bufferSize(data->getSize());
    long bufferSizeDecrypted(decrypter->getPlainBufferSizeAfter(bufferSize));
    DataBufferManaged::APtr bufferDecrypted(DataBufferManaged::create(bufferSizeDecrypted));
    decrypter->decrypt(data->data(), bufferSize, bufferDecrypted->data());
    bufferDecrypted->pkcsRemove(decrypter->getBlockSize());
    return bufferDecrypted;
}

DataBuffer::APtr SessionCrypter::encrypt_stream(const Utility::DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::encrypt_stream");
//    CheckpointScope cps(*checkpoint_handler_,
//                        "SessionCrypter::encrypt_stream",
//                        Attr_Communication(),
//                        CpAttr_debug(),
//                        CpAttr("state", state_),
//                        CpAttr("data_size", data->getSize()) );
    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Trying to encrypt stream but session crypter is not ready");
    }
    Crypt::APtr encrypter(crypter_stream_->getEncrypter());

    if(data->getSize() > 65535) {
        throw ExceptionUnexpected("Trying to encrypt to big a block of data");
    }

	encrypter->encrypt(data->data(), data->getSize(), data->data());
    return data;
}

DataBuffer::APtr SessionCrypter::decrypt_stream(const DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::decrypt_stream");
//    CheckpointScope cps(*checkpoint_handler_,
//                        "SessionCrypter::decrypt_stream",
//                        Attr_Communication(),
//                        CpAttr_debug(),
//                        CpAttr("state", state_),
//                        CpAttr("data_size", data->getSize()) );

    switch (state_) {
    case State_Ready:
        break;
    default:
        throw ExceptionUnexpected("Trying to decrypt stream but session crypter is not ready");
    }
    Crypt::APtr decrypter(crypter_stream_->getDecrypter());
    decrypter->decrypt(data->data(), data->getSize(), data->data());
    return data;
}

std::pair<std::string, unsigned long> SessionCrypter::get_ip_local(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::get_ip_local");
	return tunnelendpoint_->get_ip_local();
}
std::pair<std::string, unsigned long> SessionCrypter::get_ip_remote(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::get_ip_remote");
	return tunnelendpoint_->get_ip_remote();
}


/*
 * ------------------------------------------------------------------
 * SessionCrypterServer implementation
 * ------------------------------------------------------------------
 */
SessionCrypterServer::SessionCrypterServer(const CheckpointHandler::APtr& checkpoint_handler,
                                           const RawTunnelendpointTCP::APtr& tunnelendpoint,
                                           const Utility::DataBufferManaged::APtr& known_secret) :
    SessionCrypter(checkpoint_handler, tunnelendpoint), server_known_secret_(known_secret), ke_state_(KEState_Unknown) {
}
SessionCrypterServer::~SessionCrypterServer(void) {
}

SessionCrypterServer::APtr SessionCrypterServer::create(const CheckpointHandler::APtr& checkpoint_handler,
                                                        const RawTunnelendpointTCP::APtr& tunnelendpoint,
                                                        const Utility::DataBufferManaged::APtr& known_secret) {
    return APtr(new SessionCrypterServer(checkpoint_handler, tunnelendpoint, known_secret));
}

void SessionCrypterServer::do_key_exchange(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::do_key_exchange");
    switch (state_) {
    case State_Ready:
    case State_Initalizing:
        break;
    default:
        throw ExceptionUnexpected("State not ready for do_key_exchange");
    }
    state_ = State_KeyExchange;
    ke_state_ = KEState_Init;
    try {
        factory_ = CryptFacilityService::getInstance().getCryptFactory();
        ske_ = KeyExchange_SessionKeyExchange_Server::create(tunnelendpoint_->get_io_service(), factory_, server_known_secret_);
        send_message_SPK();
    } catch (const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterServer::handle_message_state_key_exchange(const DataBuffer::APtr& message) {
    CheckpointScope cps(*checkpoint_handler_, "SessionCrypterServer::handle_message_state_key_exchange", Attr_Communication(), CpAttr_debug(), CpAttr("ke_state", ke_state_));
    read_message_started_ = false;
    switch (ke_state_) {
    case KEState_Init: {
        ke_state_ = KEState_Error;
        break;
    }
    case KEState_wait_for_CIF: {
        handle_message_CIF(message);
        break;
    }
    case KEState_wait_for_CR: {
        handle_message_CR(message);
        break;
    }
    case KEState_Error: {
        break;
    }
    }
}

void SessionCrypterServer::send_message_SPK(void) {
    try {
        DataBufferManaged::APtr response(DataBufferManaged::create(ske_->generateMessage()));
        ke_state_ = KEState_wait_for_CIF;
        read_start_state_ready();
        send_state_key_exchange(response);
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterServer::handle_message_CIF(const DataBuffer::APtr& message) {
    try {
        crypter_block_ = Crypter::create(factory_);
        cif_ = KeyExchange_CIFExchange_Server::createFromMessage(ske_, DummyClientIdentification::create(), crypter_block_, message);
        send_message_SKP();
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterServer::send_message_SKP(void) {
    try {
        ce_ = KeyExchange_CryptExchange_Server::create(*cif_);
        DataBufferManaged::APtr response(ce_->generateMessage(*ske_, *crypter_block_, *factory_));
        crypter_stream_ = ce_->create_crypter_stream(crypter_block_, factory_);
        ke_state_ = KEState_wait_for_CR;
        read_start_state_ready();
        send_state_key_exchange(response);
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterServer::handle_message_CR(const DataBuffer::APtr& message) {
    try {
        KeyExchange_CryptStreamValidator::validateMessage(*crypter_block_, *(ce_->getReceipt()), *message);
        send_message_SR();
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterServer::send_message_SR(void) {
    try {
        DataBufferManaged::APtr response(DataBufferManaged::create(KeyExchange_CryptStreamValidator::generateMessage(*crypter_block_, *(ce_->getReceipt()))));
        send_state_key_exchange(response);
        ke_state_ = KEState_Ready;
        state_ = State_Ready;

        assert(eventhandler_ != NULL);
        eventhandler_->session_crypter_key_exchange_complete();
        Checkpoint cp(*checkpoint_handler_, "SessionCrypterServer::do_key_exchange_complete", Attr_Communication(), CpAttr_debug());
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

bool SessionCrypterServer::doing_keyexchange_phase_one(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::doing_keyexchange_phase_one");
    switch (ke_state_) {
    case KEState_Init:
    case KEState_wait_for_CIF:
        return true;
    }
	return false;
}


/*
 * ------------------------------------------------------------------
 * SessionCrypterClient implementation
 * ------------------------------------------------------------------
 */
SessionCrypterClient::SessionCrypterClient(const CheckpointHandler::APtr& checkpoint_handler,
                                           const RawTunnelendpointTCP::APtr& tunnelendpoint,
                                           const Utility::DataBufferManaged::APtr& known_secret,
                                           const KeyStore::APtr& key_store) :
    SessionCrypter(checkpoint_handler, tunnelendpoint), client_known_secret_(known_secret),
            ke_state_(KEState_Unknown), key_store_(key_store) {
}

SessionCrypterClient::~SessionCrypterClient(void) {
}

SessionCrypterClient::APtr SessionCrypterClient::create(const CheckpointHandler::APtr& checkpoint_handler,
                                                        const RawTunnelendpointTCP::APtr& tunnelendpoint,
                                                        const Utility::DataBufferManaged::APtr& known_secret) {
    return APtr(new SessionCrypterClient(checkpoint_handler, tunnelendpoint, known_secret, KeyStore::create(tunnelendpoint->get_io_service(), CryptFacilityService::getInstance().getCryptFactory())));
}

SessionCrypterClient::APtr SessionCrypterClient::create(const CheckpointHandler::APtr& checkpoint_handler,
                                                        const RawTunnelendpointTCP::APtr& tunnelendpoint,
                                                        const Utility::DataBufferManaged::APtr& known_secret,
                                                        const KeyStore::APtr& key_store) {
    return APtr(new SessionCrypterClient(checkpoint_handler, tunnelendpoint, known_secret, key_store));
}


void SessionCrypterClient::do_key_exchange(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::do_key_exchange");
    CheckpointScope cps(*checkpoint_handler_, "SessionCrypterClient::do_key_exchange", Attr_Communication(), CpAttr_debug());
    switch (state_) {
    case State_Ready:
    case State_Initalizing:
        break;
    default:
        throw ExceptionUnexpected("State not ready for do_key_exchange");
    }

    state_ = State_KeyExchange;
    ke_state_ = KEState_Init;
    try {
        factory_ = CryptFacilityService::getInstance().getCryptFactory();
        ke_state_ = KEState_wait_for_SPK;
        read_start_state_ready();
    } catch (const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterClient::handle_message_state_key_exchange(const DataBuffer::APtr& message) {
    CheckpointScope cps(*checkpoint_handler_, "SessionCrypterClient::handle_message_state_key_exchange", Attr_Communication(), CpAttr_debug(), CpAttr("ke_state", ke_state_) );
    read_message_started_ = false;
    switch (ke_state_) {
    case KEState_Init: {
        ke_state_ = KEState_Error;
        break;
    }
    case KEState_wait_for_SPK: {
        handle_message_SPK(message);
        break;
    }
    case KEState_wait_for_SKP: {
        handle_message_SKP(message);
        break;
    }
    case KEState_wait_for_SR: {
        handle_message_SR(message);
        break;
    }
    }
}

void SessionCrypterClient::handle_message_SPK(const DataBuffer::APtr& message) {
    try {
    	assert(message.get() != NULL);
    	assert(factory_.get() != NULL);
    	assert(client_known_secret_.get() != NULL);
        ske_ = KeyExchange_SessionKeyExchange_Client::createFromMessage(factory_, client_known_secret_, message, key_store_);
        send_message_CIF();
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterClient::send_message_CIF(void) {
    try {
        crypter_block_ = Crypter::create(factory_);
        cif_ = KeyExchange_CIFExchange_Client::create(*factory_);

        DataBufferManaged::APtr response(cif_->generateMessage(ske_, crypter_block_, DummyClientIdentification::create()));
        ke_state_ = KEState_wait_for_SKP;
        read_start_state_ready();
        send_state_key_exchange(response);
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterClient::handle_message_SKP(const DataBuffer::APtr& message) {
    try {
        ce_ = KeyExchange_CryptExchange_Client::createFromMessage(*ske_, *crypter_block_, *factory_, *message);
        crypter_stream_ = ce_->create_crypter_stream(crypter_block_, factory_);
        send_message_CR();
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterClient::send_message_CR(void) {
    try {
        DataBufferManaged::APtr response(KeyExchange_CryptStreamValidator::generateMessage(*crypter_block_, *(ce_->getReceipt())));
        read_start_state_ready();
        send_state_key_exchange(response);
        ke_state_ = KEState_wait_for_SR;
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

void SessionCrypterClient::handle_message_SR(const DataBuffer::APtr& message) {
    try {
        KeyExchange_CryptStreamValidator::validateMessage(*crypter_block_, *(ce_->getReceipt()), *(message));
        ke_state_ = KEState_Ready;
        state_ = State_Ready;

        assert(eventhandler_ != NULL);
        key_store_->resetSessionKeypair();
        eventhandler_->session_crypter_key_exchange_complete();
        Checkpoint cp(*checkpoint_handler_, "SessionCrypterClient::do_key_exchange_complete", Attr_Communication(), CpAttr_debug());
    }
    catch(const Exception_CF& e) {
        ke_state_ = KEState_Error;
        eventhandler_->session_crypter_key_exchange_failed(e.what());
    }
}

bool SessionCrypterClient::doing_keyexchange_phase_one(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionCrypter::doing_keyexchange_phase_one");
    switch (ke_state_) {
    case KEState_Init:
    case KEState_wait_for_SPK:
        return true;
    }
	return false;
}
