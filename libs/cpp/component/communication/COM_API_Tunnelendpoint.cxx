/*! \file COM_API_Tunnelendpoint.cxx
 *  \brief This file contains the implementation of the API wrapper for Tunnelendpoints
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <component/communication/COM_API_Tunnelendpoint.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Version;
using namespace Giritech::Communication;


/*
 * ------------------------------------------------------------------
 * APITunnelendpoint implementation
 * ------------------------------------------------------------------
 */
APITunnelendpoint::APITunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) :
    io_(io), eventhandler_(NULL), is_ready(false), checkpoint_handler_(checkpoint_handler) {
}

APITunnelendpoint::APITunnelendpoint(boost::asio::io_service& io, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, APITunnelendpointEventhandler* eventhandler) :
	io_(io), eventhandler_(eventhandler), is_ready(false), checkpoint_handler_(checkpoint_handler) {
}

APITunnelendpoint::~APITunnelendpoint(void) {
}

void APITunnelendpoint::set_eventhandler(APITunnelendpointEventhandler* eventhandler) {
    eventhandler_ = eventhandler;
}

void APITunnelendpoint::reset_eventhandler(void) {
    eventhandler_ = NULL;
    get_impl()->reset_eventhandler();
}

APIMutex::APtr APITunnelendpoint::get_mutex(void) {
    return APIMutex::create(get_impl()->get_mutex());
}

void APITunnelendpoint::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    if (eventhandler_ != NULL) {
        eventhandler_->tunnelendpoint_recieve(message->get_buffer()->toString());
    }
    return;
}

void APITunnelendpoint::tunnelendpoint_eh_connected(void) {
    if (eventhandler_ != NULL) {
        eventhandler_->tunnelendpoint_connected();
    }
}

bool APITunnelendpoint::is_connected(void) const {
    return get_impl()->is_connected();
}

void APITunnelendpoint::set_version(const Giritech::Version::Version::APtr& version) {
   get_impl()->set_version(version);
}

Giritech::Version::Version::APtr APITunnelendpoint::get_version(void) const {
    return get_impl()->get_version();
}

Giritech::Version::Version::APtr APITunnelendpoint::get_version_remote(void) const {
    return get_impl()->get_version_remote();
}

std::string APITunnelendpoint::get_id_abs() const {
    return get_impl()->get_id_abs_to_string();
}

SessionMessage::ApplProtocolType APITunnelendpoint::get_appl_protocol_type(void) {
    return get_impl()->get_appl_protocol_type();
}

boost::asio::io_service& APITunnelendpoint::get_io(void) {
	return io_;
}

unsigned long APITunnelendpoint::get_tc_read_delay_ms(void) {
    return get_impl()->get_tc_read_delay_ms();
}

unsigned long APITunnelendpoint::get_tc_read_size(void) {
    return get_impl()->get_tc_read_size();
}

void APITunnelendpoint::tc_report_read_begin(void) {
    get_impl()->tc_report_read_begin();
}

void APITunnelendpoint::tc_report_read_end(const unsigned long size) {
    get_impl()->tc_report_read_end(size);
}

void APITunnelendpoint::tc_report_push_buffer_size(const unsigned long size) {
    get_impl()->tc_report_push_buffer_size(size);
}

void APITunnelendpoint::tc_report_pop_buffer_size(const unsigned long size) {
    get_impl()->tc_report_pop_buffer_size(size);
}


/*
 * ------------------------------------------------------------------
 * APITunnelendpointReliableCrypted implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointReliableCrypted::APITunnelendpointReliableCrypted(
		const APIAsyncService::APtr& async_service,
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler)
: APITunnelendpoint(async_service->get_io_service(), checkpoint_handler) {
    impl_tunnelendpoint_ = TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, this);
}

APITunnelendpointReliableCrypted::APITunnelendpointReliableCrypted(const APIAsyncService::APtr& async_service,
                                                                   const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                   APITunnelendpointEventhandler* eventhandler) :
    APITunnelendpoint(async_service->get_io_service(), checkpoint_handler, eventhandler) {
    impl_tunnelendpoint_ = TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, this);
}

APITunnelendpointReliableCrypted::~APITunnelendpointReliableCrypted(void) {
    impl_tunnelendpoint_->reset_eventhandler();
}

APITunnelendpointReliableCrypted::APtr APITunnelendpointReliableCrypted::create(const APIAsyncService::APtr& async_service, const Utility::APICheckpointHandler::APtr& checkpoint_handler) {
    return APITunnelendpointReliableCrypted::APtr(new APITunnelendpointReliableCrypted(async_service, checkpoint_handler->get_checkpoint_handler()));
}

Tunnelendpoint::APtr APITunnelendpointReliableCrypted::get_impl(void) const {
    return impl_tunnelendpoint_;
}

void APITunnelendpointReliableCrypted::set_skip_connection_handshake(void) {
    impl_tunnelendpoint_->set_skip_connection_handshake();
}

void APITunnelendpointReliableCrypted::self_set_eventhandler(const APITunnelendpointReliableCrypted::APtr& self, APITunnelendpointEventhandler* eventhandler) {
    assert(self.get() != NULL);
    self->set_eventhandler(eventhandler);
}

void APITunnelendpointReliableCrypted::self_reset_eventhandler(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    self->reset_eventhandler();
}

bool APITunnelendpointReliableCrypted::self_is_connected(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    return self->is_connected();
}

void APITunnelendpointReliableCrypted::self_set_version(const APITunnelendpointReliableCrypted::APtr& self, const boost::python::tuple& py_version) {
    assert(self.get() != NULL);
    Version::Version::APtr version(Version::Version::create(boost::python::extract<unsigned long>(py_version[0]), boost::python::extract<unsigned long>(py_version[1]), boost::python::extract<unsigned long>(py_version[2]), boost::python::extract<unsigned long>(py_version[3])));
    self->set_version(version);
}

boost::python::tuple APITunnelendpointReliableCrypted::self_get_version(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    Version::Version::APtr version(self->get_version());
    return boost::python::make_tuple(version->get_version_major(), version->get_version_minor(), version->get_version_bugfix(), version->get_version_build_id());
}

boost::python::tuple APITunnelendpointReliableCrypted::self_get_version_remote(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    Version::Version::APtr version(self->get_version_remote());
    return boost::python::make_tuple(version->get_version_major(), version->get_version_minor(), version->get_version_bugfix(), version->get_version_build_id());
}

boost::python::str APITunnelendpointReliableCrypted::self_get_id_abs(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    return boost::python::str(self->get_id_abs());
}

APIMutex::APtr APITunnelendpointReliableCrypted::self_get_mutex(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}

SessionMessage::ApplProtocolType APITunnelendpointReliableCrypted::self_get_appl_protocol_type(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    return self->get_appl_protocol_type();
}

void APITunnelendpointReliableCrypted::self_set_skip_connection_handshake(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    self->set_skip_connection_handshake();
}


void APITunnelendpointReliableCrypted::self_tunnelendpoint_send(
		const APITunnelendpointReliableCrypted::APtr self,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCrypted::tunnelendpoint_send_decoubled, self, MessagePayload::create(DataBufferManaged::create(message))));
}
void APITunnelendpointReliableCrypted::tunnelendpoint_send_decoubled(const MessagePayload::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send(message);
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCrypted::tunnelendpoint_send_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void APITunnelendpointReliableCrypted::self_tunnelendpoint_user_signal(
		const APITunnelendpointReliableCrypted::APtr self,
		const unsigned long& signal_id,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCrypted::tunnelendpoint_user_signal_decoubled, self, signal_id, MessagePayload::create(DataBufferManaged::create(message))));
}
void APITunnelendpointReliableCrypted::tunnelendpoint_user_signal_decoubled(const unsigned long signal_id, const MessagePayload::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_user_signal(signal_id, message);
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCrypted::tunnelendpoint_user_signal_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void APITunnelendpointReliableCrypted::self_close(const APITunnelendpointReliableCrypted::APtr self) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCrypted::close_decoubled, self));
}
void APITunnelendpointReliableCrypted::close_decoubled(void) {
	try {
		impl_tunnelendpoint_->close(false);
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCrypted::close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

unsigned long APITunnelendpointReliableCrypted::self_get_tc_read_delay_ms(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
	return self->get_tc_read_delay_ms();
}

unsigned long APITunnelendpointReliableCrypted::self_get_tc_read_size(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
	return self->get_tc_read_size();
}

void APITunnelendpointReliableCrypted::self_tc_report_read_begin(const APITunnelendpointReliableCrypted::APtr& self) {
    assert(self.get() != NULL);
    self->tc_report_read_begin();
}

void APITunnelendpointReliableCrypted::self_tc_report_read_end(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_read_end(size);
}

void APITunnelendpointReliableCrypted::self_tc_report_push_buffer_size(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_push_buffer_size(size);
}

void APITunnelendpointReliableCrypted::self_tc_report_pop_buffer_size(const APITunnelendpointReliableCrypted::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_pop_buffer_size(size);
}



/*
 * ------------------------------------------------------------------
 * APITunnelendpointReliableCryptedTunnel implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointReliableCryptedTunnel::APITunnelendpointReliableCryptedTunnel(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) :
    APITunnelendpoint(async_service->get_io_service(), checkpoint_handler) {
    impl_tunnelendpoint_ = TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, this);
}

APITunnelendpointReliableCryptedTunnel::APITunnelendpointReliableCryptedTunnel(const APIAsyncService::APtr& async_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                               APITunnelendpointEventhandler* eventhandler) :
    APITunnelendpoint(async_service->get_io_service(), checkpoint_handler, eventhandler) {
    impl_tunnelendpoint_ = TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, this);
}

APITunnelendpointReliableCryptedTunnel::~APITunnelendpointReliableCryptedTunnel(void) {
    impl_tunnelendpoint_->reset_eventhandler();
}

Tunnelendpoint::APtr APITunnelendpointReliableCryptedTunnel::get_impl(void) const {
    return impl_tunnelendpoint_;
}

void APITunnelendpointReliableCryptedTunnel::set_skip_connection_handshake(void) {
    impl_tunnelendpoint_->set_skip_connection_handshake();
}

void APITunnelendpointReliableCryptedTunnel::add_tunnelendpoint(const unsigned long& child_id,
                                                                const APITunnelendpoint::APtr& tunnelendpoint) {
    impl_tunnelendpoint_->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}
void APITunnelendpointReliableCryptedTunnel::add_tunnelendpoint(const unsigned long& child_id,
                                                                const Tunnelendpoint::APtr& tunnelendpoint) {
    impl_tunnelendpoint_->add_tunnelendpoint(child_id, tunnelendpoint);
}

APITunnelendpointReliableCryptedTunnel::APtr APITunnelendpointReliableCryptedTunnel::create(const APIAsyncService::APtr& async_service, const Utility::APICheckpointHandler::APtr& checkpoint_handler) {
    return APITunnelendpointReliableCryptedTunnel::APtr(new APITunnelendpointReliableCryptedTunnel(async_service, checkpoint_handler->get_checkpoint_handler()));
}

void APITunnelendpointReliableCryptedTunnel::self_set_eventhandler(const APITunnelendpointReliableCryptedTunnel::APtr& self,
                                                                   APITunnelendpointEventhandler* eventhandler) {
    assert(self.get() != NULL);
    self->set_eventhandler(eventhandler);
}
void APITunnelendpointReliableCryptedTunnel::self_reset_eventhandler(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    self->reset_eventhandler();
}


void APITunnelendpointReliableCryptedTunnel::self_add_tunnelendpoint_reliable_crypted(const APITunnelendpointReliableCryptedTunnel::APtr& self,
                                                                                      const unsigned long& child_id,
                                                                                      const APITunnelendpointReliableCrypted::APtr& tunnelendpoint) {
    assert(self.get() != NULL);
    self->add_tunnelendpoint(child_id, tunnelendpoint);

}
void APITunnelendpointReliableCryptedTunnel::self_add_tunnelendpoint_reliable_crypted_tunnel(const APITunnelendpointReliableCryptedTunnel::APtr& self,
                                                                                             const unsigned long& child_id,
                                                                                             const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint) {
    assert(self.get() != NULL);
    self->add_tunnelendpoint(child_id, tunnelendpoint);
}

bool APITunnelendpointReliableCryptedTunnel::self_is_connected(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    return self->is_connected();
}

void APITunnelendpointReliableCryptedTunnel::self_set_version(const APITunnelendpointReliableCryptedTunnel::APtr& self, const boost::python::tuple& py_version) {
    assert(self.get() != NULL);
    Version::Version::APtr version(Version::Version::create(boost::python::extract<unsigned long>(py_version[0]), boost::python::extract<unsigned long>(py_version[1]), boost::python::extract<unsigned long>(py_version[2]), boost::python::extract<unsigned long>(py_version[3])));
    self->set_version(version);
}

boost::python::tuple APITunnelendpointReliableCryptedTunnel::self_get_version(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    Version::Version::APtr version(self->get_version());
    return boost::python::make_tuple(version->get_version_major(), version->get_version_minor(), version->get_version_bugfix(), version->get_version_build_id());
}

boost::python::tuple APITunnelendpointReliableCryptedTunnel::self_get_version_remote(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    Version::Version::APtr version(self->get_version_remote());
    return boost::python::make_tuple(version->get_version_major(), version->get_version_minor(), version->get_version_bugfix(), version->get_version_build_id());
}

boost::python::str APITunnelendpointReliableCryptedTunnel::self_get_id_abs(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    return boost::python::str(self->get_id_abs());
}

APIMutex::APtr APITunnelendpointReliableCryptedTunnel::self_get_mutex(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}

SessionMessage::ApplProtocolType APITunnelendpointReliableCryptedTunnel::self_get_appl_protocol_type(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    return self->get_appl_protocol_type();
}

void APITunnelendpointReliableCryptedTunnel::self_set_skip_connection_handshake(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    self->set_skip_connection_handshake();
}




void APITunnelendpointReliableCryptedTunnel::self_tunnelendpoint_send(const APITunnelendpointReliableCryptedTunnel::APtr self, const std::string& message) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCryptedTunnel::tunnelendpoint_send_decoubled, self, MessagePayload::create(DataBufferManaged::create(message))));
}
void APITunnelendpointReliableCryptedTunnel::tunnelendpoint_send_decoubled(const MessagePayload::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send(message);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCryptedTunnel::tunnelendpoint_send_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}


void APITunnelendpointReliableCryptedTunnel::self_tunnelendpoint_user_signal(
		const APITunnelendpointReliableCryptedTunnel::APtr self,
		const unsigned long& signal_id,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal_decoubled, self, signal_id, MessagePayload::create(DataBufferManaged::create(message))));
}
void APITunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal_decoubled(const unsigned long signal_id, const MessagePayload::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_user_signal(signal_id, message);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCryptedTunnel::tunnelendpoint_user_signal_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}


void APITunnelendpointReliableCryptedTunnel::self_remove_tunnelendpoint(
		const APITunnelendpointReliableCryptedTunnel::APtr self,
		const unsigned long child_id) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCryptedTunnel::remove_tunnelendpoint_decoubled, self, child_id));
}
void APITunnelendpointReliableCryptedTunnel::remove_tunnelendpoint_decoubled(const unsigned long child_id) {
	try {
		impl_tunnelendpoint_->remove_tunnelendpoint(child_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCryptedTunnel::remove_tunnelendpoint_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}


void APITunnelendpointReliableCryptedTunnel::self_close(const APITunnelendpointReliableCryptedTunnel::APtr self) {
    assert(self.get() != NULL);
	self->get_mutex()->get_impl()->get_strand().post(boost::bind(&APITunnelendpointReliableCryptedTunnel::close_decoubled, self));
}
void APITunnelendpointReliableCryptedTunnel::close_decoubled(void) {
	try {
		impl_tunnelendpoint_->close(false);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCryptedTunnel::close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

unsigned long APITunnelendpointReliableCryptedTunnel::self_get_tc_read_delay_ms(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
	return self->get_tc_read_delay_ms();
}

unsigned long APITunnelendpointReliableCryptedTunnel::self_get_tc_read_size(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
	return self->get_tc_read_size();
}

void APITunnelendpointReliableCryptedTunnel::self_tc_report_read_begin(const APITunnelendpointReliableCryptedTunnel::APtr& self) {
    assert(self.get() != NULL);
    self->tc_report_read_begin();
}

void APITunnelendpointReliableCryptedTunnel::self_tc_report_read_end(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_read_end(size);
}

void APITunnelendpointReliableCryptedTunnel::self_tc_report_push_buffer_size(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_push_buffer_size(size);
}

void APITunnelendpointReliableCryptedTunnel::self_tc_report_pop_buffer_size(const APITunnelendpointReliableCryptedTunnel::APtr& self, const unsigned long size) {
    assert(self.get() != NULL);
    self->tc_report_pop_buffer_size(size);
}
