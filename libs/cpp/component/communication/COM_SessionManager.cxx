/*! \file COM_SessionManager.cxx
 \brief This file contains the impplementation for the COM_SessionManager classes
 */
#include <sstream>
#include <functional>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>
#include <lib/cryptfacility/CF_KeyStore.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Servers_Spec.hxx>

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
#include <component/communication/COM_RawTunnelendpointSSLContext.hxx>
#endif

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::CryptFacility;

/*
 * ------------------------------------------------------------------
 * SessionManagerClient implementation
 * ------------------------------------------------------------------
 */
SessionManagerClient::SessionManagerClient(const AsyncService::APtr& async_service,
                                           const CheckpointHandler::APtr& checkpoint_handler,
                                           const DataBufferManaged::APtr& known_secret,
                                           const KeyStore::APtr& key_store) :
    async_service_(async_service), state_(State_Initializing), eventhandler_(NULL), session_logging_enabled_(false),
            checkpoint_handler_(checkpoint_handler), known_secret_(known_secret), key_store_(key_store), connection_id_next_(0),
            gserver_connection_group_next_(0),
            gserver_connection_group_current_selection_delay_timer_(async_service->get_io_service()),
            gserver_connection_group_current_selection_strand_(async_service->get_io_service()),
            mutex_(Mutex::create(async_service->get_io_service(), "SessionManagerClient")),
            appl_protocol_type_(SessionMessage::ApplProtocolType_python) {
}

SessionManagerClient::~SessionManagerClient(void) {
    state_ = State_Unknown;
}

SessionManagerClient::APtr SessionManagerClient::create(const AsyncService::APtr& async_service,
                                                        const CheckpointHandler::APtr& checkpoint_handler,
                                                        const DataBufferManaged::APtr& known_secret) {
    return APtr(new SessionManagerClient(async_service, checkpoint_handler, known_secret, KeyStore::create(async_service->get_io_service(), CryptFacilityService::getInstance().getCryptFactory())));
}

SessionManagerClient::APtr SessionManagerClient::create(const AsyncService::APtr& async_service,
                                                        const CheckpointHandler::APtr& checkpoint_handler,
                                                        const DataBufferManaged::APtr& known_secret,
                                                        const KeyStore::APtr& key_store) {
    return APtr(new SessionManagerClient(async_service, checkpoint_handler, known_secret, key_store));
}

Mutex::APtr SessionManagerClient::get_mutex(void) {
    return mutex_;
}

Mutex::APtr SessionManagerClient::get_session_mutex(const unsigned long& connection_id) {
    if (current_session_.get() != NULL) {
    	return current_session_->get_mutex();
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::get_session_mutex.not_found", Attr_Communication(), CpAttr_debug());
    return get_mutex();
}

void SessionManagerClient::set_eventhandler(SessionManagerEventhandler* eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::set_eventhandler");
    eventhandler_ = eventhandler;
}

void SessionManagerClient::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::reset_eventhandler");
    eventhandler_ = NULL;
}

void SessionManagerClient::set_appl_protocol_type(const SessionMessage::ApplProtocolType appl_protocol_type) {
	appl_protocol_type_ = appl_protocol_type;
}

void SessionManagerClient::set_knownsecret_and_servers(const DataBufferManaged::APtr& known_secret, const std::string& servers_spec_string) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::set_knownsecret");
    known_secret_ = known_secret;
    set_servers(servers_spec_string);
}

void SessionManagerClient::set_servers(const std::string& servers_spec_string) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::set_servers");
    switch (state_) {
    case State_Unknown:
        assert(false);
        throw ExceptionUnexpected("Session state not ready for set_servers");
    default:
        break;
    }

    // TODO: gserver_connection_groups_saved_ is leaked should be freed.
    gserver_connection_groups_.clear();

    try {
        unsigned long connection_id(0);
        ServersSpec::APtr servers_spec(ServersSpec::create());
        servers_spec->load_from_string(servers_spec_string);
        std::vector<ServersSpec::ServersConnectionGroup> servers_spec_connection_groups;
        servers_spec->get_servers_connection_groups(servers_spec_connection_groups);
        std::vector<ServersSpec::ServersConnectionGroup>::const_iterator i(servers_spec_connection_groups.begin());
        while (i != servers_spec_connection_groups.end()) {
            SessionManagerConnectionGroup::APtr connection_group(SessionManagerConnectionGroup::create(*i));
            std::vector<ServersSpec::ServersConnection>::const_iterator j(i->connections.begin());
            while (j != i->connections.end()) {
                SessionManagerConnection::APtr connection(SessionManagerConnection::create(async_service_->get_io_service(), ++connection_id,
                                                                                           checkpoint_handler_, known_secret_, key_store_, *j));
                connection->set_eventhandler(this);
                connection_group->add_connection(connection);
                j++;
            }
            gserver_connection_groups_.push_back(connection_group);
            gserver_connection_groups_saved_.push_back(connection_group);
            i++;
        }
    } catch (ServersSpec::Exception_ServersSpec& e) {
        Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::add_gservers.invalid_servers_spec", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        return;
    }
}

void SessionManagerClient::add_gserver_connection(const std::string& server_ip, const unsigned long server_port) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::add_gserver_connection");
    switch (state_) {
    case State_Unknown:
        assert(false);
        throw ExceptionUnexpected("Session state not ready for add_gserver_connection");
    default:
        break;
    }

    if (gserver_connection_groups_.size() == 0) {
        SessionManagerConnectionGroup::APtr connection_group(SessionManagerConnectionGroup::create("Default", boost::posix_time::seconds(3)));
        gserver_connection_groups_.push_back(connection_group);
        gserver_connection_groups_saved_.push_back(connection_group);
    }
    SessionManagerConnection::APtr connection(SessionManagerConnection::create(async_service_->get_io_service(), 1,
                                                                               checkpoint_handler_, known_secret_, key_store_,
                                                                               "Connection", server_ip, server_port,
                                                                               boost::posix_time::seconds(15),
                                                                               SessionManagerConnection::Type_direct));
    connection->set_eventhandler(this);
    gserver_connection_groups_.at(0)->add_connection(connection);
}

void SessionManagerClient::set_config_session_logging_enabled(const bool& session_logging_enabled) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::set_config_session_logging_enabled");
    session_logging_enabled_ = session_logging_enabled;
}

void SessionManagerClient::start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::start");
    switch (state_) {
    case State_Initializing:
    case State_Closed:
        break;
    case State_Connected:
    case State_Connecting:
    case State_Closing:
    	return;
    case State_Unknown:
        assert(false);
    default:
        Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::start.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state", state_));
        throw ExceptionUnexpected("SessionManagerClient state not ready for start");
    }
    // In case of comming from closed state
    state_ = State_Initializing;

    gserver_connection_group_next_ = 0;
    gserver_connection_group_current_ = SessionManagerConnectionGroup::APtr();
    connection_id_next_ = 0;

    /* Seslect server and try to connect */
    connect_start();
}

void SessionManagerClient::connect_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::connect_start");
    switch (state_) {
    case State_Initializing:
    case State_Connecting:
    case State_Closed:
        break;
    case State_Closing:
        return;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerClient state not ready for connect_start");
    }
    state_ = State_Connecting;

    CheckpointScope cps(*checkpoint_handler_, "SessionManagerClient::connect_start", Attr_Communication(), CpAttr_debug());
    if (gserver_connection_groups_.size() == 0) {
        cps.add_complete_attr(CpAttr("error", "No connection groups specified"));
        if (eventhandler_ != NULL) {
            eventhandler_->session_manager_failed("No connection groups specified");
        }
        close_start();
        return;
    }

    /*
     * Select the (next) connection group, if any more available
     */
    if (gserver_connection_groups_.size() <= gserver_connection_group_next_) {
        // Force expire of timer
        gserver_connection_group_current_selection_delay_timer_.expires_after(boost::chrono::seconds(1));

        cps.add_complete_attr(CpAttr("error", "No servers available at the moment"));
        if (eventhandler_ != NULL) {
            eventhandler_->session_manager_failed("No servers available at the moment");
        }
        close_start();
        return;
    }
    gserver_connection_group_current_ = gserver_connection_groups_[gserver_connection_group_next_];
    ++gserver_connection_group_next_;

    /*
     * Start selection of connections from connection group
     */
    select_connection_from_connection_group_start();

}

void SessionManagerClient::select_connection_from_connection_group_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::select_connection_from_connection_group_start");
    SessionManagerConnection::APtr connection_selected(gserver_connection_group_current_->select_connection());
    if (connection_selected.get() == NULL) {
        if (!gserver_connection_group_current_->has_pending_connections()) {
            connect_start();
        }
        return;
    }
    unsigned long connection_id = connection_id_next_++;
    Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::select_connection_from_connection_group_start",
                  Attr_Communication(), CpAttr_debug(), CpAttr("id", connection_id));

    if (eventhandler_ != NULL) {
        try {
            eventhandler_->session_manager_connecting(connection_selected->get_id(), connection_selected->get_title());
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connecting.callback",
                          Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }

    connection_selected->connect();
    session_connections_.insert(make_pair(connection_id, connection_selected));

    gserver_connection_group_current_selection_delay_timer_.expires_after(boost::chrono::seconds(gserver_connection_group_current_->get_selection_delay().total_seconds()));
    gserver_connection_group_current_selection_delay_timer_.async_wait(gserver_connection_group_current_selection_strand_.wrap(
                                                                           boost::bind(&SessionManagerClient::select_connection_from_connection_group,
                                                                           this,
                                                                           boost::asio::placeholders::error)));
}

void SessionManagerClient::select_connection_from_connection_group(const boost::system::error_code& error) {
    if (error) {
        return;
    }

    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::select_connection_from_connection_group");
    switch (state_) {
    case State_Connected:
    case State_Closing:
    case State_Closed:
    	return;
    default:
    	break;
    }
    try {
        select_connection_from_connection_group_start();
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerClient::select_connection_from_connection_group.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    }
}

void SessionManagerClient::session_manager_connection_connected(const unsigned long& id, const SessionCrypter::APtr& session_crypter) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::session_manager_connection_connected");
    switch (state_) {
    case State_Connecting:
    case State_Connected:
        break;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerClient state not ready for session_manager_connection_connected");
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connection_connected", Attr_Communication(), CpAttr_debug(), CpAttr("id", id));
    if (state_ == State_Connected) {
        Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connection_connected.session_ignored",  Attr_Communication(), CpAttr_debug(), CpAttr("id", id));
    	session_crypter->close(true);
    	return;
    }

    state_ = State_Connected;
    current_session_ = Session::create_client(async_service_->get_io_service(), 0, "0", session_logging_enabled_, checkpoint_handler_, session_crypter, this, appl_protocol_type_);
    if (eventhandler_ != NULL) {
        try {
            eventhandler_->session_manager_session_created(id, current_session_);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connection_connected.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }

}

void SessionManagerClient::session_manager_connection_resolve_connection(std::string& type,
                                                                         std::string& host,
                                                                         unsigned long& port,
                                                                         boost::posix_time::time_duration& timeout) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::session_manager_connection_resolve_connection");
    switch (state_) {
    case State_Connecting:
        break;
    default:
        return;
    }

    if (eventhandler_ != NULL) {
        try {
            eventhandler_->session_manager_resolve_connection(type, host, port, timeout);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_resolve_connection.callback",
                          Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }
}

void SessionManagerClient::session_manager_connection_failed(const unsigned long& id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::session_manager_connection_failed");
    switch (state_) {
    case State_Connecting:
        break;
    case State_Connected:
    case State_Closing:
        return;
    case State_Unknown:
        assert(false);
    default:
    	throw ExceptionUnexpected("SessionManagerClient state not ready for session_manager_connection_failed");
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connection_failed",
                  Attr_Communication(), CpAttr_debug(), CpAttr("id", id));

    if (eventhandler_ != NULL) {
        try {
            eventhandler_->session_manager_connecting_failed(id);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_connecting_failed.callback",
                          Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }
    session_connections_.erase(id);
    select_connection_from_connection_group_start();
}

void SessionManagerClient::session_closed(const unsigned long session_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::session_closed");
	switch (state_) {
    case State_Connected:
    case State_Closing:
        break;
    case State_Unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerClient state not ready for session_closed");
    }

    if (eventhandler_ != NULL) {
        try {
            eventhandler_->session_manager_session_closed(session_id);
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_session_closed.callback",
                          Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }

    if (state_ == State_Connected) {
        close_start();
    } else {
        close();
    }
}

void SessionManagerClient::session_security_closed(const unsigned long session_id, const std::string& remote_ip) {
    // Ignoreing signal, the session will be closed by the session itself
}

void SessionManagerClient::close_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::close_start");
    switch (state_) {
    case State_Closed:
    case State_Closing:
        return;
    case State_Connected:
    case State_Connecting:
        break;
    default:
        assert(false);
        throw ExceptionUnexpected("SessionManagerClient state not ready for close_start");
    }
    CheckpointScope cps(*checkpoint_handler_, "SessionManagerClient::close_start", Attr_Communication(), CpAttr_debug());

    state_ = State_Closing;
    if (current_session_.get() != NULL && !(current_session_->is_closed())) {
        current_session_->close(true);
    } else {
        close();
    }
}

void SessionManagerClient::close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerClient::close");
	CheckpointScope cps(*checkpoint_handler_, "SessionManagerClient::close", Attr_Communication(), CpAttr_debug());
    switch (state_) {
    case State_Closed:
    	return;
    case State_Closing:
        break;
    default:
        assert(false);
        throw ExceptionUnexpected("SessionManagerClient state not ready for close");
    }

    state_ = State_Closed;
    if (eventhandler_ != NULL) {
        try {
        	eventhandler_->session_manager_closed();
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_closed.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    } else {
        Checkpoint cp(*checkpoint_handler_, "SessionManagerClient::session_manager_closed.no_callback", Attr_Communication(), CpAttr_warning());
    }
}

bool SessionManagerClient::is_closed(void) const {
    switch (state_) {
    case State_Initializing:
    case State_Closed:
    case State_Unknown:
        return true;
    default:
    	break;
    }
    return false;
}

/*
 * ------------------------------------------------------------------
 * SessionManagerServer implementation
 * ------------------------------------------------------------------
 */
SessionManagerServer::SessionManagerServer(const AsyncService::APtr& async_service,
                                           const CheckpointHandler::APtr& checkpoint_handler,
                                           const DataBufferManaged::APtr& known_secret,
                                           const std::string& server_ip,
                                           const unsigned long& server_port,
                                           const std::string& server_sid) :
    async_service_(async_service), state_(State_Initializing), eventhandler_(NULL),
            checkpoint_handler_(checkpoint_handler), known_secret_(known_secret), server_ip_(server_ip),
            server_port_(server_port), server_sid_(server_sid), next_session_id_(0), session_logging_enabled_(false),
            asio_log_janitor_timer_(async_service->get_io_service()),
            asio_log_janitor_strand_(async_service->get_io_service()),
            asio_idle_janitor_timer_(async_service->get_io_service()),
            asio_idle_janitor_strand_(async_service->get_io_service()),
            idle_timeout_enabled_(true),
            mutex_(Mutex::create(async_service->get_io_service(), "SessionManagerServer")),
            security_state_(SecurityState_Normal),
		    security_state_banned_connection_count_(0),
		    security_state_failed_keyexchange_count_(0),
		    security_dos_attack_interval_banned_connection_count_(0),
		    security_dos_attack_interval_connection_count_(0) {
    acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler_, async_service->get_io_service(), server_ip_, server_port_, this, this);
    acceptor_->set_mutex(mutex_);
    set_config_idle_timeout(boost::posix_time::seconds(60), boost::posix_time::seconds(60));
    set_config_log_interval(boost::posix_time::seconds(30));
    set_config_close_when_no_more_sessions(false);
    set_config_security_dos_attack(20, 10, 4, 2, true);
}

SessionManagerServer::~SessionManagerServer(void) {
    state_ = State_Unknown;
}

SessionManagerServer::APtr SessionManagerServer::create(const AsyncService::APtr& async_service,
                                                        const CheckpointHandler::APtr& checkpoint_handler,
                                                        const DataBufferManaged::APtr& known_secret,
                                                        const std::string& server_ip,
                                                        const unsigned long& server_port,
                                                        const std::string& server_sid) {
    return APtr(new SessionManagerServer(async_service, checkpoint_handler, known_secret, server_ip, server_port, server_sid));
}

Mutex::APtr SessionManagerServer::get_mutex(void) {
    return mutex_;
}

Mutex::APtr SessionManagerServer::get_session_mutex(const unsigned long& session_id) {
	SessionsType::iterator i(sessions_.find(session_id));
    if (i != sessions_.end()) {
        return (i->second)->get_mutex();
    }
    return get_mutex();
}

void SessionManagerServer::set_eventhandler(SessionManagerEventhandler* eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_eventhandler");
    eventhandler_ = eventhandler;
}

void SessionManagerServer::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::reset_eventhandler");
    eventhandler_ = NULL;
}

void SessionManagerServer::set_config_close_when_no_more_sessions(const bool& config_close_when_no_more_sessions) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_config_close_when_no_more_sessions");
    config_close_when_no_more_sessions_ = config_close_when_no_more_sessions;
}

void SessionManagerServer::set_config_log_interval(const boost::posix_time::time_duration& config_log_interval) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_config_log_interval");
    config_log_interval_ = config_log_interval;
}

void SessionManagerServer::set_config_session_logging_enabled(const bool& session_logging_enabled) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_config_session_logging_enabled");
    session_logging_enabled_ = session_logging_enabled;
}

void SessionManagerServer::set_option_reuse_address(const bool& option_reuse_address) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_option_reuse_address");
    acceptor_->set_option_reuse_address(option_reuse_address);
}

void SessionManagerServer::set_config_security_dos_attack(const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips) {
	security_dos_attack_keyexchange_phase_one_count_high_ = keyexchange_phase_one_count_high;
	security_dos_attack_keyexchange_phase_one_count_low_ = keyexchange_phase_one_count_low;
	security_dos_attack_security_closed_count_high_ = security_closed_count_high;
    security_dos_attack_security_closed_count_low_ = security_closed_count_low;
	security_dos_attack_ban_atacker_ips_ = ban_atacker_ips;
}

void SessionManagerServer::set_config_https(
		const std::string& certificate_path,
		const bool certificate_verificatrion_enabled,
		const bool hostname_verification_enabled,
		const bool sni_enabled,
		const std::string& method,
		const unsigned long ssl_options) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	Checkpoint(*checkpoint_handler_, "SessionManagerServer::set_config_https", Attr_Communication(), CpAttr_debug(), CpAttr("method", method), CpAttr("ssl_options", ssl_options));
	RawTunnelendpointTCPSSLContext::get_instance(async_service_->get_io_service(), method, ssl_options)->init(checkpoint_handler_, certificate_path, certificate_verificatrion_enabled, hostname_verification_enabled, sni_enabled);
#else
	Checkpoint(*checkpoint_handler_, "SessionManagerServer::set_config_https.ignored", Attr_Communication(), CpAttr_warning(), CpAttr("message", "SSL not supported on this platform"));
#endif
}


void SessionManagerServer::asio_log_janitor_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::asio_log_janitor_start");
    asio_log_janitor_timer_.expires_after(boost::chrono::seconds(config_log_interval_.total_seconds()));
    asio_log_janitor_timer_.async_wait(asio_log_janitor_strand_.wrap(boost::bind(&SessionManagerServer::asio_log_janitor, this, boost::asio::placeholders::error)));
}

void SessionManagerServer::asio_log_janitor(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::asio_log_janitor");
    Checkpoint(*checkpoint_handler_, "SessionManagerServer::asio_log_janitor", Attr_Communication(), CpAttr_debug(), CpAttr("session_count", sessions_.size()), CpAttr("async_mem_guard.count",  AsyncMemGuard::global_get_element_count()));
    if(AsyncMemGuard::global_get_element_count() > 100) {
        Checkpoint(*checkpoint_handler_, "SessionManagerServer::asio_log_janitor", Attr_Communication(), CpAttr_warning(), CpAttr("session_count", sessions_.size()), CpAttr("content",  AsyncMemGuard::global_analyze()));
    }

    switch (state_) {
    case State_Closing:
    case State_Closed:
    	return;
    default:
    	break;
    }

    try {
        asio_log_janitor_start();
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerServer::asio_log_janitor", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    }
}

void SessionManagerServer::set_config_idle_timeout(const boost::posix_time::time_duration& config_session_idle_timeout,
												   const boost::posix_time::time_duration& keyexchange_idle_timeout) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::set_config_session_idle_timeout");
	idle_timeout_enabled_ = true;
    config_session_idle_timeout_ = config_session_idle_timeout;
    if (config_session_idle_timeout_ == boost::posix_time::seconds(0)) {
    	config_session_idle_timeout_ = boost::posix_time::hours(1);
    	idle_timeout_enabled_ = false;
    }
    config_keyexchange_idle_timeout_ =  keyexchange_idle_timeout;
}

void SessionManagerServer::asio_session_idle_janitor_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::asio_session_idle_janitor_start");
    asio_idle_janitor_timer_.expires_after(boost::chrono::seconds(10));
    asio_idle_janitor_timer_.async_wait(asio_idle_janitor_strand_.wrap(boost::bind(&SessionManagerServer::asio_session_idle_janitor, this, boost::asio::placeholders::error)));
}

void SessionManagerServer::asio_session_idle_janitor(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::asio_session_idle_janitor");
    CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::asio_session_idle_janitor", Attr_Communication(), CpAttr_debug());
    switch (state_) {
    case State_Closing:
    case State_Closed:
        return;
    default:
    	break;
    }

    try {
    	if (idle_timeout_enabled_) {
    		timeout_sessions(config_session_idle_timeout_, true);
    	}
		timeout_sessions_keyexchange(config_keyexchange_idle_timeout_, true, false);
    	security_dos_attack_idle_janitor();
    	asio_session_idle_janitor_start();
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerServer::asio_session_idle_janitor.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    }
}

void SessionManagerServer::com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& raw_tunnelendpoint) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::com_tunnelendpoint_accepted");
	CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::com_tunnelendpoint_accepted", Attr_Communication(), CpAttr_debug(), CpAttr("session_id", next_session_id_));
	raw_tunnelendpoint->log_socket_info(cps);

	Session::APtr session = Session::create_server(async_service_->get_io_service(), next_session_id_, server_sid_,
			session_logging_enabled_, checkpoint_handler_, raw_tunnelendpoint,
			known_secret_);
	session->set_eventhandler(this);
	sessions_.insert(make_pair(next_session_id_, session));
	next_session_id_ += 1;

	if (eventhandler_ != NULL) {
		try {
			eventhandler_->session_manager_session_created(next_session_id_, session);
		} catch (ExceptionCallback& e) {
			Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::com_tunnelendpoint_accepted.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
			raw_tunnelendpoint->aio_close_start(true);
			return;
		}
	}
	cps.add_complete_attr(CpAttr("session_id", session->get_session_id()));
	cps.add_complete_attr(CpAttr("unique_session_id", session->get_unique_session_id()));
    security_dos_attack_do_keyexchange_fence(session);
}

void SessionManagerServer::security_dos_attack_do_keyexchange_fence(const Session::APtr& session) {

	unsigned long doing_keyexchange_phase_one_count = security_dos_attack_get_count_doing_keyexchange_phase_one();
	if(doing_keyexchange_phase_one_count > security_dos_attack_keyexchange_phase_one_count_high_) {
    	std::pair<std::string, unsigned long> remote_ip_info(session->get_ip_remote());
    	security_dos_attack(remote_ip_info.first);
        session->close(true);
    }
    if(security_dos_attack_banned(session)) {
    	security_dos_attack_interval_banned_connection_count_++;
    	security_state_banned_connection_count_++;
    	Checkpoint cps_key_exchange(*checkpoint_handler_, "SessionManagerServer::security_dos_attack_do_keyexchange_fence.banned", Attr_Communication(), CpAttr_debug());
    	session->close(true);
    }
    else {
    	security_dos_attack_interval_connection_count_++;
        Checkpoint cps_key_exchange(*checkpoint_handler_, "SessionManagerServer::security_dos_attack_do_keyexchange_fence.do_key_exchange", Attr_Communication(), CpAttr_debug());
    	session->do_key_exchange();
    }
}

unsigned long SessionManagerServer::security_dos_attack_get_count_doing_keyexchange_phase_one(void) {
	/*
	 * Phase-one of the key-exchange is either generating session-keys or waiting for first package from client.
	 */
    unsigned long doing_keyexchange_phase_one_count = 0;

    SessionsType::const_iterator i(sessions_.begin());
    while (i != sessions_.end()) {
		if ((i->second)->doing_keyexchange_phase_one()) {
			doing_keyexchange_phase_one_count++;
		}
        ++i;
    }
    return doing_keyexchange_phase_one_count;
}

void SessionManagerServer::security_dos_attack(const std::string& attack_ip) {
    if (security_state_ == SecurityState_Normal) {
    	security_state_ = SecurityState_DoSAttack;
    	security_state_banned_ips_.clear();
    	security_state_banned_connection_count_ = 0;
	    security_state_failed_keyexchange_count_ = 0;

    	security_state_banned_ips_.insert(attack_ip);

    	SessionsType::const_iterator i(sessions_.begin());
        while (i != sessions_.end()) {
            if ((i->second)->doing_keyexchange_phase_one()) {
            	std::pair<std::string, unsigned long> remote_ip_info((i->second)->get_ip_remote());
            	security_state_banned_ips_.insert(remote_ip_info.first);
            }
            ++i;
        }
		string attacker_ips = security_dos_attack_get_attack_info();
        Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::security_dos_attack_start", Attr_Communication(), CpAttr_info(), CpAttr("attacker_ips", attacker_ips));
		if (eventhandler_ != NULL) {
			try {
				eventhandler_->session_manager_security_dos_attack_start(attacker_ips);
			} catch (ExceptionCallback& e) {
				Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::session_manager_security_dos_attack_start.callback",  Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
			}
		}
    }

    /*
     * Limit the insert of attacker info if false ip info are used they are anyway useless,
     * and could cause memory problems
     */
    if (security_state_banned_ips_.size() < security_dos_attack_keyexchange_phase_one_count_high_) {
    	security_state_banned_ips_.insert(attack_ip);
    }
}

void SessionManagerServer::security_dos_attack_idle_janitor(void) {
	if (security_state_ == SecurityState_Normal) {
	}
	if (security_state_ == SecurityState_DoSAttack) {

		unsigned long doing_keyexchange_phase_one_count = security_dos_attack_get_count_doing_keyexchange_phase_one();
		if ((doing_keyexchange_phase_one_count < security_dos_attack_keyexchange_phase_one_count_low_) &&
			(security_dos_attack_interval_security_closed_count_ < security_dos_attack_security_closed_count_low_) &&
			(security_dos_attack_interval_banned_connection_count_ <= security_dos_attack_interval_connection_count_)) {
			security_state_ = SecurityState_Normal;

			string attacker_ips = security_dos_attack_get_attack_info();
			CheckpointScope cps_key_exchange(*checkpoint_handler_, "SessionManagerServer::security_dos_attack_stop", Attr_Communication(), CpAttr_info(), CpAttr("attacker_ips", attacker_ips));
			if (eventhandler_ != NULL) {
				try {
					eventhandler_->session_manager_security_dos_attack_stop(attacker_ips, security_state_banned_connection_count_, security_state_failed_keyexchange_count_);
				} catch (ExceptionCallback& e) {
					Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::session_manager_security_dos_attack_stop.callback",  Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
				}
			}
		}
		else {
			timeout_sessions_keyexchange(boost::posix_time::seconds(10), true, true);
		}
	}
	security_dos_attack_interval_security_closed_count_ = 0;
	security_dos_attack_interval_banned_connection_count_ = 0;
	security_dos_attack_interval_connection_count_ = 0;
}

bool SessionManagerServer::security_dos_attack_banned(const Session::APtr& session) {
    if (security_state_ == SecurityState_Normal) {
        return false;
    }
    if(!security_dos_attack_ban_atacker_ips_) {
    	return false;
    }
	std::pair<std::string, unsigned long> remote_ip_info(session->get_ip_remote());
	return security_state_banned_ips_.count(remote_ip_info.first) > 0;
}

std::string SessionManagerServer::security_dos_attack_get_attack_info(void) {
    stringstream ss;
    ss << "[";
    std::set<std::string>::const_iterator i(security_state_banned_ips_.begin());
    while( i != security_state_banned_ips_.end()) {
        ss << *i;
        ++i;
        if (i != security_state_banned_ips_.end()) {
            ss << ", ";
        }
    }
    ss << "]";
    return ss.str();
}

void SessionManagerServer::com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::com_tunnelendpoint_acceptor_error");
    CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::com_tunnelendpoint_acceptor_error",
                        Attr_Communication(), CpAttr_error(), CpAttr("message", error_message));
    close_start();
}

void SessionManagerServer::com_tunnelendpoint_acceptor_closed(void) {
	// Ignored for now
}

bool SessionManagerServer::com_accept_continue(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::com_accept_continue");
    switch (state_) {
    case State_Closing:
    case State_Closed:
        return false;
    default:
        return true;
    }
}

void SessionManagerServer::session_state_ready(const unsigned long session_id) {
    /*
     * This is called from a session holding the session-lock. Taking
     * the session_manager-lock could result in a deadlock.
     */
}

bool SessionManagerServer::session_read_continue_state_ready(const unsigned long session_id) {
    /*
     * This is called from a session holding the session-lock. Taking
     * the session_manager-lock could result in a deadlock.
     */
    switch (state_) {
    case State_Closing:
    case State_Closed:
        return false;
    default:
        return true;
    }
}

void SessionManagerServer::timeout_sessions(const boost::posix_time::time_duration& timeout, const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::timeout_sessions");
    /*
     * Examine for idle sessions. A close of a idle session will mark
     * the session for erase via session_closed()
     */
    SessionsType::const_iterator i(sessions_.begin());
    while (i != sessions_.end()) {
        if ((i->second)->is_idle(timeout)) {
        	CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::timeout_session", Attr_Communication(), CpAttr_info(), CpAttr("unique_session_id", (i->second)->get_unique_session_id()));
        	(i->second)->close(force);
        }
        ++i;
    }
}

void SessionManagerServer::timeout_sessions_keyexchange(const boost::posix_time::time_duration& timeout, const bool force, const bool only_if_keyexchange_phase_one) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::timeout_sessions");
    /*
     * Examine for idle sessions. A close of a idle session will mark
     * the session for erase via session_closed()
     */
    SessionsType::const_iterator i(sessions_.begin());
    while (i != sessions_.end()) {
        if ((i->second)->is_idle(timeout) && (i->second)->doing_keyexchange()) {
        	if(!only_if_keyexchange_phase_one || (only_if_keyexchange_phase_one && (i->second)->doing_keyexchange_phase_one()) ) {
				CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::timeout_session", Attr_Communication(), CpAttr_info(), CpAttr("unique_session_id", (i->second)->get_unique_session_id()));
				(i->second)->close(force);
        	}
        }
        ++i;
    }
}


void SessionManagerServer::session_security_closed(const unsigned long session_id, const std::string& remote_ip) {
    /*
     * This is called from a session holding the session-lock. Taking
     * the session_manager-lock could result in a deadlock.
     */
	mutex_->get_strand().post(boost::bind(&SessionManagerServer::session_security_closed_decoupled, this, session_id, remote_ip));
}

void SessionManagerServer::session_security_closed_decoupled(const unsigned long session_id, const std::string& remote_ip) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::session_security_closed_decoupled");
	try {
		std::string unique_session_id;
		SessionsType::iterator i(sessions_.find(session_id));
		if (i != sessions_.end()) {
			unique_session_id = (i->second)->get_unique_session_id();
		}
		CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::session_security_closed_decoupled", Attr_Communication(), CpAttr_debug(), CpAttr("session_id", session_id), CpAttr("unique_session_id", unique_session_id));
		session_closed_decoupled(session_id);

		security_state_failed_keyexchange_count_++;
		security_dos_attack_interval_security_closed_count_++;
		if(security_dos_attack_interval_security_closed_count_ > security_dos_attack_security_closed_count_high_) {
			security_dos_attack(remote_ip);
		}
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerServer::session_security_closed_decoupled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void SessionManagerServer::session_closed(const unsigned long session_id) {
    /*
     * This is called from a session holding the session-lock. Taking
     * the session_manager-lock could result in a deadlock.
     * This is prevented by decoupled call.
     */
  mutex_->get_strand().post(boost::bind(&SessionManagerServer::session_closed_decoupled, this, session_id));
}

void SessionManagerServer::session_closed_decoupled(const unsigned long session_id) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::session_closed");
	try {
		std::string unique_session_id;
		SessionsType::iterator i(sessions_.find(session_id));
		if (i != sessions_.end()) {
			unique_session_id = (i->second)->get_unique_session_id();
		}
		else {
			CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::session_closed.session_not_found", Attr_Communication(), CpAttr_error(), CpAttr("session_id", session_id));
			return;
		}

		CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::session_closed_decoupled", Attr_Communication(), CpAttr_info(), CpAttr("session_id", session_id), CpAttr("unique_session_id", unique_session_id));
		if (eventhandler_ != NULL) {
			try {
				eventhandler_->session_manager_session_closed(session_id);
			} catch (ExceptionCallback& e) {
				Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::session_closed.session_manager_session_closed.callback", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
			}
		}
		else {
			CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::session_closed.eventhandler_missing", Attr_Communication(), CpAttr_critical(), CpAttr("session_id", session_id));
		}

		sessions_.erase(i);

		if (state_ == State_Closing && sessions_.size() == 0) {
			close();
		} else {
			if (config_close_when_no_more_sessions_ && sessions_.size() == 0) {
				close_start();
			}
		}
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerServer::session_closed_decoupled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}


void SessionManagerServer::close_start(void) {
    /*
     * Due to a mutex lock situation involving Session, SessionManager and Python lock the close_start has been
     * decoupled.
     */
	mutex_->get_strand().post(boost::bind(&SessionManagerServer::close_start_decoupled, this));
}

void SessionManagerServer::close_start_decoupled(void) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::close_start_decoupled");
	try {
		CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::close_start_decoupled", Attr_Communication(), CpAttr_debug(), CpAttr("state", state_), CpAttr("sessions", sessions_.size()));
		switch (state_) {
		case State_Running:
			break;
		case State_Closed:
		case State_Closing:
			return;
		default:
			assert(false);
			throw ExceptionUnexpected("SessionManagerServer state not ready for close_start_decoupled");
		}
		state_ = State_Closing;

		/*
		 * Stop for new connections
		 */
		acceptor_->aio_close_start();

		/*
		 * Timeout all sessions
		 */
		timeout_sessions(boost::posix_time::seconds(0), true);
		asio_log_janitor_timer_.expires_after(boost::chrono::seconds(0));
		asio_idle_janitor_timer_.expires_after(boost::chrono::seconds(0));

		if (sessions_.size() == 0) {
			close();
		}
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerServer::close_start_decoupled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

void SessionManagerServer::close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::close");
    CheckpointScope cps(*checkpoint_handler_, "SessionManagerServer::close", Attr_Communication(), CpAttr_debug());
    switch (state_) {
    case State_Closing:
        break;
    default:
        assert(false);
        throw ExceptionUnexpected("SessionManagerServer state not ready for close");
    }
    state_ = State_Closed;

    if (eventhandler_ != NULL) {
        SessionManagerEventhandler* temp_eventhandler(eventhandler_);
        reset_eventhandler();
        try {
            temp_eventhandler->session_manager_closed();
        } catch (ExceptionCallback& e) {
            Checkpoint cp(*checkpoint_handler_, "SessionManagerServer::session_manager_closed.callback",
                          Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
        }
    }

}

bool SessionManagerServer::is_closed(void) const {
    return state_ == State_Closed;
}

void SessionManagerServer::start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::start");
	switch (state_) {
    case State_Initializing:
        state_ = State_Running;

        asio_session_idle_janitor_start();
        asio_log_janitor_start();

        acceptor_->aio_accept_start();
        break;
    default:
        break;
    }
}

void SessionManagerServer::accept_connections_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::accept_connections_start");
	switch (state_) {
    case State_Running:
        acceptor_->aio_accept_ignore_stop();
        break;
    default:
        break;
    }
}

void SessionManagerServer::accept_connections_stop(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::accept_connections_stop");
	switch (state_) {
    case State_Running:
        acceptor_->aio_accept_ignore_start();
        break;
    default:
        break;
    }
}

int SessionManagerServer::get_session_count(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerServer::accept_connections_stop");
    return sessions_.size();
}
