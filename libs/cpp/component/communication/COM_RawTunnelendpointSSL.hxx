/*! \file COM_RawTunnelendpointSSL.hxx
 * \brief This file contains the RawTunnelendpoint abstraction
 */
#ifndef COM_RawTunnelendpointSSL_HXX
#define COM_RawTunnelendpointSSL_HXX

#include <boost/asio/ssl.hpp>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointSSLContext.hxx>


namespace Giritech {
namespace Communication {


/*! \brief This class define a RawTunnelendpoint representing a TCP socket.
 */
class RawTunnelendpointTCPSSL: public RawTunnelendpointTCP {
public:
    typedef boost::shared_ptr<RawTunnelendpointTCPSSL> APtr;
    typedef boost::weak_ptr<RawTunnelendpointTCPSSL> WAPtr;

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& get_socket_ssl(void);
    const boost::asio::ssl::stream<boost::asio::ip::tcp::socket>& get_socket_ssl(void) const;

    boost::asio::ip::tcp::socket& get_socket(void);
    const boost::asio::ip::tcp::socket& get_socket(void) const;

    ~RawTunnelendpointTCPSSL(void);

    static APtr create(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		boost::asio::io_service& io,
    		const unsigned long& connection_id,
    		const std::string& host_name,
    		const RawTunnelendpointTCPSSLContext::APtr& context);

private:
     RawTunnelendpointTCPSSL(
     		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
     		boost::asio::io_service& io,
     		const unsigned long& connection_id,
     		const std::string& host_name,
     		const RawTunnelendpointTCPSSLContext::APtr& context);

     boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket_;
     RawTunnelendpointTCPSSLContext::APtr context_;

     std::string host_name_;
     /*
      *
      */
     virtual void socket_operation_async_read(void);
     virtual void socket_operation_async_write(const DataBufferWithTime::APtr& data);
//     virtual void socket_operation_shutdown_send(void);
//     virtual void socket_operation_shutdown_both(void);
//     virtual void socket_operation_shutdown_receive(void);
//     virtual void socket_operation_close(void);
//     virtual void socket_operation_log_info(Giritech::Utility::CheckpointScope& cps) const;
//     virtual void socket_operation_set_option_nodelay(void);
     virtual bool socket_operation_is_eof(const boost::system::error_code& error_code);
};

}}
#endif
