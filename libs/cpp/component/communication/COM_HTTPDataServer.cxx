/*! \file COM_TunnelendpointTCPMessage.cxx
 \brief This file contains the implementation for the tunnelendpoint-tcp-message class
 */
#include <functional>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>


#include <lib/utility/UY_Convert.hxx>

#include <component/communication/COM_HTTPDataServer.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * COMHTTPDataServer implementation
 * ------------------------------------------------------------------
 */
COMHTTPDataServer::COMHTTPDataServer(
		const AsyncService::APtr& async_service,
        const Utility::CheckpointHandler::APtr& checkpoint_handler,
        const std::string& data,
        const std::string& data_content_type,
        const std::string& url_filename,
        const boost::posix_time::time_duration& timeout,
        const std::string& listen_host,
        const unsigned long listen_port) :
        async_service_(async_service),
        checkpoint_handler_(checkpoint_handler),
        timeout_(timeout) {

	boost::uuids::basic_random_generator<boost::mt19937> gen;
	boost::uuids::uuid u = gen();
	stringstream ss;
	ss << u;
    httpDataServer_ = Giritech::HTTPDataServer::server::create(checkpoint_handler, listen_host, listen_port, data, data_content_type, ss.str(), url_filename, timeout);
}


COMHTTPDataServer::~COMHTTPDataServer(void) {
}

COMHTTPDataServer::APtr COMHTTPDataServer::create(
		const AsyncService::APtr& async_service,
        const Utility::CheckpointHandler::APtr& checkpoint_handler,
        const std::string& data,
        const std::string& data_content_type,
        const std::string& url_filename,
        const boost::posix_time::time_duration& timeout,
        const std::string& listen_host,
        const unsigned long listen_port) {
	APtr result(new COMHTTPDataServer(async_service, checkpoint_handler, data, data_content_type, url_filename, timeout, listen_host, listen_port));
	AsyncMemGuard::global_add_element(result);
	return result;
}

std::string COMHTTPDataServer::get_url(void) {
	return httpDataServer_->get_url();
}

void COMHTTPDataServer::start(void) {
	httpDataServerThreads_.create_thread(boost::bind(&Giritech::HTTPDataServer::server::run, httpDataServer_));
}

void COMHTTPDataServer::terminate(void) {
	if(httpDataServer_->is_serving()) {
		httpDataServer_->stop();
	}
}

bool COMHTTPDataServer::async_mem_guard_is_done(void) {
	return httpDataServer_->is_done();
}

std::string COMHTTPDataServer::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}
