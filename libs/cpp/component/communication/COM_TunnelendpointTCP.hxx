/*! \file COM_TunnelendpointTCP.hxx
 \brief This file contains the tunnelendpoint tcp clases
 */
#ifndef COM_TunnelendpointTCP_HXX
#define COM_TunnelendpointTCP_HXX

#include <map>
#include <queue>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_AsyncContinuePolicy.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointTCPControlEventhandler.hxx>
#include <component/communication/COM_TunnelendpointCheckpointHandler.hxx>



namespace Giritech {
namespace Communication {

/*
 * Forward declarations of eventhandlers
 */
class TunnelendpointTCPEventhandler;
class TunnelendpointTCPTunnelClientEventhandler;
class TunnelendpointTCPTunnelServerEventhandler;
class TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler;


/*! \brief Tunnelendpoint for handling traffic for a TCP connection
 *
 * A TCP-Tunnelendpoint handles the traffic of a TCP connection using a rawtunnelendpoint.
 *
 */
class TunnelendpointTCP:
	public TunnelendpointReliableCrypted,
	public TunnelendpointEventhandler,
	public RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<TunnelendpointTCP> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointTCP(void);

    /*! \brief Set event handler
     */
    void set_tcp_eventhandler(TunnelendpointTCPEventhandler* tcp_eventhandler);
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Set tunnel id
     */
    void set_tunnel_id(const unsigned long tunnel_id);

    /*! \brief Reset event handler
     */
    void reset_tcp_eventhandler(void);
    void reset_tcp_ctrl_eventhandler(void);

    /*! \brief Close tcp tunnelendpoint
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    virtual void close(const bool force);
    bool is_closed(void);

    /*! \brief Indgoto_closed_state_icate that no more data are comming, but still open for read
     */
    void eof(void);

    /*! \brief Start reading
     */
    void read_start(void);

    /*! \brief Geters of ip/port of local and remote part of socket
     */
    std::pair<std::string, unsigned long> get_ip_local(void);
    std::pair<std::string, unsigned long> get_ip_remote(void);


    /*! \brief Notification that the tunnelendpoint is trying to connect to its server/client counterpart
     */
    void connecting(void);

    /*! \brief Notification that the tunnelendpoint is connected to its server/client counterpart
     */
    void connected(void);

    /*! \brief Signals that the raw tunnelendpoint has been closed
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_close(const unsigned long& connection_id);

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);

    /*! \brief Signals that data has been received
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);

    /*! \brief Signals that write buffer is empty
     *
     *  Implementation of RawTunnelendpointEventhandler signal handling
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			           boost::asio::io_service& io,
    		           const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                       TunnelendpointTCPControlEventhandler* tcp_ctrl_eventhandler);

protected:

    /*! \brief Constructor
     */
    TunnelendpointTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                      boost::asio::io_service& io,
                      const RawTunnelendpointTCP::APtr& raw_tunnelendpoint,
                      TunnelendpointTCPControlEventhandler* tcp_ctrl_eventhandler);

    void goto_ready_state_(void);
    void goto_closed_state_(void);

    boost::asio::io_service& io_;
    TunnelendpointTCPEventhandler* tcp_eventhandler_;
    TunnelendpointTCPControlEventhandler* tcp_ctrl_eventhandler_;

    enum StateTCP {
        StateTCP_Initializing = 0,
        StateTCP_Connecting,
        StateTCP_Ready,
        StateTCP_Closing,
        StateTCP_Closed,
        StateTCP_Unknown
    };

    StateTCP state_tcp_;
    RawTunnelendpointTCP::APtr raw_tunnelendpoint_;
    unsigned long tunnel_id_;
    unsigned long connection_id_;
};

/*! \brief Server side tunnelendpoint factory for handling TCP connections related to the same ip/port.
 *
 * This server-side factory creates tunnelendpoint for each tcp-connection using a rawtunnelendpoint.
 *
 */
class TunnelendpointTCPTunnelServerBase: public TunnelendpointReliableCryptedTunnel, TunnelendpointEventhandler {
public:

    /*! \brief Destructor
     */
    virtual ~TunnelendpointTCPTunnelServerBase(void);

    /*! \brief Set event handler
     */
    virtual void set_tcp_eventhandler(TunnelendpointTCPTunnelServerEventhandler* tcp_eventhandler);
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Reset event handler
     */
    virtual void reset_tcp_eventhandler(void);

    /*! \brief Called by parent/owner of tunnelendpoint
     */
    virtual void connect(void);

    /*! \brief Close tunnelendpoint and all its children connections
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    virtual void close(const bool force);

    /*! \brief Return true if the tunnelendpoint is closed
     */
    virtual bool is_closed();

    /*! \brief Signals that the tunnelendpoint has recieved a message

     *  Implementation of TunnelendpointEventhandler signal handling
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Sends a message
     */
    virtual void tunnelendpoint_tcp_send(const Utility::DataBufferManaged::APtr& message);


    /*! \brief Enable use of special checkpoint tunnel that logs directly to the server
     *
     */
    void enable_checkpoint_tunnel(const boost::filesystem::path& folder);


    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

protected:

    /*! \brief Constructor
     */
    TunnelendpointTCPTunnelServerBase(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle, boost::asio::io_service& io, const unsigned long id);

    /*! \brief helper function that do the actual close
     */
    virtual void close_down_(const bool force) = 0;

    virtual void goto_ready_state_(void);
    virtual void goto_closed_state_(void);

    virtual void tunnelendpoint_recieve_connect(const unsigned long tunnel_id) = 0;
    virtual void tunnelendpoint_recieve_eof(const unsigned long tunnel_id) = 0;
    virtual void tunnelendpoint_recieve_close(const unsigned long tunnel_id) = 0;
    virtual void tunnelendpoint_recieve_closed(const unsigned long tunnel_id) = 0;

    virtual void tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message);
    virtual void tunnelendpoint_recieve_checkpoint(const unsigned long tunnel_id);

    enum StateTCP {
        StateTCP_Initializing = 0,
        StateTCP_Ready = 1,
        StateTCP_Closing,
        StateTCP_Closing_no_connection,
        StateTCP_Closed,
        StateTCP_Unknown
    };
    TunnelendpointTCPTunnelServerEventhandler* tcp_tunnel_eventhandler_;
    StateTCP state_tcp_;

    unsigned long id_;

    bool checkpoint_tunnel_enabled_;
    TunnelendpointCheckpointHandlerServer::APtr checkpoint_tunnel_;
    unsigned long checkpoint_tunnel_id_;
};


/*! \brief Server side tunnelendpoint factory for handling TCP connections related to the same ip/port.
 *
 * This server-side factory creates tunnelendpoint for each tcp-connection using a rawtunnelendpoint.
 */
class TunnelendpointTCPTunnelServer:
	public TunnelendpointTCPTunnelServerBase,
	public RawTunnelendpointConnectorTCPEventhandler,
	public TunnelendpointTCPControlEventhandler {
public:
    typedef boost::shared_ptr<TunnelendpointTCPTunnelServer> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointTCPTunnelServer(void);

    /*! \brief Set event handler
     */
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Connected signal
     *
     *  Implementation of RawTunnelendpointConnectorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Failed to connect signal
     *
     *  Implementation of RawTunnelendpointConnectorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_connection_failed_to_connect(const std::string& message);

    /*! \brief Implementation of TunnelendpointTCPControlEventhandler interface
     */
    virtual void tunnelendpoint_tcp_eh_eof(const unsigned long tunnel_id);
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long tunnel_id);

    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const std::string& server_ip,
                       const unsigned long server_port,
                       const unsigned long id);

protected:

    /*! \brief Constructor
     */
    TunnelendpointTCPTunnelServer(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                  boost::asio::io_service& io,
                                  const std::string& server_ip,
                                  const unsigned long server_port,
                                  const unsigned long id);

   /*! \brief helper function that do the actual close
    */
   virtual void close_down_(const bool force);
   virtual void goto_closed_state_(void);
   virtual void tunnelendpoint_recieve_connect(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_eof(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_close(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_closed(const unsigned long tunnel_id);

    std::queue<unsigned long> tunnel_id_waiting_for_connections_;
    RawTunnelendpointConnectorTCP::APtr raw_connector_;

    std::map<unsigned long, TunnelendpointTCP::APtr> tcp_tunnelendpoints_;
};



/*! \brief Server side tunnelendpoint factory for handling TCP connections related to the same ip/port.
 *
 * This server-side factory creates tunnelendpoint for each tcp-connection using a rawtunnelendpoint.
 */
class TunnelendpointTCPTunnelServerWithConnectionEngine:
	public TunnelendpointTCPTunnelServerBase {

public:
	typedef boost::shared_ptr<TunnelendpointTCPTunnelServerWithConnectionEngine> APtr;

	/*! \brief Destructor
	 */
	virtual ~TunnelendpointTCPTunnelServerWithConnectionEngine(void);

    /*! \brief Set/Reset event handler
     */
    virtual void set_tcp_eventhandler(TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* tcp_eventhandler);
    virtual void reset_tcp_eventhandler(void);

	/*! \brief Sends eof signal to remote child
	 */
	virtual void tunnelendpoint_send_remote_eof(const unsigned long tunnel_id);
	virtual void tunnelendpoint_send_remote_close(const unsigned long tunnel_id);
	virtual void tunnelendpoint_send_remote_closed(const unsigned long tunnel_id);

	/*! \brief Sends recieve_connect responses to remote
	 */
	virtual void tunnelendpoint_send_remote_recieve_connect_ok(const unsigned long tunnel_id);
	virtual void tunnelendpoint_send_remote_recieve_connect_error(const unsigned long tunnel_id);

	/*! \brief Create instance
	 */
	static APtr create(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
			boost::asio::io_service& io,
			const unsigned long id);

protected:

	/*! \brief Constructor
	 */
	TunnelendpointTCPTunnelServerWithConnectionEngine(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
			boost::asio::io_service& io,
			const unsigned long id);

   /*! \brief helper function that do the actual close
	*/
   virtual void close_down_(const bool force);

   virtual void tunnelendpoint_recieve_connect(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_eof(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_close(const unsigned long tunnel_id);
   virtual void tunnelendpoint_recieve_closed(const unsigned long tunnel_id);

   TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* tcp_tunnel_eventhandler_with_connection_engine_;
};


/*! \brief Server side tunnelendpoint factory for handling TCP connections related to the same ip/port.
 *
 * This server-side factory creates tunnelendpoint for each tcp-connection using a rawtunnelendpoint.
 *
 */
class TunnelendpointTCPTunnelClient: public TunnelendpointReliableCryptedTunnel,
        public TunnelendpointEventhandler,
        public RawTunnelendpointAcceptorTCPEventhandler,
        public AsyncContinuePolicy,
        public TunnelendpointTCPControlEventhandler{

public:
    typedef boost::shared_ptr<TunnelendpointTCPTunnelClient> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointTCPTunnelClient(void);

    /*! \brief set tcp option result_address
     */
    void set_option_reuse_address(const bool& option_reuse_address);

    /*! \brief Set event handler
     */
    void set_tcp_eventhandler(TunnelendpointTCPTunnelClientEventhandler* tcp_eventhandler);
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /*! \brief Reset event handler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief Called by parent/owner of tunnelendpoint
     */
    virtual void connect(void);

    /*! \brief Close tunnelendpoint and all its children connections
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes already initiated might be canceled.
     */
    virtual void close(const bool force);

    /*! \brief Return true if the tunnelendpoint is closed
     */
    bool is_closed();

    /*! \brief Star the accepting connections
     */
    void accept_start(void);

    /*! \brief Return local endpoint info of acceptor
     */
    std::pair<std::string, unsigned long> get_ip_local(void);

    /*! \brief Connected signal
     *
     *  Implementation of RawTunnelendpointAcceptorTCPEventhandler signal handling
     */
    virtual void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint);

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void com_tunnelendpoint_acceptor_error(const std::string& error_message);

    /*! \brief closed
     *
     *  The acceptor send this signal when everything is closed (as a response to aio_accept_stop).
     */
    virtual void com_tunnelendpoint_acceptor_closed(void);


    /*! \brief Signals that the tunnelendpoint has recieved a message

     *  Implementation of TunnelendpointEventhandler signal handling
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);

    /*! \brief Signals that the tunnelendpoint has been connected to a remote tunnelendpoint
     */
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Connected acceptting new connections
     *
     * Implementation of AsyncContinuePolicy
     */
    virtual bool com_accept_continue(void);

    /*! \brief Sends a message
     */
    virtual void tunnelendpoint_tcp_send(const Utility::DataBufferManaged::APtr& message);

    /*! \brief Implementation of TunnelendpointTCPControlEventhandler interface
     */
    virtual void tunnelendpoint_tcp_eh_eof(const unsigned long tunnel_id);
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long tunnel_id);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);


    /*! \brief Enable use of special checkpoint tunnel that logs directly to the server
     *
     */
    Utility::CheckpointHandler::APtr enable_checkpoint_tunnel(const Utility::CheckpointFilter::APtr& checkpoint_filter, const Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler);


    /*! \brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const std::string& server_ip,
                       const unsigned long server_port,
                       const unsigned long id);

protected:

    /*! \brief Constructor
     */
    TunnelendpointTCPTunnelClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                  boost::asio::io_service& io,
                                  const std::string& listen_ip,
                                  const unsigned long listen_port,
                                  const unsigned long id);

    /*! \brief helper function that do the actual close
     */
    void close_down_(const bool force);

    void goto_ready_state_(void);
    void goto_closed_state_(void);

    /*! \brief Generates a new tunnel id
     */
    unsigned long get_new_tunnel_id();
    bool room_for_new_tunnel_id();
  void free_tunnel_id(const unsigned long& tunnel_id);

    void tunnelendpoint_recieve_connect_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc);
    void tunnelendpoint_recieve_eof(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_close(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_closed(const unsigned long tunnel_id);
    void tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message);
    void tunnelendpoint_recieve_checkpoint_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc);

    TunnelendpointTCPTunnelClientEventhandler* tcp_tunnel_eventhandler_;

    enum StateTCP {
        StateTCP_Initializing,
        StateTCP_InitCheckpointTunnel,
        StateTCP_Ready,
        StateTCP_Closing,
        StateTCP_Closing_no_connection,
        StateTCP_Closed,
        StateTCP_Unknown
    };
    StateTCP state_tcp_;
    bool close_state_all_connection_closed;
    bool close_state_accept_closed;

    RawTunnelendpointAcceptorTCP::APtr raw_acceptor_;

    std::vector<unsigned long> tunnel_id_free_;
    std::set<unsigned long> tunnel_id_used_waiting_to_close_on_client_;

    std::map<unsigned long, TunnelendpointTCP::APtr> tcp_tunnelendpoints_;

    bool accept_interupted_;
    long unsigned id_;

    bool checkpoint_tunnel_enabled_;
    TunnelendpointCheckpointHandlerClient::APtr checkpoint_tunnel_;
    unsigned long checkpoint_tunnel_id_;
};

}

}
#endif
