/*! \file COM_SessionManagerConnection.cxx
 \brief This file contains the impplementation for the COM_SessionManagerConnection classes
 */

#include <component/communication/COM_SessionManagerConnection.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * SessionManagerConnection implementation
 * ------------------------------------------------------------------
 */
SessionManagerConnection::SessionManagerConnection(boost::asio::io_service& asio_io_service,
                                                   const unsigned long& id,
                                                   const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                   const Utility::DataBufferManaged::APtr& known_secret,
                                                   const CryptFacility::KeyStore::APtr& key_store,
                                                   const std::string& title,
                                                   const std::string& host,
                                                   const unsigned long& port,
                                                   const boost::posix_time::time_duration& timeout,
                                                   const Type& type) :
    asio_io_service_(asio_io_service), state_(State_not_connected), eventhandler_(NULL), id_(id),
            checkpoint_handler_(checkpoint_handler), known_secret_(known_secret), key_store_(key_store), title_(title), host_(host),
            port_(port), timeout_(timeout), type_(type), connect_timeout_timer_(asio_io_service),
            mutex_(Mutex::create(asio_io_service, "SessionManagerConnection")) {
}

SessionManagerConnection::~SessionManagerConnection(void) {
    state_ = State_unknown;
}

SessionManagerConnection::APtr SessionManagerConnection::create(boost::asio::io_service& asio_io_service,
                                                                const unsigned long& id,
                                                                const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                const Utility::DataBufferManaged::APtr& known_secret,
                                                                const CryptFacility::KeyStore::APtr& key_store,
                                                                const std::string& title,
                                                                const std::string& host,
                                                                const unsigned long& port,
                                                                const boost::posix_time::time_duration& timeout,
                                                                const Type& type) {
    return SessionManagerConnection::APtr(new SessionManagerConnection(asio_io_service, id, checkpoint_handler,
                                                                       known_secret, key_store, title, host, port, timeout, type));
}

SessionManagerConnection::APtr SessionManagerConnection::create(boost::asio::io_service& asio_io_service,
                                                                const unsigned long& id,
                                                                const Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                const Utility::DataBufferManaged::APtr& known_secret,
                                                                const CryptFacility::KeyStore::APtr& key_store,
                                                                const ServersSpec::ServersConnection& servers_connection) {

    SessionManagerConnection::Type connection_type(SessionManagerConnection::Type_direct);
    if (servers_connection.type == ServersSpec::ServersConnection::ConnectionType_http) {
        connection_type = SessionManagerConnection::Type_http;
    }
    return SessionManagerConnection::APtr(new SessionManagerConnection(asio_io_service, id, checkpoint_handler,
                                                                       known_secret,
                                                                       key_store,
                                                                       servers_connection.title,
                                                                       servers_connection.host,
                                                                       servers_connection.port,
                                                                       servers_connection.timeout, connection_type));
}

void SessionManagerConnection::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::connect");
    switch (state_) {
    case State_not_connected:
        break;
    case State_unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerConnection state not ready for connect");
    }
    state_ = State_connecting;

    if (eventhandler_ != NULL && type_ != Type_direct) {
        string resolve_type;
        if (type_ == Type_http) {
            resolve_type = "http";
        }
        eventhandler_->session_manager_connection_resolve_connection(resolve_type, host_, port_, timeout_);
    }
    raw_tunnelendpoint_connector_ = RawTunnelendpointConnectorTCP::create(checkpoint_handler_, asio_io_service_, host_, port_, this);
    raw_tunnelendpoint_connector_->aio_connect_start();

    connect_timeout_timer_.expires_after(boost::chrono::seconds(timeout_.total_seconds()));
    connect_timeout_timer_.async_wait(boost::bind(&SessionManagerConnection::connect_timeout, this, boost::asio::placeholders::error));
}

void SessionManagerConnection::connect_timeout(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::connect_timeout");
	try {
		if (error) {
			return;
		}

		switch (state_) {
		case State_connecting:
		case State_tcp_connect:
			break;
		default:
			return;
		}

	    Checkpoint cp(*checkpoint_handler_, "SessionManagerConnection::timeout", Attr_Communication(), CpAttr_debug());

	    state_ = State_timeout;
	    raw_tunnelendpoint_connector_->close();

	    mutex_->get_strand().post(boost::bind(&SessionManagerConnection::call_session_manager_connection_failed_decoubled, this));
    }
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerConnection::timeout.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    }
}

void SessionManagerConnection::call_session_manager_connection_failed_decoubled(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::com_tunnelendpoint_connected");
	try {
		if (eventhandler_ != NULL) {
			eventhandler_->session_manager_connection_failed(id_);
		}
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"SessionManagerConnection::call_session_manager_connection_failed_decoubled.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}


void SessionManagerConnection::com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::com_tunnelendpoint_connected");
    switch (state_) {
    case State_connecting:
    case State_timeout:
        break;
    case State_unknown:
        assert(false);
    default:
        throw ExceptionUnexpected(
                                  "SessionManagerConnection state not ready for com_tunnelendpoint_connection_failed_to_connect");
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerConnection::com_tunnelendpoint_connected", Attr_Communication(), CpAttr_debug());
    if (state_ == State_timeout) {
        tunnelendpoint->aio_close_start(true);
        return;
    }
    state_ = State_tcp_connect;

    session_crypter_ = SessionCrypterClient::create(checkpoint_handler_, tunnelendpoint, known_secret_, key_store_);
    session_crypter_->set_eventhandler(this);
    session_crypter_->do_key_exchange();
}

void SessionManagerConnection::com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::com_tunnelendpoint_connection_failed_to_connect");
    switch (state_) {
    case State_connecting:
    case State_timeout:
        break;
    case State_unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerConnection state not ready for com_tunnelendpoint_connection_failed_to_connect");
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerConnection::com_tunnelendpoint_connection_failed_to_connect",
                  Attr_Communication(), CpAttr_debug(), CpAttr("message", message));

    if (state_ == State_timeout) {
        return;
    }
    // Force expire of timeout
    connect_timeout_timer_.expires_after(boost::chrono::seconds(1));

    state_ = State_tcp_failed_to_connect;
    raw_tunnelendpoint_connector_->close();

    if (eventhandler_ != NULL) {
        eventhandler_->session_manager_connection_failed(id_);
    }
}

void SessionManagerConnection::session_crypter_key_exchange_complete(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::session_crypter_key_exchange_complete");
    switch (state_) {
    case State_timeout:
    case State_tcp_connect:
        break;
    case State_unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerConnection state not ready for session_crypter_key_exchange_complete");
    }
    if (state_ == State_timeout) {
        session_crypter_->reset_eventhandler();
        session_crypter_->close(true);
        return;
    }
    state_ = State_secure_connect;
    if (eventhandler_ != NULL) {
        session_crypter_->reset_eventhandler();
        eventhandler_->session_manager_connection_connected(id_, session_crypter_);
    }
}

void SessionManagerConnection::session_crypter_key_exchange_failed(const std::string& error_message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::session_crypter_key_exchange_failed");
    switch (state_) {
    case State_timeout:
    case State_tcp_connect:
        break;
    case State_unknown:
        assert(false);
    default:
        throw ExceptionUnexpected("SessionManagerConnection state not ready for session_crypter_key_exchange_failed");
    }
    Checkpoint cp(*checkpoint_handler_, "SessionManagerConnection::session_crypter_key_exchange_failed", Attr_Communication(), CpAttr_warning(), CpAttr("message", error_message));
    if (state_ == State_timeout) {
        session_crypter_->reset_eventhandler();
        session_crypter_->close(true);
        return;
    }
    state_ = State_secure_failed_to_connect;
    raw_tunnelendpoint_connector_->close();
    if (eventhandler_ != NULL) {
        eventhandler_->session_manager_connection_failed(id_);
    }
}

void SessionManagerConnection::session_crypter_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::session_crypter_closed");
    switch (state_) {
    case State_tcp_connect:
        break;
    default:
        return;
    }
    state_ = State_secure_failed_to_connect;
    raw_tunnelendpoint_connector_->close();

    if (eventhandler_ != NULL) {
        eventhandler_->session_manager_connection_failed(id_);
    }
}

void SessionManagerConnection::set_eventhandler(SessionManagerConnectionEventhandler* eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::set_eventhandler");
    eventhandler_ = eventhandler;

}
void SessionManagerConnection::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::set_eventhandler");
    eventhandler_ = NULL;
}

SessionManagerConnection::State SessionManagerConnection::get_state(void) const {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::get_state");
    return state_;
}

std::string SessionManagerConnection::get_title(void) const {
    return title_;
}

unsigned long SessionManagerConnection::get_id(void) const {
    return id_;
}

bool SessionManagerConnection::is_pending(void) const {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "SessionManagerConnection::is_pending");
    switch (state_) {
    case State_not_connected:
    case State_connecting:
    case State_tcp_connect:
        break;
    default:
        return false;
    }
    return true;
}
