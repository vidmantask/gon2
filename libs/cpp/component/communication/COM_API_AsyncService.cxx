/*! \file COM_API_AsyncService.cxx
 *  \brief This file contains the implementation of the API asyncservice class
 */
// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <functional>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/pyapi/PYAPI_Utility.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APIMutex implementation
 * ------------------------------------------------------------------
 */
APIMutex::APIMutex(const Utility::Mutex::APtr& mutex) : mutex_(mutex) {

}

APIMutex::~APIMutex(void) {
}

APIMutex::APtr APIMutex::create(const Utility::Mutex::APtr& mutex) {
    return APIMutex::APtr(new APIMutex(mutex));
}


Utility::Mutex::APtr APIMutex::get_impl(void) {
    return  mutex_;
}

bool APIMutex::lock(void) {
	return mutex_->lock();
}

bool APIMutex::lock(const unsigned long& seconds) {
	return mutex_->lock(seconds);
}

void APIMutex::unlock(void) {
	mutex_->unlock();
}

bool APIMutex::self_lock(const APIMutex::APtr& self) {
	return self->lock();
}

bool APIMutex::self_lock_with_timeout(const APIMutex::APtr& self, const unsigned long& seconds) {
	return self->lock(seconds);
}

void APIMutex::self_unlock(const APIMutex::APtr& self) {
	self->unlock();
}



/*
 *  Class that ensure that Py_DECREF is called when this class is going out of scope
 */
class Py_DECREFHelper {
public:
	Py_DECREFHelper(PyObject* callback_self)
	: callback_self_(callback_self) {
	}
	~Py_DECREFHelper(void) {
	    Py_DECREF(callback_self_);
	}
private:
	PyObject* callback_self_;
};


/*
 * ------------------------------------------------------------------
 * APIAsyncServiceSleeper implementation
 * ------------------------------------------------------------------
 */
APIAsyncServiceSleeper::APIAsyncServiceSleeper(boost::asio::io_service& io_service,
                                               const APICheckpointHandler::APtr& api_checkpoint_handler,
                                               const Utility::Mutex::APtr& mutex,
                                               PyObject* callback_self,
                                               const std::string& callback_method_name,
                                               const boost::python::object& callback_arg) :
    mutex_(mutex), timer_(io_service), timer_strand_(io_service), api_checkpoint_handler_(api_checkpoint_handler), done_(false), callback_self_(callback_self), callback_method_name_(callback_method_name), callback_arg_(callback_arg) {
    Py_INCREF(callback_self_);
}

APIAsyncServiceSleeper::~APIAsyncServiceSleeper(void) {
}

APIAsyncServiceSleeper::APtr APIAsyncServiceSleeper::create(boost::asio::io_service& io_service,
                                                            const APICheckpointHandler::APtr& api_checkpoint_handler,
		                                                    const Utility::Mutex::APtr& mutex,
                                                            PyObject* callback_self,
                                                            const std::string& callback_method_name,
                                                            const boost::python::object& callback_arg) {
    return APIAsyncServiceSleeper::APtr(new APIAsyncServiceSleeper(io_service, api_checkpoint_handler, mutex, callback_self, callback_method_name, callback_arg));
}

void APIAsyncServiceSleeper::sleep_start(const unsigned long& min, const unsigned long& sec, const unsigned long& ms) {
    timer_.expires_after(boost::chrono::minutes(min) + boost::chrono::seconds(sec) + boost::chrono::milliseconds(ms));
    timer_.async_wait(mutex_->get_strand().wrap(boost::bind(&APIAsyncServiceSleeper::sleep_, this, boost::asio::placeholders::error)));
}

void APIAsyncServiceSleeper::sleep_(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(api_checkpoint_handler_->get_checkpoint_handler(), Attr_Communication(), mutex_, "APIAsyncServiceSleeper::sleep_(" + callback_method_name_ + ")");
    Giritech::Communication::PYAPI::ThreadLock python_thread_lock(callback_method_name_.c_str(), api_checkpoint_handler_);
    {
		Py_DECREFHelper py_decref_helper(callback_self_);
		if (!error) {
			try {
				if(callback_arg_.ptr() == Py_None) {
					boost::python::call_method<void>(callback_self_, callback_method_name_.c_str());
				}
				else {
					boost::python::call_method<void>(callback_self_, callback_method_name_.c_str(), callback_arg_);
				}
			} catch (boost::python::error_already_set) {
				PyErr_Print();
				PyErr_Clear();
			}
		}
    }
    done_ = true;
}

bool APIAsyncServiceSleeper::is_done(void) const {
    return done_;
}

/*
 * ------------------------------------------------------------------
 * APIAsyncService implementation
 * ------------------------------------------------------------------
 */
APIAsyncService::APIAsyncService(const AsyncService::APtr& async_service, const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler) :
    async_service_(async_service), update_mutex_(Mutex::create(async_service->get_io_service(), "APIAsyncService.Update")), api_checkpoint_handler_(api_checkpoint_handler) {
}

APIAsyncService::~APIAsyncService(void) {
}

void APIAsyncService::sleep_start(const APICheckpointHandler::APtr& api_checkpoint_handler,
		                          const Utility::Mutex::APtr& mutex,
                                  const unsigned long& min,
                                  const unsigned long& sec,
                                  const unsigned long& ms,
                                  PyObject* callback_self,
                                  const std::string& callback_method_name,
                                  const boost::python::object& callback_arg) {
    APIAsyncServiceSleeper::APtr sleeper(APIAsyncServiceSleeper::create(get_io_service(), api_checkpoint_handler, mutex, callback_self, callback_method_name, callback_arg));
    sleeper->sleep_start(min, sec, ms);

    {
        MutexScopeLock session_mutex_lock_(api_checkpoint_handler->get_checkpoint_handler(), Attr_Communication(), update_mutex_, "APIAsyncService::sleep_start");
        sleepers_.push_back(sleeper);

    /*
     * Clean up woken sleepers
     * sleepers_ = remove_if(sleepers_.begin(), sleepers_.end(), boost::bind(&APIAsyncServiceSleeper::is_done, _1));
     */
        std::vector<APIAsyncServiceSleeper::APtr>::iterator i(sleepers_.begin());
        while(i != sleepers_.end()) {
            if((*i)->is_done() ) {
                i = sleepers_.erase(i);
            }
            else {
                i = i + 1;
            }
        }
    }
}

void APIAsyncService::run(void) {
	Py_BEGIN_ALLOW_THREADS
	try {
		async_service_->run();
	}
	catch(std::exception& e) {
		if(api_checkpoint_handler_ != NULL) {
			Checkpoint cp(*api_checkpoint_handler_->get_checkpoint_handler(), "APIAsyncService::run.thread_crash", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
		}
	}
	catch(...) {
		if(api_checkpoint_handler_ != NULL) {
			Checkpoint cp(*api_checkpoint_handler_->get_checkpoint_handler(), "APIAsyncService::run.thread_crash.unknown_exception", Attr_Communication(), CpAttr_critical());
		}
	}
	Py_END_ALLOW_THREADS
}


void APIAsyncService::stop(void) {
    async_service_->stop();
}

void APIAsyncService::reset(void) {
    async_service_->reset();
}

AsyncService::APtr APIAsyncService::get_impl(void) {
    return async_service_;
}

boost::asio::io_service& APIAsyncService::get_io_service(void) {
    return async_service_->get_io_service();
}

void APIAsyncService::memory_guard_cleanup(void) {
    async_service_->memory_guard_cleanup();
}

bool APIAsyncService::memory_guard_is_empty(void) {
    return async_service_->memory_guard_is_empty();
}

APIAsyncService::APtr APIAsyncService::create_1(void) {
	APICheckpointHandler::APtr api_checkpoint_handler = APICheckpointHandler::create_with_handler(CheckpointHandler::create_cout_all());
	return APIAsyncService::APtr(new APIAsyncService(AsyncService::create(api_checkpoint_handler->get_checkpoint_handler()), api_checkpoint_handler));
}

APIAsyncService::APtr APIAsyncService::create_2(const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler) {
    return APIAsyncService::APtr(new APIAsyncService(AsyncService::create(api_checkpoint_handler->get_checkpoint_handler()), api_checkpoint_handler));
}

APIAsyncService::APtr APIAsyncService::create(const AsyncService::APtr& async_service) {
	APICheckpointHandler::APtr api_checkpoint_handler = APICheckpointHandler::create_with_handler(CheckpointHandler::create_cout_all());
    return APIAsyncService::APtr(new APIAsyncService(async_service, api_checkpoint_handler));
}

Utility::Mutex::APtr APIAsyncService::create_mutex(const std::string& mutex_id) {
	Mutex::APtr mutex(Mutex::create(async_service_->get_io_service(), mutex_id));
	mutexes_.push_back(mutex);
	return mutex;
}

APIMutex::APtr APIAsyncService::self_create_mutex(const APIAsyncService::APtr& self, const std::string& mutex_id) {
    assert(self.get() != NULL);
	return APIMutex::create(self->create_mutex(mutex_id));
}

void APIAsyncService::self_run(const APIAsyncService::APtr& self) {
    assert(self.get() != NULL);
    self->run();
}

void APIAsyncService::self_reset(const APIAsyncService::APtr& self) {
    assert(self.get() != NULL);
    self->reset();
}

void APIAsyncService::self_stop(const APIAsyncService::APtr& self) {
    assert(self.get() != NULL);
    self->stop();
}

void APIAsyncService::self_sleep_start(const APIAsyncService::APtr& self,
                                       const APICheckpointHandler::APtr& api_checkpoint_handler,
                                       const unsigned long& min,
                                       const unsigned long& sec,
                                       const unsigned long& ms,
                                       PyObject* callback_self,
                                       const std::string& callback_method_name,
                                       const boost::python::object& callback_arg) {
    assert(self.get() != NULL);
    self->sleep_start(api_checkpoint_handler, Utility::Mutex::create(self->get_io_service(), "APIAsyncService"), min, sec, ms, callback_self, callback_method_name, callback_arg);
}

void APIAsyncService::self_sleep_start_mutex(const APIAsyncService::APtr& self,
                                       const APICheckpointHandler::APtr& api_checkpoint_handler,
                                       const APIMutex::APtr& mutex,
                                       const unsigned long& min,
                                       const unsigned long& sec,
                                       const unsigned long& ms,
                                       PyObject* callback_self,
                                       const std::string& callback_method_name,
                                       const boost::python::object& callback_arg) {
    assert(self.get() != NULL);
    self->sleep_start(api_checkpoint_handler, mutex->get_impl(), min, sec, ms, callback_self, callback_method_name, callback_arg);
}

void APIAsyncService::self_memory_guard_cleanup(const APIAsyncService::APtr& self) {
    assert(self.get() != NULL);
    self->memory_guard_cleanup();
}

bool APIAsyncService::self_memory_guard_is_empty(const APIAsyncService::APtr& self) {
    assert(self.get() != NULL);
    return self->memory_guard_is_empty();
}
