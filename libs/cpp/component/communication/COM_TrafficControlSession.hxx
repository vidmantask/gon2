/*! \file COM_TrafficControlSession.hxx
 *  \brief This file contains TrafficControlSession class
 */
#ifndef COM_TrafficControlSession_HXX
#define COM_TrafficControlSession_HXX

#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_TrafficControlSessionEventhandler.hxx>


namespace Giritech {
namespace Communication {


/*
 * Default Values
 */
const unsigned long DEFAULT_READ_SIZE_MAX          =   50000;

const unsigned long DEFAULT_READ_SIZE_SEC_DEFAULT  =   125000;  // 1 mbit/sec
const unsigned long DEFAULT_READ_SIZE_SEC_MIN      =     5000;
const unsigned long DEFAULT_READ_SIZE_SEC_MAX      = 12500000; // 100 mbit

const unsigned long BUFFER_TRANSIT_LIMIT_MS        = 300;
const unsigned long BUFFER_TRANSIT_OK_LIMIT_MS     = 200;
const unsigned long DEFAULT_READ_BUCKETS_COUNT_SEC = 100;

const unsigned long DEFAULT_READ_DELAY_MS = 1000 / DEFAULT_READ_BUCKETS_COUNT_SEC;
const unsigned long DEFAULT_READ_BUCKET_SIZE = DEFAULT_READ_SIZE_SEC_DEFAULT / DEFAULT_READ_BUCKETS_COUNT_SEC;


enum BandwidthThrottlingState {
	BandwidthThrottlingState_Neutral = 0,
	BandwidthThrottlingState_Increasing,
	BandwidthThrottlingState_Decreasing
};


class TrafficControlSessionCallBack : public boost::noncopyable {
public:
	virtual void in_buffer_transit_ok(const unsigned long& id) = 0;
	virtual void in_buffer_transit_exceded(const unsigned long& id) = 0;
	virtual void out_buffer_transit_ok(const unsigned long& id) = 0;
	virtual void out_buffer_transit_exceded(const unsigned long& id) = 0;
};




/*! \brief Traffic Control Session Point
 *
 */
class TrafficControlSessionPoint : public boost::noncopyable {
public:
    typedef boost::shared_ptr<TrafficControlSessionPoint> APtr;
    ~TrafficControlSessionPoint(void);

	enum ConnectionType {
		ConnectionType_Unknown = 0,
		ConnectionType_GConnection = 1,
		ConnectionType_AConnection = 2
    };

	void report_read_begin(void);
	void report_read_end(const unsigned long& size);
	void report_pop_buffer_size(const unsigned long& size);
	void report_push_buffer_size(const unsigned long& size);


	unsigned long get_read_delay_ms(void) const;
	unsigned long get_read_size(void) const;
	unsigned long get_write_delay_ms(void) const;

	void interval_tick(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);
	void add_to_session(
			unsigned long& interval_in_read_size,
			unsigned long& interval_in_buffer_size,
			unsigned long& interval_in_write_size,
			unsigned long& interval_in_buffer_transit_min_ms,
			unsigned long& interval_in_buffer_transit_max_ms,
			unsigned long& interval_out_read_size,
			unsigned long& interval_out_buffer_size,
			unsigned long& interval_out_write_size,
			unsigned long& interval_out_buffer_transit_min_ms,
			unsigned long& interval_out_buffer_transit_max_ms
	);

	void add_future_to_session(
			unsigned long& future_in_interval_read_size_sec,
			unsigned long& future_in_interval_read_bucket_size,
			unsigned long& future_in_interval_read_bucket_count,
			unsigned long& future_in_interval_read_delay_ms,
			unsigned long& future_out_interval_read_size_sec,
			unsigned long& future_out_interval_read_bucket_size,
			unsigned long& future_out_interval_read_bucket_count,
			unsigned long& future_out_interval_read_delay_ms);


	void apply_future_restriction(const BandwidthThrottlingState bandwidth_throttling_state);

	bool is_limiting(unsigned long& future_interval_read_delay_ms, unsigned long& future_interval_read_size) const;

	ConnectionType getConnectionType(void) const;

    /*! \brief Create instance
     */
    static APtr create(TrafficControlSessionCallBack* traffic_control_session_callback, const unsigned long& id, const ConnectionType& connection_type);

protected:
    TrafficControlSessionPoint(TrafficControlSessionCallBack* traffic_control_session_callback, const unsigned long& id, const ConnectionType& connection_type);

	void calculate_buckets(const unsigned long& future_interval_read_size_sec);

	unsigned long get_default_read_bucket_count_sec(const unsigned long& future_interval_read_delay_ms);
	unsigned long get_default_read_bucket_count_sec_threshold(const unsigned long& future_interval_read_delay_ms);
	unsigned long get_default_read_delay_ms(const unsigned long& future_interval_read_delay_ms);

    TrafficControlSessionCallBack* traffic_control_session_callback_;
    unsigned long id_;
    ConnectionType connection_type_;

    unsigned long current_interval_read_count_;
    unsigned long current_interval_read_size_;
    unsigned long current_interval_write_count_;
    unsigned long current_interval_write_size_;
    unsigned long current_interval_buffer_size_;
    boost::posix_time::time_duration current_interval_write_durration_;
    boost::posix_time::time_duration current_interval_computation_durration_;
    unsigned long current_interval_buffer_transit_max_ms_;
    unsigned long current_interval_buffer_transit_min_ms_;


    unsigned long history_interval_read_count_;
    unsigned long history_interval_read_size_;
    unsigned long history_interval_write_size_;
    unsigned long history_interval_write_count_;
    unsigned long history_interval_buffer_size_;
    unsigned long history_interval_write_durration_bucket_ms_;
    unsigned long history_interval_read_computation_delay_ms_;

    unsigned long history_interval_buffer_transit_min_ms_;
    unsigned long history_interval_buffer_transit_max_ms_;

    unsigned long future_read_size_sec_;

    unsigned long future_read_bucket_count_;
    unsigned long future_read_delay_ms_;
    unsigned long future_read_bucket_size_;

    std::vector<boost::posix_time::ptime> current_buffer_ts_;
    std::vector<unsigned long> current_buffer_size_;
    unsigned long current_buffer_size_total_;



};



/*! \brief Traffic Control Session
 *
 */

class TrafficControlSession : public Giritech::Utility::AsyncMemGuardElement, public TrafficControlSessionCallBack {
public:
    typedef boost::shared_ptr<TrafficControlSession> APtr;
    ~TrafficControlSession(void);

	unsigned long add_endpoint(const TrafficControlSessionPoint::ConnectionType& connection_type);
	void remove_endpoint(const unsigned long& id);

	unsigned long get_read_delay_ms(const unsigned long& id) const;
	unsigned long get_read_size(const unsigned long& id) const;
	unsigned long get_write_delay_ms(const unsigned long& id) const;

	void report_read_begin(const unsigned long& id);
	void report_read_end(const unsigned long& id, const unsigned long& size);
	void report_pop_buffer_size(const unsigned long& id, const unsigned long& size);
	void report_push_buffer_size(const unsigned long& id, const unsigned long& size);

	void interval_tick_stop(void);

	bool is_active(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    void set_eventhandler(TrafficControlSessionEventhandler* eventhandler);
    void reset_eventhandler(void);

    void recieve_message(const MessageCom::APtr&);

    void interval_tick_start(void);

    bool is_connected(void);


	void in_apply_future_restriction(void);
	void out_apply_future_restriction(void);

	void set_traffic_control_enabled(const bool value);

    /*
     * Implementation of TrafficControlSessionCallBack
     */
    void in_buffer_transit_ok(const unsigned long& id);
	void in_buffer_transit_exceded(const unsigned long& id);
    void out_buffer_transit_ok(const unsigned long& id);
	void out_buffer_transit_exceded(const unsigned long& id);



    /*! \brief Create instance
     */
    static APtr create(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler_details_, const std::string& session_id);

protected:
    TrafficControlSession(boost::asio::io_service& asio_io_service, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler_details_, const std::string& session_id);

	void interval_tick(void);

    boost::asio::io_service& asio_io_service_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_details_;

    Utility::Mutex::APtr mutex_;
    boost::asio::steady_timer interval_timer_;

    std::string session_id_;
    unsigned long next_id_;

    typedef std::map<unsigned long, TrafficControlSessionPoint::APtr> Points;
    Points points_;

    enum State {
    	State_Running = 0,
    	State_Stopping,
    	State_Done
    };
    State state_;
    TrafficControlSessionEventhandler* traffic_control_session_eventhandler_;

    BandwidthThrottlingState in_bandwidth_throttling_state_;
    BandwidthThrottlingState out_bandwidth_throttling_state_;

    boost::posix_time::ptime in_last_buffer_transit_exceded_;
    boost::posix_time::ptime out_last_buffer_transit_exceded_;

    bool traffic_control_enabled_;
};

}
}
#endif
