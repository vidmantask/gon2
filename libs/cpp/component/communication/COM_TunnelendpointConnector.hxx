/*! \file COM_TunnelendpointConnector.hxx
 \brief This file contains the tunnelendpoint conector clases
 */
#ifndef COM_TunnelendpointConnector_HXX
#define COM_TunnelendpointConnector_HXX

#include <boost/shared_ptr.hpp>

namespace Giritech {
namespace Communication {

/*! \brief Abstract session endpoint connector
 *
 *  */
class TunnelendpointConnector: public boost::noncopyable {
public:

    /*! \brief Sends a message
     */
    virtual void tunnelendpoint_connector_send(const MessageCom::APtr&) = 0;

    /*! \brief User signal notification to parent from child
     */
    virtual void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) = 0;

    /*! \brief Indicate that the tunnelendpoint has been closed
     */
    virtual void tunnelendpoint_connector_closed(void) = 0;

    /*! \brief Ask for unique session id
     */
    virtual std::string tunnelendpoint_connector_get_unique_session_id(void) = 0;

};

}
}
#endif
