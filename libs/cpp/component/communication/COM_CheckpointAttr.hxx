/*! \file COM_CheckpointAttr.hxx
 \brief This file contains the global singelton for the module attribute communication
 */
#ifndef COM_CHECKPOINTATTR_HXX
#define COM_CHECKPOINTATTR_HXX

#include <lib/utility/UY_Checkpoint.hxx>

namespace Giritech {
namespace Communication {

Utility::CheckpointAttr_Module::APtr Attr_Communication(void);
Utility::CheckpointAttr_Module::APtr Attr_CommunicationTC(void);

}
}
#endif
