/*! \file COM_AsyncService.hxx
 *  \brief This file contains classes for the asyncron service
 */
#ifndef COM_AsyncService_HXX
#define COM_AsyncService_HXX

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <vector>

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>


namespace Giritech {
namespace Communication {


/*! \brief Async service
 */
class AsyncService {
public:
    typedef boost::shared_ptr<AsyncService> APtr;

    /*! \brief Destructor
     */
    ~AsyncService(void);

    /*
     * Return asyncron io_service
     */
    boost::asio::io_service& get_io_service(void);

    /*
     * Run the async service
     */
    void run(void);

    /*
     * Reset async service
     */
    void reset(void);

    /*
     * Run the async service, and stop when no more work
     */
    unsigned long poll(void);

    /*
     * Stop the async service
     */
    void stop(void);

    /*
     * Methods for memory guard
     */
    void memory_guard_cleanup(void);
    bool memory_guard_is_empty(void);

    /*!\brief Create instance
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checpoint_handler);

private:
    AsyncService(const Giritech::Utility::CheckpointHandler::APtr& checpoint_handler);

    boost::asio::io_service io_service_;
    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
};

}
}
#endif
