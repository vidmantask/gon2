/*! \file UTest_COM_Tunnelendpoint.cxx
 \brief This file contains unittest suite for tunnelendpoint abstraction
 */
#include <vector>
#include <string>
#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>
#include <lib/version/Version.hxx>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_Tunnelendpoint.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Version;
using namespace Giritech::Communication;



class MyTunnelendpointEventhandler : public TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<MyTunnelendpointEventhandler> APtr;

    MyTunnelendpointEventhandler(void) :
        recived_messages_(0), connected_(false) {
    	last_message_ = MessagePayload::create(DataBufferManaged::create(""));

    }
    void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
        ++recived_messages_;
        last_message_ = message;
        cout << "RECV" << recived_messages_ << ":" << message->get_buffer()->toString() << endl;
    }

    void tunnelendpoint_eh_connected(void) {
        cout << "connected" << endl;
        connected_ = true;
    }

    void wait_for_connect(void) {
        while(true) {
        	if(connected_) {
        		break;
        	}
        }
    }

    unsigned long recived_messages_;
    MessagePayload::APtr last_message_;
    bool connected_;

    static APtr create(void) {
        return APtr(new MyTunnelendpointEventhandler);
    }
};

class MyTunnelendpointConnector : public TunnelendpointConnector {
public:
    MyTunnelendpointConnector(void) :
        is_buffer_used(false), send_messages_(0), tunnelendpoint_(NULL) {

    }
    void tunnelendpoint_connector_send(const MessageCom::APtr& message) {
        if (message->get_package_type() == MessageCom::PackageType_Data) {
            ++send_messages_;
        }
        last_message_ = message;

        bool expected = false;
        while (!is_buffer_used.compare_exchange_weak(expected, true));

        message_buffer_.push_back(message);
        is_buffer_used = false;
        do_send_message_buffer();
        return;
    }
    void tunnelendpoint_connector_closed(void) {
        // Ignored
    }

    void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
        // Ignored
    }

    std::string tunnelendpoint_connector_get_unique_session_id(void) {
    	return "hej";
    }

    void set_tunnelendpoint(Tunnelendpoint* tunnelendpoint) {
        tunnelendpoint_ = tunnelendpoint;
        do_send_message_buffer();
    }

    void do_send_message_buffer(void) {

        if (tunnelendpoint_ != NULL) {

            bool expected = false;
            while (!is_buffer_used.compare_exchange_weak(expected, true));

            for (const MessageCom::APtr& message : message_buffer_)
            {
                cout << send_messages_ << ":" << message->create_as_buffer()->encodeHex()->toString() << " size:" << message_buffer_.size() << endl;
                tunnelendpoint_->tunnelendpoint_recieve(message);
            }
            message_buffer_.clear();
            is_buffer_used = false;
        }
    }

    std::atomic<bool> is_buffer_used;

    unsigned long send_messages_;
    MessageCom::APtr last_message_;
    Tunnelendpoint* tunnelendpoint_;

    std::vector<MessageCom::APtr> message_buffer_;
};


void timeout(void) {
	cout << "timeout";
}

/*
 * Simpel One level tunnel
 */
BOOST_AUTO_TEST_CASE( tunnel_one_level )
{
    CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));

    boost::asio::deadline_timer keep_alive_ping_timer(async_service->get_io_service());
    keep_alive_ping_timer.expires_from_now(boost::posix_time::seconds(5));
    keep_alive_ping_timer.async_wait(boost::bind(&timeout));


    MyTunnelendpointConnector source_level_0_connector;
    MyTunnelendpointEventhandler source_level_0_eventhandler;
    TunnelendpointReliableCryptedTunnel::APtr source_level_0(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &source_level_0_eventhandler));
    source_level_0->set_connector(&source_level_0_connector);

    MyTunnelendpointEventhandler source_level_0a_eventhandler;
    MyTunnelendpointEventhandler source_level_0b_eventhandler;
    MyTunnelendpointEventhandler source_level_0c_eventhandler;
    TunnelendpointReliableCrypted::APtr source_level_0a(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &source_level_0a_eventhandler));
    TunnelendpointReliableCrypted::APtr source_level_0b(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &source_level_0b_eventhandler));
    TunnelendpointReliableCrypted::APtr source_level_0c(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &source_level_0c_eventhandler));
    source_level_0a->set_version(Version::create(1,2,3,4));
    source_level_0b->set_version(Version::create(5,6,7,8));

    source_level_0->add_tunnelendpoint(1, source_level_0a);
    source_level_0->add_tunnelendpoint(2, source_level_0b);
    source_level_0->add_tunnelendpoint(3, source_level_0c);


    MyTunnelendpointConnector dest_level_0_connector;
    MyTunnelendpointEventhandler dest_level_0_eventhandler;
    TunnelendpointReliableCryptedTunnel::APtr dest_level_0(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &dest_level_0_eventhandler));
    dest_level_0->set_connector(&dest_level_0_connector);

    MyTunnelendpointEventhandler dest_level_0a_eventhandler;
    MyTunnelendpointEventhandler dest_level_0b_eventhandler;
    MyTunnelendpointEventhandler dest_level_0c_eventhandler;
    TunnelendpointReliableCrypted::APtr dest_level_0a(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &dest_level_0a_eventhandler));
    TunnelendpointReliableCrypted::APtr dest_level_0b(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &dest_level_0b_eventhandler));
    TunnelendpointReliableCrypted::APtr dest_level_0c(TunnelendpointReliableCrypted::create(async_service->get_io_service(), checkpoint_handler, &dest_level_0c_eventhandler));
    dest_level_0->add_tunnelendpoint(1, dest_level_0a);
    dest_level_0->add_tunnelendpoint(2, dest_level_0b);
    dest_level_0->add_tunnelendpoint(3, dest_level_0c);

    source_level_0_connector.set_tunnelendpoint(dest_level_0.get());
    dest_level_0_connector.set_tunnelendpoint(source_level_0.get());

    source_level_0->connect();
    dest_level_0->connect();

    boost::thread_group threads;
    threads.create_thread(boost::bind(&AsyncService::run, async_service));

    source_level_0_eventhandler.wait_for_connect();
    dest_level_0_eventhandler.wait_for_connect();
    source_level_0a_eventhandler.wait_for_connect();
    dest_level_0a_eventhandler.wait_for_connect();
    source_level_0b_eventhandler.wait_for_connect();
    dest_level_0b_eventhandler.wait_for_connect();
    source_level_0c_eventhandler.wait_for_connect();
    dest_level_0c_eventhandler.wait_for_connect();



    MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("0123456789")));
    source_level_0->tunnelendpoint_send(payload);
    source_level_0a->tunnelendpoint_send(payload);
    source_level_0b->tunnelendpoint_send(payload);
    source_level_0c->tunnelendpoint_send(payload);
    threads.join_all();

    BOOST_CHECK( source_level_0_connector.send_messages_ == 4 );
    BOOST_CHECK( dest_level_0_eventhandler.recived_messages_ == 1 );
    BOOST_CHECK( *(dest_level_0_eventhandler.last_message_->get_buffer()) == *(payload->get_buffer()) );

    BOOST_CHECK( dest_level_0a_eventhandler.recived_messages_ == 1 );
    BOOST_CHECK( *(dest_level_0a_eventhandler.last_message_->get_buffer()) == *(payload->get_buffer()) );

    BOOST_CHECK( dest_level_0b_eventhandler.recived_messages_ == 1 );
    BOOST_CHECK( *(dest_level_0b_eventhandler.last_message_->get_buffer()) == *(payload->get_buffer()) );

    BOOST_CHECK( dest_level_0c_eventhandler.recived_messages_ == 1 );
    BOOST_CHECK( *(dest_level_0c_eventhandler.last_message_->get_buffer()) == *(payload->get_buffer()) );


    BOOST_CHECK( !dest_level_0a->get_version()->equal(dest_level_0a->get_version_remote()));
    BOOST_CHECK( !dest_level_0b->get_version()->equal(dest_level_0b->get_version_remote()));
    BOOST_CHECK( dest_level_0c->get_version()->equal(dest_level_0c->get_version_remote()));
    BOOST_CHECK( dest_level_0->get_version()->equal(dest_level_0->get_version_remote()));

//    source_level_0->close(true);
    cout << "end" << endl;

}

///*
// * Multilevel tunnels
// */
//
//void helper_add_tunnelendpoint(AsyncService::APtr& async_service,
//                               const CheckpointHandler::APtr& checkpoint_handler,
//                               vector<TunnelendpointReliableCryptedTunnel::APtr>& source_tunnels,
//                               vector<MyTunnelendpointEventhandler::APtr>& source_tunnel_eventhandlers,
//                               vector<TunnelendpointReliableCryptedTunnel::APtr>& dest_tunnels,
//                               vector<MyTunnelendpointEventhandler::APtr>& dest_tunnel_eventhandlers,
//                               const int parent,
//                               const unsigned long& child_id) {
//
//    source_tunnel_eventhandlers.push_back(MyTunnelendpointEventhandler::create());
//    source_tunnels.push_back(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &(*source_tunnel_eventhandlers.back())));
//
//    dest_tunnel_eventhandlers.push_back(MyTunnelendpointEventhandler::create());
//    dest_tunnels.push_back(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &(*dest_tunnel_eventhandlers.back())));
//
//    source_tunnels[parent]->add_tunnelendpoint(child_id, source_tunnels.back());
//    dest_tunnels[parent]->add_tunnelendpoint(child_id, dest_tunnels.back());
//}
//
//BOOST_AUTO_TEST_CASE( tunnel_multi_level )
//{
//    CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
//    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
//
//    vector<MyTunnelendpointEventhandler::APtr> source_tunnel_eventhandlers;
//    vector<TunnelendpointReliableCryptedTunnel::APtr> source_tunnels;
//    vector<MyTunnelendpointEventhandler::APtr> dest_tunnel_eventhandlers;
//    vector<TunnelendpointReliableCryptedTunnel::APtr> dest_tunnels;
//
//    MyTunnelendpointConnector source_level_0_connector;
//    source_tunnel_eventhandlers.push_back(MyTunnelendpointEventhandler::create());
//    source_tunnels.push_back(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &(*source_tunnel_eventhandlers[0])));
//    source_tunnels[0]->set_connector(&source_level_0_connector);
//
//    MyTunnelendpointConnector dest_level_0_connector;
//    dest_tunnel_eventhandlers.push_back(MyTunnelendpointEventhandler::create());
//    dest_tunnels.push_back(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, &(*dest_tunnel_eventhandlers[0])));
//    dest_tunnels[0]->set_connector(&dest_level_0_connector);
//
//    source_level_0_connector.set_tunnelendpoint(&(*dest_tunnels[0]));
//    dest_level_0_connector.set_tunnelendpoint( &(*source_tunnels[0]));
//
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 0, 1);// #1
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 0, 2);// #2
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 0, 3);// #3
//
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 1, 1);// #4
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 1, 2);// #5
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 1, 3);// #6
//
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 3, 1);// #7
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 3, 2);// #8
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 3, 3);// #9
//
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 8, 1);// #10
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 8, 2);// #11
//    helper_add_tunnelendpoint(async_service, checkpoint_handler, source_tunnels, source_tunnel_eventhandlers, dest_tunnels, dest_tunnel_eventhandlers, 8, 3);// #12
//
//    vector<TunnelendpointReliableCryptedTunnel::APtr>::const_iterator i(source_tunnels.begin());
//    while(i != source_tunnels.end()) {
//        MessagePayload::APtr payload(MessagePayload::create(DataBufferManaged::create("AAAAAAAAAA")));
//        (*i)->tunnelendpoint_send(payload);
//        ++i;
//    }
//
//    vector<MyTunnelendpointEventhandler::APtr>::const_iterator j(dest_tunnel_eventhandlers.begin());
//    while(j != dest_tunnel_eventhandlers.end()) {
//        BOOST_CHECK( (*j)->recived_messages_ == 1 );
//        ++j;
//    }
//
//}
//
//BOOST_AUTO_TEST_CASE( adding_already_added_tunnel )
//{
//    CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
//    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
//
//    MyTunnelendpointEventhandler::APtr my_tunnelendpoint_eventhandler(MyTunnelendpointEventhandler::create());
//    TunnelendpointReliableCryptedTunnel::APtr my_tunnelendpoint(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, my_tunnelendpoint_eventhandler.get()));
//
//    MyTunnelendpointEventhandler::APtr sub_01_eventhandler(MyTunnelendpointEventhandler::create());
//    MyTunnelendpointEventhandler::APtr sub_02_eventhandler(MyTunnelendpointEventhandler::create());
//    TunnelendpointReliableCryptedTunnel::APtr sub_01(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, sub_01_eventhandler.get()));
//    TunnelendpointReliableCryptedTunnel::APtr sub_02(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, sub_02_eventhandler.get()));
//
//    my_tunnelendpoint->add_tunnelendpoint(1, sub_01);
//    BOOST_CHECK_THROW(my_tunnelendpoint->add_tunnelendpoint(1, sub_02), Giritech::Communication::ExceptionInvalidTunnelendpoint);
//}
//
//BOOST_AUTO_TEST_CASE( removing_tunnel )
//{
//    CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());
//    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));
//
//    MyTunnelendpointEventhandler::APtr my_tunnelendpoint_eventhandler(MyTunnelendpointEventhandler::create());
//    TunnelendpointReliableCryptedTunnel::APtr my_tunnelendpoint(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, my_tunnelendpoint_eventhandler.get()));
//
//    MyTunnelendpointEventhandler::APtr sub_01_eventhandler(MyTunnelendpointEventhandler::create());
//    MyTunnelendpointEventhandler::APtr sub_02_eventhandler(MyTunnelendpointEventhandler::create());
//    TunnelendpointReliableCryptedTunnel::APtr sub_01(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, sub_01_eventhandler.get()));
//    TunnelendpointReliableCryptedTunnel::APtr sub_02(TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler, sub_02_eventhandler.get()));
//
//    my_tunnelendpoint->add_tunnelendpoint(1, sub_01);
//    my_tunnelendpoint->add_tunnelendpoint(2, sub_02);
//    my_tunnelendpoint->remove_tunnelendpoint(1);
//    my_tunnelendpoint->remove_tunnelendpoint(2);
//
//    my_tunnelendpoint->add_tunnelendpoint(1, sub_01);
//    my_tunnelendpoint->remove_tunnelendpoint(1);
//
//    BOOST_CHECK_THROW(my_tunnelendpoint->remove_tunnelendpoint(0), Giritech::Communication::ExceptionInvalidTunnelendpoint);
//    BOOST_CHECK_THROW(my_tunnelendpoint->remove_tunnelendpoint(1), Giritech::Communication::ExceptionInvalidTunnelendpoint);
//}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
    return 0;
}
