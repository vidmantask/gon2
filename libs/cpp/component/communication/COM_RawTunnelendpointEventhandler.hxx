/*! \file COM_RawTunnelEndpointEventhandler.hxx
 * \brief This file contains the abstract interface RawTunnelendpointEventhandler
 */
#ifndef COM_RawTunnelEndpointEventhandler_HXX
#define COM_RawTunnelEndpointEventhandler_HXX

#include <lib/utility/UY_DataBuffer.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler interface for raw tunnelendpoint.
 *
 * A raw tunnelendpoint eventhandler recive signals from a raw tunnelendpoint as responses to asyncron operations.
 */
class RawTunnelendpointEventhandler : public boost::noncopyable {
public:

    /*! \brief Signals that the raw tunnelendpoint  has been closed
     *
     *  \param connection_id  Connection identifier
     *
     *  The connection send this signal as a response to call to RawTunnelendpoint::aio_close_start().
     */
    virtual void com_raw_tunnelendpoint_close(const unsigned long& connection_id) = 0;

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) = 0;

    /*! \brief Signals that data has been recived
     *
     *  \param connection_id  Connection identifier
     *  \param data           recived data
     *
     *  The connection send this signal as a response to call to aio_read_start().
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data) = 0;

    /*! \brief Signals that the write data buffer is empty
     *
     *  \param connection_id  Connection identifier
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) = 0;
};
}
}
#endif
