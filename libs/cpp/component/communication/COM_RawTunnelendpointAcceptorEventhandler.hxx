/*! \file COM_RawTunnelendpointTCPAcceptorEventhandler.hxx
 *  \brief This file contains abstract interface RawTunnelendpointAcceptorTCPEventhandler
 */
#ifndef COM_RawTunnelendpointTCPAcceptorEventhandler_HXX
#define COM_RawTunnelendpointTCPAcceptorEventhandler_HXX

#include <component/communication/COM_RawTunnelendpoint.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler interface for a raw tunnelendpoint acceptor.
 *
 * A raw tunnelendpoint acceptor eventhandler recive signals from a raw tunnelendpoint accetpro
 * when a connection is accepted.
 */
class RawTunnelendpointAcceptorTCPEventhandler: public boost::noncopyable {
public:

    /*! \brief Connected signal
     *
     *  \param tunnelendpoint connected raw tunnelendpoint
     *
     *  The acceptor send this signal as a response to call to RawTunnelendpointAcceptorTCP::aio_accept_start().
     */
    virtual void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) = 0;

    /*! \brief Accept error
     *
     *  The acceptor send this signal if it fails in binding the requested host/port.
     */
    virtual void com_tunnelendpoint_acceptor_error(const std::string& error_message) = 0;


    /*! \brief closed
     *
     *  The acceptor send this signal when everything is closed (as a response to aio_accept_stop).
     */
    virtual void com_tunnelendpoint_acceptor_closed(void) = 0;

};

}
}

#endif
