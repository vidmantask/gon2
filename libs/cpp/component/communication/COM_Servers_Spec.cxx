/*! \file COM_Servers_Spec.cxx
 \brief This file contains the implementaion of the ServersSpec class
 */
#include <sstream>
#include <iostream>
#include <string>

#include <component/communication/COM_Servers_Spec.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 ------------------------------------------------------------------
 ServersSpec
 ------------------------------------------------------------------
 */
ServersSpec::ServersSpec(void) {
}

ServersSpec::~ServersSpec() {
}

ServersSpec::APtr ServersSpec::create(void) {
    return ServersSpec::APtr(new ServersSpec());
}

void ServersSpec::load_from_file(const std::string& filename) {
    bool rc = servers_spec_doc_.LoadFile(filename.c_str());
    if (!rc) {
        throw Exception_ServersSpec(servers_spec_doc_.ErrorStr());
    }
}

void ServersSpec::load_from_string(const std::string& xml_document) {
    servers_spec_doc_.Parse(xml_document.c_str());
    if (servers_spec_doc_.Error()) {
        throw Exception_ServersSpec(servers_spec_doc_.ErrorStr());
    }
}


void ServersSpec::get_servers_connection_groups(std::vector<ServersConnectionGroup>& connection_groups) {
    tinyxml2::XMLHandle doc_handler( &servers_spec_doc_ );
    tinyxml2::XMLElement* servers_spec_connection_group = doc_handler.FirstChildElement("servers").FirstChildElement("connection_group").ToElement();
    get_servers_connection_groups(connection_groups, servers_spec_connection_group);
}

void ServersSpec::get_servers_connections(std::vector<ServersConnection>& connections) {
	std::vector<ServersConnectionGroup> connection_groups;
	get_servers_connection_groups(connection_groups);

	std::vector<ServersConnectionGroup>::const_iterator i(connection_groups.begin());
	while(i != connection_groups.end()) {
		connections.insert(connections.end(), (*i).connections.begin(), (*i).connections.end());
		i++;
	}
}

void ServersSpec::get_servers_connection_groups(std::vector<ServersConnectionGroup>& connection_groups, tinyxml2::XMLElement* servers_spec_connection_group) {
    while(servers_spec_connection_group != NULL) {
        ServersConnectionGroup server_connection_group;
        server_connection_group.title = servers_spec_connection_group->Attribute("title");

        int selection_delay_sec = 0;
        servers_spec_connection_group->QueryIntAttribute("selection_delay_sec", &selection_delay_sec);
        server_connection_group.selection_delay = boost::posix_time::seconds(selection_delay_sec);

        tinyxml2::XMLElement* servers_spec_connection_node = servers_spec_connection_group->FirstChildElement("connection");
        if(servers_spec_connection_node != NULL) {
            get_servers_connection(server_connection_group.connections, servers_spec_connection_node);
        }
        connection_groups.push_back(server_connection_group);
        servers_spec_connection_group = servers_spec_connection_group->NextSiblingElement("connection_group");
    }
}

void ServersSpec::get_servers_connection(std::vector<ServersConnection>& connections, tinyxml2::XMLElement* servers_spec_connection) {
    while(servers_spec_connection != NULL) {
        ServersConnection server_connection;

        server_connection.title = servers_spec_connection->Attribute("title");
        server_connection.host = servers_spec_connection->Attribute("host");

        int port = 0;
        servers_spec_connection->QueryIntAttribute("port", &port);
        server_connection.port = port;

        int timeout_sec = 0;
        servers_spec_connection->QueryIntAttribute("timeout_sec", &timeout_sec);
        server_connection.timeout = boost::posix_time::seconds(timeout_sec);

        string type_string(servers_spec_connection->Attribute("type"));
        if(type_string == "http") {
            server_connection.type = ServersConnection::ConnectionType_http;
        }
        else {
            server_connection.type = ServersConnection::ConnectionType_direct;
        }

        connections.push_back(server_connection);
        servers_spec_connection = servers_spec_connection->NextSiblingElement("connection");
    }
}
