/*! \file COM_StreamToMessage.cxx
 \brief This file contains the message to stream converter
 */
#include <component/communication/COM_StreamToMessage.hxx>
#include <lib/utility/UY_Convert.hxx>

#include <iostream>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * StreamToMessage implementation
 * ------------------------------------------------------------------
 */
StreamToMessage::StreamToMessage(void) :
    available_(0), used_of_first_buffer_(0) {
}

StreamToMessage::~StreamToMessage(void) {
}

StreamToMessage::APtr StreamToMessage::create(void) {
    return APtr(new StreamToMessage);
}

void StreamToMessage::push_data(const Utility::DataBuffer::APtr& data) {
    available_ += data->getSize();
    buffers_.push_back(data);
}

MessageCom::APtr StreamToMessage::pop_message_com(void) {
    return MessageCom::create_from_stream(*this);
}

MessageRaw::APtr StreamToMessage::pop_message_raw(void) {
    return MessageRaw::create_from_stream(*this);
}

bool StreamToMessage::empty(void) const {
    return available_ == 0;
}

unsigned long StreamToMessage::peek_available(void) const {
    return available_;
}

void StreamToMessage::get_buffer_chunk(const Utility::DataBuffer::APtr& buffer,
                                       unsigned long& chunk_offset,
                                       unsigned long& chunk_size) const {
    chunk_offset = 0;
    chunk_size = buffer->getSize();
}

void StreamToMessage::get_first_buffer_chunk(const Utility::DataBuffer::APtr& buffer,
                                             unsigned long& chunk_offset,
                                             unsigned long& chunk_size) const {
    chunk_offset = used_of_first_buffer_;
    chunk_size = buffer->getSize() - used_of_first_buffer_;
}

Utility::DataBufferManaged::APtr StreamToMessage::peek_buffer(const unsigned long offset,
                                                              const unsigned long size) const {
    if(! (size > 0)) {
        throw ExceptionPeekError();
    }
    if (offset + size > available_) {
        throw ExceptionPeekError();
    }
    DataBufferManaged::APtr result(DataBufferManaged::create(size, 'x'));

    bool offset_found = false;
    unsigned long offset_to_find = offset;
    unsigned long size_to_find = size;
    unsigned long size_already_found = 0;

    bool is_first = true;

    for (const Utility::DataBuffer::APtr& buffer : buffers_) {
        unsigned long current_chunk_offset;
        unsigned long current_chunk_size;

        if (is_first) {
            is_first = false;
            get_first_buffer_chunk(buffer, current_chunk_offset, current_chunk_size);
        } else {
            get_buffer_chunk(buffer, current_chunk_offset, current_chunk_size);
        }

        unsigned long chunk_fragment_size_max = current_chunk_size;
        if (!offset_found) {
            if (offset_to_find < chunk_fragment_size_max) {
                offset_found = true;
            } else {
                offset_to_find -= current_chunk_size;
            }
        }
        if (offset_found) {
            unsigned long chunk_fragment_size = chunk_fragment_size_max - offset_to_find;
            if (size_to_find <= chunk_fragment_size) {
                memcpy(result->data() + size_already_found, buffer->data() + current_chunk_offset + offset_to_find, size_to_find);
                break;
            } else {
                memcpy(result->data() + size_already_found, buffer->data() + current_chunk_offset + offset_to_find, chunk_fragment_size);
                size_to_find -= chunk_fragment_size;
                size_already_found += chunk_fragment_size;
                offset_to_find = 0;
            }
        }
    }
    return result;
}

boost::uint8_t StreamToMessage::peek_byte(const unsigned long offset) const {
    if (offset >= available_) {
        throw ExceptionPeekError();
    }

    bool offset_found = false;
    unsigned long offset_to_find = offset;
    bool is_first = true;

    for (const Utility::DataBuffer::APtr& buffer : buffers_) {

        unsigned long current_chunk_offset;
        unsigned long current_chunk_size;

        if (is_first) {
            is_first = false;
            get_first_buffer_chunk(buffer, current_chunk_offset, current_chunk_size);
        } else {
            get_buffer_chunk(buffer, current_chunk_offset, current_chunk_size);
        }

        unsigned long chunk_fragment_size_max = current_chunk_size;
        if (!offset_found) {
            if (offset_to_find < chunk_fragment_size_max) {
                offset_found = true;
            } else {
                offset_to_find -= current_chunk_size;
            }
        }
        if (offset_found) {
            return *(buffer->data() + current_chunk_offset + offset_to_find);
        }
    }
    throw ExceptionPeekError();
}

void StreamToMessage::consume(const unsigned long size) {
    assert(size > 0);
    if (size > available_) {
        throw ExceptionPeekError();
    }

    unsigned long size_to_find = size;

    while (!buffers_.empty()) {

        const Utility::DataBuffer::APtr& buffer = buffers_.at(0);

        unsigned long current_chunk_offset;
        unsigned long current_chunk_size;

        get_first_buffer_chunk(buffer, current_chunk_offset, current_chunk_size);

        unsigned long chunk_fragment_size_max = current_chunk_size;
        if (size_to_find < chunk_fragment_size_max) {
            used_of_first_buffer_ += size_to_find;
            available_ -= size_to_find;
            break;
        } else {
            available_ -= chunk_fragment_size_max;
            used_of_first_buffer_ = 0;
            size_to_find -= chunk_fragment_size_max;
            buffers_.erase(buffers_.begin());
        }
    }
}

void StreamToMessage::dump(void) const {
    cout << "StreamToMessage dump:" << endl;
    cout << "  available_:" << available_ << endl;
    cout << "  used_of_first_buffer_:" << used_of_first_buffer_ << endl;
    cout << "  chunks(" << buffers_.size()<< "):" << endl;

//    std::vector<Utility::DataBuffer::APtr>::const_iterator i(buffers_.begin());
// //   while (i != buffers_.end()) {
//        cout << "    size:" << (*i)->getSize() << endl;
////        cout << "    size:" << (*i)->getSize() << " data:" << (*i)->encodeHex()->toString() << endl;
//        ++i;
//    }
}

std::string StreamToMessage::dump_to_str(void) const {
    stringstream ss;
    ss << "StreamToMessage dump:" << endl;
    ss << "  available_:" << available_ << endl;
    ss << "  used_of_first_buffer_:" << used_of_first_buffer_ << endl;
    ss << "  chunks(" << buffers_.size()<< "):" << endl;

    for (const Utility::DataBuffer::APtr& buffer : buffers_) {
        ss << "    size:" << buffer->getSize() << " data:" << buffer->encodeHex()->toString() << endl;
    }
    return ss.str();
}

Utility::DataBufferManaged::APtr StreamToMessage::get_remaining(void) const {
	return peek_buffer(0, peek_available());
}
