/*! \file COM_API_Session.hxx
 \brief This file contains the a API wrapper for the Session
 */
#ifndef COM_API_Session_HXX
#define COM_API_Session_HXX

#include <boost/python.hpp>

#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_API_Tunnelendpoint.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for the APISession eventhandler.
 */
class APISessionEventhandlerReady: public boost::noncopyable {
public:
    /*! \brief Signals that the session is ready
     */
    virtual void session_state_ready(void) = 0;

    /*! \brief Signals that the session is closed
     */
    virtual void session_state_closed(void) = 0;

    /*! \brief Signals that the session switched to key-exchange state
     */
    virtual void session_state_key_exchange(void) = 0;

    /*! \brief User defined signal from a tunnelendpoint
     */
    virtual void session_user_signal(const unsigned long signal_id, const std::string& message) = 0;
};

/*! \brief APISession
 *  */
class APISession: public SessionEventhandlerReady {
public:
    typedef boost::shared_ptr<APISession> APtr;

    /*! \brief Destructor
     */
    ~APISession(void);

    /*! \brief Close session
     *
     *  \param force If the force flag is set then the connection will be closed hard, and read/writes alrady initiated might be cancled.
     */
    void close(const bool& force);

    /*! \brief Set eventhandler
     */
    void set_eventhandler_ready(APISessionEventhandlerReady* eventhandler_ready);
    void reset_eventhandler_ready(void);

    /*! \brief Add tunnelendpoint
     */
    void add_tunnelendpoint(const unsigned long& child_id, const APITunnelendpoint::APtr& tunnelendpoint);
    void add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint);

    /*! \brief Report back if the session has been closed
     */
    bool is_closed(void) const;

    /*! \brief Get session mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Return session id
     */
    unsigned long get_session_id(void) const;

    /*! \brief Return unique session id
     */
    std::string get_unique_session_id(void) const;

    /*! \brief Return unique session id
     */
    std::string get_remote_unique_session_id(void) const;
    bool get_remote_session_logging_enabled(void) const;


    /*! \brief Return local ip and port of socket
     */
    std::pair<std::string, unsigned long> get_ip_remote(void) const;

    /*! \brief Return local ip and port of socket
     */
    std::pair<std::string, unsigned long> get_ip_local(void) const;

    /*! \brief Signals from SessionEventhandlerReady
     */
    void session_state_ready(void);

    /*! \brief Signals that the session is closed
     */
    void session_state_closed(void);

    /*! \brief Signals that the session is dooing key exchange
     */
    void session_state_key_exchange(void);

    bool session_read_continue_state_ready(void);

    /*! \brief Start read in ready state
     */
    void read_start_state_ready(void);

    /*! \brief User defined signal from a tunnelendpoint
     */
    void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message);

    /*! \brief Set keep alive ping interval
     */
    void set_keep_alive_ping_interval(const boost::posix_time::time_duration& keep_alive_ping_interval);

    /*! \brief Create instance
     */
    static APtr create(const Session::APtr& session);

    static void self_close(const APISession::APtr& self, const bool& force);
    static bool self_is_closed(const APISession::APtr& self);
    static unsigned long self_get_session_id(const APISession::APtr& self);
    static boost::python::str self_get_unique_session_id(const APISession::APtr& self);
    static boost::python::str self_get_remote_unique_session_id(const APISession::APtr& self);
    static bool self_get_remote_session_logging_enabled(const APISession::APtr& self);
    static APIMutex::APtr self_get_mutex(const APISession::APtr& self);


    static boost::python::tuple self_get_ip_remote(const APISession::APtr& self);
    static boost::python::tuple self_get_ip_local(const APISession::APtr& self);

    static void self_add_tunnelendpoint_reliable_crypted(const APISession::APtr& self,
                                                         const unsigned long& child_id,
                                                         const APITunnelendpointReliableCrypted::APtr& tunnelendpoint);
    static void
            self_add_tunnelendpoint_reliable_crypted_tunnel(const APISession::APtr& self,
                                                            const unsigned long& child_id,
                                                            const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint);

    static void self_set_eventhandler_ready(const APISession::APtr& self,APISessionEventhandlerReady* eventhandler_ready);

    static void self_read_start_state_ready(const APISession::APtr& self);

    static void self_set_keep_alive_ping_interval_sec(const APISession::APtr& self, const long& keep_alive_ping_interval_sec);

private:
    /*! \brief Constructor
     */
    APISession(Session::APtr session);

    Session::APtr impl_session_;

    APISessionEventhandlerReady* eventhandler_ready_;
};

}
}
#endif
