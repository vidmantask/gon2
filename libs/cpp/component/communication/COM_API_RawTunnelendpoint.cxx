/*! \file COM_API_RawTunnelendpoint.cxx
 *  \brief This file contains the implementation of the API wrapper for raw-tcp-tunnelendpoints
 */
// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <component/communication/COM_API_RawTunnelendpoint.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APIRawTunnelendpointTCP implementation
 * ------------------------------------------------------------------
 */
APIRawTunnelendpointTCP::APIRawTunnelendpointTCP(const RawTunnelendpointTCP::APtr& impl_tunnelendpoint) :

    api_eventhandler_(NULL), impl_tunnelendpoint_(impl_tunnelendpoint) {
}

APIRawTunnelendpointTCP::~APIRawTunnelendpointTCP(void) {
}

void APIRawTunnelendpointTCP::set_tcp_eventhandler(APIRawTunnelendpointEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
    impl_tunnelendpoint_->set_eventhandler(this);
}

void APIRawTunnelendpointTCP::reset_tcp_eventhandler(void) {
    api_eventhandler_ = NULL;
    impl_tunnelendpoint_->reset_eventhandler();
}

void APIRawTunnelendpointTCP::aio_read_start(void) {
    impl_tunnelendpoint_->aio_read_start();
}

void APIRawTunnelendpointTCP::aio_write_start(const std::string& data) {
    impl_tunnelendpoint_->aio_write_start(Utility::DataBufferManaged::create(data));
}

void APIRawTunnelendpointTCP::aio_close_start(const bool force) {
    impl_tunnelendpoint_->aio_close_start(force);
}

void APIRawTunnelendpointTCP::aio_close_write_start(void) {
    impl_tunnelendpoint_->aio_close_write_start();
}

bool APIRawTunnelendpointTCP::is_closed(void) const {
    return impl_tunnelendpoint_->is_closed();
}

bool APIRawTunnelendpointTCP::is_ready(void) const {
    return impl_tunnelendpoint_->is_ready();
}

void APIRawTunnelendpointTCP::aio_ssl_start(const std::string& ssl_host_name, const bool& disable_certificate_verification) {
    impl_tunnelendpoint_->aio_ssl_start(ssl_host_name, disable_certificate_verification);
}

std::pair<std::string, unsigned long> APIRawTunnelendpointTCP::get_ip_local(void) const {
    return impl_tunnelendpoint_->get_ip_local();
}

std::pair<std::string, unsigned long> APIRawTunnelendpointTCP::get_ip_remote(void) const {
    return impl_tunnelendpoint_->get_ip_remote();
}

APIMutex::APtr APIRawTunnelendpointTCP::get_mutex(void) {
    return APIMutex::create(impl_tunnelendpoint_->get_mutex());
}

void APIRawTunnelendpointTCP::set_mutex(const APIMutex::APtr& api_mutex) {
    impl_tunnelendpoint_->set_mutex(api_mutex->get_impl());
}

APIRawTunnelendpointTCP::APtr APIRawTunnelendpointTCP::create(const RawTunnelendpointTCP::APtr& impl_tunnelendpoint) {
    return APIRawTunnelendpointTCP::APtr(new APIRawTunnelendpointTCP(impl_tunnelendpoint));
}

void APIRawTunnelendpointTCP::self_aio_read_start(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    self->aio_read_start();
}

void APIRawTunnelendpointTCP::self_set_tcp_eventhandler(const APIRawTunnelendpointTCP::APtr& self, APIRawTunnelendpointEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APIRawTunnelendpointTCP::self_reset_tcp_eventhandler(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}


void APIRawTunnelendpointTCP::self_aio_write_start(const APIRawTunnelendpointTCP::APtr& self, const std::string& data) {
    assert(self.get() != NULL);
    self->aio_write_start(data);
}

void APIRawTunnelendpointTCP::self_aio_close_start(const APIRawTunnelendpointTCP::APtr& self, const bool& force) {
    assert(self.get() != NULL);
    self->aio_close_start(force);
}

void APIRawTunnelendpointTCP::self_aio_close_write_start(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    self->aio_close_write_start();
}

bool APIRawTunnelendpointTCP::self_is_closed(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}

bool APIRawTunnelendpointTCP::self_is_ready(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    return self->is_ready();
}

boost::python::tuple APIRawTunnelendpointTCP::self_get_ip_local(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_local());
    return boost::python::make_tuple(ip.first, ip.second);
}

boost::python::tuple APIRawTunnelendpointTCP::self_get_ip_remote(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_remote());
    return boost::python::make_tuple(ip.first, ip.second);
}

APIMutex::APtr APIRawTunnelendpointTCP::self_get_mutex(const APIRawTunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}

void APIRawTunnelendpointTCP::self_aio_ssl_start(const APIRawTunnelendpointTCP::APtr& self, const std::string& ssl_host_name, const bool& disable_certificate_verification) {
    assert(self.get() != NULL);
    self->aio_ssl_start(ssl_host_name, disable_certificate_verification);
}

void APIRawTunnelendpointTCP::self_set_mutex(const APIRawTunnelendpointTCP::APtr& self, const APIMutex::APtr& api_mutex) {
    assert(self.get() != NULL);
    self->set_mutex(api_mutex);
}

void APIRawTunnelendpointTCP::com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_raw_tunnelendpoint_close(connection_id);
    }
}

void APIRawTunnelendpointTCP::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_raw_tunnelendpoint_close_eof(connection_id);
    }
}

void APIRawTunnelendpointTCP::com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_raw_tunnelendpoint_read(connection_id, data->toString());
    }
}

void APIRawTunnelendpointTCP::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
    if (api_eventhandler_ != NULL) {
        api_eventhandler_->com_raw_tunnelendpoint_write_buffer_empty(connection_id);
    }
}

