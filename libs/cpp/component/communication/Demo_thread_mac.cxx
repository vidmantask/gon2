/*! \file Demo_thread_mac.cxx
 */
#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>


#define THREAD_COUNT 10
#define THREAD_LOOP_COUNT 1000
#define SLEEP_MS 10

using namespace std;





void thread_main(const int thread_id) {
	for(int i=0; i < THREAD_LOOP_COUNT; i++) {
		boost::posix_time::ptime start_ts(boost::posix_time::microsec_clock::local_time());
		boost::this_thread::sleep_for(boost::chrono::milliseconds(SLEEP_MS));
		boost::posix_time::ptime stop_ts(boost::posix_time::microsec_clock::local_time());
		if((stop_ts - start_ts).total_milliseconds() > SLEEP_MS) {
			cout << "  sleep lasted " << (stop_ts - start_ts).total_milliseconds() << " ms" << endl;
		}
	}
}


/*
 * main
 */
int main(int, char* []) {
	int thread_count(THREAD_COUNT);
	cout << "main.start" << endl;
    boost::thread_group thread_group;

    for(int i=0; i<thread_count; i++) {
    	thread_group.create_thread(boost::bind(&thread_main, i));
    }
    thread_group.join_all();

	cout << "main.done" << endl;
    return 0;
}
