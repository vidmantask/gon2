/*! \file COM_TunnelEndpointDirectControlEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling control signals from a direct tunnelendpoint
 */
#ifndef COM_TunnelendpointDirectControlEventhandler_HXX
#define COM_TunnelendpointDirectControlEventhandler_HXX



namespace Giritech {
namespace Communication {


/*! \brief This class define the abstract eventhandler handling signals from a direct tunnelendpoint connection
 *
 * Used for signal to TunnelendpointTCPTunnel instances for sending events to other side
 */
class TunnelendpointDirectControlEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint has recived a eof indicating that no more data are comming
     */
    virtual void tunnelendpoint_direct_eh_eof(const unsigned long tunnel_id) = 0;


    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_direct_eh_closed(const unsigned long tunnel_id) = 0;

};

}
}
#endif
