/*! \file COM_TunnelendpointDirect.cxx
 \brief This file contains the implementation for the direct tunnelendpoint class
 */
#include <functional>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointDirect.hxx>
#include <component/communication/COM_TunnelendpointDirectEventhandler.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TunnelendpointDirect implementation
 * ------------------------------------------------------------------
 */
TunnelendpointDirect::TunnelendpointDirect(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		TunnelendpointDirectControlEventhandler* direct_ctrl_eventhandler) :
    tunnel_id_(0),
    io_(io),
    direct_ctrl_eventhandler_(direct_ctrl_eventhandler),
    TunnelendpointReliableCrypted(io, checkpoint_handler,this),
    direct_eventhandler_(NULL),
    state_direct_(StateDirect_Initializing) {
}

TunnelendpointDirect::~TunnelendpointDirect(void) {
    state_direct_ = StateDirect_Unknown;
}

TunnelendpointDirect::APtr TunnelendpointDirect::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		TunnelendpointDirectControlEventhandler* direct_ctrl_eventhandler) {
	TunnelendpointDirect::APtr new_tunnelendpoint(new TunnelendpointDirect(checkpoint_handler, io, direct_ctrl_eventhandler));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

boost::asio::io_service& TunnelendpointDirect::get_io_service(void) {
	return io_;
}
Giritech::Utility::CheckpointHandler::APtr TunnelendpointDirect::get_checkpoint_handler(void) {
	return checkpoint_handler_;
}

void TunnelendpointDirect::set_direct_eventhandler(TunnelendpointDirectEventhandler* direct_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::set_direct_eventhandler");
    direct_eventhandler_ = direct_eventhandler;
}

void TunnelendpointDirect::reset_direct_eventhandler(void) {
	assert(state_direct_!=StateDirect_Unknown);
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::reset_direct_eventhandler");
    direct_eventhandler_ = NULL;
}

void TunnelendpointDirect::reset_direct_ctrl_eventhandler(void) {
	assert(state_direct_!=StateDirect_Unknown);
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::reset_direct_ctrl_eventhandler");
    direct_ctrl_eventhandler_ = NULL;
}

void TunnelendpointDirect::set_mutex(const Utility::Mutex::APtr& mutex) {
    Tunnelendpoint::set_mutex(mutex);
}

bool TunnelendpointDirect::is_closed(void) {
	assert(state_direct_!=StateDirect_Unknown);
	return state_direct_ == StateDirect_Closed;
}

void TunnelendpointDirect::set_tunnel_id(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::set_tunnel_id");
    tunnel_id_ = tunnel_id;
}

void TunnelendpointDirect::set_unique_session_id(const std::string& unique_session_id) {
	unique_session_id_ = unique_session_id;
}

std::string TunnelendpointDirect::get_unique_session_id(void) {
	return unique_session_id_;
}

void TunnelendpointDirect::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::close");
    switch (state_direct_) {
    case StateDirect_Initializing:
    case StateDirect_Connecting:
    case StateDirect_Ready:
        break;
    default:
        return;
    }
    state_direct_ = StateDirect_Closing;
    goto_closed_state_();
}

void TunnelendpointDirect::connecting(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::connecting");
    switch (state_direct_) {
    case StateDirect_Initializing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirect not ready for connecting");
    }
    state_direct_ = StateDirect_Connecting;
}

void TunnelendpointDirect::connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::connected");
    switch (state_direct_) {
    case StateDirect_Connecting:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirect not ready for connected");
    }
    goto_ready_state_();
}


void TunnelendpointDirect::write(const DataBufferManaged::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::write");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    case StateDirect_Closing:
        return; // Data written after close are ignored
    default:
        throw ExceptionUnexpected("TunnelendpointDirect not ready for write");
    }
    tunnelendpoint_send(MessagePayload::create(data));
}

void TunnelendpointDirect::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::tunnelendpoint_eh_recieve");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirect::tunnelendpoint_eh_recieve.message_ignored", Attr_Communication(), CpAttr_debug());
        return;
    }
    if (direct_eventhandler_ != NULL) {
    	direct_eventhandler_->tunnelendpoint_direct_eh_recieve(message->get_buffer());
    }
}

void TunnelendpointDirect::tunnelendpoint_eh_connected(void) {
}

void TunnelendpointDirect::goto_ready_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::goto_ready_state_");
    switch (state_direct_) {
    case StateDirect_Initializing:
    case StateDirect_Connecting:
        break;
    default:
        return;
    }
    state_direct_ = StateDirect_Ready;
}

void TunnelendpointDirect::goto_closed_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::goto_closed_state_");
    switch (state_direct_) {
    case StateDirect_Closing:
        break;
    default:
        Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirect::goto_closed_state_.invalid_state", Attr_Communication(), CpAttr_error(), CpAttr("state", state_));
        return;
    }
    state_direct_ = StateDirect_Closed;

    if (direct_eventhandler_ != NULL) {
    	TunnelendpointDirectEventhandler* temp_direct_eventhandler(direct_eventhandler_);
        reset_direct_eventhandler();
        temp_direct_eventhandler->tunnelendpoint_direct_eh_closed();
    }

    if (direct_ctrl_eventhandler_ != NULL) {
    	TunnelendpointDirectControlEventhandler* temp_direct_ctrl_eventhandler(direct_ctrl_eventhandler_);
    	temp_direct_ctrl_eventhandler->tunnelendpoint_direct_eh_closed(tunnel_id_);
        reset_direct_ctrl_eventhandler();
    }
    TunnelendpointReliableCrypted::close(true);
}

bool TunnelendpointDirect::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirect::async_mem_guard_is_done");
    switch (state_direct_) {
    case StateDirect_Closed:
    case StateDirect_Unknown:
        break;
    default:
        return false;
    }
    return TunnelendpointReliableCrypted::async_mem_guard_is_done();
}

std::string TunnelendpointDirect::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}



/*
 * ------------------------------------------------------------------
 * TunnelendpointDirectTunnelClient implementation
 * ------------------------------------------------------------------
 */
TunnelendpointDirectTunnelClient::TunnelendpointDirectTunnelClient(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                             boost::asio::io_service& io,
                                                             const unsigned long connection_pool_size) :
    TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, this),
    direct_tunnel_eventhandler_(NULL),
    connection_pool_size_(connection_pool_size),
    state_direct_(StateDirect_Initializing),
    waiting_for_connection_(0),
    checkpoint_tunnel_checkpoint_handler_(checkpoint_handler),
    checkpoint_tunnel_enabled_(false) {
	for(int i=1; i<255; i++) {
		tunnel_ids_free_.push_back(i);
	}
}

TunnelendpointDirectTunnelClient::~TunnelendpointDirectTunnelClient(void) {
    Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::~TunnelendpointDirectTunnelClient", Attr_Communication(), CpAttr_debug());
    state_direct_ = StateDirect_Unknown;
}

void TunnelendpointDirectTunnelClient::set_direct_eventhandler(TunnelendpointDirectTunnelClientEventhandler* direct_tunnel_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::set_direct_eventhandler");
    direct_tunnel_eventhandler_ = direct_tunnel_eventhandler;
}
void TunnelendpointDirectTunnelClient::reset_direct_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::reset_direct_eventhandler");
    direct_tunnel_eventhandler_ = NULL;
}

void TunnelendpointDirectTunnelClient::set_mutex(const Utility::Mutex::APtr& mutex) {
    TunnelendpointReliableCryptedTunnel::set_mutex(mutex);
}

TunnelendpointDirectTunnelClient::APtr TunnelendpointDirectTunnelClient::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                                                          boost::asio::io_service& io,
                                                                          const unsigned long connection_pool_size) {
	TunnelendpointDirectTunnelClient::APtr new_tunnelendpoint(new TunnelendpointDirectTunnelClient(checkpoint_handle, io, connection_pool_size));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

Giritech::Utility::CheckpointHandler::APtr TunnelendpointDirectTunnelClient::get_checkpoint_handler(void) {
	return checkpoint_handler_;
}

boost::asio::io_service& TunnelendpointDirectTunnelClient::get_io_service(void) {
	return io_;
}

void TunnelendpointDirectTunnelClient::connect(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::connect");
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::connect", Attr_Communication(), CpAttr_debug());
    Tunnelendpoint::connect();
}

void TunnelendpointDirectTunnelClient::close(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::close");
    switch (state_direct_) {
    case StateDirect_Closed:
    case StateDirect_Closing:
    case StateDirect_Closing_no_connection:
        return;
    case StateDirect_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::close", Attr_Communication(),
                        CpAttr_debug(), CpAttr("num_of_tunnelendpoints", direct_tunnelendpoints_.size()),
                        CpAttr("abs_id", get_id_abs_to_string()));

    state_direct_ = StateDirect_Closing;
    close_state_all_connection_closed_ = false;
    close_down_(force);
}

void TunnelendpointDirectTunnelClient::close_down_(const bool force) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::close_down_");
    if (direct_tunnelendpoints_.empty()) {
    	close_state_all_connection_closed_ = true;
        goto_closed_state_();
    } else {
        std::set<unsigned long> direct_tunnelendpoints_to_close;

        for (const auto& pair : direct_tunnelendpoints_) {
            direct_tunnelendpoints_to_close.insert(pair.first);
        }

        std::set<unsigned long>::const_iterator j(direct_tunnelendpoints_to_close.begin());
        while (j != direct_tunnelendpoints_to_close.end()) {
            auto i = direct_tunnelendpoints_.find(*j);
            if (i != direct_tunnelendpoints_.end()) {
            	(i->second)->close(false);
            }
            ++j;
        }
    }
}

bool TunnelendpointDirectTunnelClient::is_closed() {
    return state_direct_ == StateDirect_Closed;
}

void TunnelendpointDirectTunnelClient::request_connection(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::request_connection");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    default:
    	return;
    }
    waiting_for_connection_ += 1;
    Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::request_connection", Attr_Communication(), CpAttr_debug(), CpAttr("#direct_tunnelendpoints_", direct_tunnelendpoints_.size()),CpAttr("#tunnel_ids_ready_", tunnel_ids_ready_.size()), CpAttr("#tunnel_ids_bussy", tunnel_ids_bussy_.size()));

    check_for_connection_request();
}


unsigned long TunnelendpointDirectTunnelClient::get_new_tunnel_id() {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::get_new_tunnel_id");

    unsigned long new_tunnel_id = 0;
    std::vector<unsigned long>::iterator i(tunnel_ids_free_.begin());
    if(i != tunnel_ids_free_.end()) {
        new_tunnel_id = (*i);
        tunnel_ids_free_.erase(i);
    }
	return new_tunnel_id;
}


void TunnelendpointDirectTunnelClient::check_for_connection_request(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::check_for_connection_request");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    default:
    	return;
    }

    if(waiting_for_connection_ <= 0) {
//        Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.no_connection_waiting", Attr_Communication(), CpAttr_debug());
    	return;
    }

//    // Check for pool_size
//    if(tunnel_ids_bussy_.size() >= connection_pool_size_) {
//        Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.pool_size_reached", Attr_Communication(), CpAttr_debug());
//    	return;
//    }

    // Use a free tunnelendpoint
    std::set<unsigned long>::iterator i(tunnel_ids_ready_.begin());
	if(i != tunnel_ids_ready_.end()) {
		unsigned long tunnel_id = (*i);
		tunnel_ids_bussy_.insert(tunnel_id);
		tunnel_ids_ready_.erase(tunnel_id);

	    waiting_for_connection_ -= 1;
	    if (direct_tunnel_eventhandler_ != NULL) {
	        std::map<unsigned long, TunnelendpointDirect::APtr>::iterator i(direct_tunnelendpoints_.find(tunnel_id));
	        if (i != direct_tunnelendpoints_.end()) {
	        	direct_tunnel_eventhandler_->tunnelendpoint_direct_eh_connection_ready((*i).second);
	    		return;
	        }
	        else {
	            Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.invalid_endpoint", Attr_Communication(), CpAttr_debug());
	        }
	    }
        else {
            Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.no_eventhandler_defined", Attr_Communication(), CpAttr_debug());
        }
	}

    // Check for free tunnelendpoints id
    unsigned long new_tunnel_id(get_new_tunnel_id());
    if(new_tunnel_id == 0) {
        Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.no_free_tunnelendpoint_ids", Attr_Communication(), CpAttr_error());
    	return;
    }

    // Create new direct_tunnelendpoint
    TunnelendpointDirect::APtr tunnelendpoint_direct(TunnelendpointDirect::create(checkpoint_tunnel_checkpoint_handler_, io_, this));
    tunnelendpoint_direct->set_tunnel_id(new_tunnel_id);
    tunnelendpoint_direct->set_skip_connection_handshake();
    tunnelendpoint_direct->set_unique_session_id(tunnelendpoint_get_unique_session_id());

    direct_tunnelendpoints_.insert(make_pair(new_tunnel_id, tunnelendpoint_direct));
	tunnel_ids_bussy_.insert(new_tunnel_id);
    add_tunnelendpoint(new_tunnel_id, tunnelendpoint_direct);
    tunnelendpoint_direct->connect();

    TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_connect, new_tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
    tunnelendpoint_direct->connecting();
    Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::check_for_connection_request.new_created", Attr_Communication(), CpAttr_debug(), CpAttr("#direct_tunnelendpoints_", direct_tunnelendpoints_.size()),CpAttr("#tunnel_ids_ready_", tunnel_ids_ready_.size()), CpAttr("#tunnel_ids_bussy", tunnel_ids_bussy_.size()));
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_eh_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_eh_connected");
    switch (state_direct_) {
    case StateDirect_Initializing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_eh_connected");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_eh_connected", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));

    if(checkpoint_tunnel_enabled_) {
    	state_direct_ = StateDirect_InitCheckpointTunnel;
    	checkpoint_tunnel_id_ = get_new_tunnel_id();

    	add_tunnelendpoint(checkpoint_tunnel_id_, checkpoint_tunnel_);
    	checkpoint_tunnel_->connect();

        TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_checkpoint, checkpoint_tunnel_id_, TunnelendpointTCPMessage::ReturnCodeType_ok));
        tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
    	return;
    }
    goto_ready_state_();
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_eh_recieve");

    switch (state_direct_) {
    case StateDirect_InitCheckpointTunnel:
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    case StateDirect_Closed:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_eh_recieve");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_eh_recieve", Attr_Communication(), CpAttr_debug());

    /*
     * Parse message from server
     */
    TunnelendpointTCPMessage::APtr tcp_message(TunnelendpointTCPMessage::create_from_buffer(message->get_buffer()));
    cps.add_complete_attr(CpAttr("abs_id", get_id_abs_to_string(tcp_message->get_tunnel_id())));

    switch (tcp_message->get_message_type()) {
    case TunnelendpointTCPMessage::MessageType_connect_response:
        tunnelendpoint_recieve_connect_response(tcp_message->get_tunnel_id(), tcp_message->get_rc());
        break;
    case TunnelendpointTCPMessage::MessageType_eof:
        tunnelendpoint_recieve_eof(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_close:
        tunnelendpoint_recieve_close(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_closed:
        tunnelendpoint_recieve_closed(tcp_message->get_tunnel_id());
        break;
    case TunnelendpointTCPMessage::MessageType_message:
        tunnelendpoint_recieve_message(tcp_message->get_payload());
        break;
    case TunnelendpointTCPMessage::MessageType_checkpoint_response:
        tunnelendpoint_recieve_checkpoint_response(tcp_message->get_tunnel_id(), tcp_message->get_rc());
        break;
    default:
        break;
    }
    return;
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_connect_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_connect_response");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    case StateDirect_Closed:
    case StateDirect_Closing:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_connect_response");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_connect_response", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointDirect::APtr>::iterator i(direct_tunnelendpoints_.find(tunnel_id));
    if (rc == TunnelendpointTCPMessage::ReturnCodeType_ok) {
        if (i != direct_tunnelendpoints_.end()) {
            (i->second)->connected();
            tunnel_ids_ready_.insert(tunnel_id);
            tunnel_ids_bussy_.erase(tunnel_id);
            check_for_connection_request();
        }
        else {
            waiting_for_connection_ -= 1;
            Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_connect_response.error.missing_tunnelendpoint", Attr_Communication(), CpAttr_error());
        }
    } else {
        waiting_for_connection_ -= 1;
        if (i != direct_tunnelendpoints_.end()) {
            (i->second)->close(true);
        }
        if (direct_tunnel_eventhandler_ != NULL) {
        	direct_tunnel_eventhandler_->tunnelendpoint_direct_eh_connection_failed("Server unable to connect");
        }
    }
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_eof");
    switch (state_) {
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_eof");
    }
    Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_eof", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));
    tunnelendpoint_recieve_close(tunnel_id);
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_close(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_close");
    switch (state_direct_) {
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_close");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_close", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    std::map<unsigned long, TunnelendpointDirect::APtr>::const_iterator i(direct_tunnelendpoints_.find(tunnel_id));
    if (i != direct_tunnelendpoints_.end()) {
    	TunnelendpointDirect::APtr tunnelendpoint_direct(i->second);
    	tunnelendpoint_direct->close(false);
    }
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_closed(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_closed");
    switch (state_direct_) {
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_closed");
    }
    Checkpoint cp1(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_closed", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));



    std::map<unsigned long, TunnelendpointDirect::APtr>::iterator i(direct_tunnelendpoints_.find(tunnel_id));
    if (i != direct_tunnelendpoints_.end()) {
    	(i->second)->close(false);
    }
    tunnel_ids_tunnelendpoint_recived_closed_.insert(tunnel_id);
    clean_up_tunnelendpoint(tunnel_id);
}

void TunnelendpointDirectTunnelClient::clean_up_tunnelendpoint(const unsigned long tunnel_id) {
    std::set<unsigned long>::iterator i(tunnel_ids_tunnelendpoint_recived_closed_.find(tunnel_id));
    if(i == tunnel_ids_tunnelendpoint_recived_closed_.end()) {
    	return;
    }
    std::set<unsigned long>::iterator j(tunnel_ids_tunnelendpoint_direct_eh_close_.find(tunnel_id));
    if(j == tunnel_ids_tunnelendpoint_direct_eh_close_.end()) {
    	return;
    }
	tunnel_ids_tunnelendpoint_recived_closed_.erase(tunnel_id);
	tunnel_ids_tunnelendpoint_direct_eh_close_.erase(tunnel_id);

	tunnel_ids_ready_.erase(tunnel_id);
    tunnel_ids_bussy_.erase(tunnel_id);

    // Tunnelendpoint removed indirect when close() is called
    //remove_tunnelendpoint(tunnel_id);

	std::map<unsigned long, TunnelendpointDirect::APtr>::iterator h(direct_tunnelendpoints_.find(tunnel_id));
	if (h != direct_tunnelendpoints_.end()) {
		direct_tunnelendpoints_.erase(h);
	}

    tunnel_ids_free_.push_back(tunnel_id);
    check_for_connection_request();
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_message(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_message");
    switch (state_direct_) {
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_message");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_message", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));
    if (direct_tunnel_eventhandler_ != NULL) {
        direct_tunnel_eventhandler_->tunnelendpoint_direct_eh_recieve(message);
    }
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_checkpoint_response(const unsigned long tunnel_id, TunnelendpointTCPMessage::ReturnCodeType rc) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointTCPTunnelClient::tunnelendpoint_recieve_checkpoint_response");
    switch (state_direct_) {
    case StateDirect_InitCheckpointTunnel:
        break;
    case StateDirect_Closed:
    case StateDirect_Closing:
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_recieve_checkpoint_response");
    }
    Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_recieve_checkpoint_response", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)), CpAttr("rc", rc));
    goto_ready_state_();
}

void TunnelendpointDirectTunnelClient::goto_ready_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::goto_ready_state_");
    state_direct_ = StateDirect_Ready;
    if (direct_tunnel_eventhandler_ != NULL) {
        direct_tunnel_eventhandler_->tunnelendpoint_direct_eh_ready();
    }
}

void TunnelendpointDirectTunnelClient::goto_closed_state_(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::goto_closed_state_");
    state_direct_ = StateDirect_Closed;
    if (direct_tunnel_eventhandler_ != NULL) {
        TunnelendpointDirectTunnelClientEventhandler* direct_tunnel_eventhandler_temp(direct_tunnel_eventhandler_);
        reset_direct_eventhandler();
        direct_tunnel_eventhandler_temp->tunnelendpoint_direct_eh_closed();
    }
    TunnelendpointReliableCrypted::close(true);
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_direct_send(const Utility::DataBufferManaged::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_send");
    switch (state_direct_) {
    case StateDirect_Ready:
        break;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_direct_send");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_send",
                        Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string()));

    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_message, 0,TunnelendpointTCPMessage::ReturnCodeType_ok));
    message_tcp->set_payload(message);
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_eof(const unsigned long tunnel_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_eof");
    switch (state_direct_) {
    case StateDirect_Ready:
    case StateDirect_Closing:
        break;
    case StateDirect_Closing_no_connection:
    case StateDirect_Closed:
        // Ignoring signal
        return;
    default:
        throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_direct_eh_eof");
    }
    CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_eof", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));
    TunnelendpointTCPMessage::APtr message_tcp(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_eof, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
    tunnelendpoint_send(MessagePayload::create(message_tcp->create_as_buffer()));
}

void TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_closed(const unsigned long tunnel_id) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_closed");
	switch (state_direct_) {
	case StateDirect_Ready:
	case StateDirect_Closing:
	case StateDirect_Closing_no_connection:
		break;
	case StateDirect_Closed:
		return;
	default:
		throw ExceptionUnexpected("TunnelendpointDirectTunnelClient not ready for tunnelendpoint_direct_eh_closed");
	}
	CheckpointScope cps(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::tunnelendpoint_direct_eh_closed", Attr_Communication(), CpAttr_debug(), CpAttr("abs_id", get_id_abs_to_string(tunnel_id)));

    tunnel_ids_tunnelendpoint_direct_eh_close_.insert(tunnel_id);
    clean_up_tunnelendpoint(tunnel_id);

	if (state_direct_ != StateDirect_Closing_no_connection) {
		TunnelendpointTCPMessage::APtr message(TunnelendpointTCPMessage::create(TunnelendpointTCPMessage::MessageType_closed, tunnel_id, TunnelendpointTCPMessage::ReturnCodeType_ok));
		tunnelendpoint_send(MessagePayload::create(message->create_as_buffer()));
	}

	if (state_direct_ == StateDirect_Closing || state_direct_ == StateDirect_Closing_no_connection) {
		if (direct_tunnelendpoints_.empty()) {
			close_state_all_connection_closed_ = true;
			goto_closed_state_();
		}
	}
}

bool TunnelendpointDirectTunnelClient::async_mem_guard_is_done(void) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::async_mem_guard_is_done");
    switch (state_direct_) {
    case StateDirect_Closed:
    case StateDirect_Unknown:
        break;
    default:
        return false;
    }
    if(!close_state_all_connection_closed_) {
    	return false;
    }
    return TunnelendpointReliableCrypted::async_mem_guard_is_done();
}

std::string TunnelendpointDirectTunnelClient::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

Utility::CheckpointHandler::APtr TunnelendpointDirectTunnelClient::enable_checkpoint_tunnel(const Utility::CheckpointFilter::APtr& checkpoint_filter, const Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointDirectTunnelClient::enable_checkpoint_tunnel");
    switch (state_direct_) {
    case StateDirect_Initializing:
        break;
    default:
    	Checkpoint cp(*checkpoint_handler_, "TunnelendpointDirectTunnelClient::enable_checkpoint_tunnel.invalid_state", Attr_Communication(), CpAttr_error());
        return Utility::CheckpointHandler::create_ignore();
    }

    if(checkpoint_tunnel_enabled_) {
    	return checkpoint_tunnel_->get_checkpoint_handler();
    }

	checkpoint_tunnel_enabled_ = true;
	checkpoint_tunnel_ = TunnelendpointCheckpointHandlerClient::create(checkpoint_handler_, io_, checkpoint_handler_, checkpoint_filter, checkpoint_output_handler);
	checkpoint_tunnel_->set_mutex(get_mutex());

	checkpoint_tunnel_checkpoint_handler_ = checkpoint_tunnel_->get_checkpoint_handler();
	return checkpoint_tunnel_->get_checkpoint_handler();
}
