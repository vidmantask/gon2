/*! \file COM_SessionManagerConnectionGroup.cxx
 \brief This file contains the impplementation for the COM_SessionManagerConnectionGroup classes
 */

#include <component/communication/COM_SessionManagerConnectionGroup.hxx>
#include <component/communication/COM_Servers_Spec.hxx>

#include <lib/cryptfacility/CF_Crypt_CryptoPP.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * SessionManagerConnectionGroup implementation
 * ------------------------------------------------------------------
 */
SessionManagerConnectionGroup::SessionManagerConnectionGroup(const std::string& title,
                                                             const boost::posix_time::time_duration& selection_delay) :
    title_(title), selection_delay_(selection_delay) {

}

SessionManagerConnectionGroup::~SessionManagerConnectionGroup(void) {
}

SessionManagerConnectionGroup::APtr SessionManagerConnectionGroup::create(const std::string& title,
                                                                          const boost::posix_time::time_duration& selection_delay) {
    return SessionManagerConnectionGroup::APtr(new SessionManagerConnectionGroup(title, selection_delay));
}

SessionManagerConnectionGroup::APtr SessionManagerConnectionGroup::create(const ServersSpec::ServersConnectionGroup& servers_connection_group) {
    return SessionManagerConnectionGroup::APtr(new SessionManagerConnectionGroup(servers_connection_group.title, servers_connection_group.selection_delay));
}


SessionManagerConnection::APtr SessionManagerConnectionGroup::select_connection(void) {

    std::vector<SessionManagerConnection::APtr> connections_remaining;

    for (const SessionManagerConnection::APtr& connection : connections_) {
        if (connection->get_state() == SessionManagerConnection::State_not_connected) {
            connections_remaining.push_back(connection);
        }
    }
    if(connections_remaining.size() == 0) {
        return SessionManagerConnection::APtr();
    }

    CryptFacility::RandomNumberGenerator rng;
    unsigned long pos(rng.GenerateWord32(0, connections_remaining.size() - 1));
    return connections_remaining.at(pos);
}

bool SessionManagerConnectionGroup::has_pending_connections(void) const {
    for (const SessionManagerConnection::APtr& connection : connections_) {
        if (connection->is_pending()) {
            return true;
        }
    }
    return false;
}

boost::posix_time::time_duration SessionManagerConnectionGroup::get_selection_delay(void) const {
    return selection_delay_;
}

void SessionManagerConnectionGroup::add_connection(const SessionManagerConnection::APtr& connection) {
    connections_.push_back(connection);
}
