/*! \file COM_StreamToMessage.hxx
 \brief This file contains classes for converting a stream to messages
 */
#ifndef COM_StreamToMessage_HXX
#define COM_StreamToMessage_HXX

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_Types.hxx>
#include <lib/utility/UY_DataBuffer.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_Message.hxx>

namespace Giritech {
namespace Communication {

/*! \brief StreamToMessageConsumer 
 
 Interface to peek/consume functionality of the StreamToMessage 
 
 */
class StreamToMessageConsumer : public boost::noncopyable {
public:
    typedef boost::shared_ptr<StreamToMessageConsumer> APtr;

    /*! \brief This exception is thrown if a peek or consume operation is requesting access outside the available stream 
     */
    class ExceptionPeekError : public Exception {
public:
        ExceptionPeekError(void) :
            Exception("ExceptionPeekError") {
        }
    };

    /*! \brief Peek available bytes in buffer 
     */
    virtual unsigned long peek_available(void) const = 0;

    /*! \brief Peek a byte at a given offset 
     * 
     * Throws ExceptionPeekError if it not possible to peek the byte at the given offset.
     */
    virtual boost::uint8_t peek_byte(const unsigned long offset) const = 0;

    /*! \brief Peek buffer from a given offset with a given length 
     * 
     * Throws ExceptionPeekError if it not possible to peek a buffer at the given offset.
     */
    virtual Utility::DataBufferManaged::APtr peek_buffer(const unsigned long offset,
                                                         const unsigned long size) const = 0;

    /*! \brief Peek buffer from a given offset with a given length 
     * 
     * Throws ExceptionPeekError if it not possible to consume the requested size.
     */
    virtual void consume(const unsigned long size) = 0;

    /*! \brief Dump a text representation of the stream content for debug purpose 
     */
    virtual void dump(void) const = 0;
    virtual std::string dump_to_str(void) const = 0;
};

/*! \brief Functionality for converting a stream into messages
 * 
 * A stream contains a number of data-chunks of different sizes added by the push_data method.
 * To avoid copying chunks a pointer to the original data-chunk is kept. 
 * A data-chunk is released when all data in the chunk has been consumed.
 * 
 * In the following example 9 data-chuks has been added:
 * \code
 * StreamToMessage dump:
 * available_:45
 * used_of_first_buffer_:0
 * chunks(9):
 *   size:1 data:A
 *   size:2 data:BB
 *   size:3 data:CCC
 *   size:4 data:DDDD
 *   size:5 data:EEEEE
 *   size:6 data:FFFFFF
 *   size:7 data:GGGGGGG
 *   size:8 data:HHHHHHHH
 *   size:9 data:IIIIIIIII
 * \endcode
 * 
 * 
 * A byte-peek operation for the byte with offset 4 will in this example return \c C
 *  
 * 
 * In the following example 4 bytes has been consumed. 
 * Notice that the two data-chunks has been released, and because they only contained 3 bytes (and 4 was consumed)
 * the first byte in the data-chunk \c CCC has been marked using the variabel \c used_of_first_buffer_ . 
 * \code 
 * StreamToMessage dump:
 * available_:41
 * used_of_first_buffer_:1
 * chunks(7):
 *   size:3 data:CCC
 *   size:4 data:DDDD
 *   size:5 data:EEEEE
 *   size:6 data:FFFFFF
 *   size:7 data:GGGGGGG
 *   size:8 data:HHHHHHHH
 *   size:9 data:IIIIIIIII
 * \endcode
 *
 */
class StreamToMessage : public StreamToMessageConsumer {
public:
    typedef boost::shared_ptr<StreamToMessage> APtr;

    /*! \brief destructor 
     */
    ~StreamToMessage(void);

    /*! \brief Recive part of stream 
     */
    void push_data(const Utility::DataBuffer::APtr& data);

    /*! \brief Pop a message from buffer, return null if no message is available 
     */
    MessageCom::APtr pop_message_com(void);

    /*! \brief Pop a message from buffer, return null if no message is available 
     * */
    MessageRaw::APtr pop_message_raw(void);

    /*! \brief Returns false stream is empty 
     */
    bool empty(void) const;

    /*! \brief Dump a text representation of the stream content for debug purpose 
     */
    void dump(void) const;
    std::string dump_to_str(void) const;

    /*! \brief Peek available bytes in buffer 
     */
    unsigned long peek_available(void) const;

    /* \brief Get buffer
     */
    Utility::DataBufferManaged::APtr get_remaining(void) const;

    /*! \brief Peek a byte at a given offset 
     * 
     * Throws ExceptionPeekError if it not possible to peek the byte at the given offset.
     */
    boost::uint8_t peek_byte(const unsigned long offset) const;

    /*! \brief Peek buffer from a given offset with a given length 
     * 
     * Throws ExceptionPeekError if it not possible to consume the requested size.
     */
    Utility::DataBufferManaged::APtr peek_buffer(const unsigned long offset,
                                                 const unsigned long size) const;

    /*! \brief Peek buffer from a given offset with a given length 
     * 
     * Throws ExceptionPeekError if it not possible to consume the requested size.
     */
    void consume(const unsigned long size);

    /*! \brief Create instance 
     */
    static APtr create(void);

private:
    StreamToMessage(void);

    void get_buffer_chunk(const Utility::DataBuffer::APtr& buffer,
                          unsigned long& chunk_offset,
                          unsigned long& chunk_size) const;

    void get_first_buffer_chunk(const Utility::DataBuffer::APtr& buffer,
                                unsigned long& chunk_offset,
                                unsigned long& chunk_size) const;

    unsigned long available_;
    unsigned long used_of_first_buffer_;
    std::vector<Utility::DataBuffer::APtr> buffers_;
};

}
} // End Namespace, Giritech
#endif
