/*! \file UTest_COM_SessionCrypter.cxx
 * \brief This file contains unittest suite for the session crypter functionality
 */
#include <string>

#include <boost/test/unit_test.hpp>

#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/communication/COM_SessionCrypter.hxx>
#include <component/communication/COM_SessionCrypterEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;

/*!
 *  Demo server
 */
class UTest_COM_SessionCrypter_Server : public RawTunnelendpointAcceptorTCPEventhandler,
    public AsyncContinuePolicy, public SessionCrypterEventhandler,
    public SessionCrypterEventhandlerReady {
public:
    UTest_COM_SessionCrypter_Server(const CheckpointHandler::APtr& checkpoint_handler,
                                    boost::asio::io_service& io,
                                    const std::string& host,
                                    const unsigned long port,
                                    const DataBufferManaged::APtr& known_secret) :
        checkpoint_handler_(checkpoint_handler), known_secret_(known_secret), accepted_(false),
                key_exchange_complete_(false), data_recived_(0) {
        acceptor_ = RawTunnelendpointAcceptorTCP::create(checkpoint_handler,
                                                         io,
                                                         host,
                                                         port,
                                                          this,
                                                          this);
        acceptor_->set_option_reuse_address(true);
    }

    void listen(void) {
        acceptor_->aio_accept_start();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_accepted(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        accepted_ = true;
        session_crypter_ = SessionCrypterServer::create(checkpoint_handler_,
                                                        tunnelendpoint,
                                                        known_secret_);
        session_crypter_->set_eventhandler(this);
        session_crypter_->set_eventhandler_ready(this);
        session_crypter_->do_key_exchange();
    }

    /*! \brief signal from RawTunnelendpointAcceptorTCPEventhandler
     */
    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
    }
    void com_tunnelendpoint_acceptor_closed(void) {
    }

    /*! \brief Signal from AsyncContinuePolicy
     */
    bool com_accept_continue(void) {
        return false;
    }

    /*! \brief Signal from SessionCrypterEventhandler
     */
    void session_crypter_key_exchange_complete(void) {
        key_exchange_complete_ = true;
    }

    /*! \brief Signal that the key-exchange failed
     */
    void session_crypter_key_exchange_failed(const std::string& error_message) {
    }

    /*! \brief Signal from SessionCrypterEventhandler
     */
    void session_crypter_closed(void) {
    }

    /*! \brief Signal from SessionCrypterEventhandlerReady
     */
    void session_crypter_recieve(const DataBuffer::APtr& data) {
        data_recived_ += 1;
    }

    bool accepted_;
    bool key_exchange_complete_;
    unsigned long data_recived_;
    SessionCrypterServer::APtr session_crypter_;

private:
    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointAcceptorTCP::APtr acceptor_;
};

/*!
 *  Demo client
 */
class UTest_COM_SessionCrypter_Client : public RawTunnelendpointConnectorTCPEventhandler,
    public SessionCrypterEventhandler, public SessionCrypterEventhandlerReady {
public:
    UTest_COM_SessionCrypter_Client(const CheckpointHandler::APtr& checkpoint_handler,
                                    boost::asio::io_service& io,
                                    const std::string& host,
                                    const unsigned long port,
                                    const DataBufferManaged::APtr& known_secret) :
        checkpoint_handler_(checkpoint_handler), known_secret_(known_secret), connected_(false),
                key_exchange_complete_(false), data_recived_(0) {
        connector_
                = RawTunnelendpointConnectorTCP::create(checkpoint_handler, io, host, port, this);
    }

    void connect(void) {
        connector_->aio_connect_start();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connected(RawTunnelendpointTCP::APtr& tunnelendpoint) {
        connected_ = true;
        session_crypter_ = SessionCrypterClient::create(checkpoint_handler_,
                                                        tunnelendpoint,
                                                        known_secret_);
        session_crypter_->set_eventhandler(this);
        session_crypter_->set_eventhandler_ready(this);
        session_crypter_->do_key_exchange();
    }

    /*! \brief signal from RawTunnelendpointConnectorTCPEventhandler
     */
    void com_tunnelendpoint_connection_failed_to_connect(const std::string& messag) {
    }

    /*! \brief Signal from SessionCrypterEventhandler
     */
    void session_crypter_key_exchange_complete(void) {
        key_exchange_complete_ = true;
    }

    /*! \brief Signal that the key-exchange failed
     */
    void session_crypter_key_exchange_failed(const std::string& error_message) {
    }

    /*! \brief Signal from SessionCrypterEventhandler
     */
    void session_crypter_closed(void) {
    }

    /*! \brief Signal from SessionCrypterEventhandlerReady
     */
    void session_crypter_recieve(const DataBuffer::APtr& data) {
        data_recived_ += 1;
    }

    bool connected_;
    bool key_exchange_complete_;
    unsigned long data_recived_;
    SessionCrypterClient::APtr session_crypter_;

private:
    CheckpointHandler::APtr checkpoint_handler_;
    DataBufferManaged::APtr known_secret_;
    RawTunnelendpointConnectorTCP::APtr connector_;
};



/* Create checkpoint handler */
CheckpointFilter::APtr checkpoint_filter(CheckpointFilter_true::create());
CheckpointOutputHandler::APtr checkpoint_output_handler(CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create()));
CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create(checkpoint_filter,
                                                                     checkpoint_output_handler));
/*
 * \test UTest_COM_SessionCrypter: Simpel test of key_exchange
 */
BOOST_AUTO_TEST_CASE( session_crypter_key_exchange )
{
    DataBufferManaged::APtr known_secret_client;
    DataBufferManaged::APtr known_secret_server;
    KeyExchange_SessionKeyExchange::generateClientServerKnownSecrect(known_secret_client, known_secret_server);

    /* Create asynchron io handler */
    boost::asio::io_service ios;

    std::string host("127.0.0.1");
    unsigned long port = 8045;

    UTest_COM_SessionCrypter_Server server(checkpoint_handler, ios, host, port, known_secret_server);
    UTest_COM_SessionCrypter_Client client(checkpoint_handler, ios, host, port, known_secret_client);

    server.listen();
    client.connect();
    ios.run_for(boost::asio::chrono::seconds(1));

    BOOST_CHECK( server.accepted_ );
    BOOST_CHECK( client.connected_ );
    BOOST_CHECK( server.key_exchange_complete_ );
    BOOST_CHECK( client.key_exchange_complete_ );

    /*
     * Test encryption and decryption
     */
    string message("Hej med dig");
    DataBuffer::APtr s_message_01(DataBufferManaged::create(message));

    DataBuffer::APtr s_buffer_01(server.session_crypter_->encrypt_block(s_message_01));
    BOOST_CHECK( message != s_buffer_01->toString());

    DataBuffer::APtr c_message_01(client.session_crypter_->decrypt_block(s_buffer_01));
    BOOST_CHECK( message == c_message_01->toString());

    DataBuffer::APtr c_buffer_02(client.session_crypter_->encrypt_block(c_message_01));
    BOOST_CHECK( message != c_buffer_02->toString());

    DataBuffer::APtr s_message_02(server.session_crypter_->decrypt_block(c_buffer_02));
    BOOST_CHECK( message == s_message_02->toString());
}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
