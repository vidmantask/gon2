/*! \file COM_RawTunnelendpointAcceptor.hxx
 * \brief This file contains the RawTunnelendpointAcceptor abstraction
 */
#ifndef COM_RawTunnelendpointAcceptor_HXX
#define COM_RawTunnelendpointAcceptor_HXX

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <boost/asio.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointAcceptorEventhandler.hxx>
#include <component/communication/COM_AsyncContinuePolicy.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This defines a raw tunnelendpoint acceptor
 *
 * A acceptor set up a listen port and creates a raw tunnelendpoint each time a connection is made to the port.
 */
class RawTunnelendpointAcceptorTCP : public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<RawTunnelendpointAcceptorTCP> APtr;

    /*! \brief Destructor
     */
    ~RawTunnelendpointAcceptorTCP(void);

	void set_option_reuse_address(const bool& option_reuse_address);
	void set_option_listen(const int option_listen);

	Utility::Mutex::APtr get_mutex(void);
    void set_mutex(const Utility::Mutex::APtr& mutex);

    void reset_eventhandler(void);

    /*! \brief Start accepting asynchron connection
     *
     * When a connection is accepted, the acceptor eventhandler is signaled using
     * the signal RawTunnelendpointAcceptorTCPEventhandler::com_tunnelendpoint_accepted().
     */
    void aio_accept_start(void);

    /*! \brief Stop accepting connections (Pending accepts are canceled)
     */
    void aio_accept_stop(void);

    /*! \brief Close the acceptor
     */
    void aio_close_start(void);

    /*! \brief ignore accept between start and stop
     */
    void aio_accept_ignore_start(void);
    void aio_accept_ignore_stop(void);

    /*! \brief Return true if the acceptor has been closed
     */
    bool is_closed(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Return local endpoint info
     */
    std::pair<std::string, unsigned long> get_ip_local(void);

    /*! \brief Create instance
     *
     * \param checkpoint_handler   The checkpoint handler that is signaled in case of events
     * \param io                   The asyncon serverice handler to use
     * \param listen_host          The host where the accetptor should accept connections on
     * \param listen_port          The port where the accetptor should accept connections on
     * \return A instance under reference count control
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const std::string& listen_host,
                       const long listen_port,
                       RawTunnelendpointAcceptorTCPEventhandler* eventhandler,
                       AsyncContinuePolicy* async_continue_policy);

    /*! \brief Returns next connection id
     */
    static unsigned long get_connection_id_next(void);

private:

    /*! \brief Constructor
     *
     *  \see create()
     */
            RawTunnelendpointAcceptorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                         boost::asio::io_service& io,
                                         const std::string& listen_host,
                                         const long listen_port,
                                         RawTunnelendpointAcceptorTCPEventhandler* eventhandler,
                                         AsyncContinuePolicy* async_continue_policy);

    /*! \brief Handle asynchron connection and signal the event handler
     */
    void aio_accept(RawTunnelendpointTCP::APtr tunnelendpoint, const boost::system::error_code& error);

    void call_com_tunnelendpoint_acceptor_error_decoubled(const std::string& message);

    void aio_close_final(void);

    enum State {
        State_Initializing, State_Ready, State_Closing, State_Closed, State_Done, State_Unknown
    };
    boost::asio::io_service& io_;
    State state_;

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
    boost::asio::ip::tcp::endpoint listen_endpoint_;

    RawTunnelendpointAcceptorTCPEventhandler* eventhandler_;
    AsyncContinuePolicy* async_continue_policy_;
    boost::asio::ip::tcp::acceptor acceptor_;
    bool option_reuse_address_;
    int option_listen_;

    Utility::Mutex::APtr mutex_;
    bool pending_accept_exists_;
    bool accept_ignore_;
    bool pending_accept_errors_exists_;
};

}
}
#endif
