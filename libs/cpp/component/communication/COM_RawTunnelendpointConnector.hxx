/*! \file COM_RawTunnelendpointConnector.hxx
 * \brief This file contains the RawTunnelendpointConnector abstraction
 */
#ifndef COM_RawTunnelendpointConnector_HXX
#define COM_RawTunnelendpointConnector_HXX

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>


#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>


namespace Giritech {
namespace Communication {

class RawTunnelendpointConnectorTCPConnectionStatus {
public:
    typedef boost::shared_ptr<RawTunnelendpointConnectorTCPConnectionStatus> APtr;

    ~RawTunnelendpointConnectorTCPConnectionStatus(void);

    static APtr create(
    		boost::asio::io_service& io,
    		const RawTunnelendpointTCP::APtr& raw_tunnelendpoint);

    boost::asio::steady_timer& get_timeout_timer(void);

    enum State {
        State_Init = 0,
        State_Resolving,
        State_Resolved,
        State_Connecting,
        State_Connected,
        State_Unknown
    };
    State get_state(void);

    void set_state_resolving(void);
    void set_state_resolved(void);
    void set_state_connecting(void);
    void set_state_connected(void);

    void set_timeout(void);
    bool is_timeout(void);
    bool has_async_pending(void);

    RawTunnelendpointTCP::APtr get_tunnelendpoint(void);

private:
    RawTunnelendpointConnectorTCPConnectionStatus(boost::asio::io_service& io, const RawTunnelendpointTCP::APtr& raw_tunnelendpoint);

    RawTunnelendpointTCP::APtr raw_tunnelendpoint_;
    boost::asio::steady_timer timeout_timer_;
    State state_;
    bool timeout_;
};


/*! \brief This class defines a raw tunnelendpoint connector
 *
 * A connector connect asyncron to a remote host:port and signals the eventhandler when connected.
 */
class RawTunnelendpointConnectorTCP : public Giritech::Utility::AsyncMemGuardElement {
public:
    typedef boost::shared_ptr<RawTunnelendpointConnectorTCP> APtr;

    /*! \brief Destructor
     */
    ~RawTunnelendpointConnectorTCP(void);

    Utility::Mutex::APtr get_mutex(void);
    void set_mutex(const Utility::Mutex::APtr& mutex);

    void set_connect_timeout(const boost::posix_time::time_duration& connect_timeout);

    /*! \brief Ask for a connection to a remote raw endpoint
     *
     * When a connection is created, the connectors eventhandler is signaled using the
     * signal RawTunnelendpointConnectorTCPEventhandler::com_tunnelendpoint_connected.
     */
    void aio_connect_start(void);

    /*! \brief Create instance
     *
     *  \param checkpoint_handler   The checkpoint handler that is signaled in case of events
     *  \param io                   The asyncon serverice handler to use
     *  \param remote_host          The host where the connector should connect to
     *  \param remote_port          The port on the remote host where the connector should connect to
     *  \return                     A instance under reference count control
     */
    static APtr create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                       boost::asio::io_service& io,
                       const std::string& remote_host,
                       const long remote_port,
                       RawTunnelendpointConnectorTCPEventhandler* eventhandler);


    void reset_eventhandler(void);

    /*
     * Force close of pending connect attempts
     */
    void close(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);

    /*! \brief Returns next connection id
     */
    static unsigned long get_connection_id_next(void);

    /*! \brief Enable SSL for connections
     */
    void enable_ssl(const bool& disable_certificate_verification);

private:

    /*! \brief Constructor
     *
     *  \see create()
     */
    RawTunnelendpointConnectorTCP(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
    		boost::asio::io_service& io,
    		const std::string& remote_host,
    		const long remote_port,
    		RawTunnelendpointConnectorTCPEventhandler* eventhandler);

    /*! \brief Handle asynchron connection and signal the event handler
     */
    void aio_resolve(
    		const boost::system::error_code& error,
    		const unsigned long connection_id,
    		boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

    void aio_connect(
    		const boost::system::error_code& error,
    		const unsigned long connection_id,
    		boost::asio::ip::tcp::resolver::iterator endpoint_iterator);

    void aio_connect_conected(const RawTunnelendpointConnectorTCPConnectionStatus::APtr& connection_status);

    void aio_connect_timeout(const unsigned long connection_id);

    void close_final(void);

    void aio_handle_handshake(
    		const boost::system::error_code& error,
    		const RawTunnelendpointConnectorTCPConnectionStatus::APtr& connection_status);


    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
    boost::asio::io_service& io_;
    boost::asio::ip::tcp::resolver resolver_;

    std::string remote_host_;
    long remote_port_;
    RawTunnelendpointConnectorTCPEventhandler* eventhandler_;

    Utility::Mutex::APtr mutex_;

    boost::posix_time::time_duration connect_timeout_;

    typedef std::map<unsigned long, RawTunnelendpointConnectorTCPConnectionStatus::APtr> ConnectionsStatus;
    ConnectionsStatus raw_tunnelendpoints_status_;

    enum State {
        State_ReadyToConnect = 0,
        State_Closing,
        State_Closed
    };
    State state_;

    bool ssl_enabled_;
    bool ssl_disable_certificate_verification_;
};
}
}
#endif
