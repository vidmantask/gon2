/*! \file UTest_COM_TunnelendpointDirect.cxx
 \brief This file contains unittest suite for tunnelendpoint-direct abstraction
 */
#include <vector>
#include <string>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointTCP.hxx>
#include <component/communication/COM_TunnelendpointDirectEventhandler.hxx>
#include <component/communication/COM_TunnelendpointDirect.hxx>
#include <component/communication/COM_TunnelendpointDirectHttpProxy.hxx>

#include <component/http_client/HttpClient.hxx>

#include <component/communication/asimulator/COM_ASimulator.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::HttpClient;



CheckpointHandler::APtr checkpoint_handler_ignore(CheckpointHandler::create_cout_all());
CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());


/*
 * General helper calasses for unittesting
 */
class UnittestTunnelendpointConnector: public TunnelendpointConnector {
private:
	AsyncService::APtr async_service_;
    boost::asio::io_service::strand strand_;


public:
    UnittestTunnelendpointConnector(const AsyncService::APtr& async_service) : strand_(async_service->get_io_service()){
    	async_service_ = async_service;
    }

    void tunnelendpoint_connector_send(const MessageCom::APtr& message) {
    	strand_.post(boost::bind(&UnittestTunnelendpointConnector::tunnelendpoint_connector_send_aync, this, message));
    }

    void tunnelendpoint_connector_send_aync(const MessageCom::APtr& message) {
        if (tunnelendpoint_ != NULL) {
            tunnelendpoint_->tunnelendpoint_recieve(message);
        }
    }


    void tunnelendpoint_connector_closed(void) {
    }

    void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
        // Ignored
    }

    std::string tunnelendpoint_connector_get_unique_session_id(void) {
    	return "hej";
    }

    Tunnelendpoint* tunnelendpoint_;
};


static int counter_ = 0;
static string CRLF = "\r\n";


//class TunnelendpointDirectWrapper : public TunnelendpointDirectEventhandler {
//private:
//		TunnelendpointDirect::APtr tunnelendpoint_direct_;
//
//public:
//    typedef boost::shared_ptr<TunnelendpointDirectWrapper> APtr;
//
//
//    TunnelendpointDirectWrapper(const TunnelendpointDirect::APtr& tunnelendpoint_direct) {
//    	counter_++;
//
//    	tunnelendpoint_direct_ = tunnelendpoint_direct;
//    	tunnelendpoint_direct_->set_direct_eventhandler(this);
//
//    	stringstream ss;
//    	ss << "GET http://hg.dev/ HTTP/1.1" << CRLF;
////    	ss << "Host: hg.dev"<< CRLF;
////    	ss << "Connection: keep-alive"<< CRLF;
////    	ss << "User-Agent: Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11"<< CRLF;
////    	ss << "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"<< CRLF;
////    	ss << "Accept-Encoding: gzip,deflate,sdch"<< CRLF;
////    	ss << "Accept-Language: en-US,en;q=0.8"<< CRLF;
////    	ss << "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3"<< CRLF;
//    	ss << CRLF << CRLF;
//
//    	cout << ss.str();
//
//    	tunnelendpoint_direct_->write(DataBufferManaged::create(ss.str()));
//    }
//
//    void tunnelendpoint_direct_eh_closed() {
//        cout << "TunnelendpointDirectWrapper.tunnelendpoint_direct_eh_closed" << endl;
//    }
//
//    void tunnelendpoint_direct_eh_recieve(const DataBufferManaged::APtr& message) {
//        cout << "TunnelendpointDirectWrapper.tunnelendpoint_direct_eh_recieve.begin" << endl;
//        cout << message->toString() << endl;
//        cout << "TunnelendpointDirectWrapper.tunnelendpoint_direct_eh_recieve.end" << endl;
//
//    }
//};



class UnittestRunner : public TunnelendpointDirectTunnelClientEventhandler, public HttpClientEventhandler {
private:
	TunnelendpointDirectTunnelClient::APtr direct_tunnel_client_;

	std::vector<HttpClient::APtr> clients_;

	bool done_;
	 AsyncService::APtr async_service_;


public:
	UnittestRunner(const AsyncService::APtr& async_service, const TunnelendpointDirectTunnelClient::APtr& direct_tunnel_client) {
		async_service_ = async_service;
		direct_tunnel_client_ = direct_tunnel_client;
		direct_tunnel_client_->set_direct_eventhandler(this);
		done_ = false;
	}

	/*! Implementation of TunnelendpointDirectTunnelClientEventhandler
     */
    void tunnelendpoint_direct_eh_connection_ready(const TunnelendpointDirect::APtr& connection) {
        cout << "UnittestRunner.tunnelendpoint_direct_eh_connection_ready" << endl;

        TunnelendpointDirectHttpProxy::APtr client_com(TunnelendpointDirectHttpProxy::create(connection->get_checkpoint_handler(), connection));
        client_com->enable_http_proxification();

        HttpCookieStore::APtr cookie_store(HttpCookieStore::create(async_service_->get_io_service(), connection->get_checkpoint_handler()));

        HttpClient::APtr http_client(HttpClient::create(async_service_->get_io_service(), connection->get_checkpoint_handler(), client_com, cookie_store));
    	http_client->set_eventhandler(this);

        clients_.push_back(http_client);

        cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 1" << endl;
        HttpRequest::APtr http_request(HttpRequest::create_get("http://www.dr.dk/"));
        http_client->execute_request(http_request);
        cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 2" << endl;
    }

    void tunnelendpoint_direct_eh_connection_failed(const std::string& errorMessage) {

    }

    void tunnelendpoint_direct_eh_ready(void) {

    }

    void tunnelendpoint_direct_eh_closed(void) {

    }

    void tunnelendpoint_direct_eh_recieve(const DataBufferManaged::APtr& message) {

    }

    void http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const std::vector< std::pair<std::string, std::string> >& headers) {
        cout << "UnittestRunner.http_client_response_header  header_version : " << header_version << " header_status_code :" << header_status_code << endl;
	}

	void http_client_response_body_data(DataBufferManaged::APtr& body_data) {
        cout << "UnittestRunner.http_client_response_body_data" << endl;
	}

	void http_client_response_done(void) {
        cout << "UnittestRunner.http_client_response_done" << endl;
	}

	void http_client_response_error(void) {
        cout << "UnittestRunner.http_client_response_error" << endl;
	}




    void run(void) {
        boost::asio::io_service io;
        boost::asio::deadline_timer t1(io, boost::posix_time::seconds(2));
        t1.wait();

    	for(int i = 0; i < 1; i++) {
            direct_tunnel_client_->request_connection();
    	}

        boost::asio::deadline_timer t2(io, boost::posix_time::seconds(600));
        t2.wait();

    	cout << "UnittestRunner is now done" << endl;
        direct_tunnel_client_->close(true);
        client_tunnelendpoint->close(true);

        while (!(direct_tunnel_client_->is_closed()));
        cout << "UnittestRunner is now over" << endl;
    }

    bool is_done(void) {
        return done_;
    }

    void timeout() {
    	cout << "Timeout" << endl;
    	done_ = true;
    }

    TunnelendpointTCPTunnelClient::APtr client_tunnelendpoint;
};



//BOOST_AUTO_TEST_CASE( direct_tunnelendpoint_basis )
//{
//
//
//
//    boost::thread_group threads;
//	AsyncService::APtr async_service_client(AsyncService::create(checkpoint_handler));
//	AsyncService::APtr async_service_server(AsyncService::create(checkpoint_handler));
//
//    UnittestTunnelendpointConnector client_connector(async_service_client);
//    TunnelendpointDirectTunnelClient::APtr client_tunnelendpoint(TunnelendpointDirectTunnelClient::create(checkpoint_handler, async_service_client->get_io_service(), 2));
//    client_tunnelendpoint->set_connector(&client_connector);
//    client_tunnelendpoint->enable_checkpoint_tunnel(CheckpointFilter_true::create(), CheckpointOutputHandlerTEXT::create());
//
//
//    string server_ip = "127.0.0.1";
//    unsigned long server_ip_port = 1080;
//    UnittestTunnelendpointConnector server_connector(async_service_client);
//	Mutex::APtr mutexServerSession(Mutex::create(async_service_server->get_io_service(), "Server_mutex"));
//    TunnelendpointTCPTunnelServer::APtr server_tunnelendpoint(TunnelendpointTCPTunnelServer::create(checkpoint_handler, async_service_server->get_io_service(), server_ip, server_ip_port, 0));
//    server_tunnelendpoint->set_mutex(mutexServerSession);
//    server_tunnelendpoint->set_connector(&server_connector);
//
//    client_connector.tunnelendpoint_ = &(*server_tunnelendpoint);
//    server_connector.tunnelendpoint_ = &(*client_tunnelendpoint);
//    client_tunnelendpoint->connect();
//    server_tunnelendpoint->connect();
//
//
//
//    boost::asio::deadline_timer timeout_timer_client(async_service_client->get_io_service());
//    timeout_timer_client.expires_from_now(boost::posix_time::seconds(1200));
//
//    boost::asio::deadline_timer timeout_timer_server(async_service_server->get_io_service());
//    timeout_timer_server.expires_from_now(boost::posix_time::seconds(1200));
//
//
//    UnittestRunner unittest_runner(async_service_client, client_tunnelendpoint);
//    timeout_timer_client.async_wait(boost::bind(&UnittestRunner::timeout, &unittest_runner));
//    timeout_timer_server.async_wait(boost::bind(&UnittestRunner::timeout, &unittest_runner));
//    threads.create_thread(boost::bind(&AsyncService::run, &(*async_service_client)));
//    threads.create_thread(boost::bind(&AsyncService::run, &(*async_service_server)));
//
//    threads.create_thread(boost::bind(&UnittestRunner::run, &unittest_runner));
//
//
////    async_service->run();
//    threads.join_all();
//}


BOOST_AUTO_TEST_CASE( direct_tunnelendpoint_basis )
{
    std::vector<unsigned long> tunnel_ids_free_;
	for(int i=1; i<255; i++) {
		tunnel_ids_free_.push_back(i);
	}

    unsigned long new_tunnel_id = 0;
    std::vector<unsigned long>::iterator i(tunnel_ids_free_.begin());
    if(i != tunnel_ids_free_.end()) {
        new_tunnel_id = (*i);
        tunnel_ids_free_.erase(i);
    }
    cout << "new_tunnel_id :" << new_tunnel_id << endl;
    tunnel_ids_free_.push_back(new_tunnel_id);


    i = tunnel_ids_free_.begin();
    if(i != tunnel_ids_free_.end()) {
        new_tunnel_id = (*i);
        tunnel_ids_free_.erase(i);
    }
    cout << "new_tunnel_id :" << new_tunnel_id << endl;
    tunnel_ids_free_.push_back(new_tunnel_id);


}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
