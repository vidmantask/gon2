/*! \file COM_Message.hxx
 \brief This file contains classes for handling messages
 */
#ifndef COM_Message_HXX
#define COM_Message_HXX

#include <vector>

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <component/communication/COM_Exception.hxx>


namespace Giritech {
namespace Communication {

/* Pre decleration */
class StreamToMessageConsumer;

/*! \brief Message
 */
class MessagePayload : public boost::noncopyable {
public:
    typedef boost::shared_ptr<MessagePayload> APtr;

    /* destructor */
    virtual ~MessagePayload(void);

    /* Create buffer of given size */
    static APtr create(const Utility::DataBufferManaged::APtr& payload);

    virtual Utility::DataBufferManaged::APtr get_buffer(void);

protected:
    MessagePayload(void);
    MessagePayload(const Utility::DataBufferManaged::APtr& payload);

    Utility::DataBufferManaged::APtr payload_;
};


/*! \brief Communication raw message
 * Header:
 *      protocol-version    8 bit       (currently 0)
 *      package type        8 bit       (currently 0)
 *      payload size        16 bit      (in bytes)
 * Payload:
 *
 */
class MessageRaw : public boost::noncopyable {
public:
    typedef boost::shared_ptr<MessageRaw> APtr;

    enum PackageType {PackageType_KeyExchange = 0};

    class ExceptionInvalidMessage : public Exception {
    public:
    	ExceptionInvalidMessage(void) :
            Exception("") {
        }
    };

    /* destructor */
    virtual ~MessageRaw(void);

    Utility::DataBufferManaged::APtr create_as_buffer(void);
    MessagePayload::APtr get_payload(void);

    /* Create instance */
    static APtr create_from_stream(StreamToMessageConsumer& stream);
    static APtr create(const PackageType package_type, const MessagePayload::APtr& payload);

private:
    /* Constructor */
    MessageRaw(const PackageType package_type, const MessagePayload::APtr& payload);

    unsigned char protocol_version_;
    PackageType package_type_;

    MessagePayload::APtr payload_;
};

/*! \brief Communication component message
 *
 * Header:
 *      protocol-version    8 bit       (currently 1)
 *      package type        8 bit
 *      header size         16 bit
 *      payload size        16/32 bit   (dependend on package type)
 *      crc                 32 bit      (crc of all fields except crc)
 *      address (repeated until header size):
 *          address-size    8 bit
 * Payload:
 *
 *
 * * Protocolversions:
 *  0 - Only one packagetypes, used in version < 5.2.0
 *  1 - Added the package type SessionData, current
 *
 */
class MessageCom : public boost::noncopyable {
public:
    typedef boost::shared_ptr<MessageCom> APtr;

    enum PackageType {
        PackageType_Data = 0,
        PackageType_SessionData = 1,
        PackageType_TunnelendpointData = 2,
        PackageType_KeepAlivePing = 3,
        PackageType_DataHuge = 4,
        PackageType_TC_Ping = 5,
        PackageType_TC_PingResponse = 6,
        PackageType_KeepAlivePingStart = 7,
        PackageType_KeepAlivePingStop = 8
    };

    /* Holder of a message address element */
    class MessageComAddressElement {
public:
        enum AddressSize {AddressSize_4bit = 0};

        MessageComAddressElement(const unsigned long address_);

        unsigned long get_address(void) const;

private:
        AddressSize address_size_;
        unsigned long address_;
    };

    /* destructor */
    virtual ~MessageCom(void);

    /* Add address element */
    void push_prefix_address_element(const MessageComAddressElement& address_element);

    /* Get address element */
    MessageComAddressElement pop_prefix_address_element(void);

    Utility::DataBufferManaged::APtr create_as_buffer(void);
    MessagePayload::APtr get_payload(void);
    unsigned long get_package_id(void) const;

    unsigned char get_protocol_version(void) const;
    void set_protocol_version(const unsigned char&);

    PackageType get_package_type(void) const;

    std::string get_address_as_string(void);

    /* Create instance */
    static APtr create_from_stream(StreamToMessageConsumer& stream);
    static APtr create(const PackageType package_type, const MessagePayload::APtr& payload, const unsigned long& package_id);
    static APtr create(const PackageType package_type, const MessagePayload::APtr& payload, const unsigned long& package_id, const unsigned char& protocol_version);

private:
    /* Constructor */
    MessageCom(const PackageType package_type, const MessagePayload::APtr& payload);
    MessageCom(const PackageType package_type, const MessagePayload::APtr& payload, const unsigned long& package_id);

    unsigned char protocol_version_;
    unsigned long package_id_;

    PackageType package_type_;

    std::vector<MessageComAddressElement> message_address_elements_;
    MessagePayload::APtr payload_;
};



}
} // End Namespace, Giritech
#endif
