/*! \file COM_API_RawTunnelendpoint.hxx
 *  \brief This file contains the a API wrapper for raw-tunnelendpoints
 */
#ifndef COM_API_RawTunnelendpoint_HXX
#define COM_API_RawTunnelendpoint_HXX

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler for a tcp-tunnelendpoint connection
 */
class APIRawTunnelendpointEventhandler: public boost::noncopyable {
public:

    /*! \brief Signals that the raw tunnelendpoint  has been closed
     *
     *  \param connection_id  Connection identifier
     *
     *  The connection send this signal as a response to call to RawTunnelendpoint::aio_close_start().
     */
    virtual void com_raw_tunnelendpoint_close(const unsigned long& connection_id) = 0;

    /*! \brief Signals that the raw tunnelendpoint has been half-closed
     *
     * The signal is indicating that no more data is comming, but is it still ok to write data.
     */
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) = 0;

    /*! \brief Signals that data has been recived
     *
     *  \param connection_id  Connection identifier
     *  \param data           recived data
     *
     *  The connection send this signal as a response to call to aio_read_start().
     */
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const std::string& data) = 0;

    /*! \brief Signals that the write data buffer is empty
     *
     *  \param connection_id  Connection identifier
     *
     */
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) = 0;
};

/*! \brief API for RawTunnelendpointTCP class see this class for documentation
 */
class APIRawTunnelendpointTCP: public RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<APIRawTunnelendpointTCP> APtr;

    /*! \brief Destructor
     */
    ~APIRawTunnelendpointTCP(void);

    /*! \brief set eventhandler
     */
    void set_tcp_eventhandler(APIRawTunnelendpointEventhandler* api_eventhandler);

    /*! \brief reset eventhandler
     */
    void reset_tcp_eventhandler(void);

    /*! \brief  Request a asynchron read
     */
    void aio_read_start(void);

    /*! \brief Request a asynchron write of a buffer
     */
    void aio_write_start(const std::string& data);

    /*! \brief Request to close the tunnel
     */
    void aio_close_start(const bool force);

    /*! \brief Request to half-close the tunnel, by sending eof
     */
    void aio_close_write_start(void);

    /*! \brief Report back if the tunnelendpoint has been closed
     */
    bool is_closed(void) const;

    /*! \brief Report back if the tunnelendpoint is ready
     */
    bool is_ready(void) const;

    void aio_ssl_start(
    		const std::string& ssl_host_name,
			const bool& disable_certificate_verification);


    /*! \brief Geters of ip/port of local and remote part of socket
     */
    std::pair<std::string, unsigned long> get_ip_local(void) const;
    std::pair<std::string, unsigned long> get_ip_remote(void) const;

    /*! \brief Get instance mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Set instance mutex
     */
    void set_mutex(const APIMutex::APtr& api_mutex);

    /*! \brief Create instance
     */
    static APtr create(const RawTunnelendpointTCP::APtr& impl_tunnelendpoint);

    /*
     * Implementation of eventhandlers
     */
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id);
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);

    /*
     * Self methods for easy python-api defintion
     */
    static void self_set_tcp_eventhandler(const APIRawTunnelendpointTCP::APtr& self, APIRawTunnelendpointEventhandler* api_eventhandler);
    static void self_reset_tcp_eventhandler(const APIRawTunnelendpointTCP::APtr& self);

    static void self_aio_read_start(const APIRawTunnelendpointTCP::APtr& self);
    static void self_aio_write_start(const APIRawTunnelendpointTCP::APtr& self, const std::string& data);
    static void self_aio_close_start(const APIRawTunnelendpointTCP::APtr& self, const bool& force);
    static void self_aio_close_write_start(const APIRawTunnelendpointTCP::APtr& self);
    static bool self_is_closed(const APIRawTunnelendpointTCP::APtr& self);
    static bool self_is_ready(const APIRawTunnelendpointTCP::APtr& self);
    static boost::python::tuple self_get_ip_remote(const APIRawTunnelendpointTCP::APtr& self);
    static boost::python::tuple self_get_ip_local(const APIRawTunnelendpointTCP::APtr& self);
    static void self_set_mutex(const APIRawTunnelendpointTCP::APtr& self, const APIMutex::APtr& api_mutex);
    static APIMutex::APtr self_get_mutex(const APIRawTunnelendpointTCP::APtr& self);
    static void self_aio_ssl_start(const APIRawTunnelendpointTCP::APtr& self, const std::string& ssl_host_name, const bool& disable_certificate_verification);


private:

    /*! \brief Constructor
     *
     * \see create
     */
    APIRawTunnelendpointTCP(const RawTunnelendpointTCP::APtr& impl_tunnelendpoint);

    RawTunnelendpointTCP::APtr impl_tunnelendpoint_;
    APIRawTunnelendpointEventhandler* api_eventhandler_;
};

}
}
#endif
