/*! \file COM_TunnelendpointCheckpointHandler.cxx
 \brief This file contains the implementation for the tunnelendpoint checkpoint_handler class
 */
#include <functional>
#include <boost/bind.hpp>

#include <lib/utility/UY_Convert.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_TunnelendpointCheckpointHandler.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * TunnelendpointCheckpointHandlerServer implementation
 * ------------------------------------------------------------------
 */
TunnelendpointCheckpointHandlerServer::TunnelendpointCheckpointHandlerServer(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler, boost::asio::io_service& io, const boost::filesystem::path& folder)
:  TunnelendpointReliableCryptedTunnel(io, checkpoint_handler, this), folder_(folder), file_is_open_(false) {
}

TunnelendpointCheckpointHandlerServer::~TunnelendpointCheckpointHandlerServer(void) {
}

TunnelendpointCheckpointHandlerServer::APtr TunnelendpointCheckpointHandlerServer::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		boost::asio::io_service& io,
		const boost::filesystem::path& folder) {
	TunnelendpointCheckpointHandlerServer::APtr new_tunnelendpoint(new TunnelendpointCheckpointHandlerServer(checkpoint_handler, io, folder));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

bool TunnelendpointCheckpointHandlerServer::async_mem_guard_is_done(void) {
	return true;
}

std::string TunnelendpointCheckpointHandlerServer::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void TunnelendpointCheckpointHandlerServer::set_mutex(const Utility::Mutex::APtr& mutex) {
    TunnelendpointReliableCryptedTunnel::set_mutex(mutex);
}

void TunnelendpointCheckpointHandlerServer::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointCheckpointHandlerServer::tunnelendpoint_eh_recieve");
    if(file_is_open_) {
    	(*file_) << message->get_buffer()->toString();
    	(*file_).flush();
    }
}

boost::filesystem::path TunnelendpointCheckpointHandlerServer::generate_filename(const boost::filesystem::path& folder, const unsigned long counter) {
	stringstream ss;
	ss << tunnelendpoint_get_unique_session_id() << "_" << std::setfill('0') << std::setw(5) << counter << ".log";
	return folder / ss.str();
}


boost::filesystem::path TunnelendpointCheckpointHandlerServer::generate_unique_filename(const boost::filesystem::path& basis_filename ) {
	unsigned long counter = 0;
	boost::filesystem::path abs_filename(generate_filename(basis_filename, counter));
	while( boost::filesystem::exists(abs_filename)) {
		abs_filename = generate_filename(basis_filename, counter++);
	}
	return abs_filename;
}

void TunnelendpointCheckpointHandlerServer::tunnelendpoint_eh_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointCheckpointHandlerServer::tunnelendpoint_eh_connected");

    if(!boost::filesystem::is_directory(folder_)) {
    	boost::filesystem::create_directories(folder_);
    }


    boost::filesystem::path abs_filename(generate_unique_filename(folder_));
    file_ = boost::shared_ptr<boost::filesystem::ofstream>(new boost::filesystem::ofstream(abs_filename, std::ios::out));
    file_is_open_ = file_->is_open();

	if(file_is_open_) {
		Checkpoint cp(*checkpoint_handler_, "checkpoint_handler_from_client", Attr_Communication(), CpAttr_debug(), CpAttr("filename", abs_filename.string()));
	}
	else {
		Checkpoint cp(*checkpoint_handler_, "checkpoint_handler_from_client.unable_to_create_folder", Attr_Communication(), CpAttr_error(), CpAttr("folder", folder_.string()));
	}
}




/*
 * ------------------------------------------------------------------
 * TunnelendpointCheckpointHandlerClient implementation
 * ------------------------------------------------------------------
 */
TunnelendpointCheckpointHandlerClient::TunnelendpointCheckpointHandlerClient(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
		boost::asio::io_service& io,
		const Giritech::Utility::CheckpointHandler::APtr& parent_checkpoint_handler,
		const Giritech::Utility::CheckpointFilter::APtr& checkpoint_filter,
		const Giritech::Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler)
: TunnelendpointReliableCryptedTunnel(io, checkpoint_handle, this) {
	checkpoint_output_handler->set_destination_handler(CheckpointDestinationHandler::APtr(this));
	tunnel_checkpoint_handler_ = Utility::CheckpointHandler::create(checkpoint_filter, checkpoint_output_handler, *parent_checkpoint_handler);
}

TunnelendpointCheckpointHandlerClient::APtr TunnelendpointCheckpointHandlerClient::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
		boost::asio::io_service& io,
		const Giritech::Utility::CheckpointHandler::APtr& parent_checkpoint_handler,
		const Giritech::Utility::CheckpointFilter::APtr& checkpoint_filter,
		const Giritech::Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler) {
	TunnelendpointCheckpointHandlerClient::APtr new_tunnelendpoint(new TunnelendpointCheckpointHandlerClient(checkpoint_handle, io, parent_checkpoint_handler, checkpoint_filter, checkpoint_output_handler));
	AsyncMemGuard::global_add_element(new_tunnelendpoint);
	return new_tunnelendpoint;
}

TunnelendpointCheckpointHandlerClient::~TunnelendpointCheckpointHandlerClient(void) {
}

Utility::CheckpointHandler::APtr TunnelendpointCheckpointHandlerClient::get_checkpoint_handler(void) {
	return tunnel_checkpoint_handler_;
}

void TunnelendpointCheckpointHandlerClient::set_mutex(const Utility::Mutex::APtr& mutex) {
    TunnelendpointReliableCryptedTunnel::set_mutex(mutex);
}

void TunnelendpointCheckpointHandlerClient::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
	// Messages are ignored
}

void TunnelendpointCheckpointHandlerClient::tunnelendpoint_eh_connected(void) {
	Checkpoint cp(*checkpoint_handler_, "checkpoint_handler_to_server", Attr_Communication(), CpAttr_debug());
}

bool TunnelendpointCheckpointHandlerClient::async_mem_guard_is_done(void) {
	return true;
}

std::string TunnelendpointCheckpointHandlerClient::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

boost::filesystem::path TunnelendpointCheckpointHandlerClient::get_foldername(void) {
	return "";
}

std::ostream& TunnelendpointCheckpointHandlerClient::get_os(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointCheckpointHandlerClient::get_os");
	return ss_;
}

void TunnelendpointCheckpointHandlerClient::close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointCheckpointHandlerClient::close");
	flush();
}

void TunnelendpointCheckpointHandlerClient::flush(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "TunnelendpointCheckpointHandlerClient::flush");
    tunnelendpoint_send(MessagePayload::create(Utility::DataBufferManaged::create(ss_.str())));
    ss_.str("");
}
