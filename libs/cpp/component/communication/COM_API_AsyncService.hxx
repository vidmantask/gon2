/*! \file COM_API_AsyncService.hxx
 *  \brief This file contains the API for the asyncron service
 */
#ifndef COM_API_AsyncService_HXX
#define COM_API_AsyncService_HXX

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <vector>

#include <boost/shared_ptr.hpp>
#include <boost/python.hpp>
#include <boost/python/module.hpp>
#include <boost/python/class.hpp>

#include <component/communication/COM_AsyncService.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_API_Checkpoint.hxx>

namespace Giritech {
namespace Communication {

/*! \brief Class representing a mutex
 *
 */
class APIMutex {
public:
    typedef boost::shared_ptr<APIMutex> APtr;

    /*! \brief Destructor
     */
    ~APIMutex(void);

    bool lock(void);
    bool lock(const unsigned long& seconds);
    void unlock(void);

    Utility::Mutex::APtr get_impl(void);

    static APtr create(const Utility::Mutex::APtr& mutex);

	static bool self_lock(const APIMutex::APtr& self);
	static bool self_lock_with_timeout(const APIMutex::APtr& self,const unsigned long& seconds);
    static void self_unlock(const APIMutex::APtr& self);

private:
    APIMutex(const Utility::Mutex::APtr& mutex);

    Utility::Mutex::APtr mutex_;
};


/*! \brief Class representing a sleeping callback
 */
class APIAsyncServiceSleeper {
public:
    typedef boost::shared_ptr<APIAsyncServiceSleeper> APtr;

    /*! \brief Destructor
     */
    ~APIAsyncServiceSleeper(void);

    /*! \brief Start sleeping
     */
    void sleep_start(const unsigned long& min, const unsigned long& sec, const unsigned long& ms);

    bool is_done(void) const;

    /*!\brief Create instance
     */
    static APtr create(boost::asio::io_service& io_service,
                       const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler,
       		           const Utility::Mutex::APtr& mutex,
                       PyObject* callback_self,
                       const std::string& callback_method_name,
                       const boost::python::object& callback_arg);

private:
    APIAsyncServiceSleeper(boost::asio::io_service& io_service,
                           const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler,
                           const Utility::Mutex::APtr& mutex,
                           PyObject* callback_self,
                           const std::string& callback_method_name,
                           const boost::python::object& callback_arg);

    void sleep_(const boost::system::error_code& error);

    boost::asio::steady_timer timer_;
    boost::asio::io_service::strand timer_strand_;

    bool done_;
    Utility::Mutex::APtr mutex_;

    PyObject* callback_self_;
    std::string callback_method_name_;
    boost::python::object callback_arg_;
    Giritech::Utility::APICheckpointHandler::APtr api_checkpoint_handler_;
};

/*! \brief API for asyncron service
 */
class APIAsyncService {
public:
    typedef boost::shared_ptr<APIAsyncService> APtr;

    /*! \brief Destructor
     */
    ~APIAsyncService(void);

    /*
     * Request a async sleep with a callback to a named python method on a given object
     */
    void sleep_start(const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler,
    		         const Utility::Mutex::APtr& mutex,
                     const unsigned long& min,
                     const unsigned long& sec,
                     const unsigned long& ms,
                     PyObject* callback_self,
                     const std::string& callback_method_name,
                     const boost::python::object& callback_arg);

    /*
     * Return asyncron io_service
     */
    boost::asio::io_service& get_io_service(void);

    /*
     * Run async service
     */
    void run(void);

    /*
     * Stop async service
     */
    void stop(void);

    /*
     * Reset async service
     */
    void reset(void);

    AsyncService::APtr get_impl(void);

    /*!\brief Create instance
     */
    static APtr create_1(void);
    static APtr create_2(const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler);
    static APtr create(const AsyncService::APtr& async_service);

    /*!\brief Create mutex
     */
    Utility::Mutex::APtr create_mutex(const std::string& mutex_id);
    static APIMutex::APtr self_create_mutex(const APIAsyncService::APtr& self, const std::string& mutex_id);

    /*
     * Methods for memory guard
     */
    void memory_guard_cleanup(void);
    bool memory_guard_is_empty(void);

    /*
     * Self methods for easy python-api defintion
     */
    static void self_run(const APIAsyncService::APtr& self);
    static void self_reset(const APIAsyncService::APtr& self);

    static void self_stop(const APIAsyncService::APtr& self);
    static void self_sleep_start(const APIAsyncService::APtr& self,
                                 const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler,
                                 const unsigned long& min,
                                 const unsigned long& sec,
                                 const unsigned long& ms,
                                 PyObject* callback_self,
                                 const std::string& callback_method_name,
                                 const boost::python::object& callback_arg);

    static void self_sleep_start_mutex(const APIAsyncService::APtr& self,
                                 const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler,
    		                     const APIMutex::APtr& mutex,
                                 const unsigned long& min,
                                 const unsigned long& sec,
                                 const unsigned long& ms,
                                 PyObject* callback_self,
                                 const std::string& callback_method_name,
                                 const boost::python::object& callback_arg);

    static void self_memory_guard_cleanup(const APIAsyncService::APtr& self);
    static bool self_memory_guard_is_empty(const APIAsyncService::APtr& self);

private:
    APIAsyncService(const AsyncService::APtr& async_service, const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler_);

    AsyncService::APtr async_service_;
    std::vector<APIAsyncServiceSleeper::APtr> sleepers_;
    boost::posix_time::time_duration ping_timer_duration_;

    Utility::Mutex::APtr update_mutex_;

    std::vector<Utility::Mutex::APtr> mutexes_;

    Giritech::Utility::APICheckpointHandler::APtr api_checkpoint_handler_;
};




}
}
#endif
