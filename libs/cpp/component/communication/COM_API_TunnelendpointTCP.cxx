/*! \file COM_API_TunnelendpointTCP.cxx
 *  \brief This file contains the implementation of the API wrapper for tcp-tunnelendpoints
 */
#define ARM_INSTRUCTION_SET "arm"

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>
#include <component/communication/COM_API_TunnelendpointTCP.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APITunnelendpointTCP implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointTCP::APITunnelendpointTCP(const TunnelendpointTCP::APtr& tunnelendpoint) :
    api_eventhandler_(NULL), impl_tunnelendpoint_(tunnelendpoint) {
}

APITunnelendpointTCP::~APITunnelendpointTCP(void) {
    impl_tunnelendpoint_->close(true);
}

void APITunnelendpointTCP::set_tcp_eventhandler(APITunnelendpointTCPEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
    impl_tunnelendpoint_->set_tcp_eventhandler(this);
}

void APITunnelendpointTCP::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_tcp_eventhandler();
    api_eventhandler_ = NULL;
}

void APITunnelendpointTCP::close(const bool force) {
    impl_tunnelendpoint_->close(force);

}
void APITunnelendpointTCP::read_start(void) {
    impl_tunnelendpoint_->read_start();
}

std::pair<std::string, unsigned long> APITunnelendpointTCP::get_ip_local(void) const {
    return impl_tunnelendpoint_->get_ip_local();
}

std::pair<std::string, unsigned long> APITunnelendpointTCP::get_ip_remote(void) const {
    return impl_tunnelendpoint_->get_ip_remote();
}

void APITunnelendpointTCP::tunnelendpoint_tcp_eh_ready(void) {
    api_eventhandler_->tunnelendpoint_tcp_eh_ready();
}

void APITunnelendpointTCP::tunnelendpoint_tcp_eh_closed(void) {
    api_eventhandler_->tunnelendpoint_tcp_eh_closed();
}

APITunnelendpointTCP::APtr APITunnelendpointTCP::create(const TunnelendpointTCP::APtr& tunnelendpoint) {
    return APITunnelendpointTCP::APtr(new APITunnelendpointTCP(tunnelendpoint));
}

void APITunnelendpointTCP::self_set_tcp_eventhandler(const APITunnelendpointTCP::APtr& self,
                                                     APITunnelendpointTCPEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APITunnelendpointTCP::self_reset_tcp_eventhandler(const APITunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

void APITunnelendpointTCP::self_close(const APITunnelendpointTCP::APtr& self, const bool& force) {
    assert(self.get() != NULL);
    self->close(force);
}

void APITunnelendpointTCP::self_read_start(const APITunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    self->read_start();
}

boost::python::tuple APITunnelendpointTCP::self_get_ip_local(const APITunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_local());
    return boost::python::make_tuple(ip.first, ip.second);
}

boost::python::tuple APITunnelendpointTCP::self_get_ip_remote(const APITunnelendpointTCP::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_remote());
    return boost::python::make_tuple(ip.first, ip.second);
}

/*
 * ------------------------------------------------------------------
 * APITunnelendpointTCPTunnelClient implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointTCPTunnelClient::APITunnelendpointTCPTunnelClient(
		const APIAsyncService::APtr& async_service,
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const std::string& listen_ip,
		const unsigned long& listen_port) :
    api_eventhandler_(NULL),
    io_(async_service->get_io_service()),
    checkpoint_handler_(checkpoint_handler) {
    impl_tunnelendpoint_ = TunnelendpointTCPTunnelClient::create(checkpoint_handler, async_service->get_io_service(), listen_ip, listen_port, 0);
}

APITunnelendpointTCPTunnelClient::~APITunnelendpointTCPTunnelClient(void) {
}

APITunnelendpointTCPTunnelClient::APtr APITunnelendpointTCPTunnelClient::create(
		const APIAsyncService::APtr& async_service,
		const Utility::APICheckpointHandler::APtr& checkpoint_handler,
		const std::string& listen_ip,
		const unsigned long& listen_port) {
    return APtr(new APITunnelendpointTCPTunnelClient(async_service, checkpoint_handler->get_checkpoint_handler(), listen_ip, listen_port));
}

boost::asio::io_service& APITunnelendpointTCPTunnelClient::get_io(void) {
	return io_;
}

boost::asio::io_service::strand& APITunnelendpointTCPTunnelClient::get_strand(void) {
	return impl_tunnelendpoint_->get_mutex()->get_strand();
}

void APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_ready(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_ready();
}

void APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_closed(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_closed();
}

void APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve(message->toString());
}

bool APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_connected(const unsigned long id, const TunnelendpointTCP::APtr& connection) {
    return api_eventhandler_->tunnelendpoint_tcp_eh_connected(APITunnelendpointTCP::create(connection));
}

void APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_acceptor_error(const unsigned long id, const std::string& error_message) {
    api_eventhandler_->tunnelendpoint_tcp_eh_acceptor_error(error_message);
}

bool APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_eh_accept_start_continue(const unsigned long id) {
    return api_eventhandler_->tunnelendpoint_tcp_eh_accept_start_continue();
}


void APITunnelendpointTCPTunnelClient::set_option_reuse_address(const bool& option_reuse_address) {
    impl_tunnelendpoint_->set_option_reuse_address(option_reuse_address);
}

void APITunnelendpointTCPTunnelClient::set_tcp_eventhandler(APITunnelendpointTCPTunnelClientEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
    impl_tunnelendpoint_->set_tcp_eventhandler(this);
}

void APITunnelendpointTCPTunnelClient::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_tcp_eventhandler();
    api_eventhandler_ = NULL;
}

bool APITunnelendpointTCPTunnelClient::is_closed(void) const {
    return impl_tunnelendpoint_->is_closed();
}

std::pair<std::string, unsigned long> APITunnelendpointTCPTunnelClient::get_ip_local(void) const {
    return impl_tunnelendpoint_->get_ip_local();
}

Tunnelendpoint::APtr APITunnelendpointTCPTunnelClient::get_impl(void) {
    return impl_tunnelendpoint_;
}



void APITunnelendpointTCPTunnelClient::self_set_tcp_eventhandler(const APITunnelendpointTCPTunnelClient::APtr& self,
                                                                 APITunnelendpointTCPTunnelClientEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APITunnelendpointTCPTunnelClient::self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelClient::APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

bool APITunnelendpointTCPTunnelClient::self_is_closed(const APITunnelendpointTCPTunnelClient::APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}
boost::python::tuple APITunnelendpointTCPTunnelClient::self_get_ip_local(const APITunnelendpointTCPTunnelClient::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_local());
    return boost::python::make_tuple(ip.first, ip.second);
}

void  APITunnelendpointTCPTunnelClient::self_set_option_reuse_address(const APITunnelendpointTCPTunnelClient::APtr& self, const bool& option_reuse_address) {
    assert(self.get() != NULL);
    self->set_option_reuse_address(option_reuse_address);
}


void APITunnelendpointTCPTunnelClient::self_accept_start(const APITunnelendpointTCPTunnelClient::APtr self) {
    assert(self.get() != NULL);
    /*
     * Not decoubled because it is expected that the port is lisiting after this call.
     * No callback from acceptor indicating that the port is lisiting exist at the moment.
     */
    self->accept_start();
}
void APITunnelendpointTCPTunnelClient::accept_start(void) {
    impl_tunnelendpoint_->accept_start();
}

void APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_tcp_send(message);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelClient::self_tunnelendpoint_send(
		const APITunnelendpointTCPTunnelClient::APtr self,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelClient::tunnelendpoint_tcp_send_decoubled, self, DataBufferManaged::create((message))));
}

void APITunnelendpointTCPTunnelClient::close_decoubled(const bool force) {
	try {
		impl_tunnelendpoint_->close(force);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelClient::close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelClient::self_close(const APITunnelendpointTCPTunnelClient::APtr self, const bool& force) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelClient::close_decoubled, self, force));
}

/*
 * ------------------------------------------------------------------
 * APITunnelendpointTCPTunnelServer implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointTCPTunnelServer::APITunnelendpointTCPTunnelServer(
		const APIAsyncService::APtr& async_service,
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const std::string& server_ip,
		const unsigned long& server_port) :
    api_eventhandler_(NULL),
    io_(async_service->get_io_service()),
    checkpoint_handler_(checkpoint_handler) {
    impl_tunnelendpoint_ = TunnelendpointTCPTunnelServer::create(checkpoint_handler, async_service->get_io_service(), server_ip, server_port, 0);
}

APITunnelendpointTCPTunnelServer::~APITunnelendpointTCPTunnelServer(void) {
}

APITunnelendpointTCPTunnelServer::APtr APITunnelendpointTCPTunnelServer::create(const APIAsyncService::APtr& async_service,
                                                                                const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                                                                                const std::string& server_ip,
                                                                                const unsigned long& server_port) {
    return APtr(new APITunnelendpointTCPTunnelServer(async_service, checkpoint_handler->get_checkpoint_handler(), server_ip, server_port));
}

boost::asio::io_service& APITunnelendpointTCPTunnelServer::get_io(void) {
	return io_;
}

boost::asio::io_service::strand& APITunnelendpointTCPTunnelServer::get_strand(void) {
	return impl_tunnelendpoint_->get_mutex()->get_strand();
}

Tunnelendpoint::APtr APITunnelendpointTCPTunnelServer::get_impl(void) {
    return impl_tunnelendpoint_;
}


void APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_ready(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_ready();
}

void APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_closed(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_closed();
}

void APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve(message->toString());
}


void APITunnelendpointTCPTunnelServer::set_tcp_eventhandler(APITunnelendpointTCPTunnelServerEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
    impl_tunnelendpoint_->set_tcp_eventhandler(this);
}
void APITunnelendpointTCPTunnelServer::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_tcp_eventhandler();
    api_eventhandler_ = NULL;
}

bool APITunnelendpointTCPTunnelServer::is_closed(void) const {
    return impl_tunnelendpoint_->is_closed();
}


void APITunnelendpointTCPTunnelServer::self_set_tcp_eventhandler(
		const APITunnelendpointTCPTunnelServer::APtr& self,
		APITunnelendpointTCPTunnelServerEventhandler* api_eventhandler) {

    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APITunnelendpointTCPTunnelServer::self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelServer::APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

bool APITunnelendpointTCPTunnelServer::self_is_closed(const APITunnelendpointTCPTunnelServer::APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}


void APITunnelendpointTCPTunnelServer::close_decoubled(const bool force) {
	try {
		impl_tunnelendpoint_->close(force);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServer::close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServer::self_close(const APITunnelendpointTCPTunnelServer::APtr self, const bool& force) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServer::close_decoubled, self, force));
}

void APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_tcp_send(message);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_send_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServer::self_tunnelendpoint_send(
		const APITunnelendpointTCPTunnelServer::APtr self,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServer::tunnelendpoint_tcp_send_decoubled, self, DataBufferManaged::create((message))));
}



/*
 * ------------------------------------------------------------------
 * APITunnelendpointTCPTunnelServerWithConnectionEngine implementation
 * ------------------------------------------------------------------
 */
APITunnelendpointTCPTunnelServerWithConnectionEngine::APITunnelendpointTCPTunnelServerWithConnectionEngine(
		const APIAsyncService::APtr& async_service,
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) :
		api_eventhandler_(NULL),
		io_(async_service->get_io_service()),
		checkpoint_handler_(checkpoint_handler) {
	impl_tunnelendpoint_ = TunnelendpointTCPTunnelServerWithConnectionEngine::create(checkpoint_handler, async_service->get_io_service(), 0);
}

APITunnelendpointTCPTunnelServerWithConnectionEngine::~APITunnelendpointTCPTunnelServerWithConnectionEngine(void) {
}

boost::asio::io_service& APITunnelendpointTCPTunnelServerWithConnectionEngine::get_io(void) {
	return io_;
}

boost::asio::io_service::strand& APITunnelendpointTCPTunnelServerWithConnectionEngine::get_strand(void) {
	return impl_tunnelendpoint_->get_mutex()->get_strand();
}

Tunnelendpoint::APtr APITunnelendpointTCPTunnelServerWithConnectionEngine::get_impl(void) {
    return impl_tunnelendpoint_;
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_ready(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_ready();
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_closed(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_closed();
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve(message->toString());
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_recieve_remote_connect(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve_remote_connect(id);
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_recieve_remote_close(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve_remote_close(id);
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_recieve_remote_eof(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve_remote_eof(id);
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_eh_recieve_remote_closed(const unsigned long id) {
    api_eventhandler_->tunnelendpoint_tcp_eh_recieve_remote_closed(id);
}


void APITunnelendpointTCPTunnelServerWithConnectionEngine::set_tcp_eventhandler(APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* api_eventhandler) {
    api_eventhandler_ = api_eventhandler;
    impl_tunnelendpoint_->set_tcp_eventhandler(this);
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::reset_tcp_eventhandler(void) {
    impl_tunnelendpoint_->reset_tcp_eventhandler();
    api_eventhandler_ = NULL;
}

bool APITunnelendpointTCPTunnelServerWithConnectionEngine::is_closed(void) const {
    return impl_tunnelendpoint_->is_closed();
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::add_tunnelendpoint(
		const unsigned long& child_id,
		const APITunnelendpoint::APtr& tunnelendpoint) {
	impl_tunnelendpoint_->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::add_tunnelendpoint(
		const unsigned long& child_id,
		const Tunnelendpoint::APtr& tunnelendpoint) {
	impl_tunnelendpoint_->add_tunnelendpoint(child_id, tunnelendpoint);
}


APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr APITunnelendpointTCPTunnelServerWithConnectionEngine::create(
		const APIAsyncService::APtr& async_service,
		const Utility::APICheckpointHandler::APtr& checkpoint_handler) {
    return APtr(new APITunnelendpointTCPTunnelServerWithConnectionEngine(async_service, checkpoint_handler->get_checkpoint_handler()));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_set_tcp_eventhandler(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
		APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler* api_eventhandler) {
    assert(self.get() != NULL);
    self->set_tcp_eventhandler(api_eventhandler);
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_reset_tcp_eventhandler(const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self) {
    assert(self.get() != NULL);
    self->reset_tcp_eventhandler();
}

bool APITunnelendpointTCPTunnelServerWithConnectionEngine::self_is_closed(const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_add_tunnelendpoint_reliable_crypted(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
		const unsigned long& child_id,
		const APITunnelendpointReliableCrypted::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint);
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_add_tunnelendpoint_reliable_crypted_tunnel(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& self,
		const unsigned long& child_id,
		const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint);
}


void APITunnelendpointTCPTunnelServerWithConnectionEngine::close_decoubled(const bool force) {
	try {
		impl_tunnelendpoint_->close(force);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_close(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const bool& force) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::close_decoubled, self, force));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_send_decoubled(const Utility::DataBufferManaged::APtr message) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_tcp_send(message);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_send_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const std::string& message) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_tcp_send_decoubled, self, DataBufferManaged::create((message))));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_eof_decoubled(const unsigned long tunnel_id) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send_remote_eof(tunnel_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_eof_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_eof(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long tunnel_id) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_eof_decoubled, self, tunnel_id));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close_decoubled(const unsigned long tunnel_id) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send_remote_close(tunnel_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_close(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long tunnel_id) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close_decoubled, self, tunnel_id));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_closed_decoubled(const unsigned long tunnel_id) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send_remote_closed(tunnel_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_close_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_closed(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long tunnel_id) {
    assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_closed_decoubled, self, tunnel_id));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::remove_tunnelendpoint_decoubled(const unsigned long child_id) {
	try {
		impl_tunnelendpoint_->remove_tunnelendpoint(child_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::remove_tunnelendpoint_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_remove_tunnelendpoint(
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long& child_id) {
	assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::remove_tunnelendpoint_decoubled, self, child_id));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_ok_decoubled(const unsigned long tunnel_id) {
	try {
		impl_tunnelendpoint_->tunnelendpoint_send_remote_recieve_connect_ok(tunnel_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_ok_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_recieve_connect_ok (
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long tunnel_id) {
	assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_ok_decoubled, self, tunnel_id));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_error_decoubled(const unsigned long tunnel_id) {
    try {
    	impl_tunnelendpoint_->tunnelendpoint_send_remote_recieve_connect_error(tunnel_id);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_error_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_recieve_connect_error (
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const unsigned long tunnel_id) {
	assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::tunnelendpoint_send_remote_recieve_connect_error_decoubled, self, tunnel_id));
}


void APITunnelendpointTCPTunnelServerWithConnectionEngine::self_enable_checkpoint_tunnel (
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr self,
		const std::string& folder) {
	assert(self.get() != NULL);
	self->get_strand().post(boost::bind(&APITunnelendpointTCPTunnelServerWithConnectionEngine::enable_checkpoint_tunnel, self, folder));
}

void APITunnelendpointTCPTunnelServerWithConnectionEngine::enable_checkpoint_tunnel(const boost::filesystem::path& folder) {
    try {
    	impl_tunnelendpoint_->enable_checkpoint_tunnel(folder);
	}
	catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointTCPTunnelServerWithConnectionEngine::enable_checkpoint_tunnel.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}
