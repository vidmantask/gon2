/*! \file COM_AsyncService.cxx
 *  \brief This file contains the implementation of the async service class
 */
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <lib/utility/UY_AsyncMemGuard.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APIAsyncService implementation
 * ------------------------------------------------------------------
 */
AsyncService::AsyncService(const CheckpointHandler::APtr& checkpoint_handler)  :
	checkpoint_handler_(checkpoint_handler) {
	Giritech::Utility::AsyncMemGuard::create_global_guard(io_service_);
}

AsyncService::~AsyncService(void) {
	Giritech::Utility::AsyncMemGuard::destroy_global_guard(io_service_);
}

AsyncService::APtr AsyncService::create(const CheckpointHandler::APtr& checkpoint_handler) {
    return AsyncService::APtr(new AsyncService(checkpoint_handler));
}

void AsyncService::run(void) {
	bool keep_running = true;

	while(keep_running) {
		try {
			io_service_.run();

			//
			// In case of exception the keep_running flag is still true, and the thread will be restarted
			//
			keep_running = false;
		}
		catch(std::exception& e) {
			Checkpoint cp(*checkpoint_handler_, "AsyncService::run.thread_crash", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
		}
		catch(...) {
			Checkpoint cp(*checkpoint_handler_, "AsyncService::run.thread_crash.unknown_exception", Attr_Communication(), CpAttr_critical());
		}
	}
}

void AsyncService::reset(void) {
    io_service_.reset();
}

unsigned long AsyncService::poll(void) {
    return io_service_.poll();
}

void AsyncService::stop(void) {
    io_service_.stop();
}

boost::asio::io_service& AsyncService::get_io_service(void) {
    return io_service_;
}

void AsyncService::memory_guard_cleanup(void) {
	io_service_.post(boost::bind(&Giritech::Utility::AsyncMemGuard::global_cleanup));
}

bool AsyncService::memory_guard_is_empty(void) {
	return Giritech::Utility::AsyncMemGuard::get_global_guard()->is_empty();
}

