/*! \file COM_SessionManagerConnectionGroup.hxx
 *  \brief This file contains
 */
#ifndef COM_SessionManagerConnectionGroup_HXX
#define COM_SessionManagerConnectionGroup_HXX

#include <boost/shared_ptr.hpp>

#include <component/communication/COM_SessionManagerConnection.hxx>
#include <component/communication/COM_Servers_Spec.hxx>


namespace Giritech {
namespace Communication {

/*! \brief
 *
 * <connection_group title="" selection_delay="1">
 *   <connection ... />
 *   ...
 *   <connection ... />
 * </connection_group>
 *
 *
 */
class SessionManagerConnectionGroup {
public:
    typedef boost::shared_ptr<SessionManagerConnectionGroup> APtr;

    /*! \brief Destructor
     */
    ~SessionManagerConnectionGroup(void);

    /*! \brief Select the next connection from the connection group, returns null if no more connections in the group
     */
    SessionManagerConnection::APtr select_connection(void);

    /*! \brief Returns true if one of the connection is connecting
     */
    bool has_pending_connections(void) const;

    /*! \brief Getters
     */
    boost::posix_time::time_duration get_selection_delay(void) const;


    /*! \brief Add a new connection to the group
     */
    void add_connection(const SessionManagerConnection::APtr& connection);

    /*! \brief Create instance
     */
    static APtr create(const std::string& title, const boost::posix_time::time_duration& selection_delay);
    static APtr create(const ServersSpec::ServersConnectionGroup& servers_connection_group);

private:
    SessionManagerConnectionGroup(const std::string& title, const boost::posix_time::time_duration& selection_delay);

    std::string title_;
    boost::posix_time::time_duration selection_delay_;

    std::vector<SessionManagerConnection::APtr> connections_;
};

}
}
#endif
