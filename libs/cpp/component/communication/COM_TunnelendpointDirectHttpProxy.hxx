/*! \file COM_TunnelendpointDirectHttpProxy.hxx
 \brief This file contains the TunnelendpointDirectHttpProxy clases
 */
#ifndef COM_TunnelendpointDirectHttpProxy_HXX
#define COM_TunnelendpointDirectHttpProxy_HXX

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_TunnelendpointDirect.hxx>
#include <component/communication/COM_TunnelendpointDirectEventhandler.hxx>

#include <component/http_client/HttpClient.hxx>
#include <component/http_client/HttpClientCom.hxx>
#include <component/http_client/HttpClientComEventhandler.hxx>



namespace Giritech {
namespace Communication {


/*! \brief Tunnelendpoint for handling traffic for a direct connection
 *
 */
class TunnelendpointDirectHttpProxy :
	public TunnelendpointDirectEventhandler,
	public Giritech::HttpClient::HttpClientCom {

public:
    typedef boost::shared_ptr<TunnelendpointDirectHttpProxy> APtr;

    virtual ~TunnelendpointDirectHttpProxy(void);


    /*! \brief Implementation of TunnelendpointDirectEventhandler virtual methods
     */
    void tunnelendpoint_direct_eh_closed();
    void tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message);


    /*! \brief Implementation of HttpClientCom
     */
    void connect(const std::string& host, const unsigned long port);
    void write(const Utility::DataBufferManaged::APtr& data);
    void close(void);
    bool is_closed(void);


    /*! \brief Create instance
     */
    static APtr create(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		const TunnelendpointDirect::APtr& tunnelendpoint_direct);


protected:

    /*! \brief Constructor
     */
    TunnelendpointDirectHttpProxy(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
    		const TunnelendpointDirect::APtr& tunnelendpoint_direct);

    Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;
    TunnelendpointDirect::APtr tunnelendpoint_direct_;
};


}
}
#endif
