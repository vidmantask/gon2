/*! \file COM.gdoc
    \brief This file contains documentation for the Giritech::Component::Communication namespace
*/

/*! \namespace Giritech::Component::Communication
    \brief Contains functionality for handling communication between gserver and gclient

The main functionality of the %Communication component is to offer a Tunnelendpoint srvice for 
communicating between a server component/module and its corsponding client side. 

See \ref overview_component_communication for an introduction to the communication component.

See \ref com_handling_buffers for introduction to buffer handlieng.

*/



/*! \page overview_component_communication Abstraction levels in Communication component
The RawTunnelendpoint abstraction encapsulate asyncron socket functionality.
The following drawing shows the classes in the abstraction and how the collaborate.

\section section_intro_rawtunnelendpoint RawTunnelendpoint abstraction
\dot
digraph ov_com_1 {
  rankdir=BT;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=2, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";

  
  subgraph cluster_raw {
    node [shape=box];

    RawTunnelendpointAcceptorTCPEventhandler   [URL="\ref Giritech::Communication::RawTunnelendpointAcceptorTCPEventhandler", style=filled, fillcolor=1, label="RawTunnelendpointAcceptorTCP\nEventhandler"];
    RawTunnelendpointAcceptorTCP               [URL="\ref Giritech::Communication::RawTunnelendpointAcceptorTCP", style=filled, fillcolor=3];
    RawTunnelendpointAcceptorTCP -> RawTunnelendpointAcceptorTCPEventhandler[style=dotted, label="Use"];

    RawTunnelendpointConnectorTCPEventhandler  [URL="\ref Giritech::Communication::RawTunnelendpointConnectorTCPEventhandler", style=filled, fillcolor=1, label="RawTunnelendpointConnectorTCP\nEventhandler"];
    RawTunnelendpointConnectorTCP              [URL="\ref Giritech::Communication::RawTunnelendpointConnectorTCP", style=filled, fillcolor=3];
    RawTunnelendpointConnectorTCP -> RawTunnelendpointConnectorTCPEventhandler[style=dotted, label="Use"];

    RawTunnelendpointEventhandler    [URL="\ref Giritech::Communication::RawTunnelendpointEventhandler", style=filled, fillcolor=1, label="RawTunnelendpoint\nEventhandler"];
    RawTunnelendpoint                [URL="\ref Giritech::Communication::RawTunnelendpoint", style=filled, fillcolor=2];
    RawTunnelendpointTCP             [URL="\ref Giritech::Communication::RawTunnelendpointTCP", style=filled, fillcolor=3];

    RawTunnelendpoint -> RawTunnelendpointEventhandler[style=dotted, label="Use"];

    RawTunnelendpointTCP -> RawTunnelendpoint         [arrowhead=empty];
    label="RawTunnelendpoint abstraction"
  }

}
\enddot



\section section_intro_session Session and SessionManager abstraction
The Session and SessionManager abstraction offers functionality for handling session establishment and teardown. 
The following drawings shows the classes in the abstractions and how the collaborate.

\dot
digraph ov_com_1 {
  rankdir=BT;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=2, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";

  subgraph cluster_session {
    node [shape=box];

    SessionEventhandler        [URL="\ref Giritech::Communication::SessionEventhandler", style=filled, fillcolor=1, label="Session\nEventhandler"];
    SessionEventhandlerReady   [URL="\ref Giritech::Communication::SessionEventhandlerReady", style=filled, fillcolor=1, label="Session\nEventhandlerReady"];
    Session                    [URL="\ref Giritech::Communication::Session", style=filled, fillcolor=3];

    SessionCrypterEventhandler      [URL="\ref Giritech::Communication::SessionCrypterEventhandler", style=filled, fillcolor=1, label="SessionCrypter\nEventhandler"];
    SessionCrypterEventhandlerReady [URL="\ref Giritech::Communication::SessionCrypterEventhandlerReady", style=filled, fillcolor=1, label="SessionCrypter\nEventhandlerReady"];
    SessionCrypter                  [URL="\ref Giritech::Communication::SessionCrypter", style=filled, fillcolor=2];
    SessionCrypterServer            [URL="\ref Giritech::Communication::SessionCrypterServer", style=filled, fillcolor=3];
    SessionCrypterClient            [URL="\ref Giritech::Communication::SessionCrypterClient", style=filled, fillcolor=3];

    SessionTunnelendpointEventhandler            [URL="\ref Giritech::Communication::SessionTunnelendpointEventhandler", style=filled, fillcolor=1, label="SessionTunnelendpoint\nEventhandler"];
    SessionTunnelendpoint                        [URL="\ref Giritech::Communication::SessionTunnelendpoint", style=filled, fillcolor=2];
    SessionTunnelendpointReliableSessionCrypter  [URL="\ref Giritech::Communication::SessionTunnelendpointReliableSessionCrypter", style=filled, fillcolor=3];

    SessionCrypterServer -> SessionCrypter  [arrowhead=empty];
    SessionCrypterClient -> SessionCrypter  [arrowhead=empty];

    SessionCrypter -> SessionCrypterEventhandler      [style=dotted, label="Use"];
    SessionCrypter -> SessionCrypterEventhandlerReady [style=dotted, label="Use"];

    SessionTunnelendpoint -> SessionTunnelendpointEventhandler                     [style=dotted, label="Use"];
    SessionTunnelendpointReliableSessionCrypter -> SessionTunnelendpoint           [arrowhead=empty];
    SessionTunnelendpointReliableSessionCrypter -> SessionCrypterEventhandlerReady [arrowhead=empty];

    Session -> SessionEventhandler                          [arrowhead=empty];
    Session -> SessionEventhandlerReady                     [arrowhead=empty];
    Session -> SessionCrypterEventhandler                   [arrowhead=empty];
    Session -> SessionTunnelendpointReliableSessionCrypter  [arrowhead=empty];
    Session -> SessionTunnelendpointEventhandler            [arrowhead=empty];
    Session -> SessionCrypter                               [style=dotted, label="Use"];

    RawTunnelendpointEventhandler [label="RawTunnelendpoint\nEventhandler"];
    TunnelendpointEventhandler [label="RawTunnelendpoint\nEventhandler"];
    TunnelendpointConnector [label="Tunnelendpoint\nConnector"];

    SessionCrypter -> RawTunnelendpointEventhandler                   [arrowhead=empty];
    SessionCrypter -> RawTunnelendpointTCP                            [style=dotted, label="Use"];

    Session -> TunnelendpointConnector                      [style=dotted, label="Use"];
    Session -> TunnelendpointEventhandler                   [style=dotted, label="Use"];
    Session -> TunnelendpointReliableCryptedTunnel          [style=dotted, label="Use"];

    label="Session abstraction"
  }

}
\enddot

\dot
digraph ov_com_1 {
  rankdir=BT;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=2, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";

  subgraph cluster_session {
    node [shape=box];

    SessionManagerEventhandler  [URL="\ref Giritech::Communication::SessionManagerEventhandler", style=filled, fillcolor=1, label="SessionManager\nEventhandler"];
    SessionManagerClient        [URL="\ref Giritech::Communication::SessionManagerClient", style=filled, fillcolor=3];
    SessionManagerServer        [URL="\ref Giritech::Communication::SessionManagerServer", style=filled, fillcolor=3];

    RawTunnelendpointConnectorTCPEventhandler [label="RawTunnelendpointConnectorTCP\nEventhandler"];
    RawTunnelendpointAcceptorTCPEventhandler [label="RawTunnelendpointConnectorTCP\nEventhandler"];

    SessionManagerClient -> RawTunnelendpointConnectorTCP             [style=dotted, label="Use"];
    SessionManagerServer -> RawTunnelendpointAcceptorTCP              [style=dotted, label="Use"];
    SessionManagerServer -> SessionManagerEventhandler                [style=dotted, label="Use"];
    SessionManagerClient -> SessionManagerEventhandler                [style=dotted, label="Use"];
    SessionManagerClient -> RawTunnelendpointConnectorTCPEventhandler [arrowhead=empty];
    SessionManagerServer -> RawTunnelendpointAcceptorTCPEventhandler  [arrowhead=empty];

    SessionManagerClient -> Session              [style=dotted, label="Use"];
    SessionManagerServer -> Session              [style=dotted, label="Use"];


    label="SessionManager abstraction"
  }


}
\enddot



\section section_intro_tunnelendpoint Tunnelendpoint abstraction
The Tunnelendpoint abstraction offers a logical tunnel between a point in the server and in the client.
The following drawing shows the classes in the abstraction and how the collaborate.
\dot
digraph ov_com_1 {
  rankdir=BT;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=2, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";

  subgraph cluster_tunnelendpoint {
    node [shape=box];

    TunnelendpointEventhandler          [URL="\ref Giritech::Communication::TunnelendpointEventhandler", style=filled, fillcolor=1, label="Tunnelendpoint\nEventhandler"];
    Tunnelendpoint                      [URL="\ref Giritech::Communication::Tunnelendpoint", style=filled, fillcolor=2];
    TunnelendpointReliableCrypted       [URL="\ref Giritech::Communication::TunnelendpointReliableCrypted", style=filled, fillcolor=2];
    TunnelendpointReliableCryptedTunnel [URL="\ref Giritech::Communication::TunnelendpointReliableCryptedTunnel", style=filled, fillcolor=3];
    TunnelendpointConnector             [URL="\ref Giritech::Communication::TunnelendpointConnector", style=filled, fillcolor=1, label="Tunnelendpoint\nConnector"];

    TunnelendpointReliableCrypted -> Tunnelendpoint                        [arrowhead=empty];
    TunnelendpointReliableCryptedTunnel -> TunnelendpointReliableCrypted   [arrowhead=empty];

    Tunnelendpoint -> TunnelendpointEventhandler           [style=dotted, label="Use"];
    TunnelendpointReliableCryptedTunnel -> Tunnelendpoint  [style=dotted, label="Use"];
    Tunnelendpoint -> TunnelendpointConnector              [style=dotted, label="Use"];

    label="Tunnelendpoint abstraction"
  }
}
\enddot


The basic functionality of the Tunnelendpoint abstraction is to offer a <b>transport service</b> of messages betwen two Tunnelendpoints,
where one endpoint is on the server(GServer) and the other endpoint is in a client(GClient).
\dot
digraph ov_com_1 {
  rankdir=RL;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=3, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";
 
  TEP_1 [shape=box, label="Tunnelendpoint\n(GServer)"];
  TEP_2 [shape=box, label="Tunnelendpoint\n(GClient)"];
  TEP_1 -> TEP_2 [arrowhead=normal, arrowtail=normal];
}
\enddot


A tunnelendpoint can only communicate with its mirror tunnelendpoint. 
This is seen as a feature intented to enforce communication seperation, 
because it prevent a tunnelendpoint from sending to other tunnelenpoints than its mirror.
\dot
digraph ov_com_2 {
  rankdir=RL;
  ranksep=0.5; 
  center=true;
  size="14,8.5";
  edge [minlen=3, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";
 
  TEP_1c [shape=box, label="Tunnelendpoint A\n(GServer)"];
  TEP_1s [shape=box, label="Tunnelendpoint A\n(GClient)"];
  TEP_1c -> TEP_1s [arrowhead=normal, arrowtail=normal];

  TEP_2c [shape=box, label="Tunnelendpoint B\n(GServer)"];
  TEP_2s [shape=box, label="Tunnelendpoint B\n(GClient)"];
  TEP_2c -> TEP_2s [arrowhead=normal, arrowtail=normal];

  TEP_1c -> TEP_2s [style=dotted, color=red, arrowhead=normal, arrowtail=normal];
}
\enddot




\subsection section_intro_tunnelendpoint_send_recive Sending and reciving messages
A Tunnelendpoint is offering a <b>send-method</b> that sends data to the Tunnelendpoint's mirror who recives the data through a <b>recive-event</b>.

\dot
digraph ov_com_3 {
  rankdir=RL;
  ranksep=0.5; 
  center=true;
  size="14,8.5";
  edge [minlen=3, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";
 
  subgraph cluster_tep_1 {
    node [shape=box];
    TEP_1_SEND    [label="send-method(data)",  style=filled, fillcolor=1];
    TEP_1_RECIVE  [label="recive-event(data)", style=filled, fillcolor=3];
    label="Tunnelendpoint\n(GServer)"
  }
  subgraph cluster_tep_2 {
    node [shape=box];
    TEP_2_SEND    [label="send-method(data)",  style=filled, fillcolor=1];
    TEP_2_RECIVE  [label="recive-event(data)", style=filled, fillcolor=3];
    label="Tunnelendpoint\n(GClient)"
  }
  TEP_1_SEND -> TEP_2_RECIVE
  TEP_2_SEND -> TEP_1_RECIVE
}
\enddot



\subsection section_intro_tunnelendpoint_child_tunnelendpoints Child Tunnelendpoints

A Tunnelendpoint can add child tunnelendpoints. 
The parent can not see the trafic of data send between two child tunneldpoints, but it can control the flow of data (eg. start and stop).

\dot
digraph ov_com_4 {
  rankdir=RL;
  ranksep=0.5; 
  center=true;
  size="14,8.5";
  edge [minlen=3, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";
 
  subgraph cluster_tep_1 {
    node [shape=box];
    label="Tunnelendpoint\n(GServer)"
    TEP_1    [shape=point];
    TEP_1_A    [label="Child Tunnelendpoint A"];
    TEP_1_B    [label="Child Tunnelendpoint B"];
    TEP_1_C    [label="Child Tunnelendpoint C"];
  }
  subgraph cluster_tep_2 {
    node [shape=box];
    label="Tunnelendpoint\n(GClient)"
    TEP_2    [shape=point];
    TEP_2_A    [label="Child Tunnelendpoint A"];
    TEP_2_B    [label="Child Tunnelendpoint B"];
    TEP_2_C    [label="Child Tunnelendpoint C"];
  }
  TEP_1 -> TEP_2     [arrowhead=normal, arrowtail=normal];
  TEP_1_A -> TEP_2_A [arrowhead=normal, arrowtail=normal];
  TEP_1_B -> TEP_2_B [arrowhead=normal, arrowtail=normal];
  TEP_1_C -> TEP_2_C [arrowhead=normal, arrowtail=normal];
}
\enddot



\section section_intro_communication_api  API abstraction
The API abstraction offers a api for using the communication component. Currently this api is exposed more or less directly for Python.
The following drawing shows the classes in the abstraction and how the collaborate.

\dot
digraph ov_com_1 {
  rankdir=BT;
  ranksep=0.5; 
  center=true;
  size="12,8.5";
  edge [minlen=2, fontsize=8, fontname="Helvetica" ];
  node [colorscheme=pubu3, fontsize=10, fontname="Helvetica"];
  fontname="Helvetica";

  subgraph cluster_api {
    node [shape=box];

    APISessionEventhandlerReady   [URL="\ref Giritech::Communication::APISessionEventhandlerReady", style=filled, fillcolor=1, label="APISession\nEventhandlerReady"];
    APISession                    [URL="\ref Giritech::Communication::APISession", style=filled, fillcolor=2];

    APISessionManagerEventhandler   [URL="\ref Giritech::Communication::APISessionManagerEventhandler", style=filled, fillcolor=1, label="APISessionManager\nEventhandler"];
    APISessionManagerClient         [URL="\ref Giritech::Communication::APISessionManagerClient", style=filled, fillcolor=2];
    APISessionManagerServer         [URL="\ref Giritech::Communication::APISessionManagerServer", style=filled, fillcolor=2];

    APITunnelendpointEventhandler          [URL="\ref Giritech::Communication::APISessionManagerEventhandler", style=filled, fillcolor=1, label="APITunnelendpoint\nEventhandler"];
    APITunnelendpoint                      [URL="\ref Giritech::Communication::APITunnelendpoint", style=filled, fillcolor=2];
    APITunnelendpointReliableCrypted       [URL="\ref Giritech::Communication::APITunnelendpoint", style=filled, fillcolor=2];
    APITunnelendpointReliableCryptedTunnel [URL="\ref Giritech::Communication::APITunnelendpoint", style=filled, fillcolor=2];


    APISession -> SessionEventhandlerReady    [arrowhead=empty];
    APISession -> APISessionEventhandlerReady [style=dotted, label="Use"];
    APISession -> Session                     [style=dotted, label="Use"];

    APISessionManagerClient -> APISessionManagerEventhandler [style=dotted, label="Use"];
    APISessionManagerServer -> APISessionManagerEventhandler [style=dotted, label="Use"];

    APITunnelendpointReliableCrypted -> APITunnelendpoint                         [arrowhead=empty];
    APITunnelendpointReliableCryptedTunnel -> APITunnelendpointReliableCrypted    [arrowhead=empty];
    APITunnelendpoint -> APITunnelendpointEventhandler                            [style=dotted, label="Use"];
    APITunnelendpointReliableCrypted -> TunnelendpointReliableCrypted             [style=dotted, label="Use"];
    APITunnelendpointReliableCryptedTunnel -> TunnelendpointReliableCryptedTunnel [style=dotted, label="Use"];

    label="API abstraction"
  }
}
\enddot
Giritech::Communication::APISession

*/
