/*! \file UTest_COM_ASimulator.cxx
 \brief This file contains unittest suite for the application simulator
 */
#include <vector>
#include <string>
#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/asimulator/COM_ASimulator.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 * Test of message functionality
 */
BOOST_AUTO_TEST_CASE( message )
{
    CheckpointHandler::APtr sim_checkpoint_handler(CheckpointHandler::create_cout_all());

    boost::thread_group threads;
    AsyncService::APtr async_service(AsyncService::create(sim_checkpoint_handler));


    ASimulationServer::APtr sim_server(ASimulationServer::create(sim_checkpoint_handler, async_service->get_io_service(), "127.0.0.1", 8085, 4));
    ASimulationClient::APtr sim_client_1(ASimulationClient::create(sim_checkpoint_handler, async_service->get_io_service(), "127.0.0.1", 8085, ASimulatorConnection::SimulationType_ping, 10, 50100));
    ASimulationClient::APtr sim_client_2(ASimulationClient::create(sim_checkpoint_handler, async_service->get_io_service(), "127.0.0.1", 8085, ASimulatorConnection::SimulationType_ping_server_close, 10, 50100));
    ASimulationClient::APtr sim_client_3(ASimulationClient::create(sim_checkpoint_handler, async_service->get_io_service(), "127.0.0.1", 8085, ASimulatorConnection::SimulationType_upload_until_eof, 10, 50100));
    ASimulationClient::APtr sim_client_4(ASimulationClient::create(sim_checkpoint_handler, async_service->get_io_service(), "127.0.0.1", 8085, ASimulatorConnection::SimulationType_download_until_eof, 10, 50100));

    sim_server->start();
    sim_client_1->start();
    sim_client_2->start();
    sim_client_3->start();
    sim_client_4->start();
    threads.create_thread(boost::bind(&AsyncService::run, async_service));
    while (! sim_client_1->is_done()) {
    }
    cout << "client 1 done" << endl;

    while (! sim_client_2->is_done()) {
    }
    cout << "client 2 done" << endl;

    while (! sim_client_3->is_done()) {
    }
    cout << "client 3 done" << endl;

    while (! sim_client_4->is_done()) {
    }
    cout << "client 4 done" << endl;

    sim_server->stop();
    boost::this_thread::sleep(boost::posix_time::seconds(2));

    async_service->stop();

    threads.join_all();
}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
