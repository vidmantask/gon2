/*! \file COM_RawTunnelendpointConnector.hxx
 * \brief This file contains implementation of RawTunnelendpointConnector abstraction
 */
#include <boost/foreach.hpp>

#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
#include <boost/asio/ssl.hpp>
#endif


/*
 * ------------------------------------------------------------------
 * RawTunnelendpointConnectorTCPConnectionStatus implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointConnectorTCPConnectionStatus::RawTunnelendpointConnectorTCPConnectionStatus(
    		boost::asio::io_service& io,
    		const RawTunnelendpointTCP::APtr& raw_tunnelendpoint)
    :  	timeout_timer_(io),
    	raw_tunnelendpoint_(raw_tunnelendpoint),
    	state_(State_Init),
    	timeout_(false) {
}

RawTunnelendpointConnectorTCPConnectionStatus::~RawTunnelendpointConnectorTCPConnectionStatus(void) {
}

RawTunnelendpointConnectorTCPConnectionStatus::APtr RawTunnelendpointConnectorTCPConnectionStatus::create(
		boost::asio::io_service& io,
		const RawTunnelendpointTCP::APtr& raw_tunnelendpoint) {
	return APtr(new RawTunnelendpointConnectorTCPConnectionStatus(io, raw_tunnelendpoint));
}

RawTunnelendpointTCP::APtr RawTunnelendpointConnectorTCPConnectionStatus::get_tunnelendpoint(void) {
 	return raw_tunnelendpoint_;
}

boost::asio::steady_timer& RawTunnelendpointConnectorTCPConnectionStatus::get_timeout_timer(void) {
	return timeout_timer_;
}

RawTunnelendpointConnectorTCPConnectionStatus::State RawTunnelendpointConnectorTCPConnectionStatus::get_state(void) {
	return state_;
}

void RawTunnelendpointConnectorTCPConnectionStatus::set_state_resolving(void) {
	if(state_== State_Init) {
		state_ = State_Resolving;
	}
}

void RawTunnelendpointConnectorTCPConnectionStatus::set_state_resolved(void) {
	if(state_== State_Resolving) {
		state_ = State_Resolved;
	}
}

void RawTunnelendpointConnectorTCPConnectionStatus::set_state_connecting(void) {
	if(state_== State_Resolved || state_== State_Connected) {
		state_ = State_Connecting;
	}
}

void RawTunnelendpointConnectorTCPConnectionStatus::set_state_connected(void) {
	if(state_== State_Connecting) {
		state_ = State_Connected;
	}
}

void RawTunnelendpointConnectorTCPConnectionStatus::set_timeout(void) {
	timeout_ = true;
}

bool RawTunnelendpointConnectorTCPConnectionStatus::is_timeout(void) {
	return timeout_;
}

bool RawTunnelendpointConnectorTCPConnectionStatus::has_async_pending(void) {
	switch(state_) {
	case State_Resolving:
	case State_Connecting:
		return true;
	}
	return ! timeout_;
}

/*
 * ------------------------------------------------------------------
 * RawTunnelendpointConnectorTCP implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointConnectorTCP::RawTunnelendpointConnectorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                             boost::asio::io_service& io,
                                                             const std::string& remote_host,
                                                             const long remote_port,
                                                             RawTunnelendpointConnectorTCPEventhandler* eventhandler) :
    checkpoint_handler_(checkpoint_handler),
    io_(io),
    resolver_(io),
    remote_host_(remote_host),
    remote_port_(remote_port),
    eventhandler_(eventhandler),
    connect_timeout_(boost::posix_time::seconds(20)),
    mutex_(Mutex::create(io, "RawTunnelendpointConnectorTCP")),
    state_(State_ReadyToConnect),
    ssl_enabled_(false),
	ssl_disable_certificate_verification_(false) {
}

RawTunnelendpointConnectorTCP::~RawTunnelendpointConnectorTCP(void) {
	reset_eventhandler();
}

RawTunnelendpointConnectorTCP::APtr RawTunnelendpointConnectorTCP::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
                                                                          boost::asio::io_service& io,
                                                                          const std::string& remote_host,
                                                                          const long remote_port,
                                                                          RawTunnelendpointConnectorTCPEventhandler* eventhandler) {
	APtr result(new RawTunnelendpointConnectorTCP(checkpoint_handler, io, remote_host, remote_port, eventhandler));
	AsyncMemGuard::global_add_element(result);
	return result;
}

Utility::Mutex::APtr RawTunnelendpointConnectorTCP::get_mutex(void) {
    return mutex_;
}

void RawTunnelendpointConnectorTCP::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
}

void RawTunnelendpointConnectorTCP::set_connect_timeout(const boost::posix_time::time_duration& connect_timeout) {
	connect_timeout_ = connect_timeout;
}

unsigned long RawTunnelendpointConnectorTCP::get_connection_id_next(void) {
    static unsigned long connection_id = 0;
    static boost::recursive_timed_mutex global_mutex;
    boost::recursive_timed_mutex::scoped_lock global_mutex_lock(global_mutex);
    return ++connection_id;
}

void RawTunnelendpointConnectorTCP::reset_eventhandler(void) {
    eventhandler_ = NULL;
}

void RawTunnelendpointConnectorTCP::enable_ssl(const bool& disable_certificate_verification) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
	ssl_enabled_ = true;
	ssl_disable_certificate_verification_ = disable_certificate_verification;
#else
    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::enable_ssl.not_awailable_on_platform", Attr_Communication(), Utility::CpAttr_critical());
	ssl_enabled_ = false;
#endif
}

void RawTunnelendpointConnectorTCP::aio_connect_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::aio_connect_start");
	switch (state_) {
		case State_ReadyToConnect:
			break;
		default: {
		    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect_start.invalid_state", Attr_Communication(), Utility::CpAttr_error(), CpAttr("state", state_));
			return;
		}
	}
    CheckpointScope cps(
    		*checkpoint_handler_,
    		"RawTunnelendpointConnectorTCP::aio_connect_start",
    		Attr_Communication(),
    		Utility::CpAttr_debug(),
    		CpAttr("remote_host", remote_host_),
    		CpAttr("remote_port", remote_port_));

    unsigned long connection_id = get_connection_id_next();

    RawTunnelendpointTCP::APtr raw_tunnelendpoint;
    if(ssl_enabled_) {
    #ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
    	raw_tunnelendpoint = RawTunnelendpointTCP::create_ssl(checkpoint_handler_, io_, connection_id, remote_host_, ssl_disable_certificate_verification_);
    #endif
    }
    else {
    	raw_tunnelendpoint = RawTunnelendpointTCP::create(checkpoint_handler_, io_, connection_id);
    }
    RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(RawTunnelendpointConnectorTCPConnectionStatus::create(io_, raw_tunnelendpoint));
    raw_tunnelendpoints_status_.insert(make_pair(connection_id, connection_status));

    connection_status->get_timeout_timer().expires_after(boost::chrono::seconds(connect_timeout_.total_seconds()));
    connection_status->get_timeout_timer().async_wait(mutex_->get_strand().wrap(boost::bind(&RawTunnelendpointConnectorTCP::aio_connect_timeout, this, connection_id)));

    stringstream remote_port_ss;
    remote_port_ss << remote_port_;

    connection_status->set_state_resolving();
    boost::system::error_code resolve_error;
    boost::asio::ip::tcp::resolver::query query(remote_host_, remote_port_ss.str());
    resolver_.async_resolve(query, mutex_->get_strand().wrap(boost::bind(&RawTunnelendpointConnectorTCP::aio_resolve, this, boost::asio::placeholders::error, connection_id, boost::asio::placeholders::iterator)));
}

void RawTunnelendpointConnectorTCP::aio_resolve(
		const boost::system::error_code& error,
		const unsigned long connection_id,
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::aio_resolve");
    switch (state_) {
		case State_ReadyToConnect:
			break;
		case State_Closing:
			break;
		default: {
		    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_resolve.invalid_state", Attr_Communication(), Utility::CpAttr_error(), CpAttr_message(error.message()), CpAttr("state", state_));
			return;
		}
	}
    Checkpoint cp001(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_resolve", Attr_Communication(), Utility::CpAttr_debug(), CpAttr("state", state_));

	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.find(connection_id));
	if (i == raw_tunnelendpoints_status_.end()) {
	    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect_start.connection_lookup_failed", Attr_Communication(), Utility::CpAttr_error());
	    return;
	}
	RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
	connection_status->set_state_resolved();
    if(state_ == State_Closing) {
    	close_final();
    	return;
    }
    if(!connection_status->has_async_pending()) {
    	raw_tunnelendpoints_status_.erase(connection_id);
    	return;
    }

	if (!error) {
		connection_status->set_state_connecting();
        boost::asio::ip::tcp::socket& socket(connection_status->get_tunnelendpoint()->get_socket());
        boost::asio::ip::tcp::endpoint endpoint = *endpoint_iterator;
        socket.async_connect(endpoint, mutex_->get_strand().wrap(boost::bind(
        		&RawTunnelendpointConnectorTCP::aio_connect,
        		this,
        		boost::asio::placeholders::error,
        		connection_id,
        		++endpoint_iterator)));
	}
	else {
        Checkpoint cp(
        		*checkpoint_handler_,
        		"RawTunnelendpointConnectorTCP::aio_connect_start.resolve_error",
        		Attr_Communication(),
        		CpAttr_debug(),
        		CpAttr_message(error.message()));
        if (eventhandler_ != NULL) {
			stringstream ss;
			ss << "Connect error trying to connect to " << remote_host_ << ":" << remote_port_ << " error: " << error.message();
            eventhandler_->com_tunnelendpoint_connection_failed_to_connect(ss.str());
        }
        else {
            Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointConnectorTCP::aio_connect_start.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
        }
        close();
	}
}

void RawTunnelendpointConnectorTCP::aio_connect(
		const boost::system::error_code& error,
		const unsigned long connection_id,
		boost::asio::ip::tcp::resolver::iterator endpoint_iterator) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::aio_connect");
	switch (state_) {
		case State_ReadyToConnect:
			break;
		case State_Closing:
			break;
		default: {
		    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect.invalid_state", Attr_Communication(), Utility::CpAttr_error(), CpAttr("state", state_));
			return;
		}
	}

	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.find(connection_id));
	if (i == raw_tunnelendpoints_status_.end()) {
	    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect.connection_lookup_failed", Attr_Communication(), Utility::CpAttr_error());
	    return;
	}
	RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
	connection_status->set_state_connected();
    if(state_ == State_Closing) {
    	close_final();
    	return;
    }
    if(!connection_status->has_async_pending()) {
    	raw_tunnelendpoints_status_.erase(connection_id);
    	return;
    }

    CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect", Attr_Communication(), Utility::CpAttr_debug());
    try {
		if (!error) {
	    	cps.add_complete_attr(CpAttr("status", "ok"));
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
			if(ssl_enabled_) {
				connection_status->get_tunnelendpoint()->get_socket_ssl().async_handshake(
						boost::asio::ssl::stream_base::client,
						boost::bind(&RawTunnelendpointConnectorTCP::aio_handle_handshake,
								this,
								boost::asio::placeholders::error,
								connection_status));
			}
			else {
				aio_connect_conected(connection_status);
			}
#else
			aio_connect_conected(connection_status);
#endif
		}
		else if (endpoint_iterator != boost::asio::ip::tcp::resolver::iterator()) {
			connection_status->set_state_connecting();
			boost::asio::ip::tcp::socket& socket(connection_status->get_tunnelendpoint()->get_socket());
			socket.close();
			boost::asio::ip::tcp::endpoint endpoint = *endpoint_iterator;
			socket.async_connect(endpoint, mutex_->get_strand().wrap(boost::bind(
					&RawTunnelendpointConnectorTCP::aio_connect,
					this,
					boost::asio::placeholders::error,
					connection_id,
					++endpoint_iterator)));
		} else {
			cps.add_complete_attr(CpAttr("status", "error"));
			Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect.error", Attr_Communication(), CpAttr_debug(), CpAttr_message(error.message()));

			/* Do the proper close signaling to the connection event handler */
			boost::asio::ip::tcp::socket& socket(connection_status->get_tunnelendpoint()->get_socket());
			socket.close();

			if (eventhandler_ != NULL) {
				try {
	    			stringstream ss;
	    			ss << "Connect error trying to connect to " << remote_host_ << ":" << remote_port_ << " error: ";
	    			ss << error.message();
					eventhandler_->com_tunnelendpoint_connection_failed_to_connect(ss.str());
				}
				catch (const std::exception& e) {
					Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::com_tunnelendpoint_connection_failed_to_connect.callback_failed", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
				}
			}
			else {
				Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointConnectorTCP::com_tunnelendpoint_connection_failed_to_connect.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
				close();
				return;
			}
		}
    }
    catch (const std::exception& e) {
    	Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_connect.unexpected", Attr_Communication(), Utility::CpAttr_critical(), CpAttr_message(e.what()));
    	close();
    }
}

void RawTunnelendpointConnectorTCP::aio_connect_conected(const RawTunnelendpointConnectorTCPConnectionStatus::APtr& connection_status) {
    try {
    	connection_status->get_timeout_timer().expires_after(boost::chrono::seconds(0));
    	try {
    		connection_status->get_tunnelendpoint()->set_option_nodelay();
    	}
    	catch (const std::exception& e) {
    		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_connect_conected.set_option.unexpected", Attr_Communication(), CpAttr_error(), CpAttr_message(e.what()));
    	}
    	if (eventhandler_ != NULL) {
    		try {
    			RawTunnelendpointTCP::APtr raw_tunnelendpoint(connection_status->get_tunnelendpoint());
    			eventhandler_->com_tunnelendpoint_connected(raw_tunnelendpoint);
    		}
    		catch (const std::exception& e) {
    			Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_connect_conected.callback_failed", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    		}
    	}
    	else {
    		Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointConnectorTCP::aio_connect_conected.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
    		close();
    		return;
    	}
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_connect_conected.unexpected", Attr_Communication(), Utility::CpAttr_critical(), CpAttr_message(e.what()));
		close();
    }
}

void RawTunnelendpointConnectorTCP::aio_handle_handshake(
		const boost::system::error_code& error,
		const RawTunnelendpointConnectorTCPConnectionStatus::APtr& connection_status) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::aio_handle_handshake");
    switch (state_) {
    case State_ReadyToConnect:
 	case State_Closing:
 		break;
 	default: {
 		Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_handle_handshake.invalid_state", Attr_Communication(), Utility::CpAttr_error(), CpAttr("state", state_));
 		return;
 	}
    }
    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_handle_handshake", Attr_Communication(), Utility::CpAttr_debug());

	if (!error) {
		aio_connect_conected(connection_status);
	}
    else {
		Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_handle_handshake.error", Attr_Communication(), CpAttr_error(), CpAttr_message(error.message()), CpAttr("state", "state_"));
    	if (eventhandler_ != NULL) {
    		try {
    			stringstream ss;
    			ss << "SSL handshake failed trying to connect to " << remote_host_ << ":" << remote_port_ << " error_code: " << error;
    			ss << " error_message:" << error.message();
    			eventhandler_->com_tunnelendpoint_connection_failed_to_connect(ss.str());
    		}
    	    catch (const std::exception& e) {
    			Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_handle_handshake.callback_failed", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    	    }
    	}
		close();
    }
}

void RawTunnelendpointConnectorTCP::aio_connect_timeout(const unsigned long connection_id) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::aio_connect");
	switch (state_) {
    case State_ReadyToConnect:
 	case State_Closing:
 		break;
 	default: {
 		Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect_timeout.invalid_state", Attr_Communication(), Utility::CpAttr_error(), CpAttr("state", state_));
 		return;
 	}
	}
	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.find(connection_id));
	if (i == raw_tunnelendpoints_status_.end()) {
	    Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect_timeout.connection_lookup_failed", Attr_Communication(), Utility::CpAttr_error());
	    return;
	}

    Checkpoint cp001(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::aio_connect_timeout", Attr_Communication(), Utility::CpAttr_debug(), CpAttr("state", state_));

	RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
	connection_status->set_timeout();
    if(state_ == State_Closing) {
    	close_final();
    	return;
    }

    if(!connection_status->has_async_pending()) {
    	raw_tunnelendpoints_status_.erase(connection_id);
    	return;
    }


	if (eventhandler_ != NULL) {
		try {
			stringstream ss;
			ss << "Connect timeout trying to connect to " << remote_host_ << ":" << remote_port_;
			eventhandler_->com_tunnelendpoint_connection_failed_to_connect(ss.str());
		}
	    catch (const std::exception& e) {
			Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointConnectorTCP::aio_connect_timeout.callback_failed", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	    }
	}
	else {
		Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointConnectorTCP::aio_connect_timeout.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
		close();
	}
}

void RawTunnelendpointConnectorTCP::close(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::close");
	switch (state_) {
		case State_ReadyToConnect:
			break;
		default: {
			return;
		}
	}
	state_ = State_Closing;

	CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::close", Attr_Communication(), Utility::CpAttr_debug());
	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.begin());
	while(i != raw_tunnelendpoints_status_.end()) {
		RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
		connection_status->get_tunnelendpoint()->aio_close_start(true);
		if(!connection_status->is_timeout()) {
			connection_status->get_timeout_timer().expires_after(boost::chrono::seconds(0));
		}
		++i;
	}

	close_final();
}

void RawTunnelendpointConnectorTCP::close_final(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::close_final");
	switch (state_) {
		case State_Closing:
			break;
		default: {
			return;
		}
	}
	CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::close_final", Attr_Communication(), Utility::CpAttr_debug());
	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.begin());
	while(i != raw_tunnelendpoints_status_.end()) {
		RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
		if(!connection_status->is_timeout()) {
			if(connection_status->has_async_pending()) {
				return;
			}
		}
		++i;
	}
	raw_tunnelendpoints_status_.clear();
	state_ = State_Closed;
}

bool RawTunnelendpointConnectorTCP::async_mem_guard_is_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointConnectorTCP::async_mem_guard_is_done");
	switch(state_) {
	case State_Closed:
		Checkpoint(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::async_mem_guard_is_done.done", Attr_Communication(), Utility::CpAttr_debug());
		return true;
	}
	Checkpoint(*checkpoint_handler_, "RawTunnelendpointConnectorTCP::async_mem_guard_is_done.not_dones", Attr_Communication(), Utility::CpAttr_debug());
	return false;
}

std::string RawTunnelendpointConnectorTCP::async_mem_guard_get_name(void) {
	stringstream ss;
	ss << "x" << __FUNCTION__;
	ss << " state:" << state_;
	ss << " (";
	ConnectionsStatus::const_iterator i(raw_tunnelendpoints_status_.begin());
	while(i != raw_tunnelendpoints_status_.end()) {
		RawTunnelendpointConnectorTCPConnectionStatus::APtr connection_status(i->second);
		ss << connection_status->get_state() << ", ";
		++i;
	}
	ss << " )";
	return ss.str();
}
