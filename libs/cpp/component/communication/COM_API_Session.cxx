/*! \file COM_Sessionr.cxx
 \brief This file contains the implementation of the session API wrapper
 */

// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <boost/asio.hpp>

#include <sstream>
#include <boost/bind.hpp>

#include <component/communication/COM_API_Session.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * APISession implementation
 * ------------------------------------------------------------------
 */
APISession::APISession(Session::APtr session) :
    impl_session_(session), eventhandler_ready_(NULL) {
}

APISession::~APISession(void) {
	reset_eventhandler_ready();
}

APISession::APtr APISession::create(const Session::APtr& session) {
    return APtr(new APISession(session));
}

APIMutex::APtr APISession::get_mutex(void) {
    return APIMutex::create(impl_session_->get_mutex());
}

void APISession::set_eventhandler_ready(APISessionEventhandlerReady* eventhandler_ready) {
    eventhandler_ready_ = eventhandler_ready;
    impl_session_->set_eventhandler_ready(this);
}

void APISession::reset_eventhandler_ready(void) {
	impl_session_->reset_eventhandler_ready();
    eventhandler_ready_ = NULL;
}

void APISession::session_state_ready(void) {
    if(eventhandler_ready_ != NULL) {
    	eventhandler_ready_->session_state_ready();
    }
}
void APISession::session_state_closed(void) {
    if(eventhandler_ready_ != NULL) {
    	eventhandler_ready_->session_state_closed();
    }
}

void APISession::session_state_key_exchange(void) {
    if(eventhandler_ready_ != NULL) {
    	eventhandler_ready_->session_state_key_exchange();
    }
}

bool APISession::session_read_continue_state_ready(void) {
    return true;
}

void APISession::session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    if(eventhandler_ready_ != NULL) {
    	eventhandler_ready_->session_user_signal(signal_id, message->get_buffer()->toString());
    }
}

void APISession::close(const bool& force) {
    impl_session_->close(force);
}

bool APISession::is_closed(void) const {
    return impl_session_->is_closed();
}

unsigned long APISession::get_session_id(void) const {
    return impl_session_->get_session_id();
}

std::string APISession::get_unique_session_id(void) const {
    return impl_session_->get_unique_session_id();
}

std::string APISession::get_remote_unique_session_id(void) const {
    return impl_session_->get_remote_unique_session_id();
}
bool APISession::get_remote_session_logging_enabled(void) const {
    return impl_session_->get_remote_session_logging_enabled();
}

std::pair<std::string, unsigned long> APISession::get_ip_remote(void) const {
    return impl_session_->get_ip_remote();
}

std::pair<std::string, unsigned long> APISession::get_ip_local(void) const {
    return impl_session_->get_ip_local();
}

void APISession::add_tunnelendpoint(const unsigned long& child_id, const APITunnelendpoint::APtr& tunnelendpoint) {
    impl_session_->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}

void APISession::add_tunnelendpoint(const unsigned long& child_id, const Tunnelendpoint::APtr& tunnelendpoint) {
    impl_session_->add_tunnelendpoint(child_id, tunnelendpoint);
}

void APISession::read_start_state_ready(void) {
    impl_session_->read_start_state_ready();
}

void APISession::set_keep_alive_ping_interval(const boost::posix_time::time_duration& keep_alive_ping_interval) {
    impl_session_->set_keep_alive_ping_interval(keep_alive_ping_interval);
}


void APISession::self_close(const APISession::APtr& self, const bool& force) {
    assert(self.get() != NULL);
    self->close(force);
}

bool APISession::self_is_closed(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return self->is_closed();
}

unsigned long APISession::self_get_session_id(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return self->get_session_id();
}
boost::python::str APISession::self_get_unique_session_id(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return boost::python::str(self->get_unique_session_id());
}
boost::python::str APISession::self_get_remote_unique_session_id(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return boost::python::str(self->get_remote_unique_session_id());
}
bool APISession::self_get_remote_session_logging_enabled(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return self->get_remote_session_logging_enabled();
}

boost::python::tuple APISession::self_get_ip_remote(const APISession::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_remote());
    return boost::python::make_tuple(ip.first, ip.second);
}

boost::python::tuple APISession::self_get_ip_local(const APISession::APtr& self) {
    assert(self.get() != NULL);
    std::pair<std::string, unsigned long> ip(self->get_ip_local());
    return boost::python::make_tuple(ip.first, ip.second);
}

void APISession::self_add_tunnelendpoint_reliable_crypted_tunnel(const APISession::APtr& self,
                                                                 const unsigned long& child_id,
                                                                 const APITunnelendpointReliableCryptedTunnel::APtr& tunnelendpoint) {
    assert(self.get() != NULL);
    self->add_tunnelendpoint(child_id, tunnelendpoint);
}

void APISession::self_add_tunnelendpoint_reliable_crypted(const APISession::APtr& self,
                                                          const unsigned long& child_id,
                                                          const APITunnelendpointReliableCrypted::APtr& tunnelendpoint) {
    assert(self.get() != NULL);
    self->add_tunnelendpoint(child_id, tunnelendpoint);
}


void APISession::self_set_eventhandler_ready(const APISession::APtr& self, APISessionEventhandlerReady* eventhandler) {
    assert(self.get() != NULL);
    assert(eventhandler != NULL);
    self->set_eventhandler_ready(eventhandler);
}

void APISession::self_read_start_state_ready(const APISession::APtr& self) {
    assert(self.get() != NULL);
    self->read_start_state_ready();
}


void APISession::self_set_keep_alive_ping_interval_sec(const APISession::APtr& self, const long& keep_alive_ping_interval_sec) {
    assert(self.get() != NULL);
    boost::posix_time::time_duration keep_alive_ping_interval_w = boost::posix_time::seconds(keep_alive_ping_interval_sec);
    self->set_keep_alive_ping_interval(keep_alive_ping_interval_w);
}

APIMutex::APtr APISession::self_get_mutex(const APISession::APtr& self) {
    assert(self.get() != NULL);
    return self->get_mutex();
}
