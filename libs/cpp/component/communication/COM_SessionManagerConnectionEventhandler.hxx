/*! \file COM_SessionManagerConnectionEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from the session manager connections
 */
#ifndef COM_SessionManagerConnectionEventhandler_HXX
#define COM_SessionManagerConnectionEventhandler_HXX

#include <component/communication/COM_SessionCrypter.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for handling signals from the session manager connections
 */
class SessionManagerConnectionEventhandler: public boost::noncopyable {
public:

    /*! \brief Signals that a connection has been established
     */
    virtual void session_manager_connection_connected(const unsigned long& id, const SessionCrypter::APtr&) = 0;

    /*! \brief Signals that connection failed
     */
    virtual void session_manager_connection_failed(const unsigned long& id) = 0;

    /*! \brief Ask for resolve of connection
     */
    virtual void session_manager_connection_resolve_connection(std::string& type, std::string& host, unsigned long& port, boost::posix_time::time_duration& timeout_) = 0;
};

}
}
#endif
