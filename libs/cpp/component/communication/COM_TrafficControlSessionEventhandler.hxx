/*! \file COM_TrafficControlSessionEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from the session tunnelendpont
 */
#ifndef COM_TrafficControlSessionEventhandler_HXX
#define COM_TrafficControlSessionEventhandler_HXX

#include <component/communication/COM_Message.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for handling signals to TrafficControlSession
 */
class TrafficControlSessionEventhandler : public boost::noncopyable {
public:

    /*! \brief A remote message should be send
     */
    virtual void traffic_controll_session_send_message(const MessageCom::APtr&) = 0;

};
}
}
#endif
