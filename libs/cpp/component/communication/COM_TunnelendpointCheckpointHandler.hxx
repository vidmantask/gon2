/*! \file COM_TunnelendpointCheckpointHandler.hxx
 \brief This file contains the tunnelendpoint checkpoint_handler clases
 */
#ifndef COM_TunnelendpointCheckpointHandler_HXX
#define COM_TunnelendpointCheckpointHandler_HXX

#include <map>
#include <queue>
#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>


namespace Giritech {
namespace Communication {


/*! \brief Server side tunnelendpoint checkpoint_handler.
 *
 */
class TunnelendpointCheckpointHandlerServer:
	public TunnelendpointReliableCryptedTunnel,
	public TunnelendpointEventhandler {
public:

    /*! \brief Destructor
     */
    virtual ~TunnelendpointCheckpointHandlerServer(void);

    /*! \brief Set event handler
     */
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);

    /* Implementation of interface TunnelendpointEventhandler
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);


    /*! \brief Create instance
     */
    static APtr create(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
    		boost::asio::io_service& io,
    		const boost::filesystem::path& folder);

protected:

    /*! \brief Constructor
     */
    TunnelendpointCheckpointHandlerServer(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle, boost::asio::io_service& io, const boost::filesystem::path& folder);

    boost::filesystem::path generate_unique_filename(const boost::filesystem::path& basis_filename);
    boost::filesystem::path generate_filename(const boost::filesystem::path& basis_filename, const unsigned long counter);

    unsigned long id_;
    boost::filesystem::path folder_;
    boost::shared_ptr<boost::filesystem::ofstream> file_;
    bool file_is_open_;

};




/*! \brief Server side tunnelendpoint checkpoint_handler.
 *
 */
class TunnelendpointCheckpointHandlerClient:
	public TunnelendpointReliableCryptedTunnel,
	public TunnelendpointEventhandler,
	public Utility::CheckpointDestinationHandler {

public:
    typedef boost::shared_ptr<TunnelendpointCheckpointHandlerClient> APtr;

    /*! \brief Destructor
     */
    virtual ~TunnelendpointCheckpointHandlerClient(void);

    /*
     * Get checkpoint_handler
     */
    Utility::CheckpointHandler::APtr get_checkpoint_handler(void);


    /*! \brief Set
     */
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);


    /* Implementation of interface TunnelendpointEventhandler
     */
    virtual void tunnelendpoint_eh_recieve(const MessagePayload::APtr& message);
    virtual void tunnelendpoint_eh_connected(void);

    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    virtual bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);


    /* Implementation of interface CheckpointDestinationHandler
     */
    std::ostream& get_os(void);
    void close(void);
    void flush(void);

    boost::filesystem::path get_foldername(void);


    /*! \brief Create instance
     */
    static APtr create(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
    		boost::asio::io_service& io,
    		const Giritech::Utility::CheckpointHandler::APtr& parent_checkpoint_handler,
    		const Giritech::Utility::CheckpointFilter::APtr& checkpoint_filter,
    		const Giritech::Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler);

protected:

    /*! \brief Constructor
     */
    TunnelendpointCheckpointHandlerClient(
    		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
    		boost::asio::io_service& io,
    		const Giritech::Utility::CheckpointHandler::APtr& parent_checkpoint_handler,
    		const Giritech::Utility::CheckpointFilter::APtr& checkpoint_filter,
    		const Giritech::Utility::CheckpointOutputHandler::APtr& checkpoint_output_handler);

    Giritech::Utility::CheckpointHandler::APtr tunnel_checkpoint_handler_;




    std::stringstream ss_;
};


}
}
#endif
