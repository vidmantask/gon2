/*! \file COM_TunnelEndpointTCPEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from a tcp-tunnelendpoint
 */
#ifndef COM_TunnelendpointTCPEventhandler_HXX
#define COM_TunnelendpointTCPEventhandler_HXX

#include <component/communication/COM_TunnelendpointTCP.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint connection
 */
class TunnelendpointTCPEventhandler: public boost::noncopyable {
public:
    /*! \brief Signals that the tunnelendpoint has connecde to the 'other' side and is ready to send/receive packages
     */
    virtual void tunnelendpoint_tcp_eh_ready(void) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(void) = 0;
};


/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointTCPTunnelEventhandler: public boost::noncopyable {
public:

    /*! \brief Signals that the tunnelendpoint is ready to handle traffic.
     *
     * The signal is send when the tunnelendpoint is connected to the 'other' side,
     * and is ready to handle traffic connections.
     */
    virtual void tunnelendpoint_tcp_eh_ready(const unsigned long id) = 0;

    /*! \brief Signals that the tunnelendpoint has been closed
     *
     * After this call the eventhandler is never called again
     */
    virtual void tunnelendpoint_tcp_eh_closed(const unsigned long id) = 0;

    /*! \brief Signals that the tunnelendpoint has recieved a message
     *
     *  \param message Message recived
     */
    virtual void tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message) = 0;
};

/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointTCPTunnelServerEventhandler: public TunnelendpointTCPTunnelEventhandler {
};


/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointTCPTunnelServerWithConnectionEngineEventhandler: public TunnelendpointTCPTunnelServerEventhandler {
public:
    /*! \brief Signals that the remote wants to connect to a child.
     */
    virtual void tunnelendpoint_tcp_eh_recieve_remote_connect(const unsigned long id) = 0;

    /*! \brief Signals that a remote child request close.
     */
    virtual void tunnelendpoint_tcp_eh_recieve_remote_close(const unsigned long id) = 0;

    /*! \brief Signals that a remote child has recived eof.
     */
    virtual void tunnelendpoint_tcp_eh_recieve_remote_eof(const unsigned long id) = 0;

    /*! \brief Signals that a remote child has closed.
     */
    virtual void tunnelendpoint_tcp_eh_recieve_remote_closed(const unsigned long id) = 0;
};



/*! \brief This class define the abstract eventhandler handling signals from a tcp-tunnelendpoint
 */
class TunnelendpointTCPTunnelClientEventhandler: public TunnelendpointTCPTunnelEventhandler {
public:
    /*! \brief Signals that a connection has been created.
     *
     * If true is returned the connection will be connected to the serverside,
     * else will the connection be closed and ignored
     */
    virtual bool tunnelendpoint_tcp_eh_connected(const unsigned long id, const TunnelendpointTCP::APtr& connection) = 0;

    /*! \brief Signals that it was not possible to create the requested acceptor
     */
    virtual void tunnelendpoint_tcp_eh_acceptor_error(const unsigned long id, const std::string& message) = 0;

    /*! \brief Return true if the accept_start should continue after a connection has been accepted.

     If False the the accept_start should be called again to start accepting connection.
     If no eventhandler is defined then the default value is true.
     */
    virtual bool tunnelendpoint_tcp_eh_accept_start_continue(const unsigned long id) = 0;
};

}
}
#endif
