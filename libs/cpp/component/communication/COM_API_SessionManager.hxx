/*! \file COM_API_SessionManager.hxx
 \brief This file contains the a API wrapper for the SessionManager
 */
#ifndef COM_API_SessionManager_HXX
#define COM_API_SessionManager_HXX

#include <string>

#include <boost/shared_ptr.hpp>
#include <boost/ref.hpp>
#include <boost/bind.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_SessionManagerEventhandler.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for a event handler.
 */
class APISessionManagerEventhandler {
public:

    /*! \brief Ask for resolve of connection
     */
    virtual boost::python::dict session_manager_resolve_connection(const boost::python::dict& resolve_info) = 0;

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    virtual void session_manager_connecting(const unsigned long& connection_id, const std::string& title) = 0;

    /*! \brief Signals that a the session manager failed when trying to connect to server
     */
    virtual void session_manager_connecting_failed(const unsigned long& connection_id) = 0;

    /*! \brief Signals that a the session manager failed in estabblished in connecting og setup of connection
     */
    virtual void session_manager_failed(const std::string& message) = 0;

    /*! \brief Signals that a session has been created
     */
    virtual void session_manager_session_created(const unsigned long& connection_id, APISession::APtr& session) = 0;

    /*! \brief Signals that a session has been created
     */
    virtual void session_manager_session_closed(const unsigned long& session_id) = 0;

    /*! \brief Signals that a session manager has closed
     */
    virtual void session_manager_closed(void) = 0;

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    virtual void session_manager_security_dos_attack_start(const std::string& attacker_ips) = 0;

    /*! \brief Signals that session_manager leave DoS attack mode
     */
    virtual void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) = 0;
};


/*! \brief APISessionManagerClient
 */
class APISessionManagerClient: public SessionManagerEventhandler {
public:
    typedef boost::shared_ptr<APISessionManagerClient> APtr;

    /*! \brief Destructor
     */
    ~APISessionManagerClient(void);

    /*! \brief Set servers specification
     */
    void set_servers(const std::string& servers_spec);
    void add_server_connection(const std::string& server_ip, const unsigned long server_port);

    /*! \brief Get session manager mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Start eventloop
     *
     * Releases the python-lock
     */
    void start(void);

    /*! \brief Stop eventloop, connector and session
     */
    void close_start(void);

    /*! \brief Set the eventhandler
     */
    void set_eventhandler(APISessionManagerEventhandler* session_manager_eventhandler);

    /*! \brief Reset the eventhandler
     */
    void reset_eventhandler(void);

    /*! \brief Returns true if the sessionmanager is closed
     */
    bool is_closed(void) const;

    /*! \brief Enable/disable session logging (only to tell remote session).
     */
    void set_config_session_logging_enabled(const bool& session_logging_enabled);

    /*! \brief Ask for resolve of connection
     */
    void session_manager_resolve_connection(std::string& type,
                                            std::string& host,
                                            unsigned long& port,
                                            boost::posix_time::time_duration& timeout);

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    void session_manager_connecting(const unsigned long& connection_id, const std::string& title);

    /*! \brief Signals that a the session manager failed when trying to connect to server
     */
    void session_manager_connecting_failed(const unsigned long& connection_id);

    /*! \brief Signals that a the session manager failed in estabblished in connecting og setup of connection
     */
    void session_manager_failed(const std::string& message);

    /*! \brief Signals that a session has been created
     */
    void session_manager_session_created(const unsigned long& connection_id, Session::APtr& session);

    /*! \brief Signals that a session has been created
     */
    void session_manager_session_closed(const unsigned long session_id);

    /*! \brief Signals that a session has been created
     */
    void session_manager_closed(void);

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    void session_manager_security_dos_attack_start(const std::string& attacker_ips);

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count);

    /*! \brief Create instance
     */
    static APtr create(const APIAsyncService::APtr& api_async_service,
                       APISessionManagerEventhandler* session_manager_eventhandler,
                       const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                       const std::string& known_secret,
                       const SessionMessage::ApplProtocolType appl_protocol_type);

    static void self_set_eventhandler(const APISessionManagerClient::APtr& self, APISessionManagerEventhandler* session_manager_eventhandler);
    static void self_start(const APISessionManagerClient::APtr& self);
    static void self_close_start(const APISessionManagerClient::APtr& self);
    static bool self_is_closed(const APISessionManagerClient::APtr& self);
    static void self_set_servers(const APISessionManagerClient::APtr& self, const std::string& servers_spec);
    static void self_add_server_connection(const APISessionManagerClient::APtr& self, const std::string& server_ip, const unsigned long server_port);

    static void self_set_config_session_logging_enabled(const APISessionManagerClient::APtr& self, const bool& session_logging_enabled);
    static APIMutex::APtr self_get_mutex(const APISessionManagerClient::APtr& self);

protected:

    /*! \brief Constructor
     */
    APISessionManagerClient(const APIAsyncService::APtr& api_async_service,
                            APISessionManagerEventhandler* session_manager_eventhandler,
                            const Utility::CheckpointHandler::APtr& checkpoint_handler,
                            const std::string& known_secret,
                            const SessionMessage::ApplProtocolType appl_protocol_type);

    APISessionManagerEventhandler* session_manager_eventhandler_;
    Utility::DataBufferManaged::APtr known_secret_;
    SessionManagerClient::APtr impl_session_manager_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
};

/*! \brief APISessionManagerServer
 */
class APISessionManagerServer: public SessionManagerEventhandler {
public:
    typedef boost::shared_ptr<APISessionManagerServer> APtr;

    /*! \brief Destructor
     */
    ~APISessionManagerServer(void);

    /*! \brief Get session manager mutex
     */
    APIMutex::APtr get_mutex(void);

    /*! \brief Set durration for session idle timeout
     */
    void set_config_idle_timeout(const boost::posix_time::time_duration& session_idle_timeout, const boost::posix_time::time_duration& keyexchange_idle_timeout);

    /*! \brief Set durration log entries
     */
    void set_config_log_interval(const boost::posix_time::time_duration& config_log_interval);

    /*! \brief Enable/disable session logging (only to tell remote session).
     */
    void set_config_session_logging_enabled(const bool& session_logging_enabled);

    /*! \brief Set reuse of address option for acceptor.
     */
    void set_option_reuse_address(const bool& option_reuse_address);

    /*! \brief Config of dos attack fence
     */
    void set_config_security_dos_attack(const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips);

    /*! \brief Config of https support
     */
    void set_config_https(
    		const std::string& certificate_path,
    		const bool certificate_verificatrion_enabled,
    		const bool hostname_verification_enabled,
    		const bool sni_enabled,
    		const std::string& method,
    		const unsigned long ssl_options);

    /*! \brief Start eventloop
     *
     * Releases the python-lock
     */
    void start(void);

    /*! \brief Start accepting connections
     */
    void accept_connections_start(void);

    /*! \brief Stop accepting connection
     */
    void accept_connections_stop(void);

    /*! \brief Close session manager nicely
     */
    void close_start(void);

    /*! \brief Returns true if the sessionmanager is closed
     */
    bool is_closed(void) const;

    /*! \brief Ask for resolve of connection
     */
    void session_manager_resolve_connection(std::string& type,
                                            std::string& host,
                                            unsigned long& port,
                                            boost::posix_time::time_duration& timeout);

    /*! \brief Signals that a the session manager is trying to connect to server
     */
    void session_manager_connecting(const unsigned long& connection_id, const std::string& title);

    /*! \brief Signals that a the session manager failed when trying to connect to server
     */
    void session_manager_connecting_failed(const unsigned long& connection_id);

    /*! \brief Signals that a the session manager failed in estabblished in connecting og setup of connection
     */
    void session_manager_failed(const std::string& message);

    /*! \brief Signals that a session has been created
     */
    void session_manager_session_created(const unsigned long& connection_id, Session::APtr& session);

    /*! \brief Signals that a session has been created
     */
    void session_manager_session_closed(const unsigned long session_id);

    /*! \brief Signals that a session has been created
     */
    void session_manager_closed(void);

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    void session_manager_security_dos_attack_start(const std::string& attacker_ips);

    /*! \brief Signals that session_manager enter DoS attack mode
     */
    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count);

    /*! \brief Create instance
     */
    static APtr create(const APIAsyncService::APtr& api_async_service,
                       APISessionManagerEventhandler* session_manager_eventhandler,
                       const Utility::APICheckpointHandler::APtr& checkpoint_handler,
                       const std::string& known_secret,
                       const std::string& server_ip,
                       const unsigned long& server_port,
                       const std::string& sid);

    static void self_start(const APISessionManagerServer::APtr& self);
    static void self_accept_connections_start(const APISessionManagerServer::APtr& self);
    static void self_accept_connections_stop(const APISessionManagerServer::APtr& self);
    static void self_close_start(const APISessionManagerServer::APtr& self);
    static bool self_is_closed(const APISessionManagerServer::APtr& self);
    static void self_set_config_idle_timeout_sec(const APISessionManagerServer::APtr& self, const unsigned long& session_timeout_sec, const unsigned long& keyexchange_timeout);
    static void self_set_config_log_interval_sec(const APISessionManagerServer::APtr& self, const unsigned long& sec);
    static void self_set_config_log_interval_min(const APISessionManagerServer::APtr& self, const unsigned long& min);
    static void self_set_config_session_logging_enabled(const APISessionManagerServer::APtr& self, const bool& session_logging_enabled);
    static void self_set_option_reuse_address(const APISessionManagerServer::APtr& self, const bool& session_logging_enabled);
    static void self_set_config_security_dos_attack(const APISessionManagerServer::APtr& self,const int keyexchange_phase_one_count_high, const int keyexchange_phase_one_count_low, const int security_closed_count_high, const int security_closed_count_low, const bool ban_atacker_ips);

    static void self_set_config_https(
    		const APISessionManagerServer::APtr& self,
    		const std::string& certificate_path,
    		const bool certificate_verificatrion_enabled,
    		const bool hostname_verification_enabled,
    		const bool sni_enabled,
    		const std::string& method,
    		const unsigned long ssl_options);


    static APIMutex::APtr self_get_mutex(const APISessionManagerServer::APtr& self);




protected:

    /*! \brief Constructor
     */
    APISessionManagerServer(const APIAsyncService::APtr& api_async_service,
                            APISessionManagerEventhandler* session_manager_eventhandler,
                            const Utility::CheckpointHandler::APtr& checkpoint_handler,
                            const std::string& known_secret,
                            const std::string& server_ip,
                            const unsigned long& server_port,
                            const std::string& sid);

    APISessionManagerEventhandler* session_manager_eventhandler_;
    Utility::DataBufferManaged::APtr known_secret_;
    SessionManagerServer::APtr impl_session_manager_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
};

}
}
#endif
