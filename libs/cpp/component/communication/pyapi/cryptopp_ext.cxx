/*! \file cryptopp_ext.cxx
 * \brief Python wrapper for cryptopp subset
 */

#include <string>

#include <boost/python.hpp>
#include <boost/shared_ptr.hpp>

// crypto++ headers
#include <rsa.h>
#include <pssr.h>
#include <osrng.h>
#include <integer.h>

using namespace std;
using namespace boost::python;


bool verify_public_key_signature(string mod_s, string exp_s, string msg, string signature) {
	CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Verifier verifier;
	CryptoPP::Integer m;
	m.Decode((const byte*)mod_s.c_str(), mod_s.size());
	verifier.AccessKey().SetModulus(m);
	CryptoPP::Integer e;
	e.Decode((const byte*)exp_s.c_str(), exp_s.size());
	verifier.AccessKey().SetPublicExponent(e);
    size_t signature_length = verifier.SignatureLength();
    // assert (signature.size() == signature_length);
    return verifier.VerifyMessage(
    		(const byte*)(msg.c_str()), msg.size(), 
    		(const byte*)(signature.c_str()), signature.size());
}

typedef boost::shared_ptr< CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Verifier > Verifier_APtr;

Verifier_APtr verifier_from_pair(string mod_s, string exp_s) {
	Verifier_APtr verifier = Verifier_APtr(new CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Verifier());
	CryptoPP::Integer m;
	m.Decode((const byte*)mod_s.c_str(), mod_s.size());
	verifier->AccessKey().SetModulus(m);
	CryptoPP::Integer e;
	e.Decode((const byte*)exp_s.c_str(), exp_s.size());
	verifier->AccessKey().SetPublicExponent(e);
	return verifier;
}

bool verifier_verify(Verifier_APtr verifier, string msg, string signature) {
    size_t signature_length = verifier->SignatureLength();
    assert (signature.size() == signature_length);
    return verifier->VerifyMessage(
    		(const byte*)(msg.c_str()), msg.size(), 
    		(const byte*)(signature.c_str()), signature.size());
}

typedef boost::shared_ptr< CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Signer > Signer_APtr;

Signer_APtr generate_signer(int size) {
	CryptoPP::AutoSeededRandomPool random_pool(false);
    return Signer_APtr(new CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Signer(random_pool, size));
}

Signer_APtr signer_decode(string encoded) {
	CryptoPP::StringSource bt(
			(const byte*)(encoded.c_str()), encoded.size(), 
			true);
	return Signer_APtr(new CryptoPP::RSASS<CryptoPP::PKCS1v15, CryptoPP::SHA1>::Signer(bt));
}

string signer_encode(Signer_APtr signer) {
    string encoded;
    CryptoPP::StringSink bt(encoded);
    signer->DEREncode(bt);
    return encoded;
}

string signer_get_mod(Signer_APtr signer) {
	CryptoPP::Integer mod = signer->GetKey().GetModulus();
	size_t length = mod.MinEncodedSize();
	byte* mod_buffer = new byte[length];
	mod.Encode(mod_buffer, length);
	string mod_s((char*)mod_buffer, length);
	delete[] mod_buffer;
	return mod_s;
}

string signer_get_exp(Signer_APtr signer) {
	CryptoPP::Integer exp = signer->GetKey().GetPublicExponent();
	size_t length = exp.MinEncodedSize();
	byte* exp_buffer = new byte[length];
	exp.Encode(exp_buffer, length);
	string exp_s((char*)exp_buffer, length);
	delete[] exp_buffer;
	return exp_s;
}

string signer_sign(Signer_APtr signer, string msg) {
    size_t signature_length = signer->SignatureLength();
    byte* buffer = new byte[signature_length];
    CryptoPP::AutoSeededRandomPool random_pool(false);
    size_t l = signer->SignMessage(
    		random_pool,
    		(const byte*)(msg.c_str()),
    		msg.size(),
    		buffer);
    assert(l == signature_length);
    string signature((const char*)(buffer), signature_length);
    delete[] buffer;
    return signature;
}

/*
 * Define cryptopp lib python api
 */
void def_api_lib_cryptopp(void) {
    /* Usage example:
	signer = generate_signer(1024)
	open('test.private', 'w').write(signer_encode(signer))
	verifier = signer_get_verifier(signer)
	encoded_veririfer = verifier_encode(verifier)
	random_challenge = 'the brown fox jumps and falls down again'
	signer = signer_decode(open('test.private').read())
	challenge_signature = signer_sign(signer, random_challenge)
	verifier = verifier_decode(encoded_veririfer)
	assert verifier_verify(verifier, random_challenge, challenge_signature)
    */
    def("verify_public_key_signature", &verify_public_key_signature);
	
    class_<Verifier_APtr>("Verifier");
    def("verifier_from_pair", &verifier_from_pair);
    def("verifier_verify", &verifier_verify);

    class_<Signer_APtr>("Signer");
    def("generate_signer", &generate_signer);
    def("signer_decode", &signer_decode);
    def("signer_encode", &signer_encode);
    def("signer_sign", &signer_sign);
    def("signer_get_mod", &signer_get_mod);
    def("signer_get_exp", &signer_get_exp);
}

BOOST_PYTHON_MODULE(communication_ext) {
    docstring_options doc_options(true);
    def_api_lib_cryptopp();
}
