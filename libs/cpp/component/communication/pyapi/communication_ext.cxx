/*! \file communication_ext.cxx
 * \brief Implementation of python communication component api
 *
 * The api uses python objects for callback. Because callback is done
 * from another thread we need to aquire the global python interpretenter lock before doing the callback.
 *
 */
// Includede before <iostream> to remove warning about redefinition of  "_POSIX_C_SOURCE"
#include <boost/python.hpp>
#include <iostream>

#include <string>

#include <boost/asio.hpp>
#include <boost/python/stl_iterator.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <component/communication/pyapi/PYAPI_Utility.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_API_AsyncService.hxx>
#include <component/communication/COM_API_Session.hxx>
#include <component/communication/COM_API_SessionManager.hxx>
#include <component/communication/COM_API_TunnelendpointTCP.hxx>
#include <component/communication/COM_API_RawTunnelendpoint.hxx>
#include <component/communication/COM_API_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_API_RawTunnelendpointAcceptor.hxx>
#include <lib/cryptfacility/CF_API.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/utility/UY_API_Checkpoint.hxx>
#include <lib/hagi/HAGI_APIServer.hxx>
#include <lib/rpc/pyapi/RPC_PyAPI.hxx>
#include <lib/version/Version.hxx>

#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
#include <openssl/crypto.h>
#endif

#ifdef GIRITECH_COMPILEOPTION_TARGET_LINUX
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#endif

using namespace std;
using namespace boost::python;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;


#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
extern "C" __declspec(dllexport) void __cdecl GetNewAndDeleteForCryptoPP(CryptoPP::PNew &pNew, CryptoPP::PDelete &pDelete) {
   pNew = &operator new;
   pDelete = &operator delete;
}
//static CryptoPP::PNew s_pNew = NULL;
//static CryptoPP::PDelete s_pDelete = NULL;
//
//extern "C" __declspec(dllexport) void __cdecl SetNewAndDeleteFromCryptoPP(CryptoPP::PNew pNew, CryptoPP::PDelete pDelete,  CryptoPP::PSetNewHandler pSetNewHandler) {
//    s_pNew = pNew;
//    s_pDelete = pDelete;
//}
//
//void * __cdecl operator new (size_t size) {
//    return s_pNew(size);
//}
//
//void __cdecl operator delete (void * p) {
//    s_pDelete(p);
//}
#endif

/*! \brief Initialice communication component
 *
 * Initialize python threads because it might not be initialized and cryptfacility.
 *
 */
void init_communication(const APICheckpointHandler::APtr& api_checkpoint_handler) {
    Py_Initialize();
    Giritech::CryptFacility::CryptFacilityService::getInstance().initialize(Giritech::CryptFacility::CryptFacilityService::modeofoperation_unknown);

#ifdef GIRITECH_COMPILEOPTION_CRYPTOPP_FIPS
    std::string cryptopp_module_filename;
    Giritech::CryptFacility::CryptFacilityService::getInstance().getCryptoPPModuleFilename(cryptopp_module_filename);
    Checkpoint cp(*(api_checkpoint_handler->get_checkpoint_handler()),
                  "cryptopp_fips",
                  Attr_Communication(),
                  CpAttr_info(),
                  CpAttr("cryptopp_dll_filename", cryptopp_module_filename),
                  CpAttr_message("Compiled for Crypto++ in FIPS mode"));
#endif
}



/*! \brief Callback wrapping Wrapper for APISessionManagerEventhander
 *
 */
class PYAPISessionManagerEventhandlerCallback : public APISessionManagerEventhandler {
public:
    PYAPISessionManagerEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPISessionManagerEventhandlerCallback(void) {
    }

    boost::python::dict session_manager_resolve_connection(const boost::python::dict& resolve_info) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_resolve_connection", api_checkpoint_handler_);
        try {
        	return call_method<boost::python::dict>(self_, "session_manager_resolve_connection", resolve_info);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_resolve_connection");
        }
        return resolve_info;
    }

    void session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_connecting", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_connecting", connection_id, title);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_connecting");
        }
    }

    void session_manager_connecting_failed(const unsigned long& connection_id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_connecting_failed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_connecting_failed", connection_id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_connecting_failed");
        }
    }

    void session_manager_failed(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_failed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_failed", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_failed");
        }
    }

    void session_manager_session_created(const unsigned long& connection_id, APISession::APtr& session) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_session_created", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_session_created", connection_id, session);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
        	throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_session_created");
        }
    }

    void session_manager_session_closed(const unsigned long& session_id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_session_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_session_closed", session_id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_session_closed");
        }
    }

    void session_manager_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_closed");
        }
    }

    void session_manager_security_dos_attack_start(const std::string& attacker_ips) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_security_dos_attack_start", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_security_dos_attack_start", attacker_ips);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_security_dos_attack_start");
        }
    }

    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_manager_security_dos_attack_stop", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_manager_security_dos_attack_stop", attacker_ips, banned_connection_count, failed_keyexchange_count);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionManagerEventhandlerCallback::session_manager_security_dos_attack_stop");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};

/*! \brief Callback wrapping Wrapper for APISessionEventhandlerReady
 *
 */
class PYAPISessionEventhandlerReadyCallback : public APISessionEventhandlerReady {
public:
    PYAPISessionEventhandlerReadyCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPISessionEventhandlerReadyCallback(void) {
    }
    void session_state_ready(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_state_ready", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_state_ready");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionEventhandlerReadyCallback::session_state_ready");
        }
    }
    void session_state_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_state_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_state_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionEventhandlerReadyCallback::session_state_closed");
        }
    }
    void session_state_key_exchange(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_state_key_exchange", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_state_key_exchange");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionEventhandlerReadyCallback::session_state_key_exchange");
        }
    }
    void session_user_signal(const unsigned long signal_id, const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("session_user_signal", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "session_user_signal", signal_id, message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPISessionEventhandlerReadyCallback::session_user_signal");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};

/*! \brief Callback wrapping Wrapper for APITunnelendpointEventhandler
 *
 */
class PYAPITunnelendpointEventhandlerCallback : public APITunnelendpointEventhandler {
public:
    PYAPITunnelendpointEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPITunnelendpointEventhandlerCallback(void) {
    }
    void tunnelendpoint_recieve(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_recieve", api_checkpoint_handler_);
        try {
        	call_method<void>(self_, "tunnelendpoint_recieve", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointEventhandlerCallback::tunnelendpoint_recieve");
        }
    }

    void tunnelendpoint_connected(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_connected", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_connected");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointEventhandlerCallback::tunnelendpoint_connected");
        }
    }

    void tunnelendpoint_recieve_user_signal(const unsigned long child_id, const unsigned long signal_id, const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_recieve_user_signal", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_recieve_user_signal", child_id, signal_id, message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointEventhandlerCallback::tunnelendpoint_recieve_user_signal");
        }
    }


private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};


/*! \brief Callback wrapping Wrapper for APITunnelendpointEventhandler
 *
 */
class PYAPITunnelendpointTCPEventhandlerCallback : public APITunnelendpointTCPEventhandler {
public:
    PYAPITunnelendpointTCPEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPITunnelendpointTCPEventhandlerCallback(void) {
    }
    void tunnelendpoint_tcp_eh_ready(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_ready", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_ready");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPEventhandlerCallback::tunnelendpoint_tcp_eh_ready");
        }
    }
    void tunnelendpoint_tcp_eh_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPEventhandlerCallback::tunnelendpoint_tcp_eh_closed");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};


/*! \brief Callback wrapping Wrapper for APITunnelendpointTCPTunnelClientEventhandler
 *
 */
class PYAPITunnelendpointTCPTunnelClientEventhandlerCallback : public APITunnelendpointTCPTunnelClientEventhandler {
public:
    PYAPITunnelendpointTCPTunnelClientEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPITunnelendpointTCPTunnelClientEventhandlerCallback(void) {
    }
    void tunnelendpoint_tcp_eh_ready(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_ready", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_ready");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_ready");
        }
    }
    void tunnelendpoint_tcp_eh_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_closed");
        }
    }

    void tunnelendpoint_tcp_eh_recieve(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_recieve");
        }
    }

    bool tunnelendpoint_tcp_eh_connected(const APITunnelendpointTCP::APtr& connection) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_connected", api_checkpoint_handler_);
        try {
            return call_method<bool>(self_, "tunnelendpoint_tcp_eh_connected", connection);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_connected");
        }
    }

    void tunnelendpoint_tcp_eh_acceptor_error(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_acceptor_error", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_acceptor_error", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_acceptor_error");
        }
    }

    bool tunnelendpoint_tcp_eh_accept_start_continue(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_accept_start_continue", api_checkpoint_handler_);
        try {
            return call_method<bool>(self_, "tunnelendpoint_tcp_eh_accept_start_continue");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelClientEventhandlerCallback::tunnelendpoint_tcp_eh_accept_start_continue");
        }
    }
private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};

/*! \brief Callback wrapping Wrapper for APITunnelendpointTCPTunnelServerEventhandler
 *
 */
class PYAPITunnelendpointTCPTunnelServerEventhandlerCallback : public APITunnelendpointTCPTunnelServerEventhandler {
public:
    PYAPITunnelendpointTCPTunnelServerEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPITunnelendpointTCPTunnelServerEventhandlerCallback(void) {
    }
    void tunnelendpoint_tcp_eh_ready(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_ready", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_ready");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerEventhandlerCallback::tunnelendpoint_tcp_eh_ready");
        }
    }
    void tunnelendpoint_tcp_eh_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerEventhandlerCallback::tunnelendpoint_tcp_eh_closed");
        }
    }

    void tunnelendpoint_tcp_eh_recieve(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerEventhandlerCallback::tunnelendpoint_tcp_eh_recieve");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};



/*! \brief Callback wrapping Wrapper for APITunnelendpointTCPTunnelServerEventhandler
 *
 */
class PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback :
	public APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler {
public:
		PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback(
				PyObject* self,
				const APICheckpointHandler::APtr& api_checkpoint_handler) :
		self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback(void) {
    }
    void tunnelendpoint_tcp_eh_ready(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_ready", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_ready");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_ready");
        }
    }
    void tunnelendpoint_tcp_eh_closed(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_closed");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_closed");
        }
    }

    void tunnelendpoint_tcp_eh_recieve(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_recieve");
        }
    }

    void tunnelendpoint_tcp_eh_recieve_remote_connect(const unsigned long id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve_remote_connect", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve_remote_connect", id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_recieve_remote_connect");
        }
    }

    void tunnelendpoint_tcp_eh_recieve_remote_close(const unsigned long id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve_remote_close", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve_remote_close", id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_recieve_remote_close");
        }
    }

    void tunnelendpoint_tcp_eh_recieve_remote_eof(const unsigned long id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve_remote_eof", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve_remote_eof", id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_recieve_remote_eof");
        }
    }

    void tunnelendpoint_tcp_eh_recieve_remote_closed(const unsigned long id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("tunnelendpoint_tcp_eh_recieve_remote_closed", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "tunnelendpoint_tcp_eh_recieve_remote_closed", id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback::tunnelendpoint_tcp_eh_recieve_remote_closed");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};


/*! \brief Callback wrapping Wrapper for APIRawTunnelendpointEventhandler
 *
 */
class PYAPIRawTunnelendpointEventhandlerCallback : public APIRawTunnelendpointEventhandler {
public:
    PYAPIRawTunnelendpointEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }
    ~PYAPIRawTunnelendpointEventhandlerCallback(void) {
    }
    void com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_raw_tunnelendpoint_close", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_raw_tunnelendpoint_close", connection_id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointEventhandlerCallback::com_raw_tunnelendpoint_close");
        }
    }
    void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_raw_tunnelendpoint_close_eof", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_raw_tunnelendpoint_close_eof", connection_id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointEventhandlerCallback::com_raw_tunnelendpoint_close_eof");
        }
    }
    void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const std::string& data) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_raw_tunnelendpoint_read", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_raw_tunnelendpoint_read", connection_id, data);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointEventhandlerCallback::com_raw_tunnelendpoint_read");
        }
    }
    void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_raw_tunnelendpoint_write_buffer_empty", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_raw_tunnelendpoint_write_buffer_empty", connection_id);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointEventhandlerCallback::com_raw_tunnelendpoint_write_buffer_empty");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};

/*! \brief Callback wrapping Wrapper for APIRawTunnelendpointConnectorEventhandler
 *
 */
class PYAPIRawTunnelendpointConnectorEventhandlerCallback : public APIRawTunnelendpointConnectorTCPEventhandler {
public:
    PYAPIRawTunnelendpointConnectorEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }

    ~PYAPIRawTunnelendpointConnectorEventhandlerCallback(void) {
    }

    void com_tunnelendpoint_connected(const APIRawTunnelendpointTCP::APtr& tunnelendpoint) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_tunnelendpoint_connected", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_tunnelendpoint_connected", tunnelendpoint);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointConnectorEventhandlerCallback::com_tunnelendpoint_connected");
        }
    }

    void com_tunnelendpoint_connection_failed_to_connect(const std::string& message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_tunnelendpoint_connection_failed_to_connect", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_tunnelendpoint_connection_failed_to_connect", message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
        	throw ExceptionCallback("PYAPIRawTunnelendpointConnectorEventhandlerCallback::com_tunnelendpoint_connection_failed_to_connect");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};


/*! \brief Callback wrapping Wrapper for APIRawTunnelendpointAcceptorEventhandler
 *
 */
class PYAPIRawTunnelendpointAcceptorEventhandlerCallback : public APIRawTunnelendpointAcceptorTCPEventhandler {
public:
    PYAPIRawTunnelendpointAcceptorEventhandlerCallback(PyObject* self, const APICheckpointHandler::APtr& api_checkpoint_handler) :
        self_(self), api_checkpoint_handler_(api_checkpoint_handler) {
    }

    ~PYAPIRawTunnelendpointAcceptorEventhandlerCallback(void) {
    }

    void com_tunnelendpoint_accepted(const APIRawTunnelendpointTCP::APtr& tunnelendpoint) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_tunnelendpoint_accepted", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_tunnelendpoint_accepted", tunnelendpoint);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointConnectorEventhandlerCallback::com_tunnelendpoint_accepted");
        }
    }

    void com_tunnelendpoint_acceptor_error(const std::string& error_message) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_tunnelendpoint_acceptor_error", api_checkpoint_handler_);
        try {
            call_method<void>(self_, "com_tunnelendpoint_acceptor_error", error_message);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointConnectorEventhandlerCallback::com_tunnelendpoint_acceptor_error");
        }
    }

    bool com_accept_continue(void) {
        Giritech::Communication::PYAPI::ThreadLock python_thread_lock("com_accept_continue", api_checkpoint_handler_);
        try {
            return call_method<bool>(self_, "com_accept_continue");
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            throw ExceptionCallback("PYAPIRawTunnelendpointConnectorEventhandlerCallback::com_accept_continue");
        }
    }

private:
    PyObject* const self_;
    APICheckpointHandler::APtr api_checkpoint_handler_;
};




CheckpointFilter::APtr CheckpointFilter_checkpoint_ids_set_create(const boost::python::list& l) {
    std::set<std::string> ids;
    for (stl_input_iterator<object> elem(l), end; elem != end; ++elem) {
        extract<std::string> string_value(*elem);
        if (string_value.check()) {
            /* Actually ... we try anyway to get decent error messages: */
        }
        ids.insert(string_value());
    }
    return CheckpointFilter_checkpoint_ids::create(ids);
}

CheckpointFilter::APtr CheckpointFilter_accept_if_attr_exist_create(const std::string& key,
                                                                    const std::string& value) {
    return CheckpointFilter_accept_if_attr_exist::create(CheckpointAttrS::create(key, value));
}

CheckpointOutputHandler::APtr CheckpointOutputHandlerXML_create(void) {
    return CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerCout::create());
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerXML_create_file(const std::string& filename, const bool close_on_flush) {
    return CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(filename), false, close_on_flush));
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerXML_create_file_append(const std::string& filename, const bool close_on_flush) {
    return CheckpointOutputHandlerXML::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(filename), true, close_on_flush));
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerTEXT_create(void) {
    return CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerCout::create());
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerTEXT_create_file(const std::string& filename, const bool close_on_flush) {
    return CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(filename), false, close_on_flush));
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerTEXT_create_file_append(const std::string& filename, const bool close_on_flush) {
    return CheckpointOutputHandlerTEXT::create(CheckpointDestinationHandlerFile::create(boost::filesystem::path(filename), true, close_on_flush));
}
CheckpointOutputHandler::APtr CheckpointOutputHandlerFolderXML_create(const std::string& folder) {
    return CheckpointOutputHandlerFolderXML::create(boost::filesystem::path(folder));
}

class CheckpointOutputHandler_Test : public CheckpointOutputHandler {
public:
    typedef boost::shared_ptr< CheckpointOutputHandler_Test > APtr;

    void operator()(const std::string& checkpoint_id,
              const CheckpointAttr::APtrs& event_attrs,
              const CheckpointAttr::Structure structure) {
        if (structure == CheckpointAttr::Structure_end) {
            checkpoint_ids_.insert(checkpoint_id+"_complete");
        } else {
            checkpoint_ids_.insert(checkpoint_id);
        }
    }

    void close(void) {

    }

    bool got_checkpoint_id(const std::string& checkpoint_id) const {
        std::set< std::string >::const_iterator iend(checkpoint_ids_.end());
        std::set< std::string >::const_iterator i(checkpoint_ids_.find(checkpoint_id));
        if (i != iend)
            return true;
        return false;
    }

    // Py-style method
    static bool got_checkpoint_id_aptr(APtr &self, const std::string& checkpoint_id) {
        return self->got_checkpoint_id(checkpoint_id);
    }

    // Returns APtr which doesn't inherit from base classes APtr...
    static APtr create(void) {
        return CheckpointOutputHandler_Test::APtr(new CheckpointOutputHandler_Test);
    }

    // Return APtr for downcast to base class. Py-style method
    static CheckpointOutputHandler::APtr get_CheckpointOutputHandler(APtr &self) {
        return CheckpointOutputHandler::APtr(self);
    }
private:
    std::set< std::string > checkpoint_ids_;
};


class CheckpointOutputHandlerCallback : boost::noncopyable {
public:
    CheckpointOutputHandlerCallback(PyObject* self) :
        self_(self) {
    }
    ~CheckpointOutputHandlerCallback(void) {
    }


    boost::python::object value_to_python(const CheckpointAttr::APtr& attr) {
		if (const CheckpointAttrS* attrs = dynamic_cast<const CheckpointAttrS*>(attr.get())) {
			return boost::python::str(attrs->get_value());
		} else if (const CheckpointAttrI* attri = dynamic_cast<const CheckpointAttrI*>(attr.get())) {
			return boost::python::long_(attri->get_value());
		} else if (const CheckpointAttrTS* attrts = dynamic_cast<const CheckpointAttrTS*>(attr.get())) {
			return boost::python::make_tuple(
			boost::python::long_(static_cast<long>(attrts->get_value().date().year())),
			boost::python::long_(static_cast<long>(attrts->get_value().date().month())),
			boost::python::long_(static_cast<long>(attrts->get_value().date().day())),
			boost::python::long_(static_cast<long>(attrts->get_value().time_of_day().hours())),
			boost::python::long_(static_cast<long>(attrts->get_value().time_of_day().minutes())),
			boost::python::long_(static_cast<long>(attrts->get_value().time_of_day().seconds())),
			boost::python::long_(static_cast<long>(attrts->get_value().time_of_day().fractional_seconds()))
			);
		} else if (const CheckpointAttrTD* attrtd = dynamic_cast<const CheckpointAttrTD*>(attr.get())) {
			return boost::python::make_tuple(
			boost::python::long_(static_cast<long>(attrtd->get_value().hours())),
			boost::python::long_(static_cast<long>(attrtd->get_value().minutes())),
			boost::python::long_(static_cast<long>(attrtd->get_value().seconds())),
			boost::python::long_(static_cast<long>(attrtd->get_value().fractional_seconds()))
			);
		}
		return boost::python::str("ERROR: No py converter");
	}


    void handle_output(const std::string& checkpoint_id,
              const CheckpointAttr::APtrs& event_attrs,
              const CheckpointAttr::Structure structure) {
        PyGILState_STATE gstate;
        gstate = PyGILState_Ensure();
        try {
        	boost::python::dict event_attrs_py;

            for (const auto& pair : event_attrs) {
        		event_attrs_py[pair.second->get_id()] = value_to_python(pair.second);
        	}
            call_method<void>(self_, "handle_output", checkpoint_id, event_attrs_py, structure);
            PyGILState_Release(gstate);
        }
        catch(boost::python::error_already_set) {
            PyErr_Print();
            PyErr_Clear();
            PyGILState_Release(gstate);
            throw ExceptionCallback("CheckpointOutputHandlerCallback::operator()");
        }
    }
private:
    PyObject* const self_;
};





class CheckpointOutputHandlerWrapper : public CheckpointOutputHandler {
public:
    typedef boost::shared_ptr< CheckpointOutputHandlerWrapper > APtr;

    CheckpointOutputHandlerWrapper(CheckpointOutputHandlerCallback* callback) :
		callback_(callback) {
	}
    ~CheckpointOutputHandlerWrapper(void) {
    	callback_ = NULL;
    }

    static CheckpointOutputHandler::APtr create(CheckpointOutputHandlerCallback* callback) {
    	return APtr(new CheckpointOutputHandlerWrapper(callback));
    }

    void operator()(const std::string& checkpoint_id, const CheckpointAttr::APtrs& checkpoint_attrs, const CheckpointAttr::Structure structure) {
    	if(callback_ != NULL) {
    		callback_->handle_output(checkpoint_id, checkpoint_attrs, structure);
    	}
    }

    void close(void) {
    }

private:
	CheckpointOutputHandlerCallback* callback_;
};

boost::python::tuple get_version(void) {
    Giritech::Communication::PYAPI::ThreadLockNoLog python_thread_lock;
    Version::Version::APtr version(Version::Version::create_current());
    return boost::python::make_tuple(version->get_version_major(), version->get_version_minor(), version->get_version_bugfix(), version->get_version_build_id(), version->get_build_date(), version->get_version_depot());
}

boost::python::str get_ssl_version(void) {
#ifdef GIRITECH_COMPILEOPTION_OPEN_SSL
  return boost::python::str(SSLeay_version(SSLEAY_VERSION));
#else
    return boost::python::str("ssl_not_available");
#endif
}


/*
 * fd_read_write
 *
 * Thies functions are implementation taken from python and only used for GD-token read/write on linux.
 * They were moved to c++ because they failed(memcpy) on 64-bit linux.
 */
#if defined(GIRITECH_COMPILEOPTION_TARGET_LINUX) || defined(GIRITECH_COMPILEOPTION_TARGET_MAC)
#define BLOCK_SIZE 512

boost::python::tuple fd_read(
		const int32_t fd) {
	size_t pos(lseek(fd, 0, SEEK_SET));
	if(pos != 0) {
	    return boost::python::make_tuple(-2, "");
	}

	void* mem_buffer(valloc(BLOCK_SIZE));
	size_t bytes_read = read(fd, mem_buffer, BLOCK_SIZE);
	if(bytes_read != BLOCK_SIZE) {
		return boost::python::make_tuple(-3, "");
	}

	std::string buffer((char*)mem_buffer, BLOCK_SIZE);
	free(mem_buffer);
	return boost::python::make_tuple(bytes_read, buffer);
}

int32_t fd_write(
		const int32_t fd,
		const std::string& buffer) {
	size_t pos(lseek(fd, 0, SEEK_SET));
	if(pos != 0) {
	    return -2;
	}

	if(buffer.size() != BLOCK_SIZE) {
		return -3;
	}

	void* mem_buffer(valloc(BLOCK_SIZE));
	memcpy(mem_buffer, buffer.c_str(), BLOCK_SIZE);
	int32_t rc(write(fd, mem_buffer, BLOCK_SIZE));
	fsync(fd);
	free(mem_buffer);
	return rc;
}
#endif

void sim_throw(void) {
    throw ExceptionCallback("sim_throw from C++");
}

/*
 * Define utility lib python api
 */
void def_api_lib_utility(void) {
    /* Usage example:

     import checkpoint_ext
     filter = checkpoint_ext.CheckpointFilter_true()
     output_handler = checkpoint_ext.CheckpointOutputHandlerTEXT()
     handler = checkpoint_ext.APICheckpointHandler(filter, output_handler)
     scope = handler.CheckpointScope('foo', dict(a='asdf', b='bar'))
     checkpoint_ext.add_complete_attr(scope, dict(xyzzy='1234'))
     handler.Checkpoint('baz', dict(gog='thin', gokke='fat'))
     checkpoint_ext.end_scope(scope)

     - but it is meant to be Wrapped further in python to make it more py/oo-ish
     */

	enum_<CheckpointAttr::Structure>("Structure")
	    .value("BEGIN", CheckpointAttr::Structure_begin)
	    .value("END", CheckpointAttr::Structure_end)
	    .value("BEGIN_END", CheckpointAttr::Structure_begin_end)
	    ;


	// CheckpointFilter
    class_<CheckpointFilter::APtr>("CheckpointFilter") ;
    def("CheckpointFilter_true", &CheckpointFilter_true::create);
    def("CheckpointFilter_false", &CheckpointFilter_false::create);
    def("CheckpointFilter_or", &CheckpointFilter_or::create);
    def("CheckpointFilter_and", &CheckpointFilter_and::create);
    def("CheckpointFilter_not", &CheckpointFilter_not::create);
    CheckpointFilter::APtr
            (*ids_string)(const std::string& id) = &CheckpointFilter_checkpoint_ids::create;
    def("CheckpointFilter_checkpoint_ids", ids_string);
    def("CheckpointFilter_checkpoint_ids", CheckpointFilter_checkpoint_ids_set_create);
    def("CheckpointFilter_skip_complete", &CheckpointFilter_skip_complete::create);
    def("CheckpointFilter_accept_if_attr_exist", &CheckpointFilter_accept_if_attr_exist_create);

    // CheckpointFilter
    class_<CheckpointOutputHandler::APtr>("CheckpointOutputHandler") ;
    // XML
    def("CheckpointOutputHandlerXML", &CheckpointOutputHandlerXML_create);
    def("CheckpointOutputHandlerXML", &CheckpointOutputHandlerXML_create_file);
    def("CheckpointOutputHandlerXMLAppend", &CheckpointOutputHandlerXML_create_file_append);
    // Text
    def("CheckpointOutputHandlerTEXT", &CheckpointOutputHandlerTEXT_create);
    def("CheckpointOutputHandlerTEXT", &CheckpointOutputHandlerTEXT_create_file);
    def("CheckpointOutputHandlerTEXTAppend", &CheckpointOutputHandlerTEXT_create_file_append);

    // XML folder
    def("CheckpointOutputHandlerFolderXML", &CheckpointOutputHandlerFolderXML_create);

    // Test
    def("CheckpointOutputHandler_Test", &CheckpointOutputHandler_Test::create);
    class_<CheckpointOutputHandler_Test::APtr>("CheckpointOutputHandler_Test_APtr")
    // APtr methods not possible, use Py-style
    ;
    def("got_checkpoint_id", &CheckpointOutputHandler_Test::got_checkpoint_id_aptr);
    def("get_CheckpointOutputHandler", &CheckpointOutputHandler_Test::get_CheckpointOutputHandler);

    /*
     * CheckpointOutputHandler Callback
     */
    class_<CheckpointOutputHandlerCallback, CheckpointOutputHandlerCallback, boost::noncopyable>("CheckpointOutputHandlerCallback");
    def("CheckpointOutputHandlerCallbackWrapper_create", &CheckpointOutputHandlerWrapper::create);

    // CheckpointHandler
    class_<APICheckpointHandler::APtr>("APICheckpointHandler", "Handler for instrumentation")
    .def("Checkpoint", &APICheckpointHandler::self_createCheckpoint, "Instrumentation logging point")
    .def("CheckpointScope", &APICheckpointHandler::self_createCheckpointScope, "Instrumentation logging scope")
    .def("set_output_handler", &APICheckpointHandler::self_set_output_handler, "Set new output handler")
    .def("close", &APICheckpointHandler::self_close, "Close handler");

    def("APICheckpointHandler_create", &APICheckpointHandler::create);
    def("APICheckpointHandler_create_with_parent", &APICheckpointHandler::create_with_parent);

    // CheckpointScope
    class_<APICheckpointScope::APtr>("APICheckpointScope")
    // It would be convenient to have add_complete_attr and __exit__ here - but that's not possible, so we make it py-style...
    ;
    def("add_complete_attr", &APICheckpointScope::add_complete_attr_aptr);
    def("end_scope", &APICheckpointScope::end_scope_aptr);

    def("get_version", &get_version);
    def("get_ssl_version", &get_ssl_version);

    def("sim_throw", &sim_throw);

}




void session_helper_self_add_tunnelendpoint_tcp_tunnel_server(
		const APISession::APtr& self, const unsigned long& child_id,
		const APITunnelendpointTCPTunnelServer::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}

void tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_server(
		const APITunnelendpointReliableCryptedTunnel::APtr& self,
		const unsigned long& child_id,
		const APITunnelendpointTCPTunnelServer::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}

void tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_server_with_connection_engine(
		const APITunnelendpointReliableCryptedTunnel::APtr& self,
		const unsigned long& child_id,
		const APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}

void session_helper_self_add_tunnelendpoint_tcp_tunnel_client(
		const APISession::APtr& self, const unsigned long& child_id,
		const APITunnelendpointTCPTunnelClient::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}

void tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_client(
		const APITunnelendpointReliableCryptedTunnel::APtr& self,
		const unsigned long& child_id,
		const APITunnelendpointTCPTunnelClient::APtr& tunnelendpoint) {
	assert(self.get() != NULL);
	self->add_tunnelendpoint(child_id, tunnelendpoint->get_impl());
}


/*
 * Define communication component python api
 */
void def_api_component_communication(void) {

	enum_<SessionMessage::ApplProtocolType>("ApplProtocolType")
	    .value("PYTHON", SessionMessage::ApplProtocolType_python)
	    .value("XML", SessionMessage::ApplProtocolType_xml)
	    ;

    /*
     * Initialization
     */
    def("init_communication", &init_communication);

    /*
     * APIAsyncService
     */
    class_<APIAsyncService::APtr>("APIAsyncService")
    .def("run", &APIAsyncService::self_run, "Start async service main loop")
    .def("reset", &APIAsyncService::self_reset, "Reset async service for new run")
    .def("stop", &APIAsyncService::self_stop, "Stop async service main loop")
    .def("sleep_start", &APIAsyncService::self_sleep_start, "Request a asyncron callback after a period of time")
    .def("sleep_start_mutex", &APIAsyncService::self_sleep_start_mutex, "Request a asyncron callback after a period of time")
    .def("create_mutex", &APIAsyncService::self_create_mutex, "Create a user mutex")
    .def("memory_guard_cleanup", &APIAsyncService::self_memory_guard_cleanup, "Cleanup memory guarded instances")
    .def("memory_guard_is_empty", &APIAsyncService::self_memory_guard_is_empty, "More memory guarded instances to clean up");

    def("APIAsyncService_create", &APIAsyncService::create_1);
    def("APIAsyncService_create", &APIAsyncService::create_2);

    /*
     * APIMutex
     */
    class_<APIMutex::APtr>("APIMutex")
    .def("lock", &APIMutex::self_lock, "Lock mutex, remember to call unlock. Returns false if lock failed")
    .def("lock_with_timeout", &APIMutex::self_lock_with_timeout, "Lock mutex remember to call unlock. Returns false if lock failed")
    .def("unlock", &APIMutex::self_unlock, "Unlock mutex");


    /*
     * APISessionManagerEventhandler
     */
    class_<APISessionManagerEventhandler, PYAPISessionManagerEventhandlerCallback, boost::noncopyable>("APISessionManagerEventhandler", init<const APICheckpointHandler::APtr&>());

    /*
     * APISessionManagerServer
     */
    class_<APISessionManagerServer::APtr>("APISessionManagerServer")
    .def("start", &APISessionManagerServer::self_start, "Start session manager main loop")
    .def("accept_connections_start", &APISessionManagerServer::self_accept_connections_start, "Start accepting connections")
    .def("accept_connections_stop", &APISessionManagerServer::self_accept_connections_stop, "Stop accepting connections (cancel pending accepts)")
    .def("close_start", &APISessionManagerServer::self_close_start, "Close session manager")
    .def("is_closed", &APISessionManagerServer::self_is_closed, "Return true if the session manager has been closed")
    .def("set_config_idle_timeout_sec", &APISessionManagerServer::self_set_config_idle_timeout_sec, "Set durration for session idle timeout")
    .def("set_config_log_interval_sec", &APISessionManagerServer::self_set_config_log_interval_sec, "Set durration for log interval")
    .def("set_config_log_interval_min", &APISessionManagerServer::self_set_config_log_interval_min, "Set durration for log interval")
    .def("set_config_session_logging_enabled", &APISessionManagerServer::self_set_config_session_logging_enabled, "Set session logging enablement")
    .def("get_mutex", &APISessionManagerServer::self_get_mutex, "Get session manager mutex")
    .def("set_option_reuse_address", &APISessionManagerServer::self_set_option_reuse_address, "Alow reuse of address of acceptor")
    .def("set_config_security_dos_attack", &APISessionManagerServer::self_set_config_security_dos_attack, "Configuration of DoS attac fence")
    .def("set_config_https", &APISessionManagerServer::self_set_config_https, "Configuration of HTTPS(ssl)");
    def("APISessionManagerServer_create", &APISessionManagerServer::create);

    /*
     * APISessionManagerClient
     */
    class_<APISessionManagerClient::APtr>("APISessionManagerClient", "Client Session Manager")
    .def("set_eventhandler", &APISessionManagerClient::self_set_eventhandler, "Set eventhandler")
    .def("start", &APISessionManagerClient::self_start, "Start session manager main loop")
    .def("close_start", &APISessionManagerClient::self_close_start, "Close session manger")
    .def("is_closed", &APISessionManagerClient::self_is_closed, "Return true if the session manager has been closed")
    .def("set_servers", &APISessionManagerClient::self_set_servers, "Set servers specification")
    .def("add_server_connection", &APISessionManagerClient::self_add_server_connection, "Add a single server")
    .def("get_mutex", &APISessionManagerClient::self_get_mutex, "Get session manager mutex")
    .def("set_config_session_logging_enabled", &APISessionManagerClient::self_set_config_session_logging_enabled, "Set session logging enablement") ;
    def("APISessionManagerClient_create", &APISessionManagerClient::create);

    /*
     * APISession
     */
    class_<APISessionEventhandlerReady, PYAPISessionEventhandlerReadyCallback, boost::noncopyable>("APISessionEventhandlerReady", init<const APICheckpointHandler::APtr&>());
    class_<APISession::APtr>("APISession", "Session")
    .def("close", &APISession::self_close, "Close session")
    .def("is_closed", &APISession::self_is_closed, "Is the session closed ?")
    .def("get_session_id", &APISession::self_get_session_id, "Get session id")
    .def("get_unique_session_id", &APISession::self_get_unique_session_id, "Get unique session id")
    .def("get_remote_unique_session_id", &APISession::self_get_remote_unique_session_id, "Get remote unique session id")
    .def("get_remote_session_logging_enabled", &APISession::self_get_remote_session_logging_enabled, "Get remote session logging")
    .def("get_ip_remote", &APISession::self_get_ip_remote, "Get remote part of socket")
    .def("get_ip_local", &APISession::self_get_ip_local, "Get local part of socket")
    .def("add_tunnelendpoint", &APISession::self_add_tunnelendpoint_reliable_crypted, "Add tunnelendpoint to the session")
    .def("add_tunnelendpoint_tunnel", &APISession::self_add_tunnelendpoint_reliable_crypted_tunnel, "Add tunnelendpoint to the session")
    .def("add_tunnelendpoint_tunnel", &session_helper_self_add_tunnelendpoint_tcp_tunnel_client, "Add tunnelendpoint to the session")
    .def("add_tunnelendpoint_tunnel", &session_helper_self_add_tunnelendpoint_tcp_tunnel_server, "Add tunnelendpoint to the session")
    .def("set_eventhandler_ready", &APISession::self_set_eventhandler_ready, "Set eventhandler")
    .def("read_start_state_ready", &APISession::self_read_start_state_ready, "Start reading when in ready state")
    .def("get_mutex", &APISession::self_get_mutex, "Get session mutex")
    .def("set_keep_alive_ping_interval_sec", &APISession::self_set_keep_alive_ping_interval_sec, "Set alive ping interval in seconds");

    /*
     * APITunnelendpointEventhandler
     */
    class_<APITunnelendpointEventhandler, PYAPITunnelendpointEventhandlerCallback, boost::noncopyable>("APITunnelendpointEventhandler", init<const APICheckpointHandler::APtr&>());

    /*
     * APITunnelendpointReliableCrypted
     */
    class_<APITunnelendpointReliableCrypted::APtr>("APITunnelendpointReliableCrypted", "Reliable crypted tunnelendpoint")
    .def("set_eventhandler", &APITunnelendpointReliableCrypted::self_set_eventhandler, "Set eventhandler")
    .def("reset_eventhandler", &APITunnelendpointReliableCrypted::self_reset_eventhandler, "Reet eventhandler")
    .def("tunnelendpoint_send", &APITunnelendpointReliableCrypted::self_tunnelendpoint_send, "Send a message")
    .def("is_connected", &APITunnelendpointReliableCrypted::self_is_connected, "Returns true if connected to remote tunnelendpoint")
    .def("set_version", &APITunnelendpointReliableCrypted::self_set_version, "Set the version of this tunnelendpoint")
    .def("get_version", &APITunnelendpointReliableCrypted::self_get_version, "Get the version of this tunnelendpoint")
    .def("get_version_remote", &APITunnelendpointReliableCrypted::self_get_version_remote, "Get the version of the remote tunnelendpoint")
    .def("self_get_id_abs", &APITunnelendpointReliableCrypted::self_get_id_abs, "Get absolute address of this tunnelendpoint")
    .def("get_mutex", &APITunnelendpointReliableCrypted::self_get_mutex, "Get used instance mutex")
    .def("get_appl_protocol_type", &APITunnelendpointReliableCrypted::self_get_appl_protocol_type, "Get type of appl tunnel protocol")
    .def("tunnelendpoint_user_signal", &APITunnelendpointReliableCrypted::self_tunnelendpoint_user_signal, "Send user signal to parent")
    .def("close", &APITunnelendpointReliableCrypted::self_close, "Close")
    .def("set_skip_connection_handshake", &APITunnelendpointReliableCrypted::self_set_skip_connection_handshake, "Set skip connection handshake")
    .def("get_tc_read_delay_ms", &APITunnelendpointReliableCrypted::self_get_tc_read_delay_ms, "Get tc read delay ms")
    .def("get_tc_read_size", &APITunnelendpointReliableCrypted::self_get_tc_read_size, "Get tc read size")
    .def("tc_report_read_begin", &APITunnelendpointReliableCrypted::self_tc_report_read_begin, "Report begin read to tc")
    .def("tc_report_read_end", &APITunnelendpointReliableCrypted::self_tc_report_read_end, "Report end read to tc")
    .def("tc_report_push_buffer_size", &APITunnelendpointReliableCrypted::self_tc_report_push_buffer_size, "Report begin size has been adde to buffer")
    .def("tc_report_pop_buffer_size", &APITunnelendpointReliableCrypted::self_tc_report_pop_buffer_size, "Report begin size has been removed from buffer");
    def("APITunnelendpointReliableCrypted_create", &APITunnelendpointReliableCrypted::create);


    /*
     * APITunnelendpointReliableCryptedTunnel
     */
    class_<APITunnelendpointReliableCryptedTunnel::APtr>("APITunnelendpointReliableCryptedTunnel", "Reliable crypted tunnelendpoint tunnel")
    .def("set_eventhandler", &APITunnelendpointReliableCryptedTunnel::self_set_eventhandler, "Set eventhandler")
    .def("reset_eventhandler", &APITunnelendpointReliableCryptedTunnel::self_reset_eventhandler, "Reset eventhandler")
    .def("tunnelendpoint_send", &APITunnelendpointReliableCryptedTunnel::self_tunnelendpoint_send, "Send a message")
    .def("tunnelendpoint_user_signal", &APITunnelendpointReliableCryptedTunnel::self_tunnelendpoint_user_signal, "Send user signal to parent")
    .def("is_connected", &APITunnelendpointReliableCryptedTunnel::self_is_connected, "Returns true if connected to remote tunnelendpoint")
    .def("set_version", &APITunnelendpointReliableCryptedTunnel::self_set_version, "Set the version of this tunnelendpoint")
    .def("get_version", &APITunnelendpointReliableCryptedTunnel::self_get_version, "Get the version of this tunnelendpoint")
    .def("get_version_remote", &APITunnelendpointReliableCryptedTunnel::self_get_version_remote, "Get the version of the remote tunnelendpoint")
    .def("get_id_abs", &APITunnelendpointReliableCryptedTunnel::self_get_id_abs, "Get absolute address of this tunnelendpoint")
    .def("get_mutex", &APITunnelendpointReliableCryptedTunnel::self_get_mutex, "Get used instance mutex")
    .def("get_appl_protocol_type", &APITunnelendpointReliableCryptedTunnel::self_get_appl_protocol_type, "Get type of appl tunnel protocol")
    .def("add_tunnelendpoint", &APITunnelendpointReliableCryptedTunnel::self_add_tunnelendpoint_reliable_crypted, "Add tunnelendpoint to the tunnelendpoint")
    .def("set_skip_connection_handshake", &APITunnelendpointReliableCryptedTunnel::self_set_skip_connection_handshake, "Set skip connection handshake")
    .def("add_tunnelendpoint_tunnel",&APITunnelendpointReliableCryptedTunnel::self_add_tunnelendpoint_reliable_crypted_tunnel, "Add tunnelendpoint to the tunnelendpoint")
    .def("add_tunnelendpoint_tunnel", &tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_server, "Add tunnelendpoint to the tunnelendpoint")
    .def("add_tunnelendpoint_tunnel", &tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_server_with_connection_engine, "Add tunnelendpoint to the tunnelendpoint")
    .def("add_tunnelendpoint_tunnel", &tunnel_helper_self_add_tunnelendpoint_tcp_tunnel_client, "Add tunnelendpoint to the tunnelendpoint")
    .def("close", &APITunnelendpointReliableCryptedTunnel::self_close, "Close")
    .def("remove_tunnelendpoint", &APITunnelendpointReliableCryptedTunnel::self_remove_tunnelendpoint, "Remove child tunnelendpoint")
    .def("get_tc_read_delay_ms", &APITunnelendpointReliableCryptedTunnel::self_get_tc_read_delay_ms, "Get tc read delay ms")
    .def("get_tc_read_size", &APITunnelendpointReliableCryptedTunnel::self_get_tc_read_size, "Get tc read size")
    .def("tc_report_read_begin", &APITunnelendpointReliableCryptedTunnel::self_tc_report_read_begin, "Report begin read to tc")
    .def("tc_report_read_end", &APITunnelendpointReliableCryptedTunnel::self_tc_report_read_end, "Report end read to tc")
    .def("tc_report_push_buffer_size", &APITunnelendpointReliableCryptedTunnel::self_tc_report_push_buffer_size, "Report begin size has been adde to buffer")
    .def("tc_report_pop_buffer_size", &APITunnelendpointReliableCryptedTunnel::self_tc_report_pop_buffer_size, "Report begin size has been removed from buffer");
    def("APITunnelendpointReliableCryptedTunnel_create", &APITunnelendpointReliableCryptedTunnel::create);


    /*
     * APITunnelendpointTCP
     */
    class_<APITunnelendpointTCPEventhandler, PYAPITunnelendpointTCPEventhandlerCallback, boost::noncopyable>("APITunnelendpointTCPEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APITunnelendpointTCP::APtr>("APITunnelendpointTCP", "Tunnelendpoint tcp-connection")
      .def("set_tcp_eventhandler", &APITunnelendpointTCP::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APITunnelendpointTCP::self_reset_tcp_eventhandler, "Reset eventhandler")
      .def("close", &APITunnelendpointTCP::self_close, "Close")
      .def("read_start", &APITunnelendpointTCP::self_read_start, "Start async read")
      .def("get_ip_remote", &APITunnelendpointTCP::self_get_ip_remote, "Get remote part of socket")
      .def("get_ip_local", &APITunnelendpointTCP::self_get_ip_local, "Get local part of socket");


    /*
     * APITunnelendpointTCPTunnelClient
     */
    class_<APITunnelendpointTCPTunnelClientEventhandler, PYAPITunnelendpointTCPTunnelClientEventhandlerCallback, boost::noncopyable>("APITunnelendpointTCPTunnelClientEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APITunnelendpointTCPTunnelClient::APtr>("APITunnelendpointTCPTunnelClient", "Tunnelendpoint tcp-tunnel client")
      .def("set_option_reuse_address", &APITunnelendpointTCPTunnelClient::self_set_option_reuse_address, "Set TCP option reuse address")
      .def("set_tcp_eventhandler", &APITunnelendpointTCPTunnelClient::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APITunnelendpointTCPTunnelClient::self_reset_tcp_eventhandler, "Reset eventhandler")
      .def("close", &APITunnelendpointTCPTunnelClient::self_close, "Close")
      .def("is_closed", &APITunnelendpointTCPTunnelClient::self_is_closed, "Return true if the tunnel is closed")
      .def("get_ip_local", &APITunnelendpointTCPTunnelClient::self_get_ip_local, "Returns listen host/port")
      .def("accept_start", &APITunnelendpointTCPTunnelClient::self_accept_start, "Start accepting new connections")
      .def("tunnelendpoint_send", &APITunnelendpointTCPTunnelClient::self_tunnelendpoint_send, "Send a message");

    def("APITunnelendpointTCPTunnelClient_create", &APITunnelendpointTCPTunnelClient::create);


    /*
     * APITunnelendpointTCPTunnelServer
     */
    class_<APITunnelendpointTCPTunnelServerEventhandler, PYAPITunnelendpointTCPTunnelServerEventhandlerCallback, boost::noncopyable> ("APITunnelendpointTCPTunnelServerEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APITunnelendpointTCPTunnelServer::APtr>("APITunnelendpointTCPTunnelServer", "Tunnelendpoint tcp-tunnel server")
      .def("set_tcp_eventhandler", &APITunnelendpointTCPTunnelServer::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APITunnelendpointTCPTunnelServer::self_reset_tcp_eventhandler, "Reset eventhandler")
      .def("close", &APITunnelendpointTCPTunnelServer::self_close, "Close")
      .def("is_closed", &APITunnelendpointTCPTunnelServer::self_is_closed, "Return true if the tunnel is closed")
      .def("tunnelendpoint_send", &APITunnelendpointTCPTunnelServer::self_tunnelendpoint_send, "Send a message");

    def("APITunnelendpointTCPTunnelServer_create", &APITunnelendpointTCPTunnelServer::create);

    /*
     * APITunnelendpointTCPTunnelServerWithConnectionEngine
     */
    class_<APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler, PYAPITunnelendpointTCPTunnelServerWithConnectionEngineEventhandlerCallback, boost::noncopyable> ("APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APITunnelendpointTCPTunnelServerWithConnectionEngine::APtr>("APITunnelendpointTCPTunnelServerWithConnectionEngine", "Tunnelendpoint tcp-tunnel server with connection engine")
      .def("set_tcp_eventhandler", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_reset_tcp_eventhandler, "Reset eventhandler")
      .def("close", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_close, "Close")
      .def("enable_checkpoint_tunnel", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_enable_checkpoint_tunnel, "Enable checkpoint tunnel")
      .def("is_closed", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_is_closed, "Return true if the tunnel is closed")
      .def("add_tunnelendpoint", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_add_tunnelendpoint_reliable_crypted, "Add tunnelendpoint to the tunnelendpoint")
      .def("add_tunnelendpoint_tunnel",&APITunnelendpointTCPTunnelServerWithConnectionEngine::self_add_tunnelendpoint_reliable_crypted_tunnel, "Add tunnelendpoint to the tunnelendpoint")
      .def("remove_tunnelendpoint", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_remove_tunnelendpoint, "Remove child tunnelendpoint")
      .def("tunnelendpoint_send", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send, "Send a message")
      .def("tunnelendpoint_send_remote_eof", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_eof, "Send eof to a remote child")
      .def("tunnelendpoint_send_remote_close", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_close, "Send close to a remote child")
      .def("tunnelendpoint_send_remote_closed", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_closed, "Send closed to a remote child")
      .def("tunnelendpoint_send_remote_recieve_connect_ok", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_recieve_connect_ok, "remote_recieve_connect ok response")
      .def("tunnelendpoint_send_remote_recieve_connect_error", &APITunnelendpointTCPTunnelServerWithConnectionEngine::self_tunnelendpoint_send_remote_recieve_connect_error, "remote_recieve_connect error response");

    def("APITunnelendpointTCPTunnelServerWithConnectionEngine_create", &APITunnelendpointTCPTunnelServerWithConnectionEngine::create);


    /*
     * APIRawTunnelendpointTCP
     */
    class_<APIRawTunnelendpointEventhandler, PYAPIRawTunnelendpointEventhandlerCallback, boost::noncopyable> ("APIRawTunnelendpointEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APIRawTunnelendpointTCP::APtr>("APIRawTunnelendpointTCP", "Raw tcp-tunnelendpoint")
      .def("set_tcp_eventhandler", &APIRawTunnelendpointTCP::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APIRawTunnelendpointTCP::self_reset_tcp_eventhandler, "Set eventhandler")
      .def("aio_read_start", &APIRawTunnelendpointTCP::self_aio_read_start, "Start async read")
      .def("aio_write_start", &APIRawTunnelendpointTCP::self_aio_write_start, "Start async write")
      .def("aio_close_start", &APIRawTunnelendpointTCP::self_aio_close_start, "Start async close")
      .def("aio_close_write_start", &APIRawTunnelendpointTCP::self_aio_close_write_start, "Start async write-close (eof)")
      .def("is_closed", &APIRawTunnelendpointTCP::self_is_closed, "")
      .def("is_ready", &APIRawTunnelendpointTCP::self_is_ready, "")
      .def("get_mutex", &APIRawTunnelendpointTCP::self_get_mutex, "Get instance mutex")
      .def("set_mutex", &APIRawTunnelendpointTCP::self_set_mutex, "Set instance mutex")
      .def("get_ip_remote", &APIRawTunnelendpointTCP::self_get_ip_remote, "Get remote part of socket")
      .def("get_ip_local", &APIRawTunnelendpointTCP::self_get_ip_local, "Get local part of socket")
      .def("aio_ssl_start", &APIRawTunnelendpointTCP::self_aio_ssl_start, "Establish ssl on existing socket");

    /*
     * APIRawTunnelendpointTCPConnector
     */
    class_<APIRawTunnelendpointConnectorTCPEventhandler, PYAPIRawTunnelendpointConnectorEventhandlerCallback, boost::noncopyable> ("APIRawTunnelendpointConnectorTCPEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APIRawTunnelendpointConnectorTCP::APtr>("APIRawTunnelendpointConnectorTCP", "Raw tcp-tunnelendpoint connector")
      .def("set_tcp_eventhandler", &APIRawTunnelendpointConnectorTCP::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APIRawTunnelendpointConnectorTCP::self_reset_tcp_eventhandler, "Set eventhandler")
      .def("get_mutex", &APIRawTunnelendpointConnectorTCP::self_get_mutex, "Get instance mutex")
      .def("set_mutex", &APIRawTunnelendpointConnectorTCP::self_set_mutex, "Set instance mutex")
      .def("aio_connect_start", &APIRawTunnelendpointConnectorTCP::self_aio_connect_start, "Start async connect")
      .def("set_connect_timeout_sec", &APIRawTunnelendpointConnectorTCP::self_set_connect_timeout_sec, "Set connect timeout")
      .def("enable_ssl", &APIRawTunnelendpointConnectorTCP::self_enable_ssl, "Enable SSL for connections")
      .def("close", &APIRawTunnelendpointConnectorTCP::self_close, "Close connector");

    def("APIRawTunnelendpointConnectorTCP_create", &APIRawTunnelendpointConnectorTCP::create);


    /*
     * APIRawTunnelendpointTCPAcceptor
     */
    class_<APIRawTunnelendpointAcceptorTCPEventhandler, PYAPIRawTunnelendpointAcceptorEventhandlerCallback, boost::noncopyable> ("APIRawTunnelendpointAcceptorTCPEventhandler", init<const APICheckpointHandler::APtr&>());
    class_<APIRawTunnelendpointAcceptorTCP::APtr>("APIRawTunnelendpointAcceptorTCP", "Raw tcp-tunnelendpoint acceptor")
      .def("set_option_reuse_address", &APIRawTunnelendpointAcceptorTCP::self_set_option_reuse_address, "Set TCP option reuse address")
      .def("set_tcp_eventhandler", &APIRawTunnelendpointAcceptorTCP::self_set_tcp_eventhandler, "Set eventhandler")
      .def("reset_tcp_eventhandler", &APIRawTunnelendpointAcceptorTCP::self_reset_tcp_eventhandler, "Set eventhandler")
      .def("aio_accept_start", &APIRawTunnelendpointAcceptorTCP::self_aio_accept_start, "Start async accept")
      .def("aio_close_start", &APIRawTunnelendpointAcceptorTCP::self_aio_close_start, "Start async close of accept")
      .def("get_mutex", &APIRawTunnelendpointAcceptorTCP::self_get_mutex, "Get instance mutex")
      .def("set_mutex", &APIRawTunnelendpointAcceptorTCP::self_set_mutex, "Set instance mutex")
      .def("get_ip_local", &APIRawTunnelendpointAcceptorTCP::self_get_ip_local, "Get address and port that the acceptor currently is listen on")
      .def("is_closed", &APIRawTunnelendpointAcceptorTCP::self_is_closed, "");

    def("APIRawTunnelendpointAcceptorTCP_create", &APIRawTunnelendpointAcceptorTCP::create);

}

/*
 * Define cryptfacility python api
 */
void def_api_lib_cryptfacility(void) {
  def("CryptFacility_pk_generate_keys", &Giritech::CryptFacility::APICryptFacility_pk_generate_keys);
  def("CryptFacility_pk_generate_keys_rsa", &Giritech::CryptFacility::APICryptFacility_pk_generate_keys_rsa);
  def("CryptFacility_pk_sign_challenge", &Giritech::CryptFacility::APICryptFacility_pk_sign_challenge);
  def("CryptFacility_pk_sign_challenge_rsa", &Giritech::CryptFacility::APICryptFacility_pk_sign_challenge_rsa);
  def("CryptFacility_pk_verify_challenge", &Giritech::CryptFacility::APICryptFacility_pk_verify_challenge);
  def("CryptFacility_pk_verify_challenge_rsa", &Giritech::CryptFacility::APICryptFacility_pk_verify_challenge_rsa);
  def("CryptFacility_generate_secrets", &Giritech::CryptFacility::APICryptFacility_generate_secrets);
  def("CryptFacility_verify_public_key_signature", &Giritech::CryptFacility::APICryptFacility_verify_public_key_signature);
}

/*
 * Define hagi server python api
 */
void def_api_lib_hagi_authentication(void) {
  def("HagiServer_generate_challenge", &Giritech::Hagi::APIServer::generate_challenge);
  def("HagiServer_server_athenticate", &Giritech::Hagi::APIServer::athenticate);
}



/*
 * Define RPC python api
 */
void def_api_lib_rpc(void) {
  def("rpc_xml_encode", Giritech::RPC::rpc_xml_encode);
  def("rpc_xml_decode", Giritech::RPC::rpc_xml_decode);
}


/*
 * Define FD Read/Write python api
 */
#if defined(GIRITECH_COMPILEOPTION_TARGET_LINUX) || defined(GIRITECH_COMPILEOPTION_TARGET_MAC)
void def_api_lib_fd_read_write(void) {
  def("fd_read", fd_read);
  def("fd_write", fd_write);
}
#endif

BOOST_PYTHON_MODULE(communication_ext) {
    docstring_options doc_options(true);
    def_api_lib_utility();
    def_api_component_communication();
    def_api_lib_cryptfacility();
    def_api_lib_hagi_authentication();
    def_api_lib_rpc();
#if defined(GIRITECH_COMPILEOPTION_TARGET_LINUX) || defined(GIRITECH_COMPILEOPTION_TARGET_MAC)
    def_api_lib_fd_read_write();
#endif
}
