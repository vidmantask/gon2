#include <string>
#include <iostream>
#include <boost/python.hpp>

using namespace std;
using namespace boost::python;


struct my_exc : std::exception {
    char const* what() const throw() {
        return "hello";
    }
};

void demo_throw_and_catch(void) {
	cout << "demo_throw_and_catch.begin" << endl;
	try {
        throw my_exc();
    }
    catch(const my_exc& e) {
    	cout << "demo_throw_and_catch.caught_exception_my_exc" << endl;
    }
    catch(const std::exception& e) {
    	cout << "demo_throw_and_catch.caught_exception" << endl;
    }
    catch(...) {
    	cout << "demo_throw_and_catch.caught_exception_all" << endl;
    }
	cout << "demo_throw_and_catch.end" << endl;
}

BOOST_PYTHON_MODULE(demo_ext) {
    docstring_options doc_options(true);
    def("demo_throw_and_catch", &demo_throw_and_catch);
}
