/*! \file PYAPI_Utility.cxx
 *  \brief Implementation of common utility for pyapi
 */
#include <component/communication/pyapi/PYAPI_Utility.hxx>
#include <lib/utility/UY_Checkpoint.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::Communication::PYAPI;

/*
 * ------------------------------------------------------------------
 * ThreadLock implementation
 * ------------------------------------------------------------------
 */
ThreadLock::ThreadLock(const std::string& id, const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler) :
    id_(id), api_checkpoint_handler_(api_checkpoint_handler) {
//    Checkpoint cp1(*(api_checkpoint_handler_->get_checkpoint_handler()),"ThreadLock::lock_waiting", Attr_Communication(), CpAttr_debug(), CpAttr("id", id_));
    start_ts_ = boost::posix_time::microsec_clock::local_time();
    state_ = PyGILState_Ensure();
    boost::posix_time::time_duration duration = (boost::posix_time::microsec_clock::local_time() - start_ts_);
    if(duration >  boost::posix_time::milliseconds(300)) {
        Checkpoint cp(*(api_checkpoint_handler_->get_checkpoint_handler()),"exceed_timelimit_getting_gil", Attr_Communication(), CpAttr_warning(), CpAttr("id", id_), CpAttr("duration_ms", duration.total_milliseconds()));
    }
  //    Checkpoint cp2(*(api_checkpoint_handler_->get_checkpoint_handler()),"ThreadLock::lock_got_it", Attr_Communication(), CpAttr_debug(), CpAttr("id", id_));
}

ThreadLock::~ThreadLock() {
    boost::posix_time::time_duration duration = (boost::posix_time::microsec_clock::local_time() - start_ts_);
    if(duration >  boost::posix_time::milliseconds(300)) {
        Checkpoint cp(*(api_checkpoint_handler_->get_checkpoint_handler()),"exceed_timelimit", Attr_Communication(), CpAttr_warning(), CpAttr("id", id_), CpAttr("duration_ms", duration.total_milliseconds()));
    }
    PyGILState_Release(state_);
//    Checkpoint cp(*(api_checkpoint_handler_->get_checkpoint_handler()),"ThreadLock::lock_released", Attr_Communication(), CpAttr_debug(), CpAttr("id", id_));
}

/*
 * ------------------------------------------------------------------
 * ThreadLockNoLog implementation
 * ------------------------------------------------------------------
 */
ThreadLockNoLog::ThreadLockNoLog(void) {
    state_ = PyGILState_Ensure();
}

ThreadLockNoLog::~ThreadLockNoLog() {
    PyGILState_Release(state_);
}
