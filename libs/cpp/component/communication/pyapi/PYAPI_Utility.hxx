/*! \file PYAPI_Utility.hxx
 *  \brief Common utility for pyapi
 */
#ifndef PYAPI_Utility_HXX
#define PYAPI_Utility_HXX

#include <boost/python.hpp>
#include <boost/date_time.hpp>

#include <lib/utility/UY_API_Checkpoint.hxx>


namespace Giritech {
namespace Communication {
namespace PYAPI {


class ThreadLock {
public:
	ThreadLock(const std::string& id, const Giritech::Utility::APICheckpointHandler::APtr& api_checkpoint_handler);
    ~ThreadLock();
private:
    PyGILState_STATE state_;
    std::string id_;
    Giritech::Utility::APICheckpointHandler::APtr api_checkpoint_handler_;
    boost::posix_time::ptime start_ts_;
};

class ThreadLockNoLog {
public:
    ThreadLockNoLog(void);
    ~ThreadLockNoLog();
private:
    PyGILState_STATE state_;
};

}
}
}
#endif
