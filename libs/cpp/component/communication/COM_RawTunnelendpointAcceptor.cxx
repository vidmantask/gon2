/*! \file COM_RawTunnelendpointAcceptor.hxx
 * \brief This file contains the implementation of the RawTunnelendpointAcceptorTCP abstraction
 */
#include <component/communication/COM_RawTunnelendpointAcceptor.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <boost/asio/error.hpp>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>

using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * RawTunnelendpointAcceptorTCP implementation
 * ------------------------------------------------------------------
 */
RawTunnelendpointAcceptorTCP::RawTunnelendpointAcceptorTCP(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                                           boost::asio::io_service& io,
                                                           const std::string& listen_host,
                                                           const long listen_port,
                                                           RawTunnelendpointAcceptorTCPEventhandler* eventhandler,
                                                           AsyncContinuePolicy* async_continue_policy) :
    state_(State_Initializing), checkpoint_handler_(checkpoint_handle), io_(io), eventhandler_(eventhandler),
            async_continue_policy_(async_continue_policy), acceptor_(io), option_reuse_address_(false), option_listen_(10),
            mutex_(Mutex::create(io, "RawTunnelendpointAcceptorTCP")), pending_accept_exists_(false), accept_ignore_(false), pending_accept_errors_exists_(false) {

    if (listen_host.empty() && listen_port == 0) {
        listen_endpoint_ = boost::asio::ip::tcp::endpoint();
    } else if (listen_host.empty()) {
        listen_endpoint_ = boost::asio::ip::tcp::endpoint(boost::asio::ip::address_v4::loopback(), listen_port);
    } else {
        listen_endpoint_ = boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(listen_host), listen_port);
    }
}

RawTunnelendpointAcceptorTCP::~RawTunnelendpointAcceptorTCP(void) {
    state_ = State_Unknown;
}

void RawTunnelendpointAcceptorTCP::reset_eventhandler(void) {
	eventhandler_ = NULL;
}

Utility::Mutex::APtr RawTunnelendpointAcceptorTCP::get_mutex(void) {
    return mutex_;
}

void RawTunnelendpointAcceptorTCP::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
}

void RawTunnelendpointAcceptorTCP::set_option_reuse_address(const bool& option_reuse_address) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::set_option_reuse_address");
    CheckpointScope scp_init(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::set_option_reuse_address", Attr_Communication(), Utility::CpAttr_debug(), Utility::CpAttr("option_reuse_address", option_reuse_address));
	option_reuse_address_ = option_reuse_address;
}

void RawTunnelendpointAcceptorTCP::set_option_listen(const int option_listen) {
	option_listen_ = option_listen;
}

RawTunnelendpointAcceptorTCP::APtr RawTunnelendpointAcceptorTCP::create(const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handle,
                                                                        boost::asio::io_service& io,
                                                                        const std::string& listen_host,
                                                                        const long listen_port,
                                                                        RawTunnelendpointAcceptorTCPEventhandler* eventhandler,
                                                                        AsyncContinuePolicy* async_continue_policy) {
	APtr result(new RawTunnelendpointAcceptorTCP(checkpoint_handle, io, listen_host, listen_port, eventhandler, async_continue_policy));
	AsyncMemGuard::global_add_element(result);
	return result;
}

unsigned long RawTunnelendpointAcceptorTCP::get_connection_id_next(void) {
    static unsigned long connection_id = 0;
    static boost::recursive_timed_mutex global_mutex;
    boost::recursive_timed_mutex::scoped_lock global_mutex_lock(global_mutex);
    return ++connection_id;
}

void RawTunnelendpointAcceptorTCP::aio_accept_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_accept_start");
    switch (state_) {
    case State_Initializing:
    case State_Ready:
        break;
    default:
        return;
    }

    if (state_ == State_Initializing) {
        CheckpointScope scp_init(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept_start.init", Attr_Communication(), Utility::CpAttr_debug(), Utility::CpAttr("option_reuse_address", option_reuse_address_));
        boost::system::error_code ec;
        acceptor_.open(listen_endpoint_.protocol(), ec);
        if (!ec) {
            acceptor_.set_option(boost::asio::socket_base::reuse_address(option_reuse_address_));
            acceptor_.bind(listen_endpoint_, ec);
        }
        if (!ec) {
            acceptor_.listen(option_listen_, ec);
        }
        if (ec) {
            if (eventhandler_ != NULL) {
        		pending_accept_errors_exists_ = true;
                mutex_->get_strand().post(boost::bind(&RawTunnelendpointAcceptorTCP::call_com_tunnelendpoint_acceptor_error_decoubled, this, ec.message()));
            }
            else {
				Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointAcceptorTCP::aio_accept_start.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
            }
        	aio_close_start();
        }
        else {
            state_ = State_Ready;
        }
    }

    if (state_ == State_Ready) {
		CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept_start",
							Attr_Communication(), Utility::CpAttr_debug(),
							Utility::CpAttr("ip", acceptor_.local_endpoint().address().to_string()),
							Utility::CpAttr("port", acceptor_.local_endpoint().port()),
							Utility::CpAttr("pending_accept_exists", pending_accept_exists_));
    	if (!pending_accept_exists_) {

			unsigned long connection_id = get_connection_id_next();
			RawTunnelendpointTCP::APtr raw_tunnelendpoint(RawTunnelendpointTCP::create(checkpoint_handler_, io_, connection_id));
			acceptor_.async_accept(raw_tunnelendpoint->get_socket(), boost::bind(&RawTunnelendpointAcceptorTCP::aio_accept,
																				 this, raw_tunnelendpoint,
																				 boost::asio::placeholders::error));
			pending_accept_exists_ = true;
    	}
    }
}

void RawTunnelendpointAcceptorTCP::call_com_tunnelendpoint_acceptor_error_decoubled(const std::string& message) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::call_com_tunnelendpoint_acceptor_error_decoubled");
	try {
		if(eventhandler_ == NULL) {
			eventhandler_->com_tunnelendpoint_acceptor_error(message);
		}
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"APITunnelendpointReliableCrypted::call_com_tunnelendpoint_acceptor_error_decoubled.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
	pending_accept_errors_exists_ = false;
}


void RawTunnelendpointAcceptorTCP::aio_accept_stop(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_accept_stop");
    switch (state_) {
    case State_Ready:
        break;
    default:
        return;
    }
    if (pending_accept_exists_) {
		boost::system::error_code error;
		acceptor_.cancel(error);
		if (error) {
			Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept_stop.error", Attr_Communication(), Utility::CpAttr_warning(), Utility::CpAttr_message(error.message()));
		}
    }
}

void RawTunnelendpointAcceptorTCP::aio_accept_ignore_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_accept_ignore_start");
    switch (state_) {
    case State_Ready:
        break;
    default:
        return;
    }
    accept_ignore_ = true;
}

void RawTunnelendpointAcceptorTCP::aio_accept_ignore_stop(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_accept_ignore_start");
    switch (state_) {
    case State_Ready:
        break;
    default:
        return;
    }
    accept_ignore_ = false;
}


void RawTunnelendpointAcceptorTCP::aio_accept(RawTunnelendpointTCP::APtr raw_tunnelendpoint, const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_accept");
    CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept", Attr_Communication(), Utility::CpAttr_debug());

    pending_accept_exists_ = false;
    try {
		if (!error) {
			if(state_ == State_Closing) {
				raw_tunnelendpoint->aio_close_start(true);
				aio_close_final();
				return;
			}

            raw_tunnelendpoint->set_option_nodelay();

            /* Signal eventhandler */
			if(!accept_ignore_) {
				if (eventhandler_ != NULL) {
					eventhandler_->com_tunnelendpoint_accepted(raw_tunnelendpoint);
				}
				else {
					Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointAcceptorTCP::aio_accept.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
					aio_close_start();
					return;
				}
			}
			else {
				Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointAcceptorTCP::aio_accept.ignored", Attr_Communication(), Utility::CpAttr_debug());
				raw_tunnelendpoint->aio_close_start(true);
			}

			/* Ask continue policy if listen should proceed */
            if (async_continue_policy_ != NULL) {
                if (async_continue_policy_->com_accept_continue()) {
                    aio_accept_start();
                }
            }
			return;
		}
		if (error == boost::asio::error::operation_aborted) {
			Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept.operation_aborted", Attr_Communication(), Utility::CpAttr_debug(), Utility::CpAttr_message(error.message()));
			if(state_ == State_Closing) {
				raw_tunnelendpoint->aio_close_start(true);
				aio_close_final();
			}
			return;
		}
		Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_accept.error", Attr_Communication(), Utility::CpAttr_warning(), Utility::CpAttr_message(error.message()));
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointAcceptorTCP::aio_accept.unexpected", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
    }
}

void RawTunnelendpointAcceptorTCP::aio_close_start(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_close_start");
    CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_close_start", Attr_Communication(), Utility::CpAttr_debug());
    switch (state_) {
    case State_Initializing:
    case State_Ready:
        break;
    default:
        return;
    }

    state_ = State_Closing;
    boost::system::error_code error;
    acceptor_.close(error);
    if (error) {
        Checkpoint cp(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_close_start.error", Attr_Communication(), Utility::CpAttr_warning(), Utility::CpAttr_message(error.message()));
		aio_close_final();
		return;
    }
    if (! pending_accept_exists_) {
    	mutex_->get_strand().post(boost::bind(&RawTunnelendpointAcceptorTCP::aio_close_final, this));
    }
}

void RawTunnelendpointAcceptorTCP::aio_close_final(void) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::aio_close_start");
	try {
		CheckpointScope cps(*checkpoint_handler_, "RawTunnelendpointAcceptorTCP::aio_close_final", Attr_Communication(), Utility::CpAttr_debug());
		switch (state_) {
		case State_Closing:
			break;
		default:
			return;
		}
		state_ = State_Closed;
		if (eventhandler_ != NULL) {
			eventhandler_->com_tunnelendpoint_acceptor_closed();
			eventhandler_ = NULL;
		}
		else {
			Checkpoint cp(*checkpoint_handler_,	"RawTunnelendpointAcceptorTCP::aio_close_final.missing_eventhandler", Attr_Communication(), Utility::CpAttr_debug());
		}
		state_ = State_Done;
	}
    catch (const std::exception& e) {
		Checkpoint cp(*checkpoint_handler_,"RawTunnelendpointAcceptorTCP::aio_close_final.unexpected.exception", Attr_Communication(), CpAttr_critical(), CpAttr_message(e.what()));
	}
}

bool RawTunnelendpointAcceptorTCP::is_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::is_closed");
    return (state_ == State_Closed) || (state_ == State_Done);
}

std::pair<std::string, unsigned long> RawTunnelendpointAcceptorTCP::get_ip_local(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::get_ip_local");
    string ip;
    unsigned long port(0);
    if (state_ == State_Initializing || state_ == State_Ready) {
        if (acceptor_.is_open()) {
            ip = acceptor_.local_endpoint().address().to_string();
            port = acceptor_.local_endpoint().port();
        }
    }
    return make_pair(ip, port);
}

bool RawTunnelendpointAcceptorTCP::async_mem_guard_is_done(void) {
	MutexScopeLock mutex_lock(checkpoint_handler_, Attr_Communication(), mutex_, "RawTunnelendpointAcceptorTCP::async_mem_guard_is_done");
	return (state_ == State_Done) && (!pending_accept_exists_) && (!pending_accept_errors_exists_);
}

std::string RawTunnelendpointAcceptorTCP::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}
