/*! \file UTest_COM_TunnelendpointTCP.cxx
 \brief This file contains unittest suite for tunnelendpoint-tcp abstraction
 */
#include <vector>
#include <string>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/thread.hpp>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <lib/utility/UY_DataBuffer.hxx>

#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_TunnelendpointTCPMessage.hxx>
#include <component/communication/COM_TunnelendpointTCP.hxx>

#include <component/communication/asimulator/COM_ASimulator.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * General helper calasses for unittesting
 */
class UnittestTunnelendpointConnector: public TunnelendpointConnector {
public:
    UnittestTunnelendpointConnector(void) {
    }
    void tunnelendpoint_connector_send(const MessageCom::APtr& message) {
        if (tunnelendpoint_ != NULL) {
            tunnelendpoint_->tunnelendpoint_recieve(message);
        }
        return;
    }
    void tunnelendpoint_connector_closed(void) {
    }

    void tunnelendpoint_connector_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
        // Ignored
    }

    std::string tunnelendpoint_connector_get_unique_session_id(void) {
    	return "hej";
    }

    Tunnelendpoint* tunnelendpoint_;
};

/*
 * Test of tunnel id reuse functionality
 */
class UnittestDriver {
public:
	UnittestDriver(const AsyncService::APtr& async_service) {
		async_service_ = async_service;
        done = false;
	}


    void run(void) {
        unsigned long num_of_clients = 100;

        CheckpointHandler::APtr checkpoint_handler_client(CheckpointHandler::create_cout_all());

        while (num_of_clients > 0) {
            cout << "Starting new client (" << num_of_clients << ")" << endl;

            ASimulationClient::APtr sim_client_1(ASimulationClient::create(checkpoint_handler_client, async_service_->get_io_service(), "127.0.0.1", 8851,
            		ASimulatorConnection::SimulationType_ping, 10, 50));

            ASimulationClient::APtr sim_client_2( ASimulationClient::create(checkpoint_handler_client, async_service_->get_io_service(), "127.0.0.1", 8851,
            		ASimulatorConnection::SimulationType_ping_server_close, 10, 50));

            ASimulationClient::APtr	sim_client_3(ASimulationClient::create(checkpoint_handler_client, async_service_->get_io_service(), "127.0.0.1", 8851,
            		ASimulatorConnection::SimulationType_upload_until_eof, 10, 50));

            ASimulationClient::APtr sim_client_4(ASimulationClient::create(checkpoint_handler_client, async_service_->get_io_service(), "127.0.0.1", 8851,
            		ASimulatorConnection::SimulationType_download_until_eof, 10, 50));

            clients.push_back(sim_client_1);
            clients.push_back(sim_client_2);
            clients.push_back(sim_client_3);
            clients.push_back(sim_client_4);
            sim_client_1->start();
            sim_client_2->start();
            sim_client_3->start();
            sim_client_4->start();

            cout << "waiting for client (" << num_of_clients << ")" << endl;

            while (!(sim_client_1->is_done()));
            while (!(sim_client_2->is_done()));
            while (!(sim_client_3->is_done()));
            while (!(sim_client_4->is_done()));

            cout << "Clients are now done (" << num_of_clients << ")" << endl;
            --num_of_clients;
        }
        server_tunnelendpoint->close(true);
        client_tunnelendpoint->close(true);

        while (!(client_tunnelendpoint->is_closed()))
            ;
        while (!(server_tunnelendpoint->is_closed()))
            ;

        clients.clear();

        boost::asio::deadline_timer t(async_service_->get_io_service(), boost::posix_time::seconds(2));
        t.wait();

        cout << "UnittestDriver is now over" << endl;
        done = true;
    }
    AsyncService::APtr async_service_;
    vector<ASimulationClient::APtr> clients;
    TunnelendpointTCPTunnelClient::APtr client_tunnelendpoint;
    TunnelendpointTCPTunnelServer::APtr server_tunnelendpoint;

    bool done;
};


BOOST_AUTO_TEST_CASE( tcp_tunnelendpoint_tunnel_id_reuse )
{
    CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());

    boost::thread_group threads;
    AsyncService::APtr async_service(AsyncService::create(checkpoint_handler));


    try {
        CheckpointHandler::APtr simc_checkpoint_handler(CheckpointHandler::create_ignore());
        CheckpointHandler::APtr sims_checkpoint_handler(CheckpointHandler::create_ignore());
        CheckpointHandler::APtr checkpoint_handler_client(CheckpointHandler::create_ignore());
        CheckpointHandler::APtr checkpoint_handler_server(CheckpointHandler::create_cout_all());

        string client_ip = "127.0.0.1";
        unsigned long client_ip_port = 8851;
        UnittestTunnelendpointConnector client_connector;
        TunnelendpointTCPTunnelClient::APtr client_tunnelendpoint(TunnelendpointTCPTunnelClient::create(checkpoint_handler_client, async_service->get_io_service(), client_ip, client_ip_port, 1));
        client_tunnelendpoint->set_connector(&client_connector);

        string server_ip = "127.0.0.1";
        unsigned long server_ip_port = 8852;
        UnittestTunnelendpointConnector server_connector;
        TunnelendpointTCPTunnelServer::APtr server_tunnelendpoint(TunnelendpointTCPTunnelServer::create(checkpoint_handler_server, async_service->get_io_service(), server_ip, server_ip_port, 1));
        server_tunnelendpoint->set_connector(&server_connector);

        client_connector.tunnelendpoint_ = server_tunnelendpoint.get();
        server_connector.tunnelendpoint_ = client_tunnelendpoint.get();

        client_tunnelendpoint->connect();
        server_tunnelendpoint->connect();

        ASimulationServer::APtr sim_server(ASimulationServer::create(sims_checkpoint_handler, async_service->get_io_service(), server_ip, server_ip_port, 1200));
        sim_server->start();

        threads.create_thread(boost::bind(&boost::asio::io_service::run, &async_service->get_io_service()));

        UnittestDriver unittest_driver(async_service);
        unittest_driver.client_tunnelendpoint = client_tunnelendpoint;
        unittest_driver.server_tunnelendpoint = server_tunnelendpoint;



        threads.create_thread(boost::bind(&UnittestDriver::run, &unittest_driver));
        while(!unittest_driver.done) {
            boost::this_thread::sleep(boost::posix_time::seconds(1));

        }

        async_service->stop();

        threads.join_all();
    }
    catch (const std::exception& e) {
        cout << e.what() << "\n";
        BOOST_FAIL(e.what());
    }
}

boost::unit_test::test_suite* init_unit_test_suite(int, char*[]) {
    return 0;
}
