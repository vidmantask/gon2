/*! \file COM_SessionEventhandler.hxx
 *  \brief This file contains abstract interfaces for handling signals from a session
 */
#ifndef COM_SessionEventhandler_HXX
#define COM_SessionEventhandler_HXX

#include <component/communication/COM_Message.hxx>

namespace Giritech {
namespace Communication {

/*! \brief This class define the abstract interface for handling signals from a session
 */
class SessionEventhandler : public boost::noncopyable {
public:

    /*! \brief Signals that the session has been closed
     */
    virtual void session_closed(const unsigned long session_id) = 0;

    /*! \brief Signals that an unexpected event has occured and that the session should be closed imedeally
     */
    virtual void session_security_closed(const unsigned long session_id, const std::string& remote_ip) = 0;

};

/*! \brief This class define the abstract interface for handling signals from a session operating in ready state
 */
class SessionEventhandlerReady : public boost::noncopyable {
public:

    /*! \brief Signals that the session is ready
     */
    virtual void session_state_ready(void) = 0;

    /*! \brief Signals that the session is closed
     */
    virtual void session_state_closed(void) = 0;

    /*! \brief Signals that the session is dooing key exchange
     */
    virtual void session_state_key_exchange(void) = 0;

    /*! \brief Contuniue reading in ready state

     \return True indicate that the session should continue reading when a privious asyncron read has successfully ended
     */
    virtual bool session_read_continue_state_ready(void) = 0;

    /*! \brief User defined signal from a tunnelendpoint
     */
    virtual void session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) = 0;
};
}
}
#endif
