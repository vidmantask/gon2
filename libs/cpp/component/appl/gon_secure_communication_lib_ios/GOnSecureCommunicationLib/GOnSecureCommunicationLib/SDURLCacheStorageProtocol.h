//
//  SDURLCacheStorageProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 23/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SDURLCacheStorageProtocol <NSObject>


//
// Calculate a unique key from a url. The key is used as a filename for storing the cached response for the url.
//
-(NSString *)cacheKeyForURL:(NSURL *)url;


//
// Save cache control information in a file
//
-(void) saveDiskCacheInfo:(NSString*)diskCachePath diskCacheInfo:(NSMutableDictionary*)diskCacheInfo;


//
// Load cache control information from a file
//
-(NSMutableDictionary*) loadDiskCacheInfo:(NSString*)diskCachePath;


//
// Save respone in a file
//
-(bool) saveResponse:(NSString*)cacheFilePath response:(NSCachedURLResponse*)cachedResponse;


//
// Load respone from a file
//
-(NSCachedURLResponse*) loadResponse:(NSString*)cacheFilePath;


@end
