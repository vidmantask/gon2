//
//  GOnSecureBrowserSpecialProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 05/11/10.
//  Copyright 2010 giritech.com. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <Foundation/NSURLRequest.h>
#import <Foundation/NSURLProtocol.h>

#import "GOnSecureCommunicationLibProtocol.h"

#import "GOnSecureCommunicationLibJsAPIProtocol.h"
#import "GOnSecureCommunicationLibJsAPIInternalProtocol.h"

#include <component/communication/COM_TunnelendpointDirectHttpProxy.hxx>
#include <component/appl/gon_client/APPL_ClientCommand.hxx>
#include <component/appl/gon_client/APPL_ClientDevTools.hxx>
#include <component/http_client/HttpClient.hxx>
#include <component/http_client/HttpCookieStore.hxx>


/*
 * Class DirectHttpProxyConnector
 */
class DirectHttpProxyConnector : public Giritech::HttpClient::HttpClientEventhandler  {
public:
    typedef boost::shared_ptr<DirectHttpProxyConnector> APtr;
    
    ~DirectHttpProxyConnector(void);
    
    void startRequest(NSURLProtocol* specialProtocol);
    void cancelRequest(void);
    
    static APtr create(const Giritech::Communication::TunnelendpointDirect::APtr& tunnelendpointDirect, const Giritech::HttpClient::HttpCookieStore::APtr& cookie_store);
	
    // Implementation of HttpClientEventhandler
    void http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const std::vector< std::pair<std::string, std::string> >& headers);
	void http_client_response_body_data(Giritech::Utility::DataBufferManaged::APtr& body_data);
	void http_client_response_done(void);
	void http_client_response_error(void);
    
private:
    DirectHttpProxyConnector(const Giritech::Communication::TunnelendpointDirect::APtr& tunnelendpointDirect, const Giritech::HttpClient::HttpCookieStore::APtr& cookie_store);
    
    void reset(void);
    void dump(void);
    
    void networkLogRequest(NSURLRequest* request);
    void networkLogResponse(const bool cached, NSHTTPURLResponse* response);
    void networkLogResponseBody(NSData* body);
    void networkLogError(NSString* errorMessage);
    
    Giritech::Communication::TunnelendpointDirectHttpProxy::APtr iTunnelendpointDirectHttpProxy;
    NSURLProtocol* iSpecialProtocol;
    
    NSURL* iHttpRequestURL;
    
    bool iWasRedirected;
    bool iProgress;
    
    Giritech::Utility::Mutex::APtr iMutex;
    
    std::string iSessionId;
    
    Giritech::HttpClient::HttpClient::APtr iHttpClient;
};

@protocol DirectHttpProxyConnectorProtocol <NSObject>
-(void) createConnectionResponse:(const DirectHttpProxyConnector::APtr&) directHttpProxyConnector;
@end



/*
 * Class GOnSecureConnectionFactory
 */
class GOnSecureConnectionFactory : public Giritech::Appl::ApplClientDirectConnectionFactoryEventhandler, public Giritech::HttpClient::HttpCookieStoreEventhandler {
public:
    ~GOnSecureConnectionFactory(void);
    
    typedef boost::shared_ptr<GOnSecureConnectionFactory> APtr;

    void create_connection(id<DirectHttpProxyConnectorProtocol> specialProtocolDelegate);
    
    void direct_connection_factory_connection_ready(const Giritech::Communication::TunnelendpointDirect::APtr& connection);
    void direct_connection_factory_connection_failed(const std::string& errorMessage);
        
    Giritech::Utility::CheckpointHandler::APtr get_checkpoint_handler(void);
    
    Giritech::Utility::Mutex::APtr get_mutex(void);
    
    static APtr create(const Giritech::Appl::ApplClientDirectConnectionFactory::APtr& applClientDirectConnectionFactory);
    
    // Implementation of HttpCookieStoreEventhandler
    void cookie_added(const Giritech::HttpClient::HttpSetCookieValue& cookie_value);
	void cookie_removed(const Giritech::HttpClient::HttpSetCookieValue& cookie_value);
    
private:
    GOnSecureConnectionFactory(const Giritech::Appl::ApplClientDirectConnectionFactory::APtr& applClientDirectConnectionFactory);

    Giritech::Appl::ApplClientDirectConnectionFactory::APtr iApplClientDirectConnectionFactory;
    std::set< id<DirectHttpProxyConnectorProtocol> > iPendingConnects;
    
    Giritech::HttpClient::HttpCookieStore::APtr cookie_store_;
};
static GOnSecureConnectionFactory::APtr sGOnSecureConnectionFactory;


extern id<GOnSecureCommunicationLibJsAPIProtocol> sGOnSecureBrowserJsAPIProtocol;
extern id<GOnSecureCommunicationLibJsAPIInternalProtocol> sGOnSecureBrowserJsAPIInternalProtocol;





/*
 * Interface GOnSecureBrowserSpecialProtocol
 */
@interface GOnSecureBrowserSpecialProtocol : NSURLProtocol {
}
+ (NSString*) specialProtocolVarsKey;
+ (void) setDMEURL:(NSURL*)dmeURL;
+ (void) setHandleSchemeHTTPS:(GOnSCLHandleSchemeType)handle;
@end


/*
 * Interface GOnSecureBrowserSpecialProtocolHTTP
 */
@interface GOnSecureBrowserSpecialProtocolHTTP : GOnSecureBrowserSpecialProtocol {
}
+ (void) registerSpecialProtocol:(GOnSecureConnectionFactory::APtr)secureConnectionFactory;
+ (void) unregisterSpecialProtocol;

+ (BOOL)canInitWithRequest:(NSURLRequest *)theRequest;
+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request;
@end



@interface NSURLRequest (SpecialProtocol)
- (NSDictionary *)specialVars;
@end

@interface NSMutableURLRequest (SpecialProtocol)
- (void)setSpecialVars:(NSDictionary *)caller;
@end


