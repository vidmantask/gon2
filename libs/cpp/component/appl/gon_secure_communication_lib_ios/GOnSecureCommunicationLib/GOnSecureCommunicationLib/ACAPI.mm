//
//  ACAPI.mm
//  GOnClient
//
//  Created by gbuilder on 10/10/10.
//  Copyright 2010 Giritech. All rights reserved.
//
#import "ACAPI.h"
#import "Base64.h"

//
// Functionality for moving callbacks into the mainthread
//
@implementation ACAPIMainThread

-(void) setDelegate:(id<GOnSecureCommunicationLibProtocol>)delegate {
	delegate_ = delegate;
}

- (NSString*) gonSCLGetConnectInfo {
    return [delegate_ gonSCLGetConnectInfo];
}

-(void) gonSCLConnectStateConnectingWithConnectionTitleMT:(NSString*) connectionTitle {
	[delegate_ gonSCLConnectStateConnectingWithConnectionTitle:[connectionTitle autorelease]];
}
-(void) gonSCLConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle {
	[self performSelectorOnMainThread:@selector(gonSCLConnectStateConnectingWithConnectionTitleMT:) withObject:[connectionTitle retain] waitUntilDone:NO];
}

-(void) gonSCLConnectStateConnectingFailedWithMessageMT:(NSString*) message {
	[delegate_ gonSCLConnectStateConnectingFailedWithMessage:[message autorelease]];
}
-(void) gonSCLConnectStateConnectingFailedWithMessage:(NSString*) message {
	[self performSelectorOnMainThread:@selector(gonSCLConnectStateConnectingFailedWithMessageMT:) withObject:[message retain] waitUntilDone:NO];
}

- (void) gonSCLConnectStateConnectedMT {
	[delegate_ gonSCLConnectStateConnected];    
}
- (void) gonSCLConnectStateConnected {
	[self performSelectorOnMainThread:@selector(gonSCLConnectStateConnectedMT) withObject:nil waitUntilDone:NO];    
}

- (void) gonSCLConnectStateClosingMT {
	[delegate_ gonSCLConnectStateClosing];    
}
- (void) gonSCLConnectStateClosing {
	[self performSelectorOnMainThread:@selector(gonSCLConnectStateClosingMT) withObject:nil waitUntilDone:NO];    
}

- (void) gonSCLConnectStateClosedMT {
	[delegate_ gonSCLConnectStateClosed];        
}
- (void) gonSCLConnectStateClosed {
	[self performSelectorOnMainThread:@selector(gonSCLConnectStateClosedMT) withObject:nil waitUntilDone:NO];        
}

- (void) gonSCLUserRequestLoginWithMessageMT:(NSString*)message {
	[delegate_ gonSCLUserRequestLoginWithMessage:[message autorelease]];        
}
- (void) gonSCLUserRequestLoginWithMessage:(NSString*)message {
	[self performSelectorOnMainThread:@selector(gonSCLUserRequestLoginWithMessageMT:) withObject:[message retain] waitUntilDone:NO];        
}

- (void) gonSCLDialogShowLicenseInfoMT:(GOnSCLDialogShowLicenseInfoArgs*)args {
	[delegate_ gonSCLDialogShowLicenseInfo:[args autorelease]];        
}
- (void) gonSCLDialogShowLicenseInfo:(GOnSCLDialogShowLicenseInfoArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLDialogShowLicenseInfoMT:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLDialogMenuUpdateMT {
	[self gonSCLDialogMenuUpdate];        
}
- (void) gonSCLDialogMenuUpdate {
	[self performSelectorOnMainThread:@selector(gonSCLDialogMenuUpdate) withObject:nil waitUntilDone:NO];        
}

- (void) gonSCLDialogShowMenuMT:(GOnSCLDialogShowMenuArgs*)args {
	[delegate_ gonSCLDialogShowMenu: [args autorelease]];        
}
- (void) gonSCLDialogShowMenu:(GOnSCLDialogShowMenuArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLDialogShowMenuMT:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLDialogImageFoundMT:(GOnSCLDialogImageArgs*)args {
	[delegate_ gonSCLDialogImageFound:[args autorelease]];        
}
- (void) gonSCLDialogImageFound:(GOnSCLDialogImageArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLDialogImageFoundMT:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLDialogImageNotFoundMT:(GOnSCLDialogImageArgs*)args {
	[delegate_ gonSCLDialogImageNotFound:[args autorelease]];        
}
- (void) gonSCLDialogImageNotFound:(GOnSCLDialogImageArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLDialogImageNotFoundMT:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLInfoMT:(GOnSCLInfoArgs*)args {
	[delegate_ gonSCLInfo:[args autorelease]];        
}
- (void) gonSCLInfo:(GOnSCLInfoArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLInfo:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLInfoErrorMT:(GOnSCLInfoArgs*)args {
	[delegate_ gonSCLInfoError:[args autorelease]];        
}
- (void) gonSCLInfoError:(GOnSCLInfoArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLInfoErrorMT:) withObject:[args retain] waitUntilDone:NO];        
}

- (void) gonSCLInfoWarningMT:(GOnSCLInfoArgs*)args {
	[delegate_ gonSCLInfoWarning:[args autorelease]];            
}
- (void) gonSCLInfoWarning:(GOnSCLInfoArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLInfoWarningMT:) withObject:[args retain] waitUntilDone:NO];            
}

- (void) gonSCLEnrollRequestMT:(GOnSCLEnrollRequestArgs*)args {
	[delegate_ gonSCLEnrollRequest:[args autorelease]];            
}
- (void) gonSCLEnrollRequest:(GOnSCLEnrollRequestArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLEnrollRequestMT:) withObject:[args retain] waitUntilDone:NO];            
}

- (void) gonSCLEnrollResponseMT:(GOnSCLEnrollResponseArgs*)args {
	[delegate_ gonSCLEnrollResponse:[args autorelease]];                
}
- (void) gonSCLEnrollResponse:(GOnSCLEnrollResponseArgs*)args {
	[self performSelectorOnMainThread:@selector(gonSCLEnrollResponseMT:) withObject:[args retain] waitUntilDone:NO];                
}

- (void) gonSCLEnrollGetStatusResponseMT:(GOnSCLEnrollGetStatusResponseArgs*)args {
    [delegate_ gonSCLEnrollGetStatusResponse:[args autorelease]];            
}
- (void) gonSCLEnrollGetStatusResponse:(GOnSCLEnrollGetStatusResponseArgs*)args {
    [self performSelectorOnMainThread:@selector(gonSCLEnrollGetStatusResponseMT:) withObject:[args retain] waitUntilDone:NO];            
}

- (void) gonSCLTraffficLaunchStatusChangedMT:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    [delegate_ gonSCLTraffficLaunchStatusChanged:[args autorelease]];            
}
- (void) gonSCLTraffficLaunchStatusChanged:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    [self performSelectorOnMainThread:@selector(gonSCLTraffficLaunchStatusChangedMT:) withObject:[args retain] waitUntilDone:NO];            
}

- (void) gonSCLTraffficLaunchCommandMT:(GOnSCLTrafficLaunchCommandController*)commandController {
    [delegate_ gonSCLTraffficLaunchCommand:[commandController autorelease]];            
}
- (void) gonSCLTraffficLaunchCommand:(GOnSCLTrafficLaunchCommandController*)commandController {
    [self performSelectorOnMainThread:@selector(gonSCLTraffficLaunchCommandMT:) withObject:[commandController retain] waitUntilDone:NO];            
}

- (void) gonSCLTraffficLaunchCommandTerminateMT:(NSString*) launchId {
    [delegate_ gonSCLTraffficLaunchCommandTerminate:[launchId autorelease]];            
}
- (void) gonSCLTraffficLaunchCommandTerminate:(NSString*) launchId {
    [self performSelectorOnMainThread:@selector(gonSCLTraffficLaunchCommandTerminateMT:) withObject:[launchId retain] waitUntilDone:NO];            
}

- (void) gonSCLConnectInfoUpdatedMT {
    [delegate_ gonSCLConnectInfoUpdated];
}
- (void) gonSCLConnectInfoUpdated {
    [self performSelectorOnMainThread:@selector(gonSCLConnectInfoUpdatedMT) withObject:nil waitUntilDone:NO];            
}

- (void) gonSCLKeyStoreResetMT {
    [delegate_ gonSCLKeyStoreReset];            
}
- (void) gonSCLKeyStoreReset {
    [self performSelectorOnMainThread:@selector(gonSCLKeyStoreResetMT) withObject:nil waitUntilDone:NO];            
}

- (void) gonSCLVersionErrorMT:(NSString*)serverVersion {
    [delegate_ gonSCLVersionError:[serverVersion autorelease]];            
}
- (void) gonSCLVersionError:(NSString*)serverVersion {
    [self performSelectorOnMainThread:@selector(gonSCLVersionErrorMT:) withObject:[serverVersion retain] waitUntilDone:NO];            
}
@end




using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;
using namespace Giritech::Appl;

//
// Implementation class ACAPI
//
ACAPI::ACAPI(GOnSecureCommunicationLibConfig* config, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) 
: delegate_(nil), config_(config) {
	appl_client_ = ApplClient::create(this, [[config getServers] UTF8String], [[config getKnownsecret] UTF8String], checkpoint_handler);
    acAPIReachability_ = [[ACAPIReachability alloc] init:nil];
    delegate_ = [[ACAPIMainThread alloc] init];
}

ACAPI::APtr ACAPI::create(GOnSecureCommunicationLibConfig* config, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return ACAPI::APtr(new ACAPI(config, checkpoint_handler));
}

Giritech::Appl::ApplClient::APtr ACAPI::get(void) {
	return appl_client_;
}

void ACAPI::setDelegate(id<GOnSecureCommunicationLibProtocol> delegate) {
	[delegate_ setDelegate:delegate];
}

void ACAPI::actionConnect(void) {
	appl_client_->action_connect([[config_ getKnownsecret] UTF8String], [[config_ getServers] UTF8String]);
}



/*
 * Implementation of callbacks
 */
void ACAPI::app_client_event_connecting(const std::string& connection_title) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLConnectStateConnectingWithConnectionTitle:[NSString stringWithUTF8String:connection_title.c_str()]];
        }
    }
}

void ACAPI::app_client_event_connecting_failed(const std::string& message) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLConnectStateConnectingFailedWithMessage:[NSString stringWithUTF8String:message.c_str()]];
        }
    }
}

void ACAPI::app_client_event_connected(void) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLConnectStateConnected];
        }
    }
}
	
void ACAPI::app_client_event_closing(void) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLConnectStateClosing];
        }
    }
}

void ACAPI::app_client_event_closed(void) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLConnectStateClosed];
        }
    }
}

void ACAPI::app_client_user_request_login(const std::string& msg, const bool change_password_enabled) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLUserRequestLoginWithMessage:[NSString stringWithUTF8String:msg.c_str()]];
        }
    }
}

void ACAPI::app_client_auth_request_serial(void) {
}

void ACAPI::app_client_auth_request_challenge_response(const std::string& challenge) {
}

void ACAPI::app_client_auth_failed(void) {
}

void ACAPI::app_client_dialog_show_license_info(const std::string& header, const std::string& message, const bool valid)  {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLDialogShowLicenseInfoArgs* args = [[[GOnSCLDialogShowLicenseInfoArgs alloc] initWithArgs:[NSString stringWithUTF8String:header.c_str()] message:[NSString stringWithUTF8String:message.c_str()] valid:valid] autorelease];
            [delegate_ gonSCLDialogShowLicenseInfo:args];
        }
    }
}

void ACAPI::app_client_dialog_request_inform_on_first_access(const std::string&  message, const bool close_on_cancel) {
	appl_client_->app_client_dialog_response_inform_on_first_access();
}

void ACAPI::app_client_dialog_menu_updated(void) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLDialogMenuUpdate];
        }
    }
}

void ACAPI::app_client_dialog_show_menu(const std::vector<ApplClientMenuItem>& menu_items, const std::map<std::string, ApplClientTagProperty>& tag_properties) {
    @autoreleasepool {
        if(delegate_) {
            NSMutableArray* menuItems = [[[NSMutableArray alloc] init] autorelease];
            std::vector<Giritech::Appl::ApplClientMenuItem>::const_iterator i(menu_items.begin());
            while (i != menu_items.end()) {
                Giritech::Appl::ApplClientMenuItem menuItem(*i);
                if(menuItem.tag_show && !menuItem.tag_gupdate) {
                    [(NSMutableArray*)menuItems addObject: [[GOnSCLMenuItem alloc] initWithArgs:menuItem]];
                }
                ++i;
            }
            
            GOnSCLDialogShowMenuArgs* args = [[[GOnSCLDialogShowMenuArgs alloc] initWithArgs:menuItems] autorelease];
            [delegate_ gonSCLDialogShowMenu:args];
        }	
    }	
}

void ACAPI::app_client_dialog_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data) {
    @autoreleasepool {
        if(delegate_) {
            NSData* imageData = [Base64 decode:image_data.c_str() length:image_data.length()];
            UIImage* image = [UIImage imageWithData:imageData];
            GOnSCLDialogImageArgs* args = [[[GOnSCLDialogImageArgs alloc] initWithArgs:[NSString stringWithUTF8String:request_id.c_str()] imageId:[NSString stringWithUTF8String:image_id.c_str()] image:image] autorelease];
            [delegate_ gonSCLDialogImageFound:args];
        }
    }
}

void ACAPI::app_client_dialog_image_not_found(const std::string& request_id, const std::string& image_id) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLDialogImageArgs* args = [[[GOnSCLDialogImageArgs alloc] initWithArgs:[NSString stringWithUTF8String:request_id.c_str()] imageId:[NSString stringWithUTF8String:image_id.c_str()] image:nil] autorelease];
            [delegate_ gonSCLDialogImageNotFound:args];
        }
    }
}

void ACAPI::app_client_info(const std::string& header, const std::string& message)  {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLInfoArgs* args = [[[GOnSCLInfoArgs alloc] initWithArgs:[NSString stringWithUTF8String:header.c_str()] message:[NSString stringWithUTF8String:message.c_str()]] autorelease];
            [delegate_ gonSCLInfo:args];
        }
    }
}

void ACAPI::app_client_warning(const std::string& header, const std::string& message) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLInfoArgs* args = [[[GOnSCLInfoArgs alloc] initWithArgs:[NSString stringWithUTF8String:header.c_str()] message:[NSString stringWithUTF8String:message.c_str()]] autorelease];
            [delegate_ gonSCLInfoWarning:args];
        }
    }
}

void ACAPI::app_client_error(const std::string& header, const std::string& message) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLInfoArgs* args = [[[GOnSCLInfoArgs alloc] initWithArgs:[NSString stringWithUTF8String:header.c_str()] message:[NSString stringWithUTF8String:message.c_str()]] autorelease];
            [delegate_ gonSCLInfoError:args];
        }
    }
}

void ACAPI::app_client_endpoint_request_endpoint_info(void)  {
    @autoreleasepool {
        UIDevice* device = [UIDevice currentDevice];
        appl_client_->app_client_endpoint_response_endpoint_info("os_platform", [[device model] UTF8String]);
        appl_client_->app_client_endpoint_response_endpoint_info("os_platform_group", "ios");
        appl_client_->app_client_endpoint_response_endpoint_info("os_version", [[device systemVersion] UTF8String]);

        appl_client_->app_client_endpoint_response_endpoint_info("machine_name", [[device name] UTF8String]);
        appl_client_->app_client_endpoint_response_endpoint_info("machine_uuid", "deprecated");
        appl_client_->app_client_endpoint_response_endpoint_info("machine_model", [[device model] UTF8String]);
        appl_client_->app_client_endpoint_response_endpoint_info("machine_manufacture", "Apple");
        
        appl_client_->app_client_endpoint_response_endpoint_info_all_done();
    }
}

void ACAPI::app_client_endpoint_enrollment_request_token(void) {
    @autoreleasepool {
        appl_client_->app_client_endpoint_response_token("mobile", "ios", [[config_ getSerial] UTF8String], [[[UIDevice currentDevice] name] UTF8String]);
    }
}

void ACAPI::app_client_endpoint_enrollment_token_status(const bool can_deploy, const bool auth_activated, const bool assigned_to_current_user, const std::string& status_text) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLEnrollRequestArgs* args = [[[GOnSCLEnrollRequestArgs alloc] initWithArgs:can_deploy authActivated:auth_activated assignedToCurrentUser:assigned_to_current_user statusText:[NSString stringWithUTF8String:status_text.c_str()]] autorelease];
            [delegate_ gonSCLEnrollRequest:args];
        }
    }
}

void ACAPI::app_client_auth_generate_keys_response(const std::string& private_key, const std::string& public_key) {
    @autoreleasepool {
        [config_ setKeysWithPublicKey:[NSString stringWithUTF8String:private_key.c_str()] privateKey:[NSString stringWithUTF8String:public_key.c_str()]];
        appl_client_->app_client_endpoint_enroll_token([[config_ generateSerial] UTF8String], public_key);
    }
}

void ACAPI::app_client_endpoint_enrollment_enroll_token_response(const bool rc, const std::string& msg) {
    @autoreleasepool {
        if(delegate_) {
//        GOnSCLEnrollResponseArgs* args = [[[GOnSCLEnrollResponseArgs alloc] initWithArgs:rc message:[NSString stringWithUTF8String:msg.c_str()]] autorelease];
//		[delegate_ gonSCLEnrollResponse:args];
        }
	}
}

void ACAPI::app_client_endpoint_get_enrollment_status_response(const bool enroll_error, const bool enroll_done, const std::string& enroll_message) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLEnrollGetStatusResponseArgs* args = [[[GOnSCLEnrollGetStatusResponseArgs alloc] initWithArgs:enroll_error enrollDone:enroll_done message:[NSString stringWithUTF8String:enroll_message.c_str()]] autorelease];
            [delegate_ gonSCLEnrollGetStatusResponse:args];
        }
	}
}

void ACAPI::app_client_traffic_launch_status_changed(const std::string& launch_id, const State state, const std::string& status_message) {
    @autoreleasepool {
        if(delegate_) {
            GOnSCLTrafficLaunchStatusChangedArgs* args = [[[GOnSCLTrafficLaunchStatusChangedArgs alloc] initWithArgs:[NSString stringWithUTF8String:launch_id.c_str()] state:state message:[NSString stringWithUTF8String:status_message.c_str()]] autorelease];
            [delegate_ gonSCLTraffficLaunchStatusChanged:args];
        }
    }
}

void ACAPI::app_client_traffic_launch_command(const std::string& launch_id, const ApplClientCommand& launch_command) {
    @autoreleasepool {
        if(delegate_) {
            if(launch_command.getCacheType() == ApplClientCommand::CacheType_ClearOnLaunch) {
                [[NSURLCache sharedURLCache] removeAllCachedResponses];
            }
            GOnSCLTrafficLaunchCommandArgs* args = [[[GOnSCLTrafficLaunchCommandArgs alloc] initWithArgs:[NSString stringWithUTF8String:launch_id.c_str()] command:launch_command] autorelease];
            GOnSCLTrafficLaunchCommandController* launchController = [[GOnSCLTrafficLaunchCommandController alloc] initWithArgs:args];
            [delegate_ gonSCLTraffficLaunchCommand:launchController];
        }
    }
}

void ACAPI::app_client_traffic_launch_command_terminate(const std::string& launch_id) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLTraffficLaunchCommandTerminate:[NSString stringWithUTF8String:launch_id.c_str()]];
        }
    }
}

void ACAPI::app_client_tag_get_platform(void) {
    @autoreleasepool {
        UIDevice* device = [UIDevice currentDevice];
        NSString* platform = [NSString stringWithFormat:@"dme,%@", [device model]]; 
        appl_client_->app_client_tag_get_platform_response([platform UTF8String]);
    }
}

void ACAPI::app_client_set_background_settings(const bool close_when_entering_background) {
    @autoreleasepool {
        [config_ setBackgroundSettings:close_when_entering_background];
    }
}

void ACAPI::app_client_cpm_update_connection_info(const std::string& servers) {
    @autoreleasepool {
        [config_ setConnectInfo:[NSString stringWithUTF8String:servers.c_str()]];    
        appl_client_->action_close();
        if(delegate_) {
            [delegate_ gonSCLConnectInfoUpdated];
        }
    }
}

bool ACAPI::app_client_is_network_available(void) {
    @autoreleasepool {
        bool network_available = [acAPIReachability_ canReachServer];
        return network_available;
    }
}

void ACAPI::app_client_version_error(const std::string& server_version) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLVersionError:[NSString stringWithUTF8String:server_version.c_str()]];
        }
    }
}

void ACAPI::app_client_key_store_reset(void) {
    @autoreleasepool {
        if(delegate_) {
            [delegate_ gonSCLKeyStoreReset];
        }
    }
}

void ACAPI::app_client_key_store_session_key_pair_set(Giritech::Utility::DataBufferManaged::APtr& sessionKeyPublic, Giritech::Utility::DataBufferManaged::APtr& sessionKeyPrivate) {
    @autoreleasepool {
        if(sessionKeyPublic.get() && sessionKeyPrivate.get()) {
            NSString* sessionKeyPublicStr = [NSString stringWithUTF8String:sessionKeyPublic->encodeHex()->toString().c_str()];
            NSString* sessionKeyPrivateStr = [NSString stringWithUTF8String:sessionKeyPrivate->encodeHex()->toString().c_str()];
            [config_ setSessionPublicKey:sessionKeyPublicStr];
            [config_ setSessionPrivateKey:sessionKeyPrivateStr];
        }
    }
}

void ACAPI::app_client_key_store_session_key_pair_get(Giritech::Utility::DataBufferManaged::APtr& sessionKeyPublic, Giritech::Utility::DataBufferManaged::APtr& sessionKeyPrivate) {
    @autoreleasepool {
        NSString* sessionKeyPublicStr = [config_ getSessionPublicKey];
        NSString* sessionKeyPrivateStr = [config_ getSessionPrivateKey];
        if(sessionKeyPublicStr && sessionKeyPrivateStr) {
            sessionKeyPublic = Giritech::Utility::DataBufferManaged::create([sessionKeyPublicStr UTF8String])->decodeHex();
            sessionKeyPrivate = Giritech::Utility::DataBufferManaged::create([sessionKeyPrivateStr UTF8String])->decodeHex();        
        }
    }
}

void ACAPI::app_client_dme_get_dme_value(void) {
    @autoreleasepool {
        if(config_) {
            appl_client_->app_client_dme_response_get_dme_values([[config_ getDMEValueUsername] UTF8String], [[config_ getDMEValuePassword] UTF8String], [[config_ getDMEValueTerminalId] UTF8String], [[config_ getDMEValueDeviceSignature] UTF8String], [[config_ getDMEValueDeviceTime] UTF8String]);
        }
        else {
            appl_client_->app_client_dme_response_get_dme_values_none();
        }
    }
}
