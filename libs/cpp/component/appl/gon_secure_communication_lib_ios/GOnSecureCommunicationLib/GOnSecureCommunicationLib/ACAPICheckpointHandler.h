//
//  ACAPICheckpointHandler.h
//  GOnClient
//
//  Created by gbuilder on 01/12/10.
//  Copyright 2010 Giritech. All rights reserved.
//
#import <Foundation/Foundation.h>
#include "lib/utility/UY_Checkpoint_ALL.hxx"

@interface ACAPICheckpointHandler : NSObject {
	Giritech::Utility::CheckpointHandler::APtr checkpointHandler_;
    
    BOOL logToFile_;
    NSString* logToFileName_;
    NSString* logLevel_;
    
}

-(id) init;

-(void) cpDebug:(NSString*)cpId message:(NSString*) message;
-(void) cpDebug:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr;


-(void) cpInfo:(NSString*)cpId message:(NSString*) message;
-(void) cpInfo:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr;
-(void) cpInfos:(NSString*)messageId message:(NSString*) messages;

-(void) cpWarning:(NSString*)cpId message:(NSString*) message;
-(void) cpWarning:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr;

-(void) cpError:(NSString*)cpId message:(NSString*) message;
-(void) cpError:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr;

-(void) cpCritical:(NSString*)cpId message:(NSString*) message;
-(void) cpCritical:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr;


-(Giritech::Utility::CheckpointHandler::APtr) getCheckpointHanderRaw;

+ (ACAPICheckpointHandler*)shared;

@end
