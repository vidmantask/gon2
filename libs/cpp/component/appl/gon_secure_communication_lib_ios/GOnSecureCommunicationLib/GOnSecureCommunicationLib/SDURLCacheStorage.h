//
//  SDURLCacheStorage.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 23/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SDURLCacheStorageProtocol.h"



@interface SDURLCacheStorage : NSObject <SDURLCacheStorageProtocol>

-(NSString *)cacheKeyForURL:(NSURL *)url;

-(void) saveDiskCacheInfo:(NSString*)diskCachePath diskCacheInfo:(NSMutableDictionary*)diskCacheInfo;

-(NSMutableDictionary*) loadDiskCacheInfo:(NSString*)diskCachePath;

-(bool) saveResponse:(NSString*)cacheFilePath response:(NSCachedURLResponse*)cachedResponse;

-(NSCachedURLResponse*) loadResponse:(NSString*)cacheFilePath;


@end
