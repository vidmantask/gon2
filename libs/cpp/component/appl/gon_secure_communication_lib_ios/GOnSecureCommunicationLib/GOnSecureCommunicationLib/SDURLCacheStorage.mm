//
//  SDURLCacheStorage.m
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 23/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "SDURLCacheStorage.h"

#import <CommonCrypto/CommonDigest.h>

static NSString *const kAFURLCacheInfoFileName = @"cacheInfo.plist";


@implementation SDURLCacheStorage

- (NSString *)cacheKeyForURL:(NSURL *)url {
    const char *str = [url.absoluteString UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), r);
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
}

-(void) saveDiskCacheInfo:(NSString*)diskCachePath diskCacheInfo:(NSMutableDictionary*)diskCacheInfo {
    NSData *data = [NSPropertyListSerialization dataFromPropertyList:diskCacheInfo format:NSPropertyListBinaryFormat_v1_0 errorDescription:NULL];
    if (data) {
        [data writeToFile:[diskCachePath stringByAppendingPathComponent:kAFURLCacheInfoFileName] atomically:YES];
    }
}

-(NSMutableDictionary*) loadDiskCacheInfo:(NSString*)diskCachePath {
    return [[NSMutableDictionary alloc] initWithContentsOfFile:[diskCachePath stringByAppendingPathComponent:kAFURLCacheInfoFileName]];
}

-(bool) saveResponse:(NSString*)cacheFilePath response:(NSCachedURLResponse*)cachedResponse {
    return [NSKeyedArchiver archiveRootObject:cachedResponse toFile:cacheFilePath];
}

-(NSCachedURLResponse*) loadResponse:(NSString*)cacheFilePath {
    return [NSKeyedUnarchiver unarchiveObjectWithFile:cacheFilePath];
}

@end
