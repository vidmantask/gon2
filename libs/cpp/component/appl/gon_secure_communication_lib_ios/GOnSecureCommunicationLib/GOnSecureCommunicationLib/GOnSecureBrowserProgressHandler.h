//
//  GOnSecureBrowserProgressHandler.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 08/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureBrowserProgressHandlerProtocol <NSObject>
- (void) gOnSecureCommunicationProgressBegin;
- (void) gOnSecureCommunicationProgress:(NSInteger)progress;
- (void) gOnSecureCommunicationProgressEnd;
- (void) gOnSecureCommunicationProgressContentIsNotHTML;
- (bool) gOnSecureCommunicationIsCacheEnabled;
- (void) gOnSecureCommunicationProgressErrorLoadingContent;
@end


@interface GOnSecureBrowserProgressHandler : NSObject

+ (GOnSecureBrowserProgressHandler*) getShared;

- (void) beginWithUrl:(NSURL*)url delegate:(id<GOnSecureBrowserProgressHandlerProtocol>)delegate;

- (void) resetDelegate;

- (void) reset;

- (void) reportBegin;
- (void) reportDone;

- (void) reportPartBegin:(NSInteger)size;
- (bool) reportPartBeginWirhHeaders:(NSDictionary*)httpResponseHeaderFields;
- (void) reportPartDone:(NSInteger)size;
- (void) reportContentIsNotHTML;
- (void) reportErrorLoadingContent;

- (bool) compareWithHeaders:(NSDictionary*)httpResponseHeaderFields;
- (bool) compareWithUrl:(NSURL*) url;

- (bool) isCacheEnabled;

@end
