//
//  GOnSecureCommunicationLib.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 21/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GOnSecureCommunicationLibConfigProtocol.h"
#import "GOnSecureCommunicationLibProtocol.h"
#import "GOnSecureCommunicationLibJsAPIProtocol.h"
#import "SDURLCacheStorageProtocol.h"


typedef enum {
    kGOnSCLConnectionStateInitializing = 0,
    kGOnSCLConnectionStateReadyToConnect,
    kGOnSCLConnectionStateWaitingForNetwork,
    kGOnSCLConnectionStateConnecting,
    kGOnSCLConnectionStateConnected,
    kGOnSCLConnectionStateClosing,
    kGOnSCLConnectionStateClosed,
    kGOnSCLConnectionStateUnknown
} GOnSCLConnectionState;

typedef enum {
    kGOnSCLLaunchStateInitializing = 0,
    kGOnSCLLaunchStateReadyToConnect
} GOnSCLLaunchState;




//
// GOnSecureCommunicationLib
//
@interface GOnSecureCommunicationLib : NSObject {
}

//
// Instance
//
- (id) initWithConfig:(id<GOnSecureCommunicationLibConfigProtocol>)config cacheStorageDelegate:(id<SDURLCacheStorageProtocol>)storageDelegate;
- (void) setDelegate:(id<GOnSecureCommunicationLibProtocol>)delegate;
- (void) setJsAPIDelegate:(id<GOnSecureCommunicationLibJsAPIProtocol>)jsAPIDelegate;

//
// Async control
//
- (void) gonAsyncStart;
- (void) gonAsyncRestart;
- (void) gonAsyncStop;
- (void) gonAsyncRestartIfNotRunning;

//
// Connection control
//
- (void) connectionConnect;
- (void) connectionDisconnect;
- (GOnSCLConnectionState) connectionGetState;

//
// A callback to gonSCLUserRequestLoginWithMessage should response by calling one of the following methods 
//
- (void) userResponseLogin:(NSString*)login password:(NSString*)password;
- (void) userResponseLoginCancelled;

//
// dialogGetImage*, will response in the callbacks gonSCLDialogImageFound og gonSCLDialogImageNotFound for each image in the request.
//
-(void) dialogGetImages:(NSString*) requestId imageIds:(NSArray*)imageIds imageStyle:(NSString*)imageStyle imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY imageFormat:(NSString*)imageFormat;
-(void) dialogGetImages:(NSString*) requestId imageIds:(NSArray*)imageIds imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY;
-(void) dialogGetImage:(NSString*) requestId imageId:(NSString*)imageId imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY;

//
// Launch
//
-(void) dialogMenuItemLaunch:(NSString*)launchId;

//
// Returns YES if the launchId is launched
//
-(BOOL) dialogMenuItemIsLaunched:(NSString*)launchId;

//
// Terminate launch
//
-(void) dialogMenuItemTerminate:(NSString*)launchId;

//
// Get status for launch
//
-(GOnSCLConnectionState) dialogMenuItemGetStatus:(NSString*)launchId;

//
// A callback to gonSCLTraffficLaunchCommand should response by calling this method
// In case of error, trafficLaunchCommandTerminatedWithError should be called
//
-(void) trafficLaunchCommandRunning:(NSString*)launchId;

//
// A callback to gonSCLTraffficLaunchCommandTerminate should response by calling this method
//
-(void) trafficLaunchCommandTerminated:(NSString*)launchId;
-(void) trafficLaunchCommandTerminatedWithError:(NSString*)launchId errorMessage:(NSString*)errorMessage;

//
// Start enrollment, response to gonSCLEnrollRequest
//
-(void) enroll;

//
// Get version of Secure Browser Lib
//
+(NSString*) getVersion;


@end
