//
//  GOnSecureBrowserSpecialProtocol.mm
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 05/11/10.
//  Copyright 2010 giritech.com. All rights reserved.
//

#import <Foundation/NSError.h>

#import "GOnSecureBrowserSpecialProtocol.h"
#import "GOnSecureBrowserProgressHandler.h"

#import "SDURLCache.h"
#import "ACAPICheckpointHandler.h"
#include <component/communication/COM_TunnelendpointDirectHttpProxy.hxx>
#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>


static BOOL sGOnSecureBrowserSpecialProtocolHTTPInited = NO;
static NSArray*  sGOnSecureBrowserSpecialProtocolHandledSchems = [NSArray arrayWithObjects:@"http", @"ftp", @"ws", @"wss", @"file", nil];
static NSURL*    sGOnSecureBrowserSpecialProtocolDMEURL        = NULL;
static NSString* sGOnSecureBrowserSpecialProtocolDMEURLScheme  = NULL;
static NSString* sGOnSecureBrowserSpecialProtocolDMEURLHost    = NULL;
static GOnSCLHandleSchemeType sGOnSecureBrowserSpecialProtocolHandleSchemeHTTPS = kGOnSCLHandleSchemeTypeDoNotHandle;


@implementation NSURLRequest (GOnSecureBrowserSpecialProtocol)
- (NSDictionary *)specialVars {
	return [NSURLProtocol propertyForKey:[GOnSecureBrowserSpecialProtocol specialProtocolVarsKey] inRequest:self];
}
@end

@implementation NSMutableURLRequest (GOnSecureBrowserSpecialProtocol)
- (void)setSpecialVars:(NSDictionary *)specialVars {
	NSDictionary *specialVarsCopy = [specialVars copy];
	[NSURLProtocol setProperty:specialVarsCopy forKey:[GOnSecureBrowserSpecialProtocol specialProtocolVarsKey] inRequest:self];
	[specialVarsCopy release];
}
@end



//
// Interface GOnSecureBrowserSpecialProtocol
//
@interface GOnSecureBrowserSpecialProtocol () <DirectHttpProxyConnectorProtocol> {
    DirectHttpProxyConnector::APtr iDirectHttpProxyConnector;
}
@end

@interface GOnSecureBrowserSpecialProtocol () {
    bool iLoadingDone;
    bool iProgress;    
}
@end


//
// Implementation GOnSecureBrowserSpecialProtocol
//
@implementation GOnSecureBrowserSpecialProtocol

+ (void)initialize {
    sGOnSecureBrowserSpecialProtocolDMEURL = nil;
    sGOnSecureBrowserSpecialProtocolDMEURLScheme = nil;
    sGOnSecureBrowserSpecialProtocolDMEURLHost = nil;
}

+ (void) setDMEURL:(NSURL*)dmeURL {
    if(dmeURL) {
        [sGOnSecureBrowserSpecialProtocolDMEURL release];
        [sGOnSecureBrowserSpecialProtocolDMEURLScheme release];
        [sGOnSecureBrowserSpecialProtocolDMEURLHost release];
        sGOnSecureBrowserSpecialProtocolDMEURL = [dmeURL retain];
        sGOnSecureBrowserSpecialProtocolDMEURLScheme = [[dmeURL scheme] retain];
        sGOnSecureBrowserSpecialProtocolDMEURLHost = [[dmeURL host] retain];
    }
}

+(void) setHandleSchemeHTTPS:(GOnSCLHandleSchemeType)handle {
    sGOnSecureBrowserSpecialProtocolHandleSchemeHTTPS = handle;
}

+ (NSString*) specialProtocolVarsKey {
	return @"specialVarsKey";
}

- (id)initWithRequest:(NSURLRequest *)request cachedResponse:(NSCachedURLResponse *)cachedResponse client:(id < NSURLProtocolClient >)client {
    self = [super initWithRequest:request cachedResponse:cachedResponse client:client];
    if (self) {
        iLoadingDone = false;
    }
    return self;
}

- (void)dealloc {
    [super dealloc];
}

- (void)startLoading {
    
    if(!sGOnSecureConnectionFactory) {
        NSLog(@"StartLoading.done.ignored URL : %@", [[self request] URL]);
        NSError* error = [NSError errorWithDomain:@"" code:502 userInfo:nil];
        [[self client] URLProtocol:self didFailWithError:error];
        [self loadingDone];
        return;
    }
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), sGOnSecureConnectionFactory->get_mutex(), "startLoading");

    
    // Check for dme url
    NSString* requestScheme = [[[self request] URL] scheme];
    NSString* requestHost   = [[[self request] URL] host];
    if( [requestHost isEqualToString:sGOnSecureBrowserSpecialProtocolDMEURLHost] && [requestScheme isEqualToString:sGOnSecureBrowserSpecialProtocolDMEURLScheme]) {
        Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "StartLoading.scheme.blocked.dme", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_warning(), Giritech::Utility::CpAttr("url", [[[[self request] URL] absoluteString] UTF8String]), Giritech::Utility::CpAttr("scheme", [[[[self request] URL] scheme] UTF8String]));
        NSError* error = [NSError errorWithDomain:@"Scheme not supported" code:405 userInfo:nil];
        [[self client] URLProtocol:self didFailWithError:error];
        [self loadingDone];
        return;
    }
    
    //Handle blocking of schemes 
    bool blockScheme = true;
    if ([requestScheme caseInsensitiveCompare:@"https"] == NSOrderedSame) {
        switch(sGOnSecureBrowserSpecialProtocolHandleSchemeHTTPS) {
            case kGOnSCLHandleSchemeTypeHandle:
                blockScheme = false;
                break;
            case kGOnSCLHandleSchemeTypeDoNotHandle:
            case kGOnSCLHandleSchemeTypeBlock:
                blockScheme = true;
                break;
        }
    }
    else if ([requestScheme caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        blockScheme = false;
    }
    if(blockScheme) {
        Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "StartLoading.scheme.blocked", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("url", [[[[self request] URL] absoluteString] UTF8String]), Giritech::Utility::CpAttr("scheme", [[[[self request] URL] scheme] UTF8String]));
        NSError* error = [NSError errorWithDomain:@"Scheme not supported" code:405 userInfo:nil];
        [[self client] URLProtocol:self didFailWithError:error];
        [self loadingDone];
        return;
    }

    // Handling JsAPI
    if ([self isDMEJsApiNamespace]) {
        NSMutableData* responseBody = [[[NSMutableData alloc] init] autorelease];
        NSHTTPURLResponse* response = [self handleJsApi:responseBody];
        if(response) {
            [[self client] URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageNotAllowed];
            [[self client] URLProtocol:self didLoadData:responseBody];
            [[self client] URLProtocolDidFinishLoading:self];
            [self loadingDone];
            return;
        }
    }

    Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "StartLoading", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("url", [[[[self request] URL] absoluteString] UTF8String]));

    // Handling Cache
    NSString* httpMethod = [[self request] HTTPMethod];
    if([[GOnSecureBrowserProgressHandler getShared] isCacheEnabled] && [httpMethod isEqualToString:@"GET"]) {
        NSCachedURLResponse* cashedResponse = [[NSURLCache sharedURLCache] cachedResponseForRequest:[self request]];
        if(cashedResponse && [self isCacheResponseValid:cashedResponse]) {
            bool progress = [[GOnSecureBrowserProgressHandler getShared] compareWithUrl:[[self request] URL]];
            if(progress) {
                [[GOnSecureBrowserProgressHandler getShared] reset];
                [[GOnSecureBrowserProgressHandler getShared] reportBegin];
            }
            [[self client] URLProtocol:self didReceiveResponse:[cashedResponse response] cacheStoragePolicy:[cashedResponse storagePolicy]];
            [[self client] URLProtocol:self didLoadData:[cashedResponse data]];
            [[self client] URLProtocolDidFinishLoading:self];
            [self loadingDone];
            if(progress) {
                [[GOnSecureBrowserProgressHandler getShared] reportDone];
            }
            Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "StartLoading.done.Cached", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug());
            return;
        }
    }
    sGOnSecureConnectionFactory->create_connection(self);
}

-(void) loadingDone {
    iLoadingDone = true;
}

-(void) createConnectionResponse:(const DirectHttpProxyConnector::APtr&) directHttpProxyConnector {
    iDirectHttpProxyConnector = directHttpProxyConnector;
    if(iDirectHttpProxyConnector != NULL) {
        iDirectHttpProxyConnector->startRequest(self);
    }
}

- (void)stopLoading {
    if(!iLoadingDone) {
        Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), sGOnSecureConnectionFactory->get_mutex(), "stopLoading");

        Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "stopLoading", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("url", [[[[self request] URL] absoluteString] UTF8String]));

        if(iDirectHttpProxyConnector) {
            iDirectHttpProxyConnector->cancelRequest();
        }
    }    
}


- (NSHTTPURLResponse*) handleJsApi:(NSMutableData*)responseBody {
    NSString* method = @"unknown";
    
    NSArray* pathComponents = [[[self request] URL] pathComponents];
    if([pathComponents count] > 2) {
        method = (NSString*) [pathComponents objectAtIndex:2];
    }
    
    if([method isEqualToString:@"log"]) {
        NSMutableDictionary* queryArguments = [self getQueryArguments:[[[self request] URL] query]];
        NSString* logData = [[queryArguments allValues] componentsJoinedByString:@", "];
        Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "handleJsApi.log", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("content", [logData UTF8String]));            
        return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
    }
    
    
    Giritech::Utility::Checkpoint cp1(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "handleJsApi", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("method", [method UTF8String]));
    
    if([method isEqualToString:@"dismiss_secure_browser"]) {
        [sGOnSecureBrowserJsAPIProtocol gOnSecureCommunicationLibJsAPIDismissBrowser];
        return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
    }
    
    if([method isEqualToString:@"localstorage_clear"]) {
        [sGOnSecureBrowserJsAPIProtocol gOnSecureCommunicationLibJsAPILocalStorageClear:[[[self request] URL] host]];
        return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
    }
    
    if([method isEqualToString:@"localstorage_get_item"]) {
        NSMutableDictionary* queryArguments = [self getQueryArguments:[[[self request] URL] query]];
        NSString* key = [queryArguments objectForKey:@"key"];
        if(key) {
            NSString* value = [sGOnSecureBrowserJsAPIProtocol gOnSecureCommunicationLibJsAPILocalStorageGetItem:[[[self request] URL] host] key:key];
            NSDictionary* result = [NSDictionary dictionaryWithObject:value forKey:@"value"];
            return [self generateJSONResponse:result responseBody:responseBody];
        }
    }
    
    if([method isEqualToString:@"localstorage_remove_item"]) {
        NSMutableDictionary* queryArguments = [self getQueryArguments:[[[self request] URL] query]];
        NSString* key = [queryArguments objectForKey:@"key"];
        if(key) {
            [sGOnSecureBrowserJsAPIProtocol gOnSecureCommunicationLibJsAPILocalStorageRemoveItem:[[[self request] URL] host] key:key];
            return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
        }
    }
    
    if([method isEqualToString:@"localstorage_set_item"]) {
        NSMutableDictionary* queryArguments = [self getQueryArguments:[[[self request] URL] query]];
        NSString* key = [queryArguments objectForKey:@"key"];
        NSString* value = [queryArguments objectForKey:@"value"];
        if(key) {
            [sGOnSecureBrowserJsAPIProtocol gOnSecureCommunicationLibJsAPILocalStorageSetItem:[[[self request] URL] host] key:key value:value];
            return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
        }
    }
        
    if([method isEqualToString:@"launch_menu_item"]) {
        NSMutableDictionary* queryArguments = [self getQueryArguments:[[[self request] URL] query]];
        NSString* launchId = [queryArguments objectForKey:@"launchid"];
        [sGOnSecureBrowserJsAPIInternalProtocol gOnSecureCommunicationLibJsAPILaunchMenuItem:launchId];
        return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:nil] autorelease];
    }

    
    Giritech::Utility::Checkpoint cp2(*(sGOnSecureConnectionFactory->get_checkpoint_handler()), "handleJsApi.method_not_found_on_client", Giritech::Appl::Attr_iDevice(), Giritech::Utility::CpAttr_debug(), Giritech::Utility::CpAttr("method", [method UTF8String]));
    return nil;
}

-(NSMutableDictionary*) getQueryArguments:(NSString*)query {
    NSMutableDictionary* dictParameters = [[[NSMutableDictionary alloc] init] autorelease];
    NSArray* arrParameters = [query componentsSeparatedByString:@"&"];
    for (int i = 0; i < [arrParameters count]; i++) {
        NSArray *arrKeyValue = [[arrParameters objectAtIndex:i] componentsSeparatedByString:@"="];
        if ([arrKeyValue count] >= 2) {
            NSMutableString *strKey = [NSMutableString stringWithCapacity:0];
            [strKey setString:[[[arrKeyValue objectAtIndex:0] lowercaseString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
            NSMutableString *strValue   = [NSMutableString stringWithCapacity:0];
            [strValue setString:[[[arrKeyValue objectAtIndex:1]  stringByReplacingOccurrencesOfString:@"+" withString:@" "] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
            if (strKey.length > 0) [dictParameters setObject:strValue forKey:strKey];
        }
    }
    return dictParameters;
}


-(NSHTTPURLResponse*) generateJSONResponse:(NSDictionary*)result responseBody:(NSMutableData*)responseBody {
    NSData* resultJSON = [NSJSONSerialization dataWithJSONObject:result options:0 error:nil];
    NSString* resultJSONLength = [NSString stringWithFormat:@"%i", [resultJSON length]]; 
    
    NSMutableDictionary* httpResponseHeaderFields = [[[NSMutableDictionary alloc] init] autorelease];
    [httpResponseHeaderFields setObject:@"application/json" forKey:@"Content-Type"];
    [httpResponseHeaderFields setObject:resultJSONLength forKey:@"Content-Length"];
    
    [responseBody setData:resultJSON];
    return [[[NSHTTPURLResponse alloc] initWithURL:[[self request] URL] statusCode:200 HTTPVersion:@"HTTP/1.1" headerFields:httpResponseHeaderFields] autorelease];  
}

-(bool) isDMEJsApiNamespace {
    NSArray* pathComponents = [[[self request] URL] pathComponents];
    if([pathComponents count] > 2) {
        NSString* pathComponentFirst = (NSString*) [pathComponents objectAtIndex:1];
        if([pathComponentFirst isEqualToString:@"com.excitor.dme.jsapi"]) {
            return true;
        }
    }
    return false;
}

-(bool) isDMENamespace {
    NSArray* pathComponents = [[[self request] URL] pathComponents];
    if([pathComponents count] > 2) {
        NSString* pathComponentFirst = (NSString*) [pathComponents objectAtIndex:1];
        if([pathComponentFirst hasPrefix:@"com.excitor.dme"]) {
            return true;
        }
    }
    return false;
}

-(bool) isCacheResponseValid:(NSCachedURLResponse*) cachedResponse {
    NSDictionary* headers = [(NSHTTPURLResponse *)cachedResponse.response allHeaderFields];
    NSDate* expirationDate = [SDURLCache expirationDateFromHeaders:headers withStatusCode:((NSHTTPURLResponse *)cachedResponse.response).statusCode];
    if (!expirationDate || [expirationDate timeIntervalSinceNow] <= 0) {
        return false;
    }
    return true;
}

@end




/*
 * Implementation GOnSecureBrowserSpecialProtocolHTTP 
 */
@implementation GOnSecureBrowserSpecialProtocolHTTP

+ (void) registerSpecialProtocol:(GOnSecureConnectionFactory::APtr)secureConnectionFactory{
	if (! sGOnSecureBrowserSpecialProtocolHTTPInited) {
		[NSURLProtocol registerClass:[GOnSecureBrowserSpecialProtocolHTTP class]];
		sGOnSecureBrowserSpecialProtocolHTTPInited = YES;
	}
    sGOnSecureConnectionFactory = secureConnectionFactory;
}

+ (void) unregisterSpecialProtocol {
	[NSURLProtocol unregisterClass:[GOnSecureBrowserSpecialProtocolHTTP class]];
    sGOnSecureBrowserSpecialProtocolHTTPInited = NO;
    sGOnSecureConnectionFactory.reset();
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)theRequest {
	NSString* theScheme = [[[theRequest URL] scheme] lowercaseString];

    if([theScheme isEqualToString:@"https"]) {
        switch(sGOnSecureBrowserSpecialProtocolHandleSchemeHTTPS) {
            case kGOnSCLHandleSchemeTypeDoNotHandle:
                return false;
            case kGOnSCLHandleSchemeTypeHandle:
            case kGOnSCLHandleSchemeTypeBlock:
                return true;
        }
    }
    return [sGOnSecureBrowserSpecialProtocolHandledSchems indexOfObject:theScheme] != NSNotFound; 
}

+(NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request {
    return [SDURLCache canonicalRequestForRequest:request];
}

@end



/*
 * Implementation GOnSecureConnectionFactory 
 */
GOnSecureConnectionFactory::GOnSecureConnectionFactory(const Giritech::Appl::ApplClientDirectConnectionFactory::APtr& applClientDirectConnectionFactory) 
  : iApplClientDirectConnectionFactory(applClientDirectConnectionFactory) {
      iApplClientDirectConnectionFactory->set_eventhandler(this);
      cookie_store_ = Giritech::HttpClient::HttpCookieStore::create(applClientDirectConnectionFactory->get_io_service(), applClientDirectConnectionFactory->get_checkpoint_handler());
      cookie_store_->set_eventhandler(this);
}

GOnSecureConnectionFactory::~GOnSecureConnectionFactory(void) {
    iApplClientDirectConnectionFactory->reset_eventhandler();
    cookie_store_->reset_eventhandler();
}

void GOnSecureConnectionFactory::create_connection( id<DirectHttpProxyConnectorProtocol> specialProtocolDelegate) {
    iPendingConnects.insert([specialProtocolDelegate retain]);
    iApplClientDirectConnectionFactory->create_connection();
}
    
void GOnSecureConnectionFactory::direct_connection_factory_connection_ready(const Giritech::Communication::TunnelendpointDirect::APtr& connection) {
    std::set< id<DirectHttpProxyConnectorProtocol> >::iterator i(iPendingConnects.begin());
    if(i != iPendingConnects.end()) {
        id<DirectHttpProxyConnectorProtocol> pendingConnect = *i;
        iPendingConnects.erase(i);
        
        DirectHttpProxyConnector::APtr directHttpProxyConnector(DirectHttpProxyConnector::create(connection, cookie_store_));
        [pendingConnect createConnectionResponse:directHttpProxyConnector];
        [pendingConnect release];
    }
}

void GOnSecureConnectionFactory::direct_connection_factory_connection_failed(const std::string& errorMessage) {
}
    
Giritech::Utility::CheckpointHandler::APtr GOnSecureConnectionFactory::get_checkpoint_handler(void) {
    return iApplClientDirectConnectionFactory->get_checkpoint_handler();
}

Giritech::Utility::Mutex::APtr GOnSecureConnectionFactory::get_mutex(void) {
    return iApplClientDirectConnectionFactory->get_mutex();
}

void GOnSecureConnectionFactory::cookie_added(const Giritech::HttpClient::HttpSetCookieValue& cookie_value) {

 NSDictionary* cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                      @"1", NSHTTPCookieVersion, 
                                      [NSString stringWithUTF8String:(cookie_value.key).c_str()], NSHTTPCookieName,
                                      [NSString stringWithUTF8String:(cookie_value.value).c_str()], NSHTTPCookieValue,
                                      [NSString stringWithUTF8String:(cookie_value.path).c_str()], NSHTTPCookiePath,
                                      [NSString stringWithUTF8String:(cookie_value.domain).c_str()], NSHTTPCookieDomain,
                                      nil];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:[NSHTTPCookie cookieWithProperties:cookieProperties]];
}

void GOnSecureConnectionFactory::cookie_removed(const Giritech::HttpClient::HttpSetCookieValue& cookie_value) {
    
}

GOnSecureConnectionFactory::APtr GOnSecureConnectionFactory::create(const Giritech::Appl::ApplClientDirectConnectionFactory::APtr& applClientDirectConnectionFactory) {
    return APtr(new GOnSecureConnectionFactory(applClientDirectConnectionFactory));
}



/*
 * Implementation DirectHttpProxyConnector 
 */
DirectHttpProxyConnector::DirectHttpProxyConnector(const Giritech::Communication::TunnelendpointDirect::APtr& tunnelendpointDirect, const Giritech::HttpClient::HttpCookieStore::APtr& cookie_store) 
: iSpecialProtocol(nil) {
    iProgress = false;
    iWasRedirected = false;
    iSessionId =  tunnelendpointDirect->get_unique_session_id();
    
    Giritech::Communication::TunnelendpointDirectHttpProxy::APtr http_client_com(Giritech::Communication::TunnelendpointDirectHttpProxy::create([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], tunnelendpointDirect));
    iMutex = tunnelendpointDirect->get_mutex();
    http_client_com->enable_http_proxification();

    iHttpClient = Giritech::HttpClient::HttpClient::create(tunnelendpointDirect->get_io_service(), tunnelendpointDirect->get_checkpoint_handler(), http_client_com, cookie_store);
    iHttpClient->set_eventhandler(this);
    iHttpClient->set_mutex(iMutex);
}

DirectHttpProxyConnector::~DirectHttpProxyConnector(void) {
    reset();
}

void DirectHttpProxyConnector::reset(void) {
    if(iTunnelendpointDirectHttpProxy.get()) {
        iTunnelendpointDirectHttpProxy->reset_eventhandler();
    }
    iTunnelendpointDirectHttpProxy.reset();
    
    if(iHttpClient.get()){
        iHttpClient->reset_eventhandler();
    }
}

DirectHttpProxyConnector::APtr DirectHttpProxyConnector::create(const Giritech::Communication::TunnelendpointDirect::APtr& tunnelendpointDirect, const Giritech::HttpClient::HttpCookieStore::APtr& cookie_store) {
    return APtr(new DirectHttpProxyConnector(tunnelendpointDirect, cookie_store));
}

void DirectHttpProxyConnector::startRequest(NSURLProtocol* specialProtocol) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(),iMutex, "DirectHttpProxyConnector::startRequest");
    
    iSpecialProtocol = specialProtocol;
    NSURLRequest* request = [iSpecialProtocol request];
    iHttpRequestURL = [request URL];
    
    NSString* httpMethod = [request HTTPMethod];
    Giritech::HttpClient::HttpRequest::APtr httpRequest;
    if( [httpMethod isEqualToString:@"GET"] ) {
        httpRequest = Giritech::HttpClient::HttpRequest::create_get([[iHttpRequestURL absoluteString] UTF8String]);
    }
    else if( [httpMethod isEqualToString:@"POST"] ) {
        httpRequest = Giritech::HttpClient::HttpRequest::create_post([[iHttpRequestURL absoluteString] UTF8String]);
        
        NSString* httpBody = [[[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding] autorelease];
        httpRequest->set_body([httpBody UTF8String]);
    }    
    else {
        NSString* errorMessage = [NSString stringWithFormat:@"DirectHttpProxyConnector::startRequest.unsupported_method:%@", httpMethod];
        NSLog(@"%@", errorMessage);
        reset();
        return;
    }
    
    // Adding headers
    NSDictionary* httpRequestHeaderFields = [request allHTTPHeaderFields];
    NSEnumerator* enumerator = [httpRequestHeaderFields keyEnumerator];
    NSString* key;
    while ((key = [enumerator nextObject])) {
        NSString* value = [httpRequestHeaderFields objectForKey:key];
        httpRequest->add_header(std::string([key UTF8String]), std::string([value UTF8String]), false);
    }

    if ([iSpecialProtocol isDMENamespace]) {
        httpRequest->add_header("DMESecureBrowser-Session", iSessionId, true);
    }

    // Enable progress
    iProgress = [[GOnSecureBrowserProgressHandler getShared] compareWithUrl:iHttpRequestURL];
    if(iProgress) {
        [[GOnSecureBrowserProgressHandler getShared] reset];
    }
    if(!iProgress) {
        iProgress = [[GOnSecureBrowserProgressHandler getShared] compareWithHeaders:httpRequestHeaderFields];
    }                
    if(iProgress) {
        [[GOnSecureBrowserProgressHandler getShared] reportBegin];
    }

    // Execute request
    iHttpClient->execute_request(httpRequest);
}

void DirectHttpProxyConnector::cancelRequest(void) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), iMutex, "DirectHttpProxyConnector::cancelRequest");
    iHttpClient->cancel_request();
}

void DirectHttpProxyConnector::http_client_response_header(const std::string& header_version, const unsigned long httpResponseStatusCode, const std::vector< std::pair<std::string, std::string> >& headers) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), iMutex, "DirectHttpProxyConnector::http_client_response_header");

    @autoreleasepool {
        NSString* httpResponseVersion = [NSString stringWithUTF8String:header_version.c_str()];
        
        NSMutableDictionary* httpResponseHeaderFields = [[[NSMutableDictionary alloc] init] autorelease];
        
        std::vector< std::pair<std::string, std::string> >::const_iterator i(headers.begin());
        while(i != headers.end()) {
            NSString* value = [[NSString stringWithUTF8String:(i->second).c_str()] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [httpResponseHeaderFields setObject:value forKey:[NSString stringWithUTF8String:(i->first).c_str()]];
            i++;
        } 
        NSHTTPURLResponse* response = [[[NSHTTPURLResponse alloc] initWithURL:iHttpRequestURL statusCode:httpResponseStatusCode HTTPVersion:httpResponseVersion headerFields:httpResponseHeaderFields] autorelease];
        
        if(httpResponseStatusCode == 301 || httpResponseStatusCode == 302 || httpResponseStatusCode == 303) {
            NSURL* redirectToURL;
            NSString* headerLocation = [httpResponseHeaderFields objectForKey:@"Location"];
            if(headerLocation) { 
                redirectToURL = [NSURL URLWithString:headerLocation relativeToURL:[response URL]];
                NSURLRequest* redirectedRequest = [[[NSURLRequest alloc] initWithURL:redirectToURL] autorelease];
                [[iSpecialProtocol client] URLProtocol:iSpecialProtocol wasRedirectedToRequest:redirectedRequest redirectResponse:response];
                iWasRedirected = true;
                return;
            }
        }
        
        // Can disable progress if cunked transfer encoding
        if(iProgress) {
            iProgress = [[GOnSecureBrowserProgressHandler getShared] reportPartBeginWirhHeaders:httpResponseHeaderFields];
        }
        
        // Report not-html-content-type to progress handler
        if([[GOnSecureBrowserProgressHandler getShared] compareWithUrl:iHttpRequestURL]) {
            NSString* headerContentType = [httpResponseHeaderFields objectForKey:@"Content-Type"];
            NSRange range = [headerContentType rangeOfString:@"html" options:NSCaseInsensitiveSearch];
            if(range.location == NSNotFound) {
                [[GOnSecureBrowserProgressHandler getShared] reportContentIsNotHTML];
            }
        }

        // Report error (to enable navigation gui)
        if(httpResponseStatusCode != 200 && httpResponseStatusCode != 304) {
            [[GOnSecureBrowserProgressHandler getShared] reportErrorLoadingContent];
        }
        
        NSURLCacheStoragePolicy cacheStoragePolicy = NSURLCacheStorageNotAllowed;
        if([[GOnSecureBrowserProgressHandler getShared] isCacheEnabled]) {
            cacheStoragePolicy = NSURLCacheStorageAllowed;
        };
        [[iSpecialProtocol client] URLProtocol:iSpecialProtocol didReceiveResponse:response cacheStoragePolicy:cacheStoragePolicy];    
    }

}

void DirectHttpProxyConnector::http_client_response_body_data(Giritech::Utility::DataBufferManaged::APtr& body_data) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), iMutex, "DirectHttpProxyConnector::http_client_response_body_data");    
    @autoreleasepool {
        if(iWasRedirected) {
            return;
        }
        
        NSData* httpResponseBodyData = [NSData dataWithBytes:(void*)body_data->data() length:body_data->getSize()];
        if(iProgress) {
            [[GOnSecureBrowserProgressHandler getShared] reportPartDone:[httpResponseBodyData length]];
        }
        [[iSpecialProtocol client] URLProtocol:iSpecialProtocol didLoadData:httpResponseBodyData];
    }
}

void DirectHttpProxyConnector::http_client_response_done(void) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), iMutex, "DirectHttpProxyConnector::http_client_response_done");
    @autoreleasepool {
        if(iProgress) {
            [[GOnSecureBrowserProgressHandler getShared] reportDone];
        }
        
        if(iWasRedirected) {
            return;
        }

        [[iSpecialProtocol client] URLProtocolDidFinishLoading:iSpecialProtocol];
        [iSpecialProtocol loadingDone];
        reset();
    }
}

void DirectHttpProxyConnector::http_client_response_error(void) {
    Giritech::Utility::MutexScopeLock mutex_lock([[ACAPICheckpointHandler shared] getCheckpointHanderRaw], Giritech::Appl::Attr_iDevice(), iMutex, "DirectHttpProxyConnector::http_client_response_error");

    @autoreleasepool {
        [[GOnSecureBrowserProgressHandler getShared] reportErrorLoadingContent];

        if(iProgress) {
            [[GOnSecureBrowserProgressHandler getShared] reportDone];
        }

        if(iWasRedirected) {
            return;
        }
        NSError* error = [NSError errorWithDomain:@"" code:502 userInfo:nil];
        [[iSpecialProtocol client] URLProtocol:iSpecialProtocol didFailWithError:error];
        [iSpecialProtocol loadingDone];
        reset();
    }
}




