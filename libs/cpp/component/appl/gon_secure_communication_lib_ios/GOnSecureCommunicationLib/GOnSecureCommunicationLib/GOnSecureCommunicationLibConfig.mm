//
//  GOnSecureCommunicationLibConfig.mm
//  GOnClient
//
//  Created by gbuilder on 12/10/10.
//  Copyright 2010 Giritech. All rights reserved.
//

#import "GOnSecureCommunicationLibConfig.h"
#import "SecureKeyChain.h"


@implementation GOnSecureCommunicationLibConfig


#pragma mark -
#pragma mark Secure storage functionality
static NSString* authKeysSerial = @"gon_auth_keys_serial";
static NSString* authKeysVersion = @"gon_auth_keys_version";
static NSString* authKeysPublicKey = @"gon_auth_keys_publickey";
static NSString* authKeysPrivateKey = @"gon_auth_keys_privatekey";
static NSString* connectInfoServersKey = @"gon_connect_info_servers";
static NSString* connectInfoKnownsecretKey = @"gon_connect_info_knownsecret";
static NSString* sessionPublicKeyKey = @"gon_encryption_session_public_key";
static NSString* sessionPrivateKeyKey = @"gon_encryption_session_private_key";


-(void) clearSecure {
    [SecureKeyChain deleteStringForKey:connectInfoServersKey];
    [SecureKeyChain deleteStringForKey:connectInfoKnownsecretKey];
    [SecureKeyChain deleteStringForKey:authKeysPublicKey];
    [SecureKeyChain deleteStringForKey:authKeysPrivateKey];
    [SecureKeyChain deleteStringForKey:authKeysVersion];
    [SecureKeyChain deleteStringForKey:authKeysSerial];
    [SecureKeyChain deleteStringForKey:sessionPublicKeyKey];
    [SecureKeyChain deleteStringForKey:sessionPrivateKeyKey];
}

- (void) setSerial:(NSString*)serial {
    [SecureKeyChain saveString:serial forKey:authKeysSerial];
}

- (NSString*) generateSerial {
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef serialRef = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);

    NSString* serial = [(NSString *)serialRef autorelease];
    [SecureKeyChain saveString:serial forKey:authKeysSerial];
    return serial;
}

- (NSString*) getSerial {
    return [SecureKeyChain getStringForKey:authKeysSerial];
}

- (NSString*) getSecureVersion {
    return [SecureKeyChain getStringForKey:authKeysVersion];
}

- (void) setSecureVersion:(NSString*)version {
    [SecureKeyChain saveString:version forKey:authKeysVersion];
}

- (void) setKeysWithPublicKey:(NSString*)publicKey privateKey:(NSString*)privateKey {
    [SecureKeyChain saveString:publicKey forKey:authKeysPublicKey];
    [SecureKeyChain saveString:privateKey forKey:authKeysPrivateKey];
}

- (NSString*) getPublicKey {
    return [SecureKeyChain getStringForKey:authKeysPublicKey];
}

- (NSString*) getPrivateKey {
    return [SecureKeyChain getStringForKey:authKeysPrivateKey];
}

- (void) setConnectInfo:(NSString*)servers withKnownsecret:(NSString*)knownsecret {
    [SecureKeyChain saveString:servers forKey:connectInfoServersKey];
    [SecureKeyChain saveString:knownsecret forKey:connectInfoKnownsecretKey];
}	 	 

- (void) clearConnectInfo {
    [SecureKeyChain deleteStringForKey:connectInfoServersKey];
    [SecureKeyChain deleteStringForKey:connectInfoKnownsecretKey];
}

- (NSString*) getServers {
    return [SecureKeyChain getStringForKey:connectInfoServersKey];
}

- (NSString*) getKnownsecret {
    return [SecureKeyChain getStringForKey:connectInfoKnownsecretKey];
}

- (BOOL) hasConnectInfo {
	NSString* servers = [self getServers];
	NSString* knownsecret = [self getKnownsecret];
	return ![servers isEqualToString:@""] && ![knownsecret isEqualToString:@""];
}

- (void) setConnectInfo:(NSString*)servers {
    [SecureKeyChain saveString:servers forKey:connectInfoServersKey];
}

- (void) setBackgroundSettings:(BOOL)closeWhenEnteringBackground {
    // TODO: Missing implementation
}

-(void) setSessionPublicKey:(NSString*)sessionPublicKey {
    [SecureKeyChain saveString:sessionPublicKey forKey:sessionPublicKeyKey];
}

-(void) setSessionPrivateKey:(NSString*)sessionPrivateKey {
    [SecureKeyChain saveString:sessionPrivateKey forKey:sessionPrivateKeyKey];    
}

-(NSString*) getSessionPublicKey {
    return [SecureKeyChain getStringForKey:sessionPublicKeyKey];
}

-(NSString*) getSessionPrivateKey {
    return [SecureKeyChain getStringForKey:sessionPrivateKeyKey];
}

-(NSString*) getDMEValueUsername {
    return @"dme_value_username_not_set";
}

-(NSString*) getDMEValuePassword {
    return @"dme_value_password_not_set";    
}

-(NSString*) getDMEValueTerminalId {
    return @"dme_value_terminal_id_not_set";
}

-(NSString*) getDMEValueDeviceSignature {
    return @"dme_value_device_signature_not_set";
}

-(NSString*) getDMEValueDeviceTime {
    return @"dme_value_device_time_not_set";
}

-(NSURL*) getDMEURL {
    return nil;
}

@end
