//
//  ACAPICheckpointHandler.mm
//  GOnClient
//
//  Created by gbuilder on 01/12/10.
//  Copyright 2010 Giritech. All rights reserved.
//

#import "ACAPICheckpointHandler.h"
#import "SynthesizeSingleton.h"

#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>

using namespace Giritech::Utility;
using namespace Giritech::Appl;


//
// Private methods
// 
@interface ACAPICheckpointHandler ()
- (void) rotateLogfile;
@end




@implementation ACAPICheckpointHandler

SYNTHESIZE_SINGLETON_FOR_CLASS(ACAPICheckpointHandler);

-(id) init {
	if (self == nil) {
		return (nil);
	}

    logToFile_ = NO;
    logToFileName_ = @"gon_secure.log";
    logLevel_ = @"info";
    
	CheckpointDestinationHandler::APtr checkpointDestinationHandler;
	if (logToFile_) {
		[self rotateLogfile];
		checkpointDestinationHandler = CheckpointDestinationHandlerFile::create([logToFileName_ UTF8String], false, false);
	}
	else {
		checkpointDestinationHandler = CheckpointDestinationHandlerCout::create();
	}

	CheckpointFilter::APtr checkpointFilter;
	if ([logLevel_ isEqualToString:@"debug"]) {
		checkpointFilter = CheckpointFilter_true::create();
	}
	else {
		checkpointFilter = CheckpointFilter_or::create(CheckpointFilter_accept_if_attr_exist::create(CpAttr_error()), CheckpointFilter_accept_if_attr_exist::create(CpAttr_critical()));
	}
	CheckpointOutputHandler::APtr checkpointOutputHandler(CheckpointOutputHandlerTEXT::create(checkpointDestinationHandler));	

	checkpointHandler_ = CheckpointHandler::create(checkpointFilter, checkpointOutputHandler);
	return self;
}

-(Giritech::Utility::CheckpointHandler::APtr) getCheckpointHanderRaw {
	return checkpointHandler_;
}


-(void) cpDebug:(NSString*)cpId message:(NSString*) message {
	[self cpDebug:cpId attrId:@"message" attr:message];
}

-(void) cpDebug:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr {
	Checkpoint cp(*checkpointHandler_, [cpId UTF8String], Attr_iDevice(), CpAttr_debug(), CpAttr([attrId UTF8String], [attr UTF8String]));
}

-(void) cpInfo:(NSString*)cpId message:(NSString*) message {
	[self cpInfo:cpId attrId:@"message" attr:message];
}

-(void) cpInfo:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr {
	Checkpoint cp(*checkpointHandler_, [cpId UTF8String], Attr_iDevice(), CpAttr_info(), CpAttr([attrId UTF8String], [attr UTF8String]));
}

-(void) cpInfos:(NSString*)messageId message:(NSString*) messages {
	NSArray* lines = [messages componentsSeparatedByString: @"\n"];
	for (NSString* line in lines) {
		[self cpInfo:messageId message:line];
	}
}

-(void) cpWarning:(NSString*)cpId message:(NSString*) message {
	[self cpWarning:cpId attrId:@"message" attr:message];
}

-(void) cpWarning:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr {
	Checkpoint cp(*checkpointHandler_, [cpId UTF8String], Attr_iDevice(), CpAttr_warning(), CpAttr([attrId UTF8String], [attr UTF8String]));
}

-(void) cpError:(NSString*)cpId message:(NSString*) message {
	[self cpError:cpId attrId:@"message" attr:message];
}

-(void) cpError:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr {
	Checkpoint cp(*checkpointHandler_, [cpId UTF8String], Attr_iDevice(), CpAttr_error(), CpAttr([attrId UTF8String], [attr UTF8String]));
}

-(void) cpCritical:(NSString*)cpId message:(NSString*) message {
	[self cpCritical:cpId attrId:@"message" attr:message];
}

-(void) cpCritical:(NSString*)cpId attrId:(NSString*)attrId attr:(NSString*)attr {
	Checkpoint cp(*checkpointHandler_, [cpId UTF8String], Attr_iDevice(), CpAttr_critical(), CpAttr([attrId UTF8String], [attr UTF8String]));
}


-(void) rotateLogfile {
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:logToFileName_] == YES) {
		NSString* rotateFileName; 
		int idx = 0;
		while(idx < 50) {
			rotateFileName = [NSString stringWithFormat:@"%@.%3i", logToFileName_, idx];
			if ([fileManager fileExistsAtPath:rotateFileName] == NO) {
				break;
			}
			idx++;
		}
		[fileManager copyItemAtPath:logToFileName_ toPath:rotateFileName error:nil];
	}
}

@end
