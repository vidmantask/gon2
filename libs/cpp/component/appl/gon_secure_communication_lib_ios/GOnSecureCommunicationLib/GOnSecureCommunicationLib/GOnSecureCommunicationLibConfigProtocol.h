//
//  GOnSecureCommunicationLibConfigProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureCommunicationLibConfigProtocol <NSObject>


/*
 * General info:
 *
 * All getters should return @"" if value is not available.
 *
 */


/*
 * Unique serial, used by G/On device management.
 */
- (NSString*) generateSerial;
- (NSString*) getSerial;


/*
 * Getters and setters for privat/public keypair, used by G/On secure communication
 */
- (void) setKeysWithPublicKey:(NSString*)publicKey privateKey:(NSString*)privateKey;
- (NSString*) getPublicKey;
- (NSString*) getPrivateKey;

/*
 * Getters and setters for connect info, used by G/On secure communication
 */
- (void) clearConnectInfo;
- (BOOL) hasConnectInfo;
- (void) setConnectInfo:(NSString*)servers withKnownsecret:(NSString*)knownsecret;
- (void) setConnectInfo:(NSString*)servers;

- (NSString*) getServers;
- (NSString*) getKnownsecret;


/*
 * Getters and setters session public/private keypair, used by G/On secure communication
 */
-(void) setSessionPublicKey:(NSString*)key;
-(void) setSessionPrivateKey:(NSString*)iv;

-(NSString*) getSessionPublicKey;
-(NSString*) getSessionPrivateKey;


/*
 * Getters for DME values
 */
-(NSString*) getDMEValueUsername;
-(NSString*) getDMEValuePassword;
-(NSString*) getDMEValueTerminalId;
-(NSString*) getDMEValueDeviceSignature;
-(NSString*) getDMEValueDeviceTime;
-(NSURL*)    getDMEURL;


/*
 * Getters and setters for misc settings
 */
- (void) setBackgroundSettings:(BOOL)closeWhenEnteringBackground;


@end
