//
//  ACAPI.h
//  GOnClient
//
//  Created by gbuilder on 10/10/10.
//  Copyright 2010 Giritech. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include <string>

#import "ACAPIReachability.h"
#import "GOnSecureCommunicationLibConfig.h"
#import "GOnSecureCommunicationLibProtocol.h"


#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>
#include <component/appl/gon_client/APPL_Client.hxx>
#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>


//
// Helper classe for argument wrapping
//
/*
@interface ACAPIDialogShowLicenseInfoArgs : NSObject {
	NSString* header;
    NSString* message;
    BOOL valid;
}
- (id) initWithArgs:(NSString*)header message:(NSString*)message valid:(BOOL)valid;
@end

@interface ACAPIDialogShowMenuArgs : NSObject {
	std::vector<Giritech::Appl::ApplClientMenuItem> menuItems;
	std::map<std::string, Giritech::Appl::ApplClientTagProperty> tagProperties;
}
-(id) initWithArgs:(const std::vector<Giritech::Appl::ApplClientMenuItem>&)menuItems tagProperties:(const std::map<std::string, Giritech::Appl::ApplClientTagProperty>&)tagProperties;
@end

@interface ACAPIDialogImageArgs : NSObject {
	NSString* requestId;
	NSString* imageId;
	UIImage* image;
}
-(id) initWithArgs:(NSString*)requestId imageId:(NSString*)imageId image:(UIImage*)image;
@end

@interface ACAPIInfoArgs : NSObject {
	NSString* header;
    NSString* message;
}
- (id) initWithArgs:(NSString*)header message:(NSString*)message;
@end

@interface ACAPIEndpointEnrollmentTokenStatusArgs : NSObject {
	BOOL canDeploy;
	BOOL authActivated;
	BOOL assignedToCurrentUser;
    NSString* statusText;
}
- (id) initWithArgs:(BOOL)canDeploy authActivated:(BOOL)authActivated assignedToCurrentUser:(BOOL)assignedToCurrentUser statusText:(NSString*)statusText;
@end

@interface ACAPIEndpointEnrollmentEnrollTokenResponseArgs : NSObject {
	BOOL rc;
    NSString* message;
}
- (id) initWithArgs:(BOOL)rc message:(NSString*)message;
@end

@interface ACAPIEndpointGetEnrollmentStatusResponseArgs : NSObject {
	BOOL enrollError;
	BOOL enrollDone;
    NSString* message;
}
- (id) initWithArgs:(BOOL)enrollError enrollDone:(BOOL)enrollDone message:(NSString*)message;
@end

@interface ACAPITrafficLaunchStatusChangedArgs : NSObject {
    NSString* launchId;
	Giritech::Appl::ApplClientEventhandler::State state;
    NSString* message;
}
- (id) initWithArgs:(NSString*)launchId state:(Giritech::Appl::ApplClientEventhandler::State)state message:(NSString*)message;
@end

@interface ACAPITrafficLaunchCommandArgs : NSObject {
	NSString* launchId;
	Giritech::Appl::ApplClientCommand command;
}
-(id) initWithArgs:(NSString*)launchId command:(const Giritech::Appl::ApplClientCommand&)command;
@end
*/

//
// Protocol for ACAPI 
//
/*
@protocol ACAPIProtocol <NSObject>

- (void) acAPIConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle;
- (void) acAPIConnectStateConnectingFailedWithMessage:(NSString*) message;
- (void) acAPIConnectStateConnected;
- (void) acAPIConnectStateClosing;
- (void) acAPIConnectStateClosed;

- (void) acAPIUserRequestLoginWithMessage:(NSString*)message;

- (void) acAPIDialogShowLicenseInfo:(ACAPIDialogShowLicenseInfoArgs*)arg;
- (void) acAPIDialogMenuUpdate;
- (void) acAPIDialogShowMenu:(ACAPIDialogShowMenuArgs*)args;
- (void) acAPIDialogImageFound:(ACAPIDialogImageArgs*)args;
- (void) acAPIDialogImageNotFound:(ACAPIDialogImageArgs*)args;

- (void) acAPIInfo:(ACAPIInfoArgs*)args;
- (void) acAPIInfoError:(ACAPIInfoArgs*)args;
- (void) acAPIInfoWarning:(ACAPIInfoArgs*)args;

- (void) acAPIEndpointEnrollmentTokenStatus:(ACAPIEndpointEnrollmentTokenStatusArgs*)args;
- (void) acAPIEndpointEnrollmentEnrollTokenResponse:(ACAPIEndpointEnrollmentEnrollTokenResponseArgs*)args;
- (void) acAPIEndpointGetEnrollmentStatusResponse:(ACAPIEndpointGetEnrollmentStatusResponseArgs*)args;

- (void) acAPITraffficLaunchStatusChanged:(ACAPITrafficLaunchStatusChangedArgs*)args;
- (void) acAPITraffficLaunchCommand:(ACAPITrafficLaunchCommandArgs*)args;
- (void) acAPITraffficLaunchCommandTerminate:(NSString*) launchId;

- (void) acAPIConnectInfoUpdated;

- (void) acAPIKeyStoreReset;
- (void) acAPIVersionError:(NSString*)serverVersion;
@end
*/

@interface ACAPIMainThread : NSObject <GOnSecureCommunicationLibProtocol> {
	id<GOnSecureCommunicationLibProtocol> delegate_;
}

-(void) setDelegate:(id<GOnSecureCommunicationLibProtocol>)delegate;

@end



class ACAPI : public Giritech::Appl::ApplClientEventhandler {
public:
    typedef boost::shared_ptr<ACAPI> APtr;
	
    void app_client_event_connecting(const std::string& connection_title);
    void app_client_event_connecting_failed(const std::string& message);
    void app_client_event_connected(void);
    void app_client_event_closing(void);
    void app_client_event_closed(void);
	void app_client_user_request_login(const std::string& msg, const bool change_password_enabled);
	void app_client_auth_generate_keys_response(const std::string& private_key, const std::string& public_key);	
	void app_client_auth_request_serial(void);
    void app_client_auth_request_challenge_response(const std::string& challenge);
	void app_client_auth_failed(void);

    void app_client_dialog_show_license_info(const std::string& header, const std::string& message, const bool valid);
	void app_client_dialog_request_inform_on_first_access(const std::string&  message, const bool close_on_cancel);
    void app_client_dialog_menu_updated(void);
	void app_client_dialog_show_menu(const std::vector<Giritech::Appl::ApplClientMenuItem>& menu_items, const std::map<std::string, Giritech::Appl::ApplClientTagProperty>& tag_properties);
    void app_client_dialog_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data);
    void app_client_dialog_image_not_found(const std::string& request_id, const std::string& image_id);
	
	void app_client_info(const std::string& header, const std::string& message);
    void app_client_warning(const std::string& header, const std::string& message);
    void app_client_error(const std::string& header, const std::string& message);
    void app_client_endpoint_request_endpoint_info(void);
    void app_client_endpoint_enrollment_request_token(void);
    void app_client_endpoint_enrollment_token_status(const bool can_deploy, const bool auth_activated, const bool assigned_to_current_user, const std::string& status_text);
    void app_client_endpoint_enrollment_enroll_token_response(const bool rc, const std::string& msg);
    void app_client_endpoint_get_enrollment_status_response(const bool enroll_error, const bool enroll_done, const std::string& enroll_message);
	
	void app_client_traffic_launch_status_changed(const std::string& launch_id, const State state, const std::string& status_message);
    void app_client_traffic_launch_command(const std::string& launch_id, const Giritech::Appl::ApplClientCommand& launch_command);
    void app_client_traffic_launch_command_terminate(const std::string& launch_id);
	
	void app_client_tag_get_platform(void);

    void app_client_set_background_settings(const bool close_when_entering_background);
    void app_client_cpm_update_connection_info(const std::string& servers);

    bool app_client_is_network_available(void);
    
    void app_client_version_error(const std::string& error_message);

    void app_client_key_store_reset(void);
    void app_client_key_store_session_key_pair_set(Giritech::Utility::DataBufferManaged::APtr& sessionKeyPublic, Giritech::Utility::DataBufferManaged::APtr& sessionKeyPrivate);
    void app_client_key_store_session_key_pair_get(Giritech::Utility::DataBufferManaged::APtr& sessionKeyPublic, Giritech::Utility::DataBufferManaged::APtr& sessionKeyPrivate);
    
    void app_client_dme_get_dme_value(void);
    
    
	Giritech::Appl::ApplClient::APtr get(void);

    
	void setDelegate(id<GOnSecureCommunicationLibProtocol> delegate);
	
    void actionConnect(void);
	
	static APtr create(GOnSecureCommunicationLibConfig* config, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);
	
private:
    ACAPI(GOnSecureCommunicationLibConfig* config, const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler);

	void app_client_event_connected_mainthread(void);

    ACAPIMainThread* delegate_;
    GOnSecureCommunicationLibConfig* config_;
    
    ACAPIReachability* acAPIReachability_;

    Giritech::Appl::ApplClient::APtr appl_client_;
};
