//
//  GOnSecureCommunicationLibConfig.h
//  GOnClient
//
//  Created by gbuilder on 12/10/10.
//  Copyright 2010 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GOnSecureCommunicationLibConfigProtocol.h"

@interface GOnSecureCommunicationLibConfig : NSObject <GOnSecureCommunicationLibConfigProtocol> {
}

-(void) clearSecure;

@end
