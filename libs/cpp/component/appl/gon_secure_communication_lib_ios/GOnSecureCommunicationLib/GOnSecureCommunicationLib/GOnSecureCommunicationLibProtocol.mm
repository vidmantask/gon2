//
//  GOnSecureCommunicationLibProtocol.mm
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 22/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//
#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>

#import "GOnSecureCommunicationLibProtocol.h"
#import "GOnSecureBrowserSpecialProtocol.h"

#import "GOnSecureBrowserProgressHandler.h"


//
// Implementation of helper classes for GOnSecureCommunicationLibProtocol
//
@interface GOnSCLMenuItem ()
-(id) initWithArgs:(Giritech::Appl::ApplClientMenuItem&) menuItem;
@end

@implementation GOnSCLMenuItem
@synthesize id;
@synthesize title;
@synthesize iconId;
@synthesize launchId;
@synthesize disabled;
@synthesize tagShow;
@synthesize tagTopx;
@synthesize tooltip;
@synthesize tagEnabled;
@synthesize tagGupdate;
@synthesize tagAppboxDefault;
@synthesize tagAppboxLink;
@synthesize launchState;
@synthesize launchStateMessage;

-(id) initWithArgs:(Giritech::Appl::ApplClientMenuItem&) menuItem {
	if (self) {
		title = [[NSString stringWithUTF8String:menuItem.title.c_str()] retain];
		launchId = [[NSString stringWithUTF8String:menuItem.launch_id.c_str()] retain];
		iconId = [[NSString stringWithUTF8String:menuItem.icon_id.c_str()] retain];
		tooltip = [[NSString stringWithUTF8String:menuItem.tooltip.c_str()] retain];
		disabled = menuItem.disabled;
		tagShow = menuItem.tag_show;
		tagEnabled = menuItem.tag_enabled;
		tagTopx = menuItem.tag_topx;
		tagGupdate = menuItem.tag_gupdate;
        tagAppboxDefault = menuItem.tag_appbox_default; 
        tagAppboxLink = menuItem.tag_appbox_link; 
        self.launchStateMessage = nil;
        self.launchState = kLaunchStateNotLaunched;
	}
	return self;
}
-(void) setLaunchState:(GOnSCLTrafficLaunchState)launchStateArg launchMessage:(NSString*)launchMessageArg {
    self.launchState = launchStateArg;
    self.launchStateMessage = launchMessageArg;
}

-(void)dealloc {
	[title release];
	[launchId release];
	[iconId release];
	[tooltip release];
    title = nil;
    launchId = nil;
    iconId = nil;
    tooltip = nil;
    self.launchStateMessage = nil;
	[super dealloc];
}
@end


@interface GOnSCLDialogShowLicenseInfoArgs ()
- (id) initWithArgs:(NSString*)headerArg message:(NSString*)messageArg valid:(BOOL)validArg;
@end

@implementation GOnSCLDialogShowLicenseInfoArgs
- (id) initWithArgs:(NSString*)headerArg message:(NSString*)messageArg valid:(BOOL)validArg {
	if (self) {
		header = [headerArg retain];
		message = [messageArg retain];
		valid = validArg;
	}
	return self;
}
-(void)dealloc {
	[header release];
	[message release];
    header = nil;
    message = nil;
	[super dealloc];
}
@end


@interface GOnSCLDialogShowMenuArgs ()
-(id) initWithArgs:(NSMutableArray*)menuItemsArg;
@end

@implementation GOnSCLDialogShowMenuArgs
@synthesize menuItems;
-(id) initWithArgs:(NSMutableArray*)menuItemsArg {
	if (self) {
        menuItems = [menuItemsArg retain];
 	}
	return self;
}
-(void)dealloc {
    [menuItems release];
	[super dealloc];
}
@end



@interface GOnSCLDialogImageArgs ()
-(id) initWithArgs:(NSString*)requestIdArg imageId:(NSString*)imageIdArg image:(UIImage*)imageArg;
@end

@implementation GOnSCLDialogImageArgs
@synthesize image;
@synthesize imageId;
@synthesize requestId;
-(id) initWithArgs:(NSString*)requestIdArg imageId:(NSString*)imageIdArg image:(UIImage*)imageArg {
	if (self) {
		requestId = [requestIdArg retain];
		imageId = [imageIdArg retain];
		image = [imageArg retain];
	}
	return self;
}

-(void)dealloc {
	[requestId release];
	[imageId release];
	[image release];
    requestId = nil;
    imageId = nil;
    image = nil;
	[super dealloc];
}
@end


@interface GOnSCLInfoArgs () 
- (id) initWithArgs:(NSString*)headerArg message:(NSString*)messageArg;
@end

@implementation GOnSCLInfoArgs
@synthesize header;
@synthesize message;

- (id) initWithArgs:(NSString*)headerArg message:(NSString*)messageArg {
	if (self) {
		header = [headerArg retain];
		message = [messageArg retain];
	}
	return self;
}
-(void)dealloc {
	[header release];
	[message release];
    header = nil;
    message = nil;
	[super dealloc];
}
@end


@interface GOnSCLEnrollRequestArgs ()
- (id) initWithArgs:(BOOL)canDeployArg authActivated:(BOOL)authActivatedArg assignedToCurrentUser:(BOOL)assignedToCurrentUserArg statusText:(NSString*)statusTextArg;
@end

@implementation GOnSCLEnrollRequestArgs
- (id) initWithArgs:(BOOL)canDeployArg authActivated:(BOOL)authActivatedArg assignedToCurrentUser:(BOOL)assignedToCurrentUserArg statusText:(NSString*)statusTextArg {
	if (self) {
        canDeploy = canDeployArg;;
        authActivated = authActivatedArg;
        assignedToCurrentUser = assignedToCurrentUserArg;
		statusText = [statusTextArg retain];
	}
	return self;
}
-(void)dealloc {
	[statusText release];
    statusText = nil;
	[super dealloc];
}
@end

@implementation GOnSCLEnrollResponseArgs
- (id) initWithArgs:(BOOL)rcArg message:(NSString*)messageArg {
	if (self) {
        rc = rcArg;
		message = [messageArg retain];
	}
	return self;
}
-(void)dealloc {
	[message release];
    message = nil;
	[super dealloc];
}
@end

@implementation GOnSCLEnrollGetStatusResponseArgs
- (id) initWithArgs:(BOOL)enrollErrorArg enrollDone:(BOOL)enrollDoneArg message:(NSString*)messageArg {
	if (self) {
        enrollError = enrollErrorArg;
        enrollDone = enrollDoneArg;
		message = [messageArg retain];
	}
	return self;
}
-(void)dealloc {
	[message release];
    message = nil;
	[super dealloc];
}
@end

@implementation GOnSCLTrafficLaunchStatusChangedArgs
@synthesize launchId;
@synthesize state;
@synthesize message;
- (id) initWithArgs:(NSString*)launchIdArg state:(Giritech::Appl::ApplClientEventhandler::State)stateArg message:(NSString*)messageArg {
	if (self) {
		launchId = [launchIdArg retain];
		state = (GOnSCLTrafficLaunchState)stateArg;
		message = [messageArg retain];
	}
	return self;
}
-(void)dealloc {
	[launchId release];
	[message release];
    launchId = nil;
    message = nil;
	[super dealloc];
}
@end



@interface GOnSCLTrafficLaunchCommandArgs () {
    Giritech::Appl::ApplClientCommand command_;
    Giritech::Appl::ApplClientDirectConnectionFactory::APtr applClientDirectConnectionFactory;
}
-(Giritech::Appl::ApplClientDirectConnectionFactory::APtr) getApplClientDirectConnectionFactory;
@end

@implementation GOnSCLTrafficLaunchCommandArgs
@synthesize launchType;
@synthesize skinType;
@synthesize cacheType;
@synthesize visibility;
@synthesize skinBackButton;
@synthesize skinProgressBar;
@synthesize launchId;
@synthesize proxyHost;
@synthesize proxyPort;
@synthesize url;
@synthesize handleSchemeHTTPS;


-(id) initWithArgs:(NSString*)launchIdArg command:(const Giritech::Appl::ApplClientCommand&)commandArg {
	if (self) {
        command_ = commandArg;
		launchId = [launchIdArg retain];
        launchType = (GOnSCLTrafficLaunchType) commandArg.getCommandType();
        skinType = (GOnSCLSkinType) commandArg.getSkinType();
        proxyHost = [[NSString stringWithUTF8String:commandArg.getCommandProxyHost().c_str()] retain];
        proxyPort = commandArg.getCommandProxyPort();
        url = [[NSString stringWithUTF8String:commandArg.getCommandURL().c_str()] retain];
        
        cacheType = (GOnSCLCacheType) commandArg.getCacheType();
        visibility = commandArg.getVisibility();
        skinBackButton = commandArg.getSkinBackButton();
        skinProgressBar = commandArg.getSkinProgressBar();
        applClientDirectConnectionFactory = commandArg.getDirectConnectionFactory();
        handleSchemeHTTPS = (GOnSCLHandleSchemeType)commandArg.getHandleSchemeHTTPS();
	}
	return self;
}

-(GOnSCLChangeOrientationLockType) doChangeOrientation {
    int x;
    int y;
    int ppi;
    
    
    float scale = 1;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        scale = [[UIScreen mainScreen] scale];
    }
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        ppi = 132 * scale;
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        ppi = 163 * scale;
    } else {
        ppi = 160 * scale;
    }
    
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    
    CGRect bounds = [[UIScreen mainScreen] bounds];
    x = CGRectGetHeight(bounds) * screenScale;
    y = CGRectGetWidth(bounds) * screenScale;
    
    Giritech::Appl::ApplClientCommand::OrientationType orientation_gon;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            orientation_gon = Giritech::Appl::ApplClientCommand::OrientationType_Portraite;
            break;
        default:
            orientation_gon = Giritech::Appl::ApplClientCommand::OrientationType_Landscape;
    }
    return (GOnSCLChangeOrientationLockType) command_.doChangeOrientation(x, y, ppi, orientation_gon);
}

-(void)dealloc {
	[launchId release];
    [proxyHost release];
    [url release];
    launchId = nil;
    launchType = kLaunchCommandTypeInvalid;
	[super dealloc];
}

-(Giritech::Appl::ApplClientDirectConnectionFactory::APtr) getApplClientDirectConnectionFactory {
    return applClientDirectConnectionFactory;
}
@end




//
// GOnSCLTrafficLaunchCommandController
//
@interface GOnSCLTrafficLaunchCommandController () <GOnSecureBrowserProgressHandlerProtocol> {
    GOnSCLTrafficLaunchCommandArgs* iCommandArgs;

    id<GOnSCLTrafficLaunchCommandControllerBrowserDelegate> iBrowserDelegate;
}
-(id) initWithArgs:(GOnSCLTrafficLaunchCommandArgs*) commandArgs;
@end


@implementation GOnSCLTrafficLaunchCommandController

-(id) initWithArgs:(GOnSCLTrafficLaunchCommandArgs*) commandArgs {
    if (self) {
		iCommandArgs = [commandArgs retain];
        iBrowserDelegate = nil;
	}
	return self;
}

-(void)dealloc {
	[iCommandArgs release];
	[super dealloc];
}

- (NSString*) getLaunchId {
    return iCommandArgs.launchId;
}

- (GOnSCLTrafficLaunchType) getLaunchType {
    return iCommandArgs.launchType;
}

- (NSString*) getLaunchURL {
    return iCommandArgs.url;    
}

- (GOnSCLSkinType) getSkinType {
    return iCommandArgs.skinType;
}

- (GOnSCLCacheType) getCacheType {
    return iCommandArgs.cacheType;    
}

- (BOOL) getVisibility {
    return iCommandArgs.visibility;        
}

- (BOOL) getSkinBackButton {
    return iCommandArgs.skinBackButton;     
}

- (BOOL) getSkinProgressBar {
    return iCommandArgs.skinProgressBar;     
}

-(GOnSCLHandleSchemeType) getHandleSchemeHTTPS {
    return iCommandArgs.handleSchemeHTTPS;
}

-(GOnSCLChangeOrientationLockType) doChangeOrientation {
    return [iCommandArgs doChangeOrientation];
}

- (void) setBrowserDelegate:(id<GOnSCLTrafficLaunchCommandControllerBrowserDelegate>)browserDelegate {
    iBrowserDelegate = browserDelegate;
}

-(void) clearBuildInCookieStore {
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *each in cookieStorage.cookies) {
        [cookieStorage deleteCookie:each];
    }
}

- (void) activate {
    GOnSecureConnectionFactory::APtr connectionFactory(GOnSecureConnectionFactory::create([iCommandArgs getApplClientDirectConnectionFactory]));
    
    [GOnSecureBrowserSpecialProtocolHTTP registerSpecialProtocol:connectionFactory];
    [GOnSecureBrowserSpecialProtocolHTTP setHandleSchemeHTTPS:iCommandArgs.handleSchemeHTTPS];    
    
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateIsActivated];
    }
    [self removeLocalStorage];
    [self clearBuildInCookieStore];    
    
    NSString* jsCode = @""
    "YUI().use('dme_jsapi', function(Y) {"
    "  Y.com.excitor.dme.jsapi.internal.browser_activate();"
    "});"
    "";
    [iBrowserDelegate browserDelegateStringByEvaluatingJavaScriptFromString:jsCode];
}

- (void) deActivate {
    NSString* jsCode = @""
    "YUI().use('dme_jsapi', function(Y) {"
    "  Y.com.excitor.dme.jsapi.internal.browser_deactivate();"
    "});"
    "";
    [iBrowserDelegate browserDelegateStringByEvaluatingJavaScriptFromString:jsCode];

    
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateisNotActivated];
    }
    [self removeLocalStorage];
    [self clearBuildInCookieStore];    
}

- (void) beginProgress:(NSURL*)url {
    [[GOnSecureBrowserProgressHandler getShared] beginWithUrl:url delegate:self];
}

- (void) jsAPIMenuUpdate {
    NSString* jsCode = @""
    "YUI().use('dme_jsapi', function(Y) {"
    "  Y.com.excitor.dme.jsapi.internal.menu_update();"
    "});"
    "";
    [iBrowserDelegate browserDelegateStringByEvaluatingJavaScriptFromString:jsCode];
}

- (void) jsAPIItemStatusChange:(GOnSCLTrafficLaunchStatusChangedArgs*)args  {
    NSInteger jsStatus = 0;
    switch(args.state) {
        case kLaunchStateLaunching:
            jsStatus = 0;
            break;
        case kLaunchStateReady:
            jsStatus = 1;
            break;
        case kLaunchStateClosing:
            jsStatus = 2;
            break;
        case kLaunchStateClosed:
        case kLaunchStateNotLaunched:
            jsStatus = 3;
            break;
        case kLaunchStateError:
            jsStatus = 4;
            break;
    }
    
    NSString* jsCode = @""
    "YUI().use('dme_jsapi', function(Y) {"
    "  Y.com.excitor.dme.jsapi.internal.item_status_change('%@', %d);"
    "});"
    "";
    NSString* jsCodeExpanded = [NSString stringWithFormat:jsCode, args.launchId, jsStatus];
    [iBrowserDelegate browserDelegateStringByEvaluatingJavaScriptFromString:jsCodeExpanded];
}


- (BOOL)isIOS5_1OrHigher {
    // based on: http://stackoverflow.com/a/9320041
    NSArray *versionCompatibility = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    
    if ( [[versionCompatibility objectAtIndex:0] intValue] > 5 ) {
        return YES; // iOS 6+
    }
    
    if ( [[versionCompatibility objectAtIndex:0] intValue] < 5 ) {
        return NO;  // iOS 4.x or lower
    }
    
    if ( [[versionCompatibility objectAtIndex:1] intValue] >= 1 ) {
        return YES; // ios 5.<<1>> or higher
    }
    return NO;  // ios 5.<<0.x>> or lower
}


-(void) removeLocalStorage {
    
    // build localStorage path: ~/Library/WebKit/LocalStorage/file__0.localstorage (for iOS < 5.1)
    //                          ~/Library/Caches/file__0.localstorage (for iOS >= 5.1 )
    NSString *localStoragePath;
    if ( [self isIOS5_1OrHigher] ) {
        // for IOS >= 5.1
        localStoragePath = [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Caches"];
    }
    else {
        // for IOS < 5.1;
        localStoragePath = [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"WebKit/LocalStorage"];
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *localStorageFile = [localStoragePath stringByAppendingPathComponent:@"file__0.localstorage"];
    if([fileManager fileExistsAtPath:localStorageFile]) {
        if ( [fileManager removeItemAtPath:localStorageFile error:nil] == YES) {
            NSLog(@"GOnSCLTrafficLaunchCommandController::emoveLocalStorage.deleted");
        }
        else {
            NSLog(@"GOnSCLTrafficLaunchCommandController::emoveLocalStorage.not_deleted");
        }
    }
}

//
// Implementation of protocol GOnSecureBrowserProgressHandlerProtocol
//
- (void) gOnSecureCommunicationProgressBegin {
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateLoadBegin];
    }
}

- (void) gOnSecureCommunicationProgress:(NSInteger)progress {
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateLoadProgress:progress];
    }
}

- (void) gOnSecureCommunicationProgressEnd {
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateLoadEnd];
    }
}

- (void) gOnSecureCommunicationProgressContentIsNotHTML; {
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateContentIsNotHTML];
    }
}
- (bool) gOnSecureCommunicationIsCacheEnabled {
    switch([iCommandArgs cacheType]) {
        case kGOnSCLCacheTypeOn:
        case kGOnSCLCacheTypeClearOnLaunch:
            return true;
        default:
            break;
    }
    return false;
}

- (void) gOnSecureCommunicationProgressErrorLoadingContent; {
    if(iBrowserDelegate) {
        [iBrowserDelegate browserDelegateErrorLoadingContent];
    }
}


@end




