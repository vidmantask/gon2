//
//  GOnSecureCommunicationLibJsAPIInternalProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 10/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureCommunicationLibJsAPIInternalProtocol <NSObject>

-(void) gOnSecureCommunicationLibJsAPILaunchMenuItem:(NSString*)launchId;

@end
