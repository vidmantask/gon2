//
//  GOnSecureCommunicationLibJsAPIProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 30/03/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureCommunicationLibJsAPIProtocol <NSObject>

-(void) gOnSecureCommunicationLibJsAPIDismissBrowser;

-(void) gOnSecureCommunicationLibJsAPILocalStorageClear:(NSString*)host;
-(NSString*) gOnSecureCommunicationLibJsAPILocalStorageGetItem:(NSString*)host key:(NSString*)key;
-(void) gOnSecureCommunicationLibJsAPILocalStorageRemoveItem:(NSString*)host key:(NSString*)key;
-(void) gOnSecureCommunicationLibJsAPILocalStorageSetItem:(NSString*)host key:(NSString*)key value:(NSString*)value;

@end
