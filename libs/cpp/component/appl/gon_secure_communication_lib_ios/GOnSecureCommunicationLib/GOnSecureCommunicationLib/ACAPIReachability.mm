//
//  ACAPIReachability.m
//  GOnClient
//
//  Created by gbuilder on 16/05/11.
//  Copyright 2011 Giritech. All rights reserved.
//

#import "ACAPIReachability.h"

#include "Reachability.h"


@interface ACAPIReachability () {
    Reachability* reachability_;
    id<ACAPIReachabilityProtocol> delegate_;
}
@end;


@implementation ACAPIReachability

- (id) init:(id<ACAPIReachabilityProtocol>)delegate {
    self = [super init];
	if (self) {
        delegate_ = delegate;
        reachability_ = [[Reachability reachabilityForInternetConnection] retain];
        [reachability_ startNotifier];
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(reachabilityChangedNotification:) 
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
	}
	return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [reachability_ stopNotifier];
    [reachability_ release];
	[super dealloc];
}

-(bool) canReachServer {
    bool canReachServerResult = false;
    
	switch ([reachability_ currentReachabilityStatus]) {
		case ReachableViaWiFi:
		case ReachableViaWWAN:
            canReachServerResult = ![reachability_ connectionRequired];
            break;
		default:
			break;
	}
    return canReachServerResult;
}


-(void) reachabilityChangedNotification:(NSNotification *) notification {
    if(delegate_) {
        [delegate_ ACAPIReachabilityProtocolReachabilityChanged];
    }
}

@end
