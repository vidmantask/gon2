//
//  GOnSecureCommunicationLibProtocol.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 22/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



//
// Helper classes for GOnSecureCommunicationLibProtocol
//
typedef enum {
    kLaunchStateLaunching=0,
    kLaunchStateReady,
    kLaunchStateClosing,
    kLaunchStateClosed,
    kLaunchStateError,
    kLaunchStateNotLaunched
} GOnSCLTrafficLaunchState;


@interface GOnSCLMenuItem : NSObject {
    long id;
	NSString* title;
	NSString* launchId;
	NSString* iconId;
	NSString* tooltip;
	BOOL disabled;
	BOOL tagShow;
	BOOL tagEnabled;
	BOOL tagTopx;
	BOOL tagGupdate;
	BOOL tagAppboxDefault;
	BOOL tagAppboxLink;
    GOnSCLTrafficLaunchState launchState;
    NSString* launchStateMessage;
}
@property (readonly) long id;
@property (readonly) NSString* title;
@property (readonly) NSString* launchId;
@property (readonly) NSString* iconId;
@property (readonly) NSString* tooltip;
@property (readonly) BOOL disabled;
@property (readonly) BOOL tagShow;
@property (readonly) BOOL tagEnabled;
@property (readonly) BOOL tagTopx;
@property (readonly) BOOL tagGupdate;
@property (readonly) BOOL tagAppboxDefault;
@property (readonly) BOOL tagAppboxLink;

@property (assign) GOnSCLTrafficLaunchState launchState;
@property (retain) NSString* launchStateMessage;
-(void) setLaunchState:(GOnSCLTrafficLaunchState) launchState launchMessage:(NSString*)launchMessage;
@end

@interface GOnSCLDialogShowLicenseInfoArgs : NSObject {
	NSString* header;
    NSString* message;
    BOOL valid;
}
- (id) initWithArgs:(NSString*)header message:(NSString*)message valid:(BOOL)valid;
@end

@interface GOnSCLDialogShowMenuArgs : NSObject {
	NSArray* menuItems;
}
@property (readonly) NSArray* menuItems;
@end

@interface GOnSCLDialogImageArgs : NSObject {
	NSString* requestId;
	NSString* imageId;
	UIImage* image;
}
@property (readonly) NSString* requestId;
@property (readonly) NSString* imageId;
@property (readonly) UIImage* image;
@end

@interface GOnSCLInfoArgs : NSObject {
	NSString* header;
    NSString* message;
}
@property (readonly) NSString* header;
@property (readonly) NSString* message;
@end

@interface GOnSCLEnrollRequestArgs : NSObject {
	BOOL canDeploy;
	BOOL authActivated;
	BOOL assignedToCurrentUser;
    NSString* statusText;
}
@end

@interface GOnSCLEnrollResponseArgs : NSObject {
	BOOL rc;
    NSString* message;
}
@end

@interface GOnSCLEnrollGetStatusResponseArgs : NSObject {
	BOOL enrollError;
	BOOL enrollDone;
    NSString* message;
}
@end



@interface GOnSCLTrafficLaunchStatusChangedArgs : NSObject {
    NSString* launchId;
	GOnSCLTrafficLaunchState state;
    NSString* message;
}
@property (readonly) NSString* launchId;
@property (readonly) GOnSCLTrafficLaunchState state;
@property (readonly) NSString* message;
@end



typedef enum {
    kLaunchCommandTypeSecureBrowser=0,
    kLaunchCommandTypeInvalid,
    kLaunchCommandTypeNone
} GOnSCLTrafficLaunchType;

typedef enum {
    kGOnSCLSkinTypeBrowser = 0,
    kGOnSCLSkinTypeAppFullscreen,
    kGOnSCLSkinTypeAppPopup
} GOnSCLSkinType;

typedef enum {
    kGOnSCLCacheTypeOn = 0,
    kGOnSCLCacheTypeOff,
    kGOnSCLCacheTypeClearOnLaunch
} GOnSCLCacheType;

typedef enum {
    kGOnSCLChangeOrientationLockTypeNoLock = 0,
    kGOnSCLChangeOrientationLockTypeLock,
    kGOnSCLChangeOrientationLockTypeRotateAndLock
} GOnSCLChangeOrientationLockType;

typedef enum {
    kGOnSCLHandleSchemeTypeDoNotHandle = 0,
    kGOnSCLHandleSchemeTypeHandle,
    kGOnSCLHandleSchemeTypeBlock
} GOnSCLHandleSchemeType;

@interface GOnSCLTrafficLaunchCommandArgs : NSObject {
	NSString* launchId;
    GOnSCLTrafficLaunchType launchType;
    GOnSCLSkinType skinType;
    GOnSCLCacheType cacheType;
    GOnSCLHandleSchemeType handleSchemeHTTPS;
    BOOL visibility;
    BOOL skinBackButton;
    BOOL skinProgressBar;
    NSString* proxyHost;
    long proxyPort;
    NSString* url;    
}

@property (readonly) NSString* launchId;
@property (readonly) GOnSCLTrafficLaunchType launchType;
@property (readonly) GOnSCLSkinType skinType;

@property (readonly) GOnSCLCacheType cacheType;
@property (readonly) BOOL visibility;
@property (readonly) BOOL skinBackButton;
@property (readonly) BOOL skinProgressBar;
@property (readonly) GOnSCLHandleSchemeType handleSchemeHTTPS;

@property (readonly) NSString* proxyHost;
@property (readonly) long proxyPort;
@property (readonly) NSString* url;

- (GOnSCLChangeOrientationLockType) doChangeOrientation;
@end



@protocol GOnSCLTrafficLaunchCommandControllerBrowserDelegate <NSObject>
- (void) browserDelegateIsActivated;
- (void) browserDelegateisNotActivated;
- (NSString*) browserDelegateStringByEvaluatingJavaScriptFromString:(NSString*)script;
- (void) browserDelegateLoadBegin;
- (void) browserDelegateLoadProgress:(NSInteger)progress;
- (void) browserDelegateLoadEnd;
- (void) browserDelegateContentIsNotHTML;
- (void) browserDelegateErrorLoadingContent;
@end



@interface GOnSCLTrafficLaunchCommandController : NSObject {
}
- (NSString*) getLaunchId;
- (GOnSCLTrafficLaunchType) getLaunchType;
- (NSString*) getLaunchURL;
- (GOnSCLSkinType) getSkinType;

- (GOnSCLCacheType) getCacheType;
- (BOOL) getVisibility;
- (BOOL) getSkinBackButton;
- (BOOL) getSkinProgressBar;
- (GOnSCLHandleSchemeType) getHandleSchemeHTTPS;

- (GOnSCLChangeOrientationLockType) doChangeOrientation;

- (void) setBrowserDelegate:(id<GOnSCLTrafficLaunchCommandControllerBrowserDelegate>)browserDelegate;
- (void) activate;
- (void) deActivate;

- (void) beginProgress:(NSURL*)url;

- (void) jsAPIMenuUpdate;
- (void) jsAPIItemStatusChange:(GOnSCLTrafficLaunchStatusChangedArgs*)args;

@end





//
// GOnSecureCommunicationLibProtocol
// 
@protocol GOnSecureCommunicationLibProtocol <NSObject>

- (NSString*) gonSCLGetConnectInfo;

- (void) gonSCLConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle;
- (void) gonSCLConnectStateConnectingFailedWithMessage:(NSString*) message;
- (void) gonSCLConnectStateConnected;
- (void) gonSCLConnectStateClosing;
- (void) gonSCLConnectStateClosed;

- (void) gonSCLUserRequestLoginWithMessage:(NSString*)message;

- (void) gonSCLDialogShowLicenseInfo:(GOnSCLDialogShowLicenseInfoArgs*)arg;
- (void) gonSCLDialogMenuUpdate;
- (void) gonSCLDialogShowMenu:(GOnSCLDialogShowMenuArgs*)args;
- (void) gonSCLDialogImageFound:(GOnSCLDialogImageArgs*)args;
- (void) gonSCLDialogImageNotFound:(GOnSCLDialogImageArgs*)args;

- (void) gonSCLInfo:(GOnSCLInfoArgs*)args;
- (void) gonSCLInfoError:(GOnSCLInfoArgs*)args;
- (void) gonSCLInfoWarning:(GOnSCLInfoArgs*)args;

- (void) gonSCLEnrollRequest:(GOnSCLEnrollRequestArgs*)args;
- (void) gonSCLEnrollResponse:(GOnSCLEnrollResponseArgs*)args;
- (void) gonSCLEnrollGetStatusResponse:(GOnSCLEnrollGetStatusResponseArgs*)args;

- (void) gonSCLTraffficLaunchStatusChanged:(GOnSCLTrafficLaunchStatusChangedArgs*)args;
- (void) gonSCLTraffficLaunchCommand:(GOnSCLTrafficLaunchCommandController*)launchCommandController;
- (void) gonSCLTraffficLaunchCommandTerminate:(NSString*) launchId;
- (void) gonSCLTraffficLaunchCommandBringToFront:(NSString*) launchId;

- (void) gonSCLConnectInfoUpdated;

- (void) gonSCLKeyStoreReset;
- (void) gonSCLVersionError:(NSString*)serverVersion;

- (void) gonAsyncRunning;
- (void) gonAsyncStopped;

@end
