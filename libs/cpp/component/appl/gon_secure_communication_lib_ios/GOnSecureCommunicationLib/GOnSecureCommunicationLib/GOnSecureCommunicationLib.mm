//
//  GOnSecureCommunicationLib.mm
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 21/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import "GOnSecureCommunicationLib.h"
#import "ACAPICheckpointHandler.h"
#import "ACAPI.h"
#import "SDURLCache.h"
#import "GOnSecureBrowserSpecialProtocol.h"
#import "GOnSecureCommunicationLibJsAPIInternalProtocol.h"
#import "ACAPIReachability.h"

#include <lib/version/Version.hxx>

id<GOnSecureCommunicationLibJsAPIProtocol> sGOnSecureBrowserJsAPIProtocol;
id<GOnSecureCommunicationLibJsAPIInternalProtocol> sGOnSecureBrowserJsAPIInternalProtocol;


//
// Private methods
// 
@interface GOnSecureCommunicationLib() <GOnSecureCommunicationLibJsAPIInternalProtocol, ACAPIReachabilityProtocol> {
    GOnSecureCommunicationLibConfig* config_;
    ACAPI::APtr applClient_;

    NSThread* asyncThread_;
    
    id<GOnSecureCommunicationLibProtocol> delegate_;

    UIBackgroundTaskIdentifier iBackgroundTask;
    
    ACAPIReachability* reachability_;
}
@property (retain) GOnSecureCommunicationLibConfig* config_;
@property (retain) NSThread* asyncThread_;

-(void) checkConnectInfo;
@end



//
// Implementation of GOnSecureCommunicationLib
// 
@implementation GOnSecureCommunicationLib
@synthesize config_;
@synthesize asyncThread_;

#
#pragma mark Instance lifecycle
#
-(void) initInternal:(id<SDURLCacheStorageProtocol>)storageDelegate {
    self.asyncThread_ = nil;
    
    [[ACAPICheckpointHandler shared] init];
    [self checkConnectInfo];
    
    applClient_ = ACAPI::create(config_, [[ACAPICheckpointHandler shared] getCheckpointHanderRaw]);
    applClient_->get()->appl_client_auth_set_info([[config_ getSerial] UTF8String], [[config_ getPrivateKey] UTF8String], [[config_ getPublicKey] UTF8String]);
    iBackgroundTask = UIBackgroundTaskInvalid;

    [GOnSecureBrowserSpecialProtocol setDMEURL:[config_ getDMEURL]];
    
    // Initialize url cache
    SDURLCache* urlCache = [[SDURLCache alloc] initWithMemoryCapacity:1024*1024   // 1MB mem cache
                                                         diskCapacity:1024*1024*5 // 5MB disk cache
                                                             diskPath:[SDURLCache defaultCachePath]];
    [NSURLCache setSharedURLCache:urlCache];
    [urlCache setStorageDelegate:storageDelegate];
    [urlCache release];
    
    reachability_ = [[ACAPIReachability alloc] init:self];
}

- (id) initWithConfig:(id<GOnSecureCommunicationLibConfigProtocol>)config cacheStorageDelegate:(id<SDURLCacheStorageProtocol>)storageDelegate {
    self = [super init];
    if (self) {
        delegate_ = nil;
        sGOnSecureBrowserJsAPIProtocol = nil;
        self.config_ = config;
        [self initInternal:storageDelegate];
    }
    return self;
}

-(void) setDelegate:(id<GOnSecureCommunicationLibProtocol>)delegate {
    delegate_ = delegate;
    applClient_->setDelegate(delegate_);
    [self checkConnectInfo];
}

- (void) setJsAPIDelegate:(id<GOnSecureCommunicationLibJsAPIProtocol>)jsAPIDelegate {
    sGOnSecureBrowserJsAPIProtocol = jsAPIDelegate;
    sGOnSecureBrowserJsAPIInternalProtocol = self;
}

- (void)dealloc {
    self.asyncThread_ = nil;
    self.config_ = nil;
    [reachability_ release];
	[super dealloc];
}


-(void) checkConnectInfo {
    if([config_ hasConnectInfo]) {
        return;
    }
    if(delegate_ == nil) {
        return;
    }
    NSString* connectInfo = [delegate_ gonSCLGetConnectInfo];
    
    bool decode_ok = false;
    std::string decode_servers;
    std::string decode_knownsecret;
    Giritech::Appl::ApplClient::decode_connection_info([connectInfo UTF8String], decode_ok, decode_servers, decode_knownsecret);
    if(decode_ok) {
        [config_ setConnectInfo:[NSString stringWithUTF8String:decode_servers.c_str()] withKnownsecret:[NSString stringWithUTF8String:decode_knownsecret.c_str()]];
    }
}


#pragma mark -
#pragma mark async control
- (void) asyncStart {
    applClient_->get()->async_start();
}

- (void) asyncStartAndJoin {
    applClient_->get()->async_start();
    applClient_->get()->async_join();
}

- (void) asyncStartInBackground {
    self.asyncThread_ = [[NSThread alloc] initWithTarget:self selector:@selector(asyncStartAndJoin) object:nil];
    [asyncThread_ start];
}

- (void) asyncRestart {
    applClient_->get()->async_restart();
}

- (void) asyncStop {
    applClient_->get()->async_stop();
}

- (void) asyncJoin {
    applClient_->get()->async_join();
}


#pragma mark -
#pragma mark connection control
- (void) connectionConnect {
    applClient_->get()->action_connect([[config_ getKnownsecret] UTF8String], [[config_ getServers] UTF8String]);
}

- (void) connectionDisconnect {
    applClient_->get()->action_close();
}

- (GOnSCLConnectionState) connectionGetState {
    return (GOnSCLConnectionState)applClient_->get()->get_state();
}

#pragma mark -
#pragma mark misc methods call through
- (void) userResponseLogin:(NSString*)login password:(NSString*)password {
    applClient_->get()->app_client_user_response_login([login UTF8String], [password UTF8String], false);
}

- (void) userResponseLoginCancelled {
    applClient_->get()->app_client_user_response_login_cancelled();
}


-(void) dialogGetImages:(NSString*)requestId imageIds:(NSArray*)imageIds imageStyle:(NSString*)imageStyle imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY imageFormat:(NSString*)imageFormat {
    std::vector<std::string> image_ids;
    for (NSString* imageId in imageIds) {
        image_ids.push_back([imageId UTF8String]);
    }
    applClient_->get()->app_client_dialog_get_images([requestId UTF8String], image_ids, [imageStyle UTF8String], imageSizeX, imageSizeY, [imageFormat UTF8String]);
}

-(void) dialogGetImages:(NSString*) requestId imageIds:(NSArray*)imageIds imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY {
    [self dialogGetImages:requestId imageIds:imageIds imageStyle:@"iphone" imageSizeX:imageSizeX imageSizeY:imageSizeY imageFormat:@"png"];
}

-(void) dialogGetImage:(NSString*) requestId imageId:(NSString*)imageId imageSizeX:(NSInteger)imageSizeX imageSizeY:(NSInteger)imageSizeY {
    [self dialogGetImages:requestId imageIds:[NSArray arrayWithObjects:imageId,nil] imageSizeX:imageSizeX imageSizeY:imageSizeY];
}


-(void) dialogMenuItemLaunch:(NSString*)launchId {
    applClient_->get()->app_client_dialog_menu_item_selected([launchId UTF8String], false);
}

-(BOOL) dialogMenuItemIsLaunched:(NSString*)launchId {
    return applClient_->get()->app_client_dialog_menu_item_is_launched([launchId UTF8String]);
}

-(void) dialogMenuItemTerminate:(NSString*)launchId {
    applClient_->get()->app_client_dialog_menu_item_close([launchId UTF8String]);
}

-(GOnSCLConnectionState) dialogMenuItemGetStatus:(NSString*)launchId {
    std::string state_info; // Currently not available in api
    int state = applClient_->get()->app_client_dialog_menu_item_get_status_info([launchId UTF8String], state_info);
    return (GOnSCLConnectionState)state;
}

-(void) trafficLaunchCommandRunning:(NSString*)launchId {
    applClient_->get()->app_client_traffic_launch_command_running([launchId UTF8String]);
}

-(void) trafficLaunchCommandTerminated:(NSString*)launchId {
    applClient_->get()->app_client_traffic_launch_command_terminated([launchId UTF8String], false, "");
}

-(void) trafficLaunchCommandTerminatedWithError:(NSString*)launchId errorMessage:(NSString*)errorMessage {
    applClient_->get()->app_client_traffic_launch_command_terminated([launchId UTF8String], true, [errorMessage UTF8String]);
}

-(void) enroll {
    applClient_->get()->appl_client_auth_generate_keys();
}

-(void) enrollGetStatus {
    applClient_->get()->app_client_endpoint_get_enrollment_status();
}


#
#
#
- (void) gonAsyncStart {
	UIApplication* app = [UIApplication sharedApplication];
	
	iBackgroundTask = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"gonAsyncStart.asyncStop.start");
        [self asyncStop];
		[app endBackgroundTask:iBackgroundTask];
		iBackgroundTask = UIBackgroundTaskInvalid;
        NSLog(@"gonAsyncStart.asyncStop.done");
	}];
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"gonAsyncStart.asyncJoin.start");
        [self asyncStart];
        [self asyncJoin];
		[app endBackgroundTask:iBackgroundTask];
		iBackgroundTask = UIBackgroundTaskInvalid;
        NSLog(@"gonAsyncStart.asyncJoin.done");
	});
}

- (void) gonAsyncRestart {
    UIApplication* app = [UIApplication sharedApplication];
        
    if(iBackgroundTask != UIBackgroundTaskInvalid) {
        iBackgroundTask = [app beginBackgroundTaskWithExpirationHandler:^{
            NSLog(@"gonAsyncRestart.asynStop.start");
            [self asyncStop];
            [app endBackgroundTask:iBackgroundTask];
            iBackgroundTask = UIBackgroundTaskInvalid;
            NSLog(@"gonAsyncRestart.asynStop.done");
        }];
    } 
    
    // Start the long-running task and return immediately.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSLog(@"gonAsyncRestart.asyncJoin.start");
        [self asyncRestart];
        if(delegate_) {
            [delegate_ gonAsyncRunning];
        }
        [self asyncJoin];
        if(delegate_) {
            [delegate_ gonAsyncStopped];
        }
        [app endBackgroundTask:iBackgroundTask];
        iBackgroundTask = UIBackgroundTaskInvalid;
        NSLog(@"gonAsyncRestart.asyncJoin.done");
    });
}

- (void) gonAsyncRestartIfNotRunning {
    if(applClient_->get()->async_is_running()) {
        [delegate_ gonAsyncRunning];
        return;
    }
    [self gonAsyncRestart];
}

- (void) gonAsyncStop {
    NSLog(@"gonAsyncStop");
    [self asyncStop];
}

+(NSString*) getVersion {
    Giritech::Version::Version::APtr versionFromCore(Giritech::Version::Version::create_current());
	return [NSString stringWithUTF8String: versionFromCore->get_version_string().c_str()];
}



//
// Implementation of protocol GOnSecureCommunicationLibJsAPIInternalProtocol
//
-(void) gOnSecureCommunicationLibJsAPILaunchMenuItem:(NSString*)launchId {
    if(![self dialogMenuItemIsLaunched:launchId]) {
        [self dialogMenuItemLaunch:launchId];
    }
    else {
        [delegate_ gonSCLTraffficLaunchCommandBringToFront:launchId];
    }
}


//
// Implementation of protocol ACAPIReachabilityProtocol
//
-(void) ACAPIReachabilityProtocolReachabilityChanged {
    applClient_->get()->appl_client_verify_connection();
}


@end
