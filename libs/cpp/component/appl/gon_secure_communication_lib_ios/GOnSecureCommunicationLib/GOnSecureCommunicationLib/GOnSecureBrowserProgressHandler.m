//
//  GOnSecureBrowserProgressHandler.m
//  GOnSecureCommunicationLib
//
//  Created by gbuilder 11 on 08/05/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserProgressHandler.h"

@interface GOnSecureBrowserProgressHandler () {
    NSURL* iUrl;
    id<GOnSecureBrowserProgressHandlerProtocol> iDelegate;
    
    NSInteger iSizeTotal;
    NSInteger iSizeDone;
    NSInteger iPct;
    NSInteger iPending;
    bool iDone;
}
@property (retain) NSURL* iUrl;
@end



@implementation GOnSecureBrowserProgressHandler
@synthesize iUrl;

static GOnSecureBrowserProgressHandler* sGOnSecureBrowserProgressHandler = nil;

+ (GOnSecureBrowserProgressHandler*) getShared {
    if(!sGOnSecureBrowserProgressHandler) {
        sGOnSecureBrowserProgressHandler = [[GOnSecureBrowserProgressHandler alloc] init];
    }
    return sGOnSecureBrowserProgressHandler;
}

- (void) beginWithUrl:(NSURL*)url delegate:(id<GOnSecureBrowserProgressHandlerProtocol>)delegate {
    self.iUrl = url;
    iDelegate = delegate;
}

- (void) resetDelegate {
    iDelegate = nil;
}

-(void) resetMT {
    [iDelegate gOnSecureCommunicationProgressBegin];
    [iDelegate gOnSecureCommunicationProgress:1];
}

- (void) reset {
    iSizeTotal = 0;
    iSizeDone = 0;
    iPct = 1;
    iPending = 0;
    iDone = false;
    [self performSelectorOnMainThread:@selector(resetMT) withObject:nil waitUntilDone:NO];
}

- (void) reportBegin {
    iPending++;   
}

- (void) reportDoneMTDelayed {
    if(iDone) {
        return;
    }
    if(iPending == 0) {
        iDone = true;
        [iDelegate gOnSecureCommunicationProgress:100];
        [iDelegate gOnSecureCommunicationProgressEnd];
    }
}

- (void) reportDoneMT {
    if(iDone) {
        return;
    }
    [self performSelector:@selector(reportDoneMTDelayed) withObject:nil afterDelay:2];
}

- (void) reportDone {
    iPending--;   
    if(iPending == 0) {
        [self performSelectorOnMainThread:@selector(reportDoneMT) withObject:nil waitUntilDone:NO];
    }
}

-(bool) compareWithHeaders:(NSDictionary*)httpResponseHeaderFields {
    bool doTrackProgress = false;
    NSString* headerReferer = [httpResponseHeaderFields objectForKey:@"Referer"];
    if(headerReferer) { 
        doTrackProgress =  [[iUrl absoluteString] isEqualToString:headerReferer];
    }
    return doTrackProgress;
}

-(bool) compareWithUrl:(NSURL*) url {
    return [[iUrl absoluteString] isEqualToString:[url absoluteString]];
}

-(void) updatePctAndTell {
    if(iDone) {
        return;
    }
    
    if((iSizeTotal != 0) && (iSizeDone != 0)) {
        double work = iSizeDone * 100;
        iPct = work / iSizeTotal;
    }
    if(iPct > 95) {
        iPct = 95; 
    }
    [iDelegate gOnSecureCommunicationProgress:iPct];
}

- (void) reportPartBegin:(NSInteger)size {
    iSizeTotal += size;
    [self updatePctAndTell];
}

-(bool) reportPartBeginWirhHeaders:(NSDictionary*)httpResponseHeaderFields {
    bool doTrackProgress = true;
    
    NSString* headerContentLength = [httpResponseHeaderFields objectForKey:@"Content-Length"];
    if(headerContentLength) { 
        [self reportPartBegin:[headerContentLength intValue]];
    }
    
    NSString* headerTransferEncoding = [httpResponseHeaderFields objectForKey:@"Transfer-Encoding"];
    if(headerTransferEncoding && [[headerTransferEncoding lowercaseString] isEqualToString:@"chunked"]) { 
        doTrackProgress = false;
    }
    if(!doTrackProgress) {
        [self reportDone];
    }
    return doTrackProgress;
}

- (void) reportPartDone:(NSInteger)size {
    iSizeDone += size;    
    [self updatePctAndTell];
}

- (void) reportContentIsNotHTMLMT {
    [iDelegate gOnSecureCommunicationProgressContentIsNotHTML];
}

- (void) reportContentIsNotHTML {
    [self performSelectorOnMainThread:@selector(reportContentIsNotHTMLMT) withObject:nil waitUntilDone:NO];
}

- (void) reportErrorLoadingContentMT {
    [iDelegate gOnSecureCommunicationProgressErrorLoadingContent];
}

- (void) reportErrorLoadingContent {
    [self performSelectorOnMainThread:@selector(reportErrorLoadingContentMT) withObject:nil waitUntilDone:NO];
}

- (bool) isCacheEnabled {
    return [iDelegate gOnSecureCommunicationIsCacheEnabled];
}

@end
