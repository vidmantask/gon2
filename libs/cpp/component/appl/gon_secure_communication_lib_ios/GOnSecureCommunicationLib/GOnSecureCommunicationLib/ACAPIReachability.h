//
//  ACAPIReachability.h
//  GOnClient
//
//  Created by gbuilder on 16/05/11.
//  Copyright 2011 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "string"
#include "vector"

#include <component/communication/COM_Servers_Spec.hxx>


@protocol ACAPIReachabilityProtocol <NSObject> 
-(void) ACAPIReachabilityProtocolReachabilityChanged;
@end


@interface ACAPIReachability : NSObject {
}

- (id) init:(id<ACAPIReachabilityProtocol>)delegate;
- (bool) canReachServer;

@end
