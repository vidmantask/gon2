//
//  GOnSecureCommunicationLibTestsAppTests.m
//  GOnSecureCommunicationLibTestsAppTests
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import "GOnSecureCommunicationLibTestsAppTests.h"
#import "GOnSecureCommunicationLibConfig.h"
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureCommunicationLibProtocol.h"
#import "GOnSecureBrowserJsAPIProtocol.h"


typedef enum {
    kTDStateReady = 0,
    kTDStateConnecting,
    kTDStateConnected,
    kTDStateDisconnecting,
    kTDStateDone,
    kTDStateTimeout,
    kTDStateError
} TestDriverState;


@interface TestDriver : NSObject <GOnSecureCommunicationLibProtocol, GOnSecureBrowserJsAPIProtocol> {   
    GOnSecureCommunicationLib* communication_;
    TestDriverState state_;
}
-(void) setCommunication:(GOnSecureCommunicationLib*) communication;
@end

@implementation TestDriver
-(void) setCommunication:(GOnSecureCommunicationLib*) communication {
    communication_ = communication;
    state_ = kTDStateReady;
}


- (NSString*) gonSCLGetConnectInfo {
    return @"";
}

- (void) gonSCLConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle {    
}

- (void) gonSCLConnectStateConnectingFailedWithMessage:(NSString*) message {
    state_ = kTDStateError;
}

- (void) gonSCLConnectStateConnected {
    state_ = kTDStateConnected;
}

- (void) gonSCLConnectStateClosing {
    
}

- (void) gonSCLConnectStateClosed {
    state_ = kTDStateDone;
}

- (void) gonSCLUserRequestLoginWithMessage:(NSString*)message {
    
}

- (void) gonSCLDialogShowLicenseInfo:(GOnSCLDialogShowLicenseInfoArgs*)arg {
    
}

- (void) gonSCLDialogMenuUpdate {
    
}

- (void) gonSCLDialogShowMenu:(GOnSCLDialogShowMenuArgs*)args {
    
}

- (void) gonSCLDialogImageFound:(GOnSCLDialogImageArgs*)args {
    
}

- (void) gonSCLDialogImageNotFound:(GOnSCLDialogImageArgs*)args {
    
}

- (void) gonSCLInfo:(GOnSCLInfoArgs*)args {
    
}

- (void) gonSCLInfoError:(GOnSCLInfoArgs*)args {
    
}
- (void) gonSCLInfoWarning:(GOnSCLInfoArgs*)args {
    
}

- (void) gonSCLEnrollRequest:(GOnSCLEnrollRequestArgs*)args {
    
}

- (void) gonSCLEnrollResponse:(GOnSCLEnrollResponseArgs*)args {
    
}

- (void) gonSCLEnrollGetStatusResponse:(GOnSCLEnrollGetStatusResponseArgs*)args {
    
}

- (void) gonSCLTraffficLaunchStatusChanged:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    
}

- (void) gonSCLTraffficLaunchCommand:(GOnSCLTrafficLaunchCommandArgs*)args {
    
}

- (void) gonSCLTraffficLaunchCommandTerminate:(NSString*) launchId {
    
}

- (void) gonSCLConnectInfoUpdated {
    
}

- (void) gonSCLKeyStoreReset {
    
}

- (void) gonSCLVersionError:(NSString*)serverVersion {
    
}

- (void) gOnSecureBrowserJsAPIDismissBrowser {
    NSLog(@"TestDriver::gOnSecureBrowserJsAPIDismissBrowser");  
}

-(void) stepTimeout {
    state_ = kTDStateTimeout;
}

-(void) stepTimeoutStart {
    [self performSelector:@selector(stepTimeout) withObject:nil afterDelay:10];    
}

-(void) stepTimeoutCancel {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(stepTimeout) object:nil];
}

-(void) stepConnect {
    switch (state_) {
        case kTDStateReady:
            break;
        default:
            return;
    }
    [communication_ connectionConnect];
    state_ = kTDStateConnecting;
    [self stepTimeoutStart];
}

-(void) stepWaitForConnect {
    while (true) {
        switch (state_) {
            case kTDStateConnecting:
                break;
            case kTDStateConnected:
                [self stepTimeoutCancel];
                return;
            default:
                return;
        }
    }
}

-(void) stepDisconnect {
    switch (state_) {
        case kTDStateConnected:
            break;
        default:
            return;
    }
    [communication_ connectionDisconnect];
    state_ = kTDStateDisconnecting;
    [self stepTimeoutStart];
}

-(void) stepWaitForDone {
    while (true) {
        switch (state_) {
            case kTDStateDisconnecting:
                break;
            case kTDStateDone:
                [self stepTimeoutCancel];
                return;
            default:
                return;
        }
    }    
}




@end



@implementation GOnSecureCommunicationLibTestsAppTests

- (void)setUp {
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown {
    // Tear-down code here.
    
    [super tearDown];
}



/*
   Test config
*/
- (void)testConfig {
    GOnSecureCommunicationLibConfig* config = [[[GOnSecureCommunicationLibConfig alloc] init] autorelease];

    //
    // Serial
    //
    NSString* serial = [config generateSerial];
    STAssertTrue([serial isEqualToString:[config getSerial]], @"Serial");
    
    
    //
    // Keys
    //
    NSString* privateKey = @"private_key";
    NSString* publicKey = @"public_key";
    [config setKeysWithPublicKey:publicKey privateKey:privateKey];
    STAssertTrue([publicKey isEqualToString:[config getPublicKey]], @"Keys 1");
    STAssertTrue([privateKey isEqualToString:[config getPrivateKey]], @"Keys 2");

    
    //
    // Connection info
    //
    [config clearConnectInfo];
    STAssertFalse([config hasConnectInfo], @"Connection info 1");
    
    NSString* servers1 = @"servers1";
    NSString* servers2 = @"servers2";
    NSString* knownsecret = @"knownsecret";
    [config setConnectInfo:servers1 withKnownsecret:knownsecret];
    STAssertTrue([config hasConnectInfo], @"Connection info 2");
    STAssertTrue([servers1 isEqualToString:[config getServers]], @"Connection info 3");
    STAssertTrue([knownsecret isEqualToString:[config getKnownsecret]], @"Connection info 4");

    [config setConnectInfo:servers2];
    STAssertTrue([config hasConnectInfo], @"Connection info 5");
    STAssertTrue([servers2 isEqualToString:[config getServers]], @"Connection info 6");

    [config clearConnectInfo];
    STAssertFalse([config hasConnectInfo], @"Connection info 7");

    STAssertFalse([servers1 isEqualToString:[config getServers]], @"Connection info 8");
    STAssertFalse([servers2 isEqualToString:[config getServers]], @"Connection info 9");
    STAssertFalse([knownsecret isEqualToString:[config getKnownsecret]], @"Connection info 10");

}


/*
   Test config
*/
- (void)testCommunication {
    NSString* kServers = @"<?xml version='1.0'?>"
    "<servers>"
    "  <connection_group title='Default' selection_delay_sec='1'>"
    "   <connection title='Direct-Connection' host='192.168.42.102' port='13945' timeout_sec='30' type='direct' />"
    "  </connection_group>"
    "</servers>";
    
    NSString* kKnownsecret = @"0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F";


    TestDriver* testDriver = [[[TestDriver alloc] init] autorelease];
    GOnSecureCommunicationLibConfig* config = [[[GOnSecureCommunicationLibConfig alloc] init] autorelease];
    [config setConnectInfo:kServers withKnownsecret:kKnownsecret];

    GOnSecureCommunicationLib* communication = [[[GOnSecureCommunicationLib alloc] initWithDelegate:testDriver config:config] autorelease];
    [communication setJsAPIDelegate:testDriver];
    [testDriver setCommunication:communication];
    

    [communication asyncStartInBackground];
    
    [testDriver stepConnect];
    [testDriver stepWaitForConnect];
    
    [testDriver stepDisconnect];
    [testDriver stepWaitForDone];
    
    [communication asyncStop];
    [communication asyncJoin];
    [NSThread sleepForTimeInterval:5];
}

@end
