//
//  TESTViewController.h
//  GOnSecureCommunicationLibTestsApp
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TESTViewController : UIViewController {

IBOutlet UIButton* iButtonBrowser;
}

@property (atomic, retain) IBOutlet UIButton* iButtonBrowser;

-(IBAction)actionBrowse:(id)sender;


@end
