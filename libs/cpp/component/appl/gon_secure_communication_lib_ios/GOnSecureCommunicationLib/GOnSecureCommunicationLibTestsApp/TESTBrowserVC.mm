//
//  TESTBrowserVC.m
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 21/03/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//
#import "TESTBrowserVC.h"
#import "GOnSecureCommunicationLibConfig.h"
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureCommunicationLibJsAPIProtocol.h"
#import "SDURLCacheStorage.h"


typedef enum {
    kGuiStateInit=0,
    kGuiStateNotConnected, 
    kGuiStateNotConnectedConnecting, 
    kGuiStateNotConnectedDisconnecting, 
    kGuiStateConnected,
    kGuiStateConnectedWebLoading
} GuiState;



@interface TESTBrowserVC () <GOnSecureCommunicationLibProtocol, GOnSecureCommunicationLibJsAPIProtocol, GOnSCLTrafficLaunchCommandControllerBrowserDelegate> {
    GOnSecureCommunicationLibConfig* iSecureCommunicationLibConfig;
    GOnSecureCommunicationLib* iSecureCommunicationLib;
    GuiState iGuiState;
    
    bool iDefaultBrowserLaunched;
    
    GOnSCLTrafficLaunchCommandController* iCurrentLaunchCommandController;
    NSMutableDictionary* iLocalStorage;
    SDURLCacheStorage* iCacheStorage;
}
@property (atomic, retain) IBOutlet GOnSCLTrafficLaunchCommandController* iCurrentLaunchCommandController;



- (void) updateView;
@end



@implementation TESTBrowserVC
@synthesize ivWebView;
@synthesize iCurrentLaunchCommandController;

#
#pragma mark - Instance lifecycle
#
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        iGuiState = kGuiStateInit;
        iCacheStorage = [[SDURLCacheStorage alloc] init];
        iSecureCommunicationLibConfig = [[GOnSecureCommunicationLibConfig alloc] init];
        [iSecureCommunicationLibConfig clearSecure];
        iSecureCommunicationLib = [[GOnSecureCommunicationLib alloc] initWithConfig:iSecureCommunicationLibConfig cacheStorageDelegate:iCacheStorage];
        [iSecureCommunicationLib setDelegate:self];
        [iSecureCommunicationLib setJsAPIDelegate:self];
        [iSecureCommunicationLib gonAsyncStart];
        
        iDefaultBrowserLaunched = false;
        self.iCurrentLaunchCommandController = nil;
        
        iLocalStorage = [[NSMutableDictionary alloc] init];
 
    }
    return self;
}

- (void)dealloc {
    [iSecureCommunicationLibConfig release];
    [iSecureCommunicationLib release];
    [iLocalStorage release];
    [iCacheStorage release];
    self.iCurrentLaunchCommandController = nil;

    [super dealloc];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#
#pragma mark - View lifecycle
#
- (void)loadView {
    [super loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewDidUnload {
    self.ivWebView = nil;
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    [self updateView];

    if(iGuiState == kGuiStateInit) {
        iGuiState = kGuiStateNotConnected;
        [self updateView];
    }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}


- (void) updateView {
    UIBarButtonItem* leftButton = [[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(guiActionDone:)] autorelease];
    self.navigationItem.leftBarButtonItem = leftButton;

    switch(iGuiState) {
        case kGuiStateInit: {
            break;
        }
        case kGuiStateNotConnected: {
            UIBarButtonItem* rightButton = [[[UIBarButtonItem alloc] initWithTitle:@"connect" style:UIBarButtonItemStyleBordered target:self action:@selector(guiAction:)] autorelease];
            self.navigationItem.rightBarButtonItem = rightButton;
            break;
        }
        case kGuiStateNotConnectedConnecting:
        case kGuiStateConnected: {
            UIBarButtonItem* rightButton = [[[UIBarButtonItem alloc] initWithTitle:@"disconnect" style:UIBarButtonItemStyleBordered target:self action:@selector(guiAction:)] autorelease];
            self.navigationItem.rightBarButtonItem = rightButton;
            break;
        }
        default:
            return;
    }
}

-(void) loadCurrentCommand {

    [iCurrentLaunchCommandController activate];
    [iCurrentLaunchCommandController setBrowserDelegate:self];
    
    self.ivWebView.delegate = self;
    
    
    [iCurrentLaunchCommandController beginProgress:[NSURL URLWithString:[iCurrentLaunchCommandController getLaunchURL]]];
    [self.ivWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[iCurrentLaunchCommandController getLaunchURL]]]];
}



#
#pragma mark - GUI Actions
#
-(void) guiActionDone:(id)sender {
    [iSecureCommunicationLib gonAsyncStop];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) guiAction:(id)sender {
    switch(iGuiState) {
        case kGuiStateNotConnected: {
            [iSecureCommunicationLib connectionConnect];
            break;
        }
        case kGuiStateNotConnectedConnecting:
        case kGuiStateConnected: {
            [iSecureCommunicationLib connectionDisconnect];
            break;
        }
        default:
            return;
    }
    
}

#
#pragma mark - Implementation of UIWebViewDelegate
#
-(void) showDefaultPage {
    self.ivWebView.scalesPageToFit = true;
    
    
    [self.ivWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.dailyshopbox.com"]]];  
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"TESTBrowserVC.webViewDidStartLoad url:%@", [[webView request] URL]);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"TESTBrowserVC.webViewDidFinishLoad url:%@", [[webView request] URL]);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"TESTBrowserVC.webViewDidFailLoadWithError error:%@", error);
}


#
#pragma mark - Implementation of GOnSecureCommunicationLibProtocol
#
- (NSString*) gonSCLGetConnectInfo {
    // 172.16.15.25 (twa-linux)
    return @"H4sIACkdfE8C/+1Sy24bMQy8+ysWueylsEmKoqTAcaDnbwSBs21dOLZhr4vk78u1gwDJoZecCnT3IGKoIYYz+nV4WW/GAQluv90u71+et93v4Xja7Hd3Pc6hv1/NlqfhOGHvjRu4Wc26brne73bDelTo4cdxfz5042bcDnd9Gb4/nrdj352G7Vv/adg+vj6chrVO7SfyB/o7cXNUoO9+7k+jXnQ0R5mjnZPtu8P+qJgJrPW4eR725/E6z4ACrwelP13pi4u4xWd1usjibZPVTJcFxGDAk55WTywEAo6iF/a5mgLamfAsUzXVWD7dQCBFCaYfatBu+NgFc+khRwMMwFjhK9/X+AisqhjUV0Mpemuiq85gbEihEbXGyMGKiYwhk6RmIXIORWptsQiSqAXJGldqrh5Dcs1Bs9ZGcZnJRZ9LSA19rCkkKxUyIlhpsYoaQJN6/zeFEopNKaBNubDUljAW29CZmEq7pMBgzOSBmkmESapwkwg5WszGF6yhZbHOVkRmAWEQzKicFEPRZQ1jMwDFFmlBl6RcVVcAp/MKupZD4IKYgk5TPJnmAqkUDRSiLcjTi2j1ogX+v5p/5tUwaXJE10zUkWIIoQlGEk2cnY3EzngbM7qsgXnwYm12RbwuzYlFoxeDyYY/zcm6tzIFAAA=";
    
/*    
    return @"H4sIAHzWaU8C/+1Sy27bQAy8+yuEXHQpbJLL5e4GjoN9/kYQOGrrwrENWy6Svy9lBymaQy85FagEaInhDjXk8MfhZb0ZByS4/XK7vH953nY/h+Nps9/d9TiH/n41W56G44S9J27gZjXruuV6v9sN61Ghh2/H/fnQjZtxO9z1Zfj6eN6OfXcatm/5p2H7+PpwGtZatdeSv6nvpM1Rgb77vj+NesnRHGWOVr+27w77o4ImsMbj5nnYn8drMQMKvB6U/3TlL1azSdriozb95+Ktj9VMWwXEYMCTnlZPLAQCjqIX9rmaApqZ8CxTNMVYPtxAIEUJphdq0Gz4MwvmkkOOBhiAscJnns/xEVhVMehkDaXorYmuOoOxIYVG1BojBysmMoZMkpqFyDkUqbXFIkiiI0jWuFJz9RiSaw6atTaKy0wu+lxCauhjTSFZqZARwUqLVXQANKn3f1MoodiUAtqUC0ttCWOxDZ2JqbSLCwzGTDPQYRJhkircJEKOFrPxBWtoWayzFZFZQBgEMyonxVC0WcPYDECxRVrQJilX1RXAab2CruUQuCCmoNUUT6a5QCpFDYVoC/K0Ea1etMD/rflntoZJnSO6eqITKYYQmmAkUcfZ2UjsjLcxo8tqmAcv1mZXxGvTnFjUejGYbPgF00TVIzAFAAA=";
  */  
/* 
 mini hulk
    return @"H4sIAMWx4E4C/+1STU8bMRC98ytWXPZShRnbM2MjCPKneuyBO0JhW1KFJEo2Ffz7zgZEVdRy4VSpuwfb7808vxnP9+3jYjkOaOD80/nF1ePDqvsx7PbLzfqyxxn0V/OTi/2wm7BX4hRO5yddd7HYrNfDYlTo5ttuc9h243JcDZd9Gb7eHlZj3+2H1Qt/N6xun272w0JVe5X8lfqatNwp0Hf3m/142XuYIcMsmBlD3203O8VscNRr+MOwOYzPWlbJ8Wmr6XfP6Wd/1P58ff3lPWUPf9W9H8ftpDrVe/a2YL3s7KU58xPtHyAGC97oSrpiMcAgJnp2PldbQJkJzzztpj2WNxEIRlED0w81KBt+Z8EeOXTRggNwWOEj38fyEZy6coBirEnRk41SxWJsaEIzpjWHLhDb6DBkw6kRRJdD4VpbLIyGtQWJrJSaq8eQpAk0Ioos2RmJPpeQGvpYU0jEFTIiELdYWRtgJvf+PYccCqUUkFIujmtLGAs1FBtTacdXcGDt1AOtojhHyalHx14fq+aW1BB6NhQj1OqE2BuyWTiSVukZKfucNRgosReoTFJ8tGKSUGMHVGqUpLcnklypoFKSC5PJVEyoIHWaiFaPXuD/1PwzU+OMUynz/CbakQKWUpXWvBYXIYgNHFhqiIlrwtq4+Iy+hmKpCInoSVok0+AnVLyMAIUFAAA=";
 */
}

- (void) gonSCLConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle {
    NSLog(@"TESTBrowserVC.gonSCLConnectStateConnectingWithConnectionTitle");
    switch(iGuiState) {
        case kGuiStateNotConnected:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateNotConnectedConnecting;
    [self updateView];    
}

- (void) gonSCLConnectStateConnectingFailedWithMessage:(NSString*) message {
    NSLog(@"TESTBrowserVC.gonSCLConnectStateConnectingWithConnectionTitle message:%@", message);
    switch(iGuiState) {
        case kGuiStateNotConnectedConnecting:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateNotConnected;
    [self updateView];    
}

- (void) gonSCLConnectStateConnected {
    NSLog(@"TESTBrowserVC.gonSCLConnectStateConnected");
    switch(iGuiState) {
        case kGuiStateNotConnectedConnecting:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateConnected;
    [self updateView];        
}

- (void) gonSCLConnectStateClosing {
    NSLog(@"TESTBrowserVC.gonSCLConnectStateClosing");
    switch(iGuiState) {
        case kGuiStateConnected:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateNotConnectedDisconnecting;
    [self updateView];        
    
}

- (void) gonSCLConnectStateClosed {
    NSLog(@"TESTBrowserVC.gonSCLConnectStateClosed");
    switch(iGuiState) {
        case kGuiStateConnected:
        case kGuiStateNotConnectedDisconnecting:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateNotConnected;
    iDefaultBrowserLaunched = false;
    [self updateView];        
}

- (void) gonSCLUserRequestLoginWithMessage:(NSString*)message {
    NSLog(@"TESTBrowserVC.gonSCLUserRequestLoginWithMessage message:%@", message);            
}

- (void) gonSCLDialogShowLicenseInfo:(GOnSCLDialogShowLicenseInfoArgs*)arg {
    NSLog(@"TESTBrowserVC.gonSCLDialogShowLicenseInfo");        
}

- (void) gonSCLDialogMenuUpdate {
    NSLog(@"TESTBrowserVC.gonSCLDialogMenuUpdate");
    [iCurrentLaunchCommandController jsAPIMenuUpdate];
}

- (void) gonSCLDialogShowMenu:(GOnSCLDialogShowMenuArgs*)args {
    NSLog(@"TESTBrowserVC.gonSCLDialogShowMenu");

    [iCurrentLaunchCommandController jsAPIMenuUpdate];
    
    if(!iDefaultBrowserLaunched) {
        for(GOnSCLMenuItem* menuItem in [args menuItems]) {
            if(menuItem.tagAppboxDefault) {
                NSLog(@"TESTBrowserVC.gonSCLDialogShowMenu.launching.APPBOX_DEFAULT");
                [iSecureCommunicationLib dialogMenuItemLaunch:menuItem.launchId];
                iDefaultBrowserLaunched = YES;
                return;
            }
        }    
    }
}

- (void) gonSCLDialogImageFound:(GOnSCLDialogImageArgs*)args {
    
}

- (void) gonSCLDialogImageNotFound:(GOnSCLDialogImageArgs*)args {
    
}

- (void) gonSCLInfo:(GOnSCLInfoArgs*)args {
    NSLog(@"TESTBrowserVC.gonSCLInfo");    
}

- (void) gonSCLInfoError:(GOnSCLInfoArgs*)args {
    NSLog(@"TESTBrowserVC.gonSCLInfoError");    
}

- (void) gonSCLInfoWarning:(GOnSCLInfoArgs*)args {
    NSLog(@"TESTBrowserVC.gonSCLInfoWarning");
}

- (void) gonSCLEnrollRequest:(GOnSCLEnrollRequestArgs*)args {
    
}

- (void) gonSCLEnrollResponse:(GOnSCLEnrollResponseArgs*)args {
    
}

- (void) gonSCLEnrollGetStatusResponse:(GOnSCLEnrollGetStatusResponseArgs*)args {
    
}

- (void) gonSCLTraffficLaunchStatusChanged:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    NSLog(@"TESTBrowserVC.gonSCLTraffficLaunchStatusChanged");
    [iCurrentLaunchCommandController jsAPIItemStatusChange:args];    
}

- (void) gonSCLTraffficLaunchCommand:(GOnSCLTrafficLaunchCommandController*)launchCommandController {
    NSLog(@"TESTBrowserVC.gonSCLTraffficLaunchCommand");
    self.iCurrentLaunchCommandController = launchCommandController;
   
    
    NSLog(@"command skin                 :%d", [launchCommandController getSkinType]);
    NSLog(@"command cache                :%d", [launchCommandController getCacheType]);
    NSLog(@"command visibility           :%d", [launchCommandController getVisibility]);
    NSLog(@"command skinBackButton       :%d", [launchCommandController getSkinBackButton]);
    NSLog(@"command doChangeOreientation :%d", [launchCommandController doChangeOrientation]);
    
    [iSecureCommunicationLib trafficLaunchCommandRunning:[launchCommandController getLaunchId]];
    [self loadCurrentCommand];
}

- (void) gonSCLTraffficLaunchCommandBringToFront:(NSString*) launchId {
    NSLog(@"TESTBrowserVC.gonSCLTraffficLaunchCommandBringToFront launchId:%@", launchId);    
}

- (void) gonSCLTraffficLaunchCommandTerminate:(NSString*) launchId {
    NSLog(@"TESTBrowserVC.gonSCLTraffficLaunchCommandTerminate"); 
    [iSecureCommunicationLib trafficLaunchCommandTerminated:launchId];
}

- (void) gonSCLConnectInfoUpdated {
    
}

- (void) gonSCLKeyStoreReset {
    
}

- (void) gonSCLVersionError:(NSString*)serverVersion {
    
}

- (void) gonAsyncRunning {
    NSLog(@"TESTBrowserVC.gonAsyncRunning");

    switch(iGuiState) {
        case kGuiStateInit:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateNotConnected;
    [self updateView];
}

- (void) gonAsyncStopped {
    NSLog(@"TESTBrowserVC.gonAsyncStopped");
}


#
#pragma mark - Implementation of GOnSecureBrowserJsAPIProtocol
#
-(void) gOnSecureCommunicationLibJsAPIDismissBrowser {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPIDismissBrowser");
    [self performSelectorOnMainThread:@selector(guiActionDone:) withObject:self waitUntilDone:NO];
}


-(void) gOnSecureCommunicationLibJsAPILocalStorageClear:(NSString*)host {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILocalStorageClear host:%@", host);
    [iLocalStorage removeAllObjects];
}

-(NSString*) gOnSecureCommunicationLibJsAPILocalStorageGetItem:(NSString*)host key:(NSString*)key {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILocalStorageGetItem host:%@ key:%@", host, key);

    NSString* value =  [iLocalStorage objectForKey:key];
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILocalStorageGetItem value:%@", value);
    return value;
}

-(void) gOnSecureCommunicationLibJsAPILocalStorageRemoveItem:(NSString*)host key:(NSString*)key {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILocalStorageRemoveItem host:%@ key:%@", host, key);    
    [iLocalStorage removeObjectForKey:key];
}

-(void) gOnSecureCommunicationLibJsAPILocalStorageSetItem:(NSString*)host key:(NSString*)key value:(NSString*)value {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILocalStorageSetItem host:%@ key:%@ value:%@", host, key, value);        
    [iLocalStorage setObject:value forKey:key];
}

-(void) gOnSecureCommunicationLibJsAPILaunchMenuItem:(NSString*)launchId {
    NSLog(@"TESTBrowserVC::gOnSecureCommunicationLibJsAPILaunchMenuItem launchId:%@", launchId);        
}


#
#pragma mark - Implementation of GOnSCLTrafficLaunchCommandControllerBrowserDelegate
#
- (void) browserDelegateIsActivated {
    NSLog(@"TESTBrowserVC::browserDelegateIsActivated");        
}

- (void) browserDelegateisNotActivated {
    NSLog(@"TESTBrowserVC::browserDelegateIsActivated");            
}

- (NSString*) browserDelegateStringByEvaluatingJavaScriptFromString:(NSString*)script {
    NSLog(@"TESTBrowserVC::browserDelegateStringByEvaluatingJavaScriptFromString"); 
    return [ivWebView stringByEvaluatingJavaScriptFromString:script];
}

- (void) browserDelegateLoadBegin {    
    NSLog(@"TESTBrowserVC::browserDelegateLoadBegin");            
}

- (void) browserDelegateLoadProgress:(NSInteger)progress {
    NSLog(@"TESTBrowserVC::browserDelegateLoadProgress progress:%d", progress);        
}

- (void) browserDelegateLoadEnd {
    NSLog(@"TESTBrowserVC::browserDelegateLoadEnd");            
}

- (void) browserDelegateContentIsNotHTML {
    NSLog(@"TESTBrowserVC::browserDelegateLoadProgress browserDelegateContentIsNotHTML");        
}

-(void) browserDelegateErrorLoadingContent {
    NSLog(@"TESTBrowserVC::browserDelegateLoadProgress browserDelegateErrorLoadingContent");        
}

@end
