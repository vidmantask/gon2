//
//  main.m
//  GOnSecureCommunicationLibTestsApp
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TESTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TESTAppDelegate class]));
    }
}
