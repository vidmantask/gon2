//
//  TESTAppDelegate.h
//  GOnSecureCommunicationLibTestsApp
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TESTViewController;

@interface TESTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TESTViewController *viewController;

@end
