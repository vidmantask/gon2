//
//  TESTBrowserVC.h
//  GOnSecureCommunicationLib
//
//  Created by gbuilder on 21/03/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TESTBrowserVC : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView* ivWebView;
}

@property (atomic, retain) IBOutlet UIWebView* ivWebView;



@end
