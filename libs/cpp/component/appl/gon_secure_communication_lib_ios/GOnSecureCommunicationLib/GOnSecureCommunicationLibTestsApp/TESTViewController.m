//
//  TESTViewController.m
//  GOnSecureCommunicationLibTestsApp
//
//  Created by gbuilder on 24/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import "TESTViewController.h"

@implementation TESTViewController
@synthesize iButtonBrowser;


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload {
    [super viewDidUnload];

    self.iButtonBrowser = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


-(IBAction)actionBrowse:(id)sender {
    UIStoryboard* storyboard = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"TESTBrowser.iPad" bundle:nil];
    }
    else {
        storyboard = [UIStoryboard storyboardWithName:@"TESTBrowser.iPhone" bundle:nil];
    }
    UIViewController* browserVC = [storyboard instantiateInitialViewController];
    [self presentViewController:browserVC animated:NO completion:^{
    }];
    
}
@end
