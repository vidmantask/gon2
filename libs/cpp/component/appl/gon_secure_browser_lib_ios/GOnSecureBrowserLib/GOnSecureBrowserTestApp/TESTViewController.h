//
//  TESTViewController.h
//  GOnSecureBrowserTestApp
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureCommunicationLibConfigProtocol.h"

#import "GOnSecureBrowserLib.h"
#import "GOnSecureBrowserLibProtocol.h"

GOnSecureCommunicationLib* cGOnSecureCommunicationLib;


@interface TESTViewController : UIViewController <GOnSecureBrowserLibProtocol> {
    GOnSecureBrowserLib* iSecureBrowser;
}

- (id)initWithCoder:(NSCoder *)aDecoder;

- (IBAction)ivButtonAction:(id)sender;

@end
