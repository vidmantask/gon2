//
//  TESTAppDelegate.h
//  GOnSecureBrowserTestApp
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GOnSecureCommunicationLib.h"
#import "TESTViewController.h"


@interface TESTAppDelegate : UIResponder <UIApplicationDelegate> {
    GOnSecureCommunicationLibConfig* iSecureCommunicationLibConfig;
    GOnSecureCommunicationLib* iSecureCommunicationLib;
}

@property (strong, nonatomic) UIWindow* window;

@end
