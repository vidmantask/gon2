//
//  main.m
//  GOnSecureBrowserTestApp
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TESTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TESTAppDelegate class]));
    }
}
