//
//  TESTViewController.m
//  GOnSecureBrowserTestApp
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "TESTViewController.h"
#import "GOnSecureBrowserVC.h"


@implementation TESTViewController


#
#pragma mark - Instance lifecycle
#
-(void) initInternal {
    iSecureBrowser = [[GOnSecureBrowserLib alloc] initWithDelegate:self secureCommunicationLib:cGOnSecureCommunicationLib];

    [GOnSecureBrowserLib linkHack];
    NSLog(@"SecureBrowserLib version:%@", [GOnSecureBrowserLib getVersion]);
    NSLog(@"SecureCommunicationLib version:%@", [GOnSecureCommunicationLib getVersion]);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternal];
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) dealloc {
    [iSecureBrowser release];
}

#
#pragma mark - View lifecycle
#
- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(IBAction)ivButtonAction:(id)sender {
    [iSecureBrowser presentViewController:self];
}


#
#pragma mark - Implementation of GOnSecureBrowserLibProtocol
#
-(GOnSecureBrowserLibGetCredietialsResponse*) gonSecureBrowserLibGetCredietials {
    return [[[GOnSecureBrowserLibGetCredietialsResponse alloc] initWithUsername:@"xx" password:@"xxxx"] autorelease];
}

-(NSString*) gonSecureBrowserLibGetConnectInfo {
    return @"H4sIAMWx4E4C/+1STU8bMRC98ytWXPZShRnbM2MjCPKneuyBO0JhW1KFJEo2Ffz7zgZEVdRy4VSpuwfb7808vxnP9+3jYjkOaOD80/nF1ePDqvsx7PbLzfqyxxn0V/OTi/2wm7BX4hRO5yddd7HYrNfDYlTo5ttuc9h243JcDZd9Gb7eHlZj3+2H1Qt/N6xun272w0JVe5X8lfqatNwp0Hf3m/142XuYIcMsmBlD3203O8VscNRr+MOwOYzPWlbJ8Wmr6XfP6Wd/1P58ff3lPWUPf9W9H8ftpDrVe/a2YL3s7KU58xPtHyAGC97oSrpiMcAgJnp2PldbQJkJzzztpj2WNxEIRlED0w81KBt+Z8EeOXTRggNwWOEj38fyEZy6coBirEnRk41SxWJsaEIzpjWHLhDb6DBkw6kRRJdD4VpbLIyGtQWJrJSaq8eQpAk0Ioos2RmJPpeQGvpYU0jEFTIiELdYWRtgJvf+PYccCqUUkFIujmtLGAs1FBtTacdXcGDt1AOtojhHyalHx14fq+aW1BB6NhQj1OqE2BuyWTiSVukZKfucNRgosReoTFJ8tGKSUGMHVGqUpLcnklypoFKSC5PJVEyoIHWaiFaPXuD/1PwzU+OMUynz/CbakQKWUpXWvBYXIYgNHFhqiIlrwtq4+Iy+hmKpCInoSVok0+AnVLyMAIUFAAA=";
}

-(void) gonSecureBrowserLibDismissed {
}


@end
