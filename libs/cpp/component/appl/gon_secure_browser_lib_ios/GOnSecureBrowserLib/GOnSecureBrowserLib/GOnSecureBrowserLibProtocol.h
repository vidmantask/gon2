//
//  GOnSecureBrowserLibProtocol.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GOnSecureBrowserLibGetCredietialsResponse : NSObject {
	NSString* iUsername;
	NSString* iPassword;
}
@property (readonly, getter=username) NSString* iUsername;
@property (readonly, getter=password) NSString* iPassword;
-(id) initWithUsername:(NSString*)username password:(NSString*)password;
@end



@protocol GOnSecureBrowserLibProtocol <NSObject>

-(NSString*) gonSecureBrowserLibGetConnectInfo;
-(GOnSecureBrowserLibGetCredietialsResponse*) gonSecureBrowserLibGetCredietials;
-(void) gonSecureBrowserLibDismissed;
@end
