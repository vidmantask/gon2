//
//  GOnSecureBrowserLibProtocol.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 23/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserLibProtocol.h"

@implementation GOnSecureBrowserLibGetCredietialsResponse
@synthesize iUsername;
@synthesize iPassword;

-(id) initWithUsername:(NSString*)username password:(NSString*)password {
    self = [super init];
    if (self) {
        iUsername = [username retain];
        iPassword = [password retain];
    }
    return self;
}

-(void) dealloc {
    [iUsername release];
    [iPassword release];
    [super dealloc];
}

@end
