//
//  GOnSecureBrowserMenuItemV.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserMenuItemV.h"


@interface GOnSecureBrowserMenuItemV () {
    id<GOnSecureBrowserMenuItemVProtocol> iDelegate;
    GOnSCLMenuItem* iMenuItem;
}
@property (retain) GOnSCLMenuItem* iMenuItem;
-(void) updateView;
- (UIImage *)convertToGrayscale:(UIImage*)baseImage;
@end




@implementation GOnSecureBrowserMenuItemV
@synthesize ivName;
@synthesize ivInfo;
@synthesize ivImage;
@synthesize ivImageBadge;
@synthesize ivStopButton;
@synthesize ivActivityIndicator;
@synthesize iMenuItem;


#
#pragma mark - Instance lifecycle
#
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void) dealloc {
    self.ivName = nil;
    self.ivInfo = nil;
    self.ivImage = nil;
    self.ivImageBadge = nil;
    self.ivStopButton = nil;
    self.ivActivityIndicator = nil;
    self.iMenuItem = nil;
}

#
#
#
-(void) setDelegate:(id<GOnSecureBrowserMenuItemVProtocol>)delegate {
    iDelegate = delegate;
}

-(void) updateMenuItem:(GOnSCLMenuItem*) menuItem {
    self.iMenuItem = menuItem;
    [self updateView];
}


#
#
#
-(void) updateView {
    self.ivName.text = iMenuItem.title;
    self.ivName.textColor = [UIColor blackColor];
    self.ivInfo.text = @"";

	self.ivImageBadge.hidden = YES;
    
	UIImage* iconImage = [iDelegate menuItemView:self getImage:@"menuItem" imageId:[iMenuItem iconId] imageStyle:@"iphone" imageSizeX:48 imageSizeY:48 imageFormat:@"png"]; 
	if(iconImage == nil) {
		iconImage = [UIImage imageNamed:@"g_menu_default_48x48"];
	}
	
    self.selectionStyle = UITableViewCellSelectionStyleBlue;
	if([iMenuItem disabled]) {
		iconImage = [self convertToGrayscale:iconImage];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
	}
	if (iconImage) {
		self.ivImage.image = iconImage;
		self.ivImage.alpha = 1;
	}
	
    
	[self.ivActivityIndicator stopAnimating];
	switch([iMenuItem launchState]) {
		case kLaunchStateNotLaunched:
			self.ivStopButton.enabled = NO;
			self.ivStopButton.hidden = YES;
			if (iMenuItem.disabled) {
				self.ivInfo.text = iMenuItem.tooltip;
				self.ivName.textColor = [UIColor grayColor];
				self.ivImage.alpha = 0.75f;
				self.ivImageBadge.hidden = NO;
			}
			break;
		case kLaunchStateLaunching:
			self.ivStopButton.enabled = NO;
			self.ivStopButton.hidden = NO;
			[self.ivActivityIndicator startAnimating];
			break;
		case kLaunchStateReady:
			self.ivStopButton.enabled = YES;
			self.ivStopButton.hidden = NO;
			break;
		case kLaunchStateClosing:
			self.ivStopButton.enabled = NO;
			self.ivStopButton.hidden = NO;
			[self.ivActivityIndicator startAnimating];
			break;
		case kLaunchStateClosed:
			self.ivStopButton.enabled = NO;
			self.ivStopButton.hidden = NO;
			break;
        case kLaunchStateError:
			self.ivStopButton.enabled = NO;
			self.ivStopButton.hidden = YES;
            self.ivInfo.text = @"error";
            break;
	}
}


#
#
#

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#
#
#
-(IBAction)stopButtonAction:(id)sender {
    if(iDelegate) {
        [iDelegate menuItemView:self terminate:[self.iMenuItem launchId]];
    }
}

#
#
#
typedef enum {
    ALPHA = 0,
    BLUE = 1,
    GREEN = 2,
    RED = 3
} PIXELS;

- (UIImage *)convertToGrayscale:(UIImage*)baseImage {
    CGSize size = [baseImage size];
    int width = size.width;
    int height = size.height;
	
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
	
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
	
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace, 
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
	
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [baseImage CGImage]);
	
    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
			
            // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
            uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
			
            // set the pixels to gray
            rgbaPixel[RED] = gray;
            rgbaPixel[GREEN] = gray;
            rgbaPixel[BLUE] = gray;
        }
    }
	
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
	
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
	
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image];
	
    // we're done with image now too
    CGImageRelease(image);
	
    return resultUIImage;
}
@end
