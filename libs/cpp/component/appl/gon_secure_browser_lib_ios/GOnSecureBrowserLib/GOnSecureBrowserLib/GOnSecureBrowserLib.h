//
//  GOnSecureBrowserLib.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import "GOnSecureBrowserLibProtocol.h"
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureCommunicationLibProtocol.h"


@interface GOnSecureBrowserLib : NSObject <GOnSecureCommunicationLibProtocol> {
}

//
// init
//
- (id) initWithDelegate:(id<GOnSecureBrowserLibProtocol>)delegate secureCommunicationLib:(GOnSecureCommunicationLib*) secureCommunicationLib;



//
// 
//
- (void) presentViewController:(UIViewController*)parentVC;


//
// This method should be called as part of the initialization of the app, 
// to ensure that the linker do not strip needed symbols.
//
+(void) linkHack;


//
// Get version of Secure Browser Lib
//
+(NSString*) getVersion;


@end
