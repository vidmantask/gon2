//
//  GOnSecureBrowserVCiPhonePortrait.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 19/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserVCiPhone.h"
#import "GOnSecureBrowserMenuVC.h"


@interface GOnSecureBrowserVCiPhone () {
    BOOL iMenuIsShown;
}
@end


@implementation GOnSecureBrowserVCiPhone

#
#pragma mark - Instance lifecycle
#
-(void) initInternal {
    [super initInternal];
    iMenuIsShown = false;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternal];
    }
    return self;
}

-(void) dealloc {    
    [super dealloc];
}

-(void) prepareForMenuVC:(UIStoryboardSegue *)segue menuViewController:(GOnSecureBrowserMenuVC*) menuViewController {
    NSLog(@"prepareForMenuVC self:%@", self);
    iMenuIsShown = true;
}

-(void) dismissMenuVC {
    NSLog(@"dismissMenuVC self:%@ iMenuIsShown:%d", self, iMenuIsShown );
    if(iMenuIsShown) {
        NSLog(@"dismissMenuVC dismissed");
        [self.navigationController dismissModalViewControllerAnimated:YES];
        iMenuIsShown = false;
    }
    [super dismissMenuVC];
}

#
#pragma mark - View lifecycle
#
 - (void)viewDidLoad {
    [super viewDidLoad];
 }

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
