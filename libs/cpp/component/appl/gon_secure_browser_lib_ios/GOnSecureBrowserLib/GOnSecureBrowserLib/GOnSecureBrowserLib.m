//
//  GOnSecureBrowserLib.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/11/11.
//  Copyright (c) 2011 Giritech. All rights reserved.
//

#import "GOnSecureBrowserLib.h"
#import "GOnSecureBrowserVC.h"
#import "GOnSecureBrowserVCiPad.h"
#import "GOnSecureBrowserVCiPhone.h"
#import "GOnSecureBrowserVCProtocol.h"
#import "GOnSecureBrowserRootVC.h"
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureBrowserSpecialProtocol.h"
#import "GOnSecureBrowserMenuVC.h"
#import "GOnSecureBrowserMenuItemV.h"
#import "GOnSecureBrowserStatusV.h"
#import "GOnSecureBrowserLibVersion.h"


@interface GOnSecureBrowserLib() <GOnSecureBrowserVCProtocol> {
    NSMutableDictionary* iImageCache;
	NSMutableSet* iImageCachePending;
    BOOL iDoReconnect;
    GOnSecureCommunicationLib* iSecureCommunicationLib;
    GOnSecureBrowserVC* iSecureBrowserVCDefault;
    NSMutableDictionary* iSecureBrowserVCLaunches;
    GOnSecureBrowserVC* iSecureBrowserVCCurrent;
    UINavigationController* iRootVC;
    
    UIViewController* iParentVC;
    GOnSCLDialogShowMenuArgs* iCurrentMenu;
    NSMutableArray* iSecureBrowserVCLaunchesDead;
    
    BOOL iIsTerminatingAllLaunches;
    id<GOnSecureBrowserLibProtocol> iDelegate;
    
    BOOL iDefaultBrowserHasBeenLaunched;
}
@property (retain) GOnSCLDialogShowMenuArgs* iCurrentMenu;
@property (retain) GOnSCLTrafficLaunchCommandArgs* iCurrentLaunch;
@property (retain) UINavigationController* iRootVC;

- (void) connectionUpdate;
- (GOnSecureBrowserVC*) generateSecureBrowserVC;

@end



@implementation GOnSecureBrowserLib
@synthesize iCurrentMenu;
@synthesize iCurrentLaunch;
@synthesize iRootVC;

#
#pragma mark - Instance lifecycle
#
- (id) initWithDelegate:(id<GOnSecureBrowserLibProtocol>)delegate secureCommunicationLib:(GOnSecureCommunicationLib*) secureCommunicationLib {
    self = [super init];
    if (self) {
        iDelegate = delegate;
        iSecureBrowserVCDefault = [[self generateSecureBrowserVC] retain];
        iSecureCommunicationLib = secureCommunicationLib;
        [iSecureCommunicationLib setDelegate:self];
        
        [GOnSecureBrowserSpecialProtocolHTTP registerSpecialProtocol];
        [GOnSecureBrowserSpecialProtocolHTTP setProxyDirect];
        
        iCurrentMenu = nil;
        iCurrentLaunch = nil;
        iSecureBrowserVCCurrent = nil;
        iSecureBrowserVCLaunches = [[NSMutableDictionary alloc] init];
        iSecureBrowserVCLaunchesDead = [[NSMutableArray alloc] init];
        
        iImageCache = [[NSMutableDictionary alloc] init];
		iImageCachePending = [[NSMutableSet alloc] init];
        
        iDoReconnect = false;
        iIsTerminatingAllLaunches = false;
        
        iDefaultBrowserHasBeenLaunched = false;
    }
    return self;
}

- (void) dealloc {
    [iSecureBrowserVCDefault release];
    [iSecureBrowserVCLaunches release];
    [iSecureBrowserVCLaunchesDead release];
    [iImageCache release];
    [iImageCachePending release];
    iSecureBrowserVCDefault = nil;
    self.iCurrentMenu = nil;
    self.iCurrentLaunch = nil;
    self.iRootVC = nil;
    [super dealloc];
}



#
#pragma mark - Secure Browser VC handling
#
-(UIViewController*) generateInitialVC {
    UIStoryboard* storyboard = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"GOnSecureBrowser.iPad" bundle:nil];
    }
    else {
        storyboard = [UIStoryboard storyboardWithName:@"GOnSecureBrowser.iPhone" bundle:nil];
    }
    return [storyboard instantiateInitialViewController];
}

-(GOnSecureBrowserVC*) generateSecureBrowserVC {
    UIStoryboard* storyboard = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"GOnSecureBrowser.iPad" bundle:nil];
    }
    else {
        storyboard = [UIStoryboard storyboardWithName:@"GOnSecureBrowser.iPhone" bundle:nil];
    }
    GOnSecureBrowserVC* iSecureBrowserVC = (GOnSecureBrowserVC*)[storyboard instantiateViewControllerWithIdentifier:@"GOnSecureBrowserVC"];
    [iSecureBrowserVC setDelegate:self];
    return iSecureBrowserVC;
}

- (void) presentViewController:(UIViewController*)parentVC {
    [iSecureCommunicationLib gonAsyncRestartIfNotRunning];

    iParentVC = parentVC;
    iDoReconnect = true;
    iIsTerminatingAllLaunches = false;
    if (iSecureBrowserVCCurrent == nil) {
        iSecureBrowserVCCurrent = iSecureBrowserVCDefault;
    }
    
    self.iRootVC = [self generateInitialVC];
    [iParentVC presentViewController:self.iRootVC animated:NO completion:^{
        [self.iRootVC pushViewController:iSecureBrowserVCCurrent animated:NO];
    }];
}

- (void) connectionUpdateConnected {
    [iSecureBrowserVCCurrent enterConnected];
}

- (void) connectionUpdateNotConnected:(NSString*)message {
    [iSecureBrowserVCCurrent enterNotConnected:message];
}

- (void) connectionUpdateNotConnectedConnecting:(NSString*)message {
    [iSecureBrowserVCCurrent enterNotConnectedConnecting:message];
}

-(void) connectionReconnect {
    [iSecureBrowserVCCurrent enterNotConnectedConnecting:NSLocalizedString(@"Reconnecting", @"")];
    [iSecureCommunicationLib connectionConnect];
}

-(void) connectionConnect {
    [iSecureBrowserVCCurrent enterNotConnectedConnecting:NSLocalizedString(@"Connecting", @"")];
    [iSecureCommunicationLib connectionConnect];
}

-(void) connectionReconnectWait:(NSNumber*)secToReconnect {
    if([secToReconnect intValue] > 0) {
        [iSecureBrowserVCCurrent enterNotConnected:[NSString stringWithFormat:NSLocalizedString(@"Reconnect in %d sec", @""), [secToReconnect intValue]]];
        [self performSelector:@selector(connectionReconnectWait:) withObject:[NSNumber numberWithInt:[secToReconnect intValue]-1] afterDelay:1];
        return;
    }
    [self connectionReconnect];
}

- (void) connectionUpdate {
    switch ([iSecureCommunicationLib connectionGetState]) {
        case kGOnSCLConnectionStateInitializing:
        case kGOnSCLConnectionStateReadyToConnect:
        case kGOnSCLConnectionStateClosed:
            [self connectionConnect];
            break;
        case kGOnSCLConnectionStateWaitingForNetwork:
        case kGOnSCLConnectionStateConnecting:
        case kGOnSCLConnectionStateClosing:
        case kGOnSCLConnectionStateUnknown:
            [self connectionUpdateNotConnected:@""];
            break;
        case kGOnSCLConnectionStateConnected:
            [self connectionUpdateConnected];
            break;
        default:
            break;
    }
}

-(void) activateSecureBrowser:(NSString*)launchId launchArgs:(GOnSCLTrafficLaunchCommandArgs*)args {
    GOnSecureBrowserVC* secureBrowserVC = [iSecureBrowserVCLaunches valueForKey:launchId];
    if(secureBrowserVC == nil) {
        secureBrowserVC = [self generateSecureBrowserVC];
        [secureBrowserVC setLaunchId:launchId];
    }
    [iSecureBrowserVCLaunches setObject:secureBrowserVC forKey:launchId];
    [self.iRootVC popToRootViewControllerAnimated:NO];

    iSecureBrowserVCCurrent = secureBrowserVC;
    if(args) {
        [iSecureBrowserVCCurrent updateLaunch:args];
    }
    else {
        [iSecureBrowserVCCurrent updateLaunch];
    }
    [iSecureBrowserVCCurrent updateMenu:iCurrentMenu];
    [self.iRootVC pushViewController:iSecureBrowserVCCurrent animated:NO];
    [self connectionUpdate];
}

-(void) showCurrentSecureBrowser:(GOnSCLTrafficLaunchCommandArgs*)args {
}
     
-(void) activateSecureBrowser:(NSString*)launchId {
    [self activateSecureBrowser:launchId launchArgs:nil];
}

-(void) dismissSecureBrowserIfCurrent:(NSString*)launchId {
    GOnSecureBrowserVC* secureBrowserVC = [iSecureBrowserVCLaunches valueForKey:launchId];
    if(secureBrowserVC == iSecureBrowserVCCurrent) {
        [self.iRootVC popToRootViewControllerAnimated:NO];
    }
}

-(void) activateSecureBrowserDiferent:(NSString*)launchId {
    GOnSecureBrowserVC* secureBrowserVC = [iSecureBrowserVCLaunches valueForKey:launchId];
    if(secureBrowserVC == iSecureBrowserVCCurrent) {
        iSecureBrowserVCCurrent = iSecureBrowserVCDefault;
        [self.iRootVC pushViewController:iSecureBrowserVCCurrent animated:NO];
        [self connectionUpdate];
        [secureBrowserVC updateLaunch];
        [secureBrowserVC updateMenu:iCurrentMenu];
    }
}


-(void) launchBrowserDefault {
    if(iDefaultBrowserHasBeenLaunched) {
        return;
    }
    if(iSecureBrowserVCCurrent != iSecureBrowserVCDefault) {
        return;
    }
    
    for(GOnSCLMenuItem* menuItem in [iCurrentMenu menuItems]) {
        if(menuItem.tagBrowserDefault) {
             [iSecureCommunicationLib dialogMenuItemLaunch:menuItem.launchId];
            iDefaultBrowserHasBeenLaunched = YES;
            return;
        }
    }    
}

#
#pragma mark - Implementation of GOnSecureBrowserVCProtocol
#
-(void) gonSecureBrowserVCProtocolDismissVC {
    iDoReconnect = false;
    iIsTerminatingAllLaunches = true;

    [self.iRootVC popToRootViewControllerAnimated:NO];
    if(iDelegate) {
        [iDelegate gonSecureBrowserLibDismissed];
    }
    for(NSString* launchId in [iSecureBrowserVCLaunches allKeys]) {
        [self gonSecureBrowserVCProtocolTerminate:launchId];
    }
    iSecureBrowserVCCurrent = iSecureBrowserVCDefault;
    [iSecureCommunicationLib connectionDisconnect];
    
    [iParentVC dismissViewControllerAnimated:NO completion:nil];
}

-(BOOL) gonSecureBrowserVCProtocolIsConnected {
    switch ([iSecureCommunicationLib connectionGetState]) {
        case kGOnSCLConnectionStateConnected:
            return YES;
        default:
            return NO;
    }
}

-(void) gonSecureBrowserVCProtocolSelect:(NSString*)launchId {
    GOnSecureBrowserVC* secureBrowserVC = [iSecureBrowserVCLaunches valueForKey:launchId];
    if(secureBrowserVC) {
        [self activateSecureBrowser:launchId];
    }
    else {
        [iSecureCommunicationLib dialogMenuItemLaunch:launchId];
    }
}

-(void) gonSecureBrowserVCProtocolTerminate:(NSString*)launchId {
    [self dismissSecureBrowserIfCurrent:launchId];
    if(!iIsTerminatingAllLaunches) {
        [self activateSecureBrowserDiferent:launchId];
    }
    [iSecureCommunicationLib trafficLaunchCommandTerminated:launchId];
    
    GOnSecureBrowserVC* secureBrowserVC = [iSecureBrowserVCLaunches valueForKey:launchId];
    if(secureBrowserVC) {
        [iSecureBrowserVCLaunchesDead addObject:secureBrowserVC];
        [iSecureBrowserVCLaunches removeObjectForKey:launchId];
    }
}

-(UIImage*) gonSecureBrowserVCProtocolGetImage:(NSString*)requestId
                                       imageId:(NSString*)imageId 
                                    imageStyle:(NSString*)imageStyle 
                                    imageSizeX:(NSInteger)imageSizeX 
                                    imageSizeY:(NSInteger)imageSizeY 
                                   imageFormat:(NSString*)imageFormat {
    
    
    NSString* imageCacheKey = [NSString stringWithFormat:@"%@_%@", requestId, imageId]; 
	UIImage* image = (UIImage*)[iImageCache objectForKey:imageCacheKey];
	if(image) {
		return image;
	}
	
	// Check to see if we are already waiting for this image
	if([iImageCachePending containsObject:imageCacheKey]) {
		return nil;
	}
    
	[iImageCachePending addObject:imageCacheKey];
    [iSecureCommunicationLib dialogGetImage:requestId imageId:imageId imageSizeX:imageSizeX imageSizeY:imageSizeY];
    return nil;
}

-(GOnSCLTrafficLaunchState) getLaunchState:(NSString*)launchId {
    return (GOnSCLTrafficLaunchState)[iSecureCommunicationLib dialogMenuItemGetStatus:launchId];
}

#
#pragma mark - Implementation of GOnSecureCommunicationLibProtocol
#
- (NSString*) gonSCLGetConnectInfo {
    NSLog(@"GOnSecureBrowserLib.gonSCLGetConnectInfo");
    return [iDelegate gonSecureBrowserLibGetConnectInfo];
}

- (void) gonSCLConnectStateConnectingWithConnectionTitle:(NSString*) connectionTitle {    
    [self connectionUpdateNotConnectedConnecting:NSLocalizedString(@"Connecting", @"")];
}

- (void) gonSCLConnectStateConnectingFailedWithMessage:(NSString*) message {
}

- (void) gonSCLConnectStateConnected {
    [self connectionUpdateConnected];
}

- (void) gonSCLConnectStateClosing {
    [self connectionUpdateNotConnected:NSLocalizedString(@"Closing", @"")];
}

- (void) gonSCLConnectStateClosed {
    [self connectionUpdateNotConnected:NSLocalizedString(@"Closed", @"")];
    if(iDoReconnect) {
        if (iSecureBrowserVCCurrent) {
            [self performSelector:@selector(connectionReconnectWait:) withObject:[NSNumber numberWithInt:10] afterDelay:2];
        }
    } 
    if(iIsTerminatingAllLaunches) {
        NSLog(@"gonSCLConnectStateClosed closing async");
        [iSecureCommunicationLib gonAsyncStop];
        iDefaultBrowserHasBeenLaunched = NO;
    }
}

- (void) gonSCLUserRequestLoginWithMessage:(NSString*)message {
    if(iSecureBrowserVCCurrent) {
        GOnSecureBrowserLibGetCredietialsResponse* credentials = [iDelegate gonSecureBrowserLibGetCredietials];
        [iSecureCommunicationLib userResponseLogin:[credentials username] password:[credentials password]];
    }
    else {
        [iSecureCommunicationLib userResponseLoginCancelled];
    }
}   

- (void) gonSCLDialogShowLicenseInfo:(GOnSCLDialogShowLicenseInfoArgs*)arg {
    NSLog(@"gonSCLDialogShowLicenseInfo");
}

- (void) gonSCLDialogMenuUpdate {
    if(iSecureBrowserVCCurrent) {
        [iSecureBrowserVCCurrent updateMenu];
        [self launchBrowserDefault];
    }    
}

- (void) gonSCLDialogShowMenu:(GOnSCLDialogShowMenuArgs*)args {
    self.iCurrentMenu = args;
    if(iSecureBrowserVCCurrent) {
        [iSecureBrowserVCCurrent updateMenu:args];
        [self launchBrowserDefault];
    }    
}

- (void) gonSCLDialogImageFound:(GOnSCLDialogImageArgs*)args {
    NSString* imageCacheKey = [NSString stringWithFormat:@"%@_%@", [args requestId], [args imageId]]; 
	if ([args image]) {
		[iImageCache setObject:[args image] forKey:imageCacheKey];
		[iImageCachePending removeObject:imageCacheKey];
	}
    if(iSecureBrowserVCCurrent) {
        [iSecureBrowserVCCurrent updateMenu];
    }    
}

- (void) gonSCLDialogImageNotFound:(GOnSCLDialogImageArgs*)args {
}

- (void) gonSCLInfo:(GOnSCLInfoArgs*)args {
    NSLog(@"gonSCLInfo message:%@", [args message]);
}

- (void) gonSCLInfoError:(GOnSCLInfoArgs*)args {
    NSLog(@"gonSCLInfoError message:%@", [args message]);
}

- (void) gonSCLInfoWarning:(GOnSCLInfoArgs*)args {
    NSLog(@"gonSCLInfoWarning message:%@", [args message]);
}

- (void) gonSCLEnrollRequest:(GOnSCLEnrollRequestArgs*)args {
}

- (void) gonSCLEnrollResponse:(GOnSCLEnrollResponseArgs*)args {
}

- (void) gonSCLEnrollGetStatusResponse:(GOnSCLEnrollGetStatusResponseArgs*)args {
}

- (void) gonSCLTraffficLaunchStatusChanged:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    if(iSecureBrowserVCCurrent) {
        [iSecureBrowserVCCurrent updateMenuItem:args];
    }    
}

- (void) gonSCLTraffficLaunchCommand:(GOnSCLTrafficLaunchCommandArgs*)args {
    NSLog(@"gonSCLTraffficLaunchCommand");
    switch([args launchType]) {
        case kLaunchCommandTypeBrowseProxySocks:
        case kLaunchCommandTypeBrowseProxyHTTP:
        case kLaunchCommandTypeBrowse: {
            [self activateSecureBrowser:[args launchId] launchArgs:args];
            [iSecureCommunicationLib trafficLaunchCommandRunning:[args launchId]];
            break;
        }
        default:
            [iSecureCommunicationLib trafficLaunchCommandTerminatedWithError:[args launchId] errorMessage:@"Launch type not supported"];
            break;
    }
}

- (void) gonSCLTraffficLaunchCommandTerminate:(NSString*) launchId {
    NSLog(@"gonSCLTraffficLaunchCommandTerminate");
    if(iSecureBrowserVCCurrent) {
        [iSecureCommunicationLib trafficLaunchCommandTerminated:launchId];
    }    
}

- (void) gonSCLConnectInfoUpdated {
    NSLog(@"gonSCLConnectInfoUpdated");
}

- (void) gonSCLKeyStoreReset {
}

- (void) gonSCLVersionError:(NSString*)serverVersion {
}

- (void) gonAsyncRunning {
    [self performSelectorOnMainThread:@selector(connectionUpdate) withObject:nil waitUntilDone:NO];
}

- (void) gonAsyncStopped {
}


#
#
#
+(NSString*) getVersion {
    return [NSString stringWithFormat:@"%@", kGOnSecureBrowserLibVersion];
}

+(void) linkHack {
    [GOnSecureBrowserVC class];
    [GOnSecureBrowserVCiPad class];
    [GOnSecureBrowserVCiPhone class];
    [GOnSecureBrowserRootVC class];
    [GOnSecureBrowserMenuVC class];
    [GOnSecureBrowserMenuItemV class];
    [GOnSecureBrowserStatusV  class];
}
@end
