//
//  GOnSecureBrowserStatusV.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 27/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserStatusV.h"
#import <QuartzCore/QuartzCore.h>


@interface GOnSecureBrowserStatusV () {
    BOOL ivViewStatusAnimationHideRunning;
    BOOL ivViewStatusAnimationHidePending;
    BOOL ivViewStatusAnimationShowRunning;
    BOOL ivViewStatusAnimationShowPending;
    BOOL ivViewStatusShown;
    
    UILabel* ivMessage;
    UIImageView* ivImage;
    UIImageView* ivImageStatus;
}
@property (retain) UILabel* ivMessage;
@property (retain) UIImageView* ivImage;
@property (retain) UIImageView* ivImageStatus;

-(void) stateViewShow;
-(void) stateViewHide;

@end


@implementation GOnSecureBrowserStatusV
@synthesize ivMessage;
@synthesize ivImage;
@synthesize ivImageStatus;


#
#pragma mark - Instance lifecycle
#
-(void) initInternal {
    self.backgroundColor = [UIColor clearColor];
    
    self.ivImage = [[UIImageView alloc] initWithFrame: [self bounds]];
    self.ivImage.image = [UIImage imageNamed:@"g_status_background"];

    self.ivImageStatus = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"g_status_closed"]];
    self.ivImageStatus.center = CGPointMake(26, 23);
    
    self.ivMessage = [[UILabel alloc] initWithFrame:[self bounds]];
    self.ivMessage.highlightedTextColor = [UIColor whiteColor];
    self.ivMessage.font = [UIFont boldSystemFontOfSize: 12.0];
    self.ivMessage.adjustsFontSizeToFitWidth = YES;
    self.ivMessage.minimumFontSize = 10.0;
	self.ivMessage.textAlignment = UITextAlignmentCenter;
	self.ivMessage.backgroundColor = [UIColor clearColor];
	self.ivMessage.textColor = [UIColor colorWithRed:255/224 green:255/224 blue:255/224 alpha:1];

    [self addSubview: self.ivImage];
    [self addSubview: self.ivImageStatus];
    [self addSubview: self.ivMessage];
    
    self.ivMessage.text = @"";
    
    ivViewStatusAnimationHideRunning = NO;
    ivViewStatusAnimationHidePending = NO;
    ivViewStatusAnimationShowRunning = NO;
    ivViewStatusAnimationShowPending = NO;
    ivViewStatusShown = NO;
    iState = kGOnSecureBrowserStatusVStateInit;
    
    self.alpha = 0;
    CGRect rect = self.frame;
    rect.origin.y += rect.size.height;
    [self setFrame:rect];
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternal];
    }
    return self;
}

-(void) dealloc {
    self.ivImage = nil;
    self.ivMessage = nil;
    [super dealloc];
}


#
#pragma mark - View lifecycle
#
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#
#pragma mark - Handle state changes
#
-(void) stateViewInitBussy {
    self.ivImageStatus.image = [UIImage imageNamed:@"g_status_waiting"];
    CABasicAnimation* theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration=1.8;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.0];
    theAnimation.toValue=[NSNumber numberWithFloat:0.5];
    [self.ivImageStatus.layer addAnimation:theAnimation forKey:@"animateOpacity"]; 

    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.scale"];
    theAnimation.duration=1.8;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSNumber numberWithFloat:1.00];
    theAnimation.toValue=[NSNumber numberWithFloat:0.90];
    [self.ivImageStatus.layer addAnimation:theAnimation forKey:@"animateScale"]; 
}

-(void) stateViewInitConnected {
    [self.ivImageStatus.layer removeAnimationForKey:@"animateOpacity"];
    [self.ivImageStatus.layer removeAnimationForKey:@"animateScale"];
    self.ivImageStatus.image = [UIImage imageNamed:@"g_status_connected"];
    [self performSelector:@selector(stateViewDoneConnected) withObject:nil afterDelay:2];
}

-(void) stateViewDoneConnected {
    if(iState == kGOnSecureBrowserStatusVStateConnected) {
        [self stateViewHide];
    }
}
 
-(void) stateViewInitClosed {
    [self.ivImageStatus.layer removeAnimationForKey:@"animateOpacity"];
    [self.ivImageStatus.layer removeAnimationForKey:@"animateScale"];
    self.ivImageStatus.image = [UIImage imageNamed:@"g_status_closed"];
}

-(void) toStateBussy {
    switch (iState) {
        case kGOnSecureBrowserStatusVStateBusy:
            break;
        case kGOnSecureBrowserStatusVStateInit:
        case kGOnSecureBrowserStatusVStateConnected:
        case kGOnSecureBrowserStatusVStateClosed:
            [self stateViewInitBussy];
            [self stateViewShow];
            break;
    }
}

-(void) toStateConnected {
    switch (iState) {
        case kGOnSecureBrowserStatusVStateInit:
        case kGOnSecureBrowserStatusVStateBusy:
        case kGOnSecureBrowserStatusVStateClosed:
            [self stateViewInitConnected];
            break;
        case kGOnSecureBrowserStatusVStateConnected:
            break;
    }
}

-(void) toStateClosed {
    switch (iState) {
        case kGOnSecureBrowserStatusVStateInit:
        case kGOnSecureBrowserStatusVStateBusy:
        case kGOnSecureBrowserStatusVStateConnected:
            [self stateViewInitClosed];
            [self stateViewShow];
            break;
        case kGOnSecureBrowserStatusVStateClosed:
            break;
    }
}

- (void) setState:(GOnSecureBrowserStatusVStateType)state message:(NSString*)message {
    self.ivMessage.text = message;
    if(state == iState) {
        return;
    }
    switch (state) {
        case kGOnSecureBrowserStatusVStateBusy:
            [self toStateBussy];
            break;
        case kGOnSecureBrowserStatusVStateConnected:
            [self toStateConnected];
            break;
        case kGOnSecureBrowserStatusVStateClosed:
            [self toStateClosed];
            break;
        default:
            break;
    }
    iState = state;
}


-(void) stateViewShow {
    if(ivViewStatusAnimationHideRunning) {
        ivViewStatusAnimationShowPending = YES;
        return;
    }
    if(ivViewStatusShown) {
        return;
    }
    if(ivViewStatusAnimationShowRunning) {
        return;
    }
    ivViewStatusAnimationShowRunning = YES;
    
    CGRect rect = self.frame;
    self.hidden = NO;
    
    [UIView beginAnimations:@"ShowAnimation" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(stateViewShowAnimationDone)];
    
    rect.origin.y -= rect.size.height;
    [self setFrame:rect];
    self.alpha = 1;
    [UIView commitAnimations];
}

-(void) stateViewShowAnimationDone {
    ivViewStatusShown = YES;
    ivViewStatusAnimationShowRunning = NO;
    if(ivViewStatusAnimationHidePending) {
        ivViewStatusAnimationHidePending = NO;
        [self stateViewHide];
    }
}


-(void) stateViewHide {
    if(ivViewStatusAnimationShowRunning) {
        ivViewStatusAnimationHidePending = YES;
        return;
    }
    if(!ivViewStatusShown) {
        return;
    }
    if(ivViewStatusAnimationHideRunning) {
        return;
    }
    ivViewStatusAnimationHideRunning = YES;
    
    CGRect rect = self.frame;
    
    [UIView beginAnimations:@"HideAnimation" context:nil];
    [UIView setAnimationDuration:0.7];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(stateViewHideAnimationDone)];
    
    rect.origin.y += rect.size.height;
    [self setFrame:rect];
    self.alpha = 0;
    [UIView commitAnimations];
}

-(void) stateViewHideAnimationDone {
    ivViewStatusShown = NO;
    self.hidden = YES;
    ivViewStatusAnimationHideRunning = NO;
    if(ivViewStatusAnimationShowPending) {
        ivViewStatusAnimationShowPending = NO;
        [self stateViewShow];
    }
}


@end
