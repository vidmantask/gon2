//
//  GOnSecureBrowserVC.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserVC.h"
#import "GOnSecureBrowserSpecialProtocol.h"
#import "GOnSecureBrowserMenuVC.h"


//
// Private methods forward definition
//
@interface GOnSecureBrowserVC () {
    GOnSecureBrowserVCGuiState iGuiState; 
    GOnSCLDialogShowMenuArgs* iCurrentMenu;
    GOnSecureBrowserMenuVC* iCurrentMenuViewController;
    BOOL iReloadOnDidAppear;
}
-(void) updateView;
-(void) enterConnected;
-(void) enterNotConnected:(NSString*)message;
-(void) enterConnectedWebLoading;
-(void) enterConnectedWebLoadingDone;
-(void) updateProxyConfiguration;
-(void) launchStart;
@end


//
//
//
@implementation GOnSecureBrowserVC

@synthesize ivWebView;
@synthesize ivBarButtonDMEMenu;
@synthesize ivBarButtonBack;
@synthesize ivBarButtonForward;
@synthesize ivBarButtonRefresh;
@synthesize ivBarButtonBookmarks;
@synthesize ivViewStatusNew;
@synthesize iCurrentMenu;
@synthesize iGOnSCLTrafficLaunchCommandArgs;
@synthesize iLaunchId;


#
#pragma mark - Instance lifecycle
#
-(void) initInternal {
    iGuiState = kGuiStateInit;
    iDelegate = nil;
    iGuiState = kGuiStateInit;
    iCurrentMenu = nil;
    iCurrentMenuViewController = nil;
    iLaunchId = nil;
    iReloadOnDidAppear = NO;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternal];
    }
    return self;
}

-(void) setDelegate:(id<GOnSecureBrowserVCProtocol>)delegate{
    iDelegate = delegate;
}

-(void) setLaunchId:(NSString*)launchId {
    self.iLaunchId = launchId;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) dealloc {
    self.iCurrentMenu = nil;
    [super dealloc];
}

-(void) prepareForMenuVC:(UIStoryboardSegue *)segue menuViewController:(GOnSecureBrowserMenuVC*) menuViewController {
    // Overwritten
}

-(void) dismissMenuVC {
    // Overwritten
    iCurrentMenuViewController = nil;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    iCurrentMenuViewController = [segue destinationViewController];
    [self prepareForMenuVC:segue menuViewController:iCurrentMenuViewController];
    [iCurrentMenuViewController setDelegate:self];
    [self updateView];
    [self updateMenu];
}

#
#pragma mark - View lifecycle
#
- (void)viewDidLoad {
    [self dismissMenuVC];
    [super viewDidLoad];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    self.ivWebView = nil;
    self.ivBarButtonDMEMenu = nil;
    self.ivBarButtonBack = nil;
    self.ivBarButtonForward = nil;
    self.ivBarButtonRefresh = nil;
    self.ivBarButtonBookmarks = nil;
    self.ivViewStatusNew = nil;
    self.iLaunchId = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    NSLog(@"GOnSecureBrowserVC.viewWillAppear %@", self);
    self.navigationController.navigationBar.hidden = YES;
    self.navigationItem.title = @"back";
    [super viewWillAppear:animated];
}

-(void) viewDidAppear:(BOOL)animated {
    [self updateView];
    if(iReloadOnDidAppear) {
        [self launchStart];
    }
    iReloadOnDidAppear = NO;
}

-(void) viewWillDisappear:(BOOL)animated {
    NSLog(@"GOnSecureBrowserVC.viewWillDisappear");
}

-(void) viewDidDisappear:(BOOL)animated {
    NSLog(@"GOnSecureBrowserVC.viewDidDisappear");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void) updateViewNotConnected {
    self.ivBarButtonBack.enabled = NO;
    self.ivBarButtonForward.enabled = NO;
    self.ivBarButtonRefresh.enabled = NO;
    self.ivBarButtonBookmarks.enabled = NO;
}

-(void) updateViewConnected {
    if (ivWebView.canGoBack) {
        self.ivBarButtonBack.enabled = YES;
    }
    else {
        self.ivBarButtonBack.enabled = NO;
    }
    if (ivWebView.canGoForward) {
        self.ivBarButtonForward.enabled = YES;
    }
    else {
        self.ivBarButtonForward.enabled = NO;
    }
    self.ivBarButtonRefresh.enabled = YES;

    self.ivBarButtonBookmarks.enabled = NO;
    if(iCurrentMenuViewController == nil) {
        self.ivBarButtonBookmarks.enabled = YES;
    }
}

-(void) updateViewConnectedWebLoading {
    self.ivBarButtonBack.enabled = NO;
    self.ivBarButtonForward.enabled = NO;
    self.ivBarButtonRefresh.enabled = NO;
    self.ivBarButtonBookmarks.enabled = NO;
}

- (void) updateView {
    switch (iGuiState) {
        case kGuiStateInit:
        case kGuiStateNotConnected:
        case kGuiStateNotConnectedConnecting:
            [self updateViewNotConnected];
            break;
        case kGuiStateConnected:
            [self updateViewConnected];
            break;
        case kGuiStateConnectedWebLoading:
            [self updateViewConnectedWebLoading];
            break;
    }
}



#
#pragma mark - View actions
#
-(IBAction)ivBarButtonDMEMenuAction:(id)sender {
    [iDelegate gonSecureBrowserVCProtocolDismissVC];
}

-(void) updateMenuItemsStatus {
    if(iDelegate) {
        for(GOnSCLMenuItem* menuItem in [iCurrentMenu menuItems]) {
            [menuItem setLaunchState:[iDelegate getLaunchState:[menuItem launchId]] launchMessage:@""];
        }
    }
}

-(void) updateMenu {
    [self updateMenuItemsStatus];
    if(iCurrentMenuViewController) {
        [iCurrentMenuViewController updateMenu:self.iCurrentMenu];
    }
}

-(void) updateMenu:(GOnSCLDialogShowMenuArgs*)menu {
    self.iCurrentMenu = menu;
    [self updateMenu];
}

- (void) updateMenuItem:(GOnSCLTrafficLaunchStatusChangedArgs*)args {
    [self updateMenuItemsStatus];
    for(GOnSCLMenuItem* menuItem in [iCurrentMenu menuItems]) {
        if([[menuItem launchId] isEqualToString:[args launchId]]) {
            [menuItem setLaunchState:[args state] launchMessage:[args message]];
        }
    }
    if(iCurrentMenuViewController) {
        [iCurrentMenuViewController updateMenu:self.iCurrentMenu];
    }
}


#
#pragma mark - Implementation of UIWebViewDelegate
#
-(void) showDefaultPage {
    self.ivWebView.scalesPageToFit = false;
    [ivWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"not_connected" ofType:@"html"]isDirectory:NO]]];  
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if([[request URL] isFileURL]) {
        return YES;
    }

    switch (iGuiState) {
        case kGuiStateInit:
        case kGuiStateNotConnected:
        case kGuiStateNotConnectedConnecting:
            [self performSelectorOnMainThread:@selector(showDefaultPage) withObject:nil waitUntilDone:NO];
            return NO;
        case kGuiStateConnected:
        case kGuiStateConnectedWebLoading:
            return YES;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"GOnSecureBrowserVC.webViewDidStartLoad");
    [self enterConnectedWebLoading];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"GOnSecureBrowserVC.webViewDidFinishLoad");
    [self enterConnectedWebLoadingDone];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"GOnSecureBrowserVC.didFailLoadWithError");
    [self enterConnectedWebLoadingDone];
}


#
#pragma mark - Handling of secure connection events
#
-(void) enterConnected {
    [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateConnected message:NSLocalizedString(@"Secure connection established", @"")];
    iGuiState = kGuiStateConnected;
    [self updateView];
}

-(void) enterNotConnected:(NSString*)message {
    [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateClosed message:message];
    iGuiState = kGuiStateNotConnected;
    [self showDefaultPage];
    [self dismissMenuVC];
}

-(void) enterNotConnectedConnecting:(NSString*)message {
    [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateBusy message:message];
    iGuiState = kGuiStateNotConnectedConnecting;
    [self showDefaultPage];
    [self dismissMenuVC];
}

-(void) secureConnectionConnected {
    switch (iGuiState) {
        case kGuiStateInit:
        case kGuiStateNotConnected:
        case kGuiStateNotConnectedConnecting:
            [self enterConnected];
            break;
        case kGuiStateConnected:
        case kGuiStateConnectedWebLoading:
            break;
    }
}

-(void) secureConnectionNotConnected:(NSString*)message {
    switch (iGuiState) {
        case kGuiStateInit:
        case kGuiStateConnected:
        case kGuiStateConnectedWebLoading:
            [self enterNotConnected:message];
            break;
        case kGuiStateNotConnected:
            [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateClosed message:message];
            break;
        case kGuiStateNotConnectedConnecting:
            [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateBusy message:message];
            break;
    }
}

-(void) secureConnectionNotConnectedConnecting:(NSString*)message {
    switch (iGuiState) {
        case kGuiStateInit:
        case kGuiStateConnected:
        case kGuiStateConnectedWebLoading:
            [self enterNotConnectedConnecting:message];
            break;
        case kGuiStateNotConnected:
            [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateClosed message:message];
            break;
        case kGuiStateNotConnectedConnecting:
            [self.ivViewStatusNew setState:kGOnSecureBrowserStatusVStateBusy message:message];
            break;
    }
}

-(void) enterConnectedWebLoading {
    switch (iGuiState) {
        case kGuiStateConnected:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateConnectedWebLoading;
    [self updateView];
}

-(void) enterConnectedWebLoadingDone {
    switch (iGuiState) {
        case kGuiStateConnectedWebLoading:
            break;
        default:
            return;
    }
    iGuiState = kGuiStateConnected;
    [self updateView];
}

-(void) updateLaunch {
    [self updateProxyConfiguration];
}

-(void) updateLaunch:(GOnSCLTrafficLaunchCommandArgs*)args {
    iReloadOnDidAppear = YES;
    self.iGOnSCLTrafficLaunchCommandArgs = args;
    [self updateLaunch];
}


-(void) updateProxyConfiguration {
    switch (iGuiState) {
        case kGuiStateConnected:
            break;
        case kGuiStateConnectedWebLoading:
            [ivWebView stopLoading];
            break;
        default:
            break;
    }
    switch ([iGOnSCLTrafficLaunchCommandArgs launchType]) {
        case kLaunchCommandTypeBrowse:
            [GOnSecureBrowserSpecialProtocol setProxyDirect];            
            break;
        case kLaunchCommandTypeBrowseProxyHTTP:
            [GOnSecureBrowserSpecialProtocol setProxyHTTP:[iGOnSCLTrafficLaunchCommandArgs proxyHost] proxyPort:[iGOnSCLTrafficLaunchCommandArgs proxyPort]];
            break;
        case kLaunchCommandTypeBrowseProxySocks:
            [GOnSecureBrowserSpecialProtocol setProxySocks:[iGOnSCLTrafficLaunchCommandArgs proxyHost] proxyPort:[iGOnSCLTrafficLaunchCommandArgs proxyPort]];
            break;
        default:
            [GOnSecureBrowserSpecialProtocol setProxyClosed];            
            break;
    }
}

-(void) launchStart {
    switch (iGuiState) {
        case kGuiStateConnected:
            break;
        case kGuiStateConnectedWebLoading:
            [ivWebView stopLoading];
            break;
        default:
            return;
    }
    [self enterConnectedWebLoadingDone];
    self.ivWebView.scalesPageToFit = true;
    [ivWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:iGOnSCLTrafficLaunchCommandArgs.url]]];
}


#
#pragma mark - Implementation of GOnSecureBrowserMenuVCProtocol
#
-(BOOL) menuVC:(id)sender isSelectable:(NSString*)launchId {
    return true;
}

-(void) menuVC:(id)sender select:(NSString*)launchId {
    [self dismissMenuVC];
    [iDelegate performSelector:@selector(gonSecureBrowserVCProtocolSelect:) withObject:launchId];
}

-(void) menuVC:(id)sender terminate:(NSString*)launchId {
    [self dismissMenuVC];
    [iDelegate performSelector:@selector(gonSecureBrowserVCProtocolTerminate:) withObject:launchId];
}

-(UIImage*) menuVC:(id)sender 
          getImage:(NSString*)requestId
           imageId:(NSString*)imageId 
        imageStyle:(NSString*)imageStyle 
        imageSizeX:(NSInteger)imageSizeX 
        imageSizeY:(NSInteger)imageSizeY 
       imageFormat:(NSString*)imageFormat {
    if(iDelegate) {
        return [iDelegate gonSecureBrowserVCProtocolGetImage:requestId imageId:imageId imageStyle:imageStyle imageSizeX:imageSizeX imageSizeY:imageSizeY imageFormat:imageFormat];
    }
    return nil;
}

-(void) menuVCCancel:(id)sender {
    [self dismissMenuVC];    
}

-(void) delegateDisconnected {
    iCurrentMenuViewController = nil;
    [self updateView];
}


@end
