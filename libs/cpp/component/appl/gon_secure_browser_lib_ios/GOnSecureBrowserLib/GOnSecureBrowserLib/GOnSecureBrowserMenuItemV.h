//
//  GOnSecureBrowserMenuItemV.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GOnSecureCommunicationLibProtocol.h"
#import "GOnSecureBrowserMenuItemVProtocol.h"

@interface GOnSecureBrowserMenuItemV : UITableViewCell {
    IBOutlet UILabel* ivName;
    IBOutlet UILabel* ivInfo;
    IBOutlet UIImageView* ivImage;
    IBOutlet UIImageView* ivImageBadge;
    IBOutlet UIButton* ivStopButton;
    IBOutlet UIActivityIndicatorView* ivActivityIndicator;
}

@property (retain) IBOutlet UILabel* ivName;
@property (retain) IBOutlet UILabel* ivInfo;
@property (retain) IBOutlet UIImageView* ivImage;
@property (retain) IBOutlet UIImageView* ivImageBadge;
@property (retain) IBOutlet UIButton* ivStopButton;
@property (retain) IBOutlet UIActivityIndicatorView* ivActivityIndicator;


//
//
//
-(void) setDelegate:(id<GOnSecureBrowserMenuItemVProtocol>)delegate;
-(void) updateMenuItem:(GOnSCLMenuItem*) menuItem;

//
//
//
-(IBAction)stopButtonAction:(id)sender;

@end



