//
//  GOnSecureBrowserVCProtocol.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 19/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GOnSecureCommunicationLibProtocol.h"

@protocol GOnSecureBrowserVCProtocol <NSObject>
-(void) gonSecureBrowserVCProtocolDismissVC;
-(BOOL) gonSecureBrowserVCProtocolIsConnected;

-(void) gonSecureBrowserVCProtocolSelect:(NSString*)launchId;
-(void) gonSecureBrowserVCProtocolTerminate:(NSString*)launchId;
-(UIImage*) gonSecureBrowserVCProtocolGetImage:(NSString*)requestId
           imageId:(NSString*)requestId 
        imageStyle:(NSString*)imageStyle 
        imageSizeX:(NSInteger)imageSizeX 
        imageSizeY:(NSInteger)imageSizeY 
       imageFormat:(NSString*)imageFormat;


-(GOnSCLTrafficLaunchState) getLaunchState:(NSString*)launcjId;

@end
