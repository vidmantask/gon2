//
//  GOnSecureBrowserVC.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 18/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GOnSecureBrowserVCProtocol.h"
#import "GOnSecureCommunicationLib.h"
#import "GOnSecureBrowserMenuVCProtocol.h"
#import "GOnSecureBrowserStatusV.h"


typedef enum {
    kGuiStateInit=0,
    kGuiStateNotConnected, 
    kGuiStateNotConnectedConnecting, 
    kGuiStateConnected,
    kGuiStateConnectedWebLoading
} GOnSecureBrowserVCGuiState;

@interface GOnSecureBrowserVC : UIViewController <UIWebViewDelegate, GOnSecureBrowserMenuVCProtocol> {
    id<GOnSecureBrowserVCProtocol> iDelegate;

//
// gui elements
//
    IBOutlet UIWebView* ivWebView;
    IBOutlet UIBarButtonItem* ivBarButtonDMEMenu;
    IBOutlet UIBarButtonItem* ivBarButtonBack;
    IBOutlet UIBarButtonItem* ivBarButtonForward;
    IBOutlet UIBarButtonItem* ivBarButtonRefresh;
    IBOutlet UIBarButtonItem* ivBarButtonBookmarks;
    IBOutlet GOnSecureBrowserStatusV* ivViewStatusNew;

    GOnSCLTrafficLaunchCommandArgs* iGOnSCLTrafficLaunchCommandArgs;
    
    NSString* iLaunchId;
//
// 
//
}

//
// init
//
-(void) initInternal;
-(void) setDelegate:(id<GOnSecureBrowserVCProtocol>)delegate;
-(void) updateMenuItem:(GOnSCLTrafficLaunchStatusChangedArgs*)args;



//
// gui elements
//
@property (atomic, retain) IBOutlet UIWebView* ivWebView;
@property (atomic, retain) IBOutlet UIBarButtonItem* ivBarButtonDMEMenu;
@property (atomic, retain) IBOutlet UIBarButtonItem* ivBarButtonBack;
@property (atomic, retain) IBOutlet UIBarButtonItem* ivBarButtonForward;
@property (atomic, retain) IBOutlet UIBarButtonItem* ivBarButtonRefresh;
@property (atomic, retain) IBOutlet UIBarButtonItem* ivBarButtonBookmarks;
@property (atomic, retain) IBOutlet GOnSecureBrowserStatusV* ivViewStatusNew;

@property (atomic, retain, strong) GOnSCLDialogShowMenuArgs* iCurrentMenu;
@property (atomic, retain, strong) GOnSCLTrafficLaunchCommandArgs* iGOnSCLTrafficLaunchCommandArgs;
@property (atomic, retain, strong, getter=launchId) NSString* iLaunchId;

//
// gui actions
//
-(IBAction)ivBarButtonDMEMenuAction:(id)sender;


//
// Secure connection events
//
-(void) enterConnected;
-(void) enterNotConnected:(NSString*)message;
-(void) enterNotConnectedConnecting:(NSString*)message;

//
//
//
-(void) updateMenu;
-(void) updateMenu:(GOnSCLDialogShowMenuArgs*)menu;

-(void) updateLaunch;
-(void) updateLaunch:(GOnSCLTrafficLaunchCommandArgs*)args;

-(void) launchStart;
-(void) setLaunchId:(NSString*)launchId;

-(void) dismissMenuVC;

@end
