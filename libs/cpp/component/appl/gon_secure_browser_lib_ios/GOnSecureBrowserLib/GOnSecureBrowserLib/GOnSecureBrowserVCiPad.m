//
//  GOnSecureBrowserVCiPadPortrait.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 19/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserVCiPad.h"
#import "GOnSecureBrowserMenuVC.h"


@interface GOnSecureBrowserVCiPad () {
    UIPopoverController* iCurrentMenuViewControllerPopoverController;    
}
@end


@implementation GOnSecureBrowserVCiPad

#
#pragma mark - Instance lifecycle
#
-(void) initInternal {
    [super initInternal];
    iCurrentMenuViewControllerPopoverController = nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternal];
    }
    return self;
}

-(void) dealloc {
    [super dealloc];
}

-(void) prepareForMenuVC:(UIStoryboardSegue *)segue menuViewController:(GOnSecureBrowserMenuVC*) menuViewController {
    iCurrentMenuViewControllerPopoverController = ((UIStoryboardPopoverSegue*)segue).popoverController;
    NSLog(@"GOnSecureBrowserVCiPad.prepareForMenuVC self:%@ :%@", self, iCurrentMenuViewControllerPopoverController);
}

-(void) dismissMenuVC {
    NSLog(@"GOnSecureBrowserVCiPad.dismissMenuVC self:%@ :%@", self, iCurrentMenuViewControllerPopoverController);
    if(iCurrentMenuViewControllerPopoverController) {
        NSLog(@"GOnSecureBrowserVCiPad.dismissMenuVC.doing_it");
        [iCurrentMenuViewControllerPopoverController dismissPopoverAnimated:YES];
        iCurrentMenuViewControllerPopoverController = nil;
    }
    [super dismissMenuVC];
}


#
#pragma mark - View lifecycle
#
- (void)viewDidLoad {
    [super viewDidLoad];
 }

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
