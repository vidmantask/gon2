//
//  UIImage+Grayscale.h
//  GOnClient
//
//  Created by gbuilder on 02/03/11.
//  Copyright 2011 giritech.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIImage.h>

@interface UIImage (Grayscale)
	- (UIImage *)convertToGrayscale;
@end
