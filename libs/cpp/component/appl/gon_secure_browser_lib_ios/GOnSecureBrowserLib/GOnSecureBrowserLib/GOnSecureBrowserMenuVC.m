//
//  GOnSecureBrowserMenuVC.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 24/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserMenuVC.h"
#import "GOnSecureBrowserMenuItemV.h"


@interface GOnSecureBrowserMenuVC () {
    GOnSCLDialogShowMenuArgs* iMenuData;
    
    id<GOnSecureBrowserMenuVCProtocol> iDelegate;
}
@property (retain) GOnSCLDialogShowMenuArgs* iMenuData;
@end



@implementation GOnSecureBrowserMenuVC
@synthesize ivBackButton;
@synthesize ivTitle;
@synthesize ivTableView;
@synthesize iMenuData;


#
#pragma mark - Instance lifecycle
#
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.iMenuData = nil;
        iDelegate = nil;
        NSLog(@"GOnSecureBrowserMenuVC.initWithCoder");        
    }
    return self;
}

- (void) dealloc {
    self.ivTableView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) setDelegate:(id<GOnSecureBrowserMenuVCProtocol>)delegate {
    iDelegate = delegate;
    NSLog(@"GOnSecureBrowserMenuVC.setDelegate");
}


#
#pragma mark - View lifecycle
#
- (void)viewDidLoad {
    [super viewDidLoad];
    self.ivBackButton.title = NSLocalizedString(@"back", @"");
    self.ivTitle.title = NSLocalizedString(@"Bookmarks", @"");
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBar.hidden = NO;
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"GOnSecureBrowserMenuVC.viewDidAppear");        
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    if(iDelegate) {
        [iDelegate delegateDisconnected];
    }
    iDelegate = nil;
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}


#
#
#
-(void) updateMenu:(GOnSCLDialogShowMenuArgs*) menuData {
    self.iMenuData = menuData;
    [self.ivTableView reloadData];
}

-(IBAction)backButtonAction:(id)sender {
    if(iDelegate) {
        [iDelegate menuVCCancel:self];
    }
}

#
#pragma mark - Table view data source
#
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.iMenuData) {
        return [self.iMenuData.menuItems count]; 
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* CellIdentifier = @"idMenuItemCell";
    
    GOnSecureBrowserMenuItemV* cell = (GOnSecureBrowserMenuItemV*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setDelegate:self];
    [cell updateMenuItem:[self.iMenuData.menuItems objectAtIndex:[indexPath row]]];
    
    return cell;
}

#
#pragma mark - Table view delegate
#
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GOnSCLMenuItem* menuItem = [self.iMenuData.menuItems objectAtIndex:[indexPath row]];
    if(iDelegate) {
        [iDelegate menuVC:self select:[menuItem launchId]];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GOnSCLMenuItem* menuItem = [self.iMenuData.menuItems objectAtIndex:[indexPath row]];
    if(iDelegate) {
        if(!menuItem.disabled) {
            return indexPath;
        };
    }
    return nil;
}

#
#pragma mark - Implementation of GOnSecureBrowserMenuItemVProtocol
#
-(void) menuItemView:(id)view select:(NSString*)launchId {
    if(iDelegate) {
        [iDelegate menuVC:self select:launchId];
    }
}

-(void) menuItemView:(id)view terminate:(NSString*)launchId {
    if(iDelegate) {
        [iDelegate menuVC:self terminate:launchId];
    }
}

-(UIImage*) menuItemView:(id)view 
                getImage:(NSString*)requestId
                 imageId:(NSString*)imageId 
              imageStyle:(NSString*)imageStyle 
              imageSizeX:(NSInteger)imageSizeX 
              imageSizeY:(NSInteger)imageSizeY 
             imageFormat:(NSString*)imageFormat {
    if(iDelegate) {
        return [iDelegate menuVC:self getImage:requestId imageId:imageId imageStyle:imageStyle imageSizeX:imageSizeX imageSizeY:imageSizeY imageFormat:imageStyle];
    }    
    return nil;
}

@end
