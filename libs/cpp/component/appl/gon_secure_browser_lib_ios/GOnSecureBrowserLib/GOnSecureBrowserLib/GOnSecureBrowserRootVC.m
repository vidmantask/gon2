//
//  GOnSecureBrowserRootVC.m
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 02/02/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import "GOnSecureBrowserRootVC.h"

@implementation GOnSecureBrowserRootVC

#
#pragma mark - Instance lifecycle
#
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#
#pragma mark - View lifecycle
#
- (void)viewDidUnload {
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

@end
