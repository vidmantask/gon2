//
//  GOnSecureBrowserStatusV.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 27/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kGOnSecureBrowserStatusVStateInit = 0,
    kGOnSecureBrowserStatusVStateClosed, 
    kGOnSecureBrowserStatusVStateBusy, 
    kGOnSecureBrowserStatusVStateConnected 
} GOnSecureBrowserStatusVStateType;


@interface GOnSecureBrowserStatusV : UIView {
    GOnSecureBrowserStatusVStateType iState;
    
}


- (void) setState:(GOnSecureBrowserStatusVStateType)state message:(NSString*)message;


@end
