//
//  GOnSecureBrowserMenuVC.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 24/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GOnSecureCommunicationLibProtocol.h"
#import "GOnSecureBrowserMenuItemVProtocol.h"
#import "GOnSecureBrowserMenuVCProtocol.h"

@interface GOnSecureBrowserMenuVC : UIViewController <UITableViewDataSource, UITableViewDelegate, GOnSecureBrowserMenuItemVProtocol> {
    IBOutlet UITableView* ivTableView;
    IBOutlet UIBarButtonItem* ivBackButton;
    IBOutlet UIBarButtonItem* ivTitle;
}

@property (retain) IBOutlet UITableView* ivTableView;
@property (retain) IBOutlet UIBarButtonItem* ivBackButton;
@property (retain) IBOutlet UIBarButtonItem* ivTitle;

-(IBAction)backButtonAction:(id)sender;

//
//
//
-(void) setDelegate:(id<GOnSecureBrowserMenuVCProtocol>)delegate;
-(void) updateMenu:(GOnSCLDialogShowMenuArgs*) menuData;
@end
