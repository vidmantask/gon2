//
//  GOnSecureBrowserMenuItemVProtocol.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureBrowserMenuItemVProtocol <NSObject>

-(void) menuItemView:(id)view select:(NSString*)launchId;

-(void) menuItemView:(id)view terminate:(NSString*)launchId;

-(UIImage*) menuItemView:(id)view 
                getImage:(NSString*)launchId
                 imageId:(NSString*)imageId 
              imageStyle:(NSString*)imageStyle 
              imageSizeX:(NSInteger)imageSizeX 
              imageSizeY:(NSInteger)imageSizeY 
             imageFormat:(NSString*)imageFormat;

@end
