//
//  GOnSecureBrowserMenuVCProtocol.h
//  GOnSecureBrowserLib
//
//  Created by gbuilder on 25/01/12.
//  Copyright (c) 2012 Giritech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GOnSecureBrowserMenuVCProtocol <NSObject>

-(BOOL) menuVC:(id)view isSelectable:(NSString*)launchId;

-(void) menuVC:(id)sender select:(NSString*)launchId;

-(void) menuVC:(id)sender terminate:(NSString*)launchId;

-(UIImage*) menuVC:(id)sender 
                getImage:(NSString*)requestId
                 imageId:(NSString*)imageId 
              imageStyle:(NSString*)imageStyle 
              imageSizeX:(NSInteger)imageSizeX 
              imageSizeY:(NSInteger)imageSizeY 
             imageFormat:(NSString*)imageFormat;

-(void) delegateDisconnected;

-(void) menuVCCancel:(id)sender;

@end
