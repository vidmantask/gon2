/*! \file APPL_ClientEventhandler.hxx
 *  \brief This file contains abstract interface AppClient
 */
#ifndef APPL_ClientEventhandler_HXX
#define APPL_ClientEventhandler_HXX

#include <vector>
#include <string>
#include <map>

#include <boost/utility.hpp>
#include <component/appl/gon_client/APPL_ClientCommand.hxx>



namespace Giritech {
namespace Appl {

class ApplClientMenuItem {
public:
	long id;
	std::string title;
	std::string launch_id;
	std::string icon_id;
	bool disabled;
	std::string tooltip;
	std::vector<std::string> tags;
	bool tag_show;
	bool tag_enabled;
	bool tag_topx;
	bool tag_gupdate;
	bool tag_appbox_default;
	bool tag_appbox_link;
};

class ApplClientTagProperty {
public:
	std::string name;
	bool auto_launch;
	bool auto_launch_first;
};





/*! \brief This class define the abstract eventhandler interface for a ApplClient.
 */
class ApplClientEventhandler : public boost::noncopyable {
public:
  enum State {StateLaunching = 0, StateReady, StateClosing, StateClosed, StateError, StateNotLaunched};

    /*! \brief Events from ApplClient
     */
    virtual void app_client_event_connecting(const std::string& connection_title) = 0;
    virtual void app_client_event_connecting_failed(const std::string& message) = 0;

    /*! \brief Events from ApplClientSession
     */
    virtual void app_client_event_connected(void) = 0;
    virtual void app_client_event_closing(void) = 0;
    virtual void app_client_event_closed(void) = 0;

    virtual void app_client_user_request_login(const std::string& msg, const bool change_password_enabled) = 0;

    virtual void app_client_auth_generate_keys_response(const std::string& private_key, const std::string& public_key) = 0;

    virtual void app_client_dialog_show_license_info(const std::string& header, const std::string& message, const bool valid) = 0;
    virtual void app_client_dialog_menu_updated(void) = 0;
    virtual void app_client_dialog_request_inform_on_first_access(const std::string&  message, const bool close_on_cancel) = 0;

    virtual void app_client_dialog_show_menu(const std::vector<ApplClientMenuItem>& menu_items, const std::map<std::string, ApplClientTagProperty>& tag_properties) = 0;
    virtual void app_client_dialog_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data) = 0;
    virtual void app_client_dialog_image_not_found(const std::string& request_id, const std::string& image_id) = 0;

    virtual void app_client_info(const std::string& header, const std::string& message) = 0;
    virtual void app_client_warning(const std::string& header, const std::string& message) = 0;
    virtual void app_client_error(const std::string& header, const std::string& message) = 0;

    virtual void app_client_endpoint_request_endpoint_info(void) = 0;

    virtual void app_client_endpoint_enrollment_request_token(void) = 0;
    virtual void app_client_endpoint_enrollment_token_status(const bool can_deploy, const bool auth_activated, const bool assigned_to_current_user, const std::string& status_text) = 0;
    virtual void app_client_endpoint_enrollment_enroll_token_response(const bool rc, const std::string& msg) = 0;

    virtual void app_client_endpoint_get_enrollment_status_response(const bool enroll_error, const bool enroll_done, const std::string& enroll_message) = 0;

    virtual void app_client_traffic_launch_status_changed(const std::string& launch_id, const State state, const std::string& status_message) = 0;
    virtual void app_client_traffic_launch_command(const std::string& launch_id, const ApplClientCommand& launch_command) = 0;
    virtual void app_client_traffic_launch_command_terminate(const std::string& launch_id) = 0;

    virtual void app_client_auth_failed(void) = 0;

    virtual void app_client_tag_get_platform(void) = 0;

    virtual void app_client_set_background_settings(const bool close_when_entering_background) = 0;

    virtual void app_client_cpm_update_connection_info(const std::string& servers) = 0;

    virtual bool app_client_is_network_available(void) = 0;

    virtual void app_client_key_store_reset(void) = 0;
    virtual void app_client_key_store_session_key_pair_set(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) = 0;
    virtual void app_client_key_store_session_key_pair_get(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) = 0;


    virtual void app_client_version_error(const std::string& server_version) = 0;


    virtual void app_client_dme_get_dme_value(void) = 0;
};

}
}

#endif
