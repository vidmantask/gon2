/*! \file APPL_ClientSession.cxx
 \brief This file contains the implementation for the PPL_ClientSession class
 */
#include <sstream>
#include <functional>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <component/appl/gon_client/APPL_ClientSessionTunnelendpoint.hxx>
#include <component/appl/gon_client/APPL_ClientCommand.hxx>
#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>

#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>
#include <lib/cryptfacility/CF_Hash.hxx>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_HTTPDataServer.hxx>


using namespace std;
using namespace Giritech::Appl;
using namespace Giritech::Utility;
using namespace Giritech::Communication;


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpoint implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpoint::ApplClientSessionTunnelendpoint(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
 : async_service_(async_service),
   checkpoint_handler_(checkpoint_handler),
   state_(State_Initializing),
   appl_client_eventhandler_(NULL),
   close_janitor_timer_(async_service->get_io_service()) {
	com_tunnelendpoint_ = TunnelendpointReliableCryptedTunnel::create(async_service->get_io_service(), checkpoint_handler_, this);
}

ApplClientSessionTunnelendpoint::~ApplClientSessionTunnelendpoint(void) {
	state_ = State_Unknown;
}

ApplClientSessionTunnelendpoint::State ApplClientSessionTunnelendpoint::get_state(void) const {
	return state_;
}

Tunnelendpoint::APtr ApplClientSessionTunnelendpoint::get_com_tunnelendpoint(void) {
	return com_tunnelendpoint_;
}

void ApplClientSessionTunnelendpoint::set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler) {
	appl_client_eventhandler_ = appl_client_eventhandler;
}

void ApplClientSessionTunnelendpoint::reset_appl_client_eventhandler(void) {
	appl_client_eventhandler_ = NULL;
}

void ApplClientSessionTunnelendpoint::close(void) {
	state_ = State_Closed;
}

bool ApplClientSessionTunnelendpoint::is_closed(void) {
	return state_ == State_Closed;
}

void ApplClientSessionTunnelendpoint::tunnelendpoint_eh_connected(void) {
	state_ = State_Connected;
	appl_client_session_tunnelendpoint_connected();
}

void ApplClientSessionTunnelendpoint::tunnelendpoint_eh_recieve(const MessagePayload::APtr& message) {
	try {
//		cout << message->get_buffer()->toString() << endl;
		RPC::RPCSpecCall::APtr rpm_call(RPC::RPCSpecCall::create_from_string(message->get_buffer()->toString()));
		if(! rpc_dispatch(rpm_call->get_function_name(), rpm_call->get_args())) {
		    Checkpoint cp(*checkpoint_handler_, "tunnelendpoint_eh_recieve.error", Attr_ApplClient(), CpAttr_critical(), CpAttr("function_name", rpm_call->get_function_name()));
//			stringstream ss;
//			ss << "rpc_dispatch ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR" << endl;
//			ss << com_tunnelendpoint_->get_id_abs_to_string() << endl;
//			ss << message->get_buffer()->toString() << endl;
//			ss << "rpc_dispatch ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR" << endl;
//		    Checkpoint cp(*checkpoint_handler_, "tunnelendpoint_eh_recieve.error", Attr_ApplClient(), CpAttr_critical(), CpAttr("message", ss.str()));
		}
	}
	catch (const std::exception& e) {
//	    Checkpoint cp(*checkpoint_handler_, "tunnelendpoint_eh_recieve.exception", Attr_ApplClient(), CpAttr_critical(), CpAttr("what", e.what()));
		stringstream ss;
		ss << "rpc_dispatch ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR exception" << endl;
		ss << message->get_buffer()->toString() << endl;
		ss << "rpc_dispatch -----------------------------------------------" << endl;
		ss << com_tunnelendpoint_->get_id_abs_to_string() << endl;
		ss << e.what() << endl;
		ss << "rpc_dispatch ERROR ERROR ERROR ERROR ERROR ERROR ERROR ERROR exception" << endl;
//	    Checkpoint cp(*checkpoint_handler_, "tunnelendpoint_eh_recieve.error", Attr_ApplClient(), CpAttr_critical(), CpAttr("message", ss.str()));
	}
}

bool ApplClientSessionTunnelendpoint::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "_remote_check_version_failed") {
		string error_message(args->get_as_string("error_message"));
		rpc_dispatch_remote_check_version_failed(error_message);
		return true;
	}
	return false;
}

void ApplClientSessionTunnelendpoint::rpc_dispatch_remote_check_version_failed(const std::string& error_message) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpoint::rpc_dispatch_remote_check_version_failed.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	Checkpoint cp(*checkpoint_handler_, "ApplClientSessionTunnelendpoint::rpc_dispatch_remote_check_version_failed", Attr_ApplClient(), CpAttr_warning(), CpAttr("error_message", error_message));
//	appl_client_eventhandler_->app_client_info("Invalid Version", "This G/On Client version is not support by the server");
	com_tunnelendpoint_->tunnelendpoint_user_signal(UserSignal_VERSION_ERROR_REMOTE, error_message);
}

void ApplClientSessionTunnelendpoint::rpc_remote(const RPC::RPCSpecCall::APtr& rpc_call) {
	Communication::MessagePayload::APtr payload(MessagePayload::create(Utility::DataBufferManaged::create(rpc_call->to_xml_doc_string())));
	com_tunnelendpoint_->tunnelendpoint_send(payload);
}
void ApplClientSessionTunnelendpoint::rpc_remote(const std::string& method) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	RPC::RPCSpecCall::APtr rpc_call(RPC::RPCSpecCall::create(method, args));
	rpc_remote(rpc_call);
}

void ApplClientSessionTunnelendpoint::rpc_remote(const std::string& method, RPC::RPCSpecValueDict::APtr& args) {
	RPC::RPCSpecCall::APtr rpc_call(RPC::RPCSpecCall::create(method, args));
	rpc_remote(rpc_call);
}

void ApplClientSessionTunnelendpoint::close_janitor_start(void) {
	switch(state_) {
	case State_Closing:
		break;
	default:
		return;
	}
	close_janitor_timer_.expires_from_now(boost::posix_time::milliseconds(200));
	close_janitor_timer_.async_wait(boost::bind(&ApplClientSessionTunnelendpoint::close_janitor, this));
}

void ApplClientSessionTunnelendpoint::close_janitor(void) {
	switch(state_) {
	case State_Closing:
		break;
	default:
		return;
	}

	if(close_janitor_is_closed()) {
		state_ = State_Closed;
		return;
	}
	close_janitor_start();
}

bool ApplClientSessionTunnelendpoint::close_janitor_is_closed(void) {
	return true;
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointAuth implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointAuth::ApplClientSessionTunnelendpointAuth(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
}

ApplClientSessionTunnelendpointAuth::~ApplClientSessionTunnelendpointAuth(void) {
}

void ApplClientSessionTunnelendpointAuth::set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler) {
	ApplClientSessionTunnelendpoint::set_appl_client_eventhandler(appl_client_eventhandler);
	if(plugin_mobile_.get() != NULL) {
		plugin_mobile_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_soft_token_.get() != NULL) {
		plugin_soft_token_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_micro_smart_.get() != NULL) {
		plugin_micro_smart_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_hagiwara_.get() != NULL) {
		plugin_hagiwara_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_endpoint_token_.get() != NULL) {
		plugin_endpoint_token_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_computer_token_.get() != NULL) {
		plugin_computer_token_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
	if(plugin_dme_.get() != NULL) {
		plugin_dme_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
}

void ApplClientSessionTunnelendpointAuth::reset_appl_client_eventhandler(void) {
	ApplClientSessionTunnelendpoint::reset_appl_client_eventhandler();
	if(plugin_mobile_.get() != NULL) {
		plugin_mobile_->reset_appl_client_eventhandler();
	}
	if(plugin_soft_token_.get() != NULL) {
		plugin_soft_token_->reset_appl_client_eventhandler();
	}
	if(plugin_micro_smart_.get() != NULL) {
		plugin_micro_smart_->reset_appl_client_eventhandler();
	}
	if(plugin_hagiwara_.get() != NULL) {
		plugin_hagiwara_->reset_appl_client_eventhandler();
	}
	if(plugin_endpoint_token_.get() != NULL) {
		plugin_endpoint_token_->reset_appl_client_eventhandler();
	}
	if(plugin_computer_token_.get() != NULL) {
		plugin_computer_token_->reset_appl_client_eventhandler();
	}
	if(plugin_dme_.get() != NULL) {
		plugin_dme_->reset_appl_client_eventhandler();
	}
}

ApplClientSessionTunnelendpointAuth::APtr ApplClientSessionTunnelendpointAuth::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointAuth(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointAuth::appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key) {
	auth_serial_ = serial;
	auth_private_key_ = private_key;
	auth_public_key_ = public_key;
}

void ApplClientSessionTunnelendpointAuth::app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time) {
	if (plugin_dme_.get() != NULL) {
		plugin_dme_->app_client_dme_response_get_dme_values(username, password, terminal_id, device_signature, device_time);
	}
}

void ApplClientSessionTunnelendpointAuth::app_client_dme_response_get_dme_values_none(void) {
	if (plugin_dme_.get() != NULL) {
		plugin_dme_->app_client_dme_response_get_dme_values_none();
	}
}


void ApplClientSessionTunnelendpointAuth::remote_auth_child_created_on_client(const std::string& module_name, const unsigned long& id) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("plugin_name", RPC::RPCSpecValueString::create(module_name));
	args->update("child_id", RPC::RPCSpecValueInteger::create(id));
	rpc_remote("auth_child_created_on_client", args);
}

void ApplClientSessionTunnelendpointAuth::appl_client_session_tunnelendpoint_connected(void) {
	plugin_mobile_ = ApplClientSessionTunnelendpointAuthMobile::create(async_service_, checkpoint_handler_);
	plugin_mobile_->appl_client_auth_set_info(auth_serial_, auth_private_key_, auth_public_key_);
	com_tunnelendpoint_->add_tunnelendpoint(1, plugin_mobile_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("mobile", 1);

	plugin_soft_token_ = ApplClientSessionTunnelendpointAuthTokenStub::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(2, plugin_soft_token_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("soft_token", 2);

	plugin_micro_smart_ = ApplClientSessionTunnelendpointAuthTokenStub::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(3, plugin_micro_smart_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("micro_smart", 3);

	plugin_hagiwara_ = ApplClientSessionTunnelendpointAuthTokenStub::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(4, plugin_hagiwara_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("hagiwara", 4);

	plugin_endpoint_token_ = ApplClientSessionTunnelendpointAuthTokenStub::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(5, plugin_endpoint_token_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("endpoint_token", 5);

	plugin_computer_token_ = ApplClientSessionTunnelendpointAuthTokenStub::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(6, plugin_computer_token_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("computer_token", 6);

	plugin_dme_ = ApplClientSessionTunnelendpointAuthDME::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(7, plugin_dme_->get_com_tunnelendpoint());
	remote_auth_child_created_on_client("dme", 7);

	// Setting the appl_client_eventhandler for all plugins
	set_appl_client_eventhandler(appl_client_eventhandler_);

	rpc_remote("all_auth_children_created_on_client");
}

bool ApplClientSessionTunnelendpointAuth::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_call_show_message") {
		string message(args->get_as_string("message"));
		rpc_dispatch_remote_call_show_message(message);
		return true;
	}
	else if(method == "remote_authorization_failed") {
		string message(args->get_as_string("message"));
		rpc_dispatch_remote_authorization_failed(message);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointAuth::rpc_dispatch_remote_call_show_message(const std::string& message) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointAuth::rpc_dispatch_remote_call_show_message.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_info("Authorization information", message);
}

void ApplClientSessionTunnelendpointAuth::rpc_dispatch_remote_authorization_failed(const std::string& message) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointAuth::rpc_dispatch_remote_authorization_failed.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_info("Authorization information", message);
	appl_client_eventhandler_->app_client_auth_failed();
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointAuthMobile implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointAuthMobile::ApplClientSessionTunnelendpointAuthMobile(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {

}

ApplClientSessionTunnelendpointAuthMobile::~ApplClientSessionTunnelendpointAuthMobile(void) {
}

ApplClientSessionTunnelendpointAuthMobile::APtr ApplClientSessionTunnelendpointAuthMobile::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointAuthMobile(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointAuthMobile::appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key) {
	auth_serial_ = serial;
	auth_private_key_ = private_key;
	auth_public_key_ = public_key;
}

void ApplClientSessionTunnelendpointAuthMobile::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointAuthMobile::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
    if(method == "get_serial") {
		rpc_dispatch_get_serial();
		return true;
	}
	else if(method == "get_challenge") {
		string challenge(args->get_as_string("challenge"));
		rpc_dispatch_get_challenge(challenge);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointAuthMobile::rpc_dispatch_get_serial(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	if(auth_serial_ == "") {
		args->update("serial", RPC::RPCSpecValueNone::create());
	}
	else {
		args->update("serial", RPC::RPCSpecValueString::create(auth_serial_));
	}
	rpc_remote("get_serial_response", args);
}

void ApplClientSessionTunnelendpointAuthMobile::rpc_dispatch_get_challenge(const std::string& challenge) {
	try {
		boost::shared_ptr<CryptFacility::CryptFactory> factory(CryptFacility::CryptFacilityService::getInstance().getCryptFactory());
		DataBufferManaged::APtr buf_private_key(DataBufferManaged::create(auth_private_key_)->decodeHex());
		DataBufferManaged::APtr buf_challenge(DataBufferManaged::create(challenge));
		DataBufferManaged::APtr buf_challenge_signature(factory->pk_sign_challenge(buf_private_key, buf_challenge));
		string challenge_signature(buf_challenge_signature->encodeHex()->toString());

		RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
		args->update("challenge_signature", RPC::RPCSpecValueString::create(challenge_signature));
		rpc_remote("get_challenge_response", args);
		return;
	}
	catch (const std::exception& e) {
		Checkpoint cp3(*checkpoint_handler_, "ApplClientSessionTunnelendpointAuthMobile::rpc_dispatch_get_challenge.error", Attr_ApplClient(), CpAttr_error());
	}
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("challenge_signature", RPC::RPCSpecValueNone::create());
	rpc_remote("get_challenge_response", args);
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointAuthTokenStub implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointAuthTokenStub::ApplClientSessionTunnelendpointAuthTokenStub(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
}

ApplClientSessionTunnelendpointAuthTokenStub::~ApplClientSessionTunnelendpointAuthTokenStub(void) {
}

ApplClientSessionTunnelendpointAuthTokenStub::APtr ApplClientSessionTunnelendpointAuthTokenStub::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointAuthTokenStub(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointAuthTokenStub::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointAuthTokenStub::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
    if(method == "get_serial") {
		rpc_dispatch_get_serial();
		return true;
	}
	else if(method == "get_challenge") {
		string challenge(args->get_as_string("challenge"));
		rpc_dispatch_get_challenge(challenge);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointAuthTokenStub::rpc_dispatch_get_serial(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("serial", RPC::RPCSpecValueNone::create());
	rpc_remote("get_serial_response", args);
}

void ApplClientSessionTunnelendpointAuthTokenStub::rpc_dispatch_get_challenge(const std::string& challenge) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("challenge_signature", RPC::RPCSpecValueNone::create());
	rpc_remote("get_challenge_response", args);
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointEndpoint implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointEndpoint::ApplClientSessionTunnelendpointEndpoint(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler), endpoint_enrollment_running_(false), get_token_status_is_get_enrollment_status_(false) {
	current_token_ = RPC::RPCSpecValueDict::create();
}

ApplClientSessionTunnelendpointEndpoint::~ApplClientSessionTunnelendpointEndpoint(void) {
}

ApplClientSessionTunnelendpointEndpoint::APtr ApplClientSessionTunnelendpointEndpoint::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointEndpoint(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointEndpoint::appl_client_session_tunnelendpoint_connected(void) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointEndpoint::appl_client_session_tunnelendpoint_connected.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_endpoint_request_endpoint_info();
}

void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("plugin_name", RPC::RPCSpecValueString::create("mobile"));

	RPC::RPCSpecValueList::APtr endpoint_info(RPC::RPCSpecValueList::create());

	RPC::RPCSpecValueTuple::APtr endpoint_info_element(RPC::RPCSpecValueTuple::create());
	endpoint_info_element->push_back(RPC::RPCSpecValueString::create(info_id));
	endpoint_info_element->push_back(RPC::RPCSpecValueString::create(info_value));
	endpoint_info->push_back(endpoint_info_element);

	args->update("endpoint_info_attributes", endpoint_info);
	rpc_remote("remote_report_some", args);
}

void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_response_endpoint_info_all_done(void) {
	rpc_remote("remote_all_done");
}

bool ApplClientSessionTunnelendpointEndpoint::is_launch_internal_endpoint_enrollment_ready(void) {
	return true;
}

bool ApplClientSessionTunnelendpointEndpoint::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "launch_enrollment") {
		long licensed(args->get_as_integer("licensed"));
		rpc_dispatch_launch_enrollment(licensed);
		return true;
	}
	else if(method == "deploy_token_cb") {
		RPC::RPCSpecValueDict::APtr token(args->get_as_dict("token"));
		rpc_dispatch_deploy_token_cb(token);
		return true;
	}
	else if(method == "enroll_reply") {
		bool rc(args->get_as_integer("rc"));
		string msg(args->get_as_string("msg"));
		rpc_dispatch_enroll_reply(rc, msg);
		return true;
	}
	else if(method == "get_token_status_reply") {
		std::vector<RPC::RPCSpecValue::APtr> tokens(args->get_as_list("tokens"));
		string errormsg(args->get_as_string("errormsg", ""));
		rpc_dispatch_get_token_status_reply(tokens, errormsg);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointEndpoint::rpc_dispatch_launch_enrollment(const bool licensed) {
	if (!licensed) {
		appl_client_eventhandler_->app_client_warning("Missing license for Field Enrollment", "");
		return;
	}
	appl_client_eventhandler_->app_client_endpoint_enrollment_request_token();
}

void ApplClientSessionTunnelendpointEndpoint::rpc_dispatch_get_token_status_reply(const std::vector<RPC::RPCSpecValue::APtr>& tokens, const std::string& errormsg) {
	//	result
	//	token["can_deploy"] -> result boolean
	//	token["auth_activated"]; -> result boolean
	//	token["assigned_to_current_user"]; -> result boolean
	//	token["status_text"]; -> result boolean

	if (tokens.size() > 0) {
		RPC::RPCSpecValueDict::APtr token(value_to_dict(tokens[0]));
		string status_text(token->get_as_string("status_text", ""));
		string token_title(token->get_as_string("token_title", ""));

		if (get_token_status_is_get_enrollment_status_) {
			bool enroll_error = false;
			bool enroll_done = false;
			string enroll_message;
			if (errormsg != "") {
				enroll_message = errormsg;
				enroll_error = true;
				enroll_done = false;
			}
			else {
				if (status_text != "") {
					enroll_message = status_text;
					enroll_error = false;
					enroll_done = true;
				}
				else {
					enroll_message = token_title;
					enroll_error = false;
					enroll_done = false;
				}
			}
			appl_client_eventhandler_->app_client_endpoint_get_enrollment_status_response(enroll_error, enroll_done, enroll_message);
			get_token_status_is_get_enrollment_status_ = false;
		}
		else {
			bool can_deploy(token->get_as_integer("can_deploy"));
			bool auth_activated(token->get_as_integer("auth_activated", 0));
			bool assigned_to_current_user(token->get_as_integer("assigned_to_current_user", 0));
			appl_client_eventhandler_->app_client_endpoint_enrollment_token_status(can_deploy, auth_activated, assigned_to_current_user, status_text);
		}
	}
}

void ApplClientSessionTunnelendpointEndpoint::rpc_dispatch_deploy_token_cb(const RPC::RPCSpecValueDict::APtr& token) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("token", current_token_);

    rpc_remote("enroll_deployed_token", args);
}

void ApplClientSessionTunnelendpointEndpoint::rpc_dispatch_enroll_reply(const bool rc, const std::string& msg) {
	appl_client_eventhandler_->app_client_endpoint_enrollment_enroll_token_response(rc, msg);
}

void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title) {
	//	token->update("token_type", x );
	//	token->update("token_id", x );
	//	token->update("token_status", x );
	//	token->update("token_serial", x );
	//	token->update("runtime_env_id", x );
	//	token->update("token_title", x );
	//	token->update("token_type_title", x );
	//	token->update("token_plugin_module_name", x );
	//	token->update("token_public_key", x );
	RPC::RPCSpecValueList::APtr tokens(RPC::RPCSpecValueList::create());
	current_token_ = RPC::RPCSpecValueDict::create();
	current_token_->update("token_type", RPC::RPCSpecValueString::create(token_type));
	current_token_->update("token_internal_type", RPC::RPCSpecValueString::create(token_internal_type));
	current_token_->update("token_serial", RPC::RPCSpecValueString::create(token_serial));
	current_token_->update("check_status", RPC::RPCSpecValueInteger::create(1));
	current_token_->update("token_is_endpoint", RPC::RPCSpecValueInteger::create(0) );
	current_token_->update("token_title", RPC::RPCSpecValueString::create(token_title));
	tokens->push_back(current_token_);

	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("tokens", tokens);
	get_token_status_is_get_enrollment_status_ = false;
	rpc_remote("get_token_status", args);
}

void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key) {
	current_token_->update("token_serial", RPC::RPCSpecValueString::create(token_serial));
	current_token_->update("token_public_key", RPC::RPCSpecValueString::create(token_public_key));

	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("token", current_token_);
	rpc_remote("enroll_token", args);
}

void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_token_status(void) {
	RPC::RPCSpecValueList::APtr tokens(RPC::RPCSpecValueList::create());
	tokens->push_back(current_token_);

	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("tokens", tokens);
	rpc_remote("get_token_status", args);
}
void ApplClientSessionTunnelendpointEndpoint::app_client_endpoint_get_enrollment_status(void) {
	get_token_status_is_get_enrollment_status_ = true;
	app_client_endpoint_token_status();
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointUser implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointUser::ApplClientSessionTunnelendpointUser(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler), key_store_(key_store) {
}

ApplClientSessionTunnelendpointUser::~ApplClientSessionTunnelendpointUser(void) {
}

ApplClientSessionTunnelendpointUser::APtr ApplClientSessionTunnelendpointUser::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store) {
	return APtr(new ApplClientSessionTunnelendpointUser(async_service, checkpoint_handler, key_store));
}

void ApplClientSessionTunnelendpointUser::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointUser::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "get_client_encoding") {
		rpc_dispatch_get_client_encoding();
		return true;
	}
	if(method == "show_login_prompt") {
		string msg(args->get_as_string("msg"));
		long change_password_enabled(args->get_as_integer("change_password_enabled"));
		rpc_dispatch_show_login_prompt(msg, change_password_enabled);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointUser::rpc_dispatch_get_client_encoding(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("client_encoding", RPC::RPCSpecValueString::create("utf-8"));
	rpc_remote("get_client_encoding_cb", args);
}

void ApplClientSessionTunnelendpointUser::rpc_dispatch_show_login_prompt(const std::string& msg, const bool change_password_enabled) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointUser::rpc_dispatch_show_login_prompt.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	std::string login;
	std::string password;
	appl_client_eventhandler_->app_client_user_request_login(msg, change_password_enabled);
}

void ApplClientSessionTunnelendpointUser::app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("login", RPC::RPCSpecValueString::create(login));
	args->update("password", RPC::RPCSpecValueString::create(password));
	args->update("requestedpassword", RPC::RPCSpecValueInteger::create(requestedpassword));
	rpc_remote("receive_login", args);
}

void ApplClientSessionTunnelendpointUser::app_client_user_response_login_cancelled(void) {
	rpc_remote("login_cancelled");
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointAuthDME implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointAuthDME::ApplClientSessionTunnelendpointAuthDME(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
}

ApplClientSessionTunnelendpointAuthDME::~ApplClientSessionTunnelendpointAuthDME(void) {
}

ApplClientSessionTunnelendpointAuthDME::APtr ApplClientSessionTunnelendpointAuthDME::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointAuthDME(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointAuthDME::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointAuthDME::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "get_dme_values") {
		get_dme_values();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointAuthDME::get_dme_values(void) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointAuthDME::get_dme_values.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		app_client_dme_response_get_dme_values_none();
		return;
	}
	appl_client_eventhandler_->app_client_dme_get_dme_value();
}

void ApplClientSessionTunnelendpointAuthDME::app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time) {
	RPC::RPCSpecValueDict::APtr values(RPC::RPCSpecValueDict::create());
	values->update("username", RPC::RPCSpecValueString::create(username));
	values->update("password", RPC::RPCSpecValueString::create(password));
	values->update("terminal_id", RPC::RPCSpecValueString::create(terminal_id));
	values->update("device_signature", RPC::RPCSpecValueString::create(device_signature));
	values->update("device_time", RPC::RPCSpecValueString::create(device_time));

	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("values", values);
	rpc_remote("get_dme_values_response", args);
}

void ApplClientSessionTunnelendpointAuthDME::app_client_dme_response_get_dme_values_none(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("values", RPC::RPCSpecValueNone::create());
	rpc_remote("get_dme_values_response", args);
}



/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointCPM implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointCPM::ApplClientSessionTunnelendpointCPM(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler), key_store_(key_store) {
	cpm_endpoint_manager_ = ApplClientSessionTunnelendpointCPMEndpointManager::create(async_service_, checkpoint_handler_, key_store_);
}

ApplClientSessionTunnelendpointCPM::~ApplClientSessionTunnelendpointCPM(void) {
}

ApplClientSessionTunnelendpointCPM::APtr ApplClientSessionTunnelendpointCPM::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store) {
	return APtr(new ApplClientSessionTunnelendpointCPM(async_service, checkpoint_handler, key_store));
}

void ApplClientSessionTunnelendpointCPM::set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler) {
	ApplClientSessionTunnelendpoint::set_appl_client_eventhandler(appl_client_eventhandler);
	if(cpm_endpoint_manager_.get() != NULL) {
		cpm_endpoint_manager_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
}

void ApplClientSessionTunnelendpointCPM::reset_appl_client_eventhandler(void) {
	ApplClientSessionTunnelendpoint::reset_appl_client_eventhandler();
	if(cpm_endpoint_manager_.get() != NULL) {
		cpm_endpoint_manager_->reset_appl_client_eventhandler();
	}
}

void ApplClientSessionTunnelendpointCPM::appl_client_session_tunnelendpoint_connected(void) {
	if(cpm_endpoint_manager_.get() != NULL) {
		com_tunnelendpoint_->add_tunnelendpoint(1, cpm_endpoint_manager_->get_com_tunnelendpoint());
	}
}

void ApplClientSessionTunnelendpointCPM::set_connection_info(const std::string& servers) {
	if(cpm_endpoint_manager_.get() != NULL) {
		cpm_endpoint_manager_->set_connection_info(servers);
	}
}

bool ApplClientSessionTunnelendpointCPM::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_analyze_start_connect_update") {
		rpc_dispatch_remote_analyze_start_connect_update(args->get_as_integer("do_update_servers"), args->get_as_string("servers"));
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointCPM::rpc_dispatch_remote_analyze_start_connect_update(const unsigned int& do_update_servers, const std::string& servers) {
	if(do_update_servers) {
		appl_client_eventhandler_->app_client_cpm_update_connection_info(servers);
	}
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointCPMEndpointManager implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointCPMEndpointManager::ApplClientSessionTunnelendpointCPMEndpointManager(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler), key_store_(key_store), close_when_entering_background_(true) {
}

ApplClientSessionTunnelendpointCPMEndpointManager::~ApplClientSessionTunnelendpointCPMEndpointManager(void) {
}

ApplClientSessionTunnelendpointCPMEndpointManager::APtr ApplClientSessionTunnelendpointCPMEndpointManager::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store) {
	return APtr(new ApplClientSessionTunnelendpointCPMEndpointManager(async_service, checkpoint_handler, key_store));
}

void ApplClientSessionTunnelendpointCPMEndpointManager::set_connection_info(const std::string& servers) {
	servers_ = servers;
}

void ApplClientSessionTunnelendpointCPMEndpointManager::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_query_installed_meta") {
		rpc_dispatch_remote_query_installed_meta();
		return true;
	}
	if(method == "remote_query_servers_checksum") {
		rpc_dispatch_remote_query_servers_checksum();
		return true;
	}
	if(method == "remote_set_offline_credential_timeout_min") {
		rpc_dispatch_remote_set_offline_credential_timeout_min(args->get_as_integer("timeout_min"));
		return true;
	}
	if(method == "remote_set_background_settings") {
		rpc_dispatch_set_background_settings(args->get_as_integer("close_when_entering_background"));
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch_remote_query_installed_meta(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	RPC::RPCSpecValueTuple::APtr rc_value(RPC::RPCSpecValueTuple::create());
	rc_value->push_back(RPC::RPCSpecValueInteger::create(1));
	rc_value->push_back(RPC::RPCSpecValueString::create("Ok"));
	args->update("rc", rc_value);
	args->update("meta_strings", RPC::RPCSpecValueList::create());
	rpc_remote("remote_query_installed_meta_response", args);
}

void ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch_remote_query_servers_checksum(void) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());

	std:string servers_normalized = servers_;
	servers_normalized.erase(std::remove(servers_normalized.begin(), servers_normalized.end(), '\n'), servers_normalized.end());
	servers_normalized.erase(std::remove(servers_normalized.begin(), servers_normalized.end(), '\r'), servers_normalized.end());

    boost::shared_ptr<CryptFacility::CryptFactory> factory(CryptFacility::CryptFacilityService::getInstance().getCryptFactory());
    CryptFacility::Hash::APtr hash_algo_sha1(factory->getHashSHA1());

    string servers_normalized_sha1 = hash_algo_sha1->calculateDigest(DataBufferManaged::create(servers_normalized))->encodeHex()->toString();
	args->update("servers_checksum", RPC::RPCSpecValueString::create(servers_normalized_sha1));
	rpc_remote("remote_query_servers_checksum_response", args);
}

void ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch_remote_set_offline_credential_timeout_min(const unsigned int timeout_min) {
}

void ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch_set_background_settings(const unsigned int close_when_entering_background) {
	close_when_entering_background_ = close_when_entering_background;
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointCPMEndpointManager::rpc_dispatch_set_background_settings.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_set_background_settings(close_when_entering_background);
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointDialogInformer implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointDialogInformer::ApplClientSessionTunnelendpointDialogInformer(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
}

ApplClientSessionTunnelendpointDialogInformer::~ApplClientSessionTunnelendpointDialogInformer(void) {
}

ApplClientSessionTunnelendpointDialogInformer::APtr ApplClientSessionTunnelendpointDialogInformer::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointDialogInformer(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointDialogInformer::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_inform_on_first_access") {
		rpc_dispatch_remote_inform_on_first_access(args->get_as_string("message"), args->get_as_integer("close_on_cancel"));
		return true;
	}
	else if(method == "remote_show_license_info") {
		rpc_dispatch_remote_show_license_info(args->get_as_string("header"), args->get_as_string("message"), args->get_as_integer("valid"));
		return true;
	}
	else if(method == "remote_show_menu_updated") {
		rpc_dispatch_remote_show_menu_updated();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_inform_on_first_access(const std::string& message, const long close_on_cancel) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_inform_on_first_access.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_request_inform_on_first_access(message, close_on_cancel);
}

void ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_show_license_info(const std::string& header, const std::string& message, const long valid) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_show_license_info.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_show_license_info(header, message, valid);
}

void ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_show_menu_updated(void) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialogInformer::rpc_dispatch_remote_show_menu_updated.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_menu_updated();
}

void ApplClientSessionTunnelendpointDialogInformer::app_client_dialog_response_inform_on_first_access(void) {
	rpc_remote("remote_welcome_message_response");
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointDialog implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointDialog::ApplClientSessionTunnelendpointDialog(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
	dialog_informer_ = ApplClientSessionTunnelendpointDialogInformer::create(async_service_, checkpoint_handler_);
	com_tunnelendpoint_->add_tunnelendpoint(1, dialog_informer_->get_com_tunnelendpoint());
}

ApplClientSessionTunnelendpointDialog::~ApplClientSessionTunnelendpointDialog(void) {
}

ApplClientSessionTunnelendpointDialog::APtr ApplClientSessionTunnelendpointDialog::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointDialog(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointDialog::set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler) {
	ApplClientSessionTunnelendpoint::set_appl_client_eventhandler(appl_client_eventhandler);
	if(dialog_informer_.get() != NULL) {
		dialog_informer_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
}

void ApplClientSessionTunnelendpointDialog::reset_appl_client_eventhandler(void) {
	ApplClientSessionTunnelendpoint::reset_appl_client_eventhandler();
	if(dialog_informer_.get() != NULL) {
		dialog_informer_->reset_appl_client_eventhandler();
	}
}

void ApplClientSessionTunnelendpointDialog::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointDialog::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "server_cb_show_menu") {
		rpc_dispatch_server_cb_show_menu(args->get_as_list("menu"), args->get_as_dict("tag_properties"));
		return true;
	}
	if(method == "server_cb_launch_cancled_missing_package") {
		rpc_dispatch_remote_launch_cancled_missing_package(args->get_as_string("package_id_missing"));
		return true;
	}
	if(method == "remote_image_found") {
		rpc_dispatch_remote_image_found(args->get_as_string("request_id"), args->get_as_string("image_id"), args->get_as_string("image_data"));
		return true;
	}
	if(method == "remote_image_not_found") {
		rpc_dispatch_remote_image_not_found(args->get_as_string("request_id"), args->get_as_string("image_id"));
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

bool ApplClientSessionTunnelendpointDialog::has_tag(const std::vector<std::string>& tags, const std::string& tag_to_find) {
	std::vector<std::string>::const_iterator i(tags.begin());
	while(i != tags.end()) {
		if(*i == tag_to_find) {
			return true;
		}
		++i;
	}
	return false;
}

void ApplClientSessionTunnelendpointDialog::rpc_dispatch_server_cb_show_menu(const std::vector<RPC::RPCSpecValue::APtr>& menu, const RPC::RPCSpecValueDict::APtr& rpc_tag_properties) {
	std::vector<ApplClientMenuItem> menu_items;
	std::vector<RPC::RPCSpecValue::APtr>::const_iterator i(menu.begin());
	while(i != menu.end()) {
		RPC::RPCSpecValueDict::APtr menu_as_dict(RPC::value_to_dict(*i));

		ApplClientMenuItem menu_item;
		menu_item.id = menu_as_dict->get_as_integer("id");
		menu_item.launch_id = menu_as_dict->get_as_string("launch_id");
		menu_item.title = menu_as_dict->get_as_string("title");
		menu_item.icon_id = menu_as_dict->get_as_string("icon_id", "");

		menu_item.disabled = menu_as_dict->get_as_integer("disabled", 0);
		menu_item.tooltip = menu_as_dict->get_as_string("tooltip", "");

		menu_item.tags = menu_as_dict->get_as_valuelist("tags")->get_as_list_of_strings();
		menu_item.tag_show = has_tag(menu_item.tags, "SHOW");
		menu_item.tag_enabled = has_tag(menu_item.tags, "ENABLED");
		menu_item.tag_topx = has_tag(menu_item.tags, "TOPX");
		menu_item.tag_gupdate = has_tag(menu_item.tags, "GUPDATE");
		menu_item.tag_appbox_default = has_tag(menu_item.tags, "APPBOX_DEFAULT");
		menu_item.tag_appbox_link = has_tag(menu_item.tags, "APPBOX_LINK");

		menu_items.push_back(menu_item);
		i++;
	}
	std::map<std::string, ApplClientTagProperty> tag_properties;

    std::map<std::string, RPC::RPCSpecValue::APtr> rpc_tag_property_pairs(rpc_tag_properties->get_value());
    std::map<std::string, RPC::RPCSpecValue::APtr>::const_iterator j(rpc_tag_property_pairs.begin());
	while(j != rpc_tag_property_pairs.end()) {
		string tag_name(j->first);
		RPC::RPCSpecValueDict::APtr rpc_tag_property_as_dict(RPC::value_to_dict(j->second));

		ApplClientTagProperty tag_property;
		tag_property.name = tag_name;
		tag_property.auto_launch = rpc_tag_property_as_dict->get_as_integer("auto_launch", 0);
		tag_property.auto_launch_first = rpc_tag_property_as_dict->get_as_integer("auto_launch_first", 0);
		tag_properties[tag_name] = tag_property;
		j++;
	}

	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialog::app_client_dialog_show_menu.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_show_menu(menu_items, tag_properties);
}

void ApplClientSessionTunnelendpointDialog::rpc_dispatch_remote_launch_cancled_missing_package(const std::string& package_id_missing) {
	// TODO: Ignore rpc_dispatch_remote_launch_cancled_missing_package
}

void ApplClientSessionTunnelendpointDialog::rpc_dispatch_remote_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialog::rpc_dispatch_remote_image_found.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_image_found(request_id, image_id, image_data);
}

void ApplClientSessionTunnelendpointDialog::rpc_dispatch_remote_image_not_found(const std::string& request_id, const std::string& image_id) {
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialog::rpc_dispatch_remote_image_found.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	appl_client_eventhandler_->app_client_dialog_image_not_found(request_id, image_id);
}

void ApplClientSessionTunnelendpointDialog::app_client_dialog_response_inform_on_first_access(void) {
	if(dialog_informer_.get() == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialog::app_client_response_inform_on_first_access.no_dialog_informer_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	dialog_informer_->app_client_dialog_response_inform_on_first_access();
}

void ApplClientSessionTunnelendpointDialog::app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("launch_id", RPC::RPCSpecValueString::create(launch_id));
	args->update("auto_launch", RPC::RPCSpecValueInteger::create(auto_launch));
	rpc_remote("client_cb_menu_item_selected", args);
}

void ApplClientSessionTunnelendpointDialog::app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format) {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("request_id", RPC::RPCSpecValueString::create(request_id));
	args->update("image_style", RPC::RPCSpecValueString::create(image_style));

	RPC::RPCSpecValueList::APtr arg_image_ids(RPC::RPCSpecValueList::create_from_strings(image_ids));
	args->update("image_ids", arg_image_ids);

	args->update("image_size_x", RPC::RPCSpecValueInteger::create(image_size_x));
	args->update("image_size_y", RPC::RPCSpecValueInteger::create(image_size_y));
	args->update("image_format", RPC::RPCSpecValueString::create(image_format));
	rpc_remote("remote_get_images", args);
}



/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTraffic implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTraffic::ApplClientSessionTunnelendpointTraffic(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler),
	  session_tunnelendpoint_endpoint_(session_tunnelendpoint_endpoint),
	  child_inform_pending_count_(0),
	  child_inform_timer_(async_service->get_io_service()) {
}

ApplClientSessionTunnelendpointTraffic::~ApplClientSessionTunnelendpointTraffic(void) {
}

ApplClientSessionTunnelendpointTraffic::APtr ApplClientSessionTunnelendpointTraffic::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint) {
	return APtr(new ApplClientSessionTunnelendpointTraffic(async_service, checkpoint_handler, session_tunnelendpoint_endpoint));
}

void ApplClientSessionTunnelendpointTraffic::close(void) {
	state_ = State_Closing;

	if(child_inform_pending_count_ > 0) {
		child_inform_timer_.expires_from_now(boost::posix_time::seconds(0));
	}

	std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr>::iterator i(children_.begin());
	while(i != children_.end()) {
		i->second->close();
		++i;
	}
	close_janitor_start();
}

bool ApplClientSessionTunnelendpointTraffic::close_janitor_is_closed(void) {
	// Check if all clients are closed
	std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr>::iterator i(children_.begin());
	while(i != children_.end()) {
		if(! i->second->is_closed()) {
			return false;
		}
		++i;
	}

	// Check if pending child informs
	if (child_inform_pending_count_ > 0) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointTraffic::close_janitor_is_closed", Attr_ApplClient(), CpAttr_debug(), CpAttr("child_inform_pending_count_", child_inform_pending_count_));
		return false;
	}
	return true;
}

void ApplClientSessionTunnelendpointTraffic::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointTraffic::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "traffic_launch_child_with_launch_id") {
		rpc_dispatch_traffic_launch_child_with_launch_id(args->get_as_integer("child_id"), args->get_as_integer("launch_type"), args->get_as_string("launch_id"));
		return true;
	}
	if(method == "show_message") {
		string title(args->get_as_string("title"));
		string text(args->get_as_string("text"));
		rpc_dispatch_remote_call_show_message(title, text);
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointTraffic::rpc_dispatch_traffic_launch_child_with_launch_id(const long child_id, const long launch_type, const std::string& launch_id) {

//	if(launch_type == 0 || launch_type == 8 || launch_type == 9 || launch_type == 10) {
//		ApplClientSessionTunnelendpointTrafficChild::APtr child(ApplClientSessionTunnelendpointTrafficChildPortforward::create(async_service_, checkpoint_handler_, this, child_id, launch_id));
//		children_[child_id] = child;
//		child->set_appl_client_eventhandler(appl_client_eventhandler_);
//		child->notify_state_change();
//		com_tunnelendpoint_->add_tunnelendpoint(child_id, child->get_com_tunnelendpoint());
//		return;
//	}

	if(launch_type == 11) {
		ApplClientSessionTunnelendpointTrafficChild::APtr child(ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::create(async_service_, checkpoint_handler_, this, child_id, launch_id));
		children_[child_id] = child;
		child->set_appl_client_eventhandler(appl_client_eventhandler_);
		child->notify_state_change();
		com_tunnelendpoint_->add_tunnelendpoint(child_id, child->get_com_tunnelendpoint());
		return;
	}
//	else if(launch_type == 2) {
//		ApplClientSessionTunnelendpointTrafficChild::APtr child(ApplClientSessionTunnelendpointTrafficChildInternal::create(async_service_, checkpoint_handler_, this, child_id, launch_id, session_tunnelendpoint_endpoint_));
//		children_[child_id] = child;
//		child->set_appl_client_eventhandler(appl_client_eventhandler_);
//		child->notify_state_change();
//		com_tunnelendpoint_->add_tunnelendpoint(child_id, child->get_com_tunnelendpoint());
//		return;
//	}
	if (appl_client_eventhandler_ == NULL) {
		Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointDialog::app_client_dialog_show_menu.no_eventhandler_available", Attr_ApplClient(), CpAttr_error());
		return;
	}
	stringstream ss;
	ss << "The launch type " << launch_type << " is currently not support on this platform";
	appl_client_eventhandler_->app_client_error("Launch information", ss.str());
}

void ApplClientSessionTunnelendpointTraffic::rpc_dispatch_remote_call_show_message(const std::string& title, const std::string& text) {
	appl_client_eventhandler_->app_client_info(title, text);
}

void ApplClientSessionTunnelendpointTraffic::close_start(const std::string& launch_id) {
	ApplClientSessionTunnelendpointTrafficChild::APtr child(get_child_by_launch_id(launch_id));
	if(child.get() != NULL) {
		child->close();
	}
}

void ApplClientSessionTunnelendpointTraffic::child_closed_and_done(long child_id) {
	std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr>::iterator i(children_.find(child_id));
	if(i != children_.end()) {
		child_inform_not_launched_start(i->second->get_launch_id());
		children_.erase(i);
	}
}

void ApplClientSessionTunnelendpointTraffic::child_error(long child_id) {
	switch(state_) {
	case State_Closing:
	case State_Closed:
		return;
	default:
		break;
	}
	child_inform_pending_count_ += 1;
	child_inform_timer_.expires_from_now(boost::posix_time::seconds(2));
	child_inform_timer_.async_wait(boost::bind(&ApplClientSessionTunnelendpointTraffic::child_close, this, boost::asio::placeholders::error, child_id));
}

void ApplClientSessionTunnelendpointTraffic::child_close(const boost::system::error_code& error, const long child_id) {
	switch(state_) {
	case State_Closing:
	case State_Closed:
		return;
	default:
		break;
	}
	child_inform_pending_count_ -= 1;
	std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr>::iterator i(children_.find(child_id));
	if(i != children_.end()) {
		i->second->close_with_events();
	}
}

void ApplClientSessionTunnelendpointTraffic::child_inform_not_launched_start(const std::string launch_id) {
	switch(state_) {
	case State_Closing:
	case State_Closed:
		return;
	default:
		break;
	}
	child_inform_pending_count_ += 1;
	child_inform_timer_.expires_from_now(boost::posix_time::seconds(2));
	child_inform_timer_.async_wait(boost::bind(&ApplClientSessionTunnelendpointTraffic::child_inform_not_launched, this, boost::asio::placeholders::error, launch_id));
}

void ApplClientSessionTunnelendpointTraffic::child_inform_not_launched(const boost::system::error_code& error, const std::string launch_id) {
	child_inform_pending_count_ -= 1;
	if(appl_client_eventhandler_ != NULL) {
		appl_client_eventhandler_->app_client_traffic_launch_status_changed(launch_id, ApplClientEventhandler::StateNotLaunched, "");
	}
}

ApplClientSessionTunnelendpointTrafficChild::APtr ApplClientSessionTunnelendpointTraffic::get_child_by_launch_id(const std::string& launch_id) {
	std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr>::const_iterator i(children_.begin());
	while(i != children_.end()) {
		if (i->second->get_launch_id() == launch_id) {
			return i->second;
		}
		++i;
	}
	return ApplClientSessionTunnelendpointTrafficChild::APtr();
}

bool ApplClientSessionTunnelendpointTraffic::app_client_dialog_menu_item_is_launched(const std::string& launch_id) {
	ApplClientSessionTunnelendpointTrafficChild::APtr child(get_child_by_launch_id(launch_id));
	if(child.get() != NULL) {
		return child->is_running();
	}
	return false;
}

ApplClientEventhandler::State ApplClientSessionTunnelendpointTraffic::app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info) {
	ApplClientSessionTunnelendpointTrafficChild::APtr child(get_child_by_launch_id(launch_id));
	if(child.get() != NULL) {
		return child->get_state(state_info);
	}
	return ApplClientEventhandler::StateNotLaunched;
}

void ApplClientSessionTunnelendpointTraffic::app_client_traffic_launch_command_running(const std::string& launch_id) {
	ApplClientSessionTunnelendpointTrafficChild::APtr child(get_child_by_launch_id(launch_id));
	if(child.get() != NULL) {
		child->app_client_traffic_launch_command_running();
	}
}

void ApplClientSessionTunnelendpointTraffic::app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message) {
	ApplClientSessionTunnelendpointTrafficChild::APtr child(get_child_by_launch_id(launch_id));
	if(child.get() != NULL) {
		child->app_client_traffic_launch_command_terminated(error, error_message);
	}
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTrafficChild implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTrafficChild::ApplClientSessionTunnelendpointTrafficChild(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id)
 : ApplClientSessionTunnelendpoint(async_service, checkpoint_handler), child_eventhandler_(child_eventhandler), child_id_(child_id), launch_id_(launch_id), state_(ApplClientEventhandler::StateLaunching) {
}

ApplClientSessionTunnelendpointTrafficChild::~ApplClientSessionTunnelendpointTrafficChild(void) {
}

std::string ApplClientSessionTunnelendpointTrafficChild::get_launch_id(void) {
	return launch_id_;
}

ApplClientEventhandler::State ApplClientSessionTunnelendpointTrafficChild::get_state(std::string& state_info) {
	state_info = state_info_;
	return state_;
}
void ApplClientSessionTunnelendpointTrafficChild::set_state(const ApplClientEventhandler::State state, const std::string& state_info) {
	state_info_ = state_info;
	state_ = state;
	notify_state_change();
}

void ApplClientSessionTunnelendpointTrafficChild::notify_state_change(void) {
	appl_client_eventhandler_->app_client_traffic_launch_status_changed(launch_id_, state_, state_info_);
}

void ApplClientSessionTunnelendpointTrafficChild::close(void) {
	close_start();
}

void ApplClientSessionTunnelendpointTrafficChild::close_with_events(void) {
	close_start();
}

void ApplClientSessionTunnelendpointTrafficChild::app_client_traffic_launch_command_running(void) {
	// Ignored
}

void ApplClientSessionTunnelendpointTrafficChild::app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message) {
	// Ignored
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTrafficChildPortforward implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTrafficChildPortforward::ApplClientSessionTunnelendpointTrafficChildPortforward(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id)
	: ApplClientSessionTunnelendpointTrafficChild(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id), command_state_(StateCommandLaunching) {
}

ApplClientSessionTunnelendpointTrafficChildPortforward::~ApplClientSessionTunnelendpointTrafficChildPortforward(void) {
}

ApplClientSessionTunnelendpointTrafficChildPortforward::APtr ApplClientSessionTunnelendpointTrafficChildPortforward::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id) {
	return APtr(new ApplClientSessionTunnelendpointTrafficChildPortforward(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id));
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::appl_client_session_tunnelendpoint_connected(void) {
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_ready(const unsigned long id) {
	children_waiting_for_ready_.erase(id);

	if (children_waiting_for_ready_.empty()) {
		std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr>::const_iterator i(portforwards_.begin());
		while(i != portforwards_.end()) {
			(i->second)->accept_start();
			++i;
		}
		if (state_ == ApplClientEventhandler::StateLaunching ) {
			set_state(ApplClientEventhandler::StateReady, build_state_info());
			command_state_ = StateCommandAbsent;
			if(launch_command_ != "") {
				command_state_ = StateCommandLaunching;
				if(param_file_template_ != "") {
					string param_file_template = parameter_subst(param_file_template_);
					COMHTTPDataServer::APtr httpDataServer(COMHTTPDataServer::create(async_service_, checkpoint_handler_, param_file_template, "", param_file_name_, boost::posix_time::seconds(param_file_lifetime_), "", 0));
					httpDataServer->start();
					param_file_url_ = httpDataServer->get_url();
				}
				ApplClientCommand command(parameter_subst(launch_command_));
				if(command.getCommandType() == ApplClientCommand::CommandType_Invalid) {
					app_client_traffic_launch_command_terminated(true, command.getLastError());
				    Checkpoint cp(*checkpoint_handler_, command.getLastError(), Attr_ApplClient(), CpAttr_debug());
				}
				else {
					appl_client_eventhandler_->app_client_traffic_launch_command(get_launch_id(), command);
				}
			}

		}
	}
}

std::string ApplClientSessionTunnelendpointTrafficChildPortforward::parameter_subst(const std::string& launch_command) {
	std::string portforward_host("%(portforward.host)");
	std::string portforward_port("%(portforward.port)");
	std::string launch_param_file("%(launch.param_file)");

	// Only the first portforward is used
	std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr>::const_iterator i(portforwards_.begin());
	if(i != portforwards_.end()) {
		if (!((i->second)->is_closed())) {
			std::pair<std::string, unsigned long> ip_local_info((i->second)->get_ip_local());
			portforward_host = ip_local_info.first;
			stringstream ss;
			ss << ip_local_info.second;
			portforward_port = ss.str();
		}
	}
	string launch_command_result(launch_command);
	boost::replace_all(launch_command_result, "%(portforward.host)", portforward_host);
	boost::replace_all(launch_command_result, "%(portforward.port)", portforward_port);
	boost::replace_all(launch_command_result, "%(launch.param_file)", param_file_url_);

	return launch_command_result;
}

std::string ApplClientSessionTunnelendpointTrafficChildPortforward::build_state_info(void) {
	std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr>::const_iterator i(portforwards_.begin());
	bool all_ok = true;
	stringstream ss;

	while(i != portforwards_.end()) {
		if (!((i->second)->is_closed())) {
			std::pair<std::string, unsigned long> ip_local_info((i->second)->get_ip_local());
			ss << ip_local_info.first << ":" << ip_local_info.second << " ";
		}
		++i;
	}
	switch(command_state_) {
	case StateCommandLaunching:
		ss << "Launching command";
		break;
	case StateCommandRunning:
		ss << "Command is running";
		break;
	case StateCommandNotRunning:
		ss << "Command is not running";
		break;
	}
	return ss.str();
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::app_client_traffic_launch_command_running(void) {
	switch(command_state_) {
	case StateCommandLaunching:
		break;
	default:
		return;
	}
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("is_ok", RPC::RPCSpecValueInteger::create(1));
	rpc_remote("remote_call_launch_response", args);

	command_state_ = StateCommandRunning;
	set_state(ApplClientEventhandler::StateReady, build_state_info());
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message) {
	switch(command_state_) {
	case StateCommandLaunching:
	case StateCommandRunning:
		break;
	default:
		return;
	}

	if(command_state_ == StateCommandLaunching) {
		RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
		args->update("is_ok", RPC::RPCSpecValueInteger::create(0));
		args->update("is_ok", RPC::RPCSpecValueString::create(error_message));
		rpc_remote("remote_call_launch_response_with_message", args);
	}
	command_state_ = StateCommandNotRunning;

	if (state_ == ApplClientEventhandler::StateClosing) {
		close_helper();
	}
	else {
		close_helper_from_client();
	}
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_closed(const unsigned long id) {
	std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr>::iterator i(portforwards_.find(id));
	if(i != portforwards_.end()) {
		portforwards_.erase(i);
	}
	if (state_ == ApplClientEventhandler::StateClosing) {
		close_helper();
	}
	else {
		close_helper_from_client();
	}
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message) {
}

bool ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_connected(const unsigned long id, const TunnelendpointTCP::APtr& connection) {
	return true;
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_acceptor_error(const unsigned long id, const std::string& message) {
	Checkpoint cps(*checkpoint_handler_, "ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_acceptor_error", Attr_ApplClient(), CpAttr_error());
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
		set_state(ApplClientEventhandler::StateError, message);
		if(child_eventhandler_) {
			child_eventhandler_->child_error(child_id_);
		}
		if(appl_client_eventhandler_) {
			appl_client_eventhandler_->app_client_error("Portforward error", message);
		}
		break;
	}
	close_start();
}

bool ApplClientSessionTunnelendpointTrafficChildPortforward::tunnelendpoint_tcp_eh_accept_start_continue(const unsigned long id) {
	return true;
}

bool ApplClientSessionTunnelendpointTrafficChildPortforward::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_call_launch_with_tcp_options") {
//	    def remote_call_launch_with_tcp_options(self, launch_command, working_directory,
//	                                        param_file_name, param_file_lifetime, param_file_template,
//	                                        close_with_process, kill_on_close,
//	                                        client_portforwards,
//	                                        tcp_options):
		rpc_dispatch_traffic_remote_call_launch_with_tcp_options(
				args->get_as_string("launch_command"),
				args->get_as_integer("close_with_process"),
				args->get_as_integer("kill_on_close"),
				args->get_as_dict("client_portforwards"),
				args->get_as_string("param_file_name"),
				args->get_as_integer("param_file_lifetime"),
				args->get_as_string("param_file_template")
				);
		return true;
	}
	else if(method == "remote_launch") {
//	    def remote_launch(self, launch_command, working_directory,
//	            param_file_name, param_file_lifetime, param_file_template,
//	            close_with_process, kill_on_close,
//	            client_portforwards,
//	            tcp_options):
		rpc_dispatch_traffic_remote_call_launch_with_tcp_options(
				args->get_as_string("launch_command"),
				args->get_as_integer("close_with_process"),
				args->get_as_integer("kill_on_close"),
				args->get_as_dict("client_portforwards"),
				args->get_as_string("param_file_name"),
				args->get_as_integer("param_file_lifetime"),
				args->get_as_string("param_file_template")
				);
		return true;
	}
	else if(method == "remote_call_closed") {
		rpc_dispatch_traffic_remote_call_closed();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::rpc_dispatch_traffic_remote_call_launch_with_tcp_options(
		const std::string& launch_command,
		const bool close_with_process,
		const bool kill_on_close,
		const RPC::RPCSpecValueDict::APtr& client_portforwards,
		const std::string& param_file_name,
		const int param_file_lifetime,
		const std::string& param_file_template) {
	std::map<std::string, RPC::RPCSpecValue::APtr>::const_iterator i(client_portforwards->get_value().begin());
	while(i != client_portforwards->get_value().end()) {
		RPC::RPCSpecValueDict::APtr portforward_info(value_to_dict(i->second));

		long child_id(boost::lexical_cast<long>(i->first));
		std::string client_host(portforward_info->get_as_string("client_host"));
		long client_port(portforward_info->get_as_integer("client_port"));

		TunnelendpointTCPTunnelClient::APtr portforward(TunnelendpointTCPTunnelClient::create(checkpoint_handler_, async_service_->get_io_service(), client_host, client_port, child_id));
		portforward->set_tcp_eventhandler(this);
		portforward->set_option_reuse_address(true);
		portforwards_[child_id] = portforward;
		com_tunnelendpoint_->add_tunnelendpoint(child_id, portforward);
		++i;
		children_waiting_for_ready_.insert(child_id);
	}
	launch_command_ = launch_command;
	param_file_name_ = param_file_name;
	param_file_lifetime_ = param_file_lifetime;
	param_file_template_ = param_file_template;
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::rpc_dispatch_traffic_remote_call_closed(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
		close_helper_from_server();
		break;
	case ApplClientEventhandler::StateClosing:
	case ApplClientEventhandler::StateError:
	case ApplClientEventhandler::StateNotLaunched:
		break;
	}
}

bool ApplClientSessionTunnelendpointTrafficChildPortforward::is_running(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
		return true;
	}
	return false;
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::close_start(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	default:
		return;
	}
	close_helper_from_client();
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::close_helper_from_server(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	case ApplClientEventhandler::StateClosing:
		close_helper();
		return;
	default:
		return;
	}
	set_state(ApplClientEventhandler::StateClosing, "");
	close_state_ = CloseState_Init;
	close_from_server_ = true;
	close_helper();
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::close_helper_from_client(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	case ApplClientEventhandler::StateClosing:
		close_helper();
		return;
	default:
		return;
	}
	set_state(ApplClientEventhandler::StateClosing, "");
	ApplClientSessionTunnelendpoint::state_ = ApplClientSessionTunnelendpoint::State_Closing;
	close_state_ = CloseState_Init;
	close_from_server_ = false;
	close_helper();
}

void ApplClientSessionTunnelendpointTrafficChildPortforward::close_helper(void) {
	switch(state_) {
	case ApplClientEventhandler::StateClosing:
		break;
	default:
		return;
	}

	if(close_state_ == CloseState_Init) {
		switch(command_state_) {
		case StateCommandLaunching:
		case StateCommandRunning:
			close_state_ = CloseState_WaitingForCommandToTerminate;
			appl_client_eventhandler_->app_client_traffic_launch_command_terminate(get_launch_id());
			break;
		default:
			close_state_ = CloseState_CommandTerminated;
		}
	}

	if(close_state_ == CloseState_WaitingForCommandToTerminate) {
		switch(command_state_) {
		case StateCommandNotRunning:
		case StateCommandAbsent:
			close_state_ = CloseState_CommandTerminated;
		}
	}

	if(close_state_ == CloseState_CommandTerminated) {
		std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr>::const_iterator i(portforwards_.begin());
		while(i != portforwards_.end()) {
			// Close can cause removal of element in portforwars_
			com_tunnelendpoint_->get_mutex()->get_strand().post(boost::bind(&Communication::TunnelendpointTCPTunnelClient::close, (i->second), false));
			++i;
		}
		close_state_ = CloseState_WaitingForPortforwards;
	}

	if(close_state_ == CloseState_WaitingForPortforwards) {
		if (portforwards_.size() == 0) {
			close_state_ = CloseState_AllPortforwarsDone;
		}
	}

	if(close_state_ == CloseState_AllPortforwarsDone) {
		rpc_remote("remote_call_closed");
		set_state(ApplClientEventhandler::StateClosed, "");
		ApplClientSessionTunnelendpoint::state_ = ApplClientSessionTunnelendpoint::State_Closed;
		com_tunnelendpoint_->get_mutex()->get_strand().post(boost::bind(&ApplClientSessionTunnelendpointTrafficChildEventhandler::child_closed_and_done, child_eventhandler_, child_id_));
	}
}



/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id)
	: ApplClientSessionTunnelendpointTrafficChild(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id), command_state_(StateCommandLaunching)  {
}

ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::~ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy(void) {
	if(direct_tunnelendpoint_.get()) {
		direct_tunnelendpoint_->reset_direct_eventhandler();
	}
}

ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::APtr ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id) {
	return APtr(new ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id));
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_call_launch_with_tcp_options") {
		rpc_dispatch_traffic_remote_call_launch_with_tcp_options(args->get_as_string("launch_command"), args->get_as_valuelist("tcp_options"));
		return true;
	}
	else if(method == "remote_launch") {
		rpc_dispatch_traffic_remote_call_launch_with_tcp_options(args->get_as_string("launch_command"), args->get_as_valuelist("tcp_options"));
		return true;
	}
	else if(method == "remote_call_closed") {
		rpc_dispatch_traffic_remote_call_closed();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}


bool ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tcp_option_enable_log_to_server(const RPC::RPCSpecValueList::APtr& tcp_options) {
	 std::vector<std::string> values(tcp_options->get_as_list_of_strings());
	 std::vector<std::string>::const_iterator i(values.begin());
	 while(i != values.end()) {
		 if(*i == "LOG_TO_SERVER_ENABLED") {
			return true;
		}
		i++;
	 }
	return false;
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::rpc_dispatch_traffic_remote_call_launch_with_tcp_options(const std::string& launch_command, const RPC::RPCSpecValueList::APtr& tcp_options) {
	direct_tunnelendpoint_ = TunnelendpointDirectTunnelClient::create(checkpoint_handler_, async_service_->get_io_service(), 100);
	direct_tunnelendpoint_->set_direct_eventhandler(this);

	checkpoint_handler_tunnelendpoint_ = checkpoint_handler_;
	if(tcp_option_enable_log_to_server(tcp_options)) {
		checkpoint_handler_tunnelendpoint_ = direct_tunnelendpoint_->enable_checkpoint_tunnel(CheckpointFilter_true::create(), CheckpointOutputHandlerTEXT::create());
	}

	com_tunnelendpoint_->add_tunnelendpoint(1, direct_tunnelendpoint_);
	launch_command_ = launch_command;
	directConnectionFactory_ = ApplClientDirectConnectionFactory::create(checkpoint_handler_tunnelendpoint_, direct_tunnelendpoint_);
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tunnelendpoint_direct_eh_ready(void) {
	ApplClientCommand command(launch_command_, directConnectionFactory_);
	if(command.getCommandType() == ApplClientCommand::CommandType_Invalid) {
		app_client_traffic_launch_command_terminated(true, command.getLastError());
	    Checkpoint cp(*checkpoint_handler_, command.getLastError(), Attr_ApplClient(), CpAttr_debug());
		return;
	}
	appl_client_eventhandler_->app_client_traffic_launch_command(get_launch_id(), command);
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tunnelendpoint_direct_eh_connection_ready(const Communication::TunnelendpointDirect::APtr& connection) {
	switch(command_state_) {
	case StateCommandRunning:
		break;
	default:
	    Checkpoint cp(*checkpoint_handler_, "ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy.tunnelendpoint_direct_eh_connection_ready.ignored", Attr_ApplClient(), CpAttr_debug(), CpAttr("command_state", command_state_));
		return;
	}
	directConnectionFactory_->connection_ready(connection);
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tunnelendpoint_direct_eh_connection_failed(const std::string& errorMessage) {
	switch(command_state_) {
	case StateCommandRunning:
		break;
	default:
		return;
	}
	directConnectionFactory_->connection_failed(errorMessage);
}


void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tunnelendpoint_direct_eh_closed(void) {
	if (state_ == ApplClientEventhandler::StateClosing) {
		close_helper();
	}
	else {
		close_helper_from_client();
	}
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message) {
}

std::string ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::build_state_info(void) {
	switch(command_state_) {
	case StateCommandLaunching:
		return "Launching command";
		break;
	case StateCommandRunning:
		return "Command is running";
		break;
	case StateCommandNotRunning:
		return "Command is not running";
		break;
	}
	return "Unknown";
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::app_client_traffic_launch_command_running(void) {
	switch(command_state_) {
	case StateCommandLaunching:
		break;
	default:
	    Checkpoint cp(*checkpoint_handler_, "ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy.app_client_traffic_launch_command_running.ignored", Attr_ApplClient(), CpAttr_debug(), CpAttr("command_state", command_state_));
		return;
	}
    Checkpoint cp(*checkpoint_handler_, "ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy.app_client_traffic_launch_command_running", Attr_ApplClient(), CpAttr_debug());
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("is_ok", RPC::RPCSpecValueInteger::create(1));
	rpc_remote("remote_call_launch_response", args);

	command_state_ = StateCommandRunning;
	set_state(ApplClientEventhandler::StateReady, build_state_info());
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message) {
	switch(command_state_) {
	case StateCommandLaunching:
	case StateCommandRunning:
		break;
	default:
		if(state_ == ApplClientEventhandler::StateClosing) {
			break;
		}
		return;
	}

	if(command_state_ == StateCommandLaunching) {
		RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
		args->update("is_ok", RPC::RPCSpecValueInteger::create(0));
		args->update("message", RPC::RPCSpecValueString::create(error_message));
		rpc_remote("remote_call_launch_response_with_message", args);
	}
	command_state_ = StateCommandNotRunning;

	if (state_ == ApplClientEventhandler::StateClosing) {
		close_helper();
	}
	else {
		close_helper_from_client();
	}
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::rpc_dispatch_traffic_remote_call_closed(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
		close_helper_from_server();
		break;
	case ApplClientEventhandler::StateClosing:
	case ApplClientEventhandler::StateError:
	case ApplClientEventhandler::StateNotLaunched:
		break;
	}
}

bool ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::is_running(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
		return true;
	}
	return false;
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::close_start(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	default:
		return;
	}
	close_helper_from_client();
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::close_helper_from_server(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	case ApplClientEventhandler::StateClosing:
		close_helper();
		return;
	default:
		return;
	}
	set_state(ApplClientEventhandler::StateClosing, "");
	close_state_ = CloseState_Init;
	close_from_server_ = true;
	close_helper();
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::close_helper_from_client(void) {
	switch(state_) {
	case ApplClientEventhandler::StateLaunching:
	case ApplClientEventhandler::StateReady:
	case ApplClientEventhandler::StateError:
		break;
	case ApplClientEventhandler::StateClosing:
		close_helper();
		return;
	default:
		return;
	}
	set_state(ApplClientEventhandler::StateClosing, "");
	ApplClientSessionTunnelendpoint::state_ = ApplClientSessionTunnelendpoint::State_Closing;
	close_state_ = CloseState_Init;
	close_from_server_ = false;
	close_helper();
}

void ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy::close_helper(void) {
	switch(state_) {
	case ApplClientEventhandler::StateClosing:
		break;
	default:
		return;
	}

	if(close_state_ == CloseState_Init) {
		switch(command_state_) {
		case StateCommandLaunching:
		case StateCommandRunning:
			close_state_ = CloseState_WaitingForCommandToTerminate;
			appl_client_eventhandler_->app_client_traffic_launch_command_terminate(get_launch_id());
			break;
		default:
			close_state_ = CloseState_CommandTerminated;
		}
	}

	if(close_state_ == CloseState_WaitingForCommandToTerminate) {
		switch(command_state_) {
		case StateCommandNotRunning:
		case StateCommandAbsent:
			close_state_ = CloseState_CommandTerminated;
		}
	}

	if(close_state_ == CloseState_CommandTerminated) {
		rpc_remote("remote_call_closed");
		set_state(ApplClientEventhandler::StateClosed, "");
		ApplClientSessionTunnelendpoint::state_ = ApplClientSessionTunnelendpoint::State_Closed;
		com_tunnelendpoint_->get_mutex()->get_strand().post(boost::bind(&ApplClientSessionTunnelendpointTrafficChildEventhandler::child_closed_and_done, child_eventhandler_, child_id_));
	}
}



/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTrafficChildInternal implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTrafficChildInternal::ApplClientSessionTunnelendpointTrafficChildInternal(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint)
	: ApplClientSessionTunnelendpointTrafficChild(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id), session_tunnelendpoint_endpoint_(session_tunnelendpoint_endpoint) {
}

ApplClientSessionTunnelendpointTrafficChildInternal::~ApplClientSessionTunnelendpointTrafficChildInternal(void) {
}

ApplClientSessionTunnelendpointTrafficChildInternal::APtr ApplClientSessionTunnelendpointTrafficChildInternal::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler, const long child_id, const std::string& launch_id, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint) {
	return APtr(new ApplClientSessionTunnelendpointTrafficChildInternal(async_service, checkpoint_handler, child_eventhandler, child_id, launch_id, session_tunnelendpoint_endpoint));
}

void ApplClientSessionTunnelendpointTrafficChildInternal::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointTrafficChildInternal::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_launch_endpoint_enrollment") {
		rpc_dispatch_remote_launch_endpoint_enrollment();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointTrafficChildInternal::rpc_dispatch_remote_launch_endpoint_enrollment(void) {
//    self.launched_session = self.launch_sessions.endpoint_session
//    if self.launched_session is not None and self.launched_session.launch_internal_ready():
//        self.tunnelendpoint_remote('remote_launch_endpoint_enrollment_response')
//    else:
//        self.close()

	if(session_tunnelendpoint_endpoint_.get() != NULL && session_tunnelendpoint_endpoint_->is_launch_internal_endpoint_enrollment_ready() ) {
		rpc_remote("remote_launch_endpoint_enrollment_response");
		return;
	}
	child_eventhandler_->child_closed_and_done(child_id_);
}

bool ApplClientSessionTunnelendpointTrafficChildInternal::is_running(void) {
	return com_tunnelendpoint_->is_connected();
}

void ApplClientSessionTunnelendpointTrafficChildInternal::close_start(void) {
	set_state(ApplClientEventhandler::StateClosed, "");
	com_tunnelendpoint_->get_mutex()->get_strand().post(boost::bind(&ApplClientSessionTunnelendpointTrafficChildEventhandler::child_closed_and_done, child_eventhandler_, child_id_));
}


/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTagClientOk implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTagClientOk::ApplClientSessionTunnelendpointTagClientOk(
		const Communication::AsyncService::APtr& async_service,
		const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler) {
}

ApplClientSessionTunnelendpointTagClientOk::~ApplClientSessionTunnelendpointTagClientOk(void) {
}

ApplClientSessionTunnelendpointTagClientOk::APtr ApplClientSessionTunnelendpointTagClientOk::create(
		const Communication::AsyncService::APtr& async_service,
		const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointTagClientOk(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointTagClientOk::appl_client_session_tunnelendpoint_connected(void) {
}

bool ApplClientSessionTunnelendpointTagClientOk::rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args) {
	if(method == "remote_get_platform") {
		rpc_dispatch_remote_get_platform();
		return true;
	}
	return ApplClientSessionTunnelendpoint::rpc_dispatch(method, args);
}

void ApplClientSessionTunnelendpointTagClientOk::rpc_dispatch_remote_get_platform(void)  {
	if(appl_client_eventhandler_) {
		appl_client_eventhandler_->app_client_tag_get_platform();
	}
}

void ApplClientSessionTunnelendpointTagClientOk::rpc_dispatch_remote_get_platform_response(const std::string& platform)  {
	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("platform", RPC::RPCSpecValueString::create(platform));
	rpc_remote("remote_get_platform_response", args);
}

/*
 * ------------------------------------------------------------------
 * ApplClientSessionTunnelendpointTag implementation
 * ------------------------------------------------------------------
 */
ApplClientSessionTunnelendpointTag::ApplClientSessionTunnelendpointTag(
		const Communication::AsyncService::APtr& async_service,
		const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: ApplClientSessionTunnelendpoint(async_service, checkpoint_handler){
}

ApplClientSessionTunnelendpointTag::~ApplClientSessionTunnelendpointTag(void) {
}

ApplClientSessionTunnelendpointTag::APtr ApplClientSessionTunnelendpointTag::create(
		const Communication::AsyncService::APtr& async_service,
		const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new ApplClientSessionTunnelendpointTag(async_service, checkpoint_handler));
}

void ApplClientSessionTunnelendpointTag::set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler) {
	ApplClientSessionTunnelendpoint::set_appl_client_eventhandler(appl_client_eventhandler);
	if(tag_plugin_client_ok_.get() != NULL) {
		tag_plugin_client_ok_->set_appl_client_eventhandler(appl_client_eventhandler);
	}
}

void ApplClientSessionTunnelendpointTag::reset_appl_client_eventhandler(void) {
	ApplClientSessionTunnelendpoint::reset_appl_client_eventhandler();
	if(tag_plugin_client_ok_.get() != NULL) {
		tag_plugin_client_ok_->reset_appl_client_eventhandler();
	}
}

void ApplClientSessionTunnelendpointTag::appl_client_session_tunnelendpoint_connected(void) {
	tag_plugin_client_ok_ = ApplClientSessionTunnelendpointTagClientOk::create(async_service_, checkpoint_handler_);
	tag_plugin_client_ok_->set_appl_client_eventhandler(appl_client_eventhandler_);
	com_tunnelendpoint_->add_tunnelendpoint(1, tag_plugin_client_ok_->get_com_tunnelendpoint());

	RPC::RPCSpecValueDict::APtr args(RPC::RPCSpecValueDict::create());
	args->update("plugin_name", RPC::RPCSpecValueString::create("client_ok"));
	args->update("child_id", RPC::RPCSpecValueInteger::create(1));
	rpc_remote("remote_tag_plugin_created", args);
	rpc_remote("remote_all_tag_plugins_created");
}

void ApplClientSessionTunnelendpointTag::rpc_dispatch_remote_get_platform_response(const std::string& platform)  {
	if(tag_plugin_client_ok_.get() != NULL) {
		tag_plugin_client_ok_->rpc_dispatch_remote_get_platform_response(platform);
	}
}

