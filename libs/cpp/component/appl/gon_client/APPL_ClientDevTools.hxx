/*! \file APPL_ClientDevTools.hxx
 \brief This file contains the client develoment tools
 */
#ifndef APPL_ClientDevTools_HXX
#define APPL_ClientDevTools_HXX

#include <boost/shared_ptr.hpp>

#include <string>

namespace Giritech {
namespace Appl {

/*
 */
class ApplClientDevToolsNetworkLog {
public:
	typedef boost::shared_ptr<ApplClientDevToolsNetworkLog> APtr;

	void request_http(const std::string& method, const std::string& url, const std::string& headers);
	void response_http(const bool cached, const long& status_code, const std::string& headers);
	void response_http_body(const std::string& body);
	void error(const std::string& error_message);

	bool is_enabled(void) const;

	std::string dump(void);

	static APtr create(const bool enabled, const bool log_body);

private:
	ApplClientDevToolsNetworkLog(const bool enabled, const bool log_body);

	bool enabled_;
	bool log_body_;

	std::string request_method_;
	std::string request_url_;
	std::string request_headers_;

	bool response_cached_;
	long response_status_code_;
	std::string response_headers_;
	std::string response_body_;

	std::string error_message_;

};



}
}
#endif
