/*! \file APPL_ClientCommand.cxx
 \brief This file contains the implementation for the ClientCommand class
 */
#include <iostream>
#include <sstream>
#include <functional>
#include <algorithm>
#include <math.h>
#include <exception>


#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <component/appl/gon_client/APPL_ClientCommand.hxx>

using namespace std;
using namespace Giritech::Appl;
using namespace boost;


namespace po = boost::program_options;



/*
 * ------------------------------------------------------------------
 * ApplClientDirectConnectionFactory implementation
 * ------------------------------------------------------------------
 */
ApplClientDirectConnectionFactory::ApplClientDirectConnectionFactory(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const Giritech::Communication::TunnelendpointDirectTunnelClient::APtr& direct_tunnelendpoint)
: direct_tunnelendpoint_(direct_tunnelendpoint), checkpoint_handler_(checkpoint_handler), eventhandler_(NULL) {
}

void ApplClientDirectConnectionFactory::create_connection() {
	if(!direct_tunnelendpoint_->is_closed()) {
		direct_tunnelendpoint_->request_connection();
	}
}

void ApplClientDirectConnectionFactory::set_eventhandler(ApplClientDirectConnectionFactoryEventhandler* eventhandler) {
	eventhandler_ = eventhandler;
}

void ApplClientDirectConnectionFactory::reset_eventhandler(void) {
	eventhandler_ = NULL;
}

Giritech::Utility::CheckpointHandler::APtr ApplClientDirectConnectionFactory::get_checkpoint_handler(void) {
	return checkpoint_handler_;
}

Giritech::Utility::Mutex::APtr ApplClientDirectConnectionFactory::get_mutex(void) {
    return direct_tunnelendpoint_->get_mutex();
}

boost::asio::io_service& ApplClientDirectConnectionFactory::get_io_service(void) {
	return direct_tunnelendpoint_->get_io_service();
}

void ApplClientDirectConnectionFactory::connection_ready(const Giritech::Communication::TunnelendpointDirect::APtr& connection) {
	if(eventhandler_) {
		eventhandler_->direct_connection_factory_connection_ready(connection);
	}
}

void ApplClientDirectConnectionFactory::connection_failed(const std::string& errorMessage) {
	if(eventhandler_) {
		eventhandler_->direct_connection_factory_connection_failed(errorMessage);
	}
}

ApplClientDirectConnectionFactory::APtr ApplClientDirectConnectionFactory::create(
		const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
		const Giritech::Communication::TunnelendpointDirectTunnelClient::APtr& direct_tunnelendpoint) {
	return APtr(new ApplClientDirectConnectionFactory(checkpoint_handler, direct_tunnelendpoint));
}





/*
 * ------------------------------------------------------------------
 * ApplClientCommand implementation
 * ------------------------------------------------------------------
 */
ApplClientCommand::ApplClientCommand(void) :
	command_(""),
	command_type_(CommandType_None),
	command_cache_type_(CacheType_On),
	command_skin_type_(SkinType_Browser) {
}

ApplClientCommand::ApplClientCommand(const std::string& command) :
		command_(command),
		command_type_(CommandType_None),
		command_cache_type_(CacheType_On),
		command_skin_type_(SkinType_Browser) {

	try {
		parse_command();
	}
	catch(const std::exception& e) {
		reset();
		command_type_ = CommandType_Invalid;

		stringstream ss;
		ss << "Invalid command: " << e.what() << "\n" << command_;
		command_last_error_ = ss.str();
	}
}

ApplClientCommand::ApplClientCommand(const std::string& command, ApplClientDirectConnectionFactory::APtr direct_connection_factory) :
		command_(command),
		command_type_(CommandType_None),
		command_cache_type_(CacheType_On),
		command_skin_type_(SkinType_Browser) ,
		direct_connection_factory_(direct_connection_factory) {

	try {
		parse_command();
	}
	catch(const std::exception& e) {
		reset();
		command_type_ = CommandType_Invalid;
		stringstream ss;
		ss << "Invalid command: " << e.what() << "\n" << command_;
		command_last_error_ = ss.str();
	}
}

ApplClientDirectConnectionFactory::APtr ApplClientCommand::getDirectConnectionFactory(void) const {
	return direct_connection_factory_;
}

const char* KEYWORD_secure_browser = "secure_browser";
const char* KEYWORD_url = "url";

const char* KEYWORD_skin = "skin";
const char* KEYWORD_skin_value_browser = "browser";
const char* KEYWORD_skin_value_app_fullscreen = "app_fullscreen";
const char* KEYWORD_skin_value_app_popup = "app_popup";

const char* KEYWORD_cache = "cache";
const char* KEYWORD_cache_value_on = "on";
const char* KEYWORD_cache_value_off = "off";
const char* KEYWORD_cache_value_clear_on_launch = "clear_on_launch";

const char* KEYWORD_skin_back_button = "skin_back_button";
const char* KEYWORD_skin_progress_bar = "skin_progress_bar";
const char* KEYWORD_visibility = "visibility";

const char* KEYWORD_value_on = "on";
const char* KEYWORD_value_off = "off";

const char* KEYWORD_orientation_lock = "orientation_lock";
const char* KEYWORD_orientation_lock_value_disabled         = "disabled";
const char* KEYWORD_orientation_lock_value_small_landscape  = "small_landscape";
const char* KEYWORD_orientation_lock_value_small_portrait   = "small_portrait";
const char* KEYWORD_orientation_lock_value_normal_landscape = "normal_landscape";
const char* KEYWORD_orientation_lock_value_normal_portrait  = "normal_portrait";
const char* KEYWORD_orientation_lock_value_large_landscape  = "large_landscape";
const char* KEYWORD_orientation_lock_value_large_portrait   = "large_portrait";
const char* KEYWORD_orientation_lock_value_xlarge_landscape = "xlarge_landscape";
const char* KEYWORD_orientation_lock_value_xlarge_portrait  = "xlarge_portrait";

const char* KEYWORD_handle_scheme_https = "handle_scheme_https";
const char* KEYWORD_handle_scheme_https_value_do_not_handle = "do_not_handle";
const char* KEYWORD_handle_scheme_https_value_handle        = "handle";
const char* KEYWORD_handle_scheme_https_value_block         = "block";



ApplClientCommand::SkinType ApplClientCommand::parse_skin(const std::string& value) {
	if(value == KEYWORD_skin_value_browser) {
		return SkinType_Browser;
	}
	else if(value == KEYWORD_skin_value_app_fullscreen) {
		return SkinType_AppFullscreen;
	}
	else if(value == KEYWORD_skin_value_app_popup) {
		return SkinType_AppPopup;
	}
	return SkinType_Browser;
}

ApplClientCommand::CacheType ApplClientCommand::parse_cache(const std::string& value) {
	if(value == KEYWORD_cache_value_on) {
		return CacheType_On;
	}
	else if(value == KEYWORD_cache_value_off) {
		return CacheType_Off;
	}
	else if(value == KEYWORD_cache_value_clear_on_launch) {
		return CacheType_ClearOnLaunch;
	}
	return CacheType_On;
}

bool ApplClientCommand::parse_on_off(const std::string& value) {
	if(value == KEYWORD_value_on) {
		return true;
	}
	return false;
}

ApplClientCommand::HandleSchemeType ApplClientCommand::parse_shandle_scheme(const std::string& value) {
	if(value == KEYWORD_handle_scheme_https_value_do_not_handle) {
		return HandleSchemeTypeDoNotHandle;
	}
	else if(value == KEYWORD_handle_scheme_https_value_handle) {
		return HandleSchemeTypeHandle;
	}
	else if(value == KEYWORD_handle_scheme_https_value_block) {
		return HandleSchemeTypeBlock;
	}
	return HandleSchemeTypeDoNotHandle;
}

ApplClientCommand::OrientationLockType ApplClientCommand::parse_orientation_lock(const std::string& value) {
	if(value == KEYWORD_orientation_lock_value_disabled) {
		return OrientationLockType_Disabled;
	}
	else if(value == KEYWORD_orientation_lock_value_small_landscape) {
		return OrientationLockType_SmallLandscape;
	}
	else if(value == KEYWORD_orientation_lock_value_small_portrait) {
		return OrientationLockType_SmallPortrait;
	}
	else if(value == KEYWORD_orientation_lock_value_normal_landscape) {
		return OrientationLockType_NormalLandscape;
	}
	else if(value == KEYWORD_orientation_lock_value_normal_portrait) {
		return OrientationLockType_NormalPortrait;
	}
	else if(value == KEYWORD_orientation_lock_value_large_landscape) {
		return OrientationLockType_LargeLandscape;
	}
	else if(value == KEYWORD_orientation_lock_value_large_portrait) {
		return OrientationLockType_LargePortrait;
	}
	else if(value == KEYWORD_orientation_lock_value_xlarge_landscape) {
		return OrientationLockType_XLargeLandscape;
	}
	else if(value == KEYWORD_orientation_lock_value_xlarge_portrait) {
		return OrientationLockType_XLargePortrait;
	}
	return OrientationLockType_Disabled;
}

std::set<ApplClientCommand::OrientationLockType> ApplClientCommand::parse_orientation_locks(const std::string& value) {
	std::vector<string> values;
	boost::char_separator<char> sep(",");

	if (value.find(",") != string::npos) {
		boost::tokenizer<boost::char_separator<char> > tok(value, sep);
		std::copy(tok.begin(), tok.end(), std::back_inserter(values));
    }
	else {
		values.push_back(value);
	}

	std::set<ApplClientCommand::OrientationLockType> result;
	std::vector<std::string>::const_iterator i(values.begin());
	while(i !=values.end()) {
		result.insert(parse_orientation_lock(*i));
		i++;
	}
	if(result.size()==0) {
		result.insert(OrientationLockType_Disabled);
	}
	return result;
}


void ApplClientCommand::parse_command(void) {

	if(command_ == "") {
		reset();
		return;
	}

	string command_normalized(to_lower_copy(command_));
	trim_left(command_normalized);

	if(starts_with(command_normalized, KEYWORD_secure_browser)) {
		string command_args(trim_left_copy(command_));
		erase_head(command_args, strlen(KEYWORD_secure_browser));
		trim(command_args);

		vector<string> command_arg_elements;
		boost::split(command_arg_elements, command_args, boost::is_any_of(" "));

		po::options_description desc("Allowed options");
		desc.add_options()
			(KEYWORD_url, po::value<string>(), "URL")
			(KEYWORD_skin, po::value<string>(), "skin")
			(KEYWORD_cache, po::value<string>(), "cache")
			(KEYWORD_visibility, po::value<string>(), "visibility")
			(KEYWORD_skin_back_button, po::value<string>(), "back button")
			(KEYWORD_skin_progress_bar, po::value<string>(), "progress bar")
			(KEYWORD_orientation_lock, po::value< string >()->implicit_value(KEYWORD_orientation_lock_value_disabled), "orientation_lock")
			(KEYWORD_handle_scheme_https, po::value<string>(), "handle scheme https")
		;

		po::variables_map vm;
		po::store(po::command_line_parser(command_arg_elements).options(desc).run(), vm);
		po::store(po::parse_environment(desc, command_args), vm);
		po::notify(vm);

		reset();
		command_type_ = CommandType_SecureBrowser;
		if (vm.count(KEYWORD_url)) {
			command_url_ = vm[KEYWORD_url].as< string >();
		}
		if (vm.count(KEYWORD_skin)) {
			command_skin_type_ = parse_skin(vm[KEYWORD_skin].as< string >());
		}
		if (vm.count(KEYWORD_cache)) {
			command_cache_type_ = parse_cache(vm[KEYWORD_cache].as< string >());
		}
		if (vm.count(KEYWORD_visibility)) {
			command_visibility_ = parse_on_off(vm[KEYWORD_visibility].as< string >());
		}
		if (vm.count(KEYWORD_skin_back_button)) {
			command_skin_back_button_ = parse_on_off(vm[KEYWORD_skin_back_button].as< string >());
		}
		if (vm.count(KEYWORD_skin_progress_bar)) {
			command_skin_progress_bar_ = parse_on_off(vm[KEYWORD_skin_progress_bar].as< string >());
		}
		if (vm.count(KEYWORD_orientation_lock)) {
			command_orientation_locks_ = parse_orientation_locks(vm[KEYWORD_orientation_lock].as< string >());
		}
		if (vm.count(KEYWORD_handle_scheme_https)) {
			command_handle_scheme_https_ = parse_shandle_scheme(vm[KEYWORD_handle_scheme_https].as< string >());
		}
	}
	else {
		command_type_ = CommandType_Invalid;
		stringstream ss;
		ss << "Invalid command: Command not recognized" << "\n" << command_;
		command_last_error_ = ss.str();
	}
}

void ApplClientCommand::reset(void) {
	command_type_ = CommandType_None;
	command_proxy_host_ = "";
	command_proxy_port_ = 0;
	command_url_ = "";
	command_cache_type_ = CacheType_On;
	command_skin_type_ = SkinType_Browser;
	command_visibility_ = true;
	command_skin_back_button_ = false;
	command_skin_progress_bar_ = true;
	command_handle_scheme_https_ = HandleSchemeTypeDoNotHandle;

	command_orientation_locks_.clear();
	command_orientation_locks_.insert(OrientationLockType_Disabled);
}

void ApplClientCommand::reset_invalid(void) {
	reset();
	command_type_ = CommandType_Invalid;
}

ApplClientCommand::CommandType ApplClientCommand::getCommandType(void) const {
	return command_type_;
}

std::string ApplClientCommand::getLastError(void) const {
	return command_last_error_;
}

std::string ApplClientCommand::getCommandProxyHost(void) const {
	return command_proxy_host_;
}

unsigned long ApplClientCommand::getCommandProxyPort(void) const {
	return command_proxy_port_;
}

std::string ApplClientCommand::getCommandURL(void) const {
	return command_url_;
}

ApplClientCommand::CacheType ApplClientCommand::getCacheType(void) const {
	return command_cache_type_;
}

ApplClientCommand::SkinType ApplClientCommand::getSkinType(void) const {
	return command_skin_type_;
}

bool ApplClientCommand::getVisibility(void) const {
	return command_visibility_;
}

bool ApplClientCommand::getSkinBackButton(void) const {
	return command_skin_back_button_;
}

bool ApplClientCommand::getSkinProgressBar(void) const {
	return command_skin_progress_bar_;
}

ApplClientCommand::HandleSchemeType ApplClientCommand::getHandleSchemeHTTPS(void) const {
	return command_handle_scheme_https_;
}

ApplClientCommand::ChangeOrientationType ApplClientCommand::doChangeOrientation(const unsigned int& x, const unsigned int& y, const unsigned int& ppi, const OrientationType current_orientation) const {

	std::set<OrientationLockType>::const_iterator i(command_orientation_locks_.find(OrientationLockType_Disabled));
	if(i != command_orientation_locks_.end()) {
		return ChangeOrientationLock_NoLock;
	}


	//
	// Catories are taken from android
	//
	double x_d = x;
	double y_d = y;
	double diagonal_inch = sqrt(pow(x_d,2) + pow(y_d,2)) / ppi;

	std::set<OrientationLockType> current_orientations;
	if( diagonal_inch <= 3.4 ) {
		if(current_orientation == OrientationType_Landscape) {
			current_orientations.insert(OrientationLockType_SmallLandscape);
		}
		else {
			current_orientations.insert(OrientationLockType_SmallPortrait);
		}
	}
	if( (3.2 <= diagonal_inch) && (diagonal_inch <= 4.5) ) {
		if(current_orientation == OrientationType_Landscape) {
			current_orientations.insert(OrientationLockType_NormalLandscape);
		}
		else {
			current_orientations.insert(OrientationLockType_NormalPortrait);
		}
	}
	if( (4.2 <= diagonal_inch) && (diagonal_inch <= 7.1) ) {
		if(current_orientation == OrientationType_Landscape) {
			current_orientations.insert(OrientationLockType_LargeLandscape);
		}
		else {
			current_orientations.insert(OrientationLockType_LargePortrait);
		}
	}
	if( (7 <= diagonal_inch)) {
		if(current_orientation == OrientationType_Landscape) {
			current_orientations.insert(OrientationLockType_XLargeLandscape);
		}
		else {
			current_orientations.insert(OrientationLockType_XLargePortrait);
		}
	}

	//
	// If current orientation is found in specified, then lock
	//
	std::set<OrientationLockType>::const_iterator j(current_orientations.begin());
	while(j != current_orientations.end()) {
		std::set<OrientationLockType>::const_iterator h(command_orientation_locks_.find(*j));
		if(h != command_orientation_locks_.end()) {
			return ChangeOrientationLock_Lock;
		}
		j++;
	}

	//
	// If current orientation is found in the inverse of the specified, then rotate and lock
	//
	std::set<OrientationLockType> command_orientation_locks_inverse;
	std::set<OrientationLockType>::const_iterator k(command_orientation_locks_.begin());
	while(k != command_orientation_locks_.end()) {
		switch(*k) {
		case OrientationLockType_SmallLandscape:
			command_orientation_locks_inverse.insert(OrientationLockType_SmallPortrait);
			break;
		case OrientationLockType_NormalLandscape:
			command_orientation_locks_inverse.insert(OrientationLockType_NormalPortrait);
			break;
		case OrientationLockType_LargeLandscape:
			command_orientation_locks_inverse.insert(OrientationLockType_LargePortrait);
			break;
		case OrientationLockType_XLargeLandscape:
			command_orientation_locks_inverse.insert(OrientationLockType_XLargePortrait);
			break;
		case OrientationLockType_SmallPortrait:
			command_orientation_locks_inverse.insert(OrientationLockType_SmallLandscape);
			break;
		case OrientationLockType_NormalPortrait:
			command_orientation_locks_inverse.insert(OrientationLockType_NormalLandscape);
			break;
		case OrientationLockType_LargePortrait:
			command_orientation_locks_inverse.insert(OrientationLockType_LargeLandscape);
			break;
		case OrientationLockType_XLargePortrait:
			command_orientation_locks_inverse.insert(OrientationLockType_XLargeLandscape);
			break;
		}
		k++;
	}

	j = current_orientations.begin();
	while(j != current_orientations.end()) {
		std::set<OrientationLockType>::const_iterator h(command_orientation_locks_inverse.find(*j));
		if(h != command_orientation_locks_inverse.end()) {
			return ChangeOrientationLock_RotateAndLock;
		}
		j++;
	}
	return ChangeOrientationLock_NoLock;
}

