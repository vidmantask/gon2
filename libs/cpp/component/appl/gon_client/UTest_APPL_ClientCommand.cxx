/*! \file UTest_APPL_ClientCommand.cxx
 * \brief This file contains unittest suite for the Appl ClientCommand functionality
 */
#include <string>

#include <boost/test/unit_test.hpp>

#include <component/appl/gon_client/APPL_ClientCommand.hxx>

using namespace std;
using namespace Giritech::Appl;


BOOST_AUTO_TEST_CASE( command_test_none ) {
	ApplClientCommand clientCommand001("");
    BOOST_CHECK( clientCommand001.getCommandType() ==  ApplClientCommand::CommandType_None);
}

BOOST_AUTO_TEST_CASE( command_test_browser ) {
	ApplClientCommand clientCommand001("secure_browser --url=http://test.com");
    BOOST_CHECK( clientCommand001.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand001.getCommandURL() ==  "http://test.com");

    ApplClientCommand clientCommand002("secure_browser --url=http://test.com --skin=browser");
    BOOST_CHECK( clientCommand002.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand002.getCommandURL() ==  "http://test.com");
    BOOST_CHECK( clientCommand002.getSkinType() ==  ApplClientCommand::SkinType_Browser);
    BOOST_CHECK( clientCommand002.getCacheType() ==  ApplClientCommand::CacheType_On);
    BOOST_CHECK( clientCommand002.getVisibility());
    BOOST_CHECK( !clientCommand002.getSkinBackButton());
    BOOST_CHECK( clientCommand002.getSkinProgressBar());
    BOOST_CHECK( clientCommand002.getHandleSchemeHTTPS() ==  ApplClientCommand::HandleSchemeTypeDoNotHandle);

    ApplClientCommand clientCommand003("secure_browser --url=http://test.com --skin=browser --cache=off --visibility=off --skin_back_button=on --handle_scheme_https=block --skin_progress_bar=off");
    BOOST_CHECK( clientCommand003.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand003.getCommandURL() ==  "http://test.com");
    BOOST_CHECK( clientCommand003.getSkinType() ==  ApplClientCommand::SkinType_Browser);
    BOOST_CHECK( clientCommand003.getCacheType() ==  ApplClientCommand::CacheType_Off);
    BOOST_CHECK( !clientCommand003.getVisibility());
    BOOST_CHECK( clientCommand003.getSkinBackButton());
    BOOST_CHECK( !clientCommand003.getSkinProgressBar());
    BOOST_CHECK( clientCommand003.getHandleSchemeHTTPS() ==  ApplClientCommand::HandleSchemeTypeBlock);
}


BOOST_AUTO_TEST_CASE( command_test_browser_orientations ) {
	ApplClientCommand clientCommand001("secure_browser --url=http://test.com --orientation_lock disabled");
    BOOST_CHECK( clientCommand001.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand001.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand001.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone < 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone < 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone >= 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone >= 4
    BOOST_CHECK( clientCommand001.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand001.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand001.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3
    BOOST_CHECK( clientCommand001.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3


	ApplClientCommand clientCommand002("secure_browser --url http://test.com --orientation_lock small_landscape,normal_landscape,large_landscape,xlarge_landscape");
    BOOST_CHECK( clientCommand002.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand002.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand002.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone < 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone < 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone >= 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone >= 4
    BOOST_CHECK( clientCommand002.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPad 1 + 2
    BOOST_CHECK( clientCommand002.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand002.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPad 3
    BOOST_CHECK( clientCommand002.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPad 3


	ApplClientCommand clientCommand003("secure_browser --url http://test.com --orientation_lock small_landscape,normal_landscape");
    BOOST_CHECK( clientCommand003.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand003.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand003.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone < 4
    BOOST_CHECK( clientCommand003.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone < 4
    BOOST_CHECK( clientCommand003.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone >= 4
    BOOST_CHECK( clientCommand003.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone >= 4
    BOOST_CHECK( clientCommand003.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock ); // iPad 1 + 2
    BOOST_CHECK( clientCommand003.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock);  // iPad 1 + 2
    BOOST_CHECK( clientCommand003.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock ); // iPad 3
    BOOST_CHECK( clientCommand003.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock);  // iPad 3

	ApplClientCommand clientCommand004("secure_browser --url http://test.com --orientation_lock normal_landscape");
    BOOST_CHECK( clientCommand004.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand004.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand004.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone < 4
    BOOST_CHECK( clientCommand004.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone < 4
    BOOST_CHECK( clientCommand004.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock ); // iPhone >= 4
    BOOST_CHECK( clientCommand004.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone >= 4
    BOOST_CHECK( clientCommand004.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock ); // iPad 1 + 2
    BOOST_CHECK( clientCommand004.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock);  // iPad 1 + 2
    BOOST_CHECK( clientCommand004.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock ); // iPad 3
    BOOST_CHECK( clientCommand004.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock);  // iPad 3

}


BOOST_AUTO_TEST_CASE( command_test_empty_args ) {
	ApplClientCommand clientCommand001("secure_browser --url http://test.com --skin browser --orientation_lock");
    BOOST_CHECK( clientCommand001.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand001.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand001.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone < 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone < 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone >= 4
    BOOST_CHECK( clientCommand001.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPhone >= 4
    BOOST_CHECK( clientCommand001.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand001.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand001.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3
    BOOST_CHECK( clientCommand001.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3


    cout << "TEST START" << endl;

	ApplClientCommand clientCommand002("secure_browser --url http://test.com --skin browser --orientation_lock ,small_landscape,,normal_landscape,");
    BOOST_CHECK( clientCommand002.getCommandType() ==  ApplClientCommand::CommandType_SecureBrowser);
    BOOST_CHECK( clientCommand002.getCommandURL() ==  "http://test.com");

    BOOST_CHECK( clientCommand002.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock); // iPhone < 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 320, 480, 163, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone < 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_Lock); // iPhone >= 4
    BOOST_CHECK( clientCommand002.doChangeOrientation( 640, 960, 326, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_RotateAndLock); // iPhone >= 4
    BOOST_CHECK( clientCommand002.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand002.doChangeOrientation(1024, 768, 132, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 1 + 2
    BOOST_CHECK( clientCommand002.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Landscape) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3
    BOOST_CHECK( clientCommand002.doChangeOrientation(2028,1536, 264, ApplClientCommand::OrientationType_Portraite) == ApplClientCommand::ChangeOrientationLock_NoLock); // iPad 3

}


BOOST_AUTO_TEST_CASE( command_test_error ) {
	ApplClientCommand clientCommand001("secure_bruser --url http://test.com --skin browser --orientation_lock");
    BOOST_CHECK( clientCommand001.getCommandType() == ApplClientCommand::CommandType_Invalid);
    cout << clientCommand001.getLastError() << endl;

	ApplClientCommand clientCommand002("secure_browser --urrl http://test.com");
    BOOST_CHECK( clientCommand002.getCommandType() ==  ApplClientCommand::CommandType_Invalid);
    cout << clientCommand002.getLastError() << endl;

}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
    return 0;
}
