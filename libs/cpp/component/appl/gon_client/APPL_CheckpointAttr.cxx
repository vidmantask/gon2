/*! \file APPL_CheckpointAttr.cxx
 \brief This file contains the global singelton for the module attribute appl
 */

#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>

using namespace Giritech;
using namespace Giritech::Utility;

static CheckpointAttr_Module::APtr componentApplClient_;
static CheckpointAttr_Module::APtr componentiDevice_;

CheckpointAttr_Module::APtr Giritech::Appl::Attr_ApplClient(void) {
    if (!componentApplClient_) {
    	componentApplClient_ = CheckpointAttr_Module::create("ApplClient");
    }
    return componentApplClient_;
}

CheckpointAttr_Module::APtr Giritech::Appl::Attr_iDevice(void) {
    if (!componentiDevice_) {
    	componentiDevice_ = CheckpointAttr_Module::create("iDevice");
    }
    return componentiDevice_;
}
