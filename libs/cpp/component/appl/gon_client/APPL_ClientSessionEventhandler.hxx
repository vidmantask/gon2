/*! \file APPL_ClientSessionEventhandler.hxx
 *  \brief This file contains abstract interface AppClientSession
 */
#ifndef APPL_ClientSessionEventhandler_HXX
#define APPL_ClientSessionEventhandler_HXX

#include <string>

namespace Giritech {
namespace Appl {

/*! \brief This class define the abstract eventhandler interface for a ApplClientSession.
 */
class ApplClientSessionEventhandler : public boost::noncopyable {
public:
    virtual void app_client_session_connected(void) = 0;
    virtual void app_client_session_closed(void) = 0;
};

}
}

#endif
