/*! \file APPL_ClientSessionTunnelendpoint.hxx
 \brief This file contains the client functionctionlaity
 */
#ifndef APPL_ClientSessionTunnelendpoint_HXX
#define APPL_ClientSessionTunnelendpoint_HXX

#include <string>
#include <vector>

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>
#include <lib/rpc/RPC_Spec.hxx>

#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>
#include <component/appl/gon_client/APPL_KeyStore.hxx>

#include <component/communication/COM_CheckpointAttr.hxx>
#include <component/communication/COM_Tunnelendpoint.hxx>
#include <component/communication/COM_TunnelendpointEventhandler.hxx>
#include <component/communication/COM_Message.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_TunnelendpointTCP.hxx>
#include <component/communication/COM_TunnelendpointTCPEventhandler.hxx>
#include <component/communication/COM_TunnelendpointDirect.hxx>
#include <component/communication/COM_TunnelendpointDirectEventhandler.hxx>
#include <component/communication/COM_TunnelendpointDirectHttpProxy.hxx>
#include <component/communication/COM_TunnelendpointCheckpointHandler.hxx>



namespace Giritech {
namespace Appl {


class ApplClientSessionTunnelendpoint : public Communication::TunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpoint> APtr;

    enum State {
        State_Initializing = 0,
        State_Connected,
        State_Closing,
        State_Closed,
        State_Unknown
    };

    enum UserSignal {
    	UserSignal_VERSION_ERROR = 0,
       	UserSignal_VERSION_ERROR_REMOTE,
       	UserSignal_DISPATCH_ERROR
    };

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpoint(void);

    /*! \brief Close
     *
     * Close session tunnelendpoint, should end end up with the state State_Closed
     */
    virtual void close(void);
    bool is_closed(void);

    /*! \brief Return state
     */
    State get_state(void) const;
    Communication::Tunnelendpoint::APtr get_com_tunnelendpoint(void);

	virtual void appl_client_session_tunnelendpoint_connected(void) = 0;

	virtual void set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler);
	virtual void reset_appl_client_eventhandler(void);

	virtual bool close_janitor_is_closed(void);

    /*! Implementation TunnelendpointEventhandler
     */
    void tunnelendpoint_eh_connected(void);
    void tunnelendpoint_eh_recieve(const Communication::MessagePayload::APtr& message);

    virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);

    void rpc_remote(const RPC::RPCSpecCall::APtr& rpc_call);
    void rpc_remote(const std::string& method);
    void rpc_remote(const std::string& method, RPC::RPCSpecValueDict::APtr& args);
    void rpc_remote(const std::string& method, RPC::RPCSpecValueList::APtr& args);


protected:

    /*! \brief Constructor
     */
    ApplClientSessionTunnelendpoint(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    void rpc_dispatch_remote_check_version_failed(const std::string& error_message);

	void close_janitor_start(void);
	void close_janitor(void);

    Communication::AsyncService::APtr async_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    State state_;
    Communication::TunnelendpointReliableCryptedTunnel::APtr com_tunnelendpoint_;
    ApplClientEventhandler* appl_client_eventhandler_;

    boost::asio::deadline_timer close_janitor_timer_;
};



class ApplClientSessionTunnelendpointAuthMobile : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointAuthMobile> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointAuthMobile(void);


    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

	void appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key);


protected:
	ApplClientSessionTunnelendpointAuthMobile(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_get_serial(void);
	void rpc_dispatch_get_challenge(const std::string& challenge);

private:
    std::string auth_serial_;
    std::string auth_private_key_;
    std::string auth_public_key_;
};



class ApplClientSessionTunnelendpointAuthTokenStub : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointAuthTokenStub> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointAuthTokenStub(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

protected:
	ApplClientSessionTunnelendpointAuthTokenStub(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_get_serial(void);
	void rpc_dispatch_get_challenge(const std::string& challenge);
};


class ApplClientSessionTunnelendpointAuthDME : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointAuthDME> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointAuthDME(void);

    void app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time);
    void app_client_dme_response_get_dme_values_none(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

protected:

    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointAuthDME(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void get_dme_values(void);
};



class ApplClientSessionTunnelendpointAuth : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointAuth> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointAuth(void);

	virtual void set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler);
	virtual void reset_appl_client_eventhandler(void);

	void appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key);

	void app_client_dme_response_get_dme_values(
			const std::string& username,
			const std::string& password,
			const std::string& terminal_id,
			const std::string& device_signature,
			const std::string& device_time);

	void app_client_dme_response_get_dme_values_none(void);


    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

protected:

    /*! \brief Constructor
     */
    ApplClientSessionTunnelendpointAuth(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_remote_call_show_message(const std::string& message);
	void rpc_dispatch_remote_authorization_failed(const std::string& message);

	void remote_auth_child_created_on_client(const std::string& module_name, const unsigned long& id);

    ApplClientSessionTunnelendpointAuthMobile::APtr plugin_mobile_;
    ApplClientSessionTunnelendpointAuthTokenStub::APtr plugin_soft_token_;
    ApplClientSessionTunnelendpointAuthTokenStub::APtr plugin_micro_smart_;
    ApplClientSessionTunnelendpointAuthTokenStub::APtr plugin_hagiwara_;
    ApplClientSessionTunnelendpointAuthTokenStub::APtr plugin_endpoint_token_;
    ApplClientSessionTunnelendpointAuthTokenStub::APtr plugin_computer_token_;
    ApplClientSessionTunnelendpointAuthDME::APtr plugin_dme_;

private:
    std::string auth_serial_;
    std::string auth_private_key_;
    std::string auth_public_key_;
};




class ApplClientSessionTunnelendpointEndpoint : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointEndpoint> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointEndpoint(void);


    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

	void app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value);
	void app_client_endpoint_response_endpoint_info_all_done(void);

	/*
	 * Called before internal launch
	 */
	bool is_launch_internal_endpoint_enrollment_ready(void);

	void app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title);
	void app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key);
	void app_client_endpoint_token_status(void);
	void app_client_endpoint_get_enrollment_status(void);

protected:

    /*! \brief Constructor
     */
    ApplClientSessionTunnelendpointEndpoint(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_launch_enrollment(const bool licensed);
    void rpc_dispatch_deploy_token_cb(const RPC::RPCSpecValueDict::APtr& token);
    void rpc_dispatch_enroll_reply(const bool rc, const std::string& msg);
    void rpc_dispatch_get_token_status_reply(const std::vector<RPC::RPCSpecValue::APtr>& tokens, const std::string& errormsg);

    bool endpoint_enrollment_running_;
    bool get_token_status_is_get_enrollment_status_;
    RPC::RPCSpecValueDict::APtr current_token_;
};



class ApplClientSessionTunnelendpointUser : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointUser> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointUser(void);

    void app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword);
    void app_client_user_response_login_cancelled(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

	void appl_client_session_tunnelendpoint_connected(void);

protected:

    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointUser(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

    virtual bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_get_client_encoding(void);
	void rpc_dispatch_show_login_prompt(const std::string& msg, const bool change_password_enabled);

	ApplClientKeyStore::APtr key_store_;
};






class ApplClientSessionTunnelendpointCPMEndpointManager : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointCPMEndpointManager> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointCPMEndpointManager(void);

	void set_connection_info(const std::string& servers);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

	void appl_client_session_tunnelendpoint_connected(void);

protected:

    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointCPMEndpointManager(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

	bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_remote_query_installed_meta(void);
	void rpc_dispatch_remote_query_servers_checksum(void);
	void rpc_dispatch_remote_set_offline_credential_timeout_min(const unsigned int timeout_min);
	void rpc_dispatch_set_background_settings(const unsigned int close_when_entering_background);

	ApplClientKeyStore::APtr key_store_;

	bool close_when_entering_background_;

	std::string servers_;
};


class ApplClientSessionTunnelendpointCPM : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointCPM> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointCPM(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

	virtual void set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler);
	virtual void reset_appl_client_eventhandler(void);

	void appl_client_session_tunnelendpoint_connected(void);

	void set_connection_info(const std::string& servers);

	bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
	void rpc_dispatch_remote_analyze_start_connect_update(const unsigned int& do_update_servers, const std::string& servers);


protected:

    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointCPM(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientKeyStore::APtr& key_store);

	ApplClientSessionTunnelendpointCPMEndpointManager::APtr cpm_endpoint_manager_;
	ApplClientKeyStore::APtr key_store_;
};




class ApplClientSessionTunnelendpointDialogInformer : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointDialogInformer> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointDialogInformer(void);

    void app_client_dialog_response_inform_on_first_access(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

protected:

    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointDialogInformer(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_remote_show_license_info(const std::string& header, const std::string& message, const long valid);
    void rpc_dispatch_remote_show_menu_updated(void);
    void rpc_dispatch_remote_inform_on_first_access(const std::string& message, const long close_on_cancel);

};

class ApplClientSessionTunnelendpointDialog : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointDialog> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointDialog(void);

	virtual void set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler);
	virtual void reset_appl_client_eventhandler(void);

    void app_client_dialog_response_inform_on_first_access(void);
    void app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch);
    void app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointDialog(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_server_cb_show_menu(const std::vector<RPC::RPCSpecValue::APtr>& menu, const RPC::RPCSpecValueDict::APtr& tag_properties);
    void rpc_dispatch_remote_launch_cancled_missing_package(const std::string& package_id_missing);
    void rpc_dispatch_remote_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data);
    void rpc_dispatch_remote_image_not_found(const std::string& request_id, const std::string& image_id);

    bool has_tag(const std::vector<std::string>& tags, const std::string& tag_to_find);

	ApplClientSessionTunnelendpointDialogInformer::APtr dialog_informer_;
};


/*! \brief This class define the abstract eventhandler interface for a ApplClient.
 */
class ApplClientSessionTunnelendpointTrafficChildEventhandler : public boost::noncopyable {
public:
	virtual void child_closed_and_done(long childid) = 0;
	virtual void child_error(long childid) = 0;
};


class ApplClientSessionTunnelendpointTrafficChild : public ApplClientSessionTunnelendpoint {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTrafficChild> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTrafficChild(void);

    /*
     * Getters
     */
    std::string get_launch_id(void);
    ApplClientEventhandler::State get_state(std::string& state_info);

    /*! \brief Implementation of abstract methods for ApplClientSessionTunnelendpointTrafficChild
     */
    virtual bool is_running(void) = 0;

    virtual void app_client_traffic_launch_command_running(void);
    virtual void app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message);

    void notify_state_change(void);

    /*! Close
     */
	void close(void);
	void close_with_events(void);
	virtual void close_start() = 0;

protected:
    /*! \brief Constructor
     */
    ApplClientSessionTunnelendpointTrafficChild(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_, const long child_id, const std::string& launch_id);

    void set_state(const ApplClientEventhandler::State state, const std::string& state_info);

    ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_;
    long child_id_;
    std::string launch_id_;
    ApplClientEventhandler::State state_;
    std::string state_info_;
};


class ApplClientSessionTunnelendpointTrafficChildPortforward : public ApplClientSessionTunnelendpointTrafficChild, public Communication::TunnelendpointTCPTunnelClientEventhandler {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTrafficChildPortforward> APtr;


    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTrafficChildPortforward(void);

    /*! \brief Implementation of abstract methods for ApplClientSessionTunnelendpointTrafficChild
     */
    bool is_running(void);
	void close_start(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_, const long child_id, const std::string& launch_id);

	void appl_client_session_tunnelendpoint_connected(void);

	/*
	 *
	 */
    void app_client_traffic_launch_command_running(void);
    void app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message);

	/*
	 * Implements TunnelendpointTCPTunnelClientEventhandler
	 */
    void tunnelendpoint_tcp_eh_ready(const unsigned long id);
    void tunnelendpoint_tcp_eh_closed(const unsigned long id);
    void tunnelendpoint_tcp_eh_recieve(const unsigned long id, const Utility::DataBufferManaged::APtr& message);
    bool tunnelendpoint_tcp_eh_connected(const unsigned long id, const Communication::TunnelendpointTCP::APtr& connection);
    void tunnelendpoint_tcp_eh_acceptor_error(const unsigned long id, const std::string& message);
    bool tunnelendpoint_tcp_eh_accept_start_continue(const unsigned long id);

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointTrafficChildPortforward(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_, const long child_id, const std::string& launch_id);


    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_traffic_remote_call_launch_with_tcp_options(
    		const std::string& launch_command,
    		const bool close_with_process,
    		const bool kill_on_close,
    		const RPC::RPCSpecValueDict::APtr& client_portforwards,
    		const std::string& param_file_name,
    		const int param_file_lifetime,
    		const std::string& param_file_template);

    void rpc_dispatch_traffic_remote_call_closed(void);

    std::string build_state_info(void);
    std::string parameter_subst(const std::string& launch_command);

    void close_helper_from_client(void);
    void close_helper_from_server(void);
    void close_helper(void);

    std::map<long, Communication::TunnelendpointTCPTunnelClient::APtr> portforwards_;
    std::set<unsigned long> children_waiting_for_ready_;
    bool close_from_server_;

    enum CloseState {CloseState_Init=0, CloseState_WaitingForCommandToTerminate, CloseState_CommandTerminated, CloseState_WaitingForPortforwards, CloseState_AllPortforwarsDone};
    CloseState close_state_;

    enum CommandState {StateCommandLaunching = 0, StateCommandRunning, StateCommandNotRunning, StateCommandAbsent};
    CommandState command_state_;
    std::string launch_command_;

    std::string param_file_name_;
	int param_file_lifetime_;
	std::string param_file_template_;
	std::string param_file_url_;

};



class ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy : public ApplClientSessionTunnelendpointTrafficChild, public Communication::TunnelendpointDirectTunnelClientEventhandler {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy> APtr;


    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy(void);


    /*! \brief Create instance
     */
    static APtr create(
    		const Communication::AsyncService::APtr& async_service,
    		const Utility::CheckpointHandler::APtr& checkpoint_handler,
    		ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_,
    		const long child_id, const std::string& launch_id);


    void appl_client_session_tunnelendpoint_connected(void);


    /*! \brief Implementation of abstract methods for ApplClientSessionTunnelendpointTrafficChild
     */
    void app_client_traffic_launch_command_running(void);
    void app_client_traffic_launch_command_terminated(const bool& error, const std::string& error_message);
    bool is_running(void);
	void close_start(void);

	/*
	 * Implements TunnelendpointDirectTunnelClientEventhandler
	 */
    virtual void tunnelendpoint_direct_eh_connection_ready(const Communication::TunnelendpointDirect::APtr& connection);
    virtual void tunnelendpoint_direct_eh_connection_failed(const std::string& errorMessage);
    virtual void tunnelendpoint_direct_eh_ready(void);
    virtual void tunnelendpoint_direct_eh_closed(void);
    virtual void tunnelendpoint_direct_eh_recieve(const Utility::DataBufferManaged::APtr& message);


protected:
    /*! \brief Constructor
     */
    ApplClientSessionTunnelendpointTrafficChildDirectHttpProxy(
    		const Communication::AsyncService::APtr& async_service,
    		const Utility::CheckpointHandler::APtr& checkpoint_handler,
    		ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_,
    		const long child_id,
    		const std::string& launch_id);

    bool tcp_option_enable_log_to_server(const RPC::RPCSpecValueList::APtr& tcp_options);


    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_traffic_remote_call_launch_with_tcp_options(const std::string& launch_command, const RPC::RPCSpecValueList::APtr& tcp_options);

    void rpc_dispatch_traffic_remote_call_closed(void);

    std::string build_state_info(void);
    std::string parameter_subst(const std::string& launch_command);

    void close_helper_from_client(void);
    void close_helper_from_server(void);
    void close_helper(void);

    Communication::TunnelendpointDirectTunnelClient::APtr direct_tunnelendpoint_;
    Utility::CheckpointHandler::APtr checkpoint_handler_tunnelendpoint_;
	ApplClientDirectConnectionFactory::APtr directConnectionFactory_;

    bool close_from_server_;

    enum CloseState {CloseState_Init=0, CloseState_WaitingForCommandToTerminate, CloseState_CommandTerminated};
    CloseState close_state_;

    enum CommandState {StateCommandLaunching = 0, StateCommandRunning, StateCommandNotRunning, StateCommandAbsent};
    CommandState command_state_;
    std::string launch_command_;
};



class ApplClientSessionTunnelendpointTraffic : public ApplClientSessionTunnelendpoint, public ApplClientSessionTunnelendpointTrafficChildEventhandler {
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTraffic> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTraffic(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint);

	void appl_client_session_tunnelendpoint_connected(void);

    /*! Close
     */
	void close(void);
	void close_start(const std::string& launch_id);

	/*
	 * Implementation of ApplClientSessionTunnelendpointTrafficChildEventhandler
	 */
	virtual void child_closed_and_done(long childid);
	virtual void child_error(long childid);


    bool app_client_dialog_menu_item_is_launched(const std::string& launch_id);
    ApplClientEventhandler::State app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info);
    void app_client_traffic_launch_command_running(const std::string& launch_id);
    void app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message);

	bool close_janitor_is_closed(void);

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointTraffic(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint);

    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_traffic_launch_child_with_launch_id(const long child_id, const long launch_type, const std::string& launch_id);
    ApplClientSessionTunnelendpointTrafficChild::APtr get_child_by_launch_id(const std::string& launch_id);

    void rpc_dispatch_remote_call_show_message(const std::string& title, const std::string& text);

    void child_inform_not_launched_start(const std::string launch_id);
    void child_inform_not_launched(const boost::system::error_code& error, const std::string launch_id);
	void child_close(const boost::system::error_code& error, const long child_id);


    ApplClientSessionTunnelendpointEndpoint::APtr session_tunnelendpoint_endpoint_;
    std::map<long, ApplClientSessionTunnelendpointTrafficChild::APtr> children_;
    std::vector<ApplClientSessionTunnelendpointTrafficChild::APtr> dead_children_;

    long child_inform_pending_count_;
    boost::asio::deadline_timer child_inform_timer_;
};



class ApplClientSessionTunnelendpointTrafficChildInternal : public ApplClientSessionTunnelendpointTrafficChild{
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTrafficChildInternal> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTrafficChildInternal(void);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_, const long child_id, const std::string& launch_id, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint);

	void appl_client_session_tunnelendpoint_connected(void);

    bool is_running(void);
	void close_start(void);

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointTrafficChildInternal(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, ApplClientSessionTunnelendpointTrafficChildEventhandler* child_eventhandler_, const long child_id, const std::string& launch_id, const ApplClientSessionTunnelendpointEndpoint::APtr& session_tunnelendpoint_endpoint);

    bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_remote_launch_endpoint_enrollment(void);

    ApplClientSessionTunnelendpointEndpoint::APtr session_tunnelendpoint_endpoint_;
};



class ApplClientSessionTunnelendpointTagClientOk : public ApplClientSessionTunnelendpoint{
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTagClientOk> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTagClientOk(void);

    /*! \brief Create instance
     */
    static APtr create(
			const Communication::AsyncService::APtr& async_service,
			const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

	void rpc_dispatch_remote_get_platform_response(const std::string& platform) ;

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointTagClientOk(
			const Communication::AsyncService::APtr& async_service,
			const Utility::CheckpointHandler::APtr& checkpoint_handler);

	bool rpc_dispatch(const std::string& method, const RPC::RPCSpecValueDict::APtr& args);
    void rpc_dispatch_remote_get_platform(void);
};



class ApplClientSessionTunnelendpointTag : public ApplClientSessionTunnelendpoint{
public:
    typedef boost::shared_ptr<ApplClientSessionTunnelendpointTag> APtr;

    /*! \brief Destructor
     */
    ~ApplClientSessionTunnelendpointTag(void);

    /*! \brief Create instance
     */
    static APtr create(
			const Communication::AsyncService::APtr& async_service,
			const Utility::CheckpointHandler::APtr& checkpoint_handler);

	void appl_client_session_tunnelendpoint_connected(void);

	void set_appl_client_eventhandler(ApplClientEventhandler* appl_client_eventhandler);
	void reset_appl_client_eventhandler(void);

	void rpc_dispatch_remote_get_platform_response(const std::string& platform) ;

protected:
    /*! \brief Constructor
     */
	ApplClientSessionTunnelendpointTag(
			const Communication::AsyncService::APtr& async_service,
			const Utility::CheckpointHandler::APtr& checkpoint_handler);

	ApplClientSessionTunnelendpointTagClientOk::APtr tag_plugin_client_ok_;
};


}
}
#endif
