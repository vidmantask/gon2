/*! \file APPL_KeyStoreEventhandler.hxx
 *  \brief This file contains abstract interface to KeyStore eventhandler
 */
#ifndef APPL_KeyStoreEventhandler_HXX
#define APPL_KeyStoreEventhandler_HXX

#include <string>

namespace Giritech {
namespace Appl {

/*! \brief This class define the abstract eventhandler interface for a ApplClientSession.
 */
class ApplClientKeyStoreEventhandler : public boost::noncopyable {
public:
    virtual void app_client_key_store_reset(void) = 0;

    virtual void app_client_key_store_session_key_pair_set(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) = 0;
};

}
}

#endif
