/*! \file APPL_KeyStore.hxx
 \brief This file contains the class for handling key store
 */
#ifndef APPL_KeyStore_HXX
#define APPL_KeyStore_HXX


#include <boost/thread.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/cryptfacility/CF_KeyStore.hxx>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <component/appl/gon_client/APPL_KeyStoreEventhandler.hxx>

namespace Giritech {
namespace Appl {

class ApplClientKeyStore : public CryptFacility::KeyStore {
public:
    typedef boost::shared_ptr<ApplClientKeyStore> APtr;

    virtual ~ApplClientKeyStore(void);

    void setEventhandler(ApplClientKeyStoreEventhandler* eventhandler);
    void resetEventhandler(void);

    virtual void resetSessionKeypair(void);

    static APtr create(boost::asio::io_service& io, const CryptFacility::CryptFactory::APtr& cryptFactory);

private:
    ApplClientKeyStore(boost::asio::io_service& io, const CryptFacility::CryptFactory::APtr& cryptFactory);

    void generateSessionKeypairBackgroundStart(void);
    void generateSessionKeypairBackground(void);

    ApplClientKeyStoreEventhandler* eventhandler_;

    boost::thread background_thread_;
    bool background_thread_running_;
};

}
}
#endif
