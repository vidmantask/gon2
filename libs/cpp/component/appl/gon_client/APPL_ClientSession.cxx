/*! \file APPL_ClientSession.cxx
 \brief This file contains the implementation for the PPL_ClientSession class
 */
#include <sstream>
#include <functional>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <component/appl/gon_client/APPL_ClientSession.hxx>
#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>

#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>

#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Session.hxx>


using namespace std;
using namespace Giritech::Appl;
using namespace Giritech::Utility;
using namespace Giritech::Communication;



/*
 * ------------------------------------------------------------------
 * ApplClientSession implementation
 * ------------------------------------------------------------------
 */
ApplClientSession::ApplClientSession(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const Communication::Session::APtr& session, ApplClientEventhandler* appl_client_eventhandler, ApplClientSessionEventhandler* session_eventhandler, const ApplClientKeyStore::APtr& key_store)
	: async_service_(async_service),
	  checkpoint_handler_(checkpoint_handler),
	  state_(State_Initializing),
	  com_session_(session),
	  appl_client_eventhandler_(appl_client_eventhandler),
	  session_eventhandler_(session_eventhandler),
	  close_janitor_timer_(async_service->get_io_service()),
	  key_store_(key_store) {

	com_session_->set_eventhandler_ready(this);

    session_tunnelendpoint_auth_ = ApplClientSessionTunnelendpointAuth::create(async_service_, checkpoint_handler_);
    session_tunnelendpoint_endpoint_ = ApplClientSessionTunnelendpointEndpoint::create(async_service_, checkpoint_handler_);
    session_tunnelendpoint_user_ = ApplClientSessionTunnelendpointUser::create(async_service_, checkpoint_handler_, key_store_);
    session_tunnelendpoint_cpm_ = ApplClientSessionTunnelendpointCPM::create(async_service_, checkpoint_handler_, key_store_);
    session_tunnelendpoint_dialog_ = ApplClientSessionTunnelendpointDialog::create(async_service_, checkpoint_handler_);
    session_tunnelendpoint_tag_ = ApplClientSessionTunnelendpointTag::create(async_service_, checkpoint_handler_);
    session_tunnelendpoint_traffic_ = ApplClientSessionTunnelendpointTraffic::create(async_service_, checkpoint_handler_, session_tunnelendpoint_endpoint_);

    com_session_->add_tunnelendpoint(1, session_tunnelendpoint_auth_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(2, session_tunnelendpoint_dialog_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(3, session_tunnelendpoint_traffic_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(4, session_tunnelendpoint_user_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(5, session_tunnelendpoint_cpm_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(6, session_tunnelendpoint_tag_->get_com_tunnelendpoint());
    com_session_->add_tunnelendpoint(7, session_tunnelendpoint_endpoint_->get_com_tunnelendpoint());

	session_tunnelendpoint_auth_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_user_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_endpoint_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_cpm_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_dialog_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_tag_->set_appl_client_eventhandler(appl_client_eventhandler_);
	session_tunnelendpoint_traffic_->set_appl_client_eventhandler(appl_client_eventhandler_);
}

ApplClientSession::~ApplClientSession(void) {
    state_ = State_Unknown;
}

ApplClientSession::State ApplClientSession::get_state(void) const {
	return state_;
}

std::string ApplClientSession::get_session_id(void) {
	return com_session_->get_remote_unique_session_id();
}

RawTunnelendpointTCP::APtr ApplClientSession::get_raw_tunnelendpoint(void) {
	return com_session_->get_raw_tunnelendpoint();
}

void ApplClientSession::reset_appl_client_eventhandler(void) {
	appl_client_eventhandler_ = NULL;
	session_tunnelendpoint_auth_->reset_appl_client_eventhandler();
	session_tunnelendpoint_user_->reset_appl_client_eventhandler();
	session_tunnelendpoint_endpoint_->reset_appl_client_eventhandler();
	session_tunnelendpoint_cpm_->reset_appl_client_eventhandler();
	session_tunnelendpoint_tag_->reset_appl_client_eventhandler();
	session_tunnelendpoint_traffic_->reset_appl_client_eventhandler();
}

void ApplClientSession::appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key) {
	if (session_tunnelendpoint_auth_.get() != NULL) {
		session_tunnelendpoint_auth_->appl_client_auth_set_info(serial, private_key, public_key);
	}
}

ApplClientSession::APtr ApplClientSession::create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const Communication::Session::APtr& session, ApplClientEventhandler* appl_client_eventhandler, ApplClientSessionEventhandler* session_eventhandler, const ApplClientKeyStore::APtr& key_store) {
	return APtr(new ApplClientSession(async_service, checkpoint_handler, session, appl_client_eventhandler, session_eventhandler, key_store));
}

void ApplClientSession::close(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        return;
    }
    state_ =  State_Closeing;
    reset_appl_client_eventhandler();

    com_session_->close(false);
	session_tunnelendpoint_auth_->close();
	session_tunnelendpoint_user_->close();
	session_tunnelendpoint_endpoint_->close();
	session_tunnelendpoint_cpm_->close();
	session_tunnelendpoint_tag_->close();
	session_tunnelendpoint_traffic_->close();
	close_janitor_start();
}

bool ApplClientSession::is_closed(void) {
	return state_ == State_Closed;
}

bool ApplClientSession::is_connected(void) {
	if(is_closed()) {
		return false;
	}
	return com_session_->is_connected();
}

void ApplClientSession::close_janitor_start(void) {
    switch (state_) {
    case State_Closeing:
    	break;
    default:
        return;
    }
    close_janitor_timer_.expires_from_now(boost::posix_time::milliseconds(200));
    close_janitor_timer_.async_wait(boost::bind(&ApplClientSession::close_janitor, this));
}

void ApplClientSession::close_janitor(void) {
    Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor", Attr_ApplClient(), CpAttr_debug());
    switch (state_) {
    case State_Closeing:
    	break;
    default:
        return;
    }

    if(!com_session_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.com_session", Attr_ApplClient(), CpAttr_debug());
    	close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_auth_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_auth_", Attr_ApplClient(), CpAttr_debug());
    	close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_user_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_user_", Attr_ApplClient(), CpAttr_debug());
    	close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_endpoint_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_endpoint_", Attr_ApplClient(), CpAttr_debug());
    	close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_cpm_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_cpm_", Attr_ApplClient(), CpAttr_debug());
    	close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_tag_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_tag_", Attr_ApplClient(), CpAttr_debug());
        close_janitor_start();
    	return;
    }
    if(!session_tunnelendpoint_traffic_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::close_janitor.session_tunnelendpoint_traffic_", Attr_ApplClient(), CpAttr_debug());
        close_janitor_start();
    	return;
    }

    state_ = State_Closed;
    if(session_eventhandler_ != NULL) {
    	session_eventhandler_->app_client_session_closed();
	}
}

/*
 *  Implementation of SessionEventhandlerReady events
 */
void ApplClientSession::session_state_key_exchange(void) {
	// Ignore event
}

void ApplClientSession::session_state_ready(void) {
    switch (state_) {
    case State_Initializing:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "ApplClientSession::session_state_ready.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    state_ = State_Connected;
    if(session_eventhandler_ != NULL) {
    	session_eventhandler_->app_client_session_connected();
	}
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_connected();
	}
    com_session_->read_start_state_ready();
}

void ApplClientSession::session_state_closed(void) {
    switch (state_) {
    case State_Initializing:
    case State_Connected:
    	break;
    default:
        return;
    }
    close();
}

bool ApplClientSession::session_read_continue_state_ready(void) {
	return true;
}

void ApplClientSession::session_user_signal(const unsigned long signal_id, const MessagePayload::APtr& message) {
    if(signal_id == ApplClientSessionTunnelendpoint::UserSignal_VERSION_ERROR || signal_id == ApplClientSessionTunnelendpoint::UserSignal_VERSION_ERROR_REMOTE) {
    	if(appl_client_eventhandler_ != NULL) {
    		appl_client_eventhandler_->app_client_version_error(session_tunnelendpoint_cpm_->get_com_tunnelendpoint()->get_version_remote()->get_version_string());
    	}
    	close();
	}
}


void ApplClientSession::app_client_dialog_response_inform_on_first_access(void) {
	session_tunnelendpoint_dialog_->app_client_dialog_response_inform_on_first_access();
}

void ApplClientSession::app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch) {
	session_tunnelendpoint_dialog_->app_client_dialog_menu_item_selected(launch_id, auto_launch);
}
void ApplClientSession::app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format) {
	session_tunnelendpoint_dialog_->app_client_dialog_get_images(request_id, image_ids, image_style, image_size_x, image_size_y, image_format);
}

bool ApplClientSession::app_client_dialog_menu_item_is_launched(const std::string& launch_id) {
	return session_tunnelendpoint_traffic_->app_client_dialog_menu_item_is_launched(launch_id);
}

void ApplClientSession::app_client_traffic_launch_command_running(const std::string& launch_id) {
	session_tunnelendpoint_traffic_->app_client_traffic_launch_command_running(launch_id);
}

void ApplClientSession::app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message) {
	session_tunnelendpoint_traffic_->app_client_traffic_launch_command_terminated(launch_id, error, error_message);
}

void ApplClientSession::app_client_dialog_menu_item_close(const std::string& launch_id) {
	session_tunnelendpoint_traffic_->close_start(launch_id);
}

ApplClientEventhandler::State ApplClientSession::app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info) {
	return session_tunnelendpoint_traffic_->app_client_dialog_menu_item_get_status_info(launch_id, state_info);
}

void ApplClientSession::app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword) {
	session_tunnelendpoint_user_->app_client_user_response_login(login, password, requestedpassword);
}

void ApplClientSession::app_client_user_response_login_cancelled(void) {
	session_tunnelendpoint_user_->app_client_user_response_login_cancelled();
}

void ApplClientSession::app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_response_endpoint_info(info_id, info_value);
}

void ApplClientSession::app_client_endpoint_response_endpoint_info_all_done(void) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_response_endpoint_info_all_done();
}

void ApplClientSession::app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_response_token(token_type, token_internal_type, token_serial, token_title);
}

void ApplClientSession::app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_enroll_token(token_serial, token_public_key);
}

void ApplClientSession::app_client_endpoint_token_status(void) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_token_status();
}    ApplClientSessionTunnelendpointTraffic::APtr session_tunnelendpoint_traffic_;

void ApplClientSession::app_client_endpoint_get_enrollment_status(void) {
	session_tunnelendpoint_endpoint_->app_client_endpoint_get_enrollment_status();
}

void ApplClientSession::app_client_tag_get_platform_response(const std::string& platform) {
	session_tunnelendpoint_tag_->rpc_dispatch_remote_get_platform_response(platform);
}

void ApplClientSession::appl_client_set_connection_info(const std::string& servers) {
	session_tunnelendpoint_cpm_->set_connection_info(servers);
}

void ApplClientSession::app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time) {
	if (session_tunnelendpoint_auth_.get() != NULL) {
		session_tunnelendpoint_auth_->app_client_dme_response_get_dme_values(username, password, terminal_id, device_signature, device_time);
	}
}

void ApplClientSession::app_client_dme_response_get_dme_values_none(void) {
	if (session_tunnelendpoint_auth_.get() != NULL) {
		session_tunnelendpoint_auth_->app_client_dme_response_get_dme_values_none();
	}
}
