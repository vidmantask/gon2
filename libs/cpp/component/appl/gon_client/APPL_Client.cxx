/*! \file APPL_Client.cxx
 \brief This file contains the implementation for the CPPL_Client class
 */
#include <sstream>
#include <functional>

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/predicate.hpp>

#include <cryptopp/base64.h>
#include <cryptopp/gzip.h>

#include <component/appl/gon_client/APPL_Client.hxx>
#include <component/appl/gon_client/APPL_CheckpointAttr.hxx>


#include <lib/utility/UY_Checkpoint.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/cryptfacility/CF_CryptFacilityService.hxx>
#include <lib/cryptfacility/CF_CryptFactory.hxx>

#include <component/communication/COM_Exception.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_SessionManagerEventhandler.hxx>


using namespace std;
using namespace Giritech::Appl;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

/*
 * ------------------------------------------------------------------
 * ApplClient implementation
 * ------------------------------------------------------------------
 */
ApplClient::ApplClient(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: async_service_(AsyncService::create(checkpoint_handler)),
	  appl_client_eventhandler_(app_client_eventhandler),
	  servers_(servers),
	  knownsecret_(knownsecret),
	  state_(State_Initializing),
	  async_state_(AsyncState_Initializing),
	  checkpoint_handler_(checkpoint_handler),
	  janitor_timeout_timer_(async_service_->get_io_service()),
	  janitor_timeout_(boost::posix_time::seconds(5)),
	  check_network_timeout_timer_(async_service_->get_io_service()),
	  check_network_timeout_(boost::posix_time::seconds(15)),
	  check_network_timer_(async_service_->get_io_service()) {
    CryptFacility::CryptFacilityService::getInstance().initialize(CryptFacility::CryptFacilityService::modeofoperation_unknown);
	mutex_ = Mutex::create(async_service_->get_io_service(), "ApplClient");
	key_store_ =  ApplClientKeyStore::create(async_service_->get_io_service(), CryptFacility::CryptFacilityService::getInstance().getCryptFactory());
    session_manager_ = SessionManagerClient::create(async_service_, checkpoint_handler_, DataBufferManaged::create(knownsecret_)->decodeHex(), key_store_);
    session_manager_->set_appl_protocol_type(SessionMessage::ApplProtocolType_xml);
    key_store_->setEventhandler(this);
}

ApplClient::~ApplClient(void) {
	state_ = State_Unknown;
	async_state_ = AsyncState_Unknown;
	key_store_->resetEventhandler();
}

ApplClient::APtr ApplClient::create(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret) {
	return ApplClient::APtr(new ApplClient(app_client_eventhandler, servers, knownsecret, CheckpointHandler::create_cout_all()));
}
ApplClient::APtr ApplClient::create(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return ApplClient::APtr(new ApplClient(app_client_eventhandler, servers, knownsecret, checkpoint_handler));
}

bool ApplClient::async_is_running(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "start");
    switch (async_state_) {
    case AsyncState_Running:
    	return true;
    }
    return false;
}

void ApplClient::async_start(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "start");
    switch (async_state_) {
    case AsyncState_Initializing:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "async_start.invalid_state", Attr_ApplClient(), CpAttr_critical());
        return;
    }
    janitor_start();

    threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    async_state_ = AsyncState_Running;
    state_ = State_ReadyToConnect;
}

void ApplClient::async_restart(void) {
	MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "async_restart");
    switch (async_state_) {
    case AsyncState_Initializing:
    case AsyncState_Stopped: {
    	break;
    }
    default:
        Checkpoint cp(*checkpoint_handler_, "async_restart.invalid_state", Attr_ApplClient(), CpAttr_critical());
        return;
    }
	std::vector<boost::thread*>::const_iterator i(threads_.begin());
	while(i != threads_.end()) {
		thread_group_.remove_thread(*i);
		++i;
	}
	async_state_ = AsyncState_Initializing;
	async_service_->reset();
	janitor_start();

	// Only one thread is used, ApplClient, ApplClientSession and ApplClientSessionTunnelendpoints are not thread safe (no mutex taken in start of methods).
	threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
	threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
	threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
	threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
	threads_.push_back(thread_group_.create_thread(boost::bind(&AsyncService::run, async_service_)));
    async_state_ = AsyncState_Running;

    state_ = State_ReadyToConnect;
}

void ApplClient::async_join(void) {
	thread_group_.join_all();
}

void ApplClient::async_stop(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "stop");
    switch (async_state_) {
    case AsyncState_Stopped:
    case AsyncState_Stopping:
    	return;
    case AsyncState_Running:
    	break;
    default: {
        Checkpoint cp(*checkpoint_handler_, "async_stop.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("async_state", async_state_));
        return;
    }
    }
    async_state_ = AsyncState_Stopping;
    action_close();
}

void ApplClient::async_stop_final(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "async_stop_final");
    switch (async_state_) {
    case AsyncState_Stopping:
    	break;
    default: {
        Checkpoint cp(*checkpoint_handler_, "async_stop_final.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("async_state", async_state_));
        return;
    }
    }
	async_state_ = AsyncState_Stopped;
	async_service_->stop();
	Checkpoint cp3(*checkpoint_handler_, "async_stop_final.done", Attr_ApplClient(), CpAttr_debug());
}


void ApplClient::janitor_start(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "janitor_start");
    switch (async_state_) {
    case AsyncState_Initializing:
    case AsyncState_Stopping:
    case AsyncState_Running:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "janitor_start.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("async_state", async_state_));
        return;
    }
    janitor_timeout_timer_.expires_from_now(janitor_timeout_);
    janitor_timeout_timer_.async_wait(boost::bind(&ApplClient::janitor_work, this, boost::asio::placeholders::error));
}

void ApplClient::janitor_work(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "janitor_work");
    Checkpoint cp(*checkpoint_handler_, "janitor_work", Attr_ApplClient(), CpAttr_debug(), CpAttr("async_state", async_state_), CpAttr("state_", state_));
    if ((async_state_ == AsyncState_Stopping) && (state_ == State_Closed || state_ == State_ReadyToConnect)) {
    	async_stop_final();
    	return;
    }

    janitor_start();
}

void ApplClient::action_connect(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect");
    switch (state_) {
    case State_ReadyToConnect:
    case State_Closed:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_connect.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::action_connect_decoubled, this, knownsecret_, servers_));
}

void ApplClient::action_connect(const std::string& knownsecret, const std::string& servers) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect");
    switch (state_) {
    case State_ReadyToConnect:
    case State_Closed:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_connect.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    servers_ = servers;
    mutex_->get_strand().post(boost::bind(&ApplClient::action_connect_decoubled, this, knownsecret, servers));
}

void ApplClient::action_connect_decoubled(const std::string& knownsecret, const std::string& servers) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect");
    switch (state_) {
    case State_ReadyToConnect:
    case State_Closed:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_connect.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    state_ = State_WaitingForNetwork;

    // willConnect can take some time because of key generation, this call will change state of gui to connecting
    session_manager_connecting_decoubled(0, "");


    // Ask for saved session keys
	Utility::DataBufferManaged::APtr sessionKeyPublic;
	Utility::DataBufferManaged::APtr sessionKeyPrivate;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_key_store_session_key_pair_get(sessionKeyPublic, sessionKeyPrivate);
        key_store_->setSessionKeypair(sessionKeyPublic, sessionKeyPrivate);
    }

    check_network_timeout_timer_.expires_from_now(check_network_timeout_);
    check_network_timeout_timer_.async_wait(boost::bind(&ApplClient::action_connect_check_network_timeout, this));

    action_connect_check_network_start(knownsecret, servers, true);
}

void ApplClient::action_connect_check_network_start(const std::string& knownsecret, const std::string& servers, const bool now) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect_check_network_start");
    switch (state_) {
    case State_WaitingForNetwork:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_connect_check_network_start.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if(now) {
    	check_network_timer_.expires_from_now(boost::posix_time::seconds(0));
    }
    else {
    	check_network_timer_.expires_from_now(boost::posix_time::seconds(2));
    }
    check_network_timer_.async_wait(boost::bind(&ApplClient::action_connect_check_network, this, knownsecret, servers));
}

void ApplClient::action_connect_check_network(const std::string& knownsecret, const std::string& servers) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect_check_network");
    switch (state_) {
    case State_WaitingForNetwork:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_connect_check_network.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }

    bool network_is_ok = true;
    if(appl_client_eventhandler_ != NULL) {
    	network_is_ok = appl_client_eventhandler_->app_client_is_network_available();
    }

    if(!network_is_ok) {
    	action_connect_check_network_start(knownsecret, servers, false);
    	return;
    }

    state_ = State_Connecting;
    check_network_timeout_timer_.expires_from_now(boost::posix_time::seconds(0));

    session_manager_->set_eventhandler(this);
    session_manager_->set_knownsecret_and_servers(DataBufferManaged::create(knownsecret)->decodeHex(), servers);
    session_manager_->start();
}

void ApplClient::action_connect_check_network_timeout(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_connect");
    switch (state_) {
    case State_WaitingForNetwork:
    	break;
    default:
        return;
    }
    session_manager_failed("Network is unavailable");
}

void ApplClient::action_close(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_close");
    switch (state_) {
    case State_Connected:
    case State_Connecting:
    case State_WaitingForNetwork:
    	break;
    case State_ReadyToConnect:
    case State_Closing:
    case State_Closed:
    	return;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_close.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }

    if(state_ == State_WaitingForNetwork) {
        state_ = State_Closing;
        check_network_timeout_timer_.expires_from_now(boost::posix_time::seconds(0));
    	check_network_timer_.expires_from_now(boost::posix_time::seconds(0));
        close_final();
        return;
    }

    // State is changed here and eventhandler called for more responsive ui
    // Also called by close_start
    state_ = State_Closing;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_closing();
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::close_start, this));
}

void ApplClient::action_close_no_event(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_close");
    switch (state_) {
    case State_Connected:
    case State_Connecting:
    case State_WaitingForNetwork:
    	break;
    case State_ReadyToConnect:
    case State_Closing:
    case State_Closed:
    	return;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_close.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }

    if(state_ == State_WaitingForNetwork) {
        state_ = State_Closing;
        check_network_timeout_timer_.expires_from_now(boost::posix_time::seconds(0));
    	check_network_timer_.expires_from_now(boost::posix_time::seconds(0));
        close_final();
        return;
    }
    state_ = State_Closing;
    mutex_->get_strand().post(boost::bind(&ApplClient::close_start, this));
}



void ApplClient::action_close_direct(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_close_direct");
    switch (state_) {
    case State_Connected:
    case State_Connecting:
    case State_WaitingForNetwork:
    	break;
    case State_ReadyToConnect:
    case State_Closing:
    case State_Closed:
    	return;
    default:
        Checkpoint cp(*checkpoint_handler_, "action_close_direct.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    close_start();
}

void ApplClient::close_start(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "action_close");
    Checkpoint cp(*checkpoint_handler_, "close_start", Attr_ApplClient(), CpAttr_debug());
    switch (state_) {
    case State_ReadyToConnect:
    case State_Connected:
    case State_Connecting:
    case State_WaitingForNetwork:
    case State_Closing:
        break;
    default:
        return;
    }
    state_ = State_Closing;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_closing();
    }

    // No session yes
    if(session_.get() == NULL) {
    	return;
    }

    if(!session_->is_closed()) {
    	session_->close();
    	return;
    }
    close_final();
}

void ApplClient::close_final(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "close_final");
    Checkpoint cp(*checkpoint_handler_, "close_final", Attr_ApplClient(), CpAttr_debug());
    switch (state_) {
    case State_Closing:
    	break;
    default:
        return;
    }
    if(session_.get() && !session_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "close_final.posponed.session", Attr_ApplClient(), CpAttr_debug());
    	return;
    }

    if(session_manager_.get() && !session_manager_->is_closed()) {
        Checkpoint cp(*checkpoint_handler_, "close_final.posponed.session_manager", Attr_ApplClient(), CpAttr_debug());
    	return;
    }

    state_ = State_Closed;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_closed();
    }
}

ApplClient::State ApplClient::get_state(void) const {
	return state_;
}

std::string ApplClient::get_session_id(void) {
	return session_->get_session_id();
}

RawTunnelendpointTCP::APtr ApplClient::get_raw_tunnelendpoint(void) {
	return session_->get_raw_tunnelendpoint();
}

/*
 * Implementation of SessionManager events
 */
void ApplClient::session_manager_resolve_connection(std::string& type, std::string& host, unsigned long& port, boost::posix_time::time_duration& timeout) {
	// Ignore this event
}

void ApplClient::session_manager_connecting(const unsigned long& connection_id, const std::string& title) {
	mutex_->get_strand().post(boost::bind(&ApplClient::session_manager_connecting_decoubled, this, connection_id, title));
}

void ApplClient::session_manager_connecting_decoubled(const unsigned long& connection_id, const std::string& title) {
	if(appl_client_eventhandler_ != NULL) {
		appl_client_eventhandler_->app_client_event_connecting(title);
	}
}

void ApplClient::session_manager_connecting_failed(const unsigned long& connection_id) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "session_manager_connecting_failed");
    switch (state_) {
    case State_Connecting:
    case State_Closing:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "session_manager_connecting_failed.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
}

void ApplClient::session_manager_failed(const std::string& message) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "session_manager_failed");
    switch (state_) {
    case State_WaitingForNetwork:
    case State_Connecting:
    	break;
    default:
        return;
    }

    if(appl_client_eventhandler_ != NULL) {
		appl_client_eventhandler_->app_client_event_connecting_failed(message);
	}

    state_ = State_Closing;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_closing();
    }
    close_final();
}

void ApplClient::session_manager_session_created(const unsigned long& connection_id, Session::APtr& com_session) {
	// Change mutex to use session mutex
	Mutex::safe_update(mutex_, com_session->get_mutex());

	MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "session_manager_session_created");
    switch (state_) {
    case State_Connecting:
    case State_Closing:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "session_manager_session_created.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if(state_ == State_Closing) {
    	com_session->close(true);
    	return;
    }
    session_ = ApplClientSession::create(async_service_, checkpoint_handler_, com_session, appl_client_eventhandler_, this, key_store_);
    session_->appl_client_auth_set_info(auth_serial_, auth_private_key_, auth_public_key_);
    session_->appl_client_set_connection_info(servers_);
}

void ApplClient::session_manager_session_closed(const unsigned long session_id) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "session_manager_session_closed");
    switch (state_) {
    case State_Connected:
        close_start();
    	return;
    case State_Closing:
        close_final();
    	return;
    default:
        return;
    }
}

void ApplClient::session_manager_closed(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "session_manager_closed");
    switch (state_) {
    case State_Closing:
    case State_Connecting:
    case State_Connected:
    case State_Closed:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "session_manager_closed.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    state_ = State_Closing;
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_event_closing();
    }
    close_final();
}

void ApplClient::session_manager_security_dos_attack_start(const std::string& attacker_ips) {
	// Ignore signal only relevant  on server side
}

void ApplClient::session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count) {
	// Ignore signal only relevant  on server side

}

void ApplClient::app_client_session_connected(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_session_connected");
    switch (state_) {
    case State_Connecting:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_session_connected.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    state_ = State_Connected;
}

void ApplClient::app_client_session_closed(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_session_closed");
    switch (state_) {
    case State_Closing:
        close_final();
        return;
    default:
    	break;
    }
    close_start();
}

void ApplClient::app_client_dialog_response_inform_on_first_access(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dialog_response_inform_on_first_access.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dialog_response_inform_on_first_access_decoubled, this));
}

void ApplClient::app_client_dialog_response_inform_on_first_access_decoubled(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_response_inform_on_first_access_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_dialog_response_inform_on_first_access();
    }
}

void ApplClient::app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dialog_menu_item_selected.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dialog_menu_item_selected_decoubled, this, launch_id, auto_launch));
}

void ApplClient::app_client_dialog_menu_item_selected_decoubled(const std::string& launch_id, const bool auto_launch) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_menu_item_selected_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_dialog_menu_item_selected(launch_id, auto_launch);
    }
}

void ApplClient::app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dialog_get_images.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dialog_get_images_decoubled, this, request_id, image_ids, image_style, image_size_x, image_size_y, image_format));
}

void ApplClient::app_client_dialog_get_images_decoubled(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_get_images_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_dialog_get_images(request_id, image_ids, image_style, image_size_x, image_size_y, image_format);
    }
}

bool ApplClient::app_client_dialog_menu_item_is_launched(const std::string& launch_id) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_menu_item_is_launched");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return false;
    }
    if (session_.get() != NULL) {
    	return session_->app_client_dialog_menu_item_is_launched(launch_id);
    }
    return false;
}

void ApplClient::app_client_traffic_launch_command_running(const std::string& launch_id) {
	switch (state_) {
	case State_Connected:
		break;
	default:
		Checkpoint cp(*checkpoint_handler_, "app_client_traffic_launch_command_running.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
		return;
	}
	mutex_->get_strand().post(boost::bind(&ApplClient::app_client_traffic_launch_command_running_decoubled, this, launch_id));
}

void ApplClient::app_client_traffic_launch_command_running_decoubled(const std::string& launch_id) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_traffic_launch_command_running_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_traffic_launch_command_running(launch_id);
    }
}

void ApplClient::app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message) {
	switch (state_) {
	case State_Connected:
	case State_Closing:
	case State_Closed:
		break;
	default:
		Checkpoint cp(*checkpoint_handler_, "app_client_traffic_launch_command_terminated.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
		return;
	}
	mutex_->get_strand().post(boost::bind(&ApplClient::app_client_traffic_launch_command_terminated_decoubled, this, launch_id, error, error_message));
}

void ApplClient::app_client_traffic_launch_command_terminated_decoubled(const std::string& launch_id, const bool& error, const std::string& error_message) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_traffic_launch_command_terminated_decoubled");
    switch (state_) {
    case State_Connected:
	case State_Closing:
	case State_Closed:
    	break;
    default:
		Checkpoint cp(*checkpoint_handler_, "app_client_traffic_launch_command_terminated_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_traffic_launch_command_terminated(launch_id, error, error_message);
    }
}

ApplClientEventhandler::State ApplClient::app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_menu_item_get_status_info");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return ApplClientEventhandler::StateError;
    }
    if (session_.get() != NULL) {
    	ApplClientEventhandler::State result = session_->app_client_dialog_menu_item_get_status_info(launch_id, state_info);
    	return result;
    }
	return ApplClientEventhandler::StateError;
}

void ApplClient::app_client_dialog_menu_item_close(const std::string& launch_id) {
	switch (state_) {
	case State_Connected:
		break;
	default:
		Checkpoint cp(*checkpoint_handler_, "app_client_dialog_menu_item_close.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
		return;
	}
	mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dialog_menu_item_close_decoubled, this, launch_id));
}

void ApplClient::app_client_dialog_menu_item_close_decoubled(const std::string& launch_id) {
	MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_menu_item_close_decoubled");
	switch (state_) {
	case State_Connected:
		break;
	default:
		return;
	}
	if (session_.get() != NULL) {
		session_->app_client_dialog_menu_item_close(launch_id);
	}
}

void ApplClient::app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_user_response_login.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_user_response_login_decoubled, this, login, password, requestedpassword));
}

void ApplClient::app_client_user_response_login_decoubled(const std::string& login, const std::string& password, const bool requestedpassword) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_dialog_response_inform_on_first_access_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_user_response_login(login, password, requestedpassword);
    }
}

void ApplClient::app_client_user_response_login_cancelled(void) {
    switch (state_) {
    case State_Connected:
    	break;
    case State_Closed:
    case State_Closing:
    	return;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_user_response_login_cancelled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_user_response_login_cancelled_decoubled, this));
}

void ApplClient::app_client_user_response_login_cancelled_decoubled(void) {
    MutexScopeLock mutex_lock_(checkpoint_handler_, Attr_ApplClient(), mutex_, "app_client_user_response_login_cancelled_decoubled");
    switch (state_) {
    case State_Connected:
    	break;
    default:
    	return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_user_response_login_cancelled();
    }
}

void ApplClient::app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_endpoint_info.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_response_endpoint_info_decoubled, this, info_id, info_value));
}

void ApplClient::app_client_endpoint_response_endpoint_info_decoubled(const std::string& info_id, const std::string& info_value) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_endpoint_info.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_response_endpoint_info(info_id, info_value);
    }
}

void ApplClient::app_client_endpoint_response_endpoint_info_all_done(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_endpoint_info_all_done.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_response_endpoint_info_all_done_decoubled, this));
}

void ApplClient::app_client_endpoint_response_endpoint_info_all_done_decoubled(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_endpoint_info_all_done.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_response_endpoint_info_all_done();
    }
}

void ApplClient::app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_token.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_response_token_decoubled, this, token_type, token_internal_type, token_serial, token_title));
}

void ApplClient::app_client_endpoint_response_token_decoubled(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_response_token.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_response_token(token_type, token_internal_type, token_serial, token_title);
    }
}

void ApplClient::app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_enroll_token.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_enroll_token_decoubled, this, token_serial, token_public_key));
}

void ApplClient::app_client_endpoint_enroll_token_decoubled(const std::string& token_serial, const std::string& token_public_key) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_enroll_token.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_enroll_token(token_serial, token_public_key);
    }
}


void ApplClient::app_client_endpoint_token_status(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_token_status.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_token_status_decoubled, this));
}

void ApplClient::app_client_endpoint_token_status_decoubled(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_token_status_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_token_status();
    }
}

void ApplClient::app_client_endpoint_get_enrollment_status(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_get_enrollment_status.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_endpoint_get_enrollment_status_decoubled, this));
}

void ApplClient::app_client_endpoint_get_enrollment_status_decoubled(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_endpoint_get_enrollment_status_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_endpoint_get_enrollment_status();
    }
}

void ApplClient::app_client_tag_get_platform_response(const std::string& platform) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_tag_get_platform_response.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_tag_get_platform_response_decoubled, this, platform));
}

void ApplClient::app_client_tag_get_platform_response_decoubled(const std::string& platform) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_tag_get_platform_response_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_tag_get_platform_response(platform);
    }
}


void ApplClient::app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time) {
	switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dme_response_get_dme_values.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dme_response_get_dme_values_decoubled, this, username, password, terminal_id, device_signature, device_time));
}

void ApplClient::app_client_dme_response_get_dme_values_decoubled(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dme_response_get_dme_values_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_dme_response_get_dme_values(username, password, terminal_id, device_signature, device_time);
    }
}

void ApplClient::app_client_dme_response_get_dme_values_none(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dme_response_get_dme_values_none.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::app_client_dme_response_get_dme_values_none_decoubled, this));
}

void ApplClient::app_client_dme_response_get_dme_values_none_decoubled(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        Checkpoint cp(*checkpoint_handler_, "app_client_dme_response_get_dme_values_none_decoubled.invalid_state", Attr_ApplClient(), CpAttr_critical(), CpAttr("state", state_));
        return;
    }
    if (session_.get() != NULL) {
    	session_->app_client_dme_response_get_dme_values_none();
    }
}

void ApplClient::appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key) {
	auth_serial_ = serial;
	auth_private_key_ = private_key;
	auth_public_key_ = public_key;
}

void ApplClient::appl_client_auth_generate_keys(void) {
    mutex_->get_strand().post(boost::bind(&ApplClient::appl_client_auth_generate_keys_decoubled, this));
}




void ApplClient::appl_client_auth_generate_keys_decoubled(void) {
    CryptFacility::CryptFacilityService::getInstance().initialize(CryptFacility::CryptFacilityService::modeofoperation_unknown);
	boost::shared_ptr<CryptFacility::CryptFactory> factory(CryptFacility::CryptFacilityService::getInstance().getCryptFactory());
	CryptFacility::CryptFactory::PKPair key_pair(factory->pk_generate_keys());
	if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_auth_generate_keys_response(key_pair.second->encodeHex()->toString(), key_pair.first->encodeHex()->toString());
    }
}

void ApplClient::decode_connection_info(const std::string& connection_info_raw, bool& decode_ok, std::string& decode_servers, std::string& decode_knownsecret) {
	decode_ok = false;
	std::string connection_info;
	try {
		CryptoPP::StringSink* sink = new CryptoPP::StringSink(connection_info);
		CryptoPP::Gunzip* unzip = new CryptoPP::Gunzip(sink);
		CryptoPP::Base64Decoder* base64_dec = new CryptoPP::Base64Decoder(unzip);
		CryptoPP::StringSource source(connection_info_raw, true, base64_dec);

		typedef std::vector<std::string> SplitVector;
		SplitVector split_vector;
		boost::iter_split(split_vector, connection_info, boost::first_finder(":,:"));
		if(split_vector.size() == 3) {
			if (split_vector.at(0) == "jpxcite120") {
				decode_ok = true;
				decode_servers = split_vector.at(1);
				decode_knownsecret = split_vector.at(2);
			}
		}
	} catch (const std::exception &e) {
		std::cout << "unsuccessful! caught " << "unknown" << ": " << e.what() << std::endl;
	} catch (...) {
		std::cout << "unsuccessful!" << std::endl;
	}
}

void ApplClient::appl_client_dialog_auto_launch(const std::vector<ApplClientMenuItem>& menu_items, const std::map<std::string, ApplClientTagProperty>& tag_properties, const bool auto_launch_first) {
	std::vector<ApplClientMenuItem>::const_iterator i(menu_items.begin());
	while(i != menu_items.end()) {
		if(do_auto_launch(*i, tag_properties, auto_launch_first)) {
			app_client_dialog_menu_item_selected(i->launch_id, true);
		}
		++i;
	}
}

bool ApplClient::do_auto_launch(const ApplClientMenuItem& menu_item, const std::map<std::string, ApplClientTagProperty>& tag_properties, const bool auto_launch_first) {
	std::vector<std::string>::const_iterator i(menu_item.tags.begin());
	while(i != menu_item.tags.end()) {
		std::map<std::string, ApplClientTagProperty>::const_iterator h(tag_properties.find(*i));
		if (h != tag_properties.end()) {
			if(auto_launch_first) {
				if ((h->second).auto_launch_first) {
					return true;
				}
			}
			else if ((h->second).auto_launch && menu_item.tag_enabled) {
				return true;
			}
		}
		++i;
	}
	return false;
}

void ApplClient::app_client_key_store_reset(void) {
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_key_store_reset();
    }
}

void ApplClient::app_client_key_store_session_key_pair_set(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate) {
    if(appl_client_eventhandler_ != NULL) {
    	appl_client_eventhandler_->app_client_key_store_session_key_pair_set(sessionKeyPublic, sessionKeyPrivate);
    }
}

void ApplClient::appl_client_verify_connection(void) {
    switch (state_) {
    case State_Connected:
    	break;
    default:
        return;
    }
    mutex_->get_strand().post(boost::bind(&ApplClient::appl_client_verify_connection_decoubled, this));
}

void ApplClient::appl_client_verify_connection_decoubled(void) {
    switch (state_) {
    case State_Connecting:
    case State_Connected:
    	break;
    default:
        return;
    }

    if(appl_client_eventhandler_ != NULL) {
    	if(! appl_client_eventhandler_->app_client_is_network_available()) {
        	Checkpoint cp(*checkpoint_handler_, "appl_client_verify_connection.network_check.failed", Attr_ApplClient(), CpAttr_debug());
        	close_start();
        	return;
    	}
    }
    if(! session_->is_connected() ) {
    	Checkpoint cp(*checkpoint_handler_, "appl_client_verify_connection.not_connected", Attr_ApplClient(), CpAttr_debug());
    	close_start();
    	return;
    }
}
