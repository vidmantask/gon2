/*! \file APPL_Client.hxx
 \brief This file contains the client functionctionlaity
 */
#ifndef APPL_Client_HXX
#define APPL_Client_HXX

#include <string>

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>
#include <component/communication/COM_AsyncService.hxx>
#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionManager.hxx>
#include <component/communication/COM_SessionManagerEventhandler.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>

#include <component/appl/gon_client/APPL_ClientSession.hxx>
#include <component/appl/gon_client/APPL_KeyStore.hxx>
#include <component/appl/gon_client/APPL_KeyStoreEventhandler.hxx>


namespace Giritech {
namespace Appl {

/*
 * All calls are asynchronous and thread safe.
 */
class ApplClient : public Communication::SessionManagerEventhandler, public ApplClientSessionEventhandler, public ApplClientKeyStoreEventhandler {
public:
    typedef boost::shared_ptr<ApplClient> APtr;

    enum State {
        State_Initializing = 0,
        State_ReadyToConnect,
        State_WaitingForNetwork,
        State_Connecting,
        State_Connected,
        State_Closing,
        State_Closed,
        State_Unknown
    };

    enum AsyncState {
    	AsyncState_Initializing = 0,
    	AsyncState_Running,
    	AsyncState_Stopping,
    	AsyncState_Stopped,
    	AsyncState_Unknown
    };

    /*! \brief Destructor
     */
    ~ApplClient(void);


    /*! \brief start
     *
     * Starts app_client thread
     */
    void async_start(void);
    void async_restart(void);
    void async_join(void);
    bool async_is_running(void);


    /*! \brief stop
     *
     * Stops app_client thread, this will terminate the connection.
     * After this call no more events are dispatched to the eventhandler.
     */
    void async_stop(void);

    /*! \brief action_connect
     *
     * Available in in state State_ReadyToConnect.
     *
     * The event event_connecting(server_addr, server_port, server_name) is called before a connection attempt.
     * When connected the event event_connected() will be called.
     *
     * If connection to the server fails, the event event_connect_failed(error_message) will be called.
     */
    void action_connect(void);
    void action_connect(const std::string& knownsecret, const std::string& servers);

    /*! \brief action_connect
     *
     */
    void action_close(void);
    void action_close_no_event(void);
    void action_close_direct(void);

    /*! \brief Return state
     */
    State get_state(void) const;

    /*! \brief Return session id
     */
    std::string get_session_id(void);

    /*! Get raw tunnelendpoint
     */
    Communication::RawTunnelendpointTCP::APtr get_raw_tunnelendpoint(void);

    /*! \brief Create instance
     */
    static APtr create(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret);
    static APtr create(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    /*! \brief Implementation of SessionManager events
     */
    void session_manager_resolve_connection(std::string& type, std::string& host, unsigned long& port, boost::posix_time::time_duration& timeout);
    void session_manager_connecting(const unsigned long& connection_id, const std::string& title);
    void session_manager_connecting_failed(const unsigned long& connection_id);
    void session_manager_failed(const std::string& message);
    void session_manager_session_created(const unsigned long& connection_id, Communication::Session::APtr& session);
    void session_manager_session_closed(const unsigned long session_id);
    void session_manager_closed(void);
    void session_manager_security_dos_attack_start(const std::string& attacker_ips);
    void session_manager_security_dos_attack_stop(const std::string& attacker_ips, const unsigned long banned_connection_count, const unsigned long failed_keyexchange_count);

    /*! \brief Implementation of ApplCleintSession events
     */
    void app_client_session_connected(void);
    void app_client_session_closed(void);

	/*! \brief Implementation of ApplClientKeyStoreEventhandler events
     */
    void app_client_key_store_reset(void);
    void app_client_key_store_session_key_pair_set(Utility::DataBufferManaged::APtr& sessionKeyPublic, Utility::DataBufferManaged::APtr& sessionKeyPrivate);


    /*! \brief AppClient API
     */
    void app_client_dialog_response_inform_on_first_access(void);

    void app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch);
    void app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format);
    bool app_client_dialog_menu_item_is_launched(const std::string& launch_id);
    ApplClientEventhandler::State app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info);
	void app_client_dialog_menu_item_close(const std::string& launch_id);

    void app_client_traffic_launch_command_running(const std::string& launch_id);
    void app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message);

    void app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword);
    void app_client_user_response_login_cancelled(void);

	void app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value);
	void app_client_endpoint_response_endpoint_info_all_done(void);

	void app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title);
	void app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key);
	void app_client_endpoint_token_status(void);
	void app_client_endpoint_get_enrollment_status(void);

	void appl_client_dialog_auto_launch(const std::vector<ApplClientMenuItem>& menu_items, const std::map<std::string, ApplClientTagProperty>& tag_properties, const bool auto_launch_first);

	/*! \brief Authentication functionality
	 */
	void appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key);
	void appl_client_auth_generate_keys(void);

	void appl_client_key_store_force_timeout(void);

	void app_client_tag_get_platform_response(const std::string& platform);


	void app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time);
    void app_client_dme_response_get_dme_values_none(void);


    /*
     *
     */
    void appl_client_verify_connection(void);


	static void decode_connection_info(const std::string& connection_info_raw, bool& decode_ok, std::string& servers, std::string& knownsecret);

protected:

    /*! \brief Constructor
     */
    ApplClient(ApplClientEventhandler* app_client_eventhandler, const std::string& servers, const std::string& knownsecret, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    void janitor_start(void);
    void janitor_work(const boost::system::error_code& error);

    void goto_state_closed(void);

    void session_manager_connecting_decoubled(const unsigned long& connection_id, const std::string& title);

    void close_start(void);
    void close_final(void);

    void action_connect_decoubled(void);
    void action_connect_decoubled(const std::string& knownsecret, const std::string& servers);

    void action_connect_check_network_start(const std::string& knownsecret, const std::string& servers, const bool now);
    void action_connect_check_network(const std::string& knownsecret, const std::string& servers);
    void action_connect_check_network_timeout(void);

    void app_client_dialog_response_inform_on_first_access_decoubled(void);
    void app_client_dialog_menu_item_selected_decoubled(const std::string& launch_id, const bool auto_launch);
    void app_client_dialog_get_images_decoubled(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format);

    void app_client_user_response_login_decoubled(const std::string& login, const std::string& password, const bool requestedpassword);
    void app_client_user_response_login_cancelled_decoubled(void);
	void app_client_endpoint_response_endpoint_info_decoubled(const std::string& info_id, const std::string& info_value);
	void app_client_endpoint_response_endpoint_info_all_done_decoubled(void);
	void app_client_dialog_menu_item_close_decoubled(const std::string& launch_id);

    void app_client_traffic_launch_command_running_decoubled(const std::string& launch_id);
    void app_client_traffic_launch_command_terminated_decoubled(const std::string& launch_id, const bool& error, const std::string& error_message);

	void app_client_endpoint_response_token_decoubled(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title);
	void app_client_endpoint_enroll_token_decoubled(const std::string& token_serial, const std::string& token_public_key);

	void appl_client_auth_generate_keys_decoubled(void);
	void app_client_endpoint_token_status_decoubled(void);

	void app_client_endpoint_get_enrollment_status_decoubled(void);

	void app_client_tag_get_platform_response_decoubled(const std::string& platform);


    void app_client_dme_response_get_dme_values_decoubled(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time);
    void app_client_dme_response_get_dme_values_none_decoubled(void);


	bool do_auto_launch(const ApplClientMenuItem& menu_item, const std::map<std::string, ApplClientTagProperty>& tag_properties, const bool auto_launch_first);

    void async_stop_final(void);

    void appl_client_verify_connection_decoubled(void);

    AsyncState async_state_;
    State state_;
    Communication::AsyncService::APtr async_service_;

    Utility::CheckpointHandler::APtr checkpoint_handler_;
    Utility::Mutex::APtr mutex_;
    boost::thread_group thread_group_;

    std::vector<boost::thread*> threads_;

    boost::asio::deadline_timer janitor_timeout_timer_;
    boost::posix_time::time_duration janitor_timeout_;

    ApplClientEventhandler* appl_client_eventhandler_;
    std::string servers_;
    std::string knownsecret_;

    Communication::SessionManagerClient::APtr session_manager_;
//    std::map<unsigned long, ApplClientSession::APtr> sessions_;
    ApplClientSession::APtr session_;

private:
    std::string auth_serial_;
    std::string auth_private_key_;
    std::string auth_public_key_;

    ApplClientKeyStore::APtr key_store_;

    boost::asio::deadline_timer check_network_timer_;

    boost::asio::deadline_timer check_network_timeout_timer_;
    boost::posix_time::time_duration check_network_timeout_;
};

}
}
#endif
