/*! \file APPL_ClientSession.hxx
 \brief This file contains the client functionctionlaity
 */
#ifndef APPL_ClientSession_HXX
#define APPL_ClientSession_HXX

#include <string>

#include <boost/shared_ptr.hpp>

#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_MutexLock.hxx>

#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>
#include <component/appl/gon_client/APPL_ClientSessionEventhandler.hxx>
#include <component/appl/gon_client/APPL_ClientSessionTunnelendpoint.hxx>
#include <component/appl/gon_client/APPL_KeyStore.hxx>

#include <component/communication/COM_Session.hxx>
#include <component/communication/COM_SessionEventhandler.hxx>
#include <component/communication/COM_CheckpointAttr.hxx>


namespace Giritech {
namespace Appl {



class ApplClientSession : public Communication::SessionEventhandlerReady {
public:
    typedef boost::shared_ptr<ApplClientSession> APtr;

    enum State {
        State_Initializing,
        State_Connected,
        State_Closeing,
        State_Closed,
        State_Unknown
    };

    /*! \brief Destructor
     */
    ~ApplClientSession(void);

    /*! \brief Return state
     */
    State get_state(void) const;

    /*! \brief Return session id
     */
    std::string get_session_id(void);

    /*! \brief Return raw tunnelendpoint used by session
     */
    Communication::RawTunnelendpointTCP::APtr get_raw_tunnelendpoint(void);

	void reset_appl_client_eventhandler(void);

	void appl_client_auth_set_info(const std::string& serial, const std::string& private_key, const std::string& public_key);

    /*! \brief Create instance
     */
    static APtr create(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const Communication::Session::APtr& session, ApplClientEventhandler* eventhandler, ApplClientSessionEventhandler* session_eventhandler, const ApplClientKeyStore::APtr& key_store);

    /*! \brief Implementation of SessionEventhandlerReady events
     */
    void session_state_ready(void);
    void session_state_closed(void);
    void session_state_key_exchange(void);
    bool session_read_continue_state_ready(void);
    void session_user_signal(const unsigned long signal_id, const Communication::MessagePayload::APtr& message);


    void app_client_dialog_response_inform_on_first_access(void);
    void app_client_dialog_menu_item_selected(const std::string& launch_id, const bool auto_launch);
    bool app_client_dialog_menu_item_is_launched(const std::string& launch_id);
    void app_client_dialog_get_images(const std::string& request_id, const std::vector<std::string>& image_ids, const std::string& image_style, const int image_size_x, const int image_size_y, const std::string& image_format);
    ApplClientEventhandler::State app_client_dialog_menu_item_get_status_info(const std::string& launch_id, std::string& state_info);
	void app_client_dialog_menu_item_close(const std::string& launch_id);

    void app_client_traffic_launch_command_running(const std::string& launch_id);
    void app_client_traffic_launch_command_terminated(const std::string& launch_id, const bool& error, const std::string& error_message);

    void app_client_user_response_login(const std::string& login, const std::string& password, const bool requestedpassword);
    void app_client_user_response_login_cancelled(void);

	void app_client_endpoint_response_endpoint_info(const std::string& info_id, const std::string& info_value);
	void app_client_endpoint_response_endpoint_info_all_done(void);

	void app_client_endpoint_response_token(const std::string& token_type, const std::string& token_internal_type, const std::string& token_serial, const std::string& token_title);
	void app_client_endpoint_enroll_token(const std::string& token_serial, const std::string& token_public_key);
	void app_client_endpoint_token_status(void);
	void app_client_endpoint_get_enrollment_status(void);

	void app_client_tag_get_platform_response(const std::string& platform);

	void appl_client_set_connection_info(const std::string& servers);

	void app_client_dme_response_get_dme_values(const std::string& username, const std::string& password, const std::string& terminal_id, const std::string& device_signature, const std::string& device_time);
    void app_client_dme_response_get_dme_values_none(void);

    void close(void);
    bool is_closed(void);
    bool is_connected(void);


protected:

    /*! \brief Constructor
     */
    ApplClientSession(const Communication::AsyncService::APtr& async_service, const Utility::CheckpointHandler::APtr& checkpoint_handler, const Communication::Session::APtr& session, ApplClientEventhandler* eventhandler, ApplClientSessionEventhandler* session_eventhandler, const ApplClientKeyStore::APtr& key_store);

    void close_janitor_start(void);
    void close_janitor(void);


    Communication::AsyncService::APtr async_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;
    State state_;
    Communication::Session::APtr com_session_;
    ApplClientSessionEventhandler* session_eventhandler_;
    ApplClientEventhandler* appl_client_eventhandler_;
    boost::asio::deadline_timer close_janitor_timer_;
    ApplClientKeyStore::APtr key_store_;


    ApplClientSessionTunnelendpointAuth::APtr session_tunnelendpoint_auth_;
    ApplClientSessionTunnelendpointEndpoint::APtr session_tunnelendpoint_endpoint_;
    ApplClientSessionTunnelendpointUser::APtr session_tunnelendpoint_user_;
    ApplClientSessionTunnelendpointCPM::APtr session_tunnelendpoint_cpm_;
    ApplClientSessionTunnelendpointDialog::APtr session_tunnelendpoint_dialog_;
    ApplClientSessionTunnelendpointTag::APtr session_tunnelendpoint_tag_;
    ApplClientSessionTunnelendpointTraffic::APtr session_tunnelendpoint_traffic_;
};

}
}
#endif
