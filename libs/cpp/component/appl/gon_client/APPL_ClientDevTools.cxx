/*! \file APPL_ClientDevTools.cxx
 \brief This file contains the implementation for dev tools for appl client
 */
#include <iostream>
#include <sstream>

#include <lib/utility/UY_DataBuffer.hxx>
#include <component/appl/gon_client/APPL_ClientDevTools.hxx>

using namespace std;
using namespace Giritech::Appl;
using namespace boost;



/*
 * ------------------------------------------------------------------
 * ApplClientDevToolsNetworkLog implementation
 * ------------------------------------------------------------------
 */
ApplClientDevToolsNetworkLog::ApplClientDevToolsNetworkLog(const bool enabled, const bool log_body) :
	enabled_(enabled), log_body_(log_body), response_cached_(false) {
}

void ApplClientDevToolsNetworkLog::request_http(const std::string& method, const std::string& url, const std::string& headers) {
	request_method_ = method;
	request_url_ = url;
	request_headers_ = headers;
}

void ApplClientDevToolsNetworkLog::response_http(const bool cached, const long& status_code, const std::string& headers) {
	response_cached_ = cached;
	response_status_code_ = status_code;
	response_headers_ = headers;
}

void ApplClientDevToolsNetworkLog::response_http_body(const std::string& body) {
	response_body_ += body;
}

void ApplClientDevToolsNetworkLog::error(const std::string& error_message) {
	error_message_ = error_message;
}

bool ApplClientDevToolsNetworkLog::is_enabled(void) const {
	return enabled_;
}

std::string ApplClientDevToolsNetworkLog::dump(void) {
	stringstream ss;
	ss << "**Request**" << endl;
	ss << "method:" << request_method_ << endl;
	ss << "url   :" << request_url_ << endl;
	ss << request_headers_ << endl << endl;

	ss << "**Response**" << endl;
	if(response_cached_) {
		ss << "cached" << endl;
	}
	ss << "status:" << response_status_code_ << endl;
	ss << response_headers_ << endl << endl;
	ss << error_message_ << endl;

	if(log_body_) {
		ss << "*body-start*" << endl;
//		ss << Giritech::Utility::DataBufferManaged::create(response_body_)->encodeHex()->toString();
		ss << response_body_;
		ss << "*body-end*" << endl;
	}
	return ss.str();
}

ApplClientDevToolsNetworkLog::APtr ApplClientDevToolsNetworkLog::create(const bool enabled, const bool log_body) {
	return APtr(new ApplClientDevToolsNetworkLog(enabled, log_body));
}



