/*! \file UTest_APPL_Client.cxx
 * \brief This file contains unittest suite for the Appl Client functionality
 */
#include <string>

#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>

#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_Checkpoint_ALL.hxx>

#include <component/appl/gon_client/APPL_Client.hxx>
#include <component/appl/gon_client/APPL_ClientEventhandler.hxx>

using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::CryptFacility;
using namespace Giritech::Communication;
using namespace Giritech::Appl;

/*!
 *  Demo Appl client
 */
class UTestApplClient : public ApplClientEventhandler {
public:
    typedef boost::shared_ptr<UTestApplClient> APtr;

    void app_client_event_connecting(const std::string& connection_title) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_event_connecting: " << endl;
    	cout << connection_title << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    void app_client_event_connecting_failed(const std::string& message) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_event_connecting_failed: " << endl;
    	cout << message << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    void app_client_event_connected(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_event_connected" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    void app_client_event_closing(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_event_closing" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    void app_client_event_closed(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_event_closed" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_user_request_login(const std::string& msg, const bool change_password_enabled) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_user_request_login" << endl;
    	cout << msg << endl;
    	cout << change_password_enabled << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	appl_client_->app_client_user_response_login("bb", "bb", false);
    }

    virtual void app_client_auth_generate_keys_response(const std::string& private_key, const std::string& public_key) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_auth_generate_keys_response" << endl;
    	cout << private_key << endl;
    	cout << public_key << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_auth_request_serial(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_auth_request_serial" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_auth_request_challenge_response(const std::string& challenge) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_auth_request_challenge_response" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_dialog_show_license_info(const std::string& header, const std::string& message, const bool valid)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_show_license_info" << endl;
    	cout << header << endl;
    	cout << message << endl;
    	cout << valid << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }
    virtual void app_client_dialog_request_inform_on_first_access(const std::string&  message, const bool close_on_cancel) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_request_inform_on_first_access" << endl;
    	cout << message << endl;
    	cout << close_on_cancel << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_dialog_menu_updated(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_menu_updated" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }
    virtual void app_client_dialog_show_menu(const std::vector<ApplClientMenuItem>& menu_items, const std::map<std::string, ApplClientTagProperty>& tag_properties) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_show_menu" << endl;
    	std::vector<ApplClientMenuItem>::const_iterator i(menu_items.begin());
    	while(i != menu_items.end()) {
    		cout << (*i).id << ", " << (*i).title << ", " << (*i).launch_id << ", " << (*i).icon_id;
    		cout << " tags:";
        	std::vector<std::string>::const_iterator j((*i).tags.begin());
        	while(j != (*i).tags.end()) {
        		cout << *j << " ";
        		++j;
        	}
        	cout << endl;
    		++i;
    	}
    	cout << "tag properties:" << endl;
    	std::map<std::string, ApplClientTagProperty>::const_iterator h(tag_properties.begin());
    	while(h != tag_properties.end()) {
    		cout << (h->second).name << ", " << (h->second).auto_launch << ", " << (h->second).auto_launch_first << endl;
    		++h;
    	}
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

    	appl_client_->appl_client_dialog_auto_launch(menu_items, tag_properties, false);

    	std::vector<std::string> icon_ids;
    	icon_ids.push_back("mail");
    	icon_ids.push_back("intranet");
    	icon_ids.push_back("intranetxx");
    	appl_client_->app_client_dialog_get_images("request_id_1", icon_ids, "iphone", 128, 128, "png");

    	appl_client_->appl_client_dialog_auto_launch(menu_items, tag_properties, false);

 //    	appl_client_->app_client_dialog_menu_item_selected("5", false);
    }

    virtual void app_client_info(const std::string& header, const std::string& message)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_info" << endl;
    	cout << header << endl;
    	cout << message << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_warning(const std::string& header, const std::string& message) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_warning" << endl;
    	cout << header << endl;
    	cout << message << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_error(const std::string& header, const std::string& message) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_error" << endl;
    	cout << header << endl;
    	cout << message << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_endpoint_request_endpoint_info(void)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_endpoint_request_endpoint_info" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_endpoint_enrollment_request_token(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_endpoint_enrollment_request_token" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_endpoint_enrollment_token_status(const bool can_deploy, const bool auth_activated, const bool assigned_to_current_user, const std::string& status_text) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_endpoint_enrollment_token_status" << endl;
    	cout << can_deploy << endl;
    	cout << auth_activated << endl;
    	cout << assigned_to_current_user << endl;
    	cout << status_text << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_endpoint_enrollment_enroll_token_response(const bool rc, const std::string& msg) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_endpoint_enrollment_enroll_token_response" << endl;
    	cout << rc << endl;
    	cout << msg << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }
    virtual void app_client_endpoint_get_enrollment_status_response(const bool enroll_error, const bool enroll_done, const std::string& enroll_message) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_endpoint_get_enrollment_status_response" << endl;
    	cout << enroll_error << endl;
    	cout << enroll_done << endl;
    	cout << enroll_message << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

    }

    virtual void app_client_traffic_launch_status_changed(const std::string& launch_id, const ApplClientEventhandler::State state, const std::string& state_info) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_traffic_launch_status_changed" << endl;
    	cout << launch_id << endl;
    	cout << state << endl;
    	cout << state_info << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_traffic_launch_command(const std::string& launch_id, const ApplClientCommand& launch_command) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_traffic_launch_command" << endl;
    	cout << launch_id << endl;
    	cout << "type: " << launch_command.getCommandType() << endl;
    	cout << "url: " << launch_command.getCommandURL() << endl;
    	cout << "proxy_host: " << launch_command.getCommandProxyHost() << endl;
    	cout << "proxy_port: " << launch_command.getCommandProxyPort() << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_traffic_launch_command_terminate(const std::string& launch_id) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_traffic_launch_command_terminate" << endl;
    	cout << launch_id << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_dialog_image_found(const std::string& request_id, const std::string& image_id, const std::string& image_data) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_image_found" << endl;
    	cout << request_id << endl;
    	cout << image_id << endl;
    	cout << image_data << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_dialog_image_not_found(const std::string& request_id, const std::string& image_id) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dialog_image_not_found" << endl;
    	cout << request_id << endl;
    	cout << image_id << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_auth_failed(void)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_auth_failed" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_tag_get_platform(void)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_tag_get_platform" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_set_background_settings(const bool close_when_entering_background) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_set_background_settings" << endl;
    	cout << close_when_entering_background << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_cpm_update_connection_info(const std::string& servers) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_cpm_update_connection_info" << endl;
    	cout << servers << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual bool app_client_is_network_available(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_is_network_available" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	}

    virtual void app_client_key_store_reset(void) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_key_store_reset" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
	}

    virtual void app_client_key_store_session_key_pair_set(DataBufferManaged::APtr& sessionKeyPublic, DataBufferManaged::APtr& sessionKeyPrivate) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_key_store_session_key_pair_set" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_key_store_session_key_pair_get(DataBufferManaged::APtr& sessionKeyPublic, DataBufferManaged::APtr& sessionKeyPrivate) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_key_store_session_key_pair_get" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_version_error(const std::string& server_version) {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_version_error" << endl;
    	cout << server_version << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    virtual void app_client_dme_get_dme_value(void)  {
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    	cout << "UTestApplClient::app_client_dme_get_dme_value" << endl;
    	cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
    }

    static APtr create(const std::string& servers, const std::string& knownsecret) {
    	return APtr(new UTestApplClient(servers, knownsecret));
    }

private:
    UTestApplClient(const std::string& servers, const std::string& knownsecret) {
    	appl_client_ = ApplClient::create(this, servers, knownsecret);
    }

public:
    ApplClient::APtr appl_client_;
};


BOOST_AUTO_TEST_CASE( session_test )
{
    const char servers[] = "<?xml version='1.0'?>"
"<servers>"
"  <connection_group title='Default' selection_delay_sec='1'>"
"   <connection title='Direct-Connection' host='192.168.42.102' port='13945' timeout_sec='30' type='direct' />"
"  </connection_group>"
"</servers>";

    const char knownsecret[] = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F";

    UTestApplClient::APtr appl_client(UTestApplClient::create(servers, knownsecret));
    appl_client->appl_client_->async_start();
    appl_client->appl_client_->action_connect();


    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->app_client_dialog_menu_item_selected("5", false);

    /*
    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->app_client_dialog_menu_item_close("5");

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->app_client_dialog_menu_item_selected("5", false);

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->app_client_dialog_menu_item_close("5");
*/
    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(60));
    appl_client->appl_client_->action_close();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(5));
    appl_client->appl_client_->action_connect();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->action_close();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(5));
    appl_client->appl_client_->action_connect();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->action_close();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(10));
    appl_client->appl_client_->async_stop();

    boost::thread::sleep(boost::get_system_time() + boost::posix_time::seconds(2));

}

boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
#ifdef GIRITECH_COMPILEOPTION_CRYPTFACILITY_LINKING_DYNAMIC
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_fips);
#else
    CryptFacilityService::getInstance().initialize(CryptFacilityService::modeofoperation_unknown);
#endif
    return 0;
}
