/*! \file APPL_KeyStore.cxx
 \brief This file contains the implementation of class for handling key store
 */
#include <component/appl/gon_client/APPL_KeyStore.hxx>

using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Appl;


/*
 * ------------------------------------------------------------------
 * ApplClientKeyStore implementation
 * ------------------------------------------------------------------
 */
ApplClientKeyStore::ApplClientKeyStore(boost::asio::io_service& io, const CryptFacility::CryptFactory::APtr& cryptFactory)
	: KeyStore(io, cryptFactory),
	  eventhandler_(NULL),
	  background_thread_running_(false) {
}

ApplClientKeyStore::~ApplClientKeyStore(void) {
}

void ApplClientKeyStore::setEventhandler(ApplClientKeyStoreEventhandler* eventhandler) {
	eventhandler_ = eventhandler;
}

void ApplClientKeyStore::resetEventhandler(void) {
	eventhandler_ = NULL;
}

ApplClientKeyStore::APtr ApplClientKeyStore::create(boost::asio::io_service& io, const CryptFacility::CryptFactory::APtr& cryptFactory) {
	return APtr(new ApplClientKeyStore(io, cryptFactory));
}

void ApplClientKeyStore::resetSessionKeypair(void) {
	KeyStore::resetSessionKeypair();
	generateSessionKeypairBackgroundStart();
}

void ApplClientKeyStore::generateSessionKeypairBackgroundStart(void) {
	if(background_thread_running_) {
		return;
	}
	background_thread_running_ = true;
	background_thread_ = boost::thread(boost::bind(&ApplClientKeyStore::generateSessionKeypairBackground, this));
}

void ApplClientKeyStore::generateSessionKeypairBackground(void) {
	Utility::DataBufferManaged::APtr sessionKeyPublic;
	Utility::DataBufferManaged::APtr sessionKeyPrivate;
	generateSessionKeypair(sessionKeyPublic, sessionKeyPrivate);

	if(eventhandler_ != NULL) {
		eventhandler_->app_client_key_store_session_key_pair_set(sessionKeyPublic, sessionKeyPrivate);
	}
	background_thread_running_ = false;
}
