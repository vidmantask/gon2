/*! \file APPL_ClientCommand.hxx
 \brief This file contains the client command functionality
 */
#ifndef APPL_ClientCommand_HXX
#define APPL_ClientCommand_HXX


#include <string>


#include <component/communication/COM_TunnelendpointDirect.hxx>


namespace Giritech {
namespace Appl {


/*
 */
class ApplClientDirectConnectionFactoryEventhandler {
public:
	virtual void direct_connection_factory_connection_ready(const Giritech::Communication::TunnelendpointDirect::APtr& connection) = 0;
	virtual void direct_connection_factory_connection_failed(const std::string& errorMessage) = 0;
};


class ApplClientDirectConnectionFactory {
public:
	typedef boost::shared_ptr<ApplClientDirectConnectionFactory> APtr;

	void set_eventhandler(ApplClientDirectConnectionFactoryEventhandler* eventhandler);
	void reset_eventhandler(void);

	void create_connection();

	void connection_ready(const Giritech::Communication::TunnelendpointDirect::APtr& connection);
	void connection_failed(const std::string& errorMessage);

	Giritech::Utility::CheckpointHandler::APtr get_checkpoint_handler(void);
	boost::asio::io_service& get_io_service(void);
	Giritech::Utility::Mutex::APtr get_mutex(void);

	static ApplClientDirectConnectionFactory::APtr create(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			const Giritech::Communication::TunnelendpointDirectTunnelClient::APtr& direct_tunnelendpoint);


private:
	ApplClientDirectConnectionFactory(
			const Giritech::Utility::CheckpointHandler::APtr& checkpoint_handler,
			const Giritech::Communication::TunnelendpointDirectTunnelClient::APtr& direct_tunnelendpoint);

	Giritech::Communication::TunnelendpointDirectTunnelClient::APtr direct_tunnelendpoint_;

	const Giritech::Utility::CheckpointHandler::APtr checkpoint_handler_;

	ApplClientDirectConnectionFactoryEventhandler* eventhandler_;
};




/*
 */
class ApplClientCommand {
public:
    enum CommandType {
        CommandType_SecureBrowser = 0,
        CommandType_Invalid,
        CommandType_None
    };

    /*! \brief Constructor
     *
     * browse url
     *
     * browse_proxy_socks proxyHost:proxyPort url
     *
     */
    ApplClientCommand(void);
    ApplClientCommand(const std::string& command);
    ApplClientCommand(const std::string& command, ApplClientDirectConnectionFactory::APtr direct_connection_factory);


    /*! \brief getters
     */
    ApplClientDirectConnectionFactory::APtr getDirectConnectionFactory(void) const;


    CommandType getCommandType(void) const;
    std::string  getCommandProxyHost(void) const;
    unsigned long  getCommandProxyPort(void) const;
    std::string  getCommandURL(void) const;

    enum SkinType {
        SkinType_Browser = 0,
        SkinType_AppFullscreen,
        SkinType_AppPopup,
    };
    SkinType getSkinType(void) const;

    enum CacheType {
        CacheType_On = 0,
        CacheType_Off,
        CacheType_ClearOnLaunch
    };
    CacheType getCacheType(void) const;

    bool getVisibility(void) const;
    bool getSkinBackButton(void) const;
    bool getSkinProgressBar(void) const;


    enum OrientationLockType {
    	OrientationLockType_Disabled = 0,
    	OrientationLockType_SmallLandscape,
    	OrientationLockType_SmallPortrait,
    	OrientationLockType_NormalLandscape,
    	OrientationLockType_NormalPortrait,
    	OrientationLockType_LargeLandscape,
    	OrientationLockType_LargePortrait,
    	OrientationLockType_XLargeLandscape,
    	OrientationLockType_XLargePortrait
    };

    enum OrientationType {
    	OrientationType_Landscape = 0,
    	OrientationType_Portraite
    };

    enum ChangeOrientationType {
    	ChangeOrientationLock_NoLock = 0,
    	ChangeOrientationLock_Lock,
    	ChangeOrientationLock_RotateAndLock
    };
    ChangeOrientationType doChangeOrientation(const unsigned int& x, const unsigned int& y, const unsigned int& ppi, const OrientationType current_orientation) const;


    enum HandleSchemeType {
    	HandleSchemeTypeDoNotHandle = 0,
    	HandleSchemeTypeHandle,
    	HandleSchemeTypeBlock
    };
    HandleSchemeType getHandleSchemeHTTPS(void) const;

    std::string getLastError(void) const;


private:
    void parse_command(void);
    void reset(void);
    void reset_invalid(void);

    SkinType parse_skin(const std::string& skin_value);
    CacheType parse_cache(const std::string& value);
    bool parse_on_off(const std::string& value);

    OrientationLockType parse_orientation_lock(const std::string& value);
    std::set<OrientationLockType> parse_orientation_locks(const std::string& value);

    HandleSchemeType parse_shandle_scheme(const std::string& value);

    std::string command_;
    CommandType command_type_;
    std::string command_proxy_host_;
    unsigned long command_proxy_port_;
    std::string command_url_;

    bool command_skin_back_button_;
    bool command_skin_progress_bar_;
    bool command_visibility_;

    std::set<OrientationLockType> command_orientation_locks_;

    ApplClientDirectConnectionFactory::APtr direct_connection_factory_;

    SkinType command_skin_type_;
    CacheType command_cache_type_;

    HandleSchemeType command_handle_scheme_https_;

    std::string command_last_error_;
};

}
}
#endif
