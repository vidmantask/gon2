/*! \file APPL_CheckpointAttr.hxx
 \brief This file contains the global singelton for the module attribute communication
 */
#ifndef APPL_CHECKPOINTATTR_HXX
#define APPL_CHECKPOINTATTR_HXX

#include <lib/utility/UY_Checkpoint.hxx>

namespace Giritech {
namespace Appl {

Utility::CheckpointAttr_Module::APtr Attr_ApplClient(void);
Utility::CheckpointAttr_Module::APtr Attr_iDevice(void);

}
}
#endif
