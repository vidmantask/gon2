/*! \file UTest_HttpClient.cxx
 \brief This file contains unittest suite for the HttpClienty
 */
#include <boost/test/unit_test.hpp>

#include <lib/utility/UY_Checkpoint_ALL.hxx>


#include <component/http_client/HttpClient.hxx>
#include <component/http_client/HttpClientComDirect.hxx>
#include <component/http_client/HttpCookieStore.hxx>
#include <component/communication/COM_AsyncService.hxx>


using namespace std;
using namespace Giritech::Utility;
using namespace Giritech::HttpClient;


CheckpointHandler::APtr checkpoint_handler(CheckpointHandler::create_cout_all());


BOOST_AUTO_TEST_CASE( test_request ) {

	HttpRequest::APtr http_request_001(HttpRequest::create_get("http://www.somehost.com/path/file.html"));

	stringstream ss_001_a;
	http_request_001->serialize(ss_001_a, false);
	string result_001_a = "GET /path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com" "\r\n" "\r\n";
    BOOST_CHECK( result_001_a == ss_001_a.str() );

	stringstream ss_001_b;
	http_request_001->serialize(ss_001_b, true);
	string result_001_b = "GET http://www.somehost.com/path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com" "\r\n" "\r\n";
    BOOST_CHECK( result_001_b == ss_001_b.str() );


	HttpRequest::APtr http_request_002(HttpRequest::create_get("http://www.somehost.com:8080/path/file.html"));
	stringstream ss_002_a;
	http_request_002->serialize(ss_002_a, false);
	string result_002_a = "GET /path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com:8080" "\r\n" "\r\n";
    BOOST_CHECK( result_002_a == ss_002_a.str() );

	stringstream ss_002_b;
	http_request_002->serialize(ss_002_b, true);
	string result_002_b = "GET http://www.somehost.com:8080/path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com:8080" "\r\n" "\r\n";
    BOOST_CHECK( result_002_b == ss_002_b.str() );

    BOOST_CHECK( http_request_002->get_host() == "www.somehost.com");
    BOOST_CHECK( http_request_002->get_port() == 8080);
}

BOOST_AUTO_TEST_CASE( test_request_with_headers ) {

	HttpRequest::APtr http_request_001(HttpRequest::create_get("http://www.somehost.com/path/file.html"));
	http_request_001->add_header("A", "BB", false);
	http_request_001->add_header("C", "DD", false);
	stringstream ss_001_a;
	http_request_001->serialize(ss_001_a, false);
	string result_001_a = "GET /path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com" "\r\n" "A: BB" "\r\n" "C: DD" "\r\n" "\r\n";
    BOOST_CHECK( result_001_a == ss_001_a.str() );

}

BOOST_AUTO_TEST_CASE( test_request_with_body ) {

	HttpRequest::APtr http_request_001(HttpRequest::create_post("http://www.somehost.com/path/file.html"));
	http_request_001->set_body("Hej med dig");
	stringstream ss_001_a;
	http_request_001->serialize(ss_001_a, false);
	string result_001_a = "POST /path/file.html HTTP/1.1" "\r\n" "Host: www.somehost.com" "\r\n" "Content-Length: 11" "\r\n" "\r\n" "Hej med dig";
    BOOST_CHECK( result_001_a == ss_001_a.str() );

    BOOST_CHECK( http_request_001->get_host() == "www.somehost.com");
    BOOST_CHECK( http_request_001->get_port() == 80);

}


class TestHttpClientEventhandler: public HttpClientEventhandler {
public:
	bool is_done;
	Giritech::Utility::DataBufferManaged::APtr body;

	void http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const std::vector< std::pair<std::string, std::string> >& headers)  {
		cout << "TestHttpClientEventhandler::http_client_response_header header_status_code:" << header_status_code << endl;
		is_done = false;
		body = Giritech::Utility::DataBufferManaged::create("");
	}

	void http_client_response_body_data(Giritech::Utility::DataBufferManaged::APtr& body_data)  {
		cout << "TestHttpClientEventhandler::http_client_response_body_data" << endl;
		cout << "----------------" << endl;
		cout << body_data->toString() << endl;
		cout << "================" << endl;
		body = body_data;
	}

	void http_client_response_done(void) {
		cout << "TestHttpClientEventhandler::http_client_response_done" << endl;
		is_done = true;
	}

	void http_client_response_error(void) {
		cout << "TestHttpClientEventhandler::http_client_response_error" << endl;
		is_done = true;
	}
};


BOOST_AUTO_TEST_CASE( test_request_gzip ) {
    boost::thread_group threads;

	Giritech::Communication::AsyncService::APtr async_service(Giritech::Communication::AsyncService::create(checkpoint_handler));

	TestHttpClientEventhandler testHttpClientEventhandler;

	HttpCookieStore::APtr http_cookie_store(HttpCookieStore::create(async_service->get_io_service(), checkpoint_handler));

	HttpClientCom::APtr http_client_com(HttpClientComDirect::create(async_service->get_io_service(), checkpoint_handler));
	HttpClient::APtr http_client(HttpClient::create(async_service->get_io_service(), checkpoint_handler, http_client_com, http_cookie_store));
	http_client->set_eventhandler(&testHttpClientEventhandler);

//	HttpRequest::APtr http_request_001(HttpRequest::create_get("http://www.dr.dk/"));
	HttpRequest::APtr http_request_001(HttpRequest::create_get("http://127.0.0.1:18075/com.excitor.dme.jsapi/test_gzip?data=hej"));
	http_client->execute_request(http_request_001);

    threads.create_thread(boost::bind(&Giritech::Communication::AsyncService::run, async_service));

//    boost::this_thread::sleep(boost::posix_time::seconds(20));

    while(!testHttpClientEventhandler.is_done) {
    	boost::this_thread::sleep(boost::posix_time::seconds(1));
    	cout << "Waiting" << endl;
    }
    BOOST_CHECK( testHttpClientEventhandler.body->toString() == "hej");
}


BOOST_AUTO_TEST_CASE( test_parse_set_cookies ) {
	Giritech::Communication::AsyncService::APtr async_service(Giritech::Communication::AsyncService::create(checkpoint_handler));

	HttpCookieStore::APtr http_cookie_store(HttpCookieStore::create(async_service->get_io_service(), checkpoint_handler));

	http_cookie_store->add_cookie("cookie_basis=\"2012-05-14_14:18:54.811130\"", "/", "www.excitor.com");
	http_cookie_store->add_cookie("cookie_httponly=\"2012-05-14_14:18:54.811130\"; httponly", "/", "www.excitor.com");
	http_cookie_store->add_cookie("cookie_secure=\"2012-05-14_14:18:54.811130\"; secure", "/", "www.excitor.com");
	http_cookie_store->add_cookie("cookie_with_domain=\"2012-05-14_14:18:54.811130\"; Domain=.excitor.com", "/", "www.excitor.com");
	http_cookie_store->add_cookie("cookie_with_domain_and_path=\"2012-05-14_14:18:54.811130\"; Domain=.excitor.com; Path=/test", "/", "www.excitor.com");
	http_cookie_store->add_cookie("cookie_with_domain_and_path=\"2012-05-14_14:18:54.811130\"; Domain=.excitor.com; Path=/test; httponly", "/", "www.excitor.com");

	http_cookie_store->dump(cout);




}





boost::unit_test::test_suite* init_unit_test_suite(int, char* []) {
	return 0;
}
