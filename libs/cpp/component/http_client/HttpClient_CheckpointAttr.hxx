/*! \file HttpClient_CheckpointAttr.hxx
 \brief This file contains the global singelton for the module attribute HttpClient
 */
#ifndef HttpClient_CheckpointAttr_hxx
#define HttpClient_CheckpointAttr_hxx

#include <lib/utility/UY_Checkpoint.hxx>

namespace Giritech {
namespace HttpClient {

Utility::CheckpointAttr_Module::APtr Attr_HttpClient(void);

}
}
#endif
