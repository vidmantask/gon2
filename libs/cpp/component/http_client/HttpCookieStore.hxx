/*! \file HttpCookieStore.hxx
 *
 * \brief This file contains the http cookie store functionality
 *
 */
#ifndef Component_HttpCookieStore_hxx
#define Component_HttpCookieStore_hxx

#include <ostream>
#include <boost/shared_ptr.hpp>


#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_AsyncService.hxx>



namespace Giritech {
namespace HttpClient {




/*! \brief HttpSetCookieValue
 *
 */
struct HttpSetCookieValue {
	std::string key;
	std::string value;
	std::string path;
	std::string domain;
	std::string expires;
	std::string max_age;
	std::string version;
	bool httponly;
	bool secure;

	HttpSetCookieValue(const std::string& default_path, const std::string& default_doman);
	bool operator==(const HttpSetCookieValue& other) const;

};



/*! \brief This class define the abstract eventhandler interface for a HttpCookieStore.
 *
 */
class HttpCookieStoreEventhandler: public boost::noncopyable {
public:
	virtual void cookie_added(const HttpSetCookieValue& cookie_value) = 0;
	virtual void cookie_removed(const HttpSetCookieValue& cookie_value) = 0;
};



/*! \brief http cookie store
 *
 */
class HttpCookieStore {
public:
    typedef boost::shared_ptr<HttpCookieStore> APtr;

    /*
     * Destructor
     */
    ~HttpCookieStore(void);


    /*
     * Add 'Set-Cookie' value to cookie store from a http response header
     */
    void add_cookie(const std::string& value, const std::string& default_path, const std::string& default_doman);

    /*
     * Calculate cookies for a http request for a given host and path
     */
    void calc_cookies(const std::string& host, const std::string& path, std::vector<std::string>& header_cookie_values);

    /*
     * Dump cookie store (for debug)
     */
    void dump(std::ostream& os);

    /*
     * Set/reset eventhandler
     */
    void set_eventhandler(HttpCookieStoreEventhandler* eventhandler);
    void reset_eventhandler(void);

    /*
     * Create instance
     */
    static APtr create(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);


private:
    /*
     * Constructor
     */
    HttpCookieStore(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);


    bool verify_cookie(const HttpSetCookieValue& http_set_cookie_value, const std::string& default_doman);
    void remove_existing_cookie(const HttpSetCookieValue& new_cookie);

    boost::asio::io_service& io_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;

    std::vector<HttpSetCookieValue> cookie_store_;

    HttpCookieStoreEventhandler* eventhandler_;
};



}
}
#endif
