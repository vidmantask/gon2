/*! \file HttpClientComDirect.hxx
 *
 * \brief This file contains the http client communication Direct version
 *
 */
#ifndef Component_HttpClientComDirect_hxx
#define Component_HttpClientComDirect_hxx



#include <lib/utility/UY_CheckpointHandler.hxx>
#include <component/communication/COM_AsyncService.hxx>


#include "HttpClientCom.hxx"

#include <component/communication/COM_RawTunnelendpointConnector.hxx>
#include <component/communication/COM_RawTunnelendpointConnectorEventhandler.hxx>

#include <component/communication/COM_RawTunnelendpoint.hxx>
#include <component/communication/COM_RawTunnelendpointEventhandler.hxx>

namespace Giritech {
namespace HttpClient {



/*! \brief http client com direct
 *
 */
class HttpClientComDirect : public HttpClientCom, public Giritech::Communication::RawTunnelendpointConnectorTCPEventhandler, public Giritech::Communication::RawTunnelendpointEventhandler {
public:
    typedef boost::shared_ptr<HttpClientComDirect> APtr;

    /*
     * Destructor
     */
    ~HttpClientComDirect(void);

    /*
     * Create instance
     */
    static APtr create(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    virtual void connect(const std::string& host, const unsigned long port);
    virtual void write(const Utility::DataBufferManaged::APtr& data);
    virtual void close(void);
    virtual bool is_closed(void);


    /*
     * Implementation of RawTunnelendpointConnectorTCPEventhandler
     */
    virtual void com_tunnelendpoint_connected(Giritech::Communication::RawTunnelendpointTCP::APtr& tunnelendpoint);
    virtual void com_tunnelendpoint_connection_failed_to_connect(const std::string& messag);


    /*
     * Implementation of RawTunnelendpointEventhandler
     */
    virtual void com_raw_tunnelendpoint_close(const unsigned long& connection_id);
    virtual void com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id);
    virtual void com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data);
    virtual void com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id);



private:
    /*
     * Constructor
     */
    HttpClientComDirect(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);


    void goto_closed_state(void);


    Giritech::Communication::RawTunnelendpointConnectorTCP::APtr connector_;
    Giritech::Communication::RawTunnelendpointTCP::APtr tunnelendpoint_;

    enum State {
        State_ReadyToConnect,
        State_Connecting,
        State_Ready,
        State_Bussy,
        State_Closing,
        State_Closed
    };
    State state_;

};


}
}
#endif
