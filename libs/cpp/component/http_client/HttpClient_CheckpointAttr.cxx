/*! \file HttpClient_CheckpointAttr.cxx
 \brief This file contains the global singleton for the module attribute communication
 */

#include "HttpClient_CheckpointAttr.hxx"

using namespace Giritech;
using namespace Giritech::Utility;


static CheckpointAttr_Module::APtr componentConnection_;

CheckpointAttr_Module::APtr Giritech::HttpClient::Attr_HttpClient(void) {
    if (!componentConnection_) {
        componentConnection_ = CheckpointAttr_Module::create("HttpClient");
    }
    return componentConnection_;
}
