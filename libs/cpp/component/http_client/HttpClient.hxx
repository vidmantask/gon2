/*! \file HttpClient.hxx
 *
 * \brief This file contains the http client functionality
 *
 */
#ifndef Component_HttpClient_hxx
#define Component_HttpClient_hxx

#include <map>
#include <ostream>
#include <iostream>

#include <boost/shared_ptr.hpp>



#include <lib/utility/UY_DataBuffer.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <lib/utility/UY_AsyncMemGuard.hxx>

#include <component/communication/COM_AsyncService.hxx>


#include "HttpClientCom.hxx"
#include "HttpClientComEventhandler.hxx"
#include "HttpCookieStore.hxx"



namespace Giritech {
namespace HttpClient {


typedef std::vector< std::pair<std::string, std::string> > HeadersType;



/*
 *  Exception throw in case of errors
 */
class ExceptionHttpClient : public Giritech::Utility::Exception {
public:
	ExceptionHttpClient(const std::string& message) :
        Exception(message) {
    }
    virtual ~ExceptionHttpClient(void) throw() {
    }
};



/*! \brief Http response eventhandler interface
 *
 */
class HttpResponseEventhandler: public boost::noncopyable {
public:
	virtual void http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const HeadersType& headers) = 0;
	virtual void http_client_response_body_data(Utility::DataBufferManaged::APtr& body_data) = 0;
	virtual void http_client_response_done(void) = 0;
	virtual void http_client_response_error(void) = 0;
};



/*! \brief http request
 *
 */
class HttpRequest {
public:
    typedef boost::shared_ptr<HttpRequest> APtr;


    /*
     * Destructor
     */
    ~HttpRequest(void);

    void set_headers(const HeadersType& headers);
    void add_header(const std::string& key, const std::string& value, const bool overwrite);

    void set_body(const std::string& body);

    std::string get_url(void);
    void enable_https(void);

    void serialize(std::ostream& os, const bool http_proxification);


    std::string get_host(void) const;
    unsigned long get_port(void) const;
    std::string get_path(void) const;

    boost::uint64_t get_request_id(void) const;

    void set_response_eventhandler(HttpResponseEventhandler* http_response_eventhandler);
    HttpResponseEventhandler* get_response_eventhandler(void);

    void dump(std::ostream& os);


    void add_cookies(const HttpCookieStore::APtr cookie_store);


    /*
     * Create instance
     */
    static APtr create_get(const std::string& url);
    static APtr create_post(const std::string& url);


private:
    /*
     * Constructor
     */
    HttpRequest(const std::string& method, const std::string& url);




    std::string method_;
    std::string url_;
    HeadersType headers_;
    std::string body_;

    HttpResponseEventhandler* http_response_eventhandler_;

    boost::uint64_t request_id_;

    bool https_enabled_;
};







/*! \brief http response
 *
 */
class HttpResponse {
public:
    typedef boost::shared_ptr<HttpResponse> APtr;

    /*
     * Destructor
     */
    ~HttpResponse(void);


    void read_some(const Utility::DataBuffer::APtr& data);

    void dump(std::ostream& os);

    unsigned long get_content_length_handled(void);

    /*
     * Create instance
     */
    static APtr create(const HttpRequest::APtr& request, HttpResponseEventhandler* http_response_eventhandler);


private:
    /*
     * Constructor
     */
    HttpResponse(const HttpRequest::APtr& request, HttpResponseEventhandler* http_response_eventhandler);

    bool parse(void);
    void parse_status(void);
    void parse_headers(void);
    void parse_body(void);
    void parse_body_by_length(void);
    bool parse_body_chunked(void);

    void enter_parse_state_done(void);


    enum CompressType {
    	CompressType_none = 0,
    	CompressType_gzip = 1,
    	CompressType_deflate,
    	CompressType_deflate_no_header
    };
    CompressType compress_type_;

    void parse_body_data_ready(Utility::DataBufferManaged::APtr& data);
    void initilize_and_enable_compressed_body_handling(const CompressType& compress_type);
    void finish_compressed_body_handling(void);

    HttpRequest::APtr request_;
    Utility::DataBufferManaged::APtr pending_data_;

    enum ParseState {
    	ParseState_Status = 0,
    	ParseState_Headers,
    	ParseState_Body,
    	ParseState_Done
    };
    ParseState parse_state_;

    enum HttpContentLengthMethod {
        HttpContentLengthMethod_Unknown = 0,
        HttpContentLengthMethod_Chunked,
        HttpContentLengthMethod_ByLength,
        HttpContentLengthMethod_Missing
    };
    HttpContentLengthMethod http_content_length_method_;


    std::string header_version_;
    unsigned long header_status_code_;
    std::string header_status_message_;

    HeadersType headers_;
    unsigned long body_content_length_;
    unsigned long body_content_length_handled_;


    HttpResponseEventhandler* http_response_eventhandler_;

    Utility::DataBufferManaged::APtr http_body_compressed_data_;


};




/*! \brief http client eventhandler interface
 *
 */
class HttpClientEventhandler: public HttpResponseEventhandler {
public:

};


/*! \brief http client
 *
 */
class HttpClient : public Giritech::Utility::AsyncMemGuardElement, public HttpClientComEventhandler, public HttpResponseEventhandler {
public:
    typedef boost::shared_ptr<HttpClient> APtr;

    /*
     * Destructor
     */
    ~HttpClient(void);


    /*! \brief Implementation of AsyncMemGuardElement virtual methods
     */
    bool async_mem_guard_is_done(void);
    virtual std::string async_mem_guard_get_name(void);


    void execute_request(const HttpRequest::APtr& request);
    void cancel_request(void);


    void set_eventhandler(HttpClientEventhandler* client_eventhandler);
    void reset_eventhandler(void);

    void set_mutex(const Utility::Mutex::APtr& mutex);


    /*
     * Implementation of HttpClientComEventhandler
     */
	void http_client_com_connected(void);
	void http_client_com_closed(void);
	void http_client_com_closed_with_error(const std::string& error);
	void http_client_com_read_some(const Utility::DataBuffer::APtr& data);


    /*
     * Implementation of HttpResponseEventhandler
     */
	void http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const HeadersType& headers);
	void http_client_response_body_data(Utility::DataBufferManaged::APtr& body_data);
	void http_client_response_done(void);
	void http_client_response_error(void);


    /*
     * Create instance
     */
    static APtr create(
    		boost::asio::io_service& io_service,
            const Utility::CheckpointHandler::APtr& checkpoint_handler,
            const HttpClientCom::APtr& http_client_com,
            const HttpCookieStore::APtr& cookie_store);


private:
    /*
     * Constructor
     */
    HttpClient(
    		boost::asio::io_service& io_service,
            const Utility::CheckpointHandler::APtr& checkpoint_handler,
            const HttpClientCom::APtr& http_client_com,
            const HttpCookieStore::APtr& cookie_store);

    void goto_done_state(void);

    void connect_timeout(const boost::system::error_code& error);
    void read_timeout(const boost::system::error_code& error);

    void http_client_response_header_common(const std::string& header_version, const unsigned long header_status_code, const HeadersType& headers);
    void http_client_response_done_common(void);


    boost::asio::io_service& io_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;

    HttpClientCom::APtr http_client_com_;

    HttpRequest::APtr current_request_;
    HttpResponse::APtr current_response_;

    HttpClientEventhandler* client_eventhandler_;

    enum State {
    	State_Ready,
    	State_Connecting,
    	State_Bussy,
    	State_Closing,
    	State_Done
    };
    State state_;

    unsigned int connect_pending_;

    boost::asio::deadline_timer connect_timeout_timer_;
    unsigned int connect_timeout_pending_;

    boost::asio::deadline_timer read_timeout_timer_;
    unsigned int read_timeout_pending_;

    Utility::Mutex::APtr mutex_;


    HttpCookieStore::APtr cookie_store_;

    bool logging_enabled_;

    std::string body_;
};

}
}
#endif
