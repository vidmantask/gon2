/*! \file HttpClientComDirect.cxx
*/

#include <component/http_client/HttpClientComDirect.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::HttpClient;


/*
 * ------------------------------------------------------------------
 * HttpClientComDirect implementation
 * ------------------------------------------------------------------
 */
HttpClientComDirect::HttpClientComDirect(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: HttpClientCom(io_service, checkpoint_handler), state_(State_ReadyToConnect) {
}

HttpClientComDirect::~HttpClientComDirect(void) {
}

HttpClientComDirect::APtr HttpClientComDirect::create(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new HttpClientComDirect(io_service, checkpoint_handler));
}

void HttpClientComDirect::connect(const std::string& host, const unsigned long port)  {
	switch(state_) {
	case State_ReadyToConnect:
		break;
	default:
		assert(false);
		return;
	}
	state_ = State_Connecting;
	connector_ = RawTunnelendpointConnectorTCP::create(checkpoint_handler_, io_service_, host, port, this);
	connector_->aio_connect_start();
}

bool HttpClientComDirect::is_closed(void) {
	return state_ == State_Closed;
}

void HttpClientComDirect::write(const Utility::DataBufferManaged::APtr& data) {
	switch(state_) {
	case State_Ready:
		break;
	default:
		return;
	}
	tunnelendpoint_->aio_write_start(data);
}

void HttpClientComDirect::close(void) {
	switch(state_) {
	case State_Connecting:
	case State_Ready:
		break;
	default:
		return;
	}

	if(state_ == State_Connecting) {
		state_ = State_Closing;
		connector_->close();
	}

	if(state_ == State_Ready) {
		state_ = State_Closing;
		tunnelendpoint_->aio_close_start(false);
	}

	state_ = State_Closing;
	goto_closed_state();
}

void HttpClientComDirect::com_tunnelendpoint_connected(Giritech::Communication::RawTunnelendpointTCP::APtr& tunnelendpoint) {
	switch(state_) {
	case State_Connecting:
		break;
	default:
		assert(false);
		return;
	}
	state_ = State_Ready;

	tunnelendpoint_ = tunnelendpoint;
	tunnelendpoint_->set_mutex(mutex_);
	tunnelendpoint_->set_eventhandler(this);

	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_connected();
	}
	tunnelendpoint_->aio_read_start();
}

void HttpClientComDirect::com_tunnelendpoint_connection_failed_to_connect(const std::string& messag) {
	switch(state_) {
	case State_Connecting:
	case State_Closing:
		break;
	default:
		assert(false);
		return;
	}
	state_ = State_Closed;
	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_closed_with_error(message);
	}
}

void HttpClientComDirect::com_raw_tunnelendpoint_close(const unsigned long& connection_id) {
	switch(state_) {
	case State_Ready:
	case State_Closing:
		break;
	default:
		return;
	}
	close();

	if(state_ == State_Closing) {
		goto_closed_state();
	}

}

void HttpClientComDirect::com_raw_tunnelendpoint_close_eof(const unsigned long& connection_id) {
	switch(state_) {
	case State_Ready:
	case State_Closing:
		break;
	default:
		return;
	}
	close();


	if(state_ == State_Closing) {
		goto_closed_state();
	}
}

void HttpClientComDirect::com_raw_tunnelendpoint_read(const unsigned long& connection_id, const Utility::DataBuffer::APtr& data) {
	switch(state_) {
	case State_Ready:
	case State_Closing:
		break;
	default:
		return;
	}

	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_read_some(data);
	}


	if(state_ == State_Ready) {
		tunnelendpoint_->aio_read_start();
	}


	if(state_ == State_Closing) {
		goto_closed_state();
	}
}

void HttpClientComDirect::com_raw_tunnelendpoint_write_buffer_empty(const unsigned long& connection_id) {
}

void HttpClientComDirect::goto_closed_state(void) {
	switch(state_) {
	case State_Closing:
		break;
	default:
		return;
	}


	if(tunnelendpoint_.get() && !(tunnelendpoint_->is_closed())) {
		return;
	}


	state_ = State_Closed;
	if(com_eventhandler_) {
		com_eventhandler_->http_client_com_closed();
	}
}
