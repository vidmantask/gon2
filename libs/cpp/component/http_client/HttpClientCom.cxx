/*! \file HttpClientCom.cxx
*/
#include <component/http_client/HttpClientCom.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::HttpClient;




/*
 * ------------------------------------------------------------------
 * HttpClientCom implementation
 * ------------------------------------------------------------------
 */
HttpClientCom::HttpClientCom(
		boost::asio::io_service& io_service,
        const CheckpointHandler::APtr& checkpoint_handler) :
        io_service_(io_service),
        checkpoint_handler_(checkpoint_handler),
        com_eventhandler_(NULL),
        http_proxification_(false),
        mutex_(Mutex::create(io_service, "HttpClientCom")) {
}

HttpClientCom::~HttpClientCom(void) {
}

void HttpClientCom::reset_eventhandler(void) {
	com_eventhandler_ = NULL;
}

void HttpClientCom::set_eventhandler(HttpClientComEventhandler* com_eventhandler) {
	com_eventhandler_ = com_eventhandler;
}

void HttpClientCom::enable_http_proxification(void) {
	http_proxification_ = true;
}

bool HttpClientCom::get_http_proxification(void) {
	return http_proxification_;
}

void HttpClientCom::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
}


