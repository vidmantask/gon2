/*! \file HttpClientEventhandler.hxx
 *  \brief This file contains abstract interface HttpClientEventhandler
 */
#ifndef COM_HttpClientEventhandler_HXX
#define COM_HttpClientEventhandler_HXX


namespace Giritech {
namespace HttpClient {

/*! \brief This class define the abstract eventhandler interface for a HttpClient.
 *
 */
class HttpClientEventhandler: public boost::noncopyable {
public:

};

}
}
#endif
