/*! \file HttpCookieStore.cxx
 \brief This file contains the implementation for the http cookie store functionality
 */
#include <boost/algorithm/string.hpp>


#include <component/http_client/HttpCookieStore.hxx>
#include <component/http_client/HttpClient_CheckpointAttr.hxx>


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;
using namespace Giritech::HttpClient;


//namespace qi = boost::spirit::qi;
//namespace ascii = boost::spirit::ascii;
//namespace phoenix = boost::phoenix;



/*
 * Parser for HttpSetCookieValue
 *
 * Set-Cookie: cookie_basis="2012-05-14_14:18:54.811130"
 * Set-Cookie: cookie_httponly="2012-05-14_14:18:54.811130"; httponly
 * Set-Cookie: cookie_secure="2012-05-14_14:18:54.811130"; secure
 * Set-Cookie: cookie_with_domain="2012-05-14_14:18:54.811130"; Domain=.excitor.com
 * Set-Cookie: cookie_with_domain_and_path="2012-05-14_14:18:54.811130"; Domain=.excitor.com; Path=/test
 *
 */
bool parse_http_set_cookie_value(const std::string& value, HttpSetCookieValue& http_set_cookie_value) {
	std::vector<std::string> value_elements;
	boost::split(value_elements, value, boost::algorithm::is_any_of(";"), boost::token_compress_on);

	std::vector<std::string>::const_iterator i(value_elements.begin());
	while(i != value_elements.end()) {
		std::string key;
		std::string value;

		string value_element(boost::trim_copy(*i));
		std::vector<std::string> value_element_elements;
		boost::split(value_element_elements, value_element, boost::algorithm::is_any_of("="), boost::token_compress_on);

		std::vector<std::string>::const_iterator j(value_element_elements.begin());
		if(j != value_element_elements.end()) {
			key = *j;
			j++;
		}
		if(j != value_element_elements.end()) {
			value = *j;
		}

		string key_norm = boost::to_lower_copy(boost::trim_copy(key));
		if(key_norm == "httponly") {
			http_set_cookie_value.httponly = true;
		}
		else if(key_norm == "secure") {
			http_set_cookie_value.secure = true;
		}
		else if(key_norm == "path") {
			http_set_cookie_value.path = value;
		}
		else if(key_norm == "domain") {
			http_set_cookie_value.domain = value;
		}
		else if(key_norm == "expires") {
			http_set_cookie_value.expires = value;
		}
		else if(key_norm == "max-age") {
			http_set_cookie_value.max_age = value;
		}
		else if(key_norm == "version") {
			http_set_cookie_value.version = value;
		}
		else {
			if(i == value_elements.begin()) {
				http_set_cookie_value.key = key;
				http_set_cookie_value.value = value;
			}
		}
		i++;
	}
	return true;
}


/*
 * ------------------------------------------------------------------
 * HttpSetCookieValue implementation
 * ------------------------------------------------------------------
 */
HttpSetCookieValue::HttpSetCookieValue(const std::string& default_path, const std::string& default_doman) {
	key = "";
	value = "";
	path = default_path;
	domain = default_doman;
	expires = "";
	max_age = "";
	httponly = false;
	secure = false;
	version = "";
}

bool HttpSetCookieValue::operator==(const HttpSetCookieValue& other) const {
	return (key == other.key) && (path == other.path) && (domain == other.domain);

}

std::ostream& operator<<(std::ostream &out, const HttpSetCookieValue& value) {
	out << "key: " << value.key << ", ";
	out << "value: " << value.value << ", ";
	out << "path: " << value.path << ", ";
	out << "domain: " << value.domain << ", ";
	out << "expires: " << value.expires << ", ";
	out << "max_age: " << value.max_age << ", ";
	out << "httponly: " << value.httponly << ", ";
	out << "version: " << value.version << ", ";
	out << "secure: " << value.secure;
	return out;
}

/*
 * ------------------------------------------------------------------
 * HttpCookieStore implementation
 * ------------------------------------------------------------------
 */
HttpCookieStore::HttpCookieStore(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler)
	: io_service_(io_service), checkpoint_handler_(checkpoint_handler), eventhandler_(NULL) {
}

HttpCookieStore::~HttpCookieStore(void) {
}

HttpCookieStore::APtr HttpCookieStore::create(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler) {
	return APtr(new HttpCookieStore(io_service, checkpoint_handler));
}

void HttpCookieStore::remove_existing_cookie(const HttpSetCookieValue& new_cookie) {
	std::vector<HttpSetCookieValue>::iterator i(cookie_store_.begin());
	while(i != cookie_store_.end()) {
		if( (*i) == new_cookie) {
			cookie_store_.erase(i);
			return;
		}
		i++;
	}
}

bool HttpCookieStore::verify_cookie(const HttpSetCookieValue& http_cookie_value, const std::string& default_doman) {
	if(! boost::starts_with(http_cookie_value.path, "/")) {
		return false;
	}

	int dot_count = std::count(http_cookie_value.domain.begin(), http_cookie_value.domain.end(), '.');
	if(dot_count < 2) {
		return false;
	}

	if(http_cookie_value.max_age=="0") {
		return false;
	}

	//TODO: Verify domain (cross domain)
	return true;
}


void HttpCookieStore::add_cookie(const std::string& value, const std::string& default_path, const std::string& default_doman) {
	HttpSetCookieValue http_set_cookie_value(default_path, default_doman);
	if(parse_http_set_cookie_value(value, http_set_cookie_value)) {
		remove_existing_cookie(http_set_cookie_value);
		if(eventhandler_ && (!http_set_cookie_value.httponly)) {
			eventhandler_->cookie_removed(http_set_cookie_value);
		}

		if(verify_cookie(http_set_cookie_value, default_doman)) {
			cookie_store_.push_back(http_set_cookie_value);
			if(eventhandler_ && (!http_set_cookie_value.httponly)) {
				eventhandler_->cookie_added(http_set_cookie_value);
			}
		}
		else {
			stringstream ss;
			ss << http_set_cookie_value;
			Checkpoint cp(*checkpoint_handler_, "invalid_cookie", Attr_HttpClient(), CpAttr_debug(), CpAttr("content", ss.str()));
		}
	}
}

void HttpCookieStore::calc_cookies(const std::string& host, const std::string& path, std::vector<std::string>& header_cookie_values) {
	std::vector<HttpSetCookieValue>::const_iterator i(cookie_store_.begin());
	while(i != cookie_store_.end()) {
		const HttpSetCookieValue& http_cookie_value(*i);

		bool add_cookie = false;
		if(boost::ends_with(host, http_cookie_value.domain) && boost::starts_with(path, http_cookie_value.path)) {
			add_cookie = true;
		}

		if(http_cookie_value.secure) {
			add_cookie = false;
		}
		if(add_cookie) {
			std::stringstream ss;
			ss << http_cookie_value.key << "=" << http_cookie_value.value;
			header_cookie_values.push_back(ss.str());
		}
		i++;
	}
}

void HttpCookieStore::dump(std::ostream& os) {
	std::vector<HttpSetCookieValue>::const_iterator i(cookie_store_.begin());
	while(i != cookie_store_.end()) {
		const HttpSetCookieValue& http_cookie_value(*i);
		os << http_cookie_value << endl;
		i++;
	}
}

void HttpCookieStore::set_eventhandler(HttpCookieStoreEventhandler* eventhandler) {
	eventhandler_ = eventhandler;
}

void HttpCookieStore::reset_eventhandler(void) {
	eventhandler_ = NULL;
}


