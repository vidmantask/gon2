/*! \file HttpClientCom.hxx
 *
 * \brief This file contains the http client communication abstracion
 *
 */
#ifndef Component_HttpClientCom_hxx
#define Component_HttpClientCom_hxx



#include <lib/utility/UY_MutexLock.hxx>
#include <lib/utility/UY_CheckpointHandler.hxx>
#include <component/communication/COM_AsyncService.hxx>


#include "HttpClientComEventhandler.hxx"


namespace Giritech {
namespace HttpClient {


/*! \brief http client communcation abstraction
 *
 */
class HttpClientCom {
public:
    typedef boost::shared_ptr<HttpClientCom> APtr;


    void set_eventhandler(HttpClientComEventhandler* com_eventhandler);
    void reset_eventhandler(void);

    /*
     * Destructor
     */
    ~HttpClientCom(void);

    /*
     * Expected to connect.
     * Should call one of http_client_com_connected, http_client_com_closed or http_client_com_closed_with_error once.
     */
    virtual void connect(const std::string& host, const unsigned long port) = 0;

    /*
     * Write some data
     */
    virtual void write(const Utility::DataBufferManaged::APtr& data) = 0;

    /*
     * Close, will call one of http_client_com_closed or http_client_com_closed_with_error once.
     */
    virtual void close(void) = 0;

    /*
     * is_clised, indicate if the communication is closed.
     */
    virtual bool is_closed(void) = 0;


    /*
     * set_mutex, set mutex to use
     */
    virtual void set_mutex(const Utility::Mutex::APtr& mutex);


    void enable_http_proxification(void);
    bool get_http_proxification(void);



protected:
    HttpClientCom(boost::asio::io_service& io_service, const Utility::CheckpointHandler::APtr& checkpoint_handler);

    boost::asio::io_service& io_service_;
    Utility::CheckpointHandler::APtr checkpoint_handler_;

    HttpClientComEventhandler* com_eventhandler_;

    bool http_proxification_;

    Utility::Mutex::APtr mutex_;
};


}
}
#endif
