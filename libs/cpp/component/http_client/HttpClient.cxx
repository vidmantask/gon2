/*! \file HttpClient.cxx
 \brief This file contains the implementation for the thttp client functionality
 */
#include <boost/spirit/include/qi.hpp>

#include <boost/range/iterator_range.hpp>
#include <boost/optional.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>


#include <component/http_client/HttpClient.hxx>
#include <component/http_client/HttpClient_CheckpointAttr.hxx>

#undef GIRI_ENABLE_BODY_LOG
//#define GIRI_ENABLE_BODY_LOG


using namespace std;
using namespace Giritech;
using namespace Giritech::Utility;
using namespace Giritech::Communication;

using namespace Giritech::HttpClient;


namespace qi = boost::spirit::qi;
namespace ascii = boost::spirit::ascii;
namespace phoenix = boost::phoenix;


/*
 * Parser for URI
 */
template < class FwdIter >
struct URIHierachicalPart {
	boost::optional<boost::iterator_range<FwdIter> > user_info;
	boost::optional<boost::iterator_range<FwdIter> > host;
	boost::optional<boost::iterator_range<FwdIter> > port;
	boost::optional<boost::iterator_range<FwdIter> > path;
};

template < class FwdIter >
struct URIParts {
	boost::iterator_range<FwdIter> scheme;
	URIHierachicalPart<FwdIter> hier_part;
    boost::optional<boost::iterator_range<FwdIter> > query;
    boost::optional<boost::iterator_range<FwdIter> > fragment;

    void clear() {
        scheme = boost::iterator_range<FwdIter>();
        hier_part.user_info = boost::optional<boost::iterator_range<FwdIter> >();
        hier_part.host = boost::optional<boost::iterator_range<FwdIter> >();
        hier_part.port = boost::optional<boost::iterator_range<FwdIter> >();
        hier_part.path = boost::optional<boost::iterator_range<FwdIter> >();
        query = boost::optional<boost::iterator_range<FwdIter> >();
        fragment = boost::optional<boost::iterator_range<FwdIter> >();
    }
};

BOOST_FUSION_ADAPT_TPL_STRUCT (
    (FwdIter),
    (URIHierachicalPart)(FwdIter),
    (boost::optional<boost::iterator_range<FwdIter> >, user_info)
    (boost::optional<boost::iterator_range<FwdIter> >, host)
    (boost::optional<boost::iterator_range<FwdIter> >, port)
    (boost::optional<boost::iterator_range<FwdIter> >, path)
);

BOOST_FUSION_ADAPT_TPL_STRUCT (
    (FwdIter),
    (URIParts)(FwdIter),
    (boost::iterator_range<FwdIter>, scheme)
    (URIHierachicalPart<FwdIter>, hier_part)
    (boost::optional<boost::iterator_range<FwdIter> >, query)
    (boost::optional<boost::iterator_range<FwdIter> >, fragment)
);



template < class String >
struct URIGrammar : qi::grammar < typename String::const_iterator, URIParts<typename String::const_iterator>()> {

    typedef String string_type;
    typedef typename String::const_iterator const_iterator;

    URIGrammar() : URIGrammar::base_type(start, "uri") {
        // gen-delims = ":" / "/" / "?" / "#" / "[" / "]" / "@"
        gen_delims %= qi::char_(":/?#[]@");

        // sub-delims = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
        sub_delims %= qi::char_("!$&'()*+,;=");

        // reserved = gen-delims / sub-delims
        reserved %= gen_delims | sub_delims;

        // unreserved = ALPHA / DIGIT / "-" / "." / "_" / "~"
        unreserved %= qi::alnum | qi::char_("-._~");

        // pct-encoded = "%" HEXDIG HEXDIG
        pct_encoded %= qi::char_("%") >> qi::repeat(2)[qi::xdigit];

        // pchar = unreserved / pct-encoded / sub-delims / ":" / "@"
        pchar %= qi::raw[
            unreserved | pct_encoded | sub_delims | qi::char_(":@")
            ];

        // segment = *pchar
        segment %= qi::raw[*pchar];

        // segment-nz = 1*pchar
        segment_nz %= qi::raw[+pchar];

        // segment-nz-nc = 1*( unreserved / pct-encoded / sub-delims / "@" )
        segment_nz_nc %= qi::raw[
            +(unreserved | pct_encoded | sub_delims | qi::char_("@"))
            ];

        // path-abempty  = *( "/" segment )
        path_abempty %=
            qi::raw[*(qi::char_("/") >> segment)]
            ;

        // path-absolute = "/" [ segment-nz *( "/" segment ) ]
        path_absolute %=
            qi::raw[
                    qi::char_("/")
                    >>  -(segment_nz >> *(qi::char_("/") >> segment))
                    ]
            ;

        // path-rootless = segment-nz *( "/" segment )
        path_rootless %=
            qi::raw[segment_nz >> *(qi::char_("/") >> segment)]
            ;

        // path-empty = 0<pchar>
        path_empty %=
            qi::raw[qi::eps]
            ;

        // scheme = ALPHA *( ALPHA / DIGIT / "+" / "-" / "." )
        scheme %=
            qi::raw[qi::alpha >> *(qi::alnum | qi::char_("+.-"))]
            ;

        // user_info = *( unreserved / pct-encoded / sub-delims / ":" )
        user_info %=
            qi::raw[*(unreserved | pct_encoded | sub_delims | qi::char_(":"))]
            ;

        ip_literal %=
            qi::lit('[') >> (ipv6address | ipvfuture) >> ']'
            ;

        ipvfuture %=
            qi::lit('v') >> +qi::xdigit >> '.' >> +( unreserved | sub_delims | ':')
            ;

        ipv6address %= qi::raw[
                                                                      qi::repeat(6)[h16 >> ':'] >> ls32
            |                                                 "::" >> qi::repeat(5)[h16 >> ':'] >> ls32
            | qi::raw[                                h16] >> "::" >> qi::repeat(4)[h16 >> ':'] >> ls32
            | qi::raw[            +(*(h16 >> ':')) >> h16] >> "::" >> qi::repeat(3)[h16 >> ':'] >> ls32
            | qi::raw[qi::repeat(2)[*(h16 >> ':')] >> h16] >> "::" >> qi::repeat(2)[h16 >> ':'] >> ls32
            | qi::raw[qi::repeat(3)[*(h16 >> ':')] >> h16] >> "::" >>               h16 >> ':'  >> ls32
            | qi::raw[qi::repeat(4)[*(h16 >> ':')] >> h16] >> "::"                              >> ls32
            | qi::raw[qi::repeat(5)[*(h16 >> ':')] >> h16] >> "::"                              >> h16
            | qi::raw[qi::repeat(6)[*(h16 >> ':')] >> h16] >> "::"
            ];

        // ls32 = ( h16 ":" h16 ) / IPv4address
        ls32 %= (h16 >> ':' >> h16) | ipv4address
            ;

        // h16 = 1*4HEXDIG
        h16 %= qi::repeat(1, 4)[qi::xdigit]
            ;

        // dec-octet = DIGIT / %x31-39 DIGIT / "1" 2DIGIT / "2" %x30-34 DIGIT / "25" %x30-35
        dec_octet %=
            !(qi::lit('0') >> qi::digit)
            >>  qi::raw[
                qi::uint_parser<boost::uint8_t, 10, 1, 3>()
                ];

        // IPv4address = dec-octet "." dec-octet "." dec-octet "." dec-octet
        ipv4address %= qi::raw[
            dec_octet >> qi::repeat(3)[qi::lit('.') >> dec_octet]
            ];

        // reg-name = *( unreserved / pct-encoded / sub-delims )
        reg_name %= qi::raw[
            *(unreserved | pct_encoded | sub_delims)
            ];

        // TODO, host = IP-literal / IPv4address / reg-name
        host %=
            qi::raw[ip_literal | ipv4address | reg_name]
            ;

        // port %= qi::ushort_;
        port %=
            qi::raw[*qi::digit]
            ;

        // query = *( pchar / "/" / "?" )
        query %=
            qi::raw[*(pchar | qi::char_("/?"))]
            ;

        // fragment = *( pchar / "/" / "?" )
        fragment %=
            qi::raw[*(pchar | qi::char_("/?"))]
            ;

        // hier-part = "//" authority path-abempty / path-absolute / path-rootless / path-empty
        // authority = [ userinfo "@" ] host [ ":" port ]
        hier_part %=
            (
				(("//" >> user_info >> '@') | "//")
                >>  host
                >> -(':' >> port)
                >>  path_abempty
                )
            |
            (
                    qi::attr(boost::iterator_range<const_iterator>())
                >>  qi::attr(boost::iterator_range<const_iterator>())
                >>  qi::attr(boost::iterator_range<const_iterator>())
                >>  (
                    path_absolute
                    |   path_rootless
                    |   path_empty
                    )
                )
            ;

        start %=
            scheme >> ':'
            >> hier_part
            >>  -('?' >> query)
            >>  -('#' >> fragment)
            ;
    }

    qi::rule<const_iterator, typename boost::iterator_range<const_iterator>::value_type()> gen_delims, sub_delims, reserved, unreserved;
    qi::rule<const_iterator, string_type()> pct_encoded, pchar;
    qi::rule<const_iterator, string_type()> segment, segment_nz, segment_nz_nc;
    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> path_abempty, path_absolute, path_rootless, path_empty;
    qi::rule<const_iterator, string_type()> dec_octet, ipv4address, reg_name, ipv6address, ipvfuture, ip_literal;
    qi::rule<const_iterator, string_type()> h16, ls32;
    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> host, port;
    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> scheme, user_info, query, fragment;
    qi::rule<const_iterator, URIHierachicalPart<const_iterator>()>  hier_part;

    // actual uri parser
    qi::rule<const_iterator, URIParts<const_iterator>()> start;
};

bool parse_uri(std::string::const_iterator first, std::string::const_iterator last, URIParts<std::string::const_iterator>& parts) {
    static URIGrammar<std::string> grammar;
    bool is_valid = qi::parse(first, last, grammar, parts);
    return is_valid && (first == last);
}



/*
 * Parser for HttpResponseStatus
 */
template < class FwdIter >
struct HttpResponseStatusPart {
	boost::iterator_range<FwdIter> http_response_version;
	boost::iterator_range<FwdIter> http_response_status_code;
	boost::iterator_range<FwdIter> http_response_status_message;
};

BOOST_FUSION_ADAPT_TPL_STRUCT (
    (FwdIter),
    (HttpResponseStatusPart)(FwdIter),
    (boost::iterator_range<FwdIter>, http_response_version)
    (boost::iterator_range<FwdIter>, http_response_status_code)
    (boost::iterator_range<FwdIter>, http_response_status_message)
);

template < class String >
struct HttpResponseStatusGrammar : qi::grammar < typename String::const_iterator, HttpResponseStatusPart<typename String::const_iterator>()> {

    typedef String string_type;
    typedef typename String::const_iterator const_iterator;

    HttpResponseStatusGrammar() : HttpResponseStatusGrammar::base_type(start) {
    	http_response_version %= qi::raw[ qi::string("HTTP") >> qi::char_("/") >> +(qi::digit) >> qi::char_(".")  >> +(qi::digit) ];
    	http_response_status_code %= qi::raw[ +(qi::digit) ];
    	http_response_status_message %= qi::raw[ +(qi::char_-qi::char_("\r\n")) ];

        start %= http_response_version >> +" " >> http_response_status_code >> +" " >> http_response_status_message >> "\r\n";
    }

    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> http_response_version;
    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> http_response_status_code;
    qi::rule<const_iterator, boost::iterator_range<const_iterator>()> http_response_status_message;
    qi::rule<const_iterator, HttpResponseStatusPart<const_iterator>()> start;
};

bool parse_http_response_status(std::vector<byte_t>::const_iterator& first, std::vector<byte_t>::const_iterator& last, HttpResponseStatusPart< std::vector<byte_t>::const_iterator >& http_response_status) {
	static HttpResponseStatusGrammar< std::vector<byte_t> > grammar;
	bool is_valid = qi::parse(first, last, grammar, http_response_status);
    return is_valid;
}


/*
 * Parser for HttpResponseHeaders
 */
typedef std::pair<std::string, std::string > pair_type;
typedef std::vector<pair_type> pairs_type;

template <typename Iterator>
struct HttpResponseHeadersGramma : qi::grammar<Iterator, pairs_type()> {
	HttpResponseHeadersGramma() : HttpResponseHeadersGramma::base_type(start) {
            start = *(pair >> "\r\n") >> "\r\n";
            pair  = key >> ':' >> value;
            key   = +(qi::char_ - qi::char_(":\r\n"));
            value = +(qi::char_ - qi::char_("\r\n"));
    }
	qi::rule<Iterator, pairs_type()> start;
	qi::rule<Iterator, pair_type()> pair;
	qi::rule<Iterator, std::string()> key, value;
};


template < typename Iterator >
bool parse_http_response_headers(Iterator& first, Iterator& last, pairs_type& key_values) {
	HttpResponseHeadersGramma<Iterator> gramma;
	bool is_valid = qi::parse(first, last, gramma, key_values);
	return is_valid;
}


/*
 * Parser for HttpResponseChunkHeader
 */
template <typename Iterator>
struct HttpResponseChunkHeaderGramma : qi::grammar<Iterator, std::string()> {
	HttpResponseChunkHeaderGramma() : HttpResponseChunkHeaderGramma::base_type(start) {
            start = chunk_size >> "\r\n";
            chunk_size = +(qi::alnum);
    }
	qi::rule<Iterator, std::string()> start;
	qi::rule<Iterator, std::string()> chunk_size;
};

template < typename Iterator >
bool parse_http_response_chunk_header(Iterator& first, Iterator& last, std::string& chunk_size) {
	HttpResponseChunkHeaderGramma<Iterator> gramma;
	bool is_valid = qi::parse(first, last, gramma, chunk_size);
	return is_valid;
}



/*
 * ------------------------------------------------------------------
 * HttpRequest implementation
 * ------------------------------------------------------------------
 */
boost::uint64_t global_http_request_id = 0;
boost::recursive_mutex global_http_request_id_mutex;

boost::uint64_t get_http_request_id_next(void) {
	boost::recursive_mutex::scoped_lock update_mutex_lock(global_http_request_id_mutex);
	return global_http_request_id++;
}

HttpRequest::HttpRequest(const std::string& method, const std::string& url)
	: method_(method),
	  url_(url),
	  http_response_eventhandler_(NULL),
	  request_id_(get_http_request_id_next()),
	  https_enabled_(false) {
}

HttpRequest::~HttpRequest(void) {
}

void HttpRequest::enable_https(void) {
	https_enabled_ = true;
}

boost::uint64_t HttpRequest::get_request_id(void) const {
	return request_id_;
}

void HttpRequest::set_headers(const HeadersType& headers) {
	headers_ = headers;
}

void HttpRequest::add_header(const std::string& key, const std::string& value, const bool overwrite) {
	bool found = false;

	if(overwrite) {
		HeadersType::iterator i(headers_.begin());
		while(i != headers_.end()) {
			if(i->first == key) {
				i->second = value;
				found = true;
				break;
			}
			i++;
		}
	}
	if(!found) {
		headers_.push_back(make_pair(key, value));
	}
}

void HttpRequest::set_body(const std::string& body) {
	body_ = body;
}

void HttpRequest::set_response_eventhandler(HttpResponseEventhandler* http_response_eventhandler) {
	http_response_eventhandler_ = http_response_eventhandler;
}

HttpResponseEventhandler* HttpRequest::get_response_eventhandler(void) {
	return http_response_eventhandler_;
}

std::string HttpRequest::get_url(void) {
	return url_;
}

void HttpRequest::dump(ostream& os) {
	os << "HttpRequest::dump.begin" << endl;
	os << "url:" << url_ << endl;
	os << "HttpRequest::dump.end" << endl;
}

void HttpRequest::serialize(std::ostream& os, const bool http_proxification) {
	URIParts<std::string::const_iterator> uri_parts;
	bool ok = parse_uri(url_.begin(), url_.end(), uri_parts);
	if(!ok) {
		throw ExceptionHttpClient("Invalid URL");
	}

	stringstream ssHost;
	if(uri_parts.hier_part.host) {
		ssHost << (*uri_parts.hier_part.host);
	}
	else {
		ssHost << "127.0.0.1";
	}
	if(uri_parts.hier_part.port && ((*uri_parts.hier_part.port)!="80") ) {
		ssHost << ":" << (*uri_parts.hier_part.port);
	}

	stringstream ssPath;
	if(uri_parts.hier_part.path) {
		ssPath << (*uri_parts.hier_part.path);
	}
	else {
		ssPath << "/";
	}
	if(uri_parts.query) {
		ssPath << "?" <<  (*uri_parts.query);
	}
	if(uri_parts.fragment) {
		ssPath << "#" <<  (*uri_parts.fragment);
	}

	os << method_ << " ";
	if(http_proxification) {
		if(https_enabled_) {
			os << "https://" << ssHost.str() << ssPath.str();
		}
		else {
			os << "http://" << ssHost.str() << ssPath.str();
		}
	}
	else {
		os << ssPath.str();
	}
	os << " ";
	os << "HTTP/1.1" << "\r\n";


	os << "Host: " << ssHost.str() <<  "\r\n";
	if ((method_ == "POST") && (body_.length() > 0 )) {
		os << "Content-Length: " << body_.length() << "\r\n";
	}

	HeadersType::const_iterator i(headers_.begin());
	while(i != headers_.end()) {
		os << i->first << ": " << i->second << "\r\n";
		i++;
	}
	os << "Accept-Encoding: gzip,deflate" << "\r\n";
	os << "\r\n";

	if ((method_ == "POST") && (body_.length() > 0 )) {
		os << body_ ;
	}
}

std::string HttpRequest::get_path(void) const {
	URIParts<std::string::const_iterator> uri_parts;
	bool ok = parse_uri(url_.begin(), url_.end(), uri_parts);
	if(!ok) {
		throw ExceptionHttpClient("Invalid URL");
	}

	string path;
	if(uri_parts.hier_part.path) {
		stringstream ss;
		ss << *(uri_parts.hier_part.path);
		path = ss.str();
	}
	return path;
}

std::string HttpRequest::get_host(void) const {
	URIParts<std::string::const_iterator> uri_parts;
	bool ok = parse_uri(url_.begin(), url_.end(), uri_parts);
	if(!ok) {
		throw ExceptionHttpClient("Invalid URL");
	}

	string  host("127.0.0.1");
	if(uri_parts.hier_part.host) {
		stringstream ss;
		ss << *(uri_parts.hier_part.host);
		host = ss.str();
	}
	return host;
}

unsigned long HttpRequest::get_port(void) const {
	URIParts<std::string::const_iterator> uri_parts;
	bool ok = parse_uri(url_.begin(), url_.end(), uri_parts);
	if(!ok) {
		throw ExceptionHttpClient("Invalid URL");
	}

	unsigned long port(80);
	if(uri_parts.hier_part.port) {
		port = boost::lexical_cast<unsigned long>(*(uri_parts.hier_part.port));
	}
	return port;
}

HttpRequest::APtr HttpRequest::create_get(const std::string& url) {
	return HttpRequest::APtr(new HttpRequest("GET", url));
}

HttpRequest::APtr HttpRequest::create_post(const std::string& url) {
	return HttpRequest::APtr(new HttpRequest("POST", url));
}


void HttpRequest::add_cookies(const HttpCookieStore::APtr cookie_store) {
	stringstream cookie_value_ss;
	std::vector<std::string> cookie_values;
	cookie_store->calc_cookies(get_host(), get_path(), cookie_values);
	std::vector<std::string>::const_iterator i(cookie_values.begin());
	while(i != cookie_values.end()) {
		cookie_value_ss << *i;
		i++;
		if(i != cookie_values.end()) {
			cookie_value_ss << "; ";
		}
	}
	if(cookie_values.size()>0) {
		add_header("Cookie", cookie_value_ss.str(), false);
	}
}

/*
 * ------------------------------------------------------------------
 * HttpResponse implementation
 * ------------------------------------------------------------------
 */
HttpResponse::HttpResponse(const HttpRequest::APtr& request, HttpResponseEventhandler* http_response_eventhandler)
: request_(request),
  pending_data_(Utility::DataBufferManaged::create("")),
  parse_state_(ParseState_Status),
  body_content_length_handled_(0),
  body_content_length_(0),
  http_response_eventhandler_(http_response_eventhandler),
  http_content_length_method_(HttpContentLengthMethod_Unknown),
  compress_type_(CompressType_none),
  http_body_compressed_data_(Utility::DataBufferManaged::create(0)) {
}

HttpResponse::~HttpResponse(void) {
}

void HttpResponse::dump(ostream& os) {
	os << "HttpResponse::dump.begin" << endl;
	os << "parse_state_:" << parse_state_ << endl;
	os << "http_content_length_method_:" << http_content_length_method_ << endl;
	os << "body_content_length_handled_:" << body_content_length_handled_ << endl;
	os << "body_content_length_:" << body_content_length_ << endl;
	os << "header_version_:" << header_version_ << endl;
	os << "header_status_code_:" << header_status_code_ << endl;
	os << "-----------------------" << endl;
	os << pending_data_->toString() << endl;
	os << "-----------------------" << endl;
	os << "HttpResponse::dump.end" << endl;
}

HttpResponse::APtr HttpResponse::create(const HttpRequest::APtr& request, HttpResponseEventhandler* http_response_eventhandler) {
	return APtr(new HttpResponse(request, http_response_eventhandler));
}

unsigned long HttpResponse::get_content_length_handled(void) {
	return body_content_length_handled_;
}

void HttpResponse::read_some(const Utility::DataBuffer::APtr& data) {
	pending_data_->append(data);
	while(parse());
}

bool HttpResponse::parse(void) {
	ParseState parse_state_begin(parse_state_);
	unsigned long pending_data_size_begin(pending_data_->getSize());

	switch(parse_state_) {
	case ParseState_Status:
		parse_status();
		break;
	case ParseState_Headers:
		parse_headers();
		break;
	case ParseState_Body:
		parse_body();
		break;
	default:
		break;
	}

	// Returns false(stop parsing) if no data in pending buffer
	if(pending_data_->getSize() == 0) {
		return false;
	}

	// Returns true(keep parsing) if state has changed or if data from pending buffer has been used
	return (parse_state_begin != parse_state_ ) || (pending_data_size_begin != pending_data_->getSize());
}

void HttpResponse::parse_status(void) {
	HttpResponseStatusPart< std::vector<byte_t>::const_iterator > http_response_status_part;

	std::vector<byte_t>& raw_data(pending_data_->get_raw_buffer());
	std::vector<byte_t>::const_iterator raw_data_begin(raw_data.begin());
	std::vector<byte_t>::const_iterator raw_data_end(raw_data.end());

	if(parse_http_response_status(raw_data_begin, raw_data_end, http_response_status_part)) {
		stringstream http_response_version_ss;
		http_response_version_ss << http_response_status_part.http_response_version;
		header_version_ = http_response_version_ss.str();

		stringstream http_response_status_code_ss;
		http_response_status_code_ss << http_response_status_part.http_response_status_code;
		http_response_status_code_ss >> header_status_code_;

		stringstream header_status_message_ss;
		header_status_message_ss << http_response_status_part.http_response_status_message;
		header_status_message_ = header_status_message_ss.str();

		unsigned long to_eat = raw_data_begin - raw_data.begin();
		pending_data_->eatFront(to_eat);
		parse_state_ = ParseState_Headers;
	}
}

void HttpResponse::parse_headers(void) {
	std::vector<byte_t>& raw_data(pending_data_->get_raw_buffer());
	std::vector<byte_t>::const_iterator raw_data_begin(raw_data.begin());
	std::vector<byte_t>::const_iterator raw_data_end(raw_data.end());
	if(parse_http_response_headers(raw_data_begin, raw_data_end, headers_)) {
		http_content_length_method_ = HttpContentLengthMethod_Missing;
		body_content_length_ = 0;
        pairs_type::iterator end = headers_.end();
        for (pairs_type::iterator it = headers_.begin(); it != end; ++it)  {
        	std::string key_lower((*it).first);
        	boost::algorithm::to_lower(key_lower);
        	if(key_lower == "content-length") {
        		unsigned long value(0);
        		stringstream ss((*it).second);
        		ss >> value;
        		body_content_length_ = value;
        		http_content_length_method_ = HttpContentLengthMethod_ByLength;
        	}
        	if(key_lower == "transfer-encoding") {
            	std::string value_lower((*it).second);
            	boost::algorithm::to_lower(value_lower);
            	boost::algorithm::trim(value_lower);
            	if(value_lower == "chunked") {
            		http_content_length_method_ = HttpContentLengthMethod_Chunked;
            	}
        	}
        	if(key_lower == "content-encoding") {
            	std::string value_lower((*it).second);
            	boost::algorithm::to_lower(value_lower);
            	boost::algorithm::trim(value_lower);
            	if(value_lower == "gzip") {
            		initilize_and_enable_compressed_body_handling(CompressType_gzip);
            	}
            	if(value_lower == "deflate") {
            		initilize_and_enable_compressed_body_handling(CompressType_deflate);
            	}
        	}
        }
		unsigned long to_eat = raw_data_begin - raw_data.begin();
		pending_data_->eatFront(to_eat);
		parse_state_ = ParseState_Body;

		if(http_response_eventhandler_) {
			http_response_eventhandler_->http_client_response_header(header_version_, header_status_code_, headers_);
		}

		if(http_content_length_method_ == HttpContentLengthMethod_Missing) {
			enter_parse_state_done();
		}
		if((http_content_length_method_ == HttpContentLengthMethod_ByLength) && (body_content_length_==0)) {
			enter_parse_state_done();
		}
	}
}

void HttpResponse::enter_parse_state_done(void) {
	switch(compress_type_) {
	case CompressType_gzip:
	case CompressType_deflate:
	case CompressType_deflate_no_header:
		finish_compressed_body_handling();
		break;
	default:
		break;
	}

	parse_state_ = ParseState_Done;
	if(http_response_eventhandler_) {
		http_response_eventhandler_->http_client_response_done();
	}
}

void HttpResponse::parse_body(void) {
	if(http_content_length_method_ == HttpContentLengthMethod_ByLength) {
		parse_body_by_length();
	}
	else if(http_content_length_method_ == HttpContentLengthMethod_Chunked) {
		bool cont = true;
		while((parse_state_ == ParseState_Body) && cont) {
			cont = parse_body_chunked();
		}
	}
}

void HttpResponse::parse_body_by_length(void) {
	body_content_length_handled_ += pending_data_->getSize();

	if(http_response_eventhandler_) {
		parse_body_data_ready(pending_data_);
	}
	pending_data_->clear();

	if(body_content_length_handled_ >= body_content_length_) {
		enter_parse_state_done();
	}
}

bool HttpResponse::parse_body_chunked(void) {
	bool cont(false);

	std::vector<byte_t>& raw_data(pending_data_->get_raw_buffer());
	std::vector<byte_t>::const_iterator raw_data_begin(raw_data.begin());
	std::vector<byte_t>::const_iterator raw_data_end(raw_data.end());

	std::string chunk_size_str;
	if(parse_http_response_chunk_header(raw_data_begin, raw_data_end, chunk_size_str)) {
		unsigned long chunk_size = 0;
		std::stringstream ss;
		ss << std::hex << chunk_size_str;
		ss >> chunk_size;

		if(chunk_size > 0) {
			unsigned long to_eat = raw_data_begin - raw_data.begin();
			unsigned long to_eat_with_chunk = to_eat + chunk_size + 2;
			if (pending_data_->getSize() >= to_eat_with_chunk) {
				pending_data_->eatFront(to_eat);
				Utility::DataBufferManaged::APtr chunk(Utility::DataBufferManaged::create(raw_data.begin(), chunk_size));
				parse_body_data_ready(chunk);
				pending_data_->eatFront(chunk_size + 2);
				body_content_length_handled_ += chunk_size;
				cont = true;
			}
		}
	}

	if( (pending_data_->getSize() == 5) && (pending_data_->toString() == "0\r\n\r\n")) {
		enter_parse_state_done();
		cont = false;
	}
	return cont;
}

void HttpResponse::initilize_and_enable_compressed_body_handling(const CompressType& compress_type) {
	compress_type_ = compress_type;
}

void HttpResponse::parse_body_data_ready(Utility::DataBufferManaged::APtr& data) {
	switch(compress_type_) {
	case CompressType_gzip:
	case CompressType_deflate:
	case CompressType_deflate_no_header:
		http_body_compressed_data_->append(data);
		break;
	case CompressType_none:
		if(http_response_eventhandler_) {
			http_response_eventhandler_->http_client_response_body_data(data);
		}
		break;
	}
}

void HttpResponse::finish_compressed_body_handling(void) {
	std::istringstream http_body_compressed_in;
	std::ostringstream http_body_compressed_out;

	boost::iostreams::zlib_params::zlib_params zlib_params;
	boost::iostreams::filtering_streambuf<boost::iostreams::input> http_body_compressed_filtering_buffer;
	switch(compress_type_) {
	case CompressType_deflate:
		http_body_compressed_filtering_buffer.push(boost::iostreams::zlib_decompressor());
		break;
	case CompressType_deflate_no_header:
		zlib_params.noheader = true;
		http_body_compressed_filtering_buffer.push(boost::iostreams::zlib_decompressor(zlib_params));
		break;
	case CompressType_gzip:
		http_body_compressed_filtering_buffer.push(boost::iostreams::gzip_decompressor());
		break;
	case CompressType_none:
		break;
	}
	http_body_compressed_filtering_buffer.push(http_body_compressed_in);

	try {
		if(http_body_compressed_data_->getSize() > 0) {
			http_body_compressed_in.str(http_body_compressed_data_->toString());
			boost::iostreams::copy(http_body_compressed_filtering_buffer, http_body_compressed_out);
		}
	}
	catch(boost::iostreams::zlib_error& error) {
		switch(compress_type_) {
		case CompressType_none:
			break;
		case CompressType_deflate:
			compress_type_ = CompressType_deflate_no_header;
			finish_compressed_body_handling();
			break;
		default:
			compress_type_ = CompressType_none;
			finish_compressed_body_handling();
			break;
		}
		return;
	}
	catch(boost::iostreams::gzip_error& error) {
		switch(compress_type_) {
		case CompressType_none:
			break;
		default:
			compress_type_ = CompressType_none;
			finish_compressed_body_handling();
			break;
		}
		return;
	}
	Utility::DataBufferManaged::APtr data_uncompressed(Utility::DataBufferManaged::create(http_body_compressed_out.str()));

	if(http_response_eventhandler_) {
		http_response_eventhandler_->http_client_response_body_data(data_uncompressed);
	}
}

/*
 * ------------------------------------------------------------------
 * HttpClient implementation
 * ------------------------------------------------------------------
 */


Giritech::HttpClient::HttpClient::HttpClient(
		boost::asio::io_service& io_service,
        const CheckpointHandler::APtr& checkpoint_handler,
        const HttpClientCom::APtr& http_client_com,
        const HttpCookieStore::APtr& cookie_store) :
        io_service_(io_service),
        checkpoint_handler_(checkpoint_handler),
        http_client_com_(http_client_com ),
        cookie_store_(cookie_store),
        client_eventhandler_(NULL),
        state_(State_Ready),
        connect_timeout_timer_(io_service),
        connect_timeout_pending_(0),
        read_timeout_timer_(io_service),
        read_timeout_pending_(0),
        connect_pending_(0),
        mutex_(Mutex::create(io_service, "HttpClient")),
        logging_enabled_(false) {
	http_client_com_->set_eventhandler(this);

	CheckpointIsLogging is_logging_enabled(*checkpoint_handler_, "point", Attr_HttpClient(), CpAttr_debug());
	logging_enabled_ = is_logging_enabled();
}


Giritech::HttpClient::HttpClient::~HttpClient(void) {
}

Giritech::HttpClient::HttpClient::APtr Giritech::HttpClient::HttpClient::create(
		boost::asio::io_service& io_service,
        const CheckpointHandler::APtr& checkpoint_handler,
        const HttpClientCom::APtr& http_client_com,
        const HttpCookieStore::APtr& cookie_store) {
	APtr result(new HttpClient(io_service, checkpoint_handler, http_client_com, cookie_store));
	AsyncMemGuard::global_add_element(result);
	return result;
}

bool Giritech::HttpClient::HttpClient::async_mem_guard_is_done(void) {
	return state_ == State_Done;
}

std::string Giritech::HttpClient::HttpClient::async_mem_guard_get_name(void) {
	return __FUNCTION__;
}

void Giritech::HttpClient::HttpClient::set_eventhandler(HttpClientEventhandler* client_eventhandler) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::set_eventhandler");
	client_eventhandler_ = client_eventhandler;
}
void Giritech::HttpClient::HttpClient::reset_eventhandler(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::reset_eventhandler");
	client_eventhandler_ = NULL;
}

void Giritech::HttpClient::HttpClient::set_mutex(const Utility::Mutex::APtr& mutex) {
    Mutex::safe_update(mutex_, mutex);
    http_client_com_->set_mutex(mutex);
}

void Giritech::HttpClient::HttpClient::cancel_request(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::cancel_request");
    switch(state_) {
	case State_Connecting:
	case State_Bussy:
		break;
	default:
		return;
	}
    if(current_request_.get() != NULL) {
    	Checkpoint cp(*checkpoint_handler_, "cancel_request", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));
    }
	if(client_eventhandler_) {
		client_eventhandler_->http_client_response_error();
	}
	client_eventhandler_ = NULL;
	goto_done_state();
}

void Giritech::HttpClient::HttpClient::execute_request(const HttpRequest::APtr& request) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::execute_request");
    switch(state_) {
	case State_Ready:
		break;
	default:
		assert(false);
		return;
	}
	state_ = State_Connecting;
	Checkpoint cp(*checkpoint_handler_,"execute_request", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", request->get_request_id()), CpAttr("host", request->get_host()), CpAttr("port", request->get_port()));

	current_request_ = request;
	current_response_ = HttpResponse::create(current_request_, this);
	http_client_com_->connect(request->get_host(), request->get_port());
	connect_pending_++;

	connect_timeout_timer_.expires_from_now(boost::posix_time::seconds(20));
	connect_timeout_timer_.async_wait(boost::bind(&HttpClient::HttpClient::connect_timeout, this, boost::asio::placeholders::error));
	connect_timeout_pending_++;
}

void Giritech::HttpClient::HttpClient::connect_timeout(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::connect_timeout");
    connect_timeout_pending_--;

	switch(state_) {
	case State_Bussy:
	case State_Done:
		return;
	case State_Closing:
	case State_Connecting:
		break;
	default:
		assert(false);
		return;
	}

	if(state_ == State_Closing) {
		goto_done_state();
		return;
	}
	Checkpoint cp(*checkpoint_handler_, "connect_timeout", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));

	// state connecting
	HeadersType headers;
	http_client_response_header_common("HTTP/1.1", 408, headers);
	http_client_response_done_common();
	goto_done_state();
}

void Giritech::HttpClient::HttpClient::http_client_com_connected(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_com_connected");
    connect_pending_--;

	switch(state_) {
	case State_Connecting:
	case State_Closing:
		break;
	default:
		assert(false);
		return;
	}
	if(state_ == State_Closing) {
		goto_done_state();
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"http_client_com_connected", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));
	state_ = State_Bussy;

	// Add cookies to request
	current_request_->add_cookies(cookie_store_);

	stringstream ss;
	current_request_->serialize(ss, http_client_com_->get_http_proxification());

	if(logging_enabled_) {
		Checkpoint cp(*checkpoint_handler_,"http_request", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()), CpAttr("content", ss.str()));
	}

	Utility::DataBufferManaged::APtr request_data(Utility::DataBufferManaged::create(ss.str()));
	http_client_com_->write(request_data);
}

void Giritech::HttpClient::HttpClient::http_client_com_closed(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_com_closed");
	switch(state_) {
	case State_Connecting:
	case State_Bussy:
	case State_Closing:
		break;
	default:
		assert(false);
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"http_client_com_closed", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));
	connect_pending_ = 0;

	if(state_ == State_Closing) {
		goto_done_state();
		return;
	}
}

void Giritech::HttpClient::HttpClient::http_client_com_closed_with_error(const std::string& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_com_closed_with_error");
	switch(state_) {
	case State_Connecting:
	case State_Bussy:
	case State_Closing:
		break;
	default:
		assert(false);
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"http_client_com_closed_with_erro", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()),CpAttr_message(error));
	connect_pending_ = 0;

	if(state_ == State_Closing) {
		goto_done_state();
		return;
	}
}

void Giritech::HttpClient::HttpClient::http_client_com_read_some(const Utility::DataBuffer::APtr& data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_com_read_some");
	switch(state_) {
	case State_Bussy:
		break;
	default:
		assert(false);
		return;
	}
	current_response_->read_some(data);

	read_timeout_timer_.expires_from_now(boost::posix_time::seconds(20));
	read_timeout_timer_.async_wait(boost::bind(&HttpClient::HttpClient::read_timeout, this, boost::asio::placeholders::error));
	read_timeout_pending_++;
}

void Giritech::HttpClient::HttpClient::read_timeout(const boost::system::error_code& error) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::read_timeout");
    read_timeout_pending_--;

	switch(state_) {
	case State_Done:
		return;
	case State_Bussy:
	case State_Closing:
		break;
	default:
		assert(false);
		return;
	}

	if(state_ == State_Closing) {
		goto_done_state();
		return;
	}

	// state bussy
	if(error == boost::asio::error::operation_aborted) {
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"read_timeout", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));

	HeadersType headers;
	http_client_response_header_common("HTTP/1.1", 408, headers);
	http_client_response_done_common();
	goto_done_state();
}


void Giritech::HttpClient::HttpClient::http_client_response_header(const std::string& header_version, const unsigned long header_status_code, const HeadersType& headers) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_response_header");
	switch(state_) {
	case State_Bussy:
		break;
	default:
		assert(false);
		return;
	}

	//
	// Add cookies to cookie store
	//
	HeadersType::const_iterator i(headers.begin());
	while(i != headers.end()) {
		std::string key(boost::to_lower_copy(boost::trim_copy(i->first)));
		if(key == "set-cookie") {
			cookie_store_->add_cookie(i->second, current_request_->get_path(), current_request_->get_host() );
		}
		i++;
	}
	http_client_response_header_common(header_version, header_status_code, headers);
}

void Giritech::HttpClient::HttpClient::http_client_response_header_common(const std::string& header_version, const unsigned long header_status_code, const HeadersType& headers) {
	if(logging_enabled_) {
		stringstream ss;
		ss << header_version << " " << header_status_code << endl;
		HeadersType::const_iterator i(headers.begin());
		while(i != headers.end()) {
			ss << i->first << " : " << i->second << endl;
			i++;
		}
		Checkpoint cp(*checkpoint_handler_, "http_response", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()), CpAttr("content", ss.str()));
	}

	if(client_eventhandler_) {
		client_eventhandler_->http_client_response_header(header_version, header_status_code, headers);
	}
}

void Giritech::HttpClient::HttpClient::http_client_response_done_common(void) {
	Checkpoint cp(*checkpoint_handler_, "http_response_done", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()), CpAttr("content_length_handled", current_response_->get_content_length_handled()));
#ifdef GIRI_ENABLE_BODY_LOG
	Checkpoint cp1(*checkpoint_handler_, "http_response_done.body", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()), CpAttr("content", body_));
#endif
	if(client_eventhandler_) {
		client_eventhandler_->http_client_response_done();
	}
}


void Giritech::HttpClient::HttpClient::http_client_response_body_data(Utility::DataBufferManaged::APtr& body_data) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_response_body_data");
	switch(state_) {
	case State_Bussy:
		break;
	default:
		Checkpoint cp(*checkpoint_handler_,"http_client_response_body_data.invalid_state", Attr_HttpClient(), CpAttr_error());
		assert(false);
		return;
	}
	if(client_eventhandler_) {
#ifdef GIRI_ENABLE_BODY_LOG
		body_ += body_data->toString();
#endif
		client_eventhandler_->http_client_response_body_data(body_data);
	}
	else {
		Checkpoint cp(*checkpoint_handler_,"http_client_response_body_data.no_eventhandler", Attr_HttpClient(), CpAttr_debug());
	}
}

void Giritech::HttpClient::HttpClient::http_client_response_done(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_response_done");
	switch(state_) {
	case State_Bussy:
		break;
	default:
		assert(false);
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"http_client_response_done", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));
	http_client_response_done_common();
	client_eventhandler_ = NULL;
	goto_done_state();
}

void Giritech::HttpClient::HttpClient::http_client_response_error(void) {
    MutexScopeLock mutex_lock(checkpoint_handler_, Attr_HttpClient(), mutex_, "HttpClient::http_client_response_error");
	switch(state_) {
	case State_Bussy:
		break;
	default:
		assert(false);
		return;
	}
	Checkpoint cp(*checkpoint_handler_,"http_client_response_error", Attr_HttpClient(), CpAttr_debug(), CpAttr("http_request_id", current_request_->get_request_id()));

	if(client_eventhandler_) {
		client_eventhandler_->http_client_response_error();
	}
	client_eventhandler_ = NULL;
	goto_done_state();
}

void Giritech::HttpClient::HttpClient::goto_done_state(void) {
	state_ = State_Closing;
	if(!http_client_com_->is_closed()) {
		http_client_com_->close();
		return;
	}

	if(connect_pending_ > 0) {
		return;
	}

	if(connect_timeout_pending_ > 0) {
		connect_timeout_timer_.expires_from_now(boost::posix_time::seconds(0));
		return;
	}

	if(read_timeout_pending_ > 0) {
		read_timeout_timer_.expires_from_now(boost::posix_time::seconds(0));
		return;
	}
	state_ = State_Done;
}
