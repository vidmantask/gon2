/*! \file HttpClientComEventhandler.hxx
 *  \brief This file contains abstract interface HttpClientComEventhandler
 */
#ifndef COM_HttpClientComEventhandler_HXX
#define COM_HttpClientComEventhandler_HXX


#include <lib/utility/UY_DataBuffer.hxx>


namespace Giritech {
namespace HttpClient {

/*! \brief This class define the abstract eventhandler interface for a HttpClient.
 *
 */
class HttpClientComEventhandler: public boost::noncopyable {
public:

	virtual void http_client_com_connected(void) = 0;
	virtual void http_client_com_closed(void) = 0;
	virtual void http_client_com_closed_with_error(const std::string& error) = 0;
	virtual void http_client_com_read_some(const Utility::DataBuffer::APtr& data) = 0;

};

}
}
#endif
