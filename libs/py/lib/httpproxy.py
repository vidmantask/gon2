import httpserverengine
import httpclientengine
import mime
import httpauth


def calc_host_header(scheme, host, port):
    result = host
    if scheme == 'https:' and port != 443:
        result = "%s:%d" % (result,port) 
    if scheme == 'http:' and port != 80:
        result = "%s:%d" % (result,port) 
    return result


class ProxyServer(httpserverengine.HttpServerEngineCallbacks):
    """Already connected to one client, can connect to multiple servers"""
    
    DME_HEADER_NAMES = ["username", "password", "terminal-id", "device-signature", "device-time"]

    def __init__(self, env, cb_will_listen, cb_to_client, cb_close, connector, get_authorized_ip,
            transparent_addr, username, password, expand_variables):
        httpserverengine.HttpServerEngineCallbacks.__init__(self)
        self._env = env
        self._cb_will_listen = cb_will_listen
        self._cb_to_client = cb_to_client
        self._cb_close = cb_close
        self._connector = connector
        self._get_authorized_ip = get_authorized_ip
        self._transparent_addr = transparent_addr
        self._username = username
        self._password = password
        self._clients = {} # client connections for this proxy connection - no sharing
        self._client = None
        self._tunnel = None
        self._responding = False # tunnel or client responding - too late to respond with _error
        self._error = None # if set as (code, msg, body) then reply with this when possible, with or without a client
        self._server_engine = httpserverengine.HttpServerEngine(env, self)
        self._expand_variables = expand_variables
        
        self._client_log = None

    def close(self):
        self._server_engine = None # break loop
        self._expand_variables = None
        while self._clients:
            _target, c = self._clients.popitem()
            c.close()
        tunnel, self._tunnel = self._tunnel, None
        if tunnel:
            tunnel.close()
        self._cb_close()
        self._cb_to_client = None
        self._connector = None

    # Implement virtual method
    def got_http_cancel(self, msg=None):
        self._env.log2('%s got cancelled: %s', self, msg)
        self.close()

    # Implement virtual method
    def got_http_request_start(self, method, path, protocol, headers):
        self._client_log = dict(method=method, path=path)
        self._got_http_request_start(method, path, protocol, headers)
        if self._error:
            self._env.log1('Returning %s: %s', self._error[0], self._error[1])
            self._client_log.update(dict(log_type="http_request_error", http_error=self._error))
            self._env.log0(None, **self._client_log)
            self._client_log = None
        
    def _got_http_request_start(self, method, path, protocol, headers):
        self._env.log1('%s %s', method, path, log_type="http_request_start")
        if 'groxy' in headers.get('Via', '').lower():
            self._error = ('407', 'Forbidden', "Proxy traffic looping detected (407)")
            return
        assert self._client is None, self._client # one at a time
        if self._transparent_addr: # Transparent proxy - use IP and host and auth tags and lb tags from configured target
            if not path.startswith('/'):
                self._error = ('407', 'Forbidden', "Proxy traffic not accepted by transparent proxy (407)")
                return
            self._env.log3('requested host: %s', headers.get('host'))
            hashtags = [x for x in self._transparent_addr[0].split() if x.startswith('#')]
            targethost = ' '.join(x for x in self._transparent_addr[0].split() if not x.startswith('#'))
            target = (targethost, self._transparent_addr[1])
            hostandport = '%s:%s' % (target[0], target[1])
            self._client_log.update(dict(hostandport=hostandport))
            
            enable_ssl = False
        else:
            if path.startswith('/'):
                self._error = ('407', 'Forbidden', "Non-proxy traffic not accepted by proxy (407)")
                return
            parts = path.split('/', 3)
            while len(parts) <= 3:
                parts.append('')
            assert parts[0].lower() in ['http:', 'https:'], parts
            assert not parts[1], parts
            host = parts[2]
            path = '/' + parts[3]
            
            port = 80
            if parts[0].lower() == 'https:':
                port = 443
            
            if ':' in host:
                host, sport = host.split(':', 1)
                try:
                    port = int(sport)
                except ValueError:
                    pass
            if port == 80:
                hostandport = '%s' % (host)
            else:
                hostandport = '%s:%s' % (host, port)
                               
            self._client_log.update(dict(host=hostandport))
            authorized = self._get_authorized_ip(host, port)
            if not authorized:
                self._error = ('407', 'Not authorized', "Access to %s:%s not permitted by gateway policy (407)" % (host, port))
                return
            target, hashtags = authorized
            if '#deny' in hashtags:
                self._error = ('407', 'Denied', "Access to %s:%s denied by gateway policy (407)" % (host, port))
                return
            
            enable_ssl = parts[0].lower() == 'https:'

        assert ':' not in target[0], target

        authkind = None
        authtags = [x for x in hashtags if x.startswith('#auth')]
        if authtags:
            authkind = '*'
            if '=' in authtags[0]:
                authkind = authtags[0].split('=', 1)[1]
        
        ssl_disable_certificate_verification = False
        ssl_disable_certificate_verification_tags = [x for x in hashtags if x.startswith('#disable_certificate_verification')]
        if ssl_disable_certificate_verification_tags:
            ssl_disable_certificate_verification = True
        
        proxytags = [x for x in hashtags if x.startswith('#httpproxy=')]
        traffic_to_proxy = False
        if proxytags:
            proxyhost, proxysport = proxytags[0].split('=', 1)[1].split(':', 1)
            try:
                proxyport = int(proxysport)
            except ValueError:
                proxyport = 3128
            target = proxyhost, proxyport
            if not enable_ssl:
                path = 'http://' + hostandport + path
            traffic_to_proxy = True
        # TODO: use and clean hashtags

#        enable_ssl = parts[0].lower() == 'https:'

        req = httpclientengine.HttpRequest(method=method, path=path, host=hostandport, enable_ssl=enable_ssl, ssl_disable_certificate_verification=ssl_disable_certificate_verification) # overspecified?
        req.authkind = authkind
        start_line = '%s %s %s' % (method, path, protocol)
        req.headers = mime.Headers(start_line=start_line, items=headers.items())
        if method == 'MOVE':
            reqhost = req.headers.get('Host')
            dest = req.headers.get('Destination')
            if reqhost and dest and dest.lower().startswith('http://%s/' % reqhost.lower()):
                req.headers.replace('Destination', dest[len('http://') + len(reqhost):])
        req.headers.replace('Host', calc_host_header(parts[0].lower(), host, port))
        # RFC 2616 13.5.1: hop-by-hop headers that should be removed ... assuming we are a proxy and not a gateway:
        req.headers.replace('Proxy-Connection', None)
        req.headers.replace('Proxy-Authenticate', None)
        req.headers.replace('Proxy-Authorization', None)
        req.headers.replace('Keep-Alive', None)
        req.headers.replace('TE', None)
        req.headers.replace('Trailers', None)
        req.headers.replace('Transfer-Encoding', None)
        req.headers.replace('Upgrade', None)
        req.headers.add('Via', '1.1 groxy')

        rimheadertags = [x for x in hashtags if x.startswith('#add-rim-device-email-header')]
        if rimheadertags:
            req.headers.add('Rim-device-email', self._username)

        dme_headertags = [x for x in hashtags if x.startswith('#add-dme-headers')]
        if dme_headertags:
            for header_name in self.DME_HEADER_NAMES:
                variable = "%%(dme.%s)" % header_name
                header_value = self._expand_variables(variable)
                if header_value != variable:
                    req.headers.add(header_name, header_value)

        if target not in self._clients:
            self._env.log2('new client to %s:%s', target[0], target[1])
            self._clients[target] = ProxyServerHttpClient(self._env, self._connector, target, self, self._username, self._password, enable_ssl=enable_ssl, traffic_to_proxy=traffic_to_proxy, ssl_disable_certificate_verification=ssl_disable_certificate_verification)
        self._client = self._clients[target]
        self._client.send_http_request_start(req) # req body will follow soon, so we can't do this async on connected

    # Implement virtual method
    def got_http_request_fragment(self, chunk):
        if not self._error and self._client:
            self._client.send_http_request_fragment(chunk)

    # Implement virtual method
    def got_http_request_done(self):
        if self._error:
            assert not self._client, self._client
            status, msg, body = self._error
            self._error = None
            self._env.log2('%s got request done but sending %s: %s', self, status, msg)
            self._server_engine.send_http_error_response(status, msg, body)
        else:
            assert self._client
            self._responding = True
            self._client.send_http_request_done()
            

    def cancel_client(self, target, client):
        if client is self._client:
            if self._responding:
                self._env.log2('%s got cancel from active %s to %s:%s - closing', self, client, target[0], target[1])
                self.close()
            else:
                self._error = ('500', 'Internal Error', "Error processing request")
            if self._client_log:
                if self._error:
                    self._client_log.update(dict(log_type="http_request_error", http_error=self._error))
                else:
                    self._client_log["log_type"] = "http_request_cancelled"
                self._env.log0(None, **self._client_log)
                # ('404', "Not Found", "Gateway couldn't connect to %s" % target)
        elif self._clients.get(target) is client:
            self._env.log2('%s got cancel from waiting %s to %s:%s', self, client, target[0], target[1])
            self.close()
        elif client is self._tunnel:
            #print 'responding:', self._responding
            if self._responding:
                self._env.log2('%s got cancel from active tunnel %s to %s:%s - closing', self, client, target[0], target[1])
                self.close()
            else:
                self._error = ('500', 'Internal Error', "Error processing request")
        else: # client has been replaced by something else for some reason
            self._env.log2('%s got spurious cancel from forgotten %s to %s:%s', self, client, target[0], target[1])
            self.close()

        self._clients.pop(target, None)
        self._client = None
        self._client_log = None
        
#        if self._cb_close:
#            self._cb_close()
#            self._cb_close = None
#        self._cb_will_listen = None
#        self._cb_to_client = None
        

    # Implement virtual method
    def got_tunnel_connect_request(self, host):
        self._env.log1('CONNECT %s', host)
        assert self._client is None, self._client # one at a time
        assert self._tunnel is None, self._tunnel # one at a time

        hostname, sport = host.split(':', 1)
        try:
            port = int(sport)
        except ValueError:
            port = 0
        authorized = self._get_authorized_ip(hostname, port)
        if authorized:
            target, _hashtags = authorized
            self._tunnel = ProxyServerTunnelClient(self._env, self._connector, target, self)
        else:
            self._server_engine.send_http_error_response('407', 'Forbidden', "Access to resource denied by gateway policy: %s (407)" % host, close=True)

    def got_http_response_start(self, status, headers, length):
        if self._server_engine is None:
            self._env.log1('got_http_response_start without server engine')
            return
        
        self._env.log1(' -> %r Content-Length: %s Content-Type: %s', headers.start_line, length, headers.get('Content-Type'))
        assert self._error is None, self._error
        if self._client_log:
            self._client_log.update(dict(status_code=status, content_length=length))
        if length == httpclientengine.HttpClientEngine.CLOSE:
            length = httpclientengine.HttpClientEngine.CHUNKED
            headers.replace('Transfer-Encoding', 'Chunked')
            headers.replace('Connection', None) # not Close, so we can ignore alive in ..._done
        self._server_engine.send_http_response_start(headers, chunkify=length == httpclientengine.HttpClientEngine.CHUNKED)

    def got_http_response_fragment(self, chunk):
        if self._server_engine is None:
            self._env.log1('got_http_response_fragment without server engine')
            return

        self._server_engine.send_http_response_next(chunk)

    def got_http_response_done(self, alive):
        if self._server_engine is None:
            self._env.log1('got_http_response_done without server engine')
            return

        self._env.log2('%s got response done with %s, alive=%s', self, self._client, alive) # report length too?
        self._server_engine.send_http_response_done(close=False) # ignoring alive
        self._responding = False
        self._client = None
        if self._client_log:
            self._client_log["log_type"] = "http_request_done"
            self._env.log0(None, **self._client_log)
            self._client_log = None

    # Implement virtual method
    def got_tunnel_fragment(self, chunk):
        if self._tunnel:
            self._env.log3('%s got_tunnel_fragment %s', self, len(chunk))
            self._tunnel.to_server(chunk)
            return ''
        self._env.log2('%s ignoring got_tunnel_fragment %s', self, len(chunk))

    # implemented callbacks from http engine:

    def will_listen(self, ready):
        self._env.log3('%s will listen is %s', self, ready)
        self._cb_will_listen(ready)

    def to_client(self, buf):
        self._env.log3('%s added %r to send buffer', self, buf)
        self._cb_to_client(buf)

    # implemented asyncserver callback:

    def tcp_from_client(self, data_chunk):
        self._env.log3('%s tcp_from_client: %r', self, data_chunk)
        return self._server_engine.tcp_from_client(data_chunk)

    # encapsulate for tunnel:

    def send_tunnel_confirmation(self):
        self._responding = True
        self._server_engine.send_tunnel_confirmation()

    def tunnel_not_connected(self, msg):
        self._env.log1('Tunnel connection failed, responding 404: %s', msg)
        self._server_engine.send_http_error_response('404', "Not Found", "Resource not found (404)")

    # encapsulate for auth errors:

    def send_http_error_response(self, status, msg, body):
        self._server_engine.send_http_error_response(status, msg, body)

class ProxyServerSSLConnectClient(httpclientengine.HttpClientEngineCallbacks):

    def __init__(self, proxy_server):
        self.proxy_server = proxy_server
        self._pending_auth = False
        self._pending_req = self._pending_fragments = self._pending_len = None
    
    def to_server(self, buf):
        self.proxy_server.to_server(buf)

    def got_http_response_start(self, status, headers, length):
        self.proxy_server.got_ssl_connect_response_start(status, headers, length)
        
    def got_http_response_fragment(self, chunk):
        pass

    def got_http_response_done(self, alive):
        self.proxy_server.got_ssl_connect_response_done(alive)

    def got_http_cancel(self, msg=None):
        self.proxy_server.got_ssl_connect_response_cancel(msg)
    


class ProxyServerHttpClient(httpclientengine.HttpClientEngineCallbacks):
    
    _count = 0

    def __init__(self, env, connector, target, callbacks, username, password, enable_ssl=False, traffic_to_proxy=False, ssl_disable_certificate_verification=False):
        httpclientengine.HttpClientEngineCallbacks.__init__(self)
        # Count for identifying requests and responses in log
        ProxyServerHttpClient._count += 1
        if ProxyServerHttpClient._count > 10000:
            ProxyServerHttpClient._count = 1
        self._client_id = ProxyServerHttpClient._count 

        enable_ssl_in_tcp = enable_ssl if not traffic_to_proxy else False
        self.traffic_to_proxy = traffic_to_proxy
        self.ssl_proxy = traffic_to_proxy and enable_ssl
        self.intialize_ssl_connect = self.ssl_proxy
        self.intialize_ssl_connect_succeeded = True
        self._ssl_connect_req = None
        
        self._env = env
        self._target = target
        self._callbacks = callbacks # we might call got_http_cancel from base constructor
        self._auth = httpauth.Auth(self._env, username, password)
        self._pending_auth = False
        self._pending_req = self._pending_fragments = self._pending_len = None
        self._write = None
        self.close = lambda : None
        self._client_engine = httpclientengine.HttpClientEngine(env, self)
        self._ssl_client_engine = None
        self._ssl_callback_client = None
        self._ssl_host = "";
        self._ssl_disable_certificate_verification = ssl_disable_certificate_verification
        connector(target, cb_tcp_connected=self.tcp_connected, cb_tcp_error=self.got_http_cancel, enable_ssl=enable_ssl_in_tcp, disable_certificate_verification=self._ssl_disable_certificate_verification)
        self._env.log3('%s started', self)
        


    # used as connector callback
    def tcp_connected(self, local, remote, write, close):
        self._env.log2('%s connected from %s:%s to %s:%s - forwarding request (%d)', self,
                local[0], local[1], remote[0], remote[1], self._client_id)
        self._write = write
        self.close = close
        if self._pending_req:
            fragments = self._pending_fragments
            if self._send_http_request_start():
                for fragment in fragments:
                    if fragment is not None:
                        self._client_engine.send_http_request_fragment(fragment)
                    else:
                        self._client_engine.send_http_request_done()
        return (self.from_server, self.got_http_cancel)

    def from_server(self, buf):
        self._env.log3('%s from_server %s (%d)', self, len(buf), self._client_id)
        if self._ssl_client_engine:
            return self._ssl_client_engine.from_server(buf)
        else:
            return self._client_engine.from_server(buf)

    def _send_http_request_start(self):
        req = self._pending_req

        if self._write:
            if self.intialize_ssl_connect:
                # Send connect request to proxy in order to create ssl tunnel
                method = 'CONNECT'
                hostandport = self._pending_req.host
                self._ssl_host = hostandport.split(":")[0]
                req = httpclientengine.HttpRequest(method=method, path=hostandport, host=hostandport, enable_ssl=False) # overspecified?
                req.authkind = self._pending_req.authkind
                start_line = '%s %s %s' % (method, hostandport, "HTTP/1.1")
                req.headers = mime.Headers(start_line=start_line, items=[])
                req.headers.replace('Host', hostandport)
                self.intialize_ssl_connect_succeeded = False
                self._ssl_callback_client = ProxyServerSSLConnectClient(self)
                self._ssl_client_engine = httpclientengine.HttpClientEngine(self._env, self._ssl_callback_client)
                self._env.log2('intialize_ssl_connect %s (%d)', hostandport, self._client_id)
                self._auth.initialize(req)
                self._ssl_client_engine.send_http_request(req)
                self._ssl_connect_req = req
            else:
                if not self.ssl_proxy:
                    self._auth.initialize(self._pending_req)
                #print 'really sending', self._pending_req.headers
                self._client_engine.send_http_request_start(req)
                self._env.log2('send_http_request_start to %s (%d)', req.host, self._client_id)
                
                return True
        return False
        
    def send_http_request_start(self, req):
        
        self._pending_req = req
        self._pending_fragments = []
        self._pending_len = 0
        return self._send_http_request_start()


    def got_ssl_connect_response_start(self, status, headers, length):
        # Receive respone to request from proxy to ssl tunnel
        self._env.log2('got_ssl_connect_response_start %s (%d)', status, self._client_id)
        if self._auth.react(status, headers, self._ssl_connect_req):
            self._env.log2('%s retrying with next phase of auth (%d)', self, self._client_id)
            self._pending_auth = True # mute rest of response
            return
        self.intialize_ssl_connect_succeeded = int(status) == 200 
        
    def got_ssl_connect_response_done(self, alive):
        # Receive respone to request from proxy to ssl tunnel
        self._env.log2('got_ssl_connect_response_done (%d)', self._client_id)
        if self._pending_auth:
            if alive:
                assert self._ssl_connect_req
                # assert alive # hopefully ...
                self._env.log1('replaying request with new credentials: %s (%d)', self._ssl_connect_req.headers.start_line, self._client_id)
                #print self._pending_req.headers
                self._ssl_client_engine.send_http_request(self._ssl_connect_req)
            else:
                self._env.log1('pending auth but connection not alive')
                self._callbacks.send_http_error_response('407', "Forbidden", "Internal authentication failed (407)")
                self._callbacks.cancel_client(self._target, self)
            self._pending_auth = False
            
        elif (self.intialize_ssl_connect_succeeded):
            self.intialize_ssl_connect = False
            self._ssl_client_engine = None
            self._ssl_callback_client = None
            # upgrade tcp connection to ssl
            self._write(None, self._ssl_host, self._ssl_disable_certificate_verification)
            self._env.log2('send ssl_connect %s (%d) %s', self._ssl_host, self._client_id, self._ssl_disable_certificate_verification)
            if self._pending_req:
                self._env.log2("sending pending req host='%s'", self._pending_req.host)
                fragments = self._pending_fragments # if self._pending_fragments else [None] 
                self._send_http_request_start()
                for fragment in fragments:
                    if fragment is not None:
                        self._client_engine.send_http_request_fragment(fragment)
                    else:
                        self._client_engine.send_http_request_done()
        else:
            self._env.log1('ssl_connect failed (%d)', self._client_id)
            self._callbacks.cancel_client(self._target, self)
            
            

    def got_ssl_connect_response_cancel(self, msg=None):
        self._env.log1('got_ssl_connect_response_cancel %s (%d)', msg, self._client_id)
        self.intialize_ssl_connect_succeeded = False
        self._callbacks.cancel_client(self._target, self)

    def send_http_request_fragment(self, chunk):
        self._pending_fragments.append(chunk)
        self._pending_len += len(chunk)
        assert self._pending_len < self._env.config.MAX_REQUEST_BUFFER_MB * 1000 * 1000 # TODO: fail more decently
        if self._write:
            self._client_engine.send_http_request_fragment(chunk)

    def send_http_request_done(self):
        if self._write:
            self._client_engine.send_http_request_done()
        else:
            self._pending_fragments.append(None)

    # Implement virtual method
    # used as connector tcp_error callback
    def got_http_cancel(self, msg=None):
        self._env.log2('%s got cancelled: %s (%d)', self, msg, self._client_id)
        self._callbacks.cancel_client(self._target, self)
#        self._write = None
#        self.close = lambda : None
#        self._callbacks =  None
#        self._auth = None
#        self._client_engine = None
#        self._target = None

    def got_http_response_start(self, status, headers, length):
        self._env.log2('got_http_response_start %s %s (%d)', status, length, self._client_id)
        #print 'got response', headers
        assert self._pending_req
        react = not(self.traffic_to_proxy and status=='401')
        if not react:
            self._env.log1('Ignoring 401 response for traffic through proxy, host=%s', self._pending_req.host)
        if react and self._auth.react(status, headers, self._pending_req):
            self._env.log2('%s retrying with next phase of auth (%d)', self, self._client_id)
            self._pending_auth = True # mute rest of response
            return
        elif self._pending_auth:
            self._env.log2('%s completed auth (%d)', self, self._client_id)
            self._pending_auth = False
        self._pending_req = self._pending_fragments = self._pending_len = None
        self._callbacks.got_http_response_start(status, headers, length)

    def got_http_response_fragment(self, chunk):
        if self._pending_auth:
            self._env.log2('%s ignored reponse while negotiating auth: %r (%d)', self, chunk, self._client_id)
        else:
            self._callbacks.got_http_response_fragment(chunk)

    def got_http_response_done(self, alive):
        if self._pending_auth:
            if alive:
                assert self._pending_req
                # assert alive # hopefully ...
                self._env.log1('replaying request with new credentials: %s (%d)', self._pending_req.headers.start_line, self._client_id)
                #print self._pending_req.headers
                self._client_engine.send_http_request_start(self._pending_req)
                for chunk in self._pending_fragments:
                    if chunk is not None:
                        self._client_engine.send_http_request_fragment(chunk)
                    else:
                        self._client_engine.send_http_request_done()
                #self._client_engine.send_http_request_done()
                return # we will get another got_http_response_done
            else:
                self._env.log1('pending auth but connection not alive')
                self._callbacks.send_http_error_response('403', "Forbidden", "Internal authentication failed (403)")
                self._callbacks.cancel_client(self._target, self)
        else:
            self._callbacks.got_http_response_done(alive)
            if not alive:
                self._callbacks.cancel_client(self._target, self)

    # implemented callbacks from http engine:

    def to_server(self, buf):
        self._env.log3('%s to_server %s (%d)', self, len(buf), self._client_id)
        self._write(buf)


class ProxyServerTunnelClient(object):

    def __init__(self, env, connector, target, callbacks):
        self._env = env
        self._target = target
        self._callbacks = callbacks # we might call got_http_cancel from base constructor
        self._write = None
        self.close = lambda : None
        connector(target, cb_tcp_connected=self.tcp_connected, cb_tcp_error=self.not_connected)
        self._env.log3('%s started', self)

    def tcp_connected(self, local, remote, write, close):
        self._env.log2('%s connected from %s:%s to %s:%s - sending tunnel confirmation', self,
                local[0], local[1], remote[0], remote[1])
        self._write = write
        self.close = close
        self._callbacks.send_tunnel_confirmation()
        return (self.from_server, self.connection_error)

    def not_connected(self, msg=None):
        if msg:
            self._env.log2('%s connection failure: %s', self, msg)
        self._callbacks.tunnel_not_connected(msg)

    def connection_error(self, msg=None):
        if msg:
            self._env.log2('%s closed: %s', self, msg)
        self._callbacks.cancel_client(self._target, self)

    def from_server(self, buf):
        self._env.log3('%s from_server %s', self, len(buf))
        assert self._write
        self._callbacks.to_client(buf)
        return ''

    # implemented callbacks from parent

    def to_server(self, buf):
        self._env.log3('%s to_server %s', self, len(buf))
        assert self._write
        self._write(buf)
