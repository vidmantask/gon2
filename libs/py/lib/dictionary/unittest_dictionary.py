"""
Unittest of lib.dicitonary 
"""
from StringIO import StringIO
import shutil
import tempfile
import os
import StringIO

import unittest
import lib.giri_unittest as giri_unittest

import lib.checkpoint
import lib.dictionary
import lib.dictionary.dictionary_spec as dictionary_spec



spec_001_str = """<?xml version="1.0" encoding="UTF-8"?>
<dictionary>
  <header lang_name='en' lang_sub_name='us' description='My dictionary'/>
  
  <translations>
    <translation message='Hey you 1'>Hej du 1</translation>
    <translation message='Hey you 2'>Hej du 2</translation>
    <translation message='Hey you 3'>Hej du 3</translation>

    <translation_by_id message_id='1'>Hej du med id 1</translation_by_id>
    <translation_by_id message_id='2' message_sub_id='1'>Hej du med id 2::1</translation_by_id>
    <translation_by_id message_id='ERROR' message_sub_id='0001'>Hej du med fejl i filen '%s' og pakken '%s'</translation_by_id>
    <translation_by_id message_id='ERROR' message_sub_id='0005'>Hej du med fejl i filen '%s'</translation_by_id>
  </translations>
</dictionary>"""


class Dictionary_spec(unittest.TestCase):
    def test_parse_and_dump(self):
        spec_001_file = StringIO.StringIO(spec_001_str)
        spec_001 = dictionary_spec.Dictionary.parse_from_file(spec_001_file)
        
        spec_002_file = StringIO.StringIO()
        spec_001.to_tree().write(spec_002_file)
        
        spec_003_file = StringIO.StringIO(spec_002_file.getvalue())
        spec_003 = dictionary_spec.Dictionary.parse_from_file(spec_003_file)

        self.assertEqual(spec_003.header.lang_name, 'en')
        self.assertEqual(spec_003.header.lang_name, spec_001.header.lang_name)

        self.assertEqual(len(spec_003.translations.translations), 3)
        self.assertEqual(len(spec_003.translations.translations_by_id), 4)
        

class Dictionary_lookup(unittest.TestCase):
    def test_lookup(self):
        checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
        spec_001_file = StringIO.StringIO(spec_001_str)
        dictionary = lib.dictionary.Dictionary.create_from_spec_fp(checkpoint_handler, spec_001_file)
        
        self.assertEqual(dictionary.gettext('Hey you 1'), 'Hej du 1')
        self.assertEqual(dictionary._('Hey you 1'), 'Hej du 1')
        self.assertEqual(dictionary.gettext_id('1'), 'Hej du med id 1')
        self.assertEqual(dictionary.gettext_id('2::1'), 'Hej du med id 2::1')



class Dictionary_deftext(unittest.TestCase):
    def test_deftext(self):
        lib.dictionary.deftext_id('ERROR::0001', "The package file '%s' was not found for the package %s.", ('GPM Filename', 'PPM Package Id'))
        lib.dictionary.deftext_id('ERROR::0005', "Error accessing the package file '%s'.", ('GPM Filename'))

        checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
        spec_001_file = StringIO.StringIO(spec_001_str)
        dictionary = lib.dictionary.Dictionary.create_from_spec_fp(checkpoint_handler, spec_001_file)

        self.assertNotEqual( dictionary.gettext_id('ERROR::0001', ('xxxx', 'yyyy')), dictionary.gettext_id('ERROR::0001', ('xxxx', 'yyyy', 'zzzz')) ) 


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
