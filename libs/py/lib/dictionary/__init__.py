"""
Module for holding dictionary support functionlaity
"""
import sys
import os.path

import dictionary_spec
# import lib.checkpoint


MODULE_ID = 'dictionary'
DICTIONARY_SUFIX = '.dictionary.xml'



_deftexts = []
def deftext_id(message_id, message, message_args_description=()):
    """
    lib.dictionary.deftext_id('GPM_ERROR::0001', "The package file '%s' was not found for the package %s.", ('GPM Filename', 'PPM Package Id'))
    """
    _deftexts.append( (message_id, message, message_args_description) )


def generate_dictionary_filename(lang_name=None, lang_sub_name=None):
    filename = lang_name
    if lang_sub_name is not None:
        filename += '-%s' % lang_sub_name
    filename += DICTIONARY_SUFIX
    return filename


class DictionaryLibrary(object):
    def  __init__(self):
        self._dictionarys = []


class LookupError(Exception):
    """
    Raise this excetption in case of message lookup problem (if throw_on_error)
    """
    def __init__(self, message):
        self._message = message

    def __str__(self):
        return self._message


class Dictionary(object):
    def  __init__(self, checkpoint_handler=None):
        self.checkpoint_handler = checkpoint_handler
        self.lang_name = None
        self.lang_sub_name = None
        self.translations = {}
        self.translations_by_id = {}

    def _(self, message):
        """
        Synonym for gettext
        """
        return self.gettext(message)

    def gettext(self, message):
        """
        Look up the message in the dictionary, if a translation is found  the translations is
        returned, otherwise the is message returned.

        The result is always unicode.
        """
        if self.translations.has_key(message):
            return self.translations[message]
        return message

    def gettext_id(self, message_id, message_args=(), message_sub_id=None, add_exc_info=False):
        """
        Lookup a message by id (and eventualy sub id) if the message is found the message is returned.
        If the message is not found then and errormessage is returned as the message.
        """
        message_translated = None
        message_id_used = self._build_message_id(message_id, message_sub_id)
        if self.translations_by_id.has_key(message_id_used):
            try:
                message_translated = self.translations_by_id[message_id_used] % message_args
            except TypeError:
                message_translated = self.translations_by_id[message_id_used]
                # if self.checkpoint_handler is not None:
                #     self.checkpoint_handler.Checkpoint("Dictionary::format", MODULE_ID, lib.checkpoint.ERROR, message_id=message_id, message=self.translations_by_id[message_id_used])

        if message_translated is None:
            for (deftext_message_id, deftext_message, deftext_message_args_description) in _deftexts:
                if deftext_message_id == message_id:
                    try:
                        message_translated = deftext_message % message_args
                    except TypeError:
                        message_translated = deftext_message + "(Unable to do proper translation message_id:'%s')" % message_id
                        # if self.checkpoint_handler is not None:
                        #     self.checkpoint_handler.Checkpoint("Dictionary::format", MODULE_ID, lib.checkpoint.ERROR, message_id=message_id, message=deftext_message)
                    break

        if message_translated is None:
            message_translated = u"Text not found in dictionary. Id was %s" % message_id_used
            # if self.checkpoint_handler is not None:
            #     self.checkpoint_handler.Checkpoint("Dictionary::lookup", MODULE_ID, lib.checkpoint.ERROR, message_id=message_id)

        if add_exc_info:
            etype, evalue, etraceback = sys.exc_info()
            message_translated += '\nException Info:%s' % (str(evalue))
        return message_translated

    def _build_message_id(self, message_id, message_sub_id=None):
        if message_sub_id is not None:
            return '%s::%s' %(message_id, message_sub_id)
        return message_id

    def build_from_spec(self, spec):
        self.lang_name = spec.header.lang_name
        self.lang_sub_name = spec.header.lang_sub_name

        self.translations = {}
        for spec_translation in spec.translations.translations:
            self.translations[spec_translation.message] = spec_translation.translation

        self.translations_by_id = {}
        for spec_translation_by_id in spec.translations.translations_by_id:
            message_id_used = self._build_message_id(spec_translation_by_id.message_id, spec_translation_by_id.message_sub_id)
            self.translations_by_id[message_id_used] = spec_translation_by_id.translation

    @classmethod
    def create_from_spec_fp(cls, checkpoint_handler, fp):
        dictionary = Dictionary(checkpoint_handler)
        spec = dictionary_spec.Dictionary.parse_from_file(fp)
        dictionary.build_from_spec(spec)
        return dictionary
