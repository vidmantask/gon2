"""
This module contains classes for handling dictionary specifications
"""
import os
import os.path
import glob
import string
import sys

import lib.utility
import lib.version

import elementtree.ElementTree as element_tree


#
# Helper functions for parsing and building
#
def set_node_attrib(node, name, value):
    if value != None:
        node.attrib[name] = value

def set_node_attrib_int(node, name, value):
    if value != None:
        node.attrib[name] = "%i" % value

def set_node_content(node, value):
    if node is not None:
        node.text = value

def add_as_node(gpm_instance, parent_node, *args):
    if gpm_instance != None:
        gpm_instance.add_as_node(parent_node, *args)

def get_node_attrib_int(node, name):
    if node.attrib.has_key(name):
        return int(node.attrib[name])
    return None
    
def get_node_attrib_string(node, name):
    if node.attrib.has_key(name):
        return node.attrib[name]
    return None

def get_node_content(node):
    if node is not None:
        return node.text
    return None




class DictionaryHeader(object):
    """
    Represent a header in the dictionary
    
    e.g. 
      <header lang_name='' lang_sub_name = '' description = ''/>
    """
    TAG_NAME = 'header'
    ATTRIB_NAME_lang_name = 'lang_name'
    ATTRIB_NAME_lang_sub_name = 'lang_sub_name'
    ATTRIB_NAME_description = 'description'

    def __init__(self):
        self.lang_name = None
        self.lang_sub_name = None
        self.description = None

    @classmethod
    def parse_from_parent_node(cls, parent_node):
        header_node = parent_node.find(DictionaryHeader.TAG_NAME)
        if header_node != None:
            dictionary_header = DictionaryHeader()
            dictionary_header.lang_name = get_node_attrib_string(header_node, DictionaryHeader.ATTRIB_NAME_lang_name)
            dictionary_header.lang_sub_name = get_node_attrib_string(header_node, DictionaryHeader.ATTRIB_NAME_lang_sub_name)
            dictionary_header.description = get_node_attrib_string(header_node, DictionaryHeader.ATTRIB_NAME_description)
            return dictionary_header
        return None
    
    def add_as_node(self, parent_node):
        header_node = element_tree.SubElement(parent_node, DictionaryHeader.TAG_NAME)
        set_node_attrib(header_node, DictionaryHeader.ATTRIB_NAME_lang_name, self.lang_name)
        set_node_attrib(header_node, DictionaryHeader.ATTRIB_NAME_lang_sub_name, self.lang_sub_name)
        set_node_attrib(header_node, DictionaryHeader.ATTRIB_NAME_description, self.description)

    def validate(self, validate_env, error_handler):
        if self.lang_name is None:
            error_handler.emmit("header-tag don't contain a value for the lang_name attribute")
        if self.lang_sub_name is None:
            error_handler.emmit("header-tag don't contain a value for the lang_sub_name attribute")


class DictionaryTranslation(object):
    """
    e.g.
       <translation message="xxxxx">yyyyy</translation>
    """
    TAG_NAME = 'translation'
    ATTRIB_NAME_message = 'message'

    def __init__(self):
        self.message = None
        self.translation = None
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(DictionaryTranslation.TAG_NAME)
        for node in nodes:
            dictionary_translation = DictionaryTranslation()
            dictionary_translation.message = get_node_attrib_string(node, DictionaryTranslation.ATTRIB_NAME_message)
            dictionary_translation.translation = get_node_content(node)
            result.append(dictionary_translation)
        return result

    @classmethod
    def add_as_nodes(cls, translations, parent_node):
        for t  in translations:
            node = element_tree.SubElement(parent_node, DictionaryTranslation.TAG_NAME)
            set_node_attrib(node, DictionaryTranslation.ATTRIB_NAME_message, t.message)
            set_node_content(node, t.translation)

    def validate(self, validate_env, error_handler):
        pass


class DictionaryTranslationById(object):
    """
    e.g.
        <translation_by_id message_id="xxxxx" message_sub_id='yyy'>zzzz</translation_by_id>
    """
    TAG_NAME = 'translation_by_id'
    ATTRIB_NAME_message_id = 'message_id'
    ATTRIB_NAME_message_sub_id = 'message_sub_id'

    def __init__(self):
        self.message_id = None
        self.message_sub_id = None
        self.translation = None
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(DictionaryTranslationById.TAG_NAME)
        for node in nodes:
            dictionary_translation_by_id = DictionaryTranslationById()
            dictionary_translation_by_id.message_id = get_node_attrib_string(node, DictionaryTranslationById.ATTRIB_NAME_message_id)
            dictionary_translation_by_id.message_sub_id = get_node_attrib_string(node, DictionaryTranslationById.ATTRIB_NAME_message_sub_id)
            dictionary_translation_by_id.translation = get_node_content(node)
            result.append(dictionary_translation_by_id)
        return result

    @classmethod
    def add_as_nodes(cls, translations_by_id, parent_node):
        for t  in translations_by_id:
            node = element_tree.SubElement(parent_node, DictionaryTranslationById.TAG_NAME)
            set_node_attrib(node, DictionaryTranslationById.ATTRIB_NAME_message_id, t.message_id)
            set_node_attrib(node, DictionaryTranslationById.ATTRIB_NAME_message_sub_id, t.message_sub_id)
            set_node_content(node, t.translation)

    def validate(self, validate_env, error_handler):
        pass

    def set_message_id(self, message_id, message_sub_id=None):
        if message_id.find('::') > 0:
            message_id_elements = message_id.split('::')
            message_id = message_id_elements[0]
            if message_sub_id is None:
                message_sub_id = message_id_elements[1]
        self.message_id = message_id
        self.message_sub_id = message_sub_id
        

class DictionaryTranslations(object):
    """
    e.g.
      <translations>
        <translation message="xxxxx">yyyyy</translation>
        <translation_by_id message_id="xxxxx">zzzz</translation_by_id>
      </translations>
    """
    TAG_NAME = 'translations'

    def __init__(self):
        self.translations = []
        self.translations_by_id = []
        
    @classmethod
    def parse_from_parent_node(cls, parent_node):
        node = parent_node.find(DictionaryTranslations.TAG_NAME)
        if node != None:
            dictionary_translations = DictionaryTranslations()
            dictionary_translations.translations = DictionaryTranslation.parse_from_parent_node_to_list(node)
            dictionary_translations.translations_by_id = DictionaryTranslationById.parse_from_parent_node_to_list(node)
            return dictionary_translations
        return None
    
    def add_as_node(self, parent_node):
        node = element_tree.SubElement(parent_node, DictionaryTranslations.TAG_NAME)
        DictionaryTranslation.add_as_nodes(self.translations, node)
        DictionaryTranslationById.add_as_nodes(self.translations_by_id, node)
        return node

    def validate(self, validate_env, error_handler):
        pass



class Dictionary(object):
    """
    e.g.
      <dictionary>
        <header>..</header>
        <translations>..</translations>
      </dictionary>
    """
    TAG_NAME = 'dictionary'

    def __init__(self):
        self.header = None
        self.translations = None

    @classmethod
    def parse_from_file(cls, file):
        root = element_tree.ElementTree(None, file)
        if root != None:
            dictionary = Dictionary()
            dictionary.header = DictionaryHeader.parse_from_parent_node(root)
            dictionary.translations = DictionaryTranslations.parse_from_parent_node(root)
            return dictionary
        return None

    def to_node(self):
        node = element_tree.Element(Dictionary.TAG_NAME)
        self.header.add_as_node(node)
        if self.translations != None:
            self.translations.add_as_node(node)
        return node
        
    def to_tree(self):
        return element_tree.ElementTree(self.to_node())

