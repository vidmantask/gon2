"""
This module cantains functionality for handling automatic text translation
"""
import xgoogle.translate
import urllib
import lxml.html


class AutoTranslator(object):
    def __init__(self):
        self.engine_name = None
        self.lang_name = None
        self.lang_sub_name = None
        self.description = None
        
    def translate(self, text):
        return text

    def get_languages(self):
        return []


class AutoTranslatorGoogle(AutoTranslator):
    def __init__(self, lang_name):
        self.engine_name = "Google Translate"
        self.lang_name = lang_name
        self.lang_sub_name = None
        self._translator = xgoogle.translate.Translator()
        self.description = self.translate('Auto translated from English to %s using GoogleTranslate' %  self.get_language_long_name())

    def translate(self, message):
        message = message.replace('%s', 'NOTRANSLATE_PCT_S')
        message = message.replace('%d', 'NOTRANSLATE_PCT_D')
        message_translated = self._translator.translate(message, lang_from='en', lang_to=self.lang_name)
        message_translated = message_translated.replace('NOTRANSLATE_PCT_S', '%s')
        message_translated = message_translated.replace('NOTRANSLATE_PCT_D', '%d')
        return message_translated

    def get_language_long_name(self):
        return xgoogle.translate._languages[self.lang_name]

    def get_languages(self):
        return xgoogle.translate._languages.keys()



class AutoTranslatorDev(AutoTranslator):
    def __init__(self):
        self.lang_name = 'dev'
        self.lang_sub_name = None
        self.description = 'Dev translation for discovery messages in source not properly tagged.'
        self._translate_text  = self._generate_translate_text()
    
    def _generate_translate_text(self):
        translate_text = []
        for idx in range(0, 256):
            if idx == ord(' '):
                translate_text.append(u' ')
            else:
                translate_text.append(u'x')
        return translate_text
        
    def translate(self, message):
        message = unicode(message)
        poss_s = self._get_positions(message, '%s') 
        poss_d = self._get_positions(message, '%d') 
        message_translated = message.translate(self._translate_text)
        message_translated = self._set_positions(poss_s, message_translated, '%s')
        message_translated = self._set_positions(poss_d, message_translated, '%d')
        return message_translated
 
    def _get_positions(self, text, text_to_find):
        poss = []
        pos = text.find(text_to_find)
        while pos > -1:
            poss.append(pos)
            pos = text.find(text_to_find, pos+1)
        return poss
    
    def _set_positions(self, poss, text, text_to_replace):
        for pos in poss:
            text = text[:pos] + text_to_replace + text[pos+len(text_to_replace):]
        return text 

 
    def get_language_long_name(self):
        return "Dev translation"

    def get_languages(self):
        return ['dev']




class AutoTranslatorBabelfish(AutoTranslator):
    BABLEFISH_URL = 'http://www.microsofttranslator.com/translate_tx'
    
    def __init__(self, lang_name):
        self.engine_name = "Babelfish Translate"
        self.lang_name = lang_name
        self.lang_sub_name = None
        self._babelfish_lang = 'en_%s' % self.lang_name
        self.description = self.translate('Auto translated from English to %s using YahooBabelfish' %  self.get_language_long_name())

    def get_language_long_name(self):
        long_name = "Not Found"
        try:
            url = AutoTranslatorBabelfish.BABLEFISH_URL + '?' + urllib.urlencode({'trtext':'This is a test', 'lp':self._babelfish_lang})
            page = lxml.html.parse(url)
            for h1 in page.iter('h1'):
                long_name = h1.text
                break
        except:
            pass
        return long_name
    
    def translate(self, message):
        translation = 'Translation not found'
        try:
            url = AutoTranslatorBabelfish.BABLEFISH_URL + '?' + urllib.urlencode({'trtext':message, 'lp':self._babelfish_lang})
            page = lxml.html.parse(url)
            for div in page.iter('div'):
                style = div.get('style')
                if style is not None and 'padding:0.6em;' in style:
                    translation = div.text
        except:
            pass
        return translation

    def get_languages(self):
        lang_names = []
        url = AutoTranslatorBabelfish.BABLEFISH_URL
        page = lxml.html.parse(url)
        for option in page.iter('option'):
            babefish_lang = option.get('value')
            if babefish_lang.startswith('en_'):
                lang_names.append(babefish_lang.split('en_')[1])
        return lang_names


class AutoTranslatorDialectizer(AutoTranslator):
    DIALECTIZER_URL = 'http://www.rinkworks.com/dialect/dialectt.cgi'
    
    def __init__(self, lang_sub_name):
        self.engine_name = "Dialectizer Translate"
        self.lang_name = 'en'
        self.lang_sub_name = lang_sub_name
        self._dialect = lang_sub_name
        self._translator = xgoogle.translate.Translator()
#        self.description = self.translate('Auto translated from English to %s using GoogleTranslate' % self.get_language_long_name())

    def translate(self, message):
        translation = 'Translation not found'

        url = AutoTranslatorDialectizer.DIALECTIZER_URL + '?' + urllib.urlencode({'text':message, 'dialect':self._dialect})
        print url
        page = lxml.html.parse(url)
        form_found = None
        for form in page.iter('form'):
            print form.text
            form_found = form
        form_data = lxml.html.submit_form(form_found)                
        
        print lxml.html.tostring(form_data)
        
        for div in page.iter('div'):
            print div.text
        return translation

    def get_language_long_name(self):
        return xgoogle.translate._languages[self.lang_name]
    
    def get_languages(self):
        return xgoogle.translate._languages.keys()

##
#auto_translate = AutoTranslatorDev()
#print auto_translate.translate('%soo%sI would like a piece%d of chocolate and some %s coffe %se')





