"""
This module contains functionality for encode and decode strings
"""

import locale
import types

try:
    preferredencoding = locale.getpreferredencoding()
except:
    preferredencoding = None

if not preferredencoding:
    preferredencoding = 'utf-8'


def preferred(s, errors='replace'):
    """Encode s in the preferred encoding"""
    return s.encode(preferredencoding, errors)

def get_current_encoding():
    return preferredencoding


def safe_decode(s):
    try:
        if type(s) == types.UnicodeType:
            return s
        elif type(s) == types.StringType:
            return s.decode(get_current_encoding())
        s_as_string = repr(s)
        return s_as_string.decode(get_current_encoding())
    except:
        return s.decode('ascii', 'replace')

if __name__ == '__main__':
    print get_current_encoding()

    print safe_decode(u"hej \xf8")
    print safe_decode("hej \xf8")
    