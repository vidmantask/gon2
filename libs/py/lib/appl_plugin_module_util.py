"""
Utilities for packing plugin modules
"""
import os
import os.path
import lib.version

def modul_to_data_files(plugin_modules_root, module_base_name, module_cs_part, module_ra_part, module_element_result_prefix=None):
    """
    Generate a list of data_file specifications for py2exe
    """
    result = []
    modules_base_path = 'plugin_modules'
    module_base_path = os.path.join(modules_base_path, module_base_name)
    
    module_elements = [(modules_base_path, False),
                       (module_base_path, False),
                       (os.path.join(module_base_path, 'common'), True),
                       (os.path.join(module_base_path, '%s_common' % (module_cs_part)), True),
                       (os.path.join(module_base_path, '%s_%s' % (module_cs_part, module_ra_part)), True),
                       (os.path.join(module_base_path, '%s_%s_common' % (module_cs_part, module_ra_part)), True)
                       ]


    print module_elements


    module_elements_folders = []
    for (module_element, do_deep_copy) in module_elements:
        module_element_abs = os.path.join(os.path.abspath(plugin_modules_root), module_element)
        if(os.path.isdir(module_element_abs)):
            if do_deep_copy:
                module_elements_folders.append(module_element)
                for root, dirs, files in os.walk(module_element_abs):
                    for dir in dirs:
                        module_elements_folders.append(os.path.join(module_element, dir))
            else:
                module_elements_folders.append(module_element)

    for module_element in module_elements_folders:
        module_element_abs = os.path.join(os.path.abspath(plugin_modules_root), module_element)
        if(os.path.isdir(module_element_abs)):
            module_element_files = []
            for filename in os.listdir(module_element_abs):
                filename_abs = os.path.join(module_element_abs, filename)
                if not skip_data_file(filename_abs):
                    module_element_files.append(filename_abs)
            module_element_result_path = module_element
            if module_element_result_prefix is not None:
                module_element_result_path = os.path.join(module_element_result_prefix, module_element)
            result.append((module_element_result_path, module_element_files))
    return result


def module_to_data_files_pyinstaller(plugin_modules_root, module_base_name, module_cs_part, module_ra_part):
    """
    Generate a list of data_file specifications for pyinstaller
    """
    result = []
    modules_base_path = 'plugin_modules'
    module_base_path = os.path.join(modules_base_path, module_base_name)
    
    module_elements = [modules_base_path,
                       module_base_path,
                       os.path.join(module_base_path, 'common'),
                       os.path.join(module_base_path, '%s_common' % (module_cs_part)),
                       os.path.join(module_base_path, '%s_%s' % (module_cs_part, module_ra_part)),
                       os.path.join(module_base_path, '%s_%s_common' % (module_cs_part, module_ra_part))
                       ]

    for module_element in module_elements:
        module_element_abs = os.path.join(os.path.abspath(plugin_modules_root), module_element)
        if(os.path.isdir(module_element_abs)):
            for filename in os.listdir(module_element_abs):
                filename_abs = os.path.join(module_element_abs, filename)
                if not skip_data_file(filename_abs):
                    result.append( (filename_abs[len(os.path.abspath(plugin_modules_root))+1:], filename_abs, 'DATA') )
    return result


def module_to_data_files_cxfreeze(plugin_modules_root, module_base_name, module_cs_part, module_ra_part):
    result = []
    data_files = module_to_data_files_pyinstaller(plugin_modules_root, module_base_name, module_cs_part, module_ra_part)
    for (dest, source, ignore) in data_files:
        result.append( (dest, source) )
    return result

def switch_destination_and_source(data_files):
    result = []
    for (dest, source) in data_files:
        result.append( (source, dest) )
    return result
    

    
def skip_data_file(filename_abs):
    skip_file = True
    filename_rest, filename_ext  = os.path.splitext(filename_abs)                
    if os.path.isfile(filename_abs) and filename_ext not in ['.pyc', '.pyo', '.orig'] and not filename_ext.endswith('~'):
        skip_file = False
    return skip_file

def folder_to_files(folder_root):
    """
    Returns a list of files in a folder
    """
    result = []
    if(os.path.isdir(folder_root)):
        for filename in os.listdir(folder_root):
            filename_abs = os.path.join(folder_root, filename)
            if os.path.isfile(filename_abs): 
                result.append(filename_abs)
    return result



class Py2ExeTarget(object):
    RT_MANIFEST = 24

    BRANDING_NORMAL = 0
    BRANDING_ANONYME = 1
    def __init__(self, script=None, uac=False, name='', description=None, branding=None, image_root=None, manifest=None):
        self.script = script
        self.dest_base = script.split('.')[0]
        self._build_version = lib.version.Version.create_current()
        
        if uac:
            self.uac_info = 'requireAdministrator'

        if manifest is not None:
            self.other_resources = [(Py2ExeTarget.RT_MANIFEST, 1, manifest)]

        if branding==Py2ExeTarget.BRANDING_NORMAL:
            self.icon_resources = [(1, os.path.join(image_root, 'giritech.ico'))]
            self.name = name
            if description is not None:
                self.description = description
            self.version = self._build_version.get_version_string_num()
            self.copyright = self._build_version.get_copyright()
            self.company_name = self._build_version.get_company_name()
            
        elif branding==Py2ExeTarget.BRANDING_ANONYME:
            self.icon_resources = [(1, os.path.join(image_root, 'giritech.ico'))]

class Py2ExeTargetService(Py2ExeTarget):
    def __init__(self, script=None, uac=False, name='', description=None, branding=None, image_root=None):
        Py2ExeTarget.__init__(self, script, uac, name, description, branding, image_root)
        self.modules = self.dest_base
        self.cmdline_style = 'custom'

if __name__ == '__main__':
    print modul_to_data_files('..', 'hagiwara', 'client', 'gateway')