import socket
import struct
import re

class WolException(Exception): pass
class BadFormatException(WolException): pass

mac_re = re.compile(r'[-: ]?'.join(6*[r'([0-9a-fA-F]{2})']) + '$')

def wake(mac_addr, address=None, port=0):
    """
    Send wakeup-call for mac_addr over udp to address and port.
    MAC address can be separated with ":", "-", " " or "".
    Default address is '<broadcast>' = INADDR_BROADCAST 
    Default port is 7 = echo
    """
    # find MAC address by pinging and looking at arp output
    mac_match = mac_re.match(mac_addr)
    if not mac_match:
        raise BadFormatException('%r is not a valid MAC address' % mac_addr)
    try:
        octets = [int(x, 16) for x in mac_match.groups()]
    except ValueError:
        raise BadFormatException('%r is not a valid MAC address' % mac_addr) # can't happen ...
    assert len(octets) == 6
    bsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    bsock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    magic = 6 * '\xff' + 16 * struct.pack('BBBBBB', *octets)
    bsock.sendto(magic, (address or '<broadcast>', port or 7))

if __name__ == '__main__':
    wake('00:16-cb af27:f8', '', 0) # 192.168.45.143 dev-mac-test1.giritech.com
