"""
Some tcp-connection-owner functions to use for lock to process functionality.
Currently Linux and Mac only
"""

import sys

if sys.platform == 'darwin':

    import subprocess
    import socket

    def _port_number(port):
        """
        Convert string to int - or None
        """
        try:
            return int(port)
        except ValueError, _e:
            return None # error, but not critical

    def _process_connections(filter_address):
        """
        Yield tuples for processes with TCP connections to/from address (which is a tuple)
        """
        argv = ['/usr/sbin/lsof', '-FRcn', '-n', '-P', '-a', '-i4TCP@%s:%s' % filter_address]
        print 'running %r' % ' '.join(argv)
        stdout, stderr = subprocess.Popen(argv, shell=False, stdout=subprocess.PIPE).communicate()
        #stdout example:
        #p17743
        #R17334
        #ctelnet
        #n127.0.0.1:44357->127.0.0.1:ssh
        #p17745
        #R17370
        #cssh
        #n127.0.0.1:44358->127.0.0.1:ssh
        #
        #lars-struwe-christensens-macbook:~ lsc$ /usr/sbin/lsof -FRcn -n -a -i4TCP@127.0.0.1:3389
        #p5700
        #R75
        #cgon_client
        #n127.0.0.1:ms-wbt-server
        #n127.0.0.1:ms-wbt-server->127.0.0.1:63314
        #n127.0.0.1:ms-wbt-server->127.0.0.1:63315
        #p5701
        #R5700
        #cRemote Desktop C
        #n127.0.0.1:63315->127.0.0.1:ms-wbt-server
        #
        #lars-struwe-christensens-macbook:~ lsc$ /usr/sbin/lsof -FRcn -n -a -i4TCP@127.0.0.1:63315
        #p5700
        #R75
        #cgon_client
        #n127.0.0.1:ms-wbt-server->127.0.0.1:63315
        #p5701
        #R5700
        #cRemote Desktop C
        #n127.0.0.1:63315->127.0.0.1:ms-wbt-server
        if stderr:
            return
        stdout = stdout.strip()
        if not stdout:
            return
        pid = None
        parent_pid = None
        lines = stdout.strip().split('\n')
        for line in lines:
            first = line[0]
            rest = line[1:]
            if first == 'p':
                pid = int(rest)
                parent_pid = None
                command = None
                continue
            if first == 'R':
                assert pid and not parent_pid
                parent_pid = int(rest)
                continue
            if first == 'c': # truncated to 16 chars in 10.4 and 10.5, 15 chars in 10.6
                assert pid and not command
                command = rest
                continue
            if first == 'n':
                # print pid, top_pid
                assert pid
                assert parent_pid
                assert command
                targetlocal, targetremote = rest.split('->', 1)
                targetlocaladdr, targetlocalport = targetlocal.split(':', 1)
                targetremoteaddr, targetremoteport = targetremote.split(':', 1)
                targetlocalport = _port_number(targetlocalport)
                targetremoteport = _port_number(targetremoteport)
                # print targetlocaladdr, targetlocalport, targetremoteaddr, targetremoteport
                # print filter_address, rem_address
                # print 'yield %r' % command, pid, parent_pid, (targetlocaladdr, targetlocalport), (targetremoteaddr, targetremoteport)
                yield command, pid, parent_pid, (targetlocaladdr, targetlocalport), (targetremoteaddr, targetremoteport)
                continue
            print 'Unknown line %r' % line

    def is_connection_owned_by(top_pid, local_address, rem_address, sub=False):
        """
        Tests if pid owns connection from local_address to rem_address directly or indirectly through sub processes
        """
        # FIXME: should be recursive, not only immediate children
        # print 'searching for', local_address, rem_address, 'matching', top_pid
        for _command, pid, parent_pid, a_local_address, a_rem_address in _process_connections(local_address):
            if ((pid == top_pid or sub and parent_pid == top_pid) and
                a_local_address == local_address and
                a_rem_address == rem_address):
                # print 'Connection %s:%s to %s:%s by command %r' % (local_address[0], local_address[1], rem_address[0], rem_address[1], command)
                return True
        # print 'Connection %s:%s to %s:%s not found' % (local_address[0], local_address[1], rem_address[0], rem_address[1])
        return False

    def is_connection_owned_by_named(name, local_address, rem_address):
        # print 'searching for', local_address, rem_address, 'matching %r' % name
        if not name:
            return True
        for command, _pid, _parent_pid, a_local_address, a_rem_address in _process_connections(local_address):
            # print command == name, a_local_address == local_address, a_rem_address == rem_address
            if (command[:15] == name[:15] and # Note: we only know the 15 or 16 first chars
                a_local_address == local_address and a_rem_address == rem_address):
                # print 'Connection %s:%s to %s:%s by command %r' % (local_address[0], local_address[1], rem_address[0], rem_address[1], command)
                return True
        # print 'Connection %s:%s to %s:%s not found' % (local_address[0], local_address[1], rem_address[0], rem_address[1])
        return False

    def socket_owner(local_address, rem_address):
        """
        Returns executable name and process/ancestor PIDs (starting with owner) of process owning connection from local_address to rem_address.
        Note: local_adress is at the pid end, while rem_address is the socket the pid might be connected to.
        """
        # FIXME: should be recursive, not only immediate children
        for command, pid, parent_pid, a_local_address, a_rem_address in _process_connections(local_address):
            if (a_local_address == local_address and a_rem_address == rem_address):
                pids = [pid]
                if parent_pid != 1:
                    pids.append(parent_pid)
                return (command, pids)
        return None

elif sys.platform == "win32":

    #performance note:
    #
    #1000 interations (on 3Ghz pentium, vista 32bit), positive:
    #Parent On, PID2Name: 11.3s
    #Parent off, PID2name: 9.8s
    #Parent off, No Name: 7.0s
    #
    #1000 interations, fail (good PID, invalid address):
    #Parent on, PID2Name: 2.5s
    #
    #1000 interations, fail (bad PID, good address):
    #Parent on, PID2Name: 2.8s
    #
    #So if we want to shave of the 25% added by PID2Name, _process_connections
    #can easily be modified to not return the right name unless it's needed
    #
    #But as PID2Name is only called if a possible candidate process is found
    #this may not make much sense...

    import ctypes
    import socket
    import struct

    from ctypes import byref
    from ctypes import sizeof

    PROCESS_QUERY_INFORMATION = 0x0400
    PROCESS_VM_READ = 0x0010

    #PSAPI.DLL
    psapi = ctypes.windll.psapi
    #Kernel32.DLL
    kernel = ctypes.windll.kernel32
    def PID2Name(pid):
        """
        Returns the process name to a window processID

        Core of code taken from: http://code.activestate.com/recipes/305279/
        """

# TODO: Perhaps just do this instead:
#        import win32com.client
#        WMI = win32com.client.GetObject('winmgmts:')
#        processes = WMI.ExecQuery('select Name from Win32_Process where ProcessId="%s"' % pid)
#        for p in processes:
#            return p.Properties_('Name').Value

        hModule = ctypes.c_ulong()
        count = ctypes.c_ulong()
        modname = ctypes.c_buffer(30)

        #Get handle to the process based on PID
        hProcess = kernel.OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                          False, pid)
        if hProcess:
            psapi.EnumProcessModules(hProcess, byref(hModule), sizeof(hModule), byref(count))
            psapi.GetModuleBaseNameA(hProcess, hModule.value, modname, sizeof(modname))
            r = "".join([ i for i in modname if i != '\x00'])

            #-- Clean up
            for i in range(modname._length_):
                modname[i]='\x00'

            kernel.CloseHandle(hProcess)
            return r


    def parent_processes(childPID):
        """
        Find the parent processes from a processid and yields the results
        as long as it's possible to resolve the process name for the parent process

        NOTE: Windows does NOT have a hierarchical process structures. Each process
        may, or may not keep the processID value from the process that launched
        it as it's parent process ID. Most processes does this, but it's possible
        to modify the value and there's no guarantee that the parent process is still
        running or that it hasn't been reused by another process.

        Using these values must be classified as unsafe, and should only be done
        in non critical applications or if it can't be avoided.

        cpp example code: http://www.scheibli.com/projects/getpids/
        """

        DWORD = ctypes.c_ulong

        # defing our MIB row structures
        class processBasicInformation(ctypes.Structure):
            _fields_ = [('dwExitStatus', DWORD),
                        ('pvPebBaseAddress', ctypes.c_void_p),
                        ('dwAffinityMask', DWORD),
                        ('dwBasePriority', DWORD),
                        ('ulUniqueProcessId', DWORD),
                        ('ulInheritedFromUniqueProcessId',DWORD)]

        infoRec = processBasicInformation()

        hProcess = kernel.OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, False, childPID)
        if ctypes.windll.ntdll.NtQueryInformationProcess(hProcess, 0, ctypes.byref(infoRec), sizeof(infoRec), 0) == 0:
            parentPID = infoRec.ulInheritedFromUniqueProcessId
            kernel.CloseHandle(hProcess)
            while (parentPID != 0):
                yield parentPID
                hProcess = kernel.OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, False, parentPID)
                if ctypes.windll.ntdll.NtQueryInformationProcess(hProcess, 0, ctypes.byref(infoRec), sizeof(infoRec), 0) == 0:
                    parentPID = infoRec.ulInheritedFromUniqueProcessId
                else:
                    parentPID = 0
                kernel.CloseHandle(hProcess)


    def _process_connections(filter_address, sub = False):
        """
        yields a series of processes matching the filter

        result in the format: processname, processID, processParentID, (localAddress, localPort), (remoteAddress, remotePort)

        Currently the processParentID is always -1
        Filter address is on the form: (localAddress, localPort)
        and is used against the localAddress and localPort values
        """
        #Implementation notes:
        #
        #Get  series of
        #MIB_TCPROW_OWNER_PID = packed record
        #  dwState: DWORD;
        #  dwLocalAddr: DWORD;
        #  dwLocalPort: DWORD;
        #  dwRemoteAddr: DWORD;
        #  dwRemotePort: DWORD;
        #  dwOwningPid: DWORD;
        #end;
        #PMIB_TCPROW_OWNER_PID = ^MIB_TCPROW_OWNER_PID;
        #
        #References:
        #http://msdn.microsoft.com/en-us/library/aa365928(VS.85).aspx
        #
        #Build on the code from:
        #http://code.activestate.com/recipes/392572/

        DWORD = ctypes.c_ulong
        NULL = ""

        TCP_TABLE_OWNER_PID_ALL = 5
        AF_INET         = 2

        # defing our MIB row structures
        class MIB_TCPROW(ctypes.Structure):
            _fields_ = [('dwState', DWORD),
                        ('dwLocalAddr', DWORD),
                        ('dwLocalPort', DWORD),
                        ('dwRemoteAddr', DWORD),
                        ('dwRemotePort', DWORD),
                        ('dwOwningPid',DWORD)]

        dwSize = DWORD(0)

        filteradr, filterport = filter_address

        ctypes.windll.iphlpapi.GetExtendedTcpTable(NULL, ctypes.byref(dwSize), False, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0)

        # print 'dwSize.value', dwSize.value

        ANY_SIZE = dwSize.value
        NO_ERROR = 0

        class MIB_TCPTABLE(ctypes.Structure):
            _fields_ = [('dwNumEntries', DWORD),
                        ('table', MIB_TCPROW * ANY_SIZE)]

        tcpTable = MIB_TCPTABLE()
        tcpTable.dwNumEntries = 0 # define as 0 for our loops sake

        if ctypes.windll.iphlpapi.GetExtendedTcpTable(ctypes.byref(tcpTable), ctypes.byref(dwSize), False, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0) == NO_ERROR:

            maxNum = tcpTable.dwNumEntries
            placeHolder = 0

            # loop through every connection
            while placeHolder < maxNum:

                item = tcpTable.table[placeHolder]
                placeHolder += 1
                la = socket.inet_ntoa(struct.pack('L', item.dwLocalAddr))
                lp = socket.ntohs(item.dwLocalPort)
                if (la == filteradr) and (lp == filterport):
                    ra = socket.inet_ntoa(struct.pack('L', item.dwRemoteAddr))
                    rp = socket.ntohs(item.dwRemotePort)

                    yield PID2Name(item.dwOwningPid), item.dwOwningPid, -1, (la, lp), (ra, rp)
                    # also yield the parent processes, if sub is True
                    if sub:
                        ParentID = item.dwOwningPid
                        for PID in parent_processes(item.dwOwningPid):
                            yield PID2Name(PID), PID, ParentID, (la, lp), (ra, rp)
                            ParentID = PID

                    continue

    def is_connection_owned_by(top_pid, local_address, rem_address, sub=False):
        """
        Tests if pid owns connection from local_address to rem_address directly

        Example:
        print is_connection_owned_by(3596, ('127.0.0.1', 50156), ('127.0.0.1', 49401), True)

        sub = True means that
        """
        # print 'searching for', local_address, rem_address, 'matching', top_pid
        for command, pid, parent_pid, a_local_address, a_rem_address in _process_connections(local_address, sub):
            if ((pid == top_pid or (sub and parent_pid == top_pid)) and
                a_local_address == local_address and
                a_rem_address == rem_address):
                print 'Connection %s:%s to %s:%s by command %r' % (local_address[0], local_address[1], rem_address[0], rem_address[1], command)
                return True

    def is_connection_owned_by_named(name, local_address, rem_address):
        if not name:
            return True
        name = name.upper()
        # print 'searching for', local_address, rem_address, 'matching', top_pid
        for command, pid, parent_pid, a_local_address, a_rem_address in _process_connections(local_address, False):
            if (a_local_address == local_address and
                a_rem_address == rem_address):
                if (command.upper() == name):
                    print 'Ok; connection %s:%s to %s:%s was made by %r' % (local_address[0], local_address[1], rem_address[0], rem_address[1], command)
                    return True
                else:
                    # TODO: report this in a better way to help debugging problems
                    print 'Connection %s:%s to %s:%s was made by %r NOT by %r' % (local_address[0], local_address[1], rem_address[0], rem_address[1], command, name)
                    return False

else: # Linux

    import os

    def _hex2ip(s):
        """
        Convert strings like '0100007F:0016' to tuple('127.0.0.1', 22)
        """
        addr, port = s.split(':')
        return ('.'.join(str(int(addr[2*i:2*i+2], 16)) for i in (3,2,1,0) ), int(port, 16))

    def _netstat():
        """
        Yield tuple(inode, local_address, rem_address) for current established tcp connections
        """
        for line in open('/proc/net/tcp').readlines()[1:]:
            # Parse this:
            #   sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode
            #    0: 0100007F:1F40 00000000:0000 0A 00000000:00000000 00:00000000 00000000     0        0 5133 1 f726b400 3000 0 0 2 -1
            if line[34:36] != '01': # Only connections with status ESTABLISHED are interesting
                continue
            local_address = _hex2ip(line[6:19])
            rem_address = _hex2ip(line[20:33])
            inode = int(line[91:].split(' ', 1)[0])
            # print 'inode', inode, 'is', local_address, rem_address
            yield (inode, local_address, rem_address)

    def _proc_socket_inodes(pid):
        """
        Yield inode for sockets of a process
        """
        try:
            pid_fds = os.listdir('/proc/%s/fd' % pid)
        except OSError:
            return # not users process or process disappeared
        for fd in pid_fds:
            try:
                fd_target = os.readlink('/proc/%s/fd/%s' % (pid, fd))
            except OSError:
                continue # process or fd disappeared
            if not (fd_target.startswith('socket:[') and fd_target.endswith(']')):
                continue # not nice socket and thus not interesting
            inode = int(fd_target[8:-1])
            # print 'pid', pid, 'has inode', inode
            yield inode

    def _child_proc_socket_inodes(top_pid = 1):
        """
        Yield tuple(pid, inode) for sockets of processes if exists and accessible etc
        """
        procs = {} # from parent pid to list of child pids
        for pid_s in os.listdir('/proc'):
            if not pid_s.isdigit():
                continue
            pid = int(pid_s)
            procs[pid] = []
        if top_pid not in procs:
            return # disappeared
        for pid in procs:
            try:
                ppid = int(open('/proc/%s/stat' % pid).read().rsplit(')', 1)[1].split(' ')[2])
            except:
                continue # failure is ok
            if ppid in procs: # parent might have disappeared and left a zombie
                procs[ppid].append(pid)
        def recurse_sub_processes(pid):
            for inode in _proc_socket_inodes(pid):
                yield pid, inode
            for child_pid in procs[pid]:
                for sub_child_pid, inode in recurse_sub_processes(child_pid):
                    yield sub_child_pid, inode
        for pid, inode in recurse_sub_processes(top_pid):
            yield inode

    def _named_proc_socket_inodes(name):
        """
        Yield tuple(pid, inode) for sockets of processes if exists and accessible etc
        """
        # print 'matching name', name
        for pid_s in os.listdir('/proc'):
            if not pid_s.isdigit():
                continue
            pid = int(pid_s)
            try:
                exe = os.readlink('/proc/%s/exe' % pid)
            except:
                continue # failure is ok
            # print 'exe', exe
            if exe == name:
                for inode in _proc_socket_inodes(pid):
                    # print inode
                    yield inode

    def is_connection_owned_by(top_pid, local_address, rem_address, sub=False):
        """
        Tests if top_pid owns connection from local_address to rem_address directly or indirectly through sub processes.
        Note: local_adress is at the pid end, while rem_address is the socket the pid might be connected to.
        """
        for (socket_inode, a_local_address, a_rem_address) in _netstat():
            if a_local_address == local_address and a_rem_address == rem_address:
                # We found the connection and thus the inode
                if sub:
                    return socket_inode in _child_proc_socket_inodes(top_pid)
                else:
                    return socket_inode in _proc_socket_inodes(top_pid)
        return False # connection not found

    def is_connection_owned_by_named(name, local_address, rem_address):
        """
        Tests if the name matches process owning connection from local_address to rem_address directly or indirectly through sub processes.
        Note: local_adress is at the pid end, while rem_address is the socket the pid might be connected to.
        """
        if not name:
            return True
        for (socket_inode, a_local_address, a_rem_address) in _netstat():
            #print 'netstat', socket_inode, a_local_address, a_rem_address
            if a_local_address == local_address and a_rem_address == rem_address:
                # We found the connection and thus the inode
                #print 'found', socket_inode, 'name', name
                return socket_inode in _named_proc_socket_inodes(name)
        return False # connection not found

    def socket_owner(local_address, rem_address):
        """
        Returns executable name and process/ancestor PIDs (starting with owner) of process owning connection from local_address to rem_address.
        Note: local_adress is at the pid end, while rem_address is the socket the pid might be connected to.
        """
        # Determine sockets inode
        socket_inode = None
        for (a_socket_inode, a_local_address, a_rem_address) in _netstat():
            if a_local_address == local_address and a_rem_address == rem_address:
                socket_inode = a_socket_inode
                break
        if not socket_inode:
            return None # FIXME: dummy tuple or raise???
        # Find a (the first) owning process
        pid = None
        for pid_s in os.listdir('/proc'):
            if not pid_s.isdigit():
                continue
            try:
                pid_fds = os.listdir('/proc/%s/fd' % pid_s)
            except OSError:
                continue # not users process or process disappeared
            for fd in pid_fds:
                try:
                    fd_target = os.readlink('/proc/%s/fd/%s' % (pid_s, fd))
                except OSError:
                    continue # process or fd disappeared
                if not (fd_target.startswith('socket:[') and fd_target.endswith(']')):
                    continue # not nice socket and thus not interesting
                an_inode = int(fd_target[8:-1])
                if an_inode == socket_inode:
                    pid = int(pid_s)
                    break
        if not pid:
            return None
        # Find name of executable
        try:
            exe = os.readlink('/proc/%s/exe' % pid)
        except:
            return None
        # Find process ancestors and name of executable
        pids = []
        while pid != 1:
            pids.append(pid)
            try:
                pid = int(open('/proc/%s/stat' % pid).read().rsplit(')', 1)[1].split(' ')[2])
            except OSError:
                break
        # Woaw
        return (exe, pids)


def main():
    import time
    local_address = (sys.argv[1], int(sys.argv[2]))
    rem_address = (sys.argv[3], int(sys.argv[4]))
    start = time.time()

    if len(sys.argv) == 5:
        print socket_owner(local_address, rem_address)
    else:
        if sys.argv[5].isdigit():
            pid = int(sys.argv[5])
            sub = int(sys.argv[6])
            print is_connection_owned_by(pid, local_address, rem_address, sub)
        else:
            name = sys.argv[5]
            print is_connection_owned_by_named(name, local_address, rem_address)

    end = time.time()
    print "%0.3f ms" % ((end-start))

if __name__ == '__main__':
    main()
