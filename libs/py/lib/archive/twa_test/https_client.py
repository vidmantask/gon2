'''
Created on Sep 16, 2010

@author: twa
'''
import httplib
import ssl
import socket
import pprint

HOSTNAME = '127.0.0.1'


class HTTPSConnection(httplib.HTTPConnection):
    "This class allows communication via SSL."

    def __init__(self, host, port=None, key_file=None, cert_file=None, strict=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
        httplib.HTTPConnection.__init__(self, host, port, strict, timeout)
        self.key_file = key_file
        self.cert_file = cert_file

    def connect(self):
        "Connect to a host on a given (SSL) port."

        sock = socket.create_connection((self.host, self.port), self.timeout)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, cert_reqs=ssl.CERT_OPTIONAL)

        print repr(self.sock.getpeername())
        print self.sock.cipher()
        print pprint.pformat(self.sock.getpeercert())

conn = HTTPSConnection(
    
    HOSTNAME,
#    key_file = '/home/thwang/source/main2/setup/key_store/https_server.key',
#    cert_file= '/home/thwang/source/main2/setup/key_store/https_server.crt',
    port = 8090

)
conn.putrequest('GET', '/')
conn.endheaders()
response = conn.getresponse()
print response.read()




