"""
Unittest of yubikey
"""
import sys

import unittest
from lib import giri_unittest

from binascii import a2b_hex
from lib.yubico import yubikey

if sys.platform == 'win32':
    import pythoncom


class Yubikey(unittest.TestCase):

    def setUp(self):
        if sys.platform == 'win32':
            pythoncom.CoInitialize() 
    
    def tearDown(self):
        pass

  
    def test_parse(self):
        # test parse() and resulting Token object
        token_obj = yubikey.parse('njleklcjnhibgekjjfieebinvkidllgu',
                                  yubikey.modhex_decode('hbuuvggnjbdhndcgvjrrerkgfnneicbt'))

        self.assertFalse(token_obj.capslock())
        self.assertEqual(token_obj.counter(), 1)
        self.assertEqual(token_obj.crc, 30159)
        self.assertEqual(token_obj.ctr, 1)
        self.assertEqual(token_obj.ctr32(), 256)
        self.assertEqual(token_obj.decrypted, '\xa1S\xba\x7f(I\x01\x00\x00\x00\x00\x00\xd5k\xcfu')
        self.assertEqual(token_obj.rnd, 27605)
        self.assertFalse(token_obj.static_otp())
        self.assertEqual(token_obj.tstp32(), 0)
        self.assertEqual(token_obj.tstph, 0)
        self.assertEqual(token_obj.tstpl, 0)
        self.assertEqual(token_obj.uid, '\xa1S\xba\x7f(I')
        self.assertEqual(token_obj.use, 0)

    def test_modhex(self):
        self.assertEqual(yubikey.modhex_encode('\xa1S\xba\x7f(I\x01\x00\x00\x00\x00\x00\xd5k\xcfu'), 'lbgenlivdjfkcbcccccccccctghnrvig')
        self.assertEqual(yubikey.modhex_decode('lbgenlivdjfkcbcccccccccctghnrvig'), '\xa1S\xba\x7f(I\x01\x00\x00\x00\x00\x00\xd5k\xcfu')
        
    def test_crc(self):
        self.assertEqual(yubikey.crc16('\xa1S\xba\x7f(I\x01\x00\x00\x00\x00\x00\xd5k\xcfu'), 61624)
        
    def test_aes(self):
        self.assertEqual(yubikey.aes_decrypt('\x00'*16, '\x00'*16), '\x14\x0f\x0f\x10\x11\xb5"=yXw\x17\xff\xd9\xec:')
        self.assertEqual(yubikey.aes_decrypt('\x47'*16, '\xf0'*16), '\xbf\x91\x14\xe6\xb6\xe6\xd2@\x1a\xbb\xd7\x94!\x12J{')
        
    def test_vectors(self):
        # http://forum.yubico.com/viewtopic.php?f=8&t=45
        self.assertEqual(yubikey.modhex_encode(a2b_hex('69b6481c8baba2b60e8f22179b58cd56')), 'hknhfjbrjnlnldnhcujvddbikngjrtgh')
        self.assertEqual(yubikey.modhex_decode('hknhfjbrjnlnldnhcujvddbikngjrtgh'), a2b_hex('69b6481c8baba2b60e8f22179b58cd56'))

        self.assertEqual(yubikey.modhex_encode('test'), 'ifhgieif')
        self.assertEqual(yubikey.modhex_decode('ifhgieif'), 'test')

        self.assertEqual(yubikey.modhex_encode('foobar'), 'hhhvhvhdhbid')
        self.assertEqual(yubikey.modhex_decode('hhhvhvhdhbid'), 'foobar')
        
        input_ = ''.join(chr(int(x, 16)) for x in '69 b6 48 1c 8b ab a2 b6 0e 8f 22 17 9b 58 cd 56'.split())
        aeskey = ''.join(chr(int(x, 16)) for x in 'ec de 18 db e7 6f bd 0c 33 33 0f 1c 35 48 71 db'.split())
        output = ''.join(chr(int(x, 16)) for x in '87 92 eb fe 26 cc 13 00 30 c2 00 11 c8 9f 23 c8'.split())
        self.assertEqual(yubikey.aes_decrypt(input_,aeskey), output)
        
        token_obj = yubikey.Token(output)

        self.assertFalse(token_obj.capslock())
        self.assertEqual(token_obj.counter(), 19)
        self.assertEqual(token_obj.crc, 0xc823)
        self.assertEqual(token_obj.ctr, 0x13)
        self.assertEqual(token_obj.ctr32(), 0x1311)
        self.assertEqual(token_obj.decrypted, output)
        self.assertEqual(token_obj.rnd, 0x9fc8)
        self.assertFalse(token_obj.static_otp())
        self.assertEqual(token_obj.tstp32(), 49712)
        self.assertEqual(token_obj.tstph, 0)
        self.assertEqual(token_obj.tstpl, 49712)
        self.assertEqual(token_obj.uid, '\x87\x92\xeb\xfe\x26\xcc')
        self.assertEqual(token_obj.use, 0x11)

        # http://forum.yubico.com/viewtopic.php?f=8&t=45
        for s in ['ndgtriluugngkuhguutjdfihfkleuvnjtjiljkbtbngb',
                  'ndgtriluugngkuhguutjdfihfkleuvnjtjiljkbtbngb',
                  'ndgtriluugnghggtkhvrbbrrgtdfeivgklbkviteggcu',
                  'ndgtriluugngvlbnjllchllugvhhftndludrjubuuvkh',
                  'ndgtriluugngvukkrdlbvnettvhdhcjfgtjljkufulfe',
                  'ndgtriluugngnfdeicbdnfhlinhjvkufikdvbunjjvbn',
                  'ndgtriluugngbclrcivunvrbghicrfkeflvgrkdrfbve',
                  'ndgtriluugngvkdlfkvljfcvbehdvbtelhuutgdhfdku',
                  ]:
            token_obj = yubikey.parse(s[-32:],
                                      a2b_hex('0682952d3363f225654b58a73b131259'))
            self.assertTrue(token_obj.crc_ok_p())
            self.assertEqual(token_obj.uid, '\xc5\xfc\x55\xc3\x76\x7f')
        

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'mk'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
