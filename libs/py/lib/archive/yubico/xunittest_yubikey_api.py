"""
Unittest of yubikey_api
"""
import unittest
from lib import giri_unittest

import sys
from binascii import a2b_hex, b2a_hex 

from lib.yubico import yubikey_api

class Yubikey(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_program(self):
        # Note: requires yubikey without password set
        # Note: This will reset the key and could in worst case brick it!
        
        if sys.platform != 'win32': return

        public_id, aes_key, private_id = yubikey_api.program(public_id='1'*8, 
                                                             aes_key='2'*16, 
                                                             private_id='3'*6,
                                                             append_cr=True, 
                                                             cur_pwd=None, 
                                                             new_pwd=None,
                                                             )
        self.assertEqual(public_id, '1'*8)
        self.assertEqual(aes_key, '2'*16)
        self.assertEqual(private_id, '3'*6)
        
        public_id, aes_key, private_id = yubikey_api.program(public_id='1'*8)
        self.assertEqual(len(aes_key), 16)
        self.assertEqual(len(private_id), 6)

    def test_password(self):
        # Note: This test is especially dangerous!

        if sys.platform != 'win32': return
        
        yubikey_api.YubiProgrammingFailed, yubikey_api.program(public_id='1'*8, cur_pwd=None)
        self.assertRaises(yubikey_api.YubiProgrammingFailed, 
                          yubikey_api.program,
                              public_id='1'*8, cur_pwd='1'*6)
        yubikey_api.program(public_id='1'*8, new_pwd='1'*6)
        yubikey_api.program(public_id='1'*8, cur_pwd='1'*6) # test was set and set again
        yubikey_api.program(public_id='1'*8, cur_pwd='1'*6) # test was set again
        self.assertRaises(yubikey_api.YubiProgrammingFailed, 
                          yubikey_api.program,
                              public_id='1'*8)
        self.assertRaises(yubikey_api.YubiProgrammingFailed, 
                          yubikey_api.program,
                              public_id='1'*8, cur_pwd=None)
        self.assertRaises(yubikey_api.YubiProgrammingFailed, 
                              yubikey_api.program,
                                  public_id='1'*8, cur_pwd='7'*6)
        yubikey_api.program(public_id='1'*8, cur_pwd='1'*6, new_pwd='')
        yubikey_api.program(public_id='1'*8)

    def test_get_skipped_prefix(self):
        self.assertEqual(yubikey_api.get_skipped_prefix(''), None)
        self.assertEqual(yubikey_api.get_skipped_prefix('x'*47), None)
        self.assertEqual(yubikey_api.get_skipped_prefix('x'*48), '')
        self.assertEqual(yubikey_api.get_skipped_prefix('x'*49), 'x')

    def test_get_public_id(self):
        self.assertEqual(yubikey_api.get_public_id(''), None)
        self.assertEqual(yubikey_api.get_public_id('x'*47), None)
        self.assertEqual(yubikey_api.get_public_id('x'*48), '\x00'*8)
        self.assertEqual(yubikey_api.get_public_id('f'*48), 'D'*8)
        self.assertEqual(yubikey_api.get_public_id('f'*49), 'D'*8)
        
    def test_verify(self):
        public_id = '\x00\x01\x02\x03\x04\x05\x06\x07'
        aes_key = a2b_hex('b50f89277e818dd74919b9b59dfc6048')
        private_id = a2b_hex('12780313176e')
        last_session = 0
        last_session_uses = 0
        last_timestamp = 0
        last_timeoffset = 0
        
        # First session after programming
        result = yubikey_api.verify(public_id, aes_key, private_id, 
                        last_session, last_session_uses, last_timestamp, last_timeoffset,
                        'cccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighvj', time_f=lambda:7)
        last_session, last_session_uses, last_timestamp, last_timeoffset = result
        self.assertEqual(last_session, 1)
        self.assertEqual(last_session_uses, 0)
        self.assertEqual(last_timeoffset, 0)
        
        # Lost session
        result = yubikey_api.verify(public_id, aes_key, private_id, 
                        last_session, last_session_uses, last_timestamp, last_timeoffset,
                        'cccbcdcecfcgchcicileinhflfftkgkncguckrcechbbdkbk', time_f=lambda:8)
        last_session, last_session_uses, last_timestamp, last_timeoffset = result
        self.assertEqual(last_session, 3)
        self.assertEqual(last_session_uses, 0)
        self.assertEqual(last_timeoffset, 0)

        # Lost token in session
        result = yubikey_api.verify(public_id, aes_key, private_id, 
                        last_session, last_session_uses, last_timestamp, last_timeoffset,
                        'cccbcdcecfcgchcithhieledicueftgthgrgtelivgirncii', time_f=lambda:1137)
        last_session, last_session_uses, last_timestamp, last_timeoffset = result
        self.assertEqual(last_session, 3)
        self.assertEqual(last_session_uses, 3)
        self.assertEqual(last_timeoffset, 10357)

        #  Are you sorry we drifted apart
        self.assertRaises(yubikey_api.YubiTokenTimedOutInSession,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcivgnifgglgncllvnndtuhlfkcbgciuutr', time_f=lambda:650)
        self.assertRaises(yubikey_api.YubiTokenTimedOutInSession,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcivgnifgglgncllvnndtuhlfkcbgciuutr', time_f=lambda:1600)

        # Next token in session with proper timestamp
        result = yubikey_api.verify(public_id, aes_key, private_id, 
                        last_session, last_session_uses, last_timestamp, last_timeoffset,
                        'cccbcdcecfcgchcivgnifgglgncllvnndtuhlfkcbgciuutr', time_f=lambda:1534)
        last_session, last_session_uses, last_timestamp, last_timeoffset = result
        self.assertEqual(last_session, 3)
        self.assertEqual(last_session_uses, 4)
        self.assertEqual(last_timeoffset, 12910)

        # Replay last
        self.assertRaises(yubikey_api.YubiTokenOutdatedInSession,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcivgnifgglgncllvnndtuhlfkcbgciuutr', time_f=lambda:1534)

        # Replay from old session
        self.assertRaises(yubikey_api.YubiTokenOutdatedSession,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighvj', time_f=lambda:1534)

        # Token too short
        self.assertRaises(yubikey_api.YubiBadToken,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighv', time_f=lambda:1534)

        # Bogus public id
        self.assertRaises(yubikey_api.YubiBadPublicId,
                          yubikey_api.verify, 
                              public_id, aes_key, private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'dccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighvj', time_f=lambda:1534)
        # AES too short
        self.assertRaises(yubikey_api.YubiBadAesKey,
                          yubikey_api.verify, 
                              public_id, aes_key[:-1], private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighvj', time_f=lambda:1534)
        # AES key somehow doesn't match
        self.assertRaises(yubikey_api.YubiBadDecrypt,
                          yubikey_api.verify, 
                              public_id, aes_key[:-1] + 'x', private_id, 
                              last_session, last_session_uses, last_timestamp, last_timeoffset,
                              'cccbcdcecfcgchcihibfdlfldcjkdgfvujnlvblcgntighvj', time_f=lambda:1534)

 
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'mk'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
