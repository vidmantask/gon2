"""
High level API for yubikey
"""

import random
import binascii
import time
import sys

import yubikey # low level lib ported from C

if sys.platform == 'win32':
    import win32com.client
    yubikcom_dll = win32com.client.Dispatch("YubiKcom.YubiKeyConfig") # Requires YubiKcom.dll - and perhaps regsvr32?
    # FYI: currently unused methods .ykEnableNotifications and .ykEnableDebug

PUBLIC_ID_LENGTH = 8

class constants(object): # dumped from YubiKcom.dll by makepy.py
    ykFLAG_ALLOW_HIDTRIG          = 0x0a        # from enum tagykFLAG
    ykFLAG_APPEND_CR              = 0x05        # from enum tagykFLAG
    ykFLAG_APPEND_DELAY1          = 0x03        # from enum tagykFLAG
    ykFLAG_APPEND_DELAY2          = 0x04        # from enum tagykFLAG
    ykFLAG_APPEND_TAB1            = 0x01        # from enum tagykFLAG
    ykFLAG_APPEND_TAB2            = 0x02        # from enum tagykFLAG
    ykFLAG_PACING_10MS            = 0x08        # from enum tagykFLAG
    ykFLAG_PACING_20MS            = 0x09        # from enum tagykFLAG
    ykFLAG_SEND_REF               = 0x06        # from enum tagykFLAG
    ykFLAG_STATIC_TICKET          = 0x0b        # from enum tagykFLAG
    ykFLAG_TAB_FIRST              = 0x00        # from enum tagykFLAG
    ykFLAG_TICKET_FIRST           = 0x07        # from enum tagykFLAG
    ykMORE_THAN_ONE               = 0x02        # from enum tagykRETCODE
    ykNO_DEVICE                   = 0x01        # from enum tagykRETCODE
    ykOK                          = 0x00        # from enum tagykRETCODE
    ykREAD_ERROR                  = 0x04        # from enum tagykRETCODE
    ykREAD_ONLY                   = 0x03        # from enum tagykRETCODE
    ykWRITE_ERROR                 = 0x05        # from enum tagykRETCODE
    ykID_MAX_SIZE                 = 0x10        # from enum tagykSIZES
    ykKEY_SIZE                    = 0x10        # from enum tagykSIZES
    ykUID_SIZE                    = 0x06        # from enum tagykSIZES
    
    ykRETCODEs = {ykMORE_THAN_ONE: 'MORE_THAN_ONE',
                  ykNO_DEVICE: 'NO_DEVICE',
                  ykOK: 'OK',
                  ykREAD_ERROR: 'READ_ERROR',
                  ykREAD_ONLY: 'READ_ONLY',
                  ykWRITE_ERROR: 'WRITE_ERROR'}


class YubiProgrammingException(Exception): pass # base
class YubiProgrammingPlatformNotSupported(YubiProgrammingException): pass
class YubiProgrammingCancelled(YubiProgrammingException): pass
class YubiProgrammingNotIsConfigured(YubiProgrammingException): pass
class YubiProgrammingFailed(YubiProgrammingException): 
    def __init__(self, retcode):
        YubiProgrammingException.__init__(self)
        self.retcode = retcode
        
    def desc(self):
        return constants.ykRETCODEs.get(self.retcode, "%s" % self.retcode)

    def __str__x(self):
        return '<%s %s: %s>' % (self.__class__.__name__, self.retcode, self.desc())
    
def program(public_id, aes_key=None, private_id=None, append_cr=True, cur_pwd=None, new_pwd=None, chicken=False):
    """
    Program a yubikey
    public_id: PUBLIC_ID_LENGTH bytes, not encrypted, to be used by server to find aes_key
    aes_key: 16 bytes = 128 bits - if not set then a random will be assigned and returned
    private_id: 6 bytes, encrypted - if not set then a random will be assigned and returned
    cur_pwd: 6 bytes, existing password - wrong password will be reported as READ_ONLY
    new_pwd: 6 bytes, new password to set, defaults to cur_pwd - forgetting this will brick the device!
    Returns: public_id, aes_key, private_id
    Will raise YubiProgrammingException on errors
    """
    # Program Files/Yubico/YubiKCom SDK/Doc/YubiKey Configuration - Integrators Guide.pdf
    if sys.platform != 'win32':
        raise YubiProgrammingPlatformNotSupported()

    assert len(public_id) == PUBLIC_ID_LENGTH
    public_id_hex = binascii.b2a_hex(public_id)
     
    if aes_key is None:
        aes_key = ''.join(chr(random.randrange(256)) for i in range(16))
    aes_key_hex = binascii.b2a_hex(aes_key)
    assert len(aes_key_hex) == 32, 'invalid aes_key %r' % aes_key_hex
    
    if private_id is None:
        private_id = ''.join(chr(random.randrange(256)) for i in range(6))
    private_id_hex = binascii.b2a_hex(private_id)
    assert len(private_id_hex) == 12, 'invalid private_id %r' % private_id_hex
    
    if cur_pwd: # neither "" nor None
        assert len(cur_pwd) == 6, 'invalid password %r' % cur_pwd

    if new_pwd is None:
        new_pwd = cur_pwd
    if new_pwd:
        assert len(new_pwd) == 6, 'invalid password %r' % new_pwd
    if (chicken and 
        not new_pwd is None and 
        new_pwd != cur_pwd and 
        raw_input("set new password %r?" % new_pwd) != 'yes'):
        raise YubiProgrammingCancelled()

    if not yubikcom_dll.ykIsInserted:
        raise YubiProgrammingException(constants.ykNO_DEVICE)
    yubikcom_dll.ykClear()
    yubikcom_dll.ykStaticID = public_id_hex # public prefix, hex encoded, will be exposed in cleartext only modhex encoded
    yubikcom_dll.ykKey = aes_key_hex # AES key, 32 characters hex = 128 bits
    yubikcom_dll.ykUID = private_id_hex # secret salt, 12 characters hex = 48 bits
    if append_cr: 
        yubikcom_dll.ykFlagProperty(constants.ykFLAG_APPEND_CR, True)
    if not cur_pwd is None:
        # print 'setting old %r' % cur_pwd
        yubikcom_dll.ykCurPWD = cur_pwd
    if not new_pwd is None:
        # print 'setting new %r' % new_pwd
        yubikcom_dll.ykNewPWD = new_pwd
    retcode = yubikcom_dll.ykProgram # This "property" is a method with huge side effects!!!
    
    if retcode != constants.ykOK:
        raise YubiProgrammingFailed(retcode)
    if not yubikcom_dll.ykIsConfigured:
        raise YubiProgrammingNotIsConfigured()
       
    return public_id, aes_key, private_id


def get_skipped_prefix(input_token):
    """
    Given something followed by a token from a yubikey return the something that it will skip.
    Returns None on error.
    """
    if len(input_token) >= 32 + 2 * PUBLIC_ID_LENGTH:
        return input_token[:-(32 + 2 * PUBLIC_ID_LENGTH)]
    return None

def get_public_id(input_token):
    """
    Given a token from a yubikey return the public_id it was programmed with.
    Returns None on error.
    """
    if len(input_token) >= 32 + 2 * PUBLIC_ID_LENGTH:
        return yubikey.modhex_decode(input_token[-(32 + 2 * PUBLIC_ID_LENGTH):-32])
    return None


class YubiVerificationFailed(Exception): pass # base class
class YubiBadToken(YubiVerificationFailed): pass
class YubiBadPublicId(YubiVerificationFailed): pass
class YubiBadAesKey(YubiVerificationFailed): pass
class YubiBadDecrypt(YubiVerificationFailed): pass
class YubiUnsupported(YubiVerificationFailed): pass
class YubiTokenOutdatedInSession(YubiVerificationFailed): pass
class YubiTokenOutdatedSession(YubiVerificationFailed): pass
class YubiTokenTimedOutInSession(YubiVerificationFailed): pass

def verify(public_id, aes_key, private_id, 
        last_session, last_session_uses, last_timestamp, last_timeoffset,
        input_token, time_f=time.time):
    """
    Verify that input_token matches the known state of the yubikey
    """
    if len(input_token) < 32 + 2 * PUBLIC_ID_LENGTH:
        raise YubiBadToken() # too short
    decoded_token = yubikey.modhex_decode(input_token[-(32 + 2 * PUBLIC_ID_LENGTH):])
    if decoded_token[:-16] != public_id:
        raise YubiBadPublicId() # Sanity check: get_static_id wasn't used properly
    crypted = decoded_token[-16:]
    assert len(crypted) == 16
    if len(aes_key) != 16:
        raise YubiBadAesKey() # invalid aes_key specified
    decrypted = yubikey.aes_decrypt(crypted, aes_key)
    token_obj = yubikey.Token(decrypted)
    if token_obj.uid != private_id:
        raise YubiBadDecrypt() # invalid token - bad decryption or misplaced token
    if not token_obj.crc_ok_p():
        raise YubiBadDecrypt() # invalid token - bad decryption or bad token
    if token_obj.capslock():
        raise YubiUnsupported() # shouldn't happen but we don't like it

    if token_obj.static_otp():
        raise YubiUnsupported() # "static OTP" not supported

    # OTP validation cookbook, http://forum.yubico.com/viewtopic.php?f=6&t=103
    next_session = token_obj.counter()
    next_session_uses = token_obj.use
    next_timestamp = int(time_f())
    next_timeoffset = token_obj.tstp32()
    if next_session > last_session:
        # New session
        if next_session > last_session + 1:
            pass # report lost sessions!
    elif next_session == last_session:
        # In session
        if next_session_uses <= last_session_uses:
            raise YubiTokenOutdatedInSession() # slightly too old token
        if next_session_uses > last_session_uses + 1:
            pass # report lost token in session
        my_time_diff = abs(next_timestamp - last_timestamp)
        token_time_diff = abs(next_timeoffset - last_timeoffset) / 8 # The nominal frequency is 8Hz ...
        if abs(my_time_diff - token_time_diff) > 0.30 * my_time_diff: # ... but can vary +/- 30% between devices and over temperature.
            # print 'drift', my_time_diff - token_time_diff, 'allowed', 0.30 * my_time_diff
            raise YubiTokenTimedOutInSession() # time drifted too much off - or my diff too small
    else:
        # Outdated session
        raise YubiTokenOutdatedSession() # much too old token

    return next_session, next_session_uses, next_timestamp, next_timeoffset


def demo():
    print 'Programming yubikey ...'
    # come up with a public_id (and specify cur_pwd if already set) (might also specify new_pwd)
    # (private_id _could_ be hardcoded to some value ...)
    public_id = ''.join(chr(x) for x in range(PUBLIC_ID_LENGTH))
    public_id, aes_key, private_id = program(public_id, append_cr=True)
    last_session = 0
    last_session_uses = 0
    last_timestamp = 0
    last_timeoffset = 0
    # ... store these values in database with public_id
    print
    
    while True:
        print 'State:'
        print 'public_id', repr(public_id), binascii.b2a_hex(public_id), yubikey.modhex_encode(public_id)
        print 'aes_key', binascii.b2a_hex(aes_key), yubikey.modhex_encode(aes_key)
        print 'private_id', binascii.b2a_hex(private_id)
        print 'last_session', last_session
        print 'last_session_uses', last_session_uses
        print 'last_timestamp', last_timestamp
        print 'last_timeoffset', last_timeoffset
        # Somehow get a string of mumbo from the yubikey
        input_token = raw_input('Yubii? ').strip()
        print 'Skipping %r' % get_skipped_prefix(input_token) # this could be the password ...
        read_public_id = get_public_id(input_token)
        # Look read_public_id up in database and find the corresponding state, and then verify
        try:
            result = verify(read_public_id, aes_key, private_id, 
                            last_session, last_session_uses, last_timestamp, last_timeoffset,
                            input_token)
        except YubiVerificationFailed, e:
            # might log reason for failure on server, but don't leak to client!
            print repr(e)
            exit()
        print 'Yubikey successfully verified'
        # Store new updated values and use next time:
        last_session, last_session_uses, last_timestamp, last_timeoffset = result
        print

if __name__ == '__main__':
    demo()
