# http://www.nlnetlabs.nl/downloads/publications/hsm/hsm_node9.html PKCS #11
# http://www.rsa.com/rsalabs/node.asp?id=2133 PKCS #11: Cryptographic Token Interface Standard
# http://docs.sun.com/source/816-6150-10/pkcs.htm Implementing PKCS #11 for the Netscape Security Library
# http://www.w3.org/TR/xmldsig-core/#sec-PKCS1 PKCS1 (RSA-SHA1)
# http://tools.ietf.org/html/rfc3447#appendix-A.2.4
# http://jce.iaik.tugraz.at/sic/Products/Core-Crypto-Toolkits/PKCS-11-Provider/features says
#RawRSA/PKCS1 (raw RSA encryption [on the token] without hashing but with PKCS#1 padding)
#Uses the mechanism CKM_RSA_PKCS of the underlying PKCS#11 module.
#This algorithm does not calculate a hash at all, it just signs what it gets.
#The application must provide a hash value wrapped in a DigestInfo object as input.
#The PKCS padding is defined in PKCS#1 version 1.5.

# Pending questions regarding auth:
# pkcs11 for mac and linux?
# what to use for identification instead of casing number? CKA_SERIAL_NUMBER or CKA_ID?
# can we redistribute ikpkcs11.dll or where should we expect to find it?

__all__ = ['PKCS11Exception', 'PKCS11', 'CKA', 'RVs']

import ctypes
import _ctypes
import struct

def ulong(s):
    return struct.unpack('L', s)[0]

class PKCS11Exception(Exception): pass

class RVs(object):
    CKR_OK = 0x00000000
    CKR_CANCEL = 0x00000001
    CKR_HOST_MEMORY = 0x00000002
    CKR_SLOT_ID_INVALID = 0x00000003
    CKR_GENERAL_ERROR = 0x00000005
    CKR_FUNCTION_FAILED = 0x00000006
    CKR_ARGUMENTS_BAD = 0x00000007
    CKR_NO_EVENT = 0x00000008
    CKR_NEED_TO_CREATE_THREADS = 0x00000009
    CKR_CANT_LOCK = 0x0000000A
    CKR_ATTRIBUTE_READ_ONLY = 0x00000010
    CKR_ATTRIBUTE_SENSITIVE = 0x00000011
    CKR_ATTRIBUTE_TYPE_INVALID = 0x00000012
    CKR_ATTRIBUTE_VALUE_INVALID = 0x00000013
    CKR_DATA_INVALID = 0x00000020
    CKR_DATA_LEN_RANGE = 0x00000021
    CKR_DEVICE_ERROR = 0x00000030
    CKR_DEVICE_MEMORY = 0x00000031
    CKR_DEVICE_REMOVED = 0x00000032
    CKR_ENCRYPTED_DATA_INVALID = 0x00000040
    CKR_ENCRYPTED_DATA_LEN_RANGE = 0x00000041
    CKR_FUNCTION_CANCELED = 0x00000050
    CKR_FUNCTION_NOT_PARALLEL = 0x00000051
    CKR_FUNCTION_NOT_SUPPORTED = 0x00000054
    CKR_KEY_HANDLE_INVALID = 0x00000060
    CKR_KEY_SIZE_RANGE = 0x00000062
    CKR_KEY_TYPE_INCONSISTENT = 0x00000063
    CKR_KEY_NOT_NEEDED = 0x00000064
    CKR_KEY_CHANGED = 0x00000065
    CKR_KEY_NEEDED = 0x00000066
    CKR_KEY_INDIGESTIBLE = 0x00000067
    CKR_KEY_FUNCTION_NOT_PERMITTED = 0x00000068
    CKR_KEY_NOT_WRAPPABLE = 0x00000069
    CKR_KEY_UNEXTRACTABLE = 0x0000006A
    CKR_MECHANISM_INVALID = 0x00000070
    CKR_MECHANISM_PARAM_INVALID = 0x00000071
    CKR_OBJECT_HANDLE_INVALID = 0x00000082
    CKR_OPERATION_ACTIVE = 0x00000090
    CKR_OPERATION_NOT_INITIALIZED = 0x00000091
    CKR_PIN_INCORRECT = 0x000000A0
    CKR_PIN_INVALID = 0x000000A1
    CKR_PIN_LEN_RANGE = 0x000000A2
    CKR_PIN_EXPIRED = 0x000000A3
    CKR_PIN_LOCKED = 0x000000A4
    CKR_SESSION_CLOSED = 0x000000B0
    CKR_SESSION_COUNT = 0x000000B1
    CKR_SESSION_HANDLE_INVALID = 0x000000B3
    CKR_SESSION_PARALLEL_NOT_SUPPORTED = 0x000000B4
    CKR_SESSION_READ_ONLY = 0x000000B5
    CKR_SESSION_EXISTS = 0x000000B6
    CKR_SESSION_READ_ONLY_EXISTS = 0x000000B7
    CKR_SESSION_READ_WRITE_SO_EXISTS = 0x000000B8
    CKR_SIGNATURE_INVALID = 0x000000C0
    CKR_SIGNATURE_LEN_RANGE = 0x000000C1
    CKR_TEMPLATE_INCOMPLETE = 0x000000D0
    CKR_TEMPLATE_INCONSISTENT = 0x000000D1
    CKR_TOKEN_NOT_PRESENT = 0x000000E0
    CKR_TOKEN_NOT_RECOGNIZED = 0x000000E1
    CKR_TOKEN_WRITE_PROTECTED = 0x000000E2
    CKR_UNWRAPPING_KEY_HANDLE_INVALID = 0x000000F0
    CKR_UNWRAPPING_KEY_SIZE_RANGE = 0x000000F1
    CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT = 0x000000F2
    CKR_USER_ALREADY_LOGGED_IN = 0x00000100
    CKR_USER_NOT_LOGGED_IN = 0x00000101
    CKR_USER_PIN_NOT_INITIALIZED = 0x00000102
    CKR_USER_TYPE_INVALID = 0x00000103
    CKR_USER_ANOTHER_ALREADY_LOGGED_IN = 0x00000104
    CKR_USER_TOO_MANY_TYPES = 0x00000105
    CKR_WRAPPED_KEY_INVALID = 0x00000110
    CKR_WRAPPED_KEY_LEN_RANGE = 0x00000112
    CKR_WRAPPING_KEY_HANDLE_INVALID = 0x00000113
    CKR_WRAPPING_KEY_SIZE_RANGE = 0x00000114
    CKR_WRAPPING_KEY_TYPE_INCONSISTENT = 0x00000115
    CKR_RANDOM_SEED_NOT_SUPPORTED = 0x00000120
    CKR_RANDOM_NO_RNG = 0x00000121
    CKR_DOMAIN_PARAMS_INVALID = 0x00000130
    CKR_BUFFER_TOO_SMALL = 0x00000150
    CKR_SAVED_STATE_INVALID = 0x00000160
    CKR_INFORMATION_SENSITIVE = 0x00000170
    CKR_STATE_UNSAVEABLE = 0x00000180
    CKR_CRYPTOKI_NOT_INITIALIZED = 0x00000190
    CKR_CRYPTOKI_ALREADY_INITIALIZED = 0x00000191
    CKR_MUTEX_BAD = 0x000001A0
    CKR_MUTEX_NOT_LOCKED = 0x000001A1
    CKR_NEW_PIN_MODE = 0x000001B0
    CKR_NEXT_OTP = 0x000001B1
    CKR_FUNCTION_REJECTED = 0x00000200
    CKR_VENDOR_DEFINED = 0x80000000

RV_STRING = dict((b, a) for (a, b) in RVs.__dict__.items())

def check(res, silent=None):
    if silent and res in silent:
        return res
    if res:
        raise PKCS11Exception('Error %s: %s' % (res, RV_STRING.get(res, 'unknown')))

class CK_VERSION(ctypes.Structure):
    _fields_ = [
            ("major", ctypes.c_byte), # integer portion of version number
            ("minor", ctypes.c_byte), # 1/100ths portion of version number
            ]
    def __str__(self):
        return '%s.%s' % (self.major, self.minor)

class CK_SLOT_INFO(ctypes.Structure):
    _fields_ = [
            ("slotDescription", ctypes.c_char * 64),
            ("manufacturerID", ctypes.c_char * 32),
            ("flags", ctypes.c_ulong),
            ("hardwareVersion", CK_VERSION),
            ("firmwareVersion", CK_VERSION),
            ]
    # flags:
    CKF_TOKEN_PRESENT = 0x00000001 # a token is there
    CKF_REMOVABLE_DEVICE = 0x00000002 # removable devices
    CKF_HW_SLOT = 0x00000004 # hardware slot

    def dump(self):
        print '%s:' % self.__class__.__name__
        print '\tslotDescription: %r' % self.slotDescription
        print '\tmanufacturerID: %r' % self.manufacturerID
        print '\tflags: %s (%x)' % (' '.join(
                (['CKF_TOKEN_PRESENT'] if self.flags & self.CKF_TOKEN_PRESENT else []) + # a token is there
                (['CKF_REMOVABLE_DEVICE'] if self.flags & self.CKF_REMOVABLE_DEVICE else []) + # removable devices
                (['CKF_HW_SLOT'] if self.flags & self.CKF_HW_SLOT else []) # hardware slot
                ), self.flags)
        print '\thardwareVersion: %s' % self.hardwareVersion
        print '\tfirmwareVersion: %s' % self.firmwareVersion

class CK_TOKEN_INFO(ctypes.Structure):
    _fields_ = [
            ("label", ctypes.c_char * 32),
            ("manufacturerID", ctypes.c_char * 32),
            ("model", ctypes.c_char * 16),
            ("serialNumber", ctypes.c_char * 16),
            ("flags", ctypes.c_ulong),
            ("ulMaxSessionCount", ctypes.c_ulong), # max open sessions
            ("ulSessionCount", ctypes.c_ulong), # sess. now open
            ("ulMaxRwSessionCount", ctypes.c_ulong), # max R/W sessions
            ("ulRwSessionCount", ctypes.c_ulong), # R/W sess. now open
            ("ulMaxPinLen", ctypes.c_ulong),
            ("ulMinPinLen", ctypes.c_ulong),
            ("ulTotalPublicMemory", ctypes.c_long), # ulong, but can also be -1 ...
            ("ulFreePublicMemory", ctypes.c_long),
            ("ulTotalPrivateMemory", ctypes.c_long),
            ("ulFreePrivateMemory", ctypes.c_long),
            ("hardwareVersion", CK_VERSION),
            ("firmwareVersion", CK_VERSION),
            ("utcTime", ctypes.c_char * 16),
            ]
    # flags:
    CKF_RNG = 0x00000001 # has random # generator
    CKF_WRITE_PROTECTED = 0x00000002 # token is write-protected
    CKF_LOGIN_REQUIRED = 0x00000004 # user must login
    CKF_USER_PIN_INITIALIZED = 0x00000008 # normal user's PIN is set

    def dump(self):
        print '%s:' % self.__class__.__name__
        print '\tlabel: %r' % self.label
        print '\tmanufacturerID: %r' % self.manufacturerID
        print '\tmodel: %r' % self.model
        print '\tserialNumber: %r' % self.serialNumber
        print '\tflags: %s (%x)' % (' '.join(
                (['CKF_RNG'] if self.flags & self.CKF_RNG else []) +
                (['CKF_WRITE_PROTECTED'] if self.flags & self.CKF_WRITE_PROTECTED else []) +
                (['CKF_LOGIN_REQUIRED'] if self.flags & self.CKF_LOGIN_REQUIRED else []) +
                (['CKF_USER_PIN_INITIALIZED'] if self.flags & self.CKF_USER_PIN_INITIALIZED else [])
                ), self.flags)
        print '\tulSessionCount: %s of %s' % (self.ulSessionCount, self.ulMaxSessionCount)
        print '\tulRwSessionCount: %s of %s' % (self.ulRwSessionCount, self.ulMaxRwSessionCount)
        print '\tulMinPinLen: %s ulMaxPinLen: %s' % (self.ulMinPinLen, self.ulMaxPinLen)
        print '\tulFreePublicMemory: %s of %s' % (self.ulFreePublicMemory, self.ulTotalPublicMemory)
        print '\tulFreePrivateMemory: %s of %s' % (self.ulFreePrivateMemory, self.ulTotalPrivateMemory)
        print '\thardwareVersion: %s' % self.hardwareVersion
        print '\tfirmwareVersion: %s' % self.firmwareVersion
        print '\tutcTime: %r' % self.utcTime

# C_OpenSession flags:
CKF_RW_SESSION = 0x00000002 # session is r/w
CKF_SERIAL_SESSION = 0x00000004 # no parallel

# User types, for C_Login:
CKU_SO = 0 # Security Officer
CKU_USER = 1 # Normal user
CKU_CONTEXT_SPECIFIC = 2 # Context specific # NOT supported by IronKey

class CK_ATTRIBUTE(ctypes.Structure):
    """CK_ATTRIBUTE is a structure that includes the type, length and value of an attribute"""
    _fields_ = [
            ("type", ctypes.c_ulong), # CKA.*
            ("pValue", ctypes.c_void_p),
            ("ulValueLen", ctypes.c_ulong),
            ]

# Attributes:
class CKA:
    CLASS = 0x00000000
    TOKEN = 0x00000001
    PRIVATE = 0x00000002
    LABEL = 0x00000003
    APPLICATION = 0x00000010
    VALUE = 0x00000011
    OBJECT_ID = 0x00000012 # DER-encoded
    CERTIFICATE_TYPE = 0x00000080
    ISSUER = 0x00000081 # DER-encoded
    SERIAL_NUMBER = 0x00000082 # DER-encoded
    AC_ISSUER = 0x00000083
    OWNER = 0x00000084
    ATTR_TYPES = 0x00000085
    KEY_TYPE = 0x00000100
    SUBJECT = 0x00000101 # DER-encoded
    ID = 0x00000102
    SENSITIVE = 0x00000103
    ENCRYPT = 0x00000104
    DECRYPT = 0x00000105
    WRAP = 0x00000106
    UNWRAP = 0x00000107
    SIGN = 0x00000108
    SIGN_RECOVER = 0x00000109
    VERIFY = 0x0000010A
    VERIFY_RECOVER = 0x0000010B
    DERIVE = 0x0000010C
    MODULUS = 0x00000120
    MODULUS_BITS = 0x00000121
    PUBLIC_EXPONENT = 0x00000122
    EXTRACTABLE = 0x00000162
    LOCAL = 0x00000163
    NEVER_EXTRACTABLE = 0x00000164
    ALWAYS_SENSITIVE = 0x00000165
    MODIFIABLE = 0x00000170
CKA_names = dict((b, a) for (a, b) in CKA.__dict__.items())

# CKA.CERTIFICATE_TYPE values:
CKC_X_509 = 0x00000000
CKC_X_509_ATTR_CERT = 0x00000001
CKC_WTLS = 0x00000002
CKC_VENDOR_DEFINED = 0x80000000

# Object classes - values for CKA.CLASS:
CKO_DATA = 0x00000000
CKO_CERTIFICATE = 0x00000001
CKO_PUBLIC_KEY = 0x00000002
CKO_PRIVATE_KEY = 0x00000003
CKO_SECRET_KEY = 0x00000004
CKO_HW_FEATURE = 0x00000005
CKO_DOMAIN_PARAMETERS = 0x00000006
CKO_MECHANISM = 0x00000007
CKO_VENDOR_DEFINED = 0x80000000

class CK_MECHANISM(ctypes.Structure):
    _fields_ = [
            ("mechanism", ctypes.c_ulong), # CKM_*
            ("pParameter", ctypes.c_void_p),
            ("ulParameterLen", ctypes.c_ulong),
            ]
    # Mechanism types:
    CKM_RSA_PKCS_KEY_PAIR_GEN = 0x00000000 # for CKF_GENERATE_KEY_PAIR
    CKM_RSA_PKCS = 0x00000001 # for CKF_SIGN
    CKM_RSA_X_509 = 0x00000003 # for CKF_SIGN
    CKM_SHA1_RSA_PKCS = 0x00000006 # for CKF_SIGN


class PKCS11(object):

    def __init__(self, dll='ikpkcs11.dll'): # 'aetpkss1.dll' 'gdpkcs11.dll'
        self.dll = dll
        self.p11 = None

    def __enter__(self):
        self.p11 = ctypes.CDLL(self.dll)
        self.p11.C_Initialize.argtypes = [ctypes.c_void_p]
        self.p11.C_GetSlotList.argtypes = [ctypes.c_bool, ctypes.c_void_p, ctypes.POINTER(ctypes.c_ulong)]
        self.p11.C_GetSlotInfo.argtypes = [ctypes.c_ulong, ctypes.POINTER(CK_SLOT_INFO)]
        self.p11.C_GetTokenInfo.argtypes = [ctypes.c_ulong, ctypes.POINTER(CK_TOKEN_INFO)]
        self.p11.C_OpenSession.argtypes = [ctypes.c_ulong, ctypes.c_ulong, ctypes.c_void_p, ctypes.c_void_p, ctypes.POINTER(ctypes.c_ulong)]
        self.p11.C_FindObjectsInit.argtypes = [ctypes.c_ulong, ctypes.c_void_p, ctypes.c_ulong]
        self.p11.C_FindObjects.argtypes = [ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong), ctypes.c_ulong, ctypes.POINTER(ctypes.c_ulong)]
        self.p11.C_GetAttributeValue.argtypes = [ctypes.c_ulong, ctypes.c_ulong, ctypes.c_void_p, ctypes.c_ulong]
        self.p11.C_FindObjectsFinal.argtypes = [ctypes.c_ulong]
        self.p11.C_SignInit.argtypes = [ctypes.c_ulong, ctypes.POINTER(CK_MECHANISM), ctypes.c_ulong]
        self.p11.C_Sign.argtypes = [ctypes.c_ulong, ctypes.c_void_p, ctypes.c_ulong, ctypes.c_void_p, ctypes.POINTER(ctypes.c_ulong)]
        self.p11.C_CloseSession.argtypes = [ctypes.c_ulong]
        self.p11.C_Finalize.argtypes = [ctypes.c_void_p]
        check(self.p11.C_Initialize(None))
        return self

    def getslots(self):
        ulCount = ctypes.c_ulong()
        check(self.p11.C_GetSlotList(True, None, ulCount))
        slots = (ctypes.c_ulong * ulCount.value)()
        check(self.p11.C_GetSlotList(True, slots, ulCount))
        return list(slots)

    def getslotinfo(self, slotId):
        slotInfo = CK_SLOT_INFO()
        check(self.p11.C_GetSlotInfo(slotId, slotInfo))
        return slotInfo

    def gettokeninfo(self, slotId):
        tokenInfo = CK_TOKEN_INFO()
        check(self.p11.C_GetTokenInfo(slotId, tokenInfo))
        return tokenInfo

    def session(self, slotId, writable=False):
        return PKCS11Session(self.p11, slotId, writable)

    def __exit__(self, _type, _value, _traceback):
        check(self.p11.C_Finalize(None))
        _ctypes.FreeLibrary(self.p11._handle)

class PKCS11Session(object):
    def __init__(self, p11, slotId, writable=False):
        self._p11 = p11
        self._slotId = slotId
        self._writable = writable
        self._hSession = None

    def __enter__(self):
        flags = CKF_SERIAL_SESSION | (CKF_RW_SESSION if self._writable else 0)
        pApplication = None # passed to callback
        Notify = None # callback function
        self._hSession = ctypes.c_ulong()
        check(self._p11.C_OpenSession(self._slotId, flags, pApplication, Notify, self._hSession))
        return self

    def __exit__(self, _type, _value, _traceback):
        pass # don't try to close after serious errors - that just causes trouble

    def close(self):
        check(self._p11.C_CloseSession(self._slotId),
              silent=[RVs.CKR_TOKEN_NOT_PRESENT])

    def C_GetSessionInfo(self):
        check(self._p11.C_GetSessionInfo(self._hSession))

    def login(self, pin, so=False):
        check(self._p11.C_Login(self._hSession, CKU_SO if so else CKU_USER, pin, len(pin))) # seems to not work ...

    def _find_objects(self, query=None, attribute_types=None):
        """find matching objects and return the specified attributes"""
        if query is None:
            query = {}
        findtemplate = (CK_ATTRIBUTE * len(query))()
        ul_keyClasses = [None] * len(query)
        for i, (at, v) in enumerate(query.items()):
            if isinstance(v, (int, long)):
                ul_keyClasses[i] = ctypes.c_ulong(v)
            #elif isinstance(v, str):
            #    ul_keyClasses[i] = ctypes.create_string_buffer(v)
            else:
                raise NotImplementedError
            findtemplate[i].type = at
            findtemplate[i].pValue = ctypes.addressof(ul_keyClasses[i])
            findtemplate[i].ulValueLen = ctypes.sizeof(ul_keyClasses[i])
        if attribute_types is None:
            attribute_types = [CKA.ID]
        check(self._p11.C_FindObjectsInit(self._hSession, findtemplate, len(findtemplate)))
        try:
            ulObjectCount = ctypes.c_ulong()
            results = []
            while True:
                hObject = ctypes.c_ulong()
                check(self._p11.C_FindObjects(self._hSession, hObject, 1, ulObjectCount))
                if not ulObjectCount:
                    break
                values = self.get_attributes(hObject.value, attribute_types)
                values[None] = hObject.value
                results.append(values)
            return results
        finally:
            check(self._p11.C_FindObjectsFinal(self._hSession))

    def get_attributes(self, hObject, attribute_types):
        template = (CK_ATTRIBUTE * len(attribute_types))()
        for i, at in enumerate(attribute_types):
            template[i].type = attribute_types[i]
            template[i].pValue = None
            template[i].ulValueLen = 0
        cvalues = {}
        rv = check(self._p11.C_GetAttributeValue(self._hSession, hObject, template, len(attribute_types)),
                silent=[RVs.CKR_ATTRIBUTE_TYPE_INVALID, RVs.CKR_ATTRIBUTE_SENSITIVE])
        if not rv:
            for i, at in enumerate(attribute_types):
                if template[i].ulValueLen == 0xffffffff:
                    pass
                    # print 'invalid length for', i, attribute_types[i]
                else:
                    #print i, template[i], template[i].ulValueLen
                    cvalues[at] = (ctypes.c_char * template[i].ulValueLen)()
                    template[i].pValue = ctypes.addressof(cvalues[at])
            check(self._p11.C_GetAttributeValue(self._hSession, hObject, template, len(attribute_types)))
        return dict((k, v.raw) for (k, v) in cvalues.items())

    def get_all_keys(self, query=None, attribute_types=None):
        if attribute_types is None:
            attribute_types = [
                        CKA.CLASS, # always available ... we assume ...
                        CKA.LABEL, # not always available ...
                        ]
        return self._find_objects(query=query, attribute_types=attribute_types)

    def dump_keys(self, hObject):
        print 'hObject 0x%x:' % hObject
        for i in range(0x000, 0x1000):
            r = self.get_attributes(hObject, attribute_types=[i])
            if r:
                print '\t%s\t%r' % (CKA_names.get(i, '0x%x' % i), r[i])

    def get_cert(self):
        r = self._find_objects(
            query={CKA.CLASS: CKO_CERTIFICATE},
            attribute_types=[
                    CKA.CLASS, # CKO_CERTIFICATE = 1
                    CKA.TOKEN, # 1
                    CKA.PRIVATE, # 0
                    CKA.LABEL, # 'C_Browser'
                    CKA.VALUE, # asn.1 TaggedCertificate: ...
                    CKA.CERTIFICATE_TYPE, # CKC_X_509 = 0
                    CKA.ISSUER, # asn.1 Name: commonName='IronKey Browser CA - G1', organizationName=IronKey
                    CKA.SERIAL_NUMBER, # '%&keP\x85k\x07' = 0x25266b6550856b07 = 2676945111405128455
                    CKA.SUBJECT, # asn.1 Name: commonName=0108535916024f419C0B02af98, organizationName=IronKey (apparently also available as IK_GetDeviceID but should by 8 bytes!?)
                    CKA.ID, # 'X\x9dM\n\x18\x17\xc7\x93\tv\xdb\xab/{3\x1a\x0c&Vz'
                    CKA.MODIFIABLE, # ?
                    ])
        if r and len(r) == 1:
            return r[0]
        raise PKCS11Exception("Found %s certificates" % (r and len(r) or 0))

    def get_public_key(self):
        r = self._find_objects(
                query={CKA.CLASS: CKO_PUBLIC_KEY},
                attribute_types=[
                        #CKA.CLASS, #  CKO_PUBLIC_KEY
                        #CKA.LABEL, # 'C_Browser'
                        #CKA.KEY_TYPE, # '' = 0 = CKK_RSA
                        #CKA.SUBJECT,
                        CKA.ID, # 'X\x9dM\n\x18\x17\xc7\x93\tv\xdb\xab/{3\x1a\x0c&Vz'
                        #CKA.ENCRYPT, # 1
                        #CKA.WRAP, # 1
                        #CKA.VERIFY, # 1
                        #CKA.VERIFY_RECOVER, # 1
                        #CKA.DERIVE, # 0
                        CKA.MODULUS, # the interesting 2048 bits
                        #CKA.MODULUS_BITS, # '\x08'
                        CKA.PUBLIC_EXPONENT, # '\x01\x00\x01'
                        #CKA.LOCAL, # 1
                        ])
        if r and len(r) == 1:
            return r[0][CKA.ID], r[0][CKA.MODULUS], r[0][CKA.PUBLIC_EXPONENT]
        raise PKCS11Exception("Found %s public keys" % (r and len(r) or 0))

    def get_private_key_id(self):
        r = self._find_objects(
                query={CKA.CLASS: CKO_PRIVATE_KEY},
                attribute_types=[
                        # mostly like public key but also
                        #CKA.SENSITIVE, # 1
                        #CKA.DECRYPT, # 1
                        #CKA.SIGN, # 1
                        #CKA.SIGN_RECOVER, # 1
                        #CKA.EXTRACTABLE, # 0
                        #CKA.NEVER_EXTRACTABLE, # 1
                        #CKA.ALWAYS_SENSITIVE # 1
                        ]) # nothing interesting returned, but can be used for signing ...
        if r and len(r) == 1:
            return r[0][None]
        raise PKCS11Exception("Found %s private keys" % (r and len(r) or 0))

    def get_data(self):
        r = self._find_objects(
                query={CKA.CLASS: CKO_DATA},
                attribute_types=[
                        # nothing interesting:
                        CKA.TOKEN, # 1
                        CKA.PRIVATE, # 1
                        CKA.LABEL, # 'IK_COI_INFO'
                        CKA.MODIFIABLE, # 0
                        ])
        return r

    def challenge(self, private_key_id, challenge):
        signature = ctypes.create_string_buffer(256)
        signature_len = ctypes.c_ulong(ctypes.sizeof(signature))
        # challenge = _digestInfofy(_sha1(challenge)) # alternative needed for CK_MECHANISM.CKM_RSA_PKCS
        check(self._p11.C_SignInit(self._hSession, CK_MECHANISM(CK_MECHANISM.CKM_SHA1_RSA_PKCS, None, 0), private_key_id))
        check(self._p11.C_Sign(self._hSession, challenge, len(challenge), signature, signature_len))
        return signature.raw
