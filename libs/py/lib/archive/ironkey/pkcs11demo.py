import pkcs11

import hashlib

_digestinfo_sha1_prefix = '0!0\t\x06\x05+\x0e\x03\x02\x1a\x05\x00\x04\x14'
# _digestinfo_sha1_prefix = TaggedDigestInfo.der_encode({'digestAlgorithm': {'type': ((1, 3, 14, 3, 2, 26), None), 'value': None}, 'digest': challengehash20, })

def _digestInfofy(s):
    """encapsulate in rfc3447 RSASSA-PKCS1-v1_5 DigestInfo with
    digestAlgorithm iso(1).identified-organization(3).oiw(14).secsig(3).algorithms(2).id-_sha1(26)"""
    if len(s) != 20: raise RuntimeError('Invalid sha1 %r' % s)
    return _digestinfo_sha1_prefix + s

def _sha1(s):
    return hashlib.sha1(s).digest()

def verify(challenge, signed_challenge, exponent, public_modulus, M2PKCS1=False, M2UNPAD=False):
    def pkcs1pad(s, l):
        """encapsulate in pkcs#1 1.5 padding up to the specified length
        """
        return '\x00\x01' + (l - 3 - len(s)) * '\xff' + '\x00' + s
    def pkcs1unpad(s):
        i = s.find('\0', 6)
        if s[:i + 1] != '\x00\x01' + '\xff' * (i-2) + '\0': raise RuntimeError('Invalid padding for %r' % s)
        return s[i + 1:]
    def undigestInfofy(s):
        if not s.startswith(_digestinfo_sha1_prefix): raise RuntimeError('Invalid digestinfo prefix for %r' % s)
        h = s[len(_digestinfo_sha1_prefix):]
        if len(h) != 20: raise RuntimeError('Invalid sha1 %r' % h)
        return h
    challenge_sha = _sha1(challenge) # input for CKM_SHA1_RSA_PKCS
    challenge_algotagged = _digestInfofy(challenge_sha) # input for CKM_RSA_PKCS
    challenge_algotagged_pkcs1 = pkcs1pad(challenge_algotagged, 256) # expected output in all cases
    assert undigestInfofy(pkcs1unpad(challenge_algotagged_pkcs1)) == challenge_sha # just testing ...

    if M2PKCS1 or M2UNPAD:
        import M2Crypto
        #rsa = M2Crypto.RSA.gen_key(2048, 65537)
        #print 'rsa.pub()', rsa.pub(), len(rsa.pub()[1])
        #rsa_pub = M2Crypto.RSA.new_pub_key(rsa.pub())

        if len(exponent) != 3: raise RuntimeError('Invalid exponent %r' % exponent)
        if len(public_modulus) != 0x101 - 1: raise RuntimeError('Invalid modulus %r' % public_modulus)
        m2_public_key = ('\0\0\0\x03' + exponent, '\x00\x00\x01\x01\x00' + public_modulus)
        #print 'm2_public_key', m2_public_key
        rsa_pub = M2Crypto.RSA.new_pub_key(m2_public_key)

        if M2PKCS1:
            decoded = rsa_pub.public_decrypt(signed_challenge, M2Crypto.RSA.pkcs1_padding)
            success = decoded == challenge_algotagged
        else:
            decoded = rsa_pub.public_decrypt(signed_challenge, M2Crypto.RSA.no_padding)
            success = decoded == challenge_algotagged_pkcs1
    else:
        def rsaencrypt(sig, exp, mod):
            def s2int(s):
                i = 0
                for c in s:
                    i = (i << 8) + ord(c)
                return i

            def int2s(i, l=8):
                def f(i):
                    while i:
                        yield chr(i & 0xff)
                        i >>= 8
                s = ''.join(f(i))[::-1]
                return '\0' * ((l - len(s) % l) % l) + s

            return int2s(pow(s2int(sig), s2int(exp), s2int(mod)))
        decoded = rsaencrypt(signed_challenge, exponent, public_modulus)
        success = decoded == challenge_algotagged_pkcs1
    #print 'decoded:', repr(decoded)
    return success


def demo(dll='e:/windows/ikpkcs11.dll', slot=0, writable=False, so=False, pin='', dump=False):
    with pkcs11.PKCS11(dll) as pkcs11:
        slots = pkcs11.getslots()
        print len(slots), 'slots:', slots
        assert slots
        for i, slot in enumerate(slots):
            print 'slot: %s (%s)' % (i, slot)
            pkcs11.getslotinfo(slot).dump()

        slot = slots[0]
        print 'using slot:', slot
        pkcs11.gettokeninfo(slot).dump()

        challenge = 'tra la la' # will be hashed

        with pkcs11.session(slot, writable=writable) as pkcs11session:
            if pin:
                print 'logging in:'
                pkcs11session.login(pin, so=so)

            if dump:
                print 'dumping:'
                for hObject in (x[None] for x in pkcs11session.get_all_keys(attribute_types=[])):
                    pkcs11session.dump_keys(hObject)

            if True:
                print 'getting cert:'
                r = pkcs11session.get_cert() # will fail if not logged in
                print 'cert serial number: %r' % r[pkcs11.CKA.SERIAL_NUMBER]
                print 'cert id: %r' % r[pkcs11.CKA.ID] # = public key token_id

            token_id, public_modulus, exponent = pkcs11session.get_public_key()
            print 'public key id: %r' % token_id

            private_key_id = pkcs11session.get_private_key_id()
            print 'private key id: %s' % private_key_id
            signed_challenge = pkcs11session.challenge(private_key_id, challenge)

            print 'closing:'
            pkcs11session.close()

        verified = verify(challenge, signed_challenge, exponent, public_modulus)
        print 'verified:', verified

if __name__ == '__main__':
    demo() # IronKey
    #demo(slot=1, writable=1, so=1, pin='112233') # G&D CAC
    #demo(writable=1, so=0, pin='1111', dump=1) # AET - verify fails
