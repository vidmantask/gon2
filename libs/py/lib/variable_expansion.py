import re

_variable_re = re.compile(r'%%?\(([^.%\(\)]+)\.([^%\(\)]*)\)')

def expand(template_string, getter):
    """
    Expand variables of form "%(foo.bar)" in template_string, replacing with value of getter('foo', 'bar').
    "%%" is expanded to "%" and a following round bracket isn't expanded.
    The strings can contain anything but round brackets and percent - and the first dot separates the two parts.
    
    >>> getter = lambda a,b: "<%s:%s>" % (a,b)
    >>> expand('%', getter) # could warn ...
    '%'
    >>> expand('%%', getter)
    '%%'
    >>> expand('%(', getter) # could warn ...
    '%('
    >>> expand('%()', getter) # could warn ...
    '%()'
    >>> expand('%(.)', getter) # could warn ...
    '%(.)'
    >>> expand('%(foo)', getter) # could warn ...
    '%(foo)'
    >>> expand('%(foo.)', getter)
    '<foo:>'
    >>> expand('%(foo.bar)', getter)
    '<foo:bar>'
    >>> expand('%(foo.bar.baz)', getter)
    '<foo:bar.baz>'
    >>> expand('%(foo.bar baz)', getter)
    '<foo:bar baz>'
    >>> expand('%(foo.bar%%baz)', getter) # not recursive, percent not allowed inside, could warn ...
    '%(foo.bar%%baz)'
    >>> expand('%(foo.bar%(foo.baz))', getter) # not recursive, percent not allowed inside, could warn ...
    '%(foo.bar<foo:baz>)'
    >>> expand('%(foo.bar)', lambda a,b: None)
    '%(foo.bar)'

    >>> expand(u'%(foo.bar)', (lambda a,b: "<%s:%s>" % (a,b))) # unicode template
    u'<foo:bar>'
    >>> expand('%(foo.bar)', (lambda a,b: u"<%s:%s>" % (a,b))) # unicode value
    u'<foo:bar>'

    # Some tests for backwards-compatible %% handling
    >>> expand('%%(foo.bar)', getter)
    '<foo:bar>'
    >>> expand('%%(foo.bar)', lambda a,b: None)
    '%(foo.bar)'
    >>> expand('%%%(foo.bar)', getter)
    '%<foo:bar>'
    >>> expand('%%%%(foo.bar)', getter)
    '%%<foo:bar>'
    """

    def get_variable(matchobj):
        """Return looked up value for matchobj which comes from _variable_re"""
        first = matchobj.group(1)
        second = matchobj.group(2)
        value = getter(first, second)
        if value is None:
            return "%%(%s.%s)" % (first,second)
        if isinstance(value, basestring):
            return value
        return str(value)

    if not template_string:
        return ''
    return _variable_re.sub(get_variable, template_string)

_trailing_percent_one_re = re.compile(r'(.*) +(%1|"%1") *')

def strip_trailing_percent_one(s):
    """
    Strip spaces and trailing %1's.
    
    Windows registry sometimes contains templates with %1 - for now we assume they come last and we just strip them.
    Both the problem and this workaround is quite ugly.
    Note that the old 3.4 behaviour is to only use the part up to the first .exe. 
    
    >>> strip_trailing_percent_one(None)
    >>> strip_trailing_percent_one(' foo bar ')
    'foo bar'
    >>> strip_trailing_percent_one(' foo bar %1')
    'foo bar'
    >>> strip_trailing_percent_one(' foo bar  %1  ')
    'foo bar'
    >>> strip_trailing_percent_one(' foo bar "%1" ')
    'foo bar'
    >>> strip_trailing_percent_one('foo %s bar') # not supported
    'foo %s bar'
    """
    if not s:
        return s
    matches = _trailing_percent_one_re.match(s)
    if matches:
        return matches.group(1).strip()
    return s.strip()


def _test():
    import doctest # will fail because of token.py ...
    doctest.testmod()

if __name__ == "__main__":
    _test()
