import socket
import errno
import time
import sys
import asyncore
from collections import deque

if sys.platform == 'win32':
    timer = time.clock
else:
    timer = time.time


class StreamBuffer(object):

    CLOSE = object() # should be checked with "is", not "=="

    def __init__(self):
        self._buffer = '' # pending data
        self._pending_close = False # close as soon as _buffer is empty - nothing will be added

    def pending(self):
        """Get buffered stream content as string, CLOSE if closing - thus true if anything to handle"""
        if self._buffer:
            return self._buffer
        if self._pending_close:
            return self.CLOSE
        return ''

    def add(self, chunk=None, close=False):
        """Add chunk to stream"""
        if chunk is None:
            assert close
            self._pending_close = True
        else:
            assert not close
            assert not self._pending_close
            self._buffer += chunk # Note: O(n^2), but n usually small - and if buffer or chunk is empty then it's O(1)

    def eat(self, size=None, close=False):
        """Remove size bytes of content from buffer - or pending close"""
        if size is None:
            assert close
            assert self._pending_close
            self._pending_close = False
            # Now we could go to a dead state ...
        else:
            self._buffer = self._buffer[size:]


class TcpBase(object):
    """
    Something which looks like a TcpMessage
    """

    def __init__(self, session, tcp_key, symbol, seq, closing=False):
        self.session = session
        self.tcp_key = tcp_key
        self.seq = seq
        self.closing = closing
        self.symbol = symbol

    def __repr__(self):
        return '%s.%s%s%s' % (self.session, self.tcp_key or 'ping', self.symbol, self.seq) + ('closing' if self.closing else '')

    __str__ = __repr__


class TcpMessage(TcpBase):
    """
    The kind of messages sent between TcpConnections
    """

    def __init__(self, session, tcp_key, symbol, seq, data=None, closing=False, max_retrans=10):
        TcpBase.__init__(self, session, tcp_key, symbol, seq, closing=closing)
        self.data = data
        self._retrans_deadline = None
        self._retrans_count = max_retrans

    def start_retrans_timer(self, timeout):
        self._retrans_deadline = timer() + timeout

    def stop_retrans_timer(self):
        self._retrans_deadline = None

    def should_retrans(self):
        return self._retrans_deadline and timer() > self._retrans_deadline

    def count_retrans(self):
        """
        Count one retrans, getting closer to give_up_retrans
        """
        self._retrans_count -= 1

    def give_up_retrans(self):
        """
        Return True if count_retrans done max_retrans times
        """
        return self._retrans_count == 0


class TcpConnection(asyncore.dispatcher):
    """
    A connection to a TCP service
    """

    def __init__(self, session, env, tcp_key, tcp_socket, target_addr, async_map):
        asyncore.dispatcher.__init__(self, tcp_socket, map=async_map)
        self._env = env
        self._session = session # The one we serve ...
        self._tcp_key = tcp_key

        self._out_buffer = StreamBuffer()
        self._my_seq = 0 # what I have sent
        self._my_seq_acked = 0 # how far has _my_seq been fully acked
        self._unacked = deque() # unacked transmissions, invariant: left is my_seq_acked+1, right is _my_seq
        self._their_seq = 0 # what I have fully received
        self._received_out_of_order = {} # packages received ahead of _their_seq
        self._tcp_close_to_http = False # If True: Will not send new seqs over HTTP - but will continue retransmitting
        self._http_close_to_tcp = False # If True: Will not receive new seqs over HTTP - but will continue receiving acks
        self._tcp_error = False # If True: Tcp Socket is dead - close connection as soon and nice as possible
        self._tcp_idle_deadline = timer() + self._env.config.MAX_IDLE_TCP
        self._unack_throttling = False

        self._stat_messages = 0 # Total number of messages received
        self._stat_got_out_of_order = 0 # Number of messages accepted out of order
        self._stat_too_out_of_order = 0 # Number of messages dropped out of order
        self._stat_received_outdated = 0 # Number of outdated retransmissions (indicates too many retransmissions)
        self._stat_recvs = 0 # Number of socket recvs
        self._stat_recved = 0 # Bytes received
        self._stat_sends = 0 # Nunber of socket sends
        self._stat_sent = 0 # Bytes sent
        self._stat_retrans_sent = 0 # Number of retransmissions sent (out of _my_seq)

        if tcp_socket:
            assert not target_addr
            self._send_message('') # Important on client to allow server to start - less important on server...
        else:
            assert target_addr
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connect(target_addr) # Will cause handle_connect

    def handle_connect(self):
        (source_host, source_port) = self.getsockname()
        try:
            (target_host, target_port) = self.getpeername()
        except socket.error: # have once seen 107, 'Transport endpoint is not connected', probably because target closed connection
            self._env.log1('%s created on %s from %s:%s but error getting peer name', self, self._session, source_host, source_port)
            return
        self._env.log1('%s created on %s from %s:%s to %s:%s', self, self._session, source_host, source_port, target_host, target_port)

    # asyncore
    def handle_expt(self):
        self._env.log1('%s exception', self)
        self._handle_tcp_error()

    def _handle_tcp_error(self):
        self._tcp_error = True
        # TCP to HTTP
        self.close() # pretend that TCP was closed by other end
        if self._tcp_close_to_http:
            self._env.log2('%s handling tcp_error - TCP already closed', self)
        else:
            self._env.log2('%s handling tcp_error - closing TCP', self)
            self._tcp_close_to_http = True
            self._send_message('')
        # HTTP to TCP
        self._received_out_of_order.clear() # We don't care ...
        self._their_seq = sys.maxint # We don't care ...
        self._session.enqueue_ack(self._tcp_key, self._their_seq)
        # Are we done?
        self._check_done()

    # asyncore
    def readable(self):
        return not self._unack_throttling and not self._tcp_close_to_http

    # asyncore
    def handle_close(self):
        """
        Called on various errors and when .recv reads 0 because half or fully closed
        """
        self._env.log2('%s got socket close', self)
        if not self.connected:
            self._env.log1('%s connection failed', self)
            self.close()

    # asyncore
    def handle_read(self):
        """
        Something happened on tcp_socket - it must be TCP data to send in HTTP reply
        """
        self._tcp_idle_deadline = timer() + self._env.config.MAX_IDLE_TCP
        if self._tcp_close_to_http:
            self._env.log2('%s received data after close - ignoring', self)
            return # avoid sending close over HTTP twice
        try:
            chunk = self.recv(self._env.config.TCP_RECV_BUFFER) # might call handle_close and set self._tcp_close_to_http and return empty chunk
        except socket.error, (err, msg):
            self._env.log2('%s socket error reading TCP: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
            return self._handle_tcp_error()
        self._env.log2('%s recv chunk %s bytes', self, len(chunk))
        self._env.log3('%s recv chunk %r', self, chunk)
        self._stat_recvs += 1
        self._stat_recved += len(chunk)
        if not chunk:
            self._env.log2('%s got shutdown from tcp', self)
            self._tcp_close_to_http = True
            self._check_done()
        if self._my_seq > self._my_seq_acked + self._env.config.MAX_UNACK and not self._tcp_error:
            self._env.log2('%s throttling; has too many unacked packages and slows down - has sent %s but only %s was acked', self, self._my_seq, self._my_seq_acked)
            self._unack_throttling = True
        self._send_message(chunk)

    def _send_message(self, data):
        """
        Put data in sessions queue where they will be sent asap
        Makes new self._my_seq and checks self._tcp_close_to_http
        """
        self._my_seq += 1
        out_message = TcpMessage(self._session, self._tcp_key, self._env.out_symbol, self._my_seq, data,
            self._tcp_close_to_http, self._env.config.MAX_RETRANS)
        self._env.log2('%s created %s', self, out_message)
        self._unacked.append(out_message)
        self._session.enqueue_message(out_message)

    def receive_ack(self, received_ack):
        """
        Ack was received, so seq up to this has been correctly received
        """
        if received_ack > self._my_seq:
            self._env.log2('%s received ack for %s but has only sent %s',
                           self, TcpBase(self._session, self._tcp_key, self._env.out_symbol, received_ack), self._my_seq)
            received_ack = self._my_seq
        if received_ack <= self._my_seq_acked:
            self._env.log2('%s got superfluous ack of %s',
                           self, TcpBase(self._session, self._tcp_key, self._env.out_symbol, self._my_seq_acked))
            return
        self._env.log2('%s got ack(s)', self)
        while True:
            self._env.log3('%s has %s unacked and my_seq_acked %s', self, len(self._unacked), self._my_seq_acked)
            acked_message = self._unacked.popleft()
            assert acked_message.seq == self._my_seq_acked + 1
            self._env.log3('%s other acked my %s', self, acked_message)
            self._my_seq_acked += 1
            if self._my_seq_acked == received_ack:
                break
        self._env.log2('%s is now acked to %s', self, acked_message)
        if self._unacked:
            self._env.log3('%s checking %s for retransmit', self, self._my_seq_acked + 1)
            self.retransmit_next() # Immediately retransmit the new head ... perhaps
        else:
            assert self._my_seq_acked == self._my_seq
            self._check_done()
        if self._unack_throttling and self._my_seq < self._my_seq_acked + self._env.config.MAX_UNACK:
            self._env.log2('%s no longer throttling', self)
            self._unack_throttling = False

    def retransmit_tick(self):
        if self._unacked and self._unacked[0].give_up_retrans():
            self._env.log1('%s give up after %s retrans - closing NOW', self, self._env.config.MAX_RETRANS)
            # Emergency exit - a bit like _handle_tcp_error() and _check_done() but more brutal
            self.close()
            self._session.cancel_tcp_connection(self._tcp_key)
        elif timer() > self._tcp_idle_deadline: # because nothing received or throttled
            self._env.log1('%s has been idle for %s seconds - closing', self, self._env.config.MAX_IDLE_TCP)
            self.close()
            self._session.cancel_tcp_connection(self._tcp_key)
        else:
            # Retransmit if next already in oldest bucket
            self.retransmit_next()

    def retransmit_next(self):
        """
        Retransmit one ancient transmission (just to do something or because we just got another ancient acked)
        (Assumes lower retransmit rate, and whenever something from ancient is acked then the next one should be retransmitted)
        """
        if self._unacked:
            out_message = self._unacked[0]
            assert out_message.seq == self._my_seq_acked + 1
            if out_message.should_retrans():
                self._env.log1('%s enqueue retransmit %s', self, out_message)
                self._session.enqueue_message(out_message)
                out_message.count_retrans()
                out_message.stop_retrans_timer() # Shouldn't retrans until it has actually been sent
                self._stat_retrans_sent += 1
            else:
                self._env.log2('%s no retransmit of seq %s yet', self, self._my_seq_acked + 1)
        else:
            self._env.log3('%s nothing to retransmit after %s', self, self._my_seq_acked)

    def process_message(self, in_message):
        """
        headers and data to this TcpConnection was received over HTTP
        """
        self._tcp_idle_deadline = timer() + self._env.config.MAX_IDLE_TCP
        received_seq = in_message.seq
        self._env.log2('%s at %s received %s with %s bytes', self, self._their_seq, in_message, len(in_message.data))
        self._stat_messages += 1
        if self._tcp_error:
            if in_message.closing:
                self._env.log1('%s with errors received closing %s', self, in_message)
                self._http_close_to_tcp = True
                self._check_done()
            else:
                self._env.log1('%s with errors received and dropped nonclosing %s', self, in_message)
        elif received_seq == self._their_seq + 1:
            while True:
                # Invariant: in_message matches self._their_seq + 1
                self._their_seq += 1
                if in_message.data:
                    self._out_buffer.add(in_message.data)
                if in_message.closing:
                    assert not self._received_out_of_order # If regular close from HTTP, then this must be last package - nothing can be waiting
                    self._out_buffer.add(close=True)
                in_message = self._received_out_of_order.pop(self._their_seq + 1, None)
                if in_message is None: # next data chunk wasn't waiting
                    break
                self._env.log2('%s found %s in out of order buffer', self, in_message)
                self._stat_got_out_of_order += 1
                # FIXME: Make fastpath write now???
        elif received_seq > self._their_seq + 1:
            if received_seq <= self._their_seq + self._env.config.MAX_OUT_OF_ORDER:
                self._env.log2('%s received %s out of order - had seq %s - buffering', self, in_message, self._their_seq)
                self._received_out_of_order[received_seq] = in_message # This might replace existing, and we could track how often that happens...
            else:
                self._env.log2('%s received %s out of order but too far away from %s - dropping', self, in_message, self._their_seq)
                self._stat_too_out_of_order += 1
        else:
            assert received_seq < self._their_seq + 1
            self._env.log2('%s received and dropped outdated %s', self, in_message)
            self._stat_received_outdated += 1
        self._session.enqueue_ack(self._tcp_key, self._their_seq)

    # asyncore
    def writable(self):
        return self._out_buffer.pending()

    # asyncore
    def handle_write(self):
        self._stat_sends += 1
        pending = self._out_buffer.pending()
        if pending is StreamBuffer.CLOSE:
            self._env.log1('%s selected for write - will shutdown', self)
            self._http_close_to_tcp = True
            try:
                self.shutdown(socket.SHUT_WR) # closing in one direction
            except socket.error, (err, msg):
                self._env.log2('%s socket error closing TCP: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                return self._handle_tcp_error()
            self._out_buffer.eat(close=True)
            self._check_done()
        elif not pending:
            self._env.log2('%s selected for write but nothing pending', self)
        else:
            self._env.log3('%s selected for write with send buffer size %s', self, len(pending))
            try:
                byte_count = self.send(pending)
            except socket.error, (err, msg):
                self._env.log2('%s socket error sending on TCP handled by closing: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                return self._handle_tcp_error()
            self._out_buffer.eat(byte_count)
            self._env.log2('%s sent %s bytes of %s', self, byte_count, len(pending))
            self._stat_sent += byte_count

    def _check_done(self):
        """
        Check if TcpConnection totally done: HTTP close sent to TCP, TCP close sent to HTTP and fully acked
        """
        self._env.log3("%s check done: self._tcp_error %s, self._tcp_close_to_http %s, self._http_close_to_tcp %s, self._my_seq_acked %s, self._my_seq %s",
             self, self._tcp_error, self._tcp_close_to_http, self._http_close_to_tcp, self._my_seq_acked, self._my_seq)
        if self._tcp_close_to_http and self._http_close_to_tcp and self._my_seq_acked == self._my_seq:
            self._env.log1('%s closed', self)
            self.close()
            self._session.cancel_tcp_connection(self._tcp_key)

    def status(self):
        stati = [
            '%s Messages:: My seq: %s, Acked seq: %s, Resent: %s, '
            'Received: %s, Their seq: %s, Good ooo: %s, Too ooo: %s, Too old: %s' % ( self,
            self._my_seq, self._my_seq_acked, self._stat_retrans_sent,
            self._stat_messages, self._their_seq, self._stat_got_out_of_order, self._stat_too_out_of_order, self._stat_received_outdated),
            '%s TCP:: Recvs: %s, Received: %s, Sends: %s, Sent: %s' % (self,
            self._stat_recvs, self._stat_recved, self._stat_sends, self._stat_sent)]
        if self._my_seq_acked != self._my_seq:
            stati.append(
                '%s unacked: %s' % (self, ' '.join(repr(out_message) for out_message in self._unacked)))
        if self._received_out_of_order:
            stati.append(
                '%s waiting out of order after seq %s: %s' % (self, self._their_seq,
                ' '.join(repr(in_message) for in_message in self._received_out_of_order.values())))
        return '\n'.join(stati)

    def __repr__(self):
        return '%s.%s' % (self._session, self._tcp_key)

    __str__ = __repr__
