"""
Simple registry reader
"""

import sys


# Got this from shutil.py (WindowsError is not defined on other platforms that windows)
try:
    WindowsError
except NameError:
    WindowsError = None


def log(*x):
    # print ' '.join(str(y) for y in x)
    pass 

if sys.platform == "win32":

    import _winreg
    
    def read_value(key):
        """
        Return value at key, treating last part as name of value.
        Use trailing backslash to use default value. This is also the fallback.
        Returned strings are unicode.
        Returns None if anything goes wrong.
        Note: It is possible to have registry keys and values with the same name!
        """
        aReg = None
        aKey = None
        try:
            hkey_name, full_path = key.split('\\', 1)
            try:
                hkey = getattr(_winreg, hkey_name)
            except AttributeError, e:
                log('Invalid hkey_name', hkey_name, repr(e)) 
                return None
            try:
                aReg = _winreg.ConnectRegistry(None, hkey)
            except WindowsError, e:
                log('Invalid hkey handle', hkey, repr(e))
                return None
            if '\\' in full_path:
                # Assume explicitly named value
                path, value_name = full_path.rsplit('\\', 1)
                try:
                    aKey = _winreg.OpenKey(aReg, path)
                    v, t = _winreg.QueryValueEx(aKey, value_name)
                    return v
                except WindowsError, e:
                    log('no', hkey_name, '\\', path, 'value', repr(value_name), repr(e))
            else:
                log('no possible explicit value name in', hkey_name, '\\', full_path)                
            # Assume implicit default value
            try:
                aKey = _winreg.OpenKey(aReg, full_path)
                v, t = _winreg.QueryValueEx(aKey, '')
                return v
            except WindowsError, e:
                log('no', hkey_name, '\\', full_path, '(default)', repr(e))
                return None
        finally:
            if aKey: 
                _winreg.CloseKey(aKey)
            if aReg:
                _winreg.CloseKey(aReg)
        raise NotImplementedError
                
else: # not win32
    
    def read_value(key, index=0):
        """
        Implemented on windows only - returns None on other platforms
        """
        return None

def test():
    def testdump(path, expected):
        print path, 'expected', expected, ':' 
        print repr(read_value(path))
        print
    testdump(r'HKEY_CLASSES_ROOT\Applications\iexplore.exe\shell\open\command' '\\', 'ie ... %1') # explicit default
    testdump(r'HKEY_CLASSES_ROOT\Applications\iexplore.exe\shell\open\command', 'ie ... %1') # implicit default
    testdump(r'HKEY_CLASSES_ROOT\HTTP\shell\open\command', 'ff ... %1') # implicit default
    testdump(r'HKEY_CLASSES_ROOT\.323', 'h323file') # implicit default
    testdump('HKEY_CLASSES_ROOT\\.323\\', 'h323file') # explicit value name default
    testdump('HKEY_CLASSES_ROOT\\.386', 'vxdfile') # implicit default
    testdump('HKEY_CLASSES_ROOT\\.386\\', 'vxdfile') # explicit value name default
    testdump('HKEY_CLASSES_ROOT\\.386\\PerceivedType', 'system') # explicit value name
    testdump('HKEY_CLASSES_ROOT\\.386\\PersistentHandler', '{...}') # implicit default
    testdump(r'HKEY_CLASSES_ROOT\*', 'empty') # implicit default
    testdump(r'HKEY_CLASSES_ROOT\*\InfoTip', 'prop:...') # explicit value name

    testdump(r'xHKEY_CLASSES_ROOT\Applications\iexplore.exe\shell\open\command', 'invalid hkey name')
    testdump(r'KEY_ALL_ACCESS\Applications\iexplore.exe\shell\open\command', 'invalid hkey')
    testdump(r'HKEY_CLASSES_ROOT\nonexisting', 'not found and no explicit')
    testdump('HKEY_CLASSES_ROOT\\*\\nonexisting\\', 'nonexisting key not found')
    testdump('HKEY_CLASSES_ROOT\\*\\InfoTip\\', 'value not usable as key')
    testdump(r'HKEY_CLASSES_ROOT\Applications\iexplore.exe\shell\open\commandx', 'invalid value')

if __name__ == '__main__':
    test()
