"""
ToH Server
Giritech implementation of RFC-1149-ish implementation of tunneling TCP over HTTP
Copyright Giritech 2008-2010
"""

import sys
import asyncore
import socket
import errno
import time
from collections import deque

import mime
import tcp

import asyncserver
import httpserverengine

if sys.platform == 'win32':
    timer = time.clock
else:
    timer = time.time

global_package_drop_counter = 1 # every PACKAGE_DROP_INTERVAL'nd package is dropped - in order to test robustness


class HttpManagerServer(asyncserver.Service):

    def __init__(self, env, http_addr, session_manager, async_map):
        def tcp_accepted(peername, cb_will_listen, cb_to_client, cb_close):
            server = HttpConnectionServer(env, peername, cb_will_listen, cb_to_client, cb_close, session_manager)
            return server.tcp_from_client, server.got_http_cancel
        asyncserver.Service.__init__(self, env, async_map, http_addr, cb_tcp_accepted=tcp_accepted)

class HttpConnectionServer(httpserverengine.HttpBodyServerEngineCallbacks):

    def __init__(self, env, peername, cb_will_listen, cb_to_client, cb_close, session_manager):
        httpserverengine.HttpBodyServerEngineCallbacks.__init__(self, env)
        self._env = env
        self._peername = peername
        self._cb_will_listen = cb_will_listen
        self._cb_to_client = cb_to_client
        self._cb_close = cb_close
        self._session = None
        self._session_manager = session_manager
        self._server_engine = httpserverengine.HttpServerEngine(env, self)

    def close(self):
        self._server_engine = None # break loop
        self._cb_close()

    # Implement virtual method
    def got_http_cancel(self, msg=None):
        if self._session:
            self._session.cancel_http_connection(self)

    # Implement virtual method
    def got_http_request(self, headers, content):
        #print headers
        if self._env.config.PACKAGE_DROP_INTERVAL:
            global global_package_drop_counter
            global_package_drop_counter += 1
            if global_package_drop_counter % self._env.config.PACKAGE_DROP_INTERVAL == 0:
                self._env.log1('PACKAGE_DROP_INTERVAL %s dropping receiving %s', self, global_package_drop_counter)
                return
        self._session = self._session_manager.take_http_and_content(self, content, self._peername)

    # Called from owner to send request
    def send_http_content(self, content):
        if self._env.config.PACKAGE_DROP_INTERVAL:
            global global_package_drop_counter
            global_package_drop_counter += 1
            if global_package_drop_counter % self._env.config.PACKAGE_DROP_INTERVAL == 0:
                self._env.log1('PACKAGE_DROP_INTERVAL %s dropping sending %s', self, global_package_drop_counter)
                return
        if self._server_engine:
            self._server_engine.send_http_response(content=content, content_type='application/octet-stream')
            

    # implemented callbacks from http engine:

    def will_listen(self, ready):
        self._env.log3('%s will listen is %s', self, ready)
        self._cb_will_listen(ready)

    def to_client(self, buf):
        self._env.log3('%s added %r to send buffer', self, buf)
        self._cb_to_client(buf)

    # implemented asyncserver callback:

    def tcp_from_client(self, data_chunk):
        self._env.log3('%s tcp_from_client: %r', self, data_chunk)
        if self._server_engine:
            return self._server_engine.tcp_from_client(data_chunk)
        else:
            return ''


class SessionManager(object):
    """
    Manager creating and dispatching Sessions
    """

    def __init__(self, env, target_addr, async_map):
        self._env = env
        self._target_addr = target_addr
        self._async_map = async_map
        self.sessions = {}
        self._queued_for_work = set() # Sessions that should have their .do_queued_work called
        self._ping_deadline = 0
        self._retransmit_deadline = 0

    def take_http_and_content(self, http_connection, http_content, client_addr):
        """
        Take care of http_content and its carrier http_connection - eventually a reply will be sent
        """
        session = None
        while http_content:
            headers = mime.Headers()
            unused_data = headers.parse(http_content)
            session_key = headers['SESSION-KEY']
            if not session:
                if session_key not in self.sessions:
                    if len(self.sessions) < self._env.config.MAX_SESSIONS:
                        # Create new session
                        self.sessions[session_key] = Session(self._env, self._target_addr, client_addr, session_key, self._queued_for_work, async_map=self._async_map)
                    else:
                        self._env.log1('%s already has %s Sessions and dropped creating %s', self, len(self.sessions), session_key)
                        return None # FIXME: drops HttpServer without sending any reply ...
                session = self.sessions[session_key]
            content_length_string = headers.get('CONTENT-LENGTH')
            assert content_length_string, 'Expected Content-Length header'
            try:
                content_length = int(content_length_string)
            except ValueError, msg:
                assert False, 'Invalid Content-Length: %s' % msg
            session.process_from_http(headers, unused_data[:content_length])
            http_content = unused_data[content_length:]
        session.enqueue_sendable_http_connection(http_connection) # Session will call back sooner or later
        return session # HttpServer may call back here to discard callback if it dies

    def do_queued_work_sessions(self):
        """
        Do queued work for all sessions - called after select, when TcpConnections are ready for recv and HttpConnections are ready for reply
        """
        self._env.log3('Queued work: %s', ', '.join(repr(x) for x in self._queued_for_work))
        while self._queued_for_work:
            session = self._queued_for_work.pop()
            session.do_queued_work()

    def time_to_ping(self):
        if self.sessions:
            return max(self._ping_deadline - timer(), 0)
        return 0

    def check_ping(self):
        """
        Notify all sessions that they should remember to send mom a letter
        """
        if timer() > self._ping_deadline:
            self._env.log2('')
            self._env.log2('Periodical ping')
            self._ping_deadline = timer() + self._env.config.PING_INTERVAL
            for session in self.sessions.values():
                session.ping_tick()
            self._env.log2('')

    def time_to_retransmit(self):
        if self.sessions:
            return max(self._retransmit_deadline - timer(), 0)
        return 0

    def check_retransmit(self):
        """
        Notify all sessions that it might be time to consider retransmission
        """
        if timer() > self._retransmit_deadline:
            self._env.log2('')
            self._env.log2('Periodical retransmit')
            self._retransmit_deadline = timer() + self._env.config.RETRANS_TIMEOUT
            for session_key, session in self.sessions.items():
                if session.die_if_ready():
                    del self.sessions[session_key] # An idle session has nothing to clean up
                else:
                    session.retransmit_tick()
            self._env.log2('')

    def status_sessions(self):
        return '\n'.join(session.status() for session in self.sessions.values())

    def lookup_gon_client_connect_address(self, lookup_connect_info):
        """
        Lookup the gon_client_connect_address(host,port) which is the adress from where the client connects.
        This is need by the gon_gateway_server because ToH always connect to local loopback and thus the client connect host is always the same.

        The arguments(host,port) is the ToH side of the connection to the gateway server.
        """
        for _session_key, session in self.sessions.items():
            result = session.lookup_gon_client_connect_address(lookup_connect_info)
            if result is not None:
                return result
        return None


class Session(object):
    """
    Each connected client is/has a session identified by the secret session_key.
    Session is also TcpConnectionManager
    """

    _ping_counter = 0 # unique serial number for pings - makes it easier to trace in logs

    def __init__(self, env, target_addr, client_addr, session_key, queued_for_work, async_map):
        ""
        self._env = env
        self._target_addr = target_addr
        self._client_addr = client_addr
        self._session_key = session_key
        self._queued_for_work = queued_for_work
        self._async_map = async_map

        (target_host, target_port) = target_addr
        (client_host, client_port) = client_addr
        self._env.log1("%s created targeting %s:%s initiated from %s:%s", self, target_host, target_port, client_host, client_port)
        self._tcp_connections = {} # tcp_key to TcpConnection
        self._sendable_http_connections = deque() # HttpConnections ready to send - FIFO
        self._messages = deque() # messages that should be sent asap
        self._pending_acks = {} # tcp_keys with new acks since last time
        self._pending_acks_deadline = None # when this time is reached then pending_acks should be sent ASAP by "all" means
        self._silence_deadline = timer() + self._env.config.MAX_SESSION_SILENT # Counting how long session has been without any input from client (will always start with one TcpConnection)

    def process_from_http(self, headers, data):
        """
        Process a message received over HTTP and dispatched by SessionManager
        """
        self._env.log3('%s got %s', self, headers)
        acks_string = headers.get('ACKS')
        if acks_string:
            for ack_string in acks_string.split(','):
                received_ack_tcp_key, received_ack_string = ack_string.split(':')
                received_ack = int(received_ack_string)
                ack_tcp_connection = self._tcp_connections.get(received_ack_tcp_key)
                if ack_tcp_connection:
                    ack_tcp_connection.receive_ack(received_ack)
                else:
                    self._env.log2('%s got ack for unknown connection %s - dropping',
                                   self, tcp.TcpBase(self, received_ack_tcp_key, self._env.out_symbol, received_ack))
        in_message = tcp.TcpMessage(self, headers.get('TCP-KEY'), self._env.in_symbol, int(headers['SEQ']), data, headers['STATUS'].upper() == 'CLOSE')
        if in_message.tcp_key:
            tcp_connection = self._tcp_connections.get(in_message.tcp_key)
            if not tcp_connection:
                if in_message.seq == 1: # initializing new connection
                    if len(self._tcp_connections) > self._env.config.MAX_TCP_PER_SESSION:
                        self._env.log1('%s already has %s TcpConnections and dropped creating %s', self, len(self._tcp_connections), in_message.tcp_key)
                        return
                    if len(self._async_map) > self._env.config.MAX_ASYNC:
                        self._env.log1('%s MAX_ASYNC reached - not creating %s', self, in_message.tcp_key)
                        return
                    # FIXME: It could be a retrans for an old connection ... We could avoid that by using timewait ...
                    try:

                        (target_host, target_port) = self._target_addr
                        self._env.log1('%s creating TCP connection to %s:%s', self, target_host, target_port)
                        tcp_connection = tcp.TcpConnection(self, self._env, in_message.tcp_key, tcp_socket=None, target_addr=self._target_addr, async_map=self._async_map)
                    except socket.error, (err, msg):
                        self._env.log1('Error creating TCP connection to %s:%s: %s', self._target_addr[0], self._target_addr[1], msg)
                        self._env.log2('%s socket error caused request %s to NOT be processed: %s/%s: %r', self, in_message, err, errno.errorcode.get(err), msg)
                        return
                    self._tcp_connections[in_message.tcp_key] = tcp_connection
                else:
                    if in_message.closing:
                        self._env.log2('%s got unexpected closing request %s - acking it', self, in_message)
                        self.enqueue_ack(in_message.tcp_key, in_message.seq)
                    else:
                        self._env.log2('%s got unexpected request %s - dropping it', self, in_message)
                        # It will either be retransmitted after seq 1 has been received, or they will stop retransmitting eventually.
                    return
            self._env.log3('%s dispatching to %s', self, tcp_connection)
            tcp_connection.process_message(in_message)
        else:
            self._env.log2('%s got %s', self, in_message)
            assert not in_message.data
        self._silence_deadline = timer() + self._env.config.MAX_SESSION_SILENT

    def do_queued_work(self):
        """
        Make things happen - if an HttpServer and either message or receivable TcpConnection available
        """
        while self._sendable_http_connections:
            if self._messages:
                self._env.log2('%s sending from %s', self, self._messages)
                content_size = 0
                chunks = []
                while self._messages:
                    out_message = self._messages.popleft()
                    # don't check size of first message
                    if chunks and content_size + len(out_message.data) >= self._env.config.MAX_SEND_CONTENT_LENGTH:
                        self._messages.appendleft(out_message) # undo popleft - and we already have one message
                        break
                    s = self._encode(out_message, encode_meta=not chunks) # only meta in first message
                    content_size += len(s)
                    chunks.append(s)
                    out_message.start_retrans_timer(self._env.config.RETRANS_TIMEOUT)
                assert chunks # at least one
                http_content = ''.join(chunks)
            elif (self._pending_acks_deadline and timer() > self._pending_acks_deadline or
                len(self._sendable_http_connections) > self._env.config.MAX_WAITING_HTTP_CONNECTIONS):
                out_message = self._make_ping()
                self._env.log2('%s sending %s', self, out_message)
                http_content = self._encode(out_message)
            else:
                break
            http_connection = self._sendable_http_connections.popleft()
            http_connection.send_http_content(http_content)


    def enqueue_ack(self, tcp_key, their_seq):
        """
        Something was received and TcpConnections wants to say thank-you
        """
        self._env.log2('%s enqueuing ack of %s%s%s', self, tcp_key, self._env.in_symbol, their_seq)
        if not self._pending_acks_deadline:
            self._pending_acks_deadline = timer() + self._env.config.ACK_TIMEOUT
        self._pending_acks[tcp_key] = their_seq

    def enqueue_message(self, out_message):
        self._env.log2('%s enqueuing message %s', self, out_message)
        self._messages.append(out_message)
        self._queued_for_work.add(self) # check later if an HttpServer can send the message

    def enqueue_sendable_http_connection(self, http_connection):
        """
        A HttpServer has got a request and is now waiting for a reply to send
        """
        assert http_connection
        self._env.log2('%s enqueued %s for reply', self, http_connection)
        self._sendable_http_connections.append(http_connection)
        self._queued_for_work.add(self) # check later if Message is waiting for HttpServer

    def cancel_tcp_connection(self, tcp_key):
        """
        Let session know that TcpConnection has been closed
        """
        tcp_connection = self._tcp_connections.pop(tcp_key, None)
        if tcp_connection:
            self._env.log1("%s unregistered %s", self, tcp_connection)
            self._env.log2("%s", tcp_connection.status())
        else:
            self._env.log1('%s:%s already unregistered', self, tcp_key)

    def cancel_http_connection(self, http_connection):
        """
        A HttpServer waiting for reply isn't ready anyway - probably dead
        """
        if http_connection in self._sendable_http_connections:
            self._env.log2('%s cancel %s', self, http_connection)
            self._sendable_http_connections.remove(http_connection) # this is O(n) but for small n and happens seldom
        else:
            self._env.log2('%s cancel unsendable %s', self, http_connection)

    def die_if_ready(self):
        """
        Allow Session to die decently
        """
        if timer() > self._silence_deadline:
            self._env.log1('%s has been silent for too long - closing NOW', self)
            for http_connection in self._sendable_http_connections:
                self._env.log2('%s closing %s', self, http_connection)
                http_connection.close()
            for tcp_connection in self._tcp_connections.values():
                self._env.log2('%s closing %s', self, tcp_connection)
                tcp_connection.close()
            self._tcp_connections.clear()
            return True
        return False

    def ping_tick(self):
        """
        Session pings back to client, thus ensuring acks will come back
        """
        if not self._sendable_http_connections:
            self._env.log2('%s cannot reply with ping to client', self)
            return
        http_connection = self._sendable_http_connections.popleft()
        self._env.log2('%s pinging client - sending empty reply using %s', self, http_connection)
        http_connection.send_http_content(self._encode(self._make_ping()))

    def retransmit_tick(self):
        """
        It is time to consider retransmitting whatever the sessions TcpConnections wants ...
        """
        self._env.log2('%s retransmit tick', self)
        for tcp_connection in self._tcp_connections.values():
            tcp_connection.retransmit_tick()

    def _make_ping(self):
        Session._ping_counter += 1
        return tcp.TcpMessage(self, '', self._env.out_symbol, Session._ping_counter, '', False)

    def _encode(self, out_message, encode_meta=True):
        """
        Make encode out_message by wrapping it with headers.
        Global meta data might be encoded in headers too - connection requests should only come once in each http response.
        """
        if encode_meta:
            if len(self._sendable_http_connections) < self._env.config.MIN_WAITING_HTTP_CONNECTIONS:
                connection_request = 2 # one extra FIXME: how much ramp-up, considering round-trip and multiple requests...
            elif len(self._sendable_http_connections) >= self._env.config.MAX_WAITING_HTTP_CONNECTIONS:
                connection_request = 0 # no replacement for this HttpServer
            else:
                connection_request = 1 # FIXME: should we request one for replacement when between min and max?
            acks = self._pending_acks.items()
            self._pending_acks.clear() # We have acked. If ack doesn't come through they will resend and we will re-ack.
            self._pending_acks_deadline = None
        else:
            connection_request = 0
            acks = ()
        self._env.log2('%s encoding %s with %s bytes requesting %s connections', self, out_message, len(out_message.data), connection_request)
        http_content = ''.join(('Pong\r\n',
                                'Acks:', ','.join('%s:%s' % (k, s) for (k, s) in acks), '\r\n',
                                'Connections:', str(connection_request), '\r\n',
                                'Tcp-Key:', out_message.tcp_key, '\r\n',
                                'Seq:', str(out_message.seq), '\r\n',
                                'Status:', 'Close' if out_message.closing else 'Fine', '\r\n',
                                'Content-Length:', str(len(out_message.data)), '\r\n',
                                '\r\n',
                                out_message.data))
        return http_content

    def status(self):
        stati = [tcp_connection.status() for tcp_connection in self._tcp_connections.values()]
        stati.append(
            '%s HTTP connections waiting for TCP: %s, Pending acks: %s, Messages: %s' % (
            self,
            len(self._sendable_http_connections),
            self._pending_acks, ' '.join(repr(out_message) for out_message in self._messages)))
        return '\n'.join(stati)

    def lookup_gon_client_connect_address(self, lookup_connect_info):
        try:
            self._env.log1('Reverse connect lookup %s', lookup_connect_info)
            for tcp_connection in self._tcp_connections.values():
                if tcp_connection.getsockname() == lookup_connect_info:
                    self._env.log1('Reverse connect lookup found %s', self._client_addr)
                    return self._client_addr
        except:
            pass
        self._env.log1('Reverse connect lookup not found')
        return None

    def __repr__(self):
        return self._session_key

    __str__ = __repr__


class main(object):

    def __init__(self):
        self.stopflag = False
        self.session_manager = None

    def run(self, env, http_addr, target_addr, loop_rest=0.02):
        async_map = {}

        self.session_manager = SessionManager(env, target_addr, async_map=async_map)
        try:
            http_manager = HttpManagerServer(env, http_addr, self.session_manager, async_map=async_map)
        except socket.error, (err, msg):
            env.log1('Error listening on %s:%s: %s', http_addr[0], http_addr[1], msg)
            env.log2('got socket error: %s/%s: %r', err, errno.errorcode.get(err), msg)
            raise SystemExit(1)

        while not self.stopflag:
            env.log2('')
            time_to_next = min(self.session_manager.time_to_ping(), self.session_manager.time_to_retransmit())
            asyncore.loop(time_to_next or env.config.IDLE_TIMEOUT or 1, count=1, map=async_map)
            self.session_manager.check_ping()
            self.session_manager.check_retransmit()
            self.session_manager.do_queued_work_sessions()
            http_manager.prune_idle()
            env.log2('')
            env.log2('Status:\n%s\n%s', self.session_manager.status_sessions(), http_manager.status())
            if loop_rest:
                time.sleep(loop_rest)
        env.log1("Done - stopped")

    def lookup_gon_client_connect_address(self, lookup_connect_info):
        """
        Lookup the gon_client_connect_address( (host, port) ) which is the adress from where the client connects.
        This is need by the gon_gateway_server because ToH always connect to local loopback and thus the client connect host is always the same.

        The arguments(host,port) is the ToH side of the connection to the gateway server.
        """
        return self.session_manager.lookup_gon_client_connect_address(lookup_connect_info)

    def stop(self):
        """
        Call from other thread to ask main_loop to stop - not waiting for it
        """
        self.stopflag = True
