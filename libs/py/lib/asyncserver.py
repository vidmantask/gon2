import asyncore
import socket
import errno
import sys
import time

if sys.platform == 'win32':
    timer = time.clock
else:
    timer = time.time


class Service(asyncore.dispatcher):
    """
    Listens for new connections on addr
    Creates Server instances utilizing handler_class
    """

    def __init__(self, env, async_map, addr, cb_tcp_accepted):
        asyncore.dispatcher.__init__(self, map=async_map)
        self._env = env
        self._async_map = async_map
        self._cb_tcp_accepted = cb_tcp_accepted
        self._stat_count = 0
        self._connections = set()
        self._last_prune = timer()

        host, port = addr
        self._env.log1('%r created listening on port %s:%s', self, host, port)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(addr)
        self.listen(self._env.config.LISTEN_BACKLOG)

    # asyncore
    def handle_expt(self):
        self._env.log2('%s exception - ignoring', self) # FIXME: What happened? What to do?

    def handle_accept(self):
        """
        Something happened on listen_socket - that must be a new connection
        """

        while True: # One select might have several connections waiting
            try:
                accepted = self.accept()
            except socket.error, (err, msg):
                self._env.log2('%s socket error accepting %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                return
            if not accepted:
                return
            (connected_socket, client_addr) = accepted
            if len(self._async_map) > self._env.config.MAX_ASYNC:
                # TODO: It could make sense to give some preference to new connections instead of to old idle connections ...
                self._env.log1('%s MAX_ASYNC reached - rejecting connection from %s:%s', self, client_addr[0], client_addr[1])
                connected_socket.close()
                return
            # FIXME: Limit number of Connections?
            conn = Server(self._env, self._async_map, connected_socket, self._cb_tcp_accepted)
            self._connections.add(conn)
            self._stat_count += 1

    def status(self):
        return 'Server total count: %s/%s' % (len(self._connections), self._stat_count)

    def prune_idle(self):
        now = timer()
        if now > self._last_prune + self._env.config.MAX_IDLE_HTTP / 2:
            for conn in list(self._connections):
                if not conn.connected:
                    self._connections.discard(conn)
                elif now > conn._last_activity + self._env.config.MAX_IDLE_HTTP:
                    self._env.log1('%s closing idle %s', self, conn)
                    conn.close()
                    self._connections.discard(conn)
            self._last_prune = now

    def __repr__(self):
        return '%s%X' % (self.__class__.__name__, id(self))

    __str__ = __repr__


class Server(asyncore.dispatcher):

    def __init__(self, env, async_map, connected_socket, cb_tcp_accepted):
        asyncore.dispatcher.__init__(self, connected_socket, map=async_map)
        self._env = env
        self._recv_buffer = '' # unprocessed input
        self._send_buffer = '' # unsent output
        self._send_shutdown = False # Send shutdown after _send_buffer
        self._readable = True
        self._last_activity = timer()
        (client_host, client_port) = self.peername = self.getpeername()
        (host, port) = self.getsockname()
        self._env.log2('%s created from %s:%s to %s:%s', self, client_host, client_port, host, port)
        self._cb_tcp_from_client, self._cb_tcp_error = cb_tcp_accepted(
                self.peername, self.will_listen, self.to_client, self.close)

    def close(self):
        asyncore.dispatcher.close(self)

    # implemented callbacks from _handler:

    def will_listen(self, ready):
        self._readable = ready

    def to_client(self, buf):
        self._env.log3('%s added %r to send buffer', self, buf)
        if buf:
            self._send_buffer += buf
        else:
            self._send_shutdown = True

    # implemented asyncore handlers:

    def handle_expt(self):
        self._env.log2('%s exception - closing and will disappear', self)
        self.close()
        self._cb_tcp_error('asyncore socket exception')

    def handle_connect(self):
        self._env.log2('%s handle_connect', self) # doesn't happen

    def readable(self):
        return self._readable

    def handle_close(self): # also called on half-close
        self._env.log3('%s handle_close', self)
        if not self.connected:
            self._env.log3('%s connection failed', self)
            self._cb_tcp_error("closed before connected")
            self.close()

    def handle_read(self):
        """
        Something happened on socket
        """
        try:
            data_chunk = self.recv(self._env.config.TCP_RECV_BUFFER)
        except socket.error, (err, msg):
            self._env.log2('%s socket error handled as close: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
            data_chunk = ''

        if not data_chunk:
            self._readable = False
            if self._recv_buffer:
                self._env.log2("%s EOF from client with pending data", self)
                self._cb_tcp_error("premature EOF")
                self.close()
                return

        self._recv_buffer += data_chunk
        self._env.log2("%s got %s now %s", self, len(data_chunk), len(self._recv_buffer))

        self._recv_buffer = self._cb_tcp_from_client(self._recv_buffer)
        if self._recv_buffer is None:
            self._env.log2("%s handler said close", self)
            self._cb_tcp_error(None)
            self.close()
            return
        if (self._send_buffer or self._send_shutdown) and not self._recv_buffer:
            self.handle_write() # use fastpath if connected

    def writable(self):
        return bool(self._send_buffer) or self._send_shutdown

    def handle_write(self):
        if not self.connected:
            self._env.log2('%s selected for write but will not because not connected', self) # probably fastpath ...
            return
        if self._send_buffer:
            self._env.log3('%s selected for write with send buffer size %s', self, len(self._send_buffer))
            self._last_activity = timer()
            try:
                byte_count = self.send(self._send_buffer)
            except socket.error, (err, msg):
                self._env.log2('%s closing because of socket error sending: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                self._send_buffer = ''
                self._cb_tcp_error('error sending')
                self.close()
                return
            self._send_buffer = self._send_buffer[byte_count:]
            self._env.log2('%s sent %s bytes - %s more to go', self, byte_count, len(self._send_buffer))
        elif self._send_shutdown:
            self._env.log3('%s selected for write with shutdown', self)
            self._send_shutdown = False
            try:
                self.shutdown(socket.SHUT_WR)
            except socket.error, (err, msg):
                if err != errno.ENOTCONN:
                    self._env.log2('%s closing because of socket error shutting down: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                    self._cb_tcp_error('error shutting down')
                    self.close()
                return
        else:
            self._env.log2('%s selected for write but will not because nothing to send', self)

    # internal:

    def __repr__(self):
        return '%s%X' % (self.__class__.__name__, id(self))

    __str__ = __repr__
