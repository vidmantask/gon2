"""
ToH Client
Giritech implementation of RFC-1149-ish implementation of tunneling TCP over HTTP
Copyright Giritech 2008-2010
"""
# FIXME: HttpConnection9257D8C socket error handled as close: 11/EAGAIN: 'Resource temporarily unavailable'

import sys
import socket
import errno
import time
import random
import asyncore
from collections import deque

import mime
import tcp

import asyncclient
import httpclientengine
import httpauth

if sys.platform == 'win32':
    timer = time.clock
else:
    timer = time.time


class HttpConnectionClient(httpclientengine.HttpBodyClientEngineCallbacks):

    def __init__(self, env, proxy_addr, http_addr, session_key, session_manager, async_map, first_content):
        httpclientengine.HttpBodyClientEngineCallbacks.__init__(self, env)
        self._env = env
        self._proxy = bool(proxy_addr)
        self.__host = '%s:%s' % http_addr
        self._session_key = session_key
        self._session_manager = session_manager
        self._session = None # The session we expect to call .send_http_content() (and which we should notify if dying)
        self._pending_req = None
        self._first_req = True
        self._auth = httpauth.Auth(self._env, None, None, sspi=True)
        self._client_engine = httpclientengine.HttpClientEngine(env, self)
        def continuation(local, remote, send, close):
            self._env.log2('%s connected %s -> %s', self, local, remote)
            self.to_server = send
            self.close = close
            self.send_http_content(first_content)
            return (self._client_engine.from_server, self.got_http_cancel)
        self._connection = asyncclient.AsyncClient(env, async_map, proxy_addr or http_addr, continuation, self.got_http_cancel)

    # Implement virtual method
    def got_http_cancel(self, msg=None):
        self._env.log2('%s got cancel: %s', self, msg)
        if self._session:
            self._session.cancel_http_connection(self)

    # Called from owner to send request
    def send_http_content(self, content):
        # RFC 2616 4.3 says body should be preserved on all methods unless forbidden - but POST is known to work
        # RFC 2616 5.1, 9.1: our protocol is safe and idempotent, 9.3 GET thus nice, 9.5 POST matches intentions better
        path = '/'
        if self._proxy:
            path = 'http://' + self.__host + path
        self._pending_req = httpclientengine.HttpRequest(method='POST', host=self.__host, path=path, content=content, user_agent=self._env.config.USER_AGENT)
        if self._first_req:
            self._auth.initialize(self._pending_req)
        self._first_req = False
        self._client_engine.send_http_request(self._pending_req)

    # Implement virtual method
    def got_http_response(self, status, headers, content, alive):
        #print headers
        assert self._pending_req
        if self._auth.react(status, headers, self._pending_req):
            self._env.log1('%s retrying with next phase of auth', self)
            self._client_engine.send_http_request(self._pending_req)
            return
        if status == '200':
            self._pending_req = None
            if alive:
                reusable_http_connection = self
            else:
                reusable_http_connection = None
                self.close()
                self.got_http_cancel()
            self._session = self._session_manager.take_http_and_content(reusable_http_connection, content, self._session_key)
        else:
            self._env.log1('%s got HTTP status %s: %s', self, status, headers.start_line) # Probably from proxy: 502 Bad Gateway or 503 Service Unavailable or 504 Gateway Time-out

def make_session_manager(env, forward_addr, proxy_addr, http_addr, async_map):
    try:
        return SessionManager(env, forward_addr, proxy_addr, http_addr, async_map=async_map)
    except socket.error, (err, msg):
        env.log1('Error listening on %s:%s: %s', forward_addr[0], forward_addr[1], msg)
        env.log2('got socket error: %s/%s: %r', err, errno.errorcode.get(err), msg)
        return None

class SessionManager(object):
    """
    Manager creating and dispatching Sessions
    """

    def __init__(self, env, forward_addr, proxy_addr, http_addr, async_map):
        self._env = env
        self._proxy_addr = proxy_addr
        self._http_addr = http_addr
        self._async_map = async_map
        self.sessions = {}
        self._queued_for_work = set() # Sessions that should have their .do_queued_work called
        self._ping_deadline = 0
        self._retransmit_deadline = 0
        self._stat_count = 0

        session_key = "Session%X" % random.randrange(10000000) # FIXME: should be unique and crypto-random
        session = Session(env, forward_addr, session_key, self, self._queued_for_work, async_map=self._async_map)
        self.sockname = session.getsockname()
        self.sessions[session_key] = session # we only have on session, but we try to keep client and server symmetric

    def http_connection_factory(self, session_key, first_content):
        # FIXME: if not too many HttpConnections in play RFC 2616 8.1.4: Clients that use persistent connections SHOULD limit the number of simultaneous connections that they maintain to a given server.
        try:
            self._stat_count += 1
            return HttpConnectionClient(self._env, self._proxy_addr, self._http_addr, session_key, self,
                    async_map=self._async_map, first_content=first_content)
        except socket.error, (_err, msg):
            (http_host, http_port) = self._proxy_addr or self._http_addr
            self._env.log1('Error creating HTTP connection to %s:%s: %s', http_host, http_port, msg)
            return

    def take_http_and_content(self, reusable_http_connection, http_content, session_key):
        """
        Take care of http_content and its carrier reusable_http_connection - eventually another request will be sent
        """
        # TODO: Proxy / ToH-server liveness could be checked here ...
        if session_key not in self.sessions:
            self._env.log1('%s received message for unknown session %s - dropping', self, session_key)
            return None
        session = self.sessions[session_key]
        while http_content:
            headers = mime.Headers()
            unused_data = headers.parse(http_content)
            content_length_string = headers.get('CONTENT-LENGTH')
            assert content_length_string, 'Expected Content-Length header'
            try:
                content_length = int(content_length_string)
            except ValueError, msg:
                assert False, 'Invalid Content-Length: %s' % msg
            session.process_from_http(headers, unused_data[:content_length])
            http_content = unused_data[content_length:]
        if reusable_http_connection:
            session.enqueue_sendable_http_connection(reusable_http_connection) # Session will call back sooner or later
        return session # HttpClient may call back here to discard callback if it dies

    def do_queued_work_sessions(self):
        """
        Do queued work for all sessions - called after select, when TcpConnections are ready for recv and HttpConnections are ready for reply
        """
        self._env.log3('Queued work: %s', ', '.join(repr(x) for x in self._queued_for_work))
        while self._queued_for_work:
            session = self._queued_for_work.pop()
            session.do_queued_work()

    def time_to_ping(self):
        return max(self._ping_deadline - timer(), 0)

    def check_ping(self):
        """
        Notify all sessions that they should remember to send mom a letter
        """
        if timer() > self._ping_deadline:
            self._env.log2('')
            self._env.log2('Periodical ping')
            self._ping_deadline = timer() + self._env.config.PING_INTERVAL
            for session in self.sessions.values():
                session.ping_tick()
            self._env.log2('')

    def time_to_retransmit(self):
        return max(self._retransmit_deadline - timer(), 0)

    def check_retransmit(self):
        """
        Notify all sessions that it might be time to consider retransmission
        """
        if timer() > self._retransmit_deadline:
            self._env.log2('')
            self._env.log2('Periodical retransmit')
            self._retransmit_deadline = timer() + self._env.config.RETRANS_TIMEOUT
            for session_key, session in self.sessions.items():
                if session.die_if_ready():
                    del self.sessions[session_key] # An idle session has nothing to clean up
                else:
                    session.retransmit_tick()
            self._env.log2('')

    def status_sessions(self):
        return '\n'.join(session.status() for session in self.sessions.values()) + '\nHttpConnection total count: %s' % (self._stat_count)


class Session(asyncore.dispatcher):
    """
    Listens for new TCP connections on forward_addr
    Creates TcpConnection"s utilizing register_tcp_connection_callback
    Encapsulates HTTP request/replies in plain reliable order-maintainging async interface
    Should try to utilize multiple persistent connections
    """

    _ping_counter = 0 # unique serial number for pings - makes it easier to trace in logs

    def __init__(self, env, forward_addr, session_key, session_manager, queued_for_work, async_map):
        ""
        asyncore.dispatcher.__init__(self, map=async_map)
        self._env = env
        self._session_key = session_key
        self._session_manager = session_manager
        self._queued_for_work = queued_for_work
        self._async_map = async_map

        (forward_host, forward_port) = forward_addr
        self._env.log1('%r created listening on port %s:%s', self, forward_host, forward_port)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind(forward_addr)
        self.listen(env.config.LISTEN_BACKLOG)

        self._tcp_connections = {} # tcp_key to TcpConnection
        self._sendable_http_connections = deque() # HttpConnections ready to send - FIFO
        self._messages = deque() # messages that should be sent asap
        self._pending_acks = {} # tcp_keys with new acks since last time
        self._pending_acks_deadline = None # when this time is reached then pending_acks should be sent ASAP by "all" means
        self._idle_deadline = timer() + self._env.config.MAX_INITIAL_IDLE_SESSION # First TcpConnection should come before this moment
        self._connection_request = self._env.config.INITIAL_HTTP_CONNECTIONS
        self._tcp_connection_counter = 0

    # asyncore
    def handle_expt(self):
        self._env.log2('%s exception - ignoring', self) # FIXME: What happened? What to do?

    # asyncore
    def readable(self):
        return len(self._tcp_connections) <= self._env.config.MAX_TCP_PER_SESSION

    # asyncore
    def handle_accept(self):
        """
        Something happened on listen_socket - that must be a new TCP connection
        """
        i = 1
        while True: # One select might have several connections waiting
            try:
                accepted = self.accept()
            except socket.error, (err, msg):
                self._env.log2('%s socket error accepting %s: %s/%s: %r', self, i, err, errno.errorcode.get(err), msg)
                return
            if not accepted:
                break
            (tcp_socket, _peer) = accepted
            self._tcp_connection_counter += 1
            tcp_key = "TCP%s" % (self._tcp_connection_counter) # Together with session_key a globally unique identifier for this TcpConnection
            tcp_connection = tcp.TcpConnection(self, self._env, tcp_key, tcp_socket=tcp_socket, target_addr=None, async_map=self._async_map)
            (peer_host, peer_port) = tcp_socket.getpeername()
            (source_host, source_port) = tcp_socket.getsockname()
            self._env.log1('%s created %s from %s:%s to %s:%s', self, tcp_connection, peer_host, peer_port, source_host, source_port)
            self._tcp_connections[tcp_key] = tcp_connection
            self._idle_deadline = None
            i += 1

    def process_from_http(self, headers, data):
        """
        Process a message received over HTTP and dispatched by SessionManager
        """
        self._env.log3('%s got %s', self, headers)
        acks_string = headers.get('ACKS')
        if acks_string:
            for ack_string in acks_string.split(','):
                received_ack_tcp_key, received_ack_string = ack_string.split(':')
                received_ack = int(received_ack_string)
                ack_tcp_connection = self._tcp_connections.get(received_ack_tcp_key)
                if ack_tcp_connection:
                    ack_tcp_connection.receive_ack(received_ack)
                else:
                    self._env.log2('%s got ack for unknown connection %s - dropping',
                                   self, tcp.TcpBase(self, received_ack_tcp_key, self._env.out_symbol, received_ack))
        in_message = tcp.TcpMessage(self, headers.get('TCP-KEY'), self._env.in_symbol, int(headers['SEQ']), data, headers['STATUS'].upper() == 'CLOSE')
        if in_message.tcp_key:
            tcp_connection = self._tcp_connections.get(in_message.tcp_key)
            if not tcp_connection:
                self._env.log2('%s got unexpected %s - dropping it', self, in_message)
                # We COULD send nack while in timewait... But how to tell client if it should stop sending or send later when we understand?
                return
            self._env.log3('%s dispatching to %s', self, tcp_connection)
            tcp_connection.process_message(in_message)
        else:
            self._env.log2('%s got %s', self, in_message)
            assert not in_message.data

        connection_request = int(headers.get('CONNECTIONS', ''))
        if connection_request:
            self._env.log2('%s server requested %s connections', self, connection_request)
            self._connection_request += connection_request

    def do_queued_work(self):
        """
        Make things happen - if an HttpClient and either message or receivable TcpConnection available
        """
        while True:
            if self._messages:
                self._env.log2('%s sending from %s', self, self._messages)
                content_size = 0
                chunks = []
                while self._messages:
                    out_message = self._messages.popleft()
                    # don't check size of first message
                    if chunks and content_size + len(out_message.data) >= self._env.config.MAX_SEND_CONTENT_LENGTH:
                        self._messages.appendleft(out_message) # undo popleft - and we already have one message
                        break
                    s = self._encode(out_message) # FIXME: encoded size is bigger than data
                    content_size += len(s)
                    chunks.append(s)
                    out_message.start_retrans_timer(self._env.config.RETRANS_TIMEOUT)
                assert chunks # at least one
                http_content = ''.join(chunks)
            elif (self._pending_acks_deadline and timer() > self._pending_acks_deadline or
                  self._connection_request):
                out_message = self._make_ping()
                self._env.log2('%s sending %s', self, out_message)
                http_content = self._encode(out_message)
                if self._connection_request:
                    self._connection_request -= 1 # FIXME:
            else:
                break
            if self._sendable_http_connections:
                http_connection = self._sendable_http_connections.popleft()
                self._env.log2('%s reusing %s', self, http_connection)
                # assert http_connection.connected # usually - but we can't guarantee it
                http_connection.send_http_content(http_content)
            else:
                http_connection = self._session_manager.http_connection_factory(self._session_key, http_content)
                if http_connection:
                    self._env.log2('%s created new %s', self, http_connection)
                else:
                    self._env.log1('%s error creating HttpClient caused requests to NOT be sent', self)
                return
        while len(self._sendable_http_connections) > self._env.config.MAX_WAITING_HTTP_CONNECTIONS:
            http_connection = self._sendable_http_connections.popleft()
            self._env.log2('%s has %s waiting HttpConnections - that''s too many, closing %s', self, len(self._sendable_http_connections) + 1, http_connection)
            http_connection.close()

    def enqueue_ack(self, tcp_key, their_seq):
        """
        Something was received and TcpConnections wants to say thank-you
        """
        self._env.log2('%s enqueuing ack of %s%s%s', self, tcp_key, self._env.in_symbol, their_seq)
        if not self._pending_acks_deadline:
            self._pending_acks_deadline = timer() + self._env.config.ACK_TIMEOUT
        self._pending_acks[tcp_key] = their_seq

    def enqueue_message(self, out_message):
        self._env.log2('%s enqueuing message %s', self, out_message)
        self._messages.append(out_message)
        self._queued_for_work.add(self) # check later if an HttpClient can send the message

    def enqueue_sendable_http_connection(self, reusable_http_connection):
        """
        A HttpClient has got a request and is now waiting for a reply to send
        """
        assert reusable_http_connection
        self._env.log2('%s enqueued %s for reuse', self, reusable_http_connection)
        self._sendable_http_connections.append(reusable_http_connection)
        self._queued_for_work.add(self) # check later if Message is waiting for HttpClient

    def cancel_tcp_connection(self, tcp_key):
        """
        Let session know that TcpConnection has been closed
        """
        self._env.log2("%s unregistered %s", self, tcp_key)
        tcp_connection = self._tcp_connections.pop(tcp_key)
        self._env.log1('%s', tcp_connection.status())
        if not self._tcp_connections:
            self._idle_deadline = timer() + self._env.config.MAX_IDLE_SESSION # last TcpConnection closed - start counting

    def cancel_http_connection(self, http_connection):
        """
        A HttpClient waiting for reply isn't ready anyway - probably dead
        """
        if http_connection in self._sendable_http_connections:
            self._env.log2('%s cancel %s', self, http_connection)
            self._sendable_http_connections.remove(http_connection) # this is O(n) but for small n and happens seldom
        else:
            self._env.log2('%s cancel unsendable %s', self, http_connection)

    def die_if_ready(self):
        """
        Allow Session to die decently - and make sure it eventually dies
        """
        if self._idle_deadline and timer() > self._idle_deadline:
            self._env.log1('%s has been idle for %s seconds - closing NOW', self, self._env.config.MAX_IDLE_SESSION)
            for http_connection in self._sendable_http_connections:
                self._env.log2('%s closing %s', self, http_connection)
                http_connection.close()
            for tcp_connection in self._tcp_connections.values(): # Well ... we only get here if there are not connections ...
                self._env.log2('%s closing %s', self, tcp_connection)
                tcp_connection.close()
            self._tcp_connections.clear()
            self.close()
            return True
        return False

    def ping_tick(self):
        """
        Session pings server to ensure that it has something alive it can use to reply on
        """
        self._env.log2('%s pinging server - sending empty request', self)
        self.enqueue_message(self._make_ping())

    def retransmit_tick(self):
        """
        It is time to consider retransmitting whatever the sessions TcpConnections wants ...
        """
        self._env.log2('%s retransmit tick', self)
        for tcp_connection in self._tcp_connections.values():
            tcp_connection.retransmit_tick()

    def _make_ping(self):
        Session._ping_counter += 1
        return tcp.TcpMessage(self, '', self._env.out_symbol, Session._ping_counter, '', False)

    def _encode(self, out_message):
        """
        Make properly formatted request content
        """
        self._env.log2('%s encoding %s with %s bytes', self, out_message, len(out_message.data))
        acks = self._pending_acks.items()
        self._pending_acks.clear() # We have acked. If ack doesn't come through they will resend and we will re-ack.
        self._pending_acks_deadline = None

        http_content = ''.join(('Ping\r\n',
                                'Acks:', ','.join('%s:%s' % (k, s) for (k, s) in acks), '\r\n',
                                'Session-Key:', self._session_key, '\r\n',
                                'Tcp-Key:', out_message.tcp_key, '\r\n',
                                'Seq:', str(out_message.seq), '\r\n',
                                'Status:', 'Close' if out_message.closing else 'Fine', '\r\n',
                                'Content-Length:', str(len(out_message.data)), '\r\n',
                                '\r\n',
                                out_message.data))
        return http_content

    def status(self):
        stati = [tcp_connection.status() for tcp_connection in self._tcp_connections.values()]
        stati.append(
            '%s HTTP connections waiting for TCP: %s, Pending acks: %s, Messages: %s' % (
            self,
            len(self._sendable_http_connections),
            self._pending_acks, ' '.join(repr(out_message) for out_message in self._messages)))
        return '\n'.join(stati)

    def __repr__(self):
        return self._session_key

    __str__ = __repr__


class main(object):

    def __init__(self):
        self.stopflag = False

    def run(self, env, session_manager, async_map, loop_rest=0.02):
        while session_manager.sessions and not self.stopflag:
            env.log2('')
            time_to_next = min(session_manager.time_to_ping(), session_manager.time_to_retransmit())
            asyncore.loop(time_to_next or 1, count=1, map=async_map)
            session_manager.check_ping()
            session_manager.check_retransmit()
            session_manager.do_queued_work_sessions()
            env.log2('')
            env.log2('Status:\n%s', session_manager.status_sessions())
            if loop_rest:
                time.sleep(loop_rest)
        env.log1("Done - no session")

    def stop(self):
        """
        Call from other thread to ask main_loop to stop - not waiting for it
        """
        self.stopflag = True
