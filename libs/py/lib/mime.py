"""
Our own simple mime parser
TODO: Is it really faster than the built in email package? BaseHTTPServer uses deprecated mimetools ...
"""
import collections

class Headers(object):
    """
    Parse mime header format.
    data must be in RFC 2616 4.1-4.2 format:
    CRLF-delimited lines
    first a start-line
    then fieldname-colon-value lines
    then an empty line
    Bugs: Doesn't handle line continuation and repeated headers
    """

    def __init__(self, start_line=None, items=None):
        #print 'parsing %r' % parse
        self._items = []
        self._dict = collections.defaultdict(list)
        self.start_line = start_line
        for k, v in items or []:
            self.add(k, v)

    def parse(self, s, start_line=None):
        assert not self.start_line
        assert not self._items
        assert not self._dict
        self.start_line = start_line
        line_start_pos = 0
        if self.start_line is None:
            line_end_pos = s.find('\r\n')
            self.start_line = s[:line_end_pos]
            line_start_pos = line_end_pos + 2
        while True:
            line_end_pos = s.find('\r\n', line_start_pos)
            assert line_end_pos >= 0, 'unterminated header line %r' % s[line_start_pos:]
            line = s[line_start_pos:line_end_pos]
            line_start_pos = line_end_pos + 2
            if not line:
                break
            parts = line.split(':', 1)
            # assert not line[0].isspace() # RFC 2616 4.2 says join...
            assert len(parts) == 2, 'header line %r must contain colon' % line
            self.add(parts[0], parts[1])
        #print 'parsed %r %r %r' % (self.start_line, self._items, s[line_start_pos:][:50] + '...')
        return s[line_start_pos:]

    def items(self):
        return self._items

    def add(self, k, v, replace=False):
        k = k.strip()
        kupper = k.upper()
        if replace or v is None:
            self._items = [(ik, iv) for (ik, iv) in self._items if ik.upper() != kupper]
            self._dict.pop(kupper, None)
        if not v is None:
            v = str(v).strip() # Note/todo/fixme: we don't consider RFC2616 4.2 comma separated values
            self._items.append((k, v))
            # assert k.upper() not in self._dict # RFC 2616 4.2 says append in right order if list...
            self._dict[kupper].append(v)

    def replace(self, k, v, replace=True):
        self.add(k, v, replace=replace)

    def get(self, k, default=None):
        try:
            return self._dict[k.upper()][0]
        except IndexError:
            return default

    def __getitem__(self, k):
        # like get, will not raise IndexError!
        return self.get(k)

    def getvalues(self, k, default=None):
        try:
            return self._dict[k.upper()]
        except IndexError:
            return default

    def format(self, start_line=None):
        lines = [start_line or self.start_line]
        assert lines[0] is not None, lines
        lines.extend('%s: %s' % (k, v) for (k, v) in self.items())
        return '\r\n'.join(lines) + '\r\n\r\n'

    def __str__(self):
        return '<Headers:\n%s\n>' % self.format().rstrip().replace('\r\n', '\n')

def test():
    parsed = Headers()
    assert parsed.parse(
                     "firstline\r\n"
                     "a:\r\n"
                     "b:b:b\r\n"
                     "b::\r\n"
                     "what about this : strange!\r\n"
                     "\r\n"
                     "now: the rest"
                     #" not supported: not at all"
                     ) == 'now: the rest'
    assert parsed.start_line == 'firstline'
    assert len(parsed._dict) == 3
    assert parsed['A'] == ''
    assert parsed.get('B') == 'b:b'
    assert parsed.getvalues('B') == ['b:b', ':']
    assert parsed.get('C') is None
    assert parsed['WHAT ABOUT THIS'] == 'strange!'
    parsed.add('b', 'added')
    assert parsed.getvalues('B') == ['b:b', ':', 'added']
    assert parsed.format() == 'firstline\r\na: \r\nb: b:b\r\nb: :\r\nwhat about this: strange!\r\nb: added\r\n\r\n'
    parsed.replace('b', 'replaced')
    cloned = Headers(start_line='1', items=parsed.items())
    cloned.add('B', None)
    assert parsed.getvalues('B') == ['replaced']
    assert parsed.format() == 'firstline\r\na: \r\nwhat about this: strange!\r\nb: replaced\r\n\r\n'
    assert cloned.getvalues('b') == []
    assert cloned.format() == '1\r\na: \r\nwhat about this: strange!\r\n\r\n'

    parsed = Headers()
    parsed.parse("\r\n\r\n")
    assert parsed.start_line == ''
    assert parsed._dict == {}
    try:
        Headers().parse("firstline\r\nsecondline:")
        assert False, 'should have failed'
    except AssertionError, e:
        assert e.args == ("unterminated header line 'secondline:'",)
    try:
        Headers().parse("firstline\r\nsecondline\r\n")
        assert False, 'should have failed'
    except AssertionError, e:
        assert e.args == ("header line 'secondline' must contain colon",)

if __name__ == '__main__':
    test()
