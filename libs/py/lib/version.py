"""
This file contains version information for the current build.
"""
import datetime
try:
    import version_include
    x = version_include.GIRITECH_LICENSE_PUB # to trick exception if not found
    x = version_include.GIRITECH_LICENSE_PUB_RSA # to trick exception if not found
except:
    class VersionInclude(object):
        def __init__(self):
            self.GIRITECH_VERSION_COMPANY_NAME = 'Unknown'
            self.GIRITECH_VERSION_COPYRIIGHT_NOTICE = 'Unknown'
            self.GIRITECH_LICENSE_PUB = 'Unknown'
            self.GIRITECH_LICENSE_PUB_RSA = 'Unknown'
    version_include = VersionInclude()


class Version(object):
    def __init__(self, version_major, version_minor, version_bugfix, version_build_id, version_depot=None, build_date=None):
        self._version_major = version_major
        self._version_minor = version_minor
        self._version_bugfix = version_bugfix
        self._version_build_id = version_build_id
        self._version_depot = version_depot
        self._build_date = build_date
        self._company_name = version_include.GIRITECH_VERSION_COMPANY_NAME
        self._copyright = version_include.GIRITECH_VERSION_COPYRIIGHT_NOTICE
        self._license_pub_key = version_include.GIRITECH_LICENSE_PUB
        self._license_pub_key_rsa = version_include.GIRITECH_LICENSE_PUB_RSA

        if self._version_build_id is None:
            self._version_build_id = 0

    def get_version_string(self):
        return "%i.%i.%i-%i " % (self.get_version_major(),
                           	     self.get_version_minor(),
                                 self.get_version_bugfix(),
                                 self.get_version_build_id()
                                 )

    def get_version_string_num(self):
        return "%i.%i.%i.%i " % (self.get_version_major(),
                           	     self.get_version_minor(),
                                 self.get_version_bugfix(),
                                 self.get_version_build_id()
                                 )

    def equal(self, other):
        return (self._version_major == other.get_version_major() and
                self._version_minor == other.get_version_minor() and
                self._version_bugfix == other.get_version_bugfix() and
                self._version_build_id == other.get_version_build_id())

    def less(self, other):
        return ( (self._version_major < other.get_version_major()) or
                 (self._version_major <= other.get_version_major() and self._version_minor < other.get_version_minor()) or
                 (self._version_major <= other.get_version_major() and self._version_minor <= other.get_version_minor() and self._version_bugfix < other.get_version_bugfix()) or
                 (self._version_major <= other.get_version_major() and self._version_minor <= other.get_version_minor() and self._version_bugfix <= other.get_version_bugfix() and self._version_build_id < other.get_version_build_id())
                )

    def equal_major_minor_bugfix(self, other):
        return (self._version_major == other.get_version_major() and
                self._version_minor == other.get_version_minor() and
                self._version_bugfix == other.get_version_bugfix())

    def equal_major_minor(self, other):
        return (self._version_major == other.get_version_major() and
                self._version_minor == other.get_version_minor() )

    def equal_major(self, other):
        return self._version_major == other.get_version_major()

    def get_version_major(self):
        return self._version_major

    def get_version_minor(self):
        return self._version_minor

    def get_version_bugfix(self):
        return self._version_bugfix

    def get_version_build_id(self):
        return self._version_build_id

    def get_version_depot(self):
        return self._version_depot

    def get_build_date(self):
        return self._build_date

    def get_build_date_string(self):
        return self._build_date.strftime('%Y.%m.%d %H:%M:%S')

    def get_license_stamp_key(self):
        return self._license_pub_key

    def get_license_stamp_key_rsa(self):
        return self._license_pub_key_rsa

    def expired(self):
        expire_date = self.get_build_date() + datetime.timedelta(days=180)
        return expire_date < datetime.datetime.now()

    def encode(self):
        return (self.get_version_major(), self.get_version_minor(), self.get_version_bugfix(), self.get_version_build_id())

    def encode_mmb(self):
        return (self.get_version_major(), self.get_version_minor(), self.get_version_bugfix())

    def get_company_name(self):
        return self._company_name

    def get_copyright(self):
        return self._copyright

    @classmethod
    def find_version_path_mmb(cls, from_version_mmb, to_version_mmb, version_map_mmb):
        """
        Find the version path between two versions using a version map.
        If not path exists between the two versions, None is returned.
        """
        def _find_version_path(from_version_mmb, to_version_mmb, version_path_mmb):
            from_version = Version.create_mmb(from_version_mmb)
            to_version = Version.create_mmb(to_version_mmb)
            if not from_version.equal(to_version):
                prev_version_mmb = version_map_mmb.get(to_version.encode_mmb(), None)
                if prev_version_mmb is None:
                    version_path_mmb = None
                    return
                version_path_mmb.append(prev_version_mmb)
                return _find_version_path(from_version_mmb, prev_version_mmb, version_path_mmb)
            return version_path_mmb

        version_path_mmb = [to_version_mmb]
        return _find_version_path(from_version_mmb, to_version_mmb, version_path_mmb)

    @classmethod
    def do_version_path_exists(cls, from_version, to_version, version_map_mmb):
        version_path = cls.find_version_path_mmb(from_version.encode_mmb(), to_version.encode_mmb(), version_map_mmb)
        return version_path is not None

    @classmethod
    def create(cls, version_major, version_minor, version_bugfix=0, version_build_id=0, version_depot=None, build_date=None ):
        return Version(version_major, version_minor, version_bugfix, version_build_id, version_depot, build_date)

    @classmethod
    def create_mmb(cls, (version_major, version_minor, version_bugfix) ):
        return Version(version_major, version_minor, version_bugfix, version_build_id=0)

    @classmethod
    def create_from_encode(cls, encoded_version):
        version_major = encoded_version[0]
        version_minor = encoded_version[1]
        version_bugfix = encoded_version[2]
        version_build_id = encoded_version[3]
        return Version(version_major, version_minor, version_bugfix, version_build_id)

    @classmethod
    def create_current(cls, build_id=None):
        version_major = version_include.GIRITECH_VERSION_MAJOR
        version_minor = version_include.GIRITECH_VERSION_MINOR
        version_bugfix = version_include.GIRITECH_VERSION_BUGFIX
        if build_id is None:
            version_build_id = version_include.GIRITECH_VERSION_BUILD_ID
        else:
            version_build_id = build_id
        version_depot = version_include.GIRITECH_VERSION_DEPOT
        build_date_string = version_include.GIRITECH_BUILD_DATE

        build_date = datetime.datetime.strptime(build_date_string, '%Y.%m.%d %H:%M:%S')
        return Version(version_major, version_minor, version_bugfix, version_build_id, version_depot, build_date)

    @classmethod
    def create_from_string(cls, version_as_string):
        version_as_string_split = version_as_string.replace('-','.').strip().split('.')
        version_major = int(version_as_string_split[0])
        version_minor = int(version_as_string_split[1])
        version_bugfix = int(version_as_string_split[2])

        version_build_id = 0
        if len(version_as_string_split) > 3:
            version_build_id = int(version_as_string_split[3])

        return Version(version_major, version_minor, version_bugfix, version_build_id)

    @classmethod
    def compare(cls, a, b):
        if a.equal(b):
            return 0
        elif a.less(b):
            return -1
        else:
            return 1

if __name__ == '__main__':
    print Version.create_current().get_build_date().isoformat(' ')
    print Version.create_from_encode(Version.create_current().encode()).get_version_string()
