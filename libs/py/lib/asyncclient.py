import asyncore
import socket
import errno


class AsyncClient(asyncore.dispatcher):

    def __init__(self, env, async_map, connect_addr, cb_tcp_connected, cb_tcp_error):
        asyncore.dispatcher.__init__(self, map=async_map)
        self._env = env
        self._connect_addr = connect_addr
        self._cb_tcp_connected = cb_tcp_connected
        self._cb_tcp_error = cb_tcp_error
        self._cb_tcp_from_server = None
        self._recv_buffer = '' # Stuff not offered but not yet accepted by _cb_tcp_from_server (FIXME: Why?)
        self._send_buffer = '' # When in state SENDING_REQUEST: what needs to be sent
        self._send_shutdown = False # Send shutdown after _send_buffer
        self._readable = True
        try:
            self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
            self.connect(connect_addr)
        except socket.error, (err, msg):
            self._cb_tcp_error('socket error %s connecting to %s: %s' % (err, self._connect_addr, msg))

    # implemented asyncore handlers:

    def handle_expt(self):
        self._env.log2('%s exception - closing', self)
        self.close()
        self._cb_tcp_error('asyncore socket exception')

    def handle_error(self):
        self._env.log2('%s error - closing', self)
        self.close()
        self._cb_tcp_error('asyncore socket error')

    def handle_connect(self):
        try:
            sockname = self.getsockname()
            peername = self.getpeername()
        except socket.error, (err, msg):
            self._cb_tcp_error('async socket error %s connecting to %s: %s' % (err, self._connect_addr, msg))
            return
        self._env.log3('%s created from %s:%s to %s:%s', self, sockname[0], sockname[1], peername[0], peername[1])
        self._cb_tcp_from_server, self._cb_tcp_error = self._cb_tcp_connected(
                sockname, peername, self.to_server, self.close)
        self._cb_tcp_connected = None # once

    def readable(self):
        return bool(self._cb_tcp_from_server) and self._readable # no callbacks before ready

    def handle_close(self): # also called on half-close
        self._env.log3('%s handle_close', self)
        if not self.connected:
            self._env.log1('%s connection failed', self)
            self.close()
            self._cb_tcp_error('closed before connected')

    def handle_read(self):
        """
        Something happened on socket - data or shutdown/close
        """
        try:
            data_chunk = self.recv(self._env.config.TCP_RECV_BUFFER)
        except socket.error, (err, msg):
            self._env.log2('%s socket error handled as close: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
            data_chunk = ''

        if not data_chunk:
            self._readable = False
            if self._recv_buffer:
                self._env.log2("%s EOF from client with pending data", self)
                self.close()
                self._cb_tcp_error('premature EOF')
                return

        self._recv_buffer += data_chunk
        self._env.log2("%s got %s now %s", self, len(data_chunk), len(self._recv_buffer))

        self._recv_buffer = self._cb_tcp_from_server(self._recv_buffer)
        if self._recv_buffer is None:
            self._env.log2("%s engine said close", self)
            self.close()
            self._cb_tcp_error(None)
            return
        if (self._send_buffer or self._send_shutdown) and not self._recv_buffer:
            self.handle_write() # use fastpath if connected

    def writable(self):
        return not self.connected or bool(self._send_buffer) or self._send_shutdown

    def handle_write(self):
        if not self.connected:
            self._env.log2('%s selected for write but will not because not connected', self) # probably fastpath ...
            return
        if self._send_buffer:
            self._env.log3('%s selected for write with send buffer size %s', self, len(self._send_buffer))
            try:
                byte_count = self.send(self._send_buffer)
            except socket.error, (err, msg):
                self._env.log2('%s closing because of socket error sending HTTP reply: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                self._send_buffer = ''
                self.close()
                self._cb_tcp_error('error sending')
                return
            self._send_buffer = self._send_buffer[byte_count:]
            self._env.log2('%s sent %s bytes - %s more to go', self, byte_count, len(self._send_buffer))
        elif self._send_shutdown:
            self._env.log3('%s selected for write with shutdown', self)
            self._send_shutdown = False
            try:
                self.shutdown(socket.SHUT_WR)
            except socket.error, (err, msg):
                if err != errno.ENOTCONN:
                    self._env.log2('%s closing because of socket error shutting down: %s/%s: %r', self, err, errno.errorcode.get(err), msg)
                    self._cb_tcp_error('error shutting down')
                    self.close()
                return
        else:
            self._env.log2('%s selected for write but will not because nothing to send', self)

    # public api (like close())

    def to_server(self, buf):
        self._env.log3('%s added %r to send buffer', self, buf)
        if buf:
            self._send_buffer += buf
        else:
            self._send_shutdown = True

    # internal:

    def __repr__(self):
        return '%s%X' % (self.__class__.__name__, id(self))

    __str__ = __repr__
