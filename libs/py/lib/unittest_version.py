"""
Unittest of liv.version
"""
import unittest
from lib import giri_unittest

import lib.version


class FindVersionPath(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_find_path_simple(self):

        version_map_mmb = {
                           (5,0,0) : None,
                           (5,0,1) : (5,0,0),
                           (5,0,2) : (5,0,1)
        }
        from_version_mmb = (5,0,0)
        to_version_mmb   = (5,0,2)
        version_path_mmb = lib.version.Version.find_version_path_mmb(from_version_mmb, to_version_mmb, version_map_mmb)
        self.assertEqual(len(version_path_mmb), 3)

        from_version_mmb = (4,0,0)
        to_version_mmb   = (5,0,2)
        version_path_mmb = lib.version.Version.find_version_path_mmb(from_version_mmb, to_version_mmb, version_map_mmb)
        self.assertEqual(version_path_mmb, None)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
