import subprocess
import threading
import os
import sys

if sys.platform == "win32":
    import pywintypes
    import win32process
    import win32event
    import win32con
    import win32gui
else:
    import signal
    import shlex

import checkpoint
import socket_owner


class Process(object):
    """
    Objects represents a running process which is waited on in a thread
    """

    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
        self._handle = None
        self._pid = None
        self._callback_thread = None
        self._callback = None

    def launch(self, launch_command, shell=False, callback=None, cwd=None, env=None, error_callback=None):
        """
        Start the process. Call returns True when the process has started starting and
        callback will be called from thread with self as argument when the process terminates.
        Returns False if the process couldn't be started - and callback won't be called.

        On windows subprocess.Popen calls CreateProcess Function with bInheritHandles which causes sockets to
        be inherited and thus already in use when they should be used - so we create and use our own
        Note: This subprocess will not have any stdin/out/err!
        FIXME: Use something like http://benjamin.smedbergs.us/blog/2006-11-09/adventures-in-python-launching-subprocesses/
        or http://trentm.com/projects/process/ ?
        """
        self._callback = callback

        if sys.platform == "win32":
            startupinfo = win32process.STARTUPINFO()
            if shell:
                startupinfo.dwFlags |= win32process.STARTF_USESHOWWINDOW
                startupinfo.wShowWindow = win32con.SW_HIDE
                launch_command = os.environ.get("COMSPEC", "cmd.exe") + " /c " + launch_command

            self.checkpoint_handler.Checkpoint("launch_process", "lib",
                                               checkpoint.DEBUG,
                                               #message='starting %r' % (launch_command,), # might reveal information
                                               message='starting',
                                               )
            try:
                hp, ht, pid, _tid = win32process.CreateProcess(None, launch_command,
                                                               None, None, # security stuff
                                                               0, # bInheritHandles
                                                               0, # creationflags
                                                               None, # env
                                                               cwd or None, # cwd
                                                               startupinfo)
            except pywintypes.error, err:
                # Might be error(3, 'CreateProcess', 'The system cannot find the path specified.')
                # Translate pywintypes.error to WindowsError, which is
                # a subclass of OSError.  FIXME: We should really
                # translate errno using _sys_errlist (or similar), but
                # how can this be done from Python?
                #raise WindowsError(*err.args)
                self.checkpoint_handler.Checkpoint("launch_process", "lib",
                                                   checkpoint.ERROR,
                                                   message='pywintypes.error: %r' % (err,))
                if error_callback is not None:
                    (err_code, _err_class, err_message) = err
                    err_message_unicode = err_message.decode('mbcs', 'ignore')
                    error_callback('client_launch', err_code, '%s\n%s' % (launch_command, err_message_unicode))
                return False
            # Retain the process handle, but close the thread handle
            self._handle = hp
            self._pid = pid
            ht.Close()

        else: # not win32 - Linux or Mac
            args = shlex.split(launch_command)
            if shell:
                args = ["/bin/sh", "-c"] + args
            self.checkpoint_handler.Checkpoint("Process.launch", "lib",
                                               checkpoint.DEBUG,
                                               #message='starting %r parsed as %r' % (launch_command, args,), # might reveal information
                                               message='starting',
                                               )
            try:
                # Note: subprocess will inherit out/err on unix - perhaps we should use a pipe and use it for something ...
                forked_process = subprocess.Popen(args, stdin=open('/dev/null'), close_fds=True, cwd=cwd or None, env=env)
            except OSError, err:
                self.checkpoint_handler.Checkpoint("Process.launch", "lib",
                                                   checkpoint.ERROR,
                                                   message='OSError: %r' % (err,))
                if error_callback is not None:
                    error_callback('client_launch', 0, '%s\n%s' % (launch_command, err))
                return False
            self._pid = forked_process.pid

        self._callback_thread = threading.Thread(target=self._run, args=(), name="launch_process.Process")
        self._callback_thread.setDaemon(True) # Don't necessarily wait for this thread to die before exiting # FIXME???
        self.checkpoint_handler.Checkpoint("launch_process_start_thread", "lib",
                                           checkpoint.DEBUG,
                                           message='starting thread %s with pid %s' % (self._callback_thread, self._pid))
        self._callback_thread.start()
        return True

    def _run(self):
        """
        What the thread is doing: hanging, and eventually calling callback
        """
        pid = self._pid # read value, but don't write yet
        if pid:
            if sys.platform == "win32":
                _obj = win32event.WaitForSingleObject(self._handle, win32event.INFINITE)
            else:
                _pid, _sts = os.waitpid(pid, 0)
        self.checkpoint_handler.Checkpoint("launch_process_run", "lib", checkpoint.DEBUG,
                                           message='Finished waiting for pid %s' % (pid,))
        if self._callback:
            self._callback(self)
        self._pid = None # Nothing to kill
        self._callback_thread = None # Don't join this thread

    def kill(self):
        """
        Brutally kill the process - as in "boss is coming - key removed and all traces should disappear ASAP".
        callback will be called afterwards.
        """
        pid, self._pid = self._pid, None
        self.checkpoint_handler.Checkpoint("launch_process_kill", "lib",
                                           checkpoint.DEBUG,
                                           message='killing thread %s with pid %s handle %s' % (self._callback_thread, pid, self._handle))
        if pid:
            if sys.platform == "win32":
                def closer(hwnd, dummy):
                    """
                    EnumWindows callback - sends WM_CLOSE to any window owned by this process.
                    """
                    _TId, PId = win32process.GetWindowThreadProcessId(hwnd)
                    if PId == pid:
                        try:
                            win32gui.PostMessage(hwnd, win32con.WM_CLOSE, 0, 0)
                        except Exception:
                            self.checkpoint_handler.Checkpoint("launch_process_kill.unable_to_post_close", "lib",
                                               checkpoint.DEBUG,
                                               pid='%s' % (pid))
                win32gui.EnumWindows(closer, 0)
                if win32event.WaitForSingleObject(self._handle, 2000) != win32event.WAIT_OBJECT_0:
                    win32process.TerminateProcess(self._handle, 0)
            else:
                try:
                    os.kill(pid, signal.SIGTERM)
                    # FIXME: SIGKILL???
                except OSError:
                    pass # Probably OSError: [Errno 3] No such process
            thread = self._callback_thread
            if thread:
                thread.join() # wait for callback to complete - but it might have completed already
            assert not self._callback_thread # removed by thread when it terminates
            self.checkpoint_handler.Checkpoint("launch_process_kill_joined", "lib",
                                               checkpoint.DEBUG,
                                               message='completed killing thread with pid %s' % (pid))
        else:
            self.checkpoint_handler.Checkpoint("launch_process_kill_joined", "lib",
                                               checkpoint.DEBUG,
                                               message='thread already dead')

    def owns(self, local_address, rem_address, sub):
        """
        Check if process (or sub-processes) owns the tcp connection designated by the 4-tuple
        Note: server and client see the connection from different sides
        """
        pid = self._pid
        if not pid:
            return False
        return socket_owner.is_connection_owned_by(pid, local_address, rem_address, sub)

    def running(self):
        """
        Returns true if process is running
        """
        return bool(self._pid)



def launch_command(command_args, ignore_stdout=False, ignore_stderr=True, shell=False):
    """
    This is a really dirty hack that should be changed.

    The issue is that if close_fds is not set to true and shell=True, then the launched command never terminates
    when run as java -> python -> python

    Somehow the user of stdout block/stop/halts the started subprocess.
    """
    try:
        command_output = None

        command_args = subprocess.list2cmdline(command_args)
        process = subprocess.Popen(command_args,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdin=subprocess.PIPE,
                                   shell=shell,
                                   #creationflags=win32process.CREATE_NO_WINDOW,
                                 )
        out, err = process.communicate()
        command_output = out
        if process.returncode:
            command_err = err
            raise Exception("Error in result from process '%s', return code = '%s', command_result='%s" % (command_args, process.returncode, command_err))
        return (process.returncode, command_output)
    except:
        (_etype, evalue, _etrace) = sys.exc_info()
        return (-1, repr(evalue))
