"""
Communication with G&D file-system based GSI interface

Inspired by mosec stuff
"""

import ctypes
libc = ctypes.CDLL("libc.so.6")

import lib_cpp.communication

import os # use os.O_ constants

BLOCK_SIZE = 512

class GsiFs(object):

    def __init__(self, filename):
        self._fd = libc.open(filename, os.O_RDWR | os.O_DIRECT)
        assert self._fd != -1, 'Error opening %r' % filename

    def close(self):
        libc.close(self._fd)
        self._fd = None

    def send(self, request):
        """Send GSI-encoded block"""
        bytes_written = lib_cpp.communication.fd_write(self._fd, request)
        assert bytes_written == BLOCK_SIZE, 'Incomplete write: %s bytes' % bytes_written
    
    def read(self):
        """Read GSI-encoded block"""
        bytes_read, read_buffer = lib_cpp.communication.fd_read(self._fd)
        assert bytes_read == BLOCK_SIZE, 'Incomplete read: %s bytes' % bytes_read
        return read_buffer
    