"""
Library for simple RSA-based public/private authentication with private keys stored in files.
Identification is outside this scope.
Implementation is based on custom C++ boost::python wrapper around crypto++ subset.
"""

import sys
import lib_cpp.communication


def create_keypair(private_key_filename, bits=1024, public_exponent=65537):
    """
    Generate new key pair, create private_key_filename with private RSA key, and return the public key.
    """
    signer = lib_cpp.communication.generate_signer(bits)
    open(private_key_filename, 'w').write(lib_cpp.communication.signer_encode(signer))
    mod = lib_cpp.communication.signer_get_mod(signer)
    exp = lib_cpp.communication.signer_get_exp(signer)
    return (mod, exp)

def sign_challenge(private_key_filename, challenge):
    """
    Load key pair from private_key_filename and return a signature of the challenge.
    """
    signer = lib_cpp.communication.signer_decode(open(private_key_filename).read())
    return lib_cpp.communication.signer_sign(signer, challenge)

def get_public_key(private_key_filename):
    """
    Load key pair from private_key_filename and return the public key
    """
    signer = lib_cpp.communication.signer_decode(open(private_key_filename).read())
    mod = lib_cpp.communication.signer_get_mod(signer)
    exp = lib_cpp.communication.signer_get_exp(signer)
    return (mod, exp)

def verify_signature(public_key, challenge, challenge_signature):
    """
    Return True if public_key verifies that challenge_signature signs challenge.
    """
    (mod, exp) = public_key
    verifier = lib_cpp.communication.verifier_from_pair(mod, exp)
    return lib_cpp.communication.verifier_verify(verifier, challenge, challenge_signature)
    
def demo():

    def hex(s):
        return ' '.join('%02x' % ord(x) for x in s)

    private_key_filename = 'test.private'
    
    #
    # Admin server:
    #    
    public_key = create_keypair(private_key_filename)
    print 'mangement server created %s with public key:' % (private_key_filename,), public_key
    # Now move private_key_filename to authentication device

    #
    # Runtime server:
    #
    challenge_length = 7
    import random
    random_challenge = ''.join(chr(random.randrange(0, 256)) for x in range(challenge_length))
    print 'servers random_challenge:', hex(random_challenge)
    
    #
    # Client:
    #
    # public key might be known from deployment and assumed, but can also be read from client
    public_key_serialized = get_public_key(private_key_filename)  
    print 'clients public key serialized:', public_key_serialized
    challenge_signature = sign_challenge(private_key_filename, random_challenge)
    print 'clients challenge_signature:', hex(challenge_signature)
    
    #
    # Runtime server:
    #
    print 'validation', verify_signature(public_key, random_challenge, challenge_signature) 
    print

def demo2():    
    print 'Test with values from real Smart Card:'
    exp = ''.join(chr(x) for x in [0x01, 0x00, 0x01])
    mod = ''.join(chr(x) for x in [0x8a, 0xbd, 0x92, 0x66, 0x57, 0xb2, 0x3a, 0xc0, 0xd8, 0x84, 0x9e, 0x84, 0x02, 0x77, 0x08, 0x40, 0x02, 0x3f, 0x2f, 0x37, 0x51, 0x57, 0xea, 0x23, 0x2e, 0xcc, 0x89, 0xc6, 0xf6, 0x94, 0x38, 0x26, 0xff, 0xde, 0x5e, 0x20, 0x4a, 0xb3, 0x7d, 0x12, 0xae, 0x7b, 0xb1, 0x38, 0x37, 0x7a, 0x3c, 0x95, 0x46, 0xb3, 0x78, 0x12, 0x45, 0x5c, 0xc4, 0x15, 0xbf, 0xb7, 0x80, 0x38, 0xf2, 0x65, 0xe8, 0x03, 0xea, 0xc8, 0x67, 0xad, 0x67, 0xa8, 0x0c, 0x03, 0xa8, 0x61, 0x58, 0x94, 0x98, 0x78, 0xc6, 0xc1, 0x70, 0xbb, 0x2e, 0x34, 0x02, 0x9f, 0x43, 0xd7, 0xee, 0x06, 0x17, 0xe1, 0xcf, 0xb2, 0xb2, 0x85, 0x0f, 0xb0, 0x9b, 0x5b, 0x8d, 0x45, 0xbe, 0x8a, 0xfc, 0xca, 0xee, 0x4a, 0xe1, 0xb9, 0x5d, 0x0e, 0x5a, 0x2b, 0x79, 0xbf, 0xe4, 0x5d, 0x8f, 0x1b, 0xda, 0x59, 0x25, 0xfa, 0x0a, 0x66, 0xcb, 0xa1
                                   ])
    challenge = "Weissbier"
    chalhash = ''.join(chr(x) for x in [0x92, 0x48, 0xe8, 0x19, 0x6e, 0x14, 0x01, 0xe9, 0x90, 0x73, 0xb0, 0x4b, 0xbc, 0x52, 0x35, 0x96, 0x77, 0x7f, 0x5d, 0x61])
    sign = ''.join(chr(x) for x in [0x88, 0x25, 0xEC, 0xE9, 0xC8, 0x9D, 0xC1, 0xD4, 0x26, 0xCE, 0x51, 0x1D, 0x48, 0x4D, 0x1C, 0xF0, 0xA2, 0x42, 0x04, 0x33, 0xC3, 0x2A, 0x30, 0xF4, 0xA9, 0xE2, 0x6D, 0xC9, 0x71, 0xF1, 0xC8, 0x8B, 0x81, 0x6D, 0xD0, 0xF6, 0x24, 0x65, 0x7E, 0x6E, 0x89, 0x49, 0x65, 0x66, 0x08, 0x2D, 0xBC, 0x58, 0xA4, 0x64, 0x64, 0x97, 0xD2, 0x6B, 0x7C, 0x33, 0x64, 0xC9, 0x81, 0x10, 0x6E, 0x4F, 0x86, 0xB6, 0x42, 0x6B, 0xFB, 0x48, 0x97, 0xE3, 0x2E, 0xDF, 0xF7, 0xC7, 0xBF, 0x5C, 0x3F, 0x7E, 0xCA, 0x72, 0x9B, 0x84, 0x01, 0x4B, 0x24, 0x81, 0xB7, 0x3F, 0x49, 0x00, 0x08, 0x4D, 0x5E, 0x71, 0xFF, 0x61, 0x2B, 0x45, 0x5E, 0x0C, 0x76, 0x56, 0x85, 0xB0, 0xAB, 0xE3, 0x18, 0x1F, 0x6C, 0xB1, 0xB6, 0x95, 0x03, 0xFA, 0x99, 0x83, 0xB4, 0xDB, 0x66, 0x8B, 0xE4, 0xB9, 0x27, 0x6A, 0xF9, 0x71, 0x4B, 0x4A])
    decrypted = ''.join(chr(x) for x in [0x30, 0x21, 0x30, 0x09, 0x06, 0x05, 0x2b, 0x0e, 0x03, 0x02, 0x1a, 0x05, 0x00, 0x04, 0x14, 0x92, 0x48, 0xe8, 0x19, 0x6e, 0x14, 0x01, 0xe9, 0x90, 0x73, 0xb0, 0x4b, 0xbc, 0x52, 0x35, 0x96, 0x77, 0x7f, 0x5d, 0x61])
    verifier = lib_cpp.communication.verifier_from_pair(mod, exp)
    print 'Verified:', lib_cpp.communication.verifier_verify(verifier, challenge, sign)
    print 'verify_public_key_signature:', lib_cpp.communication.verify_public_key_signature(mod, exp, challenge, sign)
    print
    
if __name__ == '__main__':
    demo()
    demo2()