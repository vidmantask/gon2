"""
APDU Communication with MSC through G&D file-system based GSI interface

Based on "Specification Mobile Security Card 3.x - Host Interface Version 0.6/ Status 22.10.2007" [SMSC]
"""

from __future__ import with_statement

import struct
import time
import os.path
import sys

if sys.platform == 'win32':
    import GSI_fs_win as GSI_fs_platform
if sys.platform == 'linux2':
    import GSI_fs_linux as GSI_fs_platform
if sys.platform == 'darwin':
    import GSI_fs_mac as GSI_fs_platform
from common import *

# Magic provided by G&D, hardcoded in the MSCs we get
MAGIC = '75 A2 30 E7 8D EA AB 43 CA EB F7 85 C0 7A 8F D6'

# SMSC 4.2.1 and 4.2.2
DIRECTION_HOST_TERMINAL = 0
DIRECTION_HOST_UICC = 1
DIRECTION_TERMINAL_HOST = 2
DIRECTION_UICC_HOST = 3
DIRECTION_PROGRESS_COUNTER = 4
# Ullrich about ID-1:
DIRECTION_HOST_ID1 = 0x21
DIRECTION_ID1_HOST = 3 # 0x23 FIXME, temporary workaround - firmware bug?

# Constants
COMMAND_HEADER_LENGTH = 22
BLOCK_SIZE = 512

_GSI_RETRY_SLEEP = 0.1 # Note: First sleep might be especially important, especially on Mac ... 0.02 is good, 0.015 is bad ...
_GSI_SLEEP_FIRST = 0.1 # Note: First sleep might be especially important, especially on Mac ... 0.02 is good, 0.015 is bad ...

# Exception thrown if bad magic or not MSC or something else wrong
class NoMagicHappened(SmartCardException): pass
class WrongMagicHappened(NoMagicHappened): pass

class MSC(object):
    """A Mobile Security Card"""

    fs_content = BLOCK_SIZE * '7'

    def __init__(self, filename, id1=False):
        self._filename = filename
        self._id1 = id1
        self._gsi_fs = None

        # Check GSI file sanity so we don't get strange failures later on
        if not os.path.exists(filename):
            try: # Try to create the file as sync as possible
                f = open(filename, 'w')
                f.write(self.fs_content)
                f.flush()
                os.fsync(f.fileno())
                f.close()
            except IOError, e:
                raise SmartCardException('GSI file cannot be created: %s' % (e,))
        if not os.path.isfile(filename):
            raise SmartCardException('GSI file "%s" not a file' % (filename,))

    def __enter__(self):
        assert not self._gsi_fs

        self._gsi_fs = GSI_fs_platform.GsiFs(self._filename)

        # Ready to go
        if False: # overkill - not needed
            #print 'Reset smart card controller, hard reset/power-cycle'
            status, response = self.command(0x20, 0x11, 2 if self._id1 else 1, 1, None, 0, target_ct=True)
            # Will give status 640D or 6A00 if id1 wrong
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error in cold reset", status, response)

        if False: # Ullrich says not supported on ID-1:
            #print 'Reset smart card controller, warm reset'
            status, response = self.command(0x20, 0x11, 0x81, 0, None, 0, target_ct=True)
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error in warm reset", status, response)

        if False: # These are also not needed, but they checks that things are working before doing important stuff ...
            #print 'CT status: Get status info from Terminal'
            status, response = self.command(0x20, 0x13, 0, 0x46, None, 0, target_ct=True)
            #print 'Status string:', repr(''.join(chr(x) for x in response))
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error getting CT status", status, response)
            card_terminal_type = ''.join(chr(x) for x in response[6:11]) # SECFL or MTID1 - could be used to auto-detect something ...
            #print 'card_terminal_type:', card_terminal_type

            #print 'ICC status: Get status info from ICC'
            status, response = self.command(0x20, 0x13, 0, 0x80, None, 0, target_ct=True)
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error getting ICC status", status, response)

        #print 'Requesting ICC'
        status, response = self.command(0x20, 0x12, 2 if self._id1 else 1, 1, None, 0, target_ct=True)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error requesting ICC", status, response)

        return self

    def __exit__(self, _type, _value, _traceback):
        try:
            # Eject ICC - opposite of request
            _status, _response = self.command(0x20, 0x15, 0x00, 0, None, None, target_ct=True)
        except: pass
        self._gsi_fs.close()
        self._gsi_fs = None

    def command(self, cla, ins, p1, p2, command_data, expected_response_length, target_ct=False, debug_printer=None, timeout=5):
        """Encode APDU and send it over GSI interface. All parts are integer values.
        command_data is tuple. Length is automatically prepended. Whole field completely left out if empty or None.
        If expected_response_length is None then it is completely left out. (0 usually means 256.)
        Command can target Card Terminal or ICC (default).
        """
        retries = int(timeout / _GSI_RETRY_SLEEP) # Key generation is the worst and usually takes around 5s insgesamt - so 10s ought to be enough

        assert self._gsi_fs
        # Ensure known content and clear cache
        self._gsi_fs.send(self.fs_content)

        #import sys
        #debug_printer = lambda x:sys.stdout.write(x+'\n')
        # build command APDU
        command_apdu = (cla, ins, p1, p2)
        #print 'apdu %s [%s]' % (command_apdu, command_data and len(command_data))
        if command_data:
            assert len(command_data) <= 255, 'command data too long'
            command_apdu += (len(command_data),) + command_data
        if not expected_response_length is None:
            command_apdu += (expected_response_length,)
        if debug_printer:
            debug_printer('Sent %s Command APDU:' % ('CT' if target_ct else 'ICC'))
            debug_printer(dump_list(command_apdu))

        #print 'Command:', dump_list(command_apdu)

        # encode according to SMSC 4.2.1
        command_header = struct.pack('>BBH16sH',
                                     COMMAND_HEADER_LENGTH,
                                     DIRECTION_HOST_TERMINAL if target_ct else DIRECTION_HOST_ID1 if self._id1 else DIRECTION_HOST_UICC,
                                     0, # RFU
                                     ''.join(chr(int(x, 16)) for x in MAGIC.split()),
                                     len(command_apdu))
        assert len(command_header) == COMMAND_HEADER_LENGTH, 'error in command header, length %s' % len(command_header)
        request = command_header + ''.join(chr(x) for x in command_apdu) + '\x00' * (BLOCK_SIZE - COMMAND_HEADER_LENGTH - len(command_apdu))
        assert len(request) == BLOCK_SIZE, 'error padding command to block size'

        if debug_printer:
            debug_printer("Writing %s bytes:" % len(request))
            debug_printer(dump_string(request))

        self._gsi_fs.send(request)

        for _retry in range(retries):

            # Not necessary on new hardware, but it will decrease system load FIXME: avoid on first try when old card are burried
            if _retry:
                time.sleep(_GSI_RETRY_SLEEP)      
            else:
                time.sleep(_GSI_SLEEP_FIRST)

            # read response
            response = self._gsi_fs.read()

            if debug_printer:
                debug_printer("Read %s bytes:" % len(response))
                debug_printer(dump_string(response))

            if response == request:
                # Note: This could also be because device isn't MSC or bad magic or whatever - but it will time out
                if debug_printer:
                    debug_printer('Command read back as response - retrying write')
                # Note: Blindly repeating commands is IMHO a bad thing ...
                #print 'GSI'*20 + ' Command bounced - sending and reading again ...', _retry
                self._gsi_fs.send(request)
                continue # try again

            if response == self.fs_content:
                if debug_printer:
                    debug_printer('Old FS content read back - our magic result was lost, retrying write')
                # Note: Blindly repeating commands is definitely a bad thing ...
                #print 'GSI'*20 + ' got fs content - sending again ...', _retry
                self._gsi_fs.send(request)
                continue # try again

            if not response.strip('\0'): # apparently 1 Gb MSC returns this instead of progress counter?
                if debug_printer:
                    debug_printer('Got zero response - retrying read')
                #print 'GSI'*20 + ' Zero response - reading again ...', _retry
                continue # try again

            # now parse response
            (response_header_size, response_payload_type, _rfu, response_payload_size) = struct.unpack('>BBHH', response[:6])

            if (response_header_size == 6 and
                response_payload_type == DIRECTION_PROGRESS_COUNTER and
                response_payload_size == 2):
                if debug_printer:
                    debug_printer('Got ICC still computing, progress counter %02X%02X - retrying ...' % (ord(response[6]), ord(response[7])))
                #print 'GSI'*20 + ' WIP response - reading again ...', _retry
                continue # Progress counter from CT - try again

            if (response_header_size == 6 and
                response_payload_type == (DIRECTION_TERMINAL_HOST if target_ct else DIRECTION_ID1_HOST if self._id1 else DIRECTION_UICC_HOST) and
                response_payload_size >= 2):
                # print 'Got response ...', response_payload_type
                break # Apparently a good response

            print 'Bogus communication:'
            print 'Request'
            print dump_string(request.rstrip('\x00'))
            print 'Response'
            print dump_string(response.rstrip('\x00'))
            print 'Reponse error:', response_payload_type
            #raise WrongMagicHappened # We got a response we don't understand - bail out

        else:
            print 'Timeout'
            raise NoMagicHappened # Timeout

        # decode response
        response_apdu = tuple(ord(x) for x in response[response_header_size : response_header_size + response_payload_size])
        if debug_printer:
            debug_printer('Got %s Response APDU:' % ('CT' if target_ct else 'ICC'))
            debug_printer(dump_list(response_apdu))

        #print 'Response:', dump_list(response_apdu)

        assert len(response_apdu) >= 2 # checked above
        response_data = response_apdu[:-2]
        status = (response_apdu[-2] << 8) + response_apdu[-1]

        return status, response_data
