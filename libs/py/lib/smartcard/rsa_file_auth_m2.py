"""
Library for simple RSA-based public/private authentication with private keys stored in files.
Identification is outside this scope.
M2Crypto wrappers for OpenSSL is used - because no good wrappers for Crypto++ popped up. 
"""

import M2Crypto.RSA


def create_keypair(private_key_filename, private_key_password, bits=1024, public_exponent=65537):
    """
    Generate new key pair,
    create private_key_filename with private RSA key protected by password,
    and return the public key tuple.
    """
    rsa = M2Crypto.RSA.gen_key(bits, public_exponent, callback=lambda: None)
    rsa.save_pem(private_key_filename, callback=lambda set: private_key_password)
    return rsa.pub()

def sign_challenge(private_key_filename, private_key_password, challenge):
    """
    Load key pair from private_key_filename and sign the challenge.
    """
    rsa = M2Crypto.RSA.load_key(private_key_filename, callback=lambda set: private_key_password)
    return rsa.sign(challenge)

def verify_signature(public_key, challenge, challenge_signature):
    """
    Return True if public_key verifies that challenge_signature signs challenge.
    Return False if simply doesn't match, None on other RSA failure. 
    """
    rsa_pub = M2Crypto.RSA.new_pub_key(public_key)
    try:
        return rsa_pub.verify(challenge, challenge_signature) == 1
    except M2Crypto.RSA.RSAError, e:
        return None
    
def demo():
    private_key_filename = 'test.private'
    
    #
    # Server
    #
    private_key_password = 'foobar' # could be per installation, could try not to store on client
    
    #
    # Admin server:
    #    
    public_key = create_keypair(private_key_filename, private_key_password)
    print 'management server created %s with public key %r' % (private_key_filename, public_key) 
    # Now _move_ pem to client
    
    #
    # Runtime server:
    #
    challenge_length = 7
    from M2Crypto import Rand
    random_challenge = Rand.rand_bytes(challenge_length)
    print 'servers random_challenge %r' % random_challenge
    
    #
    # Client:
    #
    challenge_signature = sign_challenge(private_key_filename, private_key_password, random_challenge)
    print 'clients challenge_signature %r' % challenge_signature
    
    #
    # Runtime server:
    #
    print 'validation', verify_signature(public_key, random_challenge, challenge_signature) 

if __name__ == '__main__':
    demo()
