"""
Library for simple RSA-based public/private authentication with private keys stored in files.
Identification is outside this scope.
Warning: GPL pycryptopp wrappers for crypto++ is used!
"""

import pycryptopp.publickey.rsa


def create_keypair(private_key_filename, private_key_password, bits=1024, public_exponent=65537):
    """
    Generate new key pair,
    create private_key_filename with private RSA key protected by password,
    and return the public key tuple.
    """
    rsa = pycryptopp.publickey.rsa.generate(bits)
    open(private_key_filename, 'w').write(rsa.serialize())
    return rsa.get_verifying_key().serialize()

def sign_challenge(private_key_filename, private_key_password, challenge):
    """
    Load key pair from private_key_filename and sign the challenge.
    """
    rsa = pycryptopp.publickey.rsa.create_signing_key_from_string(open(private_key_filename).read())
    return rsa.sign(challenge)

def verify_signature(public_key, challenge, challenge_signature):
    """
    Return True if public_key verifies that challenge_signature signs challenge.
    Return False if simply doesn't match, None on other RSA failure. 
    """
    rsa_pub = pycryptopp.publickey.rsa.create_verifying_key_from_string(public_key)
    return rsa_pub.verify(challenge, challenge_signature)
    
def demo():
    def hex(s):
        return ' '.join('%02x' % ord(x) for x in s)
    
    private_key_filename = 'test.private'
    
    #
    # Server
    #
    private_key_password = 'foobar' # could be per installation, could try not to store on client
    
    #
    # Admin server:
    #    
    public_key = create_keypair(private_key_filename, private_key_password)
    print 'admin server created %s with public key %r' % (private_key_filename, hex(public_key)) 
    # Now _move_ pem to client
    
    #
    # Runtime server:
    #
    challenge_length = 7
    import random
    random_challenge = ''.join(chr(random.randrange(0, 256)) for x in range(challenge_length))
    print 'servers random_challenge %r' % hex(random_challenge)
    
    #
    # Client:
    #
    challenge_signature = sign_challenge(private_key_filename, private_key_password, random_challenge)
    print 'clients challenge_signature %r' % hex(challenge_signature)
    
    #
    # Runtime server:
    #
    print 'validation', verify_signature(public_key, random_challenge, challenge_signature) 

if __name__ == '__main__':
    demo()
