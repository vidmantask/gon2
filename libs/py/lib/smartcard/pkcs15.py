"""
Lib for using a MSC as authentication factor.

Connects through GSI and filesystem and thus doesn't require any driver installed.

It is assumed that the G&D MW applet has been uploaded and the PKCS-15 structure created.
"""

import random
import hashlib

from common import *
import lib.cryptfacility
import asn1

F4 = "\x01\x00\x01" # 65537
KID = 0 # which of the 32 RSA keypairs to use
CHUNKSIZE = 0xF4 # for reading

# According to 2.9.1 Transfer format for the Public Key
PublicKeyTransfer = asn1.UntagMandatorySeq(asn1.TagRawString(0x81, 'public_modulus'),
                                           asn1.TagRawString(0x82, 'public_exponent'),
                                           )

class Pkcs15(object):
    """Object corresponding to the G&D MW applet on a Smart Card"""

    def __init__(self, apdu_handler):
        """
        Create connection to Smart Card and request its attention
        """
        #print 'pkcs15 init'
        self._apdu_handler = apdu_handler

        # PKCS #15 section 5.7.1 PKCS #15 application selection
        status, response = self._apdu_handler.command(0x00, 0xA4, 0x04, 0x0C,
                                               (0xA0, 0x00, 0x00, 0x00, 0x63, 0x50, 0x4B, 0x43, 0x53, 0x2D, 0x31, 0x35),
                                               None)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error selecting applet", status, response)

        # VERIFY password 5
        status, response = self._apdu_handler.command(0x00, 0x20, 0x00, 0x85,
                                               (0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0, 0, 0, 0, 0, 0, 0),
                                               None)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error verifying pin", status, response)

    def generate_key_pair(self, keylength=0x400):
        """
        Create new public/private key pair on Smart Card

        0x400=1024 takes 5-10s, avg 8
        0x600=1538 takes 7-25s, avg 12
        0x800=2048 takes 10-60s, avg 30
        """
        # DELETE KEY OBJECT, private
        status, response = self._apdu_handler.command(0x80, 0xEA, 0x00, 0x01,
                                               (0x80 | KID,),
                                               None)
        if status == 0x6A88: pass # OK, private key object didn't exist
        elif status != 0x9000: pass # raise SmartCardExceptionApduResponse("Error deleting private key object", status, response)

        # DELETE KEY OBJECT, public
        status, response = self._apdu_handler.command(0x80, 0xEA, 0x00, 0x02,
                                               (0x80 | KID,),
                                               None)
        if status == 0x6A88: pass # OK, public key object didn't exist
        elif status != 0x9000: pass # raise SmartCardExceptionApduResponse("Error deleting public key object", status, response)

        # CREATE KEY OBJECT, private, access xxx
        status, response = self._apdu_handler.command(0x80, 0x40, 0x00, 0x01,
                                               (0x80 | KID, keylength >> 8, keylength & 0xff, 0x3f, 0x85, 0x38, 0x84),
                                               None)
        if status == 0x6985: pass # OK, private key object already exists
        elif status != 0x9000: raise SmartCardExceptionApduResponse("Error creating private key object", status, response)

        # CREATE KEY OBJECT, public, keylength 0x400, access xxx
        status, response = self._apdu_handler.command(0x80, 0x40, 0x00, 0x02,
                                               (0x80 | KID, keylength >> 8, keylength & 0xff, 0x07, 0xaa),
                                               None)
        if status == 0x6985: pass # OK, public key object already exists
        elif status != 0x9000: raise SmartCardExceptionApduResponse("Error creating public key object", status, response)

        # MANAGE SECURITY ENVIRONMENT (SET) for RSA Keypair Generation
        status, response = self._apdu_handler.command(0x00, 0x22, 0x41, 0xb6,
                                               (0x84, 0x03, 0x80, KID, 0x00, 0x89, 0x01, 0x41),
                                               None)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error managing security environment", status, response)

        # GENERATE ASYMMETRIC KEYPAIR
        status, response = self._apdu_handler.command(0x00, 0x46, 0x00, 0x00,
                                               None,
                                               None,
                                               timeout=30 if keylength <= 1024 else 60 if keylength <= 1538 else 120)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error generating asymmetric key pair", status, response)

    def write_file(self, data, filenumber):
        """
        Write the byte sequence to file 0x47fn where fn is filenumber.
        Files must be created with a hardcoded max size on card initialization.
        """
        status, response = self._apdu_handler.command(0x00, 0xA4, 0x02, 0x04,
                                                      (0x47, filenumber),
                                                      0)
        if status >> 8 == 0x61: # seen occasionally
            status, response = self._apdu_handler.command(0x00, 0xc0, 0x00, 0x00,
                                                          None,
                                                          status & 0xff)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error selecting file %s" % filenumber, status, response)
        fcp = asn1.FileControlParameter.decode(response)
        #print fcp
        fcp_size = fcp['seq']['size']

        CHUNKSIZE = 0xF4
        data = tuple(data)
        all_data = (len(data) >> 8, len(data) & 0xff,) + data
        assert len(all_data) <= fcp_size, 'File size is %s - not room for %s encoded bytes' % (fcp_size, len(all_data))
        i = 0
        while i < len(all_data):
            this_chunk = all_data[i : i + CHUNKSIZE]
            status, response = self._apdu_handler.command(0x00, 0xD6, i >> 8, i & 0xff,
                                                          this_chunk,
                                                          None)
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error writing to file", status, response)
            i += len(this_chunk)

    def read_file(self, filenumber):
        """
        Read a byte sequence from file 0x47fn where fn is filenumber.
        """
        #print 'pkcs15 read file %s' % filenumber
        status, response = self._apdu_handler.command(0x00, 0xA4, 0x02, 0x04,
                                                      (0x47, filenumber),
                                                      0)
        if status >> 8 == 0x61: # seen occasionally
            status, response = self._apdu_handler.command(0x00, 0xc0, 0x00, 0x00,
                                                          None,
                                                          status & 0xff)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Selecting file", status, response)
        fcp = asn1.FileControlParameter.decode(response)
        fcp_size = fcp['seq']['size']

        status, response = self._apdu_handler.command(0x00, 0xb0, 0, 0,
                                                      None,
                                                      2)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Reading file", status, response)
        assert len(response) == 2, 'Error reading length from file'
        size = (response[0] << 8) + response[1]
        assert size + 2 <= fcp_size, 'Encoded length %s too large for file of size %s' % (size, fcp_size)
        data = []
        i = 2
        while len(data) < size:
            status, response = self._apdu_handler.command(0x00, 0xb0, i >> 8, i & 0xff,
                                                          None,
                                                          min(CHUNKSIZE, size - len(data)))
            if status != 0x9000: raise SmartCardExceptionApduResponse("Reading file", status, response)
            data += response
            i += len(response)

        assert len(data) == size, "Expected to read %s bytes but only got %s" % (size, len(data))
        return data

    def get_public_key(self):
        """
        Return public key from Smart Card - that is an identifier of the Smart Card, and the card can prove that it has the corresponding private key

        Returns mod as integer encoded as byte-string - exp is assumed to be F4
        """
        #print 'pkcs15 get public key'
        # We assume file 0xef99 exists
        # EXPORT PUBLIC RSA KEY
        status, response = self._apdu_handler.command(0x80, 0x50, 0x80 | KID, 0x00,
                                               (0xef, 0x99),
                                               None)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error exporting public key to transfer EF", status, response)

        # SELECT FILE
        status, response = self._apdu_handler.command(0x00, 0xa4, 0x02, 0x04,
                                                      (0xef, 0x99),
                                                      0)
        if status >> 8 == 0x61: # seen occasionally
            status, response = self._apdu_handler.command(0x00, 0xc0, 0x00, 0x00,
                                                          None,
                                                          status & 0xff)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error selecting transfer EF", status, response)
        fcp = asn1.FileControlParameter.decode(response)
        fcp_size = fcp['seq']['size']

        # READ BINARY
        data = []
        while len(data) < fcp_size:
            status, response = self._apdu_handler.command(0x00, 0xb0, len(data) >> 8, len(data) & 0xff,
                                                          None,
                                                          min(CHUNKSIZE, fcp_size - len(data)))
            if status != 0x9000: raise SmartCardExceptionApduResponse("Error reading public key from EF", status, response)
            data += response

        keys = PublicKeyTransfer.decode(data)
        #print keys

        if keys['public_exponent'] != F4: 
            raise SmartCardException("Invalid public exponent %r" % keys['public_exponent'])
        # print to_hex(keys['public_modulus'])
        return keys['public_modulus']


    def create_challenge_signature(self, challenge):
        """
        Let Smart Card create a signature of the challange. The signature can be validated with the public key.
        """
        # MANAGE SECURITY ENVIRONMENT (SET) for INTERNAL AUTHENTICATE
        status, response = self._apdu_handler.command(0x00, 0x22, 0x41, 0xa4,
                                               (0x84, 0x03, 0x80, KID, 0x00, 0x89, 0x02, 0x023, 0x13),
                                               None)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error managing for internal authenticate", status, response)

        # SmartCard just performs rfc 2313 step 10.1.3 - we must hash and wrap its input with
        # DigestInfo from RFC 2313 "10.1.2 Data encoding"
        #   0   33: SEQUENCE {
        #   2    9:   SEQUENCE {
        #   4    5:     OBJECT IDENTIFIER sha1 (1 3 14 3 2 26)
        #  11    0:     NULL
        #         :     }
        #  13   20:   OCTET STRING 'xxxxxxxxxxxxxxxxxxxx'
        #         :   }
        challenge_hash = hashlib.sha1(challenge).digest()
        assert len(challenge_hash) == 0x14
        challenge_hash_pad = '\x30\x21\x30\x09\x06\x05\x2b\x0e\x03\x02\x1a\x05\x00\x04\x14' + challenge_hash

        # INTERNAL AUTHENTICATE
        status, response = self._apdu_handler.command(0x00, 0x88, 0x00, 0x00,
                                               tuple(ord(c) for c in challenge_hash_pad),
                                               0)
        if status & 0xff00 == 0x6100: # Command successfully executed; 'xx' bytes of data area available and can be requsted using GET RESPONSE
            # It seems like T=0 mode. Well ... get response with GET RESPONSE
            status, response = self._apdu_handler.command(0x00, 0xc0, 0x00, 0x00,
                                                   None,
                                                   status & 0xff)
        if status != 0x9000: raise SmartCardExceptionApduResponse("Error in internal authenticate", status, response)
        challenge_signature = ''.join(chr(x) for x in response)

        # print to_hex(challenge_signature)
        return challenge_signature


    def verify(self, public_key, challenge=None, challenge_length=17):
        """
        Verify that Smart Card has private key matching public key as read with .get_public_key
        """
        if not challenge:
            challenge = ''.join(chr(random.randrange(0, 256)) for x in range(challenge_length))

        challenge_signature = self.create_challenge_signature(challenge)

        # Crypto++ does hashing and padding - hopefully it matches what we and the Smart Card has done...
        verified = lib.cryptfacility.verify_public_key_signature(public_key, F4, challenge, challenge_signature)
        return verified

    @staticmethod
    def verify_signature(public_key, challenge, challenge_signature):
        """
        Verify that Smart Card has private key matching public key as read with .get_public_key
        """
        # Crypto++ does hashing and padding - hopefully it matches what we and the Smart Card has done...
        verified = lib.cryptfacility.verify_public_key_signature(public_key, F4, challenge, challenge_signature)
        return verified
