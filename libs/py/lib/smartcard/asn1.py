"""
Simple parser for ASN.1
Based on http://en.wikipedia.org/wiki/Basic_Encoding_Rules and ITU-T X.690 / ISO/IEC 8825-1
"""

class ObjectDict(object):
    """
    Allows record-like dot-access to a dict
    """
    def __init__(self, val):
        if not isinstance(val, dict):
            val = dict(val)
        self.__dict__ = val

    __getitem__ = object.__getattribute__ 

def guessTag(t, name):
    """
    Guess tag type according to ASN.1 standard/guidelines
    """
    name = '?%s?' % name
    if t == -1:
        return TagEncodingError(t, name)
    if t & 0x1f == 0x1f:
        print 'two-byte tag format?'
    if t & 0x30: # Constructed data object
        return TagOptionalSeq(t, name)
    if t == 0x00:
        return TagEndOfContext(t, '0')
    if t == 0x01:
        return TagBoolean(t, name)
    if t == 0x02:
        return TagInteger(t, name)
    if t == 0x03:
        return TagBitString(t, name)
    if t == 0x04:
        return TagOctetString(t, name)
    if t == 0x05:
        return TagUnknown(t, name) # NULL
    if t == 0x06:
        return TagObjectIdentifier(t, name)
    if t == 0x07:
        return TagUnknown(t, name) # OBJECT DESCRIPTOR
    if t == 0x08:
        return TagUnknown(t, name) # EXTERNAL
    if t == 0x09:
        return TagUnknown(t, name) # REAL
    if t == 0x0a:
        return TagEnumerated(t, name)
    if t == 0x0a:
        return TagEnumerated(t, name)
    if t == 0x0c:
        return TagUnicodeString(t, name)
    return TagUnknown(t, '%s' % name)
            
class TagBase(object):
    """
    Base type for TLV attribute types
    """
    def __init__(self, tag, name):
        """
        tag is the T byte value, the V is stored under name
        """
        self.tag = tag
        self.name = name

class TagUnknown(TagBase):
    """
    Unknown TLV types
    """
    def decode(self, v):
        return '%s (%s)' % (''.join(chr(x) for x in v), ' '.join('%02x' % x for x in v)) 

class TagEncodingError(TagBase):
    """
    Zero tag
    """
    def decode(self, v):
        return 'T %02x says L %02x but max V is %02x: %s' % (v[0], v[1] if len(v) > 1 else -1, len(v) - 2,
                                                             ' '.join('%02x' % x for x in v))
class TagEndOfContext(TagBase):
    """
    Zero tag
    """
    def decode(self, v):
        assert v[0] == 0
        return v[1:] # strip zero, return rest

class TagBoolean(TagBase):
    """
    """
    def decode(self, v):
        assert len(v) == 1
        return bool(v[0])

class TagInteger(TagBase):
    """
    """
    def __init__(self, tag, name):
        TagBase.__init__(self, tag, name)

    def decode(self, v):
        i = 0
        for b in v:
            i = (i << 8) + b
        return i

class TagEnumerated(TagBase):
    """
    """
    def __init__(self, tag, name, **kwargs):
        TagBase.__init__(self, tag, name)
        self.enum_values = kwargs
        self.enum_names = dict((v, k) for k, v in self.enum_values.items())  

    def decode(self, v):
        i = 0
        for b in v:
            i = (i << 8) + b
        return self.enum_names.get(i, 'enum %s' % i)

TagReal = TagUnknown

class TagBitString(TagBase):
    """
    Bit String
    """
    def __init__(self, tag, name, **kwargs):
        TagBase.__init__(self, tag, name)
        self.bitname_values = kwargs
        self.bit_names = dict((v, k) for k, v in self.bitname_values.items())  

    def decode(self, v):
        result = set()
        bits = 8 * (len(v) - 1) - v[0]
        for i in xrange(bits):
            if v[(i >> 3) + 1] & (0x80 >> (i&7)):
                result.add(i)
                if i in self.bit_names:
                    result.add(self.bit_names[i])
        return result 
        
class TagUnicodeString(TagBase):
    """
    """
    def decode(self, v):
        s = ''.join(chr(x) for x in v)
        try:
            return s.decode('utf-8')
        except UnicodeDecodeError:
            raise Exception('UnicodeError decoding %02x.%s: %r' % (self.tag, self.name, s))
        
class TagOctetString(TagBase):
    """
    - actually a byte list
    """
    def decode(self, v):
        return v
        
class TagRawString(TagBase):
    """
    Binary safe
    """
    def decode(self, v):
        return ''.join(chr(x) for x in v)

TagPrintableString = TagRawString 
TagVisibleString = TagRawString

class TagObjectIdentifier(TagBase):
    """
    Apparently an OID path ...
    """
    def __init__(self, tag, name):
        TagBase.__init__(self, tag, name)

    def decode(self, v):
        l = []
        tmp = 0
        assert not v or v[-1] & 0x80 == 0, 'Invalid ObjectIdentifier; bit 8 in last byte %02x set' % v[-1]
        for byte in v:
            tmp = (tmp<<7) + (byte & 0xf7) 
            if byte & 0x80 == 0:
                l.append(tmp)
                tmp = 0
        assert not tmp, 'Her kommer vi da aldrig'
        return l
        
class TagOptionalSeq(TagBase):
    """
    Tagged container for any number of tags which may come in any order
    """
    def __init__(self, tag, name, *args, **kwargs):
        TagBase.__init__(self, tag, name)
        self.seq = UntagOptionalSeq(*args, **kwargs)

    def decode(self, v):
        return self.seq.decode(v)
    
class UntagOptionalSeq(object):
    """
    Untagged container for any number of tags which may come in any order
    """
    def __init__(self, *args, **kwargs):
        self.as_list = kwargs.get('as_list', False) or not kwargs.get('as_dict', True)
        self.types = {}
        for tag_type in args:
            self.types[tag_type.tag] = tag_type
    
    def decode(self, seq):
        """Data after 0 will be returned with '0' key"""
        res = []
        i = 0
        while i < len(seq):
            t, total_l, v = eat_TLV(seq, i)
            tag_type = self.types.get(t) 
            if not tag_type:
                tag_type = guessTag(t, '%02x@%04x' % (t, i))
            res.append((tag_type.name, tag_type.decode(v)))
            i += total_l
        assert i == len(seq), 'Read %s of seq of len %s' %(i, len(seq))
        if self.as_list:
            return res
        res_dict = dict(res)
        if len(res_dict) != len(res):
            print 'Duplicate tags found when converting %s to dict' % [n for n,v in res]
        return res_dict

class TagMandatorySeq(TagBase):
    """
    Tagged container for an exact sequence of tagged values
    """
    def __init__(self, tag, name, *args, **kwargs):
        TagBase.__init__(self, tag, name)
        self.seq = UntagMandatorySeq(*args, **kwargs)

    def decode(self, v):
        return self.seq.decode(v)
    
class UntagMandatorySeq(object):
    """
    Untagged container for an exact sequence of tagged values
    """
    def __init__(self, *args, **kwargs):
        self.as_list = kwargs.get('as_list', False) or not kwargs.get('as_dict', True)
        self.types = args
    
    def decode(self, seq):
        """Extra data will be returned with '' key"""
        res = []
        i = 0
        for tag_type in self.types:
            assert i < len(seq), 'Only first %s of %s mandatory tags found' % (len(res), len(self.types))
            t, total_l, v = eat_TLV(seq, i)
            if t != tag_type.tag:
                print 'Unknown %02x when expected %02x' % (t, tag_type.tag)
                tag_type = guessTag(t, tag_type.name)
            value = tag_type.decode(v)
            res.append((tag_type.name, value))
            i += total_l
        if i < len(seq):
            res.append(('', TagUnknown(0, '').decode(seq[i:])))
        if self.as_list:
            return res
        res_dict = dict(res)
        assert len(res_dict) == len(res), 'Duplicate tags found when converting to dict'
        return res_dict

class Single(object):
    """
    Exactly one tagged value
    """
    def __init__(self, single):
        self.single = single

    def decode(self, seq):
        t, total_l, v = eat_TLV(seq, 0)
        assert t == self.single.tag, 'Expected tag %02x got %02x' % (self.single.tag, t)
        if total_l != len(seq): print 'Only used %02x of %02x bytes; unused: %s' % (total_l, len(seq), ' '.join('%02x' % x for x in seq[total_l:])) 
        return self.single.decode(v)
    

def eat_TLV(seq, start_index):
    """
    Eat some TLV from seq starting at start_index, decode with types and return tag byte, eaten length, and value seq
    Return tag -1 and actual length if not enough available.
    """
    tag = seq[start_index + 0]
    if tag == 0: # FIXME: How to handle trailing zeroes?
        return (0, len(seq) - start_index, seq[start_index:]) # Eat and return rest of seq - including "T" and "L" 
    if len(seq) < start_index + 2: # FIXME: How to handle trailing zeroes?
        return (-1, len(seq) - start_index, seq[start_index:]) # Eat and return rest of seq - including "T" and "L" 
    expected_l = seq[start_index + 1]
    if expected_l < 0x80: # ASN.1 BER/DER Length coding in 1 Byte
        skip = 2
    elif expected_l == 0x81: # ASN.1 BER/DER Length coding in 2 Bytes = 0x82
        expected_l = seq[start_index + 2]
        skip = 3
    elif expected_l == 0x82: # ASN.1 BER/DER Length coding in 3 Bytes = 0x82
        expected_l = (seq[start_index + 2] << 8) + seq[start_index + 3]
        skip = 4
    else:
        raise AssertionError('Bogus length %02x' % expected_l)
    value_seq = seq[start_index + skip : start_index + skip + expected_l]
    #print 't=%02x' % t, 'l=%04x' % l, 'v=%r' % v
    if len(value_seq) != expected_l:
        print 'Expected value of len %s got %s' % (expected_l, len(value_seq))
        return (0, len(seq) - start_index, seq[start_index:]) # Eat and return rest of seq - including "T" and "L" 
    return (tag, skip + len(value_seq), value_seq)


# 7816-4 table 12
FileControlParameter = UntagMandatorySeq(TagMandatorySeq(0x62, 'seq',
                                                         TagInteger(0x80, 'size'), 
                                                         TagInteger(0x82, 'descriptor'),
                                                         ))

# What is this? Almost same as FCP?
FileControlInformation = UntagMandatorySeq(TagMandatorySeq(0x6f, 'seq', 
                                                           TagInteger(0x80, 'size'), 
                                                           TagInteger(0x82, 'descriptor'),
                                                           ))

def demo():
    from pprint import pprint
    
    
    fci_enc = [98, 7, 128, 2, 1, 44, 130, 1, 1]
    d = FileControlParameter.decode(fci_enc)
    pprint( d)

    print
    print 'FCI'
    fci_enc = [0x6f, 0x07, 0x80, 0x02, 0x00, 0x75, 0x82, 0x01, 0x01]
    d = FileControlInformation.decode(fci_enc)
    pprint( d)
    pprint( dict(d))

    print
    print 'PublicKey'
    PublicKeyTransfer = UntagOptionalSeq(TagOctetString(0x81, 'public_modulus'), 
                                         TagOctetString(0x82, 'public_exponent'),
                                         )
    
    pk_enc = [0x81, 0x82, 0x00, 0x3, 0x01, 0x00, 0x01, 0x82, 0x3, 0x01, 0x00, 0x01, 0]
    d = PublicKeyTransfer.decode(pk_enc)
    pprint(d)
    v = ObjectDict(d)
    print repr(v.public_modulus)
    print repr(v.public_exponent)
    print repr(v['0'])

    print
    print 'TokenInfo'
    TokenInfo = UntagOptionalSeq(TagOptionalSeq(0x30, 'seq',
        TagInteger(0x02, 'version'),
        TagOctetString(0x04, 'serialNumber'), # univ.OctetString()),
        TagUnicodeString(0x0c, 'manufacturerID'), # Label()),
        TagUnicodeString(0x80, 'label'), # Label()
        TagBitString(0x03, 'tokenflags', readonly=0, loginRequired=1, prnGeneration=2, eidCompliant=3), # TokenFlags()),
        TagUnknown(-1, 'seInfo'), # SecurityEnvironmentInfo()), # FIXME: SEQUENCE OF SecurityEnvironmentInfo???
        TagUnknown(0x81, 'recordInfo'), # RecordInfo()
        TagUnknown(-1, 'supportedAlgorithms'), # AlgorithmInfo() # FIXME: SEQUENCE OF AlgorithmInfo
        #...,
        TagUnicodeString(0x83, 'issuerId'), # Label()
        TagUnicodeString(0x84, 'holderId'), # Label()
        TagUnknown(0x85, 'lastUpdate'), # LastUpdate()
        TagUnicodeString(-1, 'preferredLanguage'), # PrintableString()
                                    ))
    x = [0x30, 0x73, 0x02, 0x01, 0x00, 0x04, 0x08, 0x20, 0x90, 0x70, 0x07, 0x00, 0x11, 0x0d, 0x0f, 0x0c, 0x12, 0x41, 0x2e, 0x45, 0x2e, 0x54, 0x2e, 0x20, 0x45, 0x75, 0x72, 0x6f, 0x70, 0x65, 0x20, 0x42, 0x2e, 0x56, 0x2e, 0x80, 0x20, 0x57, 0x68, 0x69, 0x74, 0x65, 0x20, 0x54, 0x65, 0x73, 0x74, 0x20, 0x43, 0x61, 0x72, 0x64, 0x20, 0x4f, 0x6e, 0x65, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x03, 0x02, 0x04, 0x50, 0x83, 0x2a, 0x43, 0x6f, 0x70, 0x79, 0x72, 0x69, 0x67, 0x68, 0x74, 0x20, 0x28, 0x63, 0x29, 0x20, 0x31, 0x39, 0x39, 0x37, 0x2d, 0x32, 0x30, 0x30, 0x36, 0x20, 0x41, 0x2e, 0x45, 0x2e, 0x54, 0x2e, 0x20, 0x45, 0x75, 0x72, 0x6f, 0x70, 0x65, 0x20, 0x42, 0x2e, 0x56, 0x2e]
    pprint( TokenInfo.decode(x))


    if False:
        token_info = TokenInfo()
        token_info.setComponentByName('version', 0)
        token_info.setComponentByName('serialNumber', ' \x90p\x07\x00\x11\r\x0f')
        token_info.setComponentByName('tokenflags', (0,1,0,1))
#        token_info.setComponentByName('tokenflags', TokenFlags().clone('loginRequired,eidCompliant'))
        token_info.setComponentByName('manufacturerID', 'A.E.T. Europe B.V.')
        token_info.setComponentByName('label', 'White Test Card One             ')
        token_info.setComponentByName('issuerId', 'Copyright (c) 1997-2006 A.E.T. Europe B.V.')
    

    print
    print 'XX'
    XX = UntagOptionalSeq(TagOptionalSeq(0x30, 'seq',
                                         TagOptionalSeq(0x30, 'seq'),
                                         ))
    test1 = (
         '0\x110\x0c\x04\x02C\x00\x02\x02\x10\xa6\x80\x02\x11\x07\x04\x01\x820\x100\x0b\x04\x02C\x01\x02\x01\x00\x80\x02\x00\xea\x04\x01\x82\x000\x100\x0b\x04\x02C\x01\x02\x01\x00\x80\x02\x00\xea\x04\x01\x82\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
         )
    #pprint( XX.decode([ord(c) for c in test1]))

    print
    print 'Cert'
    Cert = UntagOptionalSeq(TagOptionalSeq(0x30, 'topseq',
                                  TagUnicodeString(0xc2, 'someName'),
                                  TagOctetString(0x04, 'someGUID'),
                                  TagOptionalSeq(0x30, 'seqtwo',
                                                TagUnicodeString(0x0c, 'issuedesc'),
                                                )
                                                ))
    test2 = (
         "0\x81\xc20-\x0c$Mads Kiilerich's Giritech Root CA ID\x03\x02\x06@\x04\x01\x820&\x04$1ce7f113-43cd-4447-bb73-ec5877461100\xa1i0g0\x0b\x04\x02C\x00\x02\x01\x00\x80\x02\x06\x0e\xa0L0J1\x130\x11\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16"
         +
         "\x03com1\x180\x16\x06\n\t\x92&\x89\x93\xf2,d\x01\x19\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech Root CA\x02\n!\x91\xed\xfd\x00\x00\x00\x00\x00\x120\x81\x9801\x0c(Mads Kristian Kiilerich's TDC OCES CA ID\x03\x02\x06@\x04\x01\x820\x16\x04\x14{"
         +
         '\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x06\x0e\x80\x02\x05{\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04Eq\x1bc0~0*\x0c!TDC OCES CA issued by TDC '
         +
         'OCES CA\x03\x02\x06@\x04\x01\x820\x03\x04\x01\x00\xa1K0I0\x0c\x04\x02C\x00\x02\x02\x0b\x89\x80\x02\x05\x1d\xa03011\x0b0\t\x06\x03U\x04\x06\x13\x02DK1\x0c0\n\x06\x03U\x04\n\x13\x03TDC1\x140\x12\x06\x03U\x04\x03\x13\x0bTDC OCES CA\x02\x04>H\xbd\xc4\x00\x16\x08giritech1\x190\x17\x06\x03U\x04\x03\x13\x10Giritech R'
         +
         '0t0-\x0c$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x02\x06\xc0\x04\x01\x8203\x04$1a3f5778-b259-4cae-876e-9eed496c7a80\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x84\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$b3da'
         +
         '7a2d-241b-486e-b9d1-d14d10437d2b\x03\x02\x06\xc0\x04\x01\x8203\x04$b3da7a2d-241b-486e-b9d1-d14d10437d2b\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x85\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000t0-\x0c$Mads Kiilerich'
         +
         "'s Giritech Root CA ID\x03\x02\x06\xc0\x04\x01\x8203\x04$1ce7f113-43cd-4447-bb73-ec5877461100\x03\x03\x06t\x00\x03\x02\x03\xb8\x02\x02\x00\x86\xa0\x020\x00\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x000\x81\xeb01\x0c(Mads Kristian Kiilerich"
         +
         "'s TDC OCES CA ID\x03\x02\x06\xc0\x04\x01\x820#\x04\x14{\xb8~\xd9,\xc6\x08\x8f\xd1\x83\xf9\x17Qb\x12!\xb9U\xe4\xa1\x03\x03\x06t\x00\x03\x02\x03\x80\x02\x02\x00\x87\xa0\x81\x840\x81\x810\x7f1\x0b0\t\x06\x03U\x04\x06\x13\x02DK1)0'\x06\x03U\x04\n\x13 Ingen organisatorisk tilknytning1E0"
         +
         '\x1e\x06\x03U\x04\x03\x13\x17Mads Kristian Kiilerich0#\x06\x03U\x04\x05\x13\x1cPID:9208-2002-2-554999497797\xa1\n0\x080\x02\x04\x00\x02\x02\x04\x00\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
         )
    #pprint( Cert.decode([ord(c) for c in test2]))

    print
    print 'Foo'
    Foo = UntagOptionalSeq(TagOptionalSeq(0x30, 'topseq',
                                  TagUnicodeString(0xc2, 'someName'),
                                  TagOctetString(0x04, 'someGUID'),
                                  TagOptionalSeq(0x30, 'seqtwo',
                                                TagOctetString(0x0c, 'pindesc'),
                                                )
                                                ))
    test3 = (
    '\xa0>0\x16\x0c\x10User FingerPrint\x03\x02\x06\xc00\x03\x04\x01\x83\xa1\x1f0\x1d\x03\x03\x07@\x80\x06\x02(\x000\x06\n\x01\x01\n\x01\x01\x02\x02\x00\x010\x06\x04\x04?\x00P\x150J0\x11\x0c\x08User Pin\x03\x02\x06\xc0\x04\x01\x830\x03\x04\x01\x82\xa100.\x03\x03\x04\xcc\x90\n\x01\x01\x02\x01\x04\x02\x01\x08\x80\x02\x00\x82\x04\x01\x00\x18\x0f20080408112'
    +
    '059Z0\x06\x04\x04?\x00P\x150E0\x0c\x0c\x06SO Pin\x03\x02\x06\xc00\x03\x04\x01\x83\xa100.\x03\x03\x04\xcf\x90\n\x01\x01\x02\x01\x04\x02\x01\x08\x80\x02\x00\x83\x04\x01\x00\x18\x0f20080408140609Z0\x06\x04\x04?\x00P\x15\x00\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff'
    )
    #pprint( Foo.decode([ord(c) for c in test3]))


if __name__ == '__main__':
    demo()
