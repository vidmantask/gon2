r"""
Headers / APIs for windows system calls

\Python25\Scripts\h2xml.py "C:\Program Files\Microsoft Platform SDK\Include\Windows.h" -o Windows.xml
\Python25\Scripts\xml2py.py Windows.xml -o win_h.py -w -d -s CreateFileW -s WriteFile -s SetFilePointer -s VirtualAlloc -s ReadFile -s VirtualFree -s CloseHandle
"""


from ctypes import *

from ctypes.wintypes import DWORD
_stdcall_libraries = {}
_stdcall_libraries['kernel32'] = WinDLL('kernel32')
from ctypes.wintypes import BOOL
from ctypes.wintypes import HANDLE
from ctypes.wintypes import LONG
from ctypes.wintypes import LPCWSTR


LPVOID = c_void_p
ULONG_PTR = POINTER(c_ulong)
SIZE_T = ULONG_PTR

VirtualAlloc = _stdcall_libraries['kernel32'].VirtualAlloc
VirtualAlloc.restype = LPVOID
VirtualAlloc.argtypes = [LPVOID, SIZE_T, DWORD, DWORD]
VirtualAlloc.__doc__ = \
"""LPVOID VirtualAlloc(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:1833"""

VirtualFree = _stdcall_libraries['kernel32'].VirtualFree
VirtualFree.restype = BOOL
VirtualFree.argtypes = [LPVOID, SIZE_T, DWORD]
VirtualFree.__doc__ = \
"""BOOL VirtualFree(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:1842"""

LPCVOID = c_void_p
LPDWORD = POINTER(DWORD)
class _OVERLAPPED(Structure):
    pass
LPOVERLAPPED = POINTER(_OVERLAPPED)
WriteFile = _stdcall_libraries['kernel32'].WriteFile
WriteFile.restype = BOOL
WriteFile.argtypes = [HANDLE, LPCVOID, DWORD, LPDWORD, LPOVERLAPPED]
WriteFile.__doc__ = \
"""BOOL WriteFile(HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:3206"""

ReadFile = _stdcall_libraries['kernel32'].ReadFile
ReadFile.restype = BOOL
ReadFile.argtypes = [HANDLE, LPVOID, DWORD, LPDWORD, LPOVERLAPPED]
ReadFile.__doc__ = \
"""BOOL ReadFile(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:3217"""

PLONG = POINTER(LONG)
SetFilePointer = _stdcall_libraries['kernel32'].SetFilePointer
SetFilePointer.restype = DWORD
SetFilePointer.argtypes = [HANDLE, LONG, PLONG, DWORD]
SetFilePointer.__doc__ = \
"""DWORD SetFilePointer(HANDLE hFile, LONG lDistanceToMove, PLONG lpDistanceToMoveHigh, DWORD dwMoveMethod)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:3285"""

class _SECURITY_ATTRIBUTES(Structure):
    pass
LPSECURITY_ATTRIBUTES = POINTER(_SECURITY_ATTRIBUTES)
CreateFileW = _stdcall_libraries['kernel32'].CreateFileW
CreateFileW.restype = HANDLE
CreateFileW.argtypes = [LPCWSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE]
CreateFileW.__doc__ = \
"""HANDLE CreateFileW(LPCWSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:6603"""

CloseHandle = _stdcall_libraries['kernel32'].CloseHandle
CloseHandle.restype = BOOL
CloseHandle.argtypes = [HANDLE]
CloseHandle.__doc__ = \
"""BOOL CloseHandle(HANDLE hObject)
C:/Program Files/Microsoft Visual Studio .NET 2003/Vc7/PlatformSDK/Include/winbase.h:3357"""

class N11_OVERLAPPED4DOLLAR_48E(Union):
    pass
class N11_OVERLAPPED4DOLLAR_484DOLLAR_49E(Structure):
    pass
N11_OVERLAPPED4DOLLAR_484DOLLAR_49E._fields_ = [
    ('Offset', DWORD),
    ('OffsetHigh', DWORD),
]
PVOID = c_void_p
N11_OVERLAPPED4DOLLAR_48E._anonymous_ = ['_0']
N11_OVERLAPPED4DOLLAR_48E._fields_ = [
    ('_0', N11_OVERLAPPED4DOLLAR_484DOLLAR_49E),
    ('Pointer', PVOID),
]
_OVERLAPPED._anonymous_ = ['_0']
_OVERLAPPED._fields_ = [
    ('Internal', ULONG_PTR),
    ('InternalHigh', ULONG_PTR),
    ('_0', N11_OVERLAPPED4DOLLAR_48E),
    ('hEvent', HANDLE),
]
_SECURITY_ATTRIBUTES._fields_ = [
    ('nLength', DWORD),
    ('lpSecurityDescriptor', LPVOID),
    ('bInheritHandle', BOOL),
]
__all__ = ['_OVERLAPPED', 'LPOVERLAPPED', 'LPSECURITY_ATTRIBUTES',
           'N11_OVERLAPPED4DOLLAR_484DOLLAR_49E', 'LPCVOID',
           'SetFilePointer', 'LPVOID', 'N11_OVERLAPPED4DOLLAR_48E',
           'ReadFile', 'LPDWORD', 'VirtualFree', 'WriteFile',
           '_SECURITY_ATTRIBUTES', 'CreateFileW', 'SIZE_T', 'PVOID',
           'VirtualAlloc', 'ULONG_PTR', 'PLONG', 'CloseHandle']
