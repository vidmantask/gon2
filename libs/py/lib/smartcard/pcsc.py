"""
APDU Communication with SmartCard through pyscard based PC/SC interface

We use the low-level interface very close to plain PC/SC or winscard.dll.
"""

from __future__ import with_statement

try:
    from smartcard import scard # pyscard
    hresult_to_name = dict((y,x) for (x,y) in scard.__dict__.items() if isinstance(y, int))
except:
    # Will throw smartcard.pcsc.PCSCExceptions.EstablishContextException on windows if connected through Remote Desktop
    # - but there is no way to get that type, and it doesn't inherit from Exception ... 
    scard = None
    hresult_to_name = {}
import sys
import os
if sys.platform == 'darwin' and os.uname()[2].startswith('8.'):
    # Smart Card doesn't work and isn't supported on 10.4 ... for now
    scard = None 

from common import *

class SmartCardExceptionSCard(SmartCardException):
    def __init__(self, msg, hresult):
        self.msg = msg
        self.hresult = hresult
        self.hresult_name = hresult_to_name.get(hresult, 'UNKNOWN_%s' % hresult)
        self.hresult_desc = scard.SCardGetErrorMessage(hresult)
        SmartCardException.__init__(self, "%s: %s, %s" % (msg, self.hresult_name, self.hresult_desc))

class SmartCardExceptionSCardFunctionalityNotAvailable(SmartCardException):
    '''
    Smart card module not available
    Known reasons include missing pyscard, unsupported 10.4, remote desktop connection
    '''
    def __init__(self):
        SmartCardException.__init__(self, "Smart Card functionality not available")
    
class PcSc(object):

    def __init__(self):
        """
        May throw SmartCardExceptionSCardFunctionalityNotAvailable
        """
        self.hcontext = None
        if not bool(scard):
            raise SmartCardExceptionSCardFunctionalityNotAvailable()

    def __enter__(self):
        assert not self.hcontext
        (hresult, self.hcontext) = scard.SCardEstablishContext(scard.SCARD_SCOPE_USER)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('Failed to establish context', hresult)
        return self

    def __exit__(self, _type, _value, _traceback):
        if self.hcontext:
            hresult = scard.SCardReleaseContext(self.hcontext)
            if hresult != scard.SCARD_S_SUCCESS:
                raise SmartCardExceptionSCard('failed to release context', hresult)
            self.hcontext = None

    def get_readers(self):
        if self.hcontext:
            (hresult, readers) = scard.SCardListReaders(self.hcontext, [])
            if hresult != scard.SCARD_S_SUCCESS:
                raise SmartCardExceptionSCard('Failed to list readers', hresult)
            return readers
        return []

    def get_connected_transaction(self, readername, T=None):
        assert self.hcontext
        return _PcScConnected(self.hcontext, readername, T=T)


class _PcScConnected(object):
    def __init__(self, hcontext, readername, T):
        scard_protocol = (scard.SCARD_PROTOCOL_T0 | scard.SCARD_PROTOCOL_T1 ) if T is None else scard.SCARD_PROTOCOL_T1 if T == 1 else scard.SCARD_PROTOCOL_T0
        self._entered = False

        
        (hresult, self.hcard, _dwActiveProtocol) = scard.SCardConnect(hcontext,
                                                                      readername,
                                                                      scard.SCARD_SHARE_SHARED,
                                                                      scard_protocol)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('unable to connect', hresult)

        # Just testing - not necessary ...
        (hresult, _reader, _state, detected_protocol, _atr) = scard.SCardStatus(self.hcard)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('failed to get status', hresult)
        #print 'Reader: ', reader
        #print 'State: ', _state
        #print 'Protocol: ', detected_protocol
        #print 'ATR: ',
        #for i in xrange(len(atr)):
        #    print '0x%.2X' % i,
        #print
        self._T = 1 if detected_protocol == scard.SCARD_PROTOCOL_T1 else 0

    def __enter__(self):
        assert self.hcard
        assert not self._entered

        hresult = scard.SCardBeginTransaction(self.hcard)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('failed to begin transaction', hresult)
        self._entered = True
        return self

    def __exit__(self, _type, _value, _traceback):
        assert self.hcard
        assert self._entered
        hresult = scard.SCardEndTransaction(self.hcard, scard.SCARD_LEAVE_CARD)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('failed to end transaction', hresult)

        hresult = scard.SCardDisconnect(self.hcard, scard.SCARD_UNPOWER_CARD)
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('failed to disconnect', hresult)

        self.hcard = None
        self._entered = False

    def command(self, cla, ins, p1, p2, command_data, expected_response_length, debug_printer=None):
        """Encode APDU and send it over GSI interface. All parts are integer values.
        command_data is tuple. Length is automatically prepended. Whole field completely left out if empty or None.
        If expected_response_length is None then it is completely left out. (0 usually means 256.)
        Command can target Card Terminal or ICC (default).
        """
        assert self._entered
        command_apdu = (cla, ins, p1, p2)
        if command_data:
            assert len(command_data) <= 255, 'command data too long'
            command_apdu += (len(command_data),) + command_data
        if not expected_response_length is None:
            command_apdu += (expected_response_length,)
        if debug_printer:
            debug_printer('Sent Command APDU:')
            debug_printer(dump_list(command_apdu))

        #print 'Command:', dump_list(command_apdu)
        (hresult, response) = scard.SCardTransmit(self.hcard, 
                                                  scard.SCARD_PCI_T1 if self._T == 1 else scard.SCARD_PCI_T0, 
                                                  list(command_apdu))
        if hresult != scard.SCARD_S_SUCCESS:
            raise SmartCardExceptionSCard('Failed to transmit', hresult)
        #print 'Response:', dump_list(response)

        response_apdu = response
        # decode response
        if debug_printer:
            debug_printer('Got Response APDU:')
            debug_printer(dump_list(response_apdu))

        assert len(response_apdu) >= 2 # checked above
        response_data = response_apdu[:-2]
        status = (response_apdu[-2] << 8) + response_apdu[-1]

        return status, response_data
