
class SmartCardException(Exception): pass

class SmartCardExceptionApduResponse(SmartCardException):
    def __init__(self, msg, status, response):
        SmartCardException.__init__(self, '%s: status %04X, data %s' %
                                    (msg, status, ' '.join('%02x' % x for x in response) or None))

def to_hex(s, py=False):
    "Utility for converting a integer string to a hex-string"
    if py:
        return ', '.join('0x%02x' % ord(x) for x in s)
    return ' '.join('%02x' % ord(x) for x in s)

def from_hex(s):
    """
    Utility for converting hex_string to integer string - the reverse of to_hex
    """
    return ''.join(chr(int(x, 16)) for x in s.strip().split())

def dump_string(s, line_length=8):
    """Returns char-seq s as C-source-compatible hex-encoded ascii"""
    return ',\n'.join(', '.join('0x%02x' % ord(x) for x in s[line_length * i : line_length * (i + 1)])
                      for i in range((len(s) - 1) / line_length + 1))

def dump_list(l, line_length=8):
    """Returns byte-list s as C-source-compatible hex-encoded ascii"""
    return ',\n'.join(', '.join('0x%02x' % x for x in l[line_length * i : line_length * (i + 1)])
                      for i in range((len(l) - 1) / line_length + 1))
