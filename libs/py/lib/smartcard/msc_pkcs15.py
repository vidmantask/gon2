"""
Demo app for using a MSC as authentication factor.

Connects through GSI and filesystem and thus doesn't require any driver installed.

It is assumed that the G&D MW applet has been uploaded and the PKCS-15 structure created.
"""
from __future__ import with_statement

import sys
import time

import pkcs15
import msc

from common import *

def test(filename = "/media/disk/gon_deploy_smartcard/giritech.msc"):
    print 'Connecting on filename %r' % filename
    with msc.MSC(filename) as smartcard_transaction:
        msc_pkcs15 = pkcs15.Pkcs15(smartcard_transaction)
        print 'Generating key pair ...'
        msc_pkcs15.generate_key_pair()
        print 'Getting public key ...'
        mod = msc_pkcs15.get_public_key()
        print 'Public key:', to_hex(mod)
        print 'Verified:',
        print msc_pkcs15.verify(mod)

        for filenumber in [0, 1]:
            data1 = tuple(range(256))*4*4
            print 'Writing file %s with %s bytes' % (filenumber, len(data1))
            msc_pkcs15.write_file(data1, filenumber)
            print 'Reading'
            data2 = msc_pkcs15.read_file(filenumber)
            assert tuple(data1) == tuple(data2), '1:\n' + dump_list(data1) + '\n2:\n' + dump_list(data2)
            print 'Ok'

        # benchmark key generation for different key lengths
        pcsc_pkcs15 = pkcs15.Pkcs15(smartcard_transaction)
        for keylength in (0x400, 0x600, 0x800):
            print keylength
            for i in range(5):
                t0 = time.time()
                pcsc_pkcs15.generate_key_pair(keylength=keylength)
                t = time.time() - t0
                print '%s\t%s' % (t, len(pcsc_pkcs15.get_public_key()))

def main():
    if len(sys.argv) >= 3:
        filename = sys.argv[2]
        if sys.argv[1] == 'test':
            return test(filename)
        with msc.MSC(filename) as smartcard_transaction:
            pcsc_pkcs15 = pkcs15.Pkcs15(smartcard_transaction)
            if sys.argv[1].startswith("write"):
                filenumber = int(sys.argv[1][5:])
                data = (ord(c) for c in sys.stdin.read())
                return pcsc_pkcs15.write_file(data, filenumber)
            if sys.argv[1].startswith("read"):
                filenumber = int(sys.argv[1][4:])
                data = pcsc_pkcs15.read_file(filenumber)
                return sys.stdout.write(''.join(chr(x) for x in data))
            if sys.argv[1] == "generate":
                pcsc_pkcs15.generate_key_pair()
                print to_hex(pcsc_pkcs15.get_public_key())
                return
            if sys.argv[1] == "get":
                print to_hex(pcsc_pkcs15.get_public_key())
                return
            if sys.argv[1] == "challenge":
                challenge = sys.argv[3]
                print 'Challenge: %r' % challenge
                print 'Challenge: %s' % to_hex(challenge)
                public_key = pcsc_pkcs15.get_public_key()
                print 'Public key:', to_hex(pcsc_pkcs15.get_public_key())
                challenge_signature = pcsc_pkcs15.create_challenge_signature(challenge)
                print 'Signature: %s' % to_hex(challenge_signature)
                print 'Verified:', pcsc_pkcs15.verify_signature(public_key, challenge, challenge_signature)
                return
    print "Standalone interface to MSC"
    print
    print "Usage:"
    print "\t%s [generate|read|challenge|writeX|readX] [filename]" % sys.argv[0]
    print
    print "generate\t- Generate new key pair and print public key - that takes 5-10 seconds."
    print "get\t\t- Get and print public key - that takes a second."
    print "challenge\t\t- sign the challenge and verify with pubkey"
    print "writeX\t\t- Write file X from stdin."
    print "readX\t\t- Read file X to stdout."
    print "filename\t- any file on MSC such as /media/disk/giritech.gsi. The file will be overwritten."

if __name__ == '__main__':
    main()
