"""
Communication with G&D file-system based GSI interface

Sample C++ code snippets from Tobias was completed and verified and converted to Python
"""

import win_h

# stuff not included in win_h:
GENERIC_READ                     = 0x80000000
GENERIC_WRITE                    = 0x40000000
GENERIC_EXECUTE                  = 0x20000000
GENERIC_ALL                      = 0x10000000
CREATE_NEW          = 1
CREATE_ALWAYS       = 2
OPEN_EXISTING       = 3
OPEN_ALWAYS         = 4
TRUNCATE_EXISTING   = 5
FILE_FLAG_WRITE_THROUGH         = 0x80000000
FILE_FLAG_OVERLAPPED            = 0x40000000
FILE_FLAG_NO_BUFFERING          = 0x20000000
FILE_FLAG_RANDOM_ACCESS         = 0x10000000
FILE_FLAG_SEQUENTIAL_SCAN       = 0x08000000
FILE_FLAG_DELETE_ON_CLOSE       = 0x04000000
FILE_FLAG_BACKUP_SEMANTICS      = 0x02000000
FILE_FLAG_POSIX_SEMANTICS       = 0x01000000
FILE_FLAG_OPEN_REPARSE_POINT    = 0x00200000
FILE_FLAG_OPEN_NO_RECALL        = 0x00100000
FILE_FLAG_FIRST_PIPE_INSTANCE   = 0x00080000
FILE_BEGIN           = 0
FILE_CURRENT         = 1
FILE_END             = 2
PAGE_READWRITE       = 0x04     
PAGE_NOCACHE         = 0x200     
MEM_COMMIT           = 0x1000
MEM_RELEASE          = 0x8000
INVALID_HANDLE_VALUE = -1

import ctypes

BLOCK_SIZE = 512

class GsiFs(object):

    def __init__(self, filename):
        # http://msdn2.microsoft.com/en-us/library/aa363858(VS.85).aspx CreateFile Function
        self._hFile = win_h.CreateFileW(filename, 
                                        GENERIC_READ|GENERIC_WRITE, 
                                        0, 
                                        None, 
                                        OPEN_ALWAYS, 
                                        FILE_FLAG_NO_BUFFERING, 
                                        ctypes.wintypes.HANDLE())
        assert self._hFile != INVALID_HANDLE_VALUE, 'Error opening %r' % filename

        # http://msdn2.microsoft.com/en-us/library/aa366887.aspx VirtualAlloc Function
        self._buf = win_h.VirtualAlloc(ctypes.cast(None, win_h.LPVOID), 
                                       ctypes.cast(BLOCK_SIZE, win_h.SIZE_T), 
                                       MEM_COMMIT, 
                                       PAGE_READWRITE|PAGE_NOCACHE)

    def close(self):
        # http://msdn2.microsoft.com/en-us/library/ms724211(VS.85).aspx CloseHandle Function
        win_h.CloseHandle(self._hFile)
        self._hFile = None

        # http://msdn2.microsoft.com/en-us/library/aa366892(VS.85).aspx VirtualFree Function
        _status = win_h.VirtualFree(self._buf, None, MEM_RELEASE)
        self._buf = None

    def send(self, request):
        """Send GSI-encoded block"""
        assert len(request) == BLOCK_SIZE, 'Invalid request size: %s' % len(request)

        # write request
        dwPtr = win_h.SetFilePointer (self._hFile, 0, None, FILE_BEGIN)
        assert dwPtr == 0, 'Invalid seek: %x' % dwPtr
        bytes_written = ctypes.wintypes.DWORD()
        # http://msdn2.microsoft.com/en-us/library/aa365747(VS.85).aspx WriteFile Function
        status = win_h.WriteFile(self._hFile, request, len(request), ctypes.byref(bytes_written), None)
        assert status, 'Invalid WriteFile status: %s' % status
        
        assert bytes_written.value == BLOCK_SIZE, 'Incomplete write: %s bytes' % bytes_written
    
    def read(self):
        """Read GSI-encoded block"""
        # http://msdn2.microsoft.com/en-us/library/aa365541(VS.85).aspx SetFilePointer Function
        dwPtr = win_h.SetFilePointer (self._hFile, 0, None, FILE_BEGIN)
        assert dwPtr == 0, 'Invalid seek: %x' % dwPtr

        # read response
        bytes_read = ctypes.wintypes.DWORD()
        # http://msdn2.microsoft.com/en-us/library/aa365467(VS.85).aspx ReadFile Function
        status = win_h.ReadFile(self._hFile, self._buf, BLOCK_SIZE, ctypes.byref(bytes_read), None)
        assert status, 'Invalid ReadFile status: %s' % status

        assert bytes_read.value == BLOCK_SIZE, 'Incomplete read: %s bytes' % bytes_read
        return ctypes.cast(self._buf, ctypes.POINTER(ctypes.c_char))[:BLOCK_SIZE]
