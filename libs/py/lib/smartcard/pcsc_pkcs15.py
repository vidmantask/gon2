"""
Lib for using a Smart Card as authentication factor.

Connects through PC/SC and winscard.dll and thus requires an installed reader driver.

It is assumed that the G&D MW applet has been uploaded and the PKCS-15 structure created.
"""

from __future__ import with_statement

import sys

import pkcs15
import pcsc

from common import *

def test(readername = "MSC"):
    with pcsc.PcSc() as pcsc_handler:
        print 'Listing Readers ...'
        print pcsc_handler.get_readers()
        print 'Connecting on readername %r' % readername
        with pcsc_handler.get_connected_transaction(readername) as smartcard_transaction:
            pcsc_pkcs15 = pkcs15.Pkcs15(smartcard_transaction)
            print 'Generating key pair ...'
            pcsc_pkcs15.generate_key_pair()
            print 'Getting public key ...'
            mod = pcsc_pkcs15.get_public_key()
            print 'Public key:', to_hex(mod)
            print 'Verified:',
            print pcsc_pkcs15.verify(mod)

            for filenumber in [0, 1]:
                data1 = tuple(range(256))*4*4
                print 'Writing file %s with %s bytes' % (filenumber, len(data1))
                pcsc_pkcs15.write_file(data1, filenumber)
                print 'Reading'
                data2 = pcsc_pkcs15.read_file(filenumber)
                assert tuple(data1) == tuple(data2), '1:\n' + dump_list(data1) + '\n2:\n' + dump_list(data2)
                print 'Ok'

def main():
    if len(sys.argv) >= 3:
        readername = sys.argv[2]
        if sys.argv[1] == 'test':
            return test(readername)
        with pcsc.PcSc() as pcsc_handler:
            with pcsc_handler.get_connected_transaction(readername) as smartcard_transaction:
                pcsc_pkcs15 = pkcs15.Pkcs15(smartcard_transaction)
                if sys.argv[1].startswith("write"):
                    filenumber = int(sys.argv[1][5:])
                    data = (ord(c) for c in sys.stdin.read())
                    return pcsc_pkcs15.write_file(data, filenumber)
                if sys.argv[1].startswith("read"):
                    filenumber = int(sys.argv[1][4:])
                    data = pcsc_pkcs15.read_file(filenumber)
                    return sys.stdout.write(''.join(chr(x) for x in data))
                if sys.argv[1] == "generate":
                    pcsc_pkcs15.generate_key_pair()
                    print to_hex(pcsc_pkcs15.get_public_key())
                    return
                if sys.argv[1] == "get":
                    print to_hex(pcsc_pkcs15.get_public_key())
                    return
    print "Standalone interface to Smart Card"
    print
    print "Usage:"
    print "\t%s [generate|read|writeX|readX] [readername]" % sys.argv[0]
    print
    print "generate\t- Generate new key pair and print public key - that takes 5-10 seconds."
    print "get\t\t- Get and print public key - that takes a second."
    print "writeX\t\t- Write file X from stdin."
    print "readX\t\t- Read file X to stdout."
    print "readername\t- one of:"
    try:
        with pcsc.PcSc() as pcsc_handler:
            readers = pcsc_handler.get_readers()
            if readers:
                for reader in readers:
                    print '\t\t\t"%s"' % reader
            else:
                print '\t\t\tNo readers found'
    except pcsc.SmartCardExceptionSCardFunctionalityNotAvailable:
        print "PCSC smart card system service not available"


if __name__ == '__main__':
    main()
