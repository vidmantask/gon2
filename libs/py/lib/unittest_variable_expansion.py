import unittest
from lib import giri_unittest

from lib import variable_expansion

class VariableExpansionTests(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

  
    def test(self):

        def getter(a, b): 
            return '{%s/%s}' % (a, b)
        
        all_ascii = ''.join(chr(x) for x in range(256))
        self.assertEqual(variable_expansion.expand(all_ascii, getter), all_ascii)
        valid_ascii = all_ascii.replace('%', '').replace('(', '').replace(')', '') 
        self.assertEqual(variable_expansion.expand('%(' + valid_ascii.replace('.', '') + '.' + valid_ascii + ')', getter), 
                                                   '{' + valid_ascii.replace('.', '') + '/' + valid_ascii + '}')
        
        self.assertEqual(variable_expansion.expand('%(a.b)', getter), '{a/b}')
        self.assertEqual(variable_expansion.expand('%(a.)', getter), '{a/}')
        self.assertEqual(variable_expansion.expand('%(a..)', getter), '{a/.}')
        self.assertEqual(variable_expansion.expand('%(a)', getter), '%(a)')
        self.assertEqual(variable_expansion.expand('%%(a.b)', getter), '{a/b}')
        self.assertEqual(variable_expansion.expand('%(a.b.c)', getter), '{a/b.c}')
        self.assertEqual(variable_expansion.expand('%(a.%(a.b))', getter), '%(a.{a/b})')
        self.assertEqual(variable_expansion.expand('%%(a.%(a.b))', getter), '%%(a.{a/b})')
        self.assertEqual(variable_expansion.expand('%(a.b)'*117, getter), '{a/b}'*117)

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'mk'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
