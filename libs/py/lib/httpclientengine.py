import mime
import weakref

class HttpRequest(object):
    """A request, might be used several times internally for auth/redirect retries, returned with result"""
    def __init__(self, headers=None, content='', method=None, path=None, user_agent=None, host=None, enable_ssl=False, ssl_disable_certificate_verification=False):
        self.headers = headers
        self.content = content
        self.method = method
        self.host = host
        self.path = path
        self.user_agent = user_agent
        self.authkind = None
        self.enable_ssl = enable_ssl
        self.ssl_disable_certificate_verification = ssl_disable_certificate_verification


class HttpClientEngineCallbacks(object):
    """Handle engines response to the data fed to from_server"""

    def to_server(self, buf):
        """Engine has data to send to server"""
        raise NotImplementedError

    def got_http_response_start(self, status, headers, length):
        """Dispatched by engine from server"""
        # length is bytes or CHUNKED or CLOSE
        raise NotImplementedError

    def got_http_response_fragment(self, chunk):
        """Dispatched by engine from server"""
        raise NotImplementedError

    def got_http_response_done(self, alive):
        raise NotImplementedError

    def got_http_cancel(self, msg=None):
        raise NotImplementedError


class SilentHttpClientEngineCallbacks(HttpClientEngineCallbacks):
    """Mute everything - only used for safe shutdown"""
    got_http_response_start = got_http_response_fragment = got_http_response_done = got_http_cancel = lambda *a: None


class HttpClientEngine(object):
    """
    Tracking an HTTP connection to a server or proxy
    Has state, but has no data and no buffers.
    """
    # states other than None:
    WAITING_FOR_REQUEST = 'WAITING_FOR_REQUEST' # Waiting for request, might be used and persistent
    STREAMING_REQUEST = 'STREAMING_REQUEST' # Sending streamed
    WAITING_FOR_RESPONSE = 'WAITING_FOR_RESPONSE' # Reading until \r\n\r\n in buffer
    STREAMING_RESPONSE = 'STREAMING_RESPONSE' # Reading until content_length in buffer

    # response length values other than integers:
    CHUNKED = 'CHUNKED'
    CLOSE = 'CLOSE'

    def __init__(self, env, callbacks):
        self._env = env
        assert isinstance(callbacks, HttpClientEngineCallbacks)
        self._callbacks = weakref.ref(callbacks)

        self._state = self.WAITING_FOR_REQUEST
        self._method = None # Valid unless WAITING_FOR_REQUEST
        self._chunked = None # If True then we are running chunked
        self._chunk_length = None # Length of current chunk if chunked
        self._content_length = None # When not running chunked: Expected body length
        self._content_unbounded = None # Body until closed - neither chunked nor length
        self._keep_alive = None
        
        self._temp = None

    def _cancel(self):
        """Something irregular happened and we make a final call to callbacks and give up."""
        self._state = None
        self._temp = SilentHttpClientEngineCallbacks()
        self._callbacks = weakref.ref(self._temp)
        return None

    def from_server(self, buf):
        """Call when data received from server.
        No buf means EOF.
        Returns unused data from buf."""
        self._env.log3('%s from_server %s', self, buf)
        if not buf:
            if self._state is None:
                self._env.log2('%s received TCP close after close', self)
                return self._cancel()
            if not self._content_unbounded:
                # RFC 2616 8.1.4: A client, server, or proxy MAY close the transport connection at any time.
                if self._state == self.WAITING_FOR_REQUEST:
                    self._env.log3('%s received ok TCP close', self)
                else:
                    self._env.log1('%s received premature TCP close in state %s', self, self._state)
                return self._cancel()
        self._env.log2('%s from_server %s bytes', self, len(buf))
        self._env.log3('%s from_server %r', self, buf)
        if self._state is None:
            self._env.log1('%s received TCP after close', self)
            return self._cancel()
        if self._state in [self.WAITING_FOR_REQUEST, self.STREAMING_REQUEST]:
            self._env.log1('%s received TCP while waiting in state %s for a request to send - closing and will disappear', self, self._state)
            return self._cancel() # Ignore silently that recv failed.
        if self._state == self.WAITING_FOR_RESPONSE:
            if not '\r\n\r\n' in buf: # FIXME: No reason to start searching from end 0 every time - this is O(n2)
                if len(buf) > self._env.config.MAX_RECEIVE_HEADER_LENGTH:
                    self._env.log1('%s header too long - MAX_RECEIVE_HEADER_LENGTH is %s', self, self._env.config.MAX_RECEIVE_HEADER_LENGTH)
                    return self._cancel()
                return buf
            # Full header received - process it and go reading some content
            headers = mime.Headers()
            unused_data = headers.parse(buf)
            self._env.log3("%s got HTTP headers %s", self, headers.items())
            try:
                protocol, status, _msg = headers.start_line.split(' ', 2)
            except ValueError:
                self._env.log1('%s invalid start-line in reply %r', self, headers.start_line)
                return self._cancel()
            protocol = protocol.upper()
            connection_tokens = [x.strip() for x in headers.get('PROXY-CONNECTION', headers.get('CONNECTION', '')).upper().split(',') if x] # proxy-connection seems to be non-standard extension used by squid
            if protocol == 'HTTP/1.1':
                self._keep_alive = self._env.config.PERSISTENT_HTTP and 'CLOSE' not in connection_tokens # RFC 2616 8.1.2 and 14.10: Persistent unless explicit close
            elif protocol == 'HTTP/1.0': # RFC 2616 19.6.2 discusses problems with persistence in 1.0 and points to ...
                self._keep_alive = self._env.config.PERSISTENT_HTTP and 'KEEP-ALIVE' in connection_tokens # RFC 2068 19.7.1.1
            else:
                self._env.log1('%s invalid http protocol %r', self, protocol)
                return self._cancel()
            transfer_encoding = headers.get('TRANSFER-ENCODING', '')
            self._chunked = False
            self._content_length = None
            self._content_unbounded = None
            if status.startswith('1') or status in ['204', '304'] or self._method == 'HEAD': # 4.4
                self._content_length = 0 # ignore any body specification
            elif transfer_encoding.upper() == 'CHUNKED':
                self._chunked = True
                self._chunk_length = None
            else:
                content_length_string = headers.get('CONTENT-LENGTH')
                if content_length_string is None:
                    if transfer_encoding or 'CLOSE' in connection_tokens:
                        # seen with dr.dk 304 Via 'NS-CACHE-8.0: NS11'
                        self._content_unbounded = True
                    else:
                        self._content_length = 0 # RFC 2616 3.4 hints that only body when Content-Length or Transfer-Encoding
                else:
                    try:
                        self._content_length = int(content_length_string)
                    except ValueError, e:
                        self._env.log1('%s Invalid Content-Length %r: %s', self, content_length_string, e)
                        return self._cancel()

            self._env.log3('%s transfer-encoding=%r chunked=%s content-length=%s unbounded=%s',
                           self, transfer_encoding, self._chunked, self._content_length, self._content_unbounded)
            self._callbacks().got_http_response_start(status, headers,
                    self.CHUNKED if self._chunked else self.CLOSE if self._content_unbounded else self._content_length)
            if self._content_length == 0:
                self._env.log2('%s response without body - waiting for request, alive %s', self, self._keep_alive)
                self._state = self.WAITING_FOR_REQUEST
                self._callbacks().got_http_response_done(self._keep_alive)
                return unused_data
            self._state = self.STREAMING_RESPONSE # Continue directly in next if
            if not unused_data:
                return '' # don't continue as EOF just because we processed everything
            buf = unused_data
        if self._state == self.STREAMING_RESPONSE:
            if self._content_unbounded:
                if buf:
                    self._env.log3('%s got %s bytes unbounded response', self, len(buf))
                    self._callbacks().got_http_response_fragment(buf)
                    return ''
                self._env.log2('%s got footerend of unbounded response - closing', self)
                self._state = None
                self._callbacks().got_http_response_done(self._keep_alive) # can't be keep-alive...
                return self._cancel() # hmm ... might be too late ... would perhaps be nice to do this before _done ... and should perhaps also return ''
            elif self._chunked:
                assert buf
                while True:
                    if self._chunk_length is None or self._chunk_length == 0: # read length indicator
                        if self._chunk_length is None:
                            start = 0
                        else:
                            if len(buf) < 2:
                                self._env.log3('%s end-of-chunk-marker not found in %r', self, buf)
                                return buf
                            if not buf.startswith('\r\n'):
                                self._env.log1('%s invalid end-of-chunk-marker in %r', self, buf)
                                return self._cancel()
                            start = 2
                        end = buf.find('\r\n', start)
                        if end < 0:
                            self._env.log3('%s found no chunk length at %s in %r', self, start, buf)
                            return buf
                        s = buf[start:end]
                        try:
                            self._chunk_length = int(s.strip(), 16)
                        except ValueError, e:
                            self._env.log1('%s invalid chunk length %r: %s', self, s, e)
                            return self._cancel()
                        self._env.log3('%s found chunk length %s from %r', self, self._chunk_length, s)
                        if self._chunk_length:
                            buf = buf[end + 2:]
                        else: # this is final empty chunk
                            footerend = buf.find('\r\n', end + 2)
                            if footerend < 0:
                                self._env.log3('%s found no end of final chunk at %s in %r', self, end + 2, buf)
                                return buf
                            self._env.log3('%s got footerend of final chunk', self)
                            footer = buf[end + 2:footerend]
                            if footer.strip():
                                self._env.log1('%s unhandled chunk footer %r', self, footer)
                                return self._cancel()
                            self._state = self.WAITING_FOR_REQUEST
                            self._callbacks().got_http_response_done(self._keep_alive)
                            return buf[footerend + 2:]
                    elif self._chunk_length > 0: # read fragment of chunk
                        l = min(len(buf), self._chunk_length)
                        self._env.log3('%s got %s of chunk length %s', self, l, self._chunk_length)
                        self._chunk_length -= l
                        self._callbacks().got_http_response_fragment(buf[:l])
                        buf = buf[l:]
                    else:
                        self._env.log1('%s invalid chunk length %r', self, self._chunk_length)
                        return self._cancel()
                    if not buf:
                        return buf
            elif self._content_length is not None:
                l = min(len(buf), self._content_length)
                self._env.log3('%s got %s of content length %s', self, l, self._content_length)
                if l:
                    self._content_length -= l
                    self._callbacks().got_http_response_fragment(buf[:l])
                if not self._content_length:
                    self._state = self.WAITING_FOR_REQUEST
                    self._callbacks().got_http_response_done(self._keep_alive)
                return buf[l:]
            else:
                assert False, (self._content_unbounded, self._chunked, self._content_length)
        assert False, (self._state, buf)

    def send_http_request(self, req):
        """Process a HttpRequest"""
        self._env.log3('%s send_http_request', self)
        assert self._state == self.WAITING_FOR_REQUEST, self._state # will be None if sending after close
        assert req.path
        assert req.host
        # request headers are modified heavily, so we make a clone
        headers = mime.Headers(start_line='%s %s %s' % (req.method or ('POST' if req.content else 'GET'), req.path, 'HTTP/1.1'))

        if req.headers: # copy headers - and remove those specified in Connection (if this is the right place to do that ...)
            for k, v in req.headers.items():
                headers.add(k, v)
            for k in req.headers.get('connection', '').split(','): # ... remove any header field(s) from the message with the same name as the connection-token
                headers.replace(k, None)

        headers.replace('Host', req.host) # RFC 2616 5.1 says not required through proxies, 5.2 says not used in 1.1 with absolute urls, 14.23 and 9.0 says mandatory
        headers.replace('Connection', 'Keep-Alive' if self._env.config.PERSISTENT_HTTP else 'Close') # RFC 2616 14.10, and 8.1.2.1 says SHOULD send Close if intention
        #'Cache-Control': 'no-cache', # RFC 2616 14.9 (but not needed when using POST)
        #'Pragma': 'no-cache', # RFC 2616 14.32: 1.0 compatibility
        # - 14.11 says no-transform to avoid that non-transparent proxies messes with Content-Encoding
        #'Content-Encoding': 'identity', # RFC 2616 14.11 says MUST be there if encoded, 3.5 says don't show "identity"
        #'Content-Type': 'application/octet-stream', '# RFC 2616 7.2.1 says this SHOULD be included but recipients SHOULD default to this
        # RFC 2616 7.1 says extensions headers MUST be forwarded by transparent proxies, 13.5.1 says end-to-end (unless in Connection) - could be used for tcp-key etc...
        if req.user_agent:
            headers.replace('User-Agent', req.user_agent) # RFC 2616 3.8
        headers.replace('Content-Length', len(req.content)) # RFC 2616 14.13
        #print 'Sending request:', headers
        chunk = headers.format() + req.content
        self._env.log3('%s enqueuing %r', self, chunk)
        self._method = req.method
        self._callbacks().to_server(chunk)
        self._state = self.WAITING_FOR_RESPONSE

    def send_http_request_start(self, req):
        self._env.log3('%s send_http_request_start', self)
        # Note: req.path, req.host, etc are _not_ used in this case
        assert self._state == self.WAITING_FOR_REQUEST, self._state
        assert req.headers
        assert req.headers.start_line is not None, req.headers.start_line
        assert not req.content, req.content
        #print 'Sending request:', headers
        chunk = req.headers.format()
        self._env.log3('%s enqueuing %r', self, chunk)
        self._method = req.headers.start_line.split()[0]
        self._callbacks().to_server(chunk)
        self._state = self.STREAMING_REQUEST

    def send_http_request_fragment(self, chunk):
        assert self._state == self.STREAMING_REQUEST, self._state
        self._callbacks().to_server(chunk)

    def send_http_request_done(self):
        self._env.log2('%s request done and waiting for response', self)
        assert self._state == self.STREAMING_REQUEST, self._state
        self._state = self.WAITING_FOR_RESPONSE

    def __repr__(self):
        return '%s%X' % (self.__class__.__name__, id(self))

    __str__ = __repr__


class HttpBodyClientEngineCallbacks(HttpClientEngineCallbacks):
    """
    Handling a HTTP connection to a server or proxy calling back when full content received
    """

    def __init__(self, env):
        HttpClientEngineCallbacks.__init__(self)
        self.__env = env
        self.__req = None
        self.__response_len = self.__response_fragments = self.__response_status = self.__response_headers = None

    def got_http_response_start(self, status, headers, length):
        assert self.__response_fragments is None, self.__response_fragments
        self.__response_len = 0
        self.__response_fragments = []
        self.__response_status = status
        self.__response_headers = headers

    def got_http_response_fragment(self, chunk):
        self.__response_len += len(chunk)
        if self.__response_len > self.__env.config.MAX_RECEIVE_CONTENT_LENGTH:
            self.__env.log1('response body %s is too large - max is %s',
                    self.__response_len, self.__env.config.MAX_RECEIVE_CONTENT_LENGTH)
            # TODO: fail somehow
        else:
            self.__response_fragments.append(chunk)

    def got_http_response_done(self, alive):
        body = ''.join(self.__response_fragments)
        self.got_http_response(self.__response_status, self.__response_headers, body, alive)
        self.__response_len = self.__response_fragments = self.__response_status = self.__response_headers = None

    def got_http_response(self, status, headers, content, alive):
        raise NotImplementedError


import unittest

class Tests(unittest.TestCase):
    trace = []

    def setUp(self):
        trace = self.trace
        class Callbacks(HttpBodyClientEngineCallbacks):
            def to_server(self, buf):
                trace.append(('to_server', buf))
            def got_http_response(self, status, headers, content, alive):
                trace.append(('got_http_response', status, str(headers), content, alive))
        self.env = self.Env(self.trace)
        self.callbacks = Callbacks(self.env)

    class Env:
        def __init__(self, trace):
            self.trace = trace
        class config:
            PERSISTENT_HTTP = True
            MAX_RECEIVE_HEADER_LENGTH = 1234
            MAX_RECEIVE_CONTENT_LENGTH = 1234
            NEGOTIATE = False
        def log1(self, *a): pass # self.trace.append(('log1',) + a)
        def log2(self, *a): pass # self.trace.append(('log2',) + a)
        def log3(self, *a): pass # self.trace.append(('log3',) + a)

    def testContentLength(self):
        del self.trace[:]
        eng = HttpClientEngine(self.env, self.callbacks)
        eng.send_http_request(HttpRequest(host='zap', path='/zap'))
        buf = 'HTTP/1.1 200 hello\r\ncontent-length:5'
        self.assertEqual(eng.from_server(buf), buf)
        self.assertEqual(eng.from_server(buf + '\r\n\r\n'), '')
        self.assertEqual(eng.from_server('body'), '')
        self.assertEqual(eng.from_server('!unused'), 'unused')
        self.assertEqual(self.trace, [
                ('to_server', 'GET /zap HTTP/1.1\r\nHost: zap\r\nConnection: Keep-Alive\r\nContent-Length: 0\r\n\r\n'),
                ('got_http_response', '200', '<Headers:\nHTTP/1.1 200 hello\ncontent-length: 5\n>', 'body!', True),
                ])

    def testChunkedResponse(self):
        del self.trace[:]
        eng = HttpClientEngine(self.env, self.callbacks)
        eng.send_http_request(HttpRequest(host='zif', path='http://zif/'))
        self.assertEqual(eng.from_server('HTTP/1.1 200 hello\r\ntransfer-encoding:chunked\r\n\r\n4\r\nhell\r'), '\r')
        self.assertEqual(eng.from_server('\r\n1\r\no\r\n0\r\n\r\nunused'), 'unused')
        self.assertEqual(self.trace, [
                ('to_server', 'GET http://zif/ HTTP/1.1\r\nHost: zif\r\nConnection: Keep-Alive\r\nContent-Length: 0\r\n\r\n'),
                ('got_http_response', '200', '<Headers:\nHTTP/1.1 200 hello\ntransfer-encoding: chunked\n>', 'hello', True),
                ])

    def testPost(self):
        del self.trace[:]
        eng = HttpClientEngine(self.env, self.callbacks)
        eng.send_http_request(HttpRequest(host='spif', path='http://spif/', content='please'))
        self.assertEqual(eng.from_server('HTTP/1.0 200 hello\r\n\r\n'), '')
        self.assertEqual(eng.from_server('nananana'), None)
        self.assertEqual(self.trace, [
                ('to_server', 'POST http://spif/ HTTP/1.1\r\nHost: spif\r\nConnection: Keep-Alive\r\nContent-Length: 6\r\n\r\nplease'),
                ('got_http_response', '200', '<Headers:\nHTTP/1.0 200 hello\n>', '', False),
                ])

    def testMultiRequest(self):
        trace = []
        class Callbacks(HttpClientEngineCallbacks):
            def to_server(self, *a):
                trace.append(('to_server',) + a)
            def got_http_response_start(self, *a):
                trace.append(('got_http_response_start',) + tuple(map(str, a)))
            def got_http_response_fragment(self, *a):
                trace.append(('got_http_response_fragment',) + a)
            def got_http_response_done(self, *a):
                trace.append(('got_http_response_done',) + a)

        eng = HttpClientEngine(self.Env(trace), Callbacks())
        req = HttpRequest(headers=mime.Headers(start_line='GET /foo HTTP/1.1', items=[('content-length', 4)]))
        eng.send_http_request_start(req)
        eng.send_http_request_fragment('xxxx')
        eng.send_http_request_done()
        self.assertEqual(eng.from_server('HTTP/1.1 200 hello\r\nconnection:close\r\n\r\nhello'), '')
        self.assertEqual(eng.from_server(''), None)
        self.assertEqual(trace, [
                ('to_server', 'GET /foo HTTP/1.1\r\ncontent-length: 4\r\n\r\n'),
                ('to_server', 'xxxx'),
                ('got_http_response_start', '200', '<Headers:\nHTTP/1.1 200 hello\nconnection: close\n>', 'CLOSE'),
                ('got_http_response_fragment', 'hello'),
                ('got_http_response_done', False),
                ])
