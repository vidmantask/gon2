"""
This module contains functionality for unittesting
"""
import time
import os
import sys
import thread
import threading
import tempfile
import shutil
import unittest

import lib.dev_env
import lib.version
import lib.checkpoint
import lib.dev_env.tools
import lib.dictionary
import lib.gpm.gpm_env
import lib.gpm.gpm_analyzer

import components.dictionary.common
import components.config.common
import components.server_config_ws.server_config
import components.communication.async_service
import components.presentation.user_interface
import components.communication.sim.sim_tunnel_endpoint



HG_ROOT = os.path.normpath(os.path.join(os.path.dirname(__file__), '..', '..'))
PY_ROOT = os.path.join(HG_ROOT, 'py')
UNITTEST_TEMP_ROOT = os.path.join(HG_ROOT, '..', 'gon_build', 'temp')
UNITTEST_DATA_FOLDER = os.path.join(HG_ROOT, 'setup', 'dev_env', 'unittest_data')#



global_checkpoint_handler = None
def get_checkpoint_handler():
    global global_checkpoint_handler
    if global_checkpoint_handler is None:
        global_checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    return global_checkpoint_handler

def get_checkpoint_handler_null():
    return lib.checkpoint.CheckpointHandler.get_NULL()


global_async_service = None
def get_async_service():
    global global_async_service
    if global_async_service is None:
        components.communication.async_service.init(get_checkpoint_handler())
        global_async_service = components.communication.async_service.AsyncService(get_checkpoint_handler(), "Unittest", 5, keep_running=True)
        global_async_service.start()
    return global_async_service


global_sim_tunnel_endpoint_connector = None
def get_sim_tunnel_endpoint_connector():
    global global_sim_tunnel_endpoint_connector
    if global_sim_tunnel_endpoint_connector is None:
        global_sim_tunnel_endpoint_connector = components.communication.sim.sim_tunnel_endpoint.SimThread(get_checkpoint_handler(), get_async_service())
        global_sim_tunnel_endpoint_connector.start_and_wait_until_ready()
    return global_sim_tunnel_endpoint_connector

def get_dictionary():
    return lib.dictionary.Dictionary(get_checkpoint_handler())

def close():
    global global_sim_tunnel_endpoint_connector
    if global_sim_tunnel_endpoint_connector is not None:
        global_sim_tunnel_endpoint_connector.stop_and_wait_until_closed()
        global_sim_tunnel_endpoint_connector = None

    global global_async_service
    if global_async_service is not None:
        global_async_service.stop_and_wait()
        global_async_service = None


def mkdtemp():
    if not os.path.exists(UNITTEST_TEMP_ROOT):
        os.makedirs(UNITTEST_TEMP_ROOT)
    return tempfile.mkdtemp(prefix='unittest_', dir=UNITTEST_TEMP_ROOT)

def mk_temp_file():
    if not os.path.exists(UNITTEST_TEMP_ROOT):
        os.makedirs(UNITTEST_TEMP_ROOT)
    return tempfile.mktemp(prefix='unittinstance_run_rootest_', dir=UNITTEST_TEMP_ROOT)

def mk_temp_filename():
    return os.path.join(tempfile.mkdtemp(dir=UNITTEST_TEMP_ROOT), 'tempfile')

def wait_until_with_timeout(predicate, timeout_ms, **kwargs):
    counter = 0
    while not predicate(**kwargs) and counter < timeout_ms:
        time.sleep(0.1)
        counter += 100

def get_user_interface_simulate(checkpoint_handler):
    class Configuration(object):
        def __init__(self):
            pass

    dictionary = components.dictionary.common.load_dictionary(checkpoint_handler)
    return components.presentation.user_interface.UserInterface(components.presentation.user_interface.UserInterface.VIEW_TYPE_SIMULATOR, dictionary=dictionary, configuration=Configuration())

def generate_database_connect_string(folder):
    return 'sqlite:///%s/test_database.db' % folder.replace('\\', '/')

def get_image_path():
    return os.path.join(PY_ROOT, 'components', 'presentation', 'gui')


class UnittestDevEnvInstallation(object):
    def __init__(self, installation_root):
        self._installation_root = installation_root
        self.dev_env = lib.dev_env.DevEnv(lib.dev_env.tools.ToolsSetup.create_default(), HG_ROOT, lib.version.Version.create_current())
        self.dev_env_installation = lib.dev_env.DevEnvInstallation(self.dev_env, installation_root)

        configuration_server_config_folder = os.path.join(installation_root,  'gon_config_service', 'win')
        configuration_server_config = components.config.common.ConfigConfigService()
        configuration_server_config.read_config_file_from_folder(configuration_server_config_folder)
        configuration_server_config.installation_path = installation_root
        configuration_server_config.plugin_modules_path = PY_ROOT
        configuration_server_config.license_filename = os.path.join(installation_root, 'config', 'deployed', 'gon_license.lic')
        configuration_server_config.service_https_enabled = False
        configuration_server_config.get_instance_run_root = self.get_instance_run_root

        server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(configuration_server_config)

        self.server_configuration_all = server_configuration_all

        self.config_server = configuration_server_config
        self.config_server_management = server_configuration_all.configuration_server_management
        self.config_server_management.management_ws_https_enabled = False

        self.config_server_gateway = server_configuration_all.configuration_server_gateway
        self.config_client_management_service = server_configuration_all.configuration_client_management_service
        self.config_client_management_service.set_ini_path(self.server_configuration_all.get_client_management_service_path_abs())
        self.config_client_management_service.management_deploy_ws_https_enabled = False
        self.db_database = self.config_server_management.db_database
        self.db_database = self.db_database.replace('../../', '%s/' % installation_root )

        self.config_server_gateway.db_database = self.db_database
        self.config_server_management.db_database = self.db_database
        self.config_server.db_database = self.db_database

    def get_instance_run_root(self):
        return self._installation_root


    @classmethod
    def create(cls):
        temp_root = mkdtemp()
        installation_root = os.path.join(temp_root, 'gon_installation')
        unittest_dev_env_installation =  UnittestDevEnvInstallation(installation_root)
        unittest_dev_env_installation.dev_env_installation.restore_template()
        return unittest_dev_env_installation

    def generate_gpms(self):
        self.dev_env_installation.generate_gpms()

    def get_gpm_repository(self):
        self.generate_gpms()
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        repository  = lib.gpm.gpm_analyzer.GpmRepository()
        repository.init_from_gpms(error_handler, self.get_gpms_folder(), self.get_gpmcdefs_folder())
        return repository

    def generate_setupdata(self):
        self.dev_env_installation.generate_setupdata()

    def restore_plugins(self):
        self.dev_env_installation.restore_plugins()

    def get_db_connect_url(self):
        return self.config_server_management.get_db_connect_url()

    def get_config_folder(self):
        return os.path.join(self._installation_root, 'config')

    def get_gpms_folder(self):
        return self.config_server_management.cpm_gpms_abspath

    def get_gpmcdefs_folder(self):
        return self.config_server_management.cpm_gpmc_defs_abspath

    def get_soft_token_path(self):
        return self.config_client_management_service.deployment_soft_token_abspath

    def get_deployment_download_cache_folder(self):
        return self.config_client_management_service.deployment_download_cache_abspath

    def get_license_handler(self):
        return self.server_configuration_all.get_license_handler()

    def get_installation_root(self):
        return self.dev_env_installation.installation_root

    def get_message_queue_offline_filename(self):
        return self.config_server_gateway.service_management_message_queue_offline_absfilename

    def get_license_filename(self):
        return self.config_server_management.license_absfilename


class GiriUnittest(object):
    def __init__(self):
        py_root = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
        sys.path.append(py_root)
        os.chdir(py_root)

    def run(self):
        try:
            if sys.version_info < (2, 7):
                unittest.main()
            else:
                unittest.main(exit=False)
        finally:
            close()

class ConfigurationStubClient():
    def __init__(self):
        self.gui_image_path = get_image_path()
