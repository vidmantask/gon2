"""
This file contains the interfaces to the hagi authetication  functionality. 
"""
import sys
import lib_cpp.communication


HagiServer_generate_challenge = lib_cpp.communication.HagiServer_generate_challenge
HagiServer_server_athenticate = lib_cpp.communication.HagiServer_server_athenticate

#verify_update_challenge_signature = lib_cpp.communication.HagiUpdate_verify_update_challenge_signature
#generate_gpms_signature = lib_cpp.communication.HagiUpdate_generate_gpms_signature
