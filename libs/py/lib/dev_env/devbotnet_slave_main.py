'''
Created on Jun 28, 2011

@author: thwang
'''
import sys
import os
import os.path
import optparse
import time
import threading
import traceback
import ConfigParser

HG_ROOT = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..")))
ROOT_PY = os.path.join(HG_ROOT, 'py')
sys.path.append(ROOT_PY)

import lib.dev_env
import lib.dev_env.devbotnet
import lib.dev_env.hg
import lib.version



class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--foreground_with_stop_file', type='string', default=None, help='run server in foreground')
        (self._options, self._args) = self._parser.parse_args()
        self._config = self._read_config_ini()

    def _read_config_ini(self):
        config_filename = os.path.join(HG_ROOT, 'config_local.ini')
        config = ConfigParser.ConfigParser()
        if not os.path.exists(config_filename):
            tools_setup = lib.dev_env.tools.ToolsSetup.create_from_config(config) 
            dev_env = lib.dev_env.DevEnv(tools_setup, HG_ROOT, self.get_version())
            devbotnet_slave_info = lib.dev_env.devbotnet.DevbotnetSlaveInfo(dev_env)
            devbotnet_slave_info.update_config_ini(config)

            config_file = open(config_filename, 'w')
            config.write(config_file)
            config_file.close()
        else:
            config.read(config_filename)
        return config

    def get_stop_filename(self):
        return self._options.foreground_with_stop_file

    def get_version(self):
        return lib.version.Version.create_current()

    def get_tools_setup(self):
        return lib.dev_env.tools.ToolsSetup.create_default() 

    def get_devbotnet_slave_info(self, dev_env):
        return lib.dev_env.devbotnet.DevbotnetSlaveInfo.create_from_config_ini(dev_env, self._config)




class DevbotnetSlave(threading.Thread):
    def __init__(self, commandline_options):
        threading.Thread.__init__(self, name="DevbotnetSlave")
        self.is_running = True
        self.commandline_options = commandline_options

        self.dev_env = lib.dev_env.DevEnv(commandline_options.get_tools_setup(), HG_ROOT, commandline_options.get_version())
        self.devbotnet_slave = lib.dev_env.devbotnet.DevbootnetSlave(commandline_options.get_devbotnet_slave_info(self.dev_env))

    def run(self):
        try:
            self.devbotnet_slave.run()
        except:
            print traceback.print_exc()
        self.is_running = False

    def stop(self):
        if self.is_running:
            self.devbotnet_slave.shutdown()
        



if __name__ == '__main__':
    commandline_options = CommandlineOptions()
    
    devbotnet_slave = DevbotnetSlave(commandline_options)
    devbotnet_slave.start()
    
    print "devbotnet started"
    while os.path.exists(commandline_options.get_stop_filename()):
        if not devbotnet_slave.is_running:
            print "devbotnet restarted"
            devbotnet_slave = DevbotnetSlave(commandline_options)
            devbotnet_slave.start()
            time.sleep(3)
        time.sleep(1)

    if devbotnet_slave.is_running:
        print "devbotnet stopping"
        devbotnet_slave.stop()
    
    while devbotnet_slave.is_running:
        print "devbotnet waiting to stop"
        time.sleep(2)
    
    print "devbotnet stopped"
