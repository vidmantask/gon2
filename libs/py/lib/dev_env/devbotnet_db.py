'''
This module contains the database definition used for devbotnet
'''
import sqlalchemy
import sqlalchemy.ext.declarative
import sqlalchemy.orm

import datetime

Base = sqlalchemy.ext.declarative.declarative_base()


class DevBotnetDBConfig(Base):
    __tablename__ = 'devbotnet_db_config'

    uid = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    ifilter = sqlalchemy.Column(sqlalchemy.String)
    efilter = sqlalchemy.Column(sqlalchemy.String)
    builders = sqlalchemy.Column(sqlalchemy.String)

    def __init__(self):
        self.ifilter = '.*'
        self.efilter = ''
        self.builders = '127.0.0.1:4242'

    def get_slave_connect_list(self):
        slave_connect_list = []
        for builders_element in self.builders.split(','):
            builder_element_elements = builders_element.split(':')
            if len(builder_element_elements) == 2:
                ip = builder_element_elements[0].strip()
                port = builder_element_elements[1].strip()
                slave_connect_list.append( (ip, int(port)) )
        return slave_connect_list

    def __repr__(self):
        return "<DevBotnetDBConfig('%d')>" % (self.uid)

    

class DevBotnetDBChangeset(Base):
    __tablename__ = 'devbotnet_db_changeset'

    uid = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    repository = sqlalchemy.Column(sqlalchemy.String)
    changeset = sqlalchemy.Column(sqlalchemy.String)

    def __init__(self, repository, changeset):
        self.repository = repository
        self.changeset = changeset

    def __repr__(self):
        return "<DevBotnetDBChangeset('%d')>" % (self.uid)

    def copy(self):
        new_instance = DevBotnetDBChangeset(self.repository, self.changeset)
        return new_instance


class DevBotnetDBRun(Base):
    __tablename__ = 'devbotnet_db_run'

    TYPE_UNITTEST = 0
    TYPE_STATIC_ANALYSIS = 1 
    TYPE_RELEASE = 2 
    TYPE_COVERAGE = 3 
    TYPE_PROFILE = 4 

    STATUS_OK = 0
    STATUS_ERROR = 1
    STATUS_RUNNING = 2

    uid = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    changeset_uid = sqlalchemy.Column(sqlalchemy.Integer)
    type = sqlalchemy.Column(sqlalchemy.Integer)
    status = sqlalchemy.Column(sqlalchemy.Integer)
    status_message = sqlalchemy.Column(sqlalchemy.String)

    computer = sqlalchemy.Column(sqlalchemy.String)
    arch = sqlalchemy.Column(sqlalchemy.String)
    arch_target = sqlalchemy.Column(sqlalchemy.String)
    begin_ts = sqlalchemy.Column(sqlalchemy.DateTime)
    end_ts = sqlalchemy.Column(sqlalchemy.DateTime)

    sum_calculated = sqlalchemy.Column(sqlalchemy.Boolean)
    sum_count = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_info = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_warning = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_error = sqlalchemy.Column(sqlalchemy.Integer)

    compare_run_uid = sqlalchemy.Column(sqlalchemy.Integer)
    
    def __init__(self, changeset_uid, type):
        self.changeset_uid = changeset_uid
        self.type = type
        self.computer = None
        self.arch = None
        self.arch_target = None
        self.begin_ts = None
        self.end_ts = None
        self.status = DevBotnetDBRun.STATUS_OK
        self.status_message = 'ok'
        self.sum_calculated = False
        self.sum_count = 0
        self.sum_count_info = 0
        self.sum_count_warning = 0
        self.sum_count_error = 0
        self.compare_run_uid = None

    def __repr__(self):
        return "<DevBotnetDBRun('%d')>" % (self.uid)
    
    def copy(self):
        new_instance = DevBotnetDBRun(self.changeset_uid, self.type)
        new_instance.computer = self.computer
        new_instance.arch = self.arch
        new_instance.arch_target = self.arch_target
        new_instance.begin_ts = self.begin_ts
        new_instance.end_ts = self.end_ts
        new_instance.status = self.status
        new_instance.status_message = self.status_message
        return new_instance


class DevBotnetDBRunPart(Base):
    __tablename__ = 'devbotnet_db_run_part'

    uid = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    run_uid = sqlalchemy.Column(sqlalchemy.Integer)

    module = sqlalchemy.Column(sqlalchemy.String)
    filename = sqlalchemy.Column(sqlalchemy.String)

    begin_ts = sqlalchemy.Column(sqlalchemy.DateTime)
    end_ts = sqlalchemy.Column(sqlalchemy.DateTime)

    sum_calculated = sqlalchemy.Column(sqlalchemy.Boolean)
    sum_count = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_info = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_warning = sqlalchemy.Column(sqlalchemy.Integer)
    sum_count_error = sqlalchemy.Column(sqlalchemy.Integer)

    compare_run_part_uid = sqlalchemy.Column(sqlalchemy.Integer)
    
    def __init__(self, run_uid, module, filename):
        self.run_uid = run_uid
        self.module = module
        self.filename = filename
        self.begin_ts = None
        self.end_ts = None
        self.sum_calculated = False
        self.sum_count = 0
        self.sum_count_info = 0
        self.sum_count_warning = 0
        self.sum_count_error = 0
        self.compare_run_part_uid = None

    def __repr__(self):
        return "<DevBotnetDBRunPart('%d')>" % (self.uid)

    def copy(self):
        new_instance = DevBotnetDBRunPart(self.run_uid, self.module, self.filename)
        new_instance.begin_ts = self.begin_ts
        new_instance.end_ts = self.end_ts
        return new_instance


class DevBotnetDBRunPartDetail(Base):
    __tablename__ = 'devbotnet_db_run_part_detail'

    STATUS_INFO = 0
    STATUS_WARNING = 1
    STATUS_ERROR = 2
    STATUS_OK = 3

    TYPE_LINE = 0
    TYPE_MODULE = 1
    TYPE_MODULE_STDOUT = 2
    TYPE_MODULE_STDERROR = 3
    TYPE_MODULE_PROFILE = 4
    
    uid = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    run_part_uid = sqlalchemy.Column(sqlalchemy.Integer)

    type = sqlalchemy.Column(sqlalchemy.Integer)

    name = sqlalchemy.Column(sqlalchemy.String)
    status = sqlalchemy.Column(sqlalchemy.Integer)

    data = sqlalchemy.Column(sqlalchemy.BLOB)

    line_begin = sqlalchemy.Column(sqlalchemy.Integer)
    line_end = sqlalchemy.Column(sqlalchemy.Integer)


    def __init__(self, run_part_uid, status, type):
        self.run_part_uid = run_part_uid
        self.status = status
        self.type = type
        self.name = ""
        self.data = ""
        self.line_begin = 0
        self.line_end = 0

    def __repr__(self):
        return "<DevBotnetDBRunPartDetail('%d')>" % (self.uid)
    
    def is_equal(self, other_run__part_detail):
        return self.name == other_run__part_detail.name and self.type == other_run__part_detail.type and self.status == other_run__part_detail.status and self.data == other_run__part_detail.data 
    
    def copy(self):
        new_instance = DevBotnetDBRunPartDetail(self.run_part_uid, self.status, self.type)
        new_instance.name = self.name
        new_instance.data = self.data
        new_instance.line_begin = self.line_begin
        new_instance.line_end = self.line_end
        return new_instance


def create_session(connect_string):
    engine = sqlalchemy.create_engine(connect_string, echo=False)
    Session = sqlalchemy.orm.sessionmaker(bind=engine)
    return Session()

def generate_db(connect_string):
    engine = sqlalchemy.create_engine(connect_string, echo=False)
    Base.metadata.create_all(engine) 


