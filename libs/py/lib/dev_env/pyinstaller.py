import lib.version
import os.path

TEMP_FOLDER = os.path.join(os.path.normpath(os.path.abspath(os.path.dirname(__file__))), '..', '..', '..', '..', 'gon_build', 'temp')

VERSION_FILE = """
# UTF-8
#
# For more details about fixed file info 'ffi' see:
# http://msdn.microsoft.com/en-us/library/ms646997.aspx
VSVersionInfo(
  ffi=FixedFileInfo(
# filevers and prodvers should be always a tuple with four items: (1, 2, 3, 4)
# Set not needed items to zero 0.
filevers=({ver_major}, {ver_minor}, {ver_bugfix}, {ver_build_id}),
prodvers=({ver_major}, {ver_minor}, {ver_bugfix}, {ver_build_id}),
# Contains a bitmask that specifies the valid bits 'flags'r
mask=0x3f,
# Contains a bitmask that specifies the Boolean attributes of the file.
flags=0x0,
# The operating system for which this file was designed.
# 0x4 - NT and there is no need to change it.
OS=0x4,
# The general type of file.
# 0x1 - the file is an application.
fileType=0x1,
# The function of the file.
# 0x0 - the function is not defined for this fileType
subtype=0x0,
# Creation date and time stamp.
date=(0, 0)
),
  kids=[
StringFileInfo(
  [
  StringTable(
    u'040904B0',
    [StringStruct(u'CompanyName', u'{company_name}'),
    StringStruct(u'FileDescription', u'{app_description}'),
    StringStruct(u'FileVersion', u'{ver_full}'),
    StringStruct(u'InternalName', u'{app_name}'),
    StringStruct(u'LegalCopyright', u'{company_copyright}'),
    StringStruct(u'OriginalFilename', u'{app_name}.exe'),
    StringStruct(u'ProductName', u'{app_description}'),
    StringStruct(u'ProductVersion', u'{ver_full}')])
  ]),
VarFileInfo([VarStruct(u'Translation', [1033, 1200])])
  ]
)
"""


def generate_version_file(app_py, app_name, app_description):

    current_version =  lib.version.Version.create_current()
    format_options = {
        'ver_major' : current_version.get_version_major(),
        'ver_minor' : current_version.get_version_minor(),
        'ver_bugfix' : current_version.get_version_bugfix(),
        'ver_build_id' : current_version.get_version_build_id(),
        'ver_full' : current_version.get_version_string().strip(),
        'company_name' : current_version.get_company_name(),
        'company_copyright' : current_version.get_copyright(),
        'app_name' : app_name,
        'app_description' : app_description
    }
    version_file_name = os.path.join(TEMP_FOLDER, '%s.ver' % app_py)

    version_file = open(version_file_name, "w")
    version_file.write(VERSION_FILE.format(**format_options))
    version_file.close()

    return version_file_name
