'''
This module contains common functionality for packaging 
'''
import os
import os.path


def generate_image_files(image_source_folder, extensions=['.bmp']):
    result = []
    for image_filename in os.listdir(image_source_folder):
        image_filename_abs = os.path.join(image_source_folder, image_filename)
        (root, ext) = os.path.splitext(image_filename_abs)
        if ext in extensions:
            result.append(image_filename_abs)
    return result

def generate_files(image_source_folder):
    result = []
    for image_filename in os.listdir(image_source_folder):
        image_filename_abs = os.path.join(image_source_folder, image_filename)
        if (os.path.isfile(image_filename_abs)):
            result.append(image_filename_abs)
    return result

def generate_hagi_files(dev_env):
    result = []
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'lib', 'HscCDW.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'lib', 'HscCHK.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'lib', 'HscUDB.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'lib', 'HscEXD.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'lib', 'HscEXDG4.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'bin', 'fmtcnv.exe'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'bin', 'mfc71.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'bin', 'msvcp71.dll'))
    result.append(os.path.join(dev_env.cpp_lib3p_root, 'install', 'win', 'bin', 'msvcr71.dll'))
    return result
    