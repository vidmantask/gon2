'''
Created on 02/06/2010

@author: pwl
'''
import components.database.server_common.database_api as database_api 
from appl.adsync.AdInsert import ADCreator
import plugin_types.server_management.plugin_type_token as plugin_type_token 
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.admin_ws.ws.admin_ws_services

class AdminSessionCreator(object):
    
    def __init__(self, url, username, password):
        self._url = url
        self._username = username
        self._password = password
        self.gon_admin_ws_server = None
        self.session_id = None
        
        
    def _admin_ws_login(self, gon_admin_ws_server):
        request = components.admin_ws.ws.admin_ws_services.LoginRequest()
        request_content = components.admin_ws.ws.admin_ws_services_types.ns0.LoginRequestType_Def(None).pyclass()
        request_content.set_element_username(self._username)
        request_content.set_element_password(self._password)
        request.set_element_content(request_content)
        response = gon_admin_ws_server.Login(request)
        return response._content.get_element_session_id()
    
    def ping_session(self):
        request = components.admin_ws.ws.admin_ws_services.PingRequest()
        request_content = components.admin_ws.ws.admin_ws_services_types.ns0.PingRequestType_Def(None).pyclass()
        request_content.set_element_session_id(self.session_id)
        request.set_element_content(request_content)
        response = self.gon_admin_ws_server.Ping(request)
        return response._content.get_element_rc()
        
    def get_admin_session_and_session_id(self):
        if not self.gon_admin_ws_server or not self.session_id or not self.ping_session():
            self.gon_admin_ws_server = components.admin_ws.ws.admin_ws_services.BindingSOAP(self._url)
            self.session_id = self._admin_ws_login(self.gon_admin_ws_server)
        return self.gon_admin_ws_server, self.session_id

def call_enroll_token(internal_ws_connector, template, user_id, user_plugin, enroll_as_endpoint, activate_rule, unique_session_id):
    gon_admin_ws_server, session_id = internal_ws_connector.get_admin_session_and_session_id()
    request = components.admin_ws.ws.admin_ws_services.EnrollDeviceToUserRequest()
    request_content = components.admin_ws.ws.admin_ws_services_types.ns0.EnrollDeviceToUserRequestType_Def(None).pyclass()
    request_content.set_element_session_id(session_id)
    request_content.set_element_external_user_id(user_id)
    request_content.set_element_user_plugin(user_plugin)
    request_content.set_element_device_template(template)
    request_content.set_element_activate_rule(activate_rule)
    request_content.set_element_enroll_as_endpoint(enroll_as_endpoint)
    request_content.set_element_unique_session_id(unique_session_id)
    request.set_element_content(request_content)
    response = gon_admin_ws_server.EnrollDeviceToUser(request)
    return response

def get_template(internal_ws_connector, entity_type):
    gon_admin_ws_server, session_id = internal_ws_connector.get_admin_session_and_session_id()
    request = components.admin_ws.ws.admin_ws_services.GetConfigTemplateRequest()
    request_content = components.admin_ws.ws.admin_ws_services_types.ns0.GetConfigTemplateRequestType_Def(None).pyclass()
    request_content.set_element_session_id(session_id)
    request_content.set_element_entity_type(entity_type)
    request.set_element_content(request_content)
    response = gon_admin_ws_server.GetConfigTemplate(request)
    return response._content._config_template

IDENT_NAME = 'name'
IDENT_TYPE = 'plugin_type'
IDENT_SERIAL_ID = 'serial_id'
IDENT_SERIAL = 'serial'
IDENT_DESCRIPTION = 'description'
IDENT_CASING_NUMBER = 'casing_number'
IDENT_PUBLIC_KEY = 'public_key'
IDENT_ENDPOINT = 'endpoint'
        

def create_users_and_tokens(admin_ws_service_url, root_dns, ou_name, public_key, count):
    creator = ADCreator(root_dns, ou_name)
    users = creator.add_new_users(count)
#    users = [("S-1-5-21-2981504786-2415913618-1285249951-2686", "yo1")]
    internal_ws_connector = AdminSessionCreator(admin_ws_service_url, "Username", "Password")
    entity_type = "SoftToken"
    for (user_id, user_name) in users:
        template = get_template(internal_ws_connector, entity_type)
        template = ConfigTemplateSpec.copy_template(template)
        
#                template = self._get_config_spec().get_template()
#                template.set_attribute_entity_type(entity_type)

        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value(IDENT_NAME, "token_%s" % user_name)
        config_spec.set_value(IDENT_SERIAL, "serial_%s" % user_name)
        config_spec.set_value(IDENT_TYPE, "soft_token")
        config_spec.set_value(IDENT_PUBLIC_KEY, public_key)
#        config_spec.set_value(IDENT_DESCRIPTION, "Enrolled by %s" % user.user_login)
        config_spec.set_value(IDENT_ENDPOINT, False)

        activate_rule = True
        unique_session_id = None
        enroll_as_endpoint = False
        user_plugin_name = "ad_%s" % (root_dns.replace(".","_"))
        
        response = call_enroll_token(internal_ws_connector, template, user_id, user_plugin_name, enroll_as_endpoint, activate_rule, unique_session_id)

        



if __name__ == '__main__':
    print "yo"
    url = "http://127.0.0.1:18072"
    root_dns = "devtest.giritech.com" 
    ou_name = "Test"
    public_key = "abc"
    count = 1
    
    create_users_and_tokens(url, root_dns, ou_name, public_key, count)