"""
Module for generating source files in devbotnet

SET(ADMIN_WS_SPECIFICATION_DIR ${GIRITECH_COMMON_ROOT_DIR}/admin_ws)
SET(ADMIN_WS_PY_DEST ${CMAKE_CURRENT_SOURCE_DIR}/ws)
ADD_CUSTOM_TARGET(DEPLOY_PY_ADMIN_WS
  COMMAND  ${GIRITECH_CMD_WSDL2PY} -b -f binding.wsdl -o ${ADMIN_WS_PY_DEST}
  COMMAND  ${GIRITECH_CMD_WSDL2DISPATCH} -f binding.wsdl -o ${ADMIN_WS_PY_DEST}
  WORKING_DIRECTORY ${ADMIN_WS_SPECIFICATION_DIR}
)
#
# Macro for adding ws generation targets
# 
MACRO(ADD_WS_TARGET WS_ID WS_SPEC_DIR WS_COMPONENT_ID)
IF(GIRITECH_ENV_CMD)
  ADD_CUSTOM_TARGET(DEPLOY_JAVA_${WS_ID}
    COMMAND set AXIS2_HOME=${GITITECH_HOME_AXIS2}
    COMMAND echo "${COMMAND_WSDL2JAVA}" -uri ./binding.wsdl -o "${GITITECH_JAVA_COMMON_HOME}" -u
    COMMAND ${COMMAND_WSDL2JAVA} -uri ./binding.wsdl -o "${GITITECH_JAVA_COMMON_HOME}" -u -p ${WS_COMPONENT_ID} -or
    WORKING_DIRECTORY ${WS_SPEC_DIR}
  ) 
ENDIF(GIRITECH_ENV_CMD)

IF(GIRITECH_ENV_BASH)
  ADD_CUSTOM_TARGET(DEPLOY_JAVA_${WS_ID}
    COMMAND export AXIS2_HOME=${GITITECH_HOME_AXIS2}
    COMMAND echo "${COMMAND_WSDL2JAVA}" -uri ./binding.wsdl -o "${GITITECH_JAVA_COMMON_HOME}" -u 
    COMMAND ${COMMAND_WSDL2JAVA} -uri ./binding.wsdl -o "${GITITECH_JAVA_COMMON_HOME}" -u  -p ${WS_COMPONENT_ID} -or
    WORKING_DIRECTORY ${WS_SPEC_DIR}
  ) 
ENDIF(GIRITECH_ENV_BASH)
ENDMACRO(ADD_WS_TARGET WS_ID WS_SPEC_DIR WS_COMPONENT_ID)

"""
import os.path
import subprocess

import lib.dev_env.tools


def source_gen_wsdl_to_py(tools_setup, wsdl_filename, out_folder):
    work_folder = os.path.dirname(wsdl_filename)

    env = os.environ
    env['PYTHONPATH'] = lib.dev_env.path_setup.generate_path()
    command = [tools_setup.get_wsdl2py(), '-b', '-f', wsdl_filename, '-o', out_folder]
    print command
    result = subprocess.call(command, cwd=work_folder, env=env)
    if result != 0:
        return False

    command = [tools_setup.get_wsdl2dispatch(), '-f', wsdl_filename, '-o', out_folder]
    print command
    result = subprocess.call(command, cwd=work_folder, env=env)
    if result != 0:
        return False
    return True


def source_gen_wsdl_to_java(tools_setup, wsdl_filename, component_id):
    work_folder = os.path.dirname(wsdl_filename)
    giritech_common_home_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..', 'java', 'common')

    env = os.environ
    env['AXIS2_HOME'] = tools_setup.get_axis2_home()
    env['JAVA_HOME'] = tools_setup.get_java_home()
    command = [tools_setup.get_wsdl2java(), '-uri', wsdl_filename, '-o', giritech_common_home_folder, '-u', '-p', component_id, '-or']
    print command
    result = subprocess.call(command, cwd=work_folder, env=env)
    if result != 0:
        return False
    return True


if __name__ == '__main__':
    wsdl_filename_test = "/home/twa/source/main/common/admin_ws/binding.wsdl"
    
    temp_folder = "/tmp/test_wsdl"

    tools_setup = lib.dev_env.tools.ToolsSetup.create_default()
    
    print source_gen_wsdl_to_py(tools_setup, wsdl_filename_test, temp_folder)
    print source_gen_wsdl_to_java(tools_setup, wsdl_filename_test, 'test')
    
    