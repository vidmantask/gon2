'''
This module run a single unittest.
'''
import os.path
import sys
import StringIO
import os

import optparse
import unittest
import pickle
import lib.giri_unittest


import path_setup

HG_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..', '..'))
ROOT_PY = os.path.join(HG_ROOT, 'py')

class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--modulename', type='string', default=None,  help='Modulename of unitestsuite to run')
        self._parser.add_option('--out', type='string', default=None,  help='Filename of where output is written')
        self._parser.add_option('--profile_filename', type='string', default=None,  help='Filename of where output of profiling is written')
        self._parser.add_option('--coverage_filename', type="string", default=None,  help='Filename for coverage')

        (self._options, self._args) = self._parser.parse_args()

    def get_modulename(self):
        return self._options.modulename

    def get_out(self):
        return self._options.out

    def get_profile_filename(self):
        return self._options.profile_filename

    def get_coverage_filename(self):
        return self._options.coverage_filename


#def _extrac_errors(errors):
#    result = ""
#    for (error_testcase, error_traceback) in errors:
#        result += "%s\n%s\n%s\n\n" % (error_testcase.id(), error_testcase.shortDescription(), error_traceback)
#    return result

def _extrac_errors(errors, cases_error):
    results = []
    for (error_testcase, error_traceback) in errors:
        result = {}
        result['name'] = error_testcase.id()
        result['description'] = error_testcase.shortDescription()
        result['text'] = error_traceback
        result['error'] = True
        result['info'] = False
        result['warning'] = False
        result['ok'] = False
        results.append(result)
        cases_error.append(error_testcase)
    return results

def _extrac_ok(testcase):
    result = {}
    result['name'] = testcase.id()
    result['description'] = ""
    result['text'] = ""
    result['error'] = False
    result['info'] = False
    result['ok'] = True
    result['warning'] = False
    return result

globalTestsuiteRunHelper = None
class TestsuiteRunHelper(object):
    """
    This class only wraps execution of unittest in one function for profiling
    """
    def __init__(self, testsuite_runner, testsuite):
        self.testsuite_runner = testsuite_runner
        self.testsuite = testsuite
        self.testsuite_result = None

    def run(self):
        try:
            self.testsuite_result = self.testsuite_runner.run(self.testsuite)
        finally:
            lib.giri_unittest.close()


def run_unittest(modulename, profile_filename=None, coverage_filename=None):
    testsuite_output = StringIO.StringIO()

    if coverage_filename is not None:
        import coverage
        cov = coverage.coverage(data_file=coverage_filename)
        cov.load()
        cov.start()

    testsuite = unittest.defaultTestLoader.loadTestsFromName(modulename)
    testsuite_runner = unittest.TextTestRunner(stream=testsuite_output, verbosity=3)

    global globalTestsuiteRunHelper
    globalTestsuiteRunHelper = TestsuiteRunHelper(testsuite_runner, testsuite)

    rc = None
    if profile_filename is not None:
        import cProfile
        cProfile.run("globalTestsuiteRunHelper.run()", profile_filename)
    else:
        globalTestsuiteRunHelper.run()
        rc = globalTestsuiteRunHelper.testsuite_result

    if coverage_filename is not None:
        cov.stop()
        cov.save()

    testsuite_result = globalTestsuiteRunHelper.testsuite_result

    output = {}
    output['modulename'] = modulename
    output['exec_out'] = testsuite_output.getvalue()

    cases_error = []
    cases = []
    cases.extend(_extrac_errors(testsuite_result.errors, cases_error))
    cases.extend(_extrac_errors(testsuite_result.failures, cases_error))
    for testcase in testsuite:
        for testcase_element in testcase:
            if not testcase_element in cases_error:
                cases.append(_extrac_ok(testcase_element))
    output['cases'] = cases

    ok = False
    if rc is not None and len(rc.errors) == 0 and len(rc.failures) == 0:
        ok = True
    return output, ok


def main():
    commandline_options = CommandlineOptions()

    if commandline_options.get_modulename() is None:
        print "Invalid filename argument"
        return 1

    if commandline_options.get_out() is None:
        print "Invalid out-filename argument"
        return 1

    output, ok = run_unittest(commandline_options.get_modulename(), commandline_options.get_profile_filename(), commandline_options.get_coverage_filename())

    output_file = open(commandline_options.get_out(), 'w')
    pickle.dump(output, output_file)
    output_file.close()
    if ok:
        return 0
    return 1

if __name__ == '__main__':
    rc = main()
    sys.exit(rc)
