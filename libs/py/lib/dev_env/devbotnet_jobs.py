'''
This module
'''
import sys
import os
import os.path
import tempfile
import threading
import glob
import unittest
import traceback
import StringIO
import subprocess
import time
import shutil
import pickle
import datetime
import zipfile
import base64
import pstats
import coverage

import devbotnet_db

HG_ROOT = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..")))


def read_file_content(filename):
    if os.path.exists(filename):
        myfile = open(filename, 'r')
        content = myfile.read()
        return content
    return ""

def print_job_info_summary(job_info):
    print "SUMMARY - SUMMARY - SUMMARY - SUMMARY - SUMMARY - SUMMARY - SUMMARY - SUMMARY - SUMMARY"
    total_counter = 0
    total_counter_warning = 0
    total_counter_error = 0
    for part_info in job_info.part_infos:
        error_mark = ''
        if part_info.counter_error > 0:
            error_mark = 'ERROR'
        print '%-70s(%3d)  W:%3d  E:%3d %s' % (part_info.modulename, part_info.counter, part_info.counter_warning, part_info.counter_error, error_mark)
        total_counter += part_info.counter
        total_counter_warning += part_info.counter_warning
        total_counter_error += part_info.counter_error
    print '%-70s(%3d)  W:%3d  E:%3d' % ('Total', total_counter, total_counter_warning, total_counter_error)



class Job(threading.Thread):
    JOB_STATUS_INIT = -1
    JOB_STATUS_RUNNING = 0
    JOB_STATUS_DONE = 1
    JOB_STATUS_DONE_WITH_ERRORS = 2
    JOB_STATUS_DONE_WITH_WARNINGS = 3

    def __init__(self, temp_root):
        threading.Thread.__init__(self, name='Job')
        self._running = False
        self._stop = False
        self._status = Job.JOB_STATUS_INIT
        self.temp_root = temp_root
        self.db_filename = None

        if not os.path.isdir(temp_root):
            os.makedirs(temp_root)

        (self.stdout_file, self.stdout_filename) = tempfile.mkstemp('log', '_job_out_', temp_root, True)
        (self.stderr_file, self.stderr_filename) = tempfile.mkstemp('log', '_job_err_', temp_root, True)

    def run(self):
        print "Job(%s), begin" % self.name
        self._status = Job.JOB_STATUS_RUNNING
        self._running = True
        self._status = self.do_run()
        os.close(self.stdout_file)
        os.close(self.stderr_file)
        print "Job(%s) end" % self.name
        self._running = False

    def do_run(self):
        return Job.JOB_STATUS_DONE

    def do_stop(self):
        self._stop = True

    def get_status(self):
        return self._status

    def set_db_filename(self, db_filename):
        self.db_filename = db_filename

    def get_db_filename(self):
        return self.db_filename


class JobDoUnittestPart(threading.Thread):
    JOB_STATUS_INIT = -1
    JOB_STATUS_RUNNING = 0
    JOB_STATUS_DONE = 1

    def __init__(self, dev_env, db_session, devbotnet_db_run, filename, modulename, enable_profiling=False, enable_details=True, coverage_filename=None):
        threading.Thread.__init__(self, name='JobDoUnittestPart')
        self.dev_env = dev_env
        self.db_session = db_session
        self.devbotnet_db_run = devbotnet_db_run
        self.status = JobDoUnittestPart.JOB_STATUS_INIT
        self.filename = filename
        self.modulename = modulename
        self.enable_profiling = enable_profiling
        self.enable_details = enable_details
        self.enable_coverage = coverage_filename is not None
        self.coverage_filename = coverage_filename

    def run(self):
        self.devbotnet_db_run_part = devbotnet_db.DevBotnetDBRunPart(self.devbotnet_db_run.uid, self.modulename, self.filename)
        self.devbotnet_db_run_part.begin_ts = datetime.datetime.utcnow()
        self.db_session.add(self.devbotnet_db_run_part)
        self.db_session.commit()

        self.status = JobDoUnittestPart.JOB_STATUS_RUNNING
        self._run_load_module()
        if self.status == JobDoUnittestPart.JOB_STATUS_RUNNING:
            self._run_unittest()
        self.status = JobDoUnittestPart.JOB_STATUS_DONE

        self.devbotnet_db_run_part.end_ts = datetime.datetime.utcnow()
        self.db_session.commit()

    def _run_load_module(self):
        try:
            py_module = __import__(self.modulename, fromlist=[''], globals=globals(), locals=locals())
            self.ignore = self._get_modul_attr(py_module, 'GIRI_UNITTEST_IGNORE', False)
            self.owner = self._get_modul_attr(py_module, 'GIRI_UNITTEST_OWNER', 'unknown')
            self.timeout = self._get_modul_attr(py_module, 'GIRI_UNITTEST_TIMEOUT', 60)
            self.tags = self._get_modul_attr(py_module, 'GIRI_UNITTEST_TAGS', [])
        except Exception as error:
            print error
            devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDERROR)
            devbotnet_db_run_part_detail.data = traceback.format_exc()
            self.db_session.add(devbotnet_db_run_part_detail)
            self.db_session.commit()
            self.status = JobDoUnittestPart.JOB_STATUS_DONE

    def _get_modul_attr(self, py_module, attr_name, default_value):
        if hasattr(py_module, attr_name):
            return getattr(py_module, attr_name)
        return default_value

    def _run_unittest_parse_result(self, output):
        std_out = output['exec_out']
        if std_out != "":
            devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDOUT)
            devbotnet_db_run_part_detail.data = std_out
            print(std_out)
            self.db_session.add(devbotnet_db_run_part_detail)
            self.db_session.commit()

        if self.enable_details:
            for test_case in output['cases']:
                if test_case['error']:
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                if test_case['warning']:
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_WARNING, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                if test_case['info']:
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                if test_case['ok']:
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_OK, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                devbotnet_db_run_part_detail.name = test_case['name']
                devbotnet_db_run_part_detail.data = test_case['text']
                self.db_session.add(devbotnet_db_run_part_detail)
            self.db_session.commit()

    def _run_unittest(self):
        try:
            print "Running", self.modulename
            if self.ignore:
                devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_WARNING, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                devbotnet_db_run_part_detail.data = "Unittest ignored"
                self.db_session.add(devbotnet_db_run_part_detail)
                self.db_session.commit()
                self.status = JobDoUnittestPart.JOB_STATUS_DONE
                return

            tempfolder = tempfile.mkdtemp()
            stdout_filename = os.path.join(tempfolder, 'stdout')
            stdout_file = open(stdout_filename, 'w')
            stderr_filename = os.path.join(tempfolder, 'stderr')
            stderr_file = open(stderr_filename, 'w')

            output_filename = os.path.join(tempfolder, 'output')
            profile_filename = os.path.join(tempfolder, 'profile')

            env = os.environ
            env['PYTHONPATH'] = self.dev_env.py_root
            command = [self.dev_env.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'lib', 'dev_env', 'unittest_runner.py'), '--modulename', self.modulename, '--out', output_filename]

            if self.enable_profiling:
                command.append('--profile_filename')
                command.append(profile_filename)
            if self.enable_coverage:
                command.append('--coverage_filename')
                command.append(self.coverage_filename)

            process = subprocess.Popen(command, env=env, stdout=stdout_file, stderr=stderr_file)
            timeout_count_down = self.timeout
            while(process.poll() is None and timeout_count_down > 0):
                timeout_count_down -= 1
                time.sleep(1)

            process_was_terminated = False
            if process.poll() is None:
                process.terminate()
                time.sleep(2)
                process_was_terminated = True

            stdout_file.close()
            stderr_file.close()

            if os.path.exists(output_filename):
                output_file = open(output_filename, 'r')
                output = pickle.load(output_file)
                output_file.close()
                self._run_unittest_parse_result(output)

            if os.path.exists(stdout_filename):
                stdout_file = open(stdout_filename, 'r')
                stdout = stdout_file.read()
                stdout_file.close()
                if stdout != "":
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDOUT)
                    devbotnet_db_run_part_detail.data = stdout
                    self.db_session.add(devbotnet_db_run_part_detail)
                    self.db_session.commit()
                    stdout_file.close()

            if os.path.exists(stderr_filename):
                stderr_file = open(stderr_filename, 'r')
                stderr = stderr_file.read()
                stderr_file.close()
                if stderr != "":
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDERROR)
                    devbotnet_db_run_part_detail.data = stderr
                    self.db_session.add(devbotnet_db_run_part_detail)
                    self.db_session.commit()

            if self.enable_profiling and os.path.exists(profile_filename):
                profile_file = open(profile_filename, 'r')
                devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_PROFILE)
                devbotnet_db_run_part_detail.name = '%s.prof' % self.modulename
                devbotnet_db_run_part_detail.data = profile_file.read()
                self.db_session.add(devbotnet_db_run_part_detail)
                self.db_session.commit()
                profile_file.close()

            if process_was_terminated:
                devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
                devbotnet_db_run_part_detail.data = "Terminated because of timeout"
                self.db_session.add(devbotnet_db_run_part_detail)
                self.db_session.commit()


#            try:
#                if os.path.isdir(tempfolder):
#                    shutil.rmtree(tempfolder)
#            except:
#                pass

        except:
            devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(self.devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE)
            devbotnet_db_run_part_detail.data = traceback.format_exc()
            self.db_session.add(devbotnet_db_run_part_detail)
            self.db_session.commit()

            self.status = JobDoUnittestPart.JOB_STATUS_DONE
            print traceback.format_exc()



class JobDoUnittest(Job):
    def __init__(self, dev_env, db_session, ifilter=None, efilter=None, devbotnet_slave_info=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.db_session = db_session
        self.devbotnet_slave_info = devbotnet_slave_info
        self.ifilter = ifilter
        self.efilter = efilter

    def do_run(self):
        devbotnet_db_run = devbotnet_db.DevBotnetDBRun(0, devbotnet_db.DevBotnetDBRun.TYPE_UNITTEST)
        devbotnet_db_run.begin_ts = datetime.datetime.utcnow()
        if self.devbotnet_slave_info is not None:
            devbotnet_db_run.computer = self.devbotnet_slave_info.name
            devbotnet_db_run.arch = self.devbotnet_slave_info.arch
            devbotnet_db_run.arch_target = self.devbotnet_slave_info.arch_target
        self.db_session.add(devbotnet_db_run)
        self.db_session.commit()

        self.dev_env.cleanup_temp()
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest'), self.ifilter, self.efilter)
        for (py_filename, py_modulename) in unittests:
            unittest_job = JobDoUnittestPart(self.dev_env, self.db_session, devbotnet_db_run, py_filename, py_modulename)
            unittest_job.start()
            unittest_job.join()

        devbotnet_db_run.end_ts = datetime.datetime.utcnow()
        self.db_session.commit()
        return Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return Job.get_job_info(self, self.job_info)



class JobDoStaticAnalysis(Job):
    def __init__(self, dev_env, db_session, ifilter=None, efilter=None, devbotnet_slave_info=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.db_session = db_session
        self.ifilter = ifilter
        self.efilter = efilter
        self.devbotnet_slave_info = devbotnet_slave_info
        self._ignore_modules = None

    def do_run(self):
        py_lint_config_filname = os.path.join(self.dev_env.py_root, 'setup', 'tools', 'pylint', 'pylint.rcfile')

        devbotnet_db_run = devbotnet_db.DevBotnetDBRun(0, devbotnet_db.DevBotnetDBRun.TYPE_STATIC_ANALYSIS)
        devbotnet_db_run.begin_ts = datetime.datetime.utcnow()
        if self.devbotnet_slave_info is not None:
            devbotnet_db_run.computer = self.devbotnet_slave_info.name
            devbotnet_db_run.arch = self.devbotnet_slave_info.arch
            devbotnet_db_run.arch_target = self.devbotnet_slave_info.arch_target
        self.db_session.add(devbotnet_db_run)
        self.db_session.commit()

        for (py_filename, py_modulename) in self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest', exclude_prefix=True), self.ifilter, self.efilter):
            if self._do_ignore_module(py_modulename):
                continue

            devbotnet_db_run_part = devbotnet_db.DevBotnetDBRunPart(devbotnet_db_run.uid, py_modulename, py_filename)
            devbotnet_db_run_part.begin_ts = datetime.datetime.utcnow()
            self.db_session.add(devbotnet_db_run_part)
            self.db_session.commit()

            env = os.environ
            env['PYTHONPATH'] = self.dev_env.py_root

            (stdout_file, stdout_filename) = tempfile.mkstemp(dir=self.temp_root, text=True)
            (stderr_file, stderr_filename) = tempfile.mkstemp(dir=self.temp_root, text=True)
            command = [self.dev_env.tools_setup.get_pylint(), '--rcfile', py_lint_config_filname, py_filename]

            subprocess.call(command, stdout=stdout_file, stderr=stderr_file, env=env)
            devbotnet_db_run_part.end_ts = datetime.datetime.utcnow()

            std_out = read_file_content(stdout_filename)
            if std_out != "":
                devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDOUT)
                devbotnet_db_run_part_detail.data = std_out
                self.db_session.add(devbotnet_db_run_part_detail)
                self.db_session.commit()

            std_error = read_file_content(stderr_filename)
            if std_error != "":
                devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDERROR)
                devbotnet_db_run_part_detail.data = std_out
                self.db_session.add(devbotnet_db_run_part_detail)
                self.db_session.commit()

            self._parse_result(devbotnet_db_run_part, stdout_filename)

            os.close(stdout_file)
            os.close(stderr_file)
            os.unlink(stdout_filename)
            os.unlink(stderr_filename)

        devbotnet_db_run.end_ts = datetime.datetime.utcnow()
        self.db_session.commit()
        return Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return Job.get_job_info(self, self.job_info)

    def _do_ignore_module(self, py_modulename):
        if self._ignore_modules is None:
            self._ignore_modules = self._load_ignore_module_ini()
        return py_modulename in self._ignore_modules

    def _load_ignore_module_ini(self):
        ignore_modules = []
        ignore_module_filename = os.path.join(self.dev_env.hg_root, 'config_analysis_ignore.ini')
        if os.path.exists(ignore_module_filename):
            ignore_module_file = open(ignore_module_filename, 'r')
            section_found = False
            for ignore_module_file_line in ignore_module_file.read().splitlines():
                if ignore_module_file_line.startswith("["):
                    if ignore_module_file_line == "[%s]" % sys.platform:
                        section_found = True
                    else:
                        section_found = False
                if section_found and len(ignore_module_file_line) > 0 and ignore_module_file_line[0] not in ['[', '#']:
                    ignore_modules.append(ignore_module_file_line)
            ignore_module_file.close()
        return ignore_modules

    def _parse_result(self, devbotnet_db_run_part, filename):
        line_idx = 0
        content = read_file_content(filename)
        if content is not None:
            for content_line in content.splitlines():
                line_idx += 1

                content_line_elements = content_line.split(':')
                if len(content_line_elements) >= 3:
                    content_line_id = content_line_elements[0].strip()
                    content_line_id_type = content_line_elements[0].strip()[0]
                    content_line_line = content_line_elements[1].strip()
                    content_line_message = ':'.join(content_line_elements[2:])

                    content_line_line_elements = content_line_line.split(',')
                    if len(content_line_line_elements) > 1:
                        content_line_line = content_line_line_elements[0]

                    if content_line_id_type in ['E', 'W', 'R', 'F', 'C'] :
                        if content_line_id_type == 'E':
                            status = devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR
                        elif content_line_id_type == 'W':
                            status = devbotnet_db.DevBotnetDBRunPartDetail.STATUS_WARNING
                        else:
                            status = devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO

                        devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(devbotnet_db_run_part.uid, status, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_LINE)
                        devbotnet_db_run_part_detail.name = content_line_id
                        devbotnet_db_run_part_detail.data = content_line_message
                        devbotnet_db_run_part_detail.line_begin = int(content_line_line)
                        devbotnet_db_run_part_detail.line_end = int(content_line_line)
                        self.db_session.add(devbotnet_db_run_part_detail)


class JobDoCoverage(Job):
    def __init__(self, dev_env, db_session, ifilter=None, efilter=None, devbotnet_slave_info=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.db_session = db_session
        self.devbotnet_slave_info = devbotnet_slave_info
        self.ifilter = ifilter
        self.efilter = efilter

    def do_run(self):
        devbotnet_db_run = devbotnet_db.DevBotnetDBRun(0, devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE)
        devbotnet_db_run.begin_ts = datetime.datetime.utcnow()
        if self.devbotnet_slave_info is not None:
            devbotnet_db_run.computer = self.devbotnet_slave_info.name
            devbotnet_db_run.arch = self.devbotnet_slave_info.arch
            devbotnet_db_run.arch_target = self.devbotnet_slave_info.arch_target
        self.db_session.add(devbotnet_db_run)
        self.db_session.commit()

        import coverage
        cov_filename = self.dev_env.generate_temp_filename()
        print cov_filename
        cov = coverage.coverage(data_file=cov_filename)
#        cov = coverage.coverage()
        cov.erase()

        self.dev_env.cleanup_temp()
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest'), self.ifilter, self.efilter)
        for (py_filename, py_modulename) in unittests:
            unittest_job = JobDoUnittestPart(self.dev_env, self.db_session, devbotnet_db_run, py_filename, py_modulename, enable_profiling=False, enable_details=False, coverage_filename=cov_filename)
            unittest_job.start()
            unittest_job.join()
        cov = coverage.coverage(data_file=cov_filename)
        cov.load()
        cov.html_report(ignore_errors=1)

        devbotnet_db_run.end_ts = datetime.datetime.utcnow()
        self.db_session.commit()

        self._generate_coverage_result(devbotnet_db_run, cov)
        return Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return Job.get_job_info(self, self.job_info)

    def _generate_coverage_result(self, devbotnet_db_run, cov):
        cov.load()
        cov.html_report()

#        cov.html_report(ignore_errors=1)
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest', exclude_prefix=True), self.ifilter, self.efilter)
        for (py_filename, py_modulename) in unittests:
            (cov_filename, cov_lines_exec, cov_lines_exclude, cov_lines_not_exec, cov_lines_not_exec_string) = cov.analysis2(py_filename)
            print cov_filename, cov_lines_exec, cov_lines_exclude, cov_lines_not_exec, cov_lines_not_exec_string
            devbotnet_db_run_part = devbotnet_db.DevBotnetDBRunPart(devbotnet_db_run.uid, py_modulename, py_filename)
            devbotnet_db_run_part.begin_ts = datetime.datetime.utcnow()
            devbotnet_db_run_part.end_ts = datetime.datetime.utcnow()
            self.db_session.add(devbotnet_db_run_part)
            self.db_session.commit()

            if os.path.exists(cov_filename):
                cov_fil = open(cov_filename, 'rb')
                cov_file_line = 1
                cov_file_line_string = cov_fil.readline()
                while cov_file_line_string != "":
                    devbotnet_db_run_part_detail = devbotnet_db.DevBotnetDBRunPartDetail(devbotnet_db_run_part.uid, devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_LINE)
                    devbotnet_db_run_part_detail.data = cov_file_line_string
                    devbotnet_db_run_part_detail.line_begin = cov_file_line
                    devbotnet_db_run_part_detail.line_end = cov_file_line
                    if cov_file_line in cov_lines_exec:
                        devbotnet_db_run_part_detail.status = devbotnet_db.DevBotnetDBRunPartDetail.STATUS_OK
                    if cov_file_line in cov_lines_not_exec:
                        devbotnet_db_run_part_detail.status = devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR
                    self.db_session.add(devbotnet_db_run_part_detail)
                    cov_file_line_string = cov_fil.readline()
                    cov_file_line += 1
                cov_fil.close()





class JobDoProfile(Job):
    def __init__(self, dev_env, db_session, ifilter=None, efilter=None, devbotnet_slave_info=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.db_session = db_session
        self.devbotnet_slave_info = devbotnet_slave_info
        self.ifilter = ifilter
        self.efilter = efilter

    def do_run(self):
        devbotnet_db_run = devbotnet_db.DevBotnetDBRun(0, devbotnet_db.DevBotnetDBRun.TYPE_PROFILE)
        devbotnet_db_run.begin_ts = datetime.datetime.utcnow()
        if self.devbotnet_slave_info is not None:
            devbotnet_db_run.computer = self.devbotnet_slave_info.name
            devbotnet_db_run.arch = self.devbotnet_slave_info.arch
            devbotnet_db_run.arch_target = self.devbotnet_slave_info.arch_target
        self.db_session.add(devbotnet_db_run)
        self.db_session.commit()

        self.dev_env.cleanup_temp()
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='profile'), self.ifilter, self.efilter)
        for (py_filename, py_modulename) in unittests:
            unittest_job = JobDoUnittestPart(self.dev_env, self.db_session, devbotnet_db_run, py_filename, py_modulename, enable_profiling=True, enable_details=False)
            unittest_job.start()
            unittest_job.join()

        devbotnet_db_run.end_ts = datetime.datetime.utcnow()
        self.db_session.commit()
        return Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return Job.get_job_info(self, self.job_info)



if __name__ == '__main__':
    import pickle
    import lib.dev_env
    import lib.dev_env.tools
    import lib.version

    import lib.dev_env.devbotnet_db

    version = lib.version.Version.create_current()
    dev_env = lib.dev_env.DevEnv(lib.dev_env.tools.ToolsSetup.create_default(), HG_ROOT, version)

    db_connect_string = 'sqlite:///'+ HG_ROOT + '/devbotnet.sqlite'
#    db_connect_string = 'sqlite:///:memory:'
    lib.dev_env.devbotnet_db.generate_db(db_connect_string)
    db_session = lib.dev_env.devbotnet_db.create_session(db_connect_string)

#    job = JobDoUnittest(dev_env, db_session, ifilter=".*", efilter="")
#    job = JobDoProfile(dev_env, db_session, ifilter=".*gpm.*", efilter="")
    job = JobDoCoverage(dev_env, db_session, ifilter=".*", efilter="")
#    job = JobDoStaticAnalysis(dev_env, db_session, ifilter=".*gpm.*", efilter="")
    job.run()
