'''
Created on 31/08/2011

@author: twa
'''
import sys
import os.path
import site

HG_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..'))
ROOT_PY = os.path.join(HG_ROOT, 'py')

EXT_PY_NOARC = os.path.join(HG_ROOT, 'py', 'setup', 'py_ext', 'site-packages','plat_noarc')

EXT_PY_PLAT = None
if sys.platform == 'win32':
    EXT_PY_PLAT = os.path.join(HG_ROOT, 'py', 'setup', 'py_ext', 'site-packages','plat_win')
elif sys.platform == 'linux2':
    EXT_PY_PLAT = os.path.join(HG_ROOT, 'py', 'setup', 'py_ext', 'site-packages','plat_linux')
elif sys.platform == 'darwin':
    EXT_PY_PLAT = os.path.join(HG_ROOT, 'py', 'setup', 'py_ext', 'site-packages','plat_mac')

if ROOT_PY in sys.path:
    sys.path.remove(ROOT_PY) 
sys.path.insert(0, ROOT_PY)

site.addsitedir(EXT_PY_NOARC)
if EXT_PY_PLAT is not None:
    site.addsitedir(EXT_PY_PLAT)

def generate_path() :
    path = ""
    path += ROOT_PY
    path += os.path.pathsep +  EXT_PY_NOARC
    if EXT_PY_PLAT is not None:
        path += os.path.pathsep + EXT_PY_PLAT
    return path

