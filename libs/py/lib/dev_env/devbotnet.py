'''
'''
import sys
import os.path
import pickle
import ConfigParser
import traceback
import threading
import subprocess

import time
import SimpleXMLRPCServer
import xmlrpclib
import datetime
import zipfile
import base64

import lib.dev_env.devbotnet_jobs
import lib.dev_env.hg
import lib.dev_env.devbotnet_db
import lib.dev_env.devbotnet_db_update
import lib.portscan

DEVBOTNET_SLAVE_IP_RANGE = [('127.0.0.1', '127.0.0.1')]
DEVBOTNET_SLAVE_PORT_RANGE = []


ARCH_TARGET_LINUX = 'linux'
ARCH_TARGET_LINUX_64 = 'linux_64'
ARCH_TARGET_WIN = 'win'
ARCH_TARGET_WIN_64 = 'win'
ARCH_TARGET_MAC = 'mac'
ARCH_TARGET_IPHONE = 'iphone'
ARCH_TARGET_VALID = [ARCH_TARGET_LINUX, ARCH_TARGET_LINUX_64, ARCH_TARGET_WIN, ARCH_TARGET_WIN_64, ARCH_TARGET_MAC, ARCH_TARGET_IPHONE]


def is_64bits():
    return sys.maxsize > 2**32


def guess_arch_target():
    if sys.platform == 'win32':
        return ARCH_TARGET_WIN
    if sys.platform == 'linux2':
        if is_64bits():
            return ARCH_TARGET_LINUX_64
        return ARCH_TARGET_LINUX
    if sys.platform == 'darwn':
        return ARCH_TARGET_MAC



def read_file_content(filename):
    if os.path.exists(filename):
        myfile = open(filename, 'rb')
        content = myfile.read()
        return content
    return None


def encode_file_before_transport(filename):
    if not os.path.exists(filename):
        return ""

    zip_filename = filename + '.zip'
    zip_file = zipfile.ZipFile(zip_filename,  'w')
    zip_file.write(filename, 'transport_file')
    zip_file.close()

    result = base64.b64encode(read_file_content(zip_filename))
    os.unlink(zip_filename)
    return result


def decode_file_after_transport(data, dev_env):
    try:
        temp_filename = dev_env.generate_temp_filename()

        temp_file  = open(temp_filename, 'wb')
        temp_file.write(base64.b64decode(data))
        temp_file.close()

        temp_result_filename  = temp_filename + '.org';
        temp_result_file = open(temp_result_filename, 'wb')

        zip_file = zipfile.ZipFile(temp_filename,  'r')
        temp_result_file.write(zip_file.read('transport_file'))
        zip_file.close()
        temp_result_file.close()
    except:
        print "unhandle error durring decode of transport_file from slave"
        print "temp_filename:", temp_filename
        print "temp_result_filename:", temp_result_filename
        traceback.print_exc()
        print "x" * 80

    return temp_result_filename


class DevbotnetSlaveRestart(threading.Thread):
    def __init__(self, dev_env):
        threading.Thread.__init__(self, name='DevbotnetSlaveRestart')
        self.dev_env = dev_env
        self.daemon = True

    def run(self):
        print "Restarting devbotnet"
        command = [self.dev_env.tools_setup.get_python(), 'config.py', '--cmd_devbotnet', 'restart']
        process = subprocess.Popen(command, cwd=self.dev_env.hg_root, stdout=subprocess.PIPE)
        process.wait()
        print "Restarting devbotnet, done"


class DevbotnetSlaveInfo(object):
    def __init__(self, dev_env):
        self.dev_env = dev_env
        self.devbotnet_root = os.path.join(self.dev_env.hg_root, 'setup', 'dev_env', 'devbotnet')

        self.name = 'Unknown'
        self.arch = sys.platform
        self.arch_target = guess_arch_target()
        self.hg_repo = 'shared/main'
        self.listen_port = 4242
        self.rpm_target = 'unknown'

    @classmethod
    def create_from_config_ini(cls, dev_env, config):
        slave_info = DevbotnetSlaveInfo(dev_env)
        if config.has_option('devbotnet', 'name'):
            slave_info.name = config.get('devbotnet', 'name')
        if config.has_option('devbotnet', 'arch'):
            slave_info.arch = config.get('devbotnet', 'arch')
        if config.has_option('devbotnet', 'arch_target'):
            slave_info.arch_target = config.get('devbotnet', 'arch_target')
        if config.has_option('devbotnet', 'hg_repo'):
            slave_info.hg_repo = config.get('devbotnet', 'hg_repo')
        if config.has_option('devbotnet', 'listen_port'):
            slave_info.listen_port = config.getint('devbotnet', 'listen_port')
        if config.has_option('devbotnet', 'rpm_target'):
            slave_info.rpm_target = config.get('devbotnet', 'rpm_target')

        if slave_info.arch_target not in ARCH_TARGET_VALID:
            print "WARNING: invalid value for arch_target(%s) in config file" % slave_info.arch_target

        return slave_info

    @classmethod
    def create_from_local_config_ini(cls, dev_env):
        config_filename = os.path.join(dev_env.hg_root, 'config_local.ini')
        config = ConfigParser.ConfigParser()
        config.read(config_filename)
        return DevbotnetSlaveInfo.create_from_config_ini(dev_env, config)


    def update_config_ini(self, config):
        if config.has_section('devbotnet'):
            config.remove('devbotnet')
        config.add_section('devbotnet')
        config.set('devbotnet', 'name', self.name)
        config.set('devbotnet', 'arch', self.arch)
        config.set('devbotnet', 'arch_target', self.arch_target)
        config.set('devbotnet', 'hg_repo', self.hg_repo)
        config.set('devbotnet', 'listen_port', self.listen_port)
        config.set('devbotnet', 'rpm_target', self.rpm_target)

    def write_job_info(self, hg_revision, job_info):
#        hg_repo_folder = self.hg_repo.replace('/', '_').replace('\\', '_')
#        output_folder_sufix = lib.dev_env.devbotnet_jobs.JobInfo.JOB_TYPE_FOLDERNAMES[job_info.type]
#        foldername_abs = os.path.join(self.devbotnet_root, hg_repo_folder, hg_revision, job_info.slave_name)
#        if not os.path.exists(foldername_abs):
#            os.makedirs(foldername_abs)
#
#        job_info_file = open(os.path.join(foldername_abs, 'job_info_%s' % output_folder_sufix), 'w')
#        pickle.dump(job_info, job_info_file)
#        job_info_file.close()
        pass



class DevbootnetSlave(object):
    RC_OK = 0
    RC_ERROR = 1

    def __init__(self, devbotnet_slave_info):
        self.devbotnet_slave_info = devbotnet_slave_info
        self.job_id_next = 0
        self.jobs = {}
        self._closed = False

    def cmd_get_info(self):
        print "cmd_get_info, begin"
        result = {'rc':DevbootnetSlave.RC_OK, 'name':self.devbotnet_slave_info.name, 'arch':self.devbotnet_slave_info.arch, 'arch_target':self.devbotnet_slave_info.arch_target}
        print "cmd_get_info, end"
        return result

    def cmd_do_hg_pull_and_update(self):
        print "cmd_do_hg_pull_and_update, begin"
        hg_status_output_lines = lib.dev_env.hg.hg_uncommited_files()
        if len(hg_status_output_lines) != 0:
            result = {'rc':DevbootnetSlave.RC_ERROR, 'error_message':'Uncommit files exists', 'stdout':'\n'.join(hg_status_output_lines)}
            return result

        hg_pull_output = lib.dev_env.hg.hg_pull_with_update()
        result = {'rc':DevbootnetSlave.RC_OK, 'stdout':hg_pull_output}
        print "cmd_do_hg_pull_and_update, end"
        return result

    def cmd_get_hg_revision(self):
        print "cmd_get_hg_revision, begin"
        result = {'rc':DevbootnetSlave.RC_OK, 'hg_repo':self.devbotnet_slave_info.hg_repo ,'hg_revision':lib.dev_env.hg.hg_get_revision()}
        print "cmd_get_hg_revision, end"
        return result

    def cmd_get_job_status(self, job_id):
        print "cmd_get_job_status(%s), begin" % job_id

        if not self.jobs.has_key(job_id):
            result = {'rc':DevbootnetSlave.RC_ERROR, 'error_message':'Invalid job_id'}
        else:
            status = self.jobs[job_id].get_status()
            result = {'rc':DevbootnetSlave.RC_OK, 'status':status}
        print "cmd_get_job_status(%s), end" % job_id
        return result

    def cmd_get_job_output(self, job_id):
        print "cmd_get_job_output(%s), begin" % job_id
        if not self.jobs.has_key(job_id):
            result = {'rc':DevbootnetSlave.RC_ERROR, 'error_message':'Invalid job_id'}
        else:
            result = {}
            result['rc'] = DevbootnetSlave.RC_OK
            result['db_slave'] = encode_file_before_transport(self.jobs[job_id].get_db_filename())
        print "cmd_get_job_output(%s), end" % job_id
        return result

    def cmd_start_job_do_unittest(self, ifilter, efilter):
        print "cmd_start_job_do_unittest, begin"
        job_id = self.job_id_next
        self.job_id_next += 1

        db_filename = self.devbotnet_slave_info.dev_env.generate_temp_filename()

        db_connect_string = 'sqlite:///'+ db_filename
        lib.dev_env.devbotnet_db.generate_db(db_connect_string)
        db_session = lib.dev_env.devbotnet_db.create_session(db_connect_string)

        new_job = lib.dev_env.devbotnet_jobs.JobDoUnittest(self.devbotnet_slave_info.dev_env, db_session, ifilter, efilter, self.devbotnet_slave_info)
        new_job.set_db_filename(db_filename)

        self.jobs[job_id] = new_job
        new_job.start()

        result = {'rc':DevbootnetSlave.RC_OK, 'job_id':job_id}
        print "cmd_start_job_do_unittest(%s), end" % job_id
        return result

    def cmd_start_job_do_static_analysis(self, ifilter, efilter):
        print "cmd_start_job_do_static_analysis, begin"
        job_id = self.job_id_next
        self.job_id_next += 1

        db_filename = self.devbotnet_slave_info.dev_env.generate_temp_filename()

        db_connect_string = 'sqlite:///'+ db_filename
        lib.dev_env.devbotnet_db.generate_db(db_connect_string)
        db_session = lib.dev_env.devbotnet_db.create_session(db_connect_string)

        new_job = lib.dev_env.devbotnet_jobs.JobDoStaticAnalysis(self.devbotnet_slave_info.dev_env, db_session, ifilter, efilter, self.devbotnet_slave_info)
        new_job.set_db_filename(db_filename)

        self.jobs[job_id] = new_job
        new_job.start()

        result = {'rc':DevbootnetSlave.RC_OK, 'job_id':job_id}
        print "cmd_start_job_do_static_analysis(%s), end" % job_id
        return result

    def cmd_start_job_do_profile(self, ifilter, efilter):
        print "cmd_start_job_do_profile, begin"
        job_id = self.job_id_next
        self.job_id_next += 1

        db_filename = self.devbotnet_slave_info.dev_env.generate_temp_filename()

        db_connect_string = 'sqlite:///'+ db_filename
        lib.dev_env.devbotnet_db.generate_db(db_connect_string)
        db_session = lib.dev_env.devbotnet_db.create_session(db_connect_string)

        new_job = lib.dev_env.devbotnet_jobs.JobDoProfile(self.devbotnet_slave_info.dev_env, db_session, ifilter, efilter, self.devbotnet_slave_info)
        new_job.set_db_filename(db_filename)

        self.jobs[job_id] = new_job
        new_job.start()

        result = {'rc':DevbootnetSlave.RC_OK, 'job_id':job_id}
        print "cmd_start_job_do_profile(%s), end" % job_id
        return result

    def cmd_start_job_do_coverage(self, ifilter, efilter):
        print "cmd_start_job_do_coverage, begin"
        job_id = self.job_id_next
        self.job_id_next += 1

        db_filename = self.devbotnet_slave_info.dev_env.generate_temp_filename()

        db_connect_string = 'sqlite:///'+ db_filename
        lib.dev_env.devbotnet_db.generate_db(db_connect_string)
        db_session = lib.dev_env.devbotnet_db.create_session(db_connect_string)

        new_job = lib.dev_env.devbotnet_jobs.JobDoCoverage(self.devbotnet_slave_info.dev_env, db_session, ifilter, efilter, self.devbotnet_slave_info)
        new_job.set_db_filename(db_filename)

        self.jobs[job_id] = new_job
        new_job.start()

        result = {'rc':DevbootnetSlave.RC_OK, 'job_id':job_id}
        print "cmd_start_job_do_coverage(%s), end" % job_id
        return result

    def cmd_restart(self):
        print "cmd_restart, begin"
        dev_env = lib.dev_env.DevEnv.create_current(lib.dev_env.tools.ToolsSetup.create_default())
        restart_job = DevbotnetSlaveRestart(dev_env)
        restart_job.start()
        print "cmd_restart, end"
        result = {'rc':DevbootnetSlave.RC_OK}
        return result
#
#    def cmd_start_job_do_release(self):
#        print "cmd_start_job_do_release, begin"
#        job_id = self.job_id_next
#        self.job_id_next += 1
#
#        new_job = JobDoRelease(self.devbotnet_slave_info.dev_env, self.commandline_options, self.devbotnet_slave_info)
#        self.jobs[job_id] = new_job
#        new_job.start()
#
#        result = {'rc':DevbootnetSlave.RC_OK, 'job_id':job_id}
#        print "cmd_start_job_do_release(%s), end" % job_id
#        return result

    def run(self):
#        if not self.hg_uncommited_files():
#            print "Devbotnet Slave NOT started hg reports uncomitted files"
#            return -1

        self.slave_server = SimpleXMLRPCServer.SimpleXMLRPCServer(("0.0.0.0", self.devbotnet_slave_info.listen_port), allow_none=True)
        print "Devbotnet Slave listening on port %d ..." % self.devbotnet_slave_info.listen_port
        self.slave_server.register_function(self.cmd_get_info, 'cmd_get_info')
        self.slave_server.register_function(self.cmd_do_hg_pull_and_update, 'cmd_do_hg_pull_and_update')
        self.slave_server.register_function(self.cmd_get_hg_revision, 'cmd_get_hg_revision')
        self.slave_server.register_function(self.cmd_get_job_status, 'cmd_get_job_status')
        self.slave_server.register_function(self.cmd_get_job_output, 'cmd_get_job_output')
        self.slave_server.register_function(self.cmd_start_job_do_unittest, 'cmd_start_job_do_unittest')
        self.slave_server.register_function(self.cmd_start_job_do_static_analysis, 'cmd_start_job_do_static_analysis')
        self.slave_server.register_function(self.cmd_start_job_do_profile, 'cmd_start_job_do_profile')
        self.slave_server.register_function(self.cmd_start_job_do_coverage, 'cmd_start_job_do_coverage')
        self.slave_server.register_function(self.cmd_restart, 'cmd_restart')
#        self.slave_server.register_function(self.cmd_start_job_do_release, 'cmd_start_job_do_release')
        try:
            self.slave_server.serve_forever()
        except KeyboardInterrupt:
            pass
        self.slave_server.server_close()
        time.sleep(1)
        print "Devbotnet Slave no longer listening"
        self._closed = True
        time.sleep(5)

    def is_closed(self):
        return self._closed

    def shutdown(self):
        self.slave_server.shutdown()




class DevbootnetSlaveProxy(object):
    def __init__(self, slave_name, slave_port=4242):
        self.slave_server_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (slave_name, slave_port))
        self.slave_name = slave_name
        self.slave_port = slave_port
        self.info = self.cmd_get_info()

        self._cache_hg_revision = None

    def close(self):
        self.slave_server_proxy.close()

    def get_name_raw(self):
        return self.info['name']

    def get_name(self):
        return "%s(%s:%d) %s" % (self.info['name'], self.slave_name, self.slave_port, self.info['arch_target'])

    def cmd_get_info(self):
        return self.slave_server_proxy.cmd_get_info()

    def cmd_get_hg_revision(self):
        if self._cache_hg_revision is None:
            result = self.slave_server_proxy.cmd_get_hg_revision()
            if result['rc'] == DevbootnetSlave.RC_ERROR:
                return 'ERROR(%s)' % result['error_message']
            self._cache_hg_revision = (result['hg_repo'], result['hg_revision'])
        return self._cache_hg_revision

    def cmd_do_hg_pull_and_update(self):
        return self.slave_server_proxy.cmd_do_hg_pull_and_update()

    def cmd_get_job_status(self, job_id):
        return self.slave_server_proxy.cmd_get_job_status(job_id)

    def cmd_get_job_output(self, job_id):
        return self.slave_server_proxy.cmd_get_job_output(job_id)

    def cmd_close_job(self, job_id):
        return self.slave_server_proxy.cmd_close_job(job_id)

    def cmd_start_job_do_unittest(self, ifilter, efilter):
        return self.slave_server_proxy.cmd_start_job_do_unittest(ifilter, efilter)

    def cmd_start_job_do_static_analysis(self, ifilter, efilter):
        return self.slave_server_proxy.cmd_start_job_do_static_analysis(ifilter, efilter)

    def cmd_start_job_do_profile(self, ifilter, efilter):
        return self.slave_server_proxy.cmd_start_job_do_profile(ifilter, efilter)

    def cmd_start_job_do_coverage(self, ifilter, efilter):
        return self.slave_server_proxy.cmd_start_job_do_coverage(ifilter, efilter)

    def cmd_restart(self):
        return self.slave_server_proxy.cmd_restart()

    def cmd_start_job_do_release(self):
        return self.slave_server_proxy.cmd_start_job_do_release()


#
# DevbootnetMaster
#
class DevbootnetMaster(object):
    def __init__(self, devbotnet_slave_info, dev_env, db_connect_string=None, slave_connect_list=None):
        self.devbotnet_slave_info = devbotnet_slave_info
        self.dev_env = dev_env
        self.db_connect_string = db_connect_string
        self.running_slaves = []
        self.slave_connect_list = slave_connect_list

    def lookup_slaves(self):
        slaves = []
        if self.slave_connect_list is None:
            self.slave_connect_list = lib.portscan.tcp_portscan(DEVBOTNET_SLAVE_IP_RANGE, [self.devbotnet_slave_info.listen_port])
        for (slave_address, slave_port) in self.slave_connect_list:
            try:
                slaves.append( DevbootnetSlaveProxy(slave_address, slave_port) )
            except:
                print "Unable to connect to slave %s:%d" % (slave_address, slave_port)
        return slaves

    def close_slaves(self, slaves):
        slaves = []

    def get_running_slave_infos(self):
        slave_infos = []
        for slave in self.running_slaves:
            slave_info = {}
            slave_info['is_running'] =  slave.job_running
            slave_info['name'] =  slave.info['name']
            slave_info['arch_target'] =  slave.info['arch_target']
            slave_info['hg_id'] = slave.cmd_get_hg_revision()
            slave_infos.append(slave_info)
        return slave_infos

    def show_slave_info(self):
        print 'show_slave_info, begin'
        slaves = self.lookup_slaves()
        for slave in slaves:
            (hg_repo, hg_revision) = slave.cmd_get_hg_revision()
            print '%s: %s %s' % (slave.get_name(), hg_repo, hg_revision)
        self.close_slaves(slaves)
        print 'show_slave_info, end'

    def _wait_for_job_on_clients(self, slaves):
        print "Waiting for job on slaves...."
        one_is_running = True
        while one_is_running:
            one_is_running = False
            for slave in slaves:
                if slave.job_running:
                    print '%s: %s' % (slave.get_name(), slave.job_message)
                    job_status_info = slave.cmd_get_job_status(slave.job_id)
                    if job_status_info['rc'] == DevbootnetSlave.RC_OK:
                        if job_status_info['status'] in [lib.dev_env.devbotnet_jobs.Job.JOB_STATUS_DONE, lib.dev_env.devbotnet_jobs.Job.JOB_STATUS_DONE_WITH_ERRORS, lib.dev_env.devbotnet_jobs.Job.JOB_STATUS_DONE_WITH_WARNINGS]:
                            slave.job_message = 'Done'
                        else:
                            one_is_running = True
            time.sleep(10)

    def _select_slaves(self, hg_repo_requested):
        slaves = self.lookup_slaves()
        slaves_to_use = []
        for slave in slaves:
            (hg_repo, hg_revision) = slave.cmd_get_hg_revision()
            if hg_repo_requested == hg_repo:
                print '%s: %s %s' % (slave.get_name(), hg_repo, hg_revision)
                slaves_to_use.append(slave)
            else:
                print '%s: %s %s IGNORED' % (slave.get_name(), hg_repo, hg_revision)
        return slaves_to_use

    def _collect_output_from_job_on_clients(self, slaves):
        print "Collecting result from slaves ...."
        for slave in slaves:
            (hg_repo, hg_revision) = slave.cmd_get_hg_revision()
            job_output_info = slave.cmd_get_job_output(slave.job_id)
            self._import_db_from_slave(job_output_info, hg_repo, hg_revision, slave)

    def _import_db_from_slave(self, job_output_info, hg_repo, hg_revision, slave):
        if self.db_connect_string is None:
            print "Collecting result from slaves, no db_session available result from client ignored"
            return

        if job_output_info['rc'] == DevbootnetSlave.RC_ERROR:
            print "Collecting result from slaves, error from client"
            return

        db_session = lib.dev_env.devbotnet_db.create_session(self.db_connect_string)
        db_slave_filename = decode_file_after_transport(job_output_info['db_slave'], self.dev_env)
        db_slave_connect_string = 'sqlite:///' + db_slave_filename

        db_session_slave = lib.dev_env.devbotnet_db.create_session(db_slave_connect_string)
        lib.dev_env.devbotnet_db_update.import_slave_data(db_session, db_session_slave, hg_repo, hg_revision)
        db_session.close()


    def do_unittest(self, hg_repo_requested, ifilter, efilter):
        print 'do_unittest, begin', datetime.datetime.now()
        self.running_slaves = self._select_slaves(hg_repo_requested)

        print "Starting job on slaves...."
        for slave in self.running_slaves:
            job_start_info = slave.cmd_start_job_do_unittest(ifilter, efilter)
            if job_start_info['rc'] == DevbootnetSlave.RC_OK:
                slave.job_id = job_start_info['job_id']
                slave.job_running = True
                slave.job_message = 'Running'
            else:
                slave.job_id = None
                slave.job_running = False
                slave.job_message = job_start_info['error_message']

        self._wait_for_job_on_clients(self.running_slaves)

        self._collect_output_from_job_on_clients(self.running_slaves)
        self.close_slaves(self.running_slaves)
        print 'do_unittest, end', datetime.datetime.now()

    def do_static_analysis(self, hg_repo_requested, ifilter, efilter):
        print 'do_static_analysis, begin', datetime.datetime.now()
        self.running_slaves = self._select_slaves(hg_repo_requested)

        print "Starting job on slaves...."
        for slave in self.running_slaves:
            job_start_info = slave.cmd_start_job_do_static_analysis(ifilter, efilter)
            if job_start_info['rc'] == DevbootnetSlave.RC_OK:
                slave.job_id = job_start_info['job_id']
                slave.job_running = True
                slave.job_message = 'Running'
            else:
                slave.job_id = None
                slave.job_running = False
                slave.job_message = job_start_info['error_message']
        self._wait_for_job_on_clients(self.running_slaves)

        self._collect_output_from_job_on_clients(self.running_slaves)
        self.close_slaves(self.running_slaves)
        print 'do_static_analysis, end', datetime.datetime.now()

    def do_profile(self, hg_repo_requested, ifilter, efilter):
        print 'do_profile, begin', datetime.datetime.now()
        self.running_slaves = self._select_slaves(hg_repo_requested)

        print "Starting job on slaves...."
        for slave in self.running_slaves:
            job_start_info = slave.cmd_start_job_do_profile(ifilter, efilter)
            if job_start_info['rc'] == DevbootnetSlave.RC_OK:
                slave.job_id = job_start_info['job_id']
                slave.job_running = True
                slave.job_message = 'Running'
            else:
                slave.job_id = None
                slave.job_running = False
                slave.job_message = job_start_info['error_message']

        self._wait_for_job_on_clients(self.running_slaves)

        self._collect_output_from_job_on_clients(self.running_slaves)
        self.close_slaves(self.running_slaves)
        print 'do_profile, end', datetime.datetime.now()

    def do_coverage(self, hg_repo_requested, ifilter, efilter):
        print 'do_coverage, begin', datetime.datetime.now()
        self.running_slaves = self._select_slaves(hg_repo_requested)

        print "Starting job on slaves...."
        for slave in self.running_slaves:
            job_start_info = slave.cmd_start_job_do_coverage(ifilter, efilter)
            if job_start_info['rc'] == DevbootnetSlave.RC_OK:
                slave.job_id = job_start_info['job_id']
                slave.job_running = True
                slave.job_message = 'Running'
            else:
                slave.job_id = None
                slave.job_running = False
                slave.job_message = job_start_info['error_message']

        self._wait_for_job_on_clients(self.running_slaves)

        self._collect_output_from_job_on_clients(self.running_slaves)
        self.close_slaves(self.running_slaves)
        print 'do_coverage, end', datetime.datetime.now()


    def do_restart(self, hg_repo_requested):
        print 'do_restart, begin'
        slaves = self._select_slaves(hg_repo_requested)
        for slave in slaves:
            slave.restarting = False
            slave_restart_rc = slave.cmd_restart()
            if slave_restart_rc['rc'] == DevbootnetSlave.RC_OK:
                slave.restarting = True
        print 'do_restart, waiting'
        time.sleep(10)
        print 'do_restart, looking for slaves'
        self.show_slave_info()
        self.close_slaves(slaves)
        print 'do_restart, end'

    def do_release(self, hg_repo_requested):
        print 'do_release, begin', datetime.datetime.now()
        slaves = self._select_slaves(hg_repo_requested)

        print "Starting job on slaves...."
        for slave in slaves:
            job_start_info = slave.cmd_start_job_do_release()
            if job_start_info['rc'] == DevbootnetSlave.RC_OK:
                slave.job_id = job_start_info['job_id']
                slave.job_running = True
                slave.job_message = 'Running'
            else:
                slave.job_id = None
                slave.job_running = False
                slave.job_message = job_start_info['error_message']
        self._wait_for_job_on_clients(slaves)

        self._collect_output_from_job_on_clients(slaves)
        self.close_slaves(slaves)
        print 'do_release, end', datetime.datetime.now()


    def do_pull_and_update(self):
        print 'do_pull_and_update, begin', datetime.datetime.now()
        slaves = self.lookup_slaves()
        for slave in slaves:
            print slave.get_name()
            job_info = slave.cmd_do_hg_pull_and_update()
            if job_info['rc'] == DevbootnetSlave.RC_OK:
                print "ok"
            else:
                print job_info['error_message']
        self.close_slaves(slaves)
        print 'do_pull_and_update, end', datetime.datetime.now()


    def do_run_inc(self, ifilter, efilter):
        update_interval_min = 1
        print 'do_run_inc, begin', datetime.datetime.now()
        while True:
            print 'do_run_inc, checking' , datetime.datetime.now()
            if lib.dev_env.hg.hg_check_for_news():
                self.do_pull_and_update()
                self.do_restart(self.devbotnet_slave_info.hg_repo)
                self.do_unittest(self.devbotnet_slave_info.hg_repo, ifilter, efilter)
                self.do_static_analysis(self.devbotnet_slave_info.hg_repo, ifilter, efilter)
            print 'do_run_inc, checking done'
            try:
                time.sleep(update_interval_min * 60)
            except KeyboardInterrupt:
                break
        print 'do_run_inc, end', datetime.datetime.now()
