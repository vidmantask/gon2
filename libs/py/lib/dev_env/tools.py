'''
'''
import sys
import os.path
import ConfigParser

HG_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..'))

class ToolsSetup():
    def __init__(self, java_home=None):
        self.java_home = java_home

    @classmethod
    def create_from_config(cls, config):
        java_home = None
        
        if config.has_section('tools_setup'):
            if config.has_option('tools_setup', 'java_home'):
                java_home = config.get('tools_setup', 'java_home')
        return ToolsSetup(java_home=java_home)

    @classmethod
    def create_default(cls):
        return ToolsSetup()

    def get_java_home(self):
        if self.java_home is not None:
            return self.java_home
        if sys.platform == 'win32':
            return 'c:/Program Files/java/jre6'
        return '/usr/lib/jvm/java-1.6.0-openjdk-1.6.0.0/jre'
        
    def get_python(self):
        return 'python'
    
    def get_python_script_home(self):
        if sys.platform == 'win32':
            return os.path.join(HG_ROOT, 'py','setup', 'py_ext', 'scripts','plat_win')
        return ""
    
    def get_scp(self):
        if sys.platform == 'win32':
            return 'C:/utility/PuTTY/pscp.exe'
        return 'scp'
    
    def get_pylint(self):
        if sys.platform == 'win32':
            return os.path.join(self.get_python_script_home(), 'pylint.bat')
        return 'pylint'
        
    def get_wsdl2py(self):
        return os.path.join(self.get_python_script_home(), 'wsdl2py')
    
    def get_wsdl2dispatch(self):
        return os.path.join(self.get_python_script_home(), 'wsdl2dispatch')
    
    def get_paster(self):
        return os.path.join(self.get_python_script_home(), 'paster')

    def get_axis2_home(self):
        axis_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', '..', 'java', 'setup', 'lib3p', 'axis2'))
        return axis_folder
        
    def get_wsdl2java(self):
        axis_folder_bin = os.path.join(self.get_axis2_home(), 'bin')
        if sys.platform == 'win32':
            return os.path.join(axis_folder_bin, 'wsdl2java.bat')
        return os.path.join(axis_folder_bin, 'wsdl2java.sh')

    def get_make(self):
        if sys.platform == 'win32':
            return 'nmake'
        else:
            return 'make'    