'''
Created on Jun 16, 2011

@author: thwang
'''
import threading
import traceback
import sqlalchemy

import devbotnet_db
import devbotnet_db_query
import lib.dev_env.hg


global_config_thread_lock = threading.RLock()

def get_or_create_config(db_session):
    global_config_thread_lock.acquire()

    db_config = None
    try:
        db_config = db_session.query(devbotnet_db.DevBotnetDBConfig).filter_by(uid=1).one()
    except:
        pass
    
    try:
        if db_config is None:
            db_config = devbotnet_db.DevBotnetDBConfig()
            db_session.add(db_config)
            db_session.commit()
    except:
        pass
        
    global_config_thread_lock.release()
    return db_config

def set_config_filters(db_session, form_ifilter, form_efilter):
    if form_ifilter == '':
        form_ifilter = '.*'
    
    db_config = get_or_create_config(db_session)
    db_config.ifilter = form_ifilter
    db_config.efilter = form_efilter
    db_session.commit()

def set_config_builders(db_session, form_builders):
    if form_builders == '':
        form_builders = '127.0.0.1:4242'
    
    db_config = get_or_create_config(db_session)
    db_config.builders = form_builders
    db_session.commit()



def create_changeset(db_session, hg_repo, hg_revision):
    if hg_revision.endswith('+'):
        hg_revision = hg_revision[:len(hg_revision)-1]
    
    db_changeset = devbotnet_db_query.get_changeset(db_session, hg_repo, hg_revision)
    if db_changeset is None:
        db_changeset = devbotnet_db.DevBotnetDBChangeset(hg_repo, hg_revision)
        db_session.add(db_changeset)
    db_session.commit()
    return db_changeset


    
    


def import_slave_data(db_session, db_session_slave, hg_repo, hg_revision):
    db_changeset = create_changeset(db_session, hg_repo, hg_revision)
    
    for slave_devbotnet_db_run in db_session_slave.query(devbotnet_db.DevBotnetDBRun).all():
        slave_devbotnet_db_run_copy = slave_devbotnet_db_run.copy()
        slave_devbotnet_db_run_copy.changeset_uid = db_changeset.uid
        db_session.add(slave_devbotnet_db_run_copy)
        db_session.commit()
        
   
        for slave_devbotnet_db_run_part in db_session_slave.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=slave_devbotnet_db_run.uid).all():
            slave_devbotnet_db_run_part_copy = slave_devbotnet_db_run_part.copy()
            slave_devbotnet_db_run_part_copy.run_uid = slave_devbotnet_db_run_copy.uid
            db_session.add(slave_devbotnet_db_run_part_copy)
            db_session.commit()
            for slave_devbotnet_db_run_part_detail in db_session_slave.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(run_part_uid=slave_devbotnet_db_run_part.uid).all():
                slave_devbotnet_db_run_part_detail_copy = slave_devbotnet_db_run_part_detail.copy()
                slave_devbotnet_db_run_part_detail_copy.run_part_uid = slave_devbotnet_db_run_part_copy.uid
                db_session.add(slave_devbotnet_db_run_part_detail_copy)
            db_session.commit()
            calculate_sum_for_run_part(db_session, slave_devbotnet_db_run_part_copy)

        db_session.commit()
        calculate_sum_for_run(db_session, slave_devbotnet_db_run_copy)
        
        slave_devbotnet_db_run_copy_compare = find_compare_for_run(db_session, slave_devbotnet_db_run_copy)
        set_run_compare(db_session, slave_devbotnet_db_run_copy, slave_devbotnet_db_run_copy_compare)

    db_session.commit()



def set_run_compare(db_session, db_run, db_run_compare):
    if db_run_compare is None:
        db_run.compare_run_uid = None
    else:
        db_run.compare_run_uid = db_run_compare.uid
    
    for db_run_part in db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=db_run.uid).all():
        db_run_part_compare = find_compare_for_run_part(db_session, db_run_compare, db_run_part)
        if db_run_part_compare is None:
            db_run_part.compare_run_part_uid = None
        else:
            db_run_part.compare_run_part_uid = db_run_part_compare.uid

    db_session.commit()



def calculate_sum_for_run_part(db_session, db_run_part):
    info = 0
    warning = 0
    error = 0

    for db_run_part_detail in db_session.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(run_part_uid=db_run_part.uid).all():
        if db_run_part_detail.status == devbotnet_db.DevBotnetDBRunPartDetail.STATUS_OK :
            info += 1
        elif db_run_part_detail.status == devbotnet_db.DevBotnetDBRunPartDetail.STATUS_INFO:
            info += 1
        elif db_run_part_detail.status == devbotnet_db.DevBotnetDBRunPartDetail.STATUS_WARNING:
            warning += 1
        elif db_run_part_detail.status == devbotnet_db.DevBotnetDBRunPartDetail.STATUS_ERROR:
            error += 1
    
    db_run_part.sum_calculated = True
    db_run_part.sum_count = info + warning + error
    db_run_part.sum_count_info = info
    db_run_part.sum_count_warning = warning
    db_run_part.sum_count_error = error
    db_session.commit()


def calculate_sum_for_run(db_session, db_run):
    count = 0
    info = 0
    warning = 0
    error = 0

    for db_run_part in db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=db_run.uid).all():
        count += db_run_part.sum_count
        info += db_run_part.sum_count_info
        warning += db_run_part.sum_count_warning
        error += db_run_part.sum_count_error

    db_run.sum_calculated = True
    db_run.sum_count = count
    db_run.sum_count_info = info
    db_run.sum_count_warning = warning
    db_run.sum_count_error = error
    db_session.commit()


def find_compare_for_run(db_session, db_run):
    db_changeset = devbotnet_db_query.get_changeset_for_run(db_session, db_run)
    changeset = db_changeset.changeset
    
    db_run_compare = None
    while (db_run_compare is None):
        hg_changeset_parent = lib.dev_env.hg.hg_parent(changeset=changeset)
        if hg_changeset_parent is None:
            return None
        
        hg_changeset_db = devbotnet_db_query.get_changeset(db_session, db_changeset.repository, hg_changeset_parent)
        if hg_changeset_db is not None:
            try:
                db_run_compares = db_session.query(devbotnet_db.DevBotnetDBRun).filter_by(changeset_uid=hg_changeset_db.uid).filter_by(type=db_run.type).order_by(sqlalchemy.desc(devbotnet_db.DevBotnetDBRun.begin_ts)).all()
                if len(db_run_compares) > 0:
                    db_run_compare = db_run_compares[0]
            
            except:
                traceback.print_exc()
                pass
        changeset = hg_changeset_parent
        
        #hack: to stop from looping to start of repo
        if changeset == "c49a821c8c52":
            return None
    return db_run_compare
            

def find_compare_for_run_part(db_session, db_run_compare, db_run_part):
    try:
        return db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=db_run_compare.uid).filter_by(module=db_run_part.module).filter_by(filename=db_run_part.filename).one()
    except:
        pass
    return None


def delete_run_job(db_session, run_uid):
    for db_run in db_session.query(devbotnet_db.DevBotnetDBRun).filter_by(uid=run_uid).all():
        for db_job_part in db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=db_run.uid).all():
            for db_job_part_detail in db_session.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(run_part_uid=db_job_part.uid).all():
                db_session.delete(db_job_part_detail)
            db_session.delete(db_job_part)
        db_session.delete(db_run)
        db_session.commit()

        for db_run_compare in db_session.query(devbotnet_db.DevBotnetDBRun).filter_by(compare_run_uid=db_run.uid).all():
            set_run_compare(db_session, db_run_compare, None)

    db_session.commit()
    
    return True