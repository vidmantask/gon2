'''
This module
'''
import sys
import os
import os.path
import tempfile
import threading
import glob
import unittest
import traceback
import StringIO
import subprocess
import time
import shutil
import pickle
import datetime
import zipfile
import base64
import pstats
import coverage

import devbotnet_db

HG_ROOT = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..")))

def read_file_content(filename):
    if os.path.exists(filename):
        myfile = open(filename, 'r')
        content = myfile.read()
        return content
    return ""


class Job(threading.Thread):
    JOB_STATUS_INIT = -1
    JOB_STATUS_RUNNING = 0
    JOB_STATUS_DONE_OK = 1
    JOB_STATUS_DONE_ERROR = 2

    def __init__(self, temp_root):
        threading.Thread.__init__(self, name='Job')
        self._running = False
        self._stop = False
        self._status = Job.JOB_STATUS_INIT
        self.temp_root = temp_root
        self.db_filename = None

        if not os.path.isdir(temp_root):
            os.makedirs(temp_root)
        (self.stdout_file, self.stdout_filename) = tempfile.mkstemp('log', '_job_out_', temp_root, True)
        (self.stderr_file, self.stderr_filename) = tempfile.mkstemp('log', '_job_err_', temp_root, True)

    def run(self):
        print "Job(%s), begin" % self.name
        self._status = Job.JOB_STATUS_RUNNING
        self._running = True
        self._status = self.do_run()
        os.close(self.stdout_file)
        os.close(self.stderr_file)
        print "Job(%s) end" % self.name
        self._running = False

    def do_run(self):
        return Job.JOB_STATUS_DONE_OK

    def do_stop(self):
        self._stop = True

    def get_status(self):
        return self._status

    def set_db_filename(self, db_filename):
        self.db_filename = db_filename

    def get_db_filename(self):
        return self.db_filename


class JobDoUnittestPart(threading.Thread):
    JOB_STATUS_INIT = -1
    JOB_STATUS_RUNNING = 0
    JOB_STATUS_DONE_OK = 1
    JOB_STATUS_DONE_ERROR = 2
    JOB_STATUS_DONE_TIMEOUT = 3
    JOB_STATUS_DONE_IGNORED = 4
    JOB_STATUS = ["INIT", "RUNNING", "OK", "ERROR", "TIMEOUT", "IGNORED"]

    def __init__(self, dev_env, filename, modulename, enable_profiling=False, enable_details=True, coverage_filename=None):
        threading.Thread.__init__(self, name='JobDoUnittestPart')
        self.dev_env = dev_env
        self.status = JobDoUnittestPart.JOB_STATUS_INIT
        self.filename = filename
        self.modulename = modulename
        self.enable_profiling = enable_profiling
        self.enable_details = enable_details
        self.enable_coverage = coverage_filename is not None
        self.coverage_filename = coverage_filename

    def run(self):
        self.status = JobDoUnittestPart.JOB_STATUS_RUNNING
        self.status = self._run_load_module()
        if self.status == JobDoUnittestPart.JOB_STATUS_RUNNING:
            self.status = self._run_unittest()

    def _run_load_module(self):
        try:
            py_module = __import__(self.modulename, fromlist=[''], globals=globals(), locals=locals())
            self.ignore = self._get_modul_attr(py_module, 'GIRI_UNITTEST_IGNORE', False)
            self.owner = self._get_modul_attr(py_module, 'GIRI_UNITTEST_OWNER', 'unknown')
            self.timeout = self._get_modul_attr(py_module, 'GIRI_UNITTEST_TIMEOUT', 60)
            self.tags = self._get_modul_attr(py_module, 'GIRI_UNITTEST_TAGS', [])
            return JobDoUnittestPart.JOB_STATUS_RUNNING
        except Exception as error:
            print(error)
            return  JobDoUnittestPart.JOB_STATUS_DONE_ERROR

    def _get_modul_attr(self, py_module, attr_name, default_value):
        if hasattr(py_module, attr_name):
            return getattr(py_module, attr_name)
        return default_value

    def _run_unittest_parse_result(self, output):
        std_out = output['exec_out']
        if std_out != "":
            print(std_out)

    def _run_unittest(self):
        try:
            print "Running", self.modulename
            if self.ignore:
                return JobDoUnittestPart.JOB_STATUS_DONE_IGNORED

            tempfolder = tempfile.mkdtemp()
            stdout_filename = os.path.join(tempfolder, 'stdout')
            stdout_file = open(stdout_filename, 'w')
            stderr_filename = os.path.join(tempfolder, 'stderr')
            stderr_file = open(stderr_filename, 'w')

            output_filename = os.path.join(tempfolder, 'output')

            env = os.environ
            env['PYTHONPATH'] = self.dev_env.py_root
            command = [self.dev_env.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'lib', 'dev_env', 'unittest_runner.py'), '--modulename', self.modulename, '--out', output_filename]

            if self.enable_coverage:
                command.append('--coverage_filename')
                command.append(self.coverage_filename)

            process = subprocess.Popen(command, env=env, stdout=stdout_file, stderr=stderr_file)
            timeout_count_down = self.timeout
            while(process.poll() is None and timeout_count_down > 0):
                timeout_count_down -= 1
                time.sleep(1)

            process_was_terminated = False
            if process.poll() is None:
                process.terminate()
                time.sleep(2)
                process_was_terminated = True

            stdout_file.close()
            stderr_file.close()

            if os.path.exists(output_filename):
                output_file = open(output_filename, 'r')
                output = pickle.load(output_file)
                output_file.close()
                self._run_unittest_parse_result(output)

            if os.path.exists(stdout_filename):
                stdout_file = open(stdout_filename, 'r')
                stdout = stdout_file.read()
                stdout_file.close()

            if os.path.exists(stderr_filename):
                stderr_file = open(stderr_filename, 'r')
                stderr = stderr_file.read()
                stderr_file.close()

            if process_was_terminated:
                return JobDoUnittestPart.JOB_STATUS_DONE_TIMEOUT
            try:
                if os.path.isdir(tempfolder):
                    shutil.rmtree(tempfolder)
            except:
                pass
            rc = process.poll()
            if rc != 0:
                return JobDoUnittestPart.JOB_STATUS_DONE_ERROR
            return JobDoUnittestPart.JOB_STATUS_DONE_OK

        except Exception as error:
            print error
            return JobDoUnittestPart.JOB_STATUS_DONE_ERROR



class JobDoUnittest(Job):
    def __init__(self, dev_env, ifilter=None, efilter=None, devbotnet_slave_info=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.devbotnet_slave_info = devbotnet_slave_info
        self.ifilter = ifilter
        self.efilter = efilter

    def do_run(self):
        self.dev_env.cleanup_temp()
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest'), self.ifilter, self.efilter)
        for (py_filename, py_modulename) in unittests:
            unittest_job = JobDoUnittestPart(self.dev_env, py_filename, py_modulename)
            unittest_job.start()
            unittest_job.join()
            print(py_filename, unittest_job.status)
        return Job.JOB_STATUS_DONE

    def get_job_info(self, job_info=None):
        return Job.get_job_info(self, self.job_info)



class JobDoCoverage(Job):
    def __init__(self, dev_env, ifilter=None, efilter=None, devbotnet_slave_info=None, coverage_out_folder=None):
        Job.__init__(self, dev_env.temp_root)
        self.dev_env = dev_env
        self.devbotnet_slave_info = devbotnet_slave_info
        self.ifilter = ifilter
        if self.ifilter is None:
            self.ifilter = ""
        self.efilter = efilter
        if self.efilter is None:
            self.efilter = ""
        self.coverage_out_folder = coverage_out_folder

    def do_run(self):
        import coverage
        cov_filename = self.dev_env.generate_temp_filename()
        cov = coverage.coverage(data_file=cov_filename)
        cov.erase()
        error_found = False

        self.dev_env.cleanup_temp()
        unittests = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(prefix='unittest'), self.ifilter, self.efilter)
        unittests_and_job = []
        for (py_filename, py_modulename) in unittests:
            unittest_job = JobDoUnittestPart(self.dev_env, py_filename, py_modulename, enable_profiling=False, enable_details=False, coverage_filename=cov_filename)
            unittest_job.start()
            unittest_job.join()
            if unittest_job.status not in [JobDoUnittestPart.JOB_STATUS_DONE_OK, JobDoUnittestPart.JOB_STATUS_DONE_IGNORED]:
                error_found = True
            unittests_and_job.append( (py_filename, py_modulename, unittest_job) )

        print("="*80)
        print("Summary " * 10)
        print("="*80)
        for (py_filename, py_modulename, job) in unittests_and_job:
            print '%-10s %s' % (JobDoUnittestPart.JOB_STATUS[job.status+1], py_filename)

        print("="*80)
        print("Coverage report can be found in %s" % self.coverage_out_folder)
        print("="*80)
        try:
            cov = coverage.coverage(data_file=cov_filename)
            cov.load()
            cov.html_report(ignore_errors=1, directory=self.coverage_out_folder, omit=["*/unittest_*", "/usr/lib/python2.7/*"])
        except Exception as error:
            print(error)
            return Job.JOB_STATUS_DONE_ERROR

        if error_found:
            return Job.JOB_STATUS_DONE_ERROR
        else:
            return Job.JOB_STATUS_DONE_OK


if __name__ == '__main__':
    import pickle
    import lib.dev_env
    import lib.dev_env.tools
    import lib.version

    import lib.dev_env.devbotnet_db

    version = lib.version.Version.create_current()
    dev_env = lib.dev_env.DevEnv(lib.dev_env.tools.ToolsSetup.create_default(), HG_ROOT, version)

#    job = JobDoUnittest(dev_env, db_session, ifilter=".*", efilter="")
#    job = JobDoProfile(dev_env, db_session, ifilter=".*gpm.*", efilter="")
    job = JobDoCoverage(dev_env, ifilter=".*gpm.*", efilter="")
#    job = JobDoStaticAnalysis(dev_env, db_session, ifilter=".*gpm.*", efilter="")
    job.run()
