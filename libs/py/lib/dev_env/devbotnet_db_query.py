'''
Created on Jun 16, 2011

@author: thwang
'''
import devbotnet_db
import lib.dev_env.hg

import sqlalchemy


def do_changeset_exist(db_session, hg_repo, hg_revision):
    count = db_session.query(devbotnet_db.DevBotnetDBChangeset).filter_by(repository=hg_repo, changeset=hg_revision).count()
    return count > 0

def get_changeset(db_session, hg_repo, hg_revision):
    try:
        return db_session.query(devbotnet_db.DevBotnetDBChangeset).filter_by(repository=hg_repo).filter_by(changeset=hg_revision).one()
    except:
        pass
    return None


def add_pct_threshold(data, result):

    if data.sum_count > 0:
        pct = int(data.sum_count_info * 100 / data.sum_count)
    
        if pct > 80:
            result['ok_pct'] = pct
            result['diff_ok_pct'] = 0
        elif pct < 60:
            result['error_pct'] = pct
            result['diff_error_pct'] = 0
        else:
            result['warning_pct'] = pct
            result['diff_warning_pct'] = 0


def get_job_summary_for_revision(db_session, hg_repo, hg_revision, type):
    db_changeset = get_changeset(db_session, hg_repo, hg_revision)

    if db_changeset is None:
        return []
    
    result = []
    for db_run in db_session.query(devbotnet_db.DevBotnetDBRun).filter_by(changeset_uid=db_changeset.uid).filter_by(type=type).order_by(sqlalchemy.desc(devbotnet_db.DevBotnetDBRun.begin_ts)).all():
        result_job_sum = {}
        result_job_sum['uid'] = db_run.uid
        result_job_sum['computer'] = db_run.computer
        result_job_sum['arch_target'] = db_run.arch_target

        if db_run.begin_ts is not None and db_run.end_ts is not None:
            result_job_sum['begin_ts'] = db_run.begin_ts.strftime('%Y.%m.%d %H:%M')
            result_job_sum['end_ts'] = db_run.end_ts.strftime('%Y.%m.%d %H:%M')
        else:
            result_job_sum['begin_ts'] = ''
            result_job_sum['end_ts'] = ''
        
        result_job_sum['count'] = db_run.sum_count
        result_job_sum['info'] = db_run.sum_count_info
        result_job_sum['warning'] = db_run.sum_count_warning
        result_job_sum['error'] = db_run.sum_count_error
        
        (diff_count, diff_info, diff_warning, diff_error) = get_run_compare_diff(db_session, db_run)
        result_job_sum['diff_count'] = diff_count
        result_job_sum['diff_info'] = diff_info
        result_job_sum['diff_warning'] = diff_warning
        result_job_sum['diff_error'] = diff_error

        if db_run.type == devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE:
            add_pct_threshold(db_run, result_job_sum)
            
        result.append(result_job_sum)
        
    return result
    

def get_run_by_id(db_session, run_uid):
    return db_session.query(devbotnet_db.DevBotnetDBRun).filter_by(uid=run_uid).one()

def get_changeset_for_run(db_session, db_run):
    return db_session.query(devbotnet_db.DevBotnetDBChangeset).filter_by(uid=db_run.changeset_uid).one()
    
def get_run_parts(db_session, db_run):
    return db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(run_uid=db_run.uid).order_by(devbotnet_db.DevBotnetDBRunPart.module).all()
    
def get_run_part_by_id(db_session, run_part_uid):
    return db_session.query(devbotnet_db.DevBotnetDBRunPart).filter_by(uid=run_part_uid).one()

def get_run_part_details(db_session, db_run_part, type):
    return db_session.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(run_part_uid=db_run_part.uid).filter_by(type=type).order_by(devbotnet_db.DevBotnetDBRunPartDetail.line_begin).all()

def get_run_part_details_module(db_session, db_run_part):
    result = []
    for db_part_detail in db_session.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(run_part_uid=db_run_part.uid).all():
        if db_part_detail.type in [devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_PROFILE, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDERROR, devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDOUT]:
            result.append(db_part_detail)
    return result 

def get_run_part_detail_is_new(db_session, db_part_detail, type):

    db_part = get_run_part_by_id(db_session, db_part_detail.run_part_uid)
    if db_part.compare_run_part_uid is None:
        return True
    db_part_compare = get_run_part_by_id(db_session, db_part.compare_run_part_uid)
    for db_part_detail_compare in get_run_part_details(db_session, db_part_compare, type):
        if db_part_detail.is_equal(db_part_detail_compare):
            return False
    return True

def get_run_part_detail_by_id(db_session, run_part_detail_uid):
    return db_session.query(devbotnet_db.DevBotnetDBRunPartDetail).filter_by(uid=run_part_detail_uid).one()


def get_run_compare_diff(db_session, db_run):
    if db_run.compare_run_uid is None:
        return (0, 0, 0, 0)
    
    db_run_compare = get_run_by_id(db_session, db_run.compare_run_uid)
    
    diff_count = db_run.sum_count - db_run_compare.sum_count
    diff_info = db_run.sum_count_info - db_run_compare.sum_count_info
    diff_warning = db_run.sum_count_warning - db_run_compare.sum_count_warning
    diff_error = db_run.sum_count_error - db_run_compare.sum_count_error
    return (diff_count, diff_info, diff_warning, diff_error)
        

def get_run_part_compare_diff(db_session, db_run_part):
    if db_run_part.compare_run_part_uid is None:
        return (0, 0, 0, 0)
        
    db_run_part_compare = get_run_part_by_id(db_session, db_run_part.compare_run_part_uid)

    diff_count = db_run_part.sum_count - db_run_part_compare.sum_count
    diff_info = db_run_part.sum_count_info - db_run_part_compare.sum_count_info
    diff_warning = db_run_part.sum_count_warning - db_run_part_compare.sum_count_warning
    diff_error = db_run_part.sum_count_error - db_run_part_compare.sum_count_error
    return (diff_count, diff_info, diff_warning, diff_error)
    
