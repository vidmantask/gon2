'''
Created on Jun 14, 2011

@author: thwang
'''
import subprocess
import tempfile
import os.path
import os

HG_ROOT = os.path.normpath(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..")))


def read_file_content(filename):
    if os.path.exists(filename):
        myfile = open(filename, 'r')
        content = myfile.read()
        return content
    return None


class HGChange(object):
    def __init__(self, node, date, rev, desc, author):
        self.node = node
        self.date = date
        self.rev = rev
        self.desc = desc
        self.author = author

    @classmethod
    def create_from_repo(cls, hg_root=HG_ROOT, count_max=20):
        result = []
        (stdout_file, stdout_filename) = tempfile.mkstemp(text=True)
        command = ["hg", "log", "-l", str(count_max), "--template", "{node|short}#:#{date|isodate}#:#{rev}#:#{desc}#:#{author}<end>"]

        process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
        process.wait()

        process_result = process.communicate()
        process_result_out = process_result[0].replace('\n', '')
        process_result_out = process_result_out.replace('<end>', os.linesep)
        
        for line in process_result_out.splitlines():
            line_elements = line.split("#:#")
            node = line_elements[0]
            date = line_elements[1]
            rev = line_elements[2]
            desc = line_elements[3]
            author = line_elements[4]
            hg_change = HGChange(node, date, rev, desc, author)
            result.append(hg_change)

        return result
    
    @classmethod
    def create_from_repo_changeset(cls, hg_root=HG_ROOT, changeset='tip'):
        if changeset.endswith('+'):
            changeset = changeset[:len(changeset)-1]
    
        command = ["hg", "log", "-r", changeset, "--template", "{node|short}#:#{date|isodate}#:#{rev}#:#{desc}#:#{author}<end>"]
        process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
        process.wait()

        process_result = process.communicate()
        process_result_out = process_result[0].replace('\n', '')
        process_result_out = process_result_out.replace('<end>', os.linesep)

        line_elements = process_result_out.split("#:#")
        node = line_elements[0]
        date = line_elements[1]
        rev = line_elements[2]
        desc = line_elements[3]
        author = line_elements[4]
        return HGChange(node, date, rev, desc, author)

def hg_get_revision(hg_root=HG_ROOT):
    command = ["hg", "id", "-i"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].strip()

def get_changeset_local(hg_root=HG_ROOT, changeset='tip'):
    if changeset.endswith('+'):
        changeset = changeset[:len(changeset)-1]

    command = ["hg", "id", "-n", '-r', changeset]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].strip()

def get_changeset_global(hg_root=HG_ROOT, changeset='tip'):
    if changeset.endswith('+'):
        changeset = changeset[:len(changeset)-1]

    command = ["hg", "log", '-r', changeset, "--template", "{node|short}"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].strip()


def hg_uncommited_files(hg_root=HG_ROOT):
    command = ["hg", "st", "-mar"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    hg_status_output = process_result[0].rstrip()
    return hg_status_output.splitlines()
    
def hg_pull_with_update(hg_root=HG_ROOT):
    command = ["hg", "pull", "-u"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].rstrip()

def hg_check_for_news(hg_root=HG_ROOT):
    """
    Check remote repository for news, and return true if news are available
    """
    command = ["hg", "incoming"]
    process = subprocess.Popen(command, cwd=hg_root, stdout=subprocess.PIPE)
    process.wait()
    return process.returncode == 0


def hg_parent(hg_root=HG_ROOT, changeset='tip'):
    """
    This needs to be fixed, only using local prvious revision as parrent. 
    Should use parent of changeset (notise that parent of changeset is empty if only one parent)
    """
    if changeset.endswith('+'):
        changeset = changeset[:len(changeset)-1]

    hg_rev = get_changeset_local(hg_root, changeset)
    hg_rev_parent = int(hg_rev) - 1
    if hg_rev_parent < 1:
        return None
    
    hg_rev_parent_string = "%d" % hg_rev_parent
    return get_changeset_global(hg_root, hg_rev_parent_string)

if __name__ == '__main__':
    hg_root = '/home/thwang/source/main2'
    print hg_parent(hg_root)
