"""
This module contains functionality used by Development environment tools
"""
import sys
import os
import os.path
import string
import subprocess
import re
import shutil
import time
import tempfile

import tools

import lib.version
import lib.utility
import path_setup

TARGET_PLATFORM_MAC    = "mac_64"
TARGET_PLATFORM_LINUX  = "linux_64"
TARGET_PLATFORM_WIN    = "win"
def detect_platform():
    if sys.platform == "win32":
        return TARGET_PLATFORM_WIN
    elif sys.platform in [ "linux2", "linux" ]:
        return TARGET_PLATFORM_LINUX
    elif sys.platform == "darwin":
        return TARGET_PLATFORM_MAC
    return "unknown"

class DevEnv(object):
    SETUP_COMMAND_normal = 'normal'
    SETUP_COMMAND_release = 'release'

    def __init__(self, tools_setup, hg_root, version):
        self.tools_setup = tools_setup
        self.hg_root = hg_root
        self.version = version
        self.pack_root = os.path.join(self.hg_root, '..', 'gon_build', 'pack')
        self.build_root = os.path.join(self.hg_root, '..', 'gon_build', 'build', detect_platform(), 'build')
        self.py_root = os.path.join(self.hg_root, 'py')
        self.temp_root = os.path.join(self.hg_root, '..', 'gon_build', 'temp')
        self.cpp_lib3p_root = os.path.join(self.hg_root, '..', 'gon_build', 'dist', detect_platform())

        if not os.path.exists(self.temp_root):
            os.makedirs(self.temp_root)

    def setup(self, setup_command):
        self.expand_in_files(setup_command)

    def cleanup_temp(self):
        if not os.path.exists(self.temp_root):
            os.makedirs(self.temp_root)
            return

        for folder_item in os.listdir(self.temp_root):
            folder_item_name = os.path.join(self.temp_root, folder_item)
            folder_item_modifyed_time_ago_sec  = time.time() - os.path.getmtime(folder_item_name)

            try:
                if folder_item_modifyed_time_ago_sec > 60 * 60 * 4:
                    if os.path.isdir(folder_item_name):
                        shutil.rmtree(folder_item_name)
                    else:
                        os.remove(folder_item_name)
            except:
                print "Unable to remove temp-folder %s" % folder_item_name

    def generate_temp_filename(self):
        return os.path.join(tempfile.mkdtemp(dir=self.temp_root), 'tempfile')

    def expand_in_files(self, setup_command):
        dictionary = {}
        dictionary['GIRITECH_ROOT'] = self.hg_root
        dictionary['GIRITECH_ROOT_CPP'] = os.path.join(self.hg_root, 'cpp')
        dictionary['GIRITECH_ROOT_PY'] = self.py_root
        dictionary['GIRITECH_ROOT_JAVA'] = os.path.join(self.hg_root, 'java')

        dictionary['GIRITECH_OUT'] = os.path.join(self.hg_root, 'gon_build', 'out')
        dictionary['GIRITECH_DOC_OUT'] = os.path.join(self.hg_root, 'build', 'doc')
        dictionary['GIRITECH_DOC_IMAGES'] = os.path.join(self.hg_root, 'cpp', 'doc', 'images')

        dictionary['GIRITECH_VERSION_BRANCH_ID'] = "Main"
        dictionary['GIRITECH_VERSION_COMPANY_NAME'] = "Soliton Systems"
        dictionary['GIRITECH_VERSION_COPYRIIGHT_NOTICE'] = "Copyright 2003-2019 by Soliton Systems"
        dictionary['GIRITECH_VERSION_MAJOR'] = self.version.get_version_major()
        dictionary['GIRITECH_VERSION_MINOR'] = self.version.get_version_minor()
        dictionary['GIRITECH_VERSION_BUGFIX'] = self.version.get_version_bugfix()
        dictionary['GIRITECH_VERSION_BUILD_ID'] = self.version.get_version_build_id()
        dictionary['GIRITECH_VERSION_DEPOT'] = self.version.get_version_depot()
        dictionary['GIRITECH_VERSION_NUM'] = self.version.get_version_string()
        dictionary['GIRITECH_VERSION_NUM_NUM'] = self.version.get_version_string_num()
        dictionary['GIRITECH_VERSION'] = "%s(%s)" % (self.version.get_version_string(), self.version.get_version_depot())
        dictionary['GIRITECH_BUILD_DATE'] = self.version.get_build_date_string()

        license_pub_key_filename = 'invalid_filename_unknown_setup_command'
        if setup_command == DevEnv.SETUP_COMMAND_normal:
            license_pub_key_filename = os.path.join(self.hg_root, 'setup', 'dev_env', 'license', 'dev.pub')
            license_pub_key_rsa_filename = os.path.join(self.hg_root, 'setup', 'dev_env', 'license', 'dev_rsa.pub')
        if setup_command == DevEnv.SETUP_COMMAND_release:
            license_pub_key_filename = os.path.join(self.hg_root, 'setup', 'release', 'license', 'release.pub')
            license_pub_key_rsa_filename = os.path.join(self.hg_root, 'setup', 'release', 'license', 'release_rsa.pub')
        license_pub_key_file = open(license_pub_key_filename, 'r')
        dictionary['GIRITECH_LICENSE_PUB'] = license_pub_key_file.read()
        license_pub_key_file.close()

        license_pub_key_rsa_file = open(license_pub_key_rsa_filename, 'r')
        dictionary['GIRITECH_LICENSE_PUB_RSA'] = license_pub_key_rsa_file.read()
        license_pub_key_rsa_file.close()


        dictionary['PYTHON_EXECUTABLE'] = self.tools_setup.get_python()

        dictionary['gpm_build_root'] = "${gpm_build_root}"
        dictionary['name'] = "$name"
        dictionary['file'] = "$file"
        dictionary['line'] = "$line"
        dictionary['text'] = "$text"
        dictionary['version'] = "$version"

        for filename_in in self.select_files_with_suffix(self.hg_root, '.in'):
            (filename_out, filename_ext) = os.path.splitext(filename_in)
            self.expand_in_file(filename_in, filename_out, dictionary)

    def expand_in_file(self, from_filename, to_filename, dictionary):
        try:
            file_in = open(from_filename, 'r')
            file_content = string.Template(file_in.read()).safe_substitute(dictionary)
            file_in.close()

            file_out = open(to_filename, 'w')
            file_out.write(file_content)
            file_out.close()

        except ValueError:
            (etype, evalue, etrace) = sys.exc_info()
            print from_filename
            print evalue
        except KeyError:
            (etype, evalue, etrace) = sys.exc_info()
            print from_filename
            print evalue

    def select_files_with_suffix(self, root, suffix, ifilter=None, ifilters=None):
        result = []

        if ifilter is not None:
            ifilter_re = re.compile(ifilter)

        for (dirpath, dirnames, filenames) in os.walk(root):
            for filename in filenames:
                filename_no_ext = os.path.splitext(filename)[0]
                if filename.endswith(suffix):
                    in_filter = False
                    if ifilter is None and ifilters is None:
                        in_filter = True
                    elif ifilter is not None and ifilter_re.match(filename):
                        in_filter = True
                    else:
                        for filter in ifilters:
                            if filename_no_ext.startswith(filter):
                                in_filter = True
                    if in_filter:
                        result.append(os.path.join(dirpath, filename))
        return result

    def select_files_py(self, prefix=None, exclude_prefix=False):
        result = []
        py_filenames = self.select_files_with_suffix(self.py_root, '.py')
        for py_filename_abs in py_filenames:
            py_filename = os.path.basename(py_filename_abs)
            if prefix is not None:
                if exclude_prefix:
                    if not py_filename.startswith(prefix):
                        result.append(py_filename_abs)
                else:
                    if py_filename.startswith(prefix):
                        result.append(py_filename_abs)
            else:
                result.append(py_filename_abs)
        return result

    def py_filename_to_modulename(self, py_filename):
        py_modulename = None
        if py_filename.startswith(self.py_root):
            py_modulename = py_filename[len(self.py_root)+1:]
            (py_modulename, py_modulename_ext) = os.path.splitext(py_modulename)
            py_modulename = py_modulename.replace('/', '.').replace('\\', '.')
            if py_modulename.endswith('__init__'):
                py_modulename = py_modulename[:len(py_modulename)-9]
        return py_modulename

    def aply_ifilter_to_modulename(self, filenames, ifilter, efilter=''):
        result = []
        try:
            ifilter_re = re.compile(ifilter)
        except re.error:
            ifilter_re = re.compile('.*')

        try:
            efilter_re = re.compile(efilter)
        except re.error:
            efilter_re = re.compile('')

        for filename in filenames:
            modulename = self.py_filename_to_modulename(filename)
            if ifilter_re.match(modulename):
                if efilter == '' or not efilter_re.match(modulename):
                    result.append( (filename, modulename) )
        return result

    def generate_build_pack_destination(self, name):
        platform_folder = sys.platform
        if sys.platform == 'win32':
            platform_folder = 'win'
        if sys.platform == 'linux2':
            platform_folder = 'linux'
        if sys.platform == 'darwin':
            platform_folder = 'mac'
        return os.path.join(self.build_root, 'pack', self.version.get_version_string().strip(), name, platform_folder)

    def generate_pack_destination(self, name):
        platform_folder = sys.platform
        if sys.platform == 'win32':
            platform_folder = 'win'
        if sys.platform == 'linux2':
            platform_folder = 'linux'
        if sys.platform == 'darwin':
            platform_folder = 'mac'
        return os.path.join(self.pack_root, name, platform_folder)

    def generate_build_destination(self, name):
        platform_folder = sys.platform
        if sys.platform == 'win32':
            platform_folder = 'win'
        if sys.platform == 'linux2':
            platform_folder = 'linux'
        if sys.platform == 'darwin':
            platform_folder = 'mac'
        return os.path.join(self.build_root, self.version.get_version_string().strip(), name, platform_folder)

    def get_pack_root(self):
        return self.pack_root.strip()

    def get_build_pack_root(self):
        return os.path.join(self.build_root, 'pack', self.version.get_version_string()).strip()

    @classmethod
    def create_current(cls, tools_setup=None):
        version = lib.version.Version.create_current()
        hg_root = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..'))
        if tools_setup is None:
            tools_setup = tools.ToolsSetup()

        return DevEnv(tools_setup, hg_root, version)



class DevEnvInstallation(object):
    def __init__(self, dev_env, installation_root=None):
        self.dev_env = dev_env
        self.hg_root = self.dev_env.hg_root
        self.tools_setup = self.dev_env.tools_setup
        self.installation_root = installation_root
        if self.installation_root is None:
            self.installation_root = os.path.normpath(os.path.join(self.dev_env.hg_root, 'setup', 'dev_env', 'gon_installation'))
        self.installation_root_template = os.path.join(self.dev_env.hg_root, 'setup', 'dev_env', 'gon_installation_template')
        self.gon_server_gateway_service_root = os.path.join(self.installation_root, 'gon_server_gateway_service', 'win')
        self.gon_server_management_service_root = os.path.join(self.installation_root, 'gon_server_management_service', 'win')
        self.gon_config_service_root = os.path.join(self.installation_root, 'gon_config_service', 'win')
        self.gon_client_management_service_root = os.path.join(self.installation_root, 'gon_client_management_service', 'win')
        self.gon_client_root = os.path.join(self.installation_root,'gon_client', 'win')
        self.gon_client_stealth_root = os.path.join(self.installation_root,'gon_client_stealth', 'win')
        self.gon_client_installer_root = os.path.join(self.installation_root, 'gon_client_installer', 'win')
        self.gon_config_root = os.path.join(self.installation_root, 'config')

    def restore_installation_root(self):
        self.restore_template()
        self.restore_plugins(restore_complete=True)
        self.generate_gpms()
        self.generate_setupdata()

    def restore_template(self):
        if os.path.exists(self.installation_root):
            shutil.rmtree(self.installation_root)
        lib.utility.copytree(self.installation_root_template, self.installation_root)

    def restore_plugins(self, restore_complete=True):
        ignore_filter = None
        if not restore_complete:
            ignore_filter = self._restore_plugins_ignore_ini_files
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_server_gateway_service_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_server_management_service_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_config_service_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_client_management_service_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_client_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_client_stealth_root, 'plugin_modules'), ignore=ignore_filter)
        lib.utility.copytree(os.path.join(self.dev_env.py_root, 'plugin_modules'), os.path.join(self.gon_client_installer_root, 'plugin_modules'), ignore=ignore_filter)

    def _restore_plugins_ignore_ini_files(self, src, names):
        ignore_files = []
        for filename in names:
            (root, ext) = os.path.splitext(filename)
            if ext == '.ini':
                ignore_files.append(filename)
        return ignore_files

    def generate_gpms(self):
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_config_service', 'gon_config_service.py') , '--generate_gpms']
        return subprocess.call(command, cwd=self.gon_config_service_root, env=env)

    def generate_setupdata(self):
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_config_service', 'gon_config_service.py') , '--generate_setupdata']
        return subprocess.call(command, cwd=self.gon_config_service_root, env=env)

    def update_license(self):
        license_filename = os.path.join(self.gon_config_root, 'deployed', 'gon_license.lic')
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_license', 'gon_license.py') , '--stamp', '--license_file', license_filename, '--keypair_name', 'dev', '--keypair_folder', '../license']
        return subprocess.call(command, cwd=self.installation_root_template, env=env)

    def do_gon_client_run(self):
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_client', 'gon_client.py')]
        return subprocess.call(command, cwd=self.gon_client_root, env=env)

    def do_gon_client_first_start(self):
        deployed_folder = os.path.join(self.gon_config_root, 'deployed')
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'setup', 'tools', 'generate_first_start.py'), deployed_folder]
        return subprocess.call(command, cwd=self.gon_client_root, env=env)

    def do_gon_client_stealth_run(self):
        env = os.environ
        env['PYTHONPATH'] = path_setup.generate_path()
        command = [self.tools_setup.get_python(), os.path.join(self.dev_env.py_root, 'appl', 'gon_client_stealth', 'gon_client_stealth.py')]
        return subprocess.call(command, cwd=self.gon_client_stealth_root, env=env)
