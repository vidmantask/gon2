"""
Unittest of utility ip module
"""
import unittest
from lib import giri_unittest

import lib.utility.ip


class IPTest(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_ips(self):
        self.assertEqual(lib.utility.ip.build_connect_host_address('127.0.0.1'), '127.0.0.1')
        self.assertEqual(lib.utility.ip.build_connect_host_address('::1'), '::1')
        
        self.assertEqual(lib.utility.ip.build_connect_host_address(''), '127.0.0.1')
        self.assertEqual(lib.utility.ip.build_connect_host_address('0.0.0.0'), '127.0.0.1')
        self.assertEqual(lib.utility.ip.build_connect_host_address('::'), '::1')
        self.assertEqual(lib.utility.ip.build_connect_host_address('0:0:0:0:0:0:0:0'), '::1')

        self.assertEqual(lib.utility.ip.build_connect_host_address('192.168.42.42'), '192.168.42.42')

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
