'''
Created on Oct 28, 2011

@author: thwang
'''

def build_connect_host_address(host):
    
    # unspecified address
    if host.strip() in ['']:
        host = '127.0.0.1'

    # ipv4 unspecified address
    elif host.strip() in ['0.0.0.0']:
        host = '127.0.0.1'

    # ipv6 unspecified address
    elif host.strip() in ['0:0:0:0:0:0:0:0', '::']:
        host = '::1'
    
    return host


