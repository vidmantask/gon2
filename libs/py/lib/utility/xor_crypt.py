'''
Created on 08/02/2011

@author: pwl

This code was 'stolen' from http://www.daniweb.com/code/snippet216912.html
'''
import operator
import StringIO
import base64
import uuid
import os.path

xor_salt = "The secret salt"

def load_and_set_scramble_salt(scramble_salt_filename, set_if_not_found=True):
    if not os.path.isfile(scramble_salt_filename):
        if not set_if_not_found:
            return
        generate_scramble_salt(scramble_salt_filename)
    salt_file = open(scramble_salt_filename, 'rb')
    salt = salt_file.read()
    salt_file.close()
    global xor_salt 
    xor_salt = salt

def generate_scramble_salt(scramble_salt_filename):
    scramble_salt_folder = os.path.dirname(scramble_salt_filename)
    if not os.path.exists(scramble_salt_folder):
        os.makedirs(scramble_salt_folder)
    salt_file = open(scramble_salt_filename, 'wb')
    salt_file.write(str(uuid.uuid4()))
    salt_file.close()


def _xor_crypt(xor_salt, str2):
    # create two streams in memory the size of the string str2
    # one stream to read from and the other to write XOR crypted character to
    sr = StringIO.StringIO(str2)
    sw = StringIO.StringIO(str2)
    # make sure we start both streams at position zero (beginning)
    sr.seek(0)
    sw.seek(0)
    for k in range(len(str2)):
        # loop through password start to end and repeat
        n = k % len(xor_salt)
        p = ord(xor_salt[n])
        n += 1
        # read one character from stream sr
        c = sr.read(1)
        b = ord(c)
        # xor byte with password byte
        t = operator.xor(b, p)
        z = chr(t)
        # advance position to k in stream sw then write one character
        sw.seek(k)
        sw.write(z)
    # reset stream sw to beginning
    sw.seek(0)
    str3 = sw.read()
    return str3

def xor_crypt(str1):
    str3 = _xor_crypt(xor_salt, str1)
#    return base64.encodestring(str3).strip()
    return base64.b64encode(str3)

def xor_decrypt(str1):
    str2 = base64.b64decode(str1)
    return _xor_crypt(xor_salt, str2)


if __name__ == '__main__':
    
    
    
    str1 = "This should be encrypted"
    str2 = xor_crypt(str1)
    print "Encrypted: '%s'" % str2
    str3 = xor_decrypt(str2)
    print "Decrypted: '%s'" % str3


    str1 = "GiriGiri"
    str2 = xor_crypt(str1)
    print "Encrypted: '%s'" % str2
    str3 = xor_decrypt(str2)
    print "Decrypted: '%s'" % str3


    str1 = "123456\npassword"
    str2 = xor_crypt(str1)
    print "Encrypted: '%s'" % str2
    str3 = xor_decrypt(str2)
    print "Decrypted: '%s'" % str3

    str1 = ""
    str2 = xor_crypt(str1)
    print "Encrypted: '%s'" % str2
    str3 = xor_decrypt(str2)
    print "Decrypted: '%s'" % str3


