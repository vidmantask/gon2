"""
This module contains varions common utilities
"""
import hashlib
import sys
import os
import os.path
import stat
import time
import shutil

# Got this from shutil.py (WindowsError is not defined on other platforms that windows)
try:
    WindowsError
except NameError:
    WindowsError = None


def calculate_checksum_for_file(filename):
    """
    Return the hex digest of a file without loading it all into memory
    """
    fh = open(filename, 'rb')
    checksum = hashlib.sha1()
    while 1:
        buf = fh.read(4096)
        if buf == "":
            break
        checksum.update(buf)
    fh.close()
    return checksum.hexdigest()

def rmtree_fail_safe(root_dir, try_count=10):
    """
    Removed all dir's and folders under root_dir, using rmtree, except that if a
    exception is thrown by rmtree, we try a number of time.
    This is usefull when removing a folder on Windows where a indexing service
    might temporary prevent a file/folder from deletion.
    """
    if try_count < 1:
        rmtree(root_dir)
    else:
        try:
            rmtree(root_dir)
        except:
            time.sleep(1)
            rmtree_fail_safe(root_dir, try_count - 1)

def rmtree(root_dir):
    """
    Removes all dir's and folders under root_dir. Same as shutil.rmtree, except that writable flag
    is set on each file before delete in order to avoid access errors.
    """
    for root, dirs, files in os.walk(root_dir, topdown=False):
        for name in files:
            path = os.path.join(root, name)
            os.chmod(path, stat.S_IWRITE)
            os.remove(path)
        for name in dirs:
            os.rmdir(os.path.join(root, name))

def copytree(src, dst, symlinks=False, ignore=None):
    """
    Same as shutil.copytree, except that dest migtht already exists.

    Code is taken from shutil.copytree and modified.
    """
    names = os.listdir(src)
    if ignore is not None:
        ignored_names = ignore(src, names)
    else:
        ignored_names = set()

    if not os.path.exists(dst):  # Modified
        os.makedirs(dst)         # Modified

    errors = []
    for name in names:
        if name in ignored_names:
            continue
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks, ignore)
            else:
                shutil.copy2(srcname, dstname)
            # XXX What about devices, sockets etc.?
        except (IOError, os.error), why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except shutil.Error, err:
            errors.extend(err.args[0])
    try:
        shutil.copystat(src, dst)
    except OSError, why:
        if WindowsError is not None and isinstance(why, WindowsError):
            # Copying file access times may fail on Windows
            pass
        else:
            errors.extend((src, dst, str(why)))
    if errors:
        raise shutil.Error, errors





def get_home_folder():
    """
    Returns the homefolder of the user. Notice that expanduser has a unicode problem.
    """
    encoding = sys.getfilesystemencoding()
    if encoding is None:
        return os.path.expanduser('~')
    return os.path.expanduser('~').decode(encoding)
