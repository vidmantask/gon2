"""
Functions to help  track down "memory leaks"
"""
import sys
import types
import gc

import pygraph.classes.digraph
import pygraph.algorithms.traversal
import pygraph.readwrite.dot

def get_refcounts():
    d = {}
    sys.modules
    # collect all classes
    for m in sys.modules.values():
        for sym in dir(m):
            o = getattr (m, sym)
            class_name = get_object_classname(o)
            if object in type(o).__bases__ and class_name is not None:
                d[o] = sys.getrefcount(o)
                print m, sym, type(o).__name__, sys.getrefcount(o), get_object_classname(o)
#            if type(o).__name__ == 'instance':
#                print o.__class__.__name__
#            if type(o) == type(object):
#                print sym, type(o).__flags__
#            if type(o) in [types.ClassType, types.InstanceType]:
#                print sym, "ccccccccccccccccccccc"
#                d[o] = sys.getrefcount (o)
#                print "XXXXXXXXXXXX"
    pairs = map (lambda x: (x[1],x[0]), d.items())
    return pairs

def get_object_classname(o):
    class_skip_list = ['tuple', 'wrapper_descriptor', 'function', 'builtin_function_or_method', 'dict', 'method_descriptor', 'weakref', 'member_descriptor', 'getset_descriptor',
                       'list', 'module','type', 'set', 'class', 'instancemethod', 'classmethod', 'classmethod_descriptor', 'frame', 'cell',
                       'OrderedSet', 'property', 'String', 'Boolean', 'Integer', 'And', 'Text', 'Word', 'int', 'float', 'NoneType', 'method-wrapper', 'file', 
                       '_Feature', '_Printer', '_Helper', '_Environ', '_NullToken', 'Quitter', 'floatinfo']
    try:
        class_type = o.__class__
        class_name = o.__class__.__name__ 
        if not class_name in class_skip_list:
            return class_name
    except:
        pass
    return None


def get_refcounts_class():
    class_refs = {}
    for o in gc.get_objects():
        class_name = get_object_classname(o)
        if class_name is not None:
            if class_refs.has_key(class_name):
                class_refs[class_name] += 1
            else:
                class_refs[class_name] = 1
    pairs = map (lambda x: (x[1],x[0]), class_refs.items())
    return pairs

def get_top_as_string(refcounts, n=50):
    refcounts.sort()
    refcounts.reverse()
    result = ""
    for n, c in refcounts[:n]:
        result += '%10d %s\n' % (n, c)
    return result

def get_flat_objects(o):
    if type(o) == types.DictionaryType:
        objects = []
        for so in o.values():
            objects.extend(get_flat_objects(so))
        return objects
    elif type(o) == types.ListType:
        objects = []
        for so in o:
            objects.extend(get_flat_objects(so))
        return objects
    else:
        return [o]

def get_refcount_graph():
    nodes = []
    edges = []
    class_refs = {}
    for o in gc.get_objects():
        class_name = get_object_classname(o)
        if class_name is not None and class_name in ['TestA', 'TestB']:
            for o_ref in gc.get_referents(o):
                for o_ref_flat in get_flat_objects(o_ref):
                    class_name_ref = get_object_classname(o_ref_flat)
                    if class_name_ref is not None :
                        edges.append( (class_name_ref, class_name) )
    refcount_graph = pygraph.classes.digraph.digraph()
    for (from_node, to_node) in edges:
        if not refcount_graph.has_node(from_node):
            refcount_graph.add_node(from_node)
        if not refcount_graph.has_node(to_node):
            refcount_graph.add_node(to_node)
    
    for edge in edges:
        if not refcount_graph.has_edge(edge):
            refcount_graph.add_edge(edge)
        else:
            weight = refcount_graph.edge_weight(edge) + 1
            refcount_graph.set_edge_weight(edge, weight)
            refcount_graph.set_edge_label(edge, weight)
    return pygraph.readwrite.dot.write(refcount_graph)
            




#    print get_top_as_string(get_refcounts_class())
#    print gc.garbage
#   
if __name__ == '__main__':
        class TestA(object):
            def __init__(self):
                pass
            
        class TestB(object):
            def __init__(self):
                self.aer = []    
        
        t_a = TestA()
        t_aa = TestA()
        t_aaa = TestA()
        t_b = TestB() 
        t_b.aer = [TestA(), TestA()]
        t_b.ae = TestA()
#    print get_refcounts()
        print get_top_as_string(get_refcounts_class(), 1000)
#    print get_refcount_graph()
    
    