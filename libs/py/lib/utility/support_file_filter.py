'''
Created on 05/03/2010

@author: peter
'''

sensitive_replace_string = "#<-- Potential sensitive information deleted -->\n"
 

sensitive_list = ["password", "connect_string"]

def contains_sensitive_information(text):
    for s in sensitive_list:
        if s in text:
            return True
    
    return False


def sensitive_copy_file(file_name_src, file_name_dest):
    in_file = file(file_name_src, "rt")
    out_file = file(file_name_dest, "wt")
    for line in in_file.readlines():
        if contains_sensitive_information(line):
            out_file.write(sensitive_replace_string)
        else:
            out_file.write(line)
    in_file.close()
    out_file.close()
            
    


