"""How the checkpoint interface should look to python"""
from __future__ import with_statement
import traceback
import logging
import os
import os.path
import tempfile
import shutil
import sys
import re
import datetime

import lib.encode
        
# TODO: Encapsulate these with "type=" somehow?
DEBUG = 'debug'
INFO = 'info'
WARNING = 'warning'
ERROR = 'error'
CRITICAL = 'critical'

import lib_cpp.communication

Structure = lib_cpp.communication.Structure

CheckpointFilter_true = lib_cpp.communication.CheckpointFilter_true
CheckpointFilter_false = lib_cpp.communication.CheckpointFilter_false
CheckpointFilter_or = lib_cpp.communication.CheckpointFilter_or
CheckpointFilter_and = lib_cpp.communication.CheckpointFilter_and
CheckpointFilter_not = lib_cpp.communication.CheckpointFilter_not
CheckpointFilter_checkpoint_ids = lib_cpp.communication.CheckpointFilter_checkpoint_ids
CheckpointFilter_skip_complete = lib_cpp.communication.CheckpointFilter_skip_complete
CheckpointFilter_accept_if_attr_exist = lib_cpp.communication.CheckpointFilter_accept_if_attr_exist


CheckpointOutputHandlerFolderXML = lib_cpp.communication.CheckpointOutputHandlerFolderXML

CheckpointOutputHandlerCallback = lib_cpp.communication.CheckpointOutputHandlerCallback
CheckpointOutputHandlerCallbackWrapper = lib_cpp.communication.CheckpointOutputHandlerCallbackWrapper_create


def string_to_utf8(value):
    if isinstance(value, unicode):
        return value.encode('utf-8', 'replace')
    return str(value)


def CheckpointOutputHandlerTEXT(filename=None, close_on_flush=None):
    if filename is None and close_on_flush is None:
        return lib_cpp.communication.CheckpointOutputHandlerTEXT()
    return lib_cpp.communication.CheckpointOutputHandlerTEXT(string_to_utf8(filename), close_on_flush)

def CheckpointOutputHandlerTEXTAppend(filename, close_on_flush):
    return lib_cpp.communication.CheckpointOutputHandlerTEXTAppend(string_to_utf8(filename), close_on_flush)

def CheckpointOutputHandlerXML(filename=None, close_on_flush=None):
    if filename is None and close_on_flush is None:
        return lib_cpp.communication.CheckpointOutputHandlerXML()
    return lib_cpp.communication.CheckpointOutputHandlerXML(string_to_utf8(filename), close_on_flush)

def CheckpointOutputHandlerXMLAppend(filename, close_on_flush):
    return lib_cpp.communication.CheckpointOutputHandlerXMLAppend(string_to_utf8(filename), close_on_flush)



get_version = lib_cpp.communication.get_version
#sim_throw = lib_cpp.communication.sim_throw

def convert_exception_to_summary(etype, evalue, etrace):
    try:
        summary = None
        evalue_lines = str(evalue).splitlines()
        if len(evalue_lines) > 0:
            summary = evalue_lines[0]
        else:
            summary = str(etype)
        if len(summary) > 100:
            summary = summary[:100]
        return summary
    except:
        exception_lines = traceback.format_exception(etype, evalue, etrace)
        if len(exception_lines) > 0:
            return exception_lines[len(exception_lines)-1]
    return "unprintable exception"


class CheckpointOutputHandler_Test(object):
    def __init__(self):
        self.checkpoint_output_test_handler = lib_cpp.communication.CheckpointOutputHandler_Test()
    
    def get_CheckpointOutputHandler(self):
        return lib_cpp.communication.get_CheckpointOutputHandler(self.checkpoint_output_test_handler)
    
    def got_checkpoint_id(self, cp_id):
        return lib_cpp.communication.got_checkpoint_id(self.checkpoint_output_test_handler, cp_id)

class CheckpointHandler(object):
    def __init__(self, filter, outputhandler, checkpoint_handler_parent=None):
        if checkpoint_handler_parent==None:
            self.checkpoint_handler = lib_cpp.communication.APICheckpointHandler_create(filter, outputhandler)
        else:
            self.checkpoint_handler = lib_cpp.communication.APICheckpointHandler_create_with_parent(filter, outputhandler, checkpoint_handler_parent.checkpoint_handler)
        
    def Checkpoint(self, cp_id, cp_module, cp_type, **cp_attr_dict):
        cp_attr_dict['module'] = cp_module
        cp_attr_dict['type'] = cp_type
        self.checkpoint_handler.Checkpoint(cp_id, CheckpointHandler.convert_value_unicode_to_string(cp_attr_dict))

    def CheckpointMultilineMessage(self, cp_id, cp_module, cp_type, cp_message, **cp_attr_dict):
        cp_attr_dict['type'] = cp_type
        cp_attr_dict['module'] = cp_module
        cp_attr_dict['content'] = cp_message
        self.checkpoint_handler.Checkpoint(cp_id, CheckpointHandler.convert_value_unicode_to_string(cp_attr_dict))

    def CheckpointMultilineMessages(self, cp_id, cp_module, cp_type, cp_messages, **cp_attr_dict):
        cp_attr_dict['type'] = cp_type
        cp_attr_dict['module'] = cp_module
        cp_attr_dict['content'] = "\n".join(cp_messages)
        self.checkpoint_handler.Checkpoint(cp_id, CheckpointHandler.convert_value_unicode_to_string(cp_attr_dict))

    def CheckpointException(self, cp_id, cp_module, cp_type, etype, evalue, etrace):
        cp_attr_dict = {}
        cp_attr_dict['type'] = cp_type
        cp_attr_dict['module'] = cp_module
        cp_attr_dict['content'] = ''.join(traceback.format_exception(etype, evalue, etrace))
        cp_attr_dict['summary'] = convert_exception_to_summary(cp_type, evalue, etrace)
        self.checkpoint_handler.Checkpoint(cp_id, CheckpointHandler.convert_value_unicode_to_string(cp_attr_dict))
        
    def CheckpointExceptionCurrent(self, cp_id, cp_module, cp_type=CRITICAL):
        (etype, evalue, etrace) = sys.exc_info()
        self.CheckpointException(cp_id, cp_module, cp_type, etype, evalue, etrace)

    def CheckpointScope(self, cp_id, cp_module, cp_type, **cp_attr_dict):
        cp_attr_dict['module'] = cp_module
        cp_attr_dict['type'] = cp_type
        return CheckpointScopeHandler(self.checkpoint_handler.CheckpointScope(cp_id, CheckpointHandler.convert_value_unicode_to_string(cp_attr_dict)))

    def close(self):
        self.checkpoint_handler.close()
        
    def set_output_handler(self, outputhandler):
        self.checkpoint_handler.set_output_handler(outputhandler)

    @staticmethod
    def convert_value_unicode_to_string(attr_dict):
        """Convert values in attr_dict to values c++ can handle"""
        d = {}
        for key, value in attr_dict.items():
            if isinstance(value, unicode):
                d[key] = value.encode('ascii', 'replace')
            else: # str, None, True, False, anything else .__str__() 
                d[key] = str(value)
        return d


    @classmethod
    def get_cout_all(cls):
        return CheckpointHandler(CheckpointFilter_true(), CheckpointOutputHandlerTEXT())

    @classmethod
    def get_cout_errors(cls):
        checkpoint_filter = CheckpointFilter_or(CheckpointFilter_accept_if_attr_exist('type', ERROR), 
                                                CheckpointFilter_accept_if_attr_exist('type', CRITICAL))
        return CheckpointHandler(checkpoint_filter, CheckpointOutputHandlerTEXT())

    @classmethod
    def get_NULL(cls):
        return CheckpointHandler(CheckpointFilter_false(), CheckpointOutputHandlerTEXT())

    @classmethod
    def get_handler(cls, enabled, verbose, filename, is_xml, close_on_flush=False, rotate=True, checkpoint_handler_parent=None):
        if(not enabled):
            checkpoint_filter = CheckpointFilter_false()
        else:
            if(verbose == 0):
                checkpoint_filter = CheckpointFilter_or(CheckpointFilter_accept_if_attr_exist('type', ERROR), 
                                                        CheckpointFilter_accept_if_attr_exist('type', CRITICAL))
            elif(verbose == 1):
                checkpoint_filter = CheckpointFilter_or(CheckpointFilter_or(CheckpointFilter_or(CheckpointFilter_accept_if_attr_exist('type', INFO), 
                                                                                                CheckpointFilter_accept_if_attr_exist('type', WARNING)),
                                                                                                CheckpointFilter_accept_if_attr_exist('type', ERROR)),
                                                                                                CheckpointFilter_accept_if_attr_exist('type', CRITICAL))
            else:
                checkpoint_filter = CheckpointFilter_true()

        checkpoint_output_handler = CheckpointHandler.get_output_handler(enabled, verbose, filename, is_xml, close_on_flush)
        return CheckpointHandler(checkpoint_filter, checkpoint_output_handler, checkpoint_handler_parent)

    @classmethod
    def get_handler_with_in_folder(cls, enabled, verbose, filename, is_xml, close_on_flush=False, rotate=True, in_folder_enabled=False, in_folder_path=None):
        checkpoint_handler_in_folder = CheckpointHandler.get_handler_to_xml_folder(in_folder_enabled, in_folder_path)
        return CheckpointHandler.get_handler(enabled, verbose, filename, is_xml, close_on_flush, rotate, checkpoint_handler_in_folder)

    @classmethod
    def get_output_handler(cls, enabled, verbose, filename, is_xml, close_on_flush=False, rotate=True):
        if rotate:
            if os.path.exists(filename):
                shutil.move(filename, generate_rotate_filename(filename))
        
        _close_on_flush = close_on_flush
        if(enabled and verbose > 1):
            _close_on_flush = False
        
        if is_xml and filename != None:
            checkpoint_output_handler = CheckpointOutputHandlerXML(filename, _close_on_flush)
        elif is_xml:
            checkpoint_output_handler = CheckpointOutputHandlerXML()
        elif filename != None: 
            checkpoint_output_handler = CheckpointOutputHandlerTEXT(filename, _close_on_flush)
        else:
            checkpoint_output_handler = CheckpointOutputHandlerTEXT()
        return checkpoint_output_handler

    @classmethod
    def get_handler_to_xml_folder(cls, enabled=False, in_folder_path=None):
        if not enabled or in_folder_path is None:
            return None
        if not os.path.isdir(in_folder_path):
            os.makedirs(in_folder_path)
        checkpoint_filter = CheckpointFilter_or(CheckpointFilter_accept_if_attr_exist('type', ERROR), CheckpointFilter_accept_if_attr_exist('type', CRITICAL))
        return CheckpointHandler(checkpoint_filter, CheckpointOutputHandlerFolderXML(in_folder_path))


def create_session_checkpoint_handler(parent_checkpoint_handler, unique_session_id, module_id, log_root, close_on_flush=False):
    unique_session_id_filename = unique_session_id.replace(':', '-')
    with parent_checkpoint_handler.CheckpointScope("create_session_checkpoint_handler", module_id, DEBUG) as cps:
        
        session_log_root_abs = os.path.abspath(log_root)
        if not os.path.isdir(session_log_root_abs):
            os.makedirs(session_log_root_abs)

        session_log_filename_abs = os.path.join(session_log_root_abs, '%s.log' % unique_session_id_filename)
        cps.add_complete_attr(session_log_filename=session_log_filename_abs)
        
        checkpoint_filter = CheckpointFilter_true()
        checkpoint_output_handler = CheckpointOutputHandlerXML(session_log_filename_abs, close_on_flush)
        return CheckpointHandler(checkpoint_filter, checkpoint_output_handler, parent_checkpoint_handler)


class CheckpointScopeHandler(object):   
    def __init__(self, checkpointscope):
        self.checkpointscope = checkpointscope
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        lib_cpp.communication.end_scope(self.checkpointscope)
        del self.checkpointscope

    def add_complete_attr(self, **attrs):
        lib_cpp.communication.add_complete_attr(self.checkpointscope, CheckpointHandler.convert_value_unicode_to_string(attrs))



class CheckPointLoggerWrapper(object):
    def __init__(self, checkpoint_handler, module_name):
        self.checkpoint_handler = checkpoint_handler
        self.module_name = module_name
        
    def _log(self, level, msg, *args, **kwargs):
        try:
            if args:
                message = msg % args
            else:
                message = msg
            self.checkpoint_handler.CheckpointMultilineMessage("%s_%s" % (self.module_name, level), self.module_name, level, message, **kwargs)
        except Exception, e:
            print e
            
    def isEnabledFor(self, something):
        return True
        
    def info(self, msg, *args, **kwargs):
        self._log(INFO, msg, *args, **kwargs)
   
    def debug(self, msg, *args, **kwargs):
        self._log(DEBUG, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        self._log(WARNING, msg, *args, **kwargs)
    
    def error(self, msg, *args, **kwargs):
        self._log(ERROR, msg, *args, **kwargs)
   
    def critical(self, msg, *args, **kwargs):
        self._log(CRITICAL, msg, *args, **kwargs)
        
    def log(self, level, msg, *args, **kwargs):
        self._log(INFO, msg, *args, **kwargs)
    
    def __getattr__(self, arg):
        self._log(ERROR, "Unhandled log attribute '%s'" % arg)
        return getattr(logging.getLogger('Default'), arg)
    

class CheckpointHandlerGenerator(object):
    def __init__(self, enabled, verbose, filename, is_xml, close_on_flush=False, rotate=True):
        self.enabled = enabled
        self.verbose = verbose
        self.filename = filename
        self.is_xml  = is_xml
        self.rotate = rotate
        self.close_on_flush = close_on_flush
        self.checkpoint_handler = CheckpointHandler.get_handler(self.enabled, self.verbose, self.filename, self.is_xml, self.close_on_flush, self.rotate)
     
    def get_handler(self):
        return self.checkpoint_handler
        
    def set_new_logfile_folder(self, new_folder, module_id):
        filename_basename = os.path.basename(self.filename)
        filename_folders = [new_folder, os.path.dirname(self.filename)]
        new_filename = self._find_good_filename(filename_basename, filename_folders)
        new_filename = lib.encode.preferred(new_filename, 'replace')
        new_output_handler = CheckpointHandler.get_output_handler(self.enabled, self.verbose, new_filename, self.is_xml, self.close_on_flush, self.rotate)
        self.checkpoint_handler.Checkpoint("relocate_log_file", module_id, INFO, new_filename=new_filename)
        self.checkpoint_handler.set_output_handler(new_output_handler)
        self.checkpoint_handler.Checkpoint("relocated_log_file", module_id, INFO, old_filename=self.filename)
        self.filename = new_filename
        
    def _find_good_filename(self, filename_basename, filename_folders_to_try):
        for filename_folder in filename_folders_to_try:
            try:
                filename = os.path.join(filename_folder, filename_basename)
                filename_try = os.path.join(filename_folder, 'good')
                wfile = open(filename_try, 'w')
                wfile.write('good')
                wfile.close()
                os.remove(filename_try)
                return filename
            except:
                pass
        # Giving up just returning the  last tried filename
        return filename


def generate_rotate_filename(filename):
    """
    Read first 15 bytes of filename (assuming that it is a log file) which contains a timestamp.
    Read from file 
    """
    if not os.path.isfile(filename):
        return filename
    
    logfile = open(filename, 'r')
    first_part = logfile.read(15)
    logfile.close()

    filename_root, filename_ext = os.path.splitext(filename)
    
    p = re.compile('([0-9]*)T([0-9]*)')
    m = p.match(first_part)
    if m is not None:
        rotate_filename = '%s-%s-%s%s' % (filename_root, m.group(1), m.group(2), filename_ext)
    else:
        now = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
        rotate_filename = '%s-%s%s' % (filename_root, now, filename_ext)
        
    return rotate_filename
    

