import tempfile
import tarfile
import os.path
import glob
import sys
import xml.parsers.expat


import lib.utility
import lib.gpm.gpm_spec


class GpmBuilder(object):
    GPM_META_FILENAME = 'gpm_meta.xml'
    GPM_META_ARCHIVE_FILENAME = 'gpm_meta.gz.tar'
    GPM_FILES_RO_ARCHIVE_FILENAME = 'gpm_files_ro.gz.tar'
    GPM_FILES_RW_ARCHIVE_FILENAME = 'gpm_files_rw.gz.tar'
    
    def __init__(self, gpm_build_env):
        self.gpm_build_env = gpm_build_env
        
    def build_from_file(self, filename, error_handler):
        gpm_def = lib.gpm.gpm_spec.GpmDef.parse_from_file(filename)
        self.build_from_spec(gpm_def, error_handler)
    
    def _build_tar_from_filelist(self, filelist, filename = None, tar_mode="w:gz"):
        if filename == None:
            (tar_file_xx, tar_filename) = tempfile.mkstemp(dir=self.gpm_build_env.temp_root)
            tar_file_xx = os.fdopen(tar_file_xx)
            tar_file_xx.close()
            tar_file = tarfile.open(tar_filename, mode=tar_mode, format=tarfile.PAX_FORMAT, encoding='UTF-8')
        else:
            tar_filename = filename
            tar_file = tarfile.open(tar_filename, mode=tar_mode, format=tarfile.PAX_FORMAT, encoding='UTF-8')
        for afile in filelist:
            if filelist[afile]['type'] == 'file':    
                tar_file.add(afile, filelist[afile]['dest'].encode('UTF-8'))
        tar_file.close()
        return tar_filename

    def _build_tar_from_gpm(self, gpm, tar_mode="w:gz"):
        (meta_file_xx, meta_filename) = tempfile.mkstemp(dir=self.gpm_build_env.temp_root)
        meta_file_xx = os.fdopen(meta_file_xx)
        meta_file_xx.close()
        gpm.to_tree().write(meta_filename, encoding='utf-8')

        (tar_file_xx, tar_filename) = tempfile.mkstemp(dir=self.gpm_build_env.temp_root)
        tar_file_xx = os.fdopen(tar_file_xx)
        tar_file_xx.close()
        tar_file = tarfile.open(tar_filename, mode=tar_mode, format=tarfile.PAX_FORMAT, encoding='UTF-8')

        tar_file.add(meta_filename, GpmBuilder.GPM_META_FILENAME.encode('UTF-8'))
        tar_file.close()
        return tar_filename

    def build_from_spec(self, gpmdef, error_handler):
        self.gpm_build_env.validate(error_handler)
        gpmdef.validate(self.gpm_build_env, error_handler)

        #
        # Generate filelists
        #
        filelist_ro = gpmdef.generate_filelist_ro(self.gpm_build_env, error_handler)
        filelist_rw = gpmdef.generate_filelist_rw(self.gpm_build_env, error_handler)

        #
        # Create meta information for file-package
        #
        gpm = lib.gpm.gpm_spec.Gpm()
        gpm.header = gpmdef.header
        gpm.dependency = gpmdef.dependency
        gpm.files_ro = lib.gpm.gpm_spec.GpmFiles.build_from_filelist(filelist_ro) 
        gpm.files_rw = lib.gpm.gpm_spec.GpmFiles.build_from_filelist(filelist_rw) 
        gpm.update()
        
        #
        # Create file-archives
        #
        archive_ro_filename = self._build_tar_from_filelist(filelist_ro)
        archive_rw_filename = self._build_tar_from_filelist(filelist_rw)
        archive_meta_filename = self._build_tar_from_gpm(gpm)

        #
        # Create gpm-file-package
        #
        filelist = {}
        filelist[archive_meta_filename] = {'dest': GpmBuilder.GPM_META_ARCHIVE_FILENAME, 'type': 'file'}
        if len(filelist_ro) > 0:
            filelist[archive_ro_filename] = {'dest': GpmBuilder.GPM_FILES_RO_ARCHIVE_FILENAME, 'type': 'file'}
        if len(filelist_rw) > 0:
            filelist[archive_rw_filename] = {'dest': GpmBuilder.GPM_FILES_RW_ARCHIVE_FILENAME, 'type': 'file'}

        gpm_file_package_filename = "%s.gpm" % gpm.header.get_package_id()
        gpm_file_package_filename_abs = os.path.join(self.gpm_build_env.dest_root, gpm_file_package_filename)
        self._build_tar_from_filelist(filelist, gpm_file_package_filename_abs, tar_mode="w" )

        return gpm_file_package_filename_abs



def query_meta(gpm_file_package_filename, error_handler):
    if not os.path.isfile(gpm_file_package_filename):
        return None
    gpm_file_package_archive = tarfile.open(gpm_file_package_filename, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')
    gpm_meta_archive = tarfile.open(name='fake', fileobj=gpm_file_package_archive.extractfile(GpmBuilder.GPM_META_ARCHIVE_FILENAME), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
    gpm_meta_file = gpm_meta_archive.extractfile(GpmBuilder.GPM_META_FILENAME)
    gpm_meta = lib.gpm.gpm_spec.Gpm.parse_from_file(gpm_meta_file)
    gpm_meta_file.close()
    gpm_meta_archive.close()
    gpm_file_package_archive.close()
    return gpm_meta


def query_meta_all_from_gpm_files(gpms_folder, error_handler):
    metas = []
    for gpm_filename in glob.glob(os.path.join(gpms_folder, '*.gpm')):
        gpm_filename_abs = os.path.join(gpms_folder, gpm_filename)
        gpm_meta = query_meta(gpm_filename_abs, error_handler)
        if gpm_meta is not None:
            metas.append(gpm_meta)
    return metas

def query_meta_all_from_files(gpm_meta_root, error_handler):
    metas = []
    for meta_filename in glob.glob(os.path.join(gpm_meta_root, '*.gpm.xml')):
        meta_file = file(meta_filename, 'r')
        try:
            meta = lib.gpm.gpm_spec.Gpm.parse_from_file(meta_file)
            metas.append(meta)
        except xml.parsers.expat.ExpatError, e:
            error_handler.emmit_warning(str(e))
        meta_file.close()
    return metas

def query_collection_meta_from_files(gpmcdef_root):
    metas = []
    for meta_filename in glob.glob(os.path.join(gpmcdef_root, '*.gpmcdef.xml')):
        meta_file = file(meta_filename, 'r')
        meta = lib.gpm.gpm_spec.GpmcDef.parse_from_file(meta_file)
        metas.append(meta)
        meta_file.close()
    return metas

def query_meta_from_from_file(gpm_meta_root, gpm_id, error_handler):
    meta_filename_abs = os.path.join(gpm_meta_root, '%s.gpm.xml' % gpm_id)
    if os.path.exists(meta_filename_abs):
        meta_file = file(meta_filename_abs, 'r')
        try:
            meta = lib.gpm.gpm_spec.Gpm.parse_from_file(meta_file)
        except xml.parsers.expat.ExpatError, e:
            error_handler.emmit_warning(str(e))
        meta_file.close()
        return meta
    return None

def remove_meta(gpm_meta_root, gpm_id, error_handler):
    meta_filename_abs = os.path.join(gpm_meta_root, '%s.gpm.xml' % gpm_id)
    if os.path.exists(meta_filename_abs):
        os.remove(meta_filename_abs)

def install_meta(gpm_meta_root, gpm_filename, error_handler):
    if os.path.exists(gpm_filename):
        meta = query_meta(gpm_filename, error_handler)
        if meta != None and meta.header != None:
            try:
                meta_filename_abs = os.path.join(gpm_meta_root, '%s.gpm.xml' % meta.header.get_package_id())
                meta_file = file(meta_filename_abs, 'w')
                meta.to_tree().write(meta_file)
                meta_file.close()
                return
            except IOError:
                (_, evalue, _) = sys.exc_info()
                error_handler.emmit_error("Unabel to install meta-file failed. %s" % (evalue))
    error_handler.emmit_warning("Installing meta from the package-file '%s' to '%s' failed." % (gpm_filename, gpm_meta_root))

def calculate_checksum(gpm_filename):
    if os.path.exists(gpm_filename):
        return lib.utility.calculate_checksum_for_file(gpm_filename)
    else:
        return None
        
def calculate_package_size(gpm_filename):
    if os.path.exists(gpm_filename):
        gpm_package_size = os.path.getsize(gpm_filename)
        return gpm_package_size
    else:
        return None
    

def query_file_content_from_package(gpm_file_package_filename, content_filename, error_handler):
    """
    Lookup a file in the gpm and return the content as a string.
    If an error occure None is returned
    """
    if not os.path.exists(gpm_file_package_filename):
        error_handler.emmit_error("The gpm '%s' is not found." % (gpm_file_package_filename))
        return None
    
    gpm_file_package_archive = tarfile.open(gpm_file_package_filename, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')
    gpm_ro_archive = tarfile.open(name='fake', fileobj=gpm_file_package_archive.extractfile(GpmBuilder.GPM_FILES_RO_ARCHIVE_FILENAME), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
    try:
        content_file = gpm_ro_archive.extractfile(content_filename)
        content = content_file.read()
        content_file.close()
    except KeyError:
        error_handler.emmit_error("The content file '%s' is not found." % (content_filename))
        content = None

    gpm_ro_archive.close()
    gpm_file_package_archive.close()
    return content

def query_meta_for_ro_files(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta):
    """
    Look through the meta for all the packages mentioned in  install, update and remove to
    see if  one of them contains ro-files.
    """
    gpm_ids = set(gpm_ids_install)
    gpm_ids.update(gpm_id_from for (gpm_id_from, gpm_id_to) in gpm_ids_update)
    gpm_ids.update(gpm_id_to for (gpm_id_from, gpm_id_to) in gpm_ids_update)
    gpm_ids.update(gpm_ids_remove)
    for gpm_id in gpm_ids:
        if gpms_meta.has_key(gpm_id) and gpms_meta[gpm_id].has_key('contains_ro_files') and gpms_meta[gpm_id]['contains_ro_files']:
            return True
    return False

def query_meta_for_gpm_ids(gpms_meta, error_handler):
    """
    Look through the meta and return package_id for all packages
    """
    result = []
    for meta in gpms_meta:
        try:
            result.append(meta.header.get_package_id())
        except:
            error_handler.emmit_warning("Unable to locate package id")
    return result

def query_meta_for_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta):
    """
    Look through the meta for all the packages mentioned in install, update to
    see if bootification is enabled.
    """
    gpm_ids = set(gpm_ids_install)
    gpm_ids.update(gpm_id_to for (_, gpm_id_to) in gpm_ids_update)
    for gpm_id in gpm_ids:
        if gpms_meta.has_key(gpm_id) and gpms_meta[gpm_id].has_key('bootification_enabled') and gpms_meta[gpm_id]['bootification_enabled']:
            return True
    return False
