"""
This module contains functionaliy to verify filelist from gom against the filesystem
"""
import os.path
import string

import lib.utility


class GpmVerify(object):
    FILE_MISSING = 0
    FILE_CHANGED = 1

    def __init__(self, gpm_env, gpm_error_handler, gpm_files):
        self.gpm_env = gpm_env
        self.gpm_error_handler = gpm_error_handler
        self.gpm_files = gpm_files
        
    def verify(self):
        """
        Return a list of missing or changed files
        """
        result = []
        for gpm_file in self.gpm_files.files:
            gpm_file_rel = string.Template(gpm_file.dest).safe_substitute(self.gpm_env.get_subst_dict())
            if gpm_file_rel.startswith('/'):
                gpm_file_rel = gpm_file_rel[1:]
            
            gpm_filename_abs = os.path.join(self.gpm_env.dest_root, gpm_file_rel)
            
            if not os.path.isfile(gpm_filename_abs):
                result.append( (GpmVerify.FILE_MISSING, gpm_filename_abs, gpm_file.dest) )   
            else:
                gpm_filename_abs_checksum =  lib.utility.calculate_checksum_for_file(gpm_filename_abs)
                if gpm_filename_abs_checksum != gpm_file.checksum:
                    result.append( (GpmVerify.FILE_CHANGED, gpm_filename_abs, gpm_file.dest) )
        return result
    
    

