"""
Hooks ran after package installation
"""
import sys
import os.path
import re
import glob
import ctypes
import gpm_spec
import gpm_builder

if sys.platform == "win32":
    import win32com.client
    import pythoncom

import lib.launch_process




class PostHookError(Exception): pass

def get_uuid_win(drive):
    if not(len(drive) == 2 and drive.endswith(':')):
        raise PostHookError('Invalid drive specification %r' % (drive,))

    pythoncom.CoInitialize()
    vsns = (win32com.client.Dispatch("WbemScripting.SWbemLocator")
            .ConnectServer(".", "root\cimv2")
            .ExecQuery("select VolumeSerialNumber from Win32_LogicalDisk where Name = '%s'" % drive))
    if len(vsns) != 1:
        raise PostHookError('Error getting VolumeSerialNumber from drive %r' % (drive, ))
    return vsns[0].VolumeSerialNumber

def handle_bootification(root_path, error_handler=None):
    """Make a device bootable if package contained the right files.
    Windows only.
    /syslinux/isolinux.cfg and /syslinux/syslinux.exe must exist (and will be deleted)
    /syslinux/syslinux.cfg is created with the right UUID, and syslinux.exe will be run.
    Raises PostHookError on errors.
    """
    # root_path will be something like g:\ or /media/0400-0064
    if not os.path.exists(root_path):
        raise PostHookError('Path %r not found' % (root_path,))
    drive_src = os.path.join(root_path, gpm_spec.FILENAME_SYSLINUX_SRC[1:])
    drive_dst = os.path.join(root_path, 'syslinux/syslinux.cfg')
    drive_syslinux_exe = os.path.join(root_path, gpm_spec.FILENAME_SYSLINUX_EXE[1:])
    if os.path.exists(drive_src) and os.path.exists(drive_syslinux_exe):
        if sys.platform == 'win32':
            (drive, _) = os.path.splitdrive(root_path)
            uuid = get_uuid_win(drive)
            uuid = uuid[:4] + '-' + uuid[4:]
            #print 'read uuid / VolumeSerialNumber', uuid
    
            #print 'generating', drive_dst
            iso_cfg = open(drive_src).read()
            sys_cfg = re.sub(' root=[^ ]* ', ' root=UUID=%s ' % (uuid, ), iso_cfg)
            open(drive_dst, 'w').write(sys_cfg)
        
            # http://syslinux.zytor.com/wiki/index.php/SYSLINUX#Options
            args = [drive_syslinux_exe, '-a', '-m', '-d', 'syslinux', drive]
            (command_rc, command_result) = lib.launch_process.launch_command(args)
            if command_rc != 0:
                raise PostHookError("Error running syslinux: %s returned %s" % (command_rc, command_result))
            
            #print 'removing', drive_syslinux_exe
            os.remove(drive_syslinux_exe)
            #print 'removing', drive_src
            os.remove(drive_src)

            # also expand EFI grub.cfg - no further boot loader installation necessary
            for grub_cfg in glob.glob(os.path.join(root_path, 'EFI/BOOT/*/grub.cfg')):
                sys_cfg = re.sub(' root=[^ ]* ', ' root=UUID=%s ' % (uuid, ), open(grub_cfg).read())
                with open(grub_cfg, 'w') as f:
                    f.write(sys_cfg)
        else:
            if error_handler is not None:
                error_handler.emmit_warning('Bootable devices can only be created on Windows') 
            else:
                raise PostHookError('Bootable devices can only be created on Windows')
                

def is_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary):
    return gpm_builder.query_meta_for_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta)    


def is_bootification_possible(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary):
    if sys.platform != 'win32':
        return (False, dictionary._("Bootification in not support on you current platform"))
    
    if not is_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary):
        return (False, dictionary._("Package is not ready for bootification"))
    
    is_admin = ctypes.windll.shell32.IsUserAnAdmin()
    if not is_admin:
        return (False, dictionary._('You need Administrator privileges in order to bootify your key'))

    return (True, "Ok")


    
if __name__ == '__main__':
    handle_bootification('g:/')
