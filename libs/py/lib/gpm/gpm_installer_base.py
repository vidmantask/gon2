
class GpmInstallerCB(object):
    """
    Interface class for callbacks used by GpmInstaller
    """
    def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
        pass

    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        pass

    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        pass

    def gpm_installer_cb_action_tick(self, ticks):
        pass

    def gpm_installer_cb_action_done(self):
        pass

    def gpm_installer_cb_task_done(self):
        pass

    def gpm_installer_cb_task_all_done(self):
        pass

    def gpm_installer_do_cancel(self):
        return False
        

class GpmInstallerActionBase(object):
    """
    Represent a action where the actual work is done
    """
    def __init__(self, title, gpm_installer_cb, error_handler, dictionary):
        self.gpm_installer_cb = gpm_installer_cb
        self.error_handler = error_handler
        self.title = title
        self.dictionary = dictionary
   
    def get_title(self):        

        return self.title

    def execute(self):
        self.gpm_installer_cb.gpm_installer_cb_action_start(self.get_title(), self.get_ticks())
        self.do_action()
        self.gpm_installer_cb.gpm_installer_cb_action_done()

    def get_ticks(self):
        raise NotImplementedError

    def do_action(self):
        raise NotImplementedError



class GpmInstallerTaskBase(object):
    """
    Represent a group of actions
    """
    def __init__(self, title, gpm_installer_cb, error_handler, dictionary):
        self.gpm_installer_cb = gpm_installer_cb
        self.error_handler = error_handler
        self.title = title
        self.dictionary = dictionary
        self.actions = []
   
    def get_title(self):
        return self.title

    def add_action(self, action):
        self.actions.append(action)

    def get_ticks(self):
        ticks = 0
        for action in self.actions:
            ticks += action.get_ticks()
        return ticks
        
    def execute(self):
        self.gpm_installer_cb.gpm_installer_cb_task_start(self.get_title(), self.get_ticks())
        for action in self.actions:
            action.execute()
        self.gpm_installer_cb.gpm_installer_cb_task_done()



class GpmInstallerBase(object):
    """
    Represent a handler of a group of tasks
    """
    def __init__(self, gpm_installer_cb, error_handler, dictionary):
        self.gpm_installer_cb = gpm_installer_cb
        self.error_handler = error_handler
        self.dictionary = dictionary
        self.tasks = []
        
    def add_task(self, task):
        self.tasks.append(task)
    
    def reset_tasks(self):
        self.tasks = []

    def get_tasks_count_total(self):
        return len(self.tasks)
    
    def get_ticks_count_total(self):
        ticks = 0
        for task in self.tasks:
            ticks += task.get_ticks()
        return ticks
    
    def execute(self):
        self.gpm_installer_cb.gpm_installer_cb_task_all_start(self.get_tasks_count_total(), self.get_ticks_count_total())
        for task in self.tasks:
            task.execute()
            if self.error_handler.error():
                return
            if self.gpm_installer_cb.gpm_installer_do_cancel():
                return
        self.gpm_installer_cb.gpm_installer_cb_task_all_done()
