"""
Unittest of gpm lib
"""
from __future__ import with_statement
from StringIO import StringIO
import elementtree.ElementTree as element_tree

import shutil
import tempfile
import os
import StringIO
import stat

import unittest
from lib import giri_unittest


import lib.gpm.gpm_spec as gpm_spec
import lib.gpm.gpm_env as gpm_env
import lib.gpm.gpm_builder as gpm_builder
import lib.gpm.gpm_installer as gpm_installer
import lib.gpm.gpm_installer_base as gpm_installer_base
import lib.gpm.gpm_analyzer as gpm_analyzer
import lib.gpm.gpm_verify


import plugin_modules.soft_token.client_gateway


def create_folder(temp_folder, foldername_relative):
    foldername = os.path.join(temp_folder, foldername_relative)
    os.makedirs(foldername)
    return foldername

def create_file(temp_folder, filename_relative):
    filename =  os.path.join(temp_folder, filename_relative)
    new_file = open(filename, 'w')
    new_file.write('Hello in file %s' % filename_relative)
    new_file.close()
    return filename

def helper_add_meta(def_str):
    def_file = StringIO.StringIO(def_str)
    gpm_def = gpm_spec.GpmDef.parse_from_file(def_file)
    return (gpm_def.header, gpm_def.dependency, 10, 20, None, False, False, None) 


class MYGpmInstallerCB(object):
    def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
        print "MYGpmInstallerCB::gpm_installer_cb_task_all_start", tasks_count, tasks_ticks_count

    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        print "MYGpmInstallerCB::gpm_installer_cb_task_start", task_title, task_ticks_count

    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        print "MYGpmInstallerCB::gpm_installer_cb_action_start", action_title, action_ticks_conut

    def gpm_installer_cb_action_tick(self, ticks):
        print "MYGpmInstallerCB::gpm_installer_cb_action_tick", ticks

    def gpm_installer_cb_action_done(self):
        print "MYGpmInstallerCB::gpm_installer_cb_action_done"

    def gpm_installer_cb_task_done(self):
        print "MYGpmInstallerCB::gpm_installer_cb_task_done"

    def gpm_installer_cb_task_all_done(self):
        print "MYGpmInstallerCB::gpm_installer_cb_task_all_done"

    def gpm_installer_do_cancel(self):
        print "MYGpmInstallerCB::gpm_installer_cb_do_cancel"
        return False
        

class Gpm_spec(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_gpmdef_parse_and_dump(self):
        gpm_def_w_001_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="gon_client">
    <version main="5" minor="1" bugfix="0" build_id="1" />
    <version_release main="1" minor="9"/>
    <arch>maci386</arch>
    <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
  </header>
  
  <dependency>
    <requires>
      <packageref name="package_name_xxx"/>
    </requires>
    <obsoletes>
      <packageref name="package_name_yyy"/>
    </obsoletes>
    <conflicts>
      <packageref name="package_name_zzz"/>
    </conflicts>
  </dependency>

  <filedefs_ro>
      <include source="%{gpm_build_root}/gon_client/mac" dest="gon_client/mac" />
      <include source="%{gpm_build_root}/G On Mac"/>
      <ignore source="%{gpm_build_root}/gon_client/mac/ignored_file" />
      <ignore source="%{gpm_build_root}/gon_client/mac/ignored_folder" />
  </filedefs_ro>

  <filedefs_rw>
      <include source="%{gpm_build_root}/gon_client/mac" dest="gon_client/mac" />
      <include source="%{gpm_build_root}/gon_client/mac_x" dest="gon_client/mac_x" install_options="ignore_errors" />
      <include source="%{gpm_build_root}/gon_client/mac_y" dest="gon_client/mac_y" uninstall_options="ignore_errors" />
      <include source="%{gpm_build_root}/gon_client/mac_z" dest="gon_client/mac_z" install_options="ignore_errors" uninstall_options="ignore_errors" />
  </filedefs_rw>
</gpmdef>"""
    
        gpm_def_w_001_file = StringIO.StringIO(gpm_def_w_001_str)
        gpm_def_w_001 = gpm_spec.GpmDef.parse_from_file(gpm_def_w_001_file)

        gpm_def_w_002_file = StringIO.StringIO()
        gpm_def_w_001.to_tree().write(gpm_def_w_002_file)

        gpm_def_w_003_file = StringIO.StringIO(gpm_def_w_002_file.getvalue())
        gpm_def_w_003 = gpm_spec.GpmDef.parse_from_file(gpm_def_w_003_file)

        gpm_def_w_004_file = StringIO.StringIO()
        gpm_def_w_003.to_tree().write(gpm_def_w_004_file)
        
        self.assertEqual(gpm_def_w_002_file.getvalue(), gpm_def_w_004_file.getvalue())
        self.assertEqual(gpm_def_w_003.header.name, 'gon_client')
        self.assertEqual(gpm_def_w_003.header.version.get_as_string(), '5.1.0.1')
        self.assertEqual(gpm_def_w_003.header.version_release.get_as_string(), '1.9')
        self.assertEqual(gpm_def_w_003.header.arch.arch, 'maci386')

        self.assertEqual(len(gpm_def_w_003.filedefs_ro.includes), 2)
        self.assertEqual(len(gpm_def_w_003.filedefs_ro.ignores), 2)
        self.assertEqual(len(gpm_def_w_003.filedefs_rw.includes), 4)
        self.assertEqual(len(gpm_def_w_003.filedefs_rw.ignores), 0)


    def test_gpmdef_build_filelist(self):

        temp_folder = giri_unittest.mkdtemp()
        my_folder = create_folder(temp_folder, 'my_folder')
        create_file(temp_folder, os.path.join(my_folder, 'my_file_001.x'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_002.y'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_003.z'))
        my_sub_folder_ = create_folder(my_folder, 'my_sub_folder')
        create_file(my_sub_folder_, os.path.join(my_sub_folder_, 'my_sub_file_001.x'))


        gpm_def_w_001_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="gon_client">
    <version main="5" minor="1" bugfix="0" build_id="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
  </header>
  <filedefs_rw>
      <include source="${gpm_build_root}/" dest="/my/folder" />
      <ignore source="${gpm_build_root}/my_folder/my_file_002.y" />
  </filedefs_rw>
</gpmdef>"""

        gpm_def_w_001_file = StringIO.StringIO(gpm_def_w_001_str)
        gpm_def_w_001 = gpm_spec.GpmDef.parse_from_file(gpm_def_w_001_file)
        
        gpm_error_handler = gpm_env.GpmErrorHandler()
        
        gpm_env_001 = gpm_env.GpmBuildEnv()
        gpm_env_001.build_root = temp_folder
        
        filelist_rw = gpm_def_w_001.generate_filelist_rw(gpm_env_001, gpm_error_handler)
        self.assertEqual(len(filelist_rw), 6)
        
        gpm_meta = gpm_spec.Gpm()
        gpm_meta.header = gpm_def_w_001.header
        gpm_meta.files_rw = gpm_spec.GpmFiles.build_from_filelist(filelist_rw)

        self.assertEqual(len(gpm_meta.files_rw.files), 3)
        self.assertEqual(len(gpm_meta.files_rw.folders), 3)
        self.assertFalse(gpm_meta.contains_ro_files())
        self.assertFalse(gpm_meta.bootification_enabled())



    def test_bootification(self):

        temp_folder = giri_unittest.mkdtemp()
        my_folder = create_folder(temp_folder, 'syslinux')
        create_file(temp_folder, os.path.join(my_folder, 'isolinux.cfg'))
        create_file(temp_folder, os.path.join(my_folder, 'syslinux.exe'))


        gpm_def_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="gon_os">
    <version main="5" minor="1" bugfix="0" build_id="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
  </header>
  <filedefs_rw>
      <include source="${gpm_build_root}/" />
  </filedefs_rw>
</gpmdef>"""

        gpm_def_file = StringIO.StringIO(gpm_def_str)
        gpm_def = gpm_spec.GpmDef.parse_from_file(gpm_def_file)
        
        gpm_error_handler = gpm_env.GpmErrorHandler()
        
        gpm_env_w = gpm_env.GpmBuildEnv()
        gpm_env_w.build_root = temp_folder
        
        filelist_rw = gpm_def.generate_filelist_rw(gpm_env_w, gpm_error_handler)
        
        gpm_meta = gpm_spec.Gpm()
        gpm_meta.header = gpm_def.header
        gpm_meta.files_rw = gpm_spec.GpmFiles.build_from_filelist(filelist_rw)

        self.assertTrue(gpm_meta.bootification_enabled())





class Gpm_builder(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass
        

    def test_build_gpm(self):

        temp_folder = giri_unittest.mkdtemp()
        my_folder = create_folder(temp_folder, 'my_folder')
        create_file(temp_folder, os.path.join(my_folder, 'my_file_001.x'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_002.y'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_003.z'))
        my_sub_folder_ = create_folder(my_folder, 'my_sub_folder')
        create_file(my_sub_folder_, os.path.join(my_sub_folder_, 'my_sub_file_001.x'))
    

        gpm_def_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="gon_client">
    <version main="5" minor="1" bugfix="0" build_id="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
  </header>
  <filedefs_ro>
      <include source="${gpm_build_root}/my_folder" dest="/my_folder_ro" />
      <ignore source="${gpm_build_root}/my_folder/my_file_002.y" />
  </filedefs_ro>
  <filedefs_rw>
      <include source="${gpm_build_root}/my_folder/my_file_002.y" dest="/my_folder_rw/my_file_002.y" />
  </filedefs_rw>
</gpmdef>"""
        gpm_def_file = StringIO.StringIO(gpm_def_str)
        gpm_def_w = gpm_spec.GpmDef.parse_from_file(gpm_def_file)

        gpm_tmp_folder = create_folder(temp_folder, 'gpm_tmp_folder')
        gpm_dst_folder = create_folder(temp_folder, 'gpm_dst_folder')

        gpm_error_handler = gpm_env.GpmErrorHandler()

        gpm_env_w = gpm_env.GpmBuildEnv()
        gpm_env_w.build_root = temp_folder
        gpm_env_w.temp_root = gpm_tmp_folder
        gpm_env_w.dest_root  = gpm_dst_folder

        gpm_builder_w = gpm_builder.GpmBuilder(gpm_env_w)
        gpm_file_package_filename = gpm_builder_w.build_from_spec(gpm_def_w, gpm_error_handler)

        install_root = create_folder(temp_folder, 'install_root')

        runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), install_root)
        runtime_env.force_to_current()
        runtime_env.set_download_root(gpm_dst_folder)
        
        gpm_installer_w = gpm_installer.GpmInstaller(runtime_env, runtime_env.get_ro_root(), runtime_env.get_rw_root(), gpm_error_handler, giri_unittest.get_dictionary(), MYGpmInstallerCB())
        gpm_installer_w.init_tasks(['gon_client-5.1.0.1-1-maci386'], [], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        gpm_error_handler = gpm_env.GpmErrorHandler()
        gpm_meta = runtime_env.get_installed_gpm_meta_all(gpm_error_handler)
        gpm_ids = gpm_builder.query_meta_for_gpm_ids(gpm_meta, gpm_error_handler)
        self.assertTrue(len(gpm_ids) == 1)
        self.assertTrue(gpm_ids[0] == 'gon_client-5.1.0.1-1-maci386')

        

class Gpm_analyzer(unittest.TestCase):
        
    def setUp(self):
        self.temp_root = giri_unittest.mkdtemp()
    
    def tearDown(self):
        pass

    def test_gpmdef_dependency(self):
        gpm_def_a_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="a">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
  <dependency>
    <requires>
      <packageref name="b"/>
    </requires>
  </dependency>
</gpmdef>"""

        gpm_def_a_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="a">
    <version main="2" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
  <dependency>
    <requires>
      <packageref name="b"/>
      <packageref name="c"/>
    </requires>
  </dependency>
</gpmdef>"""

        gpm_def_awin_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="a">
    <version main="2" />
    <version_release main="1"/>
    <arch>win32</arch>
    <description lang="en" summary=""></description>
  </header>
  <dependency>
    <requires>
      <packageref name="b"/>
      <packageref name="c"/>
    </requires>
  </dependency>
</gpmdef>"""

        gpm_def_b_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="b">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
</gpmdef>"""

        gpm_def_b_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="b">
    <version main="2" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
</gpmdef>"""

        gpm_def_c_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="c">
    <version main="2" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
  <dependency>
    <obsoletes>
      <packageref name="d"/>
    </obsoletes>
  </dependency>
</gpmdef>"""

        gpm_def_d_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="d">
    <version main="2" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
  </header>
</gpmdef>"""

        gpm_error_handler = gpm_env.GpmErrorHandler()
        gpm_w_analyzer = gpm_analyzer.GpmRepository()
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_a_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_a_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_awin_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_b_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_b_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_c_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_d_2_str))
        
        gpm_w_analyzer._build_dependencies(gpm_error_handler)
        gpm_w_analyzer.generate_dot_files(self.temp_root)

        
        current_installs_ids = set(['a-1-1-maci386', 'b-1-1-maci386', 'd-2-1-maci386'])
        install_ids = set()
        update_ids = set()
        remove_ids = set()
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, install_ids, update_ids, remove_ids, solve_for_update=True)
        self.assertTrue('c-2-1-maci386' in install_ids)
        self.assertTrue(('a-1-1-maci386', 'a-2-1-maci386') in update_ids)
        self.assertTrue(('b-1-1-maci386', 'b-2-1-maci386') in update_ids)
        self.assertTrue('d-2-1-maci386' in remove_ids)


        current_installs_ids = set(['b-2-1-maci386'])
        install_ids = set(['a-2-1-maci386'])
        update_ids = set()
        remove_ids = set()
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, install_ids, update_ids, remove_ids)
        self.assertTrue('c-2-1-maci386' in install_ids)
        self.assertTrue(len(update_ids) == 0)
        self.assertTrue(len(remove_ids) == 0)


        current_installs_ids = set(['a-2-1-maci386', 'b-2-1-maci386', 'c-2-1-maci386'])
        install_ids = set()
        update_ids = set()
        remove_ids = set(['c-2-1-maci386'])
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, install_ids, update_ids, remove_ids)
        self.assertTrue(len(install_ids) == 0)
        self.assertTrue(len(update_ids) == 0)
        self.assertTrue('a-2-1-maci386' in remove_ids)
#        self.assertTrue('b-2-1-maci386' in remove_ids) #TODO: This is optional feature that could be added

        current_installs_ids = set(['a-2-1-maci386'])
        available_ids = gpm_w_analyzer.get_ids_available_for_installation(current_installs_ids)
        self.assertFalse('a-2-1-maci386' in available_ids)
        self.assertFalse('a-1-1-maci386' in available_ids)
        self.assertTrue('a-2-1-win32' in available_ids)


        current_installs_ids = set(['a-1-1-maci386'])
        (installed, package_id) = gpm_w_analyzer.query_package('a', 'maci386', current_installs_ids)
        self.assertTrue(package_id == 'a-2-1-maci386')
        self.assertTrue(installed)
        
        (installed, package_id) = gpm_w_analyzer.query_package('c', 'maci386', current_installs_ids)
        self.assertTrue(package_id == 'c-2-1-maci386')
        self.assertFalse(installed)

        (installed, package_id) = gpm_w_analyzer.query_package('xxxx', 'maci386', current_installs_ids)
        self.assertTrue(package_id == None)
        self.assertTrue(installed == None)


    def test_solve_for_update_connect(self):
        gpm_def_a_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="a">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='bugfix' timing='connect'/>
  </header>
</gpmdef>"""
        gpm_def_a_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="a">
    <version main="1" />
    <version_release main="2"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='bugfix' timing='connect'/>
  </header>
</gpmdef>"""
        gpm_def_b_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="b">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='security' timing='connect'/>
  </header>
</gpmdef>"""
        gpm_def_b_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="b">
    <version main="1" />
    <version_release main="2"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='security' timing='connect'/>
  </header>
</gpmdef>"""
        gpm_def_c_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="c">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='bugfix' timing='launch'/>
  </header>
</gpmdef>"""
        gpm_def_c_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="c">
    <version main="2" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='bugfix' timing='launch'/>
  </header>
</gpmdef>"""
        gpm_def_d_1_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="d">
    <version main="1" />
    <version_release main="1"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='securiy' timing='launch'/>
  </header>
</gpmdef>"""
        gpm_def_d_2_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmdef>
  <header name="d">
    <version main="1" />
    <version_release main="2"/>
    <arch>maci386</arch>
    <description lang="en" summary=""></description>
    <update type='securiy' timing='launch'/>
  </header>
</gpmdef>"""

        gpm_w_analyzer = gpm_analyzer.GpmRepository()
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_a_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_a_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_b_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_b_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_c_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_c_2_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_d_1_str))
        gpm_w_analyzer.add_meta_data(helper_add_meta(gpm_def_d_2_str))
        gpm_error_handler = gpm_env.GpmErrorHandler()
        gpm_w_analyzer._build_dependencies(gpm_error_handler)

        current_installs_ids = set(['a-1-1-maci386', 'd-1-1-maci386'])
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, set(), set(), set(), solve_for_update_connect=True)
        self.assertTrue(len(install_ids) == 0)
        self.assertTrue(len(update_ids) == 1)
        self.assertTrue(len(remove_ids) == 1)
        self.assertFalse(gpm_w_analyzer.query_is_security_update(update_ids))

        current_installs_ids = set(['a-1-1-maci386', 'c-1-1-maci386'])
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, set(), set(), set(), solve_for_update=True)
        self.assertTrue(len(install_ids) == 0)
        self.assertTrue(len(update_ids) == 2)
        self.assertTrue(len(remove_ids) == 2)
        self.assertFalse(gpm_w_analyzer.query_is_security_update(update_ids))

        current_installs_ids = set(['a-1-1-maci386', 'b-1-1-maci386'])
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, set(), set(), set(), solve_for_update_connect=True)
        self.assertTrue(len(install_ids) == 0)
        self.assertTrue(len(update_ids) == 2)
        self.assertTrue(len(remove_ids) == 2)
        self.assertTrue(gpm_w_analyzer.query_is_security_update(update_ids))

        current_installs_ids = set(['a-1-2-maci386', 'b-1-2-maci386'])
        (install_ids, update_ids, remove_ids)= gpm_w_analyzer.solve(current_installs_ids, set(), set(), set(), solve_for_update_connect=True)
        self.assertTrue(len(install_ids) == 0)
        self.assertTrue(len(update_ids) == 0)
        self.assertTrue(len(remove_ids) == 0)
        self.assertFalse(gpm_w_analyzer.query_is_security_update(update_ids))



class Gpmcdef_spec(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_gpmcdef_parse_and_dump(self):
        gpm_cdef_w_001_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpmcdef>
  <header name="win_collection">
    <description lang="en" summary="Collection of windows packages">This is the world client side of the one and only remote access solution.</description>
  </header>
  
  <packages>
      <packageref name="package_name_xxx" arch="xxxx" />
      <packageref name="package_name_yyy"/>
      <packageref name="package_name_zzz"/>
  </packages>

</gpmcdef>"""
    
        gpm_cdef_w_001_file = StringIO.StringIO(gpm_cdef_w_001_str)
        gpm_cdef_w_001 = gpm_spec.GpmcDef.parse_from_file(gpm_cdef_w_001_file)

        self.assertEqual(gpm_cdef_w_001.header.name, 'win_collection')
        self.assertEqual(len(gpm_cdef_w_001.packages), 3)


class Gpm_install_and_remove(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()
    
    def tearDown(self):
        pass
    
    def test_install_and_remove(self):
        install_root = giri_unittest.mkdtemp()
        gpm_error_handler = gpm_env.GpmErrorHandler()
        runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), install_root)
        runtime_env.force_to_current()
        runtime_env.set_download_root(self.dev_installation.get_gpms_folder())

        gpm_installer_w = gpm_installer.GpmInstaller(runtime_env, runtime_env.get_ro_root(), runtime_env.get_rw_root(), gpm_error_handler, giri_unittest.get_dictionary(), MYGpmInstallerCB())
        gpm_installer_w.init_tasks(['a-1-1-win', 'a_extra-1-1-win', 'b-1-1-win'], [], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        gpm_installer_w.init_tasks([], [('b-1-1-win','b-2-1-win')], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        gpm_installer_w.init_tasks([], [], ['a-1-1-win', 'a_extra-1-1-win', 'b-2-1-win'])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertFalse(gpm_error_handler.error())

        total_filenames = []
        for(dirpath, dirnames, filenames) in os.walk(install_root):
            total_filenames.extend(filenames)
        self.assertEqual(len(total_filenames), 0)

    def test_install_and_remove_not_empty(self):
        install_root = giri_unittest.mkdtemp()
        gpm_error_handler = gpm_env.GpmErrorHandler()
        runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), install_root)
        runtime_env.force_to_current()
        runtime_env.set_download_root(self.dev_installation.get_gpms_folder())

        gpm_installer_w = gpm_installer.GpmInstaller(runtime_env, runtime_env.get_ro_root(), runtime_env.get_rw_root(), gpm_error_handler, giri_unittest.get_dictionary(), MYGpmInstallerCB())
        gpm_installer_w.init_tasks(['d-1-1-win'], [], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        # This folder should be removed bcause it is empty
        my_temp_folder = create_folder(runtime_env.get_rw_root(), 'folder_2/sub_1')

        # This folder and file will not be removed
        my_temp_folder = create_folder(runtime_env.get_rw_root(), 'folder_1/folder_c/sub_2')
        create_file(my_temp_folder, os.path.join(my_temp_folder, 'my_file.x'))

        gpm_installer_w.init_tasks([], [], ['d-1-1-win'])
        gpm_installer_w.execute()

        total_filenames = []
        for(dirpath, dirnames, filenames) in os.walk(install_root):
            total_filenames.extend(filenames)
        self.assertEqual(len(total_filenames), 1)


    def test_install_file_options(self):
        install_root = giri_unittest.mkdtemp()
        gpm_error_handler = gpm_env.GpmErrorHandler()
        runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), install_root)
        runtime_env.force_to_current()
        runtime_env.set_download_root(self.dev_installation.get_gpms_folder())

        in_package_filename_abs = os.path.join(install_root, 'e', 'file_with_options')
        os.makedirs(os.path.dirname(in_package_filename_abs))
        in_package_file = open(in_package_filename_abs, 'w')
        in_package_file.write("Hej")
        in_package_file.close()
        os.chmod(in_package_filename_abs, stat.S_IREAD)

        gpm_installer_w = gpm_installer.GpmInstaller(runtime_env, runtime_env.get_ro_root(), runtime_env.get_rw_root(), gpm_error_handler, giri_unittest.get_dictionary(), MYGpmInstallerCB())
        gpm_installer_w.init_tasks(['e-1-1-win'], [], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertFalse(gpm_error_handler.error())

        os.chmod(in_package_filename_abs, stat.S_IREAD)
        gpm_installer_w.init_tasks([], [], ['e-1-1-win'])
        gpm_installer_w.execute()

        
    def test_install_folder_options(self):
        install_root = giri_unittest.mkdtemp()
        gpm_error_handler = gpm_env.GpmErrorHandler()
        runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), install_root)
        runtime_env.force_to_current()
        runtime_env.set_download_root(self.dev_installation.get_gpms_folder())

        gpm_installer_w = gpm_installer.GpmInstaller(runtime_env, runtime_env.get_ro_root(), runtime_env.get_rw_root(), gpm_error_handler, giri_unittest.get_dictionary(), MYGpmInstallerCB())
        gpm_installer_w.init_tasks(['f-1-1-win'], [], [])
        gpm_installer_w.execute()
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        my_temp_folder = os.path.join(install_root, 'f', 'folder_with_options', 'extra_folder_with_options')
        os.makedirs(my_temp_folder)
        create_file(my_temp_folder, os.path.join(my_temp_folder, 'extra_file.with_options'))

        my_temp_folder = os.path.join(install_root, 'f', 'folder_with_no_options', 'extra_folder_no_options')
        os.makedirs(my_temp_folder)
        create_file(my_temp_folder, os.path.join(my_temp_folder, 'extra_file.no_options'))

        gpm_installer_w.init_tasks([], [], ['f-1-1-win'])
        gpm_installer_w.execute()

        for(dirpath, dirnames, filenames) in os.walk(install_root):
            print dirpath, dirnames, filenames

        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()


class Gpm_reposotory(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()
    
    def tearDown(self):
        pass

    def test_repository(self):
        error_handler = gpm_env.GpmErrorHandler()
        
        repository  = gpm_analyzer.GpmRepository()
        repository.init_from_gpms(error_handler, self.dev_installation.get_gpms_folder(), self.dev_installation.get_gpmcdefs_folder())

        collection_info = repository.get_gpm_collection_info('collection_a')
        self.assertTrue(collection_info is not None)

        


class Gpm_install_tasks(unittest.TestCase):

    def test_copy_folder(self):
        gpm_installer_cb = gpm_installer_base.GpmInstallerCB()        
        gpm_error_handler = gpm_env.GpmErrorHandler()

        temp_root = giri_unittest.mkdtemp()

        source_root = create_folder(temp_root, 'source')
        dest_root = create_folder(temp_root, 'dest')
        create_file(source_root, 'my_file_001.x')
        create_file(source_root, 'my_file_002.x')
        create_file(source_root, 'my_file_003.x')
        source_sub_root = create_folder(source_root, 'sub')
        create_file(source_sub_root, 'my_sub_file_001.x')

        task = gpm_installer.GpmInstallerTaskCopyFolder(source_root, dest_root, None, 'test copy', gpm_installer_cb, giri_unittest.get_dictionary(), gpm_error_handler)
        task.execute()

        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        total_filenames = []
        for(dirpath, dirnames, filenames) in os.walk(dest_root):
            total_filenames.extend(filenames)
        print total_filenames
        self.assertEqual(len(total_filenames), 4)


class Gpm_filedef(unittest.TestCase):

    def test_generate_filelist(self):
        gpm_filedefs_str = """<?xml version="1.0" encoding="UTF-8"?>
<filedefs>
  <include source="${gpm_build_root}/my_folder" dest="/my_folder"/>
</filedefs>
"""
        temp_folder = giri_unittest.mkdtemp()
        my_folder = create_folder(temp_folder, 'my_folder')
        create_file(temp_folder, os.path.join(my_folder, 'my_file_001.x'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_002.y'))
        create_file(temp_folder, os.path.join(my_folder, 'my_file_003.z'))
        
        gpm_filedefs_file = StringIO.StringIO(gpm_filedefs_str)
        gpm_filedefs = gpm_spec.GpmFileDefs.parse_from_file(gpm_filedefs_file)
        
        gpm_error_handler = gpm_env.GpmErrorHandler()
        gpm_build_env = gpm_env.GpmBuildEnv()
        gpm_build_env.temp_root = None
        gpm_build_env.build_root = temp_folder
        gpm_build_env.dest_root = temp_folder
        filelist = gpm_filedefs.generate_filelist(gpm_build_env, gpm_error_handler)
        if not gpm_error_handler.ok():
            print gpm_error_handler.dump_as_string()
        self.assertTrue(gpm_error_handler.ok())

        gpm_files_file = StringIO.StringIO()
        gpm_files = gpm_spec.GpmFiles.build_from_filelist(filelist)
        gpm_files.to_tree().write(gpm_files_file)

        gpm_files_after_file = StringIO.StringIO(gpm_files_file.getvalue())
        gpm_files_after = gpm_spec.GpmFiles.parse_from_file(gpm_files_after_file)
        self.assertTrue(len(gpm_files_after.files) == 3)

        verifyer = lib.gpm.gpm_verify.GpmVerify(gpm_build_env, gpm_error_handler, gpm_files)
        self.assertTrue(len(verifyer.verify()) == 0)

        os.unlink(os.path.join(my_folder, 'my_file_001.x'))
        self.assertTrue(len(verifyer.verify()) == 1)
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
