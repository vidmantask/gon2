import tempfile
import tarfile
import os
import os.path
import sys
import shutil
import time
import stat

if sys.platform == 'win32':
    import win32com.shell.shell
    import win32com.shell.shellcon


import lib.utility
import lib.gpm.post_hook
import lib.commongon
import lib.dictionary

import lib.gpm.gpm_builder
import lib.gpm.gpm_installer_base



lib.dictionary.deftext_id('GPM_ERROR::0001', "The package file '%s' was not found for the package %s.", ('GPM Filename', 'GPM Package Id'))
lib.dictionary.deftext_id('GPM_ERROR::0002', "No meta info available for the package file '%s'.")
lib.dictionary.deftext_id('GPM_ERROR::0003', "Filename for the file '%s' is to long for Windows to handle.")
lib.dictionary.deftext_id('GPM_ERROR::0004', "Unexpected error found during unpacking the package file '%s'.", ('GPM Filename'),  )
lib.dictionary.deftext_id('GPM_ERROR::0005', "Error accessing the package file '%s'.", ('GPM Filename'))
lib.dictionary.deftext_id('GPM_ERROR::0006', "No meta info available for the package %s.", ('GPM Package Id') )
lib.dictionary.deftext_id('GPM_ERROR::0007', "Error during copying of folder.")
lib.dictionary.deftext_id('GPM_ERROR::0008', "Timeout")
lib.dictionary.deftext_id('GPM_ERROR::0009', "Unexpected error found during copy of files, error code is %d.", ('RC'),  )
lib.dictionary.deftext_id('GPM_ERROR::0010', "Unable to access the file '%s'", ('filename'),  )
lib.dictionary.deftext_id('GPM_ERROR::0011', "Unable to install the file '%s'", ('filename'),  )

lib.dictionary.deftext_id('GPM_WARNING::0001', "The file '%s' has been changed and is not removed.", ('Filename'))
lib.dictionary.deftext_id('GPM_WARNING::0002', "Unable to remove the file '%s'.", ('Filename'))
lib.dictionary.deftext_id('GPM_WARNING::0003', "The file '%s' is not found.", ('Filename'))
lib.dictionary.deftext_id('GPM_WARNING::0004', "The folder '%s' is not removed because it is not empty.", ('Foldername'))
lib.dictionary.deftext_id('GPM_WARNING::0005', "The folder '%s' is not found.", ('Foldername'))
lib.dictionary.deftext_id('GPM_WARNING::0006', "Unable to remove the folder '%s'.", ('Foldername'))
lib.dictionary.deftext_id('GPM_WARNING::0007', "Unable to remove the file '%s'.", ('Filename'))
lib.dictionary.deftext_id('GPM_WARNING::0008', "Unable to set rwx for the file '%s'.", ('Filename'))
lib.dictionary.deftext_id('GPM_WARNING::0009', "Option caused error to be ignored for the file '%s'.", ('Filename'))



class GpmInstallerActionInstall(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
    STATE_OK = 0
    STATE_ERROR = 1
    
    def __init__(self, gpm_id, runtime_env, ro_root, rw_root, gpm_installer_cb, error_handler, dictionary):
        lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, dictionary._("Installing the package %s") % gpm_id, gpm_installer_cb, error_handler, dictionary)
        self.state = GpmInstallerActionInstall.STATE_OK
        self.gpm_id = gpm_id
        self.runtime_env = runtime_env
        self.ro_root = ro_root
        self.rw_root = rw_root
        self.gpm_meta = None
        self.gpm_filename = self.runtime_env.get_download_gpm_filename(self.gpm_id)
        if not os.path.exists(self.gpm_filename):
            self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0001', (self.gpm_filename, self.gpm_id)))
            self.state = GpmInstallerActionInstall.STATE_ERROR
            return
        self.gpm_meta = lib.gpm.gpm_builder.query_meta(self.gpm_filename, self.error_handler)
        if self.gpm_meta == None:
            self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0002', (self.gpm_filename)))
            self.state = GpmInstallerActionInstall.STATE_ERROR
                
    def get_ticks(self):
        if self.state == GpmInstallerActionInstall.STATE_OK:
            ticks = 0
            if self.gpm_meta.files_ro != None:
                ticks += len(self.gpm_meta.files_ro.files)
            if self.gpm_meta.files_rw != None:
                ticks += len(self.gpm_meta.files_rw.files)
            return ticks
        return 0
                
    def _install_from_archive(self, gpm_file_package_archive, archive_file_name, install_root):
        files_archive = None
        try:
            files_archive = tarfile.open(name='fake', fileobj=gpm_file_package_archive.extractfile(archive_file_name), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
            if self.gpm_meta.has_install_options():
                if sys.platform == 'win32':
                    self._install_from_archive_by_file_win(files_archive, install_root)
                else:
                    self._install_from_archive_by_file_linux_mac(files_archive, install_root)
            else:
                if sys.platform == 'win32':
                    self._install_from_archive_win(files_archive, install_root)
                else:
                    self._install_from_archive_linux_mac(files_archive, install_root)
        except KeyError:
            pass
        except:
            self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0004', (archive_file_name), add_exc_info=True)) 
        finally:
            if files_archive is not None:
                files_archive.close()

    def _install_from_archive_linux_mac(self, files_archive, install_root):
        self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
        files_archive.extractall(path=install_root)
        # Because packages are generated on windows the execute-bit is not included in the tarfil
        # every file get its execute flag set explicit. Should only be done on selected files
        # from meta-info.
        #
        # Files starting with ._ are special attribute files on mac, and problems has been seen
        # if trying to access ._my_file
        #
        for file_in_archive in files_archive.getmembers():
            file_in_archive_filename_abs = os.path.join(install_root, file_in_archive.name)
            if not os.path.islink(file_in_archive_filename_abs):
                if not os.path.basename(file_in_archive_filename_abs).startswith('._'):
                    try:
                        os.chmod(file_in_archive_filename_abs, stat.S_IEXEC | stat.S_IREAD | stat.S_IWRITE)
                    except:
                        self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0008', (file_in_archive_filename_abs), add_exc_info=True))
        self.gpm_installer_cb.gpm_installer_cb_action_tick(len(files_archive.getmembers()) - 1)


    def _install_from_archive_by_file_linux_mac(self, files_archive, install_root):
        for file_in_archive in files_archive.getmembers():
            install_options_ignore_error = False
            #
            # Files in archive do not have '/' in the name
            #
            gpm_meta_file = self.gpm_meta.lookup_file_by_dist('/' + file_in_archive.name)
            if gpm_meta_file is not None:
                install_options_ignore_error = gpm_meta_file.has_install_option_ignore_error()

            file_in_archive_filename_abs = os.path.join(install_root, file_in_archive.name)
            try:
                files_archive.extract(file_in_archive, install_root)
                if not os.path.islink(file_in_archive_filename_abs):
                    if not os.path.basename(file_in_archive_filename_abs).startswith('._'):
                        os.chmod(file_in_archive_filename_abs, stat.S_IEXEC | stat.S_IREAD | stat.S_IWRITE)
            except:
                if install_options_ignore_error:
                    self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_ERROR::0011', (file_in_archive_filename_abs), add_exc_info=True))
                    self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0009', (file_in_archive_filename_abs)))
                else:
                    self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0011', (file_in_archive_filename_abs), add_exc_info=True))
            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
        files_archive.close()


    def _install_from_archive_win(self, files_archive, install_root):
        """
        On windows the tar file is unpacked in a temp location, and then uses
        win32 api to copy file. The win32 file copy operation is 5 time faster (to flash device)
        than un-taring directly.
        """ 
        self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
        temp_install_root = tempfile.mktemp (".dir")
        
        for file_in_archive in files_archive.getmembers():
            file_in_archive_filename_abs = os.path.normpath(os.path.join(install_root, file_in_archive.name))
            if len(file_in_archive_filename_abs) > 255:
                self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0003', (file_in_archive.name)))
                return

        files_archive.extractall(path=temp_install_root)
        flags = win32com.shell.shellcon.FOF_NOCONFIRMMKDIR | win32com.shell.shellcon.FOF_NOCONFIRMATION | win32com.shell.shellcon.FOF_NOERRORUI | win32com.shell.shellcon.FOF_SILENT 
        (rc, rc_error_flag) = win32com.shell.shell.SHFileOperation ( (0, win32com.shell.shellcon.FO_COPY, os.path.join(temp_install_root,'*'), install_root, flags, None, None) )

        lib.utility.rmtree_fail_safe(temp_install_root)

        # This rc is given if cancel is automatic chosen, the following code tries to find the file
        # that caused the problem.
        if rc == 1223:
            for file_in_archive in files_archive.getmembers():
                file_in_archive_filename_abs = os.path.normpath(os.path.join(install_root, file_in_archive.name))
                if os.path.exists(file_in_archive_filename_abs):
                    try:
                        os.unlink(file_in_archive_filename_abs)
                    except:
                        self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0010', (file_in_archive_filename_abs)))
                        return
        if rc != 0:
            self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0009', (rc)))
            return
        self.gpm_installer_cb.gpm_installer_cb_action_tick(len(files_archive.getmembers()) - 1)


    def _install_from_archive_by_file_win(self, files_archive, install_root):
        for file_in_archive in files_archive.getmembers():
            install_options_ignore_error = False
            
            #
            # Files in archive do not have '/' in the name
            #
            gpm_meta_file = self.gpm_meta.lookup_file_by_dist('/' + file_in_archive.name)
            if gpm_meta_file is not None:
                install_options_ignore_error = gpm_meta_file.has_install_option_ignore_error()

            file_in_archive_filename_abs = os.path.join(install_root, file_in_archive.name)
            try:
                files_archive.extract(file_in_archive, install_root)
                if len(file_in_archive_filename_abs) > 255:
                    if install_options_ignore_error:
                        self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_ERROR::0003', (file_in_archive.name)))
                        self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0009', (file_in_archive_filename_abs)))
                    else:
                        self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0003', (file_in_archive.name)))
                        return
            except:
                if install_options_ignore_error:
                    self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0009', (file_in_archive_filename_abs), add_exc_info=True))
                else:
                    self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0011', (file_in_archive_filename_abs), add_exc_info=True))
            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
        files_archive.close()


    def do_action(self):
        if self.state == GpmInstallerActionInstall.STATE_OK:
            try:
                gpm_file_package_archive = tarfile.open(self.gpm_filename, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')
                self._install_from_archive(gpm_file_package_archive, lib.gpm.gpm_builder.GpmBuilder.GPM_FILES_RO_ARCHIVE_FILENAME, self.ro_root)
                self._install_from_archive(gpm_file_package_archive, lib.gpm.gpm_builder.GpmBuilder.GPM_FILES_RW_ARCHIVE_FILENAME, self.rw_root)

                if not self.error_handler.error():
                    lib.gpm.post_hook.handle_bootification(self.rw_root, self.error_handler)
                
                if not self.error_handler.error():
                    self.runtime_env.install_gpm_meta(self.gpm_filename, self.error_handler)
            except:
                self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0005', (self.gpm_filename), add_exc_info=True)) 




class GpmInstallerActionRemove(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
    STATE_OK = 0
    STATE_ERROR = 1
    
    def __init__(self, gpm_id, runtime_env, ro_root, rw_root, gpm_installer_cb, error_handler, dictionary):
        lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, dictionary._("Removing the package %s") % gpm_id, gpm_installer_cb, error_handler, dictionary)
        self.state = GpmInstallerActionRemove.STATE_OK
        self.gpm_id = gpm_id
        self.ro_root = ro_root
        self.rw_root = rw_root
        self.runtime_env = runtime_env
        self.gpm_meta = self.runtime_env.get_installed_gpm_meta(self.gpm_id, self.error_handler)
        if self.gpm_meta == None:
            self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0006', (self.gpm_id)))
            self.state = GpmInstallerActionRemove.STATE_ERROR

    def get_ticks(self):
        if self.state == GpmInstallerActionRemove.STATE_OK:
            ticks = 0
            if self.gpm_meta.files_ro != None:
                ticks += len(self.gpm_meta.files_ro.files) + (len(self.gpm_meta.files_ro.folders) * 10)
            if self.gpm_meta.files_rw != None:
                ticks += len(self.gpm_meta.files_rw.files) + (len(self.gpm_meta.files_rw.folders) * 10)
            return ticks
        return 0

    def _remove_files(self, file_files, root):
        if file_files == None:
            return
        for file_w in file_files:
            #
            # Files in meta start with '/'
            #
            file_w_abs = os.path.normpath(os.path.join(root, file_w.dest[1:]))
            if os.path.isfile(file_w_abs):
                try:
                    file_w_checksum = lib.utility.calculate_checksum_for_file(file_w_abs)
                    if file_w_checksum == file_w.checksum:
                        os.remove(file_w_abs)
                        self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
                    else:
                        self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0001', (file_w_abs))) 
                except OSError, e:
                    self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0002', (file_w_abs)))
            elif os.path.islink(file_w_abs):
                # Checksum is not compared for symlinks. On mac calculating the checksum for a symlink resulted in error
                # because the link was followed.
                try:
                    os.remove(file_w_abs)
                    self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
                except OSError, e:
                    self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0002', (file_w_abs)))
            else:
                self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0003', (file_w_abs))) 

    def _remove_subfolders_(self, folder_w_abs):
        for root, dirs, _ in os.walk(folder_w_abs, topdown=False):
            for name in dirs:
                try:
                    os.rmdir(os.path.join(root, name))
                except OSError:
                    pass
        
    def _remove_folders(self, file_folders, root):
        if file_folders == None:
            return
        
        file_folders_to_remove = [] 
        for folder_w in file_folders:
            #
            # Folders in meta starts with '/'
            #
            foldername_abs = os.path.normpath(os.path.join(root, folder_w.dest[1:]))
            file_folders_to_remove.append( (foldername_abs, folder_w) )

        #
        # Sorting folders for dept-first remove order 
        #
        file_folders_to_remove.sort(key=lambda (foldername, folder): len(foldername), reverse=True)
        for (folder_w_abs, folder_w) in file_folders_to_remove:
            if os.path.isdir(folder_w_abs):
                    if folder_w.has_uninstall_option_force_recurcive_delete():
                        try:
                            lib.utility.rmtree(folder_w_abs)
                        except:
                            self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0004', (folder_w_abs)))
                    else:
                        try:
                            self._remove_subfolders_(folder_w_abs)
                            os.rmdir(folder_w_abs)
                        except OSError:
                            self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0004', (folder_w_abs)))
                    self.gpm_installer_cb.gpm_installer_cb_action_tick(10)
            else:
                self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0005', (folder_w_abs))) 

    def do_action(self):
        if self.state == GpmInstallerActionRemove.STATE_OK:
            self.runtime_env.remove_installed_gpm_meta(self.gpm_id, self.error_handler)
            if self.gpm_meta.files_ro != None:
                self._remove_files(self.gpm_meta.files_ro.files, self.ro_root)
                self._remove_folders(self.gpm_meta.files_ro.folders, self.ro_root)
            if self.gpm_meta.files_rw != None:
                self._remove_files(self.gpm_meta.files_rw.files, self.rw_root)
                self._remove_folders(self.gpm_meta.files_rw.folders, self.rw_root)
                

class GpmInstallerTaskCleanup(lib.gpm.gpm_installer_base.GpmInstallerTaskBase):
    class GpmInstallerActionCleanupFolder(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
        def __init__(self, folder, cb, error_handler, dictionary, force):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, dictionary._('Removing folder'), cb, error_handler, dictionary)
            self.folder = folder
            self.force = force

        def get_ticks(self):
            return 10
        
        def do_action(self):
            folder_removed = False
            try_again = 5
            while(not folder_removed and try_again > 0):
                try:
                    lib.utility.rmtree(self.folder)
                    folder_removed = True
                except:
                    time.sleep(1)
                    try_again -= 1
            if not folder_removed:
                self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0006', (self.folder), add_exc_info=True)) 
            self.gpm_installer_cb.gpm_installer_cb_action_tick(10)

    class GpmInstallerActionCleanupFile(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
        def __init__(self, filename, cb, error_handler, dictionary, force):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, dictionary._('Removing file'), cb, error_handler, dictionary)
            self.filename = filename
            self.force = force
            
        def get_ticks(self):
            return 1
        
        def do_action(self):
            try:
                if self.force:
                    os.chmod(self.filename, stat.S_IWRITE)
                os.remove(self.filename)
            except:
                self.error_handler.emmit_warning(self.dictionary.gettext_id('GPM_WARNING::0007', (self.filename), add_exc_info=True))
            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)

    def __init__(self, folders, filenames, task_message, cb, error_handler, dictionary, force=False):
        lib.gpm.gpm_installer_base.GpmInstallerTaskBase.__init__(self, task_message, cb, error_handler, dictionary)
        for folder in folders:
            self.add_action(GpmInstallerTaskCleanup.GpmInstallerActionCleanupFolder(folder, cb, error_handler, dictionary, force))
        for filename in filenames:
            self.add_action(GpmInstallerTaskCleanup.GpmInstallerActionCleanupFile(filename, cb, error_handler, dictionary, force))



class GpmInstallerTaskCopyFolder(lib.gpm.gpm_installer_base.GpmInstallerTaskBase):
    class GpmInstallerActionCopyFolder(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
        def __init__(self, source_root, dest_root, dest_chmod_mode, gpm_installer_cb, error_handler, dictionary, ticks=None):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, '', gpm_installer_cb, error_handler, dictionary)
            self.source_root = source_root
            self.dest_root = dest_root
            self.gpm_installer_cb = gpm_installer_cb
            self.dest_chmod_mode = dest_chmod_mode
            self.ticks = ticks

        def get_ticks(self):
            if self.ticks is not None:
                return self.ticks
            
            ticks = 0
            for _ in os.walk(self.source_root):
                ticks += 1
            return ticks
               
        def do_action(self):
            try:
                for root, _, files in os.walk(self.source_root):
                    root_abs_src = root
                    root_abs_dest = os.path.join(self.dest_root, lib.commongon.common_path_sufix(root, self.source_root))

                    if not os.path.exists(root_abs_dest):
                        os.makedirs(root_abs_dest)

                    if self.dest_chmod_mode != None:
                        os.chmod(root_abs_dest, self.dest_chmod_mode)
                        
                    for file in files:
                        file_abs_src = os.path.join(root_abs_src, file)
                        file_abs_dest = os.path.join(root_abs_dest, file)
                        shutil.copy(file_abs_src, file_abs_dest)
                        shutil.copystat(file_abs_src, file_abs_dest)
                        if self.dest_chmod_mode != None:
                            os.chmod(file_abs_dest, self.dest_chmod_mode)
                    self.gpm_installer_cb.gpm_installer_cb_action_tick(1)
                        
            except:
                self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0007', add_exc_info=True)) 

    def __init__(self, source_root, dest_root, dest_chmod_mode, title, gpm_installer_cb, error_handler, dictionary, ticks=None):
        lib.gpm.gpm_installer_base.GpmInstallerTaskBase.__init__(self, title, gpm_installer_cb, error_handler, dictionary)
        self.add_action(GpmInstallerTaskCopyFolder.GpmInstallerActionCopyFolder(source_root, dest_root, dest_chmod_mode, gpm_installer_cb, error_handler, dictionary, ticks))


class GpmInstallerTaskWaitForPredicate(lib.gpm.gpm_installer_base.GpmInstallerTaskBase):
    class GpmInstallerActionWaitForPredicate(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
        def __init__(self, predicate_cb, timeout_sec, gpm_installer_cb, error_handler, dictionary):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, '', gpm_installer_cb, error_handler, dictionary)
            self.predicate_cb = predicate_cb
            self.timeout_sec = timeout_sec
            self.gpm_installer_cb = gpm_installer_cb

        def get_ticks(self):
            return 1
               
        def do_action(self):
            timeout = self.timeout_sec
            while timeout > 0 and not self.predicate_cb():
                time.sleep(1)
                timeout -= 1
            if timeout == 0:
                self.error_handler.emmit_error(self.dictionary.gettext_id('GPM_ERROR::0008'))

            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)

    def __init__(self, predicate_cb, timeout_sec, title, gpm_installer_cb, error_handler, dictionary):
        lib.gpm.gpm_installer_base.GpmInstallerTaskBase.__init__(self, title, gpm_installer_cb, error_handler, dictionary)
        self.add_action(GpmInstallerTaskWaitForPredicate.GpmInstallerActionWaitForPredicate(predicate_cb, timeout_sec, gpm_installer_cb, error_handler, dictionary))


class GpmInstaller(lib.gpm.gpm_installer_base.GpmInstallerBase):
    def __init__(self, runtime_env, ro_root, rw_root, error_handler, dictionary, gpm_installer_cb = None):
        lib.gpm.gpm_installer_base.GpmInstallerBase.__init__(self, gpm_installer_cb, error_handler, dictionary)
        self.runtime_env = runtime_env
        self.ro_root = ro_root
        self.rw_root = rw_root
        
        self.gpm_installer_cb = gpm_installer_cb
        if self.gpm_installer_cb == None:
            self.gpm_installer_cb = lib.gpm.gpm_installer_base.GpmInstallerCB()

    def init_tasks(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, alien_pre_tasks=None, alien_post_tasks=None):
        if alien_pre_tasks is None:
            alien_pre_tasks = []
            
        if alien_post_tasks is None:
            alien_post_tasks = []
        
        self.reset_tasks()
        
        for alien_task in alien_pre_tasks:
            self.add_task(alien_task)
 
        gpm_ids_remove_set = set(gpm_ids_remove).union(set([x[0] for x in gpm_ids_update]))
        if len(gpm_ids_remove_set) > 0:
            task_remove = lib.gpm.gpm_installer_base.GpmInstallerTaskBase(self.dictionary._('Remove packages'), self.gpm_installer_cb, self.error_handler, self.dictionary)
            for gpm_id in gpm_ids_remove_set:
                task_remove.add_action(GpmInstallerActionRemove(gpm_id, self.runtime_env, self.ro_root, self.rw_root, self.gpm_installer_cb, self.error_handler, self.dictionary))
            self.add_task(task_remove)

        gpm_ids_install_set = set(gpm_ids_install).union(set([x[1] for x in gpm_ids_update]))
        if len(gpm_ids_install_set) > 0:
            task_install = lib.gpm.gpm_installer_base.GpmInstallerTaskBase(self.dictionary._('Install packages'), self.gpm_installer_cb, self.error_handler, self.dictionary)
            for gpm_id in gpm_ids_install_set:
                task_install.add_action(GpmInstallerActionInstall(gpm_id, self.runtime_env, self.ro_root, self.rw_root, self.gpm_installer_cb, self.error_handler, self.dictionary))
            self.add_task(task_install)

        for alien_task in alien_post_tasks:
            self.add_task(alien_task)
