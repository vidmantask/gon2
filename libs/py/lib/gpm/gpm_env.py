import sys
import os
import os.path

class GpmError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value
    def __str__(self):
        return repr(self.value)



class GpmErrorHandler(object):
    def __init__(self):
        self.throw_on_error = False
        self.errors = []
        self.warnings = []
        
    def emmit_error(self, message):
        if self.throw_on_error:
            raise GpmError(message)
        self.errors.append(message)

    def emmit_warning(self, message):
        self.warnings.append(message)
        
    def error(self):
        return len(self.errors) > 0

    def warning(self):
        return len(self.warnings) > 0

    def ok(self):
        return not (self.error() or self.warning())

    def dump(self):
        print self.dump_as_string()

    def dump_as_string(self, n_max=None):
        result = ""
        n = 0
        for error in self.errors:
            n += 1
            result += "Error:  %s\n" % error
            if n_max is not None and n >= n_max:
                return result

        for warning in self.warnings:
            result += "Warning: %s\n" % warning
            n += 1
            if n_max is not None and n >= n_max:
                return result

        return result
        


class GpmProgressHandler(object):
    def __init__(self):
        self._tick_max = 100
        self._tick = 0
        self._tick_data = None
    
    def begin(self, tick_max = 100):
        self._tick_max = tick_max
        self._tick_data = None
        self.tick(self._tick, {'type':'begin'})
        
    def end(self):
        self.tick(self._tick_max, {'type':'end'})
        
    def tick(self, tick, tick_data = None):
        if tick_data is None:
            tick_data = {}
        self._tick = tick
        self._tick_data = tick_data
        print "Tick %d af %d " % (self._tick, self._tick_max), self._tick_data
    
            


class GpmBuildEnv(object):
    """
    Class representing the environment for building gpm-file-packages from definition
    """
    def __init__(self):
        self.temp_root = None
        self.build_root = None
        self.dest_root = None
        self._subst_dict = {}

    def validate(self, gpm_error_handler):
        if not os.path.isdir(self.temp_root):
            gpm_error_handler.emmit("The folder '%s' is not valid as a temporary folder." % (self.temp_root))

        if not os.path.isdir(self.build_root):
            gpm_error_handler.emmit("The folder '%s' is not valid as a build root." % (self.build_root))
            
        if not os.path.isdir(self.dest_root):
            gpm_error_handler.emmit("The folder '%s' is not valid as a destination root." % (self.dest_root))

    def get_subst_dict(self):
        self._subst_dict['gpm_build_root'] = self.build_root
        return self._subst_dict
        
    def add_path_to_subst_dict(self, key, path):
        self._subst_dict[key] = path
        
        

