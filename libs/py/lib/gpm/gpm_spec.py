import os.path
import sys

import lib.utility
import lib.version

import elementtree.ElementTree as element_tree
import string


FILENAME_SYSLINUX_SRC = '/syslinux/isolinux.cfg'
FILENAME_SYSLINUX_EXE = '/syslinux/syslinux.exe'


#
# Helper functions for parsing and building
#
def set_node_attrib(node, name, value):
    if value != None:
        node.attrib[name] = value

def set_node_attrib_int(node, name, value):
    if value != None:
        node.attrib[name] = "%i" % value

def add_as_node(gpm_instance, parent_node, *args):
    if gpm_instance != None:
        gpm_instance.add_as_node(parent_node, *args)

def get_node_attrib_int(node, name):
    if node.attrib.has_key(name):
        return int(node.attrib[name])
    return None
    
def get_node_attrib_string(node, name):
    if node.attrib.has_key(name):
        return node.attrib[name]
    return None



def calculate_checksum(gpm_filename, error_handler=None):
    try:
        if os.path.exists(gpm_filename):
            return lib.utility.calculate_checksum_for_file(gpm_filename)
        else:
            return None
    except IOError:
        if error_handler is not None:
            _, evalue, _ = sys.exc_info()
            error_handler.emmit_error("Unable to generate checksum for '%s', because '%s'." % (gpm_filename, evalue))
    return None


class GpmVersion(object):
    """
    Reperesent a version tag from the gpmsepc header

    e.g. 
       <version main="5" minor="1" bugfix="0" build_id="1" />
    """

    TAG_NAME_version = 'version'
    TAG_NAME_version_release = 'version_release'
    ATTRIB_NAME_main = 'main'
    ATTRIB_NAME_minor = 'minor'
    ATTRIB_NAME_bugfix = 'bugfix'
    ATTRIB_NAME_build_id = 'build_id'
    
    def __init__(self):
        self.main = None
        self.minor = None
        self.bugfix = None
        self.build_id = None
        
    @classmethod
    def parse_from_parent_node(cls, parent_node, tag_name=None):
        if tag_name == None:
            tag_name = GpmVersion.TAG_NAME_version
        version_node = parent_node.find(tag_name)
        if version_node != None:
            gpm_version = GpmVersion()
            gpm_version.main = get_node_attrib_int(version_node, GpmVersion.ATTRIB_NAME_main)
            gpm_version.minor = get_node_attrib_int(version_node, GpmVersion.ATTRIB_NAME_minor)
            gpm_version.bugfix = get_node_attrib_int(version_node, GpmVersion.ATTRIB_NAME_bugfix)
            gpm_version.build_id = get_node_attrib_int(version_node, GpmVersion.ATTRIB_NAME_build_id)
            return gpm_version 
        return None
    
    def add_as_node(self, parent_node, tag_name=None):
        if tag_name is None:
            tag_name = GpmVersion.TAG_NAME_version
        node = element_tree.SubElement(parent_node, tag_name)
        set_node_attrib_int(node, GpmVersion.ATTRIB_NAME_main, self.main)
        set_node_attrib_int(node, GpmVersion.ATTRIB_NAME_minor, self.minor)
        set_node_attrib_int(node, GpmVersion.ATTRIB_NAME_bugfix, self.bugfix)
        set_node_attrib_int(node, GpmVersion.ATTRIB_NAME_build_id, self.build_id)

    def validate(self, gpm_build_env, error_handler):
        pass

    def _append_with_none(self, parts, value, supress_if_none):
        if supress_if_none and value is None:
            return
        
        if value is None:
            value = 0
        parts.append('%i' % value)
        

    def get_as_string(self):
        version_parts_rev = []
        self._append_with_none(version_parts_rev, self.build_id, True)
        self._append_with_none(version_parts_rev, self.bugfix, len(version_parts_rev) == 0)
        self._append_with_none(version_parts_rev, self.minor, len(version_parts_rev) == 0)
        self._append_with_none(version_parts_rev, self.main, False)
        return '.'.join(reversed(version_parts_rev))
    
    @classmethod
    def compare(cls, a, b):
        a_version = lib.version.Version.create(a.main, a.minor, a.bugfix, a.build_id)
        b_version = lib.version.Version.create(b.main, b.minor, b.bugfix, b.build_id)
        return lib.version.Version.compare(a_version, b_version)


class GpmArch(object):
    """
    Represent a arch(archetecture) tag

    e.g. 
      <arch>maci386</arch>
    """
    TAG_NAME = 'arch'
    ARCHS = ['noarch', 'win32', 'win64', 'maci386', 'linux32', 'linux64', 'linux_from', 'linux_maemo']

    def __init__(self):
        self.arch = None
        
    @classmethod
    def parse_from_parent_node(cls, parent_node):
        arch_node = parent_node.find(GpmArch.TAG_NAME)
        if arch_node != None:
            gpm_arch = GpmArch()
            gpm_arch.arch = arch_node.text
            return gpm_arch
        return None

    def add_as_node(self, parent_node):
        node = element_tree.SubElement(parent_node, GpmArch.TAG_NAME)
        node.text = self.arch

    def validate(self, error_handler):
        if not self.arch in GpmArch.ARCHS:
            error_handler.emmit("arch-tag contains invalid value '%s', valid values are [%s]" % (self.arch, ', '.join(GpmArch.ARCHS)))

    def get_as_string(self):
        return self.arch


class GpmDescriptions(object):
    """
    Represent a list of description tags

    e.g. 
      <description lang="en" summary="G/On Client">
        This is the world client side of the one and only remote access solution.
      </description>
      ...
      <description lang="dk" summary="G/On Client">
        This is the world client side of the one and only remote access solution.
      </description>
    """
    TAG_NAME = 'description'
    ATTRIB_NAME_lang = 'lang'
    ATTRIB_NAME_summary = 'summary'
    ATTRIB_NAME_description = 'description'

    def __init__(self):
        self.descriptions = {}
        
    @classmethod
    def parse_from_parent_node(cls, parent_node):
        gpm_arch = GpmDescriptions()
        for node in parent_node.findall(GpmDescriptions.TAG_NAME):
            lang = get_node_attrib_string(node, GpmDescriptions.ATTRIB_NAME_lang)
            summary = get_node_attrib_string(node, GpmDescriptions.ATTRIB_NAME_summary)
            description = node.text 
            gpm_arch.descriptions[lang] = (summary, description)
        return gpm_arch

    def add_as_node(self, parent_node):
        for lang in self.descriptions:
            (summary, description) = self.descriptions[lang]
            node = element_tree.SubElement(parent_node, GpmDescriptions.TAG_NAME)
            set_node_attrib(node, GpmDescriptions.ATTRIB_NAME_lang, lang)
            set_node_attrib(node, GpmDescriptions.ATTRIB_NAME_summary, summary)
            node.text = description

    def validate(self, gpm_build_env, error_handler):
        pass
    
    def get_as_string(self):
        (_, description) = self.get_default()
        return description          

    def get_default(self):
        default_description = "No description"
        default_summary = "No summary"
        
        if len(self.descriptions) == 0:
            return (default_summary, default_description)
        
        summary = self.descriptions.values()[0][0]
        if summary == None:
            summary = default_summary

        description = self.descriptions.values()[0][1]
        if description == None:
            summary = default_description
        return (summary, description)


class GpmUpdate(object):
    """
    Reperesent a update-tag from the gpmsepc header

    e.g. 
       <update type='new|enhancement|bugfix|security' timing='connect|launch' />
    """
    TAG_NAME_update = 'update'
    ATTRIB_NAME_type = 'type'
    ATTRIB_NAME_timing = 'timing'

    TYPE_new = 'new'
    TYPE_enhancement = 'enhancement'
    TYPE_bugfix = 'bugfix'
    TYPE_security = 'security'

    TIMINGE_connect = 'connect'
    TIMINGE_launch  = 'launch'
    
    def __init__(self):
        self.type = None
        self.timing = None
        
    @classmethod
    def parse_from_parent_node(cls, parent_node):
        tag_name = GpmUpdate.TAG_NAME_update
        update_node = parent_node.find(tag_name)
        if update_node != None:
            gpm_update = GpmUpdate()
            gpm_update.type = get_node_attrib_string(update_node, GpmUpdate.ATTRIB_NAME_type)
            gpm_update.timing = get_node_attrib_string(update_node, GpmUpdate.ATTRIB_NAME_timing)
            return gpm_update

        gpm_update = GpmUpdate()
        gpm_update.type =  GpmUpdate.TYPE_bugfix
        gpm_update.timing = GpmUpdate.TIMINGE_launch
        return gpm_update
    
    def add_as_node(self, parent_node, tag_name=None):
        if tag_name == None:
            tag_name = GpmUpdate.TAG_NAME_update
        node = element_tree.SubElement(parent_node, tag_name)
        set_node_attrib(node, GpmUpdate.ATTRIB_NAME_type, self.type)
        set_node_attrib(node, GpmUpdate.ATTRIB_NAME_timing, self.timing)

    def validate(self, gpm_build_env, error_handler):
        pass



class GpmHeader(object):
    """
    Represent a header in the gpms
    
    e.g. 
      <header name="gon_client">
        <version main="5" minor="1" bugfix="0" build_id="1" />
        <version_release main="1"/>
        <arch>maci386</arc>
        <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
       </header>
    """
    TAG_NAME = 'header'
    ATTRIB_NAME_name = 'name'
    ATTRIB_NAME_visibility_type = 'visibility_type'

    VISIBILITY_TYPE_public = 'public'
    VISIBILITY_TYPE_hidden = 'hidden'

    def __init__(self):
        self.name = None
        self.visibility_type = None
        self.version = None
        self.version_release = None
        self.arch = None
        self.descriptions = None
        self.update = None

    @classmethod
    def parse_from_parent_node(cls, parent_node):
        header_node = parent_node.find(GpmHeader.TAG_NAME)
        if header_node != None:
            gpm_header = GpmHeader()
            gpm_header.name = get_node_attrib_string(header_node, GpmHeader.ATTRIB_NAME_name)
            gpm_header.visibility_type = get_node_attrib_string(header_node, GpmHeader.ATTRIB_NAME_visibility_type)
            gpm_header.version = GpmVersion.parse_from_parent_node(header_node, GpmVersion.TAG_NAME_version)
            gpm_header.version_release = GpmVersion.parse_from_parent_node(header_node, GpmVersion.TAG_NAME_version_release)
            gpm_header.arch = GpmArch.parse_from_parent_node(header_node)
            gpm_header.descriptions = GpmDescriptions.parse_from_parent_node(header_node)
            gpm_header.update = GpmUpdate.parse_from_parent_node(header_node)
            return gpm_header
        return None
    
    def add_as_node(self, parent_node):
        header_node = element_tree.SubElement(parent_node, GpmHeader.TAG_NAME)
        set_node_attrib(header_node, GpmHeader.ATTRIB_NAME_name, self.name)
        set_node_attrib(header_node, GpmHeader.ATTRIB_NAME_visibility_type, self.visibility_type)
        add_as_node(self.version, header_node, GpmVersion.TAG_NAME_version)
        add_as_node(self.version_release, header_node, GpmVersion.TAG_NAME_version_release)
        add_as_node(self.arch, header_node)
        add_as_node(self.descriptions, header_node)
        add_as_node(self.update, header_node)

    def validate(self, gpm_build_env, error_handler):
        if self.name is None:
            error_handler.emmit("header-tag don't contain a value for the name attribute")

    def get_package_id(self):
        """
        name-version-version_release-arch
        """
        id_parts = []
        id_parts.append(self.name)
        if self.version != None:
            id_parts.append(self.version.get_as_string())
        if self.version_release != None:
            id_parts.append(self.version_release.get_as_string())
        id_parts.append(self.arch.get_as_string())
        return '-'.join(id_parts)
       
    def get_visibility_type(self):
        if self.visibility_type is None:
            return GpmHeader.VISIBILITY_TYPE_public
        return self.visibility_type


class GpmcDefHeader(object):
    """
    Represent a header in a gpmcdef
    
    e.g. 
      <header name="gon_client">
        <description lang="en" summary="G/On Client">This is the world client side of the one and only remote access solution.</description>
       </header>
    """
    TAG_NAME = 'header'
    ATTRIB_NAME_name = 'name'
    ATTRIB_NAME_visibility_type = 'visibility_type'

    VISIBILITY_TYPE_public = 'public'
    VISIBILITY_TYPE_hidden = 'hidden'

    def __init__(self):
        self.name = None
        self.visibility_type = None
        self.descriptions = None

    @classmethod
    def parse_from_parent_node(cls, parent_node):
        header_node = parent_node.find(GpmcDefHeader.TAG_NAME)
        if header_node != None:
            gpmcdef_header = GpmcDefHeader()
            gpmcdef_header.name = get_node_attrib_string(header_node, GpmcDefHeader.ATTRIB_NAME_name)
            gpmcdef_header.visibility_type = get_node_attrib_string(header_node, GpmcDefHeader.ATTRIB_NAME_visibility_type)
            gpmcdef_header.descriptions = GpmDescriptions.parse_from_parent_node(header_node)
            return gpmcdef_header
        return None
    
    def add_as_node(self, parent_node):
        header_node = element_tree.SubElement(parent_node, GpmcDefHeader.TAG_NAME)
        set_node_attrib(header_node, GpmHeader.ATTRIB_NAME_name, self.name)
        set_node_attrib(header_node, GpmHeader.ATTRIB_NAME_visibility_type, self.visibility_type)
        add_as_node(self.descriptions, header_node)

    def validate(self, gpm_build_env, error_handler):
        if self.name == None:
            error_handler.emmit("header-tag don't contain a value for the name attribute")

    def get_collection_id(self):
        """
        name-version-version_release-arch
        """
        id_parts = []
        id_parts.append(self.name)
        return '-'.join(id_parts)
       
    def get_visibility_type(self):
        if self.visibility_type is None:
            return GpmcDefHeader.VISIBILITY_TYPE_public
        return self.visibility_type


class GpmPackageRef(object):
    """
    Represent a package reference tag in gpms
    e.g.
      <packageref name="package_name_xxx" arch=""/>
    """
    
    TAG_NAME = 'packageref'
    ATTRIB_NAME_name = 'name'
    ATTRIB_NAME_arch = 'arch'
   
    def __init__(self):
        self.name = None
        self.arch = None
   
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        if parent_node != None:
            result = []
            for node in parent_node.findall(GpmPackageRef.TAG_NAME):
                gpm_package_ref = GpmPackageRef()
                gpm_package_ref.name = get_node_attrib_string(node, GpmPackageRef.ATTRIB_NAME_name)
                gpm_package_ref.arch = get_node_attrib_string(node, GpmPackageRef.ATTRIB_NAME_arch)
                result.append(gpm_package_ref)
            return result
        return None

    @classmethod
    def add_as_nodes(cls, package_refs, parent_node, tag_name):
        if package_refs != None and len(package_refs) > 0:
            node = element_tree.SubElement(parent_node, tag_name)
            for package_ref in package_refs:
                package_node = element_tree.SubElement(node, GpmPackageRef.TAG_NAME)
                set_node_attrib(package_node, GpmPackageRef.ATTRIB_NAME_name, package_ref.name)
                if package_ref.arch != None:
                    set_node_attrib(package_node, GpmPackageRef.ATTRIB_NAME_arch, package_ref.arch)
   
    def validate(self, gpm_build_env, error_handler):
        pass

 
class GpmDependency(object):
    """
    Represent a dependency tag in gpms
    
    e.g. 
      <dependency>
        <requires>
          <packageref name="package_name_xxx"/>
        </requires>
        <obsoletes>
          <packageref name="package_name_yyy"/>
        </obsoletes>
        <conflicts>
          <packageref name="package_name_zzz"/>
        </conflicts>
      </dependncy>
    """
    TAG_NAME = 'dependency'
    TAG_NAME_requires = 'requires'
    TAG_NAME_obsoletes = 'obsoletes'
    TAG_NAME_conflicts = 'conflicts'

    def __init__(self):
        self.requires = []
        self.obsoletes = []
        self.confilcts = []

    @classmethod
    def parse_from_parent_node(cls, parent_node):
        node = parent_node.find(GpmDependency.TAG_NAME)
        if node != None:
            gpm_dependency = GpmDependency()
            gpm_dependency.requires = GpmPackageRef.parse_from_parent_node_to_list(node.find(GpmDependency.TAG_NAME_requires))
            if gpm_dependency.requires == None:
                gpm_dependency.requires = []
            gpm_dependency.obsoletes = GpmPackageRef.parse_from_parent_node_to_list(node.find(GpmDependency.TAG_NAME_obsoletes))
            if gpm_dependency.obsoletes == None:
                gpm_dependency.obsoletes = []
            gpm_dependency.confilcts = GpmPackageRef.parse_from_parent_node_to_list(node.find(GpmDependency.TAG_NAME_conflicts))
            if gpm_dependency.confilcts == None:
                gpm_dependency.confilcts = []
            return gpm_dependency
        return None
    
    def add_as_node(self, parent_node):
        node = element_tree.SubElement(parent_node, GpmDependency.TAG_NAME)
        GpmPackageRef.add_as_nodes(self.requires, node, GpmDependency.TAG_NAME_requires)
        GpmPackageRef.add_as_nodes(self.obsoletes, node, GpmDependency.TAG_NAME_obsoletes)
        GpmPackageRef.add_as_nodes(self.confilcts, node, GpmDependency.TAG_NAME_conflicts)

    def validate(self, gpm_build_env, error_handler):
        pass



class GpmFileDefsInclude(object):
    TAG_NAME = 'include'
    ATTRIB_NAME_source = 'source'
    ATTRIB_NAME_dest = 'dest'
    ATTRIB_NAME_install_options = 'install_options'
    ATTRIB_NAME_uninstall_options = 'uninstall_options'
    
    ATTRIB_VALUE_INSTALL_OPTION_ignore_error = 'ignore_error'
    ATTRIB_VALUE_UNINSTALL_OPTION_ignore_error = 'ignore_error'

    def __init__(self):
        self.source = None
        self.dest = None
        self.install_options = None
        self.uninstall_options = None
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(GpmFileDefsInclude.TAG_NAME)
        for node in nodes:
            gpm_include = GpmFileDefsInclude()
            gpm_include.source = get_node_attrib_string(node, GpmFileDefsInclude.ATTRIB_NAME_source)
            gpm_include.dest = get_node_attrib_string(node, GpmFileDefsInclude.ATTRIB_NAME_dest)
            gpm_include.install_options = get_node_attrib_string(node, GpmFileDefsInclude.ATTRIB_NAME_install_options)
            gpm_include.uninstall_options = get_node_attrib_string(node, GpmFileDefsInclude.ATTRIB_NAME_uninstall_options)
            result.append(gpm_include)
        return result

    @classmethod
    def add_as_nodes(cls, includes, parent_node):
        if len(includes) > 0:
            for incl  in includes:
                incl_node = element_tree.SubElement(parent_node, GpmFileDefsInclude.TAG_NAME)
                set_node_attrib(incl_node, GpmFileDefsInclude.ATTRIB_NAME_source, incl.source)
                set_node_attrib(incl_node, GpmFileDefsInclude.ATTRIB_NAME_dest, incl.dest)
                set_node_attrib(incl_node, GpmFileDefsInclude.ATTRIB_NAME_install_options, incl.install_options)
                set_node_attrib(incl_node, GpmFileDefsInclude.ATTRIB_NAME_uninstall_options, incl.uninstall_options)

    def validate(self, gpm_build_env, error_handler):
        pass
  


class GpmFileDefsIgnore(object):
    TAG_NAME = 'ignore'
    ATTRIB_NAME_source = 'source'

    def __init__(self):
        self.source = None
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(GpmFileDefsIgnore.TAG_NAME)
        for node in nodes:
            gpm_ignore = GpmFileDefsIgnore()
            gpm_ignore.source = get_node_attrib_string(node, GpmFileDefsIgnore.ATTRIB_NAME_source)
            result.append(gpm_ignore)
        return result

    @classmethod
    def add_as_nodes(cls, ignores, parent_node):
        if len(ignores) > 0:
            for incl  in ignores:
                incl_node = element_tree.SubElement(parent_node, GpmFileDefsIgnore.TAG_NAME)
                set_node_attrib(incl_node, GpmFileDefsIgnore.ATTRIB_NAME_source, incl.source)

    def validate(self, gpm_build_env, error_handler):
        pass


class GpmFileDefs(object):
    """
    e.g.
      <filedefs_ro>
        <include source="%(gpm_build_root)/gon_client/mac" dest="gon_client/mac" />
        <include source="%(gpm_build_root)/G On Mac"/>
        <ignore source="%(gpm_build_root)/gon_client/mac/ignored_file" />
        <ignore source="%(gpm_build_root)/gon_client/mac/ignored_folder" />
      </filedefs_ro>
    """
    TAG_NAME_filedefs_ro = 'filedefs_ro'
    TAG_NAME_filedefs_rw = 'filedefs_rw'
    TAG_NAME_filedefs = 'filedefs'

    def __init__(self):
        self.includes = []
        self.ignores = []
        
    @classmethod
    def parse_from_parent_node(cls, parent_node, tag_name):
        node = parent_node.find(tag_name)
        if node != None:
            gpm_file_defs = GpmFileDefs()
            gpm_file_defs.includes = GpmFileDefsInclude.parse_from_parent_node_to_list(node)
            gpm_file_defs.ignores = GpmFileDefsIgnore.parse_from_parent_node_to_list(node)
            return gpm_file_defs
        return None
    
    @classmethod
    def parse_from_file(cls, currentfile):
        root = element_tree.ElementTree(None, currentfile)
        if root != None:
            fake_root = element_tree.Element('fake_root_node')
            fake_root.append(root.getroot())
            return GpmFileDefs.parse_from_parent_node(fake_root, GpmFileDefs.TAG_NAME_filedefs)
        return None

    def add_as_node(self, parent_node, tag_name):
        node = element_tree.SubElement(parent_node, tag_name)
        GpmFileDefsInclude.add_as_nodes(self.includes, node)
        GpmFileDefsIgnore.add_as_nodes(self.ignores, node)
        return node
        
    def to_tree(self):
        node = element_tree.Element(GpmFileDefs.TAG_NAME_filedefs)
        GpmFileDefsInclude.add_as_nodes(self.includes, node)
        GpmFileDefsIgnore.add_as_nodes(self.ignores, node)
        return element_tree.ElementTree(node)
   
    def validate(self, gpm_build_env, error_handler):
        pass

    def _to_posix_path(self, path):
        """
        On windows the path is converted to a posix path
        """
        if sys.platform == 'win32' :
            path_os = os.path.normpath(path)
            return path_os.replace('\\', '/')
        return path

    def update_filelist(self, filelist, key, dest, type, install_options=None, uninstall_options=None, size=None, checksum=None):
        if not filelist.has_key(key):
            filelist[key] = {}
            filelist[key]['install_options'] = None
            filelist[key]['uninstall_options'] = None
            filelist[key]['size'] = 0
            filelist[key]['checksum'] = None
            
        filelist[key]['dest'] = dest
        filelist[key]['type'] = type
        if size is not None:
            filelist[key]['size'] = size
        if checksum is not None:
            filelist[key]['checksum'] = checksum
        if install_options is not None:
            filelist[key]['install_options'] = install_options
        if uninstall_options is not None:
            filelist[key]['uninstall_options'] = uninstall_options
        

    def generate_filelist(self, gpm_build_env, error_handler):
        filelist = {}
        for incl in self.includes:
            incl_souce_str = os.path.normpath(string.Template(incl.source).safe_substitute(gpm_build_env.get_subst_dict()))
            incl_dest_str  = incl.dest
            incl_install_options = incl.install_options
            incl_uninstall_options = incl.uninstall_options

            if os.path.isdir(incl_souce_str):
                if incl_dest_str == None:
                    incl_dest_str = "/"
                    
                src_folder_abs = os.path.normpath(os.path.join(incl_souce_str))
                dest_folder_posix    = self._to_posix_path(os.path.join(incl_dest_str))
                filelist[src_folder_abs] = {'dest' : dest_folder_posix, 'type':'folder', 'uninstall_options':incl_uninstall_options}
                for(dirpath, dirnames, filenames) in os.walk(incl_souce_str):
                    for filename in filenames:
                        src_filename_abs = os.path.normpath(os.path.join(dirpath, filename))
                        dest_filename_posix  = self._to_posix_path(os.path.join(incl_dest_str, dirpath[len(incl_souce_str)+1:], filename))
                        self.update_filelist(filelist, src_filename_abs, dest_filename_posix, 'file', install_options=incl_install_options, uninstall_options=incl_uninstall_options)
                    for dirname in dirnames:
                        src_folder_abs = os.path.normpath(os.path.join(dirpath, dirname))
                        dest_folder_posix    = self._to_posix_path(os.path.join(incl_dest_str, dirpath[len(incl_souce_str)+1:], dirname))
                        filelist[src_folder_abs] = {'dest' : dest_folder_posix, 'type':'folder', 'uninstall_options':incl_uninstall_options}
            elif os.path.isfile(incl_souce_str):
                dest_filename_posix = self._to_posix_path(incl_dest_str)
                self.update_filelist(filelist, incl_souce_str, dest_filename_posix, 'file', install_options=incl_install_options, uninstall_options=incl_uninstall_options)
            else:
                error_handler.emmit_warning("Source '%s' not found" % incl_souce_str )
                    
        for ignore in self.ignores:
            ignore_souce_path = os.path.normpath(string.Template(ignore.source).safe_substitute(gpm_build_env.get_subst_dict()))
            if filelist.has_key(ignore_souce_path):
                del filelist[ignore_souce_path]
            if os.path.isdir(ignore_souce_path):
                for(dirpath, dirnames, filenames) in os.walk(ignore_souce_path):
                    for filename in filenames:
                        src_filename_abs = os.path.normpath(os.path.join(dirpath, filename))
                        if filelist.has_key(src_filename_abs):
                            del filelist[src_filename_abs]
                    for dirname in dirnames:
                        src_folder_abs = os.path.normpath(os.path.join(dirpath, dirname))
                        if filelist.has_key(src_folder_abs):
                            del filelist[src_folder_abs]
        
        for src_filename_abs in filelist.keys():
            if os.path.isfile(src_filename_abs):
                filelist[src_filename_abs]['size'] =  os.path.getsize(src_filename_abs)
                filelist[src_filename_abs]['checksum'] =  calculate_checksum(src_filename_abs, error_handler)
        
        return filelist


class GpmDef(object):
    TAG_NAME = 'gpmdef'

    def __init__(self):
        self.header = None
        self.dependency = None
        self.filedefs_ro = None
        self.filedefs_rw = None

    @classmethod
    def parse_from_file(cls, file):
        root = element_tree.ElementTree(None, file)
        if root != None:
            gpm_def = GpmDef()
            gpm_def.header = GpmHeader.parse_from_parent_node(root)
            gpm_def.dependency = GpmDependency.parse_from_parent_node(root)
            gpm_def.filedefs_ro = GpmFileDefs.parse_from_parent_node(root, GpmFileDefs.TAG_NAME_filedefs_ro)
            gpm_def.filedefs_rw = GpmFileDefs.parse_from_parent_node(root, GpmFileDefs.TAG_NAME_filedefs_rw)
            return gpm_def
        return None

    def to_node(self):
        node = element_tree.Element(GpmDef.TAG_NAME)
        self.header.add_as_node(node)
        if self.dependency != None:
            self.dependency.add_as_node(node)
        if self.filedefs_ro != None:
            self.filedefs_ro.add_as_node(node, GpmFileDefs.TAG_NAME_filedefs_ro)
        if self.filedefs_rw != None:
            self.filedefs_rw.add_as_node(node, GpmFileDefs.TAG_NAME_filedefs_rw)
        return node

    def to_tree(self):
        return element_tree.ElementTree(self.to_node())

    def validate(self, gpm_build_env, error_handler):
        pass
   
    def generate_filelist_ro(self, gpm_build_env, error_handler):
        if self.filedefs_ro == None:
            return []
        return self.filedefs_ro.generate_filelist(gpm_build_env, error_handler)

    def generate_filelist_rw(self, gpm_build_env, error_handler):
        if self.filedefs_rw == None:
            return []
        return self.filedefs_rw.generate_filelist(gpm_build_env, error_handler)
        

class GpmcDef(object):
    TAG_NAME = 'gpmcdef'
    TAG_NAME_packages = 'packages'

    def __init__(self):
        self.header = None
        self.packages = []

    @classmethod
    def parse_from_file(cls, thisfile):
        root = element_tree.ElementTree(None, thisfile)
        if root != None:
            gpmc_def = GpmcDef()
            gpmc_def.header = GpmcDefHeader.parse_from_parent_node(root)
            gpmc_def.packages = GpmPackageRef.parse_from_parent_node_to_list(root.find(GpmcDef.TAG_NAME_packages))
            if gpmc_def.packages == None:
                gpmc_def.packages = []
            return gpmc_def
        return None

    def to_node(self):
        node = element_tree.Element(GpmcDef.TAG_NAME)
        self.header.add_as_node(node)
        GpmPackageRef.add_as_nodes(self.packages, node, GpmcDef.TAG_NAME_packages)
        return node

    def to_tree(self):
        return element_tree.ElementTree(self.to_node())

    def validate(self, gpm_build_env, error_handler):
        pass
   


class GpmFilesFile(object):
    """
    e.g.
      <file dest="gon_client/mac/x" checksum="xxxx" size="99" />
    """
    TAG_NAME = 'file'
    ATTRIB_NAME_dest = 'dest'
    ATTRIB_NAME_checksum = 'checksum'
    ATTRIB_NAME_size = 'size'
    ATTRIB_NAME_install_options = 'install_options'
    ATTRIB_NAME_uninstall_options = 'uninstall_options'
    
    ATTRIB_VALUE_INSTALL_OPTION_ignore_error = 'ignore_error'
    ATTRIB_VALUE_UNINSTALL_OPTION_ignore_error = 'ignore_error'

    def __init__(self):
        self.dest = None
        self.checksum = None
        self.size = None
        self.install_options = None
        self.uninstall_options = None
        
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(GpmFilesFile.TAG_NAME)
        for node in nodes:
            gpm_file = GpmFilesFile()
            gpm_file.dest = get_node_attrib_string(node, GpmFilesFile.ATTRIB_NAME_dest)
            gpm_file.checksum = get_node_attrib_string(node, GpmFilesFile.ATTRIB_NAME_checksum)
            gpm_file.size = get_node_attrib_int(node, GpmFilesFile.ATTRIB_NAME_size)
            gpm_file.install_options = get_node_attrib_string(node, GpmFilesFile.ATTRIB_NAME_install_options)
            gpm_file.uninstall_options = get_node_attrib_string(node, GpmFilesFile.ATTRIB_NAME_uninstall_options)
            result.append(gpm_file)
        return result

    @classmethod
    def add_as_nodes(cls, files, parent_node):
        for f  in files:
            node = element_tree.SubElement(parent_node, GpmFilesFile.TAG_NAME)
            set_node_attrib(node, GpmFilesFile.ATTRIB_NAME_dest, f.dest)
            set_node_attrib(node, GpmFilesFile.ATTRIB_NAME_checksum, f.checksum)
            set_node_attrib_int(node, GpmFilesFile.ATTRIB_NAME_size, f.size)
            set_node_attrib(node, GpmFilesFile.ATTRIB_NAME_install_options, f.install_options)
            set_node_attrib(node, GpmFilesFile.ATTRIB_NAME_uninstall_options, f.uninstall_options)

    def validate(self, gpm_build_env, error_handler):
        pass

    def has_install_option_ignore_error(self):
        if self.install_options is None:
            return False
        return self.install_options.lower().find(GpmFilesFile.ATTRIB_VALUE_INSTALL_OPTION_ignore_error) != -1
    
    def has_uninstall_option_ignore_error(self):
        if self.uninstall_options is None:
            return False
        return self.uninstall_options.lower().find(GpmFilesFile.ATTRIB_VALUE_UNINSTALL_OPTION_ignore_error) != -1

    def install_options_cleanup(self):
        work_options = []
        if self.has_install_option_ignore_error():
            work_options.append(GpmFilesFile.ATTRIB_VALUE_INSTALL_OPTION_ignore_error)
        self.install_options = '|'.join(work_options)
    
    def uninstall_options_cleanup(self):
        work_options = []
        if self.has_uninstall_option_ignore_error():
            work_options.append(GpmFilesFile.ATTRIB_VALUE_UNINSTALL_OPTION_ignore_error)
        self.uninstall_options = '|'.join(work_options)


class GpmFilesFolder(object):
    """
    e.g.
      <folder dest="gon_client/mac" />
    """
    TAG_NAME = 'folder'
    ATTRIB_NAME_dest = 'dest'
    ATTRIB_NAME_uninstall_options = 'uninstall_options'

    ATTRIB_VALUE_UNINSTALL_OPTION_force_recurcive_delete = 'force_recurcive_delete'

    def __init__(self):
        self.dest = None
        self.uninstall_options = None
        
    @classmethod
    def parse_from_parent_node_to_list(cls, parent_node):
        result = []
        nodes = parent_node.findall(GpmFilesFolder.TAG_NAME)
        for node in nodes:
            gpm_folder = GpmFilesFolder()
            gpm_folder.dest = get_node_attrib_string(node, GpmFilesFolder.ATTRIB_NAME_dest)
            gpm_folder.uninstall_options = get_node_attrib_string(node, GpmFilesFolder.ATTRIB_NAME_uninstall_options)
            result.append(gpm_folder)
        return result

    @classmethod
    def add_as_nodes(cls, folders, parent_node):
        for f  in folders:
            node = element_tree.SubElement(parent_node, GpmFilesFolder.TAG_NAME)
            set_node_attrib(node, GpmFilesFolder.ATTRIB_NAME_dest, f.dest)
            set_node_attrib(node, GpmFilesFolder.ATTRIB_NAME_uninstall_options, f.uninstall_options)

    def validate(self, gpm_build_env, error_handler):
        pass

    def has_uninstall_option_force_recurcive_delete(self):
        if self.uninstall_options is None:
            return False
        return self.uninstall_options.lower().find(GpmFilesFolder.ATTRIB_VALUE_UNINSTALL_OPTION_force_recurcive_delete) != -1

    def uninstall_options_cleanup(self):
        work_options = []
        if self.has_uninstall_option_force_recurcive_delete():
            work_options.append(GpmFilesFolder.ATTRIB_VALUE_UNINSTALL_OPTION_force_recurcive_delete)
        self.uninstall_options = '|'.join(work_options)


class GpmFiles(object):
    """
    e.g.
      <files_ro size="9999">
        <folder dest="gon_client/mac" />
        <file dest="gon_client/mac/x" checksum="xxxx" size="99" />
        <file dest="gon_client/mac/y" checksum="xxxx" size="99" />
      </files_ro>
    """
    TAG_NAME_files_ro = 'files_ro'
    TAG_NAME_files_rw = 'files_rw'
    TAG_NAME_files = 'files'
    
    ATTRIB_NAME_size = 'size'

    def __init__(self):
        self.size = None
        self.files = []
        self.folders = []
        
    @classmethod
    def parse_from_parent_node(cls, parent_node, tag_name):
        node = parent_node.find(tag_name)
        if node != None:
            gpm_files = GpmFiles()
            gpm_files.size = get_node_attrib_int(node, GpmFilesFile.ATTRIB_NAME_size)
            gpm_files.files = GpmFilesFile.parse_from_parent_node_to_list(node)
            gpm_files.folders = GpmFilesFolder.parse_from_parent_node_to_list(node)
            return gpm_files
        return None
    
    @classmethod
    def parse_from_file(cls, thisfile):
        root = element_tree.ElementTree(None, thisfile)
        if root != None:
            fake_root = element_tree.Element('fake_root_node')
            fake_root.append(root.getroot())
            return GpmFiles.parse_from_parent_node(fake_root, GpmFiles.TAG_NAME_files)
        return None

    def add_as_node(self, parent_node, tag_name):
        node = element_tree.SubElement(parent_node, tag_name)
        set_node_attrib_int(node, GpmFiles.ATTRIB_NAME_size, self.size)
        GpmFilesFile.add_as_nodes(self.files, node)
        GpmFilesFolder.add_as_nodes(self.folders, node)
        return node

    def to_tree(self):
        node = element_tree.Element(GpmFiles.TAG_NAME_files)
        set_node_attrib_int(node, GpmFiles.ATTRIB_NAME_size, self.size)
        GpmFilesFile.add_as_nodes(self.files, node)
        GpmFilesFolder.add_as_nodes(self.folders, node)
        return element_tree.ElementTree(node)

    def validate(self, gpm_build_env, error_handler):
        pass

    @classmethod
    def build_from_filelist(cls, filelist):
        gpm_ro_files = GpmFiles() 
        for thisfile in filelist:
            if filelist[thisfile]['type'] == 'file':
                gpm_file = GpmFilesFile()
                gpm_file.dest = filelist[thisfile]['dest']
                gpm_file.size = filelist[thisfile]['size']
                gpm_file.checksum = filelist[thisfile]['checksum']
                gpm_file.install_options = filelist[thisfile]['install_options']
                gpm_file.install_options_cleanup()
                gpm_file.uninstall_options = filelist[thisfile]['uninstall_options']
                gpm_file.uninstall_options_cleanup()
                gpm_ro_files.files.append(gpm_file)
            else:
                gpm_folder = GpmFilesFolder()
                gpm_folder.dest = filelist[thisfile]['dest']
                gpm_folder.uninstall_options = filelist[thisfile]['uninstall_options']
                gpm_folder.uninstall_options_cleanup()
                gpm_ro_files.folders.append(gpm_folder)
        return gpm_ro_files

    def update(self):
        self.size = 0
        for w_file in self.files:
            self.size += w_file.size

    def get_size(self):
        if self.size != None:
            return self.size
        return 0

class Gpm(object):
    """
    e.g.
      <gpm>
        <header>..</header>
        <dependency>...</dependency>
        <files_ro>...</files_ro>
        <files_rw>...</files_rw>
      </gpm>
    """
    
    TAG_NAME = 'gpm'

    def __init__(self):
        self.header = None
        self.dependency = None
        self.files_ro = None
        self.files_rw = None

    @classmethod
    def parse_from_file(cls, file):
        root = element_tree.ElementTree(None, file)
        if root != None:
            gpm = Gpm()
            gpm.header = GpmHeader.parse_from_parent_node(root)
            gpm.dependency = GpmDependency.parse_from_parent_node(root)
            gpm.files_ro = GpmFiles.parse_from_parent_node(root, GpmFiles.TAG_NAME_files_ro)
            gpm.files_rw = GpmFiles.parse_from_parent_node(root, GpmFiles.TAG_NAME_files_rw)
            return gpm
        return None


    def to_node(self):
        node = element_tree.Element(Gpm.TAG_NAME)
        self.header.add_as_node(node)
        if self.dependency != None:
            self.dependency.add_as_node(node)
        if self.files_ro != None:
            self.files_ro.add_as_node(node, GpmFiles.TAG_NAME_files_ro)
        if self.files_rw != None:
            self.files_rw.add_as_node(node, GpmFiles.TAG_NAME_files_rw)
        return node
        
    def to_tree(self):
        return element_tree.ElementTree(self.to_node())

    def validate(self, gpm_build_env, error_handler):
        pass

    def get_size(self):
        size = 0
        if self.files_ro != None:
            size += self.files_ro.get_size()
        if self.files_rw != None:
            size += self.files_rw.get_size()
        return size

    def update(self):
        if self.files_ro != None:
            self.files_ro.update()
        if self.files_rw != None:
            self.files_rw.update()

    def contains_ro_files(self):
        if self.files_ro != None:
            return len(self.files_ro.files) > 0
        return False

    def is_file_in_package(self, filename):
        if self.files_ro != None:
            for file in self.files_ro.files:
                if filename == file.dest:
                    return True
        if self.files_rw != None:
            for file in self.files_rw.files:
                if filename == file.dest:
                    return True
        return False
    
    def bootification_enabled(self):
        files_to_find = [FILENAME_SYSLINUX_SRC, FILENAME_SYSLINUX_EXE]
        for file_to_find in files_to_find:
            if not self.is_file_in_package(file_to_find):
                return False
        return True
        
    def has_install_options(self):
        if self.files_ro != None:
            for file in self.files_ro.files:
                if file.install_options is not None:
                    return True
        if self.files_rw != None:
            for file in self.files_rw.files:
                if file.install_options is not None:
                    return True
        return False
        
    def has_uninstall_options(self):
        if self.files_ro != None:
            for file in self.files_ro.files:
                if file.uninstall_options is not None:
                    return True
        if self.files_rw != None:
            for file in self.files_rw.files:
                if file.uninstall_options is not None:
                    return True
        return False
    
    def lookup_file_by_dist(self, filename):
        if self.files_ro != None:
            for file in self.files_ro.files:
                if filename == file.dest:
                    return file
        if self.files_rw != None:
            for file in self.files_rw.files:
                if filename == file.dest:
                    return file
        return None
        
        