"""
This module contains functionality for analyzing dependencies between gpm-file-packages
"""
import os.path
import glob
import StringIO

import pygraph.classes.digraph
import pygraph.algorithms.traversal
import pygraph.readwrite.dot
import lib.gpm.gpm_spec
import lib.gpm.gpm_builder

import cPickle

import lib.utility


def add_nodes_safe(graph, nodes):
    for node_id in nodes:
        add_node_safe(graph, node_id)

def add_node_safe(graph, node_id):
    if not graph.has_node(node_id):
        graph.add_node(node_id)

def add_edge_safe(graph, edge, label):
    if not graph.has_edge(edge):
        graph.add_edge(edge)
    graph.set_edge_label(edge, label)
    
class GpmRepository(object):
    GPM_DEPENDENCY_TOTAL_FILENAME = "gpm_dependency_total.dot"
    GPM_DEPENDENCY_ERROR_FILENAME = "gpm_dependency_error.dot"
    GPM_REPOSITORY_CACHE_FILENAME = "gpm_repository_cache.dat"
    
    def __init__(self):
        self._meta_data = {}
        self._gpmcdef_meta_data = {}
        self._clean_dependencies()
        self._cache_checksum = None
    
    def _clean_dependencies(self):
        self._graph_requires = pygraph.classes.digraph.digraph()
        self._graph_requires_inverse = pygraph.classes.digraph.digraph()
        self._graph_obsoletes = pygraph.classes.digraph.digraph()
        self._graph_confilcts = pygraph.classes.digraph.digraph()
        self._graph_updates = pygraph.classes.digraph.digraph()
        self._graph_collection = pygraph.classes.digraph.digraph()

        self._graph_error = pygraph.classes.digraph.digraph()
    
    def get_ids_available(self):
        return set(self._meta_data)
    
    def get_ids_available_for_installation(self, gpm_ids_installed):
        """
        Returns a set of package_ids available for installation based on the given list of installed gpm_ids.
        
        If a package exist with the same name and archetecture then it is NOT included, this give
        the effect that it not possibel to downgrade using the install functionlaity, and upgrade
        only is availabel through the upgrade functionality.
        """
        
        result = set()
        
        gpm_ignore_ids = gpm_ids_installed
        for gpm_installed_id in gpm_ids_installed:
            if self._meta_data.has_key(gpm_installed_id):
                gpm_installed_name = self._meta_data[gpm_installed_id]['name']
                gpm_installed_arch = self._meta_data[gpm_installed_id]['arch']
                gpm_ignore_ids = gpm_ignore_ids.union(set(self._find_package_ids_by_name(gpm_installed_name, package_arch_default=gpm_installed_arch, ignore_package_ids=[gpm_installed_id])))

        for gpm_id in self._meta_data:
            if not gpm_id in gpm_ignore_ids:
                result.add(gpm_id)
        return result
    
    
    
    def get_info(self, gpm_id):
        info = None
        if gpm_id in self._meta_data:
            meta = self._meta_data[gpm_id]
            (summary, description) = meta['descriptions'].get_default()
            info = {'name':meta['name'], 
                    'arch':meta['arch'].get_as_string(),
                    'version':meta['version'].get_as_string(),
                    'version_release':meta['version_release'].get_as_string(),
                    'size':meta['size'],
                    'package_size':meta['package_size'],
                    'checksum':meta['checksum'],
                    'description':description,
                    'summary':summary,
                    'contains_ro_files': meta['contains_ro_files'],
                    'bootification_enabled': meta['bootification_enabled'],
                    'visibility_type': meta['visibility_type']
                    }
        return info

    def get_meta(self):
        return self._meta_data

    def get_gpm_collection_info(self, collection_id):
        info = None
        if collection_id in self._gpmcdef_meta_data.keys():
            meta = self._gpmcdef_meta_data[collection_id]
            if meta.header.descriptions == None:
                (summary, description) = ('No summary', 'No description')
            else:
                (summary, description) = meta.header.descriptions.get_default()
            info = {'summary': summary,
                    'description': description,
                    'gpm_ids' : self.get_ids_for_collection(collection_id)
                    }
        return info

    def get_collection_ids(self):
        return self._gpmcdef_meta_data.keys()

    def get_ids_for_collection(self, collection_id):
        gpm_ids = set()
        for gpm_id in pygraph.algorithms.traversal.traversal(self._graph_collection, collection_id, 'pre'):
            if gpm_id != collection_id:
                gpm_ids.add(gpm_id)
        return gpm_ids
    
    
    
    def _build_dependencies_add_to_graph(self, graph, current_package_id, package_ids, label, missing_package_ids, missing_label, graph_inverse=None):
        add_node_safe(graph, current_package_id)
        if not graph_inverse is None:
            add_node_safe(graph_inverse, current_package_id)
            
        for package_id in package_ids:
            add_node_safe(graph, package_id)
            graph.add_edge((current_package_id, package_id), label=label) 
            if not graph_inverse is None:
                add_node_safe(graph_inverse, package_id)
                add_edge_safe(graph_inverse, (package_id, current_package_id), label='')

        add_node_safe(self._graph_error, current_package_id)
        for package_id in missing_package_ids:
            add_node_safe(self._graph_error, package_id)
            add_edge_safe(self._graph_error, (current_package_id, package_id), label=missing_label)

    
    def build_dependencies(self, error_handler):
        self._clean_dependencies()
        return self._build_dependencies(error_handler)
    
    def _build_dependencies(self, error_handler):
        for package_id in self._meta_data:
            add_node_safe(self._graph_requires, package_id)
            default_name = self._meta_data[package_id]['name']
            default_arch = self._meta_data[package_id]['arch']

            #
            # Build Require graph
            #
            package_ids = []
            missing_package_ids = []
            for package_ref in self._meta_data[package_id]['requires']:
                found_package_ids = self._find_package_ids(package_ref, package_arch_default=default_arch, ignore_package_ids=[package_id])
                if len(found_package_ids) > 0:
                    package_ids.extend(found_package_ids)
                else:
                    missing_package_ids.append(package_ref.name)
                    error_handler.emmit_warning("In the package '%s' the required dependency '%s' was not found." % (package_id, package_ref.name))
            self._build_dependencies_add_to_graph(self._graph_requires, package_id, package_ids, 'require', missing_package_ids, 'require-missing', self._graph_requires_inverse)

            #
            # Build obsolete graph
            #
            package_ids = []
            missing_package_ids = []
            for package_ref in self._meta_data[package_id]['obsoletes']:
                found_package_ids = self._find_package_ids(package_ref, package_arch_default=default_arch, ignore_package_ids=[package_id])
                if len(found_package_ids) > 0:
                    package_ids.extend(found_package_ids)
                else:
                    missing_package_ids.append(package_ref.name)
                    error_handler.emmit_warning("In the package '%s' the obsolete dependency '%s' was not found." % (package_id, package_ref.name))
            self._build_dependencies_add_to_graph(self._graph_obsoletes, package_id, package_ids, 'obsolete', missing_package_ids, 'obsolete-missing')
        
            #
            # Build conflict graph
            #
            package_ids = []
            missing_package_ids = []
            for package_ref in self._meta_data[package_id]['conflicts']:
                found_package_ids = self._find_package_ids(package_ref, package_arch_default=default_arch, ignore_package_ids=[package_id])
                if len(found_package_ids) > 0:
                    package_ids.extend(found_package_ids)
                else:
                    missing_package_ids.append(package_ref.name)
                    error_handler.emmit_warning("In the package '%s' the conflict dependency '%s' was not found." % (package_id, package_ref.name))
            self._build_dependencies_add_to_graph(self._graph_confilcts, package_id, package_ids, 'conflict', missing_package_ids, 'conflict-missing')

            #
            # Build update graph
            #
            package_ids = []
            found_package_ids = sorted(self._find_package_ids_by_name(default_name, package_arch_default=default_arch), self._version_compare)
            head = found_package_ids.pop(0)
            add_node_safe(self._graph_updates, head)

            add_node_safe(self._graph_obsoletes, head)
            while len(found_package_ids) > 0:
                next = found_package_ids.pop(0)
                add_node_safe(self._graph_updates, next)
                add_edge_safe(self._graph_updates, (head, next), label='update')
                add_node_safe(self._graph_obsoletes, next)
                add_edge_safe(self._graph_obsoletes, (next, head), label='obsolete by update')
                head = next
                
                
        #
        # Build collection graph
        #
        for gpmcdef in self._gpmcdef_meta_data.values():
            collection_id = gpmcdef.header.name 
            add_node_safe(self._graph_collection, collection_id)
            self._graph_collection.add_node_attribute(collection_id, ('label', "%s\\ncollection"% collection_id))
            for package_ref in gpmcdef.packages:
                found_package_ids = sorted(self._find_package_ids(package_ref), self._version_compare, reverse=True)
                if len(found_package_ids) > 0:
                    found_package_id = found_package_ids[0]
                    add_node_safe(self._graph_collection, found_package_id)
                    self._graph_collection.add_edge( (collection_id, found_package_id), label='collection')
                else:
                    error_handler.emmit_warning("In the package-collection '%s' the package '%s' was not found." % (collection_id, package_ref.name))
                    

        #
        # A edge found in the error graph indicate an error
        #
        return len(self._graph_error.edges()) > 0
    
    def _version_compare(self, a_package_id, b_package_id):
        a_meta_info = self._meta_data[a_package_id]
        b_meta_info = self._meta_data[b_package_id]
        rc_version = lib.gpm.gpm_spec.GpmVersion.compare(a_meta_info['version'], b_meta_info['version'])
        rc_version_release = lib.gpm.gpm_spec.GpmVersion.compare(a_meta_info['version_release'], b_meta_info['version_release'])
        if rc_version == 1:
            rc = 1
        elif rc_version == 0:
            if rc_version_release == 1:
                rc = 1
            elif rc_version_release == 0:
                rc = 0
            else:
                rc = -1
        else:
            rc = -1
        return rc

    def _find_package_ids_by_name(self, package_name, package_arch_default=None, ignore_package_ids=[]):
        package_ref = lib.gpm.gpm_spec.GpmPackageRef()
        package_ref.name = package_name
        package_ref.arch = None
        return self._find_package_ids(package_ref, package_arch_default, ignore_package_ids)

    def _find_package_ids(self, package_ref, package_arch_default=None, ignore_package_ids=[]):
        package_arch = None
        if  package_arch_default != None:
            package_arch = package_arch_default.arch
        if package_ref.arch != None:
            package_arch = package_ref.arch
        
        result_found_package_ids = []
        for package_id in self._meta_data:
            if not package_id in ignore_package_ids:
                if self._meta_data[package_id]['name'] == package_ref.name:
                    if package_arch != None:
                        if self._meta_data[package_id]['arch'].arch == package_arch:
                            result_found_package_ids.append(package_id)
                    else:
                        result_found_package_ids.append(package_id)
        return result_found_package_ids
    
    
    def add_meta_data(self, (gpm_spec_header, gpm_spec_dependency, gpm_size, gpm_package_size, gpm_checksum, contains_ro_files, bootification_enabled, visibility_type)):
        package_id = gpm_spec_header.get_package_id()
        
        name = gpm_spec_header.name
        arch = gpm_spec_header.arch
        version = gpm_spec_header.version
        version_release = gpm_spec_header.version_release
        discriptions = gpm_spec_header.descriptions
        update = gpm_spec_header.update
        checksum = gpm_checksum
        
        if gpm_spec_dependency != None:
            requires = gpm_spec_dependency.requires
            obsoletes = gpm_spec_dependency.obsoletes
            confilcts = gpm_spec_dependency.confilcts
        else:
            requires = []
            obsoletes = []
            confilcts = []
        self._meta_data[package_id] = {'name':name, 
                                       'arch':arch, 
                                       'version':version, 
                                       'version_release':version_release,
                                       'update': update,
                                       'requires': requires, 
                                       'obsoletes':obsoletes, 
                                       'conflicts':confilcts, 
                                       'size':gpm_size, 
                                       'package_size':gpm_package_size, 
                                       'descriptions':discriptions, 
                                       'checksum':checksum,
                                       'contains_ro_files': contains_ro_files,
                                       'bootification_enabled': bootification_enabled,
                                       'visibility_type': visibility_type
                                       }


    def add_meta_data_from_string(self, gpm_meta_string):
        gpm_meta_file = StringIO.StringIO(gpm_meta_string)
        gpm_meta = lib.gpm.gpm_spec.GpmDef.parse_from_file(gpm_meta_file)
        if gpm_meta != None and gpm_meta.header != None:
            self.add_meta_data((gpm_meta.header, gpm_meta.dependency, None, None, None, False, False, None))
            return gpm_meta.header.get_package_id()
        return None
    
    def _add_graph_preserve(self, main_graph, graph):
        add_nodes_safe(main_graph, graph.nodes())
        for node in graph.nodes():
            for node_attr in graph.node_attributes(node):
                main_graph.add_node_attribute(node, node_attr)
        for edge in graph.edges():
            main_graph.add_edge(edge)
            main_graph.set_edge_label(edge, graph.edge_label(edge))


    def generate_dot_files(self, folder):
        total_graph = pygraph.classes.digraph.digraph()
        self._add_graph_preserve(total_graph, self._graph_requires)
        self._add_graph_preserve(total_graph, self._graph_obsoletes)
        self._add_graph_preserve(total_graph, self._graph_confilcts)
        self._add_graph_preserve(total_graph, self._graph_updates)
        self._add_graph_preserve(total_graph, self._graph_collection)
        
        f = open(os.path.join(folder, GpmRepository.GPM_DEPENDENCY_TOTAL_FILENAME), 'w')
        f.write(pygraph.readwrite.dot.write(total_graph))
        f.close()
        
        f = open(os.path.join(folder, GpmRepository.GPM_DEPENDENCY_ERROR_FILENAME), 'w')
        f.write(pygraph.readwrite.dot.write(self._graph_error))
        f.close()


    def generate_repository_cache(self, folder):
        repository_cache = {}
        repository_cache['gpm_meta_data'] = self._meta_data
        repository_cache['gpmc_meta_data'] = self._gpmcdef_meta_data
        
        f = open(os.path.join(folder, GpmRepository.GPM_REPOSITORY_CACHE_FILENAME), 'w')
        f.write(cPickle.dumps(repository_cache))
        f.close()


    def dump(self):
        total_graph = pygraph.classes.digraph.digraph()
        self._add_graph_preserve(total_graph, self._graph_requires)
        self._add_graph_preserve(total_graph, self._graph_obsoletes)
        self._add_graph_preserve(total_graph, self._graph_confilcts)
        self._add_graph_preserve(total_graph, self._graph_updates)
        
        print pygraph.readwrite.dot.write(total_graph)
    
    def solve(self, current_installed_ids, install_ids, update_ids, remove_ids, solve_for_update=False, solve_for_update_connect=False):
        w_install_ids = set(install_ids)
        w_update_ids  = set(update_ids)
        w_remove_ids = set(remove_ids)
    
        if solve_for_update:
            for update_id in current_installed_ids:
                neighbors = self._graph_updates.neighbors(update_id)
                if len(neighbors) > 0:
                    to_update_id = neighbors[0]
                    w_update_ids.add( (update_id, to_update_id) )

        if solve_for_update_connect:
            for update_id in current_installed_ids:
                neighbors = self._graph_updates.neighbors(update_id)
                if len(neighbors) > 0:
                    to_update_id = neighbors[0]
                    meta_update = self._meta_data[to_update_id]['update']
                    if meta_update.timing == lib.gpm.gpm_spec.GpmUpdate.TIMINGE_connect:
                        w_update_ids.add( (update_id, to_update_id) )

        for install_id in install_ids:
            for to_install_id in pygraph.algorithms.traversal.traversal(self._graph_requires, install_id, 'pre'):
                if install_id != to_install_id:
                    w_install_ids.add(to_install_id)
            for to_remove_id in pygraph.algorithms.traversal.traversal(self._graph_obsoletes, install_id, 'pre'):
                if install_id != to_remove_id:
                    w_remove_ids.add(to_remove_id)

        for (update_id, update_id_to) in w_update_ids:
            for to_install_id in pygraph.algorithms.traversal.traversal(self._graph_requires, update_id_to, 'pre'):
                if update_id_to != to_install_id:
                    w_install_ids.add(to_install_id)
            for to_remove_id in pygraph.algorithms.traversal.traversal(self._graph_obsoletes, update_id_to, 'pre'):
                w_remove_ids.add(to_remove_id)
        
        for remove_id in remove_ids:
            for remove_id_to in pygraph.algorithms.traversal.traversal(self._graph_requires_inverse, remove_id, 'pre'):
                if remove_id != remove_id_to:
                    w_remove_ids.add(remove_id_to)

        w_install_ids = w_install_ids - current_installed_ids - set([i[1] for i in w_update_ids])
        w_remove_ids = w_remove_ids.intersection(current_installed_ids) 
        
        if len(w_install_ids) == len(install_ids) and len(w_update_ids) == len(update_ids) and len(w_remove_ids) == len(remove_ids):
            return (w_install_ids, w_update_ids, w_remove_ids)
        return self.solve(current_installed_ids, w_install_ids, w_update_ids, w_remove_ids)
              

    def init_from_gpms(self, error_handler, gpms_root, gpmcdef_root):
        self._clean_dependencies()
        for gpm_filename in glob.glob(os.path.join(os.path.abspath(gpms_root), '*.gpm')):
            gpm_meta_data = lib.gpm.gpm_builder.query_meta(gpm_filename, error_handler)
            self.add_meta_data((gpm_meta_data.header, gpm_meta_data.dependency, gpm_meta_data.get_size(), lib.gpm.gpm_builder.calculate_package_size(gpm_filename), lib.gpm.gpm_builder.calculate_checksum(gpm_filename), gpm_meta_data.contains_ro_files(), gpm_meta_data.bootification_enabled(), gpm_meta_data.header.get_visibility_type()))

        self._gpmcdef_meta_data = {}
        gpmcdefs = lib.gpm.gpm_builder.query_collection_meta_from_files(gpmcdef_root)
        for gpmcdef in gpmcdefs:
            collection_id = gpmcdef.header.name 
            self._gpmcdef_meta_data[collection_id] = gpmcdef
        
        self._build_dependencies(error_handler)


    def init_using_cache(self, error_handler, gpms_root, gpmcdef_root):
        repository_cache = {}
        repository_cache['gpm_meta_data'] = self._meta_data
        repository_cache['gpmc_meta_data'] = self._gpmcdef_meta_data
        
        reposoitory_cache_filename = os.path.join(gpms_root, GpmRepository.GPM_REPOSITORY_CACHE_FILENAME)
        f = open(reposoitory_cache_filename, 'r')

        self._clean_dependencies()
        try:
            repository_cache = cPickle.loads(f.read())
            self._meta_data = repository_cache['gpm_meta_data'] 
            self._gpmcdef_meta_data = repository_cache['gpmc_meta_data'] 
        except:
            error_handler.emmit_error("Error loading package meta cache: %s" % reposoitory_cache_filename)

        f.close()
        self._build_dependencies(error_handler)
        self._cache_checksum = lib.utility.calculate_checksum_for_file(reposoitory_cache_filename)

    def has_cache_changed(self, gpms_root):
        reposoitory_cache_filename = os.path.join(gpms_root, GpmRepository.GPM_REPOSITORY_CACHE_FILENAME)
        cache_checksum = lib.utility.calculate_checksum_for_file(reposoitory_cache_filename)
        return not self._cache_checksum == cache_checksum
        
    def ids_to_filenames(self, gpms_root, gpm_ids):
        result = []
        for gpm_id in gpm_ids:
            filename = os.path.join(gpms_root, '%s.gpm' % gpm_id)
            if os.path.isfile(filename):
                result.append(filename) 
        return result

    def ids_to_filenames_relative(self, gpms_root, gpm_ids):
        result = []
        for gpm_id in gpm_ids:
            filename_relative = '%s.gpm' % gpm_id
            filename_abs = os.path.join(gpms_root, filename_relative)
            if os.path.isfile(filename_abs):
                result.append(filename_relative) 
        return result


    def query_package(self, package_name, package_arch, ids_installed):
        """
        Query the reposetory for a matching package. Returns a tuple (installed(boolean), package_id)
        
        If more versions of the package exists the the newest is returned
        """
        package_ref = lib.gpm.gpm_spec.GpmPackageRef()
        package_ref.name = package_name
        package_ref.arch = package_arch
        package_ids = sorted(self._find_package_ids_by_name(package_name, package_arch_default=package_ref), self._version_compare, reverse=True)
        if len(package_ids) > 0:
            package_id = package_ids[0]
            installed = False
            for package_id_work in package_ids:
                if package_id_work in ids_installed:
                    installed = True
                    break
            return (installed, package_id)
        return (None, None)
    

    def query_is_security_update(self, gpm_ids_update):
        """
        Returns true if one of the package is a security update
        """
        for (_, to_id) in gpm_ids_update:
            if self._meta_data[to_id]['update'].type == lib.gpm.gpm_spec.GpmUpdate.TYPE_security:
                return True 
        return False
       
