'''
Created on 22/03/2010

@author: peter
'''

from components.rest.rest_api import RestApi

def copy_token_info(token):
    new_token = RestApi.AdminDeploy.Token.TokenInfo()
    new_token.set_token_type_id(token.get_token_type_id)
    new_token.set_token_internal_type(token.get_token_internal_type)
    new_token.set_token_id(token.get_token_id)
    new_token.set_token_label(token.get_token_label)
    new_token.set_token_status(token.get_token_status)
    new_token.set_token_serial(token.get_token_serial)
    new_token.set_runtime_env_id(token.get_runtime_env_id)
    new_token.set_token_type_label(token.get_token_type_label)
    new_token.set_name(token.get_name)
    new_token.set_casing(token.get_casing)
    new_token.set_description(token.get_description)
    new_token.set_enrollable(token.get_enrollable)
    return new_token

def copy_tokens(tokens):
    return [copy_token_info(token) for token in tokens]
