"""
This module contains class for representing jobs used by ws's
"""
from __future__ import with_statement
import threading
import sys

import lib.checkpoint


class JobStatus(object):
    """
    This class represent the status of a job
    """
    STATUS_INIT = 0
    STATUS_RUNNING = 1
    STATUS_CANCELING = 2
    STATUS_DONE_ok = 3
    STATUS_DONE_error = 4
    STATUS_DONE_cancled = 5

    def __init__(self, more, status, header, sub_header, progress):
        self.more = more
        self.status = status
        self.header = header
        self.sub_header = sub_header
        self.progress = progress
        self.done_message = None
        self.done_arg_01 = None

    def get_response_job_status(self):
        status = self.status
        if status in [lib.ws.job.JobStatus.STATUS_INIT, lib.ws.job.JobStatus.STATUS_RUNNING]:
            return 'running_job_status'
        elif status == lib.ws.job.JobStatus.STATUS_CANCELING:
            return 'canceling_job_status'
        elif status == lib.ws.job.JobStatus.STATUS_DONE_ok:
            return 'done_ok_job_status'
        elif status == lib.ws.job.JobStatus.STATUS_DONE_error:
            return 'done_error_job_status'
        elif status == lib.ws.job.JobStatus.STATUS_DONE_cancled:
            return 'done_cancled_job_status'

    @classmethod
    def create_status_init(cls):
        return JobStatus(True, JobStatus.STATUS_INIT, '', '', 0)

    @classmethod
    def create_status_running(cls, header, sub_header, progress=None):
        if progress is None:
            progress = 0
        return JobStatus(True, JobStatus.STATUS_RUNNING, header, sub_header, progress)


class Job(threading.Thread):
    """
    This class represent a job
    """
    def __init__(self, checkpoint_handler, module_id, job_name):
        threading.Thread.__init__(self, name=job_name)
        self.checkpoint_handler = checkpoint_handler
        self._module_id = module_id
        self._status = JobStatus.create_status_init()
        self._status_change_callback = None
        self._parent_job = None
        self._parent_job_progress_min = 0
        self._parent_job_progress_max = 100

    def connect_to_parent_job(self, parent_job, progress_min=0, progress_max=100):
        self._parent_job = parent_job
        self._parent_job_progress_min = progress_min
        self._parent_job_progress_max = progress_max

    def disconnect_from_parent_job(self):
        self._parent_job = None
        self._parent_job_progress_min = 0
        self._parent_job_progress_max = 100

    def _do_call_parent_job(self):
        if self._parent_job is not None:
            local_progress_delta = self._parent_job_progress_max - self._parent_job_progress_min
            local_progress_pct = (local_progress_delta / 100.0)
            local_progress = self._parent_job_progress_min + (local_progress_pct * self._status.progress)
            self._parent_job.set_status_from_child(self._status.sub_header, local_progress)

    def set_status_from_child(self, sub_header, progress=None):
        self._status.more = True
        self._status.status = JobStatus.STATUS_RUNNING
        self._status.sub_header = sub_header
        self._status.progress = progress

    def set_status_change_callback(self, status_change_callback, progress_min=0, progress_max=100):
        self._status_change_callback = status_change_callback

    def reset_status_change_callback(self):
        self._status_change_callback = None

    def _do_status_change_callback(self):
        if self._status_change_callback is not None:
            self._status_change_callback(self._status)

    def get_status(self):
        return self._status

    def set_status(self, header, sub_header, progress=None):
        self._status.more = True
        self._status.status = JobStatus.STATUS_RUNNING
        self._status.header = header
        self._status.sub_header = sub_header
        if progress is not None:
            self._status.progress = progress
        self._do_call_parent_job()
        self._do_status_change_callback()

    def set_status_error(self, header, sub_header):
        self._status.more = False
        self._status.status = JobStatus.STATUS_DONE_error
        self._status.header = header
        self._status.sub_header = sub_header
        self._status.progress = 100
        self._do_status_change_callback()

    def set_status_cancel(self):
        self._status.more = True
        self._status.status = JobStatus.STATUS_CANCELING
        self._status.header = 'Canceling'
        self._status.sub_header = ''
        self._do_status_change_callback()

    def set_status_canceled(self):
        self._status.more = False
        self._status.status = JobStatus.STATUS_DONE_cancled
        self._status.header = 'Canceled'
        self._status.sub_header = ''
        self._status.progress = 100
        self._do_status_change_callback()

    def set_status_done(self, header=None, sub_header=None, done_message=None, done_arg_01=None):
        self._status.more = False
        self._status.status = JobStatus.STATUS_DONE_ok
        self._status.progress = 100
        if header is not None:
            self._status.header = header
        if sub_header is not None:
            self._status.sub_header = sub_header
        if done_message is not None:
            self._status.done_message = done_message
        if done_arg_01 is not None:
            self._status.done_arg_01 = done_arg_01
        self._do_call_parent_job()
        self._do_status_change_callback()

    def run(self):
        with self.checkpoint_handler.CheckpointScope("Job.run", self._module_id, lib.checkpoint.DEBUG):
            try:
                self._status.status = JobStatus.STATUS_RUNNING
                self.do_run()
                if self._status.status == JobStatus.STATUS_CANCELING:
                    self.set_status_canceled()
                if self._status.status == JobStatus.STATUS_RUNNING:
                    self.set_status_done()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("Job.run.error", self._module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)
                self.set_status_error('Internal error', lib.checkpoint.convert_exception_to_summary(etype, evalue, etrace))

    def cancel(self):
        with self.checkpoint_handler.CheckpointScope("AdminDeployJob.cancel", self._module_id, lib.checkpoint.DEBUG):
            self.set_status_cancel()
            self.do_cancel()

    def do_run(self):
        raise NotImplementedError

    def do_cancel(self):
        raise NotImplementedError


class JobHandler(object):
    def __init__(self):
        self._jobs = {}
        self._next_job_id = 0

    def get_job(self, job_id):
        if job_id in self._jobs:
            return self._jobs[job_id]
        return None

    def add_job(self, job):
        job_id = self._next_job_id
        self._next_job_id += 1
        self._jobs[job_id] = job
        job.start()
        return job_id
