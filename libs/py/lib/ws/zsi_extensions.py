'''
Created on Jan 5, 2011

@author: thwang
'''
import os.path
import mimetypes
import traceback

import ZSI
import ZSI.ServiceContainer

import socket
from SocketServer import BaseServer
#from OpenSSL import SSL

import ssl

import lib.checkpoint
import urllib



class GSOAPRequestHandlerHTTP(ZSI.ServiceContainer.SOAPRequestHandler):
    checkpoint_handler = None
    module_id = "unknown"
    www_html_path = None
           
    def log_error(self, *args):
        print "log_error", args
        if GSOAPRequestHandlerHTTP.checkpoint_handler is not None:
            GSOAPRequestHandlerHTTP.checkpoint_handler.Checkpoint("soap_ws", GSOAPRequestHandlerHTTP.module_id, lib.checkpoint.ERROR, client_address=self.address_string())

    def log_message(self, format, *args):
        pass
    
    def do_GET(self):
        if GSOAPRequestHandlerHTTP.www_html_path is not None:
            url_path = urllib.unquote(self.path[1:])
            url = os.path.join(os.path.abspath(GSOAPRequestHandlerHTTP.www_html_path), url_path)
            if os.path.isfile(url):
                GSOAPRequestHandlerHTTP.checkpoint_handler.Checkpoint("soap_ws_file_url", GSOAPRequestHandlerHTTP.module_id, lib.checkpoint.DEBUG, url=url)
                url_file = open(url, 'rb')
                url_file_content = url_file.read()
                url_file.close()
    
                (mime_type, encoding) = mimetypes.guess_type(url_path)
                self.send_response(200)
                self.send_header('Content-type', mime_type)
                self.send_header('Content-Length', str(len(url_file_content)))
                self.end_headers()
    
                self.wfile.write(url_file_content)
                self.wfile.flush()
                return
        ZSI.ServiceContainer.SOAPRequestHandler.do_GET(self)


class GSOAPRequestHandlerHTTPS(GSOAPRequestHandlerHTTP):
    def setup(self):
        self.connection = self.request
        self.rfile = socket._fileobject(self.request, "rb", self.rbufsize)
        self.wfile = socket._fileobject(self.request, "wb", self.wbufsize)


class ServiceContainerHTTP(ZSI.ServiceContainer.ServiceContainer):
    def __init__(self, address, services):
        ZSI.ServiceContainer.ServiceContainer.__init__(self, address, services, GSOAPRequestHandlerHTTP)

    @classmethod
    def create(cls, checkpoint_handler, module_id, address, services, www_html_path=None):
        service_container = ServiceContainerHTTP(address, services)
        GSOAPRequestHandlerHTTP.checkpoint_handler = checkpoint_handler
        GSOAPRequestHandlerHTTP.module_id = module_id
        GSOAPRequestHandlerHTTP.www_html_path = www_html_path 
        return service_container


class ServiceContainerHTTPS(ZSI.ServiceContainer.ServiceContainer):
    def __init__(self, address, services, https_certificate_filename, https_key_filename):
        BaseServer.__init__(self, address, GSOAPRequestHandlerHTTPS)
#        self.ctx = ssl.SSL.Context(SSL.SSLv23_METHOD)
#        self.ctx.use_privatekey_file (https_key_filename)
#        self.ctx.use_certificate_file(https_certificate_filename)
#        self.socket = SSL.Connection(self.ctx, socket.socket(self.address_family, self.socket_type))

        self.socket = ssl.wrap_socket(socket.socket(self.address_family, self.socket_type),
                                      server_side=True,
                                      certfile=https_certificate_filename,
                                      keyfile=https_key_filename,
                                      ssl_version=ssl.PROTOCOL_TLSv1,
                                      ciphers="ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA")

        self._nodes = self.NodeTree()
        map(lambda s: self.setNode(s), services)
        self.server_bind()
        self.server_activate()

    @classmethod
    def create(cls, checkpoint_handler, module_id, address, services, https_certificate_filename, https_key_filename, www_html_path=None):
        service_container = ServiceContainerHTTPS(address, services, https_certificate_filename, https_key_filename)
        GSOAPRequestHandlerHTTP.checkpoint_handler = checkpoint_handler
        GSOAPRequestHandlerHTTP.module_id = module_id
        GSOAPRequestHandlerHTTP.www_html_path = www_html_path 
        return service_container
