"""
TWA, 2008.05.05
This is a hack in order to throw own exceptions, otherwise all
exceptions will be translated to a Fault 
"""
import ZSI.fault 

def FaultFromException(ex, inheader, tb=None, actor=None):
    '''Return a Fault object created from a Python exception.

    <SOAP-ENV:Fault>
      <faultcode>SOAP-ENV:Server</faultcode>
      <faultstring>Processing Failure</faultstring>
      <detail>
        <ZSI:FaultDetail>
          <ZSI:string></ZSI:string>
          <ZSI:trace></ZSI:trace>
        </ZSI:FaultDetail>
      </detail>
    </SOAP-ENV:Fault>
    '''

    tracetext = None
    if tb:
        try:
            lines = '\n'.join(['%s:%d:%s' % (name, line, func)
                               for name, line, func, text in ZSI.fault.traceback.extract_tb(tb)])
        except:
            pass
        else:
            tracetext = lines
  
    exceptionName = ""
    try:
        exceptionName = ":".join([ex.__module__, ex.__class__.__name__])
    except: pass

    # HACK, begin ----- 
    if isinstance(ex, ZSI.fault.Fault): 
        return ex             
    # HACK, end   ----- 

    elt = ZSI.fault.ZSIFaultDetail(string=exceptionName + "\n" + str(ex), trace=tracetext)
    if inheader:
        detail, headerdetail = None, elt
    else:
        detail, headerdetail = elt, None
    return ZSI.fault.Fault(ZSI.fault.Fault.Server, 'Processing Failure',
                actor, detail, headerdetail)

#
# HACK, overwriting ZSI lib funcitons
#
ZSI.FaultFromException = FaultFromException
ZSI.fault.FaultFromException = FaultFromException
