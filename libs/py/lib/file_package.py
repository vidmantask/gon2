"""
A file package are a collection of files that can be distributed installed.

A file package contains a tar-file of the following files:
+ file_package.info.gz, contains compressed meta information about the package
+ file_package.files.gz, contains compressed files

A file package is created from a spec-file:
Name:xxxxxx
Version:1,2,3,4
Arch:win|mac|linux
Description::xxxxxxxxxxxxxxxxxxxxxxxxxx

%files
gon_client/win    
gon_client/win/x.txt    


"""
from __future__ import with_statement
import os
import os.path
import tempfile
import tarfile
import shutil

class FilePackageError(Exception):
    """
    Instances of this class are thrown in case or errors
    """
    def __init__(self, message):
        self.message = message
    
    def __str__(self):
        return self.message



class FilePackageSpec(object):
    """
    Represent a File Package specification
    """
    ELEMENT_TYPE_LINE = 0
    ELEMENT_TYPE_MULTILINE = 1
    
    ELEMENT_NAME_GPM_VERSION = 'gpm_version'
    ELEMENT_NAME_NAME = 'name'
    ELEMENT_NAME_VERSION = 'version'
    ELEMENT_NAME_DESCRIPTION = 'description'
    ELEMENT_NAME_ARCH = 'arch'
    ELEMENT_NAME_FILES = 'files'
    ELEMENT_NAME_FILES_EXCLUDE = 'files_exclude'
    ELEMENT_NAME_FILES_EXPANDED = 'files_expanded'
    
    CURRENT_GPM_VERSION = '1.0'
    
    def __init__(self, elements):
        self.elements = elements

    def dump(self):
        for element in self.elements.values():
            print element

    def get_info(self):
        name = self.elements[FilePackageSpec.ELEMENT_NAME_NAME][2]
        version = self.elements[FilePackageSpec.ELEMENT_NAME_VERSION][2]
        arch = self.elements[FilePackageSpec.ELEMENT_NAME_ARCH][2]
        description = self.elements[FilePackageSpec.ELEMENT_NAME_DESCRIPTION][2]
        return FilePackageInfo(name, version, arch, description)    

    def get_files(self, file_package_root):
        file_package_root_abs = os.path.abspath(file_package_root)
        files_from_tag = self.elements[FilePackageSpec.ELEMENT_NAME_FILES][2]
        files_from_tag_abs = []
        for file_from_tag in files_from_tag:
            file_from_tag_abs = os.path.join(file_package_root_abs, file_from_tag)
            if os.path.exists(file_from_tag_abs):
                files_from_tag_abs.append(file_from_tag_abs)
            else:
                print "Warning: '%s' is not found" % file_from_tag_abs 

        files_exclude_from_tag_abs = []
        if self.elements.has_key(FilePackageSpec.ELEMENT_NAME_FILES_EXCLUDE):
            files_ignore_from_tag = self.elements[FilePackageSpec.ELEMENT_NAME_FILES_EXCLUDE][2]
            for file_from_tag in files_ignore_from_tag:
                files_exclude_from_tag_abs.append(os.path.normpath(os.path.join(file_package_root_abs, file_from_tag)))
        
        files_from_tag_rel_expanded = []
        for file_from_tag_abs in files_from_tag_abs:
            if os.path.isdir(file_from_tag_abs):
                for root, dirs, files in os.walk(file_from_tag_abs):
                    for file in files:
                        filename_abs = os.path.join(os.path.abspath(root), file)
                        if os.path.normpath(filename_abs) not in files_exclude_from_tag_abs:
                            filename_rel = filename_abs[len(file_package_root_abs)+1:]
                            files_from_tag_rel_expanded.append(filename_rel)
            else:
                if os.path.normpath(file_from_tag_abs) not in files_exclude_from_tag_abs:
                    filename_rel = file_from_tag_abs[len(file_package_root_abs)+1:]
                    files_from_tag_rel_expanded.append(filename_rel)

            



        return files_from_tag_rel_expanded
                
    def get_files_expanded(self):
        result = []
        files_from_tag = self.elements[FilePackageSpec.ELEMENT_NAME_FILES_EXPANDED][2]
        for file_from_tag in files_from_tag:
            (filename, size, checksum) = file_from_tag.partition(',')
            result.append(filename.strip())
        return result

    
    @classmethod
    def _parse_element(cls, file_obj, first_line):
        if first_line.startswith('#'):
            return None
        elif first_line.startswith('%'):
            return FilePackageSpec._parse_element_multiline(first_line, file_obj)
        else:
            return FilePackageSpec._parse_element_line(first_line, file_obj)

    @classmethod
    def _parse_element_multiline(cls, first_line, file_obj):
        tag = first_line[1:len(first_line)-1]
        values = []
        while True:
            file_pos_saved = file_obj.tell()
            next_line = file_obj.readline()
            if next_line == '':
                break
            elif next_line.startswith('%'):
                file_obj.seek(file_pos_saved)
                break
            next_line = next_line.strip()
            if next_line != '':
                values.append(next_line)
        return (FilePackageSpec.ELEMENT_TYPE_MULTILINE, tag.strip().lower(), values)
    
    @classmethod
    def _parse_element_line(cls, first_line, file_obj):
        tag, seperator, value = first_line.partition(':')
        return (FilePackageSpec.ELEMENT_TYPE_LINE, tag.strip().lower(), value.strip())

    @classmethod
    def create_from_filename(cls, filename):
        file_obj = open(filename, 'r')
        return cls.create_from_file(file_obj)
        
    @classmethod
    def create_from_file(cls, file_obj):
        elements = {}
        line = file_obj.readline()
        while line:
            element = FilePackageSpec._parse_element(file_obj, line)
            if element != None and element[1] != '':
                elements[element[1]] = element
            line = file_obj.readline()

        return FilePackageSpec(elements)


class FilePackageInfo(object):
    def __init__(self, name, version, arch, description=''):
        self.name = name
        self.version = version
        self.arch = arch
        self.description = description

    def get_package_name(self):
        return '%s-%s-%s' % (self.name, self.version, self.arch)
    
    def get_package_name_as_rel_path(self):
        return os.path.join(self.name, self.arch)

    def generate_file_package(self, info_file):
        info_file.write('%s:%s\n' % (FilePackageSpec.ELEMENT_NAME_GPM_VERSION, FilePackageSpec.CURRENT_GPM_VERSION))
        info_file.write('%s:%s\n' % (FilePackageSpec.ELEMENT_NAME_NAME, self.name))
        info_file.write('%s:%s\n' % (FilePackageSpec.ELEMENT_NAME_VERSION, self.version))
        info_file.write('%s:%s\n' % (FilePackageSpec.ELEMENT_NAME_ARCH, self.arch))
        info_file.write('%s:%s\n' % (FilePackageSpec.ELEMENT_NAME_DESCRIPTION, self.description))

    def generate_file_package_header_filesss_expanded(self, info_file):
        info_file.write('%files_expanded\n')

    @classmethod
    def create_from_file_package(cls, file_package_filename):
        file_package_filename_abs = os.path.join(os.path.abspath(file_package_filename))
        file_package = tarfile.open(file_package_filename_abs, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')

        info_file_gz = tarfile.open(name='fake', fileobj=file_package.extractfile('file_package.info.gz'), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
        info_file = info_file_gz.extractfile('file_package.info')

        file_package_spec = FilePackageSpec.create_from_file(info_file)
        return  file_package_spec.get_info()
        


class FilePackageFile(object):
    def __init__(self, filename, filesize):
        self.filename = filename
        self.filesize = filesize
        self.md5 = None
    
    def calculate(self, file_package_root):
        self.md5 = '42'

    def verify(self, file_package_root):
        pass

    def generate_file_package(self, info_file):
        info_file.write('%s,%s,%s\n' % (self.filename, self.filesize, self.md5))
    

class FilePackage(object):
    def __init__(self):
        self.info = None
        self.files = {}
        
    def add_info(self, info):
        self.info = info
        
    def add_file(self, file_package_file):
        self.files[file_package_file.filename] = file_package_file
    
    def add_file_from_list(self, file_package_root, filenames_rel):
        for filename_rel in filenames_rel:
            self.add_file_from_file(file_package_root, filename_rel)

    def add_file_from_file(self, file_package_root, filename):
        filename_abs = os.path.join(os.path.abspath(file_package_root), filename)
        filename_rel = filename
        if not os.path.exists(filename_abs):
            raise FilePackageError("The file '%s' was not found" % filename_abs)
        
        filesize = os.path.getsize(filename_abs)
        file_package_file = FilePackageFile(filename_rel, filesize)
        file_package_file.calculate(filename_abs)
        self.add_file(file_package_file)

    def generate_file_package(self, temp_root, file_package_destination_root, file_package_root):
        infotemp_file_filename = 'file_package.info'
        infotemp_file_filename_abs = os.path.join(os.path.abspath(temp_root), infotemp_file_filename)
        infotemp_file = open(infotemp_file_filename_abs,'w+')
        
        files_file_filename = 'file_package.files.gz'
        files_file_filename_abs = os.path.join(os.path.abspath(temp_root), files_file_filename)
        files_file = tarfile.open(files_file_filename_abs, "w:gz", format=tarfile.PAX_FORMAT, encoding='UTF-8')

        self.info.generate_file_package(infotemp_file)
        self.info.generate_file_package_header_filesss_expanded(infotemp_file)
        for file in self.files.values():
            file.generate_file_package(infotemp_file)
#            tar_filename = os.path.join(self.info.get_package_name_as_rel_path(), file.filename)
            files_file.add(os.path.join(os.path.abspath(file_package_root), file.filename), file.filename.encode('UTF-8'))
        files_file.close()
        infotemp_file.close()

        info_file_filename = 'file_package.info.gz'
        info_file_filename_abs = os.path.join(os.path.abspath(temp_root), info_file_filename)
        info_file = tarfile.open(info_file_filename_abs, "w:gz", format=tarfile.PAX_FORMAT, encoding='UTF-8')
        info_file.add(infotemp_file_filename_abs, infotemp_file_filename.encode('UTF-8'))
        info_file.close()

        file_package_filename = '%s.gpm' %  self.info.get_package_name()
        file_package_filename_abs = os.path.join(os.path.abspath(file_package_destination_root), file_package_filename)
        file_package = tarfile.open(file_package_filename_abs, "w", format=tarfile.PAX_FORMAT, encoding='UTF-8')
        file_package.add(info_file_filename_abs, info_file_filename.encode('UTF-8'))
        file_package.add(files_file_filename_abs, files_file_filename.encode('UTF-8'))
        file_package.close()
        
        os.remove(infotemp_file_filename_abs)
        os.remove(files_file_filename_abs)
        os.remove(info_file_filename_abs)
        


class FilePackageHandler(object):
    def __init__(self, temp_root):
        self.temp_root = temp_root
        
    def build_from_folder(self, file_package_destination_root, folder_root, file_package_name, file_package_arch, file_package_version):
        file_package = FilePackage()
        file_package_info = FilePackageInfo(file_package_name, file_package_version, file_package_arch)
        file_package.add_info(file_package_info)
        
        folder_root_abs = os.walk(os.path.abspath(folder_root))
        for root, dirs, files in folder_root_abs:
            for file in files:
                filename_abs = os.path.join(os.path.abspath(root), file)
                filename_rel = filename_abs[len(folder_root)+1:]
                file_package.add_file_from_file(folder_root, filename_rel)
        file_package.generate_file_package(self.temp_root, file_package_destination_root, folder_root)

    def build_from_spec(self, file_package_destination_root,  file_package_root, file_package_spec_filename):
        file_package_spec = FilePackageSpec.create_from_filename(file_package_spec_filename)
        file_package = FilePackage()
        file_package_info = file_package_spec.get_info()
        file_package.add_info(file_package_info)
        file_package.add_file_from_list(file_package_root, file_package_spec.get_files(file_package_root))
        file_package.generate_file_package(self.temp_root, file_package_destination_root, file_package_root)
        

    def extract(self, file_package_filename, file_package_destination_root):
        file_package_filename_abs = os.path.abspath(file_package_filename)
        file_package = tarfile.open(file_package_filename_abs, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')

        files_file = tarfile.open(name='fake', fileobj=file_package.extractfile('file_package.files.gz'), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
        files_file.extractall(file_package_destination_root)
        files_file.close()
        file_package.close()

    def extract_info(self, file_package_filename, file_package_destination_info_root):
        file_package_filename_abs = os.path.abspath(file_package_filename)
        file_package = tarfile.open(file_package_filename_abs, "r", format=tarfile.PAX_FORMAT, encoding='UTF-8')

        file_package_info = FilePackageInfo.create_from_file_package(file_package_filename)
        file_package_destination_info_abs =  os.path.join(os.path.abspath(file_package_destination_info_root), file_package_info.get_package_name())

        info_file_gz = tarfile.open(name='fake', fileobj=file_package.extractfile('file_package.info.gz'), mode='r:gz', format=tarfile.PAX_FORMAT, encoding='UTF-8')
        info_file = info_file_gz.extractfile('file_package.info')

        file_package_destination_info_file = open(file_package_destination_info_abs, 'w+')
        shutil.copyfileobj(info_file, file_package_destination_info_file)

    def erase_files(self, file_package_info_filename, dest_package_folder):
        file_package_spec = FilePackageSpec.create_from_filename(file_package_info_filename)
        for filename in file_package_spec.get_files_expanded():
            filename_abs = os.path.join(dest_package_folder, filename)
            try:
                os.remove(filename_abs)
            except OSError, e:
                print "Warning: Unable to erase '%s'. %s" % (filename_abs, e) 
        try:
            os.remove(file_package_info_filename)
        except OSError, e:
            print "Warning: Unable to erase '%s'. %s" % (filename_abs, e) 
            
