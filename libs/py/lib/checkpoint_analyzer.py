"""
This module is holding functionlaity for analyzing and handling session-xml log files
"""
from __future__ import with_statement
import xml.sax
import elementtree.ElementTree as element_tree


def add_header(filename_in, filename_out):
    file_in = open(filename_in, 'r')
    file_out = open(filename_out, 'w')
    
    xml_header = "<?xml version='1.0' encoding='utf-8' ?>\n"
    xml_tag_begin = "<cps>\n"
    xml_tag_end   = "</cps>\n"
    
    do_add_header = False
    first_line = True
    for line in file_in:
        if first_line:
            if line != xml_header:
               do_add_header = True
               file_out.write(xml_header)
               file_out.write(xml_tag_begin)
            first_line = False

        line_part_count = 0
        for line_part in line.split('<'):
            if line_part_count == 0:
                pass
            elif line_part_count == 1:
                file_out.write('<')
            else:
                file_out.write('&lt;')
            file_out.write(line_part)
            line_part_count += 1

    if do_add_header:
        file_out.write(xml_tag_end)

    file_out.close()
    file_in.close()
    


class ScopeGenerator(xml.sax.ContentHandler):
    TAG_NAME_SCPS = 'SCPS'
    TAG_NAME_SCP  = 'SCP'
    
    def __init__(self):
        self.scps_tag_element = element_tree.Element(ScopeGenerator.TAG_NAME_SCPS)
        self.elements = [self.scps_tag_element]
        
    def generate(self, filename_in, filename_out):
        xml.sax.parse(filename_in, self)
        tree = element_tree.ElementTree(self.scps_tag_element)
        tree.write(filename_out)
        
    def startDocument(self):
        pass
    
    def endDocument(self):
        pass

    def startElement(self, name, attrs):
        if name != 'cp':
            return
        
        parent_node = self.elements.pop() 

        attr_names = attrs.getNames()
        if 'scope_end' in attr_names:
            node = parent_node
            for attr_name in attr_names:
                node.attrib[attr_name] = attrs.getValue(attr_name)
        elif 'scope' not in attr_names:
            node = element_tree.SubElement(parent_node, ScopeGenerator.TAG_NAME_SCP)
            for attr_name in attr_names:
                node.attrib[attr_name] = attrs.getValue(attr_name)
            self.elements.append(parent_node)
        else:
            node = element_tree.SubElement(parent_node, ScopeGenerator.TAG_NAME_SCP)
            for attr_name in attr_names:
                node.attrib[attr_name] = attrs.getValue(attr_name)
            self.elements.append(parent_node)
            self.elements.append(node)
        
    def endElement(self, name):
        pass


client_session_in = "c:\\tmp\\gon_client_log\\client_session.xml"
client_session_in_temp  = "c:\\tmp\\gon_client_log\\client_session.xml.tmp"
client_session_in_scope = "c:\\tmp\\gon_client_log\\client_session.xml.scope"
#server_session_in = "c:\\tmp\\gon_server_gateway_log\\server_session.xml"
#server_session_in_temp = "c:\\tmp\\gon_server_gateway_log\\server_session.xml.temp"


add_header(client_session_in, client_session_in_temp)
scg = ScopeGenerator()
scg.generate(client_session_in_temp, client_session_in_scope)

#add_header(server_session_in, server_session_in_temp)



    
    
    
    
    
