'''
'''
import os
import sys
if sys.platform == 'win32':
    import lib.hardware.sys_info_surveillance_win


class SysInfoSystem(object):
    def __init__(self):
        self._info = None
        
    def get_cpu_usage(self):
        """
        Returns the average cpu usage since last call to the same function.
        """
        if sys.platform == 'win32':
            self._info = lib.hardware.sys_info_surveillance_win.calc_cpu_usage(self._info)
            return self._info['cpu_usage']
        return 0

    @classmethod
    def create(cls):
        return SysInfoSystem()


class SysInfoProcess(object):
    def __init__(self, pid, name, path):
        self.pid = pid
        self.name = name
        self.path = path

    def get_name(self):
        return self.name

    def get_memory_usage(self):
        """
        Return memory usage of this process in  bytes
        """
        if sys.platform == 'win32':
            return lib.hardware.sys_info_surveillance_win.get_process_memory_usage(self.pid)
        return 0

    @classmethod
    def create_current(cls):
        return SysInfoProcess(os.getpid(), 'Current', '')
        
    @classmethod
    def create_running_gon(cls):
        result = []
        if sys.platform == 'win32':
            for (pid, name, path) in lib.hardware.sys_info_surveillance_win.lookup_gon_process_info():
                result.append(SysInfoProcess(pid, name, path))
        return result


class SysInfoNetworkInterface(object):
    def __init__(self, interface_name, mac_address, bandwidth):
        self.interface_name = interface_name
        self.mac_address = mac_address
        self.bandwidth = bandwidth
        self._state = None
    
    def get_name(self):
        return self.interface_name
    
    def get_usage(self):
        bytes_received_per_sec = 0
        bytes_send_per_sec = 0
        if sys.platform == 'win32':
            ((bytes_received_per_sec, bytes_send_per_sec), self._state) =  lib.hardware.sys_info_surveillance_win.get_network_interface_usage(self.interface_name, self._state)
        return (bytes_received_per_sec, bytes_send_per_sec)
        
    @classmethod
    def create_all(cls):
        result = []
        if sys.platform == 'win32':
            for (interface_name, mac_address, bandwidth) in lib.hardware.sys_info_surveillance_win.lookup_network_interfaces():
                result.append(SysInfoNetworkInterface(interface_name, mac_address, bandwidth))
        return result
    

if __name__ == '__main__':
    sys_info_system = SysInfoSystem.create()
    sys_info_process_gons = SysInfoProcess.create_running_gon()
    sys_info_interfaces = SysInfoNetworkInterface.create_all()
    
    import time
    for i in range(100):
        usage = []
        usage.append(sys_info_system.get_cpu_usage()) 
    
        for ni in sys_info_interfaces:
            usage.extend(ni.get_usage()) 
    
        for pi in sys_info_process_gons:
            usage.append( pi.get_memory_usage() ) 
            
        print usage
        time.sleep(1)
        