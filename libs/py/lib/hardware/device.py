"""
This module is a abstraction of devices where token can be found
"""
from __future__ import with_statement

import sys
import os
import os.path

if sys.platform == 'win32':
    SEM_FAILCRITICALERRORS = 0x0001
    SEM_NOGPFAULTERRORBOX = 0x0002
    SEM_NOALIGNMENTFAULTEXCEPT = 0x0004
    SEM_NOOPENFILEERRORBOX = 0x8000

class Device(object):
    
    @classmethod
    def get_device_list(cls):
        """
        Return a list of devices where token might be mounted
        """
        devices = []
        
        if sys.platform == 'win32':
            import win32file
            import win32api
            import string
            for drive in string.letters[len(string.letters)/2:]:
                win32api.SetErrorMode(SEM_FAILCRITICALERRORS or SEM_NOOPENFILEERRORBOX)
                if win32file.GetDriveType(drive+":")==win32file.DRIVE_REMOVABLE:
                    devices.append(drive+":\\")
        elif sys.platform == 'linux2':

            # Mount point fedore < 17
            try:
                device_root = '/media'
                for device in os.listdir(device_root):
                    device_abs = os.path.join(device_root, device)
                    if os.path.isdir(device_abs):
                        devices.append(device_abs)
            except:
                pass
                
            # Mount point fedore >= 17
            try:
                device_root = os.path.join('/run', 'media', os.getlogin())
                for device in os.listdir(device_root):
                    device_abs = os.path.join(device_root, device)
                    if os.path.isdir(device_abs):
                        devices.append(device_abs)
            except:
                pass
                
        elif sys.platform == 'darwin':
            device_root = '/Volumes'
            for device in os.listdir(device_root):
                device_abs = os.path.join(device_root, device)
                if os.path.isdir(device_abs):
                    devices.append(device_abs)
        return devices


    @classmethod
    def get_free_space(cls, device):
        """
        Return the number of bytes of free space of a given device. 
        If the information not is available then None is returnes
        """ 
        if sys.platform == 'win32':
           import win32file
           sectorsPerCluster, bytesPerSector, numFreeClusters, totalNumClusters = win32file.GetDiskFreeSpace(device)
           free_b = numFreeClusters * sectorsPerCluster * bytesPerSector 
           return free_b
        
        return None
