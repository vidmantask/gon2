"""
This module contains functionality for getting system usage on windows os.
"""
import sys
import os
import wmi
import win32con
import win32api
import win32process
import win32pdh
import ctypes

import pythoncom

class FILETIME(ctypes.Structure):
    _fields_ = [("dwLowDateTime", ctypes.c_ulong),
                ("dwHighDateTime", ctypes.c_ulong)]

    def to_int_64(self):
        return self.dwLowDateTime + (self.dwHighDateTime << 32 )


def _get_system_timers():
    idle_time = FILETIME()
    kernel_time = FILETIME()
    user_time = FILETIME()
    rc = ctypes.windll.kernel32.GetSystemTimes(ctypes.byref(idle_time), ctypes.byref(kernel_time), ctypes.byref(user_time))
    return (idle_time.to_int_64(), kernel_time.to_int_64(), user_time.to_int_64())


def calc_cpu_usage(before_info=None):
    if before_info is None:
        system_timers_before = _get_system_timers()
    else:
        system_timers_before = before_info['_system_timers_before']
    (idle_time_before, kernel_time_before, user_time_before) = system_timers_before
    (idle_time_now, kernel_time_now, user_time_now) = _get_system_timers()
    idle_time_diff = idle_time_now - idle_time_before 
    kernel_time_diff = kernel_time_now - kernel_time_before 
    user_time_diff = user_time_now - user_time_before
    cpu_usage = 0
    sys = kernel_time_diff + user_time_diff
    if sys > 0:
        cpu_usage = ((sys - idle_time_diff) * 100 ) / sys
    result = {}
    result['cpu_usage'] = cpu_usage
    result['_system_timers_before'] = (idle_time_now, kernel_time_now, user_time_now)
    return result


def lookup_gon_process_info(prefix='gon_'):
    pythoncom.CoInitialize()
    result = []
    for process in wmi.WMI().Win32_Process():
        if process.Name.startswith(prefix):
            result.append( (process.ProcessId, process.Name, process.ExecutablePath) )
    return result


def get_process_memory_usage(process_id):
    """
    Return the memory usage for a given pid. 
    """
    try:
        pythoncom.CoInitialize()
        process_handler = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, 0, process_id)
        process_mem_info = win32process.GetProcessMemoryInfo(process_handler)
        memory_usage = process_mem_info['WorkingSetSize']
        return memory_usage
    except:
        pass
    return 0


def get_process_memory_usage_peak(process_id):
    process_handler = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION, 0, process_id)
    process_mem_info = win32process.GetProcessMemoryInfo(process_handler)
    return process_mem_info['PeakWorkingSetSize']


def lookup_network_interfaces():
    pythoncom.CoInitialize()
    interface_counters, interface_names = win32pdh.EnumObjectItems(None, None, 'Network Interface', win32pdh.PERF_DETAIL_WIZARD)
    result = []
    for interface_name in interface_names:
        pdh_query = win32pdh.OpenQuery()
        cp_current_bandwith = win32pdh.MakeCounterPath( (None, 'Network Interface', interface_name, None, -1, 'Current Bandwidth') )
        hc_current_bandwith = win32pdh.AddCounter(pdh_query, cp_current_bandwith)
        try:
            win32pdh.CollectQueryData(pdh_query)
            (rc, current_bandwith) = win32pdh.GetFormattedCounterValue(hc_current_bandwith, win32pdh.PDH_FMT_LONG)
        except:
            current_bandwith = 0
        result.append( (interface_name, "", current_bandwith) )
    return result


def get_network_interface_usage(interface_name, state=None):
    if state is None:
        state = {}
        state['pdh_query'] = win32pdh.OpenQuery()
        state['cp_receive'] = win32pdh.MakeCounterPath( (None, 'Network Interface', interface_name, None, -1, 'Bytes Received/sec') )
        state['cp_send'] = win32pdh.MakeCounterPath( (None, 'Network Interface', interface_name, None, -1, 'Bytes Sent/sec') )
        state['hc_receive'] = win32pdh.AddCounter(state['pdh_query'], state['cp_receive'])
        state['hc_send'] = win32pdh.AddCounter(state['pdh_query'], state['cp_send'])
    try:
        win32pdh.CollectQueryData(state['pdh_query'])
        (rc, receive_bytes_sec) = win32pdh.GetFormattedCounterValue(state['hc_receive'], win32pdh.PDH_FMT_LONG)
        (rc, send_bytes_sec) = win32pdh.GetFormattedCounterValue(state['hc_send'], win32pdh.PDH_FMT_LONG)
    except:
        print "ERROR : ", sys.exc_info()
        receive_bytes_sec = 0
        send_bytes_sec = 0
    return ((receive_bytes_sec, send_bytes_sec), state)
