'''
Created on 12/05/2010

@author: pwl
'''
import sys
import ctypes

WSC_SECURITY_PROVIDER_FIREWALL = 1
WSC_SECURITY_PROVIDER_AUTOUPDATE_SETTINGS = 2
WSC_SECURITY_PROVIDER_ANTIVIRUS = 4
WSC_SECURITY_PROVIDER_ANTISPYWARE = 8
WSC_SECURITY_PROVIDER_INTERNET_SETTINGS = 16
WSC_SECURITY_PROVIDER_USER_ACCOUNT_CONTROL = 32
WSC_SECURITY_PROVIDER_SERVICE = 64

WSC_SECURITY_PROVIDER_NONE = 0
WSC_SECURITY_PROVIDER_ALL = WSC_SECURITY_PROVIDER_FIREWALL | \
                            WSC_SECURITY_PROVIDER_AUTOUPDATE_SETTINGS | \
                            WSC_SECURITY_PROVIDER_ANTIVIRUS | \
                            WSC_SECURITY_PROVIDER_ANTISPYWARE | \
                            WSC_SECURITY_PROVIDER_INTERNET_SETTINGS | \
                            WSC_SECURITY_PROVIDER_USER_ACCOUNT_CONTROL | \
                            WSC_SECURITY_PROVIDER_SERVICE

WSC_SECURITY_PROVIDER_HEALTH_GOOD = 0
WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED = 1
WSC_SECURITY_PROVIDER_HEALTH_POOR = 2
WSC_SECURITY_PROVIDER_HEALTH_SNOOZE = 3

wsc_value_title_map = {
                      WSC_SECURITY_PROVIDER_HEALTH_GOOD : "Good",  
                      WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED: "Not Monitored",
                      WSC_SECURITY_PROVIDER_HEALTH_POOR: "Poor" ,
                      WSC_SECURITY_PROVIDER_HEALTH_SNOOZE: "Unknown" ,
                      
                    }


wsc_property_map = {
                      "firewall" : WSC_SECURITY_PROVIDER_FIREWALL,
                      "antivirus" : WSC_SECURITY_PROVIDER_ANTIVIRUS,
                      "antispyware" : WSC_SECURITY_PROVIDER_ANTISPYWARE,
                      "windows_update" : WSC_SECURITY_PROVIDER_AUTOUPDATE_SETTINGS,
                      
                    }


wsc_property_title_map = {
                      WSC_SECURITY_PROVIDER_FIREWALL : "Firewall",  
                      WSC_SECURITY_PROVIDER_ANTIVIRUS: "Antivirus",
                      WSC_SECURITY_PROVIDER_ANTISPYWARE: "Antispyware" ,
                      WSC_SECURITY_PROVIDER_AUTOUPDATE_SETTINGS: "Auto Update" ,
                      
                    }



def get_security_all_center_status():
    security_provider = WSC_SECURITY_PROVIDER_FIREWALL
    result_dict = dict()
    while security_provider <= 64:
        result = get_security_center_status(security_provider)
        result_dict [security_provider] = result
        security_provider = security_provider*2
    
    security_provider = WSC_SECURITY_PROVIDER_ALL
    result = get_security_center_status(security_provider)
    result_dict [security_provider] = result
    return result_dict
    
        

def get_security_center_status(security_provider):
    if sys.platform != 'win32':
        raise Exception("This method is only available on Windows")
    
    import ctypes.wintypes as wintypes
    
    provider = wintypes.DWORD(security_provider) 
    health = wintypes.BYTE()
    hres = ctypes.windll.wscapi.WscGetSecurityProviderHealth(provider, ctypes.byref(health))
    return health.value
    

