
import httplib, urllib
import base64
import zlib

import gzip
import StringIO


HINT = u'jpxcite120'

def encode_qr_data(servers, knownsecret):
    data = ':,:'.join([HINT, servers, knownsecret])
    data_file = StringIO.StringIO()
    fileObj = gzip.GzipFile(fileobj=data_file, mode='wb');
    fileObj.write(data)
    fileObj.close()
    return base64.b64encode(data_file.getvalue())

def generate_qr_image(image_filename, data):
    google_chart_args = urllib.urlencode( {'chs':'400x400', 'cht':'qr', 'chld':'H', 'chl':data} )
    headers = {"Content-type": "application/x-www-form-urlencoded",  "Accept": "text/plain"}

    
    try:
        conn = httplib.HTTPConnection("chart.apis.google.com")
        conn.request("POSt", "/chart", google_chart_args, headers)
        response = conn.getresponse()
        if response.status != 200:
            return (False, response.status)
        data = response.read()
        image_file = open(image_filename, 'wb')
        image_file.write(data)
        image_file.close()
        conn.close()
        return (True, 0)
    except:
        print "Use the following properly to generate qr-image needed for mobile:"
        print "http://chart.apis.google.com/chart"
        print google_chart_args
        print headers
        return (False, 1001)
        
        
QR_HTML_TEMPLATE = """
<html>
  <head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
</head>
<body>
  <img src="http://chart.apis.google.com/chart?%s">
</body>
</html>
"""
def generate_qr_html(filename, data):
    try:
        google_chart_args = urllib.urlencode( {'chs':'400x400', 'cht':'qr', 'chld':'H', 'chl':data} )
        image_file = open(filename, 'wb')
        image_file.write(QR_HTML_TEMPLATE % google_chart_args)
        image_file.close()
        return (True, 0)
    except:
        return (False, 1002)


#servers = "<?xml version ='1.0'?><servers><connection_group title='Group_1' selection_delay_sec='2'><connection title='Connection_03' host='10.0.2.2' port='13945' timeout_sec='60' type='direct' /><connection title='Connection_01' host='192.168.1.213' port='13945' timeout_sec='60' type='direct' /></connection_group></servers>"
#knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F"
#data_qr = encode_qr_data(servers, knownsecret)
#
#print base64.b64encode(data_qr)
#image_filename = "/home/thwang/test.png"
#generate_qr_image(image_filename, data_qr)  
#
#html_filename = "/home/thwang/test.html"
#generate_qr_html(html_filename, data_qr)

#http://chart.apis.google.com/chart?cht=p3&chs=250x100&chd=t:60,40&chl=Hello|World

#http://chart.apis.google.com/chart?chl=TESTDATA&chs=400x400&cht=qr&chld=H
#
#google_chart_args = urllib.urlencode( {'chs':'400x400', 'cht':'qr', 'chld':'H', 'chl':'TESTDATA'} )
#headers = {"Content-type": "application/x-www-form-urlencoded",  "Accept": "text/plain"}
#
#conn = httplib.HTTPConnection("chart.apis.google.com")
#print google_chart_args
#conn.request("POST", "/chart", google_chart_args, headers)
#response = conn.getresponse()
#print response.status, response.reason
#data = response.read()
#print data
#http://chart.apis.google.com/chart
#   ?chs=200x200
#   &cht=qr
#   &chld=H
#   &chl=TESTDATAhfhgfhfh
   
#   
#   
#image_file = open(image_filename, 'w')
#image_file.write(data)
#image_file.close()
#
#conn.close()

#