"""
Unittest of cryptfacility lib
"""
from __future__ import with_statement
import unittest
import time

from lib import giri_unittest

from lib import cryptfacility


class CF_pk_signing_and_validation(unittest.TestCase):
   
    def test_pk_signing_and_validation(self):
        key_pair_1 = cryptfacility.pk_generate_keys()
        key_pair_2 = cryptfacility.pk_generate_keys()

        challenge_1 = "Hej 1"
        challenge_2 = "Hej 2"
        
        signature_challenge_1 = cryptfacility.pk_sign_challenge(key_pair_1[1], challenge_1)
        signature_challenge_2 = cryptfacility.pk_sign_challenge(key_pair_2[1], challenge_2)
        
        print key_pair_1[0]
        print key_pair_1[1]
        print challenge_1
        print signature_challenge_1
        
        self.assertEqual(cryptfacility.pk_verify_challenge(key_pair_1[0], challenge_1, signature_challenge_1), True)
        self.assertEqual(cryptfacility.pk_verify_challenge(key_pair_2[0], challenge_2, signature_challenge_2), True)
        self.assertEqual(cryptfacility.pk_verify_challenge(key_pair_2[0], challenge_1, signature_challenge_1), False)
        self.assertEqual(cryptfacility.pk_verify_challenge(key_pair_2[0], challenge_2, signature_challenge_1), False)


    def test_pk_signing_and_validation_rsa(self):
        print "RSA, begin"
        key_pair_1 = cryptfacility.pk_generate_keys_rsa()
        key_pair_2 = cryptfacility.pk_generate_keys_rsa()

        challenge_1 = "Hej 1"
        challenge_2 = "Hej 2"
        
        signature_challenge_1 = cryptfacility.pk_sign_challenge_rsa(key_pair_1[1], challenge_1)
        signature_challenge_2 = cryptfacility.pk_sign_challenge_rsa(key_pair_2[1], challenge_2)
        
        print key_pair_1[0]
        print key_pair_1[1]
        print challenge_1
        print signature_challenge_1
        
        self.assertEqual(cryptfacility.pk_verify_challenge_rsa(key_pair_1[0], challenge_1, signature_challenge_1), True)
        self.assertEqual(cryptfacility.pk_verify_challenge_rsa(key_pair_2[0], challenge_2, signature_challenge_2), True)
        self.assertEqual(cryptfacility.pk_verify_challenge_rsa(key_pair_2[0], challenge_1, signature_challenge_1), False)
        self.assertEqual(cryptfacility.pk_verify_challenge_rsa(key_pair_2[0], challenge_2, signature_challenge_1), False)

        print "RSA, end"


class CF_known_secret_generation(unittest.TestCase):

    def test_generation(self):
        secrets = cryptfacility.generate_secrets()
        print secrets[0]
        print secrets[1]
        self.assertNotEqual(secrets[0], secrets[1])

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
