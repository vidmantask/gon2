import binascii

try:
    import sspi as sspimod
    import sspicon
    import pywintypes
except ImportError:
    sspimod = None

import ntlm_client

def base64(s):
    return binascii.b2a_base64(s).replace('\n', '')

# http://tools.ietf.org/html/rfc2616.html#section-10.4.2 401 Unauthorized
# http://tools.ietf.org/html/rfc2616.html#section-10.4.8 407 Proxy Authentication Required
# http://tools.ietf.org/html/rfc2616.html#section-14.33 Proxy-Authenticate
# http://tools.ietf.org/html/rfc2616.html#section-14.47 WWW-Authenticate
http_response_field = {'401': 'WWW-Authenticate', '407': 'Proxy-Authenticate'}
# http://tools.ietf.org/html/rfc2616.html#section-14.8 Authorization
# http://tools.ietf.org/html/rfc2616.html#section-14.34 Proxy-Authorization
http_request_field = {'401': 'Authorization', '407': 'Proxy-Authorization'}

class Auth(object):

    def __init__(self, env, username, password, sspi=False):
        self._env = env
        self._username = username
        self._password = password
        self._basic = username and password
        self._ntlm = username and password
        self._sspi = sspimod and sspi
        self._kind = None
        self._current = None # (host, path, challenge)
        self._authstate = None
        self._authcount = 0

    def _reset(self):
        self._kind = None
        self._current = None
        self._authstate = None
        self._authcount = 0

    def initialize(self, req):
        # only when new connection not created yet - we don't want to mess things up if connection already has been authenticated
        self._reset() # new connection -> new state
        #print 'initialize', req.authkind
        if not req.authkind:
            self._kind = 'UNAUTH'
            return
        if req.authkind != '*':
            self._kind = req.authkind.upper()
            assert self._kind in ['BASIC', 'NTLM'], req.authkind
            self._authcount = 1
            reuse_auth = self._feed('401' if req.path.startswith('/') else '407', '') # only auth to next step - either server or proxy
            if reuse_auth:
                authfield, authvalue, challenge = reuse_auth
                self._env.log2('seeding auth with %s: %s', authfield, authvalue)
                req.headers.replace(authfield, authvalue)
                #print (authfield, authvalue)
                self._current = (req.host, req.path, challenge)
            else:
                self._reset() # forget experiment
        #print 'self._kind', self._kind

    def react(self, code, resp_headers, req):
        '''Returns True if response requested auth and req_headers has been modified and should be refetched'''
        if self._kind == 'UNAUTH':
            self._env.log2('unauthenticated')
            return False
        if code not in http_response_field: # success, no need for further auth
            self._reset()
            return False
        if self._current and self._current[:2] != (req.host, req.path): # gave up on previous request and sent new
            self._env.log1('%s auth for %s interrupted by %s', self._kind, self._current[1], req.path)
            self._reset() # continue new auth session
        challenges = {}
        for authresp in resp_headers.getvalues(http_response_field[code], []):
            kind, param = (authresp + ' ').split(' ', 1)
            challenges[kind.upper()] = param
        if self._current == (req.host, req.path, challenges.get(self._kind)):
            self._env.log1('%s auth loop detected after %s auth retries', self._kind, self._authcount)
            self._reset() # remove old auth header from req?
            return False
        self._authcount += 1
        if self._authcount > 5:
            self._env.log1('%s auth failed %s times', self._kind, self._authcount)
            self._reset() # remove old auth header from req?
            return False
        if not self._kind: # assert self._authcount == 1
            if 'NEGOTIATE' in challenges and self._sspi:
                self._kind = 'NEGOTIATE'
            elif 'NTLM' in challenges and self._ntlm:
                self._kind = 'NTLM'
            elif 'BASIC' in challenges and self._basic:
                self._kind = 'BASIC'
            else:
                self._env.log1('no available auth method found among %s', ', '.join(challenges.keys()))
                self._reset()
                return False
            #print 'challenges', challenges, '->', self._kind
        params = challenges.get(self._kind) # isn't None
        self._current = (req.host, req.path, params)
        next_auth = self._feed(code, params)
        self._env.log2('next_auth %s', next_auth)
        if next_auth:
            authfield, authvalue, _challenge = next_auth
            #print self._pending_req.headers
            req.headers.replace(authfield, authvalue)
            return True
        self._reset()
        return False

    def _feed(self, code, params):
        request_field = http_request_field[code]
        # Note: Use of auth injection could/should be restricted to specific paths
        if self._kind == 'BASIC':
            if self._authcount > 1:
                # it is the same request and we assume we got the same challenge (realm) back
                self._env.log1('%s using Basic auth for %s failed', self, self._username)
                return
            self._env.log1('%s got code %s - using Basic auth', self, code)
            return (request_field,
                    'Basic %s' % base64(
                            ('%s:%s' % (self._username, self._password)).encode('utf-8')),
                    (code, self._kind, params),
                    )
        if self._kind == 'NEGOTIATE':
            data = binascii.a2b_base64(params or '')
            if self._authcount == 1:
                self._authstate = sspimod.ClientAuth("negotiate")
            try:
                err, out_buf = self._authstate.authorize(data)
            except pywintypes.error, _e:
                #print 'sending Negotiation error: %s' % _e.strerror
                return
            if err not in [0, sspicon.SEC_I_CONTINUE_NEEDED]:
                #print 'sending Negotiation error status: %s' % err
                return
            #print 'sending %s: Negotiate ...' % req_field[response_status]
            return (request_field,
                    'Negotiate ' + base64(out_buf[0].Buffer),
                    (code, self._kind, params) if self._authcount == 1 else None)
        if self._kind == 'NTLM':
            if self._authcount == 1:
                if params:
                    return # this wasn't a new ntlm offer
                username = self._username
                domainname = ''
                if '\\' in username:
                    domainname, username = username.split('\\', 1)
                self._authstate = ntlm_client.NTLM_Client(username, domainname,
                        self._password, workstation='python')
                ntlm0 = 'NTLM %s' % base64(self._authstate.make_ntlm_negotiate())
                return (request_field, ntlm0, (code, self._kind, params))
            else:
                if not params:
                    return # this was apparently a new offer - we don't want that now
                ntlm1 = binascii.a2b_base64(params)
                self._authstate.parse_ntlm_challenge(ntlm1)
                ntlm2 = 'NTLM %s' % base64(self._authstate.make_ntlm_authenticate())
                return (request_field, ntlm2, None)
        assert False, self._kind


import unittest
import mime

class Tests(unittest.TestCase):

    def setUp(self):
        self.env = self.Env()

    class Env:
        def log1(self, *a): pass # print ('log1',) + a
        def log2(self, *a): pass # print ('log2',) + a
        def log3(self, *a): pass # print ('log3',) + a

    class HttpRequest(object):
        def __init__(self, headers, path=None, host=None, authkind=None):
            self.headers = headers
            self.host = host
            self.path = path
            self.authkind = authkind

    def testBasic(self):
        auth = Auth(self.env, 'login', 'password')

        req = self.HttpRequest(headers=mime.Headers('foo'), host='host', path='/path', authkind='*')
        auth.initialize(req)
        self.assertEqual(req.headers.items(), [])
        self.assertTrue(auth.react('401', mime.Headers(items=[('WWW-Authenticate', 'basic')]), req))
        self.assertEqual(req.headers.items(), [('Authorization', 'Basic bG9naW46cGFzc3dvcmQ=')])
        self.assertFalse(auth.react('401', mime.Headers(items=[('WWW-Authenticate', 'basic')]), req))
        # auth state was reset, and next challenge will be accepted
        self.assertTrue(auth.react('401', mime.Headers(items=[('WWW-Authenticate', 'basic')]), req))

        self.assertFalse(auth.react('200', mime.Headers(items=[('WWW-Authenticate', 'basic')]), req))

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='/path', authkind='basic')
        auth.initialize(req)
        self.assertEqual(req.headers.items(), [('Authorization', 'Basic bG9naW46cGFzc3dvcmQ=')])

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='/path')
        auth.initialize(req)
        self.assertEqual(req.headers.items(), [])


    def testNtlm(self):
        auth = Auth(self.env, 'login', 'password')

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='/path/')
        self.assertTrue(auth.react('407', mime.Headers(items=[('Proxy-Authenticate', 'ntlm')]), req))
        (f, v), = req.headers.items()
        self.assertEqual((f, v[:21]), ('Proxy-Authorization', 'NTLM TlRMTVNTUAABAAAA'))
        self.assertTrue(auth.react('407',
                mime.Headers(items=[('Proxy-Authenticate', 'ntlm TlRMTVNTUAACAAAACAAIADgAAAAFgomi7um4MyFow2sAAAAAAAAAALQAtABAAAAABgGxHQAAAA9EAEUATQBPAAIACABEAEUATQBPAAEAEABUAFIAQQBJAE4AMQAwADQABAAiAGQAZQBtAG8ALgBnAGkAcgBpAHQAZQBjAGgALgBjAG8AbQADADQAdAByAGEAaQBuADEAMAA0AC4AZABlAG0AbwAuAGcAaQByAGkAdABlAGMAaAAuAGMAbwBtAAUAIgBkAGUAbQBvAC4AZwBpAHIAaQB0AGUAYwBoAC4AYwBvAG0ABwAIAGe3LFOM0MwBAAAAAA==')]),
                req))
        (f, v), = req.headers.items()
        self.assertEqual((f, v[:21]), ('Proxy-Authorization', 'NTLM TlRMTVNTUAADAAAA'))
        self.assertFalse(auth.react('302',
                mime.Headers(items=[('Proxy-Authenticate', 'ignored!!!')]),
                req))

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='/something')
        auth.initialize(req)
        self.assertFalse(req.headers.items())

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='http://host/path/something', authkind='Ntlm')
        auth.initialize(req)
        (f, v), = req.headers.items()
        self.assertEqual((f, v[:21]), ('Proxy-Authorization', 'NTLM TlRMTVNTUAABAAAA'))

        req = self.HttpRequest(headers=mime.Headers(), host='host', path='/path/something', authkind='Ntlm')
        auth.initialize(req)
        (f, v), = req.headers.items()
        self.assertEqual((f, v[:21]), ('Authorization', 'NTLM TlRMTVNTUAABAAAA'))
