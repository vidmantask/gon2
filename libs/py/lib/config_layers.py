"""
Configuration in layers

Usage example:
    layers_config = '  demo/TYPE core/TYPE/hack core/TYPE'
    cls = ConfigLayers(layers_config.split())
    cls_templates = cls.config_type('test')
    for name in cls_templates.listdir(''):
        print cls_templates.locate(name)
"""

import os

class ConfigLayers(object):
    
    def __init__(self, layer_specs):
        """
        Create handler for the layer configuration used on this system.
        
        >>> cls = ConfigLayers(['/tmp/hack/TYPE', 'demo/TYPE/per', 'demo/TYPE/poul', 'core/TYPE'])
        """
        self.layer_specs = layer_specs
        
    def config_type(self, config_type_name):
        """
        Create ConfigType with this name.
        
        >>> cls_test = cls.config_type('test')
        """
        return ConfigType(self.layer_specs, config_type_name)
    
class ConfigType(object):
    
    def __init__(self, layer_specs, config_type_name):
        """
        Create handler for a certain use; corresponds to a directory distributed over the layers.
        
        config_type_name is what distinguishes this ConfigType from others. Usually just a name, but can be foo/bar/baz
        
        >>> cls_test = ConfigType(layer_specs, 'test')
        """
        self.config_type_name = config_type_name
        self.layer_paths = ['/'.join((config_type_name if x == 'TYPE' else x) for x in ls.replace('\\', '/').split('/')) for ls in layer_specs]
        
    def locate(self, filename):
        """
        Return path to first / most custom instance of filename.
        If file not found then the path to a non-existing file is returned (the last location checked).  
        """
        for layer_path in self.layer_paths:
            fullfilename = layer_path + '/' + filename
            if os.path.exists(fullfilename):
                break
        return fullfilename 

    def listdir(self, dirname):
        """
        Return filename set for which locate "dirname/filename" will give the name of an existing file. 
        dirname may be empty string.
        """
        filenames = set()
        for layer_path in self.layer_paths:
            fulldirname = layer_path + '/' + dirname
            if os.path.exists(fulldirname):
                filenames.update(os.listdir(fulldirname))
        return filenames 

def _test():
    """
    mkdir core
    mkdir core/test
    touch core/test/foo
    touch core/test/xyzzy
    mkdir core/test/hack
    touch core/test/hack/zap
    mkdir core/test/hack/ping
    mkdir core/test/hack/ping/ping
    touch core/test/hack/ping/ping/pong
    mkdir demo
    mkdir demo/test
    touch demo/test/foo
    touch demo/test/bar
    """
    layers_config = '  demo/TYPE core/TYPE/hack core/TYPE'
    cls = ConfigLayers(layers_config.split())
    cls_test = cls.config_type('test')
    r = cls_test.locate('foo'); assert r == 'demo/test/foo', r # customized file
    r = cls_test.locate('bar'); assert r == 'demo/test/bar', r # custom only file
    r = cls_test.locate('xyzzy'); assert r == 'core/test/xyzzy', r # uncustom file
    r = cls_test.locate('ping/ping'); assert r == 'core/test/hack/ping/ping', r # dir and with path
    r = cls_test.locate('samarkand'); assert r == 'core/test/samarkand', r # doesn't exist
    r = cls_test.listdir(''); assert r == set(('foo', 'xyzzy', 'hack', 'zap', 'ping', 'bar')), r # merge all together, empty
    r = cls_test.listdir('ping'); assert r == set(('ping', )), r # dir in dir
    r = cls_test.listdir('ping/ping'); assert r == set(('pong', )), r # file in dir
    r = cls_test.listdir('ping/ping/ping'); assert r == set(), r # doesn't exist

if __name__ == '__main__':
    _test()
