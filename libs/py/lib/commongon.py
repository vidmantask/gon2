""" common objects and methods """
import os.path
from datetime import datetime, time, date


dtformat = "%Y-%m-%dT%H:%M:%S"
tformat = "%H:%M:%S"
dformat = "%Y-%m-%d"

def datetime2str(dt):
    return datetime.strftime(dt, dtformat)
   
def str2datetime(str):
    return datetime.strptime(str, dtformat)
    
def time2str(dt):
    return time.strftime(dt, tformat)
   
def str2time(str):
    date = datetime.strptime(str, tformat)
    return date.time()

def date2str(dt):
    return date.strftime(dt, dformat)
   
def str2date(str):
    date = datetime.strptime(str, dformat)
    return date.date()

def common_path_sufix(path, start):
    relpath = os.path.normpath(path)
    relstart = os.path.normpath(start)
    
    common_prefix = os.path.commonprefix([relpath, relstart])
    if not os.path.exists(common_prefix):
        common_prefix = os.path.split(common_prefix)[0]

    result = relpath[len(common_prefix):]
    if result.startswith('\\') or result.startswith('/'):
        result = result[1:]
    return result
    
# Converts a string of type "a=1, b=2, c=3" to a dict {'a':1, 'b':2, 'c':3}
def string_to_dict(string_list):
    result = dict()
    string_list = [option.strip() for option in string_list.split(",")]
    for option in string_list:
        key, eq, value = option.partition("=")
        key = key.strip()
        value = value.strip()
        if key and value:
            result[key] = value
    return result
        
        
        

    
if __name__ == '__main__':
    n = datetime.now()
    a = datetime2str(n)
    b = str2datetime(a)
    print n
    print a
    print b
    
    assert str(str2datetime(datetime2str(b))) == str(str2datetime(a)) 