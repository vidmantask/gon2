from __future__ import with_statement

import sys
import os.path
import checkpoint

import unittest
from lib import giri_unittest


class CheckpointUnittest(unittest.TestCase):

    def generate_checkpoints(self, checkpoint_handler):
        checkpoint_handler.Checkpoint("point_a", "TestModule", checkpoint.ERROR, my_attr_a="my_value_a")
        with checkpoint_handler.CheckpointScope("point_b", "TestModule", checkpoint.DEBUG, my_attr_b="my_value_b") as charlie:
            checkpoint_handler.Checkpoint("point_c", "TestModule", checkpoint.WARNING, my_attr_c="my_value_c", my_unicode_a=u'davs')
            charlie.add_complete_attr(foo='bar')
            charlie.add_complete_attr(foo_bar='oh', xyzzy='yeah', my_unicode_b=u'hej')

    def test_basis(self):
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler);
    
        self.generate_checkpoints(checkpoint_handler)
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
        try:
            checkpoint_handler.Checkpoint( "point_a", "TestModule", checkpoint.DEBUG, checkpoint.DEBUG)
            self.fail("argument error expected")
        except TypeError:
            pass
    
    def test_basis_suggar(self):
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler);
    
        checkpoint_handler.Checkpoint("point_a", "TestModule", checkpoint.WARNING, my_attr_a="my_value_a")
        with checkpoint_handler.CheckpointScope("point_b", "TestModule", checkpoint.DEBUG, my_attr_b=42):
            checkpoint_handler.Checkpoint("point_c", "TestModule", checkpoint.ERROR, my_attr_c="my_value_c")
    
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_filter_checkpoint_ids(self):
        ids = ["point_a", "point_b"]
    
        checkpoint_filter = checkpoint.CheckpointFilter_checkpoint_ids(ids)
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler);
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_filter_skip_complete(self):
        checkpoint_filter = checkpoint.CheckpointFilter_skip_complete();
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler);
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_filter_accept_if_attr_exist(self):
        checkpoint_filter = checkpoint.CheckpointFilter_accept_if_attr_exist("type", checkpoint.ERROR)
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler);
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_filter_or(self):
        checkpoint_filter_a = checkpoint.CheckpointFilter_accept_if_attr_exist("type", checkpoint.ERROR)
        checkpoint_filter_b = checkpoint.CheckpointFilter_accept_if_attr_exist("type", checkpoint.WARNING)
        checkpoint_filter = checkpoint.CheckpointFilter_or(checkpoint_filter_a, checkpoint_filter_b)
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler);
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_filter_not(self):
        checkpoint_filter_a = checkpoint.CheckpointFilter_accept_if_attr_exist("type", checkpoint.WARNING)
        checkpoint_filter = checkpoint.CheckpointFilter_not(checkpoint_filter_a)
        checkpoint_output_test_handler = checkpoint.CheckpointOutputHandler_Test()
        checkpoint_output_handler = checkpoint_output_test_handler.get_CheckpointOutputHandler()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler);
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_a") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b") );
        self.assertFalse( checkpoint_output_test_handler.got_checkpoint_id("point_c") );
        self.assertTrue( checkpoint_output_test_handler.got_checkpoint_id("point_b_complete") );
    
    def test_output_handler_xml(self):
        print 'Writing foo.xml:'
        temp_folder = giri_unittest.mkdtemp()
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerXML(temp_folder, False)
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
        self.generate_checkpoints(checkpoint_handler)
    
    def test_output_handler_text(self):
        print 'Text output:'
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerTEXT()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
    
        self.generate_checkpoints(checkpoint_handler)
        checkpoint_handler.Checkpoint("point_d", "TestModule", checkpoint.DEBUG, my_attr_d="my_value_d")
    
    def test_output_handler_callback(self):
    
        class MyCallback(checkpoint.CheckpointOutputHandlerCallback):
            def __init__(self):
                checkpoint.CheckpointOutputHandlerCallback.__init__(self)
            def handle_output(self, checkpoint_id, attrs, structure):
                print "MyCallback checkpoint_id: ", checkpoint_id, attrs, structure
        
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler_callback = MyCallback()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerCallbackWrapper(checkpoint_output_handler_callback)
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)
        self.generate_checkpoints(checkpoint_handler)
        
    def test_output_handler_folder_xml(self):
        temp_folder = giri_unittest.mkdtemp()
        checkpoint_handler = checkpoint.CheckpointHandler.get_handler_to_xml_folder(True, temp_folder)

        self.generate_checkpoints(checkpoint_handler)
        try:
            test_that_fails = this_is_not_known
        except:
            checkpoint_handler.CheckpointExceptionCurrent("test_output_handler_folder_xml", "Unittest")

#
#basis()
#basis_suggar()
#filter_checkpoint_ids()
#filter_skip_complete()
#filter_accept_if_attr_exist()
#filter_or()
#filter_not()
#output_handler_xml()
#output_handler_text()
#output_handler_callback()
#

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
