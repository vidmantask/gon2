"""
Commmen functionality for handling tokens
"""
import os
import os.path
import tempfile

import lib.checkpoint
import lib.image

MODULE_ID = 'lib.config'


SERVERS_EMPTY =  """<?xml version='1.0'?>
<servers version="0">
</servers>
"""

SERVERS_TEMPLATE = """<?xml version='1.0'?>
<servers version="0">
  <connection_group title='Default' selection_delay_sec='1'>
%s
  </connection_group>
</servers>
"""
SERVERS_CONNECTION_DIRECT_TEMPLATE = "<connection title='Direct' host='%s' port='%d' timeout_sec='30' type='direct'/>\n"
SERVERS_CONNECTION_HTTP_TEMPLATE   = "<connection title='HTTP' host='%s' port='%d' timeout_sec='30' type='http'/>\n"


def parse_servers(checkpoint_handler, servers_string):
    if servers_string is None:
        return SERVERS_EMPTY

    if isinstance(servers_string, unicode):
        servers_string = servers_string.encode('utf8', 'strict')
    
    if servers_string.startswith("<?xml"):
        return servers_string
    
    #
    # Old version of servers, from < 5.3
    #
    connections = ""
    for server_string_line in servers_string.splitlines():
        if server_string_line != "" and not server_string_line.startswith('#'):
            try:
                server_string_line_parts = server_string_line.split(',')
                server_ip = server_string_line_parts[0].strip()
                server_port = int(server_string_line_parts[1].strip())
                connections += SERVERS_CONNECTION_DIRECT_TEMPLATE % (server_ip, server_port)
            except:
                checkpoint_handler.Checkpoint("parse_servers.error", MODULE_ID, lib.checkpoint.ERROR, server_string=server_string_line)

    if connections != "":
        return SERVERS_TEMPLATE % connections
    return SERVERS_EMPTY


def _generate_connections(public_addresses, public_ports, toh_enabled, toh_public_ports):
    connections = []
    for public_address in public_addresses:
        for public_port in public_ports:
            connections.append( (public_address, public_port, True) )
        if toh_enabled:
            for public_port in toh_public_ports:
                connections.append( (public_address, public_port, False) )
    return connections
    


def generate_servers(servers_filename, public_addresses, public_ports, toh_enabled=False, toh_public_ports=[]):
    servers = SERVERS_EMPTY

    connections = _generate_connections(public_addresses, public_ports, toh_enabled, toh_public_ports)
    connections_string = ""

    for (public_address, public_port, is_direct) in connections:
        if is_direct:
            connections_string += SERVERS_CONNECTION_DIRECT_TEMPLATE % (public_address, public_port)
        else:
            connections_string += SERVERS_CONNECTION_HTTP_TEMPLATE % (public_address, public_port)
    if connections_string != "":
        servers = SERVERS_TEMPLATE % connections_string

    if not os.path.exists(os.path.dirname(servers_filename)):
        os.makedirs(os.path.dirname(servers_filename))
    servers_file = open(servers_filename, 'wt')
    servers_file.write(servers)
    servers_file.close()


def generate_gon_firewall_whitelist(whitelist_filename, public_addresses, public_ports, toh_enabled=False, toh_public_ports=[]):
    whitelist = []
    connections = _generate_connections(public_addresses, public_ports, toh_enabled, toh_public_ports)
    for (public_address, public_port, is_direc) in connections:
        whitelist.append("%s %d" % (public_address, public_port))
    whitelist_file = open(whitelist_filename, 'wt')
    whitelist_file.write("\n".join(whitelist))
    whitelist_file.close()


def generate_gon_qr_image(checkpoint_handler, servers_filename, knownsecret_filename, qr_image_filename, dat_filename):
    if not os.path.exists(servers_filename):
        checkpoint_handler.Checkpoint("generate_gon_qr_image.file_not_found", MODULE_ID, lib.checkpoint.ERROR, servers_filename=servers_filename)
        return
    servers_file = open(servers_filename, 'r')
    servers = servers_file.read()
    servers_file.close()

    if not os.path.exists(knownsecret_filename):
        checkpoint_handler.Checkpoint("generate_gon_qr_image.file_not_found", MODULE_ID, lib.checkpoint.ERROR, knownsecret_filename=knownsecret_filename)
        return
    knownsecret_file = open(knownsecret_filename, 'r')
    knownsecret = knownsecret_file.read()
    knownsecret_file.close()

    qr_image_data = lib.image.encode_qr_data(servers, knownsecret)
    (is_ok, error_code) = lib.image.generate_qr_html(qr_image_filename, qr_image_data)
    if not is_ok:
        checkpoint_handler.Checkpoint("generate_gon_qr_image.not_generated", MODULE_ID, lib.checkpoint.ERROR, error_code=error_code)

    dat_file = open(dat_filename, 'wb')
    dat_file.write(qr_image_data)
    dat_file.close()
