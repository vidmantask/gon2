"""
Unittest of config lib
"""
import unittest
from lib import giri_unittest

import lib.config



class ConfigUnittest(unittest.TestCase):

    def test_old_format(self):

        servers_old_01 = """
127.0.0.1, 8001
#127.0.0.1, 8002
127.0.0.1, 8003
"""
        servers_spec = lib.config.parse_servers(None, servers_old_01)
        self.assertTrue(servers_spec.startswith('<?xml'))
    
    
    def test_new_format_unicode(self):
        servers_01 = u"""<?xml version='1.0'?>
<servers version="0">
  <connection_group title='Default' selection_delay_sec='2'>
    <connection title='Default' host='127.0.0.1' port='99' timeout_sec='15' type='direct'/>
  </connection_group>
</servers>
"""

        servers_spec = lib.config.parse_servers(None, servers_01)
        self.assertTrue( isinstance(servers_spec, basestring) )


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
