#!/usr/bin/env python

"""
ToH Mini Server
Giritech implementation of RFC-1149-ish implementation of tunneling TCP over HTTP
Copyright Giritech 2008-2010
"""

import ToHserver

class env:

    class config:
        MIN_WAITING_HTTP_CONNECTIONS = 1 # request more connections if less than this
        MAX_WAITING_HTTP_CONNECTIONS = 4 # if more than this number of connections then get rid of them
        IDLE_TIMEOUT = 60 # how often to wake up when there is nothing to do
        PING_INTERVAL = 30 # server must send ping back at least this often
        ACK_TIMEOUT = 1.0 # pending acks will be sent after this long
        RETRANS_TIMEOUT = 1.0 # unacked packages will be retransmitted after this long
        MAX_RETRANS = 20 # after retransmitting this many times the connection fails
        PERSISTENT_HTTP = True # attempt persistent connections - squid supports it from client to proxy
        TCP_RECV_BUFFER = 16384
        LISTEN_BACKLOG = 100 # how many TCP (HTTP) connections can be waiting, probably max 128
        MAX_RECEIVE_HEADER_LENGTH = 4096 # maximum received HTTP header length in bytes
        MAX_RECEIVE_CONTENT_LENGTH = 100000 # maximum received HTTP content length in bytes
        MAX_SEND_CONTENT_LENGTH = 50000 # soft limit maximum sent HTTP content length in bytes - content can however be as big as whole message
        MAX_OUT_OF_ORDER = 50 # maximum number of premature packages a TcpConnection will store
        MAX_UNACK = 20 # maximum number of unacked packages a TcpConnection will store before it throttles
        MAX_SESSIONS = 100
        MAX_TCP_PER_SESSION = 100 # maximum number of TcpConnections per Session
        MAX_IDLE_TCP = 600 # how long should a TcpConnection be allowed to be idle (perhaps because other end is dead)
        MAX_SESSION_SILENT = 60 # how long should a Session accept not to hear from client, should be longer than clients PING_INTERVAL
        MAX_IDLE_HTTP = 360 # how long should a HttpConnection be allowed to be idle (perhaps because other end is dead)
        MAX_ASYNC = 500 # windows select() has a hardcoded limit of 512 ...
        PACKAGE_DROP_INTERVAL = 0 # DESTRUCTIVE TESTING ONLY!

    @staticmethod
    def log1(s, *args):
        print '1:', s % args

    @staticmethod
    def log2(s, *args):
        print '2:', s % args

    @staticmethod
    def log3(s, *args):
        print '3:', s % args

    out_symbol = '<'
    in_symbol = '>'


def main():
    http_addr = ('0.0.0.0', 8080)
    target_addr = ('www.giritech.com', 80)

    toh = ToHserver.main()
    toh.run(env, http_addr, target_addr)
    # toh.stop() # from other thread

if __name__ == '__main__':
    main()
