"""
This file contains ...
"""


class ServiceBase(object):
    def __init__(self, name, display_name, description, main_function):
        self.name = name
        self.display_name = display_name
        self.description = description
        self.main_function = main_function
    
    def ServiceStop(self):
        raise NotImplementedError

    def ServiceStatus(self):
        raise NotImplementedError

    def ServiceRun(self):
        """
        Start _svc_function in a new thread with the stop_signal as the first argument.
        If the service is to stop sends a stop_signal.
        When _svc_function removes the stop_signal the service stops.
        """
        raise NotImplementedError


