"""
This file contains functionality for running a function as a deamon on linux
"""
import sys
import time
import signal
import os.path
import traceback
import optparse
import threading

import lib.appl.daemon


class ServiceCommandline:
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--service_start', action='store_true', help='Start server daemon')
        self._parser.add_option('--service_stop', action='store_true', help='Stop server daemon')
        self._parser.add_option('--service_restart', action='store_true', help='Restart server daemon')
        self._parser.add_option('--service_status', action='store_true', help='Get status of server daemon')
        self._parser.add_option('--instance_run_root', type='string')
        
        (self._options, self._args) = self._parser.parse_args()
    
    def get_remaining_args(self):
        return self._args
    
    def action_service_start(self):
        return self._options.service_start
    
    def action_service_stop(self):
        return self._options.service_stop
    
    def action_service_restart(self):
        return self._options.service_restart

    def action_service_status(self):
        return self._options.service_status
    
    def get_service_name(self): 
        #
        # /opt/giritech/instance/gon_server-5.6.0.4-1/gon_server_gateway_service/linux
        # ['', 'opt', 'giritech', 'instance', 'gon_server-5.6.0.4-1', 'gon_server_management_service', 'linux']
        #
        path_elements = self._options.instance_run_root.split(os.path.sep)
        if len(path_elements) > 4:
            service_name = '%s-%s' % (path_elements[4], path_elements[5])
        else:
            service_name = self._options.instance_run_root.replace(os.path.sep, '#')
        return service_name
    
    def get_instance_root(self):
        return self._options.instance_run_root

    def get_pid_filename(self):
        return os.path.join('/tmp', '%s.pid' % self.get_service_name())

    def get_stdout_filename(self):
        return os.path.join('/tmp', '%s.stdout' % self.get_service_name())

    def get_stderr_filename(self):
        return os.path.join('/tmp', '%s.stderr' % self.get_service_name())


def ServiceHandleCommandLine(service_class):
    stop_filename = None
    if len(sys.argv) > 1 and sys.argv[1] in ['--foreground_with_stop_file']:
        stop_filename = sys.argv[2]
    print "stop_filename", stop_filename 
    service_class().ServiceRun(stop_filename)



service_stop_signal =[]
service_stopped_signal=[]


def term_signal_handler(signum, frame):
    print "Service term signal"
    sys.stdout.flush()
    service_stop_signal.append(7)



class Service(lib.appl.daemon.Daemon):
    def __init__(self, service_commandline, main_function):
        lib.appl.daemon.Daemon.__init__(self, service_commandline.get_pid_filename(), stdout=service_commandline.get_stdout_filename(), stderr=service_commandline.get_stderr_filename())
        self.service_commandline = service_commandline
        self.main_function = main_function
        
    def run(self):
        print "Service(%s) running" %  self.service_commandline.get_service_name()
        try:
            signal.signal(signal.SIGTERM, term_signal_handler)
            main_thread = threading.Thread(target=self.main_function, args=(service_stop_signal, service_stopped_signal, self.service_commandline.get_service_name(), self.service_commandline.get_remaining_args(), self.service_commandline.get_instance_root()), name="Service")
            main_thread.start()
            while not service_stop_signal:
                time.sleep(2)
            main_thread.join()
        except :
            traceback.print_exc(file=sys.stdout)
            sys.stdout.flush()
        
        print "Service(%s) stopped" %  self.service_commandline.get_service_name()
        sys.stdout.flush()
        
    def action(self):
        if self.service_commandline.action_service_start():
            self.start()
        elif self.service_commandline.action_service_stop():
            self.stop()
        elif self.service_commandline.action_service_restart():
            self.restart()
        elif self.service_commandline.action_service_status():
            self.status()
    