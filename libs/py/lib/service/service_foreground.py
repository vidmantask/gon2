"""
This file contains implementation of service_base for running in foreground
"""
import sys
import time
import threading
import os.path
import traceback

import lib.service.service_base


def get_service(name, display_name, description, main_function):

    class ForegroundService(lib.service.service_base.ServiceBase):
        def __init__(self):
            lib.service.service_base.ServiceBase.__init__(self, name, display_name, description, main_function)
            self._continue = True
    
        def ServiceStop(self):
            self._continue = False
    
        def ServiceStatus(self):
            pass
    
        def ServiceRun(self, stop_filename=None):
            print "Service %s(%s) started" % (self.name, self.display_name)
            service_stop_signal = []
            service_function_stoped_signal = []
            main_thread = threading.Thread(target=self.main_function, args=(service_stop_signal, service_function_stoped_signal), name="Service")
            main_thread.start()
            try:
                while self._continue and not service_function_stoped_signal:
                    try:
                        if stop_filename is not None:
                            if not os.path.exists(stop_filename):
                                print "stop file not found", stop_filename
                                self.ServiceStop()
                        time.sleep(2)
                    except KeyboardInterrupt:
                        pass
            except:
                print traceback.print_exc()
            
            if not service_function_stoped_signal:    
                service_stop_signal.append(7)
                wait_count = 10
                while service_stop_signal and wait_count > 0:
                    try:
                        print "Service is stopping, please wait max %s sec." % wait_count
                        time.sleep(1)
                        wait_count =- 1
                    except:
                        break
                if wait_count > 0:
                    print "Service stopped very hard"
                    sys.exit()
            print "Service stopped"

    return ForegroundService
    

def ServiceHandleCommandLine(service_class):
    stop_filename = None
    if len(sys.argv) > 1 and sys.argv[1] in ['--foreground_with_stop_file']:
        stop_filename = sys.argv[2]
    print "stop_filename", stop_filename 
    service_class().ServiceRun(stop_filename)

