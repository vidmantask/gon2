"""
This file contains ...
"""
import win32serviceutil
import win32service
import win32event
import win32evtlogutil
import servicemanager
import win32api
import sys
import threading
import traceback
import time

import os
import os.path

import lib.service.service_base

def ServiceHandleCommandLine(service_class):
    if len(sys.argv) == 1:
        try:
            evtsrc_dll = os.path.abspath(servicemanager.__file__)
            servicemanager.PrepareToHostSingle(service_class)
            servicemanager.Initialize('service_class', evtsrc_dll)
            servicemanager.StartServiceCtrlDispatcher()
        except:
            servicemanager.LogErrorMsg(traceback.format_exc()) # if error print it to event log
            os._exit(-1)#return some value other than 0 to os so that service knows to restart
    elif len(sys.argv) > 1 and sys.argv[1] == '--foreground':
        service = service_class(sys.argv)
        service.main_function()
    else:
        win32serviceutil.HandleCommandLine(service_class)


def get_service(name, display_name, description, main_function):
    class WindowsService(win32serviceutil.ServiceFramework, lib.service.service_base.ServiceBase):
        _svc_name_ = name
        _svc_display_name_ = display_name
        _svc_description_ = description

        def __init__(self, args):
            win32serviceutil.ServiceFramework.__init__(self, args)
            lib.service.service_base.ServiceBase.__init__(self, name, display_name, description, main_function)
            self.hWaitStop = win32event.CreateEvent(None, 0, 0, None)
            self.worker = None

        def ServiceStop(self):
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self.name, 'ServiceStop'))
            self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
            win32event.SetEvent(self.hWaitStop)

        def ServiceStatus(self):
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self.name, 'ServiceStatus'))
            pass

        def ServiceRun(self):
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                                  servicemanager.PYS_SERVICE_STARTED,
                                  (self.name, 'ServiceRun'))
            service_stop_signal = []
            service_stopped_signal = []

            self.worker_thread = threading.Thread(target=self.main_function, args=(service_stop_signal, service_stopped_signal, self.name))
            if hasattr(sys, "frozen"):
                path = os.path.dirname(sys.executable)
                os.chdir(path)
            self.worker_thread.start()
            servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                                  servicemanager.PYS_SERVICE_STARTED,
                                  (self.name, ' - started'))

            i = 0
            while not service_stopped_signal:
                rc = win32event.WaitForSingleObject(self.hWaitStop, 3000)
                if rc == win32event.WAIT_OBJECT_0:
                    break # hWaitStop was set - stop service
                time.sleep(2)
                i += 1

            servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE,
                servicemanager.PYS_SERVICE_STOPPING,
                (self._svc_name_, " - shutting down")
                )

            service_stop_signal.append(7) # random number
            while not service_stopped_signal:
                win32api.Sleep(500)
            self.worker_thread.join()

            servicemanager.LogMsg(
                servicemanager.EVENTLOG_INFORMATION_TYPE,
                servicemanager.PYS_SERVICE_STOPPED,
                (self._svc_name_, " - stopped")
                )

        def SvcStop(self): # Called on "service stop"
            self.ServiceStop()

        def SvcDoRun(self):
            self.ServiceRun()

    return WindowsService
