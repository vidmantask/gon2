# TODO: http://www.and.org/texts/server-http

import mime

class HttpServerEngineCallbacks(object):
    """Handle engines response to the data fed to tcp_from_client"""

    def to_client(self, buf):
        """Engine has data to send to client"""
        raise NotImplementedError

    def got_http_request_start(self, method, path, protocol, headers):
        """Dispatched by engine from server"""
        raise NotImplementedError

    def got_http_request_fragment(self, chunk):
        """Dispatched by engine from server"""
        raise NotImplementedError

    def got_http_request_done(self):
        """Dispatched by engine from server"""
        raise NotImplementedError

    def got_tunnel_connect_request(self, headers):
        raise NotImplementedError

    def got_tunnel_fragment(self, chunk):
        raise NotImplementedError

    def will_listen(self, ready):
        """Hint whether tcp_from_client will accept data - default is True"""
        # server side only - client might try to pipeline requests, but server can't pipeline responses when we don't pipeline requests
        raise NotImplementedError

class SilentHttpClientEngineCallbacks(HttpServerEngineCallbacks):

    to_client = got_http_request_start = got_http_request_fragment = got_http_request_done = \
        got_tunnel_connect_request = got_tunnel_fragment = will_listen = lambda *a: None


class HttpServerEngine(object):
    """
    A connection from a client or proxy on http_socket
    Utilizes session_manager for handling of received requests and their reply
    """

    WAITING_FOR_REQUEST = 'WAITING_FOR_REQUEST' # Reading until \r\n\r\n in buffer
    STREAMING_REQUEST = 'STREAMING_REQUEST' # Reading until content_length in buffer
    WAITING_FOR_RESPONSE = 'WAITING_FOR_RESPONSE' # Waiting for callback on _send
    STREAMING_RESPONSE = 'STREAMING_RESPONSE' # Sending async streaming
    CONNECT = 'CONNECT' # Sending reply to CONNECT
    TUNNEL = 'TUNNEL' # Connected

    def __init__(self, env, callbacks):
        self._env = env
        assert isinstance(callbacks, HttpServerEngineCallbacks)
        self._callbacks = callbacks

        self._state = self.WAITING_FOR_REQUEST
        self._content_length = None
        self._keep_alive = None
        self._chunkify_response = None

    def _cancel(self):
        """Something irregular happened and we give up."""
        self._env.log3('%s cancel', self)
        self._state = None
        self._callbacks.will_listen(False)
        self._callbacks = SilentHttpClientEngineCallbacks()
        return None

    def tcp_from_client(self, buf):
        """Call when data received from client.
        No buf means EOF.
        Returns unused data from buf, None after error."""
        if not buf:
            if self._state is None:
                self._env.log2('%s received TCP close after close', self)
                return self._cancel()
            # RFC 2616 8.1.4: A client, server, or proxy MAY close the transport connection at any time.
            if self._state == self.WAITING_FOR_REQUEST:
                self._env.log3('%s received ok TCP close', self)
                return self._cancel() # Ignore silently that recv failed.
            else:
                self._env.log2('%s received half(?)-close in state %s', self, self._state)
                self._keep_alive = False # TODO: should we do more to record that?
                return '' # half-close is ok, we will try to respond anyway ... and it will fail if connection was fully closed
        self._env.log2('%s tcp_from_client %s bytes', self, len(buf))
        self._env.log3('%s tcp_from_client %r', self, buf)
        if self._state is None:
            self._env.log1('%s received data after close', self)
            return self._cancel()
        if self._state in [self.WAITING_FOR_RESPONSE, self.STREAMING_RESPONSE]:
            self._env.log2('%s received TCP while waiting in state %s for a reply to send - ignoring', self, self._state)
            return buf # someone should retry when response done - but that someone should have noticed that we wasn't listening and it shouldn't have been sent in the first place
        if self._state == self.WAITING_FOR_REQUEST:
            if not '\r\n\r\n' in buf: # FIXME: No reason to start searching from pos 0 every time - this is O(n2)
                if len(buf) > self._env.config.MAX_RECEIVE_HEADER_LENGTH:
                    self._env.log1('%s header too long - MAX_RECEIVE_HEADER_LENGTH is %s', self, self._env.config.MAX_RECEIVE_HEADER_LENGTH)
                    return self._cancel()
                return buf
            # Full header received - process it and go reading some content
            headers = mime.Headers()
            unused_data = headers.parse(buf)
            self._env.log3("%s got HTTP headers %s", self, headers.items())
            try:
                method, path, protocol = headers.start_line.split(' ', 3)
            except ValueError:
                self._env.log1('%s invalid start-line in request %r', self, headers.start_line)
                return self._cancel()
            method = method.upper()
            protocol = protocol.upper()
            connection_tokens = [x.strip() for x in headers.get('CONNECTION', '').upper().split(',') if x] # FIXME: could be in multiple headers ...
            if protocol == 'HTTP/1.1':
                self._keep_alive = 'CLOSE' not in connection_tokens # RFC 2616 8.1.2 and 14.10: Persistent unless explicit close
            elif protocol == 'HTTP/1.0': # RFC 2616 19.6.2 discusses problems with persistence in 1.0 and points to ...
                self._keep_alive = 'KEEP-ALIVE' in connection_tokens # RFC 2068 19.7.1.1
            else:
                self._env.log1('%s invalid http protocol %r', self, protocol)
                return self._cancel()
            if method == 'CONNECT':
                self._state = self.CONNECT # switch to TUNNEL once headers done
                self._callbacks.got_tunnel_connect_request(path)
            else:
                TE = headers.get('TRANSFER-ENCODING', 'identity').strip().upper()
                if TE != 'IDENTITY': # chunked requests are currently not supported
                    self._env.log1('%s can\'t handle Transfer-Encoding %r', self, TE)
                    return self._cancel()
                self._content_length = 0
                content_length_string = headers.get('CONTENT-LENGTH', '0')
                try:
                    self._content_length = max(int(content_length_string), 0)
                except ValueError, e:
                    self._env.log1('%s Invalid Content-Length %r: %s', self, content_length_string, e)
                    return self._cancel()
                self._state = self.STREAMING_REQUEST
                self._callbacks.got_http_request_start(method, path, protocol, headers)
                if not self._content_length:
                    self._content_length = None
                    self._state = self.WAITING_FOR_RESPONSE
                    self._callbacks.will_listen(False)
                    self._callbacks.got_http_request_done()
                    return unused_data
            if not unused_data:
                return ''
            buf = unused_data
        if self._state == self.STREAMING_REQUEST:
            assert buf
            l = min(len(buf), self._content_length)
            self._env.log3('%s got %s of %s bytes request', self, l, self._content_length)
            self._content_length -= l
            self._callbacks.got_http_request_fragment(buf[:l])
            if self._content_length:
                return buf[l:]
            self._env.log3('%s request done', self)
            self._state = self.WAITING_FOR_RESPONSE
            self._callbacks.will_listen(False)
            self._content_length = None
            self._callbacks.got_http_request_done()
            return buf[l:]
        elif self._state == self.TUNNEL:
            self._env.log3('%s got %s for tunnel', self, len(buf))
            self._callbacks.got_tunnel_fragment(buf)
            return ''
        assert False, (self._state, buf) # what happened here? data received in CONNECT?

    def send_http_error_response(self, err, msg, body, close=False):
        assert self._state in (self.WAITING_FOR_RESPONSE, self.CONNECT), self._state
        assert err and msg and body, (err, msg, body)
        headers = mime.Headers(start_line='HTTP/1.1 %s %s' % (err, msg), items=[('Content-Length', len(body))])
        self.send_http_response_start(headers)
        if body:
            self.send_http_response_next(body)
        self.send_http_response_done(close=close)

    def send_http_response(self, content=None, content_type=None, status='200', message='OK', close=False):
        """
        Callback from session with reply content
        This completes one HTTP request, and if the connection is persistent another request might come
        """
        assert self._state == self.WAITING_FOR_RESPONSE, self._state
        assert status
        assert isinstance(status, str) and status.isdigit() and len(status) == 3, status
        assert message
        # RFC 2616 6.1 FIXME: respond 1.1 to an 1.0 request? 10.2.1 defines "200 OK" for methods
        headers = mime.Headers(start_line='HTTP/1.1 %s %s' % (status, message))
        if not self._env.config.PERSISTENT_HTTP or not self._keep_alive or close:
            headers.replace('Connection', 'Close') # RFC 2616 8.1.2.1 SHOULD send Close if intention
            #'Server': 'ToH/%s' % shared.version, # RFC 2616 3.8
        headers.replace('Content-Length', len(content))
        # 'Transfer-Encoding': None
        if content_type:
            headers.replace('Content-Type', content_type)
        self.send_http_response_start(headers)
        if content:
            self.send_http_response_next(content)
        self.send_http_response_done(close)

    def send_http_response_start(self, headers, chunkify=False):
        # headers must match the chunkify flag
        self._env.log2('%s send_http_response_start state %s, chunkify=%s', self, self._state, chunkify)
        assert self._state in (self.WAITING_FOR_RESPONSE, self.CONNECT), self._state
        self._chunkify_response = chunkify
        chunk = headers.format()
        self._env.log3('%s enqueuing %r', self, chunk)
        self._state = self.STREAMING_RESPONSE # if it was CONNECT it is cancelled now
        self._callbacks.to_client(chunk)

    def send_http_response_next(self, chunk):
        self._env.log3('%s response chunk %s', self, len(chunk))
        assert self._state == self.STREAMING_RESPONSE, self._state
        if not chunk:
            self._env.log1('%s response length 0', self)
            return
        if self._chunkify_response:
            self._callbacks.to_client('%x\r\n' % len(chunk))
        self._callbacks.to_client(chunk)
        if self._chunkify_response:
            self._callbacks.to_client('\r\n')

    def send_http_response_done(self, close=False):
        self._env.log2('%s response done, keep-alive %s close %s', self, self._keep_alive, close)
        assert self._state == self.STREAMING_RESPONSE, self._state
        if self._chunkify_response:
            self._callbacks.to_client('0\r\n\r\n')
            self._chunkify_response = None
        if close or not self._keep_alive:
            self._state = None
            self._callbacks.to_client('')
        else:
            self._state = self.WAITING_FOR_REQUEST
        self._callbacks.will_listen(True)

    def send_tunnel_confirmation(self):
        self._env.log2('%s send_tunnel_confirmation state %s', self, self._state)
        assert self._state == self.CONNECT, self._state
        chunk = mime.Headers(start_line='HTTP/1.1 200 Connection established').format()
        self._env.log3('%s enqueuing %r', self, chunk)
        self._state = self.TUNNEL
        self._callbacks.to_client(chunk)

    def send_tunnel_chunk(self, chunk):
        self._env.log3('%s send tunnel chunk %s', self, len(chunk))
        assert self._state == self.TUNNEL or not chunk, (self._state, chunk)
        self._callbacks.to_client(chunk)
        return '' # return unprocessed data

    def __repr__(self):
        return '%s%X' % (self.__class__.__name__, id(self))

    __str__ = __repr__


class HttpBodyServerEngineCallbacks(HttpServerEngineCallbacks):

    def __init__(self, env):
        HttpServerEngineCallbacks.__init__(self)
        self.__env = env
        self.__request_headers = self.__request_len = self.__request_fragments = None

    def got_http_request_start(self, method, path, protocol, headers):
        assert self.__request_fragments is None, self.__request_fragments
        self.__request_len = 0
        self.__request_fragments = []
        self.__request_headers = headers

    def got_http_request_fragment(self, chunk):
        self.__request_len += len(chunk)
        if self.__request_len > self.__env.config.MAX_RECEIVE_CONTENT_LENGTH:
            self.__env.log1('request body %s is too large - max is %s',
                    self.__request_len, self.__env.config.MAX_RECEIVE_CONTENT_LENGTH)
            # TODO: fail somehow
        else:
            self.__request_fragments.append(chunk)

    def got_http_request_done(self):
        body = ''.join(self.__request_fragments)
        self.got_http_request(self.__request_headers, body)
        self.__request_headers = self.__request_len = self.__request_fragments = None

    def got_http_request(self, headers, content):
        raise NotImplementedError


import unittest

class Tests(unittest.TestCase):
    trace = []

    def setUp(self):
        trace = self.trace
        class Callbacks(HttpBodyServerEngineCallbacks):
            def to_client(self, buf):
                trace.append(('to_client', buf))
            def got_http_request(self, *a):
                trace.append(('got_http_request',) + tuple(map(str, a)))
            def will_listen(self, ready):
                trace.append(('will_listen', ready))
        self.env = self.Env(self.trace)
        self.callbacks = Callbacks(self.env)

    class Env:
        def __init__(self, trace):
            self.trace = trace
        class config:
            MAX_RECEIVE_HEADER_LENGTH = 1234
            MAX_RECEIVE_CONTENT_LENGTH = 1234
            PERSISTENT_HTTP = True
        def log1(self, *a): pass # self.trace.append(('log1',) + a)
        def log2(self, *a): pass # self.trace.append(('log2',) + a)
        def log3(self, *a): pass # self.trace.append(('log3',) + a)

    def testContentLength(self):
        del self.trace[:]
        eng = HttpServerEngine(self.env, self.callbacks)
        buf = 'GET http://1.2.3.4:80/ HTTP/1.1\r\nHost: 1.2.3.4:80\r\nConnection: Keep-Alive\r\nContent-Length: 0\r\n'
        self.assertEqual(eng.tcp_from_client(buf), buf)
        self.assertEqual(eng.tcp_from_client(buf + '\r\nnext'), 'next')
        eng.send_http_response('')
        self.assertEqual(self.trace, [
                ('will_listen', False),
                ('got_http_request', '<Headers:\nGET http://1.2.3.4:80/ HTTP/1.1\nHost: 1.2.3.4:80\nConnection: Keep-Alive\nContent-Length: 0\n>', ''),
                ('to_client', 'HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n'),
                ('will_listen', True),
                ])

    def testChunkedResponse(self):
        del self.trace[:]
        eng = HttpServerEngine(self.env, self.callbacks)
        buf = 'GET http://1.2.3.4:80/ HTTP/1.1\r\nHost: 1.2.3.4:80\r\nConnection: Keep-Alive\r\nContent-Length: 0\r\n\r\n'
        self.assertEqual(eng.tcp_from_client(buf), '')
        eng.send_http_response_start(
            mime.Headers('HTTP/1.1 200 hello', [('transfer-encoding', 'chunked')]),
            chunkify=True)
        eng.send_http_response_next('hell')
        eng.send_http_response_next('o')
        eng.send_http_response_done(close=True)
        self.assertEqual(eng.tcp_from_client('nananana'), None)
        self.assertEqual(self.trace, [
                ('will_listen', False),
                ('got_http_request',
                 '<Headers:\nGET http://1.2.3.4:80/ HTTP/1.1\nHost: 1.2.3.4:80\nConnection: Keep-Alive\nContent-Length: 0\n>',
                 ''),
                ('to_client', 'HTTP/1.1 200 hello\r\ntransfer-encoding: chunked\r\n\r\n'),
                ('to_client', '4\r\n'),
                ('to_client', 'hell'),
                ('to_client', '\r\n'),
                ('to_client', '1\r\n'),
                ('to_client', 'o'),
                ('to_client', '\r\n'),
                ('to_client', '0\r\n\r\n'),
                ('to_client', ''),
                ('will_listen', True),
                ('will_listen', False),
                ])

    def testPost(self):
        del self.trace[:]
        eng = HttpServerEngine(self.env, self.callbacks)
        buf = 'POST http://1.2.3.4:80/ HTTP/1.1\r\nHost: 1.2.3.4:80\r\nConnection: Keep-Alive\r\nContent-Length: 6\r\n\r\nplease!'
        self.assertEqual(eng.tcp_from_client(buf), '!')
        self.assertEqual(eng.tcp_from_client(''), '')
        eng.send_http_response(content='thanks', content_type='text/plain', status='200', message='OK', close=False)
        self.assertEqual(self.trace, [
                ('will_listen', False),
                ('got_http_request', '<Headers:\nPOST http://1.2.3.4:80/ HTTP/1.1\nHost: 1.2.3.4:80\nConnection: Keep-Alive\nContent-Length: 6\n>', 'please'),
                ('to_client', 'HTTP/1.1 200 OK\r\nConnection: Close\r\nContent-Length: 6\r\nContent-Type: text/plain\r\n\r\n'),
                ('to_client', 'thanks'),
                ('to_client', ''),
                ('will_listen', True),
                ])

    def testMultiRequest(self):
        trace = []
        class Callbacks(HttpServerEngineCallbacks):
            def to_client(self, *a):
                trace.append(('to_client',) + a)
            def got_http_request_start(self, *a):
                trace.append(('got_http_request_start',) + tuple(map(str, a)))
            def got_http_request_fragment(self, *a):
                trace.append(('got_http_request_fragment',) + a)
            def got_http_request_done(self, *a):
                trace.append(('got_http_request_done',) + a)
            def will_listen(self, ready):
                trace.append(('will_listen', ready))

        eng = HttpServerEngine(self.Env(trace), Callbacks())
        self.assertEqual(eng.tcp_from_client('GET /foo HTTP/1.1\r\ncontent-length: 4\r\n\r\nxxxx'), '')
        eng.send_http_response_start(
            mime.Headers('HTTP/1.1 200 hello', [('connection', 'close')]))
        eng.send_http_response_next('hello')
        eng.send_http_response_done(close=True)
        self.assertEqual(trace, [
                ('got_http_request_start', 'GET', '/foo', 'HTTP/1.1', '<Headers:\nGET /foo HTTP/1.1\ncontent-length: 4\n>'),
                ('got_http_request_fragment', 'xxxx'),
                ('will_listen', False),
                ('got_http_request_done',),
                ('to_client', 'HTTP/1.1 200 hello\r\nconnection: close\r\n\r\n'),
                ('to_client', 'hello'),
                ('to_client', ''),
                ('will_listen', True),
                ])
