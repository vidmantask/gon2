#!/usr/bin/env python

"""
ToH Mini Client
Giritech implementation of RFC-1149-ish implementation of tunneling TCP over HTTP
Copyright Giritech 2008-2010
"""

import sys
if sys.platform == 'win32':
    import proxy_detection

import ToHclient


class env:

    class config:
        INITIAL_HTTP_CONNECTIONS = 1 # assumed number of connections requested by server - should match servers MIN_WAITING_HTTP_CONNECTIONS
        MAX_WAITING_HTTP_CONNECTIONS = 4 # if more than this number of connections then get rid of them
        PING_INTERVAL = 30 # server must be given something at least this often to ensure that it has something to respond on
        ACK_TIMEOUT = 1.0 # pending acks will be sent after this long
        RETRANS_TIMEOUT = 1.0 # unacked packages will be retransmitted after this long
        MAX_RETRANS = 20 # after retransmitting this many times the connection fails
        PERSISTENT_HTTP = True # attempt persistent connections - squid supports it from client to proxy
        NEGOTIATE = True # let httpclient attempt to use SSO with Windows credentials to proxy or host
        TCP_RECV_BUFFER = 16384
        LISTEN_BACKLOG = 100 # how many TCP (HTTP) connections can be waiting, probably max 128
        MAX_RECEIVE_HEADER_LENGTH = 4096 # maximum received HTTP header length in bytes
        MAX_RECEIVE_CONTENT_LENGTH = 100000 # maximum received HTTP content length in bytes
        MAX_SEND_CONTENT_LENGTH = 50000 # soft limit maximum sent HTTP content length in bytes - content can however be as big as whole message
        MAX_OUT_OF_ORDER = 50 # maximum number of premature packages a TcpConnection will store
        MAX_UNACK = 20 # maximum number of unacked packages a TcpConnection will store before it throttles
        MAX_TCP_PER_SESSION = 100 # maximum number of TcpConnections per Session
        MAX_IDLE_TCP = 600 # how long should a TcpConnection be allowed to be idle (perhaps because other end is dead)
        MAX_IDLE_SESSION = 300 # how long should a Session be allowed to be idle (ie without TcpConnections) (perhaps because other end is dead)
        MAX_INITIAL_IDLE_SESSION = 300 # like MAX_IDLE_SESSION, but only used initially
        USER_AGENT = '' # RFC 2616 3.8, 'Giritech-ToH/$VERSION'

    @staticmethod
    def log1(s, *args):
        print '1:', s % args

    @staticmethod
    def log2(s, *args):
        print '2:', s % args

    @staticmethod
    def log3(s, *args):
        print '3:', s % args

    out_symbol = '>'
    in_symbol = '<'

def main():
    forward_addr = ('127.0.0.1', 0)
    http_addr = ('dev-mk.dev', 8080)
    proxy_addr = ('cessna', 3128)
    #proxy_addr = None # Proxy disabled
    #proxy_addr = proxy_detection.get_proxy_for_url('http://%s:%s/' % (http_addr[0], http_addr[1]))

    async_map = {}
    session_manager = ToHclient.make_session_manager(env, forward_addr, proxy_addr, http_addr, async_map=async_map)
    if not session_manager:
        raise SystemExit(1)
    env.log1('Listening on %s:%s', session_manager.sockname[0], session_manager.sockname[1])

    toh = ToHclient.main()
    toh.run(env, session_manager, async_map=async_map)
    # toh.stop() # from other thread

if __name__ == '__main__':
    main()
