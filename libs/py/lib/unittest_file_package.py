"""
Unittest of file_package lib
"""
from __future__ import with_statement
from StringIO import StringIO
import unittest
from lib import giri_unittest

import shutil
import tempfile
import os
import StringIO

from lib import file_package

class GPM_file_spec(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

  
    def test_info_from_spec(self):
        spec_data = """
Name:gon_client
Version:1.2.3-4
Arch:win
Description:xxxxxxxxxxxxxxxxxxxxxxxxxx

%files
gon_client/linux

"""
        spec_file = StringIO.StringIO(spec_data)
        spec = file_package.FilePackageSpec.create_from_file(spec_file)
        spec_info = spec.get_info()

        self.assertEqual(spec_info.name, 'gon_client')
        self.assertEqual(spec_info.version, '1.2.3-4')
        self.assertEqual(spec_info.arch, 'win')
        self.assertEqual(spec_info.get_package_name(), 'gon_client-1.2.3-4-win')


    def create_folder(self, temp_folder, foldername_relative):
        folder_name = os.path.join(temp_folder, foldername_relative)
        os.mkdir(folder_name)
        return folder_name

    def create_file(self, temp_folder, filename_relative):
        new_file = open(os.path.join(temp_folder, filename_relative), 'w')
        new_file.write('Hello in file %s' % filename_relative)
        new_file.close()


    def test_files_from_spec(self):
        spec_data = """
Name:gon_client
Version:1.2.3-4
Arch:win
Description:xxxxxxxxxxxxxxxxxxxxxxxxxx

%files
my_folder

%files_exclude
my_folder/my_file_002

"""
        temp_folder = tempfile.mkdtemp()
        my_folder = self.create_folder(temp_folder, 'my_folder')
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_001'))
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_002'))
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_003'))
        
        spec_file = StringIO.StringIO(spec_data)
        spec = file_package.FilePackageSpec.create_from_file(spec_file)

        spec_files = spec.get_files(temp_folder)
        self.assertEqual(len(spec_files), 2)
        self.assertTrue(os.path.join('my_folder', 'my_file_001') in spec_files)
        self.assertFalse(os.path.join('my_folder', 'my_file_002') in spec_files)
        self.assertTrue(os.path.join('my_folder', 'my_file_003') in spec_files)
        

    def test_build_and_extract(self):
        spec_data = """
Name:gon_client
Version:1.2.3-4
Arch:win
Description:xxxxxxxxxxxxxxxxxxxxxxxxxx

%files
my_folder

"""
        temp_folder = tempfile.mkdtemp()
        my_folder = self.create_folder(temp_folder, 'my_folder')
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_001'))
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_002'))
        self.create_file(temp_folder, os.path.join(my_folder, 'my_file_003'))

        spec_filename = os.path.join(temp_folder, 'spec')
        spec_file = open(spec_filename, 'w')
        spec_file.write(spec_data)
        spec_file.close()

        gpms_folder = self.create_folder(temp_folder, 'gpms')
        file_package_handler = file_package.FilePackageHandler(temp_folder)
        file_package_handler.build_from_spec(gpms_folder, temp_folder, spec_filename)

        file_package_filename = os.path.join(gpms_folder, 'gon_client-1.2.3-4-win.gpm')
        self.assertTrue(os.path.exists(file_package_filename))
        

        # Extract testing
        gpms_spec_folder = self.create_folder(temp_folder, 'gpms.spec')
        file_package_handler.extract_info(file_package_filename, gpms_spec_folder)

        extact_folder = self.create_folder(temp_folder, 'extract')
        file_package_handler.extract(file_package_filename, extact_folder)
        
        
        file_package_spec_filename = os.path.join(gpms_spec_folder, 'gon_client-1.2.3-4-win')
        file_package_spec = file_package.FilePackageSpec.create_from_filename(file_package_spec_filename)
        extracted_filenames = []
        for filename in file_package_spec.get_files_expanded():
            self.assertTrue(os.path.exists(os.path.join(extact_folder, filename)))
            extracted_filenames.append(os.path.join(extact_folder, filename))

        file_package_handler.erase_files(file_package_spec_filename, extact_folder)
        for filename in extracted_filenames:
            self.assertFalse(os.path.exists(os.path.join(extact_folder, filename)))
        
        shutil.rmtree(temp_folder)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
