"""
This module contains functionality for interfacing with a running gon_client.
"""
import sys
import re
import os
import os.path
import base64
import pickle
import subprocess

import lib.version
import lib.checkpoint
import lib.launch_process
import lib.utility
import lib.appl.gon_os

if sys.platform == 'win32':
    import _winreg
    
module_id = 'lib.appl.gon_client'


FOLDERNAME_GIRITECH = 'Giritech'


def _get_current_instance_package_id_rexp():
    arch = None
    if sys.platform == 'win32':
        arch = 'win'
    elif sys.platform == 'linux2':
        arch = 'linux'
    elif sys.platform == 'darwin':
        arch = 'mac'
    version = lib.version.Version.create_current()
    return 'gon_client-%d\.%d\.%d\.%d-.*-%s' % (version.get_version_major(), version.get_version_minor(), version.get_version_bugfix(), version.get_version_build_id(), arch)

def _get_one_instance_package_id_rexp():
    arch = None
    if sys.platform == 'win32':
        arch = 'win'
    elif sys.platform == 'linux2':
        arch = 'linux'
    elif sys.platform == 'darwin':
        arch = 'mac'
    return 'gon_client-.*-%s' % arch

def contains_current_instance(update_ids):
    """
    Examine a list of pair's (from_package_id, to_package_id) for the current version of gon_client. 
    If the current(running) version is found in the from_package_id the to_package_id is returned, else None is returned.
    """
    if lib.appl.gon_os.is_gon_os():
        return None
    package_id_rexp = _get_current_instance_package_id_rexp()
    for (update_id_from, update_id_to) in update_ids:
        if re.match(package_id_rexp, update_id_from) != None:
            return update_id_to
    return None

def remove_current_instance(gpm_ids):
    package_id_rexp = _get_current_instance_package_id_rexp()
    for gpm_id in set(gpm_ids):
        if re.match(package_id_rexp, gpm_id) != None:
            gpm_ids.remove(gpm_id)

def get_one_instance(gpm_meta):
    gpm_ids = []
    for gpm_spec in gpm_meta:
        gpm_ids.append(gpm_spec.header.get_package_id())
    
    package_id_rexp = _get_one_instance_package_id_rexp()
    for gpm_id in gpm_ids:
        if re.match(package_id_rexp, gpm_id) != None:
            return gpm_id
    return None


    
def get_installation_root(cwd_root=None):
    if cwd_root is None:
        cwd_root = os.getcwdu()
    if sys.platform == 'win32':
        return os.path.join(cwd_root, '..' , '..')
    elif sys.platform == 'linux2':
        return os.path.join(cwd_root, '..' , '..')
    elif sys.platform == 'darwin':
        return os.path.join(cwd_root, '..' , '..', '..', '..', '..')

def get_gon_client_root_relative():
        gon_client_root_relative = None
        gon_client_filename = None
        if sys.platform == 'win32':
            gon_client_root_relative = os.path.join('gon_client', 'win')
            gon_client_cwd_relative = gon_client_root_relative 
            gon_client_filename = 'gon_client.exe'
        elif sys.platform == 'linux2':
            gon_client_root_relative = os.path.join('gon_client', 'linux')
            gon_client_cwd_relative = gon_client_root_relative 
            gon_client_filename = 'gon_client.com'
        elif sys.platform == 'darwin':
            gon_client_root_relative = os.path.join('gon_client', 'mac')
            gon_client_cwd_relative = os.path.join(gon_client_root_relative, 'gon_client.app', 'Contents', 'Resources') 
            gon_client_filename = 'gon_client.app'
        return (gon_client_root_relative, gon_client_cwd_relative, gon_client_filename)

def generate_installation_root(gon_client_version=None, installation_root=None):
    if installation_root is None or installation_root == '':
        if sys.platform == 'win32':
            if os.environ.has_key('APPDATA'):
                installation_root = os.environ['APPDATA']
            elif os.environ.has_key('LOCALAPPDATA'):
                installation_root = os.environ['LOCALAPPDATA']
            else:
                with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion") as reg_key_current_version:
                    (installation_root, installation_root_type) = _winreg.QueryValueEx(reg_key_current_version, "ProgramFilesDir")
            installation_root = installation_root.decode(sys.getfilesystemencoding())
        elif sys.platform == 'linux2':
            installation_root = os.path.join(lib.utility.get_home_folder(), 'bin')
        elif sys.platform == 'darwin':
            installation_root = os.path.join(lib.utility.get_home_folder(), 'bin')

    if not os.path.exists(installation_root):
        return (None, None)

    gon_client_folder_base = os.path.join(installation_root, 'Giritech')
    gon_client_folder_sub_base_org = 'G-On Client'
    if gon_client_version is not None:
        gon_client_folder_sub_base_org = '%s %s' % (gon_client_folder_sub_base_org, gon_client_version.get_version_string())
    
    index = 1
    gon_client_folder_sub_base = gon_client_folder_sub_base_org
    gon_client_folder = os.path.join(gon_client_folder_base, gon_client_folder_sub_base)
    while os.path.exists(gon_client_folder):
        gon_client_folder_sub_base = '%s(%d)' % (gon_client_folder_sub_base_org, index)
        gon_client_folder = os.path.join(gon_client_folder_base, gon_client_folder_sub_base)
        index += 1
    return (gon_client_folder_sub_base, gon_client_folder)
    
def get_restart_state(client_runtime_env):
    restart_filename = os.path.join(client_runtime_env.get_temp_root(), GOnClientRestartHandler.GON_CLIENT_RESTART_FILENAME)
    if os.path.isfile(restart_filename):
        return GOnClientInstallState.create_from_file(restart_filename)
    return None

class GOnClientInstallState(object):
    def __init__(self, done=False, state=None):
        self.restart_root = None
        self.restart_temp_root = None
        self.offline_operation = False
        self.online_version_update_operation = False
        self.first_start_operation = False
        self.cpm_session_data = None
        self.plugin_data = None
        self.restart_data = None
        self.first_start_data = None
        self.done = done
        if state is not  None:
            self.init_from_state(state)

    def init_from_state(self, state):
        if state.has_key('offline_operation'):
            self.set_offline_operation(state['offline_operation'])
        if state.has_key('online_version_update_operation') and state['online_version_update_operation']:
            self.set_online_version_update_operation()
        if state.has_key('first_start_operation') and state['first_start_operation']:
            self.set_first_start_operation()
        if state.has_key('cpm_session_data'):
            self.set_cpm_session_data(state['cpm_session_data'])
        if state.has_key('plugin_data'):
            self.set_plugin_data(state['plugin_data'])
        if state.has_key('first_start_data'):
            self.set_first_start_data(state['first_start_data'])
    
    @classmethod
    def create_from_file(cls, state_filename):
        if os.path.isfile(state_filename):
            state_file = file(state_filename, 'rb')
            state = pickle.loads(base64.standard_b64decode(state_file.read()))
            state_file.close()
            return GOnClientInstallState(state=state)
        return None
    
    def set_restart_root(self, restart_root):
        self.restart_root = restart_root

    def get_restart_root(self):
        return self.restart_root

    def set_offline_operation(self, offline_operation):
        self.offline_operation = offline_operation

    def get_offline_operation(self):
        return self.offline_operation

    def set_online_version_update_operation(self):
        self.online_version_update_operation = True

    def get_online_version_update_operation(self):
        return self.online_version_update_operation
    
    def set_first_start_operation(self):
        self.first_start_operation = True

    def get_first_start_operation(self):
        return self.first_start_operation

    def set_plugin_data(self, plugin_data):
        self.plugin_data = plugin_data
        
    def get_plugin_data(self):
        return self.plugin_data

    def set_cpm_session_data(self, cpm_session_data):
        self.cpm_session_data = cpm_session_data
        
    def get_cpm_session_data(self):
        return self.cpm_session_data

    def set_first_start_data(self, first_start_data):
        self.first_start_data = first_start_data
        
    def get_first_start_data(self):
        return self.first_start_data

    def is_done(self):
        return self.done


class GonClientRestartedRelocatedInfo():
    """
    This info file is introduced in version 5.4 to make a reference to the used runtime_env_id currently running relocated
    """    
    CURRENT_PROTOCOL_VERSION = 1
    GON_CLIENT_RESTART_RELOCATED_FILENAME =  'gon_client_restart_relocated'

    def __init__(self, gon_client_base_runtime_env_id):
        self.gon_client_base_runtime_env_id = gon_client_base_runtime_env_id
        
    @classmethod
    def create_from_file(cls, root,  remove_after_read=False):
        relocate_info_filename = os.path.join(root, GonClientRestartedRelocatedInfo.GON_CLIENT_RESTART_RELOCATED_FILENAME)
        if os.path.isfile(relocate_info_filename):
            relocate_info_file = file(relocate_info_filename, 'rb')
            relocate_info_raw = pickle.loads(base64.standard_b64decode(relocate_info_file.read()))
            relocate_info_file.close()
            relocate_info =  GonClientRestartedRelocatedInfo(relocate_info_raw['gon_client_base_runtime_env_id'])
            if remove_after_read:
                os.remove(relocate_info_filename)
            return relocate_info
        return None
    
    def save(self, root):
        relocate_info = {}
        relocate_info['protocol_version'] = GonClientRestartedRelocatedInfo.CURRENT_PROTOCOL_VERSION
        relocate_info['gon_client_base_runtime_env_id'] = self.gon_client_base_runtime_env_id
        relocate_info_filename = os.path.join(root, GonClientRestartedRelocatedInfo.GON_CLIENT_RESTART_RELOCATED_FILENAME)
        relocate_info_file = file(relocate_info_filename, 'wb')
        relocate_info_file.write(base64.standard_b64encode(pickle.dumps(relocate_info)))
        relocate_info_file.close()
        

class GOnClientRestartHandler(object):
    """
    Holds functionality for handling restart of gon_client
    """
    GON_CLIENT_RESTART_FILENAME =  'gon_client_restart'
    CURRENT_PROTOCOL_VERSION = 1

    def __init__(self, checkpoint_handler, runtime_env_id, client_runtime_env, skip_remove_of_restart_state=False):
        self._restart_state = {}
        self._restart_state['protocol_version'] = GOnClientRestartHandler.CURRENT_PROTOCOL_VERSION
        self._restart_state['gon_client_base_cwd'] = os.getcwdu()
        self._restart_state['gon_client_base_runtime_env'] = runtime_env_id
        self._restart_state['gon_client_base_runtime_env_temp_root'] = client_runtime_env.get_temp_root()
        self._restart_state['offline_operation'] = False
        self._restart_state['online_version_update_operation'] = False
        self._restart_state['first_start_operation'] = False
        self._restart_state['plugin_data'] = None
        self._restart_state['cpm_session_data'] = None
        self._restart_state['first_start_data'] = None
        self._restart_file_found = False
        self._restart_filename = None
        try:
            self._restart_filename = os.path.join(client_runtime_env.get_temp_root(), GOnClientRestartHandler.GON_CLIENT_RESTART_FILENAME)
            if os.path.isfile(self._restart_filename):
                restart_state_file = file(self._restart_filename, 'rb')
                self._restart_state = pickle.loads(base64.standard_b64decode(restart_state_file.read()))
                restart_state_file.close()
                if not skip_remove_of_restart_state:
                    os.remove(self._restart_filename)
                self._restart_file_found = True
        except:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("GOnClientRestartHandler.init", module_id, lib.checkpoint.WARNING, etype, evalue, etrace)

    def reset_gon_client_base(self, client_runtime_env):
        self._restart_state['gon_client_base_cwd'] = os.getcwdu()
        self._restart_state['gon_client_base_runtime_env'] = client_runtime_env.get_info().get_id()
        self._restart_state['gon_client_base_runtime_env_temp_root'] = client_runtime_env.get_temp_root()

    def set_gon_client_base_cwd(self, runtime_env_root):
        (gon_client_root_relative, gon_client_cwd_relative, gon_client_filename) = get_gon_client_root_relative()
        self._restart_state['gon_client_base_cwd'] = os.path.join(runtime_env_root, gon_client_cwd_relative)

    def set_gon_client_base_runtime_env(self, client_runtime_env):
        self._restart_state['gon_client_base_runtime_env'] = client_runtime_env.get_info().get_id()
        self._restart_state['gon_client_base_runtime_env_temp_root'] = client_runtime_env.get_temp_root()

    def is_restarted_for_offline_operation(self):
        if self._restart_file_found and self._restart_state != None and self._restart_state.has_key('offline_operation'):
            return self._restart_state['offline_operation']
        return False

    def is_restared_for_online_version_update_operation(self):
        if self._restart_file_found and self._restart_state != None and self._restart_state.has_key('online_version_update_operation'):
            return self._restart_state['online_version_update_operation']
        return False
    
    def is_first_start_operation(self):
        if self._restart_file_found and self._restart_state != None and self._restart_state.has_key('first_start_operation'):
            return self._restart_state['first_start_operation']
        return False
        
    def get_gon_client_install_state(self):
        return GOnClientInstallState(state=self._restart_state)

    def get_base_runtime_environment_id(self):
        return self._restart_state['gon_client_base_runtime_env']


    def _get_new_restart_state_and_root(self, gon_client_install_state, gon_client_root_relative):
        is_done = True
        restart_root = None
        restart_state = self._restart_state
        if gon_client_install_state != None:
            restart_root = gon_client_install_state.get_restart_root()
            restart_state['cpm_session_data'] = gon_client_install_state.get_cpm_session_data()
            restart_state['plugin_data'] = gon_client_install_state.get_plugin_data()
            restart_state['first_start_data'] = gon_client_install_state.get_first_start_data()
            restart_state['offline_operation'] = gon_client_install_state.get_offline_operation()
            restart_state['online_version_update_operation'] = gon_client_install_state.get_online_version_update_operation()
            restart_state['first_start_operation'] = gon_client_install_state.get_first_start_operation()
            is_done = gon_client_install_state.is_done()
        is_relocated = restart_root is not None
        if is_relocated:
            restart_state_root = restart_root
        else:
            restart_state_root = get_installation_root(self._restart_state['gon_client_base_cwd'])
        return (restart_state, restart_state_root, is_done, is_relocated)
        
    def write_restart_state(self, checkpoint_handler, gon_client_install_state):
        (gon_client_root_relative, gon_client_cwd_relative, gon_client_filename) = get_gon_client_root_relative()
        (restart_state, restart_state_root, is_done, is_relocated) = self._get_new_restart_state_and_root(gon_client_install_state, gon_client_root_relative)
        restart_state_filename = self._write_restart_state_helper(checkpoint_handler, restart_state)

    def _write_restart_state_helper(self, checkpoint_handler, restart_state):
        restart_state_filename = None
        restart_temp_root = restart_state['gon_client_base_runtime_env_temp_root']
        if os.path.isdir(restart_temp_root):
            restart_state_filename = os.path.join(restart_temp_root, GOnClientRestartHandler.GON_CLIENT_RESTART_FILENAME)
            self._write_restart_state(restart_state_filename, restart_state)
        else:
            print "_write_restart_state_helper not found",  restart_temp_root
        return restart_state_filename

    def do_restart(self, checkpoint_handler, gon_client_install_state, skip_write_of_restart_state=False, skip_launch=False):
        """
        Do the actual restart of the gon_client. If insstall_state object constains a restart_root this will be used,
        otherwise the current location of gon_client is used when restarted. 
        """
        try:
            (gon_client_root_relative, gon_client_cwd_relative, gon_client_filename) = get_gon_client_root_relative()
            (restart_state, restart_state_root, is_done, is_relocated) = self._get_new_restart_state_and_root(gon_client_install_state, gon_client_root_relative)
            if is_relocated:
                restart_relocated_info = GonClientRestartedRelocatedInfo(restart_state['gon_client_base_runtime_env'])
                restart_relocated_info.save(os.path.join(restart_state_root, gon_client_cwd_relative))

            gon_client_root_abs = os.path.join(restart_state_root, gon_client_root_relative) 
            if os.path.isdir(gon_client_root_abs):
                checkpoint_handler.Checkpoint("GOnClientRestartHandler.restart_state", module_id, lib.checkpoint.DEBUG, restart_state=str(restart_state))
                if not skip_write_of_restart_state:
                    restart_state_filename = self._write_restart_state_helper(checkpoint_handler, restart_state)
                    if is_done and os.path.exists(restart_state_filename):
                        os.remove(restart_state_filename)

                if skip_launch:
                    return True 
    
                restart_argv = []
                if sys.platform == 'darwin':
                    restart_argv.append('open')
                gon_client_launch = os.path.normpath(os.path.join(gon_client_root_abs, gon_client_filename)).encode(sys.getfilesystemencoding())
                restart_argv.append(gon_client_launch)

                checkpoint_handler.Checkpoint("GOnClientRestartHandler.do_restart.launch", module_id, lib.checkpoint.DEBUG, restart_argv=repr(restart_argv))
                process = lib.launch_process.Process(checkpoint_handler)
                
                env = {} 
                env.update(os.environ)

                # The following environment variables need to be removed because of pyinstaller
                if sys.platform == 'linux2':
                    env_vars_to_remove = ['_MEIPASS2', 'LD_LIBRARY_PATH', 'PYTHONPATH']
                    for env_var in env_vars_to_remove:
                        if env.has_key(env_var):
                            env.pop(env_var)
                
                launched = process.launch(subprocess.list2cmdline(restart_argv), False, None, env=env)
                return launched
            else:
                checkpoint_handler.Checkpoint("GOnClientRestartHandler.do_restart.invald_restart_cwd_root", module_id, lib.checkpoint.ERROR, restart_state_root=restart_state_root)
                return False
        except:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("GOnClientRestartHandler.do_restart", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
            return False 

    def rewrite_restart_state(self):
        if os.path.isdir(os.path.dirname(self._restart_filename)):
            self._write_restart_state(self._restart_filename, self._restart_state)
            
    def _write_restart_state(self, restart_state_filename, restart_state):
        restart_state_file = file(restart_state_filename, 'wb')
        restart_state_file.write(base64.standard_b64encode(pickle.dumps(restart_state)))
        restart_state_file.close()

