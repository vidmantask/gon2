"""
This module contains functionality for finding installed G/On Systems
"""
import sys
import os
import os.path
import lib.version

class GOnSystemBase(object):
    SYSTEM_TYPE_INSTALLATION = 0
    SYSTEM_TYPE_BACKUP = 1
    
    """
    This class represent a Giritech System
    """
    def __init__(self, system_type):
        self.system_type = system_type
        self.root_folder = None
        self.valid = False
        self.version = None
        self.gateway_service = None
        self.management_service = None
    
    def generate_backup(self, checkpoint_handler, backup_folder):
        pass
    
    def get_name(self):
        return self.root_folder 
    
    def get_title(self):
        title = self.root_folder
        if len(self.root_folder) > 40:
            title = self.root_folder[:15] + '...' + self.root_folder[len(self.root_folder)-24:] 
        if self.system_type == GOnSystemBase.SYSTEM_TYPE_INSTALLATION:
            return 'G/On Installation (%s)' % (title) 
        return 'G/On Backup (%s)' % (title) 

    def is_running(self):
        management_running = False
        gateway_running = False

        if self.management_service is not None:
            management_running = self.management_service.is_running()
        if self.gateway_service is not None:
            gateway_running = self.gateway_service.is_running()
        return (management_running, gateway_running)
        
    def is_current_version(self):
        if self.version is None:
            return False
        return lib.version.Version.create_current().equal(self.version)
    
    def is_system_type_installation(self):
        return self.system_type == GOnSystemBase.SYSTEM_TYPE_INSTALLATION

    def is_system_type_backup(self):
        return self.system_type == GOnSystemBase.SYSTEM_TYPE_BACKUP

    def is_current_installation(self, current_installation_root):
        return os.path.normpath(current_installation_root).startswith(self.root_folder) and self.is_current_version() and self.is_system_type_installation() 

    @classmethod
    def create_list_backuped(cls, backup_path):
        gon_systems = []
        
        backup_path_abs = os.path.abspath(backup_path)
        if not os.path.isdir(backup_path_abs):
            return gon_systems

        for folder_item in os.listdir(backup_path_abs):
            root_folder = os.path.join(backup_path_abs, folder_item)

            if os.path.isdir(root_folder):
                version_filename = os.path.join(root_folder, 'version')
                if os.path.exists(version_filename):
                    version_file = open(version_filename, 'r')
                    version_string = version_file.read() 
                    version = lib.version.Version.create_from_string(version_string)

                    gon_system = GOnSystemBase(GOnSystemBase.SYSTEM_TYPE_BACKUP)
                    gon_system.root_folder = root_folder
                    gon_system.version = version
                    gon_system.valid = gon_system.version is not None
                    gon_systems.append(gon_system)
        return gon_systems

    @classmethod
    def create_list_installed(cls, checkpoint_handler):
        return []

    @classmethod
    def compare_by_version(cls, a, b):
        return lib.version.Version.compare(a.version, b.version)
   
    @classmethod
    def add_installed_system_in_registry(cls, installation_root):
        pass

    @classmethod
    def remove_installed_system_from_registry(cls, installation_root):
        pass
