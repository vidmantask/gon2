"""
This module contains functionality to watch a folder of cp-xml-files and do something with
each file before deleting it. 
"""
import elementtree.ElementTree as element_tree
import time
import os.path
import datetime

import lib.checkpoint
import components.communication.async_service


class CPFile(object):
    """
    This classe represent a cp-xml file
    """
    def __init__(self, filename_abs):
        self.filename_abs = filename_abs
        self.tag = None
        self.attributes = []
        self.content = None

    def get_summary(self):
        summary = ""
        if self.attributes.has_key("summary"):
            summary = self.attributes["summary"]
        return summary

    def get_source(self):
        module = ""
        checkpoint_id = ""
        if self.attributes.has_key("module"):
            module = self.attributes["module"]
        if self.attributes.has_key("checkpoint_id"):
            checkpoint_id = self.attributes["checkpoint_id"]
        return "%s(%s)" % (module, checkpoint_id)

    def get_details(self):
        if self.content is None:
            return ""
        return self.content
    
    def get_ts(self):
        ts = datetime.datetime.now()
        try:
            # ts='20100722T093840.043760'
            ts_as_string = self.attributes["ts"]
            if len(ts_as_string) == 15: 
                ts =  datetime.strptime(ts_as_string, "%Y%m%dT%H%M%S")
            else:
                ts =  datetime.strptime(ts_as_string, "%Y%m%dT%H%M%S.%f")
        except:
            pass
        return ts

    def remove(self):
        try:
            if os.path.isfile(self.filename_abs):
                os.unlink(self.filename_abs)
        except:
            pass  

    
    @classmethod
    def parse_from_filename(cls, filename_abs):
        try:
            root = element_tree.ElementTree(None, filename_abs)
            if root is None:
                return None
            node = root.getroot()
            cp_file = CPFile(filename_abs)
            cp_file.tag = node.tag
            cp_file.attributes = node.attrib
            cp_file.content = node.text
            return cp_file
        except:
            return None


class CpInFolderJanitorCallback(object):
    def handle_cp(self, cp_filename_abs, cp_file):
        print "CpInFolderJanitorCallback::handle_cp", cp_filename_abs
    
    
class CPInFolderJanitor(object):
    STATE_INIT = 0
    STATE_RUNNING = 1
    STATE_CLOSING = 2
    STATE_CLOSED = 3
    
    def __init__(self, async_service, checkpoint_handler, folder_to_watch, callback, poll_interval_sec=10):
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.mutex = async_service.create_mutex("CPInFolderJanitor")
        self.folder_to_watch = folder_to_watch
        self.callback = callback
        self.poll_interval_sec = poll_interval_sec
        
        self._state = CPInFolderJanitor.STATE_INIT
        self._stopping = False
        
    def start(self):
        self._state = CPInFolderJanitor.STATE_RUNNING
        self._janitor_start()

    def stop_and_wait(self):
        if self._state not in [CPInFolderJanitor.STATE_RUNNING]:
            return
        self._state = CPInFolderJanitor.STATE_CLOSING

        while self._state not in [CPInFolderJanitor.STATE_CLOSED]:
            time.sleep(1)
        self._state = CPInFolderJanitor.STATE_CLOSED
        
    def _janitor_start(self):
        if self._state not in [CPInFolderJanitor.STATE_RUNNING]:
            return
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.mutex, 0, self.poll_interval_sec, 0, self, '_janitor_work')
    
    def _janitor_work(self):
        if self._state in [CPInFolderJanitor.STATE_CLOSING]:
            self._state = CPInFolderJanitor.STATE_CLOSED
            return

        for cp_filename in os.listdir(self.folder_to_watch):
            cp_filename_abs = os.path.join(self.folder_to_watch, cp_filename)
            if os.path.isfile(cp_filename_abs):
                cp_file = CPFile.parse_from_filename(cp_filename_abs)
                if cp_file is not None:
                    self.callback.handle_cp(cp_filename_abs, cp_file) 
        self._janitor_start()
