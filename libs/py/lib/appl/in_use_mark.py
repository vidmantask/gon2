"""
This module contains functionality for handling in_use_mark files
"""
import sys
import os.path
import datetime
import pickle
import lib.commongon

class InUseMark(object):
    IN_USE_MARK_FILENAME = 'gon_in_use_mark'

    def __init__(self, folder):
        self.in_use_filename = os.path.join(folder, InUseMark.IN_USE_MARK_FILENAME)

    def do_mark_in_use(self):
        try:
            in_use_record = (lib.commongon.datetime2str(datetime.datetime.now()))
            in_use_file = open(self.in_use_filename, 'w')
            pickle.dump(in_use_record, in_use_file)
            in_use_file.close()
        except:
            print "InUseMark, Unable to mark_in_use", self.in_use_filename
            
    def do_mark_not_in_use(self):
        try:
            if os.path.exists(self.in_use_filename):
                os.remove(self.in_use_filename)
        except:
            print "InUseMark, Unable to mark_not_in_use", self.in_use_filename

    def is_in_use(self, timeout_sec=None):
        try:
            if timeout_sec is not None:
                if os.path.exists(self.in_use_filename):
                    in_use_file = open(self.in_use_filename, 'r')
                    in_use_record = pickle.load(in_use_file)
                    in_use_ts = lib.commongon.str2datetime(in_use_record)
                    in_use_file.close()
                    in_use_delta = datetime.datetime.now() - in_use_ts
                    return in_use_delta.seconds < timeout_sec
                else:
                    return False
            else:
                return os.path.exists(self.in_use_filename)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            print "InUseMark, Unable to is_in_use", self.in_use_filename, evalue
        return False
            