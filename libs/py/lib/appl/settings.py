'''
Created on Oct 6, 2011

@author: thwang
'''
import os.path
import pickle
import base64


class Settings(object):
    
    def __init__(self, settings=None):
        if settings is None:
            self._settings = {}
        else:
            self._settings = settings
    
    def get_value(self, key, default_value):
        try:
            if self._settings.has_key(key):
                return str(self._settings[key])
            return default_value
        except:
            return default_value
    
    def get_value_int(self, key, default_value):
        try:
            if self._settings.has_key(key):
                return int(self._settings[key])
            return default_value
        except:
            return default_value

    def get_value_bool(self, key, default_value):
        try:
            if self._settings.has_key(key):
                return self._settings[key] == True
            return default_value
        except:
            return default_value
    
    def set_value(self, key, value):
        self._settings[key] = value

    def save_to_file(self, filename):
        if filename is None:
            return
        settings_file = file(filename, 'wb')
        settings_file.write(base64.standard_b64encode(pickle.dumps(self._settings)))
        settings_file.close()
    
    @classmethod
    def create_from_file(cls, filename):
        if os.path.isfile(filename):
            settings_file = file(filename, 'rb')
            settings = pickle.loads(base64.standard_b64decode(settings_file.read()))
            settings_file.close()
            return Settings(settings)
        return Settings()
    