"""
This module contains functionality for handling giritech services
"""
import sys

import lib.version

SERVICE_NAME_PREFIX = u'GIRITECH'
SERVICE_NAME_SEPERATOR = u'_'
SERVICE_NAME_SUFIX_MANAGEMENT = u'MANAGEMENT'
SERVICE_NAME_SUFIX_GATEWAY = u'GATEWAY'
SERVICE_NAME_SUFIX_CLIENT_DEVICE = u'CLIENTDEVICE'
SERVICE_TITLE_MANAGEMENT = u'G/On Management Service'
SERVICE_TITLE_GATEWAY = u'G/On Gateway Service'
SERVICE_TITLE_CLIENT_DEVICE  = u'G/On Client Device Service'


class GServiceError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class GServiceBase(object):
    """
    This class represent a Giritech Service
    """
    def __init__(self, service_name, service_title, service_description):
        self.service_name = service_name
        self.service_title = service_title
        self.service_description = service_description
    
    def is_running(self):
        return False
    
    def is_installed(self):
        return False
    
    def is_current_version(self):
        return False

    def change(self):
        pass
    
    def install(self, exe_command_line, user_name=None, password=None, start_service=False):
        pass

    def remove(self):
        pass

    def stop_and_forget(self):
        pass

    def restart_and_forget(self):
        pass

    def start_and_forget(self):
        pass
    
    def check_for_update(self, listen_port=None, server_sid=None):
        pass

    
    @classmethod
    def create_list(cls):
        return []

    @classmethod
    def create_management_service(cls):
        version = lib.version.Version.create_current().get_version_string().strip()
        service_name = SERVICE_NAME_PREFIX + SERVICE_NAME_SEPERATOR + version + SERVICE_NAME_SEPERATOR + SERVICE_NAME_SUFIX_MANAGEMENT;
        service_title = '%s %s' %(SERVICE_TITLE_MANAGEMENT, version)
        service_description = ''
        return GServiceBase(service_name, service_title, service_description)

    @classmethod
    def create_gateway_service(cls):
        version = lib.version.Version.create_current().get_version_string().strip()
        service_name = SERVICE_NAME_PREFIX + SERVICE_NAME_SEPERATOR + version + SERVICE_NAME_SEPERATOR + SERVICE_NAME_SUFIX_GATEWAY;
        service_title = '%s %s' %(SERVICE_TITLE_GATEWAY, version)
        service_description = ''
        return GServiceBase(service_name, service_title, service_description)

    @classmethod
    def create_service_from_current_location(cls):
        return None
    
    @classmethod
    def create_service_for_installed_management_service(cls):
        return None
    
    @classmethod
    def create_from_service_name(cls, service_name):
        services = cls.create_list()
        for service in services:
            if service.service_name == service_name:
                return service
        return None

    @classmethod
    def create_management_service_description(cls, management_ws_ip=None, management_ws_port=None, management_ip=None, management_port=None):
        service_description_elements = []
        if management_ws_ip is not None:
            service_description_elements.append('management_ws_ip(%s)' % management_ws_ip)
        if management_ws_port is not None:
            service_description_elements.append('management_ws_port(%d)' % management_ws_port)
        if management_ip is not None:
            service_description_elements.append('management_ip(%s)' % management_ip)
        if management_port is not None:
            service_description_elements.append('management_port(%d)' % management_port)
        return ", ".join(service_description_elements)

    @classmethod
    def create_gateway_service_description(cls, server_sid=None, listen_ip=None, listen_port=None):
        service_description_elements = []
        if server_sid is not None:
            service_description_elements.append('sid(%s)' % server_sid)
        if listen_ip is not None:
            service_description_elements.append('listen_ip(%s)' % listen_ip)
        if listen_port is not None:
            service_description_elements.append('listen_port(%d)' % listen_port)
        return ", ".join(service_description_elements)

    @classmethod
    def create_service_for_current_management_service(cls, root):
        return None
    
    
    

if sys.platform == 'win32':
    import service_win
    GService = service_win.GServiceWin
elif sys.platform == 'linux2':
    import service_linux
    GService = service_linux.GServiceLinux
else:
    GService = GServiceBase
