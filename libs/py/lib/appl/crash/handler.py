import sys
import threading
import lib.checkpoint


def hook_crash_to_checkpoint(checkpoint_handler, checkpoint_module_id):
    """
    Hook into crash handler and report the crash into the checkpoint handler if possible
    """
    oldexcepthook = sys.excepthook
    
    def info(type, value, tb):
        try:
            checkpoint_handler.CheckpointException("crash", checkpoint_module_id, lib.checkpoint.CRITICAL, type, value, tb)
        except:
            pass
        oldexcepthook(type, value, tb)

    sys.excepthook = info

