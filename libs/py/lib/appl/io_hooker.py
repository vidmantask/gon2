import sys
import lib.checkpoint
import traceback

class IoHooker(object):
    @classmethod
    def hook(cls, extra_file):
        sys.stdout = cls(sys.stdout, extra_file)
        sys.stderr = cls(sys.stderr, extra_file)

    @classmethod
    def unhook(cls):
        sys.stdout = sys.stdout._old_file
        sys.stderr = sys.stderr._old_file
        
    def __init__(self, old_file, extra_file):
        self._old_file = old_file
        self._extra_file = extra_file

    def write(self, s):
        self._old_file.write(s)
        self._extra_file.write(s)


class IoHookerCheckpoint(object):
    @classmethod
    def hook(cls, checkpoint_handler, checkpoint_module_id):
        sys.stdout = cls(sys.stdout, checkpoint_handler, checkpoint_module_id, lib.checkpoint.DEBUG)
        sys.stderr = cls(sys.stderr, checkpoint_handler, checkpoint_module_id, lib.checkpoint.CRITICAL)

    def __init__(self, old_file, checkpoint_handler, checkpoint_module_id, cp_type):
        self._old_file = old_file
        self.checkpoint_handler = checkpoint_handler
        self._chekpoint_module_id = checkpoint_module_id
        self._cp_type = cp_type
        
    def write(self, s):
        if s.startswith('warning'):
            import pdb;pdb.set_trace()
        try:
            if s.strip():
                self.checkpoint_handler.CheckpointMultilineMessage('write', self._chekpoint_module_id, self._cp_type, s)
        except KeyboardInterrupt:
            return
        except:
            (etype, evalue, etrace) = sys.exc_info()
            error_message_full = ''.join(traceback.format_exception(etype, evalue, etrace))
            self._old_file.write(error_message_full)

        # It has been seen that the old_file handler is corrupt. Seen in connection with asyncore running in a different thread
        try:
            self._old_file.write(s)
        except:
            pass
        
        
    def flush(self):
        pass
