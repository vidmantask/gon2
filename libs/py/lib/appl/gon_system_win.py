"""
This module contains functionality for finding installed G/On Systems on windows
"""
import sys
import os
import subprocess
import time
import shutil
import tempfile
import traceback

import win32api
import win32con
import _winreg
import uuid

import lib.appl.service
import lib.appl.gon_system
import lib.version
import lib.checkpoint
import lib.launch_process


# Got this from shutil.py (WindowsError is not defined on other platforms that windows)
try:
    WindowsError
except NameError:
    WindowsError = None



GON_CONFIG_SERVICE = os.path.join('gon_config_service', 'win', 'gon_config_service.exe')





def get_system_version(checkpoint_handler, system_root):
    try:
        gon_config_service_path = os.path.normpath(os.path.join(system_root, GON_CONFIG_SERVICE))
        args = [gon_config_service_path, '--version']
        (command_rc, command_result) = lib.launch_process.launch_command(args)
        if command_rc == 0 and command_result != "":
            return lib.version.Version.create_from_string(command_result)
        checkpoint_handler.Checkpoint("get_system_version.error", "gon_system_win", lib.checkpoint.CRITICAL, command_rc=command_rc, command_result=command_result)
        return None
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("get_system_version.error", "gon_system_win", lib.checkpoint.CRITICAL, etype, evalue, etrace)
    return None


def get_installed_systems_from_registry(checkpoint_handler=None):
    result = []
    with _winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Giritech\G-On 5\Installations") as reg_key_installations:
        reg_key_index = 0
        try:
            while True:
                try:
                    reg_key_installation_sid = _winreg.EnumKey(reg_key_installations, reg_key_index)
                except WindowsError:
                    # no more keys
                    if checkpoint_handler:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("get_installed_systems_from_registry.EnumKey", "gon_system_win", lib.checkpoint.DEBUG, etype, evalue, etrace)
                    break

                try:
                    reg_key_installation = _winreg.OpenKey(reg_key_installations, reg_key_installation_sid)
                    (reg_key_installation_root, reg_key_installation_root_type) = _winreg.QueryValueEx(reg_key_installation, 'installation_root')
                    result_item = (reg_key_installation_sid, reg_key_installation_root)
                    result.append(result_item)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    if checkpoint_handler:
                        checkpoint_handler.CheckpointException("get_installed_systems_from_registry.error", "gon_system_win", lib.checkpoint.WARNING, etype, evalue, etrace)
                    else:
                        print "Error: " + repr(evalue) 
                reg_key_index += 1
        except:
            (etype, evalue, etrace) = sys.exc_info()
            if checkpoint_handler:
                checkpoint_handler.CheckpointException("get_installed_systems_from_registry.error", "gon_system_win", lib.checkpoint.WARNING, etype, evalue, etrace)
            else:
                print "Error: " + repr(evalue) 
    return result
        

def add_installed_system_in_registry(installation_root):
    installed_systems = get_installed_systems_from_registry()
    
    installation_found = False
    for (reg_key_installation_sid, reg_key_installation_root) in installed_systems:
        if installation_root == reg_key_installation_root:
            installation_found = True
            break
    
    if not installation_found:
        reg_key_installation_sid = str(uuid.uuid1())
        reg_key_installation_root = installation_root
        with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Giritech\G-On 5\Installations") as reg_key_installations:
            reg_key_installation = _winreg.CreateKey(reg_key_installations, reg_key_installation_sid)
            _winreg.SetValueEx(reg_key_installation, 'installation_root', 0, _winreg.REG_SZ, reg_key_installation_root)


def _add_system_found(checkpoint_handler, gon_system, gon_systems, gon_systems_roots_found):
    gon_system.version = get_system_version(checkpoint_handler, gon_system.root_folder)
    checkpoint_handler.Checkpoint("_add_system_found", "gon_system_win", lib.checkpoint.INFO, version=gon_system.version, root_folder=gon_system.root_folder)
    if gon_system.version is not None:
        gon_system.valid = True
        
        gon_systems_root = os.path.normpath(gon_system.root_folder)
        if not gon_systems_root in gon_systems_roots_found:
            gon_systems.append(gon_system)
            gon_systems_roots_found.add(gon_systems_root)

class GOnSystemWin(lib.appl.gon_system.GOnSystemBase):
    """
    This class represent a Giritech System on windows
    """
    def __init__(self, system_type):
        lib.appl.gon_system.GOnSystemBase.__init__(self, system_type)

    def generate_backup(self, checkpoint_handler, backup_folder):
        backup_tool_filename = os.path.normpath(os.path.join(self.root_folder, GON_CONFIG_SERVICE))
        if not os.path.exists(backup_tool_filename):
            return (-1, "Unable to find backup tool '%s'" % backup_tool_filename)
        args = [backup_tool_filename, '--backup', '--backup_path', os.path.normpath(backup_folder), '--backup_do_not_create_sub_folder']
        return lib.launch_process.launch_command(args)

    
    @classmethod
    def create_list_installed(cls, checkpoint_handler):
        gon_systems = []
        gon_systems_roots_found = set()
        
        #
        # Look for systems in default location
        #
        GON_FOLDER_PREFIX = 'gon_'
        program_files_root = os.environ.get('ProgramFiles')
        if program_files_root is None or not os.path.isdir(program_files_root):
            hKey = win32api.RegOpenKey (win32con.HKEY_LOCAL_MACHINE, r"SOFTWARE\Microsoft\Windows\CurrentVersion")
            program_files_root, type = win32api.RegQueryValueEx (hKey, "ProgramFilesDir")

        checkpoint_handler.Checkpoint("create_list_installed", "gon_system_win", lib.checkpoint.INFO, msg="Look in default location", dir=program_files_root)
        if program_files_root is not None:
            for folder_item in os.listdir(program_files_root):
                if folder_item.startswith(GON_FOLDER_PREFIX):
                    gon_system = GOnSystemWin(lib.appl.gon_system.GOnSystemBase.SYSTEM_TYPE_INSTALLATION)
                    gon_system.root_folder = os.path.join(program_files_root, folder_item)
                    _add_system_found(checkpoint_handler, gon_system, gon_systems, gon_systems_roots_found)
    
        #
        # Look for systems found in registry
        #
        gon_registry_systems = get_installed_systems_from_registry(checkpoint_handler)
        checkpoint_handler.Checkpoint("create_list_installed", "gon_system_win", lib.checkpoint.INFO, msg="Look in registry", found=len(gon_registry_systems))
        for (reg_key_installation_sid, reg_key_installation_root) in gon_registry_systems:
            gon_system = GOnSystemWin(lib.appl.gon_system.GOnSystemBase.SYSTEM_TYPE_INSTALLATION)
            gon_system.root_folder = reg_key_installation_root
            _add_system_found(checkpoint_handler, gon_system, gon_systems, gon_systems_roots_found)
            
        #
        # Update service info for found systems
        #
        gservices = lib.appl.service.GService.create_list()
        for gon_system in gon_systems:
            for gservice in gservices:
                service_exe = gservice.get_service_exe()
                if service_exe.startswith(gon_system.root_folder):
                    if gservice.is_management_service():
                        gon_system.management_service = gservice
                    if gservice.is_gateway_service():
                        gon_system.gateway_service = gservice
        
        return gon_systems

    
    @classmethod
    def add_installed_system_in_registry(cls, installation_root):
        installed_systems = get_installed_systems_from_registry()
        installation_found = False
        for (reg_key_installation_sid, reg_key_installation_root) in installed_systems:
            if installation_root == reg_key_installation_root:
                installation_found = True
                break
        if not installation_found:
            reg_key_installation_sid = str(uuid.uuid1())
            reg_key_installation_root = installation_root
            with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Giritech\G-On 5\Installations") as reg_key_installations:
                reg_key_installation = _winreg.CreateKey(reg_key_installations, reg_key_installation_sid)
                _winreg.SetValueEx(reg_key_installation, 'installation_root', 0, _winreg.REG_SZ, reg_key_installation_root)

    @classmethod
    def remove_installed_system_from_registry(cls, installation_root):
        installed_systems = get_installed_systems_from_registry()
        reg_key_installation_sid_found = None
        for (reg_key_installation_sid, reg_key_installation_root) in installed_systems:
            if installation_root == reg_key_installation_root:
                reg_key_installation_sid_found = reg_key_installation_sid
                break
            
        if reg_key_installation_sid_found is not None:
            _winreg.DeleteKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Giritech\G-On 5\Installations\%s" % reg_key_installation_sid_found)
            

if __name__ == '__main__':
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    systems = GOnSystemWin.create_list_installed(checkpoint_handler)
    for system in systems:
        print system.get_name()
   
#    GOnSystemWin.remove_installed_system_from_registry('C:\Program Files\gon_5.3.0-6')
#    GOnSystemWin.add_installed_system_in_registry('C:\Program Files\gon_5.3.0-6')
    