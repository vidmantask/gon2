"""
Unittest of upgrade lib
"""
import os
import os.path

import unittest
from lib import giri_unittest

import lib.checkpoint
import lib.appl.upgrade


class UpgradeTest(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def _create_file(self, root, name, content):
        name_abs = os.path.join(root, name)
        folder = os.path.dirname(name_abs)
        if not os.path.exists(folder):
            os.makedirs(folder)
        my_file = open(name_abs, 'wb')
        my_file.write(content)
        my_file.close()
        return name

    def test_upgrade_load_save(self):
        test_root = giri_unittest.mkdtemp()
        
        my_filename_1 = self._create_file(test_root, 'my_file_1', 'hej 1')
        my_filename_2 = self._create_file(test_root, 'my_file_2', 'hej 2')
        my_filename_3 = self._create_file(test_root, os.path.join('my_folder', 'my_file_3'), 'hej 3')
        
        checksums = lib.appl.upgrade.generate_checksums_for_folder(test_root)
        checksums_filename_abs = os.path.join(test_root, 'checksums')
        lib.appl.upgrade.save_checkums(checksums_filename_abs, checksums)

        checksums_loaded = lib.appl.upgrade.load_checkums(checksums_filename_abs)
        self.assertEqual(checksums[my_filename_1], checksums_loaded[my_filename_1])
        self.assertEqual(checksums[my_filename_2], checksums_loaded[my_filename_2])
        self.assertEqual(checksums[my_filename_3], checksums_loaded[my_filename_3])

    def test_include_if_changed(self):
        test_root = giri_unittest.mkdtemp()
        test_root_dest = giri_unittest.mkdtemp()
        
        my_filename_1 = self._create_file(test_root, 'my_file_1', 'hej 1')
        my_filename_2 = self._create_file(test_root, 'my_file_2', 'hej 2')
        my_filename_3 = self._create_file(test_root, os.path.join('my_folder', 'my_file_3'), 'hej 3')
        lib.appl.upgrade.generate_and_save_checksum_for_folder(test_root)

        my_filename_2 = self._create_file(test_root, 'my_file_2', 'hej 2(now changed)')
        lib.appl.upgrade.copy_tree_include_if_changed(test_root, test_root_dest, giri_unittest.get_checkpoint_handler(), 'unittest')

        self.assertTrue(os.path.exists(os.path.join(test_root_dest, my_filename_2)))
        self.assertFalse(os.path.exists(os.path.join(test_root_dest, my_filename_1)))
        self.assertFalse(os.path.exists(os.path.join(test_root_dest, my_filename_3)))


    def test_has_changed(self):
        test_root = giri_unittest.mkdtemp()
        test_root_dest = giri_unittest.mkdtemp()
        
        my_filename_1 = self._create_file(test_root, 'my_file_1', 'hej 1')
        my_filename_2 = self._create_file(test_root, 'my_file_2', 'hej 2')
        lib.appl.upgrade.generate_and_save_checksum_for_folder(test_root)

        self.assertFalse(lib.appl.upgrade.has_changed(test_root, my_filename_1))
        self.assertFalse(lib.appl.upgrade.has_changed(test_root, my_filename_2))

        my_filename_2 = self._create_file(test_root, 'my_file_2', 'hej 2(now changed)')
        self.assertTrue(lib.appl.upgrade.has_changed(test_root, my_filename_2))
        self.assertFalse(lib.appl.upgrade.has_changed(test_root, my_filename_1))




#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();

