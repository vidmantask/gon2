"""
This module contains functionality for handling giritech services
"""
import sys
import os.path
import traceback
import pywintypes
import win32service
import win32serviceutil
import win32api
import winerror
import time
import re

import lib.version
import service
import subprocess
import threading


class GServiceWinService(threading.Thread):
    """
    This class is used for restart/stop of service.
    Notice that fds must be closed
    """
    def __init__(self, service_name, service_exe, command):
        threading.Thread.__init__(self, name="GServiceWinService")
        self.service_name = service_name
        self.service_exe = service_exe
        self.command = command
        self.daemon = True

    def run(self):
        process = subprocess.Popen([self.service_exe, self.command], shell=True, close_fds=True)



class GServiceWin(service.GServiceBase):
    """
    This class represent a windows Giritech Service
    """
    def __init__(self, service_name, service_title, service_description):
        service.GServiceBase.__init__(self, service_name, service_title, service_description)

    def is_running(self):
        try:
            return win32serviceutil.QueryServiceStatus(self.service_name, None)[1] == win32service.SERVICE_RUNNING
        except pywintypes.error:
            return False

    def is_installed(self):
        try:
            status = win32serviceutil.QueryServiceStatus(self.service_name, None)
            return True
        except pywintypes.errocreate_client_computer_token_servicer:
            return False
    
    def is_current_version(self):
        service_name_parts = self.service_name.split(service.SERVICE_NAME_SEPERATOR)
        if len(service_name_parts) < 3:
            return False
        return service_name_parts[1].strip() == lib.version.Version.create_current().get_version_string().strip()

    def get_service_exe(self):
        try:
            installation_location = win32serviceutil.LocateSpecificServiceExe(self.service_name)
            if installation_location.startswith('"'):
                installation_location = installation_location[1:len(installation_location)-1]
            return installation_location
        except pywintypes.error:
            return None
    
    def get_title_and_description_from_service(self):
        service_title = ""
        service_description = ""
        hscm = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_ENUMERATE_SERVICE)
        try:
            hs = win32serviceutil.SmartOpenService(hscm, self.service_name, win32service.SERVICE_QUERY_CONFIG)
            try:
                service_title = win32service.GetServiceDisplayName(hs, self.service_name)
                service_description = win32service.QueryServiceConfig2(hs, win32service.SERVICE_CONFIG_DESCRIPTION)
            finally:
                win32service.CloseServiceHandle(hs)
        finally:
            win32service.CloseServiceHandle(hscm)
        return (service_title, service_description)

    def change(self):
        if not self.is_installed():
            return
        
        serviceName = self.service_name
        displayName = self.service_title
        description = self.service_description
        commandLine = self.get_service_exe()
        startType = win32service.SERVICE_NO_CHANGE
        errorControl = win32service.SERVICE_NO_CHANGE
        serviceDeps = None
        userName = None
        password = None
        exeName = None
        serviceType = win32service.SERVICE_WIN32_OWN_PROCESS
        
        hscm = win32service.OpenSCManager(None,None,win32service.SC_MANAGER_ALL_ACCESS)
        try:
            hs = win32serviceutil.SmartOpenService(hscm, serviceName, win32service.SERVICE_ALL_ACCESS)
            try:
                win32service.ChangeServiceConfig(hs,
                    serviceType,  # service type
                    startType,
                    errorControl,       # error control type
                    commandLine,
                    None,
                    0,
                    serviceDeps,
                    userName,
                    password,
                    displayName)
                if description is not None:
                    try:
                        win32service.ChangeServiceConfig2(hs, win32service.SERVICE_CONFIG_DESCRIPTION, description)
                    except NotImplementedError:
                        pass    ## ChangeServiceConfig2 and description do not exist on NT
    
            finally:
                win32service.CloseServiceHandle(hs)
        finally:
            win32service.CloseServiceHandle(hscm)
           
    def is_management_service(self):
        return self.service_name.find(service.SERVICE_NAME_SUFIX_MANAGEMENT) != -1

    def is_gateway_service(self):
        return self.service_name.find(service.SERVICE_NAME_SUFIX_GATEWAY) != -1

    def install(self, exe_command_line, user_name=None, password=None, start_service=False):
        if self.is_installed():
            return

        if not os.path.exists(exe_command_line):
            raise service.GServiceError("Service install error, unable to locate command '%s'" % exe_command_line)

        service_manager_handler = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_ALL_ACCESS)
        try:
            service_handler = win32service.CreateService(service_manager_handler, 
                                                         self.service_name, 
                                                         self.service_title,
                                                         win32service.SERVICE_ALL_ACCESS,         
                                                         win32service.SERVICE_WIN32_OWN_PROCESS,  
                                                         win32service.SERVICE_DEMAND_START,
                                     
                                                         win32service.SERVICE_ERROR_NORMAL,       
                                                         exe_command_line,
                                                         None,
                                                         0,
                                                         None,
                                                         user_name,
                                                         password)
            try:
                win32service.ChangeServiceConfig2(service_handler ,win32service.SERVICE_CONFIG_DESCRIPTION, self.service_description)
            except NotImplementedError:
                pass    ## ChangeServiceConfig2 and description do not exist on NT

            if start_service:
                self._start(service_handler, 15)
            
            win32service.CloseServiceHandle(service_handler)
            
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Service install error, %s' % evalue)
        finally:
            win32service.CloseServiceHandle(service_manager_handler)

    def remove(self):
        service_manager_handler = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_ALL_ACCESS)
        try:
            service_handler = win32service.OpenService(service_manager_handler, self.service_name, win32service.SERVICE_ALL_ACCESS)
            if self.is_running():
                self._stop(service_handler, 15)
            win32service.DeleteService(service_handler)
            win32service.CloseServiceHandle(service_handler)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when trying to remove service: %s' % evalue)
        finally:
            win32service.CloseServiceHandle(service_manager_handler)
    
    def _stop(self, service_handler, timeout_sec):
        try:
            status = win32service.ControlService(service_handler, win32service.SERVICE_CONTROL_STOP)
        except pywintypes.error, (hr, name, msg):
            if hr!=winerror.ERROR_SERVICE_NOT_ACTIVE:
                raise win32service.error, (hr, name, msg)
        for i in range(timeout_sec):
            status = win32service.QueryServiceStatus(service_handler)
            if status[1] == win32service.SERVICE_STOPPED:
                break
            win32api.Sleep(1000)
        else:
            raise pywintypes.error, (winerror.ERROR_SERVICE_REQUEST_TIMEOUT, "ControlService.stop", win32api.FormatMessage(winerror.ERROR_SERVICE_REQUEST_TIMEOUT)[:-2])

    
    def _start(self, service_handler, timeout_sec):
        try:
            status = win32service.StartService(service_handler, None)
        except pywintypes.error, (hr, name, msg):
            if hr!=winerror.ERROR_SERVICE_NOT_ACTIVE:
                raise win32service.error, (hr, name, msg)
        for i in range(timeout_sec):
            status = win32service.QueryServiceStatus(service_handler)
            if status[1] == win32service.SERVICE_RUNNING:
                break
            win32api.Sleep(1000)
        else:
            raise pywintypes.error, (winerror.ERROR_SERVICE_REQUEST_TIMEOUT, "ControlService.start", win32api.FormatMessage(winerror.ERROR_SERVICE_REQUEST_TIMEOUT)[:-2])
        
    def stop_and_forget(self):
        try:
            bg_job = GServiceWinService(self.service_name, self.get_service_exe(), 'stop')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when stop_and_forget service: %s' % evalue)

    def restart_and_forget(self):
        try:
            bg_job = GServiceWinService(self.service_name, self.get_service_exe(), 'restart')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when restart_and_forget service: %s' % evalue)

    def start_and_forget(self):
        try:
            bg_job = GServiceWinService(self.service_name, self.get_service_exe(), 'start')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when start_and_forget service: %s' % evalue)
        

    def update_description(self, service_description_new):
        if self.service_description != service_description_new:
            self.service_description = service_description_new
            self.change()

    @classmethod
    def create_from_service_name(cls, service_name):
        service = GServiceWin(service_name, "", "")
        if service.is_installed():
            (service.service_title, service.service_description) = service.get_title_and_description_from_service() 
        return service
    
    @classmethod
    def create_list(cls):
        gservices = []
        service_manager = win32service.OpenSCManager(None, None, win32service.SC_MANAGER_ENUMERATE_SERVICE)
        for service_name, service_title, service_status in win32service.EnumServicesStatus(service_manager):
            if service_name.startswith(service.SERVICE_NAME_PREFIX):
                service_description = ''
                gservices.append(GServiceWin(service_name, service_title, service_description))
        win32service.CloseServiceHandle(service_manager)
        return gservices

    @classmethod
    def generate_unique_service_name(cls, service_name_base):
        service_names = []
        for existing_service in GServiceWin.create_list():
            service_names.append(existing_service.service_name)
        id = 1
        service_name_unique = '%s(%d)' % (service_name_base, id)
        while(service_name_unique in service_names):
            id += 1
            service_name_unique = '%s(%d)' % (service_name_base, id)
        return (service_name_unique, id)

    @classmethod
    def create_management_service(cls):
        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_MANAGEMENT
        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_MANAGEMENT, id, lib.version.Version.create_current().get_version_string().strip())
        service_description = ''
        return GServiceWin(service_name, service_title, service_description)

    @classmethod
    def create_gateway_service(cls):
        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_GATEWAY;
        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_GATEWAY, id, lib.version.Version.create_current().get_version_string().strip())
        service_description = ''
        return GServiceWin(service_name, service_title, service_description)

    @classmethod
    def create_client_device_service(cls):
        service_name = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_CLIENT_DEVICE;
        service_title = '%s %s' %(service.SERVICE_TITLE_CLIENT_DEVICE, lib.version.Version.create_current().get_version_string().strip())
        service_description = ''
        return GServiceWin(service_name, service_title, service_description)

    @classmethod
    def create_service_from_current_location(cls):
        if len(sys.argv) > 0:
            current_location = win32api.GetFullPathName(sys.argv[0])
            for gservice in GServiceWin.create_list(): 
                if gservice.get_service_exe() == current_location:
                    return gservice
        return None

    @classmethod
    def create_service_for_current_management_service(cls, root):
        root_normalized = os.path.normcase(os.path.normpath(root))
        filter_re = re.compile(service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + ".*" + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_MANAGEMENT + ".*")
        for installed_service in GServiceWin.create_list():
            path = installed_service.get_service_exe()
            if path:
                service_exe_normalized = os.path.normcase(os.path.normpath(path))
                if filter_re.match(installed_service.service_name):
                    if service_exe_normalized.startswith(root_normalized):
                        return installed_service
        return None


if __name__ == '__main__':
    gservices = GServiceWin.create_list()
    for gservice in gservices:
        print gservice.service_name, gservice.is_running(), gservice.is_installed(), gservice.is_current_version(), gservice.get_service_exe() 

    current_root = "C:\Program Files\Giritech\gon_5.5.0-21"
    current_management_service = GServiceWin.create_service_for_current_management_service(root=current_root) 

    print current_management_service.service_name, current_management_service.is_current_version(), current_management_service.get_service_exe() 

#    gservice_management = GServiceWin.create_management_service(8000)
#    if gservice_management.is_installed():
#        gservice_management.remove()
#    gservice_management.install('C:\\tmp\\giritech win_5.1.1-1\\gon_client_admin_service_service\\win\\gon_client_admin_service_service.exe')
    
#    gservice_management.change()
    
#    gservice_gateway = GServiceWin.create_gateway_service(8001, 1)
#    if gservice_gateway.is_installed():
#        gservice_gateway.remove()
#    gservice_gateway.install('c:\\')
#
#
#
#
#    gservices = GServiceWin.create_list()
#    for gservice in gservices:
#        print gservice.service_name, gservice.is_running(), gservice.is_installed(), gservice.is_current_version() 
#
#    
