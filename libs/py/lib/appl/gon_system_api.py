"""
This module contains functionality for finding installed G/On Systems
"""
import sys
if sys.platform == 'win32':
    import lib.appl.gon_system_win
    GOnSystem = lib.appl.gon_system_win.GOnSystemWin
elif sys.platform == 'linux2':
    import lib.appl.gon_system
    GOnSystem = lib.appl.gon_system.GOnSystemBase
