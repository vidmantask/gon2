"""
Module holding functionality for creating shortcuts 
"""
import sys
import os.path
if sys.platform == 'win32':
    import pythoncom
    import win32com.client

import lib.encode
import lib.checkpoint

MODULE_ID = 'shortcut'

def create_shortcut(checkpoint_handler, shortcut_folder, shortcut_name, shortcut_target, shortcut_arguments=None, shortcut_working_folder=None):
    if sys.platform == 'win32':
        return _create_shortcut_win(checkpoint_handler, shortcut_folder, shortcut_name, shortcut_target, shortcut_arguments, shortcut_working_folder)
    else:
        checkpoint_handler.Checkpoint("create_shortcut.platform_not_supported", MODULE_ID, lib.checkpoint.WARNING, shortcut_folder=shortcut_folder, shortcut_name=shortcut_name)
    return None

def _create_shortcut_win(checkpoint_handler, shortcut_folder, shortcut_name, shortcut_target, shortcut_arguments=None, shortcut_working_folder=None):
    try:
        pythoncom.CoInitialize() 
        win_shell = win32com.client.Dispatch("WScript.Shell")
        start_menu_programs = win_shell.SpecialFolders("Programs")
    
        shortcut_folder_path = os.path.join(start_menu_programs, shortcut_folder)
        if (not os.path.exists(shortcut_folder_path)):
            os.makedirs(shortcut_folder_path)
        
        shortcut_link_path = os.path.join(shortcut_folder_path, shortcut_name + ".lnk")
        shortcut_link = win_shell.CreateShortcut(shortcut_link_path) 
        shortcut_link.TargetPath = lib.encode.preferred(shortcut_target)
        if shortcut_arguments is not None:
            shortcut_link.Arguments = lib.encode.preferred(shortcut_arguments)
        if shortcut_working_folder is not None:
            shortcut_link.WorkingDirectory = lib.encode.preferred(shortcut_working_folder)
        shortcut_link.Save()
        return shortcut_link_path
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("create_shortcut.unexpected_error", MODULE_ID, lib.checkpoint.WARNING, etype, evalue, etrace)
        return None
        
if __name__ == '__main__':
    create_shortcut(None, 'Giritech', 'G-On Clientx', 'C:\Documents and Settings//twa.DEV-BUILD-XP//Application Data\Giritech\G-On Client\gon_client\win\gon_client.exe')

