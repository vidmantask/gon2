"""
This module holds configuration utility for handling ini-files
"""
import os
import os.path
import ConfigParser
import StringIO

import lib.utility.support_file_filter as support_file_filter
import lib.utility.xor_crypt as xor_crypt


def convert_intlist_to_string(value_list_int):
    value_list_string = []
    for value_int in value_list_int:
        value_list_string.append(str(value_int))
    return ', '.join(value_list_string)

def convert_string_to_intlist(value_list_string):
    result = []
    value_list = value_list_string.split(',')
    for value in value_list:
        value = value.strip()
        if value.isdigit():
            result.append(int(value))
    return result

def convert_stringlist_to_string(value_list):
    return ', '.join(value_list)
    
def convert_string_to_stringlist(value_list_string):
    result = []
    value_list = value_list_string.split(',')
    for value in value_list:
        result.append(value.strip())
    return result


class ConfigBase(object):
    BOOLEAN_VALUE_TRUE = 'True'
    BOOLEAN_VALUE_FALSE = 'False'
    
    PASSWORD_SALT_SAFE = 'jp xcite 130'
    PASSWORD_SALT_SAFE_ERROR = 'invalid_salt_used'
    
    def __init__(self, config_filename_prefix):
        self.config_filename_prefix = config_filename_prefix
        self.config = ConfigParser.ConfigParser()
        self._ini_path = '.'
    
    def reset_to_default_when_restore_durring_upgrade(self):
        """ 
        This method is called when a ini-file is restored from backup durring upgrade. 
        """
        pass
    
    def set_ini_path(self, ini_path):
        self._ini_path = ini_path
        
    def _get_default_section_name(self, section_name):
        return '%s_default' % section_name

    def _get_non_default_section_name(self, section_name):
        if self._is_default_section_name(section_name):
            return section_name[:len(section_name)-len('_default')]
        return None

    def _is_default_section_name(self, section_name):
        return section_name.endswith('_default')
        
    def set_default(self, section_name, key, value):
        default_section_name = self._get_default_section_name(section_name)
        if not self.config.has_section(default_section_name):
            self.config.add_section(default_section_name)
        if self.config.has_option(default_section_name, key):
            self.config.remove_option(default_section_name, key)
        self.config.set(default_section_name, key, value)

    def reset_to_default(self, section_name, key):
        if self.config.has_option(section_name, key):
            self.config.remove_option(section_name, key)
        
    def get_default(self, section_name, key):
        default_section_name = self._get_default_section_name(section_name)
        return self.config.get(default_section_name, key)

    def get(self, section_name, key):
        if self.config.has_option(section_name, key):
            return self.config.get(section_name, key)
        default_section_name = self._get_default_section_name(section_name)
        return self.config.get(default_section_name, key)

    def get_abspath(self, section_name, key):
        if self.config.has_option(section_name, key):
            return os.path.join(self._ini_path, self.config.get(section_name, key))
        default_section_name = self._get_default_section_name(section_name)
        return os.path.normpath(os.path.join(self._ini_path, self.config.get(default_section_name, key)))

    def set(self, section_name, key, value):
        if not self.config.has_section(section_name):
            self.config.add_section(section_name)
        self.config.set(section_name, key, value)

    def getlist(self, section_name, key):
        if self.config.has_option(section_name, key):
            return convert_string_to_stringlist(self.config.get(section_name, key))
        default_section_name = self._get_default_section_name(section_name)
        return convert_string_to_stringlist(self.config.get(default_section_name, key))

    def setlist(self, section_name, key, value_list):
        if not self.config.has_section(section_name):
            self.config.add_section(section_name)
        if value_list == '' or value_list is None:
            value_list = []
        value = ','.join(value_list)
        self.config.set(section_name, key, value)

    def getint(self, section_name, key):
        if self.config.has_option(section_name, key):
            return self.config.getint(section_name, key)
        default_section_name = self._get_default_section_name(section_name)
        return self.config.getint(default_section_name, key)

    def getintlist(self, section_name, key):
        if self.config.has_option(section_name, key):
            return convert_string_to_intlist(self.config.get(section_name, key))
        default_section_name = self._get_default_section_name(section_name)
        return convert_string_to_intlist(self.config.get(default_section_name, key))

    def setintlist(self, section_name, key, value_list):
        if not self.config.has_section(section_name):
            self.config.add_section(section_name)
        value = convert_intlist_to_string(value_list)
        self.config.set(section_name, key, value)

    def getfloat(self, section_name, key):
        if self.config.has_option(section_name, key):
            return self.config.getint(section_name, key)
        default_section_name = self._get_default_section_name(section_name)
        return self.config.getfloat(default_section_name, key)

    def getboolean(self, section_name, key):
        if self.config.has_option(section_name, key):
            return self.config.getboolean(section_name, key)
        default_section_name = self._get_default_section_name(section_name)
        return self.config.getboolean(default_section_name, key)
    
    def get_password(self, section_name, key):
        value_incl_safe = xor_crypt.xor_decrypt(self.get(section_name, key))
        if value_incl_safe:
            safe = value_incl_safe[:len(ConfigBase.PASSWORD_SALT_SAFE)]
            if safe == ConfigBase.PASSWORD_SALT_SAFE:
                return value_incl_safe[len(ConfigBase.PASSWORD_SALT_SAFE):]
            return "invalid_salt_used"
        return ""

    def set_password(self, section_name, key, value):
        value_incl_safe = xor_crypt.xor_crypt(ConfigBase.PASSWORD_SALT_SAFE + value)
        self.set(section_name, key, value_incl_safe)
        
    def has_option(self, section_name, key):
        return self.config.has_option(section_name, key)
    
    def read_config_file(self, config_filename_abs):
        self._ini_path = os.path.dirname(config_filename_abs)
        if os.path.isfile(config_filename_abs):
            config_file = open(config_filename_abs, 'r')
            self.config.readfp(config_file)
            config_file.close()
            
    def read_config_file_from_folder(self, config_path):
        config_filename = os.path.join(config_path, '%s.ini' % self.config_filename_prefix)
        self._ini_path = os.path.dirname(config_filename)
        if os.path.isfile(config_filename):
            config_file = open(config_filename, 'r')
            self.config.readfp(config_file)
            config_file.close()
            
    def write_value(self, config_human, option_name, option_value, as_comment, remove_sensitive_info):
        if remove_sensitive_info and support_file_filter.contains_sensitive_information(option_name):
            config_human.write(support_file_filter.sensitive_replace_string)
        else:
            if as_comment:
                config_human.write('#%s = %s\n' % (option_name, option_value))
            else:
                config_human.write('%s = %s\n' % (option_name, option_value))
            

    def write_config_file(self, config_path, remove_sensitive_info=False):
        config_human = StringIO.StringIO()

        for section_name in self.config.sections():
            items = {}
            if not self._is_default_section_name(section_name):
                section_name_default = self._get_default_section_name(section_name)

                # Generate set of key/value/default_value for the section
                for option_name, option_value in self.config.items(section_name):
                    item_values = {}
                    item_values['value'] = option_value.strip()
                    item_values['default_value'] = None
                    if self.config.has_option(section_name_default, option_name):
                        item_values['default_value'] = self.config.get(section_name_default, option_name).strip()
                    items[option_name] = item_values
                
                if self.config.has_section(section_name_default):
                    for option_name, option_value_default in self.config.items(section_name_default):
                        if option_name not in items:
                            item_values = {}
                            item_values['value'] = None
                            item_values['default_value'] = option_value_default
                            items[option_name] = item_values

                config_human.write('[%s]\n' % (section_name) )
                for option_name in items:
                    option_value_default = items[option_name]['default_value']
                    if option_value_default is not None:
                        self.write_value(config_human, option_name, option_value_default, True, remove_sensitive_info)
                
                for option_name in items:
                    option_value = items[option_name]['value']
                    option_value_default = items[option_name]['default_value']
                    if option_value is not None and option_value != option_value_default:
                        self.write_value(config_human, option_name, option_value, False, remove_sensitive_info)
                config_human.write('\n')

            else:
                section_name_non_default = self._get_non_default_section_name(section_name)
                if not self.config.has_section(section_name_non_default):
                    config_human.write('[%s]\n' % (section_name_non_default) )
                    for option_name, option_value_default in self.config.items(section_name):
                        self.write_value(config_human, option_name, option_value_default, True, remove_sensitive_info)
                    config_human.write('\n')
        
        if not os.path.exists(config_path):
            os.makedirs(config_path)
        config_filename = os.path.join(config_path, '%s.ini' % self.config_filename_prefix)
        config_file = open(config_filename, 'w')
        config_file.write(config_human.getvalue())
        config_file.close()

    def sensitive_copy(self, src_folder, dest_folder):
        src_config_filename  = os.path.join(src_folder, '%s.ini' % self.config_filename_prefix)
        dest_config_filename = os.path.join(dest_folder, '%s.ini.org' % self.config_filename_prefix)
        if os.path.isfile(src_config_filename):
            support_file_filter.sensitive_copy_file(src_config_filename, dest_config_filename)
        else:
            dest_config_file = open(dest_config_filename, 'w')
            dest_config_file.write('# This is a fake ini-file created during support package generation\n')
            dest_config_file.write('# because the ini-file "%s" was not found\n' % src_config_filename)
            dest_config_file.close()

    @classmethod
    def INT_VALUE(cls, int_value):
        return str(int_value)

    @classmethod
    def FLOAT_VALUE(cls, float_value):
        return str(float_value)

    def do_set_defaults(self):
        raise NotImplementedError

    def get_ini_path(self):
        return self._ini_path


def get_with_none(config, section_name, key):
    if config.has_option(section_name, key):
        return config.get(section_name, key)
    return None

def getint_with_none(config, section_name, key):
    if config.has_option(section_name, key):
        return config.getint(section_name, key)
    return None

def getboolean_with_none(config, section_name, key):
    if config.has_option(section_name, key):
        return config.getboolean(section_name, key)
    return None

def def_property(section_name, key):
    return property(lambda self: self.get(section_name, key), lambda self, value: self.set(section_name, key, value))

def def_property_with_none(section_name, key):
    return property(lambda self: get_with_none(self, section_name, key), lambda self, value: self.set(section_name, key, value))

def def_property_default(section_name, key):
    return property(lambda self: self.get_default(section_name, key))

def def_property_list(section_name, key):
    return property(lambda self: self.getlist(section_name, key), lambda self, value: self.setlist(section_name, key, value))

def def_property_boolean(section_name, key):
    return property(lambda self: self.getboolean(section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_boolean_with_none(section_name, key):
    return property(lambda self: getboolean_with_none(self, section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_int(section_name, key):
    return property(lambda self: self.getint(section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_int_with_none(section_name, key):
    return property(lambda self: getint_with_none(self, section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_intlist(section_name, key):
    return property(lambda self: self.getintlist(section_name, key), lambda self, value: self.setintlist(section_name, key, value))

def def_property_float(section_name, key):
    return property(lambda self: self.getfloat(section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_abspath(section_name, key):
    return property(lambda self: self.get_abspath(section_name, key), lambda self, value: self.set(section_name, key, str(value)))

def def_property_password(section_name, key):
    return property(lambda self: self.get_password(section_name, key), lambda self, value: self.set_password(section_name, key, value))
