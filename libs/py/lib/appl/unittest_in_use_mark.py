"""
Unittest of in_use_mark module
"""
import os
import os.path
import time

import unittest
from lib import giri_unittest

import lib.appl.in_use_mark


class InUseMarkTest(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    def test_in_use_mark(self):
        test_root = giri_unittest.mkdtemp()
        
        in_use_mark = lib.appl.in_use_mark.InUseMark(test_root)
        self.assertFalse(in_use_mark.is_in_use())

        in_use_mark.do_mark_in_use()
        self.assertTrue(in_use_mark.is_in_use())
        
        in_use_mark.do_mark_in_use()
        self.assertTrue(in_use_mark.is_in_use())

        in_use_mark.do_mark_not_in_use()
        self.assertFalse(in_use_mark.is_in_use())


    def test_in_use_mark_timeout(self):
        test_root = giri_unittest.mkdtemp()

        in_use_mark = lib.appl.in_use_mark.InUseMark(test_root)
        in_use_mark.do_mark_in_use()

        time.sleep(3)
        self.assertFalse(in_use_mark.is_in_use(2))
        self.assertTrue(in_use_mark.is_in_use())
        self.assertTrue(in_use_mark.is_in_use(10))
        
        
        in_use_mark.do_mark_not_in_use()
        self.assertFalse(in_use_mark.is_in_use())


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
