"""
This module contains functionality for handling giritech services for linux
"""
import sys
import os.path
import traceback
import time
import re

import lib.version
import service
import subprocess
import threading
import glob


SERVICE_ROOT_FOLDER = '/etc/init.d'

SERVICE_NAME_SUFIX_MANAGEMENT = u'gon_server_management_service'
SERVICE_NAME_SUFIX_GATEWAY = u'gon_server_gateway_service'


class GServiceLinuxService(threading.Thread):
    """
    This class is used for restart/stop of service.
    Notice that fds must be closed
    """
    def __init__(self, service_name, command):
        threading.Thread.__init__(self, name="GServiceLinuxService")
        self.service_name = service_name
        self.command = command
        self.daemon = True

    def run(self):
        print "GServiceLinuxService", ['service', self.service_name, self.command]
        process = subprocess.Popen(['service', self.service_name, self.command])
        process.wait()


class GServiceLinux(service.GServiceBase):
    def __init__(self, service_name, service_title, service_description):
        service.GServiceBase.__init__(self, service_name, service_title, service_description)

    def is_running(self):
        command = ['service', self.service_name, 'status']
        process = subprocess.Popen(command, stdout=subprocess.PIPE)
        process.wait()
        (stdoutdata, _) = process.communicate()
        print "GServiceLinux.is_running", stdoutdata
        print "GServiceLinux.is_running", stdoutdata.startswith("running")
        if stdoutdata is not None:
            return stdoutdata.startswith("running")
        return False

    def is_installed(self):
        service_filename = os.path.join(SERVICE_ROOT_FOLDER, self.service_name)
        return os.path.exists(service_filename)
    
    def is_current_version(self):
        return False

    def is_management_service(self):
        return self.service_name.find(SERVICE_NAME_SUFIX_MANAGEMENT) != -1

    def is_gateway_service(self):
        return self.service_name.find(SERVICE_NAME_SUFIX_GATEWAY) != -1
        
    def stop_and_forget(self):
        try:
            bg_job = GServiceLinuxService(self.service_name, 'stop')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when stop_and_forget service: %s' % evalue)

    def restart_and_forget(self):
        try:
            bg_job = GServiceLinuxService(self.service_name, 'restart')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when restart_and_forget service: %s' % evalue)

    def start_and_forget(self):
        try:
            bg_job = GServiceLinuxService(self.service_name, 'start')
            bg_job.start() 
        except:
            (etype, evalue, etrace) = sys.exc_info()
            raise service.GServiceError('Error when start_and_forget service: %s' % evalue)
        
    def update_description(self, service_description_new):
        if self.service_description != service_description_new:
            self.service_description = service_description_new
            self.change()

    @classmethod
    def create_from_service_name(cls, service_name):
        service = GServiceLinux(service_name, "", "")
        return service
    
    @classmethod
    def create_list(cls):
        gservices = []
        for service_filename in glob.glob(os.path.join(SERVICE_ROOT_FOLDER, 'gon_server*')):
            service_name = os.path.basename(service_filename)
            gservices.append(GServiceLinux(service_name, "", ""))
        return gservices

#    @classmethod
#    def create_management_service(cls):
#        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_MANAGEMENT
#        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
#        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_MANAGEMENT, id, lib.version.Version.create_current().get_version_string().strip())
#        service_description = ''
#        return GServiceWin(service_name, service_title, service_description)
#
#    @classmethod
#    def create_gateway_service(cls):
#        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_GATEWAY;
#        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
#        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_GATEWAY, id, lib.version.Version.create_current().get_version_string().strip())
#        service_description = ''
#        return GServiceWin(service_name, service_title, service_description)
#
#    @classmethod
#    def create_client_device_service(cls):
#        service_name = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_CLIENT_DEVICE;
#        service_title = '%s %s' %(service.SERVICE_T#    @classmethod
#    def create_management_service(cls):
#        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_MANAGEMENT
#        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
#        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_MANAGEMENT, id, lib.version.Version.create_current().get_version_string().strip())
#        service_description = ''
#        return GServiceWin(service_name, service_title, service_description)
#
#    @classmethod
#    def create_gateway_service(cls):
#        service_name_base = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_GATEWAY;
#        (service_name, id) = GServiceWin.generate_unique_service_name(service_name_base)
#        service_title = '%s(%d) %s' %(service.SERVICE_TITLE_GATEWAY, id, lib.version.Version.create_current().get_version_string().strip())
#        service_description = ''
#        return GServiceWin(service_name, service_title, service_description)
#
#    @classmethod
#    def create_client_device_service(cls):
#        service_name = service.SERVICE_NAME_PREFIX + service.SERVICE_NAME_SEPERATOR + lib.version.Version.create_current().get_version_string().strip() + service.SERVICE_NAME_SEPERATOR + service.SERVICE_NAME_SUFIX_CLIENT_DEVICE;
#        service_title = '%s %s' %(service.SERVICE_TITLE_CLIENT_DEVICE, lib.version.Version.create_current().get_version_string().strip())
#        service_description = ''
#        return GServiceWin(service_name, service_title, service_description)
#
#    @classmethod
#    def create_service_from_current_location(cls):
#        if len(sys.argv) > 0:
#            current_location = win32api.GetFullPathName(sys.argv[0])
#            for gservice in GServiceWin.create_list():     print "hej"
#                if gservice.get_service_exe() == current_location:
#                    return gservice
#        return None

    @classmethod
    def create_service_for_current_management_service(cls, root):
        instance_name = os.path.basename(root)
        if instance_name == '':
            instance_name = os.path.basename(os.path.dirname(root))
        service_name = "%s-gon_server_management_service" % instance_name
        return GServiceLinux(service_name, 'G/On Management Server', '')


if __name__ == '__main__':
#    gservices = GServiceLinux.create_list()
#    for gservice in gservices:
#        print gservice.service_name, gservice.is_management_service(), gservice.is_gateway_service(), gservice.is_installed(), gservice.is_running()

    instance_path = '/opt/excitor/instance/gon_server-5.6.0.4-1/'
    gservice = GServiceLinux.create_service_for_current_management_service(instance_path)
    print gservice.service_name, gservice.is_management_service(), gservice.is_gateway_service(), gservice.is_installed(), gservice.is_running()
