'''
Created on Mar 9, 2011

@author: thwang
'''
import os.path

VERSION_INFO_GON_OS = "G/On OS"


def is_gon_os():
    try:
        etc_system_release_filename = os.path.join('/', 'etc', 'system-release')
        if os.path.isfile(etc_system_release_filename):
            version_info = open(etc_system_release_filename).read().split(' (', 1)[0].split(' release ')
            return len(version_info) == 2 and version_info[0] == VERSION_INFO_GON_OS
    except:
        pass
    return False

