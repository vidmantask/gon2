"""
This module contains functionality for finding installed G/On Systems on linux
"""
import sys
import os
import subprocess
import time
import shutil
import tempfile
import traceback
import glob

import uuid

import lib.appl.service
import lib.appl.gon_system
import lib.version
import lib.checkpoint
import lib.launch_process



GON_CONFIG_SERVICE = os.path.join('gon_config_service', 'linux', 'gon_config_service')




def get_system_version(checkpoint_handler, instance_root):
    try:
        soft_version_elements = os.path.basename(instance_root).split('-')
        soft_version = '-'.join(soft_version_elements[:2])
        soft_root = os.path.join('/', 'opt', 'giritech', 'soft', soft_version)
        gon_config_service_path = os.path.normpath(os.path.join(soft_root, GON_CONFIG_SERVICE))
        
        command = [gon_config_service_path, '--version']
        process = subprocess.Popen(command, stdout=subprocess.PIPE)
        process.wait()
        (command_result, _) = process.communicate()
        return lib.version.Version.create_from_string(command_result)
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("get_system_version.error", "gon_system_linux", lib.checkpoint.CRITICAL, etype, evalue, etrace)
    return None



class GOnSystemLinux(lib.appl.gon_system.GOnSystemBase):
    """
    This class represent a Giritech System on Linux
    """
    def __init__(self, system_type):
        lib.appl.gon_system.GOnSystemBase.__init__(self, system_type)

    def generate_backup(self, checkpoint_handler, backup_folder):
        backup_tool_filename = os.path.normpath(os.path.join(self.root_folder, GON_CONFIG_SERVICE))
        if not os.path.exists(backup_tool_filename):
            return (-1, "Unable to find backup tool '%s'" % backup_tool_filename)
        args = [backup_tool_filename, '--backup', '--backup_path', os.path.normpath(backup_folder), '--backup_do_not_create_sub_folder']
        return lib.launch_process.launch_command(args)

    
    @classmethod
    def create_list_installed(cls, checkpoint_handler):
        gon_systems = []
        
        #
        # Look for systems in default location
        #
        instance_root = os.path.join('/', 'opt', 'giritech', 'instance')
        for instance_folder in glob.glob(os.path.join(instance_root, 'gon_server*')):
            gon_system = GOnSystemLinux(lib.appl.gon_system.GOnSystemBase.SYSTEM_TYPE_INSTALLATION)
            gon_system.root_folder = instance_folder
            gon_system.version = get_system_version(checkpoint_handler, gon_system.root_folder)
            if gon_system.version is not None:
                gon_system.valid = True
            gon_systems.append(gon_system) 
                
        #
        # Update service info for found systems
        #
        gservices = lib.appl.service.GService.create_list()
        for gon_system in gon_systems:
            for gservice in gservices:
                gservice_service_name = gservice.service_name
                gon_system_service_name_prefix = os.path.basename(gon_system.root_folder)
                if gservice_service_name.startswith(gon_system_service_name_prefix):
                    if gservice.is_management_service():
                        gon_system.management_service = gservice
                    if gservice.is_gateway_service():
                        gon_system.gateway_service = gservice
        return gon_systems


if __name__ == '__main__':
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    systems = GOnSystemLinux.create_list_installed(checkpoint_handler)
    for system in systems:
        print system.get_name(), system.is_current_version(), system.gateway_service, system.management_service
   
    