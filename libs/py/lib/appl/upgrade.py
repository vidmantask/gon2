"""
Upgrade lib
"""
import os
import os.path
import hashlib
import pickle
import shutil

import lib.commongon
import lib.checkpoint
import lib.utility

CONFIG_FOLDER = 'config'
PLUGIN_FOLDER = 'plugin'
DATABASE_FOLDER = 'database'

VERSION_FILENAME = 'version'


CHECKSUMS_FILENAME = 'checksums'
CHECKSUMS_LOCAL_FILENAME = 'checksums_local'

def generate_checksums_for_folder(folder_root):
    result = {}
    for root, dirs, files in os.walk(folder_root):
        for filename in files:
            filename_abs = os.path.join(root, filename)
            filename_rel = os.path.join(lib.commongon.common_path_sufix(root, folder_root), filename)
            file_obj = file(filename_abs, 'rb')
            hash = hashlib.sha1(file_obj.read()).hexdigest()
            file_obj.close()

            filename_key = os.path.normpath(filename_rel)
            result[filename_key] = {'hash':hash}
    return result

def cleanup_checksums_for_folder(checksums, folder_root):
    filename_keys = []
    for root, dirs, files in os.walk(folder_root):
        for filename in files:
            filename_rel = os.path.join(lib.commongon.common_path_sufix(root, folder_root), filename)
            filename_keys.append(os.path.normpath(filename_rel))

    for checksum_key in checksums.keys():
        if checksum_key not in filename_keys:
            del checksums[checksum_key]

def save_checkums(checksums_filename_abs, checksums):
    checksums_filename_folder = os.path.dirname(checksums_filename_abs)
    if not os.path.exists(checksums_filename_folder):
        os.makedirs(checksums_filename_folder)
    checksums_file = open(checksums_filename_abs, 'wb')
    pickle.dump(checksums, checksums_file)
    checksums_file.close()

def load_checkums(checksums_filename_abs):
    result = []
    if os.path.isfile(checksums_filename_abs):
        checksums_file = open(checksums_filename_abs, 'rb')
        result = pickle.load(checksums_file)
        checksums_file.close()
    else:
        print "load_checkums.not_a_file filename:", checksums_filename_abs
    return result

def generate_and_save_checksum_for_folder(folder_root, checksums_filename=CHECKSUMS_FILENAME):
    checksums_filename_abs = os.path.join(folder_root, checksums_filename)
    checksums = generate_checksums_for_folder(folder_root)
    save_checkums(checksums_filename_abs, checksums)

def update_checksum_for_folder(folder_root, checksums_filename=CHECKSUMS_FILENAME):
    """
    Checksum for a folder is updated. Update means that existing checksums are keept,
    and new files are added. Checksums for files no longer in folder are removed.
    """
    checksums_filename_abs = os.path.join(folder_root, checksums_filename)
    old_checksums = load_checkums(checksums_filename_abs)
    new_checksums = generate_checksums_for_folder(folder_root)
    new_checksums.update(old_checksums)
    cleanup_checksums_for_folder(new_checksums, folder_root)
    save_checkums(checksums_filename_abs, new_checksums)


def has_changed(folder_root, filename, checksums_filename=CHECKSUMS_FILENAME):
    checksums_filename_abs = os.path.join(folder_root, checksums_filename)
    if not os.path.exists(checksums_filename_abs):
        return True
    checksums = load_checkums(checksums_filename_abs)

    filename_abs = os.path.join(folder_root, filename)
    filename_key = os.path.normpath(filename)
    if checksums.has_key(filename_key):
        hash = lib.utility.calculate_checksum_for_file(filename_abs)
        if hash == checksums[filename_key].get('hash', None):
            return False
    return True

def copy_tree_include_if_changed(folder_source, folder_dest, logger, module_id):
    checksums_filename_abs = os.path.join(folder_source, CHECKSUMS_FILENAME)
    checksums = {}
    if os.path.exists(checksums_filename_abs):
        checksums = load_checkums(checksums_filename_abs)
    else:
        logger.Checkpoint("copy_templates.checksum_file_not_found", module_id, lib.checkpoint.WARNING, checksums_filename=checksums_filename_abs)

    for root, dirs, files in os.walk(folder_source):
        for filename in files:
            new_file = True
            filename_abs = os.path.join(root, filename)
            filename_rel = os.path.join(lib.commongon.common_path_sufix(root, folder_source), filename)
            filename_key = os.path.normpath(filename_rel)
            if checksums.has_key(filename_key):
                new_file = False
                hash = lib.utility.calculate_checksum_for_file(filename_abs)
                if hash == checksums[filename_key].get('hash', None):
                    logger.Checkpoint("copy_tree.skipping_file_not_changed", module_id, lib.checkpoint.DEBUG, filename=filename_abs)
                    continue

            filename_abs_dest = os.path.join(folder_dest, filename_rel)
            filename_abs_dest_folder = os.path.dirname(filename_abs_dest)
            if not os.path.exists(filename_abs_dest_folder):
                os.makedirs(filename_abs_dest_folder)
            shutil.copy(filename_abs, filename_abs_dest)
            logger.Checkpoint("copy_tree.included_file", module_id, lib.checkpoint.DEBUG, filename=filename_abs_dest, new_file=new_file)


def generate_backup_filename_and_move(filename, logger, module_id):
    backup_filename_ext = '.bak'
    backup_filename = filename
    counter = 1
    while  os.path.exists(backup_filename):
        filename_root, filename_ext = os.path.splitext(filename)
        backup_filename = '%s.%04d%s' % (filename, counter, backup_filename_ext)
        counter += 1
    shutil.move(filename, backup_filename)
    return backup_filename


def copy_tree_with_backup(folder_source, folder_dest, logger, module_id):
    skip_files = [os.path.join(folder_dest, CHECKSUMS_FILENAME),
                  os.path.join(folder_dest, "gon_client-current-mac.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client-current-win.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client-current-linux.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client_management_64-current-win.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client_management_service-current-win.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client_management-current-win.gpmdef.xml"),
                  os.path.join(folder_dest, "gon_client_uninstaller-current-win.gpmdef.xml"),
                  ]
    logger.Checkpoint("copy_tree_with_backup", module_id, lib.checkpoint.DEBUG, folder_source=folder_source, folder_dest=folder_dest)
    for root, dirs, files in os.walk(folder_source):
        for filename in files:
            new_file = True
            filename_abs = os.path.join(root, filename)
            filename_rel = os.path.join(lib.commongon.common_path_sufix(root, folder_source), filename)
            filename_key = os.path.normpath(filename_rel)

            filename_abs_dest = os.path.join(folder_dest, filename_rel)
            filename_abs_dest_folder = os.path.dirname(filename_abs_dest)

            if filename_abs_dest in skip_files:
                logger.Checkpoint("copy_file.skipped_file", module_id, lib.checkpoint.DEBUG, filename=filename_abs_dest)
                continue

            if not os.path.exists(filename_abs_dest_folder):
                os.makedirs(filename_abs_dest_folder)

            if os.path.exists(filename_abs_dest):
                filename_backup_abs_dest_backup = generate_backup_filename_and_move(filename_abs_dest, logger, module_id)
                logger.Checkpoint("copy_file.backup_file", module_id, lib.checkpoint.DEBUG, filename=filename_abs_dest, filename_backup=filename_backup_abs_dest_backup)
            shutil.copy(filename_abs, filename_abs_dest)
            logger.Checkpoint("copy_file", module_id, lib.checkpoint.DEBUG, filename=filename_abs_dest)


def load_version(folder_root):
    version = None
    version_filename = os.path.join(folder_root, VERSION_FILENAME)
    if os.path.exists(version_filename):
        version_file = open(version_filename, 'rt')
        version_string =  version_file.read()
        version = lib.version.Version.create_from_string(version_string.strip())
        version_file.close()
    return version

def save_version(folder_root, version=None):
    if version is None:
        version = lib.version.Version.create_current()
    version_file_filename = os.path.join(folder_root, VERSION_FILENAME)
    version_file_file = open(version_file_filename, 'wt')
    version_file_file.write(version.get_version_string_num())
    version_file_file.close()
