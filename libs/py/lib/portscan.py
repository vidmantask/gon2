import socket
import errno
import time
import sys
if sys.platform == 'win32':
    timer = time.clock
else:
    timer = time.time
import re
import select
import os


def grouper(n, iterable, fillvalue=None):
    """
    Like in 2.6 itertools:
    >>> list(grouper(3, 'ABCDEFG', 'x')) # ABC DEF Gxx
    [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'x', 'x']]
    >>> list(grouper(1, range(3)))
    [[0], [1], [2]]
    >>> list(grouper(3, range(3)))
    [[0, 1, 2]]
    >>> list(grouper(4, range(10)))
    [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, None, None]]
    """
    it = iter(iterable)
    while True:
        l = []
        for i in range(n):
            try:
                next = it.next()
            except StopIteration:
                if l:
                    l.extend([fillvalue] * (n-i))
                    yield l
                return
            l.append(next)
        yield l
    

def ip2int(ip_string):
    """
    >>> ip2int('0.0.0.0')
    0L
    >>> ip2int('1.2.3.4')
    16909060L
    >>> ip2int('123.234.255.000')
    2078998272L
    """
    ip_octets = tuple(int(octetstring) for octetstring in ip_string.split('.'))
    ip = 0L
    for octet in ip_octets:
        ip = (ip << 8) | octet
    return ip

def int2ip(ip_int):
    """
    >>> int2ip(0)
    '0.0.0.0'
    >>> int2ip((1<<32)-1)
    '255.255.255.255'
    """
    return '.'.join(str((ip_int >> b) & 255) for b in (24, 16, 8, 0))
    
ip_re = re.compile(r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$')

def dns_lookup(name_or_ip):
    """
    Process name_or_ip in DNS and return tuple with:
    - best possible name for name_or_ip, using fqdn instead of ip if possible and safe at the moment
    - description of suggested best name (which might be slightly different from description of name_or_ip, but who cares ...)
    
    This should be applied to the list of IPs from a portscan.
    This should be used to get a description of whatever the user has entered, but the best suggestion 
    shouldn't be used without user confirmation (gui could recommend a change if specified is different from best). 
     
    # dns ok
    >>> dns_lookup('dudley.giritech.com')
    ('dudley.giritech.com', 'dudley.giritech.com (192.168.45.10)')

    # sufficiently ok; secondary alias for good, roundtrip ok but nobody knows, not canonical name
    >>> dns_lookup('dudley')
    ('dudley', 'dudley (192.168.45.10)')

    # dns ok, reverse points to name with forward unique match
    >>> dns_lookup('192.168.45.10')
    ('dudley.giritech.com', 'dudley.giritech.com (192.168.45.10)')
    
    # sufficiently ok; dns with multiple
    >>> dns_lookup('pain.giritech.com')
    ('pain.giritech.com', 'pain.giritech.com (192.168.45.13, 10.0.45.252)')

    # reverse points to name with forward disambiguity
    >>> dns_lookup('192.168.45.13')
    ('192.168.45.13', '192.168.45.13 (one of pain.giritech.com)')

    # no reverse dns
    >>> dns_lookup('80.82.100.222') 
    ('80.82.100.222', '80.82.100.222 (no name)')

    # dns not found - this is bad
    >>> dns_lookup('dash.giritech.com')
    (None, 'dash.giritech.com (not found)')
    """
    if ip_re.match(name_or_ip):
        ip = name_or_ip
        
        try:
            hostname = socket.getfqdn(ip)
        except socket.error, _e:
            # usually (-2, 'Name or service not known')
            hostname = None
        else:
            if hostname == ip:
                hostname = None
    else:
        ip = None
        hostname = name_or_ip

    ips = [] 
    if hostname:
        try:
            infos = socket.getaddrinfo(hostname, None, socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
            for info in infos:
                (_family, _socktype, _proto, _canonname, sockaddr) = info
                (host, _port) = sockaddr
                ips.append(host)
        except socket.error, _e:
            pass # dns failed - no IPs to report

    if ip:
        if not ips: # ip with reverse
            return (ip, '%s (no name)' % (ip, ))
        if ip in ips: # ip with good roundtrip but ambiguous
            if len(ips) == 1: # ip with good roundtrip
                return (hostname, '%s (%s)' % (hostname, ip)) 
            return (ip, '%s (one of %s)' % (ip, hostname))
        return (ip, '%s (erroneous name %s)' % (ip, hostname))
    if ips:
        return (hostname, '%s (%s)' % (hostname, ', '.join(ips)))
    return (None, '%s (not found)' % (hostname, )) 
    
def ip_range_iterator(ip_ranges, ports):
    """
    >>> list(ip_range_iterator([], []))
    []
    >>> list(ip_range_iterator([('1.2.3.4', '1.2.3.4')], [80, 8080]))
    [('1.2.3.4', 80), ('1.2.3.4', 8080)]
    >>> list(ip_range_iterator([('192.168.45.1', '192.168.45.2'), ('192.168.45.3', '192.168.45.5')], [123]))
    [('192.168.45.1', 123), ('192.168.45.2', 123), ('192.168.45.3', 123), ('192.168.45.4', 123), ('192.168.45.5', 123)]
    """
    for port in ports:
        for (start_ip, end_ip) in ip_ranges:
            for ip_int in range(ip2int(start_ip), ip2int(end_ip) + 1):
                yield (int2ip(ip_int), port)

def tcp_portscan(ip_ranges, ports, parallel_connections=128, timeout=0.5, delay=None):
    """
    Return iterator with available addresses - the ones where tcp connections are accepted within timeout.
    Even if all parallel_connections returns within timeout it will wait at at least delay - and thus limit the connection rate.
    NOTE: This function isn't instantaneous and shouldn't be called in the main thread ...
    NOTE: Some network policies (and firewalls) doesn't allow port scanning.
    NOTE: All known postive results has been before 0.05 seconds. Many false on ethernet LAN times out after 3 seconds.
    NOTE: "Too many files open" has been seen on Mac with parallel_connections=256 ...
    """
    open_ports = []
    for addrs in grouper(parallel_connections, ip_range_iterator(ip_ranges, ports)):
        #print timer(), 'addr chunk:', addrs
        connecting_sockets = []
        for addr in addrs:
            if not addr:
                continue

            #print 'addr:', addr
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.setblocking(False)
            try:
                err = s.connect_ex(addr)
            except socket.error, _why:
                pass #print s, 'connection exception to', addr, _why
            else:
                if err not in (0, errno.EINPROGRESS, errno.EALREADY, errno.EWOULDBLOCK):
                    pass #print s, 'connection error to', addr, os.strerror(err)
                else:
                    if err: # some kind of in progress
                        pass #print s, 'connecting to', addr, os.strerror(err)
                    else: # if _really_ fast
                        pass #print s, 'connected to', addr
                    connecting_sockets.append((s))
        
        start_time = timer()
        #print 'started', start_time
        while connecting_sockets:
            #print "+ %.3f" % (timer() - start_time), len(connecting_sockets), len(open_ports) 
            time_left = start_time + timeout - timer()
            if time_left <= 0:
                break
            #print "+ %.3f" % (timer() - start_time), 'selecting ...'
            rlist, wlist, xlist = select.select(connecting_sockets, connecting_sockets, connecting_sockets, time_left)
            #print "+ %.3f" % (timer() - start_time), 'selection done'
            for s in xlist:
                if s in connecting_sockets:
                    try:
                        peername = s.getpeername()
                    except socket.error, _why:
                        pass #print 'error getting peername for error', s, _why
                    else:
                        pass #print 'error', peername
                    s.close()
                    connecting_sockets.remove(s)
            for s in rlist:
                if s in connecting_sockets:
                    try:
                        peername = s.getpeername()
                    except socket.error, _why: # perhaps "Too many files open"
                        pass #print 'error getting peername for recv', s, _why
                    else:
                        try:
                            _chunk = repr(s.recv(1))
                        except socket.error, _why:
                            pass #print 'recv', peername, why, _why
                        else:
                            #print 'recv', peername, repr(_chunk)
                            open_ports.append(peername)
                    s.close()
                    connecting_sockets.remove(s)
            for s in wlist:
                if s in connecting_sockets:
                    try:
                        peername = s.getpeername()
                    except socket.error, _why:
                        pass #print 'error getting peername for send', s, _why
                    else: # we already processed the error list, we could we get peername, and we are allowed to write - so we assume it is OK
                        # we _could_ try to send something and check we don't get an error ... but that is so brutal, so instead we are naive 
                        #print 'sendable', peername
                        open_ports.append(peername)
                    s.close()
                    connecting_sockets.remove(s)
        # now: no connecting_sockets or timeout
        for s in connecting_sockets:
            try:
                peername = s.getpeername()
            except socket.error, _why:
                pass #print 'error getting peername for timeout', s, _why
            else:
                pass #print 'timeout', peername
            s.close()
            connecting_sockets.remove(s)
        if delay:
            time_left = start_time + delay - timer()
            if time_left > 0:
                time.sleep(time_left)

    return open_ports


def _test():
    import doctest # will fail because of token.py ...
    doctest.testmod()
    
    
def example():
    lan = ('192.168.45.0', '192.168.45.255')
    test = ('10.0.0.0', '10.0.0.255')
    devtest = ('172.16.0.0', '172.16.0.255')
    localhost = ('127.0.0.1', '127.0.0.1')
    scan_result = tcp_portscan([lan], 
                               [3389],
                               #timeout=10,
                               #parallel_connections=10,
                               #delay=10,
                               )
    for i, addr in enumerate(scan_result):
        print i, addr, dns_lookup(addr[0])
        

if __name__ == "__main__":
    #_test()
    example()
