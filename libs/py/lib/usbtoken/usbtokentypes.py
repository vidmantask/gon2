class Base(object):
    name = None
    hub_vidpid = None
    serial = None
    storage_vidpid = None
    storage_path = None
    cd_vidpid = None
    cd_path = None
    gsi_vidpid = None
    gsi_path = None

    def __repr__(self):
        return '<%r%s%s%s%s>' % (self.name,
                (' storage=%r' % self.storage_path) if self.storage_vidpid else '',
                (' cd=%r' % self.cd_path) if self.cd_vidpid else '',
                (' gsi=%r' % self.gsi_path) if self.gsi_vidpid else '',
                (' serial=%r' % self.serial) if self.serial else '',)

class Hub(Base): pass

class StorageDevice(Base): pass

class CdDevice(Base): pass

class GsiDevice(Base): pass

class Msc(StorageDevice, GsiDevice):
    name = "G&D MSC" # Note: Actually it is the G&D microSD reader ...
    storage_vidpid = gsi_vidpid = (0x13fe, 0x2240) # Kingston
    # Note: if it appears under a Classic Hub then it is an old unsupported Classic ...

class Classic(Hub, StorageDevice, CdDevice, GsiDevice):
    name = "G&D Classic"
    hub_vidpid = (0x1059, 0x0015)
    storage_vidpid = cd_vidpid = (0x1059, 0x0016)
    gsi_vidpid = (0x13fe, 0x3700) # New firmware - almost works (we don't like 2240)

class Id1(StorageDevice, CdDevice, GsiDevice):
    name = "G&D ID-1"
    hub_vidpid = (0x1059, 0x0010)
    storage_vidpid = cd_vidpid = (0x1059, 0x0011)
    gsi_vidpid = (0x1059, 0x0012)

class IronKey(StorageDevice, CdDevice):
    name = "IronKey"
    storage_vidpid = cd_vidpid = (0x1953, 0x0202)

class Hagiwara(StorageDevice, CdDevice): pass

class HagiwaraG1(Hagiwara):
    name = "Hagiwara G1"
    storage_vidpid = cd_vidpid = (0x0930, 0x6511)

class HagiwaraG2(Hagiwara):
    name = "Hagiwara G2"
    storage_vidpid = cd_vidpid = (0x0930, 0x6541)

class HagiwaraG3(Hagiwara):
    name = "Hagiwara G3"
    storage_vidpid = cd_vidpid = (0x0693, 0x0026)

class YubiKey(Base):
    name = "YubiKey"
    vidpid = (0x1050, 0x0010)

class Flash(StorageDevice):
    name = "Flash"
    storage_vidpid = True # hack

device_classes = [Msc,  Classic, Id1, IronKey, HagiwaraG1, HagiwaraG2, HagiwaraG3, YubiKey]

vidpid_classes = {}
for d in device_classes:
    if issubclass(d, StorageDevice):
        assert d.storage_vidpid, d
        if d.storage_vidpid in vidpid_classes:
            assert vidpid_classes[d.storage_vidpid] is d, (vidpid_classes[d.storage_vidpid], d, d.storage_vidpid)
        vidpid_classes[d.storage_vidpid] = d
    if issubclass(d, CdDevice):
        assert d.cd_vidpid, d
        if d.cd_vidpid in vidpid_classes:
            assert vidpid_classes[d.cd_vidpid] is d, (vidpid_classes[d.cd_vidpid], d, d.cd_vidpid)
        vidpid_classes[d.cd_vidpid] = d
    if issubclass(d, GsiDevice):
        assert d.gsi_vidpid, d
        if d.gsi_vidpid in vidpid_classes:
            assert vidpid_classes[d.gsi_vidpid] is d, (vidpid_classes[d.gsi_vidpid], d, d.gsi_vidpid)
        vidpid_classes[d.gsi_vidpid] = d
    if issubclass(d, YubiKey):
        assert d.vidpid, d
        if d.vidpid in vidpid_classes:
            assert vidpid_classes[d.vidpid] is d, (vidpid_classes[d.vidpid], d, d.vidpid)
        vidpid_classes[d.vidpid] = d
