#http://msdn.microsoft.com/en-us/library/bb540532%28v=VS.85%29.aspx About Volume Management
#http://msdn.microsoft.com/en-us/library/aa363785%28v=VS.85%29.aspx Basic and Dynamic Disks
#
#http://www.osronline.com/ddkx/install/idstrings_8tt3.htm Device Identification Strings
#http://msdn.microsoft.com/en-us/library/ff541224%28v=VS.85%29.aspx Device Identification Strings
#http://msdn.microsoft.com/en-us/library/ff547656%28v=VS.85%29.aspx Instance IDs
#http://msdn.microsoft.com/en-us/library/ff541327%28v=VS.85%29.aspx Device Instance IDs
#
#http://msdn.microsoft.com/en-us/library/ff558728%28VS.85%29.aspx USBVIEW Sample Application
#http://support.microsoft.com/kb/311272 DevCon tool
#http://www.winvistatips.com/setupdi-equivelant-cm_get_device_id_ex-t179121.html explanation of CM calls, relating to DevCon
#
#http://www.winvistatips.com/match-between-physical-usb-device-and-its-drive-letter-t182754.html Kiran
#
#Uwe Sieber knows! ;-)
#http://www.winvistatips.com/make-link-between-usb-physical-port-and-drive-letter-mount-bywindows-t194764.html :
#You need an enumeration of all VOLUMES. Get their device numbers. Then enumerate all DISKs, CDROMs and FLOPPYs. Match them with the volumes by their device numbers.
#For each drive with BusType==BusTypeUsb get the parent device of the DISK/CDROM/FLOPPY which is the USB device itself. Get their "driver key name" (SPDRP_DRIVER / CM_DRP_DRIVER).
#Match it with the driver key name of the USB devices found thru the USB API (IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME).
#You get a parent device by CM_Get_Parent and the driver key name by CM_Get_DevInst_Registry_Property called with CM_DRP_DRIVER.
#Using CM_xxx functions is not suggested by MS, but they are so easy too use...
#http://www.codeproject.com/KB/system/RemoveDriveByLetter.aspx
#http://www.uwe-sieber.de/usbstick_e.html
#
#http://www.winvistatips.com/given-usb-mounted-storage-volume-path-find-usb-controlle-t799867.html
#Resolve volume to physical disk by using IOCTL_STORAGE_GET_DEVICE_NUMBER on disks and IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS on the volume.
#Then use the CM_xxx trick on physical disk. Immediate parent is USBSTOR's FDO, on which you can try to call USB IOCTLs.
#
#http://www.emmet-gray.com/Articles/USB_SerialNumbers.htm - almost exactly the same as we do here
#http://social.msdn.microsoft.com/Forums/en-US/windowssdk/thread/23aed79d-c023-4668-a606-f2160b318498 similar example, navigating device tree with Setup and CM functions
#
#Note: Unified Device Property Model is Vista and Later

import ctypes.wintypes
import pywintypes
import win32api # http://timgolden.me.uk/pywin32-docs/win32api.html
import winioctlcon # http://www.koders.com/python/fid287F01CA6BB78C4C7E6220F4EA6FCD863CB98007.aspx
import win32file # http://timgolden.me.uk/pywin32-docs/win32file.html
import usbtokentypes

class mystruct(ctypes.Structure):
    """Extendable pretty-printing ctypes.Structure"""
    def extras(self): return []
    def lines(self, indent='\t'):
        yield "%s" % self.__class__.__name__
        for n, _t in self._fields_:
            v = getattr(self, n)
            if isinstance(v, mystruct):
                first = n + ': '
                for l in v.lines():
                    if first:
                        yield indent + first + l
                        first = None
                    else:
                        yield indent + l
            else:
                yield indent + '%s=%r' % (n, v)
        for n, v in self.extras():
            yield indent + '+%s=%r' % (n, v)
    def __str__(self):
        return '\n'.join(self.lines())

HDEVINFO = ctypes.c_int
DEVINST = ctypes.wintypes.DWORD
UCHAR = ctypes.c_ubyte
PCTSTR = ctypes.c_char_p

def VerifyValidHandle(value):
    if value == 0:
        raise ctypes.WinError()
    return value

class Guid(mystruct):
    _fields_ = [
        ('Data1', ctypes.c_ulong),
        ('Data2', ctypes.c_ushort),
        ('Data3', ctypes.c_ushort),
        ('Data4', ctypes.c_ubyte * 8),
    ]

    def __init__(self, guid):
        mystruct.__init__(self)
        a, b, c, d, e = guid.split('-')
        assert len(a) == 8, (a, [a, b, c, d, e])
        self.Data1 = int(a, 16)
        assert len(b) == 4, (b, [a, b, c, d, e])
        self.Data2 = int(b, 16)
        assert len(c) == 4, (c, [a, b, c, d, e])
        self.Data3 = int(c, 16)
        assert len(d) == 4, (d, [a, b, c, d, e])
        assert len(e) == 12, (e, [a, b, c, d, e])
        for i in range(0, 8):
            self.Data4[i] = int((d + e)[2 * i:2 * i + 2], 16)

    def __str__(self):
        return "{%08x-%04x-%04x-%s-%s}" % (self.Data1, self.Data2, self.Data3,
            ''.join(["%02x" % d for d in self.Data4[:2]]),
            ''.join(["%02x" % d for d in self.Data4[2:]]))

    def lines(self, indent='\t'):
        yield '%s: %s' % (self.__class__.__name__, self)

#
# getDeviceNumber
#

IOCTL_STORAGE_GET_DEVICE_NUMBER = winioctlcon.CTL_CODE(winioctlcon.IOCTL_STORAGE_BASE, 0x0420, winioctlcon.METHOD_BUFFERED, winioctlcon.FILE_ANY_ACCESS)
assert IOCTL_STORAGE_GET_DEVICE_NUMBER == winioctlcon.IOCTL_STORAGE_GET_DEVICE_NUMBER

# http://msdn.microsoft.com/en-us/library/ff566974%28VS.85%29.aspx STORAGE_DEVICE_NUMBER Structure
DEVICE_TYPE = ctypes.wintypes.ULONG
class STORAGE_DEVICE_NUMBER(mystruct):
    _fields_ = [
        ('DeviceType', DEVICE_TYPE),
        ('DeviceNumber', ctypes.wintypes.ULONG),
        ('PartitionNumber', ctypes.wintypes.ULONG),
        ]

def getDeviceNumber(devicepath):
    """return STORAGE_DEVICE_NUMBER - available on Flash and CD but not HD"""
    try:
        hDevice = win32file.CreateFile(devicepath,
                0,
                win32file.FILE_SHARE_READ,
                None,
                win32file.OPEN_EXISTING,
                0, 0)
    except pywintypes.error, e:
        print 'Error opening "%s": %r' % (devicepath, e[2])
        return None
    try:
        sdn = STORAGE_DEVICE_NUMBER()
        outbuffer = win32file.DeviceIoControl(hDevice,
                IOCTL_STORAGE_GET_DEVICE_NUMBER,
                None,
                ctypes.sizeof(sdn),
                None)
        assert len(outbuffer) <= ctypes.sizeof(sdn), (len(outbuffer), ctypes.sizeof(sdn))
        ctypes.memmove(ctypes.addressof(sdn), outbuffer, len(outbuffer))
        return sdn

    except pywintypes.error, e:
        print 'Error getting device number of "%s": %r' % (devicepath, e[2])
        return None
    finally:
        win32file.CloseHandle(hDevice)

#
# getDiskDevicePaths
#

GUID_DEVINTERFACE_DISK = Guid('53f56307-b6bf-11d0-94f2-00a0c91efb8b')
GUID_DEVINTERFACE_CDROM = Guid('53f56308-b6bf-11d0-94f2-00a0c91efb8b')
#GUID_DEVINTERFACE_USB_DEVICE = Guid('A5DCBF10-6530-11D2-901F-00C04FB951ED') # at another level ... apparently ...
#GUID_DEVINTERFACE_USB_HUB = Guid('f18a0e88-c30c-11d0-8815-00a0c906bed8')
#GUID_DEVINTERFACE_VOLUME = Guid('53f5630d-b6bf-11d0-94f2-00a0c91efb8b') # could this be used to find out how many parent levels we need?

# http://msdn.microsoft.com/en-us/library/ff541247.aspx Device Information Sets

SetupDiGetClassDevs = ctypes.windll.setupapi.SetupDiGetClassDevsA
SetupDiGetClassDevs.argtypes = [ctypes.POINTER(Guid), PCTSTR, ctypes.wintypes.HWND, ctypes.wintypes.DWORD]
SetupDiGetClassDevs.restype = VerifyValidHandle #HDEVINFO
DIGCF_PRESENT = 2
DIGCF_ALLCLASSES = 4
DIGCF_DEVICEINTERFACE = 16

# http://msdn.microsoft.com/en-us/library/ff550996.aspx SetupDiDestroyDeviceInfoList
SetupDiDestroyDeviceInfoList = ctypes.windll.setupapi.SetupDiDestroyDeviceInfoList
SetupDiDestroyDeviceInfoList.argtypes = [HDEVINFO]
SetupDiDestroyDeviceInfoList.restype = ctypes.wintypes.BOOL

class SP_DEVINFO_DATA(mystruct):
    _fields_ = [
            ('cbSize', ctypes.wintypes.DWORD),
            ('ClassGuid', Guid),
            ('DevInst', ctypes.wintypes.DWORD),
            ('Reserved', ctypes.c_void_p),
            ]

PSP_DEVINFO_DATA = ctypes.POINTER(SP_DEVINFO_DATA)

# http://msdn.microsoft.com/en-us/library/ff552342%28v=VS.85%29.aspx SP_DEVICE_INTERFACE_DATA Structure
class SP_DEVICE_INTERFACE_DATA(mystruct):
    _fields_ = [
            ('cbSize', ctypes.wintypes.DWORD),
            ('InterfaceClassGuid', Guid),
            ('Flags', ctypes.wintypes.DWORD),
            ('Reserved', ctypes.c_void_p),
            ]

# http://msdn.microsoft.com/en-us/library/ff551015%28v=VS.85%29.aspx SetupDiEnumDeviceInterfaces Function
SetupDiEnumDeviceInterfaces = ctypes.windll.setupapi.SetupDiEnumDeviceInterfaces
SetupDiEnumDeviceInterfaces.argtypes = [HDEVINFO, PSP_DEVINFO_DATA, ctypes.POINTER(Guid), ctypes.wintypes.DWORD, ctypes.POINTER(SP_DEVICE_INTERFACE_DATA)]
SetupDiEnumDeviceInterfaces.restype = ctypes.wintypes.BOOL

# http://msdn.microsoft.com/en-us/library/ff552343%28v=VS.85%29.aspx SP_DEVICE_INTERFACE_DETAIL_DATA Structure
class SP_DEVICE_INTERFACE_DETAIL_DATA(mystruct):
    _fields_ = [
        ('cbSize', ctypes.wintypes.DWORD),
        ('DevicePath', ctypes.c_char * 1000),
    ]

# http://msdn.microsoft.com/en-us/library/ff551120%28VS.85%29.aspx SetupDiGetDeviceInterfaceDetail Function
SetupDiGetDeviceInterfaceDetail = ctypes.windll.setupapi.SetupDiGetDeviceInterfaceDetailA
SetupDiGetDeviceInterfaceDetail.argtypes = [HDEVINFO, ctypes.POINTER(SP_DEVICE_INTERFACE_DATA),
        ctypes.POINTER(SP_DEVICE_INTERFACE_DETAIL_DATA),
        ctypes.wintypes.DWORD, ctypes.POINTER(ctypes.wintypes.DWORD), PSP_DEVINFO_DATA]
SetupDiGetDeviceInterfaceDetail.restype = ctypes.wintypes.BOOL

# http://msdn.microsoft.com/en-us/library/ff551106%28VS.85%29.aspx SetupDiGetDeviceInstanceId Function
SetupDiGetDeviceInstanceId = ctypes.windll.setupapi.SetupDiGetDeviceInstanceIdA
SetupDiGetDeviceInstanceId.argtypes = [HDEVINFO, PSP_DEVINFO_DATA, ctypes.POINTER(ctypes.c_char), ctypes.wintypes.DWORD, ctypes.POINTER(ctypes.wintypes.DWORD)]
SetupDiGetDeviceInstanceId.restype = ctypes.wintypes.BOOL

CR_SUCCESS = 0x00000000
CR_INVALID_POINTER = 0x00000003
CR_NO_SUCH_DEVNODE = 0x0000000D

# http://www.osronline.com/ddkx/install/cfgmgrfn_7zea.htm - good description of cfgmgr CM_ functions
CM_Get_Parent = ctypes.windll.setupapi.CM_Get_Parent
CM_Get_Parent.argtypes = [ctypes.POINTER(DEVINST), DEVINST, ctypes.wintypes.ULONG]
CM_Get_Parent.restype = ctypes.wintypes.INT

CM_Get_Child = ctypes.windll.setupapi.CM_Get_Child
CM_Get_Child.argtypes = [ctypes.POINTER(DEVINST), DEVINST, ctypes.wintypes.ULONG]
CM_Get_Child.restype = ctypes.wintypes.INT

# http://msdn.microsoft.com/en-us/library/ff538496%28VS.85%29.aspx CM_Get_DevNode_Registry_Property Function
CM_Get_DevNode_Registry_Property = ctypes.windll.setupapi.CM_Get_DevNode_Registry_PropertyA
CM_Get_DevNode_Registry_Property.argtypes = [DEVINST, ctypes.wintypes.ULONG, ctypes.POINTER(ctypes.wintypes.ULONG),
        ctypes.POINTER(ctypes.c_char), ctypes.POINTER(ctypes.wintypes.ULONG), ctypes.wintypes.ULONG]
CM_Get_DevNode_Registry_Property.restype = ctypes.wintypes.INT

# http://msdn.microsoft.com/en-us/library/cc238962%28PROT.10%29.aspx
CM_DRP_DRIVER = 0x0000000A

def getDeviceData(guid):
    """Enumerate all present devices of the given guid type and yield data objects
    """
    hDevInfo = SetupDiGetClassDevs(guid, None, None, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE)
    try:
        deviceinterfacedata = SP_DEVICE_INTERFACE_DATA(cbSize=ctypes.sizeof(SP_DEVICE_INTERFACE_DATA))
        for i in range(100):
            if not SetupDiEnumDeviceInterfaces(hDevInfo, None, guid, i, deviceinterfacedata):
                break

            deviceinterfacedetails = SP_DEVICE_INTERFACE_DETAIL_DATA(cbSize=ctypes.sizeof(ctypes.wintypes.DWORD) + 1) # fake size!!!
            devinfodata = SP_DEVINFO_DATA(cbSize=ctypes.sizeof(SP_DEVINFO_DATA))
            if not SetupDiGetDeviceInterfaceDetail(
                hDevInfo,
                ctypes.byref(deviceinterfacedata),
                ctypes.byref(deviceinterfacedetails), ctypes.sizeof(deviceinterfacedetails), None,
                ctypes.byref(devinfodata)
            ):
                raise ctypes.WinError()
            yield hDevInfo, deviceinterfacedetails, devinfodata
    finally:
        if not SetupDiDestroyDeviceInfoList(hDevInfo):
            raise ctypes.WinError()

def getDiskDevicePaths(guid):
    """Enumerate all present devices of the given guid type and return mapping from dt/dn to parent (or grand-parent) DevicePaths
    """
    for hDevInfo, deviceinterfacedetails, devinfodata in getDeviceData(guid):
        sb = ctypes.create_string_buffer(1024)
        SetupDiGetDeviceInstanceId(hDevInfo, ctypes.byref(devinfodata), sb, ctypes.sizeof(sb), None)

        drivers = []
        parent_devinst = DEVINST()
        assert not CM_Get_Parent(ctypes.byref(parent_devinst), devinfodata.DevInst, 0)
        parent_driver = ctypes.create_string_buffer(1024)
        l = ctypes.wintypes.ULONG(ctypes.sizeof(parent_driver))
        if CM_Get_DevNode_Registry_Property(parent_devinst, CM_DRP_DRIVER, None, parent_driver, ctypes.byref(l), 0) == CR_SUCCESS:
            drivers.append(parent_driver.value)

        # inspired by http://social.msdn.microsoft.com/Forums/en-US/windowsgeneraldevelopmentissues/thread/ea01f558-abed-4808-b2e0-5682967d2fb3
        #child_devinst = DEVINST()
        #cr = CM_Get_Child(ctypes.byref(child_devinst), devinfodata.DevInst, 0)

        # FIXME: When is grand parent needed? Apparently for huawei and classic "testing"? Getting the driver will fail for some devices.
        grand_parent_devinst = DEVINST()
        if CM_Get_Parent(ctypes.byref(grand_parent_devinst), parent_devinst, 0) == CR_SUCCESS:
            grand_parent_driver = ctypes.create_string_buffer(1024)
            l = ctypes.wintypes.ULONG(ctypes.sizeof(grand_parent_driver))
            if not CM_Get_DevNode_Registry_Property(grand_parent_devinst, CM_DRP_DRIVER, None, grand_parent_driver, ctypes.byref(l), 0):
                drivers.append(grand_parent_driver.value)

        dn = getDeviceNumber(deviceinterfacedetails.DevicePath)
        if dn:
            yield (dn.DeviceType, dn.DeviceNumber), drivers, parent_devinst
        #else: print 'No device found for %s' % deviceinterfacedetails.DevicePath

# http://msdn.microsoft.com/en-us/library/aa364972%28v=VS.85%29.aspx GetLogicalDrives Function
def getDriveLetters():
    """yield the present drive letters"""
    drives_bitmap = win32file.GetLogicalDrives()
    i = 0
    while drives_bitmap >= 1 << i:
        if drives_bitmap & 1 << i:
            yield chr(65 + i)
        i += 1

class DeviceInfos(object):
    """Enumerate all volumes of relevant types and find mapping from dt/dn to (parent/grandparent) driver path"""
    def __init__(self):
        device_paths = {}
        for guid in [GUID_DEVINTERFACE_DISK, GUID_DEVINTERFACE_CDROM]:
            for dtdn, drivers, _devinst in getDiskDevicePaths(guid):
                device_paths[dtdn] = drivers

        # For each drive: if flash/cd: get dt/dn -> for each device path: get driver
        self.driver_drives = {}
        for driveletter in getDriveLetters():
            drivetype = win32file.GetDriveType(driveletter + ':') # _not_ the same as DeviceType
            if drivetype not in [win32file.DRIVE_REMOVABLE, win32file.DRIVE_CDROM]:
                continue
            dn = getDeviceNumber(r'\\.\%s:' % driveletter)
            if not dn:
                #print 'No device found for %s:' % driveletter
                continue
            drivers = device_paths.get((dn.DeviceType, dn.DeviceNumber))
            if drivers:
                for i, driver in enumerate(drivers):
                    if (drivetype, driver) not in self.driver_drives:
                        self.driver_drives[drivetype, driver] = driveletter
                    else:
                        if i == 0: # quite fatal
                            print 'collision %s between %s: and %s:' % (i, driveletter, self.driver_drives[drivetype, driver])
                        #else: # grand-parents might collide - no problem
                        self.driver_drives[drivetype, driver] += driveletter
            #elif drivers is None: print 'device for %s: not found' % (driveletter,) # quite fatal
            #else: print 'driver for device on %s: not found' % (driveletter,) # quite fatal

    def get_flash(self, di):
        return self.driver_drives.get((win32file.DRIVE_REMOVABLE, di.driverkeyname))

    def get_cdrom(self, di):
        return self.driver_drives.get((win32file.DRIVE_CDROM, di.driverkeyname))

#
# getHostControllers
#

HubControllerGuid = Guid("3abf6f2d-71c4-462a-8a92-1e6861e6af27")

def getHostControllers(guid=HubControllerGuid):
    for _hDevInfo, deviceinterfacedetails, _devinfodata in getDeviceData(guid):
        yield deviceinterfacedetails.DevicePath

#
# getRootHub
#

IOCTL_USB_GET_ROOT_HUB_NAME = 0x220408

class USB_ROOT_HUB_NAME(mystruct):
    _fields_ = [
        ('ActualLength', ctypes.wintypes.ULONG),
        ('RootHubName', ctypes.wintypes.WCHAR * 1024),
    ]

    def getHubDevicePath(self):
        return self.RootHubName and "\\\\.\\" + self.RootHubName

    def extras(self):
        return [('HubDevicePath', self.getHubDevicePath())]

def getRootHub(ControllerDevicePath):
    """Return Root Hub for this Controller"""
    try:
        hDevice = win32file.CreateFile(ControllerDevicePath,
                0,
                win32file.FILE_SHARE_READ,
                None,
                win32file.OPEN_EXISTING,
                0, 0)
    except pywintypes.error, e:
        print 'Error opening "%s": %r' % (ControllerDevicePath, e[2])
        return
    try:
        HubName = USB_ROOT_HUB_NAME()
        outbuffer = win32file.DeviceIoControl(hDevice,
                IOCTL_USB_GET_ROOT_HUB_NAME,
                None,
                ctypes.sizeof(HubName),
                None)
        assert len(outbuffer) < ctypes.sizeof(HubName)
        ctypes.memmove(ctypes.addressof(HubName), outbuffer, len(outbuffer))
    except pywintypes.error, e:
        print 'Error getting name of "%s": %r' % (ControllerDevicePath, e[2])
        return
    finally:
        win32file.CloseHandle(hDevice)

    return HubName.getHubDevicePath()

#
# analyzeHubDevicePath
#

class USB_HUB_DESCRIPTOR(mystruct):
    _fields_ = [
        ('bDescriptorLength', ctypes.c_ubyte),
        ('bDescriptorType', ctypes.c_ubyte),
        ('bNumberOfPorts', ctypes.c_ubyte),
        ('wHubCharacteristics', ctypes.wintypes.USHORT),
        ('bPowerOnToPowerGood', ctypes.c_ubyte),
        ('HubIsBusPowered', ctypes.c_ubyte),
        ('bRemoveAndPowerMask', ctypes.c_ubyte * 64),
    ]
class USB_HUB_INFORMATION(mystruct):
    _fields_ = [
        ('HubDescriptor', USB_HUB_DESCRIPTOR),
        ('HubIsBusPowered', ctypes.wintypes.BOOLEAN),
    ]
class USB_NODE_INFORMATION(mystruct):
    _fields_ = [
        ('NodeType', ctypes.c_int),
        ('HubInformation', USB_HUB_INFORMATION),
    ]
# enum USB_HUB_NODE, USB_NODE_INFORMATION.NodeType
UsbHub, UsbMIParent = range(2)

IOCTL_USB_GET_NODE_INFORMATION = 0x220408

# http://msdn.microsoft.com/en-us/library/ff539280%28VS.85%29.aspx USB_DEVICE_DESCRIPTOR
class USB_DEVICE_DESCRIPTOR(mystruct):
    _fields_ = [
            ('bLength', UCHAR),
            ('bDescriptorType', UCHAR),
            ('bcdUSB', ctypes.wintypes.USHORT),
            ('bDeviceClass', UCHAR),
            ('bDeviceSubClass', UCHAR),
            ('bDeviceProtocol', UCHAR),
            ('bMaxPacketSize0', UCHAR),
            ('idVendor', ctypes.wintypes.USHORT),
            ('idProduct', ctypes.wintypes.USHORT),
            ('bcdDevice', ctypes.wintypes.USHORT),
            ('iManufacturer', UCHAR),
            ('iProduct', UCHAR),
            ('iSerialNumber', UCHAR),
            ('bNumConfigurations', UCHAR),
            ]

# http://msdn.microsoft.com/en-us/library/ff540094%28VS.85%29.aspx USB_NODE_CONNECTION_INFORMATION_EX Structure
class USB_NODE_CONNECTION_INFORMATION_EX(mystruct):
    _fields_ = [
            ('ConnectionIndex', ctypes.wintypes.ULONG),
            ('DeviceDescriptor', USB_DEVICE_DESCRIPTOR),
            ('CurrentConfigurationValue', UCHAR),
            ('Speed', UCHAR),
            ('DeviceIsHub', ctypes.wintypes.BOOLEAN),
            ('DeviceAddress', ctypes.wintypes.USHORT),
            ('NumberOfOpenPipes', ctypes.wintypes.ULONG),
            ('ConnectionStatus', ctypes.c_int), # USB_CONNECTION_STATUS http://msdn.microsoft.com/en-us/library/ff539247%28v=VS.85%29.aspx
            #('PipeList', ctypes.c_char*1), # USB_PIPE_INFO[]
            ]

    def vidpid(self):
        return self.DeviceDescriptor.idVendor, self.DeviceDescriptor.idProduct

    def extras(self):
        return [('vid', '%04X' % self.vidpid()[0]), ('pid', '%04X' % self.vidpid()[1])]

IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX = 0x220448

class USB_NODE_CONNECTION_NAME(mystruct):
    _fields_ = [
        ('ConnectionIndex', ctypes.wintypes.ULONG),
        ('ActualLength', ctypes.wintypes.ULONG),
        ('NodeName', ctypes.wintypes.WCHAR * 1024),
        ]

    def getHubDevicePath(self):
        return self.NodeName and "\\\\.\\" + self.NodeName

    def extras(self):
        return [('HubDevicePath', self.getHubDevicePath())]

IOCTL_USB_GET_NODE_CONNECTION_NAME = 0x220414

class USB_NODE_CONNECTION_DRIVERKEY_NAME(mystruct):
    _fields_ = [
        ('ConnectionIndex', ctypes.wintypes.ULONG),
        ('ActualLength', ctypes.wintypes.ULONG),
        ('DriverKeyName', ctypes.wintypes.WCHAR * 1024),
        ]

FILE_DEVICE_USB = winioctlcon.FILE_DEVICE_UNKNOWN
USB_GET_NODE_CONNECTION_DRIVERKEY_NAME = 264
IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME = winioctlcon.CTL_CODE(FILE_DEVICE_USB, USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
        winioctlcon.METHOD_BUFFERED, winioctlcon.FILE_ANY_ACCESS)
assert IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME == 0x220420

# http://msdn.microsoft.com/en-us/library/ff539272%28VS.85%29.aspx USB_DESCRIPTOR_REQUEST Structure
class USB_DESCRIPTOR_REQUEST(mystruct):
    _fields_ = [
        ('ConnectionIndex', ctypes.wintypes.ULONG),
        ('SetupPacket_bmRequest', ctypes.c_ubyte),
        ('SetupPacket_bRequest', ctypes.c_ubyte),
        ('SetupPacket_wValue', ctypes.wintypes.USHORT),
        ('SetupPacket_wIndex', ctypes.wintypes.USHORT),
        ('SetupPacket_wLength', ctypes.wintypes.USHORT),
        #('Data', ctypes.c_char*1), # NOTE: Left out to help odd size calculations
        ]
USB_DEVICE_DESCRIPTOR_TYPE = 0x1
USB_STRING_DESCRIPTOR_TYPE = 0x3
class USB_STRING_DESCRIPTOR(mystruct):
    _fields_ = [
        ('bLength', ctypes.c_ubyte),
        ('bDescriptorType', ctypes.c_ubyte),
        ('bString', ctypes.wintypes.WCHAR * 256)
        ]

# http://msdn.microsoft.com/en-us/library/ff537310%28VS.85%29.aspx IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION Control Code
USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION = 260
IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION = \
  winioctlcon.CTL_CODE(FILE_DEVICE_USB, USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION, winioctlcon.METHOD_BUFFERED, winioctlcon.FILE_ANY_ACCESS)

def GetDescriptor(hDevice, port_number, index):
    if index:
        try:
            # build a request for string descriptor
            usd = USB_STRING_DESCRIPTOR()
            Request = USB_DESCRIPTOR_REQUEST(
                    ConnectionIndex=port_number,
                    #bmRequest is ignored for IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION and set to 0x80
                    #bRequest is ignored for IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION and set to 0x06
                    SetupPacket_wValue=(USB_STRING_DESCRIPTOR_TYPE << 8) + index,
                    SetupPacket_wIndex=0x409, # Language Code
                    SetupPacket_wLength=ctypes.sizeof(usd),
                    )
            outbuffer = win32file.DeviceIoControl(hDevice,
                    IOCTL_USB_GET_DESCRIPTOR_FROM_NODE_CONNECTION,
                    Request,
                    ctypes.sizeof(Request) + ctypes.sizeof(usd),
                    None)
            outbuffer2 = outbuffer[ctypes.sizeof(Request):]
            assert 0 < len(outbuffer2) <= ctypes.sizeof(usd), (len(outbuffer2), ctypes.sizeof(usd))
            l = ord(outbuffer2[0])
            if l > len(outbuffer2):
                #print 'error: descriptor length not %s as announced but only %s' % (l, len(outbuffer2))
                l = len(outbuffer2)
            ctypes.memmove(ctypes.addressof(usd), outbuffer2, l)
            return usd.bString
        except pywintypes.error, e:
            return 'Error getting descriptor %s: %r' % (index, e[2])

class DeviceInfo(object):
    def __init__(self, driverkeyname, vidpid, serial=None):
        self.driverkeyname = driverkeyname
        self.vidpid = vidpid
        self.serial = serial

    def __repr__(self):
        return '<%s %04x:%04x>' % (self.driverkeyname, self.vidpid[0], self.vidpid[1])

def analyzeHubDevicePath(HubDevicePath):
    """Return list of paths to subhubs and list of DeviceInfo objects"""
    subhubs = []
    devices = []
    try:
        hDevice = win32file.CreateFile(HubDevicePath,
                0,
                win32file.FILE_SHARE_READ,
                None,
                win32file.OPEN_EXISTING,
                0, 0)
    except pywintypes.error, e:
        print 'Error opening "%s": %r' % (HubDevicePath, e[2])
        return subhubs, devices
    try:
        NodeInfo = USB_NODE_INFORMATION(NodeType=UsbHub)
        outbuffer = win32file.DeviceIoControl(hDevice,
                IOCTL_USB_GET_NODE_INFORMATION,
                None,
                ctypes.sizeof(NodeInfo),
                None)
        assert len(outbuffer) < ctypes.sizeof(NodeInfo)
        ctypes.memmove(ctypes.addressof(NodeInfo), outbuffer, len(outbuffer))

        for port_number in range(1, NodeInfo.HubInformation.HubDescriptor.bNumberOfPorts + 1):
            usbnodeinfo = USB_NODE_CONNECTION_INFORMATION_EX(ConnectionIndex=port_number)
            try:
                outbuffer = win32file.DeviceIoControl(hDevice,
                        IOCTL_USB_GET_NODE_CONNECTION_INFORMATION_EX,
                        usbnodeinfo,
                        ctypes.sizeof(usbnodeinfo),
                        None)
            except pywintypes.error, e:
                print 'Error analyzing "%s" port %s: %r' % (HubDevicePath, port_number, e[2])
                continue
            assert len(outbuffer) <= ctypes.sizeof(usbnodeinfo), (len(outbuffer), ctypes.sizeof(usbnodeinfo))
            ctypes.memmove(ctypes.addressof(usbnodeinfo), outbuffer, len(outbuffer))

            if usbnodeinfo.DeviceIsHub:
                NodeName = USB_NODE_CONNECTION_NAME(ConnectionIndex=port_number)
                outbuffer = win32file.DeviceIoControl(hDevice,
                        IOCTL_USB_GET_NODE_CONNECTION_NAME,
                        NodeName,
                        ctypes.sizeof(NodeName),
                        None)
                assert len(outbuffer) <= ctypes.sizeof(NodeName), (len(outbuffer), ctypes.sizeof(NodeName))
                ctypes.memmove(ctypes.addressof(NodeName), outbuffer, len(outbuffer))

                if NodeName.getHubDevicePath(): # no path before ready
                    subhubs.append((NodeName.getHubDevicePath(), usbnodeinfo.vidpid()))

            else:
                if usbnodeinfo.CurrentConfigurationValue:
                    # Get the Driver Key Name (usefull in locating a device) # but only available for hubs???
                    DriverKey = USB_NODE_CONNECTION_DRIVERKEY_NAME(ConnectionIndex=port_number)
                    # Use an IOCTL call to request the Driver Key Name - "the driver registry key name that is associated with the device that is connected to the indicated port."
                    outbuffer = win32file.DeviceIoControl(hDevice,
                            IOCTL_USB_GET_NODE_CONNECTION_DRIVERKEY_NAME,
                            DriverKey,
                            ctypes.sizeof(DriverKey),
                            None)
                    assert len(outbuffer) <= ctypes.sizeof(DriverKey), (len(outbuffer), ctypes.sizeof(DriverKey))
                    ctypes.memmove(ctypes.addressof(DriverKey), outbuffer, len(outbuffer))

                    #manufacturer = GetDescriptor(hDevice, port_number, usbnodeinfo.DeviceDescriptor.iManufacturer)
                    #if usbnodeinfo.DeviceDescriptor.idVendor == 0x1059 and usbnodeinfo.DeviceDescriptor.idProduct == 0x0015:
                    #    product = 'MTCLS' # workaround old Classics that announces iProduct=0xb but fails when trying to retrieve it
                    #else:
                    #    product = GetDescriptor(hDevice, port_number, usbnodeinfo.DeviceDescriptor.iProduct)
                    serialnumber = GetDescriptor(hDevice, port_number, usbnodeinfo.DeviceDescriptor.iSerialNumber)
                    di = DeviceInfo(DriverKey.DriverKeyName, usbnodeinfo.vidpid(), serial=serialnumber)
                    devices.append(di)
                else:
                    pass # don't care if not present ...

    except pywintypes.error, e:
        print 'Error analyzing hub "%s": %r' % (HubDevicePath, e[2])
        return subhubs, devices
    finally:
        win32file.CloseHandle(hDevice)

    return subhubs, devices


#
# getStorageProperties
#

# http://msdn.microsoft.com/en-us/library/ff800830%28VS.85%29.aspx IOCTL_STORAGE_QUERY_PROPERTY Control Code
IOCTL_STORAGE_QUERY_PROPERTY = winioctlcon.CTL_CODE(
        winioctlcon.IOCTL_STORAGE_BASE, 0x0500, winioctlcon.METHOD_BUFFERED, winioctlcon.FILE_ANY_ACCESS)

# http://msdn.microsoft.com/en-us/library/ff800840%28v=VS.85%29.aspx STORAGE_PROPERTY_QUERY Structure
# retrieve the properties of a storage device or adapter.
class STORAGE_PROPERTY_QUERY(mystruct):
    _fields_ = [
        ('PropertyId', ctypes.c_int), # STORAGE_PROPERTY_ID
        ('QueryType', ctypes.c_int), # STORAGE_QUERY_TYPE
        ('AdditionalParameters', ctypes.c_ubyte * 1),
    ]

# STORAGE_PROPERTY_ID
(StorageDeviceProperty, StorageAdapterProperty, StorageDeviceIdProperty) = range(3)

# STORAGE_QUERY_TYPE
(PropertyStandardQuery, PropertyExistsQuery, PropertyMaskQuery, PropertyQueryMaxDefined) = range(4)

# STORAGE_BUS_TYPE, http://msdn.microsoft.com/en-us/library/ff800833%28v=VS.85%29.aspx
STORAGE_BUS_TYPE = dict(
    BusTypeUnknown=0x00,
    BusTypeScsi=0x1,
    BusTypeAtapi=0x2,
    BusTypeAta=0x3,
    BusType1394=0x4,
    BusTypeSsa=0x5,
    BusTypeFibre=0x6,
    BusTypeUsb=0x7,
    BusTypeRAID=0x8,
    BusTypeiScsi=0x9,
    BusTypeSas=0xA,
    BusTypeSata=0xB,
    BusTypeSd=0xC,
    BusTypeMmc=0xD,
    BusTypeVirtual=0xE,
    BusTypeFileBackedVirtual=0xF,
    BusTypeMax=0x10,
    BusTypeMaxReserved=0x7F
    )
STORAGE_BUS_TYPE_NAME = dict((v, k) for k, v in STORAGE_BUS_TYPE.items())

# http://msdn.microsoft.com/en-us/library/ff800835%28v=VS.85%29.aspx STORAGE_DEVICE_DESCRIPTOR Structure
class STORAGE_DEVICE_DESCRIPTOR(mystruct):
    _fields_ = [
        ('Version', ctypes.c_ulong),
        ('Size', ctypes.c_ulong),
        ('DeviceType', ctypes.c_ubyte),
        ('DeviceTypeModifier', ctypes.c_ubyte),
        ('RemovableMedia', ctypes.c_bool),
        ('CommandQueueing', ctypes.c_bool),
        ('VendorIdOffset', ctypes.c_ulong),
        ('ProductIdOffset', ctypes.c_ulong),
        ('ProductRevisionOffset', ctypes.c_ulong),
        ('SerialNumberOffset', ctypes.c_ulong),
        ('BusType', ctypes.c_int), # STORAGE_BUS_TYPE
        ('RawPropertiesLength', ctypes.c_ulong),
        ('RawDeviceProperties', ctypes.c_ubyte * 1), # extendable
        ]

    def get_at_offset(self, offset):
        if offset:
            return ctypes.string_at(ctypes.addressof(self) + offset)
    def BusTypeName(self):
        return STORAGE_BUS_TYPE_NAME.get(self.BusType)
    def VendorId(self):
        return self.get_at_offset(self.VendorIdOffset)
    def ProductId(self):
        return self.get_at_offset(self.ProductIdOffset)
    def ProductRevision(self):
        return self.get_at_offset(self.ProductRevisionOffset)
    def SerialNumber(self):
        return self.get_at_offset(self.SerialNumberOffset)

    def extras(self):
        return [('Bus', self.BusTypeName()),
                ('VendorId', self.VendorId()),
                ('ProductId', self.ProductId()),
                ('ProductRevision', self.ProductRevision()),
                ('SerialNumber', self.SerialNumber())]

# http://msdn.microsoft.com/en-us/library/ff800832%28v=VS.85%29.aspx STORAGE_ADAPTER_DESCRIPTOR Structure
class STORAGE_ADAPTER_DESCRIPTOR(mystruct):
    _fields_ = [
        ('Version', ctypes.wintypes.DWORD),
        ('Size', ctypes.wintypes.DWORD),
        ('MaximumTransferLength', ctypes.wintypes.DWORD),
        ('MaximumPhysicalPages', ctypes.wintypes.DWORD),
        ('AlignmentMask', ctypes.wintypes.DWORD),
        ('AdapterUsesPio', ctypes.wintypes.BOOLEAN),
        ('AdapterScansDown', ctypes.wintypes.BOOLEAN),
        ('CommandQueueing', ctypes.wintypes.BOOLEAN),
        ('AcceleratedTransfer', ctypes.wintypes.BOOLEAN),
        ('BusType', ctypes.wintypes.BYTE),
        ('BusMajorVersion', ctypes.wintypes.WORD),
        ('BusMinorVersion', ctypes.wintypes.WORD),
        ]

    def extras(self):
        return [('Bus', STORAGE_BUS_TYPE_NAME.get(self.BusType))]

def getStorageProperties(driveletter):
    """Return structs with lots of information about a drive"""
    device = adapter = None
    try:
        hDevice = win32file.CreateFile(r"\\?\%s:" % driveletter,
                0,
                win32file.FILE_SHARE_READ,
                None,
                win32file.OPEN_EXISTING, 0, 0)
    except pywintypes.error, e:
        print 'Error opening "%s": %r' % (driveletter, e[2])
        return adapter, device

    # http://msdn.microsoft.com/en-us/library/ff800830%28VS.85%29.aspx IOCTL_STORAGE_QUERY_PROPERTY Control Code
    # http://timgolden.me.uk/pywin32-docs/win32file__DeviceIoControl_meth.html
    try:
        # http://msdn.microsoft.com/en-us/library/ff800830%28v=VS.85%29.aspx STORAGE_ADAPTER_DESCRIPTOR Structure
        query = STORAGE_PROPERTY_QUERY(
            PropertyId=StorageAdapterProperty,
            QueryType=PropertyStandardQuery)
        outbuffer = win32file.DeviceIoControl(hDevice, # device handle
                IOCTL_STORAGE_QUERY_PROPERTY, # dwIoControlCode
                query, # input buffer - STORAGE_PROPERTY_QUERY structure
                1024, # pDevDesc, pDevDesc->Size, # output data buffer
                None)
        adapter = STORAGE_ADAPTER_DESCRIPTOR()
        assert ctypes.sizeof(adapter) == len(outbuffer), (len(outbuffer), ctypes.sizeof(adapter))
        ctypes.memmove(ctypes.addressof(adapter), outbuffer, ctypes.sizeof(adapter))

        # http://msdn.microsoft.com/en-us/library/ff800840%28v=VS.85%29.aspx STORAGE_PROPERTY_QUERY Structure
        query = STORAGE_PROPERTY_QUERY(
            PropertyId=StorageDeviceProperty,
            QueryType=PropertyStandardQuery)

        outbuffer = win32file.DeviceIoControl(hDevice, # device handle
                IOCTL_STORAGE_QUERY_PROPERTY, # info of device property
                query, # input data buffer
                1024, # pDevDesc, pDevDesc->Size, # output data buffer
                None)

        device = STORAGE_DEVICE_DESCRIPTOR()
        ctypes.resize(device, len(outbuffer))
        assert ctypes.sizeof(device) == len(outbuffer), (len(outbuffer), ctypes.sizeof(device))
        ctypes.memmove(ctypes.addressof(device), outbuffer, ctypes.sizeof(device))

    except pywintypes.error, e:
        print 'Error getting storage properties of "%s": %r' % (driveletter, e[2])
    finally:
        win32file.CloseHandle(hDevice)

    return adapter, device

# http://msdn.microsoft.com/en-gb/library/ff549728.aspx PNP_VETO_TYPE Enumeration
PNP_VETO_TYPE = ctypes.wintypes.INT
(PNP_VetoTypeUnknown,
 PNP_VetoLegacyDevice,
 PNP_VetoPendingClose,
 PNP_VetoWindowsApp,
 PNP_VetoWindowsService,
 PNP_VetoOutstandingOpen,
 PNP_VetoDevice,
 PNP_VetoDriver,
 PNP_VetoIllegalDeviceRequest,
 PNP_VetoInsufficientPower,
 PNP_VetoNonDisableable,
 PNP_VetoLegacyDriver,
 PNP_VetoInsufficientRights) = range(13)

# http://msdn.microsoft.com/en-gb/library/ff539806.aspx CM_Request_Device_Eject Function
CM_Request_Device_Eject = ctypes.windll.setupapi.CM_Request_Device_EjectA
CM_Request_Device_Eject.argtypes = [DEVINST, ctypes.POINTER(PNP_VETO_TYPE), ctypes.c_char_p, ctypes.wintypes.ULONG, ctypes.wintypes.ULONG]
CM_Request_Device_Eject.restype = ctypes.wintypes.INT

MAX_PATH = 260

def ejectDevInst(devinst):
    # could depend on http://msdn.microsoft.com/en-gb/library/ff543095.aspx DEVICE_CAPABILITIES Structure
    VetoType = PNP_VETO_TYPE(PNP_VetoTypeUnknown)
    VetoName = ctypes.create_string_buffer(0) # None doesn't work and string isn't used

    res = CM_Request_Device_Eject(devinst, ctypes.byref(VetoType), VetoName, MAX_PATH, 0)
    if res == CR_SUCCESS and VetoType.value == PNP_VetoTypeUnknown:
        return True
    print 'Failed to eject', 'res', res, 'veto', VetoType.value

def tokenlist():
    tokens = {}
    devinfos = DeviceInfos()
    hub_queue = [(getRootHub(host_controller), 'Root Hub') for host_controller in getHostControllers()]
    while hub_queue:
        hub, vidpid = hub_queue.pop()
        assert hub, (hub, vidpid, hub_queue)
        subhubs, devices = analyzeHubDevicePath(hub)
        hub_queue.extend(subhubs)
        for di in devices:
            c = usbtokentypes.vidpid_classes.get(di.vidpid)
            if c:
                if issubclass(c, usbtokentypes.Hub):
                    assert vidpid == c.hub_vidpid, (vidpid, c.hub_vidpid)
                    key = hub
                else:
                    key = di.driverkeyname
                if key in tokens:
                    assert isinstance(tokens[key], c)
                else:
                    tokens[key] = c()

                if issubclass(c, usbtokentypes.StorageDevice) and c.storage_vidpid == di.vidpid:
                    tokens[key].storage_path = devinfos.get_flash(di)
                if issubclass(c, usbtokentypes.GsiDevice) and c.gsi_vidpid == di.vidpid:
                    tokens[key].gsi_path = devinfos.get_flash(di)
                if issubclass(c, usbtokentypes.CdDevice) and c.cd_vidpid == di.vidpid:
                    tokens[key].cd_path = devinfos.get_cdrom(di)
                # YubiKey goes here as well
                tokens[key].serial = di.serial
            else:
                assert not di.driverkeyname in tokens
                tokens[di.driverkeyname] = usbtokentypes.Flash()
                tokens[di.driverkeyname].storage_path = devinfos.get_flash(di)
    return [v for k,v in sorted(tokens.items())]

def eject(path):
    path = path.rstrip(':') + ':'
    dn = getDeviceNumber('\\\\.\\' + path)
    if not dn:
        return None

    drivetype = win32file.GetDriveType(path) # _not_ the same as DeviceType
    if drivetype == win32file.DRIVE_REMOVABLE:
        guid = GUID_DEVINTERFACE_DISK
    elif drivetype == win32file.DRIVE_CDROM:
        guid = GUID_DEVINTERFACE_CDROM
    else:
        return None
    for dtdn, _drivers, devinst in getDiskDevicePaths(guid):
        if dtdn == (dn.DeviceType, dn.DeviceNumber):
            return ejectDevInst(devinst)
