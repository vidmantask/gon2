import sys

if sys.platform == 'linux2':
    import usbtokenlinux as _usbtoken
if sys.platform == 'win32':
    import usbtokenwin as _usbtoken
if sys.platform == 'darwin':
    import usbtokenmac as _usbtoken


def main():
    if sys.argv[1:]:
        if sys.argv[1] == 'eject' and sys.argv[2:3]:
            path = sys.argv[2]
            _usbtoken.eject(path)
            return
        elif sys.argv[1] == 'list':
            for d in _usbtoken.tokenlist():
                print d
            return
    print 'usuage: list or eject PATH'

if __name__ == '__main__':
    main()
