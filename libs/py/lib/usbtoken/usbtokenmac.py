import collections
import re
import subprocess
import os
import usbtokentypes

mountre = re.compile(r"""
/dev/
(?P<dev>[^ ]+)
[ ]on[ ]
(?P<mp>.*) # greedy
[ ][(] # assuming no space-( follows
""", re.VERBOSE)

def mounts():
    """Return dict mapping from device to mount point"""
    d = {}
    for l in subprocess.Popen('mount', stdout=subprocess.PIPE).stdout:
        m = mountre.match(l)
        if m:
            d[m.group('dev')] = m.group('mp')
    return d

linere = re.compile(r"""
(?P<indent>[-+| ]*)
(?:
    o[ ](?P<node>.*)$
|
    "(?P<key>[^"]*)"[ ]=[ ]
        (?:
            (?P<value_int>[0-9]+)$
        |
            "(?P<value_string>.*)"$
        )
)
""", re.VERBOSE)

class LineParser(object):
    def __init__(self, it):
        self.iter = it
        self.peek = None
        self.indent = None
        self.key = None
        self.value = None
        self.node = None
        self.next()

    def next(self):
        m = None
        while not m:
            self.peek = self.iter.next()
            m = linere.match(self.peek)
        self.key = m.group('key')
        self.value_int = m.group('value_int')
        self.value_string = m.group('value_string')
        self.node = m.group('node')
        self.indent = len(m.group('indent')) / 2 - (1 if self.node else 3)

class Device(object):
    """Struct for USB device"""

interesting = ('idVendor', 'idProduct', 'BSD Name', "device-type", "locationID")

def usb_devices():
    m = mounts()
    lp = LineParser(subprocess.Popen(['ioreg', '-w', '0', '-cIOUSBInterface', '-l', '-r'], stdout=subprocess.PIPE).stdout)
    vals = collections.defaultdict(dict)

    while True:
        if lp.node:
            #print ' '*lp.indent + lp.node + ':'
            vals[lp.indent].clear()
        if lp.key and lp.key in interesting:
            #print ' '*lp.indent + lp.key + ' = ' + '%r' % (lp.value_string or lp.value_int)
            if lp.key in interesting:
                key = lp.key.replace(' ', '_').replace('-', '_')
                if lp.value_string:
                    value = lp.value_string
                else:
                    value = lp.value_int
                vals[lp.indent][key] = value
                if lp.key == 'BSD Name':
                    dev = Device()
                    for i in range(lp.indent + 1):
                        dev.__dict__.update(vals[i])
                    if 'idVendor' in dev.__dict__: # USB
                        # http://www.opensource.apple.com/source/IOUSBFamily/IOUSBFamily-206.4.1/IOUSBFamily/Classes/IOUSBDevice.cpp
                        # LocationID is used to uniquely identify a device or interface and it's
                        # suppose to remain constant across reboots as long as the USB topology doesn't
                        # change.  It is a 32-bit word.  The top 2 nibbles (bits 31:24) represent the
                        # USB Bus Number.  Each nibble after that (e.g. bits 23:20 or 19:16) correspond
                        # to the port number of the hub that the device is connected to.
                        dev.location = ('%x' % int(dev.locationID)).rstrip('0')
                        dev.vidpid = (int(dev.idVendor), int(dev.idProduct))
                        dev.mp = m.get(dev.BSD_Name)
                        yield dev
        lp.next()

def tokenlist():
    tokens = {}
    for di in usb_devices():
        c = usbtokentypes.vidpid_classes.get(di.vidpid)
        if c:
            if issubclass(c, usbtokentypes.Hub):
                key = di.location[:-1]
            else:
                key = di.location
            if key in tokens:
                assert isinstance(tokens[key], c)
            else:
                tokens[key] = c()

            if di.device_type == 'Generic':
                if issubclass(c, usbtokentypes.StorageDevice) and c.storage_vidpid == di.vidpid:
                    tokens[key].storage_path = di.mp
                if issubclass(c, usbtokentypes.GsiDevice) and c.gsi_vidpid == di.vidpid:
                    tokens[key].gsi_path = di.mp
            if di.device_type == 'CDROM':
                if issubclass(c, usbtokentypes.CdDevice) and c.cd_vidpid == di.vidpid:
                    tokens[key].cd_path = di.mp
            # FIXME: YubiKey???
        else:
            assert not di.location in tokens
            tokens[di.location] = usbtokentypes.Flash()
            tokens[di.location].storage_path = di.mp
    return [v for _k, v in sorted(tokens.items())]

def eject(path):
    return os.system('umount %s' % path) == 0
