import os
import re
import pyudev # http://packages.python.org/pyudev/
import usbtokentypes

hexenc_re = re.compile(r'\\x([0-9a-fA-F]{2})')
def hexenc_f(mounts):
    return chr(int(mounts.group(1), 16))
def hexenc_decode(s):
    """unencode hex escaped USB device info string"""
    return hexenc_re.sub(hexenc_f, s)

octenc_re = re.compile(r'\\([0-3][0-8]{2})')
def octenc_f(mounts):
    return chr(int(mounts.group(1), 8))
def octenc_decode(s):
    """unencode octal escaped string"""
    return octenc_re.sub(octenc_f, s)

def mounts():
    """Yield all mounted devices and their mount point"""
    for l in open('/proc/mounts'):
        devnode, mp = l.split()[:2]
        if not devnode.startswith('/dev/'):
            continue
        yield devnode, octenc_decode(mp)

def devinfos():
    """find and yield all devinfos"""
    context = pyudev.Context()
    for devnode, mp in mounts():
        for fs_dev in context.list_devices().match_property('DEVNAME', devnode):
            if fs_dev.get('ID_BUS') == 'usb':
                yield DevInfo(fs_dev, mp)

class DevInfo(object):
    """Handle all kinds of info about a device"""
    def __init__(self, fs_dev, mp):
        self.mp = mp
        self.devname = fs_dev['DEVNAME']
        self.id_type = fs_dev['ID_TYPE']
        #self.id_fs_uuid = fs_dev.get('ID_FS_UUID', '???')
        #self.id_fs_label = fs_dev.get('ID_FS_LABEL', '???')
        #self.id_vendor = hexenc_decode(fs_dev['ID_VENDOR_ENC'])
        #self.id_model = hexenc_decode(fs_dev['ID_MODEL_ENC'])
        #self.id_revision = fs_dev['ID_REVISION']
        #self.id_serial_short = fs_dev['ID_SERIAL_SHORT']
        # find usb device of fs
        usb_dev = fs_dev.parent
        while usb_dev and usb_dev['DEVTYPE'] != 'usb_device':
            usb_dev = usb_dev.parent
        self.devpath = usb_dev['DEVPATH']
        #self.id_vendor2 = hexenc_decode(usb_dev['ID_VENDOR_ENC'])
        #self.id_model2 = hexenc_decode(usb_dev['ID_MODEL_ENC'])
        self.vidpid = (int(usb_dev['ID_VENDOR_ID'], 16), int(usb_dev['ID_MODEL_ID'], 16))
        # find hub of usb device
        usb_dev_hub = usb_dev.parent
        #pprint(usb_dev_hub.items())
        #print 'parent', usb_dev_hub, usb_dev_hub['ID_VENDOR_ID'], usb_dev_hub['ID_MODEL_ID']
        self.hubdevpath = usb_dev_hub['DEVPATH']
        self.hubvidpid = (int(usb_dev_hub['ID_VENDOR_ID'], 16), int(usb_dev_hub['ID_MODEL_ID'], 16))

def tokenlist():
    tokens = {}
    for di in devinfos():
        c = usbtokentypes.vidpid_classes.get(di.vidpid)
        if c:
            if issubclass(c, usbtokentypes.Hub):
                #assert di.hubvidpid == c.hub_vidpid, (di.hubvidpid, c.hub_vidpid)
                key = di.hubdevpath
            else:
                key = di.devpath
            if key in tokens:
                assert isinstance(tokens[key], c)
            else:
                tokens[key] = c()

            if di.id_type == 'disk':
                if issubclass(c, usbtokentypes.StorageDevice) and c.storage_vidpid == di.vidpid:
                    tokens[key].storage_path = di.mp
                if issubclass(c, usbtokentypes.GsiDevice) and c.gsi_vidpid == di.vidpid:
                    tokens[key].gsi_path = di.mp
            if di.id_type == 'cd':
                if issubclass(c, usbtokentypes.CdDevice) and c.cd_vidpid == di.vidpid:
                    tokens[key].cd_path = di.mp
            # FIXME: YubiKey???
        else:
            assert not di.devpath in tokens
            tokens[di.devpath] = usbtokentypes.Flash()
            tokens[di.devpath].storage_path = di.mp
    return [v for _k, v in sorted(tokens.items())]

def eject(path):
    return os.system('umount %s' % path) == 0
