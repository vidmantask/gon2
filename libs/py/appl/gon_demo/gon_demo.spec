# -*- mode: python -*-
from distutils.core import setup


data_files = []
data_files.append(('modules', ['./modules/__init__.py']))
data_files.append(('modules/a', ['./modules/a/__init__.py']))
data_files.append(('modules/b', ['./modules/b/__init__.py']))

#
# PyInstaller configuration
#
app_py = 'gon_demo.py'
app_name = 'gon_demo'
app_description = "G/On Demo"
app_version_file = ''
app_icon_file = ''
app_options = []

datas = []
for (data_file_dest_folder, data_file_src_files) in data_files:
  for data_file_src_file in data_file_src_files:
    datas.append( (data_file_src_file, data_file_dest_folder))

hiddenimports = [
 'uuid',
 'os',
 'os.path',
]

exclude_modules = [
  'modules',
  'modules.a',
  'modules.b',
]


#
# PyInstaller generate folder app
#
a = Analysis(
  [app_py],
  pathex=['.'],
  binaries=[],
  datas=datas,
  hiddenimports=hiddenimports,
  hookspath=[],
  runtime_hooks=[],
  excludes=exclude_modules,
  win_no_prefer_redirects=False,
  win_private_assemblies=False,
  cipher=None
)

pyz = PYZ(
  a.pure,
  a.zipped_data,
  cipher=None
)

exe = EXE(
  pyz,
  a.scripts,
  options=app_options,
  exclude_binaries=True,
  name=app_name,
  debug=False,
  strip=False,
  upx=True,
  console=True,
  version=app_version_file,
  icon=app_icon_file
)

coll = COLLECT(
  exe,
  a.binaries,
  a.zipfiles,
  a.datas,
  strip=False,
  upx=True,
  name=''
)
