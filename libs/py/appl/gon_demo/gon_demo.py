import sys
import os
import importlib
import traceback

def load_plugin_module(module_name):
    py_module_name = 'modules.%s' % (module_name)

    try:
        py_module = importlib.import_module(py_module_name)
        for py_plugin_name in py_module.__dict__:
            attr = getattr(py_module, py_plugin_name)
            print("py_module", py_plugin_name)
        attr = getattr(py_module, 'run')
        attr()

    except Exception as error:
        print("ERROR", error)
        traceback.print_exc()

def main():
    print("start")
    load_plugin_module("a")
    load_plugin_module("b")
    print("done")


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)

    main()
