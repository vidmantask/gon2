import lib.dev_env.path_setup
from distutils.core import setup
import py2exe
import lib.appl_plugin_module_util
import os.path

import lib.dev_env
import lib.dev_env.pack

dev_env = lib.dev_env.DevEnv.create_current()

data_files = []
data_files.append(('gon_client_template/gon_client', lib.dev_env.pack.generate_files('gon_client_template/gon_client')))
data_files.append(('gon_client_template/gon_client/gon_deploy_soft_token', lib.dev_env.pack.generate_files('gon_client_template/gon_client/gon_deploy_soft_token')))
data_files.append(('gon_client_template/gon_client/gon_init_soft_token', lib.dev_env.pack.generate_files('gon_client_template/gon_client/gon_init_soft_token')))
data_files.append(('gon_botnet_commands', lib.dev_env.pack.generate_files('gon_botnet_commands')))
data_files.append(('gauge_sim_data', lib.dev_env.pack.generate_files('gauge_sim_data')))


data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'gateway', module_element_result_prefix="gon_client_template/gon_client"))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'gateway_common', module_element_result_prefix="gon_client_template/gon_client"))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'client_ok', 'client', 'gateway', module_element_result_prefix="gon_client_template/gon_client"))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway', module_element_result_prefix="gon_client_template/gon_client"))


data_files.append(('', ['../../setup/dlls/win/libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                        '../../setup/dlls/win/msvcr90.dll', 
                        '../../setup/dlls/win/msvcp90.dll',
                        '../../setup/dlls/win/msvcm90.dll',
                        '../../setup/dlls/win/Microsoft.VC90.CRT.manifest',
]))


manifest_template = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
  <compatibility xmlns="urn:schemas-microsoft-com:compatibility.v1">
    <application>
      <!--The ID below indicates application support for Windows Vista -->
        <supportedOS Id="{e2011457-1546-43c5-a5fe-008deee3d3f0}"/>
      <!--The ID below indicates application support for Windows 7 -->
        <supportedOS Id="{35138b9a-5d96-4fbd-8e2d-a2440225f93a}"/>
    </application>
  </compatibility>
  <trustInfo xmlns="urn:schemas-microsoft-com:asm.v3">
    <security>
      <requestedPrivileges>
        <requestedExecutionLevel level="asInvoker" uiAccess="false"></requestedExecutionLevel>
      </requestedPrivileges>
    </security>
  </trustInfo>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity type="win32" name="Microsoft.Windows.Common-Controls" version="6.0.0.0"  processorArchitecture="X86" publicKeyToken="6595b64144ccf1df" language="*"></assemblyIdentity>
    </dependentAssembly>
  </dependency>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity type="win32" name="Microsoft.VC90.CRT" version="9.0.21022.8" processorArchitecture="x86" publicKeyToken="1fc8b3b9a1e18e3b"></assemblyIdentity>
    </dependentAssembly>
  </dependency>
</assembly>
"""

setup(
      console = [ lib.appl_plugin_module_util.Py2ExeTarget(
                  script='gon_botnet.py',
                  uac = False,
                   branding = lib.appl_plugin_module_util.Py2ExeTarget.BRANDING_NORMAL,
                   name = 'G/On Botnet',
                   description = 'G/On Botnet for Windows',
                   image_root='../../components/presentation/gui/mfc/images',
                   manifest = manifest_template
                 )
               ],
      options = {
            "py2exe": {
                  "dll_excludes": ['w9xpopen.exe'],
                  "dist_dir" : dev_env.generate_pack_destination('gon_botnet'),
                  "packages":[
                                     'logging',
                                     'sqlalchemy.dialects.sqlite',
                                     'sqlalchemy.dialects.mssql',
                                     'lib.cryptfacility',
                                     'elementtree',
                                     'win32com.client',
                                     'ZSI.version'
                             ],
                  "includes":[],
                  "excludes":[
                             ],
                  "bundle_files" : 3,
                  "compressed" : True,
                  "optimize" : 0,
            }
      },
      data_files = data_files,
      zipfile = None,
)
