"""
gon_botnet.py

This tool plays both the role of clients and server in the gon_botnet.

As a client it can be used for collecting performance data from one or
more gateway servers, and it can be used as simulator of a number of 
gon_clients connecting to a existing gon installation.

As a server it controls the execution plan given from a command.ini file.


Examples:
-------------------------------------------------------------------------------
  Start a gon_botnet client. The client can after this, be used by the
  cmd_master command (and a matching command.ini file).
  $python gon_botnet.py --cmd_serve

  Start the gon_botnet: 
  $python gon_botnet.py --cmd_master run --command_ini command_002.ini

  Analyze result, calculating ping stattistic for all clients:
  $python gon_botnet.py --cmd_master analyze


Configuration (command.ini):
-------------------------------------------------------------------------------
[global_settings]
duration_sec = 800

[performance_monitor_001]
ip = 192.168.42.91
port = 4500
monitor_interval_sec = 1

[client_202_x_50]
ip = 192.168.42.202
port = 4500
delay_sec = 0
;menu_items =ping,load_light_email
menu_items =ping,load_heavy_powerpoint

[client_203_x_50]
ip = 192.168.42.203
port = 4500
delay_sec = 0
;menu_items =ping,load_light_email
menu_items =ping,load_heavy_powerpoint


Configuration (launch commands):
-------------------------------------------------------------------------------
ping:
python "C:/utility/gauge/ging.py" --LOG_FILENAME="%(cpm.rw_root)/ping.log" --PING_INTERVAL=5 %(portforward.host):%(portforward.port)/1/1

load_light_email:
python "C:/utility/gauge/ging.py" --script_filename="C:/utility/gon_botnet/win/gauge_sim_data/light_email.txt" --CONNECT_ADDR=%(portforward.host):%(portforward.port) --LOG_FILENAME="%(cpm.rw_root)/load_light_email.log" --loop

load_heavy_powerpoint:
python "C:/utility/gauge/ging.py" --script_filename="C:/utility/gon_botnet/win/gauge_sim_data/heavy_powerpoint.txt" --CONNECT_ADDR=%(portforward.host):%(portforward.port) --LOG_FILENAME="%(cpm.rw_root)/load_heavy_powerpoint.log" --loop


"""
import sys
import os
import os.path
import optparse
import xmlrpclib
import SocketServer
import SimpleXMLRPCServer
import ConfigParser
import datetime
import threading
import tempfile
import shutil
import time
import pickle
import copy
import subprocess
import string
import re
import tarfile
import StringIO
import datetime
import base64
import random
import traceback

if not hasattr(sys, "frozen"):
    HG_ROOT = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..'))
    ROOT_PY = os.path.join(HG_ROOT, 'py')
    sys.path.append(ROOT_PY)

    import lib.dev_env.path_setup

import numpy

import lib.utility
import lib.version

import lib.hardware.sys_info_surveillance        


MODULE_ID = 'gon_botnet'

GPM_DEPLOY_ID = 'gon_client_deploy'
GPM_DEPLOY_CONTENT_FILENAME_SERVERS = 'gon_client/gon_deploy_soft_token/gon_client.servers'
GPM_DEPLOY_CONTENT_FILENAME_KS = 'gon_client/gon_deploy_soft_token/gon_client.ks'


FILENAME_MASTER_DATETIME_DIFF = 'master_datetime_diff_sec'

# ========================================================================================================================
class CommandlineOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--cmd_serve', action='store_true', help='Start client for use in gon_botnet')
        self._parser.add_option('--cmd_master', type='string', default=None, help='[run, analyze, slave_cleanup]')

        self._parser.add_option('--command_ini', type='string', default=None, help='Optional option for --cmd_master')
        (self._options, self._args) = self._parser.parse_args()
        self._ini = self._read_ini()

    def _read_ini(self):
        ini_filename = os.path.join(self._get_root(), 'gon_botnet.ini')
        ini = ConfigParser.ConfigParser()
        ini.read(ini_filename)
        return ini

    def _get_root(self):
        if hasattr(sys, "frozen"):
            return os.getcwd()
        else:
            return os.path.normpath(os.path.dirname(__file__))

    def cmd_serve(self):
        return self._options.cmd_serve

    def cmd_master(self):
        return self._options.cmd_master

    def get_info_root(self):
        return os.path.join(self._get_root(), 'botnet_temp')

    def get_botnet_result_root(self):
        return os.path.join(self._get_root(), 'botnet_result')

    def get_client_template_root(self):
        return os.path.join(self._get_root(), 'gon_client_template')
        
    def get_listen_port(self):
        return 4500
    
    def get_command_ini(self, command_ini_filename=None):
        if command_ini_filename is None:
            command_ini_filename = os.path.join(self._get_root(), self._options.command_ini)
        command_ini = ConfigParser.ConfigParser()
        command_ini.read(command_ini_filename)
        return command_ini
        
    def print_help(self):
        self._parser.print_help()
        
    def dump_command_ini(self, dest_command_init_filename):
        command_ini_filename = os.path.join(self._get_root(), self._options.command_ini)
        shutil.copy(command_ini_filename, dest_command_init_filename)

# ========================================================================================================================
def read_file_content(filename):
    content = None
    if os.path.exists(filename):
        fileobj = open(filename, 'r')
        content = fileobj.read()
        fileobj.close()
    return content 

def write_file_content(filename, content):
    if os.path.exists(filename):
        fileobj = open(filename, 'w')
        content = fileobj.write(content)
        fileobj.close()
        

# ========================================================================================================================
import lib.checkpoint
import lib.gpm.gpm_env
import lib.gpm.gpm_analyzer
import lib.gpm.gpm_installer_base

import components.communication.async_service
import components.dictionary.common
import components.presentation.user_interface
import components.communication.async_service
import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_token

import plugin_modules.soft_token.client_gateway

import appl.gon_client.gon_client



class UserInterfaceControlerStub(object):
    def __init__(self, user_interface):
        self.user_interface = user_interface
    def set_message(self, message, message_show_dots=True, end_after_display=False, message_show_only_once=True):
        pass
    def message_update_interval(self, message_update_interval_sec):
        pass
    def end(self):
        pass


class BotnetClient(threading.Thread):
    STATE_INIT = 0
    STATE_WAIT_TO_CONNECT = 1
    STATE_CONNECTING = 2
    STATE_CONNECTED = 3
    STATE_LOGIN_AVAILABLE = 4
    STATE_MENU_AVAILABLE = 5
    STATE_DONE = 6
    STATE_ERROR = 7
    
    STATUS_CONNECTING = 0
    STATUS_MENU_AVAILABLE = 1
    STATUS_DONE = 2

    STATUS_FILENAME_CONNECT_START = 'status_connect_start'
    STATUS_FILENAME_CONNECTED = 'status_connected'
    STATUS_FILENAME_MENU_AVAILABLE = 'status_menu_available'
    STATUS_FILENAME_DONE = 'status_done'
    
    def __init__(self, async_service, job_root, job_id, serial):
        threading.Thread.__init__(self, name="BotnetClient_%s" % job_id)
        self.state = BotnetClient.STATE_INIT 
        self.job_root = job_root
        self.job_id = job_id
        self._stop = False
        self.user_interface = None
        self.async_service = None 
#        self.async_service = async_service
        self.gon_client_installation_root = os.path.join(self.job_root, 'gon_client_installation')
        self.error_message = ""
        
        self.connected_and_ready_ts = datetime.datetime.now()
        
        if serial is not None:
            serial_filename = os.path.join(self.gon_client_installation_root, 'gon_client', 'gon_init_soft_token', 'serial')
            write_file_content(serial_filename, serial)
        
    def _write_status_file(self, filename):
        status_file = open(os.path.join(self.gon_client_installation_root, filename), 'w')
        ts = datetime.datetime.now()        
        status_file.write(ts.isoformat(' '))
        status_file.close()

    def run(self):
        try:
            sys.argv = []
            client_config_filename = os.path.join(self.gon_client_installation_root, 'gon_client', 'gon_client.ini')
            client_config = appl.gon_client.gon_client.ClientGatewayOptions()
            client_config.read_config_file(client_config_filename)
            
            client_config.log_file = os.path.join(self.gon_client_installation_root, 'gon_client', 'gon_client.log')
            client_config.plugin_modules_path = os.path.join(self.gon_client_installation_root, 'gon_client')
            
            checkpoint_handler_generator = lib.checkpoint.CheckpointHandlerGenerator(client_config.log_enabled,
                                                                                     client_config.log_verbose,
                                                                                     client_config.log_file,
                                                                                     client_config.is_log_type_xml(),
                                                                                     True,
                                                                                     client_config.log_rotate)
            checkpoint_handler = checkpoint_handler_generator.get_handler()
            self.checkpoint_handler = checkpoint_handler

            self.async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 1, keep_running=True)
            self.async_service.start()
            
            dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, client_config.get_dictionary_path_abs())
            user_interface = components.presentation.user_interface.UserInterface(configuration=client_config, dictionary=dictionary, view_type=components.presentation.user_interface.UserInterface.VIEW_TYPE_SIMULATOR)
    
            user_interface_controler = UserInterfaceControlerStub(user_interface)
            self.user_interface = user_interface
            
            plugin_manager = components.plugin.client_gateway.manager.Manager(checkpoint_handler, client_config.plugin_modules_path)
            additional_device_roots = [self.gon_client_installation_root]
            plugins = plugin_manager.create_instances(async_service=self.async_service, checkpoint_handler=checkpoint_handler, user_interface=user_interface, additional_device_roots=additional_device_roots)
            token_plugin_socket = components.plugin.client_gateway.plugin_socket_token.PluginSocket(plugin_manager, plugins) 
            client_runtime_env_plugin_socket = components.plugin.client_gateway.plugin_socket_client_runtime_env.PluginSocket(plugin_manager, plugins, checkpoint_handler) 
    
            client_runtime_env = plugin_modules.soft_token.client_gateway.PluginClientRuntimeEnvInstance(checkpoint_handler, self.gon_client_installation_root)
            client_runtime_env_plugin_socket = None

            self.state = BotnetClient.STATE_WAIT_TO_CONNECT
            while self.state in [BotnetClient.STATE_WAIT_TO_CONNECT]:
                time.sleep(1)

            self._write_status_file(BotnetClient.STATUS_FILENAME_CONNECT_START)
            if self.state not in [BotnetClient.STATE_DONE, BotnetClient.STATE_ERROR]:
                knownsecret = read_file_content(os.path.join(self.gon_client_installation_root, GPM_DEPLOY_CONTENT_FILENAME_KS))
                servers = read_file_content(os.path.join(self.gon_client_installation_root, GPM_DEPLOY_CONTENT_FILENAME_SERVERS))
                session_manager = appl.gon_client.gon_client.ClientSessionManager(self.async_service, checkpoint_handler, plugin_manager, plugins, token_plugin_socket, user_interface_controler, knownsecret, client_runtime_env_plugin_socket, client_config, False, dictionary, False, client_runtime_env)
                self.session_manager = session_manager
                session_manager.set_servers(servers)
                session_manager.start()
            self._connect(self.connect_username, self.connect_password, self.connect_login_delay_sec, self.connect_menu_item_to_watch)
            
            self.connected_and_ready_ts = datetime.datetime.now()
            while not self._stop:
                time.sleep(1)
                if not self.async_service.is_running():
                    self._stop = True
                if not self.session_manager.is_running():
                    self._stop = True
                if self.session_manager.is_closed():
                    self._stop = True
                if self.disconnect_delay_sec > 0:
                    running_sec = (datetime.datetime.now() - self.connected_and_ready_ts).total_seconds()
                    if self.disconnect_delay_sec < running_sec:
                        self.checkpoint_handler.Checkpoint("run.stopping.disconnect_delay_sec.reached", "gon_botnet", lib.checkpoint.INFO)
                        self._stop = True

            self.checkpoint_handler.Checkpoint("run.stopping", "gon_botnet", lib.checkpoint.INFO)
            session_manager.stop()
            while session_manager.is_running() or not session_manager.is_closed():
                self.checkpoint_handler.Checkpoint("run.wait_for_session_manager", "gon_botnet", lib.checkpoint.INFO)
                print "Waiting for session_manager to stop"
                time.sleep(1)
            self.async_service.stop()
            while self.async_service.is_running():
                self.checkpoint_handler.Checkpoint("run.wait_for_async_service", "gon_botnet", lib.checkpoint.INFO)
                print "Waiting for asunc_service to stop"
                time.sleep(1)

            self._write_status_file(BotnetClient.STATUS_FILENAME_DONE)
            self.state = BotnetClient.STATE_DONE
            self.checkpoint_handler.Checkpoint("run.done", "gon_botnet", lib.checkpoint.INFO)

        except:
            self.state = BotnetClient.STATE_ERROR
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("error", "gon_botnet", lib.checkpoint.ERROR, etype, evalue, etrace)
            self.error_message = evalue
     
    def _check_for_stop(self):
        
        if not self.async_service.is_running():
            self._stop = True
        if not self.session_manager.is_running():
            self._stop = True
            
    def connect(self, username, password, login_delay_sec, disconnect_delay_sec, menu_item_to_watch):
        if self.state in [BotnetClient.STATE_INIT]:
            while self.state in [BotnetClient.STATE_INIT]:
                time.sleep(0.1)
                
        if self.state in [BotnetClient.STATE_WAIT_TO_CONNECT]:
            self.connect_username = username
            self.connect_password = password
            self.connect_login_delay_sec = login_delay_sec
            self.disconnect_delay_sec = disconnect_delay_sec
            self.connect_menu_item_to_watch = menu_item_to_watch
            self.state = BotnetClient.STATE_CONNECTING
            return (True, 'Ok')
        return (False, 'Not ready for connect')
        
    def _connect(self, username, password, login_delay_sec, menu_item_to_watch):
        try:
            self.checkpoint_handler.Checkpoint("run.connecting", "gon_botnet", lib.checkpoint.INFO)
            if username is not None:
                while self.state in [BotnetClient.STATE_CONNECTING] and not self._stop:
                    if self.user_interface.login.view.display_called:
                        self.state = BotnetClient.STATE_LOGIN_AVAILABLE
                    else:
                        self._check_for_stop()
                        time.sleep(1)
                        
                if self.state in [BotnetClient.STATE_LOGIN_AVAILABLE]:
                    self.checkpoint_handler.Checkpoint("login_available", "gon_botnet", lib.checkpoint.INFO)
                    self._write_status_file(BotnetClient.STATUS_FILENAME_CONNECTED)
                    time.sleep(login_delay_sec)
                    self.user_interface.login.set_credentials(username, password)
                    self.user_interface.login.set_next_clicked(True)

            self.checkpoint_handler.Checkpoint("run.connecting.wait", "gon_botnet", lib.checkpoint.INFO)
            while self.state in [BotnetClient.STATE_CONNECTING, BotnetClient.STATE_LOGIN_AVAILABLE] and not self.launch_menu_item_exists(menu_item_to_watch) and not self._stop:
                self._check_for_stop()
                time.sleep(1)

            if self.launch_menu_item_exists(menu_item_to_watch):
                self.checkpoint_handler.Checkpoint("menu_item_available", "gon_botnet", lib.checkpoint.INFO, menu_item_to_watch=menu_item_to_watch)
                self.state = BotnetClient.STATE_MENU_AVAILABLE
                
                if username is None:
                    self._write_status_file(BotnetClient.STATUS_FILENAME_CONNECTED)
                self._write_status_file(BotnetClient.STATUS_FILENAME_MENU_AVAILABLE)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("error", "gon_botnet", lib.checkpoint.ERROR, etype, evalue, etrace)

    def launch_menu_item_exists(self, menu_item_to_watch):
        for (id, launch_id, menu_item) in self.user_interface.menu.model.get_item_list():
            if menu_item == menu_item_to_watch:
                return True
        return False
    
    def launch_menu_item(self, menu_item_to_launch):
        for (id, launch_id, menu_item) in self.user_interface.menu.model.get_item_list():
            if menu_item == menu_item_to_launch:
                self.user_interface.menu.add_to_launch_list(launch_id)
                return (True, 'Ok')
            
        print "LAUNCH_MENU_ITEM_FAILED", self.state
        return (False, "Menu '%s' item not found" % menu_item_to_launch)

    def get_status(self):
        if self.state in [BotnetClient.STATE_DONE, BotnetClient.STATE_ERROR]:
            return BotnetClient.STATUS_DONE
        if self.state in [BotnetClient.STATE_MENU_AVAILABLE]:
            return BotnetClient.STATUS_MENU_AVAILABLE
        return BotnetClient.STATUS_CONNECTING




    @classmethod
    def create(cls, async_service, job_root, job_id, client_tar_data, serial):
        client_tarfile_fileobj = StringIO.StringIO(client_tar_data)
        client_tarfile = tarfile.open(fileobj=client_tarfile_fileobj, mode='r:gz')
        client_tarfile.extractall(path=job_root)
        client_tarfile.close()
        client_tarfile_fileobj.close()
        return BotnetClient(async_service, job_root, job_id, serial)
    
    def stop(self):
        self._stop = True
    
    def info_collect_begin(self):
        pass
    
    def info_collect_end(self):
        pass


# ========================================================================================================================
class BotnetGatewayServer(object):
    def __init__(self, commandline_options):
        self.commandline_options = commandline_options 
    
    def collect_gateway_server_info(self):
        return "xxxx"



# ========================================================================================================================
import components.database.server_common.connection_factory
import components.access_log.server_common.database_schema 
from components.database.server_common import schema_api
from components.database.server_common import database_api
import components.config.common
import lib.dev_env.user_and_token_creator
import plugin_modules.ad.server_common.ad_config

class PerformanceMonitor(threading.Thread):
    def __init__(self, commandline_options, job_root, monitor_interval_sec):
        threading.Thread.__init__(self, name="PerformanceMonitor")
        self.commandline_options = commandline_options
        self.job_root = job_root
        self.monitor_interval_sec = monitor_interval_sec
        self._stop =  False

        gateway_root = os.path.abspath(os.path.join('..', '..', 'gon_server_gateway_service', 'win'))
        gateway_config = components.config.common.ConfigServerGateway()
        gateway_config.read_config_file_from_folder(gateway_root)
        gateway_config.load_and_set_scramble_salt()

        print "Using db_connect_string '%s'" % gateway_config.get_db_connect_info()
        db_connection = components.database.server_common.connection_factory.ConnectionFactory(gateway_config.get_db_connect_url(), gateway_config.db_encoding).get_default_connection()
        components.access_log.server_common.database_schema.connect_to_database_using_connection(db_connection)

    def run(self):
        sys_info_system = lib.hardware.sys_info_surveillance.SysInfoSystem.create()
        sys_info_process_gons = lib.hardware.sys_info_surveillance.SysInfoProcess.create_running_gon()
        sys_info_interfaces = lib.hardware.sys_info_surveillance.SysInfoNetworkInterface.create_all()

        headline2 = []
        headline2.append('timestamp')
        headline2.append('CPU(%)')
        for ni in sys_info_interfaces:
            headline2.append("%s Received(bytes/sec)" % ni.get_name())
            headline2.append("%s Send(bytes/sec)" % ni.get_name())
        for pi in sys_info_process_gons:
            headline2.append("%s Memory(bytes)" % pi.get_name())

        headline2.append("Sessions(#)")
        headline2.append("Proxys(#)")

        monitor_filename = os.path.join(self.job_root, 'monitor.csv')
        monitor_file = open(monitor_filename, 'w')
        monitor_file.write('%s\n' % ','.join(headline2))
        monitor_file.close()

        counter = 1
        while(not self._stop):
            usage = []
            usage.append(self.get_timestamp()) 
            usage.append('%d' % sys_info_system.get_cpu_usage()) 
            for ni in sys_info_interfaces:
                (bytes_received_per_sec, bytes_send_per_sec) = ni.get_usage()
                usage.append('%d' % bytes_received_per_sec) 
                usage.append('%d' % bytes_send_per_sec) 
            for pi in sys_info_process_gons:
                usage.append('%d' % pi.get_memory_usage()) 

            (sessions, proxys) = self.get_session_count()
            usage.append('%d' % sessions)
            usage.append('%d' % proxys)

            monitor_file = open(monitor_filename, 'a')
            monitor_file.write('%s\n' % ','.join(usage))
            monitor_file.close()
            time.sleep(self.monitor_interval_sec)
            counter += self.monitor_interval_sec


    def get_timestamp(self):
        timestamp = datetime.datetime.now()
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def get_session_count(self):
        sessions = 0
        proxies = 0
        with database_api.ReadonlySession() as dbs:
            sessions = dbs._session.query(components.access_log.server_common.database_schema.Session).filter(components.access_log.server_common.database_schema.Session.close_ts == None).count()
            proxies = dbs._session.query(components.access_log.server_common.database_schema.TrafficProxy).filter(components.access_log.server_common.database_schema.TrafficProxy.close_ts == None).count()
        return (sessions, proxies)                       

    def stop(self):
        self._stop = True
    
    def info_collect_begin(self):
        pass
    
    def info_collect_end(self):
        pass


# ========================================================================================================================
class GOnBotnetSlave(object):
    RC_OK = 0
    RC_ERROR = 1
    
    def __init__(self, commandline_options):
        self.commandline_options = commandline_options
        self.job_id_next = 0
        self.jobs = {}
        self.master_datetime_diff = datetime.timedelta(0)
    
        self.async_service = None 
    
    def cmd_client_start(self, client_tar_data_b64, serial):
        job_id = self.job_id_next
        self.job_id_next += 1
        job_root = self._create_job_root(job_id)
        self.jobs[job_id] = BotnetClient.create(self.async_service, job_root, job_id, base64.b64decode(client_tar_data_b64), serial)
        self.jobs[job_id].start()
        result = {'rc':GOnBotnetSlave.RC_OK, 'job_id':job_id}
        return result

    def cmd_client_connect(self, job_id, username, password, login_delay_sec, disconnect_delay_sec, menu_item_to_watch):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        (launch_ok, error_message) = self.jobs[job_id].connect(username, password, login_delay_sec, disconnect_delay_sec, menu_item_to_watch)
        if not launch_ok:
            return  self._build_error_result(error_message)
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result 

    def cmd_client_status(self, job_id):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        status = self.jobs[job_id].get_status()
        result = {'rc':GOnBotnetSlave.RC_OK, 'status':status}
        return result 
    
    def cmd_client_launch_menu_item(self, job_id, menu_item):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        (launch_ok, error_message) = self.jobs[job_id].launch_menu_item(menu_item)
        if not launch_ok:
            return  self._build_error_result(error_message)
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result
    
    def cmd_client_stop(self, job_id):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        self.jobs[job_id].stop()
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result
    
    def _get_job_root(self, job_id):
        return os.path.join(self.commandline_options.get_info_root(), '%d' % job_id)

    def _create_job_root(self, job_id):
        job_root = self._get_job_root(job_id)
        if os.path.exists(job_root):
            shutil.rmtree(job_root)
        if not os.path.exists(job_root):
            os.makedirs(job_root)
        return job_root
    
    def _build_error_result(self, message):
        return {'rc':GOnBotnetSlave.RC_ERROR, 'message': message}

    def cmd_performance_monitor_start(self, monitor_interval_sec):
        job_id = self.job_id_next
        self.job_id_next += 1

        job_root = self._create_job_root(job_id)
        self.jobs[job_id] = PerformanceMonitor(self.commandline_options, job_root, monitor_interval_sec)
        self.jobs[job_id].start()

        result = {'rc':GOnBotnetSlave.RC_OK, 'job_id':job_id}
        return result
    
    def cmd_performance_monitor_stop(self, job_id):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        self.jobs[job_id].stop()
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result
        
    def cmd_info_collect(self, job_id):
        if not self.jobs.has_key(job_id):
            return  self._build_error_result('Invalid job id')
        info_filename = os.path.join(self.commandline_options.get_info_root(), '%d.tar.gz' % job_id)
        self.jobs[job_id].info_collect_begin()
        info_tarfile = tarfile.open(info_filename, mode='w:gz')
        info_tarfile.add(self._get_job_root(job_id), arcname='info')
        info_tarfile.close()    
        self.jobs[job_id].info_collect_end()

        info_tar_data_file = open(info_filename, 'rb')
        info_tar_data = info_tar_data_file.read()
        info_tar_data_file.close()
        
        master_datetime_diff_sec = (self.master_datetime_diff.days * 3600 * 24) + self.master_datetime_diff.seconds 
        print "cmd_info_collect", self.master_datetime_diff.days, self.master_datetime_diff.seconds, master_datetime_diff_sec
        result = {'rc':GOnBotnetSlave.RC_OK, 'info_tar_data':base64.b64encode(info_tar_data), 'master_datetime_diff_sec': master_datetime_diff_sec}
        os.unlink(info_filename)
        return result
    
    def cmd_cleanup(self):
        for job_id in self.jobs.keys():
            print "Stopping job", job_id
            self.cmd_client_stop(job_id)
        time.sleep(2)
        if os.path.exists(self.commandline_options.get_info_root()):
            shutil.rmtree(self.commandline_options.get_info_root())
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result

    def cmd_ping(self):
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result

    def cmd_time_sync(self, master_datetime_iso):
        master_datetime = datetime.datetime.strptime(master_datetime_iso, '%Y-%m-%dT%H:%M:%S')
        self.master_datetime_diff = datetime.datetime.now() - master_datetime
        print "cmd_time_sync", master_datetime_iso, master_datetime, datetime.datetime.now(), self.master_datetime_diff
        result = {'rc':GOnBotnetSlave.RC_OK}
        return result
    
    def cmd_create_users_and_tokens(self, ou_name, public_key, count):
        try:
            management_root = os.path.abspath(os.path.join('..', '..', 'gon_server_management_service', 'win'))
            management_config = components.config.common.ConfigServerManagement()
            management_config.read_config_file_from_folder(management_root)
            admin_ws_service_url = 'http://%s:%d' %(management_config.management_ws_ip,  management_config.management_ws_port)
            
            ad_plugin_root = os.path.abspath(os.path.join('..', '..', 'gon_server_management_service', 'win', 'plugin_modules', 'ad', 'server_management' ))
            ad_plugin_config = plugin_modules.ad.server_common.ad_config.Config(ad_plugin_root)
            
            root_dns = ad_plugin_config.authorization_domains[0]
            lib.dev_env.user_and_token_creator.create_users_and_tokens(admin_ws_service_url, root_dns, ou_name, public_key, count)
            result = {'rc':GOnBotnetSlave.RC_OK}
            return result
        except:
            traceback.print_exc()
        
    def run(self):
        SocketServer.TCPServer.request_queue_size = 1000
        slave_server = SimpleXMLRPCServer.SimpleXMLRPCServer(("0.0.0.0", self.commandline_options.get_listen_port()), allow_none=True)
        print "G/On Botnet Slave listening on port %d ..." % self.commandline_options.get_listen_port()
        slave_server.register_function(self.cmd_performance_monitor_start, 'cmd_performance_monitor_start')
        slave_server.register_function(self.cmd_performance_monitor_stop, 'cmd_performance_monitor_stop')
        slave_server.register_function(self.cmd_info_collect, 'cmd_info_collect')
        slave_server.register_function(self.cmd_client_start, 'cmd_client_start')
        slave_server.register_function(self.cmd_client_connect, 'cmd_client_connect')
        slave_server.register_function(self.cmd_client_status, 'cmd_client_status')
        slave_server.register_function(self.cmd_client_launch_menu_item, 'cmd_client_launch_menu_item')
        slave_server.register_function(self.cmd_client_stop, 'cmd_client_stop')
        slave_server.register_function(self.cmd_cleanup, 'cmd_cleanup')
        slave_server.register_function(self.cmd_ping, 'cmd_ping')
        slave_server.register_function(self.cmd_time_sync, 'cmd_time_sync')
        slave_server.register_function(self.cmd_create_users_and_tokens, 'cmd_create_users_and_tokens')
        
        checkpoint_handler = lib.checkpoint.CheckpointHandler.get_NULL()
        components.communication.async_service.init(checkpoint_handler)
#        self.async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 5, keep_running=True)
#        self.async_service.start()
        try:
            slave_server.serve_forever()
        except KeyboardInterrupt:
            pass
        print "Interupted please wait while terminating"
#        self.async_service.stop()
        time.sleep(2)
        print "G/On Botnet Slave no longer listening"
        


# ========================================================================================================================

def save_job_info(info_root, client_id, info_tar_data, master_datetime_diff_sec):
    job_info_root = os.path.join(info_root, client_id)
    if not os.path.exists(job_info_root):
        os.makedirs(job_info_root)
    info_tarfile_fileobj = StringIO.StringIO(info_tar_data)
    info_tarfile = tarfile.open(fileobj=info_tarfile_fileobj, mode='r:gz')
    info_tarfile.extractall(path=job_info_root)
    info_tarfile.close()
    info_tarfile_fileobj.close()

    time_diff_file = open(os.path.join(job_info_root, FILENAME_MASTER_DATETIME_DIFF), 'w')
    time_diff_file.write('%d' % master_datetime_diff_sec)
    time_diff_file.close()
    

SECTION_GLOBAL_SETTINGS = 'global_settings'
def get_duration_sec(command_ini, section):
    if command_ini.has_section(section):
        if command_ini.has_option(section, 'duration_sec'):
            return command_ini.getint(section, 'duration_sec')
    if command_ini.has_section(SECTION_GLOBAL_SETTINGS):
        if command_ini.has_option(SECTION_GLOBAL_SETTINGS, 'duration_sec'):
            return command_ini.getint(SECTION_GLOBAL_SETTINGS, 'duration_sec')
    return 60

def get_ini_num(command_ini, section, option, default_value):
    if command_ini.has_section(section):
        if command_ini.has_option(section, option):
            return command_ini.getint(section, option)
    return default_value

def get_ini(command_ini, section, option, default_value):
    if command_ini.has_section(section):
        if command_ini.has_option(section, option):
            return command_ini.get(section, option).strip()
    return default_value

def get_ini_bool(command_ini, section, option, default_value):
    if command_ini.has_section(section):
        if command_ini.has_option(section, option):
            value = command_ini.get(section, option)
            return value.strip() in ['True', '1', 'true']
    return default_value

class GOnBotnetClientBase(threading.Thread):
    STATE_INIT = 0
    STATE_RUNNING_SETUP = 1
    STATE_RUNNING_SETUP_DONE = 2
    STATE_RUNNING = 3
    STATE_RUNNING_DONE = 4
    STATE_RUNNING_COLLECT = 5
    STATE_DONE = 6
    STATE_DONE_WITH_ERROR = 7
    
    def __init__(self, name, estimate_time_sec):
        threading.Thread.__init__(self, name=name)
        self.name = name
        self.estimate_time_sec = estimate_time_sec
        self.state = GOnBotnetClientBase.STATE_INIT
        self.start_time = None
        self.error_message = None

    def get_remaining_time_sec(self):
        if self.state == GOnBotnetClientBase.STATE_INIT:
            return self.estimate_time_sec
        if self.state == GOnBotnetClientBase.STATE_RUNNING:
            runned = datetime.datetime.now() - self.start_time
            remaining_time_sec =  self.estimate_time_sec - runned.seconds 
            if remaining_time_sec < 1:
                return 1
            return remaining_time_sec
        return 1

    def get_error_message(self):
        return self.error_message

    def do_connect_by_setup(self):
        return True

    def setup_is_done(self):
        return self.state in [GOnBotnetClientBase.STATE_RUNNING_SETUP_DONE, GOnBotnetClientBase.STATE_DONE_WITH_ERROR, GOnBotnetClientBase.STATE_DONE]

    def run(self):
        self.state = GOnBotnetClientBase.STATE_RUNNING_SETUP
        try:
            self.run_setup()
            if not self.state in [GOnBotnetClientBase.STATE_DONE_WITH_ERROR, GOnBotnetClientBase.STATE_DONE]:
                self.state = GOnBotnetClientBase.STATE_RUNNING_SETUP_DONE
                while(self.state == GOnBotnetClientBase.STATE_RUNNING_SETUP_DONE):
                    time.sleep(1)
        except:
            print sys.exc_info()
            self.done_with_error('Not responding running setup')
        
        try:
            if not self.state in [GOnBotnetClientBase.STATE_DONE_WITH_ERROR, GOnBotnetClientBase.STATE_DONE]:
                self.run_action()
        except:
            print sys.exc_info()
            self.done_with_error('Not responding running action')

        try:
            if not self.state in [GOnBotnetClientBase.STATE_DONE_WITH_ERROR, GOnBotnetClientBase.STATE_DONE]:
                self.state = GOnBotnetClientBase.STATE_RUNNING_DONE
                while(self.state == GOnBotnetClientBase.STATE_RUNNING_DONE):
                    time.sleep(1)
    
                self.run_collect()
                self.done()
        except:
            print sys.exc_info()
            self.done_with_error('Not responding running collect')

    def run_setup(self):
        pass
    
    def run_action(self):
        pass
    
    def run_collect(self):
        pass
  
    def done_with_error(self, error_message):
        self.state = GOnBotnetClientBase.STATE_DONE_WITH_ERROR
        self.error_message = error_message

    def go_running(self):
        if self.state == GOnBotnetClientBase.STATE_RUNNING_SETUP_DONE:
            self.start_time = datetime.datetime.now()
            self.state = GOnBotnetClientBase.STATE_RUNNING

    def go_running_collect(self):
        if self.state == GOnBotnetClientBase.STATE_RUNNING_DONE:
            self.state = GOnBotnetClientBase.STATE_RUNNING_COLLECT

    def done(self):
        self.state = GOnBotnetClientBase.STATE_DONE


class UsernameGenerator(object):
    def __init__(self):
        self.next_id = 0
    
    def generate(self, ini_string):
        if ini_string.find('%d') != -1 :
            id = self.next_id
            self.next_id += 1
            return ini_string % id
        return ini_string

class GOnBotnetClientProxy(GOnBotnetClientBase):
    TRY_COUNT = 10
    def __init__(self, info_root_result, name, ip, port, delay_sec, duration_sec, menu_items, client_template_tardata_b64, connect_by_setup, connect_username, connect_password, connect_login_delay_sec, disconnect_delay_sec, serial):
        GOnBotnetClientBase.__init__(self, name, delay_sec + duration_sec + connect_login_delay_sec + 5)
        self.slave_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port), allow_none=1)
        self.delay_sec = delay_sec
        self.duration_sec = duration_sec
        self.menu_items = menu_items
        self.client_template_tardata_b64 = client_template_tardata_b64
        self.info_root_result = info_root_result
        self.job_id = None

        self.connect_by_setup = connect_by_setup
        self.connect_username = connect_username
        self.connect_password = connect_password
        self.connect_login_delay_sec = connect_login_delay_sec
        self.disconnect_delay_sec = disconnect_delay_sec
        self.serial = serial 
        
    def _connect(self):
        rc_info = self.slave_proxy.cmd_client_connect(self.job_id, self.connect_username, self.connect_password, self.connect_login_delay_sec, self.disconnect_delay_sec, self.menu_items[0])
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return False

        status = BotnetClient.STATUS_CONNECTING
        while status in [BotnetClient.STATUS_CONNECTING]:
            time.sleep(1)
            rc_info = self.slave_proxy.cmd_client_status(self.job_id)
            if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
                self.done_with_error(rc_info['message'])
                return False
            status = rc_info['status']
        return status in [BotnetClient.STATUS_MENU_AVAILABLE]
    
    def do_connect_by_setup(self):
        return self.connect_by_setup

    def run_setup(self):
        rc_info = self.slave_proxy.cmd_client_start(self.client_template_tardata_b64, self.serial)
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return
        self.job_id = rc_info['job_id']  
    
        if self.connect_by_setup:
            self._connect()
            
    def run_action(self):
        run_action_start = datetime.datetime.now()
        time.sleep(self.delay_sec)
        
        connect_ok = True
        if not self.connect_by_setup:
            connect_ok = self._connect()

        if connect_ok:
            for menu_item in self.menu_items:
                rc_info = self.slave_proxy.cmd_client_launch_menu_item(self.job_id, menu_item)
                if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
                    self.done_with_error(rc_info['message'])
                    return
            
            while(datetime.datetime.now() - run_action_start) < datetime.timedelta(seconds=self.duration_sec):
                time.sleep(1)

            rc_info = self.slave_proxy.cmd_client_stop(self.job_id)
            if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
                self.done_with_error(rc_info['message'])
                return
    
    def run_collect(self):
        rc_info = self.slave_proxy.cmd_info_collect(self.job_id)
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return
        save_job_info(self.info_root_result, self.name, base64.b64decode(rc_info['info_tar_data']), rc_info['master_datetime_diff_sec'])


    @classmethod
    def create_from_command_ini(cls, client_template_root, info_root, info_root_result, command_ini, username_generator):
        result = []
        client_template_tarfile_filename = os.path.join(info_root, 'gon_client_template.tar.gz')
        client_template_tarfile = tarfile.open(client_template_tarfile_filename, mode='w:gz')
        client_template_tarfile.add(client_template_root, arcname='gon_client_installation')
        client_template_tarfile.close()
        
        client_template_tarfile = open(client_template_tarfile_filename, 'rb')
        client_template_tardata = client_template_tarfile.read()
        client_template_tardata_b64 = base64.b64encode(client_template_tardata)
        client_template_tarfile.close()

        for section in command_ini.sections():
            if section.startswith('client'):
                ip = command_ini.get(section, 'ip')
                port = command_ini.getint(section, 'port')
                delay_sec = command_ini.getint(section, 'delay_sec')
                menu_items = command_ini.get(section, 'menu_items').split(',')
                num_of_clients = get_ini_num(command_ini, section, 'count', 1)
                pattern_repeat_count = get_ini_num(command_ini, section, 'pattern_repeat_count', 1)
                pattern_repeat_delay_sec = get_ini_num(command_ini, section, 'pattern_repeat_delay_sec', 0)

                connect_by_setup = get_ini_bool(command_ini, section, 'connect_by_setup', True)
                connect_login_delay_sec = get_ini_num(command_ini, section, 'connect_login_delay_sec', 0)
                disconnect_delay_sec = get_ini_num(command_ini, section, 'disconnect_delay_sec', 0)
                for client_idx in range(num_of_clients):
                    for pattern_idx in range(pattern_repeat_count):
                        connect_username = username_generator.generate(get_ini(command_ini, section, 'connect_username', None))
                        connect_password = get_ini(command_ini, section, 'connect_password', None)
                        name = '%s_%d_%d' % (section, client_idx, pattern_idx)
                        current_delay_sec = delay_sec + (pattern_idx * pattern_repeat_delay_sec)
                        current_duration_sec = get_duration_sec(command_ini, section)
                        serial = 'serial_%s@devtest.giritech.com' % connect_username
                        result.append(GOnBotnetClientProxy(info_root_result, name, ip, port, current_delay_sec, current_duration_sec, menu_items, client_template_tardata_b64, connect_by_setup, connect_username, connect_password, connect_login_delay_sec, disconnect_delay_sec, serial))
        return result


class GOnBotnetPerformanceMonitor(GOnBotnetClientBase):
    def __init__(self, info_root, name, ip, port, duration_sec, monitor_interval_sec):
        GOnBotnetClientBase.__init__(self, name, duration_sec + 5)
        self.slave_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port))
        self.info_root = info_root
        self.name = name
        self.duration_sec = duration_sec
        self.monitor_interval_sec = monitor_interval_sec
        self.job_id = None

    def run_setup(self):
        try:
            rc_info = self.slave_proxy.cmd_ping()
        except:
            print sys.exc_info()
            self.done_with_error('Not responding')
            return

    def run_action(self):
        rc_info = self.slave_proxy.cmd_performance_monitor_start(self.monitor_interval_sec)
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self._error(rc_info['message'])
            return
        self.job_id = rc_info['job_id']

        time.sleep(self.duration_sec)

        rc_info = self.slave_proxy.cmd_performance_monitor_stop(self.job_id)
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return

    def run_collect(self):
        rc_info = self.slave_proxy.cmd_info_collect(self.job_id)
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return
        save_job_info(self.info_root, self.name, base64.b64decode(rc_info['info_tar_data']), rc_info['master_datetime_diff_sec'])

    @classmethod
    def create_from_command_ini(cls, info_root, command_ini):
        result = []
        for section in command_ini.sections():
            if section.startswith('performance_monitor'):
                name = section
                ip = command_ini.get(section, 'ip')
                port = command_ini.getint(section, 'port')
                duration_sec = get_duration_sec(command_ini, section)
                monitor_interval_sec = command_ini.getint(section, 'monitor_interval_sec')
                result.append(GOnBotnetPerformanceMonitor(info_root, name, ip, port, duration_sec, monitor_interval_sec))
        return result




class GOnManagementServerProxy(GOnBotnetClientBase):
    def __init__(self, info_root, name, ip, port, setup_users_and_tokens=None):
        GOnBotnetClientBase.__init__(self, name, 20)
        self.slave_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port))
        self.info_root = info_root
        self.name = name
        self.job_id = None
        self.setup_users_and_tokens = setup_users_and_tokens

    def run_setup(self):
        try:
            if self.setup_users_and_tokens is not None:
                ou_name = self.setup_users_and_tokens['ou_name']
                public_key  = self.setup_users_and_tokens['public_key'] 
                count  = self.setup_users_and_tokens['count']
            rc_info = self.slave_proxy.cmd_create_users_and_tokens(ou_name, public_key, count)
        except:
            traceback.print_exc()
            self.done_with_error('Not responding')
            return

    def run_action(self):
        pass
    
    def run_collect(self):
        pass
    
    @classmethod
    def create_from_command_ini(cls, client_template_root, info_root, info_root_result, command_ini):
        result = []
        for section in command_ini.sections():
            if section.startswith('management_server'):
                ip = command_ini.get(section, 'ip')
                port = command_ini.getint(section, 'port')
                name = section
                setup_users_and_tokens = {}
                setup_users_and_tokens['ou_name'] = command_ini.get(section, 'ou_name')
                setup_users_and_tokens['count'] = command_ini.getint(section, 'count')
                public_key_filename = os.path.join(client_template_root, 'gon_client', 'gon_deploy_soft_token', 'public')
                setup_users_and_tokens['public_key'] = read_file_content(public_key_filename)
                result.append(GOnManagementServerProxy(info_root, name, ip, port, setup_users_and_tokens))
        return result


class GOnBotnetSlaveProxy(object):
    def __init__(self, name, ip, port):
        self.slave_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port))

    def time_sync(self):
        master_date_time = datetime.datetime.now()
        master_date_time = master_date_time - datetime.timedelta(microseconds=master_date_time.microsecond)
        rc_info = self.slave_proxy.cmd_time_sync(master_date_time.isoformat())
        if not rc_info['rc'] == GOnBotnetSlave.RC_OK:
            self.done_with_error(rc_info['message'])
            return

    @classmethod
    def create_from_command_ini(cls, command_ini):
        result = []
        for section in command_ini.sections():
            if section not in [SECTION_GLOBAL_SETTINGS]:
                name = section
                ip = command_ini.get(section, 'ip')
                port = command_ini.getint(section, 'port')
                result.append(GOnBotnetSlaveProxy(name, ip, port))
        return result


class AnalyzeData(object):
    def __init__(self):
        self.ping_rows = {}
        self.performance_rows = {}
        self.performance_header = AnalyzePerformanceHeader([])
        self.connect_status_rows = {}
         
    def get_min_max_row_keys(self, use_connect_status=True):
        keys = self.performance_rows.keys() + self.ping_rows.keys()
        if use_connect_status:
            keys += self.connect_status_rows.keys()
        if len(keys) == 0:
            return (datetime.datetime.now(), datetime.datetime.now())
        return (min(keys), max(keys))
    
    def update_ping_row(self, datetime, ping_delay):
        if not self.ping_rows.has_key(datetime):
            self.ping_rows[datetime] = AnalyzePingRow(datetime)
        self.ping_rows[datetime].ping_delay_data.append(ping_delay)

    def update_ping_row_from_string(self, master_datetime_diff, ping_line):
        try:
            ping_line_elements = ping_line.split(' ')
            ping_line_datetime = datetime.datetime.strptime(ping_line_elements[0]+' '+ping_line_elements[1], '%Y-%m-%d %H:%M:%S')
            ping_line_datetime -= master_datetime_diff
            ping_line_delay = float(ping_line_elements[4])
            self.update_ping_row(ping_line_datetime, ping_line_delay)
        except:
#            print "Ignore ping line: '%s'" % ping_line.strip()
            pass

    def _calc_ts_sec(self, ts):
        if ts is None:
            return None
        return ts - datetime.timedelta(microseconds=ts.microsecond)

    def update_connect_status(self, connect_ts, connected_ts, menu_available_ts, done_ts, duration_sec):
        connect_ts_sec = connect_ts - datetime.timedelta(microseconds=connect_ts.microsecond)

        start_ts = connect_ts_sec - datetime.timedelta(seconds=duration_sec)
        end_ts = connect_ts_sec + datetime.timedelta(seconds=duration_sec) 
        key_ts = start_ts

        while (key_ts < end_ts):
            if not self.connect_status_rows.has_key(key_ts):
                self.connect_status_rows[key_ts] = AnalyzeConnectStatusRow(key_ts)
            if key_ts < connect_ts:
                self.connect_status_rows[key_ts].waiting_to_connect()
            elif connected_ts is not None and key_ts < connected_ts:
                self.connect_status_rows[key_ts].connecting()
            elif menu_available_ts is not None and key_ts < menu_available_ts:
                self.connect_status_rows[key_ts].connected()
            elif menu_available_ts is None and done_ts is not None:
                self.connect_status_rows[key_ts].not_connected()
            elif done_ts is not None and key_ts < done_ts:
                self.connect_status_rows[key_ts].logged_in()
            elif done_ts is None:
                self.connect_status_rows[key_ts].logged_in()
            elif done_ts is not None and key_ts > done_ts:
                self.connect_status_rows[key_ts].not_connected()
            else:
                print "IGNORED connect", start_ts, key_ts, end_ts
                            
            key_ts += datetime.timedelta(seconds=1)
        if not self.connect_status_rows.has_key(connect_ts_sec):
            self.connect_status_rows[connect_ts_sec] = AnalyzeConnectStatusRow(connect_ts_sec)
        self.connect_status_rows[connect_ts_sec].update(connect_ts_sec, connected_ts, menu_available_ts, done_ts)
        
    def update_performance_header_from_string(self, line):
        line_elements = line.strip().split(',')
        self.performance_header = AnalyzePerformanceHeader(line_elements[1:])

    def update_performance_row_from_string(self, master_datetime_diff, line):
        try:
            line_elements = line.strip().split(',')
            line_datetime = datetime.datetime.strptime(line_elements[0], '%Y-%m-%d %H:%M:%S')
            line_datetime -= master_datetime_diff
            self.performance_rows[line_datetime] = AnalyzePerformanceRow(line_datetime, line_elements[1:])
        except:
            print "Ignore performance line: '%s'" % line.strip()

    def generate_raw_result(self, analysis_folder):
        dest_filename = os.path.join(analysis_folder, 'raw_result.csv')
        dest_file = open(dest_filename, 'w')
        
        header_elements = []
        header_elements.append('timestamp')
        header_elements.append('timestamp offset')
        header_elements.extend(AnalyzePingRow.generate_header_elements())
        header_elements.extend(self.performance_header.generate_header_elements())
        dest_file.write('%s\n' % ','.join(header_elements))

        (data_key_min, data_key_max) = self.get_min_max_row_keys(use_connect_status=False)
        data_key = data_key_min
        while data_key <= data_key_max:
            line_elements = []
            line_elements.append(data_key.strftime('%Y-%m-%d %H:%M:%S'))
            line_elements.append('%d' % (data_key - data_key_min).seconds)
            line_elements.extend(AnalyzePingRow.generate_row_elements(self.ping_rows, data_key))
            line_elements.extend(AnalyzePerformanceRow.generate_row_elements(self.performance_header, self.performance_rows, data_key))
            dest_file.write('%s\n' % ','.join(line_elements))
            data_key += datetime.timedelta(seconds=1)
        dest_file.close()


    def generate_summary_data(self, analysis_folder, analysis_interval_sec, analysis_interval_ignore_sec):
        (data_key_min, data_key_max) = self.get_min_max_row_keys(use_connect_status=False)
        analyze_summary_rows = {}
        
        data_key = data_key_min
        while data_key <= data_key_max:
            data_key_offset = (data_key - data_key_min).seconds
            
            data_key_offset_mod = data_key_offset % analysis_interval_sec
            ignore_data_key = data_key_offset_mod < analysis_interval_ignore_sec or data_key_offset_mod > analysis_interval_sec - analysis_interval_ignore_sec
            
            if data_key_offset_mod == 0:
                analyze_summary_row_current = AnalyzeSummaryRow()
                analyze_summary_rows[data_key_offset] = analyze_summary_row_current
            
            if not ignore_data_key:
                if self.ping_rows.has_key(data_key):
                    analyze_summary_row_current.ping_rows.append(self.ping_rows[data_key])
                if self.performance_rows.has_key(data_key):
                    analyze_summary_row_current.performance_rows.append(self.performance_rows[data_key])
            
            data_key += datetime.timedelta(seconds=1)

        dest_filename = os.path.join(analysis_folder, 'summary_result.csv')
        dest_file = open(dest_filename, 'w')

        header_elements = []
        header_elements.append('idx')
        header_elements.extend(AnalyzePingRow.generate_header_elements())
        header_elements.extend(self.performance_header.generate_header_elements())
        dest_file.write('%s\n' % ','.join(header_elements))
        
        keys = analyze_summary_rows.keys()
        keys.sort()
        for analyze_summary_row_key in keys:
            analyze_summary_rows[analyze_summary_row_key].generate_summary_row(analyze_summary_row_key, dest_file, self.performance_header)
        dest_file.close()

    def generate_connect_status(self, analysis_folder):
        dest_filename = os.path.join(analysis_folder, 'connect_status.csv')
        dest_file = open(dest_filename, 'w')

        header_elements = []
        header_elements.append('timestamp')
        header_elements.append('timestamp offset')
        header_elements.extend(AnalyzeConnectStatusRow.generate_header_elements())
        dest_file.write('%s\n' % ','.join(header_elements))

        (data_key_min, data_key_max) = self.get_min_max_row_keys()
        data_key = data_key_min
        line_state = {'count':0 }

        keys = self.connect_status_rows.keys()
        keys.sort()
        skip_row = True
        for data_key in keys:
            if self.connect_status_rows.has_key(data_key) and len(self.connect_status_rows[data_key].connect_time_secs) > 0:
                skip_row = False
            if not skip_row:
                line_elements = []
                line_elements.append(data_key.strftime('%Y-%m-%d %H:%M:%S'))
                line_elements.append('%d' % (data_key - data_key_min).seconds)
                line_elements.extend(AnalyzeConnectStatusRow.generate_row_elements(self.connect_status_rows, data_key, line_state))
                dest_file.write('%s\n' % ','.join(line_elements))
            if not skip_row and self.connect_status_rows.has_key(data_key) and self.connect_status_rows[data_key].count_waiting_to_connect == 0 and self.connect_status_rows[data_key].count_connecting == 0 and self.connect_status_rows[data_key].count_connected == 0: 
                skip_row = True
            
#            data_key += datetime.timedelta(seconds=1)
        dest_file.close()


class AnalyzePingRow(object):
    def __init__(self, datetime):
        self.datetime = datetime
        self.ping_delay_data = []

    @classmethod
    def generate_header_elements(cls):
        header_elements = []
        header_elements.append('min')
        header_elements.append('max')
        header_elements.append('average')
        header_elements.append('std')
        header_elements.append('var')
        header_elements.append('count')
        return header_elements

    @classmethod
    def generate_row_elements(cls, analyze_ping_rows, data_key):
        line_elements = []
        if analyze_ping_rows.has_key(data_key) and len(analyze_ping_rows[data_key].ping_delay_data)>0:
            ping_delay_data = analyze_ping_rows[data_key].ping_delay_data
            line_elements.append('%.10g' % numpy.min(ping_delay_data))
            line_elements.append('%.10g' % numpy.max(ping_delay_data))
            line_elements.append('%.10g' % numpy.average(ping_delay_data))
            line_elements.append('%.10g' % numpy.std(ping_delay_data, dtype=numpy.float64))
            line_elements.append('%.10g' % numpy.var(ping_delay_data, dtype=numpy.float64))
            line_elements.append('%d' % len(ping_delay_data))
        else:
            line_elements.extend([''] * 6)
        return line_elements

    @classmethod
    def generate_summary(cls, ping_rows):
        ping_row_summary = AnalyzePingRow(datetime.datetime.now())
        for ping_row in ping_rows:
            ping_row_summary.ping_delay_data.extend(ping_row.ping_delay_data)
        return ping_row_summary


class AnalyzePerformanceRow(object):
    def __init__(self, datetime, elements):
        self.datetime = datetime
        self.elements = elements

    @classmethod
    def generate_row_elements(cls, performance_header, performance_rows, data_key):
        line_elements = []
        if performance_rows.has_key(data_key):
            line_elements.extend(performance_rows[data_key].elements)
        elif performance_header is not None:
            line_elements.extend(',' * len(performance_header.elements))
        return line_elements

    @classmethod
    def generate_summary(cls, performance_rows):
        performance_row_summary = AnalyzePerformanceRow(datetime.datetime.now(), [])
        if len(performance_rows) < 1:
            return performance_row_summary

        col_count = len(performance_rows[0].elements)
        for col_idx in range(col_count):
            col_values = [] 
            
            for performance_row in performance_rows:
                col_values.append(int(performance_row.elements[col_idx]))
            performance_row_summary.elements.append('%d' % numpy.average(col_values))
        return performance_row_summary


class AnalyzePerformanceHeader(object):
    def __init__(self, elements):
        self.elements = elements

    def generate_header_elements(self):
        return self.elements


class AnalyzeSummaryRow(object):
    def __init__(self):
        self.ping_rows = []
        self.performance_rows = []
    
    def generate_summary_row(self, key, dest_file, performance_header):
        ping_row_summary = AnalyzePingRow.generate_summary(self.ping_rows)
        performance_row_summary = AnalyzePerformanceRow.generate_summary(self.performance_rows)
        line_elements = []
        line_elements.append('%d' % key)
        line_elements.extend(AnalyzePingRow.generate_row_elements({'1':ping_row_summary}, '1'))
        line_elements.extend(AnalyzePerformanceRow.generate_row_elements(performance_header, {'1':performance_row_summary}, '1'))
        dest_file.write('%s\n' % ','.join(line_elements))


class AnalyzeConnectStatusRow(object):
    def __init__(self, ts):
        self.datetime = ts
        self.connect_time_secs = []
        self.login_time_secs = []

        self.count_waiting_to_connect = 0
        self.count_connecting = 0
        self.count_connected = 0
        self.count_logged_in = 0
        self.count_not_connected = 0

    def waiting_to_connect(self):
        self.count_waiting_to_connect += 1
        
    def connecting(self):
        self.count_connecting += 1
        
    def connected(self):
        self.count_connected += 1

    def logged_in(self):
        self.count_logged_in += 1

    def not_connected(self):
        self.count_not_connected += 1

    def _calc_and_append_ts_delta(self, data, ts_start, ts_stop):
        if ts_start is not None and ts_stop is not None:
            ts_delta = (ts_stop - ts_start)
            ts_delta_sec = ts_delta.seconds + (ts_delta.days * 86400) 
            ts_delta_sec_float = float('%d.%d' % (ts_delta_sec, ts_delta.microseconds))
            data.append(ts_delta_sec_float)

    def update(self, connect_ts, connected_ts, menu_available_ts, done_ts):
        self._calc_and_append_ts_delta(self.connect_time_secs, connect_ts, connected_ts)
        self._calc_and_append_ts_delta(self.login_time_secs, connected_ts, menu_available_ts)

    @classmethod
    def generate_header_elements(cls):
        line_elements = []
        line_elements.append('avg connect_time_sec')
        line_elements.append('avg login_time_sec')
        line_elements.append('count')
        line_elements.append('waiting_to_connect')
        line_elements.append('connecting')
        line_elements.append('connected')
        line_elements.append('logged_in')
        line_elements.append('not_connected')
        return line_elements
    
    @classmethod
    def generate_row_elements(cls, analyze_connect_status_rows, data_key, line_state):
        line_elements = []
        if analyze_connect_status_rows.has_key(data_key):
            status_row = analyze_connect_status_rows[data_key]
            if len(status_row.connect_time_secs) > 0:

                if len(status_row.connect_time_secs) > 0:
                    line_elements.append('%.10g' % numpy.average(status_row.connect_time_secs))
                else:
                    line_elements.append('0')
                if len(status_row.login_time_secs) > 0:
                    line_elements.append('%.10g' % numpy.average(status_row.login_time_secs))
                else:
                    line_elements.append('0')
                line_elements.append('%d' % len(status_row.connect_time_secs))
                line_state['count'] += len(status_row.connect_time_secs)
            else:
                line_elements.append('')
                line_elements.append('')
                line_elements.append('')

            line_elements.append('%d' % status_row.count_waiting_to_connect)
            line_elements.append('%d' % status_row.count_connecting)
            line_elements.append('%d' % status_row.count_connected)
            line_elements.append('%d' % status_row.count_logged_in)
            line_elements.append('%d' % status_row.count_not_connected)

        else:
            print "MISSING", data_key
            line_elements.extend([''] * 4)
        return line_elements

class GOnBotnetMaster(object):
    def __init__(self, commandline_options):
        self.commandline_options = commandline_options

    def run(self):
        client_template_root = self.commandline_options.get_client_template_root()
        if not os.path.exists(client_template_root):
            os.makedirs(client_template_root)

        info_root = self.commandline_options.get_info_root()
        if not os.path.exists(info_root):
            os.makedirs(info_root)

        info_root_result = os.path.join(self.commandline_options.get_botnet_result_root(), datetime.datetime.now().strftime('%Y%m%d_%H%M'))
        if not os.path.exists(info_root_result):
            os.makedirs(info_root_result)

        slaves = []
        slaves.extend(GOnBotnetSlaveProxy.create_from_command_ini(self.commandline_options.get_command_ini()))

        username_generator = UsernameGenerator()
        clients = []
        clients.extend(GOnManagementServerProxy.create_from_command_ini(client_template_root, info_root, info_root_result, self.commandline_options.get_command_ini()))
        clients.extend(GOnBotnetClientProxy.create_from_command_ini(client_template_root, info_root, info_root_result, self.commandline_options.get_command_ini(), username_generator))
        clients.extend(GOnBotnetPerformanceMonitor.create_from_command_ini(info_root_result, self.commandline_options.get_command_ini()))
        
        try:
            for slave in slaves:
                slave.time_sync()

            for client in clients:
                (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message) = self._calc_clients_running(clients)
                print "Waiting for botnet clients to run setup (%d running setup, %d done setup, %d done)" % (clients_running_setup, clients_running_setup_done, clients_done)
                client.start()
                if client.do_connect_by_setup():
                    while not client.setup_is_done():
                        time.sleep(1)
 
        except KeyboardInterrupt:
            print "Waiting for botnet clients to run setup interupted"
            return
        
        time.sleep(10)

        print "All botnet clients are done running setup"
        print ""
        
        for client in clients:
            client.go_running()
            
        (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message) = self._calc_clients_running(clients)
        estimated_remaining_message = 'unknown'
        while (clients_running > 0):
            try:
                time.sleep(2)
                (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message) = self._calc_clients_running(clients)
                print "Waiting for botnet clients (%d running, %d done) estimated time %s" % (clients_running, clients_running_done+clients_done, estimated_remaining_message)
            except KeyboardInterrupt:
                print "Waiting for botnet clients interupted"
                return
        print "All clients are done running"
        print ""
        
        for client in clients:
            time.sleep(0.1)
            client.go_running_collect()

        (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message) = self._calc_clients_running(clients)
        estimated_remaining_message = 'unknown'
        while (clients_running_collect > 0):
            time.sleep(1)
            (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message) = self._calc_clients_running(clients)
            print "Waiting for collection of result (%d collecting, %d done)" % (clients_running_collect, clients_done)

        print ""
        print "Summary:"
        for client in clients:
            message = 'OK'
            if client.state == GOnBotnetClientBase.STATE_DONE_WITH_ERROR:
                message = 'ERROR, ' + client.error_message
            print "  %s %s" % (client.name, message)

        command_ini_filename = os.path.join(info_root_result, 'command.ini')
        self.commandline_options.dump_command_ini(command_ini_filename)



    def _calc_clients_running(self, clients):
        clients_running_setup = 0
        clients_running_setup_done = 0
        clients_running = 0
        clients_running_done = 0
        clients_running_collect = 0
        clients_done = 0
        time_remaing = 1
        for client in clients:
            time_remaing = max(time_remaing, client.get_remaining_time_sec())
            if client.state in [GOnBotnetClientBase.STATE_INIT, GOnBotnetClientBase.STATE_RUNNING_SETUP]:
                clients_running_setup += 1
            elif client.state == GOnBotnetClientBase.STATE_RUNNING_SETUP_DONE:
                clients_running_setup_done += 1
            elif client.state == GOnBotnetClientBase.STATE_RUNNING:
                clients_running += 1
            elif client.state == GOnBotnetClientBase.STATE_RUNNING_DONE:
                clients_running_done += 1
            elif client.state == GOnBotnetClientBase.STATE_RUNNING_COLLECT:
                clients_running_collect += 1
            elif client.state in [GOnBotnetClientBase.STATE_DONE, GOnBotnetClientBase.STATE_DONE_WITH_ERROR]:
                clients_done += 1
        estimated_remaining_message = '%d sec.' % time_remaing
        return (clients_running_setup, clients_running_setup_done, clients_running, clients_running_done, clients_running_collect, clients_done, estimated_remaining_message)
    
    
    def slave_cleanup(self):
        command_ini = self.commandline_options.get_command_ini()
        for section in command_ini.sections():
            if command_ini.has_option(section, 'ip'):
                ip = command_ini.get(section, 'ip')
                port = command_ini.getint(section, 'port')
                try:
                    print "Cleanning up slave %s:%d" % (ip, port)
                    slave_proxy = xmlrpclib.ServerProxy('http://%s:%d' % (ip, port))
                    slave_proxy.cmd_cleanup()
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    print "Cleanup failed", evalue

        print "Cleanning up done"
    
    def analyze(self):
        info_result_root = self.commandline_options.get_botnet_result_root()
        for result_folder in os.listdir(info_result_root):
            result_folder_abs = os.path.join(info_result_root, result_folder)
            self._calc_analyze(result_folder_abs)
        
    def _calc_analyze(self, result_folder):
        analyze_data = AnalyzeData()
        analysis_folder = os.path.join(result_folder, 'analysis')
        if os.path.exists(analysis_folder):
            shutil.rmtree(analysis_folder)
        os.makedirs(analysis_folder)

        command_ini_filename = os.path.join(result_folder, 'command.ini')
        command_ini = self.commandline_options.get_command_ini(command_ini_filename)
        analysis_interval_sec = get_ini_num(command_ini, SECTION_GLOBAL_SETTINGS, 'analysis_interval_sec', 100)
        analysis_interval_ignore_sec = get_ini_num(command_ini, SECTION_GLOBAL_SETTINGS, 'analysis_interval_ignore_sec', 25)
        duration_sec = get_ini_num(command_ini, SECTION_GLOBAL_SETTINGS, 'duration_sec', 240)
        
        print "Calculating %s" % result_folder
        for folder in os.listdir(result_folder):
            master_datetime_diff_sec = 0
            
            time_diff_filename = os.path.join(result_folder, folder, FILENAME_MASTER_DATETIME_DIFF)
            if os.path.exists(time_diff_filename):
                time_diff_file = open(time_diff_filename, 'r')
                master_datetime_diff_sec = int(time_diff_file.read())
                time_diff_file.close()
            
            master_datetime_diff = datetime.timedelta(seconds=master_datetime_diff_sec)
            
            if folder.startswith('client_'):
                gon_client_installation_folder = os.path.join(result_folder, folder, 'info', 'gon_client_installation')
                ping_filename = os.path.join(gon_client_installation_folder, 'ping.log')
                self._add_ping_to_data(analyze_data, ping_filename, master_datetime_diff)
                self._add_connect_status_to_data(analyze_data, gon_client_installation_folder, master_datetime_diff, duration_sec)
                
            if folder.startswith('performance_monitor_'):
                monitor_filename = os.path.join(result_folder, folder, 'info', 'monitor.csv')
                self._add_monitor_to_data(analyze_data, monitor_filename, master_datetime_diff)

        analyze_data.generate_raw_result(analysis_folder)
        analyze_data.generate_summary_data(analysis_folder, analysis_interval_sec, analysis_interval_ignore_sec)
        analyze_data.generate_connect_status(analysis_folder)


    def _add_ping_to_data(self, analyze_data, ping_filename, master_datetime_diff):
        if not os.path.exists(ping_filename):
#            print "Ping file not found : %s " % ping_filename
            return
        
        ignore_line_count = 6
        ping_file = open(ping_filename, 'r')
        for ping_line in ping_file.readlines():
            if ignore_line_count > 0:
               ignore_line_count -= 1 
            else:
                analyze_data.update_ping_row_from_string(master_datetime_diff, ping_line)
        ping_file.close()

    def _read_ts_from_file(self, filename, master_datetime_diff, default_value):
        if not os.path.exists(filename):
            return default_value
        ts_file = open(filename, 'r')
        ts_string = ts_file.read()
        ts_file.close()
        try:
            ts = datetime.datetime.strptime(ts_string, '%Y-%m-%d %H:%M:%S.%f')
        except:
            ts = datetime.datetime.strptime(ts_string, '%Y-%m-%d %H:%M:%S')
        ts -= master_datetime_diff
        return ts

    def _add_connect_status_to_data(self, analyze_data, gon_client_installation_folder, master_datetime_diff, duration_sec):
        connect_filename = os.path.join(gon_client_installation_folder, BotnetClient.STATUS_FILENAME_CONNECT_START)
        connect_ts = self._read_ts_from_file(connect_filename, master_datetime_diff, None)
        connected_filename = os.path.join(gon_client_installation_folder, BotnetClient.STATUS_FILENAME_CONNECTED)
        connected_ts = self._read_ts_from_file(connected_filename, master_datetime_diff, None)
        menu_available_filename = os.path.join(gon_client_installation_folder, BotnetClient.STATUS_FILENAME_MENU_AVAILABLE)
        menu_available_ts = self._read_ts_from_file(menu_available_filename, master_datetime_diff, None)
        done_filename = os.path.join(gon_client_installation_folder, BotnetClient.STATUS_FILENAME_DONE)
        done_ts = self._read_ts_from_file(done_filename, master_datetime_diff, None)
        analyze_data.update_connect_status(connect_ts, connected_ts, menu_available_ts, done_ts, duration_sec)

    def _add_monitor_to_data(self, analyze_data, monitor_filename, master_datetime_diff):
        if not os.path.exists(monitor_filename):
            print "Monitor file not found : %s " % monitor_filename
            return

        is_headerline = True
        monitor_file = open(monitor_filename, 'r')
        for monitor_line in monitor_file.readlines():
            if is_headerline:
                analyze_data.update_performance_header_from_string(monitor_line)
                is_headerline = False
            else:
                analyze_data.update_performance_row_from_string(master_datetime_diff, monitor_line)
        monitor_file.close()

            
            
            
# ========================================================================================================================
def main():
    commandline_options = CommandlineOptions()
    if commandline_options.cmd_serve():
        gon_devbotnet_slave = GOnBotnetSlave(commandline_options)
        gon_devbotnet_slave.run()
        return 0

    if commandline_options.cmd_master() is not None:
        if not commandline_options.cmd_master() in ['run', 'analyze', 'slave_cleanup']:
            print "Invalid command '%s' for --cmd_master" %  commandline_options.cmd_master()
            return 1

        gon_devbotnet_master = GOnBotnetMaster(commandline_options)
        if commandline_options.cmd_master() == 'run':
            gon_devbotnet_master.run()
        if commandline_options.cmd_master() == 'analyze':
            gon_devbotnet_master.analyze()
            
        if commandline_options.cmd_master() == 'slave_cleanup':
            gon_devbotnet_master.slave_cleanup()
        return 0

    commandline_options.print_help()
    print __doc__
    return 0


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    main()
    sys.exit()
