"""
SoftToken plugins for runtime client deploy
"""
from __future__ import with_statement

import os
import os.path
import shutil
import tempfile

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device

import plugin_modules.soft_token.client_gateway_common

from plugin_types.client_gateway_deploy import plugin_type_token


class PluginToken(plugin_type_token.PluginTypeToken):
    def __init__(self, checkpoint_handler):
        plugin_type_token.PluginTypeToken.__init__(self, checkpoint_handler, u'soft_token')
        self._soft_token_tokens = plugin_modules.soft_token.client_gateway_common.SoftTokenTokens(checkpoint_handler)
   
    def initialize_token(self, device):
        self._soft_token_tokens.initialize_token(device)

    def get_tokens(self, additional_device_roots):
        return self._soft_token_tokens.get_tokens(self.plugin_name, additional_device_roots)

    def deploy_token(self, token_id, client_knownsecret, servers):
        self._soft_token_tokens.deploy_token(token_id, client_knownsecret, servers)

    def generate_keypair(self, token_id):
        self._soft_token_tokens.generate_keypair(token_id)

    def get_public_key(self, token_id):
        return self._soft_token_tokens.get_public_key(token_id)

    def set_serial(self, token_id, serial):
        self._soft_token_tokens.set_serial(token_id, serial)

    def get_serial(self, token_id):
        return self._soft_token_tokens.get_serial(token_id)
    
    def set_enrolled(self, token_id):
        self._soft_token_tokens.set_enrolled(token_id)

    def reset_enrolled(self, token_id):
        self._soft_token_tokens.reset_enrolled(token_id)

    def is_enrolled(self, token_id):
        return self._soft_token_tokens.is_enrolled(token_id)
    