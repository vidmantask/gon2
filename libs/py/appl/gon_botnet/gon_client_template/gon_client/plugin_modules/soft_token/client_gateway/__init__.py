"""
SoftToken plugin for Gateway Client
"""
from __future__ import with_statement

import sys
import os
import lib.checkpoint
import lib.cryptfacility
import lib.appl.gon_client

from components.communication import message

from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_client_runtime_env
import plugin_types.client_gateway_deploy.plugin_type_token

import plugin_modules.soft_token.client_gateway_common

import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_installer



class PluginToken(plugin_type_token.PluginTypeToken):
    """
    Token plugin 
    """
    PLUGIN_NAME = u'soft_token_token'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginToken.PLUGIN_NAME, user_interface, additional_device_roots)
        self._soft_token_tokens = plugin_modules.soft_token.client_gateway_common.SoftTokenTokens(checkpoint_handler)    

    def get_tokens(self, additional_device_roots):
        return self._soft_token_tokens.get_tokens(self.plugin_name, additional_device_roots)
    
    def get_knownsecret_and_servers(self, token_id):
        return self._soft_token_tokens.get_knownsecret_and_servers(token_id)


class PluginAuthentication(plugin_type_auth.PluginTypeAuth):
    """
    Client part of SoftToken authentication plugin
    """
    PLUGIN_NAME = u'soft_token'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginAuthentication.PLUGIN_NAME, user_interface, additional_device_roots)
        self._soft_token_tokens = plugin_modules.soft_token.client_gateway_common.SoftTokenTokens(checkpoint_handler)    
        self._token_id = None

    def _select_token(self):
        if self._token_id == None:
            tokens = self._soft_token_tokens.get_tokens(self.plugin_name, self.additional_device_roots )
            if len(tokens) > 0:
                return tokens[0].token_id
            return None
        else:
            return self._token_id
        
    def _get_serial(self):
        token_id = self._select_token() 
        if token_id != None:
            try:
                return unicode(self._soft_token_tokens.get_serial(token_id))
            except:
                self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.DEBUG, message='No serial found')
                return None
        else:
            self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None
        
    def _get_private(self):
        token_id = self._select_token() 
        if token_id != None:
            try:
                return self._soft_token_tokens.get_private_key(token_id)
            except plugin_types.client_gateway_deploy.plugin_type_token.Error:
                self.checkpoint_handler.Checkpoint('_get_private', self.plugin_name, lib.checkpoint.WARNING, message='No private key available')
                return None
        else:
            self.checkpoint_handler.Checkpoint('_get_private', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None

    msg_receivers = ['get_serial', 'get_challenge']
    
    def get_serial(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            serial = self._get_serial()
            cps.add_complete_attr(serial=serial)
            self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))
                
    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            try:
                private_key = self._get_private()
                if private_key != None:
                    challenge_signature = lib.cryptfacility.pk_sign_challenge(private_key, challenge)
                    self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))
                else:
                    self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=None))
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_challenge.failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=None))


class PluginClientRuntimeEnv(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv):
    """
    Client runtime envrionment plugin
    """
    PLUGIN_NAME = u'soft_token_client_runtime_env'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv.__init__(self, async_service, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, user_interface, additional_device_roots)
        self._instances = {}

    def _build_instances(self):
        self._instances = {}
        soft_token_tokens = plugin_modules.soft_token.client_gateway_common.SoftTokenTokens(self.checkpoint_handler)    
        tokens = soft_token_tokens.get_tokens(PluginClientRuntimeEnv.PLUGIN_NAME, self.additional_device_roots)
        for token in tokens:
            self._instances[token.token_id] = PluginClientRuntimeEnvInstance(self.checkpoint_handler, token.token_id)

    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin. 
        """
        self._build_instances()
        return self._instances.keys()

    def get_instance(self, runtime_env_id):
        """
        Return the instance. 
        """
        self._build_instances()
        if runtime_env_id in self._instances.keys():
            return self._instances[runtime_env_id]
        return None
    


class PluginClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase):
    def __init__(self, checkpoint_handler, token_id):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase.__init__(self, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, token_id, 'SoftToken',
                                                                                                   [plugin_modules.soft_token.client_gateway_common.SoftTokenTokens.INIT_FOLDERNAME, plugin_modules.soft_token.client_gateway_common.SoftTokenTokens.DEPLOY_FOLDERNAME])
    def set_knownsecre_and_servers(self, knownsecret, servers, generate_keypair):
        endpoint_token_deploy = plugin_modules.soft_token.client_gateway_common.SoftTokenTokens(self.checkpoint_handler)
        if generate_keypair:
            endpoint_token_deploy.initialize_token(self.get_root())
            endpoint_token_deploy.generate_keypair(self.get_root())
            endpoint_token_deploy.reset_enrolled(self.get_root())
        endpoint_token_deploy.deploy_token(self.get_root(), knownsecret, servers)
