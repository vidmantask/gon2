"""
ClientOk plugin for Gateway Client
"""
from __future__ import with_statement

import sys

from components.communication import message


from plugin_types.client_gateway import plugin_type_tag


class PluginTag(plugin_type_tag.PluginTypeTag):
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_tag.PluginTypeTag.__init__(self, async_service, checkpoint_handler, u'client_ok', user_interface, additional_device_roots)
        
    def remote_get_platform(self):
        self.tunnelendpoint_remote('remote_get_platform_response', platform=sys.platform)
