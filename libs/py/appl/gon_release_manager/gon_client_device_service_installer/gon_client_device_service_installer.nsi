!include "FileFunc.nsh"

!include "gon_client_device_service_installer_include.nsh"
!define GON_INSTALLER_DESTINATION_ROOT "${GIRI_BUILD_FILES}\installers\win"
!define GON_INSTALLATION_ROOT "${GIRI_BUILD_FILES}\build_store"
!define GON_ICON_FILENAME   "..\graphics\gon_installer.ico"

;
; NSIS Configuration
;
Name "G/On Client Device Service Installer"
Icon "${GON_ICON_FILENAME}"
OutFile "${GON_INSTALLER_DESTINATION_ROOT}\gon_client_device_service_installer.exe"

InstallDir $PROGRAMFILES\Giritech\gon_client_device_service
RequestExecutionLevel admin
SilentInstall silent
BrandingText " "


;
; Sections
;
Section "Install Section" InstallSection
  SetOutPath "$INSTDIR"
  File /r "${GON_INSTALLATION_ROOT}\gon_client_device_service\win\*"

  ; Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "DisplayName" "G-On Client Device Service"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "Publisher" "Giritech"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "DisplayVersion" "${GIRI_VERSION}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service" "NoRepair" 1

  nsExec::ExecToStack '"$INSTDIR\gon_client_device_service.exe" --startup auto install'
  Sleep 1000
  nsExec::ExecToStack '"$INSTDIR\gon_client_device_service.exe" start'
  Sleep 1000
SectionEnd

Section "Uninstall"
  nsExec::ExecToStack '"$INSTDIR\gon_client_device_service.exe" stop'
  Sleep 1000
  nsExec::ExecToStack '"$INSTDIR\gon_client_device_service.exe" remove'
  Sleep 1000

  DetailPrint "Deleting Files"
  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Client Device Service"
SectionEnd
