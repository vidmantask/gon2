!include "MUI2.nsh"
!include "x64.nsh"

;
; Include generated definitions
;
;!define GIRI_VERSION '5.1.1-3'
;!define GIRI_OUT_PATH 'C:\release_store\${GIRI_VERSION}'
;!define GIRI_BUILD_FILES 'C:\release_store\${GIRI_VERSION}\build_files'
!include "gon_5_include.nsh"

;
; Giritech variabel definitions
;
!ifdef GIRI_BETA
 !define GIRI_TITLE 'G/On BETA'
 !define GIRI_IMAGE_INSTALLER_SPLASH gon_5_install_splash_beta.bmp
 !define GIRI_IMAGE_INSTALLER_V gon_5_v_beta.bmp
 !define GIRI_IMAGE_INSTALLER_H gon_5_installer_h_beta.bmp
 !define GIRI_IMAGE_UNINSTALLER_SPLASH gon_5_install_splash_beta.bmp
 !define GIRI_IMAGE_UNINSTALLER_V gon_5_uninstaller_v_beta.bmp
 !define GIRI_IMAGE_UNINSTALLER_H gon_5_uninstaller_h_beta.bmp
!else
 !define GIRI_TITLE 'G/On'
 !define GIRI_IMAGE_INSTALLER_SPLASH gon_5_install_splash.bmp
 !define GIRI_IMAGE_INSTALLER_V gon_5_v.bmp
 !define GIRI_IMAGE_INSTALLER_H gon_5_installer_h.bmp
 !define GIRI_IMAGE_UNINSTALLER_SPLASH gon_5_install_splash.bmp
 !define GIRI_IMAGE_UNINSTALLER_V gon_5_uninstaller_v.bmp
 !define GIRI_IMAGE_UNINSTALLER_H gon_5_uninstaller_h.bmp
!endif

;
; Misc variabel definitions
;
!define MUI_WELCOMEPAGE_TITLE "Welcome to the G/On Server Installation"
!define MUI_WELCOMEPAGE_TEXT "You are about to install ${GIRI_TITLE} ${GIRI_VERSION}$\r$\n$\r$\nWhen the installation is completed, you have the option to use a configuration wizard to configure the G/On Server.$\r$\n$\r$\n$\r$\nClick Next to install the G/On Server or Cancel to exit."

!define MUI_UNTEXT_WELCOME_INFO_TITLE "Welcome to the G/On Uninstaller"
!define MUI_UNTEXT_WELCOME_INFO_TEXT "WARNING: This will uninstall G/On and all G/On settings. If you are completely sure you want to continue, click Next.$\r$\n$\r$\nClick Cancel to cancel this uninstallation."

!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH
!define MUI_WELCOMEFINISHPAGE_BITMAP "${GIRI_IMAGE_INSTALLER_H}"
!define MUI_UNWELCOMEFINISHPAGE_BITMAP_NOSTRETCH
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "${GIRI_IMAGE_UNINSTALLER_H}"

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${GIRI_IMAGE_INSTALLER_V}"

!define MUI_COMPONENTSPAGE_SMALLDESC

!define MUI_FINISHPAGE_TITLE "G/On 5 Successfully Installed"
!define MUI_FINISHPAGE_TEXT "You are now ready to configure the G/On Server.$\r$\n$\r$\nClick Finish now and use the configuration wizard to configure the G/On Server or remove the checkmark below and click Finish to exit."

!define MUI_FINISHPAGE_LINK "Visit www.solitonsystems.com for the latest news."
!define MUI_FINISHPAGE_LINK_LOCATION "https://solitonsystems.com/it-security/products/g-on"

!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_FUNCTION "LaunchGONConfig"
!define MUI_FINISHPAGE_RUN_TEXT "Run G/On Server Configuration wizard"
!define MUI_FINISHPAGE_NOREBOOTSUPPORT


BrandingText " "


;
; Function definitions
;
Function .onInit
        InitPluginsDir
        File /oname=$PLUGINSDIR\splash.bmp "${GIRI_IMAGE_INSTALLER_SPLASH}"
        advsplash::show 1500 600 400 -1 $PLUGINSDIR\splash
        Delete $PLUGINSDIR\splash.bmp
FunctionEnd


;
;
;
!include LogicLib.nsh
Var ReturnCode



;
; NSIS Configuration
;
OutFile ${GIRI_OUT_PATH}\gon_${GIRI_VERSION}.exe

InstallDir $PROGRAMFILES\Giritech\gon_${GIRI_VERSION}
RequestExecutionLevel admin

Name "${GIRI_TITLE}"
Caption "${GIRI_TITLE} Installer"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE gon_5_license_eula.rtf
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

Icon "..\graphics\gon_installer.ico"
UninstallIcon "..\graphics\gon_uninstaller.ico"



Section "Install Section" InstallSection
;  SetOutPath "$INSTDIR\application_clients"
;  File /r "${GIRI_BUILD_FILES}\client_file_packages\application_clients\*"

  SetOutPath "$INSTDIR\gon_client"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client\*"

  SetOutPath "$INSTDIR\gon_client_launch"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_launch\*"

  SetOutPath "$INSTDIR\gon_client_management"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_management\*"

  SetOutPath "$INSTDIR\gon_client_management_service"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_management_service\*"

  SetOutPath "$INSTDIR\gon_server_management_service"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_server_management_service\*"

  SetOutPath "$INSTDIR\gon_server_gateway_service"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_server_gateway_service\*"

  SetOutPath "$INSTDIR\gon_config"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_config\*"

  SetOutPath "$INSTDIR\gon_config_service"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_config_service\*"

  SetOutPath "$INSTDIR\gon_client_installer"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_installer\*"

;  SetOutPath "$INSTDIR\gon_client_stealth"
;  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_stealth\*"

;  SetOutPath "$INSTDIR\gon_client_device_service"
;  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_device_service\*"

  SetOutPath "$INSTDIR\gon_client_uninstaller\win"
  File "${GIRI_BUILD_FILES}\installers\win\gon_client_uninstaller.exe"

  SetOutPath "$INSTDIR\distribution\*"
  File /r "${GIRI_BUILD_FILES}\distribution\"

;  SetOutPath "$INSTDIR\distribution\gon_client_device_service_installer\win"
;  File "/oname=G-On Client Device Service Installer ${GIRI_VERSION}.exe" "${GIRI_BUILD_FILES}\installers\win\gon_client_device_service_installer.exe"

  SetOutPath "$INSTDIR\config\*"
  File /r "${GIRI_BUILD_FILES}\config\"

  SetOutPath "$INSTDIR"
  File /oname=LICENSE.txt "gon_5_license_3part.txt"
  File /oname=LICENSE_EULA.rtf "gon_5_license_eula.rtf"

  ; Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "DisplayName" "G-On Server ${GIRI_VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "Publisher" "Soliton"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "DisplayVersion" "${GIRI_VERSION}"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}" "NoRepair" 1

  DetailPrint "Installing G/On Management Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_management_service\win\gon_server_management_service.exe" install'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to install G/On Management Server Service"
  ${EndIf}

  DetailPrint "Installing G/On Gateway Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_gateway_service\win\gon_server_gateway_service.exe" install'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to install G/On Gateway Server Service"
  ${EndIf}

  ; Create menu short cuts
  DetailPrint "Creating short cuts"
  CreateDirectory "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}"

${If} ${RunningX64}
  SetOutPath "$INSTDIR\gon_client_management\win_64"
  CreateShortCut "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Management.lnk" "$INSTDIR\gon_client_management\win_64\gon_client_management.exe"
${Else}
  SetOutPath "$INSTDIR\gon_client_management\win"
  CreateShortCut "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Management.lnk" "$INSTDIR\gon_client_management\win\gon_client_management.exe"
${EndIf}

${If} ${RunningX64}
  SetOutPath "$INSTDIR\gon_config\win_64"
  CreateShortCut "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Configuration.lnk" "$INSTDIR\gon_config\win_64\gon_config.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers" "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Configuration.lnk" "RUNASADMIN"
${Else}
  SetOutPath "$INSTDIR\gon_config\win"
  CreateShortCut "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Configuration.lnk" "$INSTDIR\gon_config\win\gon_config.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers" "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Configuration.lnk" "RUNASADMIN"
${EndIf}

SectionEnd


Section "Uninstall"
  DetailPrint "Removing G/On Management Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_management_service\win\gon_server_management_service.exe" remove'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to remove G/On Management Server Service"
  ${EndIf}

  DetailPrint "Removing G/On Gateway Server Service"
  nsExec::ExecToStack '"$INSTDIR\gon_server_gateway_service\win\gon_server_gateway_service.exe" remove'
  Pop $ReturnCode
  ${If} $ReturnCode != 0
  	MessageBox MB_OK "Unable to remove G/On Gateway Server Services"
  ${EndIf}

  DetailPrint "Deleting Files"
  Delete "$INSTDIR\Uninstall.exe"
  RMDir /r "$INSTDIR"
  RMDir /r "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}"

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\G-On Server ${GIRI_VERSION}"
SectionEnd

Function LaunchGONConfig
  ExecShell "" "$SMPROGRAMS\Giritech\G-On Server ${GIRI_VERSION}\G-On Server Configuration.lnk"
FunctionEnd
