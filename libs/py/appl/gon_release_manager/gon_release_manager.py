"""
release_manager

This tool collect and build a release package
"""
import os
import sys
import os.path
import shutil
import subprocess
import optparse
import difflib
import glob
import tarfile

home_path = os.path.abspath(os.path.dirname(__file__))
lib_path = os.path.normpath(os.path.join(home_path, '..', '..'))
sys.path.append(lib_path)

#os.chdir(home_path)

import lib.utility
import lib.appl.upgrade
import lib.commongon
import lib.version
import lib.utility

GIT_ROOT = os.path.join(home_path, '..', '..', '..', '..')
BUILD_STORE_ROOT = os.path.join(GIT_ROOT, 'gon_build', 'build_store')
#RELEASE_STORE_ROOT = os.path.join(GIT_ROOT, 'gon_build', 'release_store')
RELEASE_STORE_ROOT = 'C:\\gon_release'


SETUP_RELEASE_ROOT = os.path.join(home_path, '..', '..', '..', 'setup', 'release')
SETUP_KEY_STORE_ROOT = os.path.join(home_path, '..', '..', '..', 'setup', 'key_store')
WEB_ROOT = os.path.join(home_path, '..', '..', '..', 'web_app', 'www_root')


HG_EXE = 'hg'
NSIS_EXE = 'C:\\Program Files (x86)\\NSIS\\makensis.exe'
NSIS_GON_5 = os.path.join(home_path, 'gon_5', 'gon_5.nsi')
NSIS_GON_5_INCLUDE = os.path.join(home_path, 'gon_5', 'gon_5_include.nsh')

#DISTRIBUTE_ORGIN_LIST = ['dev-build-mac-1', 'dev-build-win-5-6', 'dev-build-fedora21-x64-3', 'dev-build-centos5_x64-1', 'dev-build-centos6_x64-1', 'dev-build-centos7_x64-1']
DISTRIBUTE_ORGIN_LIST = ['mac_64', 'win_32', 'linux_64']


#NSIS_CLIENT_INSTALLER = os.path.join(home_path, 'gon_client_installer', 'gon_client_installer.nsi')
#NSIS_CLIENT_INSTALLER_INCLUDE = os.path.join(home_path, 'gon_client_installer', 'gon_client_installer_include.nsh')

NSIS_CLIENT_UNINSTALLER = os.path.join(home_path, 'gon_client_uninstaller', 'gon_client_uninstaller.nsi')
NSIS_CLIENT_UNINSTALLER_INCLUDE = os.path.join(home_path, 'gon_client_uninstaller', 'gon_client_uninstaller_include.nsh')

#NSIS_CLIENT_DEVICE_SERVICE_INSTALLER = os.path.join(home_path, 'gon_client_device_service_installer', 'gon_client_device_service_installer.nsi')
#NSIS_CLIENT_DEVICE_SERVICE_INSTALLER_INCLUDE = os.path.join(home_path, 'gon_client_device_service_installer', 'gon_client_device_service_installer_include.nsh')


SIGN_EXE = "signtool"
SIGN_FILES = [
              '.\\build_store\\gon_server_management_service\\win\\gon_server_management_service.exe',
              '.\\build_store\\gon_server_gateway_service\\win\\gon_server_gateway_service.exe',
              '.\\build_store\\gon_config_service\\win\\gon_config_service.exe',
              '.\\build_store\\gon_config\\gon_config.exe',
              '.\\build_store\\gon_client_management_service\\win\\gon_client_management_service.exe',
              '.\\build_store\\gon_client_management\\gon_client_management.exe',
              '.\\build_store\\gon_client_launch\\win\\gon_client_launch.exe',
              '.\\build_store\\gon_client\\win\\gon_client.exe',
#              '.\\build_store\\gon_client_installer\\win\\gon_client_installer.exe',
#              '.\\build_store\\gon_client_uninstaller\\win\\gon_client_uninstaller.exe',
#              '.\\installers\\win\\gon_client_installer.exe',
#              '.\\installers\\win\\gon_client_device_service_installer.exe',

              '.\\installers\\win\\gon_client_uninstaller.exe',

             ]

class ReleaseError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class Options(object):
    """
    Wrapping of commandline options
    """
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--list_versions', action='store_true', help='Show all available versions and exit')
        self._parser.add_option('--build_release', action='store_true', help='Build G/On 5 Release')
        self._parser.add_option('--build_release_prepare', action='store_true', help='Build G/On 5 Release preperation')
        self._parser.add_option('--version', type='string', default=None, help='Version to build')
        self._parser.add_option('--do_not_clean', action='store_true', default=False, help='Do not clean build_store')
        self._parser.add_option('--not_beta', action='store_true', default=False, help='Build release version')
        self._parser.add_option('--list_release_versions', action='store_true', help='Show all available release versions and exit')
        self._parser.add_option('--build_installation_diff', action='store_true', help='Build a diff report of diffs between installed versions in release store.')
        self._parser.add_option('--build_client_installer', action='store_true', help='Build a client installer alone.')
        self._parser.add_option('--sign', action='store_true', help='Sign all exe-files.')
        self._parser.add_option('--versions', type='string', default=None, help='Versions to diff')
        self._parser.add_option('--build_id',  type='string', default='0', help='')
        self._parser.add_option('--build_store', type='string', default=BUILD_STORE_ROOT, help='')
        self._parser.add_option('--release_store', type='string', default=RELEASE_STORE_ROOT, help='')
        (self._options, self._args) = self._parser.parse_args()

    def cmd_list_versions(self):
        return self._options.list_versions

    def cmd_list_release_versions(self):
        return self._options.list_release_versions

    def cmd_build_release(self):
        return self._options.build_release

    def cmd_build_release_prepare(self):
        return self._options.build_release_prepare

    def cmd_build_installation_diff(self):
        return self._options.build_installation_diff

    def cmd_build_client_installer(self):
        return self._options.build_client_installer

    def cmd_sign(self):
        return self._options.sign

    def get_version(self):
        version = lib.version.Version.create_current(build_id=int(self._options.build_id)).get_version_string().strip()
        return version

    def get_versions(self):
        return self._options.versions

    def get_beta(self):
        return not self._options.not_beta

    def get_clean(self):
        return not self._options.do_not_clean

    def build_id(self):
        return self._options.build_id

    def build_store(self):
        return self._options.build_store

    def release_store(self):
        return self._options.release_store

    def get_versions_in_store(self):
        versions = []
        for folder in os.listdir(self.build_store()):
            if folder[0].isdigit():
                version = lib.version.Version.create_from_string(folder)
                versions.append(version)

        versions.sort(lib.version.Version.compare)

        result = []
        for version in versions:
            result.append(version.get_version_string().strip())
        return result

    def get_release_versions_in_store(self):
        dirs = []
        for dir in os.listdir(self.release_store()):
            if dir[0].isdigit():
                dirs.append(dir)

        result = []
        for dir in sorted(dirs):
            result.append(dir)
        return result

def cmd_list_versions(options):
    dirs = options.get_versions()
    for dir in dirs:
        print dir

def cmd_list_release_versions(options):
    dirs = options.get_versions_in_store(options)
    for dir in dirs:
        print dir


def cmd_release_prepare(options):
    version = options.get_version()
    print "Prepare release for version %s" % version

#    source_build_store_path = os.path.join(BUILD_STORE_ROOT, version)
#    if os.path.exists(source_build_store_path):
#        print "cleaning old build_store '%s'" % source_build_store_path
#        shutil.rmtree(source_build_store_path)
#    os.makedirs(source_build_store_path)

    release_store_path = os.path.join(options.release_store(), version)
    if os.path.exists(release_store_path):
        print "cleaning old release_store '%s'" % release_store_path
        shutil.rmtree(release_store_path)
    os.makedirs(release_store_path)


def cmd_release(options, clean=True, beta=True):
    version = options.get_version()
    if beta:
        print "Building BETA release for the version %s" % (version)
    else:
        print "Building release for the version %s" % (version)

    source_build_store_path = os.path.join(options.build_store(), version)
    if not os.path.exists(source_build_store_path):
        raise ReleaseError("Build store '%s' for this version can not be found. " % (source_build_store_path))

    release_store_path = os.path.join(options.release_store(), version)
#    if clean:
#        if os.path.exists(release_store_path):
#            shutil.rmtree(release_store_path)

    if not os.path.exists(release_store_path):
        os.makedirs(release_store_path)

    release_log_filename = os.path.join(release_store_path, 'release_manager.log')
    release_log_file = open(release_log_filename, 'wt')
    print release_log_filename

    release_store_build_files_path = os.path.join(release_store_path, 'build_files')
    if not os.path.exists(release_store_build_files_path):
        os.makedirs(release_store_build_files_path)

    build_store_path = _copy_build_store(source_build_store_path, release_store_build_files_path, release_log_file)
    client_file_package_path = _copy_client_file_package(release_store_build_files_path, release_log_file)
#    if clean:
    _copy_file_magic(build_store_path, client_file_package_path, release_log_file, release_store_build_files_path, version)
    _copy_to_release_store(build_store_path, release_store_path)

#    _build_client_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file)
    _build_client_uninstaller(version, beta, release_store_path, release_store_build_files_path, release_log_file)

# Disabled never released
#    _build_client_device_service_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file)

#    _sign_files(version, beta, release_store_path, release_store_build_files_path, release_log_file)

    _build_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file)
    _build_filelist(version, release_store_path, build_store_path, release_log_file)


def _copy_program_for_arch(src_root, dst_root, platforms, programs):
    for platform_from, platform_to in platforms:
        for program in programs:
            gon_config_src = os.path.join(src_root, program, platform_from, program)
            gon_config_dest = os.path.join(dst_root, program, platform_to)
            print(gon_config_src, gon_config_dest)
            if os.path.exists(gon_config_src):
                print "Copying %s to %s" % (gon_config_src, gon_config_dest)
                _copy_files(gon_config_src, gon_config_dest, include_sub_folders=True)

def _copy_build_store(source_build_store_path, release_store_build_files_path, release_log_file):
    build_store_path = os.path.join(release_store_build_files_path, 'build_store')
    if not os.path.exists(build_store_path):
        _write_release_log_seperator(release_log_file, 'Creating build_store')
        if not os.path.exists(release_store_build_files_path):
            os.makedirs(release_store_build_files_path)

        for distribution_filename in glob.glob(os.path.join(source_build_store_path, 'distribute.*.tgz' )):
            distribution_filename_elements = os.path.basename(distribution_filename).split('.')
            if len(distribution_filename_elements) > 2:
                distribution_orgin = distribution_filename_elements[1]
                if distribution_orgin in DISTRIBUTE_ORGIN_LIST:
                    print "Extracting %s" % os.path.basename(distribution_filename)
                    tar = tarfile.open(distribution_filename, 'r:gz')
                    tar.extractall(build_store_path)
                    tar.close()
                else:
                    print "WARNING, distribution from %s not used" % distribution_orgin

#        shutil.copytree(source_build_store_path, build_store_path)
        platforms = [('win32.win32.x86', 'win'), ('win32.win32.x86_64', 'win_64')]
        programs = ['gon_config', 'gon_client_management']
        _copy_program_for_arch(os.path.join(GIT_ROOT, 'build_store_prebuild'), build_store_path, platforms, programs)

        print "Creating build_store, done"

    return build_store_path


def _copy_client_file_package(release_store_build_files_path, release_log_file):
    client_file_package_path = os.path.join(release_store_build_files_path, 'client_file_packages')

    if not os.path.exists(client_file_package_path):
        _write_release_log_seperator(release_log_file, 'Copy client_file_packages')
        _copy_files(os.path.join(GIT_ROOT, 'client_file_packages_prebuild'), client_file_package_path, include_sub_folders=True)

        print "Copy client_file_packages_prebuild, done"
    return client_file_package_path

def _copy_files(source_folder, dest_folder, include_sub_folders=False, exts=[]):
    if not os.path.exists(source_folder):
        return

    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)

    for filename in os.listdir(source_folder):
        filename_source_abs = os.path.join(source_folder, filename)
        (root, ext) = os.path.splitext(filename_source_abs)
        include_file = len(exts) == 0 or ext in exts
        if include_file and os.path.isfile(filename_source_abs):
            shutil.copy(filename_source_abs, dest_folder)
        if include_sub_folders and os.path.isdir(filename_source_abs):
            _copy_files(filename_source_abs, os.path.join(dest_folder, filename), include_sub_folders, exts)

def _copy_file(filename_source_abs, dest_folder):
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    shutil.copy(filename_source_abs, dest_folder)

def _copy_to_release_store(build_store_path, release_store_path):
    folders_to_copy = ['tgzs']
    for folder_to_copy in folders_to_copy:
        _copy_files(os.path.join(build_store_path, folder_to_copy), os.path.join(release_store_path, folder_to_copy), include_sub_folders=True)

def _copy_file_magic(build_store_path, client_file_package_path, release_log_file, release_store_build_files_path, version):
    _write_release_log_seperator(release_log_file, 'Copy File Magic')

    config_root_dest = os.path.join(release_store_build_files_path, 'config')

    #
    # Dooing gpm magic
    #
    gpmdefs_root_dest = os.path.join(config_root_dest, 'gpm', 'gpmdefs')
    gpmcdefs_root_dest = os.path.join(config_root_dest, 'gpm', 'gpmcdefs')
    gpms_root_dest = os.path.join(config_root_dest, 'gpm', 'gpms')

    # gpm files from source depot (e.g. gon_client)
    gpmdefs_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'gpm', 'gpmdefs')
    gpmcdefs_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'gpm', 'gpmcdefs')
    gpms_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'gpm', 'gpms')
    _copy_files(gpmdefs_root_source, gpmdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(gpmcdefs_root_source, gpmcdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(gpms_root_source, gpms_root_dest, include_sub_folders=True, exts=['.gpm'])

    # gpm files from client_file_package source
    client_file_package_gpmdefs_root_source = os.path.join(client_file_package_path, 'gpm', 'gpmdefs')
    client_file_package_gpmcdefs_root_source = os.path.join(client_file_package_path, 'gpm', 'gpmcdefs')
    client_file_package_gpms_root_source = os.path.join(client_file_package_path, 'gpm', 'gpms')
    _copy_files(client_file_package_gpmdefs_root_source, gpmdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(client_file_package_gpmcdefs_root_source, gpmcdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(client_file_package_gpms_root_source, gpms_root_dest, include_sub_folders=True, exts=['.gpm'])

    # gpm files from client_file_package prebuild
    client_file_package_prebuild_gpmdefs_root_source = os.path.join(GIT_ROOT, 'client_file_package_prebuild', 'gpm', 'gpmdefs')
    client_file_package_prebuild_gpmcdefs_root_source = os.path.join(GIT_ROOT, 'client_file_package_prebuild', 'gpm', 'gpmcdefs')
    client_file_package_prebuild_gpms_root_source = os.path.join(GIT_ROOT, 'client_file_package_prebuild', 'gpm', 'gpms')
    _copy_files(client_file_package_prebuild_gpmdefs_root_source, gpmdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(client_file_package_prebuild_gpmcdefs_root_source, gpmcdefs_root_dest, include_sub_folders=True, exts=['.xml'])
    _copy_files(client_file_package_prebuild_gpms_root_source, gpms_root_dest, include_sub_folders=True, exts=['.gpm'])

    lib.appl.upgrade.generate_and_save_checksum_for_folder(gpmdefs_root_dest)
    lib.appl.upgrade.generate_and_save_checksum_for_folder(gpmcdefs_root_dest)

    #
    # Dooing Templates magic
    #
    template_root_dest = os.path.join(config_root_dest, 'templates')
    template_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'templates')
    _copy_files(template_root_source, template_root_dest, include_sub_folders=True, exts=['.xml'])
    lib.appl.upgrade.generate_and_save_checksum_for_folder(template_root_dest)

    #
    # Dooing distribution  magic
    #
    distribution_root_source = os.path.join(SETUP_RELEASE_ROOT, 'distribution')
    distribution_root_dest = os.path.join(release_store_build_files_path, 'distribution')
    _copy_files(distribution_root_source, distribution_root_dest, include_sub_folders=True)

    nsi_include_filename = os.path.join(release_store_build_files_path, 'distribution', 'gon_server_gateway_service_installer', 'win', 'nsis', 'gon_5_include.nsh')
    _build_nsi_include_gateway_server(nsi_include_filename, version)


    #
    # Dooing images  magic
    #
    images_root_dest = os.path.join(config_root_dest, 'images')
    images_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'images')
    _copy_files(images_root_source, images_root_dest, include_sub_folders=False)

    #
    # Dooing license magic
    #
    deployed_root_dest = os.path.join(config_root_dest, 'deployed')
    deployed_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'deployed')
    _copy_files(deployed_root_source, deployed_root_dest, include_sub_folders=False)


    #
    # Dooing certificate magic
    #
    certificate_filename = os.path.join(SETUP_KEY_STORE_ROOT, 'gon_config.cacerts')
    gon_config_root_dest = os.path.join(release_store_build_files_path, 'build_store', 'gon_config')
    _copy_file(certificate_filename, os.path.join(gon_config_root_dest, 'win'))
    _copy_file(certificate_filename, os.path.join(gon_config_root_dest, 'win_64'))

    certificate_filename = os.path.join(SETUP_KEY_STORE_ROOT, 'gon_management_client.cacerts')
    gon_client_management_root_dest = os.path.join(release_store_build_files_path, 'build_store', 'gon_client_management')
    _copy_file(certificate_filename, os.path.join(gon_client_management_root_dest, 'win'))
    _copy_file(certificate_filename, os.path.join(gon_client_management_root_dest, 'win_64'))

    certificate_root_dest = os.path.join(config_root_dest, 'certificates')
    certificate_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config', 'certificates')
    _copy_files(certificate_root_source, certificate_root_dest, include_sub_folders=True)

    #
    # Dooing config magic
    #
    config_root_source = os.path.join(SETUP_RELEASE_ROOT, 'config')
    _copy_files(config_root_source, config_root_dest, include_sub_folders=False)
    lib.appl.upgrade.generate_and_save_checksum_for_folder(config_root_dest)

    #
    # Dooing web magic
    #
    web_root_dest = os.path.join(config_root_dest, 'www_root', 'dme')
    web_root_source = os.path.join(WEB_ROOT, 'dme')
    _copy_files(web_root_source, web_root_dest, include_sub_folders=True)

    web_root_dest = os.path.join(config_root_dest, 'www_root', 'web_app_demo', 'vacation_app', 'build')
    web_root_source = os.path.join(WEB_ROOT, 'web_app_demo', 'vacation_app', 'build')
    _copy_files(web_root_source, web_root_dest, include_sub_folders=True)

    web_root_dest = os.path.join(config_root_dest, 'www_root')
    lib.appl.upgrade.generate_and_save_checksum_for_folder(web_root_dest)


    _write_release_log_seperator(release_log_file, 'Copy File Magic, done')


def _build_nsi_include(nis_include_filename, version, release_store_path, release_store_build_files_path, beta):
    nis_include_file = open(nis_include_filename, 'wt')
    nis_include_file.write("!define GIRI_VERSION '%s'\n" % version)
    nis_include_file.write("!define GIRI_OUT_PATH '%s'\n" % release_store_path)
    nis_include_file.write("!define GIRI_BUILD_FILES '%s'\n" % release_store_build_files_path)
    if beta:
        nis_include_file.write("!define GIRI_BETA\n")
    nis_include_file.close()


def _build_nsi_include_gateway_server(nis_include_filename, version):
    nis_include_file = open(nis_include_filename, 'wt')
    nis_include_file.write("!define GIRI_VERSION '%s'\n" % version)
    nis_include_file.close()

#def _build_client_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file):
#    installers_path = os.path.join(release_store_build_files_path, 'installers', 'win')
#    if not os.path.exists(installers_path):
#        os.makedirs(installers_path)
#
#    _write_release_log_seperator(release_log_file, 'Generating client installer')
#    _build_nsi_include(NSIS_CLIENT_INSTALLER_INCLUDE, version, release_store_path, release_store_build_files_path , beta)
#
#    args = [NSIS_EXE, NSIS_CLIENT_INSTALLER]
#    process = subprocess.Popen(args=args, stdout=release_log_file, stderr=release_log_file, shell=False)
#    process.wait()
#    if process.returncode != 0:
#        raise ReleaseError('Error during building of client installer')
#    print "Generating client installer, done"

def _build_client_uninstaller(version, beta, release_store_path, release_store_build_files_path, release_log_file):
    installers_path = os.path.join(release_store_build_files_path, 'installers', 'win')
    if not os.path.exists(installers_path):
        os.makedirs(installers_path)

    _write_release_log_seperator(release_log_file, 'Generating client uninstaller')
    _build_nsi_include(NSIS_CLIENT_UNINSTALLER_INCLUDE, version, release_store_path, release_store_build_files_path , beta)

    args = [NSIS_EXE, NSIS_CLIENT_UNINSTALLER]
    process = subprocess.Popen(args=args, stdout=release_log_file, stderr=release_log_file, shell=False)
    process.wait()
    if process.returncode != 0:
        raise ReleaseError('Error during building of client uninstaller')
    print "Generating client uninstaller, done"

#def _build_client_device_service_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file):
#    installers_path = os.path.join(release_store_build_files_path, 'installers', 'win')
#    if not os.path.exists(installers_path):
#        os.makedirs(installers_path)
#
#    _write_release_log_seperator(release_log_file, 'Generating client device service installer')
#    _build_nsi_include(NSIS_CLIENT_DEVICE_SERVICE_INSTALLER_INCLUDE, version, release_store_path, release_store_build_files_path , beta)
#
#    args = [NSIS_EXE, NSIS_CLIENT_DEVICE_SERVICE_INSTALLER]
#    process = subprocess.Popen(args=args, stdout=release_log_file, stderr=release_log_file, shell=False)
#    process.wait()
#    if process.returncode != 0:
#        raise ReleaseError('Error during building of client device service installer')
#    print "Generating client device service installer, done"

def _build_installer(version, beta, release_store_path, release_store_build_files_path, release_log_file):
    _write_release_log_seperator(release_log_file, 'Generating installer')
    _build_nsi_include(NSIS_GON_5_INCLUDE, version, release_store_path, release_store_build_files_path, beta)

    args = [NSIS_EXE, NSIS_GON_5]
    process = subprocess.Popen(args=args, stdout=release_log_file, stderr=release_log_file, shell=False)
    process.wait()
    if process.returncode != 0:
        raise ReleaseError('Error during building of installer')

    checksum_filename = os.path.join(release_store_path, 'gon_%s.checksum.txt' % (version))
    checksum_file = open(checksum_filename, 'wt')

    installer_filename = os.path.join(release_store_path, 'gon_%s.exe' % (version))
    checksum = lib.utility.calculate_checksum_for_file(installer_filename)
    checksum_file.write(checksum)
    checksum_file.close()
    _sign_file(version, beta, release_store_path, release_store_build_files_path, release_log_file, installer_filename)
    print "Generating installer, done"


def _build_filelist(version, release_store_path, build_store_path, release_log_file):
    _write_release_log_seperator(release_log_file, 'Generating filelist')

    filelist_filename = os.path.join(release_store_path, 'gon_%s.filelist.txt' % (version))
    fileslist_file = open(filelist_filename, 'wt')
    for root, dirs, files in os.walk(build_store_path):
        for filename in files:
            filename_abs = os.path.join(root, filename)
            size = os.path.getsize(filename_abs)
            checksum = lib.utility.calculate_checksum_for_file(filename_abs)
            fileslist_file.write('%s, %10d, %s\n' % (checksum, size, filename_abs))
    fileslist_file.close()
    print "Generating filelist, done"





def _write_release_log_seperator(release_log_file, header):
    print header
    release_log_file.write('%-61sxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n' %(header))
    release_log_file.write('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n')
    release_log_file.write('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n')
    release_log_file.flush()


def cmd_build_installation_diff(options):
    versions = options.get_versions()
    versions_list = versions.split(':')
    if len(versions_list) != 2:
        print "Error: Versions '%s' do not contain two versions" % versions
        return

    version_from =  versions_list[0]
    version_to =  versions_list[1]

    version_from_root = os.path.join(options.release_store(), version_from)
    if not os.path.isdir(version_from_root):
        print "Error: From version folder '%s' not found." % version_from_root
        return

    version_to_root = os.path.join(options.release_store(), version_to)
    if not os.path.isdir(version_to_root):
        print "Error: To version folder '%s' not found." % version_to_root
        return

    diff_filename = 'installation_diff_%s_%s.txt' % (version_from, version_to)
    diff_filename_abs = os.path.join(version_to_root, diff_filename)

    print "Building release diff between '%s' and '%s'" % (version_from, version_to)
    print "Diff will be written to '%s'" % diff_filename_abs

    diff_file = open(diff_filename_abs, 'wt')

    _cmd_build_installation_diff_files(diff_file, version_from, version_to, version_from_root, version_to_root)
    _cmd_build_installation_diff_content(diff_file, version_from, version_to, version_from_root, version_to_root, 'Templates', os.path.join('config','templates'))
    _cmd_build_installation_diff_content(diff_file, version_from, version_to, version_from_root, version_to_root, 'GPM', os.path.join('config','gpm'))

    diff_file.close()
    print "Done"

def _collect_files(root):
    files = []
    for (dirpath, dirnames, filenames) in os.walk(root):
        relative_path = lib.commongon.common_path_sufix(dirpath, root)
        for filename in filenames:
            files.append(os.path.join(relative_path, filename))
    return files

def _cmd_build_installation_diff_files(diff_file, version_from, version_to, version_from_root, version_to_root):
    _write_release_log_seperator(diff_file, "New and removed files")

    version_from_installation_root = os.path.join(version_from_root, 'build_files')
    if not os.path.isdir(version_from_installation_root):
        print "Error: Two installation exists for the version '%s'." % version_from
        print "version_from_installation_root:", version_from_installation_root
        return
    version_to_installation_root = os.path.join(version_to_root, 'build_files')
    if not os.path.isdir(version_from_installation_root):
        print "Error: Two installation exists for the version '%s'." % version_to_installation_root
        print "version_to_installation_root:", version_to_installation_root
        return

    from_files = _collect_files(version_from_installation_root)
    to_files = _collect_files(version_to_installation_root)

    for diff_line in difflib.unified_diff(from_files, to_files, fromfile=version_from, tofile=version_to, n=0,lineterm=''):
        diff_file.write(diff_line + '\n')

def _cmd_build_installation_diff_content(diff_file, version_from, version_to, version_from_root, version_to_root, title, folder_relative):
    _write_release_log_seperator(diff_file, title)

    version_from_base = os.path.join(version_from_root, 'build_files', folder_relative)
    if not os.path.isdir(version_from_base):
        print "Error: Unable to find the folder '%s'." % version_from_base
        return
    version_to_base = os.path.join(version_to_root, 'build_files', folder_relative)
    if not os.path.isdir(version_to_base):
        print "Error: Unable to find the folder '%s'." % version_to_base
        return

    from_files = set(_collect_files(version_from_base))
    to_files = set(_collect_files(version_to_base))

    new_files = to_files.difference(from_files)
    diff_file.write('New files:\n')
    if len(new_files) > 0:
        for filename in new_files:
            diff_file.write("    %s\n" % filename)
    else:
        diff_file.write("    No new files\n")
    diff_file.write("\n")

    removed_files = from_files.difference(to_files)
    diff_file.write('Removed files:\n')
    if len(removed_files) > 0:
        for filename in removed_files:
            diff_file.write("    %s\n" % filename)
    else:
        diff_file.write("    No removed files\n")
    diff_file.write("\n")

    diff_file.write('Changed files:\n')
    common_files = to_files.intersection(from_files)

    for filename in common_files:
        if not os.path.basename(filename) in ['checksums']:
            filename_from = os.path.join(version_from_base, filename)
            filename_to = os.path.join(version_to_base, filename)
            file_from = open(filename_from, 'r')
            file_to = open(filename_to, 'r')
            for diff_line in difflib.context_diff(file_from.readlines(), file_to.readlines(), fromfile='%s(%s)'%(filename,version_from), tofile='%s(%s)'%(filename,version_to)):
                diff_file.write(diff_line)
            file_from.close()
            file_to.close()

def cmd_build_client_installer(options):
    version = options.get_version()
    release_store_path = os.path.join(options.release_store(), version)
    if not os.path.exists(release_store_path):
        os.makedirs(release_store_path)

    release_log_filename = os.path.join(release_store_path, 'release_manager.log')
    release_log_file = open(release_log_filename, 'wt')

    release_store_build_files_path = os.path.join(release_store_path, 'build_files')
    if not os.path.exists(release_store_build_files_path):
        os.makedirs(release_store_build_files_path)

#    _build_client_installer(version, False, release_store_path, release_store_build_files_path, release_log_file)
    _build_client_uninstaller(version, False, release_store_path, release_store_build_files_path, release_log_file)



def _sign_file(version, beta, release_store_path, release_store_build_files_path, release_log_file, filename):
    print "Signing", filename
    args = [SIGN_EXE, 'sign', '/t' , 'http://timestamp.verisign.com/scripts/timstamp.dll', filename]
    process = subprocess.Popen(args=args, stdout=release_log_file, stderr=release_log_file, shell=False)
    process.wait()
    if process.returncode != 0:
        raise ReleaseError('Error during signing of file')


def _sign_files(version, beta, release_store_path, release_store_build_files_path, release_log_file):
    for filename in SIGN_FILES:
        filename_abs = os.path.normpath(os.path.join(release_store_build_files_path, filename))
        _sign_file(version, beta, release_store_path, release_store_build_files_path, release_log_file, filename_abs)

def cmd_sign(options):
    version = options.get_version()
    release_store_path = os.path.join(options.release_store(), version)
    if not os.path.exists(release_store_path):
        os.makedirs(release_store_path)

    release_log_filename = os.path.join(release_store_path, 'release_manager.log')
    release_log_file = open(release_log_filename, 'wt')

    release_store_build_files_path = os.path.join(release_store_path, 'build_files')
    if not os.path.exists(release_store_build_files_path):
        os.makedirs(release_store_build_files_path)

    _sign_files(version, False, release_store_path, release_store_build_files_path, release_log_file)



def verify_environment(options):
    print "Verifying environment"

    ok = os.path.exists(options.build_store())
    print "BUILD_STORE_ROOT(%s)   : %s" % (ok, options.build_store())

    ok = os.path.exists(options.release_store())
    print "RELEASE_STORE_ROOT(%s) : %s" % (ok, options.release_store())

    ok = os.path.exists(NSIS_EXE)
    print "NSIS_EXE(%s)           : %s" % (ok, NSIS_EXE)


def main():
    options = Options()

    if options.cmd_list_versions():
        cmd_list_versions(options)
        return 0

    if options.cmd_list_release_versions():
        cmd_list_release_versions(options)
        return 0

    if options.cmd_build_release_prepare():
        cmd_release_prepare(options)
        return 0

    if options.cmd_build_release():
        cmd_release(options, clean=options.get_clean(), beta=options.get_beta())
        return 0

    if options.cmd_build_installation_diff():
        cmd_build_installation_diff(options)
        return 0

    if options.cmd_build_client_installer():
        cmd_build_client_installer(options)
        return 0

    if options.cmd_sign():
        cmd_sign(options)
        return 0

    verify_environment(options)

if __name__ == '__main__':
    main()
