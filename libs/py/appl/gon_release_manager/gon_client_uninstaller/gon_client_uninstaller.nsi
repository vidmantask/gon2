!include "FileFunc.nsh"
;
; Include generated definitions
;
;!define GIRI_VERSION '5.4.0-0'
;!define GIRI_OUT_PATH 'd:\gon_5\release_store\5.4.0-0'
;!define GIRI_BUILD_FILES 'd:\gon_5\release_store\5.4.0-0\build_files'
!include "gon_client_uninstaller_include.nsh"

;
; Giritech variabel definitions
;
!ifdef GIRI_BETA
 !define GIRI_IMAGE_UNINSTALLER_SPLASH "..\graphics\gon_client_uninstaller_splash_beta.bmp"
!else
 !define GIRI_IMAGE_UNINSTALLER_SPLASH "..\graphics\gon_client_uninstaller_splash.bmp"
!endif


;
; NSIS Configuration
;
Name "G/On Client Uninstaller"
Icon "..\graphics\gon_uninstaller.ico"
OutFile "${GIRI_BUILD_FILES}\installers\win\gon_client_uninstaller.exe"
InstallDir "$TEMP\gon_client_uninstaller_${GIRI_VERSION}"
RequestExecutionLevel user
SilentInstall silent
BrandingText " "


;
; Function definitions
;
Function .onInit
  InitPluginsDir
  File /oname=$PLUGINSDIR\splash.bmp "${GIRI_IMAGE_UNINSTALLER_SPLASH}"
  advsplash::show 1500 600 400 -1 $PLUGINSDIR\splash
  Delete $PLUGINSDIR\splash.bmp
FunctionEnd


Section
  SetOutPath "$INSTDIR"
  File /r "${GIRI_BUILD_FILES}\build_store\gon_client_uninstaller\win\*"

  ${GetParent} "$EXEPATH" $R0
  Exec '"$INSTDIR\gon_client_uninstaller.exe" --installation_root="$R0"'
SectionEnd
