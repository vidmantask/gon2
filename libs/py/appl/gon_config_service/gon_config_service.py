"""
This module contains the G/On Server Cofiguration tool
"""
from __future__ import with_statement
import warnings
from lib.utility.xor_crypt import xor_crypt
warnings.filterwarnings("ignore")

import sys
if not hasattr(sys, "frozen"):
    import lib.dev_env.path_setup


import os.path
import shutil
import tempfile
import glob
import shutil
import optparse
import ConfigParser
import time
import socket
import ssl

import lib.version
import lib.checkpoint

import lib.appl.crash.handler
import lib.appl.io_hooker

import components.communication.session_manager
import components.environment
import components.plugin.server_config.manager

import lib.version

import components.plugin.server_config.plugin_socket_upgrade
import components.plugin.server_config.plugin_socket_config_specification

import components.config.common
import components.server_config_ws.server_config
import components.server_config_ws.server_config_jobs
import components.server_config_ws.server_config_ws_service
import components.communication
import components.communication.async_service


MODULE_ID = "appl_gon_config_service"


import components.server_config_ws.database_schema
import components.database.server_common.schema_api
import components.auth.server_common.database_schema
import components.dialog.server_common.database_schema
import components.management_message.server_common.database_schema
import components.traffic.server_common.database_schema
import components.access_log.server_common.database_schema
import components.user.server_common.database_schema
import components.endpoint.server_common.database_schema
import components.templates.server_common.database_schema
import components.admin_ws.database_schema

class ServerConfigOptions(components.config.common.ConfigConfigService):
    """
    Options for the Configuration. General rule is that commandline options overwrites options from ini-file.
    """
    def __init__(self):
        components.config.common.ConfigConfigService.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--config', type='string', default='./gon_config_service.ini', help='configuration file')
        self._parser.add_option('--instance_run_root', type='string', default=None, help='Root of instance')
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')

        # not management_features_enabled
        self._parser.add_option('--generate_support_package',  action='store_true', help='Generate Support Package')

        # management_features_enabled
        self._parser.add_option('--foreground', action='store_true', default=False, help='run server in foreground')
        self._parser.add_option('--serve', action='store_true', help='Start Config Web-Service')
        self._parser.add_option('--ip', type='string', default=None, help='Option to --server: Listen on given ip')
        self._parser.add_option('--port', type='int', default=None,  help='Option to --server: Listen on given port')
        self._parser.add_option('--generate_setupdata', action='store_true', help='Generate system wide setupdata')
        self._parser.add_option('--generate_knownsecrets', action='store_true', help='Generate new pair of known secrets for the installation')
        self._parser.add_option('--generate_gpms',  action='store_true', help='Generate File Packages reposotory')
        self._parser.add_option('--install_services',  action='store_true', help='Install services')
        self._parser.add_option('--remove_services',  action='store_true', help='Remove installed services')
        self._parser.add_option('--backup', action='store_true', help='Backup database and system configuration')
        self._parser.add_option('--backup_path', type='string', default=None, help='Option to --backup : Path of backup')
        self._parser.add_option('--backup_do_not_create_sub_folder', action='store_true', help='Option to --backup : Do not create a subfolder for the backup')
        self._parser.add_option('--restore', action='store_true', help='Restore database and system configuration')
        self._parser.add_option('--restore_backup_path', type='string', default=None, help='Option to --restore : Path backup that should be restored')
        self._parser.add_option('--restore_create_schema', action='store_true', help='Option to --restore : Restore schema as well as data')
        self._parser.add_option('--restore_skip_database', action='store_true', help="Don't restore database during restore")
        self._parser.add_option('--restore_skip_configuration', action='store_true', help="Don't restore configuration files during restore")
        self._parser.add_option('--scramble_password', type='string', help='Option to scramble a password')
        self._parser.add_option('--prune_access_log', type='string', help='Option delete access log information in database prior to specified date (date format : YYYY-MM-DD)')
        self._parser.add_option('--reset_mgmt_access', action='store_true', help='Reset Management access - give all Administrator access')

        self._parser.add_option('--backup_other_installation', action='store_true', help='Backup database and system configuration')
        self._parser.add_option('--backup_other_installation_path', type='string', default=None, help='Option to --backup_other_installation : Root of other installation, e.g. C:\\Program Files\\Giritech\\gon_5.5.0-21')
        self._parser.add_option('--backup_other_installation_version', type='string', default=None, help='Option to --backup_other_installation : Version of other installation, e.g. 5.5.0')

        (self._options, self._args) = self._parser.parse_args()
        if self._options.instance_run_root is None:
            self._options.instance_run_root = os.getcwd()

        config_absfilename = os.path.join(self._options.instance_run_root, self._options.config)
        self.read_config_file(config_absfilename)
        self.load_and_set_scramble_salt()

    def cmd_show_version(self):
        return self._options.version

    def get_instance_run_root(self):
        return self._options.instance_run_root

    def get_management_features_enabled(self):
        config_local = components.config.common.ConfigConfigServiceLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        return config_local.management_features_enabled

    def get_foreground(self):
        return self._options.foreground

    def get_service_ip(self):
        if self._options.ip != None:
            return self._options.ip
        return self.service_ip

    def get_service_port(self):
        if self._options.port != None:
            return self._options.port
        return self.service_port

    def cmd_serve(self):
        if self.get_management_features_enabled():
            return self._options.serve
        return False

    def cmd_generate_setupdata(self):
        if self.get_management_features_enabled():
            return self._options.generate_setupdata
        return False

    def cmd_reset_mgmt_access(self):
        if self.get_management_features_enabled():
            return self._options.reset_mgmt_access
        return False

    def cmd_generate_knownsecrets(self):
        if self.get_management_features_enabled():
            return self._options.generate_knownsecrets
        return False

    def cmd_generate_gpms(self):
        if self.get_management_features_enabled():
            return self._options.generate_gpms
        return False

    def cmd_generate_support_package(self):
        return self._options.generate_support_package

    def cmd_remove_services(self):
        if self.get_management_features_enabled():
            return self._options.remove_services
        return False

    def cmd_install_services(self):
        if self.get_management_features_enabled():
            return self._options.install_services
        return False

    def cmd_backup(self):
        if self.get_management_features_enabled():
            return self._options.backup
        return False

    def get_backup_abspath(self):
        backup_path = self._options.backup_path
        if backup_path is None:
            backup_path = self.backup_abspath
        return backup_path

    def get_backup_do_not_create_sub_folder(self):
        return self._options.backup_do_not_create_sub_folder

    def cmd_restore(self):
        if self.get_management_features_enabled():
            return self._options.restore
        return False

    def get_restore_backup_path(self):
        return self._options.restore_backup_path

    def get_restore_create_schema(self):
        return self._options.restore_create_schema

    def get_restore_skip_database(self):
        return self._options.restore_skip_database

    def get_restore_skip_configuration(self):
        return self._options.restore_skip_configuration

    def get_scramble_password(self):
        return self._options.scramble_password

    def get_prune_access_log(self):
        return self._options.prune_access_log

    def cmd_upgrade(self):
        if self.get_management_features_enabled():
            return self._options.upgrade
        return False

    def get_upgrade_backup_path(self):
        return self._options.upgrade_backup_path

    def cmd_backup_other_installation(self):
        if self.get_management_features_enabled():
            return self._options.backup_other_installation
        return False

    def get_backup_other_installation_path(self):
        return self._options.backup_other_installation_path

    def get_backup_other_installation_version(self):
        return self._options.backup_other_installation_version


def show_job_status(job_status):
    print '%3d%%' % job_status.progress, job_status.header, job_status.sub_header


def serve(server_config, checkpoint_handler):
    #
    # This is a hack that seems to fix a later call to the same function(with same argument).
    # The later call is in standard socket.py and would block on some XP if this program was called from a java process.
    #
    result = socket.gethostbyaddr('127.0.0.1')

    checkpoint_handler.Checkpoint("serve", MODULE_ID, lib.checkpoint.INFO,
                                  service_ip=server_config.get_service_ip(),
                                  service_port=server_config.get_service_port())

    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    server_configuration_all.create_missing_folders()
    server_config_service = components.server_config_ws.server_config_ws_service.ServerConfigWSService.run_default(checkpoint_handler,
                                                                                                                   server_configuration_all,
                                                                                                                   server_config.get_service_ip(),
                                                                                                                   server_config.get_service_port())
    if server_config.get_foreground():
        try:
            while True:
                user_input = raw_input('command : ')
                if(user_input == 'stop'):
                    break
                else:
                    print "Error, the command '%s' is not known." % user_input
            print "Stopping"
        except EOFError:
            pass
        except:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("Job.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            print etrace
        server_config_service.stop()
        time.sleep(1)
    else:
        server_config_service.join()
        time.sleep(1)


def generate_knownsecrets(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobGenerateKnownsecrets(server_configuration_all, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def generate_gpms(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobGenerateGPMS(server_configuration_all, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def generate_setupdata(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobGenerateDemodata(server_configuration_all, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def reset_mgmt_access(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobResetMgmtAccess(server_configuration_all, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def generate_support_package(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    plugin_manager = components.plugin.server_config.manager.Manager(checkpoint_handler, server_config.plugin_modules_abspath, server_configuration_all)
    config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(plugin_manager)

    job = components.server_config_ws.server_config_jobs.JobGenerateSupportPackage(server_configuration_all, checkpoint_handler, config_specification_socket, management_features_enabled=server_config.get_management_features_enabled())
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def remove_services(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobRemoveServices(server_configuration_all, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def install_services(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobInstallServices(server_configuration_all, checkpoint_handler, False)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def backup(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    plugin_manager = components.plugin.server_config.manager.Manager(checkpoint_handler, server_config.plugin_modules_abspath, server_configuration_all)
    config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(plugin_manager)

    job = components.server_config_ws.server_config_jobs.JobBackup(checkpoint_handler, server_configuration_all, config_specification_socket, server_config.get_backup_abspath(), create_subfolder_for_backup=not server_config.get_backup_do_not_create_sub_folder())
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def restore(server_config, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    plugin_manager = components.plugin.server_config.manager.Manager(checkpoint_handler, server_config.plugin_modules_abspath, server_configuration_all)
    config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(plugin_manager)

    job = components.server_config_ws.server_config_jobs.JobRestore(server_configuration_all, checkpoint_handler, server_config.get_restore_backup_path(), config_specification_socket, server_config.get_restore_create_schema())
    job.set_status_change_callback(show_job_status)
    if server_config.get_restore_skip_configuration():
        job.skip_config_restore = True
    if server_config.get_restore_skip_database():
        job.skip_db_restore = True
    job.start()
    job.join()
    return 0

def prune(server_config, date_before, checkpoint_handler):
    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    job = components.server_config_ws.server_config_jobs.JobPruneData(server_configuration_all, date_before, checkpoint_handler)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0

def backup_other_installation(server_config, checkpoint_handler):
    if server_config.get_backup_other_installation_path() is None:
        print "Missing argument: backup_other_installation_path must be specified"
        return 1

    other_installation_path_abs = server_config.get_backup_other_installation_path()
    if not os.path.isdir(other_installation_path_abs):
        print "Invalid argument: '%s' is not folder" % other_installation_path_abs
        return 1

    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
    plugin_manager = components.plugin.server_config.manager.Manager(checkpoint_handler, server_config.plugin_modules_abspath, server_configuration_all)
    config_specification_socket = components.plugin.server_config.plugin_socket_config_specification.PluginSocket(plugin_manager)

    backup_version = None
    backup_version_str = server_config.get_backup_other_installation_version()
    if backup_version_str is not None:
        backup_version = lib.version.Version.create_from_string(server_config.get_backup_other_installation_version())
    job = components.server_config_ws.server_config_jobs.JobBackupOtherInstallation(checkpoint_handler, server_configuration_all, other_installation_path_abs, config_specification_socket, plugin_manager, backup_version)
    job.set_status_change_callback(show_job_status)
    job.start()
    job.join()
    return 0



#def db_upgrade(server_config, checkpoint_handler):
#    server_configuration_all = components.server_config_ws.server_config.ServerConfigurationAll(server_config)
#    job = components.server_config_ws.server_config_jobs.JobDBUpgrade(server_configuration_all, checkpoint_handler, server_config.get_upgrade_backup_path())
#    job.set_status_change_callback(show_job_status)
#    job.start()
#    job.join()
#    return 0


def main():
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)

    server_config = ServerConfigOptions()

    #
    # Show version and exit if requested
    #
    if(server_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    #
    # Initialize logging
    #
    if server_config.log_rotate and os.path.exists(server_config.log_absfile):
        shutil.move(server_config.log_absfile, lib.checkpoint.generate_rotate_filename(server_config.log_absfile))
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(server_config.log_enabled,
                                                                      server_config.log_verbose,
                                                                      server_config.log_absfile,
                                                                      server_config.is_log_type_xml())
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    #
    # Initialize communication component (and crypt lib)
    #
    components.communication.async_service.init(checkpoint_handler)

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                  )

    checkpoint_handler.Checkpoint("ssl_version", MODULE_ID, lib.checkpoint.INFO,
                                  cpp_ssl_version=components.communication.get_ssl_version(),
                                  py_ssl_version=ssl.OPENSSL_VERSION
                                  )

    checkpoint_handler.Checkpoint("db", MODULE_ID, lib.checkpoint.INFO, db_connect_string=server_config.get_db_connect_info())

    if(server_config.cmd_serve()):
        return serve(server_config, checkpoint_handler)

    if(server_config.cmd_generate_knownsecrets()):
        return generate_knownsecrets(server_config, checkpoint_handler)

    if(server_config.cmd_generate_gpms()):
        return generate_gpms(server_config, checkpoint_handler)

    if(server_config.cmd_generate_setupdata()):
        return generate_setupdata(server_config, checkpoint_handler)

    if(server_config.cmd_reset_mgmt_access()):
        return reset_mgmt_access(server_config, checkpoint_handler)

    if(server_config.cmd_generate_support_package()):
        return generate_support_package(server_config, checkpoint_handler)

    if(server_config.cmd_remove_services()):
        return remove_services(server_config, checkpoint_handler)

    if(server_config.cmd_install_services()):
        return install_services(server_config, checkpoint_handler)

    if(server_config.cmd_backup()):
        return backup(server_config, checkpoint_handler)

    if(server_config.cmd_restore()):
        return restore(server_config, checkpoint_handler)

    if server_config.cmd_backup_other_installation():
        return backup_other_installation(server_config, checkpoint_handler)


#   if(server_config.cmd_upgrade()):
#       return db_upgrade(server_config, checkpoint_handler)

    if server_config.get_scramble_password():
        pwd = server_config.get_scramble_password()
        print xor_crypt(pwd)

    if server_config.get_prune_access_log():
        import datetime
        date_before_str = server_config.get_prune_access_log()
        try:
            date_before = datetime.datetime.strptime(date_before_str, '%Y-%m-%d')
        except Exception, e:
            print "Error: ", e
            return 1
        return prune(server_config, date_before, checkpoint_handler)


if __name__ == '__main__':
    main()
