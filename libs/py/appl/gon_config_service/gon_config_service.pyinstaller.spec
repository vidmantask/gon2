# -*- mode: python -*-
import lib.dev_env.path_setup
from distutils.core import setup

import sys
import lib.appl_plugin_module_util

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()

import lib.dev_env.pyinstaller

data_files = []
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'upgrade', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ad', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ldap', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'local_win_user', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_security', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ip_address', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'login_interval', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'dme', 'server', 'config'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'vacation_app', 'server', 'config'))


#
# PyInstaller configuration
#
app_py = 'gon_config_service.py'
app_name = 'gon_config_service'
app_description = "G/On Config Service"
app_version_file = lib.dev_env.pyinstaller.generate_version_file(app_py, app_name, app_description)
app_icon_file = "..\\..\\components\\presentation\\gui\\mfc\\images\\giritech.ico"
app_options = []

datas = []
for (data_file_dest_folder, data_file_src_files) in data_files:
  for data_file_src_file in data_file_src_files:
    datas.append( (data_file_src_file, data_file_dest_folder))

hiddenimports = [
  'logging',
  'sqlalchemy.dialects.sqlite',
  'sqlalchemy.dialects.mssql',
  'sqlalchemy.dialects.mysql',
  'sqlalchemy.ext.baked',
  'win32com.client',
  'win32security',
  'elementtree',
  'plugin_types.common.plugin_type_token',
  'plugin_types.server_config.plugin_type_user_config_specification',
  'ldap',
  'ldap.filter',
  'ldap.async',
  'pyodbc',
  'MySQLdb',
]

exclude_modules = [
'plugin_modules',
'plugin_modules.upgrade',
'plugin_modules.upgrade.server_config',
'plugin_modules.ad',
'plugin_modules.ad.server_config',
'plugin_modules.ldap',
'plugin_modules.ldap.server_config',
'plugin_modules.local_win_user',
'plugin_modules.local_win_user.server_config',
'plugin_modules.endpoint_security',
'plugin_modules.endpoint_security.server_config',
'plugin_modules.ip_address',
'plugin_modules.ip_address.server_config',
'plugin_modules.login_interval',
'plugin_modules.login_interval.server_config',
'plugin_modules.dme.server',
'plugin_modules.dme.server_config',
'plugin_modules.vacation_app',
'plugin_modules.vacation_app.server_config',
]

#
# PyInstaller generate folder app
#
a = Analysis(
  [app_py],
  pathex=['.'],
  binaries=[],
  datas=datas,
  hiddenimports=hiddenimports,
  hookspath=[],
  runtime_hooks=[],
  excludes=exclude_modules,
  win_no_prefer_redirects=False,
  win_private_assemblies=False,
  cipher=None
)

pyz = PYZ(
  a.pure,
  a.zipped_data,
  cipher=None
)

exe = EXE(
  pyz,
  a.scripts,
  options=app_options,
  exclude_binaries=True,
  name=app_name,
  debug=False,
  strip=False,
  upx=True,
  console=True,
  version=app_version_file,
  icon=app_icon_file
)

coll = COLLECT(
  exe,
  a.binaries,
  a.zipfiles,
  a.datas,
  strip=False,
  upx=True,
  name=''
)
