# -*- mode: python -*-
import lib.dev_env.path_setup
from distutils.core import setup

import sys
import lib.appl_plugin_module_util

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()

import lib.dev_env.pyinstaller

data_files = []
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ad', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ad', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'hagiwara', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'hagiwara', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_2', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_2', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_pe', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_pe', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'access_log', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'access_log', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'launch', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'launch', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'token', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'token', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ldap', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ldap', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_mac', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_mac', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'local_win_user', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'local_win_user', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'mobile', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'mobile', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'smart_card', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'smart_card', 'server', 'common'))

data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ip_address', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ip_address', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'login_interval', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'login_interval', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_security', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_security', 'server', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'dme', 'server', 'management'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'dme', 'server', 'common'))


#
# PyInstaller configuration
#
app_py = 'gon_server_management_service.py'
app_name = 'gon_server_management_service'
app_description = "G/On Management Server"
app_version_file = lib.dev_env.pyinstaller.generate_version_file(app_py, app_name, app_description)
app_icon_file = "..\\..\\components\\presentation\\gui\\mfc\\images\\giritech.ico"
app_options = []

datas = []
for (data_file_dest_folder, data_file_src_files) in data_files:
  for data_file_src_file in data_file_src_files:
    datas.append( (data_file_src_file, data_file_dest_folder))

hiddenimports = [
  'logging',
  'sqlalchemy.dialects.sqlite',
  'sqlalchemy.dialects.mssql',
  'sqlalchemy.dialects.mysql',
  'sqlalchemy.ext.baked',
  'email.mime.text',  # Due to casing bug in std-lib, mk filed a bug report
  'email.iterators',  # Due to casing bug in std-lib, mk filed a bug report
  'email.generator',  # Due to casing bug in std-lib, mk filed a bug report
  'email.utils',      # Due to casing bug in std-lib, mk filed a bug report
  'email.base64mime', # Due to casing bug in std-lib, mk filed a bug report
  'win32security',
  'win32api',
  'win32net',
  'win32com.client',
  'win32com.adsi.adsicon',
  'win32netcon',
  'components.auth.server_management.rule_plugin_api',
  'elementtree',
  'ldap',
  'ldap.async',
  'ldap.filter',
  'lib.hardware.windows_security_center',
  'IPy',
  'pyodbc',
  'MySQLdb',
  'os.path',
  'ntpath'
]

exclude_modules = [
'plugin_modules',
'plugin_modules.ad',
'plugin_modules.ad.server_management',
'plugin_modules.ad.server_common',
'plugin_modules.hagiwara',
'plugin_modules.hagiwara.server_management',
'plugin_modules.hagiwara.server_common',
'plugin_modules.soft_token',
'plugin_modules.soft_token.server_management',
'plugin_modules.soft_token.server_common',
'plugin_modules.micro_smart',
'plugin_modules.micro_smart.server_management',
'plugin_modules.micro_smart.server_common',
'plugin_modules.micro_smart_swissbit]',
'plugin_modules.micro_smart_swissbit.server_management',
'plugin_modules.micro_smart_swissbit.server_common',
'plugin_modules.micro_smart_swissbit_2',
'plugin_modules.micro_smart_swissbit_2.server_management',
'plugin_modules.micro_smart_swissbit_2.server_common',
'plugin_modules.micro_smart_swissbit_pe',
'plugin_modules.micro_smart_swissbit_pe.server_management',
'plugin_modules.micro_smart_swissbit_pe.server_common',
'plugin_modules.access_log',
'plugin_modules.access_log.server_management',
'plugin_modules.access_log.server_common',
'plugin_modules.launch',
'plugin_modules.launch.server_management',
'plugin_modules.launch.server_common',
'plugin_modules.token',
'plugin_modules.token.server_management',
'plugin_modules.token.server_common',
'plugin_modules.endpoint',
'plugin_modules.endpoint.server_management',
'plugin_modules.endpoint.server_common',
'plugin_modules.ldap',
'plugin_modules.ldap.server_management',
'plugin_modules.ldap.server_common',
'plugin_modules.endpoint_mac',
'plugin_modules.endpoint_mac.server_management',
'plugin_modules.endpoint_mac.server_common',
'plugin_modules.local_win_user',
'plugin_modules.local_win_user.server_management',
'plugin_modules.local_win_user.server_common',
'plugin_modules.mobile',
'plugin_modules.mobile.server_management',
'plugin_modules.mobile.server_common',
'plugin_modules.ip_address',
'plugin_modules.ip_address.server_management',
'plugin_modules.ip_address.server_common',
'plugin_modules.login_interval',
'plugin_modules.login_interval.server_management',
'plugin_modules.login_interval.server_common',
'plugin_modules.endpoint_security',
'plugin_modules.endpoint_security.server_management',
'plugin_modules.endpoint_security.server_common',
'plugin_modules.dme',
'plugin_modules.dme.server_management',
'plugin_modules.dme.server_common',
'plugin_modules.smart_card',
'plugin_modules.smart_card.server_management',
'plugin_modules.smart_card.server_common',
]


#
# PyInstaller generate folder app
#
a = Analysis(
  [app_py],
  pathex=['.'],
  binaries=[],
  datas=datas,
  hiddenimports=hiddenimports,
  hookspath=[],
  runtime_hooks=[],
  excludes=exclude_modules,
  win_no_prefer_redirects=False,
  win_private_assemblies=False,
  cipher=None
)

pyz = PYZ(
  a.pure,
  a.zipped_data,
  cipher=None
)

exe = EXE(
  pyz,
  a.scripts,
  options=app_options,
  exclude_binaries=True,
  name=app_name,
  debug=False,
  strip=False,
  upx=True,
  console=False,
  version=app_version_file,
  icon=app_icon_file
)

coll = COLLECT(
  exe,
  a.binaries,
  a.zipfiles,
  a.datas,
  strip=False,
  upx=True,
  name=''
)
