"""
The G/On Management Server
"""
import sys
import os
import os.path
import gon_server_management_main
import lib.appl.service
import servicemanager
import traceback

import lib.service.service_foreground

gon_service = lib.appl.service.GService.create_service_from_current_location()
if gon_service is None:
    gon_service = lib.appl.service.GService.create_management_service()

FGServiceHandleCommandLine = lib.service.service_foreground.ServiceHandleCommandLine
FGService = lib.service.service_foreground.get_service(gon_service.service_name, gon_service.service_title, gon_service.service_description, gon_server_management_main.main)


if sys.platform == 'win32':
    import lib.service.service_win
    ServiceHandleCommandLine = lib.service.service_win.ServiceHandleCommandLine
    Service = lib.service.service_win.get_service(gon_service.service_name, gon_service.service_title, gon_service.service_description, gon_server_management_main.main)
elif sys.platform == 'linux2':
    ServiceHandleCommandLine = FGServiceHandleCommandLine
    Service = FGService


def HandleCommandLine():
    """
    Entry point for py2exe with cmdline_style='custom'
    """
    if len(sys.argv) > 1 and sys.argv[1] in ['--foreground', '--foreground_with_stop_file']:
        FGServiceHandleCommandLine(FGService)
    else:
        ServiceHandleCommandLine(Service)

if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    HandleCommandLine()
