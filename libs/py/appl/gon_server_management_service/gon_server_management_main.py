"""
The G/On Management Server
"""
from __future__ import with_statement
import warnings
warnings.filterwarnings("ignore")

import sys
if not hasattr(sys, "frozen"):
    import lib.dev_env.path_setup

import time
import os.path
import optparse
import threading
import shutil
import uuid
import ssl
import httplib

import lib.version
import lib.checkpoint
import lib.cryptfacility
import lib.appl.io_hooker
import lib.appl.crash.handler

import lib.appl.cp_in_folder_janitor

import components.config.common
import components.license.gonlicense

import components.admin_ws.admin_ws_service
import components.admin_ws.ws.admin_ws_services
from components.rest.rest_api import RestApi

import components.database.server_common.database_api
import components.environment
import components.dialog.server_management.dialog
import components.user.server_management.user_admin

import components.auth.server_common.database_schema
import components.dialog.server_common.database_schema
import components.management_message.server_common.database_schema
import components.traffic.server_common.database_schema
import components.admin_ws.database_schema

import components.access_log.server_management.access_log
import components.access_log.server_common.database_schema

import components.endpoint.server_common.database_schema
import components.endpoint.server_management.endpoint


import components.plugin.server_management.manager

import plugin_types.server_management.plugin_type_user
import plugin_types.server_management.plugin_type_token

import plugin_types.common.plugin_type_user
import plugin_types.common.plugin_type_token

import components.dialog.server_common.dialog_api
import components.dialog.server_management.dialog_api

import components.templates.server_common

import components.communication
import components.communication.session_manager
import components.communication.async_service

import components.admin_ws.portscan_api
import components.auth.server_management.rule_api
import components.traffic.server_management.traffic_admin

from components.admin_ws.activity_log import ActivityLog, ActivityLogCreator

from components.management_message.server_common.session_server import ManagementMessageServerSessionManager

import components.management_message.server_management.session_recieve

import sqlalchemy.sql.default_comparator
import six


MODULE_ID = "appl_gon_server_management"

class HTTPSConnectionWrapper(httplib.HTTPSConnection):
    def __init__(self, *args, **kwargs):
        httplib.HTTPSConnection.__init__(self, *args, context=ssl._create_unverified_context(), **kwargs)

#import ZSI.wstools.logging
#ZSI.wstools.logging.setBasicLoggerDEBUG()

class ServerManagementOptions(components.config.common.ConfigServerManagement):
    """
    Options and ini-file values for the Management Server.
    """
    def __init__(self, instance_run_root, args=None):
        components.config.common.ConfigServerManagement.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--foreground', action='store_true', help='run server in foreground')
        self._parser.add_option('--foreground_with_stop_file', action='store_true', help='run server in foreground')
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--config', type='string', default='./gon_server_management.ini', help='configuration file')

        (self._options, self._args) = self._parser.parse_args(args=args)
        self.instance_run_root = instance_run_root
        config_filename = os.path.join(instance_run_root, self._options.config)
        self.read_config_file(config_filename)
        self.load_and_set_scramble_salt()

    def cmd_show_version(self):
        return self._options.version

    def get_gateway_server_path(self, checkpoint_handler):
        instance_root_front, instance_root_back = os.path.split(self.instance_run_root)
        gateway_server_path = os.path.normpath(os.path.join(instance_root_front, '..', 'gon_server_gateway_service', instance_root_back))
        if os.path.isdir(gateway_server_path):
            return gateway_server_path
        checkpoint_handler.Checkpoint("gateway_server_path.not_found", MODULE_ID, lib.checkpoint.ERROR, gateway_server_path=gateway_server_path)
        return None


def get_client_knownsecret(checkpoint_handler, server_config):
    filename = os.path.join(server_config.deployment_abspath, components.config.common.KS_CLIENT_FILENAME)
    if os.path.exists(filename):
        return open(filename, 'r').read()
    checkpoint_handler.Checkpoint("get_client_knownsecret.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
    return None

def get_server_knownsecret(checkpoint_handler, server_config):
    filename = os.path.join(server_config.deployment_abspath, components.config.common.KS_SERVER_FILENAME)
    if os.path.exists(filename):
        return open(filename, 'r').read()
    checkpoint_handler.Checkpoint("get_server_knownsecret.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
    return None

def get_servers(checkpoint_handler, server_config):
    filename = os.path.join(server_config.deployment_abspath, components.config.common.CLIENT_SERVERS_FILENAME)
    if os.path.exists(filename):
        return open(filename, 'r').read()
    checkpoint_handler.Checkpoint("get_servers.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
    return None

def get_service_management_server_knownsecret(checkpoint_handler, server_config):
    filename = os.path.join(server_config.deployment_abspath, components.config.common.SERVICE_MANAGEMENT_KS_SERVER_FILENAME)
    if os.path.exists(filename):
        return open(filename, 'r').read()
    checkpoint_handler.Checkpoint("get_service_management_server_knownsecret.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
    return None

def set_portscan_parameters(server_config):
    if server_config.portscan_enabled:
        components.admin_ws.portscan_api.ip_ranges = components.admin_ws.portscan_api.convert_ip_range(server_config.portscan_ip_ranges)
        components.admin_ws.portscan_api.timeout = float(server_config.portscan_timeout)
        components.admin_ws.portscan_api.parallel_connections = int(server_config.portscan_parallel_connections)
        if int(server_config.portscan_delay_sec) != 0:
            components.admin_ws.portscan_api.delay = int(server_config.portscan_delay_sec)



class MainServer(threading.Thread):
    def __init__(self, checkpoint_handler, admin_ws_service, service_stop_signal, service_stopped_signal):
        threading.Thread.__init__(self)
        self.checkpoint_handler = checkpoint_handler
        self._admin_ws_service = admin_ws_service
        self.service_stop_signal = service_stop_signal
        self.service_stopped_signal = service_stopped_signal

    def run(self):
        try:
            self._admin_ws_service.start()
            while not self._admin_ws_service.is_started():
                time.sleep(1)

            while not self.service_stop_signal and self._admin_ws_service.is_running() :
                time.sleep(2)
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("main_server.run.error", MODULE_ID, lib.checkpoint.CRITICAL)

        try:
            self._admin_ws_service.stop()
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("main_server.run.stop.error", MODULE_ID, lib.checkpoint.CRITICAL)


class AdminSessionCreator(object):
    def __init__(self, url):
        self._url = url
        self.gon_admin_ws_server = None
        self._internal_logins = dict()
        self._internal_login_count = 0

    def _admin_ws_login(self):
        gon_admin_ws_server = self.get_admin_ws_server()
        request = RestApi.Admin.OpenSessionRequest()
        response = gon_admin_ws_server.OpenSession(request)
        session_id = response.get_session_id

        if session_id:
            request = RestApi.Admin.InternalLoginRequest()
            request.set_session_id(session_id)
            request.set_login_index(self._internal_login_count)
            login = str(uuid.uuid4())
            self._internal_logins[self._internal_login_count] = login
            self._internal_login_count += 1
            request.set_login(login)
            response = gon_admin_ws_server.InternalLogin(request)
            ok = response.get_rc
            if not ok:
                self.close_session(session_id)
                return None

        return session_id

    def ping_session(self, session_id):
        request = RestApi.Admin.PingRequest()
        request.set_session_id(session_id)
        response = self.get_admin_ws_server().Ping(request)
        return response.get_rc

    def get_admin_ws_server(self):
        if not self.gon_admin_ws_server:
            self.gon_admin_ws_server = components.admin_ws.ws.admin_ws_services.BindingSOAP(self._url, transport=HTTPSConnectionWrapper)
        return self.gon_admin_ws_server

    def create_session(self):
        session_id = self._admin_ws_login()
        if not session_id:
            raise Exception("Error creating ws sesssion")
        return session_id

    def close_session(self, session_id):
        try:
            gon_admin_ws_server = self.get_admin_ws_server()
            request = RestApi.Admin.CloseSessionRequest()
            request.set_session_id(session_id)
            gon_admin_ws_server.CloseSession(request)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("close_session", MODULE_ID, lib.checkpoint.WARNING, etype, evalue, etrace)


    def verify_internal_login(self, login_index, login):
        try:
            saved_login = self._internal_logins.pop(login_index)
        except KeyError:
            return False

        return saved_login == login




class CpInFolderJanitorCallback(lib.appl.cp_in_folder_janitor.CpInFolderJanitorCallback):
    """
    Called when a checkpoint file is found
    """
    def __init__(self, checkpoint_handler, folder_to_watch, message_receiver_access_log):
        self.checkpoint_handler = checkpoint_handler
        self.folder_to_watch = folder_to_watch
        self.message_receiver_access_log = message_receiver_access_log

    def handle_cp(self, cp_filename_abs, cp_file):
        self.message_receiver_access_log.access_log_management_critical(cp_file.get_ts(), cp_file.get_source(), 0, cp_file.get_summary(), cp_file.get_details())
        cp_file.remove()



# The async_service is a global instance because it contains a async-io object
# that should be deleted at the very end.
async_service = None


def main(service_stop_signal=[], service_stopped_signal=[], service_name=None, args=None, instance_run_root=None):
    #
    # Setting current directory
    #
    if instance_run_root is None:
        if hasattr(sys, "frozen"):
            instance_run_root = os.path.dirname(sys.executable)
        else:
            instance_run_root = os.getcwd()

    server_config = ServerManagementOptions(instance_run_root, args)


    #
    # Show version and exit if requested
    #
    if(server_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        service_stopped_signal.append(7)
        return 0

    #
    # Initialize logging
    #
    try:
        log_absfilename = server_config.log_absfile
        if server_config.log_rotate and os.path.exists(log_absfilename):
            shutil.move(log_absfilename, lib.checkpoint.generate_rotate_filename(log_absfilename))
    except:
        log_absfilename = server_config.log_absfile + ("_%i.log" % os.getpid())

    checkpoint_handler_in_folder = lib.checkpoint.CheckpointHandler.get_handler_to_xml_folder(server_config.log_in_folder_enabled,server_config.log_in_folder_path)
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(server_config.log_enabled,
                                                                      server_config.log_verbose,
                                                                      server_config.log_absfile,
                                                                      server_config.is_log_type_xml(),
                                                                      checkpoint_handler_parent = checkpoint_handler_in_folder)

    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '))

    checkpoint_handler.Checkpoint("ssl_version", MODULE_ID, lib.checkpoint.INFO,
                                  cpp_ssl_version=components.communication.get_ssl_version(),
                                  py_ssl_version=ssl.OPENSSL_VERSION
                                  )

    #
    # Initialize Communication component (CryptFacility)
    #
    components.communication.async_service.init(checkpoint_handler)
    global async_service
    async_service = components.communication.async_service.AsyncService(checkpoint_handler, "Management Service", server_config.service_management_num_of_threads)


    #
    # Load license file, and add license issues in log-file
    # The server keeps running, in order to report license problems to management_client
    #
    license_handler = server_config.get_license_handler()
    license = license_handler.get_license()
    if len(license.errors) > 0:
        checkpoint_handler.CheckpointMultilineMessages("license.error", MODULE_ID, lib.checkpoint.ERROR, license.errors)
    if len(license.warnings) > 0:
        checkpoint_handler.CheckpointMultilineMessages("license.warning", MODULE_ID, lib.checkpoint.WARNING, license.warnings)

    checkpoint_handler.Checkpoint("database", MODULE_ID, lib.checkpoint.INFO, db_connect_string=server_config.get_db_connect_info() )
    checkpoint_handler.Checkpoint("service_ws", MODULE_ID, lib.checkpoint.INFO, ip=server_config.management_ws_ip, port=server_config.management_ws_port)
    checkpoint_handler.Checkpoint("service_management", MODULE_ID, lib.checkpoint.INFO, ip=server_config.service_management_listen_ip, port=server_config.service_management_listen_port)

    try:
        environment = components.environment.Environment(checkpoint_handler, server_config.get_db_connect_url(), MODULE_ID, server_config.db_encoding, db_logging=server_config.db_log_enabled)
        components.database.server_common.database_api.SchemaFactory.connect(environment.get_default_database())
        components.templates.server_common.template_api.TemplateHandler.set_base_path(server_config.templates_abspath)

        # initialize portscan
        set_portscan_parameters(server_config)

        # initialize rule_api
        components.auth.server_management.rule_api.setup(server_config.get_ini_path(), checkpoint_handler)

        #
        # Load modules
        #
        environment.plugin_manager = components.plugin.server_management.manager.Manager(checkpoint_handler, environment.get_default_database(), license_handler, server_config.plugin_modules_abspath)

        #
        # Create Management web-server thread
        #
        environment.client_knownsecret = get_client_knownsecret(checkpoint_handler, server_config)
        environment.server_knownsecret = get_server_knownsecret(checkpoint_handler, server_config)
        environment.get_servers = lambda : get_servers(checkpoint_handler, server_config)
        environment.gpms_root = server_config.cpm_gpms_abspath
        environment.gpmc_defs_root = server_config.cpm_gpmc_defs_abspath
        admin_ws_service = components.admin_ws.admin_ws_service.AdminWSService(environment, server_config, license_handler)


        internal_ws_connector = AdminSessionCreator(admin_ws_service.get_connect_url())

        _activity_log_manager = ActivityLog(environment)
        _activity_log_creator = ActivityLogCreator(_activity_log_manager, severity=ActivityLog.INFO)

        #
        # Add recievers of messages from gateway servers
        #
        message_receiver = components.management_message.server_management.session_recieve.ManagementMessageSessionRecieve(checkpoint_handler)

        message_receiver_access_log = components.access_log.server_management.access_log.AccessLogAdminReceiver(environment)
        message_receiver.add_reciever(message_receiver_access_log)

        message_receiver_dialog = components.dialog.server_management.dialog.DialogAdminReceiver(environment)
        message_receiver.add_reciever(message_receiver_dialog)

        message_receiver_user = components.user.server_management.user_admin.UserAdmin(environment, license_handler, _activity_log_creator)
        message_receiver_user.enable_message_receiver_access_log(message_receiver_access_log, server_config)
        message_receiver.add_reciever(message_receiver_user)

        message_receiver_endpoint = components.endpoint.server_management.endpoint.EndpointAdmin(environment, server_config, license_handler, None, internal_ws_connector = internal_ws_connector)
        message_receiver.add_reciever(message_receiver_endpoint)

        message_receiver_auth = components.auth.server_management.rule_api.AuthAdminReceiver(checkpoint_handler)
        message_receiver.add_reciever(message_receiver_auth)

        message_receiver_traffic = components.traffic.server_management.traffic_admin.TrafficAdminSink(environment)
        message_receiver.add_reciever(message_receiver_traffic)


        for plugin in environment.plugin_manager.plugins.values():
            if isinstance(plugin, components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
                message_receiver.add_reciever(plugin)

        #
        # Create management service to handle connections to gateway servers
        #
        service_management_ks_server = get_service_management_server_knownsecret(checkpoint_handler, server_config)
        if service_management_ks_server is None:
            service_stopped_signal.append(7)
            return 0

        dictionary = None
        server_filedist_abspath = server_config.service_management_filedist_abspath
        server_filedist_tickets_max = server_config.service_management_filedist_tickets_max
        server_filedist_check_interval_sec = server_config.service_management_filedist_check_interval_sec
        service_management = ManagementMessageServerSessionManager(async_service,
                                                                   checkpoint_handler,
                                                                   service_management_ks_server,
                                                                   dictionary,
                                                                   internal_ws_connector,
                                                                   server_config.service_management_listen_ip,
                                                                   server_config.service_management_listen_port,
                                                                   "1",
                                                                   message_receiver,
                                                                   server_filedist_abspath,
                                                                   server_filedist_tickets_max,
                                                                   server_filedist_check_interval_sec,
                                                                   server_config.get_gateway_server_path(checkpoint_handler)
                                                                   )
        service_management.start()
        admin_ws_service.set_service_management(service_management)

        #
        #
        #
        cp_in_folder_janitor = None
        if server_config.log_in_folder_enabled:
            folder_to_watch = server_config.log_in_folder_abspath
            cp_in_folder_janitor_callback = CpInFolderJanitorCallback(checkpoint_handler, folder_to_watch, message_receiver_access_log)
            cp_in_folder_janitor = lib.appl.cp_in_folder_janitor.CPInFolderJanitor(async_service, checkpoint_handler, folder_to_watch, cp_in_folder_janitor_callback)
            cp_in_folder_janitor.start()

        #
        # Start server thread
        #
        main_server = MainServer(checkpoint_handler, admin_ws_service, service_stop_signal, service_stopped_signal)
        main_server.start()
        async_service.start()
        main_server.join()

        #
        # Shut down
        #
        service_management.stop_and_wait()
        if server_config.log_in_folder_enabled:
            cp_in_folder_janitor.stop_and_wait()
        async_service.stop()

    except:
        checkpoint_handler.CheckpointExceptionCurrent("main.error", MODULE_ID, lib.checkpoint.CRITICAL)


    # Signal the servicehandler that this program has stopped
    service_stopped_signal.append(7)
    checkpoint_handler.Checkpoint("stopped", MODULE_ID, lib.checkpoint.INFO)


if __name__ == '__main__':
    print 'This file is not intented to be runned directly from the commandline,'
    print 'please use service file.'

    service_stop_signal=[]
    service_stopped_signal=[]
    main(service_stop_signal, service_stopped_signal)
