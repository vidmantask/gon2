"""
The G/On Client Uninstaller
"""
from __future__ import with_statement
import sys
import optparse
import threading
import time
import os
import os.path
import ConfigParser
import tempfile
import shutil

import lib.version
import lib.checkpoint
import lib.appl.gon_client
import lib.appl.shortcut
import lib.gpm.gpm_env
import lib.gpm.gpm_analyzer
import lib.gpm.gpm_builder

import components.dictionary.common

import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_client_runtime_env
import components.plugin.client_gateway.plugin_socket_token
import plugin_types.client_gateway.plugin_type_client_runtime_env

import components.communication.async_service


import components.config.common
import components.presentation.user_interface

from components.presentation.update import UpdateController as gui_update_controller

import lib.appl.io_hooker
import lib.appl.crash.handler

if sys.platform == 'win32':
    import lib.hardware.windows_security_center


#
# Imports used in plugins
#
import lib.config
import components.communication.message
import plugin_types.client_gateway.plugin_type_auth
import plugin_types.client_gateway.plugin_type_client_runtime_env
import plugin_types.client_gateway.plugin_type_endpoint
import plugin_types.client_gateway.plugin_type_token


MODULE_ID = "appl_gon_client_uninstaller"


class ClientUninstallerOptions(components.config.common.ConfigClientUninstaller):
    """
    Options and ini-file values for the Uninstaller
    """
    def __init__(self):
        components.config.common.ConfigClientUninstaller.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--config', type='string', default='./gon_client_uninstaller.ini', help='configuration file')
        self._parser.add_option('--installation_root', type='string', default=None, help='Path of installation to uninstall')
        (self._options, self._args) = self._parser.parse_args()

        ini_filename = self._options.config
        self.read_config_file(ini_filename)

    def cmd_show_version(self):
        return self._options.version

    def get_plugin_modules_path_abs(self):
        return os.path.abspath(self.plugin_modules_path)

    def get_gui_image_path_abs(self):
        return os.path.abspath(self.gui_image_path)

    def get_dictionary_path_abs(self):
        return os.path.abspath(self.dictionary_path)

    def get_installation_root(self):
        return self._options.installation_root.decode(sys.getfilesystemencoding())


class TaskDiscoverControler(threading.Thread):
    """
    Controler that handles the discovery of installations
    """
    def __init__(self, checkpoint_handler, dictionary, client_uninstaller_config, plugin_socket_runtime_env, gui_controler):
        threading.Thread.__init__(self, name="TaskDiscoverControlerThread.run")
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.client_uninstaller_config = client_uninstaller_config
        self.plugin_socket_runtime_env = plugin_socket_runtime_env
        self.gui_controler = gui_controler
        self._running = False
        self._runtime_env = None

    def run(self):
        with self.checkpoint_handler.CheckpointScope("TaskDiscoverControlerThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                time.sleep(1)
                runtime_env_ids = self.plugin_socket_runtime_env.get_available_ids()
                self._select_runtime_env_for_uninstallation(runtime_env_ids)
                time.sleep(1)
                self._running = False
                self.gui_controler.cb_state_discovering_done()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("TaskDiscoverControlerThread.run", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                self.gui_controler.cb_state_error(str(evalue))
            self._running = False

    def _select_runtime_env_for_uninstallation(self, runtime_env_ids):
        installation_root = self.client_uninstaller_config.get_installation_root()
        if installation_root is None:
            return
        for runtime_env_id in runtime_env_ids:
            runtime_env = self.plugin_socket_runtime_env.get_instance(runtime_env_id)
            runtime_env_root_normpath = os.path.normpath(runtime_env.get_root())
            installation_root_normpath = os.path.normpath(installation_root)
            self.checkpoint_handler.Checkpoint("_select_runtime_env_for_uninstallation", MODULE_ID, lib.checkpoint.DEBUG, runtime_env_root_normpath=repr(runtime_env_root_normpath), installation_root_normpath=repr(installation_root_normpath))
            if os.path.normcase(runtime_env_root_normpath) == os.path.normcase(installation_root_normpath):
                self.checkpoint_handler.Checkpoint("_select_runtime_env_for_uninstallation.found", MODULE_ID, lib.checkpoint.DEBUG, runtime_env_id=runtime_env_id)
                self._runtime_env = runtime_env
                break

    def is_running(self):
        return self._running

    def get_runtime_env(self):
        return self._runtime_env


class TaskUninstallControler(threading.Thread):
    """
    Controler that handles the uninsatllation
    """
    STATE_INIT = 0
    STATE_RUNNING = 1
    STATE_DONE = 4
    STATE_CANCELING = 5
    STATE_CANCELED = 6
    STATE_ERROR = 7

    def __init__(self, checkpoint_handler, dictionary, runtime_env, gui_controler):
        threading.Thread.__init__(self, name="TaskUninstallControlerThread.run")
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.gui_controler = gui_controler
        self.runtime_env = runtime_env
        self._state = TaskUninstallControler.STATE_INIT

    def run(self):
        with self.checkpoint_handler.CheckpointScope("TaskUninstallControlerThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._state = TaskUninstallControler.STATE_RUNNING

                if self._state not in [TaskUninstallControler.STATE_CANCELING, TaskUninstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(0, self.dictionary._('Removing installation'))
                    self._state_remove_folder_start()

                if self._state not in [TaskUninstallControler.STATE_CANCELING, TaskUninstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(80, self.dictionary._('Removing shortcuts'))
                    self._state_remove_shortcuts_start()

                if self._state not in [TaskUninstallControler.STATE_CANCELING, TaskUninstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(90, self.dictionary._('Removing registry information'))
                    self._state_remove_regkey_start()

                if self._state not in [TaskUninstallControler.STATE_CANCELING, TaskUninstallControler.STATE_ERROR]:
                    self._state = TaskUninstallControler.STATE_DONE

                if self._state in [TaskUninstallControler.STATE_CANCELING]:
                    self._state = TaskUninstallControler.STATE_CANCELED
                    self.gui_controler.cb_state_uninstallation_canceled()

                if self._state not in [TaskUninstallControler.STATE_CANCELED, TaskUninstallControler.STATE_ERROR]:
                    self._state = TaskUninstallControler.STATE_DONE
                    self.gui_controler.cb_state_uninstallation_done()

            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("TaskUninstallControlerThread.run", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                self.gui_controler.cb_state_error(self.dictionary._('Internal error'))
                self._state = TaskUninstallControler.STATE_ERROR

    def is_running(self):
        return self._state not in [TaskUninstallControler.STATE_DONE, TaskUninstallControler.STATE_ERROR, TaskUninstallControler.STATE_CANCELED]

    def do_cancel(self):
        self._state = TaskUninstallControler.STATE_CANCELING

    def _state_remove_regkey_start(self):
        import plugin_modules.endpoint.client_gateway_common
        endpoint_ginfo = plugin_modules.endpoint.client_gateway_common.EndpointGInfo()
        serial = endpoint_ginfo.lookup_serial(plugin_modules.endpoint.client_gateway_common.EndpointToken.REG_KEY_INSTALLATION_LOCATION, self.runtime_env.get_root(), None)
        if serial is not None:
            endpoint_ginfo.remove(serial)
        else:
            self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_regkey.serial_not_found", MODULE_ID, lib.checkpoint.WARNING, serial=serial)

    def _state_remove_folder_start(self):
        (gon_client_folder_sub_base, gon_client_folder) =  lib.appl.gon_client.generate_installation_root()
        safe_installation_root_base = os.path.dirname(gon_client_folder)
        self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_folder.looking", MODULE_ID, lib.checkpoint.DEBUG, safe_installation_root_base=repr(safe_installation_root_base), runtime_env_root=repr(self.runtime_env.get_root()))
        if os.path.normpath(self.runtime_env.get_root()).startswith(safe_installation_root_base):
            shutil.rmtree(self.runtime_env.get_root(), ignore_errors=True)
        else:
            self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_folder.not_removed_becaues_of_location", MODULE_ID, lib.checkpoint.WARNING, installation_location=self.runtime_env.get_root())

    def _state_remove_shortcuts_start(self):
        with self.checkpoint_handler.CheckpointScope("TaskUninstallControler.remove_shortcuts", MODULE_ID, lib.checkpoint.DEBUG) as cps:
            import plugin_modules.endpoint.client_gateway_common
            endpoint_ginfo = plugin_modules.endpoint.client_gateway_common.EndpointGInfo()
            serial = endpoint_ginfo.lookup_serial(plugin_modules.endpoint.client_gateway_common.EndpointToken.REG_KEY_INSTALLATION_LOCATION, self.runtime_env.get_root(), None)
            cps.add_complete_attr(serial=serial)
            if serial is not None:
                shortcuts_string = endpoint_ginfo.read(serial, plugin_modules.endpoint.client_gateway_common.EndpointToken.REG_KEY_SHORTCUTS, None)
                cps.add_complete_attr(shortcuts_string=shortcuts_string)
                if shortcuts_string is not None:
                    shortcuts = shortcuts_string.split(',')
                    print shortcuts
                    for shortcut in shortcuts:
                        if os.path.exists(shortcut):
                            os.unlink(shortcut)
                            self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_shortcuts_start.shortcut_removed", MODULE_ID, lib.checkpoint.DEBUG, serial=serial, shortcut=shortcut)
                        else:
                            self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_shortcuts_start.shortcut_not_found", MODULE_ID, lib.checkpoint.WARNING, serial=serial, shortcut=shortcut)
            else:
                self.checkpoint_handler.Checkpoint("TaskUninstallControler.remove_shortcuts_start.serial_not_found", MODULE_ID, lib.checkpoint.WARNING, serial=serial)


class GUIControler(object):
    """
    Handling of gui state and interaction with user
    """
    STATE_INIT = 0
    STATE_DISCOVER = 1
    STATE_CONFIRM = 2
    STATE_UNINSTALLING = 3
    STATE_DONE = 4
    STATE_ERROR = 5
    STATE_CANCELLING = 6
    STATE_CANCELED = 7

    def __init__(self, checkpoint_handler, dictionary, user_interface, client_uninstaller_config, plugin_socket_token, plugin_socket_runtime_env):
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.user_interface = user_interface
        self.client_uninstaller_config = client_uninstaller_config
        self.plugin_socket_token = plugin_socket_token
        self.plugin_socket_runtime_env = plugin_socket_runtime_env
        self.user_interface.update.subscribe(self._gui_on_update)
        self._state = GUIControler.STATE_INIT
        self._gui_update_state()
        self.user_interface.update.set_banner(gui_update_controller.ID_BANNER_UNINSTALL)
        self.user_interface.update.display()

        self._state_error_message = ''
        self._state_installation_canceling = False

        self._state_discovering_start()

    def _gui_update_tasklist(self):
        task_discovering = {'name': self.dictionary._('Detect'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_confirm = {'name': self.dictionary._('Confirm'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_unsinstalling = {'name': self.dictionary._('Unstalling'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_done = {'name': self.dictionary._('Done'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_error = {'name': self.dictionary._('Error'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}
        task_cancelling = {'name': self.dictionary._('Cancelling'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}
        task_canceled = {'name': self.dictionary._('Canceled'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}

        if self._state in [GUIControler.STATE_DISCOVER]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_CONFIRM]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_confirm['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_UNINSTALLING]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_confirm['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_unsinstalling['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_CANCELLING]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_confirm['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_cancelling['state'] = gui_update_controller.ID_PHASE_ACTIVE
            task_canceled['state'] = gui_update_controller.ID_PHASE_PENDING
            task_unsinstalling['show'] = False
            task_cancelling['show'] = True
            task_canceled['show'] = True
            task_done['show'] = False
        elif self._state in [GUIControler.STATE_CANCELED]:
            task_cancelling['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_canceled['state'] = gui_update_controller.ID_PHASE_ACTIVE
            task_unsinstalling['show'] = False
            task_cancelling['show'] = True
            task_canceled['show'] = True
            task_done['show'] = False
        elif self._state in [GUIControler.STATE_DONE]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_confirm['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_unsinstalling['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_done['state'] = gui_update_controller.ID_PHASE_ACTIVE
        else:
            task_discovering['show'] = False
            task_confirm['show'] = False
            task_unsinstalling['show'] = False
            task_done['show'] = False
            task_error['show'] = True
            task_error['state'] = gui_update_controller.ID_PHASE_ACTIVE

        tasklist = []
        self._gui_append_to_tasklist(tasklist, task_discovering)
        self._gui_append_to_tasklist(tasklist, task_confirm)
        self._gui_append_to_tasklist(tasklist, task_unsinstalling)
        self._gui_append_to_tasklist(tasklist, task_cancelling)
        self._gui_append_to_tasklist(tasklist, task_canceled)
        self._gui_append_to_tasklist(tasklist, task_done)
        self._gui_append_to_tasklist(tasklist, task_error)
        self.user_interface.update.set_phase_list(tasklist)

    def _gui_append_to_tasklist(self, tasklist, task):
        if task['show']:
            tasklist.append(task)

    def _gui_update_state(self):
        if self._state == GUIControler.STATE_DISCOVER:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self.user_interface.update.set_info_headline(self.dictionary._('Detecting installation'))
            self.user_interface.update.set_info_progress_mode(gui_update_controller.ID_MODE_PROGRESS_UNKNOWN)
            self.user_interface.update.set_info_progress_subtext('')
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_CONFIRM:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('G/On Client Uninstallation'))
            text = self.dictionary._("If you proceed, the G/On Client in the folder '%s' and the associated ComputerUserToken will be removed.") % self._state_discover_runtime_env.get_root()
            self.user_interface.update.set_info_free_text(text)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Uninstall'))
        elif self._state == GUIControler.STATE_UNINSTALLING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self.user_interface.update.set_info_progress_mode(gui_update_controller.ID_MODE_PROGRESS_UNKNOWN)
            self.user_interface.update.set_info_headline(self.dictionary._('Uninstalling G/On Client'))
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_DONE:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Uninstallation complete'))
            self.user_interface.update.set_info_free_text(self.dictionary._('The G/On Client has now been uninstalled.'))
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))
        elif self._state == GUIControler.STATE_CANCELLING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Cancel uninstallation, please wait'))
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_CANCELED:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Uninstallation of the G/On Client canceled'))
            self.user_interface.update.set_info_free_text(self._state_error_message)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))
        elif self._state == GUIControler.STATE_ERROR:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline('')
            self.user_interface.update.set_info_free_text(self._state_error_message)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))
        self._gui_update_tasklist()

    def _gui_on_update(self):
        if self._state == GUIControler.STATE_CONFIRM:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_uninstallation_start()
        elif self._state == GUIControler.STATE_UNINSTALLING:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_installation_cancel()
        elif self._state == GUIControler.STATE_DONE:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()
        elif self._state == GUIControler.STATE_ERROR:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()
        elif self._state == GUIControler.STATE_CANCELED:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()

    def _state_discovering_start(self):
        if self._state in [GUIControler.STATE_INIT]:
            self._task_discover_controler = TaskDiscoverControler(self.checkpoint_handler, self.dictionary, self.client_uninstaller_config, self.plugin_socket_runtime_env, self)
            self._state = GUIControler.STATE_DISCOVER
            self._gui_update_state()
            self._task_discover_controler.start()

    def cb_state_discovering_done(self):
        if self._state in [GUIControler.STATE_DISCOVER]:
            self._state_discover_runtime_env =  self._task_discover_controler.get_runtime_env()
            if self._state_discover_runtime_env is None:
                self._state_error_start(self.dictionary._('Unable to find installation'))
                return
            self._state = GUIControler.STATE_CONFIRM
            self._gui_update_state()

    def _state_error_start(self, error_message):
        self._state_error_message = error_message
        self._state = GUIControler.STATE_ERROR
        self._gui_update_state()

    def cb_state_error(self, message):
        self._state_error_start(message)

    def _state_uninstallation_start(self):
        if self._state in [GUIControler.STATE_CONFIRM]:
            self._state = GUIControler.STATE_UNINSTALLING
            self._task_uninstall_controler = TaskUninstallControler(self.checkpoint_handler, self.dictionary, self._state_discover_runtime_env, self)
            self._gui_update_state()
            self._task_uninstall_controler.start()

    def _state_installation_cancel(self):
        self._state = GUIControler.STATE_CANCELLING
        self._task_uninstall_controler.do_cancel()
        self._gui_update_state()

    def cb_progress(self, progress, progress_message=None):
        if self._state in [GUIControler.STATE_DISCOVER, GUIControler.STATE_UNINSTALLING]:
            self.user_interface.update.set_info_progress_complete(progress)
            if progress_message is not None:
                self.user_interface.update.set_info_progress_subtext(progress_message)

    def cb_state_uninstallation_done(self):
        if self._state == GUIControler.STATE_UNINSTALLING:
            self._state = GUIControler.STATE_DONE
            self._gui_update_state()

    def cb_state_uninstallation_canceled(self):
        self._state = GUIControler.STATE_CANCELED
        self._gui_update_state()

    def _state_exit(self):
        self.user_interface.destroy()


def main():
    #
    # Parsing arguments from commandline and ini-file
    #
    client_uninstaller_config = ClientUninstallerOptions()

    #
    # Show version and exit if requested
    #
    if(client_uninstaller_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    #
    # Initialize logging
    #
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(client_uninstaller_config.log_enabled,
                                                                      client_uninstaller_config.log_verbose,
                                                                      client_uninstaller_config.log_file,
                                                                      client_uninstaller_config.is_log_type_xml())
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)
    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                  )

    try:
        #
        # Initialize async service
        #
        components.communication.async_service.init(checkpoint_handler)
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 1)

        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, client_uninstaller_config.get_dictionary_path_abs())
        user_interface = components.presentation.user_interface.UserInterface(configuration=client_uninstaller_config, dictionary=dictionary)
        user_interface.common.set_visible(False)
        user_interface.update.set_window_frame_text(dictionary._('G/On Uninstall'))

        #
        # Load plugins, and create socket
        #
        plugin_manager       = components.plugin.client_gateway.manager.Manager(checkpoint_handler, client_uninstaller_config.get_plugin_modules_path_abs())
        plugins = plugin_manager.create_instances(async_service=async_service, checkpoint_handler=checkpoint_handler, user_interface=user_interface, additional_device_roots=[])
        plugin_socket_runtime_env = components.plugin.client_gateway.plugin_socket_client_runtime_env.PluginSocket(plugin_manager, plugins, checkpoint_handler)
        plugin_socket_token = components.plugin.client_gateway.plugin_socket_token.PluginSocket(plugin_manager, plugins)

        async_service.start()

        #
        # Start gui-main loop
        #
        gui_handler = GUIControler(checkpoint_handler, dictionary, user_interface, client_uninstaller_config, plugin_socket_token, plugin_socket_runtime_env)
        user_interface.start()

        async_service.stop()
        async_service.join()

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
        sys.path.append(path) # To be able to import plugin_modules dirrectly
    main()
