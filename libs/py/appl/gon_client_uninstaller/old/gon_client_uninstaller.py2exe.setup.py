import lib.dev_env.path_setup
from distutils.core import setup
import py2exe
import lib.appl_plugin_module_util

import lib.dev_env
import lib.dev_env.pack
dev_env = lib.dev_env.DevEnv.create_current()

#
#call: python setup.py py2exe
#
data_files = [('images', lib.dev_env.pack.generate_image_files('../../components/presentation/gui/mfc/images', ['.bmp', '.ico']))]

data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway_common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway_deploy'))

data_files.append(('', ['../../setup/dlls/win/libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                        '../../setup/dlls/win/msvcr90.dll',
                        '../../setup/dlls/win/msvcp90.dll',
                        '../../setup/dlls/win/msvcm90.dll',
                        '../../setup/dlls/win/Microsoft.VC90.CRT.manifest'
]) )

manifest_template = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
  <compatibility xmlns="urn:schemas-microsoft-com:compatibility.v1">
    <application>
      <!--The ID below indicates application support for Windows Vista -->
        <supportedOS Id="{e2011457-1546-43c5-a5fe-008deee3d3f0}"/>
      <!--The ID below indicates application support for Windows 7 -->
        <supportedOS Id="{35138b9a-5d96-4fbd-8e2d-a2440225f93a}"/>
    </application>
  </compatibility>
  <trustInfo xmlns="urn:schemas-microsoft-com:asm.v3">
    <security>
      <requestedPrivileges>
        <requestedExecutionLevel level="asInvoker" uiAccess="false"></requestedExecutionLevel>
      </requestedPrivileges>
    </security>
  </trustInfo>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity type="win32" name="Microsoft.Windows.Common-Controls" version="6.0.0.0"  processorArchitecture="X86" publicKeyToken="6595b64144ccf1df" language="*"></assemblyIdentity>
    </dependentAssembly>
  </dependency>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity type="win32" name="Microsoft.VC90.CRT" version="9.0.21022.8" processorArchitecture="x86" publicKeyToken="1fc8b3b9a1e18e3b"></assemblyIdentity>
    </dependentAssembly>
  </dependency>
</assembly>
"""

setup(
      windows = [ lib.appl_plugin_module_util.Py2ExeTarget(
                          script='gon_client_uninstaller.py',
                          uac = False,
                          branding = lib.appl_plugin_module_util.Py2ExeTarget.BRANDING_NORMAL,
                          name = 'G/On Client Uninstaller',
                          description = 'G/On Client Uninstaller for Windows',
                          image_root='../../components/presentation/gui/mfc/images',
                          manifest = manifest_template
                        )
               ],
      options = {
            "py2exe": {
                  "dll_excludes": [
                    'w9xpopen.exe',
                    'MSVCP90.dll',
                    'libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                    'api-ms-win-core-string-l1-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-0.dll',
                    'api-ms-win-core-libraryloader-l1-2-1.dll',
                    'api-ms-win-core-atoms-l1-1-0.dll',
                    'api-ms-win-core-winrt-error-l1-1-1.dll',
                    'api-ms-win-core-sidebyside-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-3-0.dll',
                    'api-ms-win-core-heap-l1-2-0.dll',
                    'api-ms-win-core-heap-l2-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-1.dll',
                    'api-ms-win-core-libraryloader-l1-2-0.dll',
                    'api-ms-win-core-rtlsupport-l1-2-0.dll',
                    'api-ms-win-core-shlwapi-obsolete-l1-2-0.dll',
                    'api-ms-win-security-base-l1-2-0.dll',
                    'api-ms-win-core-synch-l1-2-0.dll',
                    'api-ms-win-core-handle-l1-1-0.dll',
                    'api-ms-win-core-registry-l1-1-0.dll',
                    'api-ms-win-core-synch-l1-1-0.dll',
                    'api-ms-win-core-localization-l1-2-0.dll',
                    'api-ms-win-core-profile-l1-1-0.dll',
                    'api-ms-win-core-sysinfo-l1-1-0.dll',
                    'api-ms-win-core-errorhandling-l1-1-0.dll',
                    'api-ms-win-core-file-l1-1-0.dll',
                    'api-ms-win-core-timezone-l1-1-0.dll',
                    'api-ms-win-core-processenvironment-l1-1-0.dll',
                    'api-ms-win-security-base-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-2-0.dll',
                    'api-ms-win-core-string-obsolete-l1-1-0.dll',
                    'api-ms-win-crt-private-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-1.dll',
                    'api-ms-win-crt-string-l1-1-0.dll',
                    'api-ms-win-crt-runtime-l1-1-0.dll',
                    'api-ms-win-core-heap-l1-1-0.dll',
                    'api-ms-win-core-interlocked-l1-1-0.dll',
                    'api-ms-win-core-debug-l1-1-0.dll'
                    ],
                  "dist_dir" :  dev_env.generate_pack_destination('gon_client_uninstaller'),
                  "packages":[
                                     'logging',
                                     'sqlalchemy.dialects.sqlite',
                                     'lib.cryptfacility',
                                     'elementtree',
                                     'win32com.client',
                                     'uuid'
                             ],
                  "includes":[],
                  "excludes":[
                              'plugin_modules'
                             ],
                  "bundle_files" : 3,
                  "compressed" : True,
                  "optimize" : 0,
            }
      },
      data_files = data_files,
      zipfile = None,
)
