"""
The G/On License
"""
from __future__ import with_statement
import sys
import optparse
import os.path

import components.license.licensefile as licensefile
import components.license.gonlicense  as gonlicense
import lib.version

import lib.checkpoint 
import components.communication.async_service
import struct
from Crypto.Cipher import AES
import hashlib

import random
import string
import StringIO


module_id = "appl_gon_licenser"



def encrypt_file(key, in_filename, out_filename=None, chunksize=64*1024):
    """ Encrypts a file using AES (CBC mode) with the
        given key.

        key:
            The encryption key - a string that must be
            either 16, 24 or 32 bytes long. Longer keys
            are more secure.

        in_filename:
            Name of the input file

        out_filename:
            If None, '<in_filename>.enc' will be used.

        chunksize:
            Sets the size of the chunk which the function
            uses to read and encrypt the file. Larger chunk
            sizes can be faster for some files and machines.
            chunksize must be divisible by 16.
    """
    if not out_filename:
        out_filename = in_filename + '.enc'

    iv = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)

    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
            outfile.write(struct.pack('<Q', filesize))
            outfile.write(iv)

            while True:
                chunk = infile.read(chunksize)
                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 != 0:
                    chunk += ' ' * (16 - len(chunk) % 16)

                outfile.write(encryptor.encrypt(chunk))
                
                


def decrypt_file(key, in_filename, out_filename=None, chunksize=24*1024):
    """ Decrypts a file using AES (CBC mode) with the
        given key. Parameters are similar to encrypt_file,
        with one difference: out_filename, if not supplied
        will be in_filename without its last extension
        (i.e. if in_filename is 'aaa.zip.enc' then
        out_filename will be 'aaa.zip')
    """
    if not out_filename:
        out_filename = os.path.splitext(in_filename)[0]

    with open(in_filename, 'rb') as infile:
        origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        iv = infile.read(16)
        decryptor = AES.new(key, AES.MODE_CBC, iv)

        outfile = StringIO.StringIO()
        while True:
            chunk = infile.read(chunksize)
            if len(chunk) == 0:
                break
            outfile.write(decryptor.decrypt(chunk))

        outfile.truncate(origsize)
        return outfile.getvalue()
    return None
            
            



class LicenseOptions(object):
    """
    Options and ini-file values for the gon_license tool. 
    """
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--stamp', action='store_true', help='Stamp license file')
        self._parser.add_option('--generate_keypair', action='store_true', help='Generate a new keypair')
        self._parser.add_option('--license_file', type='string', default='./gon_license.lic', help='Name of license file')
        self._parser.add_option('--private', type='string', help='Private key to use for stamping')
        self._parser.add_option('--public', type='string', help='Public key to use for stamping')
        self._parser.add_option('--keypair_name', type='string', default=None, help='Name of keypair to use/generate')
        self._parser.add_option('--keypair_folder', type='string', default=None, help='Folder where keypair are read/written')
        self._parser.add_option('--private_enc_key', type='string', default=None, help='Key to decrypt private key file')

        (self._options, self._args) = self._parser.parse_args()

    def cmd_show_version(self):
        return self._options.version

    def cmd_stamp(self):
        return self._options.stamp

    def cmd_generate_keypair(self):
        return self._options.generate_keypair

    def get_license_file(self):
        return self._options.license_file

    def get_keypair_name(self):
        keypair_folder = self._options.keypair_folder
        if keypair_folder is not None:
            return os.path.join(keypair_folder, self._options.keypair_name)
        return self._options.keypair_name

    def get_private(self):
        return self._options.private

    def get_public(self):
        return self._options.public
    
    def get_private_enc_key(self):
        return hashlib.sha256(self._options.private_enc_key).digest()
    


def stamp_license_file(license_filename, private, public):
    """
    Functionality for restamping a license file
    """
    try:
        if not os.path.exists(license_filename):
            print "License file '%s' not found" % license_filename
            return 1
    
        print "Stamping license file '%s' " % license_filename
        gonlicense.gon_licensefile_stamper(pub=public, priv=private, filename=license_filename) 
    
        print "Validating stamped license file for G/On"
        license = gonlicense.GonLicense(license_filename, public_key=public)
        if license.errors:
            print "ERROR: License is not valid"
            for error in license.errors:
                print "ERROR: %s" % error
            return 2
        print "License is ok"
        return 0
    except:
        print "License is NOT ok"
        return 1
        


def generate_keypair(keypair_name):
    print "Generating keypair" 
    licensefile.generate_keypair(keypair_name)

    print "Encrypt private key"    
    password = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
    key = hashlib.sha256(password).digest()
    private_key_filename = licensefile.priv_name(keypair_name)
    private_key_filename_enc = private_key_filename + '.enc'
    
    encrypt_file(key, private_key_filename, private_key_filename_enc)

    print "Encrypt private key, password is %s" % password    
    return 0
    

def main():

    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    components.communication.async_service.init(checkpoint_handler)

    
    license_config = LicenseOptions()
    
    #
    # Show version and exit if requested
    #
    if(license_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0


    #
    # Restamp license file
    #
    if(license_config.cmd_stamp()):
        priv = None
        pub = None
        keypair_name = license_config.get_keypair_name()
        if keypair_name is not None:
            
            if license_config.get_private_enc_key() is not None:
                priv_filename = '%s.priv.enc' % keypair_name
                if os.path.exists(priv_filename):
                    priv = decrypt_file(license_config.get_private_enc_key(), priv_filename)
                else:
                    print "Private key-file('%s') not found" % priv_filename
                    return 0
            else:
                priv_filename = '%s.priv' % keypair_name
                if os.path.exists(priv_filename):
                    priv = open(priv_filename, 'r').read()
                else:
                    print "Private key-file('%s') not found" % priv_filename
                    return 0
            pub_filename = '%s.pub' % keypair_name
            if os.path.exists(pub_filename):
                pub = open(pub_filename, 'r').read()
            else:
                print "Public key-file('%s') not found" % pub_filename
                return 0
        else:
            priv = license_config.get_private()
            if not priv:
                print "Private key is missing"
                return 0
        
            pub = license_config.get_public()
            if not pub:
                pub = lib.version.Version.create_current().get_license_stamp_key()
        return stamp_license_file(license_config.get_license_file(), priv, pub)

    if(license_config.cmd_generate_keypair()):
        keypair_name = license_config.get_keypair_name()
        if keypair_name is None:
            print "Missing keypair name"
            return 0
        return generate_keypair(license_config.get_keypair_name())

    print  "Nothing to do"
    return 0

if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    main()
