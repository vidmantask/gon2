"""
The G/On Gateway Server
"""
from __future__ import with_statement
import warnings
warnings.filterwarnings("ignore")

import sys
if not hasattr(sys, "frozen"):
    import lib.dev_env.path_setup

import optparse
import threading
import time
import ConfigParser
import os.path
import shutil
import uuid
import datetime
import os
import ssl

import lib.version
import lib.checkpoint
import lib.appl.crash.handler
import lib.appl.io_hooker
import lib.ToHserver
import lib.appl.service
import lib.appl.gon_os

import lib.appl.cp_in_folder_janitor
import components.config.common
import components.license.gonlicense

import components.environment
import components.database.server_common.database_connection

import components.communication
import components.communication.session
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base
import components.communication.session_manager as com_session_manager
import components.communication.async_service


import components.dictionary.common

import components.management_message
import components.management_message.server_common.session_client
import components.management_message.server_gateway.session_send

#import components.auth.server_gateway.auth_session as component_auth
import components.auth.server_gateway.auth_session as component_auth

import components.auth.server_common.database_schema as component_auth_database_schema
import components.dialog.server_gateway.dialog_session as component_dialog
import components.traffic.server_gateway.traffic_session as component_traffic
import components.user.server_gateway.user_session as component_user
import components.cpm.server_gateway.cpm_session as component_cpm
import components.cpm.server_gateway.cpm_download_manager as component_cpm_download_manager
import components.access_log.server_gateway.access_log
import components.endpoint.server_gateway.endpoint_session as component_endpoint
import components.web_server.server_gateway

import components.plugin.server_gateway.plugin_socket_tag as plugin_socket_tag
import components.traffic.server_gateway.server_launch_internal


import components.web_server.server_gateway

import components.traffic.common.selector

import components.plugin.server_gateway.manager

import plugin_types.common.plugin_type_token
import plugin_types.server_gateway.plugin_type_auth
import plugin_types.server_gateway.plugin_type_tag
import plugin_types.server_gateway.plugin_type_traffic
import plugin_types.server_gateway.plugin_type_user

import lib.utility.mem_leaks

import sqlalchemy.sql.default_comparator
import six

MODULE_ID = "gon_server_gateway"


class ServerGatewayOptions(components.config.common.ConfigServerGateway):
    """
    Options and ini-file values for the Gateway Server.
    """
    def __init__(self, instance_run_root, args=None):
        components.config.common.ConfigServerGateway.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--foreground', action='store_true', help='run server in foreground')
        self._parser.add_option('--foreground_with_stop_file', action='store_true', help='run server in foreground')
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--config', type='string', default='./gon_server_gateway.ini', help='configuration file')

        (self._options, self._args) = self._parser.parse_args(args=args)

        self.instance_run_root = instance_run_root
        config_filename = os.path.join(instance_run_root, self._options.config)
        self.read_config_file(config_filename)
        self.load_and_set_scramble_salt()

    def get_instance_run_root(self):
        return self.instance_run_root

    def cmd_show_version(self):
        return self._options.version

    def get_service_sid(self):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        return config_local.service_sid

    def set_server_sid(self, service_sid):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        config_local.service_sid = service_sid
        config_local.write_config_file(self.get_instance_run_root())

    def get_service_title(self):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        return config_local.service_title

    def get_service_ip(self):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        if config_local.service_ip is not None:
            return config_local.service_ip
        return self.service_ip

    def get_service_port(self):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        if config_local.service_port is not None:
            return config_local.service_port
        return self.service_port

    def get_service_management_filedist_enabled(self):
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(self.get_instance_run_root())
        if config_local.service_management_filedist_enabled is not None:
            return config_local.service_management_filedist_enabled
        return self.service_management_filedist_enabled

    def get_server_knownsecret(self, checkpoint_handler):
        filename = os.path.join(self.deployment_abspath, components.config.common.KS_SERVER_FILENAME)
        if os.path.exists(filename):
            return open(filename, 'r').read()
        checkpoint_handler.Checkpoint("get_server_knownsecret.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
        return None

    def get_client_servers(self, checkpoint_handler):
        filename = os.path.join(self.deployment_abspath, components.config.common.CLIENT_SERVERS_FILENAME)
        if os.path.exists(filename):
            return open(filename, 'r').read()
        checkpoint_handler.Checkpoint("get_client_servers.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
        return None

    def get_service_management_client_knownsecret(self, checkpoint_handler):
        filename = os.path.join(self.deployment_abspath, components.config.common.SERVICE_MANAGEMENT_KS_CLIENT_FILENAME)
        if os.path.exists(filename):
            return open(filename, 'r').read()
        checkpoint_handler.Checkpoint("get_service_management_client_knownsecret.not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filename)
        return None



class IServerSessionCallback(object):
    """
    Interface for component sessions to make callback
    """
    def child_close(self, force):
        """
        Child is asking to close down the session
        """
        raise NotImplementedError


class SessionInfo(object):
    def __init__(self, com_session):
        self.unique_session_id = com_session.get_unique_session_id()
        self.socket_info_remote = com_session.get_ip_remote()
        self.socket_info_local = com_session.get_ip_local()


class ServerSession(components.communication.session.APISessionEventhandlerReady,
                    IServerSessionCallback):
    def __init__(self, access_log_server_session, async_service, com_session, environment, server_config, cpm_download_manager, server_knownsecret, license_handler, dictionary, service_management_message_send, http_proxy_server_thread):
        components.communication.session.APISessionEventhandlerReady.__init__(self, environment.checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = environment.checkpoint_handler
        self.session_checkpoint_handler = None
        self._server_config = server_config
        self._server_knownsecret = server_knownsecret
        self._license_handler = license_handler
        self._dictionary = dictionary
        self._service_management_message_send = service_management_message_send
        self._access_log_server_session = access_log_server_session
        self.ignore_child_close = False
        self.session_info = SessionInfo(com_session)
        self.http_proxy_server_thread = http_proxy_server_thread

        if server_config.log_session_enabled or server_config.log_session_enabled_by_remote:
            session_log_root = server_config.log_session_abspath
            self.session_checkpoint_handler = lib.checkpoint.create_session_checkpoint_handler(self.checkpoint_handler, self.session_info.unique_session_id, MODULE_ID, session_log_root)
            self.checkpoint_handler = self.session_checkpoint_handler

        self._async_service = async_service

        self._environment = environment
        self._com_session = com_session
        self._cpm_download_manager = cpm_download_manager
        self._com_session.set_keep_alive_ping_interval_sec(server_config.session_keep_alive_ping_interval_sec)

        components.database.server_common.database_api.SchemaFactory.connect(environment.get_default_database())
        self._com_session.set_eventhandler_ready(self)
        self._sessions_initialized = False
        self._sessions_closed = False


    def _init_session_elements(self, dummy_arc):
        with self.checkpoint_handler.CheckpointScope("ServerSession::_init_session_elements", MODULE_ID, lib.checkpoint.DEBUG, remote_ip=self.session_info.socket_info_remote[0], unique_session_id=self.session_info.unique_session_id):
            try:
                self._auth_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._dialog_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._traffic_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._user_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._cpm_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._tag_socket_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)
                self._endpoint_tunnelendpoint = tunnel_endpoint_base.create_tunnelendpoint_tunnel(self._async_service, self.checkpoint_handler)

                self._com_session.add_tunnelendpoint_tunnel(1, self._auth_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(2, self._dialog_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(3, self._traffic_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(4, self._user_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(5, self._cpm_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(6, self._tag_socket_tunnelendpoint)
                self._com_session.add_tunnelendpoint_tunnel(7, self._endpoint_tunnelendpoint)

                plugins = self._environment.plugin_manager.create_instances(async_service=self._async_service, checkpoint_handler=self.checkpoint_handler, database=self._environment.get_default_database(), management_message_session=self._service_management_message_send, license_handler=self._license_handler, session_info=self.session_info, access_log_server_session=self._access_log_server_session)

                self._user_session = component_user.UserSession(self._environment,
                                                                self._async_service,
                                                                self.checkpoint_handler,
                                                                self._user_tunnelendpoint,
                                                                plugins,
                                                                self._dictionary,
                                                                self._access_log_server_session,
                                                                self._service_management_message_send
                                                                )


                if self._server_config.login_enable_sso:
                    self._user_session.initialize_gss(self._server_config.login_local_gateway_sso,
                                                      self._server_config.login_security_package,
                                                      self._server_config.login_target_spn)

                if self._server_config.login_show_last_login:
                    self._user_session.show_last_login = True
                elif  self._server_config.login_show_last_user_directory:
                    self._user_session.show_last_user_directory = True


                self._cpm_session = component_cpm.CPMSession(self._async_service,
                                                             self.checkpoint_handler,
                                                             self._cpm_tunnelendpoint,
                                                             self._user_session,
                                                             self._server_config.cpm_gpms_abspath,
                                                             self._cpm_download_manager,
                                                             self._server_knownsecret,
                                                             self._server_config,
                                                             self._access_log_server_session)

                self._tag_plugin_socket_session = plugin_socket_tag.PluginSocket(self._async_service,
                                                                                 self.checkpoint_handler,
                                                                                 self._tag_socket_tunnelendpoint,
                                                                                 self._environment.plugin_manager,
                                                                                 plugins,
                                                                                 self._cpm_session)

                self._endpoint_session = component_endpoint.EndpointSession(self._async_service,
                                                                            self.checkpoint_handler,
                                                                            self._endpoint_tunnelendpoint,
                                                                            self._service_management_message_send,
                                                                            self.session_info,
                                                                            self._user_session,
                                                                            self._environment.plugin_manager,
                                                                            plugins,
                                                                            self._dictionary,
                                                                            self._license_handler,
                                                                            )

                self.launch_sessions = components.traffic.server_gateway.server_launch_internal.LaunchSessions(self._cpm_session, self._endpoint_session, self._user_session)

                self._traffic_session = component_traffic.TrafficSession(self._environment,
                                                                         self._async_service,
                                                                         self.checkpoint_handler,
                                                                         self._traffic_tunnelendpoint,
                                                                         plugins,
                                                                         self._user_session,
                                                                         self.launch_sessions,
                                                                         self._tag_plugin_socket_session,
                                                                         self._access_log_server_session,
                                                                         self._service_management_message_send,
                                                                         self._server_config)

                self._dialog_session = component_dialog.DialogSession(self._environment,
                                                                      self._async_service,
                                                                      self.checkpoint_handler,
                                                                      self._dialog_tunnelendpoint,
                                                                      self._user_session,
                                                                      self._server_config,
                                                                      self._cpm_session,
                                                                      self.session_info,
                                                                      self._dictionary,
                                                                      self._service_management_message_send
                                                                      )

                auth_session_options = component_auth.AuthorizationSessionOptions(self._server_config.authorization_always_allow_access,
                                                                                  self._server_config.authorization_timeout_sec,
                                                                                  self._server_config.authorization_access_rule,
                                                                                  self._server_config.authorization_use_tri_state_logic,
                                                                                  self._server_config.authorization_require_full_login)

                self._auth_session = component_auth.AuthorizationSession(self._environment,
                                                                         self._async_service,
                                                                         self.checkpoint_handler,
                                                                         self._auth_tunnelendpoint,
                                                                         self._service_management_message_send,
                                                                         self.session_info,
                                                                         plugins,
                                                                         self,
                                                                         self._user_session,
                                                                         self._endpoint_session,
                                                                         auth_session_options,
                                                                         self._dictionary)

                self._web_app_sesssion = components.web_server.server_gateway.WebAppSession(self._environment,
                                                                                            self._async_service,
                                                                                            self.checkpoint_handler,
                                                                                            self._user_session,
                                                                                            plugins)

                self._auth_session.set_traffic_callback(self._traffic_session)
                self._dialog_session.set_traffic_callback(self._traffic_session)
                self._traffic_session.set_dialog_callback(self._dialog_session)
                self._traffic_session.set_auth_callback(self._auth_session)

                # If connected using ToH the ip info should be corrected before reporting to access log
                if self.http_proxy_server_thread is not None:
                    real_ip_remote = self.http_proxy_server_thread.lookup_gon_client_connect_address(self._com_session.get_ip_remote())
                    if real_ip_remote is not None:
                        self._access_log_server_session.report_access_log_corrected_ip(real_ip_remote[0])

                self._access_log_server_session.report_access_log_start()
                self._sessions_initialized = True
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("ServerSession::_init_session_elements", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
                self.child_close()

    def session_state_ready(self):
        if not (self._server_config.log_session_enabled or self._server_config.log_session_enabled_by_remote and self._com_session.get_remote_session_logging_enabled()):
            if self.session_checkpoint_handler != None:
                self.session_checkpoint_handler.close()
        with self.checkpoint_handler.CheckpointScope("ServerSession::session_state_ready", MODULE_ID, lib.checkpoint.DEBUG, remote_ip=self.session_info.socket_info_remote[0], unique_session_id=self.session_info.unique_session_id, remote_unique_session_id=self._com_session.get_remote_unique_session_id()):
#            import gc; print gc.collect(2)
#            self.checkpoint_handler.CheckpointMultilineMessage("ServerSession::mem", MODULE_ID, lib.checkpoint.INFO,lib.utility.mem_leaks.get_top_as_string(lib.utility.mem_leaks.get_refcounts_class()))
#            import pdb; pdb.set_trace()
            self._init_session_elements(None)
            if self._sessions_initialized:
                self._com_session.read_start_state_ready()
                self._cpm_session.session_start()
                self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0, 0, self, '_session_state_ready_delayed')

    def _session_state_ready_delayed(self):
        if self._sessions_closed:
            return
        try:
            # Wait for cpm_session to connect
            if not self._cpm_session.is_connected():
                if not self.is_closed():
                    self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0,200, self, '_session_state_ready_delayed')
                return

            if self._cpm_session.connect_check_failed():
                self.checkpoint_handler.Checkpoint("ServerSession::_session_state_ready_delayed.connect_check_failed", MODULE_ID, lib.checkpoint.DEBUG)
                self.child_close()
                return

            if not self._cpm_session.connect_check_ok():
                if not self.is_closed():
                    self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0, 200, self, '_session_state_ready_delayed')
                return

            self.checkpoint_handler.Checkpoint("ServerSession::_session_state_ready_delayed.connect_check_ok", MODULE_ID, lib.checkpoint.DEBUG)
            self._dialog_session.session_start()
            self._auth_session.session_start()
            self._traffic_session.session_start()
            self._user_session.session_start()
            self._tag_plugin_socket_session.session_start()
            self._endpoint_session.session_start()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("ServerSession::_session_state_ready_delayed", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self.child_close()

    def session_state_closed(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::session_state_closed", MODULE_ID, lib.checkpoint.DEBUG):
            if not self._sessions_initialized:
                return
            try:
                self._traffic_session.session_close()
                self._dialog_session.session_close()
                self._auth_session.session_close()
                self._user_session.session_close()
                self._endpoint_session.session_close()
                self._cpm_session.session_close()
                self._tag_plugin_socket_session.session_close()

                self.launch_sessions.reset()
                self._access_log_server_session.report_access_log_close()

                if self.session_checkpoint_handler != None:
                    self.session_checkpoint_handler.close()
                    self.session_checkpoint_handler = None

                self._traffic_session = None
                self._dialog_session = None
                self._auth_session = None
                self._user_session = None
                self._endpoint_session = None
                self._cpm_session = None
                self._tag_plugin_socket_session = None

                self.launch_sessions = None
                self._access_log_server_session = None

            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("ServerSession::session_state_closed", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            finally:
                self._sessions_closed = True

    def session_state_key_exchange(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::session_state_key_exchange", MODULE_ID, lib.checkpoint.DEBUG):
            pass

    def session_user_signal(self, signal_id, message):
        with self.checkpoint_handler.CheckpointScope("ServerSession::session_user_signal", MODULE_ID, lib.checkpoint.DEBUG, message=message):
            if signal_id in [components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_VERSION_ERROR, components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_VERSION_ERROR_REMOTE]:
                self.ignore_child_close = True
            elif signal_id in [components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_DISPATCH_ERROR]:
                self.child_close()

    def is_closed(self):
        return self._com_session.is_closed()

    def child_close(self):
        """
        This waiting is a hack.
        We are waiting a little while before we close down, because the client might have send some info to the client(a message to be shown).
        This should be fixed when we have better control over enduser messaging.
        """
        with self.checkpoint_handler.CheckpointScope("ServerSession::child_close", MODULE_ID, lib.checkpoint.DEBUG, ignore_child_close=self.ignore_child_close):
            if not self.ignore_child_close:
                self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0, 2, self, '_child_close_after_a_while')

    def _child_close_after_a_while(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::_child_close_after_a_while", MODULE_ID, lib.checkpoint.DEBUG, ignore_child_close=self.ignore_child_close):
            if not self.ignore_child_close:
                self._com_session.close(False)

    def get_unique_session_id(self):
        return self.session_info.unique_session_id

    def close_decoubled(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::close_decoubled", MODULE_ID, lib.checkpoint.DEBUG):
            self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0, 2, self, '_close')

    def _close(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::_close", MODULE_ID, lib.checkpoint.DEBUG):
            self._com_session.close(False)

    def recalculate_menu(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::recalculate_menu", MODULE_ID, lib.checkpoint.DEBUG):
            self._traffic_session.recalculate_menu()

    # WEB API

    def get_menu(self):
        with self.checkpoint_handler.CheckpointScope("ServerSession::get_menu", MODULE_ID, lib.checkpoint.DEBUG):
            return self._dialog_session.get_current_menu()

    def launch_menu_item(self, launch_id):
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session.get_mutex(), 0, 0, 2, self, 'launch_menu_item_decoubled', launch_id)

    def launch_menu_item_decoubled(self, launch_id):
        with self.checkpoint_handler.CheckpointScope("ServerSession::launch_menu_item_decoubled", MODULE_ID, lib.checkpoint.DEBUG, launch_id=launch_id):
            self._traffic_session.traffic_launch(launch_id)

    def get_web_app_session(self):
        return self._web_app_sesssion

class WebServerSession(object):
    def __init__(self, async_service, environment, server_config, license_handler, dictionary):
        self.checkpoint_handler = environment.checkpoint_handler
        self._server_config = server_config
        self._license_handler = license_handler
        self._dictionary = dictionary

        self._async_service = async_service
        self._access_log_server_session = None

        self._environment = environment

        components.database.server_common.database_api.SchemaFactory.connect(environment.get_default_database())

        with self.checkpoint_handler.CheckpointScope("WebServerSession::_init_session_elements", MODULE_ID, lib.checkpoint.DEBUG):
            try:

                plugins = self._environment.plugin_manager.create_instances(async_service=self._async_service, checkpoint_handler=self.checkpoint_handler, database=self._environment.get_default_database(), management_message_session=None, license_handler=self._license_handler, session_info=None, access_log_server_session=self._access_log_server_session)

                self._user_session = component_user.UserSession(self._environment,
                                                                self._async_service,
                                                                self.checkpoint_handler,
                                                                None,
                                                                plugins,
                                                                self._dictionary,
                                                                self._access_log_server_session,
                                                                None
                                                                )


                self._web_app_sesssion = components.web_server.server_gateway.WebAppSession(self._environment,
                                                                                            self._async_service,
                                                                                            self.checkpoint_handler,
                                                                                            self._user_session,
                                                                                            plugins)

            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("WebServerSession::_init_session_elements", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)


    def get_web_app_session(self):
        return self._web_app_sesssion


class ServerSessionError(components.communication.session.APISessionEventhandlerReady):
    """
    Special session created in error situation to ensure that client is disconnected
    """
    def __init__(self, checkpoint_handler, com_session):
        components.communication.session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self.com_session = com_session
        self.com_session.set_eventhandler_ready(self)

    def session_state_key_exchange(self):
        pass

    def session_state_closed(self):
        pass

    def session_state_ready(self):
        self.com_session.close(True)


class ServerSessionManager(components.communication.session_manager.APISessionManagerEventhandler, components.management_message.server_common.session_client.ManagementServiceRecieveCalllback):
    def __init__(self, async_service, checkpoint_handler, environment, server_config, server_knownsecret, access_log_server, license_handler, dictionary, service_management_message_send, service_name, http_proxy_server_thread):
        components.communication.session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self._async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self._environment = environment
        self._server_knownsecret = server_knownsecret
        self._server_config = server_config
        self._license_handler = license_handler
        self._dictionary = dictionary
        self._service_management_message_send = service_management_message_send
        self.service_name = service_name
        self.http_proxy_server_thread = http_proxy_server_thread
        self._com_session_manager = com_session_manager.APISessionManagerServer_create(self._async_service.get_com_async_service(),
                                                                                       self,
                                                                                       self.checkpoint_handler.checkpoint_handler,
                                                                                       server_knownsecret,
                                                                                       server_config.get_service_ip(),
                                                                                       server_config.get_service_port(),
                                                                                       server_config.get_service_sid())
        self._com_session_manager.set_config_idle_timeout_sec(server_config.session_timeout_min * 60, server_config.session_keyexchange_timeout_sec)
        self._com_session_manager.set_config_session_logging_enabled(server_config.log_session_enabled)
        self._com_session_manager.set_config_security_dos_attack(server_config.security_dos_attack_keyexchange_phase_one_high,
                                                                 server_config.security_dos_attack_keyexchange_phase_one_low,
                                                                 server_config.security_dos_attack_security_closed_high,
                                                                 server_config.security_dos_attack_security_closed_low,
                                                                 server_config.security_dos_attack_ban_attacker_ips)

        #
        # From OpenSSL
        #define SSL_OP_NO_SSLv2       0x01000000L
        #define SSL_OP_NO_SSLv3       0x02000000L
        #define SSL_OP_NO_TLSv1       0x04000000L
        #define SSL_OP_NO_TLSv1_2     0x08000000L
        #define SSL_OP_NO_TLSv1_1     0x10000000L
        #
        ssl_options = 0
        if server_config.ssl_as_client_op_no_sslv2:
            ssl_options |= 0x01000000
        if server_config.ssl_as_client_op_no_sslv3:
            ssl_options |= 0x02000000
        if server_config.ssl_as_client_op_no_tlsv1:
            ssl_options |= 0x04000000
        if server_config.ssl_as_client_op_no_tlsv1_1:
            ssl_options |= 0x08000000
        if server_config.ssl_as_client_op_no_tlsv1_2:
            ssl_options |= 0x10000000
        self._com_session_manager.set_config_https(server_config.ssl_as_client_certificate_path,
                                                   server_config.ssl_as_client_certificate_verification_enabled,
                                                   server_config.ssl_as_client_hostname_verification_enabled,
                                                   server_config.ssl_as_client_sni_enabled,
                                                   server_config.ssl_as_client_method,
                                                   ssl_options)


        self._cpm_download_manager = component_cpm_download_manager.DownloadManager(self._async_service, self._com_session_manager.get_mutex(), self.checkpoint_handler, server_config.cpm_concurent_downloads)
        self._sessions = {}
        self._access_log_server = access_log_server
        self._restart_service_when_no_connections = False
        self._stop_service_when_no_connections = False

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_connecting(self, connection_id, connection_title):
        pass

    def session_manager_connecting_failed(self, connection_id):
        pass

    def session_manager_failed(self, message):
        pass

    def session_manager_session_created(self, connection_id, com_session):
        session_id = com_session.get_session_id()
        self.checkpoint_handler.Checkpoint("session_manager.session_created", MODULE_ID, lib.checkpoint.INFO, session_id=session_id, unique_session_id=com_session.get_unique_session_id(), remote_ip=com_session.get_ip_remote()[0])
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager::session_manager_session_created", MODULE_ID, lib.checkpoint.DEBUG, session_id=session_id):
            try:
                access_log_server_session = self._access_log_server.create_access_log_session(com_session.get_unique_session_id(), com_session.get_ip_remote()[0])
                self._sessions[session_id] = ServerSession(access_log_server_session, self._async_service, com_session, self._environment, self._server_config, self._cpm_download_manager, self._server_knownsecret, self._license_handler, self._dictionary, self._service_management_message_send, self.http_proxy_server_thread)
            except:
                self._sessions[session_id] = ServerSessionError(self.checkpoint_handler, com_session)
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("ServerSessionManager.session_manager_session_created.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

    def session_manager_session_closed(self, session_id):
        self.checkpoint_handler.Checkpoint("session_manager.session_closed", MODULE_ID, lib.checkpoint.INFO, session_id=session_id)
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager::session_manager_session_closed", MODULE_ID, lib.checkpoint.DEBUG, session_id=session_id, session_count=len(self._sessions)):
            if session_id in self._sessions.keys():
                del self._sessions[session_id]
            if len(self._sessions) == 0:
                if self._restart_service_when_no_connections:
                    self._do_management_service_restart()
                if self._stop_service_when_no_connections:
                    self._do_management_service_stop()

    def session_manager_closed(self):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager::session_manager_closed", MODULE_ID, lib.checkpoint.DEBUG):
            self._access_log_server.report_access_log_traffic_stop_server()

    def session_manager_security_dos_attack_start(self, attacker_ips):
        ts = datetime.datetime.now()
        source = "Security, DoS attack"
        code = "001-start"
        summary = "DoS attack started"
        details  = "The gateway server has detected what appears to be a DoS attack and has entered a protective stage. The gateway server might be unavailable during the attack, but should be available again after the attack has finished\n\n"
        details += "The attack appears to originate from one or more of the IP-addresses: %s." % attacker_ips
        self._access_log_server.report_access_log_gateway_critical(ts, source, code, summary, details)

    def session_manager_security_dos_attack_stop(self, attacker_ips, banned_connection_count, failed_keyexchange_count):
        ts = datetime.datetime.now()
        source = "Security, DoS attack"
        code = "001-stop"
        summary = "DoS attack stopped"
        details  = "The gateway server is resuming normal operation following an apparent DoS attack.\n\n"
        details += "The attack appeared to originate from one or more of the IP-addresses: %s.\n" % attacker_ips
        if banned_connection_count > 0:
            details += "The server banned %d connection(s) during the attack\n\n" % banned_connection_count
        if failed_keyexchange_count > 0:
            details += "During the attack %d connections failed during key exchange\n" % failed_keyexchange_count
        self._access_log_server.report_access_log_gateway_info(ts, source, code, summary, details)

    def is_closed(self):
        return self._com_session_manager.is_closed()

    def has_started(self):
        return True

    def is_running(self):
        return not self.is_closed()

    def start(self):
        try:
            self._access_log_server.report_access_log_traffic_start_server()
            self._com_session_manager.start()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("ServerSessionManager.start.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)

    def stop(self):
        try:
            self._com_session_manager.close_start()
            while not self.is_closed():
                time.sleep(0.5)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("ServerSessionManager.stop.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)

    def join(self):
        pass

    def management_service_stop(self, when_no_sessions):
        self.checkpoint_handler.Checkpoint("ServerSessionManager.management_service_stop", MODULE_ID, lib.checkpoint.INFO)
        if when_no_sessions and len(self._sessions) > 0:
            self._com_session_manager.accept_connections_stop()
            self._restart_service_when_no_connections = False
            self._stop_service_when_no_connections = True
        else:
            self._do_management_service_stop()

    def get_session_count(self):
        return len(self._sessions)

    def _do_management_service_stop(self):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager._do_management_service_stop", MODULE_ID, lib.checkpoint.DEBUG):
            service = lib.appl.service.GService.create_from_service_name(self.service_name)
            if service is not None:
                service.stop_and_forget()
            else:
                self.checkpoint_handler.Checkpoint("ServerSessionManager._do_management_service_stop.no_service_found", MODULE_ID, lib.checkpoint.WARNING, service_name=self.service_name)

    def management_service_restart(self, when_no_sessions):
        self.checkpoint_handler.Checkpoint("ServerSessionManager.management_service_restart", MODULE_ID, lib.checkpoint.INFO)
        if when_no_sessions and len(self._sessions) > 0:
            self._com_session_manager.accept_connections_stop()
            self._restart_service_when_no_connections = True
            self._stop_service_when_no_connections = False
        else:
            self._do_management_service_restart()

    def _do_management_service_restart(self):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager._do_management_service_restart", MODULE_ID, lib.checkpoint.DEBUG):
            service = lib.appl.service.GService.create_from_service_name(self.service_name)
            if service is not None:
                service.restart_and_forget()
            else:
                self.checkpoint_handler.Checkpoint("ServerSessionManager._do_management_service_restart.no_service_found", MODULE_ID, lib.checkpoint.WARNING, service_name=self.service_name)

        self._restart_service_when_no_connections = False
        self._stop_service_when_no_connections = False


    def management_service_session_close(self, unique_session_id):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager.management_service_session_close", MODULE_ID, lib.checkpoint.DEBUG):
            session = self._lookup_session(unique_session_id)
            if session is not None:
                session.close_decoubled()

    def management_service_session_recalculate_menu(self, unique_session_id):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager.management_service_session_recalculate_menu", MODULE_ID, lib.checkpoint.DEBUG):
            session = self._lookup_session(unique_session_id)
            if session is not None:
                session.recalculate_menu()

    def management_service_accept_sessions(self, do_accept):
        with self.checkpoint_handler.CheckpointScope("ServerSessionManager.management_service_accept_sessions", MODULE_ID, lib.checkpoint.DEBUG, do_accept=do_accept):
            if do_accept:
                self._com_session_manager.accept_connections_start()
            else:
                self._com_session_manager.accept_connections_stop()

    def _lookup_session(self, unique_session_id):
        for session in self._sessions.values():
            if session.get_unique_session_id() == unique_session_id:
                return session
        self.checkpoint_handler.Checkpoint("ServerSessionManager._lookup_session.not_found", MODULE_ID, lib.checkpoint.WARNING, unique_session_id=unique_session_id)
        return None

    def get_first_session_id(self):
        if self._sessions:
            return self._sessions[0].get_unique_session_id()
        return None

    def get_session(self, unique_session_id):
        return self._lookup_session(unique_session_id)

    def get_first_session(self):
        if self._sessions:
            return self._sessions.values()[0]
        return None

    def server_sid_changed(self, old_server_sid, new_server_sid):
        self.checkpoint_handler.Checkpoint("server_sid_changed", MODULE_ID, lib.checkpoint.INFO, old_server_sid=old_server_sid, new_server_sid=new_server_sid)
        self._server_config.set_server_sid(new_server_sid)
        session_ids = [session.get_unique_session_id() for session in self._sessions.values()]
        if session_ids:
            self._access_log_server.report_server_sid_changed(session_ids, old_server_sid, new_server_sid)

    def get_server_sid(self):
        return self._server_config.get_service_sid()

    def create_web_server_session(self):
        session = WebServerSession(self._async_service, self._environment, self._server_config, self._license_handler, self._dictionary)
        return session




class ApplicationCommunicationThread(threading.Thread):
    def __init__(self, checkpoint_handler):
        threading.Thread.__init__(self, name="ApplicationCommunication")
        self.checkpoint_handler = checkpoint_handler
        self._running = False

    def run(self):
        with self.checkpoint_handler.CheckpointScope("ApplicationCommunicationThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                components.traffic.common.selector.start(1.0)
                while self._running:
                    time.sleep(1)

                components.traffic.common.selector.stop()
                components.traffic.common.selector.join()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("ApplicationCommunication.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self._running = False

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("ApplicationCommunicationThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def is_running(self):
        return self._running

    def has_started(self):
        return self._running




class HTTPProxyServerThread(threading.Thread):
    class HTTPProxyServerEnvironment:
        class config:
            MIN_WAITING_HTTP_CONNECTIONS = 1 # request more connections if less than this
            MAX_WAITING_HTTP_CONNECTIONS = 4 # if more than this number of connections then get rid of them
            IDLE_TIMEOUT = 60 # how often to wake up when there is nothing to do
            PING_INTERVAL = 30 # server must send ping back at least this often
            ACK_TIMEOUT = 1.0 # pending acks will be sent after this long
            RETRANS_TIMEOUT = 1.0 # unacked packages will be retransmitted after this long
            MAX_RETRANS = 20 # after retransmitting this many times the connection fails
            PERSISTENT_HTTP = True # attempt persistent connections - squid supports it from client to proxy
            HTTP_RECV_BUFFER = 16384
            TCP_RECV_BUFFER = 16384
            LISTEN_BACKLOG = 100 # how many TCP (HTTP) connections can be waiting, probably max 128
            MAX_RECEIVE_HEADER_LENGTH = 4096 # maximum received HTTP header length in bytes
            MAX_RECEIVE_CONTENT_LENGTH = 100000 # maximum received HTTP content length in bytes
            MAX_SEND_CONTENT_LENGTH = 50000 # soft limit maximum sent HTTP content length in bytes - content can however be as big as whole message
            MAX_OUT_OF_ORDER = 50 # maximum number of premature packages a TcpConnection will store
            MAX_UNACK = 20 # maximum number of unacked packages a TcpConnection will store before it throttles
            MAX_SESSIONS = 100
            MAX_TCP_PER_SESSION = 100 # maximum number of TcpConnections per Session
            MAX_IDLE_TCP = 50 # how long should a TcpConnection be allowed to be idle (perhaps because other end is dead)
            MAX_SESSION_SILENT = 60 # how long should a Session accept not to hear from client, should be longer than clients PING_INTERVAL
            MAX_IDLE_HTTP = 360 # how long should a HttpConnection be allowed to be idle (perhaps because other end is dead)
            MAX_ASYNC = 500 # windows select() has a hardcoded limit of 512 ...
            PACKAGE_DROP_INTERVAL = 0 # DESTRUCTIVE TESTING ONLY!

        out_symbol = '<'
        in_symbol = '>'

    MODULE_ID = 'ToH'

    def __init__(self, checkpoint_handler, server_config):
        threading.Thread.__init__(self, name="HTTPProxyServerThread")
        self.checkpoint_handler = checkpoint_handler
        self._running = False
        self._has_started = False
        self._toh = lib.ToHserver.main()
        self.server_config = server_config

        self.http_addr = (server_config.service_http_ip, server_config.service_http_port)
        if server_config.get_service_ip() != '0.0.0.0':
            self.target_addr = (server_config.get_service_ip(), server_config.get_service_port())
        else:
            self.target_addr = ('127.0.0.1', server_config.service_port)

        self.HTTPProxyServerEnvironmentInstance = HTTPProxyServerThread.HTTPProxyServerEnvironment
        self.HTTPProxyServerEnvironmentInstance.log1 = self.log1
        self.HTTPProxyServerEnvironmentInstance.log2 = self.log2
        self.HTTPProxyServerEnvironmentInstance.log3 = self.log3

    def log1(self, s, *args):
        self.checkpoint_handler.Checkpoint("HTTPProxyServerThread.log_1", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def log2(self, s, *args):
        if self.server_config.service_http_log_2_enabled:
            self.checkpoint_handler.Checkpoint("HTTPProxyServerThread.log_2", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def log3(self, s, *args):
        if self.server_config.service_http_log_3_enabled:
            self.checkpoint_handler.Checkpoint("HTTPProxyServerThread.log_3", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def run(self):
        with self.checkpoint_handler.CheckpointScope("HTTPProxyServerThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                self._has_started = True
                self._toh.run(HTTPProxyServerThread.HTTPProxyServerEnvironment, self.http_addr, self.target_addr)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("HTTPProxyServerThread.run.error", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

            try:
                self._toh.stop()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("HTTPProxyServerThread.run.stop.error", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

            self._running = False

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("HTTPProxyServerThread.stop", HTTPProxyServerThread.MODULE_ID, lib.checkpoint.DEBUG):
            if self._running:
                self._toh.stop()

    def is_running(self):
        return self._running

    def has_started(self):
        return self._has_started

    def lookup_gon_client_connect_address(self, lookup_connect_info):
        if self.is_running():
            return self._toh.lookup_gon_client_connect_address(lookup_connect_info)
        return None



class AliveThread(threading.Thread):
    def __init__(self, checkpoint_handler, service_stop_signal):
        threading.Thread.__init__(self, name="AliveThread")
        self.checkpoint_handler = checkpoint_handler
        self._running = False
        self._threads_to_watch = []
        self._service_stop_signal = service_stop_signal

    def run(self):
        with self.checkpoint_handler.CheckpointScope("AliveThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True

                # Wait for all threads to report that they are running
                all_threads_started = False
                while not all_threads_started:
                    all_threads_started = True
                    for thread_to_watch in self._threads_to_watch:
                        all_threads_started = all_threads_started and thread_to_watch.has_started()

                # Watch running threads
                while self._running and not self._service_stop_signal:
                    for thread_to_watch in self._threads_to_watch:
                        if not thread_to_watch.is_running():
                            self.stop()
                    time.sleep(2)

                # Stop all threads
                self._running = False
                for thread_to_watch in self._threads_to_watch:
                    thread_to_watch.stop()

                # Whit for all threads to stop
                for thread_to_watch in self._threads_to_watch:
                    thread_to_watch.join()

            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("AliveThread.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("AliveThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def add_thread_to_watch(self, thread):
        self._threads_to_watch.append(thread)



class CpInFolderJanitorCallback(lib.appl.cp_in_folder_janitor.CpInFolderJanitorCallback):
    """
    Called when a checkpoint file is found
    """
    def __init__(self, checkpoint_handler, folder_to_watch, access_log_server):
        self.checkpoint_handler = checkpoint_handler
        self.folder_to_watch = folder_to_watch
        self.access_log_server = access_log_server

    def handle_cp(self, cp_filename_abs, cp_file):
        self.access_log_server.report_access_log_gateway_critical(cp_file.get_ts(), cp_file.get_source(), 0, cp_file.get_summary(), cp_file.get_details())
        cp_file.remove()



# The async_service is a global instance because it contains a async-io object
# that should be deleted at the very end.
async_service = None


def main(service_stop_signal=[], service_stopped_signal=[], service_name=None, args=None, instance_run_root=None):
    """
    Main function of G/ON Gateway Server
    """
#    import gc; gc.set_debug(gc.DEBUG_LEAK)
    if instance_run_root is None:
        if hasattr(sys, "frozen"):
            instance_run_root = os.path.dirname(sys.executable)
        else:
            instance_run_root = os.getcwd()
    server_config = ServerGatewayOptions(instance_run_root, args)

    #
    # Show version and exit if requested
    #
    if(server_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0


    #
    # Initialize logging
    #
    try:
        log_absfilename = server_config.log_absfile
        if server_config.log_rotate and os.path.exists(log_absfilename):
            shutil.move(log_absfilename, lib.checkpoint.generate_rotate_filename(log_absfilename))
    except:
        log_absfilename = server_config.log_absfile + ("_%i.log" % os.getpid())

    checkpoint_handler_in_absfolder = lib.checkpoint.CheckpointHandler.get_handler_to_xml_folder(server_config.log_in_folder_enabled, server_config.log_in_folder_abspath)
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(server_config.log_enabled,
                                                                      server_config.log_verbose,
                                                                      log_absfilename,
                                                                      server_config.is_log_type_xml(),
                                                                      checkpoint_handler_parent=checkpoint_handler_in_absfolder)
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                  service_name=service_name
                                  )

    checkpoint_handler.Checkpoint("ssl_version", MODULE_ID, lib.checkpoint.INFO,
                                  cpp_ssl_version=components.communication.get_ssl_version(),
                                  py_ssl_version=ssl.OPENSSL_VERSION
                                  )

    #
    # Initialize communication component (and crypt lib)
    #
    components.communication.async_service.init(checkpoint_handler)


    #
    # Load license file, and add license issues in log-file
    # If invalid license the stop server
    #
    license_handler = server_config.get_license_handler()
    license = license_handler.get_license()
    if (not license.valid):
        checkpoint_handler.Checkpoint("license.invalid", MODULE_ID, lib.checkpoint.CRITICAL)
        service_stopped_signal.append(7)
        return -1
    if len(license.errors) > 0:
        checkpoint_handler.CheckpointMultilineMessages("license.error", MODULE_ID, lib.checkpoint.ERROR, license.errors)
    if len(license.warnings) > 0:
        checkpoint_handler.CheckpointMultilineMessages("license.warning", MODULE_ID, lib.checkpoint.WARNING, license.warnings)

    dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, server_config.dictionary_abspath)
    try:
        environment = components.environment.Environment(checkpoint_handler, server_config.get_db_connect_url(), MODULE_ID, server_config.db_encoding, num_of_threads=server_config.service_num_of_threads, db_logging=server_config.db_log_enabled)
        environment.checkpoint_handler = checkpoint_handler
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        service_stopped_signal.append(7)
        return -1

    checkpoint_handler.Checkpoint("db", MODULE_ID, lib.checkpoint.INFO, db_connect_string=server_config.get_db_connect_info() )
    checkpoint_handler.Checkpoint("server", MODULE_ID, lib.checkpoint.INFO,
                                  gateway_server_sid=server_config.get_service_sid(),
                                  service_title=server_config.get_service_title(),
                                  service_ip=server_config.get_service_ip(),
                                  service_port=server_config.get_service_port())


    #
    # Get known secret
    #
    server_knownsecret = server_config.get_server_knownsecret(checkpoint_handler)
    if server_knownsecret == None:
        service_stopped_signal.append(7)
        return 1

    access_log_server = None

    try:
        #
        # Load plugins
        #
        environment.plugin_manager = components.plugin.server_gateway.manager.Manager(environment.checkpoint_handler, server_config.plugin_modules_abspath)

        #
        # Initialize async service
        #
        global async_service
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, server_config.service_num_of_threads)

        #
        # Initialize management service connection
        #
        service_management_connect_ip = server_config.service_management_connect_ip
        service_management_connect_port = server_config.service_management_connect_port
        service_management_client_knownsecret = server_config. get_service_management_client_knownsecret(checkpoint_handler)
        service_management_gateway_sid = server_config.get_service_sid()
        service_management_gateway_title = server_config.get_service_title()
        service_management_gateway_listen_ip = server_config.get_service_ip()
        service_management_gateway_listen_port = server_config.get_service_port()
        service_management_filedist_enabled = server_config.get_service_management_filedist_enabled()
        service_management_filedist_abspath = server_config.service_management_filedist_abspath
        service_management_message_queue_offline_absfilename = server_config.service_management_message_queue_offline_absfilename
        server_gateway_root = server_config.get_instance_run_root()
        service_management = components.management_message.server_common.session_client.ManagementMessageClientSessionManager(async_service, checkpoint_handler, dictionary, license_handler, service_management_connect_ip, service_management_connect_port, service_management_client_knownsecret,
                                                                                                                              service_management_gateway_sid, service_management_gateway_title, service_management_gateway_listen_ip, service_management_gateway_listen_port,
                                                                                                                              service_management_filedist_enabled, service_management_filedist_abspath, service_management_message_queue_offline_absfilename, server_gateway_root)
        service_management_message_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSend(checkpoint_handler, service_management)
        access_log_server = components.access_log.server_gateway.access_log.AccessLogServer(service_management_message_send, server_config)

        #
        #
        #
        cp_in_folder_janitor = None
        if server_config.log_in_folder_enabled:
            folder_to_watch = server_config.log_in_folder_abspath
            cp_in_folder_janitor_callback = CpInFolderJanitorCallback(checkpoint_handler, folder_to_watch, access_log_server)
            cp_in_folder_janitor = lib.appl.cp_in_folder_janitor.CPInFolderJanitor(async_service, checkpoint_handler, folder_to_watch, cp_in_folder_janitor_callback)
            cp_in_folder_janitor.start()

        #
        # Start http proxy
        #
        http_proxy_server_thread = HTTPProxyServerThread(checkpoint_handler, server_config)
        http_proxy_server_enabled = server_config.service_http_enabled and license_handler.get_license().has('Feature', 'HTTP Encapsulation')
        if http_proxy_server_enabled:
            http_proxy_server_thread.start()

        #
        # Initialize session manager
        #
        session_manager = ServerSessionManager(async_service, checkpoint_handler, environment, server_config, server_knownsecret, access_log_server, license_handler, dictionary, service_management_message_send, service_name, http_proxy_server_thread)
        service_management.set_service_recieve_callback(session_manager)

        #
        # Start async service
        #
        service_management.start()
        session_manager.start()
        async_service.start()

        #
        # Start application communication thread
        #
        application_communication_thread = ApplicationCommunicationThread(checkpoint_handler)
        application_communication_thread.start()


        #
        # Start web server
        #
        if server_config.web_server_enable:
            web_server_thread = components.web_server.server_gateway.WebServer(checkpoint_handler, session_manager, server_config.web_server_host, server_config.web_server_port, server_config.web_server_www_absroot, server_config.image_abspath)
            web_server_thread.start()


        #
        # Start alive thread
        #
        alive_thread = AliveThread(checkpoint_handler, service_stop_signal)
        alive_thread.add_thread_to_watch(session_manager)
        alive_thread.add_thread_to_watch(async_service)
        alive_thread.add_thread_to_watch(application_communication_thread)
        if http_proxy_server_enabled:
            alive_thread.add_thread_to_watch(http_proxy_server_thread)
        if server_config.web_server_enable:
            alive_thread.add_thread_to_watch(web_server_thread)
        alive_thread.start()

        #service_management_servers
        # Suspending main thread until main server thread terminates
        #
        alive_thread.join()


        #
        #
        #
        if server_config.log_in_folder_enabled:
            cp_in_folder_janitor.stop_and_wait()


    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)


    #
    # List thread status
    #
    for t in threading.enumerate():
        checkpoint_handler.Checkpoint("main.threadinfo", MODULE_ID, lib.checkpoint.DEBUG, thread_name=t.getName(), thread_is_alive=t.isAlive())

    #
    # Signale the service handler that this program has ended
    #
    service_stopped_signal.append(7)

    checkpoint_handler.Checkpoint("stopped", MODULE_ID, lib.checkpoint.INFO)


if __name__ == '__main__':
    print 'This file is not intended to be run directly from the commandline,'
    print 'please use service file.'

    service_stop_signal=[]
    service_stopped_signal=[]
    main(service_stop_signal, service_stopped_signal)
