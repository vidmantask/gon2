import lib.dev_env.path_setup
from distutils.core import setup
import py2exe
import lib.appl_plugin_module_util

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()


import sqlalchemy.databases


#
# Get plugin module as datafiles
#
data_files = []
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ad', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'access_log', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'client_ok', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ldap', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'local_win_user', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_mac', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'mobile', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'ip_address', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'login_interval', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint_security', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'dme', 'server', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'vacation_app', 'server', 'gateway'))


data_files.append(('', ['../../setup/dlls/win/libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                        '../../setup/dlls/win/msvcr90.dll',
                        '../../setup/dlls/win/msvcp90.dll',
                        '../../setup/dlls/win/msvcm90.dll',
                        '../../setup/dlls/win/Microsoft.VC90.CRT.manifest']))

#
#call: python setup.py py2exe
#
setup(
      service = [ lib.appl_plugin_module_util.Py2ExeTargetService(
                          script='gon_server_gateway_service.py',
                          uac = True,
                          branding = lib.appl_plugin_module_util.Py2ExeTarget.BRANDING_NORMAL,
                          name = 'G/On Gateway Server',
                          description = 'G/On Gateway Server',
                          image_root='../../components/presentation/gui/mfc/images'
                        )
               ],
      options = {
            "py2exe": {
                  "dll_excludes": [
                    'w9xpopen.exe',
                    'MSVCP90.dll',
                    'libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                    'api-ms-win-core-string-l1-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-0.dll',
                    'api-ms-win-core-libraryloader-l1-2-1.dll',
                    'api-ms-win-core-atoms-l1-1-0.dll',
                    'api-ms-win-core-winrt-error-l1-1-1.dll',
                    'api-ms-win-core-sidebyside-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-3-0.dll',
                    'api-ms-win-core-heap-l1-2-0.dll',
                    'api-ms-win-core-heap-l2-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-1.dll',
                    'api-ms-win-core-libraryloader-l1-2-0.dll',
                    'api-ms-win-core-rtlsupport-l1-2-0.dll',
                    'api-ms-win-core-shlwapi-obsolete-l1-2-0.dll',
                    'api-ms-win-security-base-l1-2-0.dll',
                    'api-ms-win-core-synch-l1-2-0.dll',
                    'api-ms-win-core-handle-l1-1-0.dll',
                    'api-ms-win-core-registry-l1-1-0.dll',
                    'api-ms-win-core-synch-l1-1-0.dll',
                    'api-ms-win-core-localization-l1-2-0.dll',
                    'api-ms-win-core-profile-l1-1-0.dll',
                    'api-ms-win-core-sysinfo-l1-1-0.dll',
                    'api-ms-win-core-errorhandling-l1-1-0.dll',
                    'api-ms-win-core-file-l1-1-0.dll',
                    'api-ms-win-core-timezone-l1-1-0.dll',
                    'api-ms-win-core-processenvironment-l1-1-0.dll',
                    'api-ms-win-security-base-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-2-0.dll',
                    'api-ms-win-core-string-obsolete-l1-1-0.dll',
                    'api-ms-win-crt-private-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-1.dll',
                    'api-ms-win-crt-string-l1-1-0.dll',
                    'api-ms-win-crt-runtime-l1-1-0.dll',
                    'api-ms-win-core-heap-l1-1-0.dll',
                    'api-ms-win-core-interlocked-l1-1-0.dll',
                    'api-ms-win-core-debug-l1-1-0.dll'
                    ],
                  "dist_dir" :  dev_env.generate_pack_destination('gon_server_gateway_service'),
                  "packages":[
                     'logging',
                     'sqlalchemy.dialects.sqlite',
                     'sqlalchemy.dialects.mssql',
                     'sqlalchemy.dialects.mysql',
                     'pywintypes',
                     'win32security',
                     'win32api',
                     'win32net',
                     'win32com.client',
                     'win32com.adsi.adsicon',
                     'win32netcon',
                     'lib.cryptfacility',
                     'lib.smartcard.msc_pkcs15',
                     'lib.smartcard.pcsc_pkcs15',
                     'elementtree',
                     '_ssl',
                     'ldap',
                     'ldap.filter',
                     'lib.hardware.windows_security_center',
                     'IPy',
                     'pyodbc',
                     'MySQLdb',

                             ],
                  "includes":[],
                  "excludes":[
                              'plugin_modules'
                             ],
                  "bundle_files" : 3,
                  "compressed" : True,
                  "optimize" : 0,
            }
      },
      data_files = data_files,
      zipfile = None,
)
