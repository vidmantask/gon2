"""
The G/On Gateway Server runned as a daemon on linux
"""
import gon_server_gateway_main

import lib.service.service_linux

if __name__ == '__main__':
    service_commandline = lib.service.service_linux.ServiceCommandline()
    service = lib.service.service_linux.Service(service_commandline, gon_server_gateway_main.main)
    service.action()
