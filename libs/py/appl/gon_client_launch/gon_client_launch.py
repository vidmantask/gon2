"""
The G/On Client Launcher
"""
from __future__ import with_statement
import warnings
warnings.filterwarnings("ignore")

MODULE_ID = "gon_client_launch"

import sys
import os.path
import threading
import subprocess
import time
import traceback

class GOnClientLaunch(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self, name='GOnClientLaunch')
        self.daemon = True

    def run(self):
        try:
            gon_client_root_relative = None
            gon_client_filename = None
            if sys.platform == 'win32':
                gon_client_root_relative = os.path.join('gon_client', 'win')
                gon_client_filename = 'gon_client.exe'

            if gon_client_root_relative is not None and gon_client_filename is not None:
                gon_client_filename_abs = os.path.join(os.path.abspath(gon_client_root_relative), gon_client_filename)
                command = []
                command.append(gon_client_filename_abs)
                result = subprocess.call(command)
        except:
            log_filename = 'gon_client_launch.log'
            log_file = open(log_filename, 'w')
            traceback.print_exc(file=log_file)
            log_file.close()

def main():
    gon_client_launch = GOnClientLaunch()
    gon_client_launch.start()
    time.sleep(2)

if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    main()
