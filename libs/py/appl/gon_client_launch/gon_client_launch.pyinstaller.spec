# -*- mode: python -*-
import lib.dev_env.path_setup
from distutils.core import setup

import sys
import lib.appl_plugin_module_util

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()

import lib.dev_env.pyinstaller

data_files = []

#
# PyInstaller configuration
#
app_py = 'gon_client_launch.py'
app_name = 'gon_client_launch'
app_description = "G/On Client Launch"
app_version_file = lib.dev_env.pyinstaller.generate_version_file(app_py, app_name, app_description)
app_icon_file = "..\\..\\components\\presentation\\gui\\mfc\\images\\giritech.ico"
app_options = []

datas = []
for (data_file_dest_folder, data_file_src_files) in data_files:
  for data_file_src_file in data_file_src_files:
    datas.append( (data_file_src_file, data_file_dest_folder))

hiddenimports = [
  'plugin_modules.endpoint.client.gateway_deploy',
]



#
# PyInstaller generate folder app
#
a = Analysis(
  [app_py],
  pathex=['.'],
  binaries=[],
  datas=datas,
  hiddenimports=hiddenimports,
  hookspath=[],
  runtime_hooks=[],
  excludes=[],
  win_no_prefer_redirects=False,
  win_private_assemblies=False,
  cipher=None
)

pyz = PYZ(
  a.pure,
  a.zipped_data,
  cipher=None
)

exe = EXE(
  pyz,
  a.scripts,
  options=app_options,
  exclude_binaries=True,
  name=app_name,
  debug=False,
  strip=False,
  upx=True,
  console=False,
  version=app_version_file,
  icon=app_icon_file
)

coll = COLLECT(
  exe,
  a.binaries,
  a.zipfiles,
  a.datas,
  strip=False,
  upx=True,
  name=''
)
