import cx_Freeze
import lib.appl_plugin_module_util
import shutil

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()


import os
print "PATH PATH", os.getcwd()

py_root = os.path.abspath(os.path.join('..', '..'))

extra_files = [
  ('gon_client.ini', '../../../setup/release/gon_client.ini'),
  ('images/giritech.bmp', '../../components/presentation/gui/gtk/images/giritech.bmp'),
  ('images/giritech.ico', '../../components/presentation/gui/gtk/images/giritech.ico'),
  ('images/GoForward.bmp', '../../components/presentation/gui/gtk/images/GoForward.bmp'),
  ('images/GoForwardGray.bmp', '../../components/presentation/gui/gtk/images/GoForwardGray.bmp'),
  ('images/Package.bmp', '../../components/presentation/gui/gtk/images/Package.bmp'),
  ('images/giritech_48x48.png', '../../components/presentation/gui/gtk/images/giritech_48x48.png'),
  ('images/g_login_banner.bmp', '../../components/presentation/gui/gtk/images/g_login_banner.bmp'),

  ('images/g_enroll_banner.bmp', '../../components/presentation/gui/gtk/images/g_enroll_banner.bmp'),
  ('images/g_install_banner.bmp', '../../components/presentation/gui/gtk/images/g_install_banner.bmp'),
  ('images/g_remove_banner.bmp', '../../components/presentation/gui/gtk/images/g_remove_banner.bmp'),
  ('images/g_update_banner.bmp', '../../components/presentation/gui/gtk/images/g_update_banner.bmp'),

  ('images/g_arrow_red.bmp', '../../components/presentation/gui/gtk/images/g_arrow_red.bmp'),
  ('images/g_arrow_grey.bmp', '../../components/presentation/gui/gtk/images/g_arrow_grey.bmp'),

  ('images/giritech_32x32.bmp', '../../components/presentation/gui/gtk/images/giritech_32x32.bmp'),
  ('images/g_smart_card_32x32.bmp', '../../components/presentation/gui/gtk/images/g_smart_card_32x32.bmp'),
  ('images/g_smart_card_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_smart_card_marked_32x32.bmp'),
  ('images/g_micro_smart_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_32x32.bmp'),
  ('images/g_micro_smart_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_marked_32x32.bmp'),
  ('images/g_micro_smart_swissbit_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_32x32.bmp'),
  ('images/g_micro_smart_swissbit_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_marked_32x32.bmp'),
  ('images/g_micro_smart_swissbit_2_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_2_32x32.bmp'),
  ('images/g_micro_smart_swissbit_2_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_2_marked_32x32.bmp'),
  ('images/g_micro_smart_swissbit_pe_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_pe_32x32.bmp'),
  ('images/g_micro_smart_swissbit_pe_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_micro_smart_swissbit_pe_marked_32x32.bmp'),
  ('images/g_endpoint_32x32.bmp', '../../components/presentation/gui/gtk/images/g_endpoint_32x32.bmp'),
  ('images/g_endpoint_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_endpoint_marked_32x32.bmp'),
  ('images/g_soft_token_32x32.bmp', '../../components/presentation/gui/gtk/images/g_soft_token_32x32.bmp'),
  ('images/g_soft_token_marked_32x32.bmp', '../../components/presentation/gui/gtk/images/g_soft_token_marked_32x32.bmp'),

  ('images/g_package_default_32x32.bmp', '../../components/presentation/gui/gtk/images/g_package_default_32x32.bmp'),
  ('images/g_package_linux_32x32.bmp', '../../components/presentation/gui/gtk/images/g_package_linux_32x32.bmp'),
  ('images/g_package_mac_32x32.bmp', '../../components/presentation/gui/gtk/images/g_package_mac_32x32.bmp'),
  ('images/g_package_win_32x32.bmp', '../../components/presentation/gui/gtk/images/g_package_win_32x32.bmp'),

  ('../shortcut/G-On Linux.com', '../../../setup/release/client_shortcuts/G-On Linux.com'),
]


extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'soft_token', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'micro_smart', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'micro_smart_swissbit', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'micro_smart_swissbit_2', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'micro_smart_swissbit_pe', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'client_ok', 'client', 'gateway'))
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'endpoint', 'client', 'gateway'))

# Mobile is included to fix the authorization timeout (plugin not respoding).
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'mobile', 'client', 'gateway'))

# DME is included to fix the authorization timeout (plugin not respoding).
extra_files.extend(lib.appl_plugin_module_util.module_to_data_files_cxfreeze(py_root, 'dme', 'client', 'gateway'))



#
# Additional so-files for additional linux support
#
extra_files.extend([
#  ('libssl.so.10', '/usr/lib/libssl.so.10'),
#  ('libcrypto.so.8', '/usr/lib/libcrypto.so.8'),
#  ('libpyglib-2.0.so.0', '/usr/lib/libpyglib-2.0.so.0'),
#  ('libffi.so.5', '/usr/lib/libffi.so.5'),                 # Added for suselinux support
#  ('libodbc.so.2', '/usr/lib/libodbc.so.2'),               # Added for fedora 14 support
])
extra_files = lib.appl_plugin_module_util.switch_destination_and_source(extra_files)

exclude_modules = [
  'lib_cpp.communication.ext_mac.communication_ext',
  'lib_cpp.communication.ext_linux.communication_ext',
]

include_modules = [
  'lib_cpp.swissbit.ext_linux_64.swissbit_ext',
]

build_options = {
  'include_files' : extra_files,
  'optimize' : 0,
  'build_exe' : dev_env.generate_pack_destination('gon_client'),
  'excludes' : exclude_modules,
  'includes' : include_modules,
  'silent' : True,
  "packages": ["gi"]
}

exe_instance = cx_Freeze.Executable(script='gon_client.py')

cx_Freeze.setup (
    name = "G/On Client",
    version = dev_env.version.get_version_string_num(),
    description = 'G/On Client for Linux',
    executables = [ exe_instance ],
    options = {'build_exe': build_options}
)

#
# Fix name of exe-file
#
#gon_client_filename_org = os.path.join(dev_env.generate_pack_destination('gon_client'), 'gon_client')
#gon_client_filename_new = os.path.join(dev_env.generate_pack_destination('gon_client'), 'gon_client.com')
#shutil.move(gon_client_filename_org, gon_client_filename_new)

#
# Fix plugin_folder
#
#plugin_folder_org = os.path.join(dev_env.generate_pack_destination('gon_client'), 'lib64', 'plugin_modules')
#plugin_folder_new = os.path.join(dev_env.generate_pack_destination('gon_client'), 'plugin_modules')
#shutil.move(plugin_folder_org, plugin_folder_new)
