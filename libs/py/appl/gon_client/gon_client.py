"""
The G/On Client

"""
from __future__ import with_statement
import warnings
warnings.filterwarnings("ignore")

import sys
import optparse
import threading
import time
import os
import os.path
import lib.cryptfacility
import lib.hardware.device
import shutil
import subprocess
import marshal
import base64
import datetime

import ConfigParser

import uuid

if sys.platform == 'darwin':
    import xml.etree
    import xml.etree.ElementTree

if sys.platform == "linux2":
    import lib.config
    import lib.smartcard.msc_pkcs15
    import gi._error
    import gi
    gi.require_version("Gtk", "3.0")
    import gi.repository.GLib
    import gi.repository.GObject
    import gi.repository.Gdk
    import gi.repository.Gio
    import gi.repository.Gtk
    import gi.repository.Pango
    import gi.repository.WebKit2
    import gi.repository.GdkPixbuf



import lib.version
import lib.checkpoint
import lib.appl.gon_client
import lib.launch_process
import lib.ToHclient
if sys.platform == 'win32':
    import lib.proxy_detection
    import lib.hardware.windows_security_center
    import lib.winreg

import lib_cpp.communication
import components.config.common
import components.communication
import components.communication.session
import components.communication.session_manager as com_session_manager
import components.communication.tunnel_endpoint_base
import components.communication.async_service

import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_token
import components.plugin.client_gateway.plugin_socket_client_runtime_env
import components.plugin.client_gateway.plugin_socket_tag as plugin_socket_tag

import components.auth.client_gateway.auth_session as component_auth
import components.dialog.client_gateway.dialog_session as component_dialog
import components.traffic.client_gateway.traffic_session as component_traffic
import components.user.client_gateway.user_session as component_user
import components.cpm.client_gateway.cpm_session as component_cpm
import components.endpoint.client_gateway.endpoint_session as component_endpoint
import components.traffic.client_gateway.client_launch_internal

import components.presentation.user_interface

import components.traffic.common.selector
import lib.appl.io_hooker
import lib.appl.crash.handler

import lib.smartcard
import lib.smartcard.pkcs15
import lib.appl.gon_os

import components.dictionary.common
import appl.gon_client_device_service.gon_client_device_service_proxy


MODULE_ID = "appl_gon_client"



#
# Arguments on mac breakes gui-functionality (the use of py2app argv_emulation = True), and
# without this option the argument parser reads the commaindline options wrong.
#
class ClientGatewayOptionsMac(object):
    def __init__(self):
        self.version = False
        self.config = './gon_client.ini'


class ClientGatewayOptions(components.config.common.ConfigClientGateway):
    """
    Options and ini-file values for the Gateway Server.
    """
    def __init__(self):
        components.config.common.ConfigClientGateway.__init__(self)
        if sys.platform == 'darwin':
            self._options = ClientGatewayOptionsMac()
        else:
            self._parser = optparse.OptionParser()
            self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
            self._parser.add_option('--config', type='string', default='./gon_client.ini', help='configuration file')
            (self._options, self._args) = self._parser.parse_args()

        self.read_config_file(self._options.config)

    def cmd_show_version(self):
        return self._options.version

    def get_dictionary_path_abs(self):
        return os.path.abspath(self.dictionary_path)


class HTTPProxyClientThread(threading.Thread):
    """
    Class holding functionality for client side http proxy
    """
    class HTTPProxyClientEnvironment:
        class config:
            INITIAL_HTTP_CONNECTIONS = 1 # assumed number of connections requested by server - should match servers MIN_WAITING_HTTP_CONNECTIONS
            MAX_WAITING_HTTP_CONNECTIONS = 4 # if more than this number of connections then get rid of them
            PING_INTERVAL = 30 # server must be given something at least this often to ensure that it has something to respond on
            ACK_TIMEOUT = 1.0 # pending acks will be sent after this long
            RETRANS_TIMEOUT = 1.0 # unacked packages will be retransmitted after this long
            MAX_RETRANS = 20 # after retransmitting this many times the connection fails
            PERSISTENT_HTTP = True # attempt persistent connections - squid supports it from client to proxy
            NEGOTIATE = True # let httpclient attempt to use SSO with Windows credentials to proxy or host
            HTTP_RECV_BUFFER = 16384
            TCP_RECV_BUFFER = 16384
            LISTEN_BACKLOG = 100 # how many TCP (HTTP) connections can be waiting, probably max 128
            MAX_RECEIVE_HEADER_LENGTH = 4096 # maximum received HTTP header length in bytes
            MAX_RECEIVE_CONTENT_LENGTH = 100000 # maximum received HTTP content length in bytes
            MAX_SEND_CONTENT_LENGTH = 50000 # soft limit maximum sent HTTP content length in bytes - content can however be as big as whole message
            MAX_OUT_OF_ORDER = 50 # maximum number of premature packages a TcpConnection will store
            MAX_UNACK = 20 # maximum number of unacked packages a TcpConnection will store before it throttles
            MAX_TCP_PER_SESSION = 100 # maximum number of TcpConnections per Session
            MAX_IDLE_TCP = 50 # how long should a TcpConnection be allowed to be idle (perhaps because other end is dead)
            MAX_IDLE_SESSION = 300 # how long should a Session be allowed to be idle (ie without TcpConnections) (perhaps because other end is dead)
            MAX_INITIAL_IDLE_SESSION = 300 # like MAX_IDLE_SESSION, but only used initially
            USER_AGENT = '' # RFC 2616 3.8, 'Giritech-ToH/$VERSION'

        out_symbol = '<'
        in_symbol = '>'

    MODULE_ID = 'ToH'

    def __init__(self, checkpoint_handler, client_config, http_addr_ip, http_addr_port):
        threading.Thread.__init__(self, name="HTTPProxyClientThread")
        self.checkpoint_handler = checkpoint_handler
        self._running = False
        self.client_config = client_config

        self.forward_addr = (client_config.service_http_ip, client_config.service_http_port)
        self.proxy_addr = None
        if client_config.service_http_proxy_enabled:
            if sys.platform == 'win32' and client_config.service_http_proxy_auto:
                url = 'http://%s:%s/' % (http_addr_ip, http_addr_port)
                try:
                    self.proxy_addr = lib.proxy_detection.get_proxy_for_url(url)
                    self.checkpoint_handler.Checkpoint("Detected proxy", MODULE_ID, lib.checkpoint.DEBUG, proxy_addr=str(self.proxy_addr))
                except lib.proxy_detection.NoProxy, msg:
                    self.checkpoint_handler.Checkpoint("No proxy detected", MODULE_ID, lib.checkpoint.DEBUG, msg=str(msg))
            if self.proxy_addr is None and client_config.service_http_proxy_ip:
                self.proxy_addr = (client_config.service_http_proxy_ip, client_config.service_http_proxy_port)
        self.http_addr = (http_addr_ip, http_addr_port)

        self.HTTPProxyClientEnvironment = HTTPProxyClientThread.HTTPProxyClientEnvironment
        self.HTTPProxyClientEnvironment.log1 = self.log1
        self.HTTPProxyClientEnvironment.log2 = self.log2
        self.HTTPProxyClientEnvironment.log3 = self.log3

        self.HTTPProxyClientEnvironment.config.USER_AGENT = client_config.service_http_user_agent

        self.toh_async_map = {}
        self.toh_session_manager = lib.ToHclient.make_session_manager(self.HTTPProxyClientEnvironment, self.forward_addr, self.proxy_addr, self.http_addr, async_map=self.toh_async_map)
        self._toh = lib.ToHclient.main()

    def get_listen_info(self):
        if self.toh_session_manager is None:
            return None
        return (self.toh_session_manager.sockname[0], self.toh_session_manager.sockname[1])

    def log1(self, s, *args):
        self.checkpoint_handler.Checkpoint("HTTPProxyClientThread.log_1", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def log2(self, s, *args):
        if self.client_config.service_http_log_2_enabled:
            self.checkpoint_handler.Checkpoint("HTTPProxyClientThread.log_2", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def log3(self, s, *args):
        if self.client_config.service_http_log_3_enabled:
            self.checkpoint_handler.Checkpoint("HTTPProxyClientThread.log_3", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG, message=repr(s % args))

    def run(self):
        with self.checkpoint_handler.CheckpointScope("HTTPProxyClientThread.run", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                self._toh.run(self.HTTPProxyClientEnvironment, self.toh_session_manager, async_map=self.toh_async_map)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("HTTPProxyClientThread.run.error", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self._running = False

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("HTTPProxyClientThread.stop", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG):
            self._toh.stop()

    def is_running(self):
        self.checkpoint_handler.Checkpoint("HTTPProxyClientThread.is_running", HTTPProxyClientThread.MODULE_ID, lib.checkpoint.DEBUG, is_running=self._running)
        return self._running


class ClientEnvironment(object):
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler


class ClientSession(components.communication.session.APISessionEventhandlerReady):
    def __init__(self, checkpoint_handler, com_session, plugin_manager, plugins, token_plugin_socket, user_interface_controler, async_service, cb_close, client_runtime_env_plugin_socket, client_config, client_knownsecret, servers, dictionary, first_start, client_runtime_env, cb_close_with_error):
        components.communication.session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self._is_closed = True
        self._version_check_close_in_process = False
        self._checkpoint_handler = checkpoint_handler
        self._client_config = client_config
        self._client_knownsecret = client_knownsecret
        self._servers = servers
        self.dictionary = dictionary
        self._client_runtime_env = client_runtime_env
        self._session_checkpoint_handler = None
        if client_config.log_session_enabled or client_config.log_session_enabled_by_remote:
            session_log_root = client_config.log_session_path
            self._session_checkpoint_handler = lib.checkpoint.create_session_checkpoint_handler(self._checkpoint_handler, com_session.get_unique_session_id(), MODULE_ID, session_log_root)
            self._checkpoint_handler = self._session_checkpoint_handler

        self._environment = ClientEnvironment(self._checkpoint_handler)
        self._com_session = com_session
        self._com_session.set_keep_alive_ping_interval_sec(client_config.session_keep_alive_ping_interval_sec)
        self._async_service = async_service
        self._user_interface_controler = user_interface_controler
        self._user_interface = self._user_interface_controler.user_interface
        self._user_interface.set_cb_close(self._cb_user_interface_close)
        self._com_session.set_eventhandler_ready(self)
        self._cb_close = cb_close
        self._cb_close_with_error = cb_close_with_error
        self._plugins = plugins
        self._token_plugin_socket = token_plugin_socket

        self._auth_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._dialog_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._traffic_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._user_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._cpm_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._tag_socket_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)
        self._endpoint_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, self._checkpoint_handler)

        self._com_session.add_tunnelendpoint_tunnel(1, self._auth_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(2, self._dialog_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(3, self._traffic_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(4, self._user_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(5, self._cpm_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(6, self._tag_socket_tunnelendpoint)
        self._com_session.add_tunnelendpoint_tunnel(7, self._endpoint_tunnelendpoint)

        self._user_session = component_user.UserSession(self._async_service,
                                                        self._checkpoint_handler,
                                                        self._user_tunnelendpoint,
                                                        self._user_interface,
                                                        self._client_runtime_env)

        self._user_session.use_sso = self._client_config.login_try_sso
        self._user_session.use_interactive_login = self._client_config.login_show_password_prompt

        self._cpm_session = component_cpm.CPMSessionOnline(self._async_service,
                                                           self._checkpoint_handler,
                                                           self._cpm_tunnelendpoint,
                                                           self._user_interface,
                                                           self.dictionary,
                                                           self._client_runtime_env,
                                                           self)
        self._endpoint_session = component_endpoint.EndpointSession(self._async_service,
                                                                    self._checkpoint_handler,
                                                                    self._endpoint_tunnelendpoint,
                                                                    self._user_interface,
                                                                    self.dictionary,
                                                                    plugin_manager,
                                                                    self._plugins,
                                                                    self._client_knownsecret,
                                                                    self._servers,
                                                                    self._cb_close,
                                                                    self._cb_close_with_error,
                                                                    self._client_runtime_env)


        self.launch_sessions = components.traffic.client_gateway.client_launch_internal.LaunchSessions(self._cpm_session, self._endpoint_session)

        self._traffic_session = component_traffic.TrafficSession(self._async_service,
                                                                 self._checkpoint_handler,
                                                                 self._traffic_tunnelendpoint,
                                                                 self._user_interface,
                                                                 self._client_runtime_env,
                                                                 self.launch_sessions)

        self._dialog_session = component_dialog.DialogSession(self._async_service,
                                                              self._checkpoint_handler,
                                                              self._dialog_tunnelendpoint,
                                                              self._user_interface,
                                                              first_start,
                                                              self._cb_close)

        self._auth_session = component_auth.AuthorizationSession(self._async_service,
                                                                 self._checkpoint_handler,
                                                                 self._auth_tunnelendpoint,
                                                                 self._user_interface,
                                                                 plugin_manager,
                                                                 self._plugins,
                                                                 self._async_service,
                                                                 self._client_runtime_env)


        self._tag_plugin_socket = plugin_socket_tag.PluginSocket(self._async_service,
                                                                 self._checkpoint_handler,
                                                                 self._tag_socket_tunnelendpoint,
                                                                 plugin_manager,
                                                                 self._plugins)

    def session_state_ready(self):
        unique_session_id = self._com_session.get_unique_session_id()
        remote_unique_session_id = self._com_session.get_remote_unique_session_id()

        if not (self._client_config.log_session_enabled or self._client_config.log_session_enabled_by_remote and self._com_session.get_remote_session_logging_enabled()):
            if self._session_checkpoint_handler != None:
                self._session_checkpoint_handler.close()

        with self._checkpoint_handler.CheckpointScope("ClientSession::session_state_ready", MODULE_ID, lib.checkpoint.DEBUG, unique_session_id=unique_session_id, remote_unique_session_id=remote_unique_session_id):
            self._is_closed = False
            self._user_interface_controler.set_message(self.dictionary._("Connected"), message_show_dots=False, message_show_only_once=True)
            self._com_session.read_start_state_ready()
            self._cpm_session.session_start()
            self._async_service.sleep_start_mutex(self._checkpoint_handler, self._com_session.get_mutex(), 0, 0, 0, self, '_session_state_ready_delayed')

    def _session_state_ready_delayed(self):
        # Wait for cpm_session to connect
        self._checkpoint_handler.Checkpoint("ClientSession::_session_state_ready_delayed", MODULE_ID, lib.checkpoint.DEBUG)
        if not self._cpm_session.is_connected():
            if not self.is_closed():
                self._async_service.sleep_start_mutex(self._checkpoint_handler, self._com_session.get_mutex(), 0, 0, 50, self, '_session_state_ready_delayed')
            return

        with self._checkpoint_handler.CheckpointScope("ClientSession::_session_state_ready_delayed.ready", MODULE_ID, lib.checkpoint.DEBUG):
            self._auth_session.session_start()
            self._user_session.session_start()
            self._traffic_session.session_start(self._com_session.get_remote_unique_session_id())
            self._dialog_session.session_start()
            self._tag_plugin_socket.session_start()
            self._endpoint_session.session_start()

    def session_state_closed(self):
        if not self._is_closed:
            with self._checkpoint_handler.CheckpointScope("ClientSession::session_state_closed", MODULE_ID, lib.checkpoint.DEBUG):
                self._cpm_session.session_close()
                self._traffic_session.session_close()
                self._dialog_session.session_close()
                self._auth_session.session_close()
                self._user_session.session_close()
                self._tag_plugin_socket.session_close()
                self._endpoint_session.session_close()

                if not self._version_check_close_in_process:
                    self._user_interface_controler.set_message(self.dictionary._("Closed"), message_show_dots=False, message_show_only_once=True)
                else:
                    self._cb_close(restart=True, install_state='hej')
                self._is_closed = True

    def session_state_key_exchange(self):
        pass

    def session_user_signal(self, signal_id, message):
        with self._checkpoint_handler.CheckpointScope("ClientSession::session_user_signal", MODULE_ID, lib.checkpoint.DEBUG, signal_id=signal_id, message=message):
            if signal_id in [components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_VERSION_ERROR, components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_VERSION_ERROR_REMOTE]:
                self._user_interface_controler.set_message(message, message_show_dots=False, message_show_only_once=True)
                restart_state = lib.appl.gon_client.GOnClientInstallState()
                restart_state.set_online_version_update_operation()
                self._cb_close(restart=True, install_state=restart_state)
            elif signal_id in [components.communication.tunnel_endpoint_base.TunnelendpointBase.USER_SIGNAL_DISPATCH_ERROR]:
                self._cb_close()

    def is_closed(self):
        return self._is_closed

    def _cb_user_interface_close(self):
        if not self._is_closed:
            self._cb_close()
            self._user_interface.reset_cb_close()

    def cpm_cb_restart(self, install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted
        and that the new instance is found in the restart_root, and the install_state are given as argument to the install module when relaunched.
        """
        self._cb_close(restart=True, install_state=install_state)

    def cpm_cb_save_restart_state(self, gon_client_install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted at some point
        and that the install_stat should be passed to the new instance of the gon_client.
        The
        """
        pass

    def cpm_cb_closed(self):
        """
        Callback from cpm_session telling that it has closed down.
        """
        pass

    def is_first_access_finish(self):
        if self._dialog_session is not None:
            return self._dialog_session.is_first_access_finish()
        return True


class ClientSessionVersionUpgrade(components.communication.session.APISessionEventhandlerReady):
    def __init__(self, checkpoint_handler, com_session, user_interface_controler, async_service, cb_close, client_runtime_env_plugin_socket, client_config, dictionary, client_runtime_env):
        components.communication.session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self._is_closed = True
        self._checkpoint_handler = checkpoint_handler
        self._client_config = client_config
        self.dictionary = dictionary
        self._com_session = com_session
        self._user_interface_controler = user_interface_controler
        self._user_interface = user_interface_controler.user_interface
        self._user_interface.set_cb_close(self._cb_user_interface_close)
        self._com_session.set_eventhandler_ready(self)
        self._cb_close = cb_close
        self._client_runtime_env = client_runtime_env
        self.async_service = async_service

        self._cpm_tunnelendpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(self.async_service, self._checkpoint_handler)
        self._com_session.add_tunnelendpoint_tunnel(5, self._cpm_tunnelendpoint)
        self._cpm_session = component_cpm.CPMSessionOnlineVersionUpgrade(self.async_service,
                                                                         self._checkpoint_handler,
                                                                         self._cpm_tunnelendpoint,
                                                                         self._user_interface,
                                                                         self.dictionary,
                                                                         self._client_runtime_env,
                                                                         self)

    def session_state_ready(self):
        unique_session_id = self._com_session.get_unique_session_id()
        remote_unique_session_id = self._com_session.get_remote_unique_session_id()
        self._checkpoint_handler.CheckpointScope("session.ready", MODULE_ID, lib.checkpoint.INFO, remote_unique_session_id=remote_unique_session_id)
        with self._checkpoint_handler.CheckpointScope("ClientSessionVersionUpgrade::session_state_ready", MODULE_ID, lib.checkpoint.DEBUG, unique_session_id=unique_session_id, remote_unique_session_id=remote_unique_session_id):
            self._is_closed = False
            self._user_interface_controler.set_message(self.dictionary._("Connected"), message_show_dots=False, message_show_only_once=True)
            self._com_session.read_start_state_ready()
            self._cpm_session.start_version_update()

    def session_state_closed(self):
        if not self._is_closed:
            self._checkpoint_handler.CheckpointScope("session.closed", MODULE_ID, lib.checkpoint.INFO)
            with self._checkpoint_handler.CheckpointScope("ClientSessionVersionUpgrade::session_state_closed", MODULE_ID, lib.checkpoint.DEBUG):
                self._user_interface_controler.set_message(self.dictionary._("Closed"), message_show_dots=False, message_show_only_once=True)
                self._is_closed = True

    def session_state_key_exchange(self):
        pass

    def is_closed(self):
        return self._is_closed

    def _cb_user_interface_close(self):
        if not self._is_closed:
            self._cb_close()
            self._user_interface.reset_cb_close()

    def cpm_cb_restart(self, install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted
        and that the new instance is found in the restart_root, and the install_state are given as argument to the install module when relaunched.
        """
        self._cb_close(restart=True, install_state=install_state)

    def cpm_cb_save_restart_state(self, gon_client_install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted at some point
        and that the install_stat should be passed to the new instance of the gon_client.
        The
        """
        pass

    def cpm_cb_closed(self):
        """
        Callback from cpm_session telling that it has closed down.
        """
        self._cb_close(restart=False, install_state=None)

    def session_user_signal(self, signal_id, message):
        with self._checkpoint_handler.CheckpointScope("ClientSessionVersionUpgrade::session_user_signal", MODULE_ID, lib.checkpoint.DEBUG, signal_id=signal_id, message=message):
            pass

    def is_first_access_finish(self):
        return True


class ClientSessionManager(components.communication.session_manager.APISessionManagerEventhandler):
    def __init__(self, async_service, checkpoint_handler, plugin_manager, plugins, token_plugin_socket, user_interface_controler, client_knownsecret, client_runtime_env_plugin_socket, client_config, online_version_update_operation, dictionary, first_start, client_runtime_env):
        com_session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self._async_service = async_service
        self._checkpoint_handler = checkpoint_handler
        self._plugin_manager = plugin_manager
        self._plugins = plugins
        self._token_plugin_socket = token_plugin_socket
        self._client_runtime_env_plugin_socket = client_runtime_env_plugin_socket
        self._user_interface_controler = user_interface_controler
        self.dictionary = dictionary
        self._client_knownsecret = client_knownsecret
        self._servers = None
        self._user_interface = user_interface_controler.user_interface
        self._com_session_manager = com_session_manager.APISessionManagerClient_create(async_service.get_com_async_service(), self, checkpoint_handler.checkpoint_handler, client_knownsecret, lib_cpp.communication.ApplProtocolType.PYTHON)
        self._com_session_manager.set_config_session_logging_enabled(client_config.log_session_enabled)
        self._client_config = client_config
        self._online_version_update_operation = online_version_update_operation
        self._session = None
        self._is_running = True
        self._is_stopping = False
        self._restart = False
        self._restart_root = None
        self._install_state = None
        self._toh_clients = []
        self._connecting_info = {}
        self.first_access_finish = False
        self.first_start = first_start
        self.client_runtime_env = client_runtime_env
        self._has_been_connected = False
        self._exit_with_error = False
        self._exit_with_error_message = ""

    def set_servers(self, servers_spec):
        if servers_spec is not None:
            self._com_session_manager.set_servers(servers_spec)
            self._servers = servers_spec

    def session_manager_resolve_connection(self, resolve_info):
        if resolve_info['type'] == 'http':
            http_addr_ip =  resolve_info['host']
            http_addr_port =  resolve_info['port']

            toh_client_thread = HTTPProxyClientThread(self._checkpoint_handler, self._client_config, http_addr_ip, http_addr_port)
            self._toh_clients.append(toh_client_thread)

            listen_info = toh_client_thread.get_listen_info()
            if listen_info is not None:
                resolve_info['host'] = listen_info[0]
                resolve_info['port'] = listen_info[1]
                toh_client_thread.start()
        return resolve_info

    def session_manager_connecting(self, connection_id, connection_title):
        self._connecting_info[connection_id] = connection_title
        self._update_conneting_info()

    def session_manager_connecting_failed(self, connection_id):
        del self._connecting_info[connection_id]
        self._update_conneting_info()

    def _update_conneting_info(self):
        self._user_interface_controler.set_message(self.dictionary._('Connecting'), message_show_dots=True, message_show_only_once=False)

    def session_manager_failed(self, message):
        # hack to include messages from c++ in dictionary
        ignore_str = self.dictionary._('No connection groups specified')
        ignore_str = self.dictionary._('No servers available at the moment')
        with self._checkpoint_handler.CheckpointScope("ClientSessionManager::session_manager_failed", MODULE_ID, lib.checkpoint.DEBUG, message=message):
            self._user_interface_controler.set_message(self.dictionary._(message), message_show_dots=False, message_show_only_once=True)

    def session_manager_session_created(self, connection_id, com_session):
        session_id = com_session.get_session_id()
        with self._checkpoint_handler.CheckpointScope("ClientSessionManager::session_manager_session_created", MODULE_ID, lib.checkpoint.DEBUG, session_id=session_id):
            if self._online_version_update_operation:
                self._session = ClientSessionVersionUpgrade(self._checkpoint_handler, com_session, self._user_interface_controler, self._async_service, self.stop, self._client_runtime_env_plugin_socket, self._client_config, self.dictionary, self.client_runtime_env)
            else:
                self._session = ClientSession(self._checkpoint_handler, com_session, self._plugin_manager, self._plugins, self._token_plugin_socket, self._user_interface_controler, self._async_service, self.stop, self._client_runtime_env_plugin_socket, self._client_config, self._client_knownsecret, self._servers, self.dictionary, self.first_start, self.client_runtime_env, self.stop_with_error)
        self._user_interface.splash.hide()
        self._user_interface_controler.use_splash_for_messages = False
        self._has_been_connected = True

    def session_manager_session_closed(self, session_id):
        with self._checkpoint_handler.CheckpointScope("ClientSessionManager::session_manager_session_closed", MODULE_ID, lib.checkpoint.DEBUG, session_id=session_id):
            if self._session is not None:
                self.first_access_finish = self._session.is_first_access_finish()
            self._is_running = False
            self._session = None
            self.stop()
            self._stop_final()

    def session_manager_closed(self):
        with self._checkpoint_handler.CheckpointScope("ClientSessionManager::session_manager_closed", MODULE_ID, lib.checkpoint.DEBUG):
            if self._session is not None:
                self.first_access_finish = self._session.is_first_access_finish()
            self._is_running = False
            self._session = None
            self.stop()
            self._stop_final()

    def is_closed(self):
        return self._session == None or self._session.is_closed()

    def has_been_connected(self):
        return self._has_been_connected

    def is_running(self):
        if self._is_running:
            return True

        for toh_client in self._toh_clients:
            if toh_client.is_running():
                return True
        return False


    def start(self):
        try:
            self._com_session_manager.start()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self._checkpoint_handler.CheckpointException("ClientSessionManager.start.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)

    def stop_with_error(self, message):
        self._checkpoint_handler.Checkpoint("ClientSessionManager::stop_with_error", MODULE_ID, lib.checkpoint.DEBUG)
        self._exit_with_error = True
        self._exit_with_error_message = message
        self.stop()

    def stop(self, restart=False, install_state=None):
        self._checkpoint_handler.Checkpoint("ClientSessionManager::stop", MODULE_ID, lib.checkpoint.DEBUG)
        # Can be called with session mutex taken, decouble in order to switch to session_manager mutex
        self._async_service.sleep_start_mutex(self._checkpoint_handler, self._com_session_manager.get_mutex(), 0, 0, 0, self, '_stop_decoubled', (restart, install_state))

    def _stop_decoubled(self, (restart, install_state) ):
        with self._checkpoint_handler.CheckpointScope("ClientSessionManager::_stop_decoubled", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                if not self._is_stopping:
                    self._is_stopping = True

                    if not self._com_session_manager.is_closed():
                        self._com_session_manager.close_start()

                    for toh_client in self._toh_clients:
                        toh_client.stop()

                    self._restart = restart
                    self._install_state = install_state
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self._checkpoint_handler.CheckpointException("ClientSessionManager.stop.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)

    def _stop_final(self):
        self._checkpoint_handler.Checkpoint("ClientSessionManager::stop_final", MODULE_ID, lib.checkpoint.DEBUG)

        if self.is_running():
            self._async_service.sleep_start_mutex(self._checkpoint_handler, self._com_session_manager.get_mutex(), 0, 0, 1000, self, '_stop_final')

        self._user_interface_controler.end()


    def do_restart(self):
        return self._restart

    def get_install_state(self):
        return self._install_state

    def is_first_access_finish(self):
        return self.first_access_finish

    def do_exit_with_error(self):
        return (self._exit_with_error, self._exit_with_error_message)



class UserInterfaceControler(threading.Thread):
    def __init__(self, checkpoint_handler, user_interface):
        threading.Thread.__init__(self, name="UserInterfaceControler")
        self.checkpoint_handler = checkpoint_handler
        self.user_interface = user_interface
        self.dictionary = user_interface.dictionary
        self.wait_counter = 0
        self.current_message = None
        self.current_message_show_dots = True
        self.current_message_update_interval_sec = 2
        self.end_after_display = False
        self.use_splash_for_messages = True

    def run(self):
        with self.checkpoint_handler.CheckpointScope("UserInterfaceControler.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:

                self._running = True
                while self._running:
                    self._show_current_message_and_sleep()
                    if self.end_after_display:
                        self.end()
                time.sleep(2)

                self.current_message_update_interval_sec = 1
                self._show_current_message_and_sleep()

                self.checkpoint_handler.Checkpoint("UserInterfaceControler.destroy", MODULE_ID, lib.checkpoint.DEBUG)
                self.user_interface.destroy()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserInterfaceControler.run.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
            self._running = False

    def _show_current_message_and_sleep(self):
        if self.current_message is not None:
            dots = '.' * (self.wait_counter % 5)
            if self.current_message_show_dots:
                if self.use_splash_for_messages:
                    self.user_interface.splash.set_text("%s %6s" % (self.current_message, dots))
                else:
                    self.user_interface.message.set_message(self.dictionary._("G/On Client"), "%s %6s" % (self.current_message, dots))
                    self.user_interface.message.display()
            else:
                if self.use_splash_for_messages:
                    self.user_interface.splash.set_text(self.current_message)
                else:
                    self.user_interface.message.set_message(self.dictionary._("G/On Client"), self.current_message)
                    self.user_interface.message.display()
            if self.current_message_show_only_once:
                self.current_message = None
        time.sleep(self.current_message_update_interval_sec)
        self.wait_counter += 1

    def set_message(self, message, message_show_dots=True, end_after_display=False, message_show_only_once=True):
        self.current_message = message
        self.current_message_show_dots = message_show_dots
        self.end_after_display = end_after_display
        self.current_message_show_only_once = message_show_only_once

    def message_update_interval(self, message_update_interval_sec):
        self.current_message_update_interval_sec = message_update_interval_sec

    def end(self):
        self._running = False

    def is_running(self):
        return self._running





class MountPointCheckThread(threading.Thread):
    def __init__(self, checkpoint_handler, mount_path, client_runtime_env):
        threading.Thread.__init__(self, name="MountPointCheckThread")
        self._checkpoint_handler = checkpoint_handler
        self._mount_path = mount_path
        self.client_runtime_env = client_runtime_env
        self._running = False
        self._has_started = False

    def run(self):
        with self._checkpoint_handler.CheckpointScope("MountPointCheckThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                self._has_started = True
                self.client_runtime_env.mark_in_use()
                in_use_ts = datetime.datetime.now()

                while self._running:
                    if not os.path.isdir(self._mount_path):
                        self.stop()
                    in_use_delta = datetime.datetime.now() - in_use_ts
                    if  in_use_delta.seconds > (60 * 5):
                        self.client_runtime_env.mark_in_use()
                        in_use_ts = datetime.datetime.now()
                    time.sleep(1)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self._checkpoint_handler.CheckpointException("MountPointCheckThread.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self._running = False
            self.client_runtime_env.mark_not_in_use()

    def stop(self):
        with self._checkpoint_handler.CheckpointScope("MountPointCheckThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def is_running(self):
        return self._running

    def has_started(self):
        return self._has_started


class ApplicationCommunicationThread(threading.Thread):
    def __init__(self, checkpoint_handler):
        threading.Thread.__init__(self, name="ApplicationCommunication")
        self._checkpoint_handler = checkpoint_handler
        self._running = False
        self._has_started = False

    def run(self):
        with self._checkpoint_handler.CheckpointScope("ApplicationCommunicationThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._has_started = True
                self._running = True
                components.traffic.common.selector.start(1.0)
                while self._running:
                    time.sleep(1)

                components.traffic.common.selector.stop()
                components.traffic.common.selector.join()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self._checkpoint_handler.CheckpointException("ApplicationCommunication.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self._running = False

    def stop(self):
        with self._checkpoint_handler.CheckpointScope("ApplicationCommunicationThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def is_running(self):
        return self._running

    def has_started(self):
        return self._has_started



class AliveThread(threading.Thread):
    """
    Survilicence thread that terminate all threads if on of the thread is stopping
    """
    def __init__(self, checkpoint_handler):
        threading.Thread.__init__(self, name="AliveThread")
        self._checkpoint_handler = checkpoint_handler
        self._running = False
        self._threads_to_watch = []
        self._predicates_to_watch = []

    def run(self):
        with self._checkpoint_handler.CheckpointScope("AliveThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True

                # Wait for all threads to report that they are running
                all_threads_started = False
                while not all_threads_started:
                    all_threads_started = True
                    for thread_to_watch in self._threads_to_watch:
                        all_threads_started = all_threads_started and thread_to_watch.has_started()

                # Watch running threads
                while self._running:
                    for thread_to_watch in self._threads_to_watch:
                        if not thread_to_watch.is_running():
                            self.stop()
                    for (predicate_to_watch, thread_to_watch) in self._predicates_to_watch:
                        if not predicate_to_watch():
                            self.stop()
                    time.sleep(1)

                # Stop all threads
                self._running = False
                for thread_to_watch in self._threads_to_watch:
                    thread_to_watch.stop()
                for (predicate_to_watch, thread_to_watch) in self._predicates_to_watch:
                    thread_to_watch.stop()

                # Wait for all threads to stop
                for thread_to_watch in self._threads_to_watch:
                    thread_to_watch.join()

            except:
                (etype, evalue, etrace) = sys.exc_info()
                self._checkpoint_handler.CheckpointException("AliveThread.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

    def stop(self):
        with self._checkpoint_handler.CheckpointScope("AliveThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def add_thread_to_watch(self, thread):
        self._threads_to_watch.append(thread)

    def add_predicate_to_watch(self, predicate):
        self._predicates_to_watch.append(predicate)



class ClientSessionOfflineThread(threading.Thread, component_cpm.CPMSessionCB):
    def __init__(self, async_service, checkpoint_handler, user_interface_controler, gon_client_restart_handler, client_runtime_env):
        threading.Thread.__init__(self, name="ClientSessionOfflineThread")
        self.async_service = async_service
        self._checkpoint_handler = checkpoint_handler
        self._user_interface_controler = user_interface_controler
        self._gon_client_restart_handler = gon_client_restart_handler
        self._client_runtime_env = client_runtime_env
        self.gon_client_install_state = None
        self._running = False
        self.do_restart = False

    def run(self):
        with self._checkpoint_handler.CheckpointScope("ClientSessionOfflineThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                cpm_session_offline =  component_cpm.CPMSessionOffline(self.async_service, self._checkpoint_handler, self._client_runtime_env, self._user_interface_controler.user_interface, self._user_interface_controler.user_interface.dictionary, self._gon_client_restart_handler.get_gon_client_install_state(), self)
                cpm_session_offline.install_start_offline()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self._checkpoint_handler.CheckpointException("ClientSessionOfflineThread.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            self._running = False

    def stop(self):
        with self._checkpoint_handler.CheckpointScope("ClientSessionOfflineThread.stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._running = False

    def is_running(self):
        return self._running

    def has_started(self):
        return self._running

    def cpm_cb_restart(self, gon_client_install_state):
        self.do_restart = True
        self.gon_client_install_state = gon_client_install_state
        self._user_interface_controler.end()

    def cpm_cb_save_restart_state(self, gon_client_install_state):
        self._gon_client_restart_handler.do_restart(self._checkpoint_handler, gon_client_install_state, skip_launch=True)

    def cpm_cb_closed(self):
        self._user_interface_controler.end()


# The async_service is a global instance because it contains a async-io object
# that should be deleted at the very end.
async_service = None


def main():
    #
    # Parsing arguments from commandline and ini-file
    #
    client_config = ClientGatewayOptions()

    #
    # Show version and exit if requested
    #
    if(client_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    #
    # Initialize logging
    #
    checkpoint_handler_generator = lib.checkpoint.CheckpointHandlerGenerator(client_config.log_enabled,
                                                                             client_config.log_verbose,
                                                                             client_config.log_file,
                                                                             client_config.is_log_type_xml(),
                                                                             True,
                                                                             client_config.log_rotate)

    checkpoint_handler = checkpoint_handler_generator.get_handler()
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    try:
        checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                      version=lib.version.Version.create_current().get_version_string(),
                                      build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                      )
        #
        # Disable App Nap feature on mac os X 10.9
        #
        if sys.platform == 'darwin':
            try:
                checkpoint_handler.Checkpoint("main.darwin.disable.app_nap", MODULE_ID, lib.checkpoint.DEBUG)
                process = lib.launch_process.Process(checkpoint_handler)
                process.launch("defaults write com.giritech.gon_client NSAppSleepDisabled -bool YES", False, None)
            except:
                checkpoint_handler.Checkpoint("main.darwin.disable.app_nap.failed", MODULE_ID, lib.checkpoint.ERROR)

        #
        # Initialize Communication component (CryptFacility)
        #
        components.communication.async_service.init(checkpoint_handler)


        #
        # Initialize user interface
        #
        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, client_config.get_dictionary_path_abs())
        user_interface = components.presentation.user_interface.UserInterface(configuration=client_config, dictionary=dictionary)
        user_interface_controler = UserInterfaceControler(checkpoint_handler, user_interface)
        user_interface_controler.start()
        user_interface_controler.set_message(dictionary._('Starting please wait'), message_show_dots= True, message_show_only_once=False)

        #
        # Initialize async service
        #
        global async_service
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, client_config.service_num_of_threads)

        #
        # Load plugins
        #
        additional_device_roots = []
        additional_device_roots.append(client_config.deployment_path)
        plugin_manager = components.plugin.client_gateway.manager.Manager(checkpoint_handler, client_config.plugin_modules_path)
        plugins = plugin_manager.create_instances(async_service=async_service, checkpoint_handler=checkpoint_handler, user_interface=user_interface, additional_device_roots=additional_device_roots)
        token_plugin_socket = components.plugin.client_gateway.plugin_socket_token.PluginSocket(plugin_manager, plugins)
        client_runtime_env_plugin_socket = components.plugin.client_gateway.plugin_socket_client_runtime_env.PluginSocket(plugin_manager, plugins, checkpoint_handler)

        #
        # Handle request for off-line and/or relocated operation
        #
        (client_runtime_env_id, client_runtime_env) = (None, None)
        checkpoint_handler.Checkpoint("main.runtime_env.lookup", MODULE_ID, lib.checkpoint.DEBUG, cwd=os.getcwdu())
        restart_relocated_info = lib.appl.gon_client.GonClientRestartedRelocatedInfo.create_from_file(os.getcwdu(), remove_after_read=True)
        if restart_relocated_info is not None:
            checkpoint_handler.Checkpoint("main.runtime_env.lookup.relocated", MODULE_ID, lib.checkpoint.DEBUG)
            client_runtime_env_id = restart_relocated_info.gon_client_base_runtime_env_id
            client_runtime_env = client_runtime_env_plugin_socket.get_instance(client_runtime_env_id)
        if client_runtime_env is None:
            (client_runtime_env_id, client_runtime_env) = client_runtime_env_plugin_socket.get_current_instance()
        # This is only necessary while 5.3 clients need to be updated
        if client_runtime_env is None:
            installation_root = lib.appl.gon_client.get_installation_root()
            (client_runtime_env_id, client_runtime_env) = client_runtime_env_plugin_socket.get_relocated_instance(installation_root)
        if client_runtime_env is None:
            user_interface_controler.set_message(dictionary._("Unable to determinate location of G/On Client"), end_after_display=True)
            checkpoint_handler.Checkpoint("main.invalid_runtime_env_id", MODULE_ID, lib.checkpoint.ERROR)
            user_interface.splash.display()
            user_interface.start()
            user_interface_controler.join()
            return 1

        # Change the location of the logfile to temp folder
        checkpoint_handler_generator.set_new_logfile_folder(client_runtime_env.get_temp_root(), MODULE_ID)
        checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                      version=lib.version.Version.create_current().get_version_string(),
                                      build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                      )
        checkpoint_handler.Checkpoint("main.runtime_env", MODULE_ID, lib.checkpoint.INFO, runtime_env_root=client_runtime_env.get_root())

        gon_client_restart_handler = lib.appl.gon_client.GOnClientRestartHandler(checkpoint_handler, client_runtime_env_id, client_runtime_env)
        if gon_client_restart_handler.is_restarted_for_offline_operation():
            client_runtime_env_id_base = gon_client_restart_handler.get_base_runtime_environment_id()
            client_runtime_env_base = client_runtime_env_plugin_socket.get_instance(client_runtime_env_id_base)
            if client_runtime_env_base == None:
                checkpoint_handler.Checkpoint("main.offline.invalid_runtime_env_id", MODULE_ID, lib.checkpoint.ERROR, runtime_env_id_base=client_runtime_env_id_base)
                return 1

            user_interface_controler.set_message(dictionary._('Working offline please wait'), message_show_dots=False, message_show_only_once=True)
            client_session_offline  = ClientSessionOfflineThread(async_service, checkpoint_handler, user_interface_controler, gon_client_restart_handler, client_runtime_env_base)
            client_session_offline.start()

            user_interface.splash.hide()
            user_interface_controler.use_splash_for_messages = False
            user_interface.start()

            client_session_offline.stop()
            client_session_offline.join()
            if client_session_offline.do_restart:
                gon_client_restart_handler.do_restart(checkpoint_handler, client_session_offline.gon_client_install_state)
            return 0
        else:
            # This is used when a client is installed by gon_client_installer which write a restart file for mark as first_start
            # but if the key was moved after the installation to a different computer and mounted at a different location
            # a update of the gon_client in the first run failed because the mountpoint was not correct.
            gon_client_restart_handler.reset_gon_client_base(client_runtime_env)

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main.unexpected.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
        return 1

    #
    # Get known secret and servers
    #
    try:
        (client_knownsecret, servers_spec) = token_plugin_socket.get_knownsecret_and_servers(client_runtime_env)
        if servers_spec is None:
            checkpoint_handler.Checkpoint("main.error", MODULE_ID, lib.checkpoint.ERROR, message="Unable to identify server")
            user_interface_controler.set_message(dictionary._("Unable to identify server"), end_after_display=True)
            user_interface.splash.display()
            user_interface.start()
            user_interface_controler.join()
            return 1
        if client_knownsecret is None:
            checkpoint_handler.Checkpoint("main.error", MODULE_ID, lib.checkpoint.ERROR, message="Unable to locate known secret")
            user_interface_controler.set_message(dictionary._("Unable to connect to server because the known-secret is missing"), end_after_display=True)
            user_interface.splash.display()
            user_interface.start()
            user_interface_controler.join()
            return 1

        #
        # Initialize session manager
        #
        first_start  = gon_client_restart_handler.is_first_start_operation()
        online_version_update_operation = gon_client_restart_handler.is_restared_for_online_version_update_operation()
        session_manager = ClientSessionManager(async_service, checkpoint_handler, plugin_manager, plugins, token_plugin_socket, user_interface_controler, client_knownsecret, client_runtime_env_plugin_socket, client_config, online_version_update_operation, dictionary, first_start, client_runtime_env)
        session_manager.set_servers(servers_spec)

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
        return 1

    try:
        #
        # Start mountpoint watcher thread (if enabled)
        #
        moint_point_check_thread = MountPointCheckThread(checkpoint_handler, os.getcwdu(), client_runtime_env)
        if client_config.deployment_auto_close:
            moint_point_check_thread.start()

        #
        # Start async service
        #
        session_manager.start()

        #
        # Start application communication thread
        #
        application_communication_thread = ApplicationCommunicationThread(checkpoint_handler)
        application_communication_thread.start()

        #
        # Start alive thread
        #
        alive_thread = AliveThread(checkpoint_handler)
        alive_thread.add_thread_to_watch(moint_point_check_thread)
        alive_thread.add_thread_to_watch(application_communication_thread)
        alive_thread.add_predicate_to_watch( (session_manager.is_running, session_manager) )
        alive_thread.start()

        #
        # Start userinterface in main thread. This will interupt the mainthread until user_interface.start() terminates.
        #
        async_service.start()
        user_interface.splash.display()
        user_interface.start()
        checkpoint_handler.Checkpoint("main.user_interface_done", MODULE_ID, lib.checkpoint.DEBUG)

        #
        # Let the alive thread stop all threads
        #
        alive_thread.stop()
        alive_thread.join()

        #
        # Stop the interface controler
        #
        user_interface_controler.end()
        user_interface_controler.join()

        #
        # Stop async thread
        #
        for t in threading.enumerate():
            checkpoint_handler.Checkpoint("main.threadinfo.1", MODULE_ID, lib.checkpoint.DEBUG, thread_name=t.getName(), thread_is_alive=t.isAlive())

        checkpoint_handler.Checkpoint("main.async_stop", MODULE_ID, lib.checkpoint.DEBUG)
        async_service.stop()
        checkpoint_handler.Checkpoint("main.async_join", MODULE_ID, lib.checkpoint.DEBUG)
        async_service.join()
        checkpoint_handler.Checkpoint("main.async_done", MODULE_ID, lib.checkpoint.DEBUG)

        for t in threading.enumerate():
            checkpoint_handler.Checkpoint("main.threadinfo.2", MODULE_ID, lib.checkpoint.DEBUG, thread_name=t.getName(), thread_is_alive=t.isAlive())
        #
        # Handles restart of client
        #
        if session_manager.do_restart():
            gon_client_restart_handler.do_restart(checkpoint_handler, session_manager.get_install_state())

        if gon_client_restart_handler.is_first_start_operation() and not session_manager.is_first_access_finish():
            checkpoint_handler.Checkpoint("main.rewriting_first_start", MODULE_ID, lib.checkpoint.DEBUG)
            gon_client_restart_handler.rewrite_restart_state()

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)

    #
    # List thread status
    #
    for t in threading.enumerate():
        checkpoint_handler.Checkpoint("main.threadinfo.3", MODULE_ID, lib.checkpoint.DEBUG, thread_name=t.getName(), thread_is_alive=t.isAlive())
    checkpoint_handler.Checkpoint("stopped", MODULE_ID, lib.checkpoint.INFO)


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        if sys.platform == 'darwin':
            path = os.path.join(path, '..', 'Resources')

            # Removing PYTHONHOME, PYTHONPATH and RESOURCEPATH from py2app'ed
            # Gave problems when launcing a python client application (ask MK)
            os.environ.pop('PYTHONHOME', None)
            os.environ.pop('PYTHONPATH', None)
            os.environ.pop('RESOURCEPATH', None)

        os.chdir(path)

    main()
    sys.exit()
