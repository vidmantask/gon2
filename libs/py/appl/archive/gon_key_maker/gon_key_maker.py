"""
The G/On Key Maker
"""
from __future__ import with_statement
import sys
import optparse
import threading
import time
import os
import os.path
import lib.cryptfacility
import lib.hardware.device
import shutil
import subprocess
import marshal
import base64
import time
import stat

import ConfigParser

import uuid

import lib.version
import lib.checkpoint 
import lib.appl.gon_client
import lib.launch_process

import components.config.common
import components.communication
import components.communication.session
import components.communication.session_manager as com_session_manager
import components.communication.tunnel_endpoint_base

import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_token
import components.plugin.client_gateway.plugin_socket_client_runtime_env
import components.plugin.client_gateway.plugin_socket_tag as plugin_socket_tag

import components.auth.client_gateway.auth_session as component_auth
import components.dialog.client_gateway.dialog_session as component_dialog
import components.traffic.client_gateway.traffic_session as component_traffic
import components.user.client_gateway.user_session as component_user
import components.cpm.client_gateway.cpm_session as component_cpm
import components.endpoint.client_gateway.endpoint_session as component_endpoint

import components.presentation.user_interface

import components.traffic.common.selector
import lib.appl.io_hooker
import lib.appl.crash.handler

import components.communication.async_service

import lib.smartcard
import lib.smartcard.pkcs15



import lib.smartcard.msc
import lib.hardware.device
import lib.variable_expansion

from components.presentation.update import UpdateController as gui_update_controller

try:
    from custom_key_maker_tools_config import key_maker_tools_config
except Exception, e:
    from key_maker_tools_config import key_maker_tools_config



if sys.platform == 'win32':
    import win32file
    import win32com.shell.shell
    import win32com.shell.shellcon


module_id = "appl_gon_key_maker"




def get_tmp_log_file():
    tmp_log_filename = './tmp_applet_init.log'
    tmp_log_file = open(tmp_log_filename, 'wt')
    return tmp_log_file

def dump_output_to_log(checkpoint_handler, f):
    if f:
        try:
            f.close()
            f = open(f.name, "rt")
            lines = f.readlines()
            f.close()
            checkpoint_handler.CheckpointMultilineMessages("dump_output_to_log", module_id, lib.checkpoint.DEBUG, lines)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("dump_output_to_log", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)


class InitialiseGDToken(threading.Thread):


    def __init__(self, args, cwd, checkpoint_handler):
        threading.Thread.__init__(self, name="InitialiseGDToken")
        self._running = False
        self._has_started = False
        self._error_message = None
        self._args = args
        self._cwd = cwd
        self.checkpoint_handler = checkpoint_handler
        
    def run(self):
        self._running = True
        with self.checkpoint_handler.CheckpointScope("InitialiseGDToken", module_id, lib.checkpoint.DEBUG):
            try:
                tmp_log_file = get_tmp_log_file()                
                process = subprocess.Popen(args=self._args, cwd=self._cwd, stdout=tmp_log_file, stderr=tmp_log_file, shell=True)
    #            process = subprocess.Popen(args=args, cwd="C:\\temp\\smartcard\\", stdout=None, stderr=None, shell=True)
                process.wait()
                
                
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("KeyMaker.initialization", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
                self._error_message = repr(evalue)
                self._running = False
                return
            finally:
                dump_output_to_log(self.checkpoint_handler, tmp_log_file)
            
    
            
            if process.returncode != 0:
                self._error_message = "Error : An error occurred during applet initialization. Please try again."

                self._running = False

        
    def is_running(self):
        return self._running
    
    def get_error(self):
        return self._error_message



class InitialiseProgress(threading.Thread):
    def __init__(self, user_interface_update, start_progress=0, end_progress=100):
        threading.Thread.__init__(self, name="InitialiseProgress")
        self._user_interface_update = user_interface_update
        self._start_progress = start_progress
        self._end_progress = end_progress
        self._progress = start_progress
        self._running = False
        self._has_started = False
        
    def run(self):
        while self._progress < self._end_progress:
            self._user_interface_update.set_info_progress_complete(self._progress)
            self._progress += 1
            time.sleep(1)
            

    def stop(self):
        self._progress = self._end_progress
        self._running = False
        
    def is_running(self):
        return self._running

    def has_started(self):
        return self._has_started


class KeyMakerOptions(components.config.common.ConfigKeyMaker):
    """
    Options and ini-file values for the Key Maker. 
    """
    def __init__(self):
        components.config.common.ConfigKeyMaker.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--config', type='string', default='./gon_key_maker.ini', help='configuration file')
        (self._options, self._args) = self._parser.parse_args()
        self.read_config_file(self._options.config)

    def cmd_show_version(self):
        return self._options.version


class KeyMaker(threading.Thread):
    STATE_READY = 0
    STATE_RUNNING = 1
    STATE_DONE = 2

    TOKEN_TYPE_soft_token = 0
    TOKEN_TYPE_micro_smart = 1
     
    
    def __init__(self, device_list, user_interface, token_plugin_socket, checkpoint_handler, set_state):
        threading.Thread.__init__(self, name="KeyMaker")
        self.user_interface = user_interface
        self.token_plugin_socket = token_plugin_socket
        self.checkpoint_handler = checkpoint_handler
        self.set_state = set_state
        self.device_list = device_list


    def expand_variables(self, arg, device):
        def get_replace_string(name, field_name):
            if name == "config" and field_name == "volume_letter":
                return device
            return None

        return lib.variable_expansion.expand(arg, get_replace_string)
                
    def expand_list_variables(self, args, device):
        return [self.expand_variables(arg, device) for arg in args]
        
                
    
    def run(self):
        self.set_state(GUIHandler.STATE_RUNNING)
        self.user_interface.update.set_info_headline('Looking for tokens')

        if self.device_list:
            device_list = self.device_list
        else:
            device_list = lib.hardware.device.Device.get_device_list() 

        if len(device_list) == 0:
            self.set_state(GUIHandler.STATE_DONE, "ERROR : No token found")
            return

#        if len(device_list) > 1:
#            self.set_state(GUIHandler.STATE_DONE, "ERROR : More than one token found: %s" % " ".join(device_list))
#            return

        self.user_interface.update.set_info_headline('')
        self.user_interface.update.set_info_progress_subtext('')
        self._done_message = ""
        for device in device_list:
            self.user_interface.update.set_info_progress_complete(0)
            with self.checkpoint_handler.CheckpointScope("KeyMaker.detect_token_type", module_id, lib.checkpoint.DEBUG):
                device = device.replace("\\", "")
                
    
    
                self.user_interface.update.set_info_headline("Checking token in %s" % device)
                
                
                token_type = GUIHandler.TOKEN_TYPE_soft_token
                msc_filename_probe = os.path.join(device, "giritech_probe.msc")
                try:
                    with lib.smartcard.msc.MSC(msc_filename_probe) as smart_card_transaction:
                        token_type = GUIHandler.TOKEN_TYPE_micro_smart
                except:
                    pass

            if token_type == GUIHandler.TOKEN_TYPE_micro_smart:
                
                self.user_interface.update.set_info_headline('G&D MSC Token')
                
                calc_and_labe_time = 1
                initialise_time = 40
                dismount_time = 1

                with self.checkpoint_handler.CheckpointScope("KeyMaker.calculate_time", module_id, lib.checkpoint.DEBUG):

                    self.user_interface.update.set_info_progress_subtext("Calculating remaining time")
                    
                    all_files = [f for f in os.walk(device, topdown=False)]
                    all_files_count = 1
                    for root, dirs, files in all_files:
                        all_files_count += len(dirs)
                        all_files_count += len(files)
                    
                    delete_files_time = all_files_count * 0.02
                
                    complete_time = calc_and_labe_time + initialise_time + dismount_time + delete_files_time
                    progress_per_second = 100.0 / complete_time
                    
                    calc_and_label_progress = calc_and_labe_time * progress_per_second
                    dismount_time_progress = dismount_time * progress_per_second
                    initalise_progress = initialise_time * progress_per_second
    
                    all_files_progress = delete_files_time * progress_per_second
                    each_file_progress = all_files_progress / all_files_count

                with self.checkpoint_handler.CheckpointScope("KeyMaker.labelling", module_id, lib.checkpoint.DEBUG):
                    try:
                        self.user_interface.update.set_info_progress_subtext('G&D MSC Token, labelling')
    
                        tool_config = key_maker_tools_config.get("label_volume_config")
                        args = self.expand_list_variables(tool_config.get("args"), device)
                        cwd = self.expand_variables(tool_config.get("cwd", "./"), device)
                        
                        tmp_log_file = get_tmp_log_file()
                        process = subprocess.Popen(args=args, cwd=cwd, stdout=tmp_log_file, stderr=tmp_log_file, shell=True)
                        process.wait()
                    except Exception, e:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("KeyMaker.label", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
                        self.set_state(GUIHandler.STATE_DONE, "Error : %s" % repr(e))
                        return
                    finally:
                        dump_output_to_log(self.checkpoint_handler, tmp_log_file)
                    
                    
                    self.user_interface.update.set_info_progress_complete(calc_and_label_progress)

                with self.checkpoint_handler.CheckpointScope("KeyMaker.initializing", module_id, lib.checkpoint.DEBUG):
                
                    self.user_interface.update.set_info_progress_subtext('G&D MSC Token, initialising')
    
                    # Initialize
                    tool_config = key_maker_tools_config.get("init_applet_config")
                    args = self.expand_list_variables(tool_config.get("args"), device)
                    cwd = self.expand_variables(tool_config.get("cwd", "./"), device)
                    
                    initialising_thread = InitialiseGDToken(args, cwd, self.checkpoint_handler)
                    initialising_thread.start()
                    
                    progress = self.user_interface.update.get_info_progress_complete()
                    seconds = 0
                    final_initialise_progress = progress + initalise_progress
                    while initialising_thread.is_alive():
                        try:
                            if progress < final_initialise_progress:
                                self.user_interface.update.set_info_progress_complete(progress)
                                progress += progress_per_second
                            time.sleep(1)
                        except:
                            (etype, evalue, etrace) = sys.exc_info()
                            self.checkpoint_handler.CheckpointException("KeyMaker.update_progress", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
                    
                    
                    if initialising_thread.get_error():
                        self.set_state(GUIHandler.STATE_DONE, initialising_thread.get_error())
                        return
                    
                    self.user_interface.update.set_info_progress_complete(final_initialise_progress)
                
                # Deleting files
                with self.checkpoint_handler.CheckpointScope("KeyMaker.cleaning", module_id, lib.checkpoint.DEBUG, file_count=all_files_count):
                
                    try:
                        self.user_interface.update.set_info_progress_subtext('G&D MSC Token, cleaning')
                        progress = self.user_interface.update.get_info_progress_complete()
                        final_delete_progress = progress + all_files_progress
                        for root, dirs, files in all_files:
                            for name in files:
                                os.chmod(os.path.join(root, name), stat.S_IWRITE)
                                os.remove(os.path.join(root, name))
                                progress += each_file_progress
                            for name in dirs:
                                os.rmdir(os.path.join(root, name))
                                progress += each_file_progress
                            if progress < final_delete_progress:
                                self.user_interface.update.set_info_progress_complete(progress)
                        
                        init_dir = os.path.join(device, 'gon_client', 'gon_init_micro_smart')
                        os.makedirs(init_dir)
                        
                        # Copy all files in extra_files to root
                        root_folder = './extra_files'
                        for filename in os.listdir(root_folder):
                            filename_dest = os.path.join(root_folder, filename)
                            shutil.copy(filename_dest, device)
                        
                        self.user_interface.update.set_info_progress_complete(final_delete_progress)
                    except Exception, e:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("KeyMaker.cleaning", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
                        self.set_state(GUIHandler.STATE_DONE, "Error : %s" % repr(e))
                        return


            else:
                self.set_state(GUIHandler.STATE_DONE, "ERROR : The token type in %s is not recognised" % device)
                continue
             
            
            with self.checkpoint_handler.CheckpointScope("KeyMaker.unmount", module_id, lib.checkpoint.DEBUG):
                try:
                    self.user_interface.update.set_info_progress_subtext('G&D MSC Token, unmounting')
                    #log_filename = 'release_manager.log' 
    #                release_log_file = open(release_log_filename, 'wt')                
                    tool_config = key_maker_tools_config.get("dismount_config")
                    args = self.expand_list_variables(tool_config.get("args"), device)
                    cwd = self.expand_variables(tool_config.get("cwd", "./"), device)
                    
                    tmp_log_file = get_tmp_log_file()
                    process = subprocess.Popen(args=args, cwd=cwd, stdout=tmp_log_file, stderr=tmp_log_file, shell=True)
                    process.wait()
                    
                    win32com.shell.shell.SHChangeNotify(win32com.shell.shellcon.SHCNE_DRIVEREMOVED, win32com.shell.shellcon.SHCNF_PATH, device)
                    win32file.DeleteVolumeMountPoint(device)
                    time.sleep(2)

                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("KeyMaker.unmount", module_id, lib.checkpoint.WARNING, etype, evalue, etrace)
                finally:
                    dump_output_to_log(self.checkpoint_handler, tmp_log_file)
            
            self.user_interface.update.set_info_progress_complete(100)
            self.set_state(GUIHandler.STATE_DONE)
            # Only one token can be initialized
            return

    def _calc_process_complete(self, max, idx):
        return (idx * 100) / max
    


class GUIHandler(object):
    STATE_READY = 0
    STATE_RUNNING = 1
    STATE_DONE = 2

    TOKEN_TYPE_soft_token = 0
    TOKEN_TYPE_micro_smart = 1
     
    
    def __init__(self, user_interface, token_plugin_socket, key_maker_config, checkpoint_handler):
        self.user_interface = user_interface
        self.token_plugin_socket = token_plugin_socket
        self.user_interface.update.subscribe(self._gui_on_update)
        self.checkpoint_handler = checkpoint_handler
        self._state = GUIHandler.STATE_READY
        self._gui_update_state()
        self.user_interface.update.set_banner(components.presentation.update.UpdateController.ID_BANNER_INSTALL)
        self.user_interface.update.set_phase_list([{'name': 'Keymaker', 'state': components.presentation.update.UpdateController.ID_PHASE_PENDING}])
        self.user_interface.update.display()
        self._done_message = ''
        self.key_maker_config = key_maker_config
        
        
    def _gui_update_state(self, message=None):
        if self._state == GUIHandler.STATE_READY:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline('Insert token')
            self.user_interface.update.set_info_free_text("")
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_cancel_description('Press Cancel to quit')
            self.user_interface.update.set_next_description('Press Next to initialize token')
        elif self._state == GUIHandler.STATE_RUNNING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_next_allowed(False)
        elif self._state == GUIHandler.STATE_DONE:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_info_free_text("")
            self.user_interface.update.set_cancel_description('Press Cancel to quit')
            self.user_interface.update.set_next_description('Press Next to restart')
            self.user_interface.update.set_info_headline('Done')
        if message:
            self.user_interface.update.set_info_free_text(message)
    
    def _gui_on_update(self):
        if self._state == GUIHandler.STATE_READY:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._run()
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self.user_interface.destroy()

        elif self._state == GUIHandler.STATE_DONE:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state = GUIHandler.STATE_READY
                self._gui_update_state()
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self.user_interface.destroy()
                
    def set_state(self, new_state, message=None):
        self._state = new_state
        self._gui_update_state(message)

    def _run(self):
        device_list=None
        if self.key_maker_config.device_list:
            device_list = [s.strip() for s in self.key_maker_config.device_list.split(",") if s] 
        key_maker = KeyMaker(device_list, self.user_interface, self.token_plugin_socket, self.checkpoint_handler, self.set_state)
        key_maker.start()
    
    

# The async_service is a global instance because it contains a async-io object
# that should be deleted at the very end.
async_service = None



def main():
    #
    # Parsing arguments from commandline and ini-file
    #
    key_maker_config = KeyMakerOptions()
    
    #
    # Show version and exit if requested
    #
    if(key_maker_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    #
    # Initialize logging
    #
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(key_maker_config.log_enabled,
                                                                      key_maker_config.log_verbose,
                                                                      key_maker_config.log_file,
                                                                      key_maker_config.is_log_type_xml())
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, module_id)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, module_id)


    #
    # Initialize async service
    #
    global async_service
    async_service = components.communication.async_service.AsyncService(checkpoint_handler, "key_maker", 1)

    #
    # Initialize userinterface
    #
    user_interface = components.presentation.user_interface.UserInterface(configuration=key_maker_config)

    with checkpoint_handler.CheckpointScope("main", module_id,
                                            lib.checkpoint.DEBUG,
                                            version=lib.version.Version.create_current().get_version_string(),
                                            build_date=lib.version.Version.create_current().get_build_date().isoformat(' '), argv=str(sys.argv)):

        #
        # Load plugins
        #
        additional_device_roots = []
        plugin_manager = components.plugin.client_gateway.manager.Manager(checkpoint_handler, key_maker_config.plugin_modules_path)
        plugins = plugin_manager.create_instances(async_service=async_service, checkpoint_handler=checkpoint_handler, user_interface=user_interface, additional_device_roots=additional_device_roots)
        token_plugin_socket = components.plugin.client_gateway.plugin_socket_token.PluginSocket(plugin_manager, plugins)
        client_runtime_env_plugin_socket = components.plugin.client_gateway.plugin_socket_client_runtime_env.PluginSocket(plugin_manager, plugins, checkpoint_handler)


        #
        # Start userinterface in main thread. This will interupt the mainthread until user_interface.start() terminates.
        #
        gui_handler = GUIHandler(user_interface, token_plugin_socket, key_maker_config, checkpoint_handler)
        async_service.start()
        user_interface.start()
            
        async_service.stop()


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)

    main()
