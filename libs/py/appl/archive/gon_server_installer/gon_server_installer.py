"""
This script will create/remove instances of the G/On Server (G/On Gateway Server and G/On Management Server) on the local server.

Create instances:
  Will create a instance of the G/On Servers, with the default instance id:
  $ gon_server_installer --create_full_instance
  
  Will create a instance of the G/On Servers, with the default instance id, without user interaction:
  $ gon_server_installer --create_full_instance --auto

  Will create a instance of the G/On Servers, with the instance id 2:
  $ gon_server_installer --create_full_instance --instance_id 2

  Will create a instance of the G/On Gateway Server using setup package:
  $ gon_server_installer --create_gateway_instance --gateway_setup_package /tmp/gon_server-5.6.0.11-1_gateway_setup_package.tar.bz2


Remove instances:
  Will remove a instance of the G/On Servers, with the instance id 2:
  $ gon_server_installer --remove_instance --instance_id 2
  
  
Generate gateway server setup package:
  Will generate a gateway server setup package for use when installing multiple gateway servers
  $ gon_server_installer --generate_gateway_setup_package
  

  
"""
import sys
import os
import optparse
import shutil
import string
import stat
import traceback
import threading
import subprocess
import tarfile

import components.config.common


GIRITECH_ROOT_ABS = os.path.join('/', 'opt', 'giritech')
GIRITECH_INSTANCE_ROOT_ABS = os.path.join(GIRITECH_ROOT_ABS, 'instance')
GIRITECH_INSTANCE_BACKUP_ROOT_ABS = os.path.join(GIRITECH_ROOT_ABS, 'instance_backup')



#
# Misc
#
def is_64bits():
    return sys.maxsize > 2**32    


def get_arch_target():
    if is_64bits():
        return 'linux_64'
    return 'linux'
    


def is_root(logger):
    if not os.geteuid()==0:
        logger.write_error("this need to be runned as root")
        return False
    return True


def generate_backup_folder(instance_id_full):
    def generate_folder_name(instance_id_full, n):
        if n == 0:
            return '%s' %(instance_id_full)
        return '%s(%d)' %(instance_id_full, n)
    
    if not os.path.exists(GIRITECH_INSTANCE_BACKUP_ROOT_ABS):
        os.makedirs(GIRITECH_INSTANCE_BACKUP_ROOT_ABS)
    
    n = 0
    backup_folder = os.path.join(GIRITECH_INSTANCE_BACKUP_ROOT_ABS, generate_folder_name(instance_id_full, n))
    while os.path.exists(backup_folder):
        n += 1
        backup_folder = os.path.join(GIRITECH_INSTANCE_BACKUP_ROOT_ABS, generate_folder_name(instance_id_full, n))
    return backup_folder


def get_server_name():
    command = ['uname', '-n']
    process = subprocess.Popen(command, stdout=subprocess.PIPE)
    process.wait()
    (stdoutdata, _) = process.communicate()
    return stdoutdata

#
#
#
class Commandline(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--create_full_instance', action='store_true', help='Create full G/On Server instance')
        self._parser.add_option('--remove_instance', action='store_true', help='Remove instance')
        self._parser.add_option('--instance_id', type='string', default='1')
        self._parser.add_option('--auto', action='store_true', help='Do not ask user, use default vaules')
        
        self._parser.add_option('--generate_gateway_setup_package', action='store_true', help='Generate a setup package for use in multiple gateway server setup')

        self._parser.add_option('--create_gateway_instance', action='store_true', help='Create G/On Gateway Servier instance')
        self._parser.add_option('--gateway_setup_package', type='string', default=None)
        
        (self._options, self._args) = self._parser.parse_args()
    
    def action_create_full_instance(self):
        return self._options.create_full_instance

    def action_create_gateway_instance(self):
        return self._options.create_gateway_instance

    def action_remove_instance(self):
        return self._options.remove_instance
    
    def action_generate_gateway_setup_package(self):
        return self._options.generate_gateway_setup_package

    def is_auto(self):
        return self._options.auto

    def get_instance_id(self):
        return self._options.instance_id
    
    def get_gateway_setup_package(self):
        return self._options.gateway_setup_package
    
    def get_soft_root(self):
        soft_root = None
        server_version_full = None
        if hasattr(sys, "frozen"):
            soft_root = os.path.normpath(os.path.join(os.path.dirname(sys.executable), '..', '..'))
            (_, server_version_full) = os.path.split(soft_root)
        else:
            soft_root = os.path.dirname(__file__) 
        return (soft_root, server_version_full)

    def get_instance_root(self):
        (soft_root, server_version_full) = self.get_soft_root()
        instance_id_full = server_version_full + "-" + self.get_instance_id()
        instance_root = os.path.normpath(os.path.join(GIRITECH_INSTANCE_ROOT_ABS, instance_id_full))
        return (instance_root, instance_id_full)

    def print_help(self):
        self._parser.print_help()


class ExceptionInstallationFail(Exception):
    def __init__(self, message):
        self.message = message
    
    def __str__(self):
        return repr(self.message)

    
class Logger(object):
    def __init__(self, command_line):
        self.command_line = command_line
        
    def write(self, message):
        print message

    def write_error(self, message):
        print "ERROR, %s" % message

    def write_warning(self, message):
        print "Warning, %s" % message


class Installer(object):
    def __init__(self, command_line, logger):
        self.command_line = command_line
        self.logger = logger
        
    def expand_in_file(self, from_filename, to_filename, dictionary):
        try:
            file_in = open(from_filename, 'r')
            file_content = string.Template(file_in.read()).safe_substitute(dictionary)
            file_in.close()

            file_out = open(to_filename, 'w')
            file_out.write(file_content)
            file_out.close()
            
        except ValueError:
            (etype, evalue, etrace) = sys.exc_info()
            print from_filename
            print evalue
        except KeyError:
            (etype, evalue, etrace) = sys.exc_info()
            print from_filename
            print evalue

    def install_configuration(self):
        self.logger.write("Installing configuration")
        (soft_root, server_version_full) = self.command_line.get_soft_root()
        (instance_root, instance_id_full) = self.command_line.get_instance_root()
        
        if not os.path.exists(instance_root):
            os.makedirs(instance_root)
        
        soft_config_root = os.path.join(soft_root, 'config')
        instance_config_root = os.path.join(instance_root, 'config')

        shutil.copytree(soft_config_root, instance_config_root)
        self.logger.write("Installing configuration, done")

    def copy_plugin_modules(self, src_base, dst_base):
        plugin_root_src = os.path.join(src_base, 'plugin_modules')
        plugin_root_dest = os.path.join(dst_base, 'plugin_modules')
        shutil.copytree(plugin_root_src, plugin_root_dest)

    def install_service(self, name):
        self.logger.write("Installing service %s" % name)

        (soft_root, _) = self.command_line.get_soft_root()
        (instance_root, instance_id_full) = self.command_line.get_instance_root()
        run_root = os.path.join(instance_root, name, 'linux')
        os.makedirs(run_root)

        self.copy_plugin_modules(os.path.join(soft_root, name, get_arch_target()), run_root)
        service_instance_id_full = instance_id_full + '-' + name
        
        dictionary = {}
        dictionary['GON_SERVICE_RUN_ROOT'] = os.path.join(soft_root, name, get_arch_target())
        dictionary['GON_SERVICE_INSTANCE_FULL_ID'] = service_instance_id_full
        dictionary['GON_SERVICE_INSTANCE_RUN_ROOT'] = run_root
        
        service_filename_template = os.path.join(soft_root, 'instance_templates', '%s.template' % name)
        service_filename = os.path.join('/', 'etc', 'init.d', service_instance_id_full)
        self.expand_in_file(service_filename_template, service_filename, dictionary)
        os.chmod(service_filename, stat.S_IXUSR | stat.S_IRUSR | stat.S_IWUSR)
        self.logger.write("Installing service %s, done" % name)

    def install_config_service(self):
        self.logger.write("Installing gon_config_service")

        (soft_root, _) = self.command_line.get_soft_root()
        (instance_root, _) = self.command_line.get_instance_root()
        run_root = os.path.join(instance_root, 'gon_config_service', 'linux')
        os.makedirs(run_root)

        gon_config_service_folder_abs = os.path.join(soft_root, 'gon_config_service', get_arch_target())
        gon_config_service_path_abs = os.path.join(gon_config_service_folder_abs, 'gon_config_service')
        self.copy_plugin_modules(gon_config_service_folder_abs, run_root)
        
        gon_config_path_abs = os.path.join(soft_root, 'gon_config', get_arch_target(), 'gon_config')
        gon_config_instance_path_abs =  os.path.join(instance_root, 'gon_config', 'linux', 'gon_config')
        
        os.makedirs(os.path.dirname(gon_config_instance_path_abs))
        gon_config_instance_launcher = open(gon_config_instance_path_abs, 'w')
        gon_config_instance_launcher.write('#!/bin/bash\n')
        gon_config_instance_launcher.write(gon_config_path_abs)
        gon_config_instance_launcher.write(' -gon_config_service_path ' + gon_config_service_path_abs)
        gon_config_instance_launcher.write(' -gon_config_service_working_folder ' + run_root)
        gon_config_instance_launcher.write('\n')
        gon_config_instance_launcher.close()
        os.chmod(gon_config_instance_path_abs, stat.S_IXUSR | stat.S_IRUSR | stat.S_IWUSR)
        self.logger.write("Installing gon_config_service, done")

    def remove_service(self, name):
        self.logger.write("Removing service %s" % name)

        (_, instance_id_full) = self.command_line.get_instance_root()
        service_instance_id_full = instance_id_full + '-' + name

        service_filename = os.path.join('/', 'etc', 'init.d', service_instance_id_full)
        if os.path.exists(service_filename):
            os.remove(service_filename)
            self.logger.write("Removing service %s, done" % name)
        else:
            self.logger.write_warning("service %s not found" % name)
    
    def remove_instance(self):
        self.logger.write("Removing instance")
        
        (instance_root, instance_id_full) = self.command_line.get_instance_root()
        baskup_path = generate_backup_folder(instance_id_full)
        shutil.move(instance_root, baskup_path)
        self.logger.write("Instance data has been saved in %s" % baskup_path)
        self.logger.write("Removing instance, done")


    def install_gateway_local_ini(self):
        (instance_root, _) = self.command_line.get_instance_root()

        gateway_server_instance_root = os.path.join(instance_root, 'gon_server_gateway_service', 'linux')
        config_local = components.config.common.ConfigServerGatewayLocal()
        
        config_local.service_title = get_server_name()
        config_local.write_config_file(gateway_server_instance_root, False)


    def _fix_config_ini(self, filename_base, xulrunner_root):
        if not os.path.exists(filename_base):
            return;
        filename = os.path.join(filename_base, get_arch_target(), 'configuration', 'config.ini')
        if os.path.exists(filename):
            config_file = open(filename, 'a')
            config_file.write("\n")
            config_file.write("org.eclipse.swt.browser.XULRunnerPath=%s" % xulrunner_root)
            config_file.close()
            self.logger.write("Updating xulrunner location of %s" % filename)


    def fix_xulrunner_location(self):
        xulrunner_root_found = False
        
        (soft_root, _) = self.command_line.get_soft_root()

        # locate xulrunner in giri location
        xulrunner_root = os.path.join(soft_root, '..', 'xulrunner', get_arch_target())
        if os.path.isdir(xulrunner_root):
            xulrunner_root_found = True
            
        # locate xulrunner in std location
        if not xulrunner_root_found:
            xulrunner_root = os.path.join('/usr', 'lib', 'xulrunner-2')
            if is_64bits():
                xulrunner_root = os.path.join('/usr', 'lib64', 'xulrunner-2')
            if os.path.isdir(xulrunner_root):
                xulrunner_root_found = True
        
        # Patch java configuration file
        xulrunner_root = os.path.normpath(xulrunner_root)
        if xulrunner_root_found:  
            self.logger.write("Fixing xulrunner, xulrunner found in %s" % xulrunner_root)
            gon_config_base = os.path.join(soft_root, 'gon_config')
            self._fix_config_ini(gon_config_base, xulrunner_root)
            
            gon_client_managemet_base = os.path.join(soft_root, 'gon_client_management')
            self._fix_config_ini(gon_client_managemet_base, xulrunner_root)

#
# Functionality for launching G/On Config
#
def launch_gon_config(command_line, logger):
    class GONConfigLauncher(threading.Thread):
        def __init__(self, gon_config_instance_path_abs):
            threading.Thread.__init__(self, name="GONConfigLauncher")
            self.gon_config_instance_path_abs = gon_config_instance_path_abs
            self.daemon = True
    
        def run(self):
            command = [self.gon_config_instance_path_abs]
            subprocess.call(command)
            
    if not command_line.is_auto():
        logger.write("")
        do_proceed = raw_input("Do you want to configure the installation now ? [YES],NO : ")
        if not (do_proceed.upper() == "YES" or do_proceed == ''):
            return

    logger.write("Launching G/On Config")
    (instance_root, _) = command_line.get_instance_root()
    
    arch_folder = 'linux'
    if is_64bits():
        arch_folder = 'linux_64'

    gon_config_instance_path_abs =  os.path.join(instance_root, 'gon_config', 'linux', 'gon_config')
    gon_config_launcher = GONConfigLauncher(gon_config_instance_path_abs)
    gon_config_launcher.start()
    logger.write("Launching G/On Config, done")
    
    

#
# Functionality for creating instances
#
def main_action_create_full_instance(command_line, logger):
    installer = Installer(command_line, logger)
    (instance_root, instance_id_full) = command_line.get_instance_root()
    if os.path.exists(instance_root):
        logger.write_error("instance %s already exists" % instance_id_full)
        logger.write_error("please use the option --instance_id to create a new instance")
        return 1
    
    logger.write("")
    logger.write("You are about to install G/On on your server.")
    if not command_line.is_auto():
        do_proceed = raw_input("Do you want to proceed installation ? [YES],NO : ")
        if not (do_proceed.upper() == "YES" or do_proceed == ''):
            logger.write("installation abandon by user")
            return 1
    try:
        installer.install_configuration()
        installer.install_service('gon_server_gateway_service')
        installer.install_service('gon_server_management_service')
        installer.install_gateway_local_ini()
        installer.install_config_service()
        installer.fix_xulrunner_location()
        
        launch_gon_config(command_line, logger)
        
    except ExceptionInstallationFail as e:
        logger.write_error('installation failed %s' % e.message)
        return 1
    except:
        logger.write_error('installation failed with unexpected error')
        traceback.print_exc()
        return 1
    return 0


#
# Functionality for creating gateway server only instance
#
def main_action_create_gateway_instance(command_line, logger):
    installer = Installer(command_line, logger)
    (instance_root, instance_id_full) = command_line.get_instance_root()
    if os.path.exists(instance_root):
        logger.write_error("instance %s already exists" % instance_id_full)
        logger.write_error("please use the option --instance_id to create a new instance")
        return 1

    package_filename = command_line.get_gateway_setup_package()
    if package_filename is None:
        logger.write_error("missing gateway setup package")
        return 1
    
    logger.write("")
    logger.write("You are about to install G/On Gateway on your server.")
    if not command_line.is_auto():
        do_proceed = raw_input("Do you want to proceed installation ? [YES],NO : ")
        if not (do_proceed.upper() == "YES" or do_proceed == ''):
            logger.write("installation abandon by user")
            return 1
    try:
        installer.install_configuration()
        installer.install_service('gon_server_gateway_service')
        installer.install_gateway_local_ini()
        installer.install_config_service()

        logger.write("Applying setup package")
        package_tarfile = tarfile.open(package_filename, mode='r:bz2')
        package_tarfile.extractall(path=instance_root)
        package_tarfile.close()
        logger.write("Applying setup package, done")
        
    except ExceptionInstallationFail as e:
        logger.write_error('installation failed %s' % e.message)
        return 1
    except:
        logger.write_error('installation failed with unexpected error')
        traceback.print_exc()
        return 1
    return 0

#
# Functionality for creating instances
#
def main_action_remove_instance(command_line, logger):
    installer = Installer(command_line, logger)
    (instance_root, instance_id_full) = command_line.get_instance_root()
    if not os.path.exists(instance_root):
        logger.write_error("instance %s not found" % instance_id_full)
        logger.write_error("please use the option --instance_id to specify existing instance")
        return 1
    
    logger.write("")
    
    logger.write("You are about to REMOVE a G/On Server instance %s" % instance_id_full)
    do_proceed = raw_input("Do you want to proceed ? YES,[NO] : ")
    if not (do_proceed.upper() == "YES" or do_proceed != ''):
        logger.write("remove of instance abandon by user")
        return 1

    try:
        installer.remove_service('gon_server_gateway_service')
        installer.remove_service('gon_server_management_service')
        installer.remove_instance()
        
    except ExceptionInstallationFail as e:
        logger.write_error('removal of instance failed %s' % e.message)
        return 1
    except:
        logger.write_error('removal of instance failed with unexpected error')
        traceback.print_exc()
        return 1
    return 0
    

#
# Functionality generating gateway setup package
#
def main_action_generate_gateway_setup_package(command_line, logger):
    (instance_root, instance_id_full) = command_line.get_instance_root()
    if not os.path.exists(instance_root):
        logger.write_error("instance %s not found" % instance_id_full)
        logger.write_error("please use the option --instance_id to specify existing instance")
        return 1

    logger.write("")
    logger.write("Generating G/On Gateway Server setup package")
    
    package_filename = os.path.join(instance_root, '%s_gateway_setup_package.tar.bz2' % instance_id_full)
    package_tarfile = tarfile.open(package_filename, mode='w:bz2')

    filenames_to_include = [os.path.join('config', 'deployed', 'gon_service_management_client.ks'),
                            os.path.join('config', 'deployed', 'gon_license.lic'),
                            os.path.join('config', 'deployed', 'gon_client.servers'),
                            os.path.join('config', 'deployed', 'gon_client.ks'),
                            os.path.join('config', 'deployed', 'gon_server.ks'),
                            os.path.join('config', 'gon_server_gateway.filedefs.xml'),
                            os.path.join('gon_server_gateway_service', 'linux', 'gon_server_gateway.ini')
                            ]
    for filename_to_include in filenames_to_include:
        if os.path.exists(os.path.join(instance_root, filename_to_include)):
            package_tarfile.add(os.path.join(instance_root, filename_to_include), filename_to_include)

    package_tarfile.close()
    
    logger.write("Generate package is located here %s" % package_filename)
    logger.write("Generating G/On Gateway Server setup package, done")


#
# Main
#
def main():
    command_line = Commandline()
    logger = Logger(command_line)

    logger.write("G/On Server Installer for linux")
    if not is_root(logger):
        return 1
    
    if command_line.action_create_full_instance():
        return main_action_create_full_instance(command_line, logger)
    elif command_line.action_create_gateway_instance():
        return main_action_create_gateway_instance(command_line, logger)
    elif command_line.action_remove_instance():
        return main_action_remove_instance(command_line, logger)
    elif command_line.action_generate_gateway_setup_package():
        return main_action_generate_gateway_setup_package(command_line, logger)

    print __doc__
    command_line.print_help()

    return 0

if __name__ == '__main__':
    main()
