import cx_Freeze
import lib.appl_plugin_module_util
import shutil
import sys

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()


import os
print "PATH PATH", os.getcwd()

py_root = os.path.abspath(os.path.join('..', '..'))

extra_files = [
]

#
# Additional so-files for additional linux support
#
extra_files.extend([
])
extra_files = lib.appl_plugin_module_util.switch_destination_and_source(extra_files)

exclude_modules = [
  'lib_cpp.communication.ext_mac.communication_ext'
]

is_64bits = sys.maxsize > 2**32
if is_64bits:
    exclude_modules.append('lib_cpp.communication.ext_linux.communication_ext')
else:
    exclude_modules.append('lib_cpp.communication.ext_linux_64.communication_ext')
    


build_options = {
  'compressed' : True,
  'include_files' : extra_files,
  'optimize' : 0,
  'build_exe' : dev_env.generate_build_pack_destination('gon_server_installer'),
  'excludes' : exclude_modules,
  'silent' : True
}

exe_instance = cx_Freeze.Executable('gon_server_installer.py')

cx_Freeze.setup (
    name = "G/On Server Installer",
    version = dev_env.version.get_version_string_num(),
    description = 'G/On Server Installer for linux',
    executables = [ exe_instance ],
    options = {'build_exe': build_options }
)

