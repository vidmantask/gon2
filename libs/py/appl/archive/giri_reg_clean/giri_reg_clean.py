"""
This utility removes entries in the windows registry caused by inserting hagi-keys.

The relevant registry entries has been found using reverse ingenereing, and
are thus not garentied to work in all environments. 

The keys are found in two ways:
1 the smart way:
    Find all entries under 'Control\DeviceClasses' that has a sub-sub entry containing hagi or removeable, e.g.
       # [HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\DeviceClasses\{53f5630d-b6bf-11d0-94f2-00a0c91efb8b}\##?#STORAGE#RemovableMedia#7&139679d9&0&RM#{53f5630d-b6bf-11d0-94f2-00a0c91efb8b}
       # [HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Control\DeviceClasses\{53f56308-b6bf-11d0-94f2-00a0c91efb8b}\##?#USBSTOR#CdRom&Ven_HAGIWARA&Prod_UD-ROM&Rev_1.0F#005059140000097&0#{53f56308-b6bf-11d0-94f2-00a0c91efb8b}]
    each of this has the attirbute 'DeviceInstance' whis holds a reference to a relevant storage-entry, e.g.
       # "DeviceInstance"="STORAGE\\RemovableMedia\\7&139679d9&0&RM"
    each of the storage-entries holds the attribute 'Driver' whitch holds a reference to a relevant driver-entry, e.g.
       # [HKEY_LOCAL_MACHINE\SYSTEM\ControlSet001\Enum\USBSTOR\Disk&Ven_HAGIWARA&Prod_UD-RAM&Rev_1.0F\005059140000097&1]
       # "Driver"="{4D36E967-E325-11CE-BFC1-08002BE10318}\\0046"

2 the brute force way:   
    Find all entires under 'Enum\USBSTOR' that contains hagi 
    Find all entries under 'Enum\STORAGE\RemoveableMedia'

"""
import _winreg as winreg
import optparse


def get_subkey_with_id(key, subkey_id):
    subkeys = []
    for i in xrange(1000):
        try:
            sub_key = winreg.EnumKey(key, i)
            if not sub_key.lower().find(subkey_id.lower()) == -1:
                subkeys.append(sub_key)
        except EnvironmentError:
            break
    return subkeys

def get_value_from_key(key, value_id_to_find):
    result = None
    for i in xrange(1000):
        try:
            (value_id, value, type) = winreg.EnumValue(key, i)
            if value_id == value_id_to_find:
                result = value
        except EnvironmentError:
            break

    return result


def get_value(key, sub_key_id, value_id_to_find):
    result = None
    sub_key = winreg.OpenKey(key, sub_key_id)
    for i in xrange(1000):
        try:
            (value_id, value, type) = winreg.EnumValue(sub_key, i)
            if value_id == value_id_to_find:
                result = value
        except EnvironmentError:
            break
        finally:
            winreg.CloseKey(sub_key)

    winreg.CloseKey(sub_key)
    return result



def get_driver(key_ctrlset, enum_entry):
    try:
        a_reg_key = winreg.OpenKey(key_ctrlset, enum_entry)
        return get_value_from_key(a_reg_key, 'Driver')
    except EnvironmentError:
        return None


def delete_all_subkeys(del_key):
    while(True):
        try:
            sub_key_id = winreg.EnumKey(del_key, 0)
            sub_key = winreg.OpenKey(del_key, sub_key_id, 0, winreg.KEY_ALL_ACCESS)
            delete_all_subkeys(sub_key)
            show_all_subkeys(sub_key)
            winreg.CloseKey(sub_key)
            
            try:
                print 'subkey-delete' , sub_key_id
                winreg.DeleteKey(del_key, sub_key_id)
            except EnvironmentError:
                print "Error unable to delete key '%s'" % sub_key_id 
            finally:
                winreg.CloseKey(sub_key)

        except EnvironmentError:
            break
    
def show_all_subkeys(del_key):
    for i in xrange(1000):
        try:
            sub_key_id = winreg.EnumKey(del_key, i)
        except EnvironmentError:
            break
    
    
    
def delete_key_id(a_reg, key_id):
    try:
        a_reg_del = winreg.OpenKey(a_reg, key_id, 0, winreg.KEY_ALL_ACCESS)
        delete_all_subkeys(a_reg_del)
        show_all_subkeys(a_reg_del)
        winreg.CloseKey(a_reg_del)
        try:
            print 'key-delete' , key_id
            winreg.DeleteKey(a_reg, key_id)
        except EnvironmentError:
            print "Error unable to delete key '%s'" % key_id 
        finally:
            winreg.CloseKey(a_reg_del)
    except EnvironmentError:
        print "Error unable to open key '%s' for delete" % key_id 
    



def lookup_with_hagi_Control_DeviceClasses(key_ctrlset):
    """
    Smart entry lookup
    """
    key = winreg.OpenKey(key_ctrlset, r'Control\DeviceClasses')
    
    found_hagi_sub_keys = []
    temp_found_storage_entry = []
    
    for i in xrange(1000):
        try:
            sub_key_id = winreg.EnumKey(key, i)
            a_sub_key = winreg.OpenKey(key, sub_key_id)

            subkeys_hagi   = get_subkey_with_id(a_sub_key, 'HAGI') 
            subkeys_remove = get_subkey_with_id(a_sub_key, 'RemovableMedia')

            if(len(subkeys_remove)>0 or len(subkeys_hagi)>0):
                for subkey in subkeys_hagi:
                    found_hagi_sub_keys.append(r'Control\DeviceClasses\%s\%s' % (sub_key_id,subkey))
                    temp_found_storage_entry.append(get_value(a_sub_key, subkey, 'DeviceInstance'))
                 
                for subkey in subkeys_remove:
                    found_hagi_sub_keys.append(r'Control\DeviceClasses\%s\%s' % (sub_key_id,subkey))
                    temp_found_storage_entry.append(get_value(a_sub_key, subkey, 'DeviceInstance'))

        except EnvironmentError:
            break
        finally:
            winreg.CloseKey(a_sub_key)

    found_storage_entry = []
    for storage_entry in temp_found_storage_entry:
        found_storage_entry.append(r'Enum\%s' % storage_entry)

    found_storage_entry_drivers = []
    for entry in found_storage_entry:
        driver = get_driver(a_reg_ctrlset, entry)
        if(driver): found_storage_entry_drivers.append(r'Control\Class\%s' % driver)

    winreg.CloseKey(key)
    return (found_hagi_sub_keys, found_storage_entry, found_storage_entry_drivers)


def lookup_with_hagi_Enum_USBSTOR(key_ctrlset):
    """
    Brute force lookup in usbstor
    """
    temp_found_storage = []
    key = winreg.OpenKey(key_ctrlset, r'Enum\USBSTOR')
    temp_found_storage += get_subkey_with_id(key, 'HAGI') 
    winreg.CloseKey(key)

    found_usbstor = []
    for storage_entry in temp_found_storage:
        found_usbstor.append(r'Enum\USBSTOR\%s' % storage_entry)

    return found_usbstor


def lookup_with_hagi_Enum_STORAGE(key_ctrlset):
    """
    Brute force lookup in storage
    """
    temp_found_storage = []
    key = winreg.OpenKey(key_ctrlset, r'Enum\STORAGE\RemovableMedia')
    for i in xrange(1000):
        try:
            sub_key_id = winreg.EnumKey(key, i)
            temp_found_storage.append(sub_key_id)
        except EnvironmentError:
            break

    winreg.CloseKey(key)

    found_storage = []
    for storage_entry in temp_found_storage:
        found_storage.append(r'Enum\STORAGE\RemovableMedia\%s' % storage_entry)
    return found_storage




if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option("--show", action="store_true", dest='show_entries', help='Show found entries.')
    parser.add_option("--delete", action="store_true", dest='do_delete', help='Delete entry in registry.')
    parser.add_option("--brute_force", action="store_true", dest='use_brute_force', help='Use a brute force method to find entries, it might be .... uhhhhhh.')
    (options, args) = parser.parse_args()


    reg_ctrlset_id = r'SYSTEM\CurrentControlSet'
    a_reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)
    a_reg_ctrlset = winreg.OpenKey(a_reg, reg_ctrlset_id, 0, winreg.KEY_ALL_ACCESS)

    # Smart search
    (found_hagi_sub_keys, found_storage_entry, found_storage_entry_drivers) = lookup_with_hagi_Control_DeviceClasses(a_reg_ctrlset)
    reg_entry_to_delete = found_hagi_sub_keys + found_storage_entry + found_storage_entry_drivers 

    # Bruteforce search
    if options.use_brute_force:
        found_usbstor = lookup_with_hagi_Enum_USBSTOR(a_reg_ctrlset)
        reg_entry_to_delete += found_usbstor

        found_storage = lookup_with_hagi_Enum_STORAGE(a_reg_ctrlset)
        reg_entry_to_delete += found_storage


    print "  Found %i relevant entries" % len(reg_entry_to_delete) 
    if options.show_entries:
        for x in reg_entry_to_delete:
            print "  ", x

    if options.do_delete:
        for x in reg_entry_to_delete:
            delete_key_id(a_reg_ctrlset, '%s' %(x))

    winreg.CloseKey(a_reg_ctrlset)
    winreg.CloseKey(a_reg)