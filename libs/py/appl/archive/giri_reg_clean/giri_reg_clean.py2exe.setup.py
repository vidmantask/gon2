import lib.dev_env.path_setup
from distutils.core import setup
import py2exe

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()

#
#call: python setup.py py2exe
#
#setup(console=['giri_reg_clean.py'], options=dict( packages = dict( items = []), includes = dict( items = []) ))

setup(console=['giri_reg_clean.py', { "script":'giri_reg_clean.py'} ], 
      options = { "py2exe": {"dll_excludes": [], "dist_dir" : dev_env.generate_pack_destination('giri_reg_clean'), "packages":[], "includes":[],} },
      #packages=["."],
      )
