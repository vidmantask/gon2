"""
The G/On Client Installer
"""
from __future__ import with_statement
import sys
import optparse
import threading
import time
import os
import os.path
import ConfigParser
import tempfile

import lib.version
import lib.checkpoint
import lib.appl.gon_client
import lib.appl.shortcut
import lib.gpm.gpm_env
import lib.gpm.gpm_analyzer
import lib.gpm.gpm_builder

import components.dictionary.common

import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_client_runtime_env
import plugin_types.client_gateway.plugin_type_client_runtime_env

import components.plugin.client_gateway.manager
import components.plugin.client_gateway.plugin_socket_token
import plugin_types.client_gateway.plugin_type_token

import components.communication.async_service


import components.config.common
import components.presentation.user_interface

from components.presentation.update import UpdateController as gui_update_controller

import lib.appl.io_hooker
import lib.appl.crash.handler


#
# Imports used in plugins
#
import lib.config
import components.communication.message
import plugin_types.client_gateway.plugin_type_token
import plugin_types.client_gateway.plugin_type_auth
import plugin_types.client_gateway.plugin_type_client_runtime_env
import plugin_types.client_gateway.plugin_type_endpoint
import plugin_types.client_gateway.plugin_type_token
import lib.hardware.windows_security_center

MODULE_ID = "appl_gon_client_installer"


GPM_DEPLOY_ID = 'gon_client_deploy'
GPM_UNINSTALL_ID = 'gon_client_uninstaller'

GPM_DEPLOY_CONTENT_FILENAME_SERVERS = 'gon_client/gon_temp/gon_client.servers'
GPM_DEPLOY_CONTENT_FILENAME_KS = 'gon_client/gon_temp/gon_client.ks'


class ClientInstallerOptions(components.config.common.ConfigClientInstaller):
    """
    Options and ini-file values for the Key Maker.
    """
    def __init__(self):
        components.config.common.ConfigClientInstaller.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--config', type='string', default='./gon_client_installer.ini', help='configuration file')
        self._parser.add_option('--installer_folder', type='string', default=None, help='Path of installer that launches this installer')
        (self._options, self._args) = self._parser.parse_args()

        ini_filename = self._options.config
        if self._options.installer_folder is not None:
            ini_filename_from_installer = os.path.join(self._options.installer_folder, 'gon_client_installer.ini')
            if os.path.isfile(ini_filename_from_installer):
                ini_filename = ini_filename_from_installer
        self.read_config_file(ini_filename)

    def cmd_show_version(self):
        return self._options.version

    def get_cpm_gpms_path_abs(self):
        if self._options.installer_folder is not None:
            cpm_gpms_path_abs = self._options.installer_folder
            if sys.platform == 'win32':
                if not cpm_gpms_path_abs.endswith('\\'):
                    cpm_gpms_path_abs += '\\'
            return cpm_gpms_path_abs
        return os.path.abspath(self.cpm_gpms_path)

    def get_plugin_modules_path_abs(self):
        return os.path.abspath(self.plugin_modules_path)

    def get_gui_image_path_abs(self):
        return os.path.abspath(self.gui_image_path)

    def get_dictionary_path_abs(self):
        return os.path.abspath(self.dictionary_path)

class TaskDiscoverControler(threading.Thread):
    """
    Controler that handles the discovery of installation destinations
    """
    def __init__(self, checkpoint_handler, dictionary, client_installer_config, plugin_socket_token, gui_controler):
        threading.Thread.__init__(self, name="TaskDiscoverControlerThread.run")
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.client_installer_config = client_installer_config
        self.plugin_socket_token = plugin_socket_token
        self.gui_controler = gui_controler
        self._running = False
        self._tokens = []

    def run(self):
        with self.checkpoint_handler.CheckpointScope("TaskDiscoverControlerThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self._running = True
                time.sleep(1)
                self._tokens = self.plugin_socket_token.get_tokens()
                time.sleep(1)
                self._running = False
                self.gui_controler.cb_state_discovering_done()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("TaskDiscoverControlerThread.run", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                self.gui_controler.cb_state_error(str(evalue))
            self._running = False

    def is_running(self):
        return self._running

    def _create_selection_element(self, id, token_title, runtime_env_id=None, token_type=None, token_id=None, is_computer_user_token=False, icon_filename=''):
        install_enabled = True
        details = ''
        if sys.platform == 'win32':
            import ctypes
            if not ctypes.windll.shell32.IsUserAnAdmin() and token_type == 'hagiwara':
                install_enabled = False
                details = self.dictionary._('You need administrator privileges to install on this token')

        return {'id': id,
                'selected': False,
                'icon': icon_filename,
                'name': token_title,
                'details': details,
                'description': '',
                'runtime_env_id': runtime_env_id,
                'token_type': token_type,
                'token_id': token_id,
                'is_computer_user_token' : is_computer_user_token,
                'install_enabled' : install_enabled
                }

    def get_selection_list(self):
        selection_id = -1
        if self.is_running():
            return []
        selection_list = []

        if self.client_installer_config.dicover_tokens_enabled:
            for token in self._tokens:
                if not (token.runtime_env_id is None or token.token_is_endpoint):
                    selection_id += 1
                    selection_list.append(self._create_selection_element(selection_id, token.get_token_title(), token.runtime_env_id, token.token_type, token.token_id, icon_filename=token.get_image_filename(image_path=self.client_installer_config.get_gui_image_path_abs())))

        if self.client_installer_config.dicover_desktop_enabled:
            selection_id += 1
            import plugin_modules.endpoint.client_gateway_common
            icon_filename = plugin_types.client_gateway.plugin_type_token.Token.get_token_image_filename('endpoint', plugin_types.client_gateway.plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, image_path=self.client_installer_config.get_gui_image_path_abs())
            token_title = plugin_modules.endpoint.client_gateway_common.get_token_title()
            new_token_title = self.dictionary._('New %s') % token_title
            selection_list.append(self._create_selection_element(selection_id, new_token_title, is_computer_user_token=True, icon_filename=icon_filename))

        return selection_list



class TaskInstallControler(threading.Thread, plugin_types.client_gateway.plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
    """
    Controler that handles the installation of packages
    """
    STATE_INIT = 0
    STATE_DESTINATION_EXAMINE_CLEAN = 1
    STATE_DESTINATION_EXAMINE_CLEAN_DONE = 2
    STATE_DESTINATION_EXAMINE_INSTALL_SUPPORT = 3
    STATE_DESTINATION_EXAMINE_INSTALL_SUPPORT_DONE = 4
    STATE_INSTALLING = 5
    STATE_INITIALIZE_TOKEN = 6
    STATE_PREPARING_RESTART = 8
    STATE_DONE = 9
    STATE_CANCELING = 10
    STATE_CANCELED = 11
    STATE_ERROR = 12

    def __init__(self, checkpoint_handler, dictionary, client_installer_config, plugin_socket_token, gui_controler, runtime_env, token_type, token_id, is_computer_user_token):
        threading.Thread.__init__(self, name="TaskInstallControlerThread.run")
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.client_installer_config = client_installer_config
        self.plugin_socket_token = plugin_socket_token
        self.gui_controler = gui_controler
        self.runtime_env = runtime_env
        self.token_type = token_type
        self.token_id = token_id
        self.is_computer_user_token = is_computer_user_token
        self.generate_keypair = True
        self._state = TaskInstallControler.STATE_INIT

        self._gpm_meta = []
        self._gpm_meta_installed = []

    def run(self):
        with self.checkpoint_handler.CheckpointScope("TaskInstallControlerThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            try:
                self.gui_controler.cb_progress(1, self.dictionary._('Examining destination for existing installation'))
                self._state_destination_examine_clean_start()
                while self._state == TaskInstallControler.STATE_DESTINATION_EXAMINE_CLEAN:
                    time.sleep(1)

                if self._state not in [TaskInstallControler.STATE_CANCELING, TaskInstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(5, self.dictionary._('Examining packages to be installed'))
                    self._state_destination_examine_install_support_start()
                    while self._state == TaskInstallControler.STATE_DESTINATION_EXAMINE_INSTALL_SUPPORT:
                        time.sleep(1)

                if self._state not in [TaskInstallControler.STATE_CANCELING, TaskInstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(10, self.dictionary._('Installing'))
                    self._state_install_start()

                if self._state not in [TaskInstallControler.STATE_CANCELING, TaskInstallControler.STATE_ERROR]:
                    self.gui_controler.cb_progress(95, self.dictionary._('Preparing restart'))
                    self._state_prepare_restart_start()

                if self._state not in [TaskInstallControler.STATE_CANCELING, TaskInstallControler.STATE_ERROR]:
                    self._state = TaskInstallControler.STATE_DONE
                    self.gui_controler.cb_progress(100)
                    self.gui_controler.cb_state_installation_done()

                if self._state in [TaskInstallControler.STATE_CANCELING]:
                    self._state = TaskInstallControler.STATE_CANCELED
                    self.gui_controler.cb_state_installation_canceled()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("TaskInstallControlerThread.run", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)
                self.gui_controler.cb_state_error(self.dictionary._('Internal error'))
                self._state = TaskInstallControler.STATE_ERROR

    def is_running(self):
        return self._state not in [TaskInstallControler.STATE_DONE, TaskInstallControler.STATE_ERROR, TaskInstallControler.STATE_CANCELED]

    def _state_destination_examine_clean_start(self):
        self._state = TaskInstallControler.STATE_DESTINATION_EXAMINE_CLEAN
        runtime_env_info = self.runtime_env.get_info()
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        self._gpm_meta = self.runtime_env.get_installed_gpm_meta_all(error_handler)
        if error_handler.error():
            self.checkpoint_handler.CheckpointMultilineMessage("TaskInstallControlerThread.error", MODULE_ID, lib.checkpoint.ERROR, error_handler.dump_as_string())
            self.gui_controler.cb_state_error(error_handler.dump_as_string(1))
            self._state = TaskInstallControler.STATE_ERROR
            return

        self.generate_keypair = True
        if len(self._gpm_meta) > 0:
            self.generate_keypair = self.client_installer_config.keys_generate_keypair_enabled
            if self.generate_keypair:
                self.gui_controler.user_interface.modalmessage.set_text(self.dictionary._('<b>The token already contains a G/On Client installation</b><br><br>Warning: If you click OK, the token will need to be re-enrolled before it can be used. Please check with your administrator, before clicking OK.'))
            else:
                self.gui_controler.user_interface.modalmessage.set_text(self.dictionary._('<b>The token already contains a G/On Client installation</b><br>'))
            self.gui_controler.user_interface.modalmessage.display()
            self.gui_controler.user_interface.modalmessage.subscribe(self._state_destination_examine_clean_on_update)
        else:
            self._state = TaskInstallControler.STATE_DESTINATION_EXAMINE_CLEAN_DONE

    def _state_destination_examine_clean_on_update(self):
        if self.gui_controler.user_interface.modalmessage.get_next_clicked():
            self._state = TaskInstallControler.STATE_DESTINATION_EXAMINE_CLEAN_DONE
        elif self.gui_controler.user_interface.modalmessage.get_cancel_clicked():
            self._state = TaskInstallControler.STATE_CANCELING

    def _state_destination_examine_install_support_start(self):
        self._state = TaskInstallControler.STATE_DESTINATION_EXAMINE_INSTALL_SUPPORT
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()

        self._gpm_meta_install = lib.gpm.gpm_builder.query_meta_all_from_gpm_files(self.client_installer_config.get_cpm_gpms_path_abs(), error_handler)
        gpm_meta = self._gpm_meta + self._gpm_meta_installed
        self._gpm_ids_install = []

        ignore_packages = [GPM_DEPLOY_ID]
        if not self.is_computer_user_token:
            ignore_packages.append(GPM_UNINSTALL_ID)

        for gpm_spec in self._gpm_meta_install:
            if gpm_spec.header.name not in ignore_packages:
                self._gpm_ids_install.append(gpm_spec.header.get_package_id())
        if len(self._gpm_ids_install) == 0:
            self.gui_controler.cb_state_error(self.dictionary._('No package found to be installed'))
            self._state = TaskInstallControler.STATE_ERROR
            return

        self._gpm_ids_remove = []
        for gpm_spec in self._gpm_meta:
            self._gpm_ids_remove.append(gpm_spec.header.get_package_id())

        gpms_meta = {}
        for gpm_meta in self._gpm_meta:
            meta_info = {}
            meta_info['contains_ro_files'] = gpm_meta.contains_ro_files()
            gpms_meta[gpm_meta.header.get_package_id()] = meta_info

        knownsecret = "x" # Just to indicate that knocnsecret is to be updated (shoud be not None)
        servers     = "x" # Just to indicate that servers is to be updated (shoud be not None)
        support_install = self.runtime_env.support_install_gpms(self._gpm_ids_install, [], self._gpm_ids_remove, gpms_meta, None, self.dictionary, knownsecret, servers)
        if not support_install['supported']:
            self.gui_controler.cb_state_error(support_install['message'])
            self._state = TaskInstallControler.STATE_ERROR
            return
        self._state = TaskInstallControler.STATE_DESTINATION_EXAMINE_INSTALL_SUPPORT_DONE

    def _state_install_start(self):
        self.checkpoint_handler.Checkpoint("TaskInstallControlerThread.install", MODULE_ID, lib.checkpoint.DEBUG, install_ids=str(self._gpm_ids_install), remove_ids=str(self._gpm_ids_remove))
        self._state = TaskInstallControler.STATE_INSTALLING

        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reposetory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reposetory.init_from_gpms(error_handler, self.client_installer_config.get_cpm_gpms_path_abs(), self.client_installer_config.get_cpm_gpms_path_abs())
        (installed, gon_client_deploy_package_id) = gpm_reposetory.query_package(GPM_DEPLOY_ID, None, [])
        if gon_client_deploy_package_id is None:
            self.gui_controler.cb_state_error(self.dictionary._("Unable to find the gpm-package for '%s' neede during installation.") % GPM_DEPLOY_ID)
            self._state = TaskInstallControler.STATE_ERROR
            return
        package_filename = os.path.join(self.client_installer_config.get_cpm_gpms_path_abs(), '%s.gpm' % gon_client_deploy_package_id )
        servers = lib.gpm.gpm_builder.query_file_content_from_package(package_filename, GPM_DEPLOY_CONTENT_FILENAME_SERVERS, error_handler)
        knownsecret = lib.gpm.gpm_builder.query_file_content_from_package(package_filename, GPM_DEPLOY_CONTENT_FILENAME_KS, error_handler)
        if error_handler.error():
            self.checkpoint_handler.CheckpointMultilineMessage("TaskInstallControlerThread.error", MODULE_ID, lib.checkpoint.ERROR, error_handler.dump_as_string())
            self.gui_controler.cb_state_error(error_handler.dump_as_string(1))
            self._state = TaskInstallControler.STATE_ERROR
            return

        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        self.runtime_env.set_download_root(self.client_installer_config.get_cpm_gpms_path_abs())
        self.runtime_env.install_gpms_clean(self._gpm_ids_install, self, error_handler, self.dictionary, knownsecret, servers, self.generate_keypair)
        if error_handler.error():
            self.checkpoint_handler.CheckpointMultilineMessage("TaskInstallControlerThread.error", MODULE_ID, lib.checkpoint.ERROR, error_handler.dump_as_string())
            self.gui_controler.cb_state_error(error_handler.dump_as_string(1))
            self._state = TaskInstallControler.STATE_ERROR
            return

        if error_handler.warning():
            self.checkpoint_handler.CheckpointMultilineMessage("TaskInstallControlerThread.warning", MODULE_ID, lib.checkpoint.WARNING, error_handler.dump_as_string())

    def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
        self._gui_calc_progress_start(10, 95, tasks_ticks_count)

    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        self._gui_action = task_title

    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        pass

    def gpm_installer_cb_action_tick(self, ticks):
        self.gui_controler.cb_progress(self._gui_calc_progress(ticks), self._gui_action)

    def gpm_installer_cb_action_done(self):
        pass

    def gpm_installer_cb_task_done(self):
        pass

    def gpm_installer_cb_task_all_done(self):
        self._state = TaskInstallControler.STATE_DONE

    def gpm_installer_do_cancel(self):
        return self._state == TaskInstallControler.STATE_CANCELING

    def _gui_calc_progress_start(self, progress_begin=0, progress_end=90, ticks_max=100):
        self._gui_progress_begin = progress_begin
        self._gui_progress_end = progress_end
        self._gui_ticks = 0
        self._gui_ticks_max = ticks_max

    def _gui_calc_progress(self, ticks):
        self._gui_ticks += ticks
        local_progress_pct = 1
        if self._gui_ticks_max > 0:
            local_progress_pct = (self._gui_ticks * 1.0) / self._gui_ticks_max
        local_progress = (self._gui_progress_end - self._gui_progress_begin) * local_progress_pct
        return self._gui_progress_begin + local_progress

    def do_cancel(self):
        self._state = TaskInstallControler.STATE_CANCELING

    def _state_prepare_restart_start(self):
        self._state = TaskInstallControler.STATE_PREPARING_RESTART
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reposetory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reposetory.init_from_gpms(error_handler, self.client_installer_config.get_cpm_gpms_path_abs(), self.client_installer_config.get_cpm_gpms_path_abs())

        (installed, gon_client_deploy_package_id) = gpm_reposetory.query_package(GPM_DEPLOY_ID, None, [])
        if gon_client_deploy_package_id is None:
            self.gui_controler.cb_state_error(self.dictionary._("Unable to find the gpm-package for '%s' neede during installation.") % GPM_DEPLOY_ID)
            self._state = TaskInstallControler.STATE_ERROR
            return
        package_filename = os.path.join(self.client_installer_config.get_cpm_gpms_path_abs(), '%s.gpm' % gon_client_deploy_package_id )
        if not error_handler.error():
            restart_state = lib.appl.gon_client.GOnClientInstallState()
            restart_state.set_restart_root(self.runtime_env.get_ro_root())
            if self.generate_keypair:
                restart_state.set_first_start_operation()
            restart_state.set_first_start_data(None)
            restart_handler = lib.appl.gon_client.GOnClientRestartHandler(self.checkpoint_handler, self.runtime_env.get_info().get_id(), self.runtime_env)
            restart_handler.set_gon_client_base_cwd(self.runtime_env.get_ro_root())
            restart_handler.set_gon_client_base_runtime_env(self.runtime_env)
            restart_handler.write_restart_state(self.checkpoint_handler, restart_state)
            if self.token_type in ['endpoint_token']:
                shortcuts = []
                (gon_client_root_relative, gon_client_cwd_relative, gon_client_filename) = lib.appl.gon_client.get_gon_client_root_relative()
                shortcut_target = os.path.join(self.runtime_env.get_root(), gon_client_root_relative, gon_client_filename)
                shortcut_name = os.path.basename(os.path.normpath(self.runtime_env.get_root()))
                shortcut = lib.appl.shortcut.create_shortcut(self.checkpoint_handler, 'Giritech', shortcut_name, shortcut_target)
                if shortcut is not None:
                    shortcuts.append(shortcut)
                gon_uninstaller = os.path.join(self.runtime_env.get_root(), 'gon_client_uninstaller.exe')
                if os.path.exists(gon_uninstaller):
                    gon_uninstaller_name = '%s Uninstall' % shortcut_name
                    gon_unistaller_target_args = '--install_root="%s"' % self.runtime_env.get_root()
                    shortcut_uninstaller = lib.appl.shortcut.create_shortcut(self.checkpoint_handler, 'Giritech', gon_uninstaller_name, gon_uninstaller, gon_unistaller_target_args)
                    if shortcut_uninstaller is not None:
                        shortcuts.append(shortcut_uninstaller)
                if len(shortcuts) != 0:
                    import plugin_modules.endpoint.client_gateway_common
                    endpoint_ginfo = plugin_modules.endpoint.client_gateway_common.EndpointGInfo()
                    serial = endpoint_ginfo.lookup_serial(plugin_modules.endpoint.client_gateway_common.EndpointToken.REG_KEY_INSTALLATION_LOCATION, self.runtime_env.get_root(), None)
                    if serial is not None:
                        shortcuts_string = ",".join(shortcuts)
                        endpoint_ginfo.write(serial, plugin_modules.endpoint.client_gateway_common.EndpointToken.REG_KEY_SHORTCUTS, shortcuts_string)
        else:
            self.checkpoint_handler.CheckpointMultilineMessage("TaskInstallControlerThread.error", MODULE_ID, lib.checkpoint.ERROR, error_handler.dump_as_string())
            self.gui_controler.cb_state_error(error_handler.dump_as_string(1))
            self._state = TaskInstallControler.STATE_ERROR
            return


    def get_runtime_env(self):
        return self.runtime_env


class GUIControler(object):
    """
    Handling of gui state and interaction with user
    """
    STATE_INIT = 0
    STATE_DISCOVERING = 1
    STATE_SELECTION_NO_TOKENS_FOUND = 2
    STATE_SELECTION_ONE_TOKEN_FOUND = 3
    STATE_SELECTION_MULTIBLE_TOKENS_FOUND = 4
    STATE_DESTINATION_INFO = 5
    STATE_DESTINATION = 6
    STATE_INSTALLING = 7
    STATE_CANCELLING = 8
    STATE_DONE = 9
    STATE_ERROR = 10
    STATE_CANCELED = 11

    def __init__(self, checkpoint_handler, dictionary, user_interface, client_installer_config, plugin_socket_token, plugin_socket_runtime_env):
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.user_interface = user_interface
        self.client_installer_config = client_installer_config
        self.plugin_socket_token = plugin_socket_token
        self.plugin_socket_runtime_env = plugin_socket_runtime_env
        self.user_interface.update.subscribe(self._gui_on_update)
        self._state = GUIControler.STATE_INIT
        self._gui_update_state()
        self.user_interface.update.set_banner(gui_update_controller.ID_BANNER_INSTALL)
        self.user_interface.update.display()
        self._state_discovering_start()

        self._state_error_message = ''
        self._state_selection_selected = None
        self._state_selection_list = []
        self._state_installation_canceling = False

        self._state_destination_sub_folder = None

        self._do_restart_flag = False

        self._task_install_controler = None

    def _gui_update_tasklist(self, selection_enabled):
        task_discovering = {'name': self.dictionary._('Detect'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_selection = {'name': self.dictionary._('Selection'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': selection_enabled}
        task_installing = {'name': self.dictionary._('Installation'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_done = {'name': self.dictionary._('Done'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': True}
        task_error = {'name': self.dictionary._('Error'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}
        task_cancelling = {'name': self.dictionary._('Cancelling'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}
        task_canceled = {'name': self.dictionary._('Canceled'), 'state':gui_update_controller.ID_PHASE_PENDING, 'show': False}

        if self._state in [GUIControler.STATE_DISCOVERING]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_SELECTION_NO_TOKENS_FOUND, GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND, GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND, GUIControler.STATE_DESTINATION_INFO, GUIControler.STATE_DESTINATION]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_selection['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_INSTALLING]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_installing['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state in [GUIControler.STATE_CANCELLING]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_installing['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_cancelling['state'] = gui_update_controller.ID_PHASE_ACTIVE
            task_canceled['state'] = gui_update_controller.ID_PHASE_PENDING
            task_installing['show'] = False
            task_cancelling['show'] = True
            task_canceled['show'] = True
            task_done['show'] = False
        elif self._state in [GUIControler.STATE_CANCELED]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_installing['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_cancelling['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_canceled['state'] = gui_update_controller.ID_PHASE_ACTIVE
            task_installing['show'] = False
            task_cancelling['show'] = True
            task_canceled['show'] = True
            task_done['show'] = False
        elif self._state in [GUIControler.STATE_DONE]:
            task_discovering['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_installing['state'] = gui_update_controller.ID_PHASE_COMPLETE
            task_done['state'] = gui_update_controller.ID_PHASE_ACTIVE
        else:
            task_discovering['show'] = False
            task_selection['show'] = False
            task_installing['show'] = False
            task_done['show'] = False
            task_error['show'] = True
            task_error['state'] = gui_update_controller.ID_PHASE_ACTIVE

        tasklist = []
        self._gui_append_to_tasklist(tasklist, task_discovering)
        self._gui_append_to_tasklist(tasklist, task_selection)
        self._gui_append_to_tasklist(tasklist, task_installing)
        self._gui_append_to_tasklist(tasklist, task_cancelling)
        self._gui_append_to_tasklist(tasklist, task_canceled)
        self._gui_append_to_tasklist(tasklist, task_done)
        self._gui_append_to_tasklist(tasklist, task_error)
        self.user_interface.update.set_phase_list(tasklist)

    def _gui_append_to_tasklist(self, tasklist, task):
        if task['show']:
            tasklist.append(task)

    def _gui_update_state(self):
        if self._state == GUIControler.STATE_DISCOVERING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self.user_interface.update.set_info_headline(self.dictionary._('Detecting tokens'))
            self.user_interface.update.set_info_progress_mode(gui_update_controller.ID_MODE_PROGRESS_UNKNOWN)
            self.user_interface.update.set_info_progress_subtext('')
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_SELECTION_NO_TOKENS_FOUND:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('No installation destination found'))
            self.user_interface.update.set_info_free_text(self.dictionary._('Please insert token where the G/On Client should be installed'))
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Detect'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))
        elif self._state == GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)

            selected_idx = self.user_interface.update.get_selection_highlighted_id()
            self._state_selection_list[selected_idx]
            headline  = self.dictionary._('Token found')
            free_text = self.dictionary._("The token '%s' has been found. Press 'Install' to install the G/On Client on this token." % self._state_selection_list[selected_idx]['name'])
            self.user_interface.update.set_info_headline(headline)
            self.user_interface.update.set_info_free_text(free_text)

            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Install'))
        elif self._state == GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Select token'))
            self.user_interface.update.set_info_free_text(self.dictionary._('Please select token where the G/On Client should be installed'))
            self.user_interface.update.set_show_selection_list(True)
            self.user_interface.update.set_selection_method(gui_update_controller.ID_SELECTION_SINGLE)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label(self.dictionary._('Install'))
        elif self._state == GUIControler.STATE_DESTINATION_INFO:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('G/On Client Installation'))
            import plugin_modules.endpoint.client_gateway_common
            token_title = plugin_modules.endpoint.client_gateway_common.get_token_title()
            self.user_interface.update.set_info_free_text(self.dictionary._('You are about to install a new %s.') % token_title)
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_DESTINATION:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_GET_FOLDER)
            self.user_interface.update.set_info_headline(self.dictionary._('Select installation folder'))
            self.user_interface.update.set_info_folder_path(self._state_destination_root)
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Install'))
        elif self._state == GUIControler.STATE_INSTALLING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self.user_interface.update.set_info_progress_mode(gui_update_controller.ID_MODE_PROGRESS_KNOWN)
            self.user_interface.update.set_info_progress_complete(0)
            self.user_interface.update.set_info_progress_subtext('')
            self.user_interface.update.set_info_headline(self.dictionary._('Installing G/On Client'))
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_CANCELLING:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Cancel installation, please wait'))
            self.user_interface.update.set_info_free_text('')
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(False)
            self.user_interface.update.set_next_label()
        elif self._state == GUIControler.STATE_DONE:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Installation complete'))
            self.user_interface.update.set_info_free_text(self.dictionary._('The G/On Client has now been installed.'))
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_cancel_allowed(True)
            self.user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Launch'))
            self.user_interface.update.set_next_description(self.dictionary._('Press Next to Launch G/On Client'))
        elif self._state == GUIControler.STATE_CANCELED:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline(self.dictionary._('Installation of G/On Client canceled'))
            self.user_interface.update.set_info_free_text(self._state_error_message)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))
        elif self._state == GUIControler.STATE_ERROR:
            self.user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.user_interface.update.set_info_headline('')
            self.user_interface.update.set_show_selection_list(False)
            self.user_interface.update.set_info_free_text(self._state_error_message)
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_cancel_label()
            self.user_interface.update.set_next_allowed(True)
            self.user_interface.update.set_next_label(self.dictionary._('Exit'))

        self._gui_update_tasklist(True)

    def _gui_on_update(self):
        if self._state == GUIControler.STATE_SELECTION_NO_TOKENS_FOUND:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_discovering_start()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()
        elif self._state == GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_installation_start()
        elif self._state == GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND:
            selected_id = self.user_interface.update.get_selection_highlighted_id()
            if selected_id is not None:
                install_allowed = self._state_selection_list[selected_id]['install_enabled']
                if install_allowed != self.user_interface.update.get_next_allowed():
                    self.user_interface.update.set_next_allowed(install_allowed)
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_installation_start()
        elif self._state == GUIControler.STATE_DESTINATION_INFO:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_destination_start()
        elif self._state == GUIControler.STATE_DESTINATION:
            destination_root_selection = self.user_interface.update.get_info_user_selected_folder_path()
            (self._state_destination_sub_folder, self._state_destination_root) = self._state_destination_get_default_root(destination_root_selection)
            self.user_interface.update.set_info_folder_path(self._state_destination_root)
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_destination_root = self.user_interface.update.get_info_user_written_folder_path()
                self._state_destination_done()
        elif self._state == GUIControler.STATE_INSTALLING:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_installation_cancel()
        elif self._state == GUIControler.STATE_DONE:
            if self.user_interface.update.get_cancel_clicked():
                self.user_interface.update.reset_cancel_clicked()
                self._state_exit()
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_launch_client()
        elif self._state == GUIControler.STATE_ERROR:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()
        elif self._state == GUIControler.STATE_CANCELED:
            if self.user_interface.update.get_next_clicked():
                self.user_interface.update.reset_next_clicked()
                self._state_exit()

    def _state_discovering_start(self):
        if self._state in [GUIControler.STATE_INIT, GUIControler.STATE_SELECTION_NO_TOKENS_FOUND]:
            self._task_discover_controler = TaskDiscoverControler(self.checkpoint_handler, self.dictionary, self.client_installer_config, self.plugin_socket_token, self)
            self._state = GUIControler.STATE_DISCOVERING
            self._gui_update_state()
            self._task_discover_controler.start()

    def cb_state_discovering_done(self):
        if self._state == GUIControler.STATE_DISCOVERING:
            self._state_selection_list = self._task_discover_controler.get_selection_list()
            if len(self._state_selection_list) == 0:
                self._state = GUIControler.STATE_SELECTION_NO_TOKENS_FOUND
            elif len(self._state_selection_list) == 1:
                if self._state_selection_list[0]['is_computer_user_token']:
                    self._state = GUIControler.STATE_DESTINATION_INFO
                else:
                    self._state = GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND
                    self.user_interface.update.set_selection_list(self._state_selection_list)
                    self.user_interface.update.set_selection_user_highlighted(0)
            else:
                self._state = GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND
                self.user_interface.update.set_selection_list(self._state_selection_list)
                self.user_interface.update.set_selection_user_highlighted(0)
            self._gui_update_state()

    def cb_state_error(self, message):
        self._state_error_message = message
        self._state = GUIControler.STATE_ERROR
        self._gui_update_state()

    def _state_installation_start(self, runtime_env=None, token_type=None, token_id=None, is_computer_user_token=False):
        if self._state in [GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND, GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND, GUIControler.STATE_DESTINATION, GUIControler.STATE_INSTALLING]:
            self.user_interface.update.set_cancel_allowed(False)
            self.user_interface.update.set_next_allowed(False)
            if runtime_env is None:
                selected_idx = self.user_interface.update.get_selection_highlighted_id()
                if selected_idx is None:
                    selected_idx = 0

                if self._state != GUIControler.STATE_INSTALLING and self._state_selection_list[selected_idx]['is_computer_user_token']:
                    self._state_destination_info_start()
                    return

                self._state = GUIControler.STATE_INSTALLING
                self._gui_update_state()
                runtime_env_id = self._state_selection_list[selected_idx]['runtime_env_id']
                runtime_env = self.plugin_socket_runtime_env.get_instance(runtime_env_id)
                token_type = self._state_selection_list[selected_idx]['token_type']
                token_id = self._state_selection_list[selected_idx]['token_id']
                is_computer_user_token = self._state_selection_list[selected_idx]['is_computer_user_token']

            if runtime_env is None:
                self.cb_state_error(self.dictionary._('Unexpected error, unable to create runtime  environment for the  selected token. See logfile for additional information.'))
                return
            self._state = GUIControler.STATE_INSTALLING
            self._gui_update_state()
            self._task_install_controler = TaskInstallControler(self.checkpoint_handler, self.dictionary, self.client_installer_config, self.plugin_socket_token, self, runtime_env, token_type, token_id, is_computer_user_token)
            self._task_install_controler.start()

    def _state_installation_cancel(self):
        self._state = GUIControler.STATE_CANCELLING
        if self._task_install_controler is not None:
            self._task_install_controler.do_cancel()
        self._gui_update_state()

    def cb_progress(self, progress, progress_message=None):
        if self._state in [GUIControler.STATE_DISCOVERING, GUIControler.STATE_INSTALLING]:
            self.user_interface.update.set_info_progress_complete(progress)
            if progress_message is not None:
                self.user_interface.update.set_info_progress_subtext(progress_message)

    def cb_state_installation_done(self):
        if self._state == GUIControler.STATE_INSTALLING:
            self._state = GUIControler.STATE_DONE
            self._gui_update_state()

    def cb_state_installation_canceled(self):
        self._state = GUIControler.STATE_CANCELED
        self._gui_update_state()

    def _state_destination_info_start(self):
        if self._state in [GUIControler.STATE_SELECTION_ONE_TOKEN_FOUND, GUIControler.STATE_SELECTION_MULTIBLE_TOKENS_FOUND]:
            self._state = GUIControler.STATE_DESTINATION_INFO
            self._gui_update_state()

    def _state_destination_start(self):
        if self._state in [GUIControler.STATE_DESTINATION_INFO]:
            self._state = GUIControler.STATE_DESTINATION
            (self._state_destination_sub_folder, self._state_destination_root) = self._state_destination_get_default_root()
            self._gui_update_state()

    def _state_destination_done(self):
        if self._state in [GUIControler.STATE_DESTINATION]:
            self._state = GUIControler.STATE_INSTALLING
            runtime_env = self.plugin_socket_runtime_env.create_runtime_env_instance_endpoint(self.checkpoint_handler, self._state_destination_root)
            token = self.plugin_socket_token.create_token_endpoint(self._state_destination_root)
            self._state_installation_start(runtime_env, token.token_type, token.token_id, True)

    def _state_destination_get_default_root(self, selection_root=None):
        (default_sub_folder, default_root) = lib.appl.gon_client.generate_installation_root(installation_root=selection_root)
        if default_root is None:
            self.cb_state_error(self.dictionary._('Unable to locate installation root'))
            return (None, None)
        return (default_sub_folder, default_root)

    def _state_exit(self):
        self.user_interface.destroy()

    def _state_launch_client(self):
        self._do_restart_flag = True
        self.user_interface.destroy()

    def do_restart(self):
        return self._do_restart_flag

    def get_runtime_env(self):
        return self._task_install_controler.get_runtime_env()

def main():
    #
    # Parsing arguments from commandline and ini-file
    #
    client_installer_config = ClientInstallerOptions()

    #
    # Show version and exit if requested
    #
    if(client_installer_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    #
    # Initialize logging
    #
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(client_installer_config.log_enabled,
                                                                      client_installer_config.log_verbose,
                                                                      client_installer_config.log_file,
                                                                      client_installer_config.is_log_type_xml())
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)
    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                  )

    try:
        #
        # Initialize async service
        #
        components.communication.async_service.init(checkpoint_handler)
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 1)

        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, client_installer_config.get_dictionary_path_abs())
        user_interface = components.presentation.user_interface.UserInterface(configuration=client_installer_config, dictionary=dictionary)
        user_interface.common.set_visible(False)
        user_interface.update.set_window_frame_text(dictionary._('G/On Installer'))


        #
        # Load plugins, and create socket
        #
        plugin_manager = components.plugin.client_gateway.manager.Manager(checkpoint_handler, client_installer_config.get_plugin_modules_path_abs())

        plugins = plugin_manager.create_instances(async_service=async_service, checkpoint_handler=checkpoint_handler, user_interface=user_interface, additional_device_roots=[])
        plugin_socket_runtime_env = components.plugin.client_gateway.plugin_socket_client_runtime_env.PluginSocket(plugin_manager, plugins, checkpoint_handler)
        plugin_socket_token = components.plugin.client_gateway.plugin_socket_token.PluginSocket(plugin_manager, plugins)

        async_service.start()

        gui_handler = GUIControler(checkpoint_handler, dictionary, user_interface, client_installer_config, plugin_socket_token, plugin_socket_runtime_env)
        user_interface.start()

        async_service.stop()
        async_service.join()

        if gui_handler.do_restart():
            runtime_env = gui_handler.get_runtime_env()
            restart_handler = lib.appl.gon_client.GOnClientRestartHandler(checkpoint_handler, runtime_env.get_info().get_id(), runtime_env, skip_remove_of_restart_state=True)
            restart_handler.set_gon_client_base_cwd(runtime_env.get_root())
            restart_handler.do_restart(checkpoint_handler, None, skip_write_of_restart_state=True)

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main.error", MODULE_ID, lib.checkpoint.ERROR, etype, evalue, etrace)

if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    main()
