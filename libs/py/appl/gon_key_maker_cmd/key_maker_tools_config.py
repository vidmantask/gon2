'''
Created on 09/10/2009

@author: pwl
'''

key_maker_tools_config = {
    
        "init_applet_config" : {
                                "args" : ["java",  "-jar",  "SmartCardManager.jar",  "Mobile Security Card 0001 0",  "mwapplet"],
                                "cwd" : "./micro_smart"
                             },
                             
        "label_volume_config" : {
                                "args" : ["label", "%(config.volume_letter)", "G-ON"],
                                "cwd" : "."
                                },
                                
        "dismount_config" : {
                                "args" : ["sync", "-e", "%(config.volume_letter)"],
                                "cwd" : "./bin"
                                },
                                
    }