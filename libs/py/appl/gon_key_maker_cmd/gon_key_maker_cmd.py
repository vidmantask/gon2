"""
The G/On Key Maker cmd (Commandline version for multible token initialization)
"""
import sys
import optparse
import os
import os.path
import ConfigParser
import subprocess
import traceback
import shutil
import stat
import time
import uuid
import glob
import shutil

if sys.platform == 'win32':
    import win32serviceutil
    import win32service
    import _winreg
    import win32file
    import win32com.shell.shell
    import win32com.shell.shellcon
    import win32com

import components.config.common

import lib.smartcard.msc
import lib.hardware.device
import lib.variable_expansion
import lib.checkpoint
import lib.dictionary


import lib.gpm.gpm_installer
import lib.gpm.gpm_installer_base
import lib.gpm.gpm_spec
import lib.gpm.gpm_env
import lib.gpm.gpm_builder

import plugin_modules.micro_smart.client_gateway_common
import plugin_modules.micro_smart.client_gateway


from key_maker_tools_config import key_maker_tools_config


global_checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()



module_id = "appl_gon_key_maker"


class KeyMakerError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)


class KeyMakerCmdOptions(object):
    """
    Options and ini-file values for the Key Maker. 
    """
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--list', action='store_true', help='List G&D devices found')
        self._parser.add_option('--init', action='store_true', help='Initialize G&D devices found')
        self._parser.add_option('--verify', action='store_true', help='Verify G&D devices found')
        self._parser.add_option('--config', type='string', default='./gon_key_maker.ini', help='configuration file')
        self._parser.add_option('--count_check', type='int', default=None)
        self._parser.add_option('--load_test', action='store_true', help='Perform load test on G&D devices found')
        (self._options, self._args) = self._parser.parse_args()

    def cmd_show_version(self):
        return self._options.version

    def cmd_list(self):
        return self._options.list

    def cmd_init(self):
        return self._options.init

    def cmd_verify(self):
        return self._options.verify

    def cmd_load_test(self):
        return self._options.load_test

    def get_count_check(self):
        return self._options.count_check

    def get_gpms_path(self):
        return os.path.abspath('gpms')

#
# KeyMaker
#    
class KeyMaker(object):
    SERVICE_NAME = "SysD"
    
    def __init__(self, config):
        self.config = config

    def _is_service_running(self):
        print "service status", win32serviceutil.QueryServiceStatus(KeyMaker.SERVICE_NAME, None)
        return win32serviceutil.QueryServiceStatus(KeyMaker.SERVICE_NAME, None)[1] == win32service.SERVICE_RUNNING

    def msc_service_stop(self):
        if not self._is_service_running():
            return
        rc = win32serviceutil.StopService(KeyMaker.SERVICE_NAME)
        while self._is_service_running():
            time.sleep(1)
        print "service stopped"
        time.sleep(2)
        
    def msc_service_start(self):
        if self._is_service_running():
            return
        rc = win32serviceutil.StartService(KeyMaker.SERVICE_NAME)
        while not self._is_service_running():
            time.sleep(1)
        print "service started"
        time.sleep(2)

    def msc_service_fix(self, device):
        self.msc_service_stop()
        drive_letter = device.replace(":", "")
        with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\MSC\\00", 0, _winreg.KEY_ALL_ACCESS) as msc_reg_key:
            print "service fix value before:",_winreg.QueryValueEx(msc_reg_key, 'LoDisk')
            _winreg.SetValueEx(msc_reg_key, 'LoDisk', 0, _winreg.REG_SZ, drive_letter)
            print "service fix value after :",_winreg.QueryValueEx(msc_reg_key, 'LoDisk')
        self.msc_service_start()

    def _expand_variables(self, arg, device):
        def get_replace_string(name, field_name):
            if name == "config" and field_name == "volume_letter":
                return device
            return None
        return lib.variable_expansion.expand(arg, get_replace_string)
                
    def _expand_list_variables(self, args, device):
        return [self._expand_variables(arg, device) for arg in args]
           
    def get_device_list(self):
        device_list = lib.hardware.device.Device.get_device_list() 
        device_list_found = []
        for device in device_list:
            device = device.replace("\\", "")
            msc_filename_probe = os.path.join(device, "giritech_probe.msc")
            try:
                with lib.smartcard.msc.MSC(msc_filename_probe) as smart_card_transaction:
                    device_list_found.append(device)
                os.remove(msc_filename_probe)
            except:
                pass
        return device_list_found

    def devices_list(self):
        print "Listing MSC devices"
        devices = self.get_device_list()
        for device in devices:
            print device

        if len(devices) == 0:
            print "No devices found"
        else:
            print "%d devices found" % len(devices)
            
    def devices_verify(self, config):
        devices = self.get_device_list()
        if config.get_count_check() is not None:
            if len(devices) != config.get_count_check():
                raise KeyMakerError("Count check failed, devices found: %s" % str(devices))
        
        for device in devices:
            self.devices_verify_device(device)
            
    def devices_verify_device(self, device):
        print "=" * 50
        print "Verify %s" % device
        print "-" * 50

        print "Verifying"
        self.devices_verify_device_init(device)
        print "Verifying, ok"

        print "Chkdsk"
        self.devices_initialize_device_chkdsk(device)
        print "Chkdsk, ok"

        print "Unmount"
        self.devices_initialize_device_unmount(device)
        print "Unmount, ok"

        print ""

    def devices_verify_device_init(self, device):
        deploy_plugin = plugin_modules.micro_smart.client_gateway_common.SmartCardTokens(global_checkpoint_handler)

        serial = str(uuid.uuid1())
        deploy_plugin.initialize_token(device)
        deploy_plugin.set_serial(device, serial)
        if serial != deploy_plugin.get_serial(device):
            raise KeyMakerError("Verification failed, invalid serial")

        deploy_plugin.generate_keypair(device)
        public_key = deploy_plugin.get_public_key(device)


    def devices_initialize(self, config):
        devices = self.get_device_list()
        if config.get_count_check() is not None:
            if len(devices) != config.get_count_check():
                raise KeyMakerError("Count check failed, devices found: %s" % str(devices))
        
        for device in devices:
            self.devices_initialize_device(device)

    def devices_initialize_device(self, device):
        print "=" * 50
        print "Initialize %s" % device
        print "-" * 50

        print "Start MSC service fix registry"
        self.msc_service_fix(device)
        print "Start MSC service fix registry, ok"

        print "Start MSC init"
        self.devices_initialize_device_init_applet(device)
        print "Start MSC init, ok"

        print "Labeling"
        self.devices_initialize_device_labelling(device)
        print "Labeling, ok"

        print "Delete files"
        self.devices_initialize_device_delete_files(device)
        print "Delete files, ok"

        print "Init files"
        self.devices_initialize_device_init_files(device)
        print "Init files, ok"

        print "Chkdsk"
        self.devices_initialize_device_chkdsk(device)
        print "Chkdsk, ok"

        print "Unmount"
        self.devices_initialize_device_unmount(device)
        print "Unmount, ok"

        print ""

    def devices_initialize_device_labelling(self, device):
        tool_config = key_maker_tools_config.get("label_volume_config")
        args = self._expand_list_variables(tool_config.get("args"), device)
        cwd = self._expand_variables(tool_config.get("cwd", "./"), device)
        
        process = subprocess.Popen(args=args, cwd=cwd, shell=True)
        rc = process.wait()
        if rc != 0:
            raise KeyMakerError("Labling failed")
        
    def devices_initialize_device_delete_files(self, device):
        if device in ["C:", "c:"]:
            raise "Prevented delete of files"
            
        for root, dirs, files in os.walk(device, topdown=False):
            for name in files:
                os.chmod(os.path.join(root, name), stat.S_IWRITE)
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))

    def devices_initialize_device_init_files(self, device):
        init_dir = os.path.join(device, 'gon_client', 'gon_init_micro_smart')
        os.makedirs(init_dir)
                        
        root_folder = './extra_files'
        for filename in os.listdir(root_folder):
            filename_dest = os.path.join(root_folder, filename)
            shutil.copy(filename_dest, device)
                        
    def devices_initialize_device_init_applet(self, device):
        tool_config = key_maker_tools_config.get("init_applet_config")
        args = self._expand_list_variables(tool_config.get("args"), device)
        cwd = self._expand_variables(tool_config.get("cwd", "./"), device)
        
        process = subprocess.Popen(args=args, cwd=cwd, shell=True)
        rc = process.wait()
        if rc != 0:
            raise KeyMakerError("Init applet failed")

    def devices_initialize_device_chkdsk(self, device):
        time.sleep(5)
        args = ['chkdsk', device, '/F', '/X']
        process = subprocess.Popen(args=args, shell=True)
        rc = process.wait()
        if rc != 0:
            raise KeyMakerError("Chkdsk failed")

    def devices_initialize_device_unmount(self, device):
        tool_config = key_maker_tools_config.get("dismount_config")
        args = self._expand_list_variables(tool_config.get("args"), device)
        cwd = self._expand_variables(tool_config.get("cwd", "./"), device)
        
        process = subprocess.Popen(args=args, cwd=cwd, shell=True)
        rc = process.wait()
        if rc != 0:
            raise KeyMakerError("Device unmount failed")
        
        win32com.shell.shell.SHChangeNotify(win32com.shell.shellcon.SHCNE_DRIVEREMOVED, win32com.shell.shellcon.SHCNF_PATH, device)
#        win32file.DeleteVolumeMountPoint(device)

    def devices_load_test(self, config):
        devices = self.get_device_list()
        if len(devices) == 0:
            raise KeyMakerError("No device found")
        
        try:
            while True:
                for device in devices:
                    self.devices_load_test_device(device)
                    time.sleep(5)                
        except KeyboardInterrupt:
            pass
        

    def devices_load_test_device(self, device):
        work_filename = os.path.join(device, "load_test_file.txt")
        try:
            print "Load testing %s" % device
            print "Generate keys"
            self.devices_verify_device_init(device)
            print "Generate keys, ok"
            
            self.devices_load_test_device_gpms_install(device)
            
        except KeyboardInterrupt:
            pass
        finally:
            time.sleep(2)
            if os.path.exists(work_filename):
                os.remove(work_filename)


    def devices_load_test_device_gpms_install(self, device):
        print "GPMS install testing"
        
        gpms_source_path = self.config.get_gpms_path()
        if not os.path.exists(gpms_source_path):
            print "GPMS source path %s not found" % gpms_source_path
            return
        
        gpms_dest_path = os.path.join(device, 'gpms')
        if os.path.exists(gpms_dest_path):
            shutil.rmtree(gpms_dest_path)

        device_path = device
        if device.endswith(':'):
            device_path += "/"
            
        runtime_env = plugin_modules.micro_smart.client_gateway.PluginClientRuntimeEnvInstance(global_checkpoint_handler, device_path)
        print "  copy gpms files to device", runtime_env.get_download_root()
        if os.path.exists(runtime_env.get_download_root()):
            shutil.rmtree(runtime_env.get_download_root())
        shutil.copytree(gpms_source_path, runtime_env.get_download_root())
        print "  copy gpms files to device, done"
        
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_metas = lib.gpm.gpm_builder.query_meta_all_from_gpm_files(runtime_env.get_download_root(), error_handler)
        gpm_ids = lib.gpm.gpm_builder.query_meta_for_gpm_ids(gpm_metas, error_handler)
        
        dictionary = lib.dictionary.Dictionary(global_checkpoint_handler)
        runtime_env.install_gpms(gpm_ids, [], [], gpm_metas, GpmInstallerCBGUI(), None, error_handler, dictionary, '', '')
        runtime_env.install_gpms([], [], gpm_ids, gpm_metas, GpmInstallerCBGUI(), None, error_handler, dictionary, '', '')
            
        if error_handler.error():
            error_handler.dump()
        print "GPMS install testing, done"
        
        
class GpmInstallerCBGUI(lib.gpm.gpm_installer_base.GpmInstallerCB):
    def __init__(self):
        pass
    
    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        print "  %s" % task_title
        
    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        print "    %s" % action_title

#
# Main
#
def main():
    config = KeyMakerCmdOptions()
    
    if(config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    key_maker = KeyMaker(config)

    if(config.cmd_list()):
        key_maker.devices_list()
        return 0

    if(config.cmd_init()):
        key_maker.devices_initialize(config)
        return 0

    if(config.cmd_verify()):
        key_maker.devices_verify(config)
        return 0

    if(config.cmd_load_test()):
        key_maker.devices_load_test(config)
        return 0

if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)
    main()
