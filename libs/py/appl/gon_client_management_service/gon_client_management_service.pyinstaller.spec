# -*- mode: python -*-
import lib.dev_env.path_setup
from distutils.core import setup

import sys
import lib.appl_plugin_module_util

import lib.dev_env
dev_env = lib.dev_env.DevEnv.create_current()

import lib.dev_env.pyinstaller

data_files = []
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_2', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_2', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_pe', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit_pe', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'computer_token', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'computer_token', 'client', 'common'))

data_files.append(('certificates', ['../../../setup/release/config/certificates/gon_https_management_client_service.crt',
                                    '../../../setup/release/config/certificates/gon_https_management_client_service.key.noencrypt.pem']))

#
# PyInstaller configuration
#
app_py = 'gon_client_management_service.py'
app_name = 'gon_client_management_service'
app_description = "G/On Client Management Service"
app_version_file = lib.dev_env.pyinstaller.generate_version_file(app_py, app_name, app_description)
app_icon_file = "..\\..\\components\\presentation\\gui\\mfc\\images\\giritech.ico"
app_options = []

datas = []
for (data_file_dest_folder, data_file_src_files) in data_files:
  for data_file_src_file in data_file_src_files:
    datas.append( (data_file_src_file, data_file_dest_folder))

hiddenimports = [
  'logging',
  'sqlalchemy.dialects.sqlite',
  'lib.appl.gon_client',
  'lib.appl.gon_client_device_service',
  'lib.config',
  'lib.cryptfacility',
  'lib.hardware.device',
  'lib.smartcard.msc_pkcs15',
  'lib.smartcard.pcsc_pkcs15',
  'lib_cpp.swissbit',
  'elementtree',
  'os.path',
]

exclude_modules = [
'plugin_modules',
'plugin_modules.soft_token',
'plugin_modules.soft_token.client_gateway',
'plugin_modules.soft_token.client_gateway_common',
'plugin_modules.micro_smart',
'plugin_modules.micro_smart.client_gateway',
'plugin_modules.micro_smart.client_gateway_common',
'plugin_modules.micro_smart_swissbit',
'plugin_modules.micro_smart_swissbit.client_gateway',
'plugin_modules.micro_smart_swissbit.client_gateway_common',
'plugin_modules.micro_smart_swissbit_2',
'plugin_modules.micro_smart_swissbit_2.client_gateway',
'plugin_modules.micro_smart_swissbit_2.client_gateway_common',
'plugin_modules.micro_smart_swissbit_pe',
'plugin_modules.micro_smart_swissbit_pe.client_gateway',
'plugin_modules.micro_smart_swissbit_pe.client_gateway_common',
'plugin_modules.endpoint',
'plugin_modules.endpoint.client_gateway',
'plugin_modules.endpoint.client_gateway_common',
'plugin_modules.computer_token',
'plugin_modules.computer_token.client_gateway',
'plugin_modules.computer_token.client_gateway_common',
]

#
# PyInstaller generate folder app
#
a = Analysis(
  [app_py],
  pathex=['.'],
  binaries=[],
  datas=datas,
  hiddenimports=hiddenimports,
  hookspath=[],
  runtime_hooks=[],
  excludes=exclude_modules,
  win_no_prefer_redirects=False,
  win_private_assemblies=False,
  cipher=None
)

pyz = PYZ(
  a.pure,
  a.zipped_data,
  cipher=None
)

exe = EXE(
  pyz,
  a.scripts,
  options=app_options,
  exclude_binaries=True,
  name=app_name,
  debug=False,
  strip=False,
  upx=True,
  console=True,
  version=app_version_file,
  icon=app_icon_file
)

coll = COLLECT(
  exe,
  a.binaries,
  a.zipfiles,
  a.datas,
  strip=False,
  upx=True,
  name=''
)
