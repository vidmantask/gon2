import lib.dev_env.path_setup
from distutils.core import setup
import py2exe
import lib.appl_plugin_module_util

import lib.dev_env
import lib.dev_env.pack

dev_env = lib.dev_env.DevEnv.create_current()

#
#call: python setup.py py2exe
#
data_files = []
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'soft_token', 'client', 'gateway_common'))
#data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'smart_card', 'client', 'gateway'))
#data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'smart_card', 'client', 'gateway_common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart', 'client', 'gateway_common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'micro_smart_swissbit', 'client', 'gateway_common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'endpoint', 'client', 'gateway_common'))
data_files.extend(lib.appl_plugin_module_util.modul_to_data_files('../..', 'computer_token', 'client', 'gateway'))


data_files.append(('', ['../../setup/dlls/win/libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                        '../../setup/dlls/win/msvcr90.dll',
                        '../../setup/dlls/win/msvcp90.dll',
                        '../../setup/dlls/win/msvcm90.dll',
                        '../../setup/dlls/win/Microsoft.VC90.CRT.manifest']))

data_files.append(('certificates', ['../../../setup/release/config/certificates/gon_https_management_client_service.crt',
                                    '../../../setup/release/config/certificates/gon_https_management_client_service.key.noencrypt.pem']))


setup(
      console = [ lib.appl_plugin_module_util.Py2ExeTarget(
                          script='gon_client_management_service.py',
                          uac = False,
                          branding = lib.appl_plugin_module_util.Py2ExeTarget.BRANDING_NORMAL,
                          name = 'G/On Management Client Service',
                          description = 'G/On Management Client Service',
                          image_root='../../components/presentation/gui/mfc/images'
                        )
               ],
      options = {
            "py2exe": {
                  "dll_excludes": [
                    'w9xpopen.exe',
                    'MSVCP90.dll',
                    'libopenblas.KZGMSHIV2CB7DB4HQYDV2LEPUALNSYMY.gfortran-win32.dll',
                    'api-ms-win-core-string-l1-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-0.dll',
                    'api-ms-win-core-libraryloader-l1-2-1.dll',
                    'api-ms-win-core-atoms-l1-1-0.dll',
                    'api-ms-win-core-winrt-error-l1-1-1.dll',
                    'api-ms-win-core-sidebyside-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-3-0.dll',
                    'api-ms-win-core-heap-l1-2-0.dll',
                    'api-ms-win-core-heap-l2-1-0.dll',
                    'api-ms-win-core-delayload-l1-1-1.dll',
                    'api-ms-win-core-libraryloader-l1-2-0.dll',
                    'api-ms-win-core-rtlsupport-l1-2-0.dll',
                    'api-ms-win-core-shlwapi-obsolete-l1-2-0.dll',
                    'api-ms-win-security-base-l1-2-0.dll',
                    'api-ms-win-core-synch-l1-2-0.dll',
                    'api-ms-win-core-handle-l1-1-0.dll',
                    'api-ms-win-core-registry-l1-1-0.dll',
                    'api-ms-win-core-synch-l1-1-0.dll',
                    'api-ms-win-core-localization-l1-2-0.dll',
                    'api-ms-win-core-profile-l1-1-0.dll',
                    'api-ms-win-core-sysinfo-l1-1-0.dll',
                    'api-ms-win-core-errorhandling-l1-1-0.dll',
                    'api-ms-win-core-file-l1-1-0.dll',
                    'api-ms-win-core-timezone-l1-1-0.dll',
                    'api-ms-win-core-processenvironment-l1-1-0.dll',
                    'api-ms-win-security-base-l1-1-0.dll',
                    'api-ms-win-core-localization-obsolete-l1-2-0.dll',
                    'api-ms-win-core-string-obsolete-l1-1-0.dll',
                    'api-ms-win-crt-private-l1-1-0.dll',
                    'api-ms-win-core-processthreads-l1-1-1.dll',
                    'api-ms-win-crt-string-l1-1-0.dll',
                    'api-ms-win-crt-runtime-l1-1-0.dll',
                    'api-ms-win-core-heap-l1-1-0.dll',
                    'api-ms-win-core-interlocked-l1-1-0.dll',
                    'api-ms-win-core-debug-l1-1-0.dll'
                    ],
                  "dist_dir" : dev_env.generate_pack_destination('gon_client_management_service'),
                  "packages":[
                              'logging',
                              'sqlalchemy.dialects.sqlite',
                              'lib.appl.gon_client',
                              'lib.config',
                              'lib.cryptfacility',
                              'lib.hardware.device',
                              'lib.smartcard.msc_pkcs15',
                              'lib.smartcard.pcsc_pkcs15',
                              'lib_cpp.swissbit',
                              'elementtree'
                             ],
                  "includes":[],
                  "excludes":[
                              'plugin_modules'
                             ],
                  "bundle_files" : 3,
                  "compressed" : True,
                  "optimize" : 0,
            }
      },
      data_files = data_files,
      zipfile = None,
)
