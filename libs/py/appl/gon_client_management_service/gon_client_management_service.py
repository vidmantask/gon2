"""
The G/On Management Client Service
"""
from __future__ import with_statement
import warnings
warnings.filterwarnings("ignore")

import time
import sys
import os.path
import shutil
import datetime

import optparse
import threading
import ConfigParser
import socket
import ssl


import lib.version
import lib.checkpoint

import components.communication.async_service

import components.management_message.server_common.database_schema

import components.environment
import components.admin_deploy_ws.admin_deploy_ws_service
import components.admin_deploy_ws.admin_deploy_ws_session
from components.rest.rest_api import RestApi

import components.plugin.client_gateway.manager
import components.communication.async_service

import plugin_types.client_gateway.plugin_type_token
import plugin_types.client_gateway.plugin_type_client_runtime_env
import plugin_types.client_gateway.plugin_type_tag
import plugin_types.client_gateway.plugin_type_auth
import plugin_types.client_gateway.plugin_type_endpoint

import components.config.common

import lib.appl.io_hooker
import lib.appl.crash.handler

import components.dictionary.common
import uuid

if sys.platform == 'win32':
    import lib.hardware.windows_security_center

MODULE_ID = "appl_gon_client_management_service"



class ManagementClientServiceConfig(components.config.common.ConfigClientManagementService):
    """
    Options for the Management Client Service. General rule is that commandline options overwrites options from ini-file.
    """
    def __init__(self):
        components.config.common.ConfigClientManagementService.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--foreground', action='store_true', default=False, help='run server in foreground')
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')
        self._parser.add_option('--serve', action='store_true', help='Start Management Client Web-Service')
        self._parser.add_option('--config', type='string', default='./gon_client_management_service.ini', help='configuration file')
        self._parser.add_option('--ip', type='string', default=None, help='Option to --server: Listen on given ip')
        self._parser.add_option('--port', type='int', default=None,  help='Option to --server: Listen on given port')
        self._parser.add_option('--management_server_ip', type='string', default=None, help='Option to --server: Contact Management Server on given ip')
        self._parser.add_option('--management_server_port', type='int', default=None,  help='Option to --server: Contact Management Server on given port')
        self._parser.add_option('--deploy_tokens', action='store_true', default=False, help="Update 'servers' and 'client knownsecret' on inserted tokens.")

        (self._options, self._args) = self._parser.parse_args()
        self.read_config_file(self._options.config)

    def cmd_show_version(self):
        return self._options.version

    def cmd_serve(self):
        return self._options.serve

    def cmd_deploy_tokens(self):
        return self._options.deploy_tokens

    def get_foreground(self):
        return self._options.foreground

    def get_management_ws_url(self):
        management_ws_ip = self._options.management_server_ip
        if management_ws_ip is None:
            management_ws_ip = self.management_ws_ip
        management_ws_port = self._options.management_server_port
        if management_ws_port is None:
            management_ws_port = self.management_ws_port
        return "http://%s:%d" % (management_ws_ip, management_ws_port)

    def get_service_ip(self):
        ip = self._options.ip
        if ip is None:
            ip = self.management_deploy_ws_ip
        return ip

    def get_service_port(self):
        port = self._options.port
        if port is None:
            port = self.management_deploy_ws_port
        return port

    def get_dictionary_path_abs(self):
        return self.dictionary_abspath



class TimeoutThread(threading.Thread):
    def __init__(self, checkpoint_handler, config, admin_deploy_ws_service):
        threading.Thread.__init__(self, name="TimeoutThread")
        self.checkpoint_handler = checkpoint_handler
        self.config = config
        self.admin_deploy_ws_service = admin_deploy_ws_service
        self._stop = False

    def run(self):
        with self.checkpoint_handler.CheckpointScope("TimeoutThread.run", MODULE_ID, lib.checkpoint.DEBUG):
            while not self._stop:
                if self.config.management_deploy_timeout_sec > 0:
                    time_since_last_ping = datetime.datetime.now() - self.admin_deploy_ws_service.get_ping_ts()
                    if time_since_last_ping.total_seconds() > self.config.management_deploy_timeout_sec:
                        self.checkpoint_handler.Checkpoint("TimeoutThread.stop", MODULE_ID, lib.checkpoint.INFO, msg="Client Management service stopping")
                        self.stop()
                        self.admin_deploy_ws_service.stop()
                        break
                time.sleep(2)

    def stop(self):
        self._stop = True


class Environment(object):
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler



def main_server(server_config):
    #
    # This is a hack that seems to fix a later call to the same function(with same argument).
    # The later call is in standard socket.py and would block on some XP if this program was called from a java process.
    #
    result = socket.gethostbyaddr('127.0.0.1')

    if server_config.log_rotate and os.path.exists(server_config.log_file):
        shutil.move(server_config.log_file, lib.checkpoint.generate_rotate_filename(server_config.log_file))
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(server_config.log_enabled,
                                                                      server_config.log_verbose,
                                                                      server_config.log_file,
                                                                      server_config.is_log_type_xml())

    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '))

    checkpoint_handler.Checkpoint("serve", MODULE_ID, lib.checkpoint.INFO,
                                  service_ip=server_config.get_service_ip(),
                                  service_port=server_config.get_service_port(),
                                  mangement_ws_url=server_config.get_management_ws_url())

    timeout_thread = None

    try:
        components.communication.async_service.init(checkpoint_handler)
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 1, True)

        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, server_config.get_dictionary_path_abs())
        environment = Environment(checkpoint_handler)
        environment.soft_token_root = server_config.deployment_soft_token_abspath
        environment.plugin_modules_root = server_config.plugin_modules_abspath
        environment.download_root = server_config.deployment_download_cache_abspath
        if not os.path.exists(environment.soft_token_root):
            os.makedirs(environment.soft_token_root)

        if not os.path.exists(environment.download_root):
            os.makedirs(environment.download_root)

        admin_deploy_ws_service = components.admin_deploy_ws.admin_deploy_ws_service.AdminDeployWSService(async_service,
                                                                                                          environment,
                                                                                                          server_config,
                                                                                                          server_config.get_management_ws_url(),
                                                                                                          server_config.deployment_download_cache_abspath,
                                                                                                          dictionary)
        timeout_thread = TimeoutThread(checkpoint_handler, server_config, admin_deploy_ws_service)
        timeout_thread.start()

        if server_config.get_foreground():
            admin_deploy_ws_service.start()
            while True:
                try:
                    user_input = raw_input('command : ')
                except EOFError:
                    break
                except KeyboardInterrupt:
                    break

                if(user_input == 'stop'):
                    break
                else:
                    print "Error, the command '%s' is not known." % user_input

                print "stopping"
            admin_deploy_ws_service.stop()
            admin_deploy_ws_service.join()
            time.sleep(1)

        else:
            async_service.start()
            admin_deploy_ws_service.start()
            admin_deploy_ws_service.join()

        timeout_thread.stop()
        timeout_thread.join()

        async_service.stop()
        async_service.join()

    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main_serve.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        if timeout_thread is not None:
            timeout_thread.stop()

        print evalue
        return -1

    return 0


def _create_session(server_config, checkpoint_handler, dictionary, async_service):
        environment = Environment(checkpoint_handler)
        environment.soft_token_root = server_config.deployment_soft_token_abspath
        environment.plugin_modules_root = server_config.plugin_modules_abspath
        environment.download_root = server_config.deployment_download_cache_abspath

        additional_device_roots = []
        if environment.soft_token_root and os.path.exists(environment.soft_token_root):
            additional_device_roots.append(environment.soft_token_root)

        environment.runtime_plugin_manager = components.plugin.client_gateway.manager.Manager(environment.checkpoint_handler, environment.plugin_modules_root)
        environment.runtime_plugins        = environment.runtime_plugin_manager.create_instances(async_service=async_service, checkpoint_handler=environment.checkpoint_handler, user_interface=None, additional_device_roots=additional_device_roots)

        session = components.admin_deploy_ws.admin_deploy_ws_session.AdminDeployWSSession(environment, 1, 'dummy_user', 'dummy_pwd', server_config.get_management_ws_url(), environment.download_root, additional_device_roots, dictionary)
        return session


def deploy_tokens(server_config):
    #
    # This is a hack that seems to fix a later call to the same function(with same argument).
    # The later call is in standard socket.py and would block on some XP if this program was called from a java process.
    #
    result = socket.gethostbyaddr('127.0.0.1')
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(server_config.log_enabled,
                                                                      server_config.log_verbose,
                                                                      server_config.log_file,
                                                                      server_config.is_log_type_xml())

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '))
    checkpoint_handler.Checkpoint("deploy_tokens", MODULE_ID, lib.checkpoint.INFO)
    try:
        components.communication.async_service.init(checkpoint_handler)
        async_service = components.communication.async_service.AsyncService(checkpoint_handler, MODULE_ID, 1, True)

        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, server_config.get_dictionary_path_abs())
        session = _create_session(server_config, checkpoint_handler, dictionary, async_service)

        print "Looking for tokens"

        # Get tokens currently inserted
        request = RestApi.AdminDeploy.GetTokensRequest()
        response = session.GetTokens(request)

        tokens = response.get_tokens
        if len(tokens) == 0:
            print "No tokens found"
            return 0
        print "Looking for tokens, done (%d tokens found)" % (len(tokens))

        print "Deploying tokens(updating 'servers' and 'knownsecret'):"
        for token in response.get_tokens:
            print "  %s %s" % (token.get_token_serial.ljust(18), token.get_token_label)

            request = RestApi.AdminDeploy.DeployTokenRequest()
            request.set_token_type_id(token.get_token_type_id)
            request.set_token_id(token.get_token_id)
            request.set_generate_keypair(False)
            response = session.DeployToken(request)

        print "Deploying found tokens, done"
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("main_serve.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        print evalue
        return -1

    return 0

def main():
    server_config = ManagementClientServiceConfig()

    #
    # Show version and exit if requested
    #
    if(server_config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0

    if(server_config.cmd_serve()):
        return main_server(server_config)

    if(server_config.cmd_deploy_tokens()):
        return deploy_tokens(server_config)


if __name__ == '__main__':
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)

    main()
