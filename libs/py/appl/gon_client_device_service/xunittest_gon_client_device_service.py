"""
Unittest of gon_client_device_service
"""
import base64

import unittest
from lib import giri_unittest

import time
import httplib

import appl.gon_client_device_service.gon_client_device_service_proxy as gon_client_device_service_proxy
import lib.cryptfacility



#class SecureStoreTest(unittest.TestCase):
#
#    def setUp(self):
#        self.secure_store = appl.gon_client_computer_token_service.gon_client_computer_token_main.SecureStore()
#        pass
#    
#    def tearDown(self):
#        pass
#
#    def test_serial(self):
#        serial_1 = self.secure_store.get_serial()
#
#    def test_keypair(self):
#        serial_1 = self.secure_store.get_serial()
#        public_key_1 = self.secure_store.get_public_key()
#
#
#    def test_sign(self):
#        serial = self.secure_store.get_serial()
#        public_key = self.secure_store.get_public_key()
#
#        challenge = "hej med dig"
#        challenge_signed = self.secure_store.sign_challenge(challenge)
#        self.assertNotEqual(challenge, challenge_signed)
#        self.assertTrue(lib.cryptfacility.pk_verify_challenge(public_key, challenge, challenge_signed))


class HTTPServerTest(unittest.TestCase):

    def setUp(self):
        self.device_service_proxy = gon_client_device_service_proxy.GOnClientComputerTokenProxy()
    
    def tearDown(self):
        pass
    
    def test_server(self):
        serial = self.device_service_proxy.get_serial() 
        self.assertTrue(serial is not None)

        public_key = self.device_service_proxy.get_public_key() 
        self.assertTrue(public_key is not None)
        
        challenge = "Hej med dig"
        challenge_signed = self.device_service_proxy.sign_challenge(challenge)
        print challenge_signed
        self.assertTrue(lib.cryptfacility.pk_verify_challenge(public_key, challenge, challenge_signed))
        

#        
#        (status, public_key) = self.http_server_query("/get_public_key")
#        self.assertEqual(status, 200)
#
#        (status, serial) = self.http_server_query("/get_serial")
#        self.assertEqual(status, 200)
#
#        challenge = "Hej med dig"
#        (status, challenge_signed) = self.http_server_query("/sign_challenge/%s" % base64.b64encode(challenge))
#        self.assertEqual(status, 200)
#        self.assertNotEqual(challenge, challenge_signed)
#        self.assertTrue(lib.cryptfacility.pk_verify_challenge(public_key, challenge, challenge_signed))




#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();

