import httplib
import base64

import gon_client_device_info


class GOnClientDeviceServiceProxy():
    def __init__(self):
        self._info = gon_client_device_info.GOnClientDeviceInfo()
    
    def http_server_query(self, path):
        try:
            (host, port) = self._info.read_host_and_port()
            conn = httplib.HTTPConnection(host, port)
            conn.request("GET", path)
            res = conn.getresponse()
            data = res.read()
            conn.close()
            return (res.status, data)
        except:
            pass
        return (None, None)

    def get_serial(self):
        (status, response) = self.http_server_query("/get_serial")
        if status == 200:
            return response
        return None
    
    def get_public_key(self):
        (status, response) = self.http_server_query("/get_public_key")
        if status == 200:
            return response
        return None

    def sign_challenge(self, challenge):
        (status, response) = self.http_server_query("/sign_challenge/%s" % base64.b64encode(challenge))
        if status == 200:
            return base64.b64decode(response)
        return None

    def is_enrolled(self):
        (status, response) = self.http_server_query("/is_enrolled")
        return status == 200 and response == '1'
    
    def set_enrolled(self):
        (status, response) = self.http_server_query("/set_enrolled")
        return status == 200 and response == 'OK'
    
    def reset_enrolled(self):
        (status, response) = self.http_server_query("/reset_enrolled")
        return status == 200 and response == 'OK'
    
    