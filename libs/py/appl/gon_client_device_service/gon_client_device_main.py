"""
This module contains the G/On Client Device Service

This is a mini web server that listen on a port and handles the following command given by url-path:
The host:port that the server listen on is reported to registry so that external programs can build the url.


Initilaization of secure store. Generates serial and reset key/pair
command:
  http://host:port/init
  OK

Generation of keypair:
command:
  http://host:port/generate_keypair
  OK

Get serial:
command:
  http://host:port/get_serial
  123456

Get public key:
command:
  http://host:port/get_public_key
  abcdef

Signe challenge using the public key:
command:
  http://host:port/sign_challenge/challenge
  xyz

"""

import sys
import os.path
import optparse
import threading
import time
import uuid
import BaseHTTPServer
import SocketServer
import base64
import traceback
import subprocess
import shutil

if sys.platform == 'win32':
    import win32security
    import ntsecuritycon as con


import lib.version
import lib.checkpoint 
import lib.utility

import lib.appl.crash.handler
import lib.appl.io_hooker
import lib.cryptfacility

import components.config.common
import gon_client_device_info
import components.communication.async_service

MODULE_ID = "appl_gon_device_service"



class ConfigOptions(components.config.common.ConfigClientDeviceService):
    """
    Options for the Configuration.
    """
    def __init__(self):
        components.config.common.ConfigClientDeviceService.__init__(self)
        self._parser = optparse.OptionParser()
        self._parser.add_option('--config', type='string', default='./gon_client_device_service.ini', help='configuration file')
        self._parser.add_option('--version', '-v', action='store_true', help='show version and exit')

        (self._options, self._args) = self._parser.parse_args()
        self.read_config_file(self._options.config)

    def cmd_show_version(self):
        return self._options.version


global_checkpoint_handler = None
global_secure_store = None


class SecureStore(object):
    SERIAL_FILENAME = "serial"
    PUBLIC_KEY_FILENAME = "public_key"
    PRIVATE_KEY_FILENAME = "private_key"
    ENROLLED_FILENAME = "enrolled"
    
    ENROLLED_VALUE_YES = "1"
    ENROLLED_VALUE_NO = "0"
    
    class SecureStoreError(Exception):
        def __init__(self, message):
            self.message = message

        def __str__(self):
            return repr(self.message)
    
    def __init__(self):
        pass
    
    def check(self):
        serial = None
        try:
            serial = self.get_serial()
        except:
            pass
        
        if serial is None:
            self.generate_serial()
            self.generate_keypair()
            self.reset_enrolled()
    
    def get_secure_root(self):
        if sys.platform == 'win32':
            installation_root = os.path.join(os.getcwd(), 'store')
        elif sys.platform == 'linux2':
            installation_root = os.path.join('/etc', 'giritech', 'device')
        elif sys.platform == 'darwin':
            installation_root = os.path.join('/ext', 'giritech', 'device')
        try:
            if not os.path.isdir(installation_root):
                os.makedirs(installation_root)
        except:
            print traceback.print_exc()
            return None
        
        self.set_access_rights(installation_root)
        return installation_root

    def read_data(self, filename):
        try:
            datafile = open(os.path.join(self.get_secure_root(), filename), 'rb')
            data = datafile.read()
            datafile.close()
            return data
        except:
            print traceback.print_exc()
        return None

    def write_data(self, filename, data):
        try:
            filename_abs = os.path.join(self.get_secure_root(), filename)
            datafile = open(filename_abs, 'wb')
            datafile.write(data)
            datafile.close()
            self.set_access_rights(filename_abs)
            return True
        except:
            print traceback.print_exc()
        return False

    def generate_keypair(self):
        (public_key, private_key) = lib.cryptfacility.pk_generate_keys()
        if not self.write_data(SecureStore.PUBLIC_KEY_FILENAME, public_key):
            raise SecureStore.SecureStoreError('Unable to generate keypair')
        if not self.write_data(SecureStore.PRIVATE_KEY_FILENAME, private_key):
            raise SecureStore.SecureStoreError('Unable to generate keypair')
    
    def generate_serial(self):
        serial = '%s' % uuid.uuid4()
        if not self.write_data(SecureStore.SERIAL_FILENAME, serial):
            raise SecureStore.SecureStoreError('Unable to generate serial')
        pass
    
    def get_public_key(self):
        public_key = self.read_data(SecureStore.PUBLIC_KEY_FILENAME)
        if public_key is None:
            raise SecureStore.SecureStoreError('Public key is not available')
        return public_key
    
    def get_serial(self):
        serial = self.read_data(SecureStore.SERIAL_FILENAME)
        if serial is None:
            raise SecureStore.SecureStoreError('Serial is not available')
        return serial

    def sign_challenge(self, challenge):
        private_key = self.read_data(SecureStore.PRIVATE_KEY_FILENAME)
        if private_key is None:
            raise SecureStore.SecureStoreError('Private key is not available')
        return lib.cryptfacility.pk_sign_challenge(private_key, challenge)

    def set_enrolled(self):
        if not self.write_data(SecureStore.ENROLLED_FILENAME, SecureStore.ENROLLED_VALUE_YES):
            raise SecureStore.SecureStoreError('Unable to set enrolled')

    def reset_enrolled(self):
        if not self.write_data(SecureStore.ENROLLED_FILENAME, SecureStore.ENROLLED_VALUE_NO):
            raise SecureStore.SecureStoreError('Unable to reset enrolled')

    def is_enrolled(self):
        enrolled = self.read_data(SecureStore.ENROLLED_FILENAME)
        return enrolled == SecureStore.ENROLLED_VALUE_YES

    def set_access_rights(self, path):
        if sys.platform == 'win32':
            system = win32security.ConvertStringSidToSid('S-1-5-18') # Local System
            admins = win32security.ConvertStringSidToSid('S-1-5-32-544') # Local Admins

            sd = win32security.GetFileSecurity(path, win32security.DACL_SECURITY_INFORMATION)
            dacl = win32security.ACL()
            dacl.AddAccessAllowedAce(win32security.ACL_REVISION, con.FILE_ALL_ACCESS, system)
            sd.SetSecurityDescriptorDacl(1, dacl, 0)
            win32security.SetFileSecurity(path, win32security.DACL_SECURITY_INFORMATION, sd)


class HTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    COMMAND_get_serial = 'get_serial'
    COMMAND_get_public_key = 'get_public_key'
    COMMAND_sign_challenge = 'sign_challenge'
    COMMAND_set_enrolled = 'set_enrolled'
    COMMAND_reset_enrolled = 'reset_enrolled'
    COMMAND_is_enrolled = 'is_enrolled'

    def get_path_elements(self):
        path_element_command = None
        path_element_args = []
        path_elements = self.path.split('/')
        if len(path_elements) >= 2:
            path_element_command =  path_elements[1]
            path_element_args = path_elements[2:]
        return path_element_command, path_element_args
    
    def do_GET(self):
        path_element_command, path_element_args = self.get_path_elements()
        if path_element_command == HTTPRequestHandler.COMMAND_get_serial:
            self.exec_command_get_serial()
        elif path_element_command == HTTPRequestHandler.COMMAND_get_public_key:
            self.exec_command_get_public_key()
        elif path_element_command == HTTPRequestHandler.COMMAND_sign_challenge:
            self.exec_command_sign_challenge(path_element_args)
        elif path_element_command == HTTPRequestHandler.COMMAND_set_enrolled:
            self.exec_command_set_enrolled()
        elif path_element_command == HTTPRequestHandler.COMMAND_reset_enrolled:
            self.exec_command_reset_enrolled()
        elif path_element_command == HTTPRequestHandler.COMMAND_is_enrolled:
            self.exec_command_is_enrolled()
        else:
            self.send_error(401)
        
    def exec_command_get_serial(self):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_get_serial", MODULE_ID, lib.checkpoint.INFO)
            serial = global_secure_store.get_serial()
            self.send_response_and_header(serial)
            global_checkpoint_handler.Checkpoint("main.exec_command_get_serial.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_get_serial.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))
        
    def exec_command_get_public_key(self):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_get_public_key", MODULE_ID, lib.checkpoint.INFO)
            get_public_key = global_secure_store.get_public_key()
            self.send_response_and_header(get_public_key)
            global_checkpoint_handler.Checkpoint("main.exec_command_get_public_key.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_get_public_key.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))
        
    def exec_command_sign_challenge(self, path_element_args):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_sign_challenge", MODULE_ID, lib.checkpoint.INFO)
            challenge = base64.b64decode(path_element_args[0])
            signed_challenge = global_secure_store.sign_challenge(challenge)
            self.send_response_and_header(base64.b64encode(signed_challenge))
            global_checkpoint_handler.Checkpoint("main.exec_command_sign_challenge.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_sign_challenge.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))

    def exec_command_set_enrolled(self):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled", MODULE_ID, lib.checkpoint.INFO)
            global_secure_store.set_enrolled()
            self.send_response_and_header('OK')
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))

    def exec_command_reset_enrolled(self):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled", MODULE_ID, lib.checkpoint.INFO)
            global_secure_store.reset_enrolled()
            self.send_response_and_header('OK')
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_set_enrolled.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))

    def exec_command_is_enrolled(self):
        try:
            global_checkpoint_handler.Checkpoint("main.exec_command_is_enrolled", MODULE_ID, lib.checkpoint.INFO)
            is_enrolled = global_secure_store.is_enrolled()
            if is_enrolled:
                self.send_response_and_header('1')
            else:
                self.send_response_and_header('0')
            global_checkpoint_handler.Checkpoint("main.exec_command_is_enrolled.ok", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.send_response_and_header('ERROR\n%s' % evalue)
            global_checkpoint_handler.Checkpoint("main.exec_command_is_enrolled.error", MODULE_ID, lib.checkpoint.INFO, error_message=str(evalue))


    def send_response_and_header(self, response):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.send_header("Content-Length", len(response))
        self.end_headers()
        self.wfile.write(response)

    def log_message(self, format, *args):
        pass
    
    def log_error(self, format, *args):
        pass


class HttpServer(threading.Thread):
    STATE_INIT = 0
    STATE_RUNNING = 1
    STATE_STOPPING = 2
    STATE_STOPPED = 3
    
    def __init__(self, checkpoint_handler, secure_store):
        threading.Thread.__init__(self, name="HttpServer")
        self.checkpoint_handler = checkpoint_handler
        self.state = HttpServer.STATE_INIT
        self.listen_host = '127.0.0.1'
        self.listen_port = 0
        self.httpd = SocketServer.TCPServer((self.listen_host, self.listen_port), HTTPRequestHandler)
        self.secure_store = secure_store
        
    def run(self):
        try:
            self.state = HttpServer.STATE_RUNNING
            (self.listen_host, self.listen_port) = self.httpd.server_address
            self.checkpoint_handler.Checkpoint("HttpServer.run", MODULE_ID, lib.checkpoint.INFO, listen_host=self.listen_host, listen_port=self.listen_port)
            try:
                self.secure_store.check();
                self.httpd.serve_forever()
            except:
                print traceback.print_exc()
            self.checkpoint_handler.Checkpoint("HttpServer.run_done", MODULE_ID, lib.checkpoint.INFO)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("ApplicationCommunication.run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        self.state = HttpServer.STATE_STOPPED

    def stop(self):
        self.checkpoint_handler.Checkpoint("HttpServer.stop", MODULE_ID, lib.checkpoint.INFO)
        self.state = HttpServer.STATE_STOPPING
        self.httpd.server_close()
        
    def is_running(self):
        return self.state in [HttpServer.STATE_RUNNING]

    def has_started(self):
        return self.state not in [HttpServer.STATE_INIT]





def main(service_stop_signal=[], service_stopped_signal=[], service_name=None):
    #
    # Setting current directory
    #
    if hasattr(sys, "frozen"):
        path = os.path.dirname(sys.executable)
        os.chdir(path)

    config = ConfigOptions()

    #
    # Show version and exit if requested
    #
    if(config.cmd_show_version()):
        print lib.version.Version.create_current().get_version_string()
        return 0


    #
    # Initialize logging
    
    #
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_handler(config.log_enabled,
                                                                      config.log_verbose,
                                                                      config.log_file,
                                                                      config.is_log_type_xml(),
                                                                      checkpoint_handler_parent=None)
    lib.appl.crash.handler.hook_crash_to_checkpoint(checkpoint_handler, MODULE_ID)
    lib.appl.io_hooker.IoHookerCheckpoint.hook(checkpoint_handler, MODULE_ID)

    checkpoint_handler.Checkpoint("version", MODULE_ID, lib.checkpoint.INFO,
                                  version=lib.version.Version.create_current().get_version_string(),
                                  build_date=lib.version.Version.create_current().get_build_date().isoformat(' '),
                                  service_name=service_name)
    
    global global_checkpoint_handler
    global_checkpoint_handler = checkpoint_handler

    global global_secure_store
    global_secure_store = SecureStore()

    #
    # Initialize communication component (and crypt lib)
    #
    components.communication.async_service.init(global_checkpoint_handler)

    httpServer = HttpServer(checkpoint_handler, global_secure_store)
    httpServer.start()
    while not httpServer.has_started():
        time.sleep(1)

    # Write in registry which host:port are used    
    gon_device_info = gon_client_device_info.GOnClientDeviceInfo()
    gon_device_info.write_host_and_port(httpServer.listen_host, httpServer.listen_port)
    
    while not service_stop_signal and httpServer.is_running():
        time.sleep(1)

    httpServer.stop()
    try:
        gon_device_info.reset()
    except:
        pass
    
    while httpServer.is_running():
        time.sleep(1)

    #
    # Signale the service handler that this program has ended
    #
    service_stopped_signal.append(7)
    checkpoint_handler.Checkpoint("stopped", MODULE_ID, lib.checkpoint.INFO)


if __name__ == '__main__':
    print 'This file is not intended to be run directly from the commandline,'
    print 'please use service file.'

    service_stop_signal=[]
    service_stopped_signal=[]
    main(service_stop_signal, service_stopped_signal)
