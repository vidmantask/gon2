import os.path
import marshal
import sys
import lib.utility


class GOnClientDeviceInfo():
    REG_KEY_ROOT = "SOFTWARE\Giritech\G-On Client\Device"
    REG_KEY_ROOT_BASE = "SOFTWARE\Giritech\G-On Client"

    def __init__(self):
        pass
    
    def has_write_access(self):
        return True
    
    def write_host_and_port(self, host, port):
        self._write('listen_host', host)
        self._write('listen_port', "%d" % port)

    def reset(self):
        self._remove()
    
    def read_host_and_port(self):
        listen_host = self._read('listen_host', None)
        listen_port = int(self._read('listen_port', 0))
        return (listen_host, listen_port)
    
    def _write(self, key, value):
        if sys.platform == 'win32':
            self._write_win(key, value)
        elif sys.platform == 'linux2':
            self._write_unix(key, value)
        elif sys.platform == 'darwin':
            self._write_unix(key, value)

    def _write_win(self, key, value):
        import _winreg as winreg 
        with winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE) as reg_handler:
            try:
                reg_key = winreg.CreateKey(reg_handler, GOnClientDeviceInfo.REG_KEY_ROOT)
                winreg.SetValueEx(reg_key, key, 0, winreg.REG_SZ, value)
                winreg.CloseKey(reg_key)        
            except WindowsError:
                (etype, evalue, etrace) = sys.exc_info()
                return evalue
        return None

    def _write_unix(self, key, value):
        try:
            reg_foldername = os.path.join('/','etc', 'giritech', 'device')
            reg_key_filename = os.path.join(reg_foldername, key)
            if not os.path.isdir(reg_foldername):
                os.makedirs(reg_foldername)
            reg_key_file = open(reg_key_filename, 'wb')
            reg_key_file.write(marshal.dumps(value))
            reg_key_file.close()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            return evalue
        return None

    def _read(self, key, default):
        if sys.platform == 'win32':
            return self._read_win(key, default)
        elif sys.platform == 'linux2':
            return self._read_unix(key, default)
        elif sys.platform == 'darwin':
            return self._read_unix(key, default)
        
    def _read_win(self, key, default):
        import _winreg as winreg 
        try:
            with winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE) as reg_handler:
                with winreg.OpenKey(reg_handler, GOnClientDeviceInfo.REG_KEY_ROOT) as reg_key_ginfo:
                    (key_value, key_value_type) = winreg.QueryValueEx(reg_key_ginfo, key)
                    return key_value
        except WindowsError:
            (etype, evalue, etrace) = sys.exc_info()
            return default
        return default
    
    def _read_unix(self, key, default):
        try:
            reg_foldername = os.path.join('/','etc', 'giritech', 'device')
            reg_key_filename = os.path.join(reg_foldername, key)
            reg_key_file = open(reg_key_filename, 'rb')
            reg_key_value = marshal.loads(reg_key_file.read())
            reg_key_file.close()
            return reg_key_value
        except:
            return default
        return default

    def _remove(self):
        if sys.platform == 'win32':
            self._remove_win()
        elif sys.platform == 'linux2':
            self._remove_unix()
        elif sys.platform == 'darwin':
            self._remove_unix()
        
    def _remove_win(self):
        import _winreg as winreg 
        try:
            with winreg.ConnectRegistry(None,  winreg.HKEY_LOCAL_MACHINE) as reg_handler:
                with winreg.OpenKey(reg_handler, GOnClientDeviceInfo.REG_KEY_ROOT_BASE, 0, winreg.KEY_SET_VALUE) as reg_key_ginfo:
                    winreg.DeleteKey(reg_key_ginfo, "Device")
        except WindowsError:
            (etype, evalue, etrace) = sys.exc_info()
            pass
        
    def _remove_unix(self):
        try:
            reg_foldername = os.path.join('/','etc', 'giritech', 'device')
            if os.path.isdir(reg_foldername):
                lib.utility.rmtree_fail_safe(reg_foldername)
                os.rmdir(reg_foldername)
        except:
            pass
