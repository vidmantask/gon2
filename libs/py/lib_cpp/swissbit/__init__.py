"""
This module contains the precompiled communication component
"""
import sys

is_64bits = sys.maxsize > 2**32


if sys.platform == 'win32':
    # pylint: disable-msg=E0611
    from ext_win.swissbit_ext import *  
elif sys.platform == 'linux2':
    if is_64bits:
        # pylint: disable-msg=E0611
        from ext_linux_64.swissbit_ext import *  
    else:    
        # pylint: disable-msg=E0611
        from ext_linux.swissbit_ext import *  
elif sys.platform == 'darwin':
    # pylint: disable-msg=E0611
    from ext_mac.swissbit_ext import *  

else:
    ERROR_MESSAGE = 'Swissbit MicroSmart not supported on this platform'
    
    def key_init():
        return (False, ERROR_MESSAGE)

    def key_create_challenge_signature(msc_filename, challenge):
        return (False, ERROR_MESSAGE, None)

    def key_generate_key_pair(msc_filename, keylength):
        return (False, ERROR_MESSAGE)

    def key_get_public_key(msc_filename):
        return (False, ERROR_MESSAGE, None)

    def key_write_file(msc_filename, file_id, data):
        return (False, ERROR_MESSAGE)

    def key_read_file(msc_filename, file_id):
        return (False, ERROR_MESSAGE, None)

