"""
This module contains the precompiled communication component
"""
import sys

is_64bits = sys.maxsize > 2**32

if sys.platform == 'win32':
    # pylint: disable-msg=E0611
    from ext_win.communication_ext import *  
elif sys.platform == 'linux2':
    if is_64bits:
        # pylint: disable-msg=E0611
        from ext_linux_64.communication_ext import *
    else:    
        # pylint: disable-msg=E0611
        from ext_linux.communication_ext import *  
elif sys.platform == 'darwin':
    # pylint: disable-msg=E0611
    from ext_mac.communication_ext import *  


