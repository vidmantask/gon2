# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys

import os.path
import ConfigParser
import ldap
import ldap.filter

from plugin_types.server_management import plugin_type_user
from plugin_types.server_management import plugin_type_config
from plugin_types.server_management import plugin_type_element

from plugin_modules.ldap.server_common.ldap_config import Config
from plugin_modules.ldap.server_common.login_module import LoginModule
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import plugin_modules.ldap.server_common.connection as ldap_connection
import lib.checkpoint

from plugin_modules.ldap.server_common.default_options import default_props_ad, default_props_edir, default_props_domino

config = Config(os.path.abspath(os.path.dirname(__file__)))    


#config = Config()    

class PluginAuthentication(plugin_type_user.PluginTypeUser):
    
    def __init__(self, checkpoint_handler, database, license_handler, config_dict):
        plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, database, license_handler, config_dict.get(Config.PLUGIN_NAME))

        self.login_postfix = config_dict.get(Config.LOGIN_SUFFIX)
        
        self.dn = config_dict.get("dn")
#        if not self.dn:
#            raise Exception("Option 'dn' not set in config file")
        
        hosts = config_dict.get("hosts")
        if not hosts:
            raise Exception("No host defined in config file")
        
        host_list = config.get_list(hosts)
        
        checkpoint_handler.Checkpoint("PluginAuthentication::init", "ldap", lib.checkpoint.DEBUG, version="new")
        
            
        try:
            trace_level = int(config_dict.get("trace_level", 0))
        except:
            trace_level = 0

        self.connection_factory = ldap_connection.LDAPConnectionFactory(checkpoint_handler, host_list, trace_level)
        if config.get_boolean(config_dict.get("ssl", False)):
            self.connection_factory.set_use_ssl(True)
            
        encoding = config_dict.get("encoding", "")
        if encoding:
            self.connection_factory.set_encoding(encoding)

        username = config_dict.get("username", None)
        self.connection_factory.set_username(username)

        password = config_dict.get("password", None)
        self.connection_factory.set_password(password)
        
        never_require_server_cert = config.get_boolean(config_dict.get("never_require_server_cert", False))
        if never_require_server_cert:
            self.connection_factory.add_ldap_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

        ca_cert_file = config_dict.get("ca_cert_file")
        if ca_cert_file:
            self.connection_factory.add_ldap_option(ldap.OPT_X_TLS_CACERTFILE, ca_cert_file)
        

        if config.get_boolean(config_dict.get("ad", False)):
            self.props = default_props_ad.copy()
            self.is_ad = True
        else:
            ldap_type = config_dict.get("ldap_type")
            if ldap_type == "domino":
                self.props = default_props_domino.copy()
            else:
                self.props = default_props_edir.copy()
            self.is_ad = False
            
        self.props.update(config_dict)

        self.user_id_property = self.props.get("user_id")
        self.binary_user_id = self.user_id_property.lower() in ldap_connection.LDAPSession.binary_fields 
        
        self.group_id_property = self.props.get("group_id")
        self.binary_group_id = self.group_id_property.lower() in ldap_connection.LDAPSession.binary_fields
        
        self._login_module = LoginModule(self.checkpoint_handler, self.plugin_name, config_dict)
        self._login_module.change_password_disabled = True
                
#        self.dn = "dc=giritech,dc=com"
#        self.server_handle =  connect("dudley", "giritech\pwl", "FREM2009e")
        
#        self.dn = "dc=giritech,dc=com"
#        self.connection_factory =  ldap_connection.LDAPConnectionFactory("dudley", 389, 1)
#        self.connection_factory.set_username("giritech\pwl")
#        self.connection_factory.set_password("FREM2009e")
#
#        
#        self.props = default_props_ad
        # get props from config
        
        

    element_types = ["user", "group"]

        
        
    @classmethod
    def generator(cls, checkpoint_handler, database, license_handler):
        domains = dict()
        
        if config.enabled:
            license = license_handler.get_license()
            if license.has('Feature', 'LDAP User Directory'):
                for section in config.authorization_domains:
                    try:
                        plugin = PluginAuthentication(checkpoint_handler, database, license_handler, section)
                        domains[plugin.plugin_name] = plugin
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("load_ldap_module.error", "ldap", lib.checkpoint.ERROR, etype, evalue, etrace)
            else:
                checkpoint_handler.Checkpoint("load_ldap_module", "ldap", lib.checkpoint.ERROR, msg="LDAP User Directory enabled without license")
        return domains

    


    def _get_value(self, ldap_dict, name, default=None):
        value = ldap_dict.get(name, None)
        if value is None:
            return default
        if isinstance(value, list):
            if value:
                return self._decode_value(value[0])
            else:
                return default
        return self._decode_value(value)
    
    def _decode_value(self, value):
        if isinstance(value, basestring):
            return value.decode("utf-8")
        return value

    def get_user_by_id(self, ldap_session, user_id, attr_list):
        user_id_property = self.props.get("user_id")
        if user_id_property=="dn":
            return ldap_session.query(user_id, "(objectClass=*)", attr_list, scope=ldap.SCOPE_BASE)
        else:
            if self.binary_user_id:
                user_id = ldap_connection.LDAPSession.string2binary(user_id, self.user_id_property)
            search_predicate = "(%s=%s)" % (user_id_property, user_id)
            search_str = "(&%s%s)" % (self.props.get("user_query"), search_predicate)
            return ldap_session.query(self.dn, search_str, attr_list)
            

    def user_exists(self, user_id):
        with self.connection_factory.get_connection() as ldap_session:
            user_obj = self.get_user_by_id(ldap_session, user_id, ["dn"])
            if user_obj:
                return True
            return False

    def get_login_and_name_from_id(self, user_id, internal_user_login):
        with self.connection_factory.get_connection() as ldap_session:
            user_name_property = self.props.get("user_name")
#            user_obj = ldap_session.query(user_id, "(objectClass=*)", [user_name_property, user_label_property])
            
            user_obj = self.get_user_by_id(ldap_session, user_id, [user_name_property] + self._login_module.user_login_list)
            if not user_obj:
                self.checkpoint_handler.Checkpoint('get_login_and_name_from_id', self.plugin_name, lib.checkpoint.ERROR, message='User not found', user_id=user_id)
                raise Exception("User '%s' not found" % user_id)
            user_obj = user_obj[0]
            internal_user_login =  ldap_session.get_query_value(user_obj, "dn")
            user_login = None
            for user_label_property in self._login_module.user_login_list:
                user_login = ldap_session.get_query_value(user_obj, user_label_property, "")
                if user_login:
                    if not '@' in user_login:
                        user_login = "%s@%s" % (user_login, self._login_module.login_postfix)
                    break
            if not user_login:
                user_login = internal_user_login
            user_name = ldap_session.get_query_value(user_obj, user_name_property, "")
            if not user_name:
                user_name = internal_user_login
            return internal_user_login, user_login, user_name
        

    def fetch_users(self, search_filter=None):
        """Get list of AD group names suitable as parameteres to is_logged_in_name"""

        attr_list = list(set([self.user_id_property, 
                              self.props.get("user_label"),
                              self.props.get("user_name"),
                              ]))
        
        user_login_list = self._login_module.user_login_list
        attr_list.extend(user_login_list)
        with self.connection_factory.get_connection() as ldap_session:
            if not search_filter:
                lusers = ldap_session.query(self.dn, self.props.get("user_query"), attr_list)
            else:
                user_label_property = self.props.get("user_label")
                user_name_property = self.props.get("user_name")
                search_predicates = []
                if user_label_property and user_label_property!="dn":
                    search_predicates.append("(%s=%s)" % (user_label_property, search_filter))
                if user_name_property and user_name_property!="dn":
                    search_predicates.append("(%s=%s)" % (user_name_property, search_filter))
                search_predicates.extend(["(%s=%s)" % (login_property, search_filter) for login_property in user_login_list])
                if search_predicates:
                    search_predicate = "(|%s)" % "".join(search_predicates)
                    search_str = "(&%s%s)" % (self.props.get("user_query"), search_predicate)
                    lusers = ldap_session.query(self.dn, search_str, attr_list)
                else:
                    lusers = []
                
            
            for ldap_dict in lusers:
#                login_name = "%s(@%s)" % (ldap_session.get_query_value(ldap_dict, self.props.get("user_label"), ""), self._login_module.login_postfix)
                login_name =  ldap_session.get_query_value(ldap_dict, "dn")
                for login_name_property in user_login_list:
                    login_name_property_value = ldap_session.get_query_value(ldap_dict, login_name_property)
                    if login_name_property_value:
                        if '@' in login_name_property_value:
                            login_name = login_name_property_value
                        else:
                            login_name = "%s(@%s)" % (login_name_property_value, self._login_module.login_postfix)
                        break
                id = ldap_session.get_query_value(ldap_dict, self.user_id_property)
                if self.binary_user_id:
                    id = ldap_connection.LDAPSession.binary2string(id, self.user_id_property)
                fullname = ldap_session.get_query_value(ldap_dict, self.props.get("user_name"), "")
                user = self.create_element(id, login_name, fullname)                    
                yield user




    def fetch_groups(self, search_filter=None):
        """Get list of AD group names suitable as parameteres to is_logged_in_member"""

        with self.connection_factory.get_connection() as ldap_session:
            
            group_elements = config.get_list(self.props.get("group_elements"))
            if 'group' in group_elements:

                attr_list = list(set([self.props.get("group_id"), 
                                      self.props.get("group_label"),
                                      self.props.get("group_info"),
                                      ]))
                
                if not search_filter:
                    lgroups = ldap_session.query(self.dn, self.props.get("group_query"), attr_list)
                else:
                    group_label_property = self.props.get("group_label")
                    group_info_property = self.props.get("group_info")
                    search_predicates = []
                    if group_label_property and group_label_property!="dn":
                        search_predicates.append("(%s=%s)" % (group_label_property, search_filter))
                    if group_info_property and group_info_property!="dn":
                        search_predicates.append("(%s=%s)" % (group_info_property, search_filter))
                    if search_predicates:
                        search_predicate = "(|%s)" % "".join(search_predicates)
                        search_str = "(&%s%s)" % (self.props.get("group_query"), search_predicate)
                        lgroups = ldap_session.query(self.dn, search_str, attr_list)
                    else:
                        lgroups = []
                
                    
                for ldap_dict in lgroups:
                    name = ldap_session.get_query_value(ldap_dict, self.props.get("group_label"), "")
                    group_id = ldap_session.get_query_value(ldap_dict, self.props.get("group_id"))
                    if self.binary_group_id:
                        group_id = ldap_connection.LDAPSession.binary2string(group_id, self.group_id_property)
                    id = "group.%s" % group_id
                    info = ldap_session.get_query_value(ldap_dict, self.props.get("group_info"), "")
                    group = self.create_element(id, "%s (%s)"  % (name, self.login_postfix), info)
                    yield group

            if 'ou' in group_elements:

                attr_list = list(set([self.props.get("ou_id"), 
                                      self.props.get("ou_label"),
                                      self.props.get("ou_info"),
                                      ]))
                
                if not search_filter:
                    lous = ldap_session.query(self.dn, self.props.get("ou_query"), attr_list)
                else:
                    ou_label_property = self.props.get("ou_label")
                    ou_info_property = self.props.get("ou_info")
                    search_predicates = []
                    if ou_label_property and ou_label_property!="dn":
                        search_predicates.append("(%s=%s)" % (ou_label_property, search_filter))
                    if ou_info_property and ou_info_property!="dn":
                        search_predicates.append("(%s=%s)" % (ou_info_property, search_filter))
                    if search_predicates:
                        search_predicate = "(|%s)" % "".join(search_predicates)
                        search_str = "(&%s%s)" % (self.props.get("ou_query"), search_predicate)
                        lous = ldap_session.query(self.dn, search_str, attr_list)
                    else:
                        lous = []
                    
                for ldap_dict in lous:
                    name = ldap_session.get_query_value(ldap_dict, self.props.get("ou_label"), "")
                    id = "ou.%s" % ldap_session.get_query_value(ldap_dict, self.props.get("ou_id"))
                    info = ldap_session.get_query_value(ldap_dict, self.props.get("ou_info"), "")
                    group = self.create_element(id, name, info)
                    yield group
                    

    def _get_group_members(self, ldap_session, group_dn, users, user_label_prop, seen_groups):
        if group_dn in seen_groups:
            return
        seen_groups.add(group_dn)
        
        attr_list = ["member"]
        
        if self.is_ad:
            attr_list.append("primaryGroupToken")
        lgroup = ldap_session.query(group_dn, self.props.get("group_query"), attr_list, scope=ldap.SCOPE_BASE)
        if lgroup:
            group_dict = lgroup[0]
            member_list = group_dict.get("member", [])
            user_attr_list = [user_label_prop]
            if self.user_id_property != "dn":
                user_attr_list.append(self.user_id_property)
            for member_dn in member_list:
                try:
                    user = ldap_session.query(member_dn, self.props.get("user_query"), user_attr_list, scope=ldap.SCOPE_BASE)
                    if user:
                        user_id = ldap_session.get_query_value(user[0], self.user_id_property)
                        if self.binary_user_id:
                            user_id = ldap_connection.LDAPSession.binary2string(user_id, self.user_id_property)
                        user_label = ldap_session.get_query_value(user[0], user_label_prop)
                        users.add((user_id, user_label))
                    else:
                        self._get_group_members(ldap_session, member_dn, users, user_label_prop, seen_groups)
                except Exception, e:
                    print e
            if self.is_ad:
                group_rid = ldap_session.get_query_value(group_dict, "primaryGroupToken")
                primary_group_members = ldap_session.query(self.dn, "(&(PrimaryGroupID=%s)%s)" % (group_rid, self.props.get("user_query")), user_attr_list)
                for user in primary_group_members:
                    user_id = ldap_session.get_query_value(user, self.user_id_property)
                    if self.binary_user_id:
                        user_id = ldap_connection.LDAPSession.binary2string(user_id, self.user_id_property)
                    user_label = ldap_session.get_query_value(user, user_label_prop)
                    users.add((user_id, user_label))
                    
                

    def get_group_members(self, group_id):
        with self.connection_factory.get_connection() as ldap_session:
            user_label_prop = self.props.get("user_label")
            group_type, dummy, id = group_id.partition(".")
            if group_type=="group":
                users = set()
                seen_groups = set()
                group_dn = self._login_module.get_group_dn(id)
                self._get_group_members(ldap_session, group_dn, users, user_label_prop, seen_groups)
            elif group_type=="ou":
                group_dn = id
                user_attr_list = [user_label_prop]
                if self.user_id_property != "dn":
                    user_attr_list.append(self.user_id_property)
                user_result = ldap_session.query(group_dn, self.props.get("user_query"), user_attr_list)
                users = []
                for user in user_result:
                    user_id = ldap_session.get_query_value(user, self.user_id_property)
                    if self.binary_user_id:
                        user_id = ldap_connection.LDAPSession.binary2string(user_id, self.user_id_property)
                    user_label = ldap_session.get_query_value(user, user_label_prop)
                    users.append((user_id, user_label))
            else:
                raise Exception("Unknown group type '%s'" % group_type)
        
        return users
        


    def get_id_column(self):
        return plugin_type_config.Column(self.plugin_name, 'id',   plugin_type_config.Column.TYPE_STRING, 'dn', hidden=True, is_id=True)


        
    
    def get_email(self, user_id):
        with self.checkpoint_handler.CheckpointScope('get_email', self.plugin_name, lib.checkpoint.DEBUG, user_id=user_id):
            with self.connection_factory.get_connection() as ldap_session:
                user_email_property = self.props.get("user_email")
                user_obj = self.get_user_by_id(ldap_session, user_id, [user_email_property])
                if not user_obj:
                    self.checkpoint_handler.Checkpoint('get_email', self.plugin_name, lib.checkpoint.ERROR, message='User not found', user_id=user_id)
                    raise Exception("User '%s' not found" % user_id)
     
                user_obj = user_obj[0]
                return ldap_session.get_query_value(user_obj, user_email_property, "")

    def get_login_module(self):
        return self._login_module

    def get_plugin_title(self):
        return self.login_postfix
