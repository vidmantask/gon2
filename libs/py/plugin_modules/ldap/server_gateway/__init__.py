"""
ldap plugin for Gateway Server
"""
from __future__ import with_statement
from components.communication import message
from plugin_modules.ldap.server_common.ldap_config import Config
from plugin_types.server_gateway import plugin_type_auth, plugin_type_traffic, plugin_type_user
import ConfigParser
import datetime
import time
import ldap
import ldap.filter
import lib.checkpoint
import os.path
import plugin_modules.ldap.server_common.connection as ldap_connection
import sys

from plugin_modules.ldap.server_common.login_module import LoginModule


config = Config(os.path.abspath(os.path.dirname(__file__)))    



class PluginAuthentication(plugin_type_traffic.PluginTypeTraffic, plugin_type_user.PluginTypeUser, LoginModule):
    """
    Server part of ldap module, plugging into server
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, config_dict):
        name = config_dict.get(Config.PLUGIN_NAME)
        plugin_type_traffic.PluginTypeTraffic.__init__(self, async_service, checkpoint_handler, name, license_handler, session_info)
        plugin_type_user.PluginTypeUser.__init__(self, async_service, checkpoint_handler, name, license_handler, session_info, database, management_message_session, access_log_server_session)
        LoginModule.__init__(self, checkpoint_handler, self.plugin_name, config_dict)


        
        

    @classmethod
    def generator(cls, async_service, checkpoint_handler, license_handler, session_info, management_message_session, access_log_server_session, database):
        domains = dict()
        if config.enabled:
            license = license_handler.get_license()
            if license.has('Feature', 'LDAP User Directory'):
                for config_dict in config.authorization_domains:
                    try:
                        plugin = PluginAuthentication(async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, config_dict)
                        domains[plugin.plugin_name] = plugin
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("load_ldap_module.error", config_dict.get(Config.PLUGIN_NAME, "Unnamed directory"), lib.checkpoint.ERROR, etype, evalue, etrace)
            else:
                checkpoint_handler.Checkpoint("load_ldap_module", "ldap", lib.checkpoint.ERROR, msg="LDAP User Directory enabled without license")
        return domains


    def start(self):
        """
        Initiates the login process
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::start", self.plugin_name, lib.checkpoint.DEBUG) as cps:
            self.request_login()
            self.set_started()


    
    def login_cancelled(self):
        self.authenticated = False
        self.set_ready()

    def receive_login(self, login, password, internal_user_login=None):
        LoginModule.receive_login(self, login, password, internal_user_login)
        if not self.must_change_password:
            self.set_ready()

    
    def change_password_cancelled(self):
        if self.ldap_session:
            self.ldap_session.close()
        self.ldap_session = None
        if self.must_change_password:
            self.authenticated = False
            self.set_ready()
    
                 
        
        
    def get_attribute(self, attribute_name):
        value = self.props.get(attribute_name)
        if not value:
            try:
                with self.connection_factory.get_connection() as ldap_session:
                    users = ldap_session.query(self.user_dn, "(objectClass=*)", [attribute_name], scope=ldap.SCOPE_BASE)
                    assert(len(users)==1)
                    user_dict = users[0]
                    return ldap_session.get_query_value(user_dict, attribute_name)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_attribute", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                return None
                
        
        return value
    
    
    def is_domain(self, dns=None):
        if not dns:
            return True
        domain = self.get_attribute("domain")
        return dns.lower() == domain.lower()
    
    def sso_authenticate_user(self, nt_user_name, user_sid):
        return LoginModule.sso_authenticate_user(self, nt_user_name, user_sid)
