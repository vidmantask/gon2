"""
Server config specification plugin  

"""
from __future__ import with_statement

import os.path
import shutil

import lib.checkpoint
import lib.utility.support_file_filter as support_file_filter

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import plugin_types.server_config.plugin_type_user_config_specification as plugin_type_user_config_specification
from plugin_modules.ldap.server_common.default_options import default_props_ad, default_props_edir, default_props_domino, default_props_titles

import plugin_modules.ldap
import plugin_modules.ldap.server_common.ldap_config as ldap_config
from plugin_types.server_config import plugin_type_upgrade


class ConfigSpecificationPluginLDAP(plugin_type_user_config_specification.PluginTypeUserConfigSpecification):


#class ConfigSpecificationPluginLDAP():
    ID = 'plugin_ldap'
    TITLE = 'LDAP User Directory Plugin Configuration' 
    DESCRIPTION = 'This is the description of LDAP User Directory Plugin Configuration' 

    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_user_config_specification.PluginTypeUserConfigSpecification.__init__(self, checkpoint_handler, server_configuration_all, __name__, "ldap")
        self.server_configuration_all = server_configuration_all
        self.management_config = None
        self.enabled = False
        self.template_list = []


    def _get_existing_directories(self, gateway_config, management_config):
        existing_dirs = None
        if gateway_config.authorization_domains:
            existing_dirs = gateway_config.authorization_domains
        
        if not existing_dirs and management_config.authorization_domains:
            existing_dirs = management_config.authorization_domains
        
        return existing_dirs


    def _get_configurations(self):
        management_config = ldap_config.Config(self.get_path_management_ini(), raise_error=False)
        gateway_config = ldap_config.Config(self.get_path_gateway_ini(), raise_error=False)
        return gateway_config, management_config


    def get_path_management_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_management_plugin_root(server_configuration_all, 'ldap')

    def get_path_gateway_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_gateway_plugin_root(server_configuration_all, 'ldap')

    def get_version(self):
        return lib.version.Version.create_from_encode(plugin_modules.ldap.plugin_module_version)
    
    
    def test_config(self, template, test_type=None):

        import ldap
        import plugin_modules.ldap.server_common.connection as ldap_connection
        
        config_spec = ConfigTemplateSpec(template)

        dn = config_spec.get_value("dn")
        hosts = config_spec.get_value("hosts")
        ssl = config_spec.get_value("ssl")
        ca_cert_file = config_spec.get_value("ca_cert_file")
        never_require_server_cert = config_spec.get_value("never_require_server_cert")
        username = config_spec.get_value("username")
        password = config_spec.get_value("password")
        

        try:
            if len(hosts.strip())==0:
                return False, "No host(s) defined"
            
            host_list = [name.strip() for name in hosts.split(",")]

            self.checkpoint_handler.Checkpoint("test.error", plugin_modules.ldap.MODULE_ID, lib.checkpoint.ERROR, first_host=host_list[0])

            success = []
            errors = []
            for host in host_list:
                connection_factory = ldap_connection.LDAPConnectionFactory(self.checkpoint_handler, [host])
                if ssl:
                    connection_factory.set_use_ssl(True)
                connection_factory.set_username(username)
                connection_factory.set_password(password)
                
                if never_require_server_cert:
                    connection_factory.add_ldap_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
            
                if ca_cert_file:
                    connection_factory.add_ldap_option(ldap.OPT_X_TLS_CACERTFILE, ca_cert_file)
                
                
                user_query = config_spec.get_value("user_query")
                user_id_property =  config_spec.get_value("user_id")
                user_label_property =  config_spec.get_value("user_label")
                user_login_property =  config_spec.get_value("user_login")
                search_properties = [name.strip() for name in user_login_property.split(",") if name]
                search_properties.append(user_id_property)
                search_properties.append(user_label_property)

                try:
                    with connection_factory.get_connection() as ldap_session:
                        ldap_session.query(dn, user_query, search_properties, scope=ldap.SCOPE_ONELEVEL)
                    success.append("%s: LDAP connection ok" % host)
                except Exception, e:
                    errors.append((host, repr(e)))

            if success:
                if len(host_list)==1 or not errors:
                    return True, "LDAP connection ok"
                else:
                    msg = "\n".join(success)
                    msg += "\n"
                    msg += "\n".join(["%s: Error: %s " % (host, e) for (host,e) in errors])
                    return True, msg
            else:
                if len(errors) > 1:
                    msg = "\n".join(["%s: Error: %s " % (host, e) for (host,e) in errors])
                else:
                    msg = errors[0][1]
                return False, msg
                
        
        except Exception, e:
            return False, repr(e)
    
    def get_type_and_priority(self):
        return ("User", 2)
    
    def get_title(self):
        return "Directory names"



    def _convert_boolean_value(self, management_config, existing_directory, field_name):
        if existing_directory.has_key(field_name):
            try:
                existing_directory[field_name] = management_config.get_boolean(existing_directory.get(field_name))
            except ValueError, e:
                self.checkpoint_handler.Checkpoint("_convert_boolean_value.error", plugin_modules.ldap.MODULE_ID, lib.checkpoint.ERROR, field_name=field_name, value=existing_directory.get(field_name))
                raise e


    def get_management_config(self):
        if not self.management_config:
            self.management_config = ldap_config.Config(self.get_path_management_ini(), raise_error=False)
        return self.management_config

    def is_enabled(self):
        management_config = self.get_management_config()
        return management_config.enabled
    
    def check_template(self, template):
        config_spec = ConfigTemplateSpec(template)
        if config_spec.get_field(ldap_config.Config.DIRECTORY_NAME):
            dir_name = config_spec.get_value(ldap_config.Config.DIRECTORY_NAME)
            if not dir_name:
                raise Exception("Directory name cannot be empty")
            config_spec.set_title(dir_name)
            dir_name_error = ldap_config.Config.dir_name_error(dir_name)
            
            if dir_name_error:
                raise Exception(dir_name_error)
        else:
            dir_name = config_spec.get_value(ldap_config.Config.PLUGIN_NAME)

        if not config_spec.get_value('hosts'):
            raise Exception("Server host list cannot be empty")
        
        return dir_name

    def _get_default_props(self, ldap_type):
        if ldap_type == "ad":
            return default_props_ad
        elif ldap_type == "domino":
            return default_props_domino
        else:
            return default_props_edir
        

    def get_config_specifications(self, parent_module_id):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        config_specs = dict()

        license = self.server_configuration_all.get_license_handler().get_license()
        ldap_disabled = not license.has('Feature', 'LDAP User Directory')
        
        if not ldap_disabled:
        
            management_config = self.get_management_config()
            existing_dirs = management_config.authorization_domains
        
        
            for existing_directory in existing_dirs:
                dir_name = existing_directory.get(ldap_config.Config.PLUGIN_NAME)
                
                self._convert_boolean_value(management_config, existing_directory, 'ad')
                self._convert_boolean_value(management_config, existing_directory, 'ssl')
                self._convert_boolean_value(management_config, existing_directory, 'never_require_server_cert')
                self._convert_boolean_value(management_config, existing_directory, 'change_password_disabled')
                
                is_ad = existing_directory.get('ad')
                ldap_type = existing_directory.get("ldap_type")
                if not ldap_type:
                    ldap_type = "ad" if is_ad else "unknown"
                
                config_spec = self.create_common_spec(ldap_type, parent_module_id, dir_name, self.DESCRIPTION, False)
                config_spec.set_values_from_dict(existing_directory)
        
                config_specs[dir_name] = config_spec.get_template()
       
        return config_specs


    def create_common_spec(self, ldap_type, parent_module_id, title, description, is_create):
        config_spec = ConfigTemplateSpec()
        
        config_spec.init(ldap_type, title, description)
        is_ad = ldap_type=="ad"
        default_props = self._get_default_props(ldap_type)
        
        if is_create:
            field = config_spec.add_field(ldap_config.Config.DIRECTORY_NAME, 'Directory name', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True, tooltip="Name for directory, e.g. the domain. This name is used as part of the plugin name to identify the directory and can never be changed")
        else:
            field = config_spec.add_field(ldap_config.Config.PLUGIN_NAME, 'Plugin name', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, mandatory=True, read_only=True)

        config_spec.add_test_config_action(field, parent_module_id, sub_type=self.ID, title="Test Connection")

        ldap_type_field_type = ConfigTemplateSpec.FIELD_TYPE_HIDDEN if is_create else ConfigTemplateSpec.FIELD_TYPE_EDIT
        config_spec.add_field("ldap_type", 'LDAP Type', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ldap_type_field_type, advanced=False, read_only=True, value=ldap_type)
            
        config_spec.add_field('hosts', 'Server host list', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="List of hosts in the form server[:port], e.g. myserver:636 - default port is 389")
        config_spec.add_field('ad', 'Is Active Directory', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, advanced=False, value=is_ad)
        config_spec.add_field('ssl', 'Use SSL', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False)
        config_spec.add_field('username', 'User DN', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
        field_password = config_spec.add_field('password', 'Password', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
        field_password.set_attribute_secret(True)
        config_spec.add_field('dn', 'Root DN', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="The base dn, e.g. dc=example,dc=com>")
        config_spec.add_field(ldap_config.Config.LOGIN_SUFFIX, 'Full login suffix', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)

        config_spec.add_field('domain', 'Domain (dns)', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Domain used for single sign on in AD")
        config_spec.add_field('netbios', 'Netbios', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=not is_ad, tooltip="Netbios name used for single sign on in AD")

        config_spec.add_field("user_id", 'User ID property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Unique ID property for a user. Do NOT change after users have been registered")
        config_spec.add_field("group_id", 'Group ID property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Unique ID property for a group. Do NOT change after adding groups to authorization rules")

        config_spec.add_field('ca_cert_file', 'SSL Certificate', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True)
        config_spec.add_field('never_require_server_cert', "Don't Require Server Certificate", ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
        config_spec.add_field('change_password_disabled', 'Password Change Disabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True)
        config_spec.add_field('password_expiry_warning', 'Password Expiry Warning Time', ConfigTemplateSpec.VALUE_TYPE_INTEGER, value=14, advanced=True)
        
        config_spec.add_field("user_label", 'User name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The user name property shown in G/On Management")
        config_spec.add_field("user_name", 'User full name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The full user name property used for searches in G/On Management")
        config_spec.add_field("user_query", 'User query', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="LDAP query to find users in directory")
        config_spec.add_field("user_login", 'User login properties', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="List of properties which can be used for login")
        config_spec.add_field("user_group_property", 'User group membership property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Property on user object containing group memberships")
        config_spec.add_field("user_password", 'User password property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="Property on user object containing password")
        
        management_config = self.get_management_config()
        group_elements = management_config.get_list(default_props.get("group_elements"))
        if "group" in group_elements:
            config_spec.add_field("group_label", 'Group name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The group name property shown in G/On Management")
            config_spec.add_field("group_info", 'Group full name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The full group name property used for searches in G/On Management")
            config_spec.add_field("group_query", 'Group query', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="LDAP query to find groups in directory")
        
        if "ou" in group_elements:
            config_spec.add_field("ou_label", 'Organizational Unit name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The organizational unit name property shown in G/On Management")
            config_spec.add_field("ou_info", 'Organizational Unit full name property', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="The full organizational unit name property used for searches in G/On Management")
            config_spec.add_field("ou_query", 'Organizational Unit query', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, tooltip="LDAP query to find organizational units in directory")
        
        config_spec.set_values_from_dict(default_props)
        
        return config_spec
    
        

    def get_create_config_specification(self, parent_module_id):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        license = self.server_configuration_all.get_license_handler().get_license()
        ldap_disabled = not license.has('Feature', 'LDAP User Directory')
        
        if ldap_disabled:
            return None
        
        
        config_spec_ad = self.create_common_spec("ad", parent_module_id, "LDAP to AD", self.DESCRIPTION, True)
        config_spec_domino = self.create_common_spec("domino", parent_module_id, "LDAP to Domino", self.DESCRIPTION, True)
        config_spec_other = self.create_common_spec("unknown", parent_module_id, "LDAP to other directory", self.DESCRIPTION, True)
        
        return [config_spec_ad.get_template(), config_spec_domino.get_template(), config_spec_other.get_template()]

    def save_data(self, enabled, template_list):
        self.enabled = enabled
        self.template_list = template_list

    def finalize(self):
        authorization_domains = []
        for template in self.template_list:
            config_spec = ConfigTemplateSpec(template)
            
            if config_spec.get_value("ad"):
                values = default_props_ad.copy()
            else:
                ldap_type = config_spec.get_value("ldap_type")
                if ldap_type == "domino":
                    values = default_props_domino.copy()
                else:
                    values = default_props_edir.copy()
            
            values.update(config_spec.get_field_value_dict())
            authorization_domains.append(values)
            
            if config_spec.get_field(ldap_config.Config.DIRECTORY_NAME):
                dir_name = config_spec.get_value(ldap_config.Config.DIRECTORY_NAME)
                if not dir_name:
                    raise Exception("Directory name cannot be empty")
                values[ldap_config.Config.PLUGIN_NAME] = "ldap_%s" % dir_name.replace(".","_")
                if not values.get(ldap_config.Config.LOGIN_SUFFIX):
                    values[ldap_config.Config.LOGIN_SUFFIX] = dir_name
        
        ldap_config.Config.write(self.get_path_management_ini(), self.enabled, authorization_domains)
        ldap_config.Config.write(self.get_path_gateway_ini(), self.enabled, authorization_domains)
        
        self.management_config = None

    def generate_support_package(self, support_package_root):
        """
        This method is called with a folder where the plugin can dump files/info. The information dumped will be packaed to a support package for giritech.
        """
        support_log_filename_abs = os.path.join(support_package_root, 'ldap.txt') 
        support_log = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(support_log_filename_abs, False), None)

        support_log.Checkpoint('Yo ldap', plugin_modules.ldap.MODULE_ID, lib.checkpoint.DEBUG)
        self._copy_ini_files(support_log, support_package_root, remove_sensitive_info=True)

    def backup(self, backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all):
        """
        This method is called with a folder where the plugin can backup files/info. The information dumped will be included in the backup.
        """
        self._copy_ini_files(backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all)


    def _copy_ini_files(self, logger, dest_root, cb_config_backup=None, alternative_server_configuration_all=None, remove_sensitive_info=False):
        ini_filename_src = os.path.join(self.get_path_management_ini(alternative_server_configuration_all), ldap_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'management_%s' % ldap_config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.ldap.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("LDAP-Plugin ini file not found, see log file.")

        ini_filename_src = os.path.join(self.get_path_gateway_ini(alternative_server_configuration_all), ldap_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'gateway_%s' % ldap_config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.ldap.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("LDAP-Plugin ini file not found, see log file.")


    def restore(self, restore_log, restore_package_plugin_root, cb_restore):
        ini_filename_src = os.path.join(restore_package_plugin_root, 'management_%s' % ldap_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_management_ini(), ldap_config.Config.CONFIG_FILENAME) 
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)
        
        ini_filename_src = os.path.join(restore_package_plugin_root, 'gateway_%s' % ldap_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_gateway_ini(), ldap_config.Config.CONFIG_FILENAME) 
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)
