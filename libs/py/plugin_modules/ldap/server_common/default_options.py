'''
Created on 13/11/2009

@author: peter
'''

default_props_titles = {
                       'user_id' : ('User ID property', 'Unique ID for a user'),
                       'user_label' : ('User name (short) property', 'The user name used in G/On Management'), 
                       'user_name' : ('User full name', 'The full user name used in G/On Management'),
                       'user_query' : ('User ldap query', 'LDAP query for finding users'),
                       'user_email' : ('User email', 'The full user name used in G/On Management'),

                       'user_login' : 'cn, sn',
                       'user_group_property' : 'groupMembership',
                       'user_password' : 'userPassword',
                       
                       'group_elements' : 'group, ou',
                       
                       'group_id' : 'dn',
                       'group_label' : 'cn',
                       'group_info' : 'description',
                       'group_query' : '(&(objectClass=group)(!(objectClass=rbsScope2)))',

                       'ou_id' : 'dn',
                       'ou_label' : 'dn',
                       'ou_info' : 'name',
                       'ou_query' : '(objectClass=organizationalUnit)',
                       
                       
                      }

default_props_edir = {
                       'user_id' : 'dn',
                       'user_label' : 'cn',
                       'user_query' : '(objectClass=user)',
                       'user_name' : 'fullName',
                       'user_email' : 'mail',

                       'user_login' : 'cn, sn',
                       'user_group_property' : 'groupMembership',
                       'user_password' : 'userPassword',
                       
                       'group_elements' : 'group, ou',
                       
                       'group_id' : 'dn',
                       'group_label' : 'cn',
                       'group_info' : 'description',
                       'group_query' : '(&(objectClass=group)(!(objectClass=rbsScope2)))',

                       'ou_id' : 'dn',
                       'ou_label' : 'dn',
                       'ou_info' : 'name',
                       'ou_query' : '(objectClass=organizationalUnit)',
                       
                       'ldap_type' : 'edirectory'
                       
                      }

default_props_domino = {
                       'user_id' : 'uid',
                       'user_label' : 'cn',
                       'user_query' : '(objectClass=dominoPerson)',
                       'user_name' : 'displayName',
                       'user_email' : 'mail',

                       'user_login' : 'mail, uid',
                       'user_group_property' : 'dominoAccessGroups',
                       'user_password' : 'userpassword',
                       
                       'group_elements' : 'group',
                       
                       'group_id' : 'dn',
                       'group_label' : 'cn',
                       'group_info' : 'displayName',
                       'group_query' : '(objectClass=dominoGroup)',

                       'ou_id' : 'dn',
                       'ou_label' : 'dn',
                       'ou_info' : 'name',
                       'ou_query' : '(objectClass=organizationalUnit)',

                       
                       'ldap_type' : 'domino'
                       
                      }


default_props_ad   = {
                       'user_id' : 'objectGUID',
                       'user_label' : 'samaccountname',
                       'user_query' : '(&(objectClass=user)(objectCategory=person)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))',
                       'user_name' : 'displayName',
                       'user_email' : 'mail',

                       'user_login' : 'userPrincipalName,samAccountName',
#                       'user_query' : '(objectClass=user)',
                       'user_group_property' : 'memberOf',
                       'user_password' : 'unicodePwd',

                       'group_elements' : 'group',
                       
                       'group_id' : 'objectGUID',
                       'group_label' : 'cn',
                       'group_info' : 'description',
                       'group_query' : '(objectClass=group)',

                       'ou_id' : 'dn',
                       'ou_label' : 'dn',
                       'ou_info' : 'name',
                       'ou_query' : '(objectClass=organizationalUnit)',
                       
                       'ldap_type' : 'ad'
                       
                      }

login_property_queries = {
                          "__default__" :   ["(${_}=${login})"],
                          
                          "userprincipalname" : ["(${_}=${login_with_at})",
                                                 "(${_}=${login}@${domain})"],
                          
                          "mail" : ["(${_}=${login_with_at})"],
                        } 


def get_login_query(property_name):
    query = login_property_queries.get(property_name.lower())
    if not query:
        query = login_property_queries.get("__default__")
    return query
