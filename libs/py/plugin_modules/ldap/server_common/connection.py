'''
Created on 01/07/2009

@author: pwl
'''
from __future__ import with_statement
import ldap
import ldap.async
import ldap.filter
from ldap.cidict import cidict
import binascii

import lib.checkpoint
import sys

        
class LDAPSession(object):
    
    binary_fields = ["objectguid", "objectsid"]
    guid_fields = ["objectguid"]
    
    def __init__(self, checkpoint_handler, host, ssl=False, trace_level=0, encoding="utf-8", options=[]):
        prefix = "ldaps" if ssl else "ldap"
        ldap_uri = "%s://%s" % (prefix, host)
        self.handle = ldap.initialize(ldap_uri, trace_level)
        checkpoint_handler.Checkpoint("LDAPSession::init", "ldap", lib.checkpoint.DEBUG, options=options)
        for option in options:
            self.handle.set_option(option[0], option[1])
        if ssl:
            self.handle.set_option(ldap.OPT_X_TLS_NEWCTX, 0)            
        self.encoding = encoding
        self.checkpoint_handler = checkpoint_handler

    def __enter__(self):
        return self
    
    def _decode_value(self, value):
        if isinstance(value, basestring):
            return value.decode(self.encoding)
        if isinstance(value, list):
            return [self._decode_value(v) for v in value]
        return value
        
    def encode(self, value):
        return value.encode(self.encoding)
    
    
    @classmethod
    def _hex_revert(cls, str):
        rev_list = [ str[i-2:i] for i in range(len(str),0,-2)]
        return "".join(rev_list)
    
    @classmethod
    def convert_to_guid(cls, value):
        return "{%s-%s-%s-%s-%s}" % (cls._hex_revert(value[:8]), 
                                     cls._hex_revert(value[8:12]), 
                                     cls._hex_revert(value[12:16]), 
                                     value[16:20], 
                                     value[20:]) 
    

    @classmethod
    def convert_from_guid(cls, value):
        value = value.replace("-", "")
        value = value[1:len(value)-1]
        
        return "%s%s%s%s%s" % (cls._hex_revert(value[:8]), 
                                 cls._hex_revert(value[8:12]), 
                                 cls._hex_revert(value[12:16]), 
                                 value[16:20], 
                                 value[20:]) 
    
    @classmethod
    def binary2string(cls, value, field_name=None):
        hex_value = binascii.b2a_hex(value)
        if field_name:
            if field_name.lower() in cls.guid_fields:
                hex_value = cls.convert_to_guid(hex_value)
        return hex_value

    @classmethod
    def string2binary(cls, value, field_name=None, filter_escape=True):
        if field_name:
            if field_name.lower() in cls.guid_fields:
                value = cls.convert_from_guid(value)
        bin_value = binascii.a2b_hex(value)
        if filter_escape:
            bin_value = ldap.filter.escape_filter_chars(bin_value, 2)
        return bin_value

    def get_query_value(self, result_dict, key, default_value=None, decode=False):
        value_list = self.get_query_values(result_dict, key, [default_value], decode)
        return value_list[0]

    def get_query_values(self, result_dict, key, default_value=[], decode=False):
        value_list = result_dict.get(key, default_value)
        if decode:
            value_list = self._decode_value(value_list)
        return value_list
        
    def bind(self, username, password):
        self.handle.bind_s(username.encode(self.encoding), password.encode(self.encoding))
    
    def query1(self, root_dn, filter, attr_list, no_conversion_attrs=[]):
        
        result = self.handle.search_s(root_dn, ldap.SCOPE_SUBTREE, filter.encode(self.encoding), attr_list)
        return_result = []
        conversion_list = [a for a in attr_list if not a in no_conversion_attrs]
        for (dn, result_dict) in result:
            if dn:
                result_dict = cidict(result_dict)
                for key in conversion_list:
                    value = result_dict.get(key)
                    if not value is None:
                        if key.lower() in self.binary_fields:
                            result_dict[key] = value
                        else:
                            result_dict[key] = self._decode_value(value)
                        
                result_dict['dn'] = [self._decode_value(dn)]
                return_result.append(result_dict)
        return return_result

    def _remove_duplicates(self, l):
        return list(set([e.encode(self.encoding) for e in l]))

    def query(self, root_dn, filter, attr_list, no_conversion_attrs=[], scope=ldap.SCOPE_SUBTREE, log_error = True):
        try:
            attr_list = self._remove_duplicates(attr_list)
            search = ldap.async.List(self.handle)
            search.startSearch(root_dn.encode(self.encoding), scope, filter.encode(self.encoding), attr_list)
    #        search.startSearch(root_dn, ldap.SCOPE_SUBTREE, filter.encode(self.encoding), attr_list)
    
            with self.checkpoint_handler.CheckpointScope("LDAPSession::processResults", "ldap", lib.checkpoint.DEBUG):        
                try:
                  partial = search.processResults()
                  if partial:
                      self.checkpoint_handler.Checkpoint("LDAPSession::query", "ldap", lib.checkpoint.WARNING, root_dn=root_dn, filter=filter, msg='Only partial results received')
                except ldap.SIZELIMIT_EXCEEDED:
                  self.checkpoint_handler.Checkpoint("LDAPSession::query", "ldap", lib.checkpoint.WARNING, root_dn=root_dn, filter=filter, msg='Server-side size limit exceeded')
            
            with self.checkpoint_handler.CheckpointScope("LDAPSession::build_result", "ldap", lib.checkpoint.DEBUG):        
                return_result = []
                conversion_list = [a for a in attr_list if not a in no_conversion_attrs]
                for res in search.allResults:
                    (dn, result_dict) = res[1]
                    if dn:
                        result_dict = cidict(result_dict)
                        converted_values = dict()
                        for key in conversion_list:
                            value = result_dict.get(key)
                            if not value is None:
                                if key.lower() in self.binary_fields:
                                    result_dict[key] = value
                                else:
                                    converted_values[key] = self._decode_value(value)
                        result_dict['dn'] = [self._decode_value(dn)]
                        result_dict.update(converted_values)
                        return_result.append(result_dict)
            return return_result
        except:
            if log_error:
                self.checkpoint_handler.Checkpoint("LDAPSession::query", "ldap", lib.checkpoint.ERROR, scope=scope, root_dn=root_dn, filter=filter, attr_list=attr_list, msg='Exception raised')
            raise

    def test(self):
        try:
            self.query("", "(objectClass=o)", [], log_error=False)
        except ldap.SERVER_DOWN:
            return False
        except:
            pass
        return True
            
            
    def close(self):
        if self.handle:
            try:
                self.handle.unbind()
            except:
                pass
    
    def __exit__(self, type, value, traceback):
        self.close()


class LDAPConnectionFactory(object):
    
    def __init__(self, checkpoint_handler, host_list=[], trace_level=0):
        self.host_list = host_list
        self.trace_level = trace_level
        self.ssl = False
        self.encoding = "utf-8"
        self.username = None
        self.password = None
        self.working_host = None
        self.ldap_options = []
        self.checkpoint_handler = checkpoint_handler


    def set_username(self, value):
        self.username = value


    def set_password(self, value):
        self.password = value


    def set_use_ssl(self, use_ssl):
        self.ssl = use_ssl
        
    def set_encoding(self, encoding):
        self.encoding = encoding
        
    def add_ldap_option(self, key, value):
        self.ldap_options.append((key, value))

    def _get_connection(self, host, test):
        session = LDAPSession(self.checkpoint_handler, host, self.ssl, self.trace_level, self.encoding, self.ldap_options)
        if test and not session.test():
            return None
        if self.username and self.password:
            try:
                session.bind(self.username, self.password)
            except ldap.SERVER_DOWN:
                if test:
                    return None
                else:
                    raise
            except:
                self.checkpoint_handler.Checkpoint("LDAPConnectionFactory::get_connection.bind_error", "ldap", lib.checkpoint.ERROR, host=host)
                if test:
                    return None
                else:
                    raise
        return session
        
    def get_connection(self):
        if self.host_list and len(self.host_list) == 1:
            return self._get_connection(self.host_list[0], False)
        # None or more than one ldap server:
        if self.working_host:
            session = self._get_connection(self.working_host, True)
            if session:
                return session
        if not self.host_list:
            raise Exception("Host(s) not specified")
        # Find host that works
        tmp_host_list = list(self.host_list)
        if self.working_host:
            # No need to try this host again right now
            try:
                tmp_host_list.remove(self.working_host)
            except ValueError:
                pass
        while tmp_host_list:
            host = tmp_host_list.pop(0)
            session = self._get_connection(host, True)
            if session:
                self.working_host = host
                return session
            else:
                self.checkpoint_handler.Checkpoint("LDAPConnectionFactory::get_connection.server_down", "ldap", lib.checkpoint.WARNING, host=host)
        raise Exception("All ldap servers down: %s"  % self.host_list)
        
        


#    def query(self, filter, session=None):
#        if session:
#            handle = self.server_handle
#        return handle.search_s(self.domain, ldap.SCOPE_SUBTREE, filter.encode("utf-8"))
        