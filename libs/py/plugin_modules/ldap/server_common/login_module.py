from __future__ import with_statement
from components.communication import message
from plugin_modules.ldap.server_common.ldap_config import Config
import ConfigParser
import datetime
import time
import string
import ldap
import ldap.filter
import lib.checkpoint
import os.path
import plugin_modules.ldap.server_common.connection as ldap_connection
import sys

from plugin_modules.ldap.server_common.default_options import default_props_ad, default_props_edir, default_props_domino, get_login_query
from plugin_types.common.plugin_type_user import LoginModuleBase

if sys.platform == 'win32':
    import win32com.adsi.adsicon
    ADS_UF_DONT_EXPIRE_PASSWD = win32com.adsi.adsicon.ADS_UF_DONT_EXPIRE_PASSWD
else:
    ADS_UF_DONT_EXPIRE_PASSWD = 0x10000



    
    
class LoginModule(LoginModuleBase):
    """
    Server part of ldap module, plugging into server
    """
    def __init__(self, checkpoint_handler, plugin_name, config_dict):
        
        self.checkpoint_handler = checkpoint_handler
        self.plugin_name = plugin_name

        self.login_postfix = config_dict.get(Config.LOGIN_SUFFIX)
        self.dn = config_dict.get("dn")
#        if not self.dn:
#            raise Exception("Option 'dn' not set in config file")
        
        hosts = config_dict.get("hosts")
        if not hosts:
            raise Exception("No host defined in config file")

        host_list = Config.get_list(hosts)
        
#        self.server_handle = ldap.initialize("ldaps://192.168.45.140:636", 1)
#        self.server_handle = ldap.initialize("ldap://192.168.45.140:389", 1)
        try:
            trace_level = int(config_dict.get("trace_level", 0))
        except:
            trace_level = 0

        
        self.connection_factory = ldap_connection.LDAPConnectionFactory(checkpoint_handler, host_list, trace_level)
        
        use_ssl = Config.get_boolean(config_dict.get("ssl", False))
        if use_ssl:
            self.connection_factory.set_use_ssl(True)

        encoding = config_dict.get("encoding", "")
        if encoding:
            self.connection_factory.set_encoding(encoding)

        self.change_password_disabled = Config.get_boolean(config_dict.get("change_password_disabled", False))

        username = config_dict.get("username", None)
        self.connection_factory.set_username(username)

        password = config_dict.get("password", None)
        self.connection_factory.set_password(password)
        
        never_require_server_cert = Config.get_boolean(config_dict.get("never_require_server_cert", False))
        if never_require_server_cert:
            self.connection_factory.add_ldap_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

        ca_cert_file = config_dict.get("ca_cert_file")
        if ca_cert_file:
            self.connection_factory.add_ldap_option(ldap.OPT_X_TLS_CACERTFILE, ca_cert_file)
        

        if Config.get_boolean(config_dict.get("ad", False)):
            self.props = default_props_ad.copy()
            self.is_ad = True
        else:
            ldap_type = config_dict.get("ldap_type")
            if ldap_type == "domino":
                self.props = default_props_domino.copy()
            else:
                self.props = default_props_edir.copy()
            self.is_ad = False
            
        if self.is_ad and not use_ssl:
            if not self.change_password_disabled:
                self.checkpoint_handler.Checkpoint("PluginAuthentication::init", self.plugin_name, lib.checkpoint.WARNING, msg="Forced disable of change password feature. The feature requires ssl connection")
            self.change_password_disabled = True
            
        self.props.update(config_dict)
        self.checkpoint_handler.Checkpoint("PluginAuthentication::init::props", self.plugin_name, lib.checkpoint.DEBUG, **self.props)

        self.user_login_list = Config.get_list(self.props.get("user_login"))
        self.login = ''
        self.password = ''
        self.user_dn = None
        self.user_dn_list = None
        self.user_id = None
        self.group_dn_list = []
        self.primary_group_rid = None
        self.recursive_group_membership = True
        self.recursive_group_memberships_calculated = False
        
        self.users_found = dict()
        self.ldap_session = None
        
        self.user_id_property = self.props.get("user_id")
        if self.user_id_property == "dn":
            self.user_id_property = None
            self.binary_user_id = False
        else:
            self.binary_user_id = self.user_id_property.lower() in  ldap_connection.LDAPSession.binary_fields 

        self.group_id_property = self.props.get("group_id")
        if self.group_id_property == "dn":
            self.group_id_property = None
            self.binary_group_id = False
        else:
            self.binary_group_id = self.group_id_property.lower() in  ldap_connection.LDAPSession.binary_fields 
        
        if config_dict.has_key("password_expiry_warning"):
            self.set_password_warning_days(config_dict.get("password_expiry_warning"))
            
        self.max_password_age = None # Used for AD password expiry calculation
        
        


    def is_logged_in_name(self, login):
        """
        True if the user is logged in as login
        """
        user_id = login[0]
        if self.authenticated and user_id:
            return self.user_id == user_id
        return False
    
    def addate2datetime(self, date_str):
        time_nano = long(date_str)
        # convert to microseconds
        time_milli = time_nano/10000
        time_milli -= 11644473600000L # adjust factor for converting it to python epoch (1970) from Windows epoch (1601)
        # convert to seconds
        time_seconds = time_milli/1000
        return datetime.datetime.fromtimestamp(time_seconds)
        
        


    def get_group_dn(self, group_id):
        if self.group_id_property:
            try:
                with self.connection_factory.get_connection() as ldap_session:
                    if self.binary_group_id:
                        group_id = ldap_connection.LDAPSession.string2binary(group_id, self.group_id_property)
                    search_predicate = "(%s=%s)" % (self.group_id_property, group_id)
                    search_str = "(&%s%s)" % (self.props.get("group_query"), search_predicate)
                    groups = ldap_session.query(self.dn, search_str, ["dn"])
                    if len(groups)==1:
                        group_dict = groups[0]
                        return ldap_session.get_query_value(group_dict, "dn")
                    else:
                        return None
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_group_dn", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                return None
        else:
            return group_id

    def is_logged_in_member(self, group):
        """
        True if the user is logged in as member of group
        """
        #name = group[1]
        if self.authenticated:
            try:
                group_id = group[0]
                if group_id.startswith("group."):
                    id = group_id.partition("group.")[2]
                    group_dn = self.get_group_dn(id)
                    if group_dn:
                        try:
                            self.group_dn_list.index(group_dn)
                            return True
                        except ValueError:
                            pass
                        
                        # Check primary group membership if ad and not already found
                        if self.is_ad and self.primary_group_rid:
                            with self.connection_factory.get_connection() as ldap_session:
                                groups = ldap_session.query(group_dn, "(objectClass=*)", ['primaryGroupToken'], scope=ldap.SCOPE_BASE)
                                if len(groups)==1:
                                    group_dict = groups[0]
                                    primaryGroupToken = ldap_session.get_query_value(group_dict, "primaryGroupToken")
                                    if primaryGroupToken==self.primary_group_rid:
                                        self.group_dn_list.append(group_dn)
                                        self.primary_group_rid = None
                                        return True
                                    
                        if self.recursive_group_membership and not self.recursive_group_memberships_calculated:
                            self.get_recursive_group_memberships()
                            try:
                                self.group_dn_list.index(group_dn)
                                return True
                            except ValueError:
                                pass
                                    
                        return False
                if group_id.startswith("ou."):
                    ou = group_id.partition("ou.")[2]
                    if ou:
                        if not self.user_dn_list:
                            self.user_dn_list = self._create_dn_list(self.user_dn)
                        ou_list = self._create_dn_list(ou)
                        if len(ou_list)<len(self.user_dn_list):
                            for i in range(0,len(ou_list)):
                                if ou_list[i]!=self.user_dn_list[i]:
                                    return False
                            return True
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("is_logged_in_member", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)

        return False
    
    def get_group_ids_for_logged_in_user(self):
        return None


    def _create_dn_list(self, dn):
        l = [s.strip().lower() for s in dn.split(",")]
        l.reverse()
        return l

    def is_authenticated(self):
        return self.authenticated
    


    def login_template_substitute(self, template, property, login=None, login_with_at=None):
        try:
            if login:
                return template.substitute(_=property, login=login, **self.props)
            elif login_with_at:
                return template.substitute(_=property, login_with_at=login_with_at, **self.props)
            else:
                return template.substitute(_=property, **self.props)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.Checkpoint("login_template_substitute", self.plugin_name, lib.checkpoint.ERROR, template=template.template, property=property, login=login, **self.props)
            self.checkpoint_handler.CheckpointException("login_template_substitute", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            raise
            
    

    def _lookup_user(self, user_login, ldap_session):
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::lookup_user", self.plugin_name, lib.checkpoint.DEBUG, user_login=user_login) as cps:
            user_found = self.users_found.get(user_login)
            if user_found:
                return user_found
            
            user_query = self.props.get("user_query")
            if not user_query.startswith("("):
                user_query = "(%s)" % user_query

            if '@' in user_login:
                login_with_at = user_login
                login_without_at = user_login.rsplit("@")[0]
            else:
                login_with_at = None
                login_without_at = user_login
                
            
            for property in self.user_login_list:
                queries = get_login_query(property)
                for query in queries:
                    has_at = "login_with_at" in query
                    template = string.Template(query)
                    if has_at:
                        if login_with_at:
                            search_query = self.login_template_substitute(template, property, login_with_at=login_with_at)
                        else:
                            search_query = None
                    else:
                        if login_with_at:
                            if user_login.lower().endswith("@" + self.login_postfix):
                                search_query = self.login_template_substitute(template, property, login=login_without_at)
                            else:
                                search_query = None
                        else:
                            search_query = self.login_template_substitute(template, property, login=login_without_at)
                        
                    if search_query:
                        filter = "(&%s%s)" % (user_query, search_query)
                        lusers = ldap_session.query(self.dn, filter, [])
                        results = len(lusers)
                        if results==1:
                            self.users_found[user_login] = lusers[0] 
                            return lusers[0]
            return None

    def lookup_user(self, user_login):
        try:
            with self.connection_factory.get_connection() as ldap_session:
                user_dict = self._lookup_user(user_login, ldap_session)
                if user_dict:
                    return ldap_session.get_query_value(user_dict, "dn")
                return None
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("lookup_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            return None
            
        
    
    def lookup_user1(self, user_login):
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::lookup_user", self.plugin_name, lib.checkpoint.DEBUG, login=user_login):
            if '@' in user_login:
                if user_login.lower().endswith("@" + self.login_postfix):
                    user_login = user_login.rsplit("@")[0]
                else:
                    return None
            try:
                with self.connection_factory.get_connection() as ldap_session:
                    user = self._lookup_user(ldap_session, user_login)
                if user:
                    return ldap_session.get_query_value(user, "dn")
                return None
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("lookup_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                return None
            
        
    def _lookup_user1(self, ldap_session,user_login):
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::_lookup_user", self.plugin_name, lib.checkpoint.DEBUG) as cps:
            user_found = self.users_found.get(user_login)
            if user_found:
                return user_found
            user_query = self.props.get("user_query")
            if not user_query.startswith("("):
                user_query = "(%s)" % user_query
            
            for property in self.user_login_list:
                search_user_login = user_login
                if self.is_ad and property.lower()=="userprincipalname":
                    if not '@' in user_login:
                        search_user_login = "%s@*" % user_login
                filter = "(&%s(%s=%s))" % (user_query, property, search_user_login)
                lusers = ldap_session.query(self.dn, filter, [])
                results = len(lusers)
                if results==1:
                    self.users_found[user_login] = lusers[0] 
                    return lusers[0]
            return None

    def receive_login(self, login, password, internal_user_login=None):
        
        """
        Recieves result from login and password prompt and continues the authentication proces
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_login", self.plugin_name, lib.checkpoint.DEBUG, login=login):
            self.authenticated = False
            self.login = login
            self.login_error_code = self.login_error_message = None
            
            if self._CheckPassword(self.login, password, internal_user_login):
                self.checkpoint_handler.Checkpoint("PluginAuthentication::receive_login.entered", self.plugin_name, lib.checkpoint.DEBUG, login=login, user_dn=self.user_dn)
                self.authenticated = True


    def _get_primary_group_dn(self, ldap_session, user_dict):
        try:
            if sys.platform == 'win32':
                import win32security    
                
                def convert_to_sid(objectSid):
                    if objectSid is None: return None
                    return win32security.SID (objectSid)
    
                def convert_to_domain_sid(py_sid):
                    if py_sid is None: return None
                    partition = str(py_sid).rpartition('-')
                    partition = partition[0].rpartition(':')
                    return partition[2]
    
                  
                group_rid = ldap_session.get_query_value(user_dict, "primaryGroupID")
                user_sid = convert_to_sid(ldap_session.get_query_value(user_dict, "objectSid"))
              
                domain_sid = convert_to_domain_sid(user_sid)
                
                group_sid_str = domain_sid + "-" + str(group_rid)
                
                filter = "objectSid=%s" % group_sid_str
                lgroups = ldap_session.query(self.dn, filter, [])
                
                if lgroups:
                    return ldap_session.get_query_value(lgroups[0],"dn")
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_primary_group_dn.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
        return None        

    def _CheckPassword(self, login, password, internal_user_login=None):
        """
        Private function for validating username and password
        """
        
        # Bind with empty password is accepted as an anomynous bind, so we have to reject it here
        if not password:
            self.login_error_message = "Empty password given"
            return False
        
        ldap_session = None
        try:
            ldap_session = self.connection_factory.get_connection()
            if not internal_user_login:
                user_dict = self._lookup_user(ldap_session, login)
                if not user_dict:
                    self.login_error_message = "The user could not be found"
                    return False
                self.user_dn = ldap_session.get_query_value(user_dict, "dn")
                if self.user_id_property:
                    self.user_id = ldap_session.get_query_value(user_dict, self.user_id_property)
                else:
                    self.user_id = self.user_dn
            else:
                self.user_dn = internal_user_login
            
            
            self.checkpoint_handler.Checkpoint("PluginAuthentication::attempt_login", self.plugin_name, lib.checkpoint.DEBUG, user_dn=self.user_dn)
            try:
                
                #self.server_handle.bind_s("cn=Peter,ou=testou2,ou=testou1,ou=testou,o=GC", "pwl")
                ldap_session.bind(self.user_dn, password)
                self.checkpoint_handler.Checkpoint("PluginAuthentication::login_succeeded", self.plugin_name, lib.checkpoint.DEBUG)
               
                self.password = password  # FIXME: Security issue???
               
                users = ldap_session.query(self.user_dn, "(objectClass=*)", [], scope=ldap.SCOPE_BASE)
                
                assert(len(users)==1)
                user_dict = users[0]
               
                if self.user_id_property:
                    self.user_id = ldap_session.get_query_value(user_dict, self.user_id_property)
                    if self.binary_user_id:
                        self.user_id = ldap_connection.LDAPSession.binary2string(self.user_id, self.user_id_property) 
                    
                else:
                    self.user_id = self.user_dn
                
                ldap_type = self.props.get("ldap_type")
                if ldap_type == "domino":
                    try:
                        # Must search explicit for group member property
                        user_group_property = self.props.get("user_group_property")
                        user_dict1 = ldap_session.query(self.user_dn, "(objectClass=*)", [user_group_property], scope=ldap.SCOPE_BASE)[0]
                        self.group_dn_list = ldap_session.get_query_values(user_dict1, self.props.get("user_group_property"))
                    except Exception, e:
                        self.checkpoint_handler.Checkpoint("find_user_groups", self.plugin_name, lib.checkpoint.WARNING, mgs="Unable to find group membership", error=repr(e))
                        
                    self.recursive_group_memberships_calculated = True
                else:
                    self.group_dn_list = ldap_session.get_query_values(user_dict, self.props.get("user_group_property"), decode=True)  
                
                if self.is_ad:
                    primary_group_dn = self._get_primary_group_dn(ldap_session, user_dict)
                    if primary_group_dn:
                        self.group_dn_list.append(primary_group_dn)
                    else:
                        self.primary_group_rid = ldap_session.get_query_value(user_dict, "primaryGroupID")


                        
                try:
                    if not self.change_password_disabled:
                        self._check_password_expiration(ldap_session, user_dict)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("_check_password_expiration.failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                    
               
                return True
            
            except ldap.INVALID_CREDENTIALS, err:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("_CheckPassword.failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                
                self.login_error_code = "-1"
                message = err.args[0]
                if type(message) == dict and message.has_key('desc'):
                    self.login_error_message = message['desc']
                else:
                    self.login_error_message = message

        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_CheckPassword.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            self.login_error_code = "-1"
            self.login_error_message = str(e)
        finally:
            if ldap_session and not self.ldap_session:
                ldap_session.close()
            
        return False
    
    
    def get_recursive_group_memberships(self):
        with self.connection_factory.get_connection() as ldap_session:
            group_dn_list = [g for g in self.group_dn_list]
            for group_dn in group_dn_list:
                self._get_recursive_group_memberships(ldap_session, group_dn)
            self.recursive_group_memberships_calculated = True

    def _get_recursive_group_memberships(self, ldap_session, group_dn):
        try:
            groups = ldap_session.query(group_dn, "(objectClass=*)", [], scope=ldap.SCOPE_BASE)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_recursive_group_memberships", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            return
        assert len(groups)==1
        parent_groups = ldap_session.get_query_values(groups[0], self.props.get("user_group_property"), decode=True)
        for parent_group in parent_groups:
            if not parent_group in self.group_dn_list:
                self.group_dn_list.append(parent_group)
                self._get_recursive_group_memberships(ldap_session, parent_group)
    
    def ad_encode_password(self, password):
        quoted_password = '"%s"' % password
        return quoted_password.encode("utf-16le")
    

    def get_normalised_login(self, user_login):
        if '@' in user_login:
            return user_login
        else:
            return "%s@%s" % (user_login, self.login_postfix)
    
                
    def receive_changed_password(self, old_password, new_password):
        '''This should work according to doc, but we get this error on our sample edirectory :  {'info': 'Unrecognized extended operation', 'desc': 'Protocol error'} 
        self.server_handle.passwd_s(self.user_dn, self.password, new_password)
        '''
        if self.change_password_disabled:
            return "It is not possible to change password in this configuration"
        try:
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_changed_password", self.plugin_name, lib.checkpoint.DEBUG) as cps:
                password_property = self.props.get("user_password")
                try:
                    if self.is_ad:
                        if not self.connection_factory.ssl:
                            return "Change password in Active Directory is disabled on non-encrypted connections"
                    
                    
    #                return "Change password is currently not supported on Active Directory via LDAP"
                
        #            self.connection_factory.ssl = True
                    if not self.ldap_session:
                        self.ldap_session = self.connection_factory.get_connection() 
                    
                    cps.add_complete_attr(user_dn = self.user_dn)
                    self.ldap_session.bind(self.user_dn, old_password)
                    
                    attr = self.ldap_session.encode(password_property)
                    if self.is_ad:
                        old_password_encoded = self.ad_encode_password(old_password)
                        new_password_encoded = self.ad_encode_password(new_password)
                    else:
                        old_password_encoded = self.ldap_session.encode(old_password)
                        new_password_encoded = self.ldap_session.encode(new_password)
                        
                    new = (ldap.MOD_ADD, attr, [new_password_encoded])
                    old = (ldap.MOD_DELETE, attr, [old_password_encoded])
                    self.ldap_session.handle.modify_s(self.ldap_session.encode(self.user_dn), [old, new])
                    
                    self.ldap_session.close()
                    self.ldap_session = None
                    cps.add_complete_attr(password_changed="Yes")
        
                    self.password = new_password
                    if self.must_change_password:
                        self.must_change_password = False
                        self.receive_login(self.login, new_password)
                    return None
                except ldap.LDAPError, err:
                    cps.add_complete_attr(password_changed="No")
                    message = err.args[0]
                    if type(message) == dict and message.has_key('desc'):
                        message = message['desc']
                    if not message:
                        message = "Error : %s" % str(err)
                    cps.add_complete_attr(error_message=message)
                    return message
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("receive_changed_password", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            return "Unexpected error while changing password"
                
    
    
    def _check_password_expiration(self, ldap_session, user_object):
        if self.change_password_disabled or self._password_expiration_warning_days <= 0:
            return
        if not self.is_ad:
            pw_expiraton_time_list = user_object.get("passwordExpirationTime")
            if pw_expiraton_time_list:
                pw_expiraton_time_str = pw_expiraton_time_list[0]
                pw_expiration_time = datetime.datetime.strptime(pw_expiraton_time_str, "%Y%m%d%H%M%SZ")
                self.days_to_password_expiration = (pw_expiration_time - datetime.datetime.now()).days
        else:
            
            intUserAccountControl = int(ldap_session.get_query_value(user_object, "userAccountControl"))
            never_expires = intUserAccountControl & ADS_UF_DONT_EXPIRE_PASSWD
            if never_expires:
                return 

            #Determine When Password Was Changed
            last_changed_str = ldap_session.get_query_value(user_object, "pwdLastSet")
            if not last_changed_str:
                return 
            
            pwd_last_changed =  self.addate2datetime(last_changed_str)
            
            if not self.max_password_age:
                root_obj = ldap_session.query(self.dn, "(distinguishedName=%s)" % self.dn, [], scope=ldap.SCOPE_BASE)[0]
                self.max_password_age = abs(long(ldap_session.get_query_value(root_obj, "maxPwdAge")))
                
            now = datetime.datetime.now()
            max_password_delta = datetime.timedelta(microseconds=self.max_password_age/10)
             
            time_to_expire = pwd_last_changed + max_password_delta - now
            self.days_to_password_expiration = time_to_expire.days
            
        
        if not self.days_to_password_expiration is None and self.days_to_password_expiration < self.get_password_warning_days():
            self.ldap_session = ldap_session
                 
        
        
    
    def get_current_user_id(self):
        if self.authenticated:
            return self.user_id
    
    def sso_authenticate_user(self, nt_user_name, user_sid):
        if not self.is_ad:
            return False
        try:
            with self.connection_factory.get_connection() as ldap_session:
                if nt_user_name:
                    user_dict = None
                    sAMAccountName = nt_user_name.split("\\")[-1]
                    user_query = self.props.get("user_query")
                    filter = "(&%s(%s=%s))" % (user_query, "sAMAccountName", sAMAccountName)
                    users = ldap_session.query(self.dn, filter, [])
                    if len(users)!=1:
                        self.checkpoint_handler.Checkpoint("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, msg="Found %s users matching name '%s'" % (len(users), sAMAccountName))                
                        return False
    
                    user_dict = users[0]
                    self.login = nt_user_name
                        
                else:
                    filter = "(%s=%s)" % ("objectSid", user_sid)
                    users = ldap_session.query(self.dn, filter, [])
                    if len(users)!=1:
                        self.checkpoint_handler.Checkpoint("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, msg="Found %s users matching SID '%s'" % (len(users), user_sid))                
                        return False
        
                    user_dict = users[0]
                    self.login = user_sid
                    
                    self.authenticated = True
    
                if user_dict:
                    self.user_dn = ldap_session.get_query_value(user_dict, "dn")
                    if self.user_id_property:
                        self.user_id = ldap_session.get_query_value(user_dict, self.user_id_property)
                    else:
                        self.user_id = self.user_dn
                    
                    self.group_dn_list = ldap_session.get_query_values(user_dict, self.props.get("user_group_property"), decode=True)  

                    primary_group_dn = self._get_primary_group_dn(ldap_session, user_dict)
                    if primary_group_dn:
                        self.group_dn_list.append(primary_group_dn)
                    else:
                        self.primary_group_rid = ldap_session.get_query_value(user_dict, "primaryGroupID")
    
                    self.authenticated = True
                    return True
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                
        
        return False
