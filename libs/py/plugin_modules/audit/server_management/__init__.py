"""
Audit plugin
"""
from __future__ import with_statement
import sys
import os.path
import datetime

import lib.checkpoint

from plugin_types.server_management import plugin_type_audit
from components.database.server_common import database_api
import components.database.server_common.connection_factory

class Trail(database_api.PersistentObject):   
    
    @classmethod
    def map_to_table(cls, table):
        database_api.mapper(cls, table)

class PluginAudit(plugin_type_audit.PluginTypeAudit):
    
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_audit.PluginTypeAudit.__init__(self, checkpoint_handler, database, license_handler, u"audit_sqlite")
        self.version = "unknown"
        self.db_created = False
        self.dbapi = self.create_tables()


    def save_entry(self, **value_dict):
        if not self.db_created:
            self.create_db()
        if self.db_created:
            with database_api.Transaction() as dbt:
                trail = dbt.add(Trail())
                content_list = ["%s: '%s'" % item for item in value_dict.items()]
                trail.content = "\n".join(content_list)
                trail.timestamp = datetime.datetime.now()
        
    def create_tables(self):
        dbapi = database_api.SchemaFactory.get_creator("audit")
        trail_table = dbapi.create_table("trail",
                                           database_api.Column('content', database_api.Text),
                                           database_api.Column('timestamp', database_api.DateTime, default=datetime.datetime.now()),
                                           )
        
        Trail.map_to_table(trail_table)
        return dbapi
        
    def create_db(self):
        folder = os.path.abspath(os.path.dirname(__file__))
        file_folder = os.path.join(folder, "audit_%s.sqlite" % self.version)
        connect_info = database_api.DB_ConnectInfo()
        connect_info.database=file_folder
        connect_info.db_type = "sqlite"
        db_url = database_api.get_connect_url(connect_info)
        connection_factory = components.database.server_common.connection_factory.ConnectionFactory(db_url)
        self.dbapi.bind(connection_factory.get_default_connection())
        self.db_created = True
        

    def set_version_string(self, version_string):
        self.version = version_string