"""
Endpoint MAC plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import lib.cryptfacility

from plugin_types.server_gateway import plugin_type_auth
from plugin_types.server_gateway import plugin_type_endpoint

from plugin_modules.endpoint_mac.server_common import mac_address_attribute_name, mac_address_blacklist


class PluginEndpointMac(plugin_type_auth.PluginTypeAuth, 
                        plugin_type_endpoint.PluginTypeEndpoint):
    """
    Runtime server part of Endpoint authentication plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'endpoint_mac', license_handler, session_info, database, management_message_session, access_log_server_session)
        plugin_type_endpoint.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, u'endpoint_mac', license_handler, session_info)
        self.mac_addresses = set()
        self.all_done = False

    def reset(self):
        plugin_type_auth.PluginTypeAuth.reset(self)
        plugin_type_endpoint.PluginTypeEndpoint.reset(self)
        
    def start(self):
        """
        Initiates the process
        """
        self.set_started()
        if self.all_done:
            self.set_ready()

    def check(self, endpoint_id):
        if self.mac_addresses:
            registered_addresses = self.endpoint_session_plugin_cb.get_registered_endpoint_info(endpoint_id, mac_address_attribute_name)
            for address in self.mac_addresses:
                if address in registered_addresses: 
                    return True
        return False

    def endpoint_info_report_some(self, endpoint_info_plugin_name, endpoint_info_attributes):
        """
        Inform the plugin that some endpoint information is available from the given plugin
        """
        for (name, value) in endpoint_info_attributes:
            if name == mac_address_attribute_name:
                blacklisted = False
                for blacklist_value in mac_address_blacklist:
                    if value.startswith(blacklist_value):
                        blacklisted = True
                        break
                if not blacklisted:
                    self.mac_addresses.add(value)
        
    def endpoint_info_all_done(self):
        """
        Inform the plugin that all endpoint information has been collected
        """
        self.all_done = True
        if self.is_started():
            self.set_ready()

        