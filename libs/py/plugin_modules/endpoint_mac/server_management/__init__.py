"""
Endpoint MAC plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import lib.cryptfacility

from plugin_types.server_management import plugin_type_endpoint

from plugin_modules.endpoint_mac.server_common import mac_address_attribute_name, mac_address_blacklist

element_defs = {
                 u"EndpointMAC" : dict( type_=u"module", plugin=u"endpoint_mac",  
                                        internal_element_type = u"endpoint_id", title=u"MAC address check"),                
               }


class PluginEndpointMac(plugin_type_endpoint.PluginTypeEndpoint):
    """
    Runtime server part of Endpoint authentication plugin module.
    """
    def __init__(self, checkpoint_handler, license_handler, database):
        plugin_type_endpoint.PluginTypeEndpoint.__init__(self, checkpoint_handler, license_handler, database, u'endpoint_mac')

    

    def supports_endpoint_authentication(self):
        return True
        
        
    def get_element_definitions(self):
        return element_defs  
        
        
    def get_attribute_filter(self):
        return { mac_address_attribute_name : mac_address_blacklist }  

        
        
        