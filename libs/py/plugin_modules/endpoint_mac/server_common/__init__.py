
mac_address_attribute_name = 'net_mac'

mac_address_blacklist = [
                          "00:50:56", # WMware adapter
                          "50:50:54:50:30:30", # WAN Miniport (PPTP)
                          "33:50:6f:45:30:30", # WAN Miniport (PPPOE)
                          "02:00:54:55:4e:01", # Microsoft Tun Miniport Adapter
                          "20:41:53:59:4e:ff", # RAS Async Adapter
                        ]