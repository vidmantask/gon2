"""
Access Log plugin module for the Management Server 

This following datasets are available from this plugin module:
+ '1', All access log entries
+ '2', Current online access log entries
+ '11', All traffic entries
+ '12', All traffic entries related to current log entries 


"""
from __future__ import with_statement

import os.path
import ConfigParser

import linecache
import datetime
from operator import itemgetter

import lib.checkpoint

from plugin_modules import access_log
from plugin_types.server_management import plugin_type_report

import components.access_log.server_common.database_schema as database_schema_al
import components.user.server_common.database_schema as database_schema_user
import components.admin_ws.database_schema as database_schema_man


from components.database.server_common import schema_api
from components.database.server_common import database_api

from sqlalchemy.sql import func

def encode_ts(ts):
    if ts is None:
        return None
    return ts.isoformat()

def encode_bool(flag):
    if flag:
        return 'true'
    return 'false'


class AccessLogReportConfig(plugin_type_report.ReportConfig):
    def __init__(self):
        plugin_type_report.ReportConfig.__init__(self, u'al', os.path.dirname(os.path.abspath(__file__)))
        self.usage_limit_days = 90
        self.session_errors_limit_days = 3
        self.load()
    
    def load(self):
        if self.config.has_section('restrictions'):
            if self.config.has_option('restrictions', 'usage_limit_days'):
                self.usage_limit_days = self.config.getint('restrictions', 'usage_limit_days')
            if self.config.has_option('restrictions', 'session_errors_limit_days'):
                self.session_errors_limit_days = self.config.getint('restrictions', 'session_errors_limit_days')


def getSessionsForUsers(checkpoint_handler):
    
    with checkpoint_handler.CheckpointScope("getSessionsForUsers", "access_log", lib.checkpoint.DEBUG) as cps:            

        with database_api.QuerySession() as dbs:
            session_select = dbs.select(database_schema_al.Session, 
                                 filter=database_api.and_(database_schema_al.Session.external_user_id <> None, database_schema_al.Session.user_plugin <> None),
                                 order_by=database_api.desc(database_schema_al.Session.start_ts)
                                 )
            
            user_dict = dict()
            for session in session_select:
                session_found = user_dict.get((session.user_plugin, session.external_user_id))
                if not session_found:
                    user_dict[(session.user_plugin, session.external_user_id)] = session

        checkpoint_handler.Checkpoint("__init__", "getSessionsForUsers", lib.checkpoint.DEBUG, msg="found %d last session user records" % len(user_dict))
        return user_dict.values()
        
def getSessionsForUsers1(checkpoint_handler):
    
    with checkpoint_handler.CheckpointScope("getSessionsForUsers", "access_log", lib.checkpoint.DEBUG) as cps:            

        with database_api.QuerySession() as dbs:

            session_select = dbs.select_expression([func.max(database_schema_al.Session.start_ts).label("start_ts"), 
#            session_records = dbs.select([database_api.max_(database_schema_al.Session.start_ts), 
                                  database_schema_al.Session.external_user_id,
                                  database_schema_al.Session.user_plugin,
                                  ],
                                  from_= database_schema_al.Session,
                                 filter=database_api.and_(database_schema_al.Session.external_user_id <> None, database_schema_al.Session.user_plugin <> None),
                                 group_by=(database_schema_al.Session.external_user_id, database_schema_al.Session.user_plugin))
            
            max_sessions = session_select.alias("max_sessions")
            session_records = dbs.select([max_sessions.c.external_user_id,
                                           max_sessions.c.user_plugin,
                                           max_sessions.c.start_ts,
                                  database_schema_al.Session.unique_session_id,
                                  database_schema_al.Session.server_id,
                                  database_schema_al.Session.server_sid,
                                  database_schema_al.Session.close_ts,
                                  database_schema_al.Session.client_ip,
                                  database_schema_al.Session.login,                                          
                                  database_schema_al.Session.user_login,
                                  database_schema_al.Session.user_name,
                                ], 
                                filter=database_api.and_(max_sessions.c.external_user_id==database_schema_al.Session.external_user_id,
                                                         max_sessions.c.user_plugin==database_schema_al.Session.user_plugin,
                                                         max_sessions.c.start_ts==database_schema_al.Session.start_ts)
                                )

        checkpoint_handler.Checkpoint("__init__", "getSessionsForUsers", lib.checkpoint.DEBUG, msg="found %d last session user records" % len(session_records))
        return session_records


class PluginAccessLogReport(plugin_type_report.PluginTypeReport):
    def __init__(self, checkpoint_handler, license_handler, database):
        plugin_type_report.PluginTypeReport.__init__(self, checkpoint_handler, license_handler, database, u'al')
        self.report_config = AccessLogReportConfig()
        self.report_config.load()
    
    def get_reports(self):
        return self.report_config.get_reports()
   
    def get_report_specification(self, report_id):
        report = plugin_type_report.Report.find_by_id(self.report_config.get_reports(), report_id)
        if report is not None:
            return report.get_specification(self.checkpoint_handler)
        self.checkpoint_handler.Checkpoint("get_report_spec.not_found", self.plugin_name, lib.checkpoint.ERROR, report_id=report_id)
        return ''

    def find_report_specification_by_specification_filename(self, report_specification_filename):
        report_specification_filename_abs = os.path.join(self.report_config.config_folder, report_specification_filename)
        if os.path.exists(report_specification_filename_abs):
            report = plugin_type_report.Report(self.plugin_name, 'temp', 'Temp', report_specification_filename_abs)
            return report.get_specification(self.checkpoint_handler)
        return None
    
    def get_report_data(self, data_id, args):
        with self.checkpoint_handler.CheckpointScope("get_report_data", self.plugin_name, lib.checkpoint.DEBUG, data_id=data_id, args=args) as cps:
#            if data_id == u'1':
#                return self._get_report_data_access_log(online_only=False)
#            elif data_id == u'2':
#                return self._get_report_data_access_log(online_only=True)
#            elif data_id == u'11':
#                return self._get_report_data_traffic_proxy()
#            elif data_id == u'12':
#                return self._get_report_data_traffic_proxy_only_online()
#            elif data_id == u'20':
#                return self._get_report_data_activity()
#            elif data_id == u'21':
#                return self._get_report_data_activity_online()
            if data_id == u'30':
                return self._get_report_data_usage(args)
            elif data_id == u'31':
                return self._get_report_data_usage_new(args)
            elif data_id == u'40':
                return self._get_report_data_access_log_session_errors()
#            elif data_id == u'50':
#                return self._get_report_data_authentication()
            elif data_id == u'100':
                return self._get_report_data_100_sessions_toc(args)
            elif data_id == u'101':
                return self._get_report_data_101_sessions(args)
            elif data_id == u'110':
                return self._get_report_data_110_session_traffic_proxy(args)
            elif data_id == u'111':
                return self._get_report_data_111_session_auth_results(args)
            elif data_id == u'112':
                return self._get_report_data_112_session_notification(args)
            elif data_id == u'113':
                return self._get_report_data_113_session(args)
            elif data_id == u'210':
                return self._get_report_data_210_activity_log(args) 
            elif data_id == u'220':
                return self._get_report_data_220_session_menu_items(args) 
            elif data_id == u'221':
                return self._get_report_data_221_menu_item_traffic_proxy(args) 
            elif data_id == u'222':
                return self._get_report_data_222_menu_item_http_connections(args) 
            elif data_id == u'223':
                return self._get_report_data_223_menu_item_http_denials(args) 
            elif data_id == u'224':
                return self._get_report_data_224_menu_item_tcp_connections(args) 
            return []

    def _session_has_user(self, session):
        if session.external_user_id and session.user_plugin:
            return True
        return False
    
    def _lookup_user(self, session):
        with database_api.QuerySession() as dbs:
            return dbs.select_first(database_schema_user.UserInfo, database_api.and_(database_schema_user.UserInfo.user_plugin==session.user_plugin,
                                                                                     database_schema_user.UserInfo.external_user_id==session.external_user_id))
            
    def _get_user_internal_id(self, session):        
        return "%s.%s" % (session.user_plugin, session.external_user_id)
                
    def _get_report_data_activity(self):
        data = []
        with database_api.QuerySession() as dbs:
            users = dbs.select(database_schema_user.UserInfo)
            session_fetcher = SessionsForUsers(self.checkpoint_handler, users)
            for user in users:
                session = session_fetcher.get_last_session(user)
#                session = dbs.select_first(database_schema_al.Session, database_api.and_(database_schema_al.Session.user_plugin==user.user_plugin, database_schema_al.Session.external_user_id==user.external_user_id), order_by=database_api.desc(database_schema_al.Session.start_ts))
                if session is not None:
                    data.append(self._get_report_data_activity_generate_row('session', session, user))
                    traffic_proxies = dbs.select(database_schema_al.TrafficProxy, database_schema_al.TrafficProxy.unique_session_id==session.unique_session_id)
                    for traffic_proxy in traffic_proxies:
                        data.append(self._get_report_data_activity_generate_row('session_proxy', session, user, traffic_proxy))
        return data

    def _get_report_data_activity_online(self):
        data = []
        with database_api.QuerySession() as dbs:
            sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.close_ts == None)
            user_fetcher = UsersInSessions(self.checkpoint_handler, sessions)
            for session in sessions:
                user = user_fetcher.get_user(session)
                data.append(self._get_report_data_activity_generate_row('session', session, user))
                traffic_proxies = dbs.select(database_schema_al.TrafficProxy, database_api.and_(database_schema_al.TrafficProxy.unique_session_id==session.unique_session_id, database_schema_al.TrafficProxy.close_ts==None))
                for traffic_proxy in traffic_proxies:
                    data.append(self._get_report_data_activity_generate_row('session_proxy', session, user, traffic_proxy))
        return data
        
        
    def _get_report_data_activity_generate_row(self, row_type, session, user=None, traffic_proxy=None, traffic_proxy_skip=False):
        row = []
        row.append(session.unique_session_id)
        row.append(row_type)
        
        user_external_id = None
        if user is not None:
            user_plugin_id = user.user_plugin
            user_login = unicode(user.user_login)
            user_external_id = unicode(user.external_user_id)
            user_name = unicode(user.user_name)
            row.append(user_plugin_id)
            row.append(user_external_id)
            row.append(user_login)
            row.append(user_name)
            row.append("")
        else:
            row.append("")
            row.append("")
            row.append("")
            row.append("")
            session_login = session.login
            if session_login is None:
                session_login = ""
            row.append(session_login.lower())

        session_start_ts = session.start_ts
        session_close_ts = session.close_ts
        if session_close_ts is None:
            session_close_ts = datetime.datetime.now()
        session_session_duration_sec = "%d" % (session_close_ts - session_start_ts).total_seconds()
        row.append(session_start_ts.isoformat())
        row.append(session_session_duration_sec)

        session_online = session.close_ts is None
        row.append(str(session_online))

        if not traffic_proxy_skip:
            if traffic_proxy is not None:
                row.append(traffic_proxy.proxy_id)
                row.append(traffic_proxy.proxy_id_sub)
                row.append(traffic_proxy.client_ip)
                row.append(traffic_proxy.client_port)
                row.append(traffic_proxy.server_ip)
                row.append(traffic_proxy.server_port)
    
                proxy_start_ts = traffic_proxy.start_ts
                proxy_close_ts = traffic_proxy.close_ts
                if proxy_close_ts is None:
                    proxy_close_ts = datetime.datetime.now()
                proxy_duration_sec = "%d" % (proxy_close_ts - proxy_start_ts).total_seconds()
                row.append(proxy_start_ts.isoformat())
                row.append(proxy_duration_sec)
    
                proxy_online = session_online and traffic_proxy.close_ts is None
                row.append(str(proxy_online))
            else:
                row.append("")
                row.append("")
                row.append("")
                row.append("")
                row.append("")
                row.append("")
                row.append("")
                row.append("")
                row.append("")
        return row
    
    def _get_restriction_from_args(self, args, arg_id, default_value):
        if args.has_key(arg_id):
            return args[arg_id]
        return default_value 
    
    def _get_restriction_from_args_date(self, args, arg_id, default_value):
        try:
            if args.has_key(arg_id):
                arg = args[arg_id]
                arg_year = int(arg[:4])
                arg_month = int(arg[5:7])
                arg_day = int(arg[8:])
                arg_as_data = datetime.date(arg_year, arg_month, arg_day)
                
                return arg_as_data
        except:
            pass
        return default_value

    def _get_report_data_usage(self, args):
        restriction_login = self._get_restriction_from_args(args, 'arg_string_01', None)
        restriction_days = self._get_restriction_from_args(args, 'arg_int_01', None)
        
        if restriction_days is None:
            start_date = datetime.date.today() - datetime.timedelta(days=90)
            #start_date = datetime.date.today() - datetime.timedelta(days=self.report_config.usage_limit_days)
        else:
            start_date = datetime.date.today() - datetime.timedelta(days=restriction_days)
            
        
        data = []
        with database_api.QuerySession() as dbs:
            if restriction_login is None:
                sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.start_ts >= start_date, order_by=database_schema_al.Session.login)
            else:
                sessions = dbs.select(database_schema_al.Session, database_api.and_(database_schema_al.Session.start_ts >= start_date, database_schema_al.Session.login==restriction_login), order_by=database_schema_al.Session.login)

            
            
            for session in sessions:
                if self._session_has_user(session):
                    row = []
                    session_start_ts = session.start_ts
                    session_close_ts = session.close_ts
                    if session_close_ts is None:
                        session_close_ts = datetime.datetime.now()
                    session_session_duration_sec = "%d" % (session_close_ts - session_start_ts).total_seconds()
                    row.append(session.unique_session_id)
                    row.append(session_start_ts.isoformat())
                    row.append(session_session_duration_sec)
                    
                    row.append(session.user_plugin)
                    row.append(unicode(session.external_user_id))
                    row.append(unicode(session.user_login))
                    row.append(unicode(session.user_name))
                    data.append(row)
        return data


    def _get_report_data_usage_new(self, args):
        restriction_login = self._get_restriction_from_args(args, 'arg_string_01', None)
        restriction_days = self._get_restriction_from_args(args, 'arg_int_01', None)
        
        if restriction_days is None:
            start_date = datetime.date.today() - datetime.timedelta(days=90)
            #start_date = datetime.date.today() - datetime.timedelta(days=self.report_config.usage_limit_days)
        else:
            if restriction_days==100000:
                start_date = None
            else:
                start_date = datetime.date.today() - datetime.timedelta(days=restriction_days)
            
        
        data = []
        with database_api.QuerySession() as dbs:
            if restriction_login is None:
                if start_date is None:
                    filter = None
                else:
                    filter = database_schema_al.Session.start_ts >= start_date
            else:
                if start_date is None:
                    filter = database_schema_al.Session.login==restriction_login
                else:
                    filter = database_api.and_(database_schema_al.Session.start_ts >= start_date, database_schema_al.Session.login==restriction_login)
                
            default_filter = database_api.and_(database_schema_al.Session.external_user_id <> None, database_schema_al.Session.user_plugin <> None)
            if filter is None:
                filter = default_filter  
            else:
                filter = database_api.and_(default_filter, filter)
            
            sessions = dbs.select(database_schema_al.Session, filter,  order_by=database_api.desc(database_schema_al.Session.start_ts))
            user_dict = dict()
            for session in sessions:
                session_start_ts = session.start_ts
                session_close_ts = session.close_ts if session.close_ts else datetime.datetime.now()
                session_session_duration_sec = (session_close_ts - session_start_ts).total_seconds()
                row = user_dict.get((session.user_plugin, session.external_user_id))
                if not row:
                    row = dict()
                    row["unique_session_id"] = session.unique_session_id
                    row["session_duration_sec"] = session_session_duration_sec
                    row["session_count"] = 1
                    
                    row["user_plugin"] = session.user_plugin
                    row["external_user_id"] = unicode(session.external_user_id)
                    row["user_login"] = unicode(session.user_login)
                    row["user_name"] = unicode(session.user_name)
                    row["sort_key"] = session.user_name.lower()if session.user_name else ""
                else:
                    row["session_duration_sec"] = row.get("session_duration_sec") + session_session_duration_sec
                    row["session_count"] = row.get("session_count") + 1

                user_dict[(session.user_plugin, session.external_user_id)] = row
                
            sorted_user_dict = sorted(user_dict.values(), key=itemgetter("sort_key"))
                
            for session in sorted_user_dict:
                row = []
                row.append(session.get("unique_session_id"))
                row.append("%d" % session.get("session_count"))
                row.append("%d" % session.get("session_duration_sec"))
                
                row.append(session.get("user_plugin"))
                row.append(session.get("external_user_id"))
                row.append(session.get("user_login"))
                row.append(session.get("user_name"))
                data.append(row)
                
#                print session.get("user_login"), session.get("user_name")
        return data


    def _get_report_data_access_log(self, online_only=False):
        data = []
        with database_api.QuerySession() as dbs:
            if online_only:
                sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.close_ts == None)
            else:
                sessions = dbs.select(database_schema_al.Session)
            
            for session in sessions:
                row = []
                row.append(unicode(session.unique_session_id))
                row.append(unicode(session.start_ts.isoformat(' ')))
                if session.close_ts != None :
                    row.append(unicode(session.close_ts.isoformat(' ')))
                else:
                    row.append(unicode('None'))
                
                if session.client_ip != None :
                    row.append(unicode(session.client_ip))
                else:
                    row.append(unicode('None'))
                
                if session.user_email != None:
                    row.append(session.user_email)
                else:
                    row.append(unicode('None'))

                if session.user_sid != None:
                    row.append(session.user_sid)
                else:
                    row.append(unicode('None'))

                data.append(row)
        return data


    def _get_report_data_traffic_proxy(self):
        data = []
        with database_api.QuerySession() as dbs:
            traffic_proxies = dbs.select(database_schema_al.TrafficProxy)
            self._append_traffic_proxy(data, traffic_proxies)
        return data

    def _get_report_data_traffic_proxy_only_online(self):
        data = []
        with database_api.QuerySession() as dbs:
            sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.close_ts == None)
            for session in sessions:
                traffic_proxies = dbs.select(database_schema_al.TrafficProxy, database_schema_al.TrafficProxy.unique_session_id==session.unique_session_id)
                self._append_traffic_proxy(data, traffic_proxies)
        return data


    def _append_traffic_proxy(self, data, traffic_proxies):
            for traffic_proxy in traffic_proxies:
                row = []
                row.append(unicode(traffic_proxy.unique_session_id))
                row.append(unicode(traffic_proxy.proxy_id))
                row.append(unicode(traffic_proxy.start_ts.isoformat(' ')))
                if traffic_proxy.close_ts != None :
                    row.append(unicode(traffic_proxy.close_ts.isoformat(' ')))
                else:
                    row.append(unicode('None'))
                row.append(unicode(traffic_proxy.server_ip))
                row.append(unicode(traffic_proxy.server_port))
                data.append(row)


    def _get_report_data_access_log_session_errors(self):
        start_date = datetime.date.today() - datetime.timedelta(days=self.report_config.session_errors_limit_days)

        data = []
        with database_api.QuerySession() as dbs:
            errors = dbs.select(database_schema_al.Notification, database_schema_al.Notification.timestamp >= start_date, order_by=database_schema_al.Notification.timestamp)
            
            session_ids_exp = dbs.select_expression(database_schema_al.Notification.unique_session_id, database_schema_al.Notification.timestamp >= start_date, order_by=database_schema_al.Notification.timestamp)
            session_records = dbs.select(database_schema_al.Session,database_api.in_(database_schema_al.Session.unique_session_id, session_ids_exp))
            session_dict = dict([(s.unique_session_id, s) for s in session_records])
                        
            for error in errors:
                row = []
                row.append(error.timestamp.isoformat(' '))
                row.append(error.type)
                row.append(error.source)
                row.append(error.code)
                row.append(error.details)
                row.append(error.severity)
                row.append(error.summary)
                
                session = session_dict.get(error.unique_session_id)
                if session is not None:
                    row.append(session.unique_session_id)
                    if self._session_has_user(session):
                        row.append(session.user_plugin)
                        row.append(unicode(session.external_user_id))
                        row.append(unicode(session.user_login))
                        row.append(unicode(session.user_name))
                    else:
                        row.append("")
                        row.append("")
                        row.append(session.login)
                        row.append("")
                else:
                    row.append("")
                    row.append("")
                    row.append("")
                    row.append("")
                    row.append("")
                data.append(row)
            return data

    def _get_report_data_authentication(self):
        data = []
        with database_api.QuerySession() as dbs:
            users = dbs.select(database_schema_user.UserInfo)
            session_fetcher = SessionsForUsers(self.checkpoint_handler, users)
            for user in users:
                session = session_fetcher.get_last_session(user)
#                session = dbs.select_first(database_schema_al.Session, database_api.and_(database_schema_al.Session.user_plugin==user.user_plugin, database_schema_al.Session.external_user_id==user.external_user_id), order_by=database_api.desc(database_schema_al.Session.start_ts))
                if session is not None:
                    auth_results = dbs.select(database_schema_al.AuthResult, database_schema_al.AuthResult.unique_session_id==session.unique_session_id, 
                                              order_by=[database_schema_al.AuthResult.result_type, database_schema_al.AuthResult.criteria_type])
                    if len(auth_results) > 0:
                        for auth_result in auth_results:
                            row = self._get_report_data_activity_generate_row('session', session, user, traffic_proxy_skip=True)
                            row.append(auth_result.result_type)
                            row.append(unicode(auth_result.criteria_type))
                            row.append(unicode(auth_result.criteria_text))
                            data.append(row)
                    else:
                        row = self._get_report_data_activity_generate_row('session', session, user, traffic_proxy_skip=True)
                        row.append("")
                        row.append("")
                        row.append("")
                        data.append(row)
            return data


    

    SESSIONS_QUERY_TYPE_CURRENT = 0 
    SESSIONS_QUERY_TYPE_LATEST = 1
    SESSIONS_QUERY_TYPE_INTERVAL = 2

    SESSIONS_TOC_TYPE_VALUE = 0
    SESSIONS_TOC_TYPE_ALL = 1
    SESSIONS_TOC_TYPE_OTHER = 2
         
    def _get_report_data_100_sessions_toc(self, args):
        """
        Return rows for toc of sessions with the following columns:
            (toc_parmvalue, toc_parmvalue_type, toc_title, toc_count, toc_selected)
    
        """
        def add_row(data, toc_parmvalue, toc_title, toc_count, toc_parmvalue_type=PluginAccessLogReport.SESSIONS_TOC_TYPE_VALUE, toc_selected=False):
            row = []
            row.append(toc_parmvalue)
            row.append(toc_parmvalue_type)
            row.append(toc_title)
            row.append(str(toc_count))
            row.append(toc_selected)
            data.append(row)

        def get_user_toc_key(user):
            if user.user_name:
                return user.user_name[0].lower()
            return ""
        
        def add_user_to_toc(toc, user):
            user_toc_key = get_user_toc_key(user)
            if not toc.has_key(user_toc_key):
                toc[user_toc_key] = 0
            toc[user_toc_key] += 1
            
        parm_sessions_query_type = self._get_restriction_from_args(args, 'arg_int_01', None)
        parm_toc_parmvalue       = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_toc_parmvalue_type  = self._get_restriction_from_args(args, 'arg_int_02', PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL)
        parm_interval_from       = self._get_restriction_from_args_date(args, 'arg_string_02', datetime.date.min)
        parm_interval_to         = self._get_restriction_from_args_date(args, 'arg_string_03', datetime.date.max)
        
        if parm_interval_to != datetime.date.max:
            parm_interval_to += datetime.timedelta(days=1)
        
        
        data = []
        if parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_CURRENT:
            with database_api.QuerySession() as dbs:
                toc = {}
                all_count = 0
                other_count = 0
                sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.close_ts == None)
                for session in sessions:
                    all_count += 1
                    if self._session_has_user(session):
                        add_user_to_toc(toc, session)
                    else:
                        other_count += 1

                add_row(data, "all", "All", all_count, PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL)
                for toc_key in sorted(toc):
                    add_row(data, toc_key, toc_key.upper(), toc[toc_key])
                if other_count > 0:
                    add_row(data, "other", "Other", other_count, PluginAccessLogReport.SESSIONS_TOC_TYPE_OTHER)

        elif parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_LATEST:
            with database_api.QuerySession() as dbs:
                toc = {}
                all_count = 0
                count = 0
                sessions = getSessionsForUsers(self.checkpoint_handler)
                for session in sessions:
                    all_count += 1
                    add_user_to_toc(toc, session)
                    count += 1
                    if count % 100 == 0:
                        self.checkpoint_handler.Checkpoint("SESSIONS_QUERY_TYPE_LATEST", self.plugin_name, lib.checkpoint.DEBUG, count=count)
                        
                        
                add_row(data, "all", "All", all_count, PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL)
                for toc_key in sorted(toc):
                    add_row(data, toc_key, toc_key.upper(), toc[toc_key])
                    
        elif parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_INTERVAL:
            with database_api.QuerySession() as dbs:
                toc = {}
                all_count = 0
                other_count = 0
                sessions = dbs.select(database_schema_al.Session, database_api.or_(database_api.and_(database_schema_al.Session.close_ts >= parm_interval_from, database_schema_al.Session.close_ts < parm_interval_to),database_api.and_(database_schema_al.Session.start_ts >= parm_interval_from, database_schema_al.Session.start_ts < parm_interval_to)) )
                for session in sessions:
                    all_count += 1
                    if self._session_has_user(session):
                        add_user_to_toc(toc, session)
                    else:
                        other_count += 1

                add_row(data, "all", "All", all_count, PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL)
                for toc_key in sorted(toc):
                    add_row(data, toc_key, toc_key.upper(), toc[toc_key])
                if other_count > 0:
                    add_row(data, "other", "Other", other_count, PluginAccessLogReport.SESSIONS_TOC_TYPE_OTHER)

        self.checkpoint_handler.Checkpoint("_get_report_data_100_sessions_toc", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
        return data

    def _get_report_data_101_sessions(self, args):
        """
        Return rows for sessions with the following columns:
            (user_internal_id, user_name, session_start_ts, session_close_ts, session_unique_session_id)
        """
        parm_sessions_query_type = self._get_restriction_from_args(args, 'arg_int_01', None)
        parm_toc_parmvalue       = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_toc_parmvalue_type  = self._get_restriction_from_args(args, 'arg_int_02', PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL)
        parm_interval_from       = self._get_restriction_from_args_date(args, 'arg_string_02', datetime.date.min)
        parm_interval_to         = self._get_restriction_from_args_date(args, 'arg_string_03', datetime.date.max)
        
        if parm_interval_to != datetime.date.max:
            parm_interval_to += datetime.timedelta(days=1)


        def add_row(data, user_internal_id, user_name, user_login, session_start_ts, session_close_ts, session_unique_session_id):
            row = []
            row.append(user_internal_id)
            row.append(user_name)
            row.append(user_login)
            row.append(encode_ts(session_start_ts))
            row.append(encode_ts(session_close_ts))
            row.append(encode_bool(session_close_ts is None))
            row.append(session_unique_session_id)
            data.append(row)

        def add_row_session_with_user(data, session):
            add_row(data, "%s.%s" % (session.user_plugin, session.external_user_id), session.user_name, session.user_login, session.start_ts, session.close_ts, session.unique_session_id)

        def add_row_session(data, session):
            add_row(data, "", "", "", session.start_ts, session.close_ts, session.unique_session_id)

        def get_user_toc_key(session):
            if session.user_name:
                return session.user_name[0].lower()
            return ""

        def handle_session_with_user(data, session):
            if parm_toc_parmvalue_type in [PluginAccessLogReport.SESSIONS_TOC_TYPE_VALUE]:
                if parm_toc_parmvalue == get_user_toc_key(session):
                    add_row_session_with_user(data, session)
            elif parm_toc_parmvalue_type in [PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL]:
                add_row_session_with_user(data, session)
        
        def handle_session(data, session):
            if parm_toc_parmvalue_type in [PluginAccessLogReport.SESSIONS_TOC_TYPE_ALL, PluginAccessLogReport.SESSIONS_TOC_TYPE_OTHER]:
                add_row_session(data, session)
        
        data = []
        if parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_CURRENT:
            with database_api.QuerySession() as dbs:
                sessions = dbs.select(database_schema_al.Session, database_schema_al.Session.close_ts == None)
                for session in sessions:
                    if self._session_has_user(session):
                        handle_session_with_user(data, session)
                    else:
                        handle_session(data, session)

        elif parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_LATEST:
            with database_api.QuerySession() as dbs:
                sessions = getSessionsForUsers(self.checkpoint_handler)
                for session in sessions:
                    handle_session_with_user(data, session)
        
        elif parm_sessions_query_type == PluginAccessLogReport.SESSIONS_QUERY_TYPE_INTERVAL:
            with database_api.QuerySession() as dbs:
                sessions = dbs.select(database_schema_al.Session, database_api.or_(database_api.or_(database_api.and_(database_schema_al.Session.close_ts >= parm_interval_from, 
                                                                                                                      database_schema_al.Session.close_ts < parm_interval_to), 
                                                                                                    database_api.and_(database_schema_al.Session.start_ts >= parm_interval_from, 
                                                                                                                      database_schema_al.Session.start_ts < parm_interval_to)) , 
                                                                                                     database_api.and_(database_schema_al.Session.start_ts < parm_interval_from, 
                                                                                                                       database_schema_al.Session.close_ts >= parm_interval_to)))
                for session in sessions:
                    if self._session_has_user(session):
                        handle_session_with_user(data, session)
                    else:
                        handle_session(data, session)
        self.checkpoint_handler.Checkpoint("_get_report_data_101_sessions", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
        return data


    def _get_report_data_110_session_traffic_proxy(self, args):
        """
        Return traffic_proxy rows for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
    
        def add_row(data, traffic_proxy):
            row = []
            row.append(traffic_proxy.unique_session_id)
            row.append(traffic_proxy.proxy_id)
            row.append(traffic_proxy.proxy_id_sub)
            row.append(encode_ts(traffic_proxy.start_ts))
            row.append(encode_ts(traffic_proxy.close_ts))
            row.append(encode_bool(traffic_proxy.close_ts is None))
            row.append(traffic_proxy.client_ip)
            row.append(traffic_proxy.client_port)
            row.append(traffic_proxy.server_ip)
            row.append(traffic_proxy.server_port)
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            traffic_proxies = dbs.select(database_schema_al.TrafficProxy, database_schema_al.TrafficProxy.unique_session_id==parm_unique_session_id)
            for traffic_proxy in traffic_proxies:
                add_row(data, traffic_proxy)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_110_session_traffic_proxy", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data
    

    
    def _get_report_data_111_session_auth_results(self, args):
        """
        Return auth_results rows for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        
        def add_row(data, auth_result):
            row = []
            row.append(auth_result.unique_session_id)
            row.append(auth_result.result_type)
            row.append(auth_result.criteria_type)
            row.append(auth_result.criteria_text)
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            auth_results = dbs.select(database_schema_al.AuthResult, database_schema_al.AuthResult.unique_session_id==parm_unique_session_id, 
                                              order_by=[database_schema_al.AuthResult.criteria_type, database_schema_al.AuthResult.criteria_text])
            for auth_result in auth_results:
                add_row(data, auth_result)
        return data
        
    def _get_report_data_112_session_notification(self, args):
        """
        Return auth_results rows for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        
        def add_row(data, notification):
            row = []
            row.append(notification.unique_session_id)
            row.append(notification.server_sid)
            row.append(notification.timestamp)
            row.append(notification.type)
            row.append(notification.severity)
            row.append(notification.source)
            row.append(notification.code)
            row.append(notification.summary)
            row.append(notification.details)
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            auth_results = dbs.select(database_schema_al.Notification, database_schema_al.Notification.unique_session_id==parm_unique_session_id)
            for auth_result in auth_results:
                add_row(data, auth_result)
        return data

    def _get_report_data_113_session(self, args):
        """
        Return one session(and optional user) data for for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        
        def add_row(data, session, user):
            row = []
            row.append(session.unique_session_id)
            row.append(session.server_sid)
            row.append(encode_ts(session.start_ts))
            row.append(encode_ts(session.close_ts))
            row.append(encode_bool(session.close_ts is None))
            row.append(session.client_ip)
            if user is not None:
                row.append(user.internal_id)
                row.append(user.external_user_id)
                row.append(user.user_plugin)
                row.append(user.user_login)
                row.append(user.internal_user_login)
                row.append(user.user_name)
                row.append(user.email)
            else:
                row.append(self._get_user_internal_id(session))
                row.append(session.external_user_id)
                row.append(session.user_plugin)
                row.append(session.user_login)
                row.append("")
                row.append(session.user_name)
                row.append("")
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            session = dbs.select_first(database_schema_al.Session, database_schema_al.Session.unique_session_id==parm_unique_session_id)
            if session is not None:
                user = self._lookup_user(session)
                add_row(data, session, user)
        return data
        
    
    def _get_report_data_210_activity_log(self, args):
        
        query_type = self._get_restriction_from_args(args, 'arg_int_01', 1)
        
        
        def add_row(data, activity_log):
            row = []
            row.append(activity_log.user_login)
            row.append(activity_log.user_id)
            row.append(activity_log.user_plugin)
            row.append(activity_log.internal_user_login)
            row.append(activity_log.access_right)
            row.append(activity_log.session_id)
            row.append(activity_log.timestamp)
            row.append(activity_log.type)
            row.append(activity_log.severity)
            row.append(activity_log.summary)
            row.append(activity_log.details)
            data.append(row)
            
            
        if query_type == 0:
            
            restriction_days = self._get_restriction_from_args(args, 'arg_int_02', None)
        
            """ Work around: BIRT makes an error when passing 1 as a parameter between reports. 
            Therefore 0 is passed as an substitute for 1 """
            if restriction_days == 0:
                restriction_days = 1
              
              
            if restriction_days is None:
                chosen_date = datetime.date.today() - datetime.timedelta(days=90)
            else:
                if restriction_days==100000:
                    chosen_date = None
                else:
                    chosen_date = datetime.date.today() - datetime.timedelta(days=restriction_days)
 
            data = []
            with database_api.QuerySession() as dbs:
                if chosen_date is None:
                    filter = None
                else:
                    filter = database_schema_man.ActivityLog.timestamp >= chosen_date
                
                activity_logs = dbs.select(database_schema_man.ActivityLog, filter=filter, order_by=database_api.desc(database_schema_man.ActivityLog.timestamp))
                for activity_log in activity_logs:
                    add_row(data, activity_log)

        else:
            
            parm_interval_from       = self._get_restriction_from_args_date(args, 'arg_string_01', datetime.date.min)
            parm_interval_to         = self._get_restriction_from_args_date(args, 'arg_string_02', datetime.date.max)
        
            if parm_interval_to != datetime.date.max:
                parm_interval_to += datetime.timedelta(days=1)
        
            """ database_api.and_( """
            data = []
            with database_api.QuerySession() as dbs:
                activity_logs = dbs.select(database_schema_man.ActivityLog, database_api.and_(database_schema_man.ActivityLog.timestamp >= parm_interval_from, database_schema_man.ActivityLog.timestamp < parm_interval_to), order_by=database_schema_man.ActivityLog.timestamp)
                for activity_log in activity_logs:
                    add_row(data, activity_log)
        return data
    
    
    def _get_report_data_220_session_menu_items(self, args):
        """
        Return traffic_proxy rows for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
    
        def add_row(data, traffic_launch):
            row = []
            row.append(traffic_launch.unique_session_id)
            row.append(traffic_launch.proxy_id)
            row.append(traffic_launch.menu_item_name)
            row.append(traffic_launch.menu_item_title)
            row.append(encode_ts(traffic_launch.start_ts))
            row.append(encode_ts(traffic_launch.close_ts))
            row.append(traffic_launch.launch_info)
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            traffic_launches = dbs.select(database_schema_al.TrafficLaunch, database_schema_al.TrafficLaunch.unique_session_id==parm_unique_session_id)
            for traffic_launch in traffic_launches:
                add_row(data, traffic_launch)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_220_session_menu_items", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data
    
    

    def _get_report_data_221_menu_item_traffic_proxy(self, args):
        """
        Return traffic_proxy rows for a given unique_session_id
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_proxy_id = self._get_restriction_from_args(args, 'arg_int_01', None)
    
        def add_row(data, traffic_proxy):
            row = []
            row.append(traffic_proxy.unique_session_id)
            row.append(traffic_proxy.proxy_id)
            row.append(traffic_proxy.proxy_id_sub)
            row.append(encode_ts(traffic_proxy.start_ts))
            row.append(encode_ts(traffic_proxy.close_ts))
            row.append(encode_bool(traffic_proxy.close_ts is None))
            row.append(traffic_proxy.client_ip)
            row.append(traffic_proxy.client_port)
            row.append(traffic_proxy.server_ip)
            row.append(traffic_proxy.server_port)
            data.append(row)

        data = []
        with database_api.QuerySession() as dbs:
            traffic_proxies = dbs.select(database_schema_al.TrafficProxy, database_api.and_(database_schema_al.TrafficProxy.unique_session_id==parm_unique_session_id,
                                                                                            database_schema_al.TrafficProxy.proxy_id == parm_proxy_id))
            for traffic_proxy in traffic_proxies:
                add_row(data, traffic_proxy)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_221_menu_item_traffic_proxy", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data
    

    def _get_report_data_222_menu_item_http_connections(self, args):
        """
        Return http traffic information for a given menu item launch
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_proxy_id = self._get_restriction_from_args(args, 'arg_int_01', None)
    
        def add_row(data, http_proxy):
            row = []
            row.append(http_proxy.unique_session_id)
            row.append(http_proxy.proxy_id)
            row.append(http_proxy.proxy_id_sub)
            row.append(encode_ts(http_proxy.timestamp))
            row.append(http_proxy.host)
            row.append(http_proxy.url)
            row.append(http_proxy.method)
            row.append(http_proxy.status_code)
            row.append(http_proxy.content_length)
            row.append(encode_bool(http_proxy.cancelled))
            data.append(row)
            

        data = []
        with database_api.QuerySession() as dbs:
            http_proxies = dbs.select(database_schema_al.HttpProxyLog, database_api.and_(database_schema_al.HttpProxyLog.unique_session_id==parm_unique_session_id,
                                                                                            database_schema_al.HttpProxyLog.proxy_id == parm_proxy_id))
            for http_proxy in http_proxies:
                add_row(data, http_proxy)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_222_menu_item_http_connections", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data

    def _get_report_data_223_menu_item_http_denials(self, args):
        """
        Return denied http traffic connections for a given menu item launch
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_proxy_id = self._get_restriction_from_args(args, 'arg_int_01', None)
    
        def add_row(data, http_proxy):
            row = []
            row.append(http_proxy.unique_session_id)
            row.append(http_proxy.proxy_id)
            row.append(http_proxy.proxy_id_sub)
            row.append(encode_ts(http_proxy.first_ts))
            row.append(encode_ts(http_proxy.last_ts))
            row.append(http_proxy.host_or_url)
            row.append(http_proxy.method)
            row.append(http_proxy.error_code)
            row.append(http_proxy.error_message)
            row.append(http_proxy.count)
            data.append(row)
            
        data = []
        with database_api.QuerySession() as dbs:
            http_proxies = dbs.select(database_schema_al.HttpProxyErrorLog, database_api.and_(database_schema_al.HttpProxyErrorLog.unique_session_id==parm_unique_session_id,
                                                                                            database_schema_al.HttpProxyErrorLog.proxy_id == parm_proxy_id))
            for http_proxy in http_proxies:
                add_row(data, http_proxy)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_223_menu_item_http_denials", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data
    
    def _get_report_data_224_menu_item_tcp_connections(self, args):
        """
        Return tcp traffic information rows for a given menu item launch
        """
        parm_unique_session_id = self._get_restriction_from_args(args, 'arg_string_01', None)
        parm_proxy_id = self._get_restriction_from_args(args, 'arg_int_01', None)
    
        def add_row(data, traffic_proxy):
            row = []
            row.append(traffic_proxy.unique_session_id)
            row.append(traffic_proxy.proxy_id)
            row.append(traffic_proxy.proxy_id_sub)
            row.append(encode_ts(traffic_proxy.timestamp))
            row.append(encode_bool(traffic_proxy.failed))
            row.append(traffic_proxy.local_ip)
            row.append(traffic_proxy.local_port)
            row.append(traffic_proxy.remote_ip)
            row.append(traffic_proxy.remote_port)
            data.append(row)


        data = []
        with database_api.QuerySession() as dbs:
            traffic_proxies = dbs.select(database_schema_al.TcpConnectionLog, database_api.and_(database_schema_al.TcpConnectionLog.unique_session_id==parm_unique_session_id,
                                                                                                database_schema_al.TcpConnectionLog.proxy_id == parm_proxy_id))
            for traffic_proxy in traffic_proxies:
                add_row(data, traffic_proxy)
                
            self.checkpoint_handler.Checkpoint("_get_report_data_224_menu_item_tcp_connections", self.plugin_name, lib.checkpoint.DEBUG, data_count=len(data))
            return data
    