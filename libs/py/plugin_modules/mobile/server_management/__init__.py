"""
Mobile plugin for the Management Server 
"""
from __future__ import with_statement

import lib.checkpoint
from components.database.server_common.database_api import *
from plugin_types.server_management import plugin_type_token


class PluginMobile(plugin_type_token.PluginToken):
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_token.PluginToken.__init__(self, checkpoint_handler, database, license_handler, u'mobile')

        
