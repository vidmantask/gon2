"""
Mobile stub plugin for Gateway Client
"""
from __future__ import with_statement

import sys
import os
import lib.checkpoint

from components.communication import message

from plugin_types.client_gateway import plugin_type_auth
import plugin_types.client_gateway.plugin_type_token

import plugin_modules.soft_token.client_gateway_common


class PluginTokenAndAuth(plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'mobile'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)

    msg_receivers = ['get_serial', 'get_challenge']
    
    def get_serial(self, token_id=None):
        self.tunnelendpoint_remote('get_serial_response', serial=None)
                
    def get_challenge(self, challenge):
        self.tunnelendpoint_remote('get_challenge_response', challenge_signature=None)

