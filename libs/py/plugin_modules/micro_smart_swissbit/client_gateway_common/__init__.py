"""
Common functionality for G&D MicroSmart on Gateway Client level
"""
from __future__ import with_statement

import sys
import os
import os.path
import shutil
import tempfile
import traceback

import pickle
import zlib
import uuid

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device
import lib_cpp.swissbit
import lib.config
import lib.utility
import lib.smartcard.pkcs15

import plugin_modules.micro_smart_swissbit

from plugin_types.client_gateway import plugin_type_token

CHUNK_ID_INIT = 0
CHUNK_ID_DEPLOY = 1


class SmartCardTokens(object):
    INIT_FOLDERNAME = os.path.join('gon_client', 'gon_init_micro_smart_swissbit')
    DEPLOY_FOLDERNAME = os.path.join('gon_client', 'gon_deploy_micro_smart_swissbit')
   
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
        self._data_chunks_init = {}
        self._data_chunks_deploy = {}

    def initialize_token(self, device):
        if not os.path.isdir(device):
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        
        init_folder_abs = self._get_init_folder(device)
        if not os.path.exists(init_folder_abs):
            os.mkdir(init_folder_abs)

        deploy_folder_abs = self._get_deploy_folder(device)
        if os.path.exists(deploy_folder_abs):
            lib.utility.rmtree_fail_safe(deploy_folder_abs)

        serial_new = '%s' % uuid.uuid4()
        self.set_serial(device, serial_new)

    def get_tokens(self, plugin_name, additional_device_roots):
        tokens = []
        devices = lib.hardware.device.Device.get_device_list()
        for device in devices:
            init_folder_abs = self._get_init_folder(device)
            if os.path.exists(init_folder_abs):
                client_runtime_env = '%s::%s' % (u'micro_smart_swissbit_client_runtime_env', device) 
                token = plugin_type_token.Token(plugin_name, device, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, client_runtime_env, token_type_title="MicroSmartSwissbit", token_plugin_module_name='micro_smart_swissbit')
                msc_filename = self._get_msc_filename(device)
                is_ok, error_message, token.token_serial = self._get_data_chunk_init(device, msc_filename, use_cache=False)
                if token.token_serial is None:
                    token.token_serial = ''

                is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(device, msc_filename, use_cache=False)
                if enrolled is not None and enrolled:
                    token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED

                tokens.append(token)
        return tokens


    def deploy_token(self, token_id, client_knownsecret, servers):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)
        
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message = self._put_data_chunk_deploy(token_id, msc_filename, client_knownsecret, servers, False)
        if not is_ok:
            raise plugin_type_token.Error(error_message)

    def generate_keypair(self, token_id):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)

        msc_filename = self._get_msc_filename(device)
        is_ok, error_message = lib_cpp.swissbit.key_generate_keypair(msc_filename, 2048)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
   
    def get_public_key(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message, public_key = lib_cpp.swissbit.key_get_public_key(msc_filename)
        if is_ok:
            return lib.smartcard.pkcs15.to_hex(public_key)
        raise plugin_type_token.Error(error_message)


    def get_serial(self, token_id):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message, serial = self._get_data_chunk_init(device, msc_filename)
        if is_ok:
            return serial
        raise plugin_type_token.Error(error_message)


    def set_serial(self, token_id, serial):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message = self._put_data_chunk_init(token_id, msc_filename, serial)
        if not is_ok:
            raise plugin_type_token.Error(error_message)

    def set_enrolled(self, token_id):
        device = token_id
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(device, msc_filename)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
        is_ok, error_message = self._put_data_chunk_deploy(device, msc_filename, client_knownsecret, servers_raw, True)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
        
    def reset_enrolled(self, token_id):
        device = token_id
        msc_filename = self._get_msc_filename(device)
        is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(device, msc_filename)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
        is_ok, error_message = self._put_data_chunk_deploy(device, msc_filename, client_knownsecret, servers_raw, False)
        if not is_ok:
            raise plugin_type_token.Error(error_message)

    def is_enrolled(self, token_id):
        msc_filename = self._get_msc_filename(token_id)
        is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(token_id, msc_filename)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
        return enrolled

    def get_knownsecret_and_servers(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            return (None, None)
        msc_filename = self._get_msc_filename(token_id)
        is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(device, msc_filename)
        if not is_ok:
            raise plugin_type_token.Error(error_message)
        servers = lib.config.parse_servers(self.checkpoint_handler, servers_raw)
        return (client_knownsecret, servers)

    def set_servers(self, token_id, servers):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            return "The token '%s' has not been deployed'"
        msc_filename = self._get_msc_filename(token_id)
        is_ok, error_message, client_knownsecret, servers_raw, enrolled = self._get_data_chunk_deploy(device, msc_filename)
        if not is_ok:
            return error_message
        is_ok, error_message = self._put_data_chunk_deploy(device, msc_filename, client_knownsecret, servers, False)
        if not is_ok:
            return error_message
        return None
    
    def _get_gpms_folder(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gpms_installed')

    def _get_init_folder(self, device):
        return os.path.join(os.path.abspath(device), SmartCardTokens.INIT_FOLDERNAME)
        
    def _get_deploy_folder(self, device):
        return os.path.join(os.path.abspath(device), SmartCardTokens.DEPLOY_FOLDERNAME)

    def _get_msc_filename(self, device):
        return str(device)

    def _put_data_chunk_init(self, token_id, msc_filename, serial):
        chunk = zlib.compress(pickle.dumps( (serial) ), 9)
        is_ok, error_message = lib_cpp.swissbit.key_write_file(msc_filename, CHUNK_ID_INIT, chunk)
        if self._data_chunks_init.has_key(token_id):
            del self._data_chunks_init[token_id]
        return is_ok, error_message
    
    def _put_data_chunk_deploy(self, token_id, msc_filename, client_knownsecret, servers, enrolled):
        chunk = zlib.compress(pickle.dumps( (client_knownsecret, servers, enrolled) ), 9)
        is_ok, error_message = lib_cpp.swissbit.key_write_file(msc_filename, CHUNK_ID_DEPLOY, chunk)
        if self._data_chunks_deploy.has_key(token_id):
            del self._data_chunks_deploy[token_id]
        return is_ok, error_message

    def _get_data_chunk_init(self, token_id, msc_filename, use_cache=True):
        if self._data_chunks_init.has_key(token_id) and use_cache:
            return self._data_chunks_init[token_id]
        else:
            try:
                is_ok, error_message, data = lib_cpp.swissbit.key_read_file(msc_filename, CHUNK_ID_INIT)
                if is_ok:
                    self._data_chunks_init[token_id] = (is_ok, error_message, pickle.loads(zlib.decompress(data)))
                    return self._data_chunks_init[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_init.failed', plugin_modules.micro_smart_swissbit.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None, None, None)
    
    def _get_data_chunk_deploy(self, token_id, msc_filename, use_cache=True):
        if self._data_chunks_deploy.has_key(token_id) and use_cache:
            return self._data_chunks_deploy[token_id]
        else:
            try:
                is_ok, error_message, data = lib_cpp.swissbit.key_read_file(msc_filename, CHUNK_ID_DEPLOY)
                if is_ok:
                    chunk = pickle.loads(zlib.decompress(data))
                    client_knownsecret = None
                    servers_raw = None
                    enrolled = False
                    if len(chunk) == 2:
                        (client_knownsecret, servers_raw) = chunk
                    if len(chunk) == 3:
                        (client_knownsecret, servers_raw, enrolled) = chunk
                    self._data_chunks_deploy[token_id] = (is_ok, error_message, client_knownsecret, servers_raw, enrolled)
                    return self._data_chunks_deploy[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_deploy.failed', plugin_modules.micro_smart_swissbit.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None, None, None, None, None)
