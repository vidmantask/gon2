"""
Unittest of Swissbit MicroSmart plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile
import random

import unittest
from lib import giri_unittest
import lib.checkpoint
from lib import cryptfacility
import lib.hardware.device
import lib_cpp.swissbit

import lib.gpm.gpm_env
import lib.smartcard.common


from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_client_runtime_env
from components.communication.sim import sim_tunnel_endpoint
from components.database.server_common.connection_factory import ConnectionFactory
import components.management_message.server_gateway.session_send


import plugin_modules.micro_smart_swissbit.client_gateway_common
import plugin_modules.micro_smart_swissbit.server_gateway
import plugin_modules.micro_smart_swissbit.client_gateway
from plugin_types.common import plugin_type_token as database_model 




class Deployment(unittest.TestCase):

    def setUp(self):
        deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [])
        self.assertTrue(len(tokens) > 0, "No Swissbit MicroSmart found, unittest aborted")
        self.token_device = tokens[0].token_id
        
        self.token_serial = 'my_smart_card_001'
        self.client_knownsecret = 'my_known_secret'
        self.servers = '127.0.0.1, 8043'
        
    def test_initialization_and_deploy(self):
        deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
 
        deploy_plugin.initialize_token(self.token_device)
        deploy_plugin.set_serial(self.token_device, self.token_serial)
        tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [self.token_device])
         
        self.assertEqual(len(tokens), 1)
        self.assertEqual(tokens[0].token_serial, self.token_serial)
        self.assertEqual(tokens[0].token_status, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED)
 
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
        tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [self.token_device])
        self.assertEqual(len(tokens), 1)
        self.assertEqual(tokens[0].token_serial, self.token_serial)
 
        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))
  
    def test_gets(self):
        deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        deploy_plugin.initialize_token(self.token_device)
        deploy_plugin.set_serial(self.token_device, self.token_serial)
   
        self.assertEqual(deploy_plugin.get_serial(self.token_device), self.token_serial)
           
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
        self.assertEqual(deploy_plugin.get_serial(self.token_device), self.token_serial)
        self.assertNotEqual(deploy_plugin.get_public_key(self.token_device), '')
        self.assertNotEqual(deploy_plugin.get_public_key(self.token_device), None)
   
        (knownsecret, servers) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
        self.assertNotEqual(knownsecret, None)
        self.assertNotEqual(servers, None)
           
        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))
        deploy_plugin.set_enrolled(self.token_device)
        self.assertTrue(deploy_plugin.is_enrolled(self.token_device))
        deploy_plugin.reset_enrolled(self.token_device)
        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))
   
        (knownsecret_after, servers_after) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
        self.assertEqual(knownsecret, knownsecret_after)
        self.assertEqual(servers, servers_after)

    def test_challenge(self):
        deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
  
        challenge = ''.join(chr(random.randrange(60, 127)) for x in range(10))
        msc_filename = deploy_plugin._get_msc_filename(self.token_device)
        is_ok, error_message, challenge_signature = lib_cpp.swissbit.key_create_challenge_signature(msc_filename, challenge)
        
        public_key = lib.smartcard.common.from_hex(deploy_plugin.get_public_key(self.token_device))
        
        F4 = "\x01\x00\x01" 
        authenticated = lib.cryptfacility.verify_public_key_signature(public_key, F4, challenge, challenge_signature)
        print "Authenticated:", authenticated
        self.assertTrue(authenticated)

  
 
# class AuthCallbackStub(object):
#       
#     def plugin_event_state_change(self, name, predicate_info=[]):
#         pass
#   
#   
# class Auth_plugin_test(unittest.TestCase):
#   
#     def setUp(self):
#         deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
#         tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [])
#         self.assertTrue(len(tokens) > 0, "No micro_smart_swissbit token found, unittest aborted")
#         self.token_device = tokens[0].token_id
#   
#         self.token_serial = 'my_smart_card_001'
#         self.client_knownsecret = 'my_known_secret'
#         self.servers = '127.0.0.1, 8043'
#   
#         self.db_filename = os.tmpnam()
#         self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
#         database_model.connect_to_database_using_connection(self.db)
#   
#     def tearDown(self):
#         self.db.dispose()
#         try:
#             os.remove(self.db_filename)
#         except:
#             pass
#   
#     def test_set_servers(self):
#         deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
#         deploy_plugin.initialize_token(self.token_device)
#         deploy_plugin.set_serial(self.token_device, self.token_serial)
#         deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
#   
#         (knownsecret, servers_before) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
#         error_message = deploy_plugin.set_servers(self.token_device,  '127.0.0.1, 8044')
#         self.assertEqual(error_message, None)
#   
#         (knownsecret, servers_after) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
#         self.assertNotEqual(servers_before, servers_after)
#           
#           
#     def test_auth_plugin_simple(self):
#         tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
#         tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()
#   
#         management_message_session_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSendBlackHole()
#         self.plugin_client = plugin_modules.micro_smart_swissbit.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
#         self.plugin_server = plugin_modules.micro_smart_swissbit.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, self.db, management_message_session_send, None)
#         self.plugin_server.set_callback(AuthCallbackStub())
#           
#         self.plugin_client._token_id = self.token_device
#           
#         self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
#         self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
#           
#         deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
#         deploy_plugin.initialize_token(self.token_device)
#         deploy_plugin.set_serial(self.token_device, self.token_serial)
#         deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
#           
#         with database_model.Transaction() as dbt:
#             new_token =  dbt.add(database_model.Token())
#             new_token.serial = self.token_serial
#             new_token.plugin_type = u'micro_smart_swissbit'
#             new_token.public_key = deploy_plugin.get_public_key(self.token_device)
#             dbt.commit()
#   
#         self.plugin_server.start()
#         giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((self.token_serial, self.token_serial)), 5000)
#         self.assertEqual(self.plugin_server.check_predicate((self.token_serial, self.token_serial)), True)
#   
  
  
# class Installation_test(unittest.TestCase):
#     def setUp(self):
#         self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
#         self.gon_installation.generate_gpms()
#         self.knownsecret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
#         self.knownsecret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'
#         self.servers  = "hej med dig"
#   
#     def tearDown(self):
#         pass
#       
#     def test_auth_plugin_simple(self):
#           
#         class Install_CB(plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
#             def __init__(self, download_root):
#                 self.download_root = download_root
#                 self.gpm_meta_signature = None
#                                   
#             def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
#                 print "Install_CB::gpm_installer_cb_task_all_start", tasks_count, tasks_ticks_count
#           
#             def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
#                 print "Install_CB::gpm_installer_cb_task_start", task_title, task_ticks_count
#           
#             def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
#                 print "Install_CB::gpm_installer_cb_action_start", action_title, action_ticks_conut
#           
#             def gpm_installer_cb_action_tick(self, ticks):
#                 print "Install_CB::gpm_installer_cb_actioplugin_type_client_runtime_envn_tick", ticks
#           
#             def gpm_installer_cb_action_done(self):
#                 print "Install_CB::gpm_installer_cb_action_done"
#           
#             def gpm_installer_cb_task_done(self):
#                 print "Install_CB::gpm_installer_cb_task_done"
#           
#             def gpm_installer_cb_task_all_done(self):
#                 print "Install_CB::gpm_installer_cb_task_all_done"
#           
#         deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
#         tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [])
#         device_id = tokens[0].token_id
#           
#         runtime_env = plugin_modules.micro_smart_swissbit.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), device_id)
#         runtime_env.set_download_root(self.gon_installation.get_gpms_folder()) 
#               
#         dictionary = giri_unittest.get_dictionary()
#               
#         error_handler = lib.gpm.gpm_env.GpmErrorHandler()
#         runtime_env.install_gpms_clean(['a_extra-1-1-win', 'b-1-1-win'], Install_CB(self.gon_installation.get_gpms_folder()), error_handler, dictionary, self.knownsecret_client, self.servers, False)
#         error_handler.dump()
#         self.assertTrue(error_handler.ok())
#   
#         gpm_repository = self.gon_installation.get_gpm_repository()
#         support = runtime_env.support_install_gpms(['boot-1-1-win'], [], [], gpm_repository.get_meta(), None, dictionary, self.knownsecret_client, self.servers)
#         print support
#         if sys.platform == "win32":
#             self.assertTrue(support['supported'])
#         else:
#             self.assertFalse(support['supported'])

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
# 
def micro_smart_token_found():
    deploy_plugin = plugin_modules.micro_smart_swissbit.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
    tokens = deploy_plugin.get_tokens(u'micro_smart_swissbit', [])
    return len(tokens) > 0
GIRI_UNITTEST_IGNORE = not micro_smart_token_found()

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
