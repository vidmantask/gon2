"""
SoftToken for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import lib.cryptfacility

import random


from components.communication import message

from plugin_modules import soft_token
from plugin_types.common import plugin_type_token as database_model 
from plugin_types.server_gateway import plugin_type_auth

from components.database.server_common.database_api import *


class PluginAuthentication(plugin_type_auth.PluginTypeAuth):
    """
    Runtime server part of SoftToken authentication plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'soft_token', license_handler, session_info, database, management_message_session, access_log_server_session)

        self.serial = None
        self.serial_authenticated = False
        self.challenge_size = 10
        self.challenge = None

    def start(self):
        """
        Initiates the process
        """
        if self._tunnel_endpoint:
            self.tunnelendpoint_send(message.Message('get_serial'))
            self.set_started()
        else:
            raise plugin_type_auth.PluginWaitingForConnectionException()
            
    def get_predicate_info(self):
        if self.serial_authenticated:
            return [dict(value = self.serial)]
        else:
            return [dict(value =None)]
            
    def _check_element(self, param, internal_type=None):
        """
        Returns true if the the serial(label) is matching the found serial and the found serial is authenticated
        """
        key_id  = param[0]
        key_label = param[1]
        self.checkpoint_handler.Checkpoint('has_id', self.plugin_name, lib.checkpoint.DEBUG, key_id=key_id, key_label=key_label, serial=self.serial, serial_authenticated='%r' % self.serial_authenticated)
        return key_id == self.serial and self.serial_authenticated

    msg_receivers = ['get_serial_response', 'get_challenge_response']

    def get_serial_response(self, serial):
        with self.checkpoint_handler.CheckpointScope('get_serial_response', self.plugin_name, lib.checkpoint.DEBUG, serial=serial):
            if serial:
                self.serial = serial
                self.challenge = ''.join(chr(random.randrange(60, 127)) for x in range(self.challenge_size))
                self.tunnelendpoint_send(message.Message('get_challenge', challenge=self.challenge))
            else:
                self.set_ready()

    def get_challenge_response(self, challenge_signature):
        with self.checkpoint_handler.CheckpointScope('get_challenge_response', self.plugin_name, lib.checkpoint.DEBUG, serial=self.serial) as cps:
            self.serial_authenticated = False
            if self.challenge != None and challenge_signature != None:
                with database_model.ReadonlySession() as dbs:
                    keys = dbs.select(database_model.Token, database_model.and_(database_model.Token.serial==self.serial))
                    if len(keys) == 0:
                        self.checkpoint_handler.CheckpointScope('get_challenge_response.error', self.plugin_name, lib.checkpoint.ERROR, message='Requested serial not found in the database')
                        self.access_log_server_session.report_access_log_gateway_warning(self.plugin_name, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_ID, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_SUMMARY, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_DETAILS)
                    else:
                        public_key = keys[0].public_key.encode('ascii')
                        self.serial_authenticated = lib.cryptfacility.pk_verify_challenge(public_key, self.challenge, challenge_signature)
                        if not self.serial_authenticated:
                            self.access_log_server_session.report_access_log_gateway_warning(self.plugin_name, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_ID, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_SUMMARY, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_DETAILS)
                        else:
                            self.get_message_sender().send_message("sink_serial_authenticated", serial=self.serial)
                
            cps.add_complete_attr(serial_authenticated='%r' % self.serial_authenticated)
            self.set_ready()
