"""
Common functionality for SoftToken on Gateway Client level
"""
from __future__ import with_statement

import os
import os.path
import shutil
import tempfile
import uuid

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device
import lib.config
import lib.utility

from plugin_types.client_gateway import plugin_type_token


class SoftTokenTokens(object):
    INIT_FOLDERNAME = os.path.join('gon_client', 'gon_init_soft_token')
    DEPLOY_FOLDERNAME = os.path.join('gon_client', 'gon_deploy_soft_token')
    
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
   
    def initialize_token(self, device):
        try:
            if not os.path.isdir(device):
                os.makedirs(device)
        except os.error:
            pass

        if not os.path.isdir(device):
            raise plugin_type_token.Error("'%s' is a invalid device" % device)
        
        init_folder_abs = self._get_init_folder(device)
        if not os.path.exists(init_folder_abs):
            os.makedirs(init_folder_abs)
            
        deploy_folder_abs = self._get_deploy_folder(device)
        if os.path.exists(deploy_folder_abs):
            lib.utility.rmtree_fail_safe(deploy_folder_abs)
        
        serial_new = '%s' % uuid.uuid4()
        self.set_serial(device, serial_new)
    
    def get_tokens(self, plugin_name, additional_device_roots):
        devices = []
        if additional_device_roots is not None:
            for additional_device_root in additional_device_roots:
                if os.path.isdir(additional_device_root):
                    for root, dirs, files in os.walk(additional_device_root):
                        devices.append(os.path.abspath(root))
        devices.extend(lib.hardware.device.Device.get_device_list())

        tokens = []
        for device in devices:
            init_folder_abs = self._get_init_folder(device)
            if os.path.exists(init_folder_abs):
                client_runtime_env = '%s::%s' % (u'soft_token_client_runtime_env', device) 
                token = plugin_type_token.Token(plugin_name, device, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, client_runtime_env, token_type_title="SoftToken",token_plugin_module_name='soft_token')
                serial_filename = self._get_serial_filename(device)
                if os.path.exists(serial_filename):
                    serial_file = open(serial_filename, 'r')
                    token.token_serial = serial_file.read(1000)
                else:
                    token.token_serial = ''

                if self.is_enrolled(device):
                    token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED
                    
                tokens.append(token)
        return tokens

    def deploy_token(self, token_id, client_knownsecret, servers):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)

        client_knownsecrete_filename = self._get_client_knownsecret_filename(device)
        client_knownsecrete_file = open(client_knownsecrete_filename, 'w')
        client_knownsecrete_file.write(client_knownsecret)
        client_knownsecrete_file.close()

        servers_filename = self._get_servers_filename(device)
        servers_file = open(servers_filename, 'w')
        servers_file.write(servers)
        servers_file.close()

    def generate_keypair(self, token_id):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)
        
        key_pair = lib.cryptfacility.pk_generate_keys()
        public_key_filename = self._get_public_key_filename(device)
        public_key_file = open(public_key_filename, 'w')
        public_key_file.write(key_pair[0])
        public_key_file.close()

        private_key_filename = self._get_private_key_filename(device)
        private_key_file = open(private_key_filename, 'w')
        private_key_file.write(key_pair[1])
        private_key_file.close()

    def get_public_key(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        return self._get_file_content_as_string(self._get_public_key_filename(device))

    def get_private_key(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        return self._get_file_content_as_string(self._get_private_key_filename(device))

    def get_serial(self, token_id):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been initialized'" % device)
        return self._get_file_content_as_string(self._get_serial_filename(device))

    def set_serial(self, token_id, serial):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been initialized'" % device)

        serial_file_name = self._get_serial_filename(device)
        serial_file = open(serial_file_name, 'w')
        serial_file.write(serial)
        serial_file.close()

    def _get_knownsecret(self, token_id):
        device = token_id
        if os.path.exists(self._get_client_knownsecret_filename(device)):
            return self._get_file_content_as_string(self._get_client_knownsecret_filename(device))
        return None

    def _get_servers(self, token_id):
        device = token_id
        if os.path.exists(self._get_servers_filename(device)):
            return lib.config.parse_servers(self.checkpoint_handler, self._get_file_content_as_string(self._get_servers_filename(device)))
        return None

    def set_enrolled(self, token_id):
        device = token_id
        enrolled_filename = self._get_client_enrolled_filename(device)
        enrolled_file = open(enrolled_filename, 'w')
        enrolled_file.write('X')
        enrolled_file.close()

    def reset_enrolled(self, token_id):
        device = token_id
        enrolled_filename = self._get_client_enrolled_filename(device)
        if os.path.exists(enrolled_filename):
            os.remove(enrolled_filename) 
            
    def is_enrolled(self, token_id):
        device = token_id
        return os.path.exists(self._get_client_enrolled_filename(device))

    def get_knownsecret_and_servers(self, token_id):
        return (self._get_knownsecret(token_id), self._get_servers(token_id))
      
    def set_servers(self, token_id, servers):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            return "The token '%s' has not been deployed'" % device

        servers_filename = self._get_servers_filename(device)
        servers_file = open(servers_filename, 'w')
        servers_file.write(servers)
        servers_file.close()
        return None
        
    def _get_file_content_as_string(self, filename):
        if not os.path.exists(filename):
            raise plugin_type_token.Error("The file '%s' can not be found" % filename)
        file = open(filename, 'r')
        value = file.read()
        file.close()
        return value

    def _get_init_folder(self, device):
        return os.path.join(os.path.abspath(device), SoftTokenTokens.INIT_FOLDERNAME)
        
    def _get_deploy_folder(self, device):
        return os.path.join(os.path.abspath(device), SoftTokenTokens.DEPLOY_FOLDERNAME)

    def _get_gpms_folder(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gpms_installed')

    def _get_serial_filename(self, device):
        return os.path.join(self._get_init_folder(device), 'serial')
        
    def _get_private_key_filename(self, device):
        return os.path.join(self._get_deploy_folder(device), 'private')

    def _get_public_key_filename(self, device):
        return os.path.join(self._get_deploy_folder(device), 'public')

    def _get_client_knownsecret_filename(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gon_client.ks')

    def _get_servers_filename(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gon_client.servers')

    def _get_client_enrolled_filename(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gon_client.enrolled')
