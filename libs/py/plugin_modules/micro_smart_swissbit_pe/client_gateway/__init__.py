"""
MicroSmart Swissbit plugin for Gateway Client
"""
from __future__ import with_statement

import sys
import os
import os.path

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device
import lib.appl
import lib_cpp.swissbit


from components.communication import message

from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_client_runtime_env

import plugin_modules.micro_smart_swissbit_pe.client_gateway_common

import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_installer

MODULE_ID = 'micro_smart_swissbit_pe'

class PluginTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'micro_smart_swissbit_pe'
    """
    Token plugin
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        self._tokens = plugin_modules.micro_smart_swissbit_pe.client_gateway_common.SmartCardTokens(checkpoint_handler)
        self._token_id = None

    def get_tokens(self, additional_device_roots):
        return self._tokens.get_tokens(self.plugin_name, additional_device_roots)

    def initialize_token(self, device):
        self._tokens.initialize_token(device)

    def deploy_token(self, token_id, client_knownsecret, servers):
        self._tokens.deploy_token(token_id, client_knownsecret, servers)

    def generate_keypair(self, token_id):
        self._tokens.generate_keypair(token_id)

    def set_serial(self, token_id, serial):
        self._tokens.set_serial(token_id, serial)

    def get_public_key(self, token_id):
        return self._tokens.get_public_key(token_id)

    def set_enrolled(self, token_id):
        self._tokens.set_enrolled(token_id)

    def reset_enrolled(self, token_id):
        self._tokens.reset_enrolled(token_id)

    def is_enrolled(self, token_id):
        return self._tokens.is_enrolled(token_id)

    def get_knownsecret_and_servers(self, token_id):
        return self._tokens.get_knownsecret_and_servers(token_id)

    def set_servers(self, token_id, servers):
        return self._tokens.set_servers(token_id, servers)

    def get_serial(self, token_id = None):
        if token_id is None:
            return self.get_serial_auth()
        return self._tokens.get_serial(token_id)

    def _select_token(self):
        if self._token_id == None:
            tokens = self._tokens.get_tokens(self.plugin_name, self.additional_device_roots)
            if len(tokens) > 0:
                return tokens[0].token_id
            return None
        else:
            return self._token_id

    def _get_serial(self):
        token_id = self._select_token()
        if token_id != None:
            try:
                return unicode(self._tokens.get_serial(token_id))
            except:
                self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.DEBUG, message='No serial found')
                return None
        else:
            self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None

    def _get_challenge(self, challenge):
        token_id = self._select_token()
        if token_id != None:
            msc_filename = self._tokens._get_msc_filename(token_id)
            is_ok, error_message, signature = lib_cpp.swissbit.key_create_challenge_signature(msc_filename, challenge)
            if is_ok:
                return signature
            self.checkpoint_handler.Checkpoint('get_challenge.error', self.plugin_name, lib.checkpoint.ERROR, message='%s' % error_message)
            return None
        else:
            self.checkpoint_handler.Checkpoint('_get_challenge', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None

    msg_receivers = ['get_serial', 'get_challenge']

    def get_serial_auth(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            serial = self._get_serial()
            cps.add_complete_attr(serial=serial)
            self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))

    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            challenge_signature = self._get_challenge(challenge)
            self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))



class PluginClientRuntimeEnv(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv):
    """
    Client runtime envrionment plugin
    """
    PLUGIN_NAME = u'micro_smart_swissbit_pe_client_runtime_env'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._user_interface = user_interface
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv.__init__(self, async_service, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, user_interface, additional_device_roots)
        self._instances = {}

    def _build_instances(self):
        msc_tokens = plugin_modules.micro_smart_swissbit_pe.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        tokens = msc_tokens.get_tokens(PluginClientRuntimeEnv.PLUGIN_NAME, self.additional_device_roots)
        for token in tokens:
            self._instances[token.token_id] = PluginClientRuntimeEnvInstance(self.checkpoint_handler, token.token_id)

    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin.
        """
        self._build_instances()
        return self._instances.keys()

    def get_instance(self, runtime_env_id):
        """
        Return the instance.
        """
        self._build_instances()
        if runtime_env_id in self._instances.keys():
            return self._instances[runtime_env_id]
        return None


class PluginClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstance):
    def __init__(self, checkpoint_handler, token_id):
        self.checkpoint_handler = checkpoint_handler
        self._token_type = PluginClientRuntimeEnv.PLUGIN_NAME
        self._token_id = token_id
        self._token_title = 'MicroSmartSwissbitPE'
        self.set_root(self._token_id, '/mnt/live')
        self._forced_to_current = False
        self.clean_skip_folders = plugin_modules.micro_smart_swissbit_pe.client_gateway_common.SmartCardTokens.INIT_FOLDERNAME


    def set_knownsecret_and_servers(self, knownsecret, servers, generate_keypair):
        endpoint_token_deploy = plugin_modules.micro_smart_swissbit_pe.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        if generate_keypair:
            endpoint_token_deploy.initialize_token(self.get_root())
            endpoint_token_deploy.generate_keypair(self.get_root())
            # Ignoring exceptions because token might not be deployed yet
            try:
                endpoint_token_deploy.reset_enrolled(self.get_root())
            except:
                self.checkpoint_handler.Checkpoint('reset_enrolled', PluginTokenAndAuth.PLUGIN_NAME, lib.checkpoint.WARNING, message='exception ignored')
        endpoint_token_deploy.deploy_token(self.get_root(), knownsecret, servers)

    def get_knownsecret_and_servers(self):
        endpoint_token_deploy = plugin_modules.micro_smart_swissbit_pe.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        return endpoint_token_deploy.get_knownsecret_and_servers(self.get_root())







    def force_to_current(self):
        """
        Tell this instance that it has be chosen to be the current.
        """
        self._forced_to_current = True

    def get_info(self):
        """
        Return info object about the client runtime environment
        """
        info = plugin_type_client_runtime_env.Info(self._token_type, self._token_id, self._token_title)
        return info

    def set_root(self, rw_root, ro_root):
        """
        Set the folder root for all envs of this plugin
        """
        self._runtime_root = ro_root
        self._ro_root = ro_root
        self._rw_root = rw_root
        self._gpm_meta_root = os.path.join(self._ro_root, 'gon_client' ,'gpm_meta')
        self._download_root = os.path.join(self.get_temp_root(), 'download_cache')

    def get_root(self):
        return self._runtime_root

    def set_download_root(self, download_root):
        self._download_root = download_root

    def is_current(self):
        """
        Expected to return true if this client runtime environment is the one that is currently being used.
        Only one plugin is expected to be the current one
        """
        if self._runtime_root is None:
            return self._forced_to_current
        current_installation_root_normalized = os.path.normpath(lib.appl.gon_client.get_installation_root())
        runtime_root_normalized = os.path.normpath(self._runtime_root)
        self.checkpoint_handler.Checkpoint("is_current", MODULE_ID, lib.checkpoint.DEBUG, token_type=self._token_type, current_installation_root_normalized=repr(current_installation_root_normalized), runtime_root_normalized=repr(runtime_root_normalized))

        #should use os.path.samefile but it only works on linux
        if os.path.normcase(current_installation_root_normalized) == os.path.normcase(runtime_root_normalized):
            return True
        return self._forced_to_current

    def is_current_relocated(self, installation_root):
        return False

    def support_download(self, size, dictionary):
        return {'supported': False, 'message': dictionary._("Download not supported")}

    def get_download_root(self):
        return self._download_root

    def support_bootification(self, dictionary):
        return (False, dictionary._('Bootification not supported'))

    def support_install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gon_client_install_state, dictionary, knownsecret, servers):
        return {'supported': False, 'message': dictionary._("Installation of packages not supported")}

    def get_installed_gpm_meta_all(self, error_handler):
        return lib.gpm.gpm_builder.query_meta_all_from_files(self._gpm_meta_root, error_handler)

    def get_installed_gpm_meta(self, gpm_id, error_handler):
        return lib.gpm.gpm_builder.query_meta_from_from_file(self._gpm_meta_root, gpm_id, error_handler)

    def remove_installed_gpm_meta(self, gpm_id, error_handler):
        error_handler.emmit_error("Remove of gpm_meta not supported")

    def install_gpm_meta(self, gpm_filename, error_handler):
        error_handler.emmit_error("Install of gpm_meta not supported")

    def get_download_gpm_filename(self, gpm_id):
        gpm_filename_abs = os.path.join(self._download_root, '%s.gpm' % gpm_id)
        return gpm_filename_abs

    def get_ro_root(self):
        return self._ro_root

    def get_rw_root(self):
        return self._rw_root

    def get_temp_root(self):
        temp_root = os.path.join(self.get_rw_root(), 'gon_client', 'gon_temp')
        if not os.path.exists(temp_root):
            os.makedirs(temp_root)
        return temp_root

    def is_in_use(self, timeout=None):
        """
        Expected to return true if this client runtime environment is in use at the moment.
        The timeout attribute indicate when a mark is no longer valid
        """
        temp_root = os.path.join(self.get_rw_root(), 'gon_client', 'gon_temp')
        if not os.path.exists(temp_root):
            return False

        in_use_mark = lib.appl.in_use_mark.InUseMark(temp_root)
        return in_use_mark.is_in_use(timeout)

    def mark_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return true.
        """
        try:
            in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
            in_use_mark.do_mark_in_use()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            print 'mark_in_use.exception', evalue

    def mark_not_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return false.
        """
        try:
            in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
            in_use_mark.do_mark_not_in_use()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            print 'mark_not_in_use.exception', evalue

    def support_update_connection_info(self, dictionary):
        """
        Examine if the connection info can be updated.

        Expected to return a dict with the following content:
          {'supported': bool, 'supported_by_installation':bool, 'message': String}
        """
        return {'supported':True, 'supported_by_installation':False, 'message':'Supported'}

    def update_connection_info(self, knownsecret, servers, error_handler):
        """
        Expected to update the connection info
        """
        self.set_knownsecret_and_servers(knownsecret, servers, False)

    def get_connection_info(self):
        """
        Expected to return connection information in the form (knownsecret, servers)
        """
        return self.get_knownsecret_and_servers()
