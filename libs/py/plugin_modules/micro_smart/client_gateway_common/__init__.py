"""
Common functionality for G&D MicroSmart on Gateway Client level
"""
from __future__ import with_statement

import sys
import os
import os.path
import shutil
import tempfile
import traceback

import pickle
import zlib
import uuid

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device
import lib.smartcard.pkcs15
import lib.smartcard.msc
import lib.config
import lib.utility

import plugin_modules.micro_smart

from plugin_types.client_gateway import plugin_type_token

CHUNK_ID_INIT = 0
CHUNK_ID_DEPLOY = 1


class SmartCardTokens(object):
    INIT_FOLDERNAME = os.path.join('gon_client', 'gon_init_micro_smart')
    DEPLOY_FOLDERNAME = os.path.join('gon_client', 'gon_deploy_micro_smart')
   
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
        self._data_chunks_init = {}
        self._data_chunks_deploy = {}

    def initialize_token(self, device):
        if not os.path.isdir(device):
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        
        init_folder_abs = self._get_init_folder(device)
        if not os.path.exists(init_folder_abs):
            os.mkdir(init_folder_abs)

        deploy_folder_abs = self._get_deploy_folder(device)
        if os.path.exists(deploy_folder_abs):
            lib.utility.rmtree_fail_safe(deploy_folder_abs)

        serial_new = '%s' % uuid.uuid4()
        self.set_serial(device, serial_new)

    def get_tokens(self, plugin_name, additional_device_roots):
        tokens = []
        devices = lib.hardware.device.Device.get_device_list()
        for device in devices:
            init_folder_abs = self._get_init_folder(device)
            if os.path.exists(init_folder_abs):
                client_runtime_env = '%s::%s' % (u'micro_smart_client_runtime_env', device) 
                token = plugin_type_token.Token(plugin_name, device, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, client_runtime_env, token_type_title="MicroSmart", token_plugin_module_name='micro_smart')
                try:
                    
                    with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                        msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)

                        (token.token_serial) = self._get_data_chunk_init(device, msc_pkcs15, use_cache=False)
                        if token.token_serial == None:
                            token.token_serial = ''

                        (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15, use_cache=False)
                        if enrolled is not None and enrolled:
                            token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED

                except lib.smartcard.common.SmartCardException, e:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointMultilineMessages('get_tokens.failed', plugin_modules.micro_smart.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))

                tokens.append(token)
        return tokens


    def deploy_token(self, token_id, client_knownsecret, servers):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                self._put_data_chunk_deploy(token_id, msc_pkcs15, client_knownsecret, servers, False)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def generate_keypair(self, token_id):
        device = token_id
        deploy_folder_abs = self._get_deploy_folder(token_id)
        if not os.path.exists(deploy_folder_abs):
            os.mkdir(deploy_folder_abs)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                msc_pkcs15.generate_key_pair(keylength=2048)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)

   
    def get_public_key(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                public_key = msc_pkcs15.get_public_key()
                return lib.smartcard.pkcs15.to_hex(public_key)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)


    def get_serial(self, token_id):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (serial) = self._get_data_chunk_init(device, msc_pkcs15)
                return serial
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)


    def set_serial(self, token_id, serial):
        device = token_id
        if not os.path.exists(self._get_init_folder(device)):
            raise plugin_type_token.Error("The token '%s' has not been deployed'" % device)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                self._put_data_chunk_init(device, msc_pkcs15, serial)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)


    def set_enrolled(self, token_id):
        device = token_id
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15)
                self._put_data_chunk_deploy(device, msc_pkcs15, client_knownsecret, servers_raw, True)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)
        
    def reset_enrolled(self, token_id):
        device = token_id
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15)
                self._put_data_chunk_deploy(device, msc_pkcs15, client_knownsecret, servers_raw, False)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def is_enrolled(self, token_id):
        device = token_id
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15)
                return enrolled
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)
        return False

    def get_knownsecret_and_servers(self, token_id):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            return (None, None)
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15)
                servers = lib.config.parse_servers(self.checkpoint_handler, servers_raw)
                return (client_knownsecret, servers)
        except lib.smartcard.msc.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def set_servers(self, token_id, servers):
        device = token_id
        if not os.path.exists(self._get_deploy_folder(device)):
            return "The token '%s' has not been deployed'"
        try:
            with lib.smartcard.msc.MSC(self._get_msc_filename(device)) as smart_card_transaction:
                msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, msc_pkcs15)
                self._put_data_chunk_deploy(device, msc_pkcs15, client_knownsecret, servers, enrolled)
                return None
        except lib.smartcard.msc.SmartCardException, e:
            return str(e)
    
    def _get_gpms_folder(self, device):
        return os.path.join(self._get_deploy_folder(device), 'gpms_installed')

    def _get_init_folder(self, device):
        return os.path.join(os.path.abspath(device), SmartCardTokens.INIT_FOLDERNAME)
        
    def _get_deploy_folder(self, device):
        return os.path.join(os.path.abspath(device), SmartCardTokens.DEPLOY_FOLDERNAME)

    def _get_msc_filename(self, device):
        if sys.platform == 'darwin':
            return str(os.path.join(self._get_init_folder(device), '.giritech.dat'))
        else:
            return str(os.path.join(self._get_init_folder(device), 'giritech.msc'))

    def _put_data_chunk_init(self, token_id, msc_pkcs15, serial):
        chunk = (ord(c) for c in zlib.compress(pickle.dumps( (serial) ), 9))
        msc_pkcs15.write_file(chunk, CHUNK_ID_INIT)
        if self._data_chunks_init.has_key(token_id):
            del self._data_chunks_init[token_id]
    
    def _put_data_chunk_deploy(self, token_id, msc_pkcs15, client_knownsecret, servers, enrolled):
        chunk = (ord(c) for c in zlib.compress(pickle.dumps( (client_knownsecret, servers, enrolled) ), 9))
        msc_pkcs15.write_file(chunk, CHUNK_ID_DEPLOY)
        if self._data_chunks_deploy.has_key(token_id):
            del self._data_chunks_deploy[token_id]

    def _get_data_chunk_init(self, token_id, msc_pkcs15, use_cache=True):
        if self._data_chunks_init.has_key(token_id) and use_cache:
            return self._data_chunks_init[token_id]
        else:
            try:
                chunk = ''.join(chr(x) for x in msc_pkcs15.read_file(CHUNK_ID_INIT))
                self._data_chunks_init[token_id] = pickle.loads(zlib.decompress(chunk))
                return self._data_chunks_init[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_init.failed', plugin_modules.micro_smart.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None)
    
    def _get_data_chunk_deploy(self, token_id, msc_pkcs15, use_cache=True):
        if self._data_chunks_deploy.has_key(token_id) and use_cache:
            return self._data_chunks_deploy[token_id]
        else:
            try:
                chunk_raw = ''.join(chr(x) for x in  msc_pkcs15.read_file(CHUNK_ID_DEPLOY))
                chunk = pickle.loads(zlib.decompress(chunk_raw))
                client_knownsecret = None
                servers_raw = None
                enrolled = False
                if len(chunk) == 2:
                    (client_knownsecret, servers_raw) = chunk
                if len(chunk) == 3:
                    (client_knownsecret, servers_raw, enrolled) = chunk
                self._data_chunks_deploy[token_id] = (client_knownsecret, servers_raw, enrolled)
                return self._data_chunks_deploy[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_init.failed', plugin_modules.micro_smart.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None, None, None)
