# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import datetime
from plugin_types.server_management import plugin_type_element, plugin_type_config
from components.database.server_common import database_api
from plugin_modules.login_interval.server_common import database_schema 
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

# ??? allowed
from components.auth.server_management import OperationNotAllowedException

class PluginLoginInterval(plugin_type_config.PluginTypeConfig, plugin_type_element.PluginTypeElement):

    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_name = "login_interval"
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        
    element_types = ["login_interval"]
        
    def get_elements(self, element_type):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.LoginInterval)
            element_list = []
            for e in elements:
                element_list.append(self._create_element(e))
            return element_list
             
    def _create_element(self, e):
        element = self.create_element(e.id, e.name, e.description)
        return element

    def get_element_count(self, element_type):
        with database_api.ReadonlySession() as dbs:
            return dbs.select_count(database_schema.LoginInterval) 
        
        
    def get_specific_elements(self, internal_element_type, element_ids):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.LoginInterval, database_api.in_(database_schema.LoginInterval.id, element_ids))
            return [self._create_element(e) for e in elements]
        

    def config_get_template(self, element_type, element_id, db_session=None):
        with database_api.SessionWrapper(db_session) as dbs:
            element = None
            if element_id:
                element = self._lookup_element(dbs, element_id)
            config_spec = self._get_config_spec(element)
            return config_spec.get_template()
        
    
    def get_config_enabling(self, element_type):
        return True, True
    
    
    def _get_config_spec(self, element):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (self.plugin_name,), "Login Interval", "")

        config_spec.add_field("name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, mandatory=True)
        config_spec.add_field("id", 'ID', ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        config_spec.add_field("description", 'Description', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        default_start = datetime.time(9,0,0)
        default_end = datetime.time(17,0,0)
        for day in range(0,7):
            config_spec.add_field("day%s_allday" % day, 'All Day%s' % day, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, value=True)
            config_spec.add_field("day%s_active" % day, 'Never Day%s' % day, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
            config_spec.add_field("day%s_time" % day, 'Time Day%s' % day, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
            config_spec.add_field("day%s_start" % day, 'Start Day%s' % day, ConfigTemplateSpec.VALUE_TYPE_TIME, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, value=default_start)
            config_spec.add_field("day%s_end" % day, 'End Day%s' % day, ConfigTemplateSpec.VALUE_TYPE_TIME, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, value=default_end)
        
        if element:
            config_spec.set_values_from_object(element)
            for day in range(0,7):
                value = not getattr(element, "day%s_allday" % day) and not getattr(element, "day%s_active" % day) 
                config_spec.set_value("day%s_time" % day, value)
            
        return config_spec

    
    def _check_constraints(self, new_element, db_session):
        elementes = db_session.select(database_schema.LoginInterval, database_schema.LoginInterval.name==new_element.name)
        if len(elementes)>1:
            raise OperationNotAllowedException("An IP Range with this name already exists")
    

    def _update_obj_with_template(self, new_element, template, transaction):
        config_spec = ConfigTemplateSpec(template)
        if not config_spec.get_value("name"):
            raise OperationNotAllowedException("Name cannot be empty")
        row = config_spec.get_field_value_dict()
        self._update_obj_with_row(new_element, row, transaction)
        
        
        
    
    
    def config_create_row(self, element_type, template, dbt):
        #new_key =  dbt.add(Token(**row))
        new_element = dbt.add(database_schema.LoginInterval())
        self._update_obj_with_template(new_element, template, dbt)
        dbt.flush()
        
        return "%s.%s" % (self.plugin_name, new_element.id) 
            
    
    def config_update_row(self, element_type, template, dbt):
        config_spec = ConfigTemplateSpec(template)
        element = self._lookup_element(dbt, config_spec.get_value("id"))
        if element:
            self._update_obj_with_template(element, template, dbt)
            return config_spec.get_value("name")
        else:
            raise Exception("The Element was not found in the database")
    
    def config_delete_row(self, element_type, element_id, dbt):
        element = self._lookup_element(dbt, element_id)
        if element:
            dbt.delete(element)
            return True
        return False


    def _lookup_element(self, dbs, element_id):
        return dbs.get(database_schema.LoginInterval, element_id)


