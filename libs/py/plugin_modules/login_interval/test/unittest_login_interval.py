"""
Unittest of certificate plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility
import lib.hardware.device

import datetime, time


import components.database.server_common.database_api as database_api 

from components.communication.sim import sim_tunnel_endpoint

from components.database.server_common.connection_factory import ConnectionFactory
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from components.auth.server_management import OperationNotAllowedException


import plugin_modules.login_interval.server_gateway
import plugin_modules.login_interval.server_management
from plugin_modules.login_interval.server_common import database_schema as database_model 




class AuthCallbackStub(object):
    
    def plugin_event_state_change(self, name, info=[]):
        pass

class Auth_plugin_test(unittest.TestCase):

    def setUp(self):
        self.db_filename = giri_unittest.mk_temp_file()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)
        

        
    def tearDown(self):
        self.db.dispose()
        try:
            os.remove(self.db_filename)
        except:
            pass

    def test_mgmt0(self):
        plugin = plugin_modules.login_interval.server_management.PluginLoginInterval(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)
        n = len(elements)
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test1")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), n+1)
        
        element = elements[0]
        template = plugin.config_get_template(None, element.get_internal_id())
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "")
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_update_row(None, template, dbt))
        config_spec.set_value("name", "test2")
        config_spec.set_value("description", "a"*10000)

        config_spec.set_value("day0_allday", True)
        config_spec.set_value("day1_active", True)
        config_spec.set_value("day2_time", True)
        config_spec.set_value("day3_time", True)
        config_spec.set_value("day3_start", datetime.datetime.strptime("2011-06-15 13:55:00", '%Y-%m-%d %H:%M:%S'))
        config_spec.set_value("day4_time", True)
        config_spec.set_value("day4_start", datetime.datetime.strptime("2011-06-15 13:55:00", '%Y-%m-%d %H:%M:%S'))
        config_spec.set_value("day4_end", datetime.datetime.strptime("2011-06-15 13:56:00", '%Y-%m-%d %H:%M:%S'))

        plugin.config_update_row(None, template, dbt)
        dbt.commit()
        
        elements = plugin.get_specific_elements(None, [element.get_internal_id()])
        self.assert_(len(elements)==1)
        element = elements[0]
        self.assertEquals(element.get_label(), "test2")
        self.assertEquals(element.get_info(), "a"*10000)
        
        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)
        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 0)

    def test_mgmt1(self):
        plugin = plugin_modules.login_interval.server_management.PluginLoginInterval(giri_unittest.get_checkpoint_handler(), None, None)
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_create_row(None, template, dbt) )

    def _set_day_type_allday(self, config_spec, day):
        config_spec.set_value("day%s_allday" % day, True)
        config_spec.set_value("day%s_active" % day, False)
        
    def _set_day_type_none(self, config_spec, day):
        config_spec.set_value("day%s_allday" % day, False)
        config_spec.set_value("day%s_active" % day, True)

    def _set_day_type_time(self, config_spec, day):
        config_spec.set_value("day%s_allday" % day, False)
        config_spec.set_value("day%s_active" % day, False)

    def test_gateway0(self):
        plugin = plugin_modules.login_interval.server_management.PluginLoginInterval(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)
        
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "ok0")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
            
        current_day = datetime.datetime.now().weekday()
            
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "ok1")
        self._set_day_type_allday(config_spec, current_day)
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
        
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "ok2")
        self._set_day_type_time(config_spec, current_day)
        config_spec.set_value("day%s_start" % current_day, datetime.datetime.now().time())
        d_end = datetime.datetime.now() + datetime.timedelta(seconds=60)
        config_spec.set_value("day%s_end" % current_day, d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)


        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "ok3")
        self._set_day_type_none(config_spec, current_day)
        prev_day = current_day-1 % 7
        self._set_day_type_time(config_spec, prev_day)
        d_start = datetime.datetime.now() + datetime.timedelta(seconds=120)
        config_spec.set_value("day%d_start" % (prev_day,), d_start.time())
        d_end = datetime.datetime.now() + datetime.timedelta(seconds=60)
        config_spec.set_value("day%d_end" % (prev_day), d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
            

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "ok4")
        self._set_day_type_time(config_spec, current_day)
        d_start = datetime.datetime.now() - datetime.timedelta(seconds=60)
        config_spec.set_value("day%s_start" % current_day, d_start.time())
        config_spec.set_value("day%s_end" % current_day, datetime.datetime.now().time())
        prev_day = current_day-1 % 7
        self._set_day_type_time(config_spec, prev_day)
        d_start = datetime.datetime.now() + datetime.timedelta(seconds=120)
        config_spec.set_value("day%d_start" % (prev_day,), d_start.time())
        d_end = datetime.datetime.now() + datetime.timedelta(seconds=60)
        config_spec.set_value("day%d_end" % (prev_day), d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
            


        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "fail1")
        self._set_day_type_none(config_spec, current_day)
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "fail2")
        self._set_day_type_time(config_spec, current_day)
        d_start = datetime.datetime.now() - datetime.timedelta(seconds=60)
        config_spec.set_value("day%s_start" % current_day, d_start.time())
        config_spec.set_value("day%s_end" % current_day, datetime.datetime.now().time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "fail3")
        self._set_day_type_none(config_spec, current_day)
        prev_day = current_day-1 % 7
        self._set_day_type_time(config_spec, prev_day)
        d_start = datetime.datetime.now() - datetime.timedelta(seconds=120)
        config_spec.set_value("day%d_start" % (prev_day,), d_start.time())
        d_end = datetime.datetime.now() + datetime.timedelta(seconds=60)
        config_spec.set_value("day%d_end" % (prev_day), d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "fail4")
        self._set_day_type_none(config_spec, current_day)
        prev_day = current_day-1 % 7
        self._set_day_type_allday(config_spec, prev_day)
        d_start = datetime.datetime.now() + datetime.timedelta(seconds=120)
        config_spec.set_value("day%d_start" % (prev_day,), d_start.time())
        d_end = datetime.datetime.now() + datetime.timedelta(seconds=60)
        config_spec.set_value("day%d_end" % (prev_day), d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "fail5")
        self._set_day_type_none(config_spec, current_day)
        prev_day = current_day-1 % 7
        self._set_day_type_time(config_spec, prev_day)
        d_start = datetime.datetime.now() + datetime.timedelta(seconds=120)
        config_spec.set_value("day%d_start" % (prev_day,), d_start.time())
        d_end = datetime.datetime.now()
        config_spec.set_value("day%d_end" % (prev_day), d_end.time())
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
        
        plugin_gateway = plugin_modules.login_interval.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())

        elements = plugin.get_elements(None)
        for e in elements:
            value = plugin_gateway.check_predicate((e.get_internal_id(), e.get_label()))
            if e.get_label().startswith("ok"):
                self.assertTrue(value)
            else:
                self.assertFalse(value)
                
        
#        for e in elements:
#            with database_api.Transaction() as dbt:
#                plugin.config_delete_row(None, e.get_internal_id(), dbt)
#        elements = plugin.get_elements(None)
#        self.assertEqual(len(elements), 0)

def atest_auth_plugin_simple(self):
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        self.plugin_client = plugin_modules.smart_card.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
        self.plugin_server = plugin_modules.smart_card.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, self.db, None, None, None)
        self.plugin_server.set_callback(AuthCallbackStub())
        
        self.plugin_client._token_id = self.token_device
        
        self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
        self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
        
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        deploy_plugin.initialize_token(self.token_device)
        deploy_plugin.set_serial(self.token_device, self.token_serial)
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
        
        with database_model.Transaction() as dbt:
            new_token =  dbt.add(database_model.Token())
            new_token.serial = self.token_serial
            new_token.plugin_type = u'smart_card'
            new_token.public_key = deploy_plugin.get_public_key(self.token_device)
            dbt.commit()

        self.plugin_server.start()
        giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((self.token_serial, self.token_serial)), 10000)
        self.assertEqual(self.plugin_server.check_predicate((self.token_serial, self.token_serial)), True)


        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
