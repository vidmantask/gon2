"""
Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
from plugin_types.server_gateway import plugin_type_auth
from plugin_modules.login_interval.server_common import database_schema 
from components.database.server_common import database_api

import datetime
    
class PluginAuthentication(plugin_type_auth.PluginTypeAuth):

    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'login_interval', license_handler, session_info, database, management_message_session, access_log_server_session)
        self.elements = None
        self.element_values = dict()
        

    def start(self):
        self.set_ready()
        
    def get_login_interval(self, login_interval_id):
        if self.elements is None:
            with database_api.ReadonlySession() as db_session:
                login_interval_list = db_session.select(database_schema.LoginInterval)
            self.elements = dict((str(e.id), e) for e in login_interval_list)
        return self.elements.get(str(login_interval_id))
                

    def _check_element(self, param, internal_type=None):
        login_interval_id  = param[0]
        login_interval_label = param[1]
        self.checkpoint_handler.Checkpoint('check_interval', self.plugin_name, lib.checkpoint.DEBUG, login_interval_id=login_interval_id, login_interval_label=login_interval_label)
        
        value = self.element_values.get(login_interval_id)
        if value is None:
            _login_interval = self.get_login_interval(login_interval_id)
            value = self._check_interval(_login_interval)
            self.element_values[login_interval_id] = value
        return value
    
    def _check_day_before(self, login_interval, current_time, current_week_day):
        week_day = (current_week_day-1) % 7
        active = getattr(login_interval, "day%d_active" % week_day)
        if active:
            return False
        allday = getattr(login_interval, "day%d_allday" % week_day)
        if allday:
            return False
        start = getattr(login_interval, "day%d_start" % week_day)
        end = getattr(login_interval, "day%d_end" % week_day)
        if start > end:
            return current_time < end
        
        return False 
        

    def _check_interval(self, login_interval):
        current_date = datetime.datetime.now()
        week_day = current_date.weekday()
        current_time = current_date.time()
        
        active = getattr(login_interval, "day%d_active" % week_day)
        if active:
            return self._check_day_before(login_interval, current_time, week_day)
        allday = getattr(login_interval, "day%d_allday" % week_day)
        if allday:
            return True

        start = getattr(login_interval, "day%d_start" % week_day)
        end = getattr(login_interval, "day%d_end" % week_day)
        if start <= current_time < end:
            return True
        elif start > end and start <= current_time:
            return True
        else:
            return self._check_day_before(login_interval, current_time, week_day)
        
        return False 
                    
        