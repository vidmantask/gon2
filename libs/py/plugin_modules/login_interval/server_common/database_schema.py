from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api



dbapi = schema_api.SchemaFactory.get_creator("plugin_login_interval")

table_login_interval = dbapi.create_table("login_interval",
                                         schema_api.Column('name', schema_api.String(256), nullable=False),
                                         schema_api.Column('description', schema_api.Text()),

                                         schema_api.Column('day0_active', schema_api.Boolean()),
                                         schema_api.Column('day1_active', schema_api.Boolean()),
                                         schema_api.Column('day2_active', schema_api.Boolean()),
                                         schema_api.Column('day3_active', schema_api.Boolean()),
                                         schema_api.Column('day4_active', schema_api.Boolean()),
                                         schema_api.Column('day5_active', schema_api.Boolean()),
                                         schema_api.Column('day6_active', schema_api.Boolean()),

                                         schema_api.Column('day0_allday', schema_api.Boolean()),
                                         schema_api.Column('day0_start', schema_api.Time()),
                                         schema_api.Column('day0_end', schema_api.Time()),
                                         schema_api.Column('day1_allday', schema_api.Boolean()),
                                         schema_api.Column('day1_start', schema_api.Time()),
                                         schema_api.Column('day1_end', schema_api.Time()),
                                         schema_api.Column('day2_allday', schema_api.Boolean()),
                                         schema_api.Column('day2_start', schema_api.Time()),
                                         schema_api.Column('day2_end', schema_api.Time()),
                                         schema_api.Column('day3_allday', schema_api.Boolean()),
                                         schema_api.Column('day3_start', schema_api.Time()),
                                         schema_api.Column('day3_end', schema_api.Time()),
                                         schema_api.Column('day4_allday', schema_api.Boolean()),
                                         schema_api.Column('day4_start', schema_api.Time()),
                                         schema_api.Column('day4_end', schema_api.Time()),
                                         schema_api.Column('day5_allday', schema_api.Boolean()),
                                         schema_api.Column('day5_start', schema_api.Time()),
                                         schema_api.Column('day5_end', schema_api.Time()),
                                         schema_api.Column('day6_allday', schema_api.Boolean()),
                                         schema_api.Column('day6_start', schema_api.Time()),
                                         schema_api.Column('day6_end', schema_api.Time()),
                                         )


schema_api.SchemaFactory.register_creator(dbapi)

class LoginInterval(object):
    pass


schema_api.mapper(LoginInterval, table_login_interval)

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)

