"""
ClientOk plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint

from components.communication import message

from plugin_modules import client_ok
from plugin_types.server_gateway import plugin_type_tag


class PluginTag(plugin_type_tag.PluginTypeTag):
    TAG_if_platform_is = plugin_type_tag.Tag('client_ok', 'IfPlatformIs')
    TAG_CLIENT_OK = plugin_type_tag.Tag(None, 'CLIENTOK')
    

    """
    Runtime server part of ClientOk plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_tag.PluginTypeTag.__init__(self, async_service, checkpoint_handler, 'client_ok', license_handler, session_info)
        self._remote_platform = None

    def client_connected(self):
        self.tunnelendpoint_remote('remote_get_platform')

    def generate_tags(self, tags):
        if self._remote_platform == None:
            return []

        tag_if_platform_is_list = PluginTag.TAG_if_platform_is.in_tags_get(tags)
        
        for tag_if_platform_is in tag_if_platform_is_list:
            assert isinstance(tag_if_platform_is.tag_args, basestring), "Invalid parameter(s) for %s: '%s'" % (PluginTag.TAG_if_platform_is.tag_name, tag_if_platform_is.tag_args)
            
            remote_platform = set(self._remote_platform.lower().split(','))
            
            if self._remote_platform == 'win32' and tag_if_platform_is.tag_args.lower() == 'win': 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'darwin' and tag_if_platform_is.tag_args.lower() == 'mac': 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'linux2' and tag_if_platform_is.tag_args.lower() == 'linux': 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'g-on-os' and tag_if_platform_is.tag_args.lower() in ['linux','linux-g-on-os']: 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'iPhone' and tag_if_platform_is.tag_args.lower() in ['ios', 'ios-iphone', 'mobile', 'mobile-normal', 'ios-normal']: 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'iPod touch' and tag_if_platform_is.tag_args.lower() in ['ios', 'ios-ipod', 'mobile', 'mobile-normal', 'ios-normal']: 
                return [PluginTag.TAG_CLIENT_OK]
            elif self._remote_platform == 'iPad' and tag_if_platform_is.tag_args.lower() in ['ios', 'ios-ipad', 'mobile', 'mobile-xlarge', 'ios-xlarge']: 
                return [PluginTag.TAG_CLIENT_OK]


            elif 'dme' in remote_platform and 'iphone' in remote_platform:
                ok_tags = ['dme-ios', 'dme-ios-iphone', 'dme-mobile', 'dme-mobile-normal', 'dme-ios-normal']
                if tag_if_platform_is.tag_args.lower() in ok_tags:
                    return [PluginTag.TAG_CLIENT_OK]

            elif 'dme' in remote_platform and 'ipod touch' in remote_platform:
                ok_tags = ['dme-ios', 'dme-ios-ipod', 'dme-mobile', 'dme-mobile-normal', 'dme-ios-normal']
                if tag_if_platform_is.tag_args.lower() in ok_tags:
                    return [PluginTag.TAG_CLIENT_OK]

            elif 'dme' in remote_platform and 'ipad' in remote_platform:
                ok_tags = ['dme-ios', 'dme-ios-ipad', 'dme-mobile', 'dme-mobile-xlarge', 'dme-ios-xlarge']
                if tag_if_platform_is.tag_args.lower() in ok_tags:
                    return [PluginTag.TAG_CLIENT_OK]
            
            elif 'dme' in remote_platform and 'android' in remote_platform :
                ok_tags = ['dme-android', 'dme-mobile']
                if 'screen-small' in remote_platform:
                    ok_tags.append('dme-android-small')
                    ok_tags.append('dme-mobile-small')
                if 'screen-normal' in remote_platform:
                    ok_tags.append('dme-android-normal')
                    ok_tags.append('dme-mobile-normal')
                if 'screen-large' in remote_platform:
                    ok_tags.append('dme-android-large')
                    ok_tags.append('dme-mobile-large')
                if 'screen-xlarge' in remote_platform:
                    ok_tags.append('dme-android-xlarge')
                    ok_tags.append('dme-mobile-xlarge')
                if tag_if_platform_is.tag_args.lower() in ok_tags:
                    return [PluginTag.TAG_CLIENT_OK]
                
            elif 'android' in remote_platform:
                ok_tags = ['android', 'mobile']
                if 'screen-small' in remote_platform:
                    ok_tags.append('android-small')
                    ok_tags.append('mobile-small')
                if 'screen-normal' in remote_platform:
                    ok_tags.append('android-normal')
                    ok_tags.append('mobile-normal')
                if 'screen-large' in remote_platform:
                    ok_tags.append('android-large')
                    ok_tags.append('mobile-large')
                if 'screen-xlarge' in remote_platform:
                    ok_tags.append('android-xlarge')
                    ok_tags.append('mobile-xlarge')
                if tag_if_platform_is.tag_args.lower() in ok_tags:
                    return [PluginTag.TAG_CLIENT_OK]

        return []

    def get_argument_taginfos(self):
        return []
    
    def remote_get_platform_response(self, platform):
        self._remote_platform = platform
        self.notify_tag_changed()
        

