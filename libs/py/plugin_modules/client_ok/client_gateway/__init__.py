"""
ClientOk plugin for Gateway Client
"""
from __future__ import with_statement


import sys

import lib.appl.gon_os
from components.communication import message

from plugin_modules import client_ok
from plugin_types.client_gateway import plugin_type_tag


class PluginTag(plugin_type_tag.PluginTypeTag):
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_tag.PluginTypeTag.__init__(self, async_service, checkpoint_handler, u'client_ok', user_interface, additional_device_roots)
        
    def remote_get_platform(self):
        if sys.platform == 'linux2' and lib.appl.gon_os.is_gon_os():
            self.tunnelendpoint_remote('remote_get_platform_response', platform="g-on-os")
        else:
            self.tunnelendpoint_remote('remote_get_platform_response', platform=sys.platform)
