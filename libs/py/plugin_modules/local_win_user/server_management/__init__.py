# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys

if sys.platform == 'win32':
    
    import os.path
    
    import lib.checkpoint
    
    from plugin_modules.local_win_user.server_common.config import Config
    from plugin_modules.local_win_user.server_common.login_module import LoginModule
    
    from plugin_types.server_management import plugin_type_user
    from plugin_types.server_management import plugin_type_config
    from plugin_types.server_management import plugin_type_access_notification
    from plugin_types.server_management import plugin_type_element
   
    import win32com.client
    import pythoncom
    import win32security
    import win32netcon
    import win32net
    
    
    
    config = Config(os.path.abspath(os.path.dirname(__file__)))    
    
    
    
    
    class PluginAuthentication(plugin_type_user.PluginTypeUser):
        
        def __init__(self, checkpoint_handler, database, license_handler, server_name="localhost"):
            name = "local_win_user_%s" % (server_name.replace(".","_"))
            plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, database, license_handler, name)
            
            if not server_name or server_name=="localhost":
                self.server_name = None
            else:
                self.server_name = server_name

            self.options = config.server_options.get(server_name)
            
            self.login_postfix = self.options.get(Config.LOGIN_SUFFIX, "local")
            
            self._login_module = LoginModule(self.checkpoint_handler, self.plugin_name, server_name, self.login_postfix)

            
        element_types = ["user", "group"]
    
        @classmethod
        def generator(cls, checkpoint_handler, database, license_handler):
            plugins = {}
            if config.enabled and config.servers:
                try:
                    server_name = config.servers[0]
                    plugin = PluginAuthentication(checkpoint_handler, database, license_handler, server_name=server_name)
                    plugins[plugin.plugin_name] = plugin
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    checkpoint_handler.CheckpointException("load_local_win_user_module.error", "local_win_user", lib.checkpoint.ERROR, etype, evalue, etrace)
            return plugins
#            
            
        def user_exists(self, user_id):
            user_obj = self.get_user_by_id(user_id)
            if user_obj:
                return True
            return False
            
            

        def get_user_by_id(self, user_id):
            res = 1
            user_info = None
            try:
                while res:
                    users, total, res = win32net.NetUserEnum(self.server_name, 3)
                    for user in users:
                        if str(user.get("user_id")) == str(user_id):
                            user_info = user
                            res = 0
                            break
            except win32net.error as e:
                etype, evalue, etrace = sys.exc_info()
                self.checkpoint_handler.CheckpointException("fetch_users.error", self.server_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            
            return user_info

        def get_login_and_name_from_id(self, user_id, internal_user_login):
            if internal_user_login:
                user_info = win32net.NetUserGetInfo(self.server_name, internal_user_login, 3)
            else:
                user_info = None
            if user_info and str(user_info.get("user_id")) == str(user_id):
                user_login = "%s@%s" % (internal_user_login, self.login_postfix)
                user_name = internal_user_login
            else:
                user_info = self.get_user_by_id(user_id) 
                if user_info:    
                    internal_user_login = user_info.get("name") 
                    user_login = "%s@%s" % (internal_user_login, self.login_postfix)
                    user_name = internal_user_login
                else:
                    self.checkpoint_handler.Checkpoint('get_login_and_name_from_id', self.plugin_name, lib.checkpoint.ERROR, message='No login name found for user', user_id=user_id)
                    raise Exception("Found no login name for user with RID '%s'" % (user_id))
                    
            
            return internal_user_login, user_login, user_name
                
    
        def fetch_users(self, search_filter=None):
            """Get list of AD group names suitable as parameteres to is_logged_in_name"""

    
            res=1
            level = 3
            if search_filter:
                # remove '*'
                search_filter = search_filter[1:-1].lower() 
            
            try:
                while res:
#                    (users,total,res) = win32net.NetUserEnum(self.server_name, level, win32netcon.FILTER_NORMAL_ACCOUNT, res, win32netcon.MAX_PREFERRED_LENGTH)
                    (users,total,res) = win32net.NetUserEnum(self.server_name, level)
                    for user in users:
                         name = user['name']
                         info = user.get("full_name", "")
                         if search_filter:
                             if not search_filter in name.lower() and not search_filter in info.lower():
                                 continue
                         rid = str(user['user_id'])
                         flags = user['flags']
                         if flags & win32netcon.UF_ACCOUNTDISABLE:
                             continue
                         print user
#                         info_dict = win32net.NetUserGetInfo(self.server_name, name, 3)
#                         print info_dict
#                         login_name = info_dict['full_name']
                         login_name = "%s@%s" % (name, self.login_postfix)
                         new_user = self.create_element(rid, login_name, info)
                         yield new_user
            except win32net.error, e:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("fetch_users.error", self.server_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                print e
            
    
                            
    
        def fetch_groups(self, search_filter=None):
            """Get list of AD group names suitable as parameteres to is_logged_in_member"""
            res=1
            level = 1
            if search_filter:
                # remove '*'
                search_filter = search_filter[1:-1].lower() 
            try:
                while res:
#                    (groups,total,res) = win32net.NetgroupEnum(self.server_name, level, win32netcon.FILTER_NORMAL_ACCOUNT, res, win32netcon.MAX_PREFERRED_LENGTH)
                    (groups,total,res) = win32net.NetLocalGroupEnum(self.server_name, level)
                    for group in groups:
                         name = group['name']
                         info = group.get("comment", "")
                         if search_filter:
                             if not search_filter in name.lower() and not search_filter in info.lower():
                                 continue
#                         info_dict = win32net.NetgroupGetInfo(self.server_name, name, 3)
#                         print info_dict
#                         login_name = info_dict['full_name']
                         new_group = self.create_element(name, "%s (%s)" % (name, self.login_postfix), info)
                         yield new_group
            except win32net.error, e:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("fetch_groups.error", self.server_name, lib.checkpoint.ERROR, etype, evalue, etrace)
    
                        
    
                    
    
        def get_group_members(self, group_id):
            res=1
            level = 1

            try:
                users = set()
                while res:
                    (members,total,res) = win32net.NetLocalGroupGetMembers(self.server_name, group_id, level)
                    for member in members:
                        sid_type = member.get("sidusage")
                        if sid_type == win32security.SidTypeGroup:
                            sub_group_name = member.get("name") 
                            users.update(self.get_group_members(sub_group_name))
                        elif sid_type == win32security.SidTypeUser:
                            user_name = member.get("name")
                            user_sid = win32security.ConvertSidToStringSid(member.get("sid"))
                            user_rid = user_sid.rpartition("-")[-1]
                            users.add((user_rid, user_name))
            except win32net.error, e:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("fetch_groups.error", self.server_name, lib.checkpoint.ERROR, etype, evalue, etrace)
    
            
            return users
            
    
    
    
        def get_id_column(self):
            return plugin_type_config.Column(self.plugin_name, 'id',   plugin_type_config.Column.TYPE_STRING, 'RID', hidden=True, is_id=True)
    
    
    
        def get_email(self, user_sid):
            raise NotImplementedError()
    

        def get_login_module(self):
            return self._login_module
        
    
        def get_plugin_title(self):
            return self.login_postfix
