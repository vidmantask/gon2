"""
Local Windows User plugin for Gateway Server
"""
from __future__ import with_statement
import sys

if sys.platform == 'win32':

    import os.path
    import datetime
    
    import lib.checkpoint
    
    
    from components.communication import message
    from plugin_modules.local_win_user.server_common.config import Config
    from plugin_modules.local_win_user.server_common.login_module import LoginModule
    
    from plugin_types.server_gateway import plugin_type_traffic
    from plugin_types.server_gateway import plugin_type_user
    
    
    config = Config(os.path.abspath(os.path.dirname(__file__)))    
    
    
    
    
    
    class PluginAuthentication(plugin_type_traffic.PluginTypeTraffic, plugin_type_user.PluginTypeUser, LoginModule):
        """
        Server part of AD module, plugging into server
        """
        def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, server_name="."):
            plugin_name = "local_win_user_%s" % (server_name.replace(".","_"))
            plugin_type_traffic.PluginTypeTraffic.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
            plugin_type_user.PluginTypeUser.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info, database, management_message_session, access_log_server_session)
            
            self.options = config.server_options.get(server_name)
            self.login_postfix = self.options.get(Config.LOGIN_SUFFIX, "local")

            LoginModule.__init__(self, checkpoint_handler, plugin_name, server_name, self.login_postfix)

            
            if self.options.has_key("password_expiry_warning"):
                self.set_password_warning_days(self.options.get("password_expiry_warning"))

            if self.options.has_key(Config.KEY_MAX_PASSWORD_AGE):
                self._max_password_age = int(self.options.get(Config.KEY_MAX_PASSWORD_AGE))
                
            self.change_password_disabled = self.options.get("change_password_disabled", False)                
            
            
        @classmethod
        def generator(cls, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
            plugins = {}
            if config.enabled and config.servers:
                try:
                    server_name = config.servers[0]
                    plugin = PluginAuthentication(async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, server_name=server_name)
                    plugins[plugin.plugin_name] = plugin
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    checkpoint_handler.CheckpointException("load_local_win_user_module.error", "local_win_user", lib.checkpoint.ERROR, etype, evalue, etrace)
            return plugins
            
    
        def start(self):
            """
            Initiates the login process
            """
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::start", self.plugin_name, lib.checkpoint.DEBUG) as cps:
                self.request_login()
                self.set_started()
    
    
    
        
        def login_cancelled(self):
            self.authenticated = False
            self.set_ready()
            
            
    
        def receive_login(self, login, password, internal_user_login=None):
            LoginModule.receive_login(self, login, password, internal_user_login)
            if not self.must_change_password:
                self.set_ready()
                    
    
        def change_password_cancelled(self):
            if self.must_change_password:
                self.authenticated = False
                self.set_ready()
        
            
        def get_attribute(self, attribute_name):
            return None
        
        
        
        
