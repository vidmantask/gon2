"""
Server config specification plugin  

"""
from __future__ import with_statement

import os.path
import shutil


import lib.checkpoint
import lib.utility.support_file_filter as support_file_filter



import plugin_types.server_config.plugin_type_user_config_specification as plugin_type_user_config_specification
import plugin_modules.local_win_user
import plugin_modules.local_win_user.server_common.config as config

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec


class ConfigSpecificationPluginLocalWinUser(plugin_type_user_config_specification.PluginTypeUserConfigSpecification):
    ID = 'plugin_local_win_user'
    TITLE = 'Local Windows User Plugin Configuration' 
    DESCRIPTION = 'Local Windows User Plugin Configuration' 
    
    NAME = 'Local Windows User'

    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_user_config_specification.PluginTypeUserConfigSpecification.__init__(self, checkpoint_handler, server_configuration_all, __name__, "local_win_user")
        self.server_configuration_all = server_configuration_all
        self.management_config = None
        self.enabled = False
        self.template_list = []

    def get_path_management_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_management_plugin_root(server_configuration_all, 'local_win_user')

    def get_path_gateway_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_gateway_plugin_root(server_configuration_all, 'local_win_user')

    def get_version(self):
        return lib.version.Version.create_from_encode(plugin_modules.local_win_user.plugin_module_version)
    
    def get_type_and_priority(self):
        return ("User", 3)
    

    def get_config_specification(self):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        config_spec = ConfigTemplateSpec()
        config_spec.init(ConfigSpecificationPluginLocalWinUser.ID, ConfigSpecificationPluginLocalWinUser.TITLE, ConfigSpecificationPluginLocalWinUser.DESCRIPTION)

        management_config = config.Config(self.get_path_management_ini())
        gateway_config = config.Config(self.get_path_gateway_ini())

        existing_directory = dict()
        if management_config.servers:
            existing_directory = management_config.server_options.get(management_config.servers[0])
        if not existing_directory and gateway_config.servers:
            existing_directory = gateway_config.server_options.get(gateway_config.servers[0])

        
        config_spec.add_field(config.Config.KEY_ENABLED_NAME, 'Enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False)
        config_spec.add_field(config.Config.KEY_MAX_PASSWORD_AGE, 'Maximum Password Age (days)', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False)
        config_spec.add_field(config.Config.KEY_NAME, 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, value="localhost")
        config_spec.add_field(config.Config.LOGIN_SUFFIX, 'Full login suffix', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, mandatory=True)

        enabled = management_config.enabled and gateway_config.enabled
        config_spec.set_value(config.Config.KEY_ENABLED_NAME, enabled)
        config_spec.set_value(config.Config.KEY_MAX_PASSWORD_AGE, 42)
        config_spec.set_value(config.Config.LOGIN_SUFFIX, "local")

        if existing_directory:
            config_spec.set_values_from_dict(existing_directory)
        
       
        return config_spec.get_template()
        

    def save(self, template):
        """
        If the config_specification does not match the current instance then None should be returned
        else should a instance of components.server_config_ws.ws.server_config_ws_services_types.ns0.SaveConfigSpecificationResponseType_Def(None).pyclass() be returned.
        """
        if template.get_attribute_name() != ConfigSpecificationPluginLocalWinUser.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        
        other_values = config_spec.get_field_value_dict()
        enabled = other_values.pop(config.Config.KEY_ENABLED_NAME)
        
        max_password_age = other_values.get(config.Config.KEY_MAX_PASSWORD_AGE)
        if max_password_age is None:
            if enabled:
                raise Exception("Error : Maximum Password Age must be set")
            else:
                other_values[config.Config.KEY_MAX_PASSWORD_AGE] = 0

        if not other_values.get(config.Config.LOGIN_SUFFIX):
            other_values[config.Config.LOGIN_SUFFIX] = "local"
        
        config.Config.write(self.get_path_management_ini(), enabled, [other_values])
        config.Config.write(self.get_path_gateway_ini(), enabled, [other_values])
        return self.create_save_response(True)


    def check_template(self, template):
        return self.NAME
    
    def get_title(self):
        return ""
    

    def get_config_specifications(self, parent_module_id):
        
        config_spec = ConfigTemplateSpec()
        config_spec.init(ConfigSpecificationPluginLocalWinUser.ID, ConfigSpecificationPluginLocalWinUser.TITLE, ConfigSpecificationPluginLocalWinUser.DESCRIPTION)

        management_config = self.get_management_config()

        existing_directory = dict()
        if management_config.servers:
            existing_directory = management_config.server_options.get(management_config.servers[0])

        
        config_spec.add_field(config.Config.KEY_MAX_PASSWORD_AGE, 'Maximum Password Age (days)', ConfigTemplateSpec.VALUE_TYPE_INTEGER, advanced=False)
        config_spec.add_field(config.Config.KEY_NAME, 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, value="localhost")
        config_spec.add_field(config.Config.LOGIN_SUFFIX, 'Full login suffix', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=True, mandatory=True)

        config_spec.set_value(config.Config.KEY_MAX_PASSWORD_AGE, 42)
        config_spec.set_value(config.Config.LOGIN_SUFFIX, "local")

        config_spec.set_values_from_dict(existing_directory)
        
        config_dict = dict()
        config_dict[self.NAME] = config_spec.get_template()

        return config_dict
    
    def get_create_config_specification(self, parent_module_id):
        return None
    
    def get_management_config(self):
        if not self.management_config:
            self.management_config = config.Config(self.get_path_management_ini())
        return self.management_config

    def is_singleton(self):
        return True

    def is_enabled(self):
        management_config = self.get_management_config()
        return management_config.enabled
    
    def save_data(self, enabled, template_list):
        self.enabled = enabled
        self.template_list = template_list
    
    def finalize(self):
        config_spec = ConfigTemplateSpec(self.template_list[0])
        values = config_spec.get_field_value_dict()

        config.Config.write(self.get_path_management_ini(), self.enabled, [values])
        config.Config.write(self.get_path_gateway_ini(), self.enabled, [values])
        
        self.management_config = None
        


    def generate_support_package(self, support_package_root):
        """
        This method is called with a folder where the plugin can dump files/info. The information dumped will be packaed to a support package for giritech.
        """
        support_log_filename_abs = os.path.join(support_package_root, 'local_win_user.txt') 
        support_log = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(support_log_filename_abs, False), None)

        support_log.Checkpoint('local_win_user.generate_support_package', plugin_modules.local_win_user.MODULE_ID, lib.checkpoint.DEBUG)
        self._copy_ini_files(support_log, support_package_root, remove_sensitive_info=True)


    def backup(self, backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all):
        """
        This method is called with a folder where the plugin can backup files/info. The information dumped will be included in the backup.
        """
        self._copy_ini_files(backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all)

    def _copy_ini_files(self, logger, dest_root, cb_config_backup=None, alternative_server_configuration_all=None, remove_sensitive_info=False):
        ini_filename_src = os.path.join(self.get_path_management_ini(alternative_server_configuration_all), config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'management_%s' % config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.local_win_user.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("Local Win User Plugin : ini file not found, see log file.")

        ini_filename_src = os.path.join(self.get_path_gateway_ini(alternative_server_configuration_all), config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'gateway_%s' % config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst) 
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.local_win_user.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("AD-Plugin ini file not found, see log file.")


    def restore(self, restore_log, restore_package_plugin_root, cb_restore):
        ini_filename_src = os.path.join(restore_package_plugin_root, 'management_%s' % config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(self.get_path_management_ini(), config.Config.CONFIG_FILENAME)
            dirname = os.path.dirname(ini_filename_dst)
            if not os.path.exists(dirname): 
                os.makedirs(os.path.dirname(ini_filename_dst))
            shutil.copy(ini_filename_src, ini_filename_dst)
        
        ini_filename_src = os.path.join(restore_package_plugin_root, 'gateway_%s' % config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(self.get_path_gateway_ini(), config.Config.CONFIG_FILENAME) 
            dirname = os.path.dirname(ini_filename_dst)
            if not os.path.exists(dirname): 
                os.makedirs(os.path.dirname(ini_filename_dst))
            shutil.copy(ini_filename_src, ini_filename_dst)
