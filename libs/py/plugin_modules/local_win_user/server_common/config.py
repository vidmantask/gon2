import os.path
import ConfigParser

class Config(object):
    CONFIG_FILENAME = 'config.ini'

    SECTION_SERVER_PREFIX = 'server'
    KEY_NAME = 'name'
    KEY_MAX_PASSWORD_AGE = 'maximum_password_age'
    LOGIN_SUFFIX = 'login_suffix'

    SECTION_PLUGIN = 'plugin'
    KEY_ENABLED_NAME = 'enabled'
    
    def __init__(self, path):
        self.servers = None
        self.server_options = dict()
        self.path = path
        
        self.enabled = False
        
        self._config_ini = ConfigParser.ConfigParser()
        self.load()
        
    def load(self):
        filename = os.path.join(self.path, Config.CONFIG_FILENAME)
        if os.path.exists(filename):
            self._config_ini.read(filename)
            try:
                self.enabled = self._config_ini.getboolean(self.SECTION_PLUGIN, self.KEY_ENABLED_NAME)
            except:
                pass
            
            try:
                self.check_and_update_server_sections()
                
            except Exception, e:
                if self.enabled:
                    raise e
                   
        #self.authorization_domain = self._config_ini.get('authorization', 'domain')

    def check_and_update_server_sections(self):
        if self.servers is None:
            self.servers = []
            server_sections = []
            sections = self._config_ini.sections()
            sections.sort()
            for section in sections:
                if section.lower().startswith(Config.SECTION_SERVER_PREFIX):
                    server_sections.append(section)
            
            for section in server_sections:
                try:
                    name = self._config_ini.get(section, Config.KEY_NAME)
                except:
                    name = "localhost"
                    
                self.servers.append(name)
                options = self._config_ini.options(section)
                config_dict = dict()
                for option in options:
                    config_dict[option] = self._config_ini.get(section, option)

                login_suffix = config_dict.get(Config.LOGIN_SUFFIX)
                if not login_suffix:
                    config_dict[Config.LOGIN_SUFFIX] = "local"
                
                self.server_options[name] = config_dict
                break 


    @classmethod
    def convert_to_list(cls, option_str):
        return [s.strip() for s in option_str.split(",") if s]        

    @classmethod
    def write(cls, path, enabled, servers):
        config_ini = ConfigParser.ConfigParser()
        for server in servers:
            server_name = server.get(Config.KEY_NAME)
            if server_name:
                section_name = '%s %s' %(Config.SECTION_SERVER_PREFIX, server_name)
                config_ini.add_section(section_name)
                for (key,value) in server.items():
                    config_ini.set(section_name, key, value)
            
        
        if not os.path.exists(path):
            os.makedirs(path)
        
        config_filenameabs = os.path.join(path, Config.CONFIG_FILENAME)

        found_plugin_section = False
        if os.path.exists(config_filenameabs):
            config_ini_existing = ConfigParser.ConfigParser()
            config_ini_existing.read(config_filenameabs)
        
            for section in config_ini_existing.sections():
                if not section.startswith(Config.SECTION_SERVER_PREFIX):
                    config_ini.add_section(section)
                    for option in config_ini_existing.options(section):
                        config_ini.set(section, option, config_ini_existing.get(section, option))
                    if section==Config.SECTION_PLUGIN:
                        found_plugin_section = True
                        config_ini.set(section, Config.KEY_ENABLED_NAME, enabled)
                elif config_ini.has_section(section):
                    for option in config_ini_existing.options(section):
                        if not config_ini.has_option(section, option):
                            config_ini.set(section, option, config_ini_existing.get(section, option))
        
        if not found_plugin_section:
            config_ini.add_section(cls.SECTION_PLUGIN)
            config_ini.set(cls.SECTION_PLUGIN, Config.KEY_ENABLED_NAME, enabled)
            
        
        config_file = open(config_filenameabs, 'w')
        config_ini.write(config_file)
        config_file.close()


if __name__ == '__main__':
    import tempfile
    import shutil
    
    temp_folder = tempfile.mkdtemp()
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'w')
    test_init_file.write('[access_notification]\n') 
    test_init_file.write('enabled = True\n') 
    test_init_file.write('xxx = 10\n')
    test_init_file.write('[domain 1]\n') 
    test_init_file.write('dns name = domain_xxxxx\n') 
    test_init_file.close()
    

    Config.write(temp_folder, ['domain_a', 'domain_b'])
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'r')
    print test_init_file.read()
    
    test_init_file.close()
    shutil.rmtree(temp_folder)
