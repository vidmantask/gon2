from __future__ import with_statement
import sys

if sys.platform == 'win32':

    import os.path
    import datetime
    
    import lib.checkpoint
    
    
    import pywintypes
    import winerror
    import win32security
    import win32api
    import win32net
    import win32netcon
    
    from plugin_types.common.plugin_type_user import LoginModuleBase

    
    class LoginModule(LoginModuleBase):
        """
        Server part of AD module, plugging into server
        """
        def __init__(self, checkpoint_handler, plugin_name, server_name=".", login_postfix="local"):
            LoginModuleBase.__init__(self)
            self.server_name = server_name
            self.login = ''
            self.internal_user_login = None
            self.password = ''
            self.user_RID = None
            self.user_groups = []
            self._max_password_age = 42
            self.checkpoint_handler = checkpoint_handler
            self.plugin_name = plugin_name

            if not server_name or server_name=="localhost":
                self.server_name = None
            else:
                self.server_name = server_name
            
            self.login_postfix = login_postfix
            self.change_password_disabled = True                
            
            
    
        def is_logged_in_name(self, login):
            """
            True if the user is logged in as login
            """
            user_rid = login[0]
            if self.authenticated and user_rid:
                return self.user_RID == user_rid
            return False
    
        def is_logged_in_member(self, group):
            """
            True if the user is logged in as member of group
            """
            group_name = group[0]
            if self.authenticated and group_name in self.user_groups:
                return True
            return False
    
        def get_group_ids_for_logged_in_user(self):
            if self.authenticated:
                return self.user_groups
            return None
    
        def is_authenticated(self):
            return self.authenticated
        
            
    
        def receive_login(self, login, password, internal_user_login=None):
            
            """
            Recieves result from login and password prompt and continues the authentication proces
            """
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_login", self.plugin_name, lib.checkpoint.DEBUG, login=login):
                self.authenticated = False
                self.login = login
    
                if self.authenticated:
                    return
                if self._CheckPassword(self.login, password, internal_user_login):
                    self.checkpoint_handler.Checkpoint("PluginAuthentication::receive_login.entered", self.plugin_name, lib.checkpoint.DEBUG, login=login, user_sid=self.user_RID)
                    self.password = password  # FIXME: Security issue???
                    self.authenticated = True
                #self.tunnelendpoint_send(message.Message('show_messagebox', msg='Welcome, %s!' % self.login))
                    
        def get_normalised_login(self, user_login):
            if '@' in user_login:
                return user_login
            else:
                return "%s@%s" % (user_login, self.login_postfix)
        
        def lookup_user(self, user_login):
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::lookup_user", self.plugin_name, lib.checkpoint.DEBUG, login=user_login):
                try:
                    if user_login.lower().endswith("@" + self.login_postfix):
                        user_login = user_login.rsplit("@")[0]
                    user_info = win32net.NetUserGetInfo(self.server_name, user_login, 1)
                except:
                    return None
                
                return user_info.get('name')

    
        def _CheckPassword(self, login, password, internal_user_login=None):
            """
            Private function for validating username and password
            """
            
            if not login:
                self.checkpoint_handler.Checkpoint("login_failed", self.plugin_name, lib.checkpoint.WARNING, msg="No login name specified")
                return False
            if not internal_user_login:
                index = login.find("@")
                if index < 0:
                    internal_user_login = login
                else:
                    internal_user_login = login[0:index]
                    
                
            handle = None
            try:
                handle = win32security.LogonUser(internal_user_login, self.server_name, password, win32security.LOGON32_LOGON_NETWORK, win32security.LOGON32_PROVIDER_DEFAULT)
                user_info = win32security.GetTokenInformation(handle, win32security.TokenUser)
                user_SID = win32security.ConvertSidToStringSid(user_info[0])
                self.user_RID = user_SID.rpartition("-")[2]
                self.internal_user_login = internal_user_login
                
                
                #This simple method doesn't work properly because it apparently mixes local and global group memberships
                #self.group_SIDs = [ win32security.ConvertSidToStringSid(sid[0]) for sid in win32security.GetTokenInformation(handle, win32security.TokenGroups)]
                user_object = None
                try:
                    user_object = win32net.NetUserGetInfo(self.server_name, internal_user_login, 3)
                    if str(user_object.get('user_id')) != str(self.user_RID):
                        found_user_id = user_object.get('user_id')
                        user_object = None
                        raise Exception("User id from login and NetUserGetInfo does not match. login='%s', login user id='%s', NetUserGetInfo user id='%s'" % (internal_user_login, self.user_RID, found_user_id))
                    self.user_groups = win32net.NetUserGetLocalGroups(self.server_name, internal_user_login) 
            
                except:
                    self.checkpoint_handler.Checkpoint("login", self.plugin_name, lib.checkpoint.ERROR, msg="Unable to get group membership information from AD")
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("login", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                
                if not self.change_password_disabled and user_object:
                    self._check_password_expiration(user_object)
                    
                return True
            except pywintypes.error, (error_code, module, message):
#                error_code = win32api.GetLastError()
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("login_failed", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                

                # check if password has expired and/or needs to be changed
                if not self.change_password_disabled and (error_code==winerror.ERROR_PASSWORD_EXPIRED or error_code==winerror.ERROR_PASSWORD_MUST_CHANGE):
                    self.password = password
                    self.must_change_password = True
                    self.internal_user_login = internal_user_login
                else:
                    self.login_error_code = error_code
                    self.login_error_message = message
                    
                # Because of the sheer number of Windows-specific errors that can
                # occur here, we have to assume any of them mean that the
                # credentials were not valid.
                
                return False
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("login", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                return False
            finally:
                if handle:
                    handle.Close()  
                    
        def receive_changed_password(self, old_password, new_password):
            if self.change_password_disabled:
                return "It is not possible to change password in this configuration"
            try:
                server_name = self.server_name if self.server_name else "."
                win32net.NetUserChangePassword(server_name, self.internal_user_login, old_password, new_password)
                self.password = new_password
                self.days_to_password_expiration = None
                if self.must_change_password:
                    self.must_change_password = False
                    return self.receive_login(self.login, new_password)
                return None
            except pywintypes.error, (error_code, module, message):
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("change_password_failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                
            return message
    
        
        def _check_password_expiration(self, user_object):
            if self.change_password_disabled or self._password_expiration_warning_days <= 0 or self._max_password_age <= 0:
                return
            
            try:
                
                flags = user_object.get("flags")
                
                #Check if password expires
                never_expires = flags & win32netcon.UF_DONT_EXPIRE_PASSWD
                if never_expires:
                    return 
                
                #Check if password change is possible
                cannot_change = flags & win32netcon.UF_PASSWD_CANT_CHANGE
                if cannot_change:
                    return 
                

                #Determine When Password Was Changed
                password_age_in_seconds = user_object.get("password_age")
                if not password_age_in_seconds:
                    return 
                
                password_age_in_days = password_age_in_seconds / 86400
                    
                self.days_to_password_expiration = self._max_password_age - password_age_in_days                     
                    
                    
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("check_password_expiration", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                
            
        def get_current_user_id(self):
            if self.authenticated:
                return self.user_RID
        
