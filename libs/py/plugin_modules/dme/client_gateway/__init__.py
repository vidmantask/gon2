"""
Computer Token Gateway Client
"""
from __future__ import with_statement

import tempfile
import sys
import subprocess
import StringIO

import os
import os.path
import uuid

import lib.checkpoint
import lib.utility

from components.communication import message

from plugin_types.client_gateway import plugin_type_auth



class DMETokenAuth(plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'dme'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, DMETokenAuth.PLUGIN_NAME, user_interface, additional_device_roots)

    def get_dme_values(self):
        self.tunnelendpoint_send(message.Message('get_dme_values_response', values=None))

#        # get values from dme client
#        values = dict()
#        values["username"] = "GON"
#        values["password"] = "gon"
#        values["terminal_id"] = "gondevice"
#        values["device_signature"] = "abc"
#        values["device_time"] = "today"
#        self.tunnelendpoint_send(message.Message('get_dme_values_response', values=values))

