"""
Computer Token plugin for Gateway Server
"""
from __future__ import with_statement
import sys
import urllib2
import base64

import lib.checkpoint
import lib.version

from components.communication import message

from plugin_modules import soft_token
from plugin_types.common import plugin_type_token as database_model
from plugin_types.server_gateway import plugin_type_auth
from plugin_types.server_gateway import plugin_type_traffic

from components.database.server_common.database_api import *
from plugin_modules.dme.server_common.dme_config import Config


config = Config(os.path.abspath(os.path.dirname(__file__)))


#
# Hack to control ssl_version
#
# This was introdiced due to SSL vulnerability "Poodle"
#
import httplib
from httplib import HTTPConnection, HTTPS_PORT
import ssl
import socket
class HTTPSConnectionTLSv1_2(HTTPConnection):
    "This class allows communication via SSL."
    default_port = HTTPS_PORT

    def __init__(self, host, port=None, key_file=None, cert_file=None,
            strict=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT,
            source_address=None):
        HTTPConnection.__init__(self, host, port, strict, timeout,
                source_address)
        self.key_file = key_file
        self.cert_file = cert_file

    def connect(self):
        "Connect to a host on a given (SSL) port."
        sock = socket.create_connection((self.host, self.port),
                self.timeout, self.source_address)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        # this is the only line we modified from the httplib.py file
        # we added the ssl_version variable
        self.sock = ssl.wrap_socket(sock, self.key_file, self.cert_file, ssl_version=ssl.PROTOCOL_TLSv1_2)


httplib.HTTPSConnection = HTTPSConnectionTLSv1_2
#
# end of hack
#



class PluginAuthentication(plugin_type_auth.PluginTypeAuth, plugin_type_traffic.PluginTypeTraffic):
    """
    Runtime server part of DME device authentication plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'dme', license_handler, session_info, database, management_message_session, access_log_server_session)
        plugin_type_traffic.PluginTypeTraffic.__init__(self, async_service, checkpoint_handler, u'dme', license_handler, session_info)

        self.serial_authenticated = None
        config_values = config.dme_server_config_dict
        self.dme_server = config_values.get("dme_server")
        self.port = config_values.get("port")
        https = config_values.get("https")
        self.protocol = "https" if https else "http"
        server = "%s:%s" % (self.dme_server, self.port) if self.port else self.dme_server
        self.dme_url = "%s://%s/nam_xml/1.5/" % (self.protocol, server)
        self.checkpoint_handler.Checkpoint('__init__', self.plugin_name, lib.checkpoint.DEBUG, dme_server=self.dme_server, port=self.port, https=https, url=self.dme_url)
        self._server_version = lib.version.Version.create_current().get_version_string()

        self._attributes = dict()


    def start(self):
        """
        Initiates the process
        """
        if self._tunnel_endpoint != None:
            self.tunnelendpoint_send(message.Message('get_dme_values'))
            self.set_started()
        else:
            raise plugin_type_auth.PluginWaitingForConnectionException()

    def get_predicate_info(self):
        if self.serial_authenticated:
            return [dict(value = "dme_token")]
        else:
            return [dict(value = None)]

    def _check_element(self, param, internal_type=None):
        return self.serial_authenticated

    def get_attribute(self, attribute_name):
        value = self._attributes.get(attribute_name)
        if value:
            return value
        elif attribute_name == "server":
            return self.dme_server
        elif attribute_name == "port":
            return self.port
        elif attribute_name == "protocol":
            return self.protocol

        return None

    def get_dme_values_response(self, values):
        with self.checkpoint_handler.CheckpointScope('receive_dme_values', self.plugin_name, lib.checkpoint.INFO) as cps:
            if values:
                self._attributes = values
                try:
                    # Call DME Server and check
                    username = values.get("username")
                    cps.add_complete_attr(username=username)
                    password = values.get("password")
                    terminal_id = values.get("terminal_id")
                    cps.add_complete_attr(terminal_id=terminal_id)
                    device_signature = values.get("device_signature")
                    cps.add_complete_attr(device_signature=device_signature)
                    device_time = values.get("device_time")
                    cps.add_complete_attr(device_time=device_time)

                    self._attributes["terminal-id"] = terminal_id
                    self._attributes["device-signature"] = device_signature
                    self._attributes["device-time"] = device_time

                    request = urllib2.Request(url=self.dme_url)
                    username_utf8 = username.encode('utf-8')
                    password_utf8 = password.encode('utf-8')
                    base64string = base64.encodestring('%s:%s' % (username_utf8, password_utf8)).replace("\n", "")
                    authheader =  "Basic %s" % base64string
                    request.add_header("Authorization", authheader)
                    request.add_header("terminal-id", terminal_id)
                    request.add_header("device-signature", device_signature)
                    request.add_header("device-time", device_time)
                    request.add_header("User-agent", "G/On %s" % self._server_version)
                    reply = urllib2.urlopen(request)
                    if int(reply.getcode()) == 200:
                        self.serial_authenticated = True
                    else:
                        self.serial_authenticated = False
                        self.checkpoint_handler.Checkpoint('get_dme_values_response', self.plugin_name, lib.checkpoint.ERROR, server_reply_code=reply.getcode())
                        self.access_log_server_session.report_access_log_gateway_warning("%s plugin" % self.plugin_name, reply.getcode(), "Check for DME Approved Device failed", getattr(reply, "msg", "HTTP Error"))
                except urllib2.HTTPError, e:
                    self.serial_authenticated = False
                    self.checkpoint_handler.Checkpoint("get_dme_values_response", self.plugin_name, lib.checkpoint.ERROR, msg=e.msg)
                    self.access_log_server_session.report_access_log_gateway_warning("%s plugin" % self.plugin_name, e.code, "Check for DME Approved Device failed", e.msg)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("get_dme_values_response", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                    self.access_log_server_session.report_access_log_gateway_warning("%s plugin" % self.plugin_name, "", "Check for DME Approved Device failed", "Exception raised - check server log file for details")
            else:
                self.access_log_server_session.report_access_log_gateway_warning("%s plugin" % self.plugin_name, "", "Check for DME Approved Device failed", "No values returned from client")



            self.set_ready()
