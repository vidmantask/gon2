"""
Computer Token plugin module for the Management Server 
"""
from __future__ import with_statement

import os.path
import ConfigParser

import linecache

import lib.checkpoint

from plugin_modules import access_log
from plugin_types.server_management import plugin_type_report

from components.endpoint.server_common import database_schema
from components.database.server_common import schema_api
from components.database.server_common import database_api

from plugin_types.server_management import plugin_type_element, plugin_type_config

plugin_name = "dme"

class PluginDMEToken(plugin_type_config.PluginTypeConfig, plugin_type_element.PluginTypeElement):
    def __init__(self, checkpoint_handler, database, license_handler):
        
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        self.token = self.create_element("dme_token", "DME Approved Device")

    element_types = ["dme_token"]

    def get_elements(self, element_type):
        yield self.token
#                elements.append(token)
#            return elements
        
    def get_specific_elements(self, internal_element_type, element_ids):
        if "dme_token" in element_ids:
            return [self.token]
        return []

    def get_element_count(self, element_type):
        return 1
    
    
    def config_get_template(self, element_type, element_id, db_session):
        return None

