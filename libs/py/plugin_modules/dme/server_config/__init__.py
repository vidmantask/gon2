"""
Server config specification plugin  

"""
from __future__ import with_statement

import os.path
import shutil

import lib.checkpoint
import lib.version
import lib.utility.support_file_filter as support_file_filter

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from plugin_types.server_config import plugin_type_upgrade

import plugin_types.server_config.plugin_type_config_specification as plugin_type_config_specification 
import plugin_modules.dme.server_common.dme_config as dme_config
import plugin_modules.dme


class ConfigSpecificationPluginDME(plugin_type_config_specification.PluginTypeConfigSpecification):


#class ConfigSpecificationPluginLDAP():
    ID = 'plugin_dme'
    TITLE = 'DME Server plugin configuration' 
    DESCRIPTION = '' 

    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_config_specification.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration_all, __name__, u"dme")
        self.server_configuration_all = server_configuration_all
        self.config_template = None
        
        
    def get_version(self):
        return lib.version.Version.create_from_encode(plugin_modules.dme.plugin_module_version)

    def get_path_management_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_management_plugin_root(server_configuration_all, 'dme')

    def get_path_gateway_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_gateway_plugin_root(server_configuration_all, 'dme')

    def get_config_specification(self):
        config_spec = ConfigTemplateSpec()
        config_spec.init(ConfigSpecificationPluginDME.ID, ConfigSpecificationPluginDME.TITLE, ConfigSpecificationPluginDME.DESCRIPTION)
        config_spec.add_field('dme_server', 'DME Server', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="IP or DNS of DME Server")
        config_spec.add_field('port', 'DME Server port', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, tooltip="Port number DME Server is listening on")
        config_spec.add_field('https', 'HTTPS', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False, tooltip="Use https for DME queries", value=True)
        
        management_config = dme_config.Config(self.get_path_management_ini())
        for key, value in management_config.dme_server_config_dict.items():
            config_spec.set_value(key, value)
        
        return config_spec.get_template()

    def save(self, template):
        if template.get_attribute_name() != self.ID:
            return None
        
        self.config_template = template

        return self.create_save_response(True)
    
    def finalize(self):
        config_spec = ConfigTemplateSpec(self.config_template)
        values = config_spec.get_field_value_dict()
        
        dme_config.Config.write(self.get_path_management_ini(), values)
        dme_config.Config.write(self.get_path_gateway_ini(), values)
    

    def generate_support_package(self, support_package_root):
        support_log_filename_abs = os.path.join(support_package_root, 'dme.txt') 
        support_log = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(support_log_filename_abs, False), None)
        self._copy_ini_files(support_log, support_package_root, remove_sensitive_info=True)
    
    def backup(self, backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all):
        """
        This method is called with a folder where the plugin can backup files/info. The information dumped will be included in the backup.
        """
        self._copy_ini_files(backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all)

    def _copy_ini_files(self, logger, dest_root, cb_config_backup=None, alternative_server_configuration_all=None, remove_sensitive_info=False):
        ini_filename_src = os.path.join(self.get_path_management_ini(alternative_server_configuration_all), dme_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'management_%s' % dme_config.Config.CONFIG_FILENAME)
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.dme.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("AD-Plugin ini file not found, see log file.")

        ini_filename_src = os.path.join(self.get_path_gateway_ini(alternative_server_configuration_all), dme_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'gateway_%s' % dme_config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.dme.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("AD-Plugin ini file not found, see log file.")


    def restore(self, restore_log, restore_package_plugin_root, cb_restore):
        ini_filename_src = os.path.join(restore_package_plugin_root, 'management_%s' % dme_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_management_ini(), dme_config.Config.CONFIG_FILENAME)
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)
        
        ini_filename_src = os.path.join(restore_package_plugin_root, 'gateway_%s' % dme_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_gateway_ini(), dme_config.Config.CONFIG_FILENAME) 
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)


