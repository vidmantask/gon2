import sys
import os.path
import datetime

import lib.checkpoint

from components.communication import message

from plugin_types.server_gateway import plugin_type_traffic
from plugin_types.server_gateway import plugin_type_user

from plugin_modules.dev_env_user.server_common import global_users


class PluginAuthentication(plugin_type_traffic.PluginTypeTraffic, plugin_type_user.PluginTypeUser):
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_traffic.PluginTypeTraffic.__init__(self, async_service, checkpoint_handler, 'dev_env_user_user', license_handler, session_info)
        plugin_type_user.PluginTypeUser.__init__(self, async_service, checkpoint_handler, 'dev_env_user_user', license_handler, session_info, database, management_message_session, access_log_server_session)
        self.login = ''
        self.password = ''
    
    @classmethod
    def generator(cls, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugins = {}
        plugin = PluginAuthentication(async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session)
        plugins[plugin.plugin_name] = plugin
        return plugins
    
    def start(self):
        """
        Initiates the login process
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::start", self.plugin_name, lib.checkpoint.DEBUG) as cps:
            self.request_login()
            self.set_started()
    
    def is_logged_in_name(self, login):
        """
        True if the user is logged in as login
        """
        user_login = login[0]
        user_name = login[1]
        if self.authenticated:
            return self.login == self.user_login
        return False
    
    def is_logged_in_member(self, group):
        """
        True if the user is logged in as member of group
        """
        return False
    
    def get_group_ids_for_logged_in_user(self):
        return None
    
    def is_authenticated(self):
        return self.authenticated
    
    def login_cancelled(self):
        self.authenticated = False
        self.set_ready()
    
    def receive_login(self, login, password, internal_user_login=None):
        """
        Recieves result from login and password prompt and continues the authentication proces
        """
        with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_login", self.plugin_name, lib.checkpoint.DEBUG, login=login):
            self.authenticated = False
            self.login = login
            if self.authenticated:
                return
            for user in global_users.values():
                if user['user_login'] == self.login and user['user_password'] == password:
                    self.user_id  = user['user_id']
                    self.password = password
                    self.authenticated = True
            self.set_ready()
                
    def get_normalised_login(self, user_login):
        return user_login
    
    def lookup_user(self, user_login):
        for user in global_users.values():
            if user['user_login'] == user_login:
                return user['user_login']
        return None
                
    def receive_changed_password(self, old_password, new_password):
        return "It is not possible to change password in this configuration"
    
    def change_password_cancelled(self):
        if self.must_change_password:
            self.authenticated = False
            self.set_ready()
        
    def get_attribute(self, attribute_name):
        return None
    
    def get_current_user_id(self):
        if self.authenticated:
            return self.user_id
        return None


if __name__ == '__main__':
    print global_users
    