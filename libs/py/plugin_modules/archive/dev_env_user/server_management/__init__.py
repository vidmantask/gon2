# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
import sys
import os.path


import lib.checkpoint

from plugin_types.server_management import plugin_type_user
from plugin_types.server_management import plugin_type_config
from plugin_types.server_management import plugin_type_access_notification
from plugin_types.server_management import plugin_type_element

from plugin_modules.dev_env_user.server_common import global_users


class PluginAuthentication(plugin_type_user.PluginTypeUser):
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, database, license_handler, 'dev_env_user_user')
        
    element_types = ["user", "group"]

    @classmethod
    def generator(cls, checkpoint_handler, database, license_handler):
        plugins = {}
        plugin = PluginAuthentication(checkpoint_handler, database, license_handler)
        plugins[plugin.plugin_name] = plugin
        return plugins

    def user_exists(self, user_id):
        if global_users.has_key(user_id):
            return True
        return False
        
    def get_login_and_name_from_id(self, user_id, internal_user_login):
        if global_users.has_key(user_id):
            user = global_users[user_id]
            return user['user_id'], user['user_login'], user['user_name']
        raise Exception("Found no login name for user with RID '%s'" % (user_id) ) 

    def fetch_users(self, search_filter=None):
        if search_filter:
            search_filter = search_filter[1:len(search_filter)-1]
        for user in global_users.values():
            if not search_filter or search_filter in user['user_login'] or search_filter in user['user_name']:
                new_user = self.create_element(user['user_id'], user['user_login'], user['user_name'])
                yield new_user

    def fetch_groups(self, search_filter=None):
        return

    def get_group_members(self, group_id):
        return []

    def get_id_column(self):
        return plugin_type_config.Column(self.plugin_name, 'id', plugin_type_config.Column.TYPE_STRING, 'RID', hidden=True, is_id=True)

    def get_email(self, user_sid):
        raise NotImplementedError()

