"""
Unittest of hagiwara plugin module
"""
from __future__ import with_statement
import sys
import os
import os.path

import sys
import shutil
import tempfile

import unittest
import lib_cpp.hagi
import lib.gpm.gpm_env
import lib.gpm.gpm_builder

import lib.hagi.gon_hagi_authentication

from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility


from plugin_types.client_gateway import plugin_type_token
import plugin_types.client_gateway.plugin_type_client_runtime_env

from components.communication.sim import sim_tunnel_endpoint
from components.database.server_common.connection_factory import ConnectionFactory
import components.management_message.server_gateway.session_send

import plugin_modules.hagiwara.server_gateway
import plugin_modules.hagiwara.client_gateway
from plugin_types.common import plugin_type_token as database_model 


import lib_cpp.hagi as hagi_lib


import components.dictionary.common






class AuthCallbackStub(object):
    def plugin_event_state_change(self, name, predicate_info=[]):
        pass

class Auth_plugin_test(unittest.TestCase):
    def setUp(self):
        self.db_filename = giri_unittest.mk_temp_filename()
        self.db = ConnectionFactory('sqlite:///%s' % self.db_filename).get_default_connection()
        database_model.connect_to_database_using_connection(self.db)
        
    def tearDown(self):
        self.db.dispose()

    def test_auth_plugin_simple(self):
        license_handler = None
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        management_message_session_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSendBlackHole()
        self.plugin_client = plugin_modules.hagiwara.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
        self.plugin_server = plugin_modules.hagiwara.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), license_handler, None, self.db, management_message_session_send, None)
        
        self.plugin_server.set_callback(AuthCallbackStub())
        
        self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
        self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
        
        token_plugin = plugin_modules.hagiwara.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), user_interface=None, additional_device_roots=[])

        tokens = token_plugin.get_tokens([])
        self.assertTrue(len(tokens)>0)

        token_id = tokens[0].token_id
        token_serial = tokens[0].token_serial

        token_plugin.generate_keypair(token_id)
        
        with database_model.Transaction() as dbt:
            new_key =  dbt.add(database_model.Token())
            new_key.serial = token_serial
            new_key.plugin_type = u'hagiwara'
            new_key.public_key = token_plugin.get_public_key(token_id)
            dbt.commit()

        self.plugin_server.start()
        giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((token_serial, token_serial)), 10000)
        self.assertEqual(self.plugin_server.check_predicate((token_serial, token_serial)), True)
        

class enrolled_test(unittest.TestCase):
    def test_enrolled(self):
        if sys.platform == 'win32':
            (rc, message) = hagi_lib.key_set_enrolled()
            self.assertTrue(rc)

            (rc, message, type, ro_device, rw_device, enrolled) = hagi_lib.key_discover_device()
            self.assertTrue(rc)
            self.assertTrue(enrolled)
    
            (rc, message) = hagi_lib.key_reset_enrolled()
            self.assertTrue(rc)
    
            (rc, message, type, ro_device, rw_device, enrolled) = hagi_lib.key_discover_device()
            self.assertTrue(rc)
            self.assertFalse(enrolled)


class Installation_test(unittest.TestCase):
    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.gon_installation.generate_gpms()
        self.knownsecret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        self.knownsecret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'
        self.servers  = "hej med dig"

    def tearDown(self):
        pass
    
    def test_auth_plugin_simple(self):
        class Install_CB(plugin_types.client_gateway.plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
            def __init__(self, download_root):
                self.download_root = download_root
                self.gpm_meta_signature = None
                                
            def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
                print "Install_CB::gpm_installer_cb_task_all_start", tasks_count, tasks_ticks_count
        
            def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
                print "Install_CB::gpm_installer_cb_task_start", task_title, task_ticks_count
        
            def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
                print "Install_CB::gpm_installer_cb_action_start", action_title, action_ticks_conut
        
            def gpm_installer_cb_action_tick(self, ticks):
                print "Install_CB::gpm_installer_cb_action_tick", ticks
        
            def gpm_installer_cb_action_done(self):
                print "Install_CB::gpm_installer_cb_action_done"
        
            def gpm_installer_cb_task_done(self):
                print "Install_CB::gpm_installer_cb_task_done"
        
            def gpm_installer_cb_task_all_done(self):
                print "Install_CB::gpm_installer_cb_task_all_done"
        
        (rc, message, type, ro_device, rw_device, enrolled) = hagi_lib.key_discover_device()
        (rc, message, unique_id) = hagi_lib.key_get_unique_id()

        if rc:
            runtime_env = plugin_modules.hagiwara.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), unique_id, ro_device, rw_device)
            runtime_env.set_download_root(self.gon_installation.get_gpms_folder()) 
            
            dictionary = components.dictionary.common.load_dictionary(giri_unittest.get_checkpoint_handler(), '')
            
            error_handler = lib.gpm.gpm_env.GpmErrorHandler()
            runtime_env.install_gpms_clean(['a_extra-1-1-win', 'b-1-1-win'], Install_CB(self.gon_installation.get_gpms_folder()), error_handler, dictionary, self.knownsecret_client, self.servers, False)
    
            error_handler.dump()
            self.assertTrue(error_handler.ok())
        
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

def hagi_token_found():
    token_plugin = plugin_modules.hagiwara.client_gateway.PluginTokenAndAuth(None, giri_unittest.get_checkpoint_handler(), user_interface=None, additional_device_roots=[])
    tokens = token_plugin.get_tokens([])
    return len(tokens) > 0
GIRI_UNITTEST_IGNORE = not hagi_token_found()


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
