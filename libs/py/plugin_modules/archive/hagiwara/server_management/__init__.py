"""
Hagiwara plugin for the Management Server
"""
from __future__ import with_statement

import lib.checkpoint
from components.database.server_common.database_api import *
from plugin_types.server_management import plugin_type_token


class PluginSoftToken(plugin_type_token.PluginToken):


    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_token.PluginToken.__init__(self, checkpoint_handler, database, license_handler, u'hagiwara')


    def get_info(self,db_token):
        casing_or_serial = db_token.casing_number if db_token.casing_number else db_token.serial
        if db_token.description and casing_or_serial:
            info = "%s : %s" % (casing_or_serial, db_token.description)
        elif db_token.description:
            info = db_token.description
        else:
            info = casing_or_serial
        return info
