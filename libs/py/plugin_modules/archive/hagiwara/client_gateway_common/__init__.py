"""
Hagiwara plugins for commen Gateway Client functionality
"""
from __future__ import with_statement


import os
import os.path
import lib_cpp.hagi
import sys
import plugin_types.client_gateway.plugin_type_token


module_id = 'HagiwaraPluginModule'




_global_hagi_cache = {}

def hagi_cache_data():
    global _global_hagi_cache
    (rc, message, unique_id) = lib_cpp.hagi.key_get_unique_id()
    ro_device = None
    rw_device = None
    public_key = None
    type  = None
    enrolled = False
    if rc:
        if _global_hagi_cache.has_key(unique_id):
            (type, ro_device, rw_device, enrolled) = _global_hagi_cache[unique_id]
        else:
            (rc, message, type, ro_device, rw_device, enrolled) = lib_cpp.hagi.key_discover_device()
            if rc:
                _global_hagi_cache[unique_id] = (type, ro_device, rw_device, enrolled)
                
    return (rc, message, unique_id, type, ro_device, rw_device, enrolled)

def get_tokens(plugin_name):
    (rc, message, unique_id, type, ro_device, rw_device, enrolled) = hagi_cache_data()
    if rc and unique_id is not None and ro_device is not None and rw_device is not None:
        token_title = "%s:\(CD-Rom) %s:\(Flash) (Hagiwara %s)" % (ro_device.upper(), rw_device.upper(), type) 
        token_type_title = "Hagiwara%s" % type
        token_id = "%s-%s" % (ro_device.upper(), rw_device.upper())
        token = plugin_types.client_gateway.plugin_type_token.Token(plugin_name, token_id, plugin_types.client_gateway.plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, None, token_type_title=token_type_title, token_title=token_title, token_plugin_module_name='hagiwara')
        token.token_serial = unique_id
        token.runtime_env_id = u"%s::%s" %('hagiwara_client_runtime_env', unique_id)
        token.token_status = plugin_types.client_gateway.plugin_type_token.Token.TOKEN_STATUS_INITIALIZED
        if enrolled:
            token.token_status = plugin_types.client_gateway.plugin_type_token.Token.TOKEN_STATUS_ENROLLED
        return [token]
    return []
