"""
Hagiwara plugin for Gateway Client
"""
from __future__ import with_statement

import os
import stat
import sys
import os.path
import tempfile
import shutil
import lib.checkpoint
import lib.cryptfacility
import lib_cpp.hagi
import lib.hardware.device
import time
import ctypes
import re

if sys.platform == 'win32':
    import win32file
    import win32com.shell.shell
    import win32com.shell.shellcon
    
import lib.config
import lib.gpm.gpm_installer_base
import lib.gpm.gpm_installer
import lib.gpm.gpm_builder
import lib.gpm.gpm_spec
import lib.appl.gon_client

import lib_cpp.hagi
import lib.appl.in_use_mark

from components.communication import message


from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_client_runtime_env

import plugin_modules.hagiwara.client_gateway_common

MODULE_ID = 'hagiwara'



class PluginTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'hagiwara'
    """
    Token plugin 
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
    
    def get_tokens(self, additional_device_roots):
        return plugin_modules.hagiwara.client_gateway_common.get_tokens(self.plugin_name)

    def initialize_token(self, token_id):
        pass
    
    def deploy_token(self, token_id, client_knownsecret, servers):
        pass

    def generate_keypair(self, token_id):
        return self._generate_keypair(token_id, 5)

    def _generate_keypair(self, token_id, counter):
        (rc, message) = lib_cpp.hagi.key_generate_keypair();
        if not rc:
            if counter > 0:
                time.sleep(1)
                return self._generate_keypair(token_id, counter-1)
            raise plugin_type_token.Error("Unable to generate_keypair for the hagiwara device '%s', %s" % (token_id, message))
        (rc, message) = lib_cpp.hagi.key_reset_enrolled();

    def set_serial(self, token_id, serial):
        pass
    
    def get_public_key(self, token_id):
        (rc, message, public_key) = lib_cpp.hagi.key_get_public_key();
        return public_key

    def set_enrolled(self, token_id):
        (rc, message) = lib_cpp.hagi.key_set_enrolled();
        
    def reset_enrolled(self, token_id):
        (rc, message) = lib_cpp.hagi.key_reset_enrolled();

    def is_enrolled(self, token_id):
        (rc, message, unique_id, type, ro_device, rw_device, enrolled) = lib_cpp.hagi.hagi_cache_data()
        return enrolled

    def get_knownsecret_and_servers(self, token_id):
        """
        Asking for knownsecret and servers in a situation where it not  should be available is unusual.
        It has been seen  that the hagiwara functions retun a ERROR_DEVICE_NOT_READY when launch of gon_client
        is tricked by inserting the hagiwara key (autolaunch).
        """
        knownsecret = None
        servers = None
        (rc, message, knownsecret, servers) = lib_cpp.hagi.key_get_knownsecret_and_servers();
        if not rc:
            self.checkpoint_handler.Checkpoint('get_knownsecret_and_servers.error', self.plugin_name, lib.checkpoint.ERROR, message=message)
        return (knownsecret, servers)

    def set_servers(self, token_id, servers):
        return "Not available for hagiwara token type"

    def get_serial(self, token_id = None):
        if token_id is None:
            return self.get_serial_auth()
        
        (rc, message, token_serial) = lib_cpp.hagi.key_get_unique_id()
        return token_serial
    
    
    msg_receivers = ['get_serial', 'get_challenge']
    
    def get_serial_auth(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            (rc, message, serial) = lib_cpp.hagi.key_get_unique_id()
            self.tunnelendpoint_remote('get_serial_response', serial=serial)
                
    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            (rc, message, challenge_signature) = lib_cpp.hagi.key_gon_hagi_authentication(challenge);
            if rc:
                self.tunnelendpoint_remote('get_challenge_response', challenge_signature=challenge_signature)
            else:
                self.tunnelendpoint_remote('get_challenge_response', challenge_signature=None)



class PluginClientRuntimeEnv(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv):
    """
    Client runtime envrionment plugin
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'hagiwara_client_runtime_env'
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin. 
        """
        (rc, message, unique_id, type, ro_device, rw_device, enrolled) = plugin_modules.hagiwara.client_gateway_common.hagi_cache_data()
        if rc:
            runtime_env_id = unicode(unique_id)
            return [runtime_env_id]
        return []

    def get_instance(self, runtime_env_id):
        """
        Return the instance. 
        """
        (rc, message, unique_id, type, ro_device, rw_device, enrolled) = plugin_modules.hagiwara.client_gateway_common.hagi_cache_data()
        if rc:
            runtime_env_id = unicode(unique_id)
            runtime_env = PluginClientRuntimeEnvInstance(self.checkpoint_handler, unique_id, ro_device, rw_device)
            return runtime_env
        return None
    

class AlienTaskCreateAndBurn(lib.gpm.gpm_installer_base.GpmInstallerTaskBase):
    class AlienActionCreateAndBurn(lib.gpm.gpm_installer_base.GpmInstallerActionBase, lib_cpp.hagi.CreateAndBurnISOCallback):
        def __init__(self, gpm_meta_root, ro_root, gpm_ids_install, gpm_ids_update, gpm_ids_remove, cb, error_handler, dictionary, temp_root, knownsecret, servers, generate_keypair):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, '', cb, error_handler, dictionary)
            lib_cpp.hagi.CreateAndBurnISOCallback.__init__(self)
            self.gpm_meta_root = gpm_meta_root
            self.ro_root = ro_root
            self.gpm_ids_install = [id for id in set(gpm_ids_install).union(set([x[1] for x in gpm_ids_update]))]
            self._last_process_count = 0
            self.knownsecret = knownsecret 
            self.servers = servers
            self.temp_root = temp_root
            self.generate_keypair = generate_keypair
            
        def get_ticks(self):
            return 100

        def progress(self, message, count):
            ticks = count - self._last_process_count
            self._last_process_count = count
            self.gpm_installer_cb.gpm_installer_cb_action_tick(ticks)

        def do_action(self):
            if self.knownsecret is None and self.servers is None:
                (burn_ok, message) = lib_cpp.hagi.key_create_and_burn_iso(self.ro_root, self.temp_root, "G-On", self)
            else:
                (burn_ok, message) = lib_cpp.hagi.key_create_and_burn_iso_clean(self.ro_root, self.temp_root, "G-On", str(self.knownsecret), str(self.servers), self.generate_keypair, self)
            if not burn_ok :
                self.error_handler.emmit_error(message)
                return

    def __init__(self, gpm_meta_root, ro_root, gpm_ids_install, gpm_ids_update, gpm_ids_remove, cb, error_handler, dictionary, temp_root, knownsecret=None, servers=None, generate_keypair=True):
        lib.gpm.gpm_installer_base.GpmInstallerTaskBase.__init__(self, 'Creating and Burning ISO', cb, error_handler, dictionary)
        self.add_action(AlienTaskCreateAndBurn.AlienActionCreateAndBurn(gpm_meta_root, ro_root, gpm_ids_install, gpm_ids_update, gpm_ids_remove, cb, error_handler, dictionary, temp_root, knownsecret, servers, generate_keypair))



class AlienTaskRemount(lib.gpm.gpm_installer_base.GpmInstallerTaskBase):
    class AlienActionRemount(lib.gpm.gpm_installer_base.GpmInstallerActionBase):
        def __init__(self, ro_root, rw_root, timeout_sec, cb, error_handler, dictionary):
            lib.gpm.gpm_installer_base.GpmInstallerActionBase.__init__(self, '', cb, error_handler, dictionary)
            self.ro_root = ro_root
            self.rw_root = rw_root
            self.timeout_sec = timeout_sec
            
        def get_ticks(self):
            return self.timeout_sec + 2

        def _remount(self, device):
            try:
                win32com.shell.shell.SHChangeNotify(win32com.shell.shellcon.SHCNE_DRIVEREMOVED, win32com.shell.shellcon.SHCNF_PATH, device)
                volume_name =  win32file.GetVolumeNameForVolumeMountPoint(device) 
                win32file.DeleteVolumeMountPoint(device)
                time.sleep(2)
                
                win32file.SetVolumeMountPoint(device, volume_name)
                time.sleep(2)

                win32com.shell.shell.SHChangeNotify(win32com.shell.shellcon.SHCNE_DRIVEADD, win32com.shell.shellcon.SHCNF_PATH, device)
                time.sleep(1)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.error_handler.emmit_warning('Unexpected error when remounting device' + repr(evalue))

        def do_action(self):
            self._remount(self.ro_root)
            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)

            self._remount(self.rw_root)
            self.gpm_installer_cb.gpm_installer_cb_action_tick(1)

            while(self.timeout_sec > 0):
                if os.path.isdir(self.ro_root) and os.path.isdir(self.rw_root):
                    break
                time.sleep(1)
                self.timeout_sec -= 1
                self.gpm_installer_cb.gpm_installer_cb_action_tick(1)

    def __init__(self, ro_root, rw_root, timeout_sec, cb, error_handler, dictionary):
        lib.gpm.gpm_installer_base.GpmInstallerTaskBase.__init__(self, dictionary._('Remounting hagiwara device'), cb, error_handler, dictionary)
        self.add_action(AlienTaskRemount.AlienActionRemount(ro_root, rw_root, timeout_sec, cb, error_handler, dictionary))

        
class PluginClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstance):
    def __init__(self, checkpoint_handler, hagi_device_id, ro_device, rw_device):
        self.checkpoint_handler = checkpoint_handler
        self._hagi_device_id = hagi_device_id
        self._forced_to_current = False
        self._set_root_from_letters(ro_device, rw_device)

    def force_to_current(self):
        """
        Tell this instance that it has be chosen to be the current.
        """
        self._forced_to_current = True

    def get_info(self):
        info = plugin_type_client_runtime_env.Info(u'hagiwara_client_runtime_env', self._hagi_device_id, 'Hagiwara')
        return info

    def _set_root_from_letters(self, ro_drive_letter, rw_drive_letter):
        self._set_root(os.path.abspath('%s:\\' % ro_drive_letter), os.path.abspath('%s:\\' % rw_drive_letter))

    def _set_root(self, ro_root, rw_root, download_root=None):
        self._ro_root = ro_root
        self._rw_root = rw_root
        self._gpm_meta_root = os.path.join(self._rw_root, 'gon_client', 'gpm_meta')
        self._runtime_root = self._ro_root
        if download_root is not None:
            self._download_root = download_root
        else:
            self._download_root = os.path.join(self.get_temp_root(),'download_cache')
       
    def set_download_root(self, download_root): 
        self._download_root = download_root

    def is_current(self):
        cwd = os.path.normcase(os.path.normpath(os.path.abspath(os.getcwdu())))
        if self._runtime_root != None and cwd.startswith(os.path.normcase(os.path.normpath(self._runtime_root))):
            return True
        return self._forced_to_current

    def support_download(self, size, dictionary):
        if not self.is_current():
            return {'supported':False, 'message': dictionary._('This runtime environment is not the current one')}
        if not os.path.exists(self._download_root):
            os.makedirs(self._download_root)
        if not os.access(self._download_root, os.W_OK):
            return {'supported':False, 'message':dictionary._('No write-access to the folder %s') % self._download_root}
        free_space = lib.hardware.device.Device.get_free_space(self._download_root)
        if free_space != None and free_space < size: 
            return {'supported':False, 'message': dictionary._('Not enough space on device to download and install packages')}
        return {'supported':True}

    def get_download_root(self, ):
        return self._download_root

    def _is_install_state_none(self, gon_client_install_state):
        if gon_client_install_state == None:
            return True
        plugin_data = gon_client_install_state.get_plugin_data()
        return plugin_data == None or not plugin_data.has_key('hagiwara_data')

    def _is_install_state_relocated(self, gon_client_install_state):
        if gon_client_install_state == None:
            return False
        plugin_data = gon_client_install_state.get_plugin_data()
        if plugin_data != None and plugin_data.has_key('hagiwara_data'):
            hagiwara_data = plugin_data['hagiwara_data']
            if hagiwara_data.has_key('state') and  hagiwara_data['state']=='relocated':
                return True
        return False
    
    def _is_install_state_cleanup(self, gon_client_install_state):
        if gon_client_install_state == None:
            return False
        plugin_data = gon_client_install_state.get_plugin_data()
        if plugin_data != None and plugin_data.has_key('hagiwara_data'):
            hagiwara_data = plugin_data['hagiwara_data']
            if hagiwara_data.has_key('state') and  hagiwara_data['state']=='cleanup':
                return True
        return False

    def _set_install_state(self, state, cleanup_root=None):
        restart_gon_client_install_state = lib.appl.gon_client.GOnClientInstallState()
        hagiwara_data = {'state': state, 'cleanup_root' : cleanup_root}
        plugin_data = {'hagiwara_data': hagiwara_data}
        restart_gon_client_install_state.set_plugin_data(plugin_data)
        return restart_gon_client_install_state

    def _get_install_state_data(self, gon_client_install_state):
        if gon_client_install_state == None:
            return None
        plugin_data = gon_client_install_state.get_plugin_data()
        plugin_data_data = plugin_data['hagiwara_data']
        return (plugin_data_data['cleanup_root'])

    def support_install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gon_client_install_state, dictionary, knownsecret, servers):
        if lib.gpm.post_hook.is_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary):
            return {'supported': False, 'message': dictionary._('Bootification not supported on a Hagiwara key')}
        
        contains_ro_files = lib.gpm.gpm_builder.query_meta_for_ro_files(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta)
        if not contains_ro_files:
            return {'supported': True, 'message': '', 'require_restart':False}
        if ctypes.windll.shell32.IsUserAnAdmin():
            return {'supported': True, 'message': '', 'require_restart':True}
        return {'supported': False, 'message': dictionary._('You need Administrator privileges in order to update your key'), 'require_restart':False}
        
    def _is_ro_awailable(self):
        try:
            return os.path.isdir(self.get_ro_root())
        except:
            return False
        
    def install_gpms_clean(self, gpm_ids_install, gpm_installer_cb, error_handler, dictionary, knownsecret, servers, generate_keypair):

        # gpm_ids to be removed must be found before changing root of runtime_env
        gpm_ids_remove = []
        try:
            gpm_ids_remove = lib.gpm.gpm_builder.query_meta_for_gpm_ids(self.get_installed_gpm_meta_all(error_handler), error_handler)
        except:
            self.error_handler.emmit_warning('Unexpected error looking for installed packages')

        temp_root = tempfile.mkdtemp()
        temp_root_ro = os.path.join(temp_root, 'ro_root')
        temp_root_rw = os.path.join(temp_root, 'rw_root')
        os.mkdir(temp_root_ro)
        os.mkdir(temp_root_rw)
        
        saved_download_root = self._download_root 
        saved_ro_root = self.get_ro_root()
        saved_rw_root = self.get_rw_root()
        self._set_root(temp_root_ro, temp_root_rw)
        self._download_root = saved_download_root 

        task_backup_rw_clean_up_folders = []
        # Remove rw-hagi folder because it contain the used public key, and enrolled file 
        if generate_keypair:
            backup_rw_hagi_folder = os.path.join(temp_root_rw, 'gon_client', 'gon_hagi')
            task_backup_rw_clean_up_folders.append(backup_rw_hagi_folder)

        task_create_and_burn_iso = AlienTaskCreateAndBurn(self._gpm_meta_root, temp_root_ro, gpm_ids_install, [], gpm_ids_remove, gpm_installer_cb, error_handler, dictionary, temp_root, knownsecret, servers, generate_keypair)
        task_remount = AlienTaskRemount( saved_ro_root, saved_rw_root, 10, gpm_installer_cb, error_handler, dictionary)
        task_backup_rw = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(saved_rw_root, temp_root_rw, None, dictionary._('Creating backup of flash drive'), gpm_installer_cb, error_handler, dictionary)
        task_backup_rw_clean_up = lib.gpm.gpm_installer.GpmInstallerTaskCleanup(task_backup_rw_clean_up_folders, [], dictionary._('Creating backup of flash drive'), gpm_installer_cb, error_handler, dictionary)
        task_restore_rw = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(temp_root_rw, saved_rw_root, None, dictionary._('Restoring flash drive'), gpm_installer_cb, error_handler, dictionary, ticks=task_backup_rw.get_ticks())
        task_clean_up = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([temp_root], [], dictionary._('Cleaning up temporary files'), gpm_installer_cb, error_handler, dictionary)
        task_remount_2 = AlienTaskRemount( saved_ro_root, saved_rw_root, 10, gpm_installer_cb, error_handler, dictionary)

        tasks_pre = [task_backup_rw, task_backup_rw_clean_up]
        tasks_post = [task_create_and_burn_iso, task_remount, task_restore_rw, task_clean_up, task_remount_2]

        # Installation need to be split in two, because meta info need to be copied to temp location because it is needed when removing packages
        installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root_ro, temp_root_rw, error_handler, dictionary, gpm_installer_cb)
        installer.init_tasks([], [], [], alien_pre_tasks=tasks_pre)
        installer.execute()
        
        installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root_ro, temp_root_rw, error_handler, dictionary, gpm_installer_cb)
        installer.init_tasks(gpm_ids_install, [], gpm_ids_remove, alien_pre_tasks=[], alien_post_tasks=tasks_post)
        installer.execute()
        self._set_root(saved_ro_root, saved_rw_root)
        

    def install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, cb, gon_client_install_state, error_handler, dictionary, knownsecret, servers):
        if self._is_install_state_none(gon_client_install_state):
            require_restart = lib.gpm.gpm_builder.query_meta_for_ro_files(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta)
            require_restart_connect_info = not (knownsecret is None and servers is None)
            if not require_restart and not require_restart_connect_info:
                self._install_gpms_no_restart(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, cb, gon_client_install_state, error_handler, dictionary)
                return

            cleanup_root = tempfile.mkdtemp()
            root_relocated = os.path.join(cleanup_root, 'root_relocated')
            os.mkdir(root_relocated)

            gon_client_update_package_id = lib.appl.gon_client.contains_current_instance(gpm_ids_update)
            if gon_client_update_package_id is not None:
                self._set_root(root_relocated, root_relocated, download_root=self._download_root)
                installer = lib.gpm.gpm_installer.GpmInstaller(self, root_relocated, root_relocated, error_handler, dictionary, cb)
                installer.init_tasks([gon_client_update_package_id], [], [])
            else:
                task_copy_ro = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(self.get_ro_root(), root_relocated, stat.S_IWRITE, dictionary._('Preparing installation'), cb, error_handler, dictionary)
                installer = lib.gpm.gpm_installer.GpmInstaller(self, root_relocated, root_relocated, error_handler, dictionary, cb)
                installer.init_tasks([], [], [], alien_pre_tasks=[task_copy_ro])
            installer.execute()

            restart_gon_client_install_state = self._set_install_state('relocated', cleanup_root)
            restart_gon_client_install_state.set_restart_root(root_relocated)
            restart_gon_client_install_state.set_offline_operation(True)
            if not error_handler.error():
                cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state)
        elif self._is_install_state_relocated(gon_client_install_state):
            org_root_rw = self.get_rw_root()
            cleanup_root = self._get_install_state_data(gon_client_install_state)
            temp_root_ro = os.path.join(cleanup_root, 'ro_root_copy')
            temp_root_rw = os.path.join(cleanup_root, 'rw_root_copy')
            os.mkdir(temp_root_ro)
            os.mkdir(temp_root_rw)

            if not (knownsecret is None and servers is None):
                knownsecret_current, servers_current = self.get_connection_info()
                if knownsecret is None:
                    knownsecret = knownsecret_current
                if servers is None:
                    servers = servers_current

            # Create copy of rw and ro
            task_copy_ro = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(self.get_ro_root(), temp_root_ro, stat.S_IWRITE, dictionary._('Preparing installation'), cb, error_handler, dictionary)
            task_copy_rw = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(self.get_rw_root(), temp_root_rw, None, dictionary._('Preparing installation'), cb, error_handler, dictionary)
            installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root_ro, temp_root_rw, error_handler, dictionary, cb)
            installer.init_tasks([], [], [], alien_pre_tasks=[task_copy_ro, task_copy_rw])
            installer.execute()
            
            # Upgrade copy
            saved_ro_root = self.get_ro_root()
            saved_rw_root = self.get_rw_root()
            self._set_root(temp_root_ro, temp_root_rw)
            task_create_and_burn_iso = AlienTaskCreateAndBurn(self._gpm_meta_root, temp_root_ro, gpm_ids_install, gpm_ids_update, gpm_ids_remove, cb, error_handler, dictionary, cleanup_root, knownsecret, servers, generate_keypair=False)
            task_cleanup = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([self.get_download_root()], [], dictionary._('Cleaning download cache'), cb, error_handler, dictionary)
            task_remount = AlienTaskRemount(saved_ro_root, saved_rw_root, 10, cb, error_handler, dictionary)
            task_copy_rw = lib.gpm.gpm_installer.GpmInstallerTaskCopyFolder(temp_root_rw, org_root_rw, None, dictionary._('Restoring flash drive'), cb, error_handler, dictionary)
            task_remount_2 = AlienTaskRemount(saved_ro_root, saved_rw_root, 10, cb, error_handler, dictionary)
            installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root_ro, temp_root_rw, error_handler, dictionary, cb)
            installer.init_tasks(gpm_ids_install, gpm_ids_update, gpm_ids_remove, alien_post_tasks=[task_create_and_burn_iso, task_remount, task_cleanup, task_copy_rw, task_remount_2])
            installer.execute()
            restart_gon_client_install_state = self._set_install_state('cleanup', cleanup_root)
            restart_gon_client_install_state.set_offline_operation(True)
            if not error_handler.error():
                cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state, do_restart_but_skip_launch=True, wait_for_device=saved_ro_root)
        elif self._is_install_state_cleanup(gon_client_install_state):
            cleanup_root = self._get_install_state_data(gon_client_install_state)
            task_cleanup = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([cleanup_root], [], dictionary._('Cleaning up temporary files'), cb, error_handler, dictionary)
            installer = lib.gpm.gpm_installer.GpmInstaller(self, self.get_ro_root(), self.get_rw_root(), error_handler, dictionary, cb)
            installer.init_tasks([], [], [], [], [task_cleanup])
            installer.execute()
            restart_gon_client_install_state = lib.appl.gon_client.GOnClientInstallState(done=True)
            if not error_handler.error():
                cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state)
        else:
            error_handler.emmit_error('Invalid restart state, try to run update again')

    def _install_gpms_no_restart(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, cb, gon_client_install_state, error_handler, dictionary):
        cleanup_root = tempfile.mkdtemp()
        temp_root_ro = os.path.join(cleanup_root, 'ro_root')
        os.mkdir(temp_root_ro)
        task_cleanup_cache = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([self.get_download_root()], [], dictionary._('Cleaning download cache'), cb, error_handler, dictionary)
        task_cleanup_temp = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([cleanup_root], [], dictionary._('Cleaning download cache'), cb, error_handler, dictionary)
        installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root_ro, self.get_rw_root(), error_handler, dictionary, cb)
        installer.init_tasks(gpm_ids_install, gpm_ids_update, gpm_ids_remove, alien_post_tasks=[task_cleanup_temp, task_cleanup_cache])
        installer.execute()

    def get_installed_gpm_meta_all(self, error_handler):
        return lib.gpm.gpm_builder.query_meta_all_from_files(self._gpm_meta_root, error_handler)

    def get_installed_gpm_meta(self, gpm_id, error_handler):
        return lib.gpm.gpm_builder.query_meta_from_from_file(self._gpm_meta_root, gpm_id, error_handler)

    def remove_installed_gpm_meta(self, gpm_id, error_handler):
        lib.gpm.gpm_builder.remove_meta(self._gpm_meta_root, gpm_id, error_handler)

    def install_gpm_meta(self, gpm_filename, error_handler):
        if not os.path.exists(self._gpm_meta_root):
            os.makedirs(self._gpm_meta_root)
        lib.gpm.gpm_builder.install_meta(self._gpm_meta_root, gpm_filename, error_handler)

    def get_download_gpm_filename(self, gpm_id):
        gpm_filename_abs = os.path.join(self._download_root, '%s.gpm' % gpm_id)
        return gpm_filename_abs
    
    def get_ro_root(self):
        return self._ro_root

    def get_rw_root(self):
        return self._rw_root

    def get_root(self):
        return self._ro_root

    def get_temp_root(self):
        temp_root = os.path.join(self.get_rw_root(), 'gon_client', 'gon_temp')
        if not os.path.exists(temp_root):
            os.makedirs(temp_root)
        return temp_root

    def is_in_use(self, timeout=None):
        """
        Expected to return true if this client runtime environment is in use at the moment.
        The timeout attribute indicate when a mark is no longer valid
        """
        in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
        return in_use_mark.is_in_use(timeout)

    def mark_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return true.
        """
        in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
        in_use_mark.do_mark_in_use()
        
    def mark_not_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return false.
        """
        in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
        in_use_mark.do_mark_not_in_use()

    def support_update_connection_info(self, dictionary):
        """
        Examine if the connection info can be updated.
        
        Expected to return a dict with the following content:
          {'supported': bool, 'supported_by_installation':bool, 'message': String}
        """
        if ctypes.windll.shell32.IsUserAnAdmin():
            return {'supported': False, 'supported_by_installation':True, 'message': 'Ok'}
        return {'supported': False, 'supported_by_installation':False, 'message': dictionary._('Connection information has changed, but can not be updated on your key. Please start the G/On Client with Administrator privileges, in order to update the connection information.')}

    def update_connection_info(self, servers, knownsecret, error_handler):
        """
        Expected to update the connection info
        """
        raise NotImplementedError

    def get_connection_info(self):
        (rc, message, knownsecret, servers) = lib_cpp.hagi.key_get_knownsecret_and_servers();
        if not rc:
            self.checkpoint_handler.Checkpoint('get_knownsecret_and_servers.error', self.plugin_name, lib.checkpoint.ERROR, message=message)
        return (knownsecret, servers)
