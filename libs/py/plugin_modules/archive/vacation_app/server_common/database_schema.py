from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api



dbapi = schema_api.SchemaFactory.get_creator("plugin_vacation_app")

table_user = dbapi.create_table("user",
                                 schema_api.Column('user_id', schema_api.String(256), nullable=False),
                                 schema_api.Column('first_month', schema_api.Integer, default=5),
                                 schema_api.Column('vacation_days', schema_api.Integer, default=30),
                                 )

table_user_vacation_year = dbapi.create_table("user_vacation_year",
                                         schema_api.Column('user_id', schema_api.String(256), nullable=False),
                                         schema_api.Column('vacation_year', schema_api.Integer, nullable=False),
                                         schema_api.Column('first_month', schema_api.Integer, default=5),
                                         schema_api.Column('vacation_days', schema_api.Integer, default=30),
                                         )

table_user_vacation_day = dbapi.create_table("user_vacation_days",
                                         schema_api.Column('user_vacation_year_id', schema_api.Integer, nullable=False),
                                         schema_api.Column('user_id', schema_api.String(256), nullable=False),
                                         schema_api.Column('vacation_year', schema_api.Integer, nullable=False),
                                         schema_api.Column('year', schema_api.Integer, nullable=False),
                                         schema_api.Column('month', schema_api.Integer, nullable=False),
                                         schema_api.Column('day', schema_api.Integer, nullable=False),
                                   )




dbapi.add_foreign_key_constraint(table_user_vacation_day, 'user_vacation_year_id', table_user_vacation_year)

dbapi.add_unique_constraint(table_user_vacation_day, "user_id", "year", "month", "day")

schema_api.SchemaFactory.register_creator(dbapi)

class User(object):
    pass

class UserVacationYear(object):
    pass

class UserVacationDay(object):
    pass


schema_api.mapper(UserVacationDay, table_user_vacation_day)
schema_api.mapper(UserVacationYear, table_user_vacation_year, relations=[database_api.Relation(UserVacationDay, 'reported_vacation_days')])
schema_api.mapper(User, table_user)
#schema_api.mapper(AddressElement, table_adress_element, relations=[database_api.Relation(IpRange, 'ip_ranges', backref_property_name='address_element')])

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)
