"""
Audit plugin
"""
from __future__ import with_statement
import sys
import os.path
import datetime
import json
import urlparse
import threading
import cgi
import base64

import lib.checkpoint

from plugin_types.server_gateway import plugin_type_web_app
from components.database.server_common import database_api
import components.database.server_common.connection_factory
import plugin_modules.vacation_app.server_common.database_schema as database_schema


class PluginVacationApp(plugin_type_web_app.PluginTypeWebApp):
    

    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_web_app.PluginTypeWebApp.__init__(self, async_service, checkpoint_handler, u"vacation_app", license_handler, session_info)
        self.user_id = None

    def get_user_id(self):
        if self.user_id is None:
            if self.user_session_callback:
                self.user_id = self.user_session_callback.get_user_id()
            else:
                self.user_id = "-1"
        return self.user_id
            


    def _get_user_record(self, user_record, user_id, dbs):
        if not user_record:
            user_record = dbs.select_first(database_schema.User, filter=database_schema.User.user_id == user_id)
            if not user_record:
                user_record = database_schema.User()
                user_record.vacation_days = 30
        return user_record

    def authorize(self, base_http_server):
        if self.user_id is None:
            self.user_id = self.user_session_callback.get_user_id()
        if not self.user_id is None:
            return True
        basic_auth_string = self.user_session_callback.get_basic_auth_string(base_http_server)
        if basic_auth_string:
            username_password = base64.decodestring(basic_auth_string)
            username, password = username_password.split(":")
            if username == "_DEMO_":
                self.user_id = -1
            else:
                self.user_id = self.user_session_callback.validate_user(username, password)

        if self.user_id:
            return True
        else:
            base_http_server.do_AUTHHEAD()
            return False
        

    def do_GET(self, base_http_server):
        if not self.authorize(base_http_server):
            return 
        try:
            url_obj = urlparse.urlparse(base_http_server.path)
    
            path_elements = url_obj.path.split('/')
            if len(path_elements) < 4:
                base_http_server.send_error(404, 'vacation_app function not found: %s' % base_http_server.path)
                return
    
            method_name = path_elements[3]
            print method_name
            
            if method_name == 'get_vacation':
                args = urlparse.parse_qs(url_obj.query)
                year = args.get('year', [None])[0]
                if not year:
                    year = datetime.datetime.now().year
                year = int(year)
                user_id = self.get_user_id()
                with database_api.QuerySession() as dbs:
                    vacation_days = dbs.select(database_schema.UserVacationDay, 
                                               filter=database_api.and_(database_schema.UserVacationDay.user_id==user_id,
                                                                        database_schema.UserVacationDay.vacation_year==year))
                reply = {
                    "success": True,
                    "dates" : [{    "day" : d.day,
                                    "month" : d.month,
                                    "year" : d.year,
                                } for d in vacation_days]
                    }
                
                base_http_server._send_json_response(reply)            
                return
            elif method_name == 'get_vacation_years':
                user_id = self.get_user_id()
                with database_api.QuerySession() as dbs:
                    vacation_years = dbs.select(database_schema.UserVacationYear, 
                                                database_schema.UserVacationYear.user_id==user_id, 
                                                order_by = database_schema.UserVacationYear.vacation_year)
                    used_vacation_years = dbs.select_count(database_schema.UserVacationDay,
                                                     filter=database_schema.UserVacationDay.user_id==user_id, 
                                                     group_by=database_schema.UserVacationDay.vacation_year)
                    used_vacation_years_dict = dict([(x.vacation_year, x.count_1) for x in used_vacation_years ])
                    
                current_year = datetime.datetime.now().year
                mandatory_years = [current_year, current_year+1]
                user_record = None
                result = []
                for vacation_year in vacation_years:
                    if not mandatory_years or vacation_year.vacation_year <= mandatory_years[0]:
                        used_days = used_vacation_years_dict.get(vacation_year.vacation_year, 0)
                        result.append({    "year" : vacation_year.vacation_year,
                                            "days_left" : vacation_year.vacation_days-used_days,
                                            "max_days" : vacation_year.vacation_days
                                })
                        if mandatory_years and vacation_year.vacation_year == mandatory_years[0]:
                            mandatory_years.pop(0)
                    else:
                        user_record = self._get_user_record(user_record, user_id, dbs)
                        
                        result.append({    "year" : mandatory_years[0],
                                            "days_left" : user_record.vacation_days,
                                            "max_days" : user_record.vacation_days
                                })
                        mandatory_years.pop(0)
                for mandatory_year in mandatory_years:
                    user_record = self._get_user_record(user_record, user_id, dbs)
                    result.append({    "year" : mandatory_year,
                                        "days_left" : user_record.vacation_days,
                                        "max_days" : user_record.vacation_days
                            })
                    
                    
                reply = {
                    "success": True,
                    "years" : result
                    }
                
                base_http_server._send_json_response(reply)            
                return
            else:
                raise Exception("Unknown GET method '%s'" % method_name)
        
        
        except Exception, e:
            base_http_server.send_error(404, repr(e))
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("doGET.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
        

    def _calc_vacation_year(self, date_dict,  first_month):
        year = date_dict.get("year")
        month = date_dict.get("month")
        if month < first_month:
            return year-1
        else:
            return year
        

    def _get_user(self, user_id, dbt):
        user = dbt.select_first(database_schema.User, filter=database_schema.User.user_id == user_id)
        if not user:
            user = dbt.add(database_schema.User())
            user.user_id = user_id
        return user

    def get_user_vacation_year(self, user_record, vacation_year, dbt):
        user_vacation_year = dbt.select_first(database_schema.UserVacationYear, 
                                              filter=database_api.and_(database_schema.UserVacationYear.user_id == user_record.user_id,
                                                                       database_schema.UserVacationYear.vacation_year == vacation_year,
                                                                       )
                                              )
        if not user_vacation_year:
            user_vacation_year = dbt.add(database_schema.UserVacationYear())
            user_vacation_year.user_id = user_record.user_id
            user_vacation_year.vacation_year = vacation_year
            user_vacation_year.first_month = user_record.first_month
            user_vacation_year.vacation_days = user_record.vacation_days
        return user_vacation_year
    
    def do_POST(self, base_http_server):
        if not self.authorize(base_http_server):
            return 
        try:
            url_obj = urlparse.urlparse(base_http_server.path)
    
            path_elements = url_obj.path.split('/')
            if len(path_elements) < 4:
                base_http_server.send_error(404, 'vacation_app function not found: %s' % base_http_server.path)
                return
    
            method_name = path_elements[3]
            print method_name
    
            ctype, pdict = cgi.parse_header(base_http_server.headers.getheader('content-type'))
            if ctype == 'multipart/form-data':
                postvars = cgi.parse_multipart(base_http_server.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(base_http_server.headers.getheader('content-length'))
                postvars = cgi.parse_qs(base_http_server.rfile.read(length), keep_blank_values=1)
            else:
                 postvars = {}
    
            print postvars
            
            if method_name == 'add_vacation':
    
                from_date = datetime.datetime.strptime(postvars.get("fromDate")[0], "%Y-%m-%dT%H:%M:%S").date()
                to_date = datetime.datetime.strptime(postvars.get("toDate")[0], "%Y-%m-%dT%H:%M:%S").date()
                if from_date > to_date:
                    base_http_server.send_error(405, 'Invalid date period %s-%s' % (from_date, to_date))
                    return
                
                new_dates = []
                one_day = datetime.timedelta(1)
                while from_date <= to_date:
                    weekday = from_date.weekday()
                    if weekday < 5:
                        new_dates.append({    
                                            "day" : from_date.day,
                                            "month" : from_date.month,
                                            "year" : from_date.year,
                                        })
                    from_date += one_day 
    
                user_id = self.get_user_id()
                return_dates = []
                with database_api.Transaction() as dbt:
                    user_record = self._get_user(user_id, dbt)
                    for date_dict in new_dates:
                        vacation_year = self._calc_vacation_year(date_dict, user_record.first_month)
                        vacation_year_rec = self.get_user_vacation_year(user_record, vacation_year, dbt)
                        
                        existing = dbt.select_first(database_schema.UserVacationDay, 
                                                    filter=database_api.and_(database_schema.UserVacationDay.user_id==user_id,
                                                                             database_schema.UserVacationDay.year==date_dict.get("year"),
                                                                             database_schema.UserVacationDay.month==date_dict.get("month"),
                                                                             database_schema.UserVacationDay.day==date_dict.get("day"),
                                                                             )
                                                    )
                        if not existing:
                            vacation_date = database_schema.UserVacationDay()
                            vacation_date.day = date_dict.get("day")
                            vacation_date.month = date_dict.get("month")
                            vacation_date.year = date_dict.get("year")
                            vacation_date.user_id = user_id
                            vacation_date.vacation_year = vacation_year
                            
                            vacation_year_rec.reported_vacation_days.append(vacation_date)
                            date_dict['vacation_year'] = vacation_year
                            return_dates.append(date_dict)
    
                
                reply = {
                        "success": True,
                        "dates": return_dates
                        }
                
                base_http_server._send_json_post_response(reply)
            elif method_name == 'delete_vacation':
                if postvars:
                    with database_api.Transaction() as dbt:
                        vaction_days = dbt.select(database_schema.UserVacationDay, 
                                                  filter=database_api.and_(database_schema.UserVacationDay.day == postvars.get("day")[0],
                                                            database_schema.UserVacationDay.month == postvars.get("month")[0],
                                                            database_schema.UserVacationDay.year == postvars.get("year")[0],
                                                            ))
                        dbt.delete_sequence(vaction_days)
            
                base_http_server._send_json_post_response("")
                            
            else:
                raise Exception("Unknown POST method '%s'" % method_name)
                
            
        except Exception, e:
            base_http_server.send_error(404, repr(e))
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("do_POST.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)

#            reply = {
#                    "success": True,
#                    "dates": [
#                        { "day" : 2,
#                          "month" : 11,
#                          "year" : 2012,
#                          "vacation_year" : 2012
#                        },
#                        { "day" : 2,
#                          "month" : 3,
#                          "year" : 2013,
#                          "vacation_year" : 2012
#                        },
#                        { "day" : 4,
#                          "month" : 5,
#                          "year" : 2012,
#                          "vacation_year" : 2013
#                        },
#                        ]
#                    }
#    
#            base_http_server._send_json_post_response(reply);
