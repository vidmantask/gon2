"""
Common functionality for SoftToken on Gateway Client level
"""
from __future__ import with_statement

import sys
import os
import os.path
import marshal
import uuid
import shutil

import lib.checkpoint
import lib.cryptfacility
import lib.config
import lib.utility

import lib.gpm.gpm_env

import plugin_modules.endpoint
from plugin_types.client_gateway import plugin_type_token

# Got this from shutil.py (WindowsError is not defined on other platforms that windows)
try:
    WindowsError
except NameError:
    WindowsError = None


def get_token_title(installation_location=None):
    user_folder = os.path.basename(lib.utility.get_home_folder())
    if installation_location is not None:
        installation_location_folder = os.path.basename(installation_location)
        return "ComputerUserToken '%s' on local account '%s'" % (installation_location_folder, user_folder)
    return "ComputerUserToken on local account '%s'" % user_folder



class EngpointGInfoLocations(object):
    def __init__(self):
        self.reg_key_root = None
        self.reg_key_base = None
        self.key_folder = None
    
    def get_reg_key_root(self):
        return self.reg_key_root

    def get_reg_key_base(self):
        return self.reg_key_base

    def get_key_folder(self):
        return self.key_folder

    @classmethod
    def create_endpoint(cls):
        endpoint_ginfo_locations = EngpointGInfoLocations()
        endpoint_ginfo_locations.reg_key_root = "SOFTWARE\Giritech\G-On Client\Endpoint"
        if sys.platform == 'win32':
            import _winreg as winreg 
            endpoint_ginfo_locations.reg_key_base = winreg.HKEY_CURRENT_USER
        endpoint_ginfo_locations.key_folder = os.path.join(lib.utility.get_home_folder(), '.giritech', 'ginfo')
        return endpoint_ginfo_locations

    @classmethod
    def create_computer_token(cls):
        endpoint_ginfo_locations = EngpointGInfoLocations()
        endpoint_ginfo_locations.reg_key_root = "SOFTWARE\Giritech\G-On Client\ComputerToken"
        if sys.platform == 'win32':
            import _winreg as winreg 
            endpoint_ginfo_locations.reg_key_base = winreg.HKEY_LOCAL_MACHINE
        endpoint_ginfo_locations.key_folder = os.path.join(lib.utility.get_home_folder(), '.giritech', 'ginfo')
#        endpoint_ginfo_locations.key_folder = os.path.join('/','etc', 'giritech', 'ginfo')
        return endpoint_ginfo_locations


class EndpointGInfo(object):
    def __init__(self, ginfo_locations=None):
        self.ginfo_locations = ginfo_locations
        if self.ginfo_locations is None:
            self.ginfo_locations = EngpointGInfoLocations.create_endpoint()
            
    def write(self, serial, key, value):
        """
        If the write is ok None is returned, else would a error message be returned
        """
        if sys.platform == 'win32':
            self._write_win(serial, key, value)
        elif sys.platform == 'linux2':
            self._write_unix(serial, key, value)
        elif sys.platform == 'darwin':
            self._write_unix(serial, key, value)

    def _write_win(self, serial, key, value):
        import _winreg as winreg 
        with winreg.ConnectRegistry(None, self.ginfo_locations.get_reg_key_base()) as reg_handler:
            try:
                reg_key = winreg.CreateKey(reg_handler, '%s\\%s' % (self.ginfo_locations.get_reg_key_root(), serial))
                winreg.SetValueEx(reg_key, key, 0, winreg.REG_SZ, value)
                winreg.CloseKey(reg_key)        
            except WindowsError:
                (etype, evalue, etrace) = sys.exc_info()
                return evalue
        return None

    def _write_unix(self, serial, key, value):
        try:
            reg_foldername = os.path.join(self.ginfo_locations.get_key_folder(), serial)
            reg_key_filename = os.path.join(reg_foldername, key)
            if not os.path.isdir(reg_foldername):
                os.makedirs(reg_foldername)
            reg_key_file = open(reg_key_filename, 'wb')
            reg_key_file.write(marshal.dumps(value))
            reg_key_file.close()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            return evalue
        return None

    def read(self, serial, key, default):
        if sys.platform == 'win32':
            return self._read_win(serial, key, default)
        elif sys.platform == 'linux2':
            return self._read_unix(serial, key, default)
        elif sys.platform == 'darwin':
            return self._read_unix(serial, key, default)
        
    def _read_win(self, serial, key, default):
        import _winreg as winreg 
        try:
            with winreg.ConnectRegistry(None, self.ginfo_locations.get_reg_key_base()) as reg_handler:
                with winreg.OpenKey(reg_handler, '%s\\%s' % (self.ginfo_locations.get_reg_key_root(), serial)) as reg_key_ginfo:
                    (key_value, key_value_type) = winreg.QueryValueEx(reg_key_ginfo, key)
                    return key_value
        except WindowsError:
            (etype, evalue, etrace) = sys.exc_info()
            return default
        return default
    
    def _read_unix(self, serial, key, default):
        try:
            reg_key_filename = os.path.join(self.ginfo_locations.get_key_folder(), serial, key)
            reg_key_file = open(reg_key_filename, 'rb')
            reg_key_value = marshal.loads(reg_key_file.read())
            reg_key_file.close()
            return reg_key_value
        except:
            return default
        return default

    def remove(self, serial, key=None):
        if sys.platform == 'win32':
            self._remove_win(serial, key)
        elif sys.platform == 'linux2':
            self._remove_unix(serial, key)
        elif sys.platform == 'darwin':
            self._remove_unix(serial, key)
        
    def _remove_win(self, serial, key):
        import _winreg as winreg 
        try:
            with winreg.ConnectRegistry(None, self.ginfo_locations.get_reg_key_base()) as reg_handler:
                if key is None:
                    with winreg.OpenKey(reg_handler, self.ginfo_locations.get_reg_key_root(), 0, winreg.KEY_SET_VALUE) as reg_key_ginfo:
                        winreg.DeleteKey(reg_key_ginfo, serial)
                else:
                    with winreg.OpenKey(reg_handler, '%s\\%s' % (self.ginfo_locations.get_reg_key_root(), serial), 0, winreg.KEY_SET_VALUE) as reg_key_ginfo:
                        winreg.DeleteValue(reg_key_ginfo, key)
        except WindowsError:
            (etype, evalue, etrace) = sys.exc_info()
            print evalue
            pass
        
    def _remove_unix(self, serial, key):
        try:
            reg_serial_folder = os.path.join(self.ginfo_locations.get_key_folder(), serial)
            if key is None:
                if os.path.isdir(reg_serial_folder):
                    lib.utility.rmtree_fail_safe(reg_serial_folder)
                    os.rmdir(reg_serial_folder)
            else:
                reg_key_filename = os.path.join(reg_serial_folder, key)
                if os.path.isfile(reg_key_filename):
                    os.remove(reg_key_filename)
        except:
            pass

    def lookup_serial(self, key, value, default):
        if sys.platform == 'win32':
            return self._lookup_serial_win(key, value, default)
        elif sys.platform == 'linux2':
            return self._lookup_serial_unix(key, value, default)
        elif sys.platform == 'darwin':
            return self._lookup_serial_unix(key, value, default)

    def _lookup_serial_win(self, key, value, default):
        try:
            import _winreg as winreg 
            with winreg.OpenKey(self.ginfo_locations.get_reg_key_base(), self.ginfo_locations.get_reg_key_root()) as reg_key_serials:
                reg_key_index = 0
                while True:
                    reg_serial = winreg.EnumKey(reg_key_serials, reg_key_index)
                    try:
                        reg_key_serial = winreg.OpenKey(reg_key_serials, reg_serial)
                        (reg_key_serial_value, reg_key_serial_value_type) = winreg.QueryValueEx(reg_key_serial, key)
                        if reg_key_serial_value == value:
                            return reg_serial
                    except WindowsError:
                        pass
                    reg_key_index += 1
        except:
            pass
        return default
            
    def _lookup_serial_unix(self, key, value, default):
        reg_serial_folder_root = self.ginfo_locations.get_key_folder()
        if  os.path.isdir(reg_serial_folder_root):
            for reg_serial_folder in os.listdir(reg_serial_folder_root):
                reg_serial_folder_abs = os.path.join(reg_serial_folder_root, reg_serial_folder)
                if os.path.isdir(reg_serial_folder_abs):
                    if value == self._read_unix(reg_serial_folder, key, None):
                        return reg_serial_folder
        return default

    def lookup_serials(self, key, default):
        if sys.platform == 'win32':
            return self._lookup_serials_win(key, default)
        elif sys.platform == 'linux2':
            return self._lookup_serials_unix(key, default)
        elif sys.platform == 'darwin':
            return self._lookup_serials_unix(key, default)

    def _lookup_serials_win(self, key, default):
        serials = []
        try:
            import _winreg as winreg 
            with winreg.OpenKey(self.ginfo_locations.get_reg_key_base(), self.ginfo_locations.get_reg_key_root()) as reg_key_serials:
                reg_key_index = 0
                while True:
                    reg_serial = winreg.EnumKey(reg_key_serials, reg_key_index)
                    try:
                        reg_key_serial = winreg.OpenKey(reg_key_serials, reg_serial)
                        (reg_key_serial_value, reg_key_serial_value_type) = winreg.QueryValueEx(reg_key_serial, key)
                        serials.append( (reg_serial, reg_key_serial_value) )
                    except WindowsError:
                        pass
                    reg_key_index += 1
        except:
            pass
        return serials

    def _lookup_serials_unix(self, key, default):
        serials = []
        reg_serial_folder_root = self.ginfo_locations.get_key_folder()
        if os.path.exists(reg_serial_folder_root):
            for reg_serial_folder in os.listdir(reg_serial_folder_root):
                reg_serial_folder_abs = os.path.join(reg_serial_folder_root, reg_serial_folder)
                if os.path.isdir(reg_serial_folder_abs):
                    serials.append((reg_serial_folder_abs, self._read_unix(reg_serial_folder, key, None)))
        return serials

    def check_access(self):
        try:
            org_value = 'check_access_value'
            self.write('check_access_serial', 'check_access_key', org_value)
            value = self.read('check_access_serial', 'check_access_key', None)
            return org_value == value
        except:
            pass
        return False
    

class EndpointToken(object):
    REG_KEY_INSTALLATION_LOCATION = 'installation_location'
    REG_KEY_CLIENT_KNOWNSECRET = 'client_knownsecret'
    REG_KEY_SERVERS = 'servers'
    REG_KEY_PUBLIC_KEY = 'public_key'
    REG_KEY_PRIVATE_KEY = 'private_key'
    REG_KEY_SHORTCUTS = 'shortcuts'
    REG_KEY_ENROLLED = 'enrolled'
    
    def __init__(self, checkpoint_handler, plugin_name, ginfo_locations=None):
        self.checkpoint_handler = checkpoint_handler
        self.plugin_name = plugin_name
        self.endpoint_ginfo = EndpointGInfo(ginfo_locations=ginfo_locations)
        
    def _lookup_serial(self, installation_location, throw_on_error=True):
        serial = self.endpoint_ginfo.lookup_serial(EndpointToken.REG_KEY_INSTALLATION_LOCATION, installation_location, None)
        if serial is None:
            if throw_on_error:
                raise plugin_type_token.Error("Endpoint token '%s' not found" % installation_location)
            return None
        return serial
   
    def do_exist(self, installation_location):
        serial = self.endpoint_ginfo.lookup_serial(EndpointToken.REG_KEY_INSTALLATION_LOCATION, installation_location, None)
        if serial is not None:
            return True
        return False
   
    def remove_token(self, installation_location):
        serial = self.endpoint_ginfo.lookup_serial(EndpointToken.REG_KEY_INSTALLATION_LOCATION, installation_location, None)
        if serial is not None:
            self.endpoint_ginfo.remove(serial)
    
    def initialize_token(self, installation_location):
        self._initialize_token(installation_location)
        
    def _initialize_token(self, installation_location):
        shortcuts = None
        
        old_serial = self.endpoint_ginfo.lookup_serial(EndpointToken.REG_KEY_INSTALLATION_LOCATION, installation_location, None)
        if old_serial is not None:
            shortcuts = self.endpoint_ginfo.read(old_serial, EndpointToken.REG_KEY_SHORTCUTS, None)
            self.endpoint_ginfo.remove(old_serial)

        serial_new = '%s' % uuid.uuid4()
        rc = self.endpoint_ginfo.write(serial_new, EndpointToken.REG_KEY_INSTALLATION_LOCATION, installation_location)
        if rc is not None:
            raise plugin_type_token.Error(rc)
        if shortcuts is not None:
            rc = self.endpoint_ginfo.write(serial_new, EndpointToken.REG_KEY_SHORTCUTS, shortcuts)
            if rc is not None:
                raise plugin_type_token.Error(rc)

    def deploy_token(self, installation_location, client_knownsecret, servers):
        serial = self._lookup_serial(installation_location)
        rc = self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_CLIENT_KNOWNSECRET, client_knownsecret)
        if rc is not None:
            raise plugin_type_token.Error(rc)
        rc = self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_SERVERS, servers)
        if rc is not None:
            raise plugin_type_token.Error(rc)

    def generate_keypair(self, installation_location):
        serial = self._lookup_serial(installation_location)
        (public_key, private_key) = lib.cryptfacility.pk_generate_keys()
        rc = self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_PUBLIC_KEY, public_key)
        if rc is not None:
            raise plugin_type_token.Error(rc)
        rc = self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_PRIVATE_KEY, private_key)
        if rc is not None:
            raise plugin_type_token.Error(rc)

    def get_public_key(self, installation_location):
        serial = self._lookup_serial(installation_location)
        public_key = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_PUBLIC_KEY, None)
        if public_key is None:
            raise plugin_type_token.Error("No public-key is available for the endpoint")
        return str(public_key)

    def get_private_key(self, installation_location):
        serial = self._lookup_serial(installation_location)
        private_key = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_PRIVATE_KEY, None)
        if private_key is None:
            raise plugin_type_token.Error("No private-key is available for the endpoint")
        return str(private_key)

    def get_serial(self, installation_location, throw_on_error=True):
        return self._lookup_serial(installation_location, throw_on_error)

    def set_serial(self, installation_location, serial):
        # New serial is generated
        self._initialize_token(installation_location)

    def set_enrolled(self, installation_location):
        serial = self._lookup_serial(installation_location)
        rc = self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_ENROLLED, 'x')
        if rc is not None:
            raise plugin_type_token.Error(rc)

    def reset_enrolled(self, installation_location):
        serial = self._lookup_serial(installation_location)
        self.endpoint_ginfo.remove(serial, EndpointToken.REG_KEY_ENROLLED)

    def is_enrolled(self, installation_location):
        serial = self._lookup_serial(installation_location)
        enrolled = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_ENROLLED, None)
        return enrolled is not None
        
    def _get_knownsecret(self, installation_location):
        serial = self._lookup_serial(installation_location, False)
        if serial is not None:
            knownsecret = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_CLIENT_KNOWNSECRET, None)
            if knownsecret is not None:
                knownsecret = str(knownsecret)
            return knownsecret
        return None
    
    def _get_servers(self, installation_location):
        serial = self._lookup_serial(installation_location, False)
        if serial is not None:
            servers = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_SERVERS, None)
            if servers is not None:
                servers = str(servers)
            return servers
        return None
    
    def get_knownsecret_and_servers(self, installation_location):
        return (self._get_knownsecret(installation_location), self._get_servers(installation_location))

    def set_servers(self, installation_location, servers):
        serial = self._lookup_serial(installation_location, False)
        if serial is not None:
            return self.endpoint_ginfo.write(serial, EndpointToken.REG_KEY_SERVERS, servers)
        return "Endpoint token '%s' not found" % installation_location

    def get_tokens(self, additional_device_roots, runtime_env_plugin_name):
        serials = self.endpoint_ginfo.lookup_serials(EndpointToken.REG_KEY_INSTALLATION_LOCATION, None)
        tokens = []
        for (serial, installation_location) in serials:
            if installation_location is not None and os.path.exists(installation_location):
                token_title = get_token_title(installation_location)
                token = plugin_type_token.Token(self.plugin_name, installation_location, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, None, token_type_title="ComputerUserToken", token_title=token_title, token_plugin_module_name='endpoint')
                token.token_serial = serial
                token.runtime_env_id = "%s::%s" %(runtime_env_plugin_name, installation_location)
                enrolled = self.endpoint_ginfo.read(serial, EndpointToken.REG_KEY_ENROLLED, None)
                if enrolled is not None:
                    token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED
                tokens.append(token)
        return tokens

    def get_installation_locations(self):
        installation_locations = []
        serials = self.endpoint_ginfo.lookup_serials(EndpointToken.REG_KEY_INSTALLATION_LOCATION, None)
        for (serial, installation_location) in serials:
            if installation_location is not None and os.path.isdir(installation_location):
                installation_locations.append(installation_location)
        return installation_locations

    def check_access(self):
        return self.endpoint_ginfo.check_access()
    
    

if __name__ == '__main__':
    ginfo = EndpointGInfo(ginfo_locations=EngpointGInfoLocations.create_endpoint())
    print "check_access:", ginfo.check_access()
    
    ginfo.write('test_serial', 'hej', 'ducc')
    print ginfo.read('test_serial', 'hej', None)
#
#    print ginfo.lookup_serial('hej','ducc', None)
#    
##    ginfo.remove('test_serial')
##    print ginfo.read('test_serial', 'hej', None)
#    
#    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
#    endpoint_token = EndpointToken(checkpoint_handler, 'xx')
#    print endpoint_token.get_installation_locations()

#    endpoint = EndpointToken(checkpoint_handler, 'hej')
#    endpoint.initialize_token(u"C:\\Documents and Settings\\twa.DEV-BUILD-XP\\Application Data\\Giritech\\G-On Client(6)")
