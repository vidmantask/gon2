"""
Unittest of Endpoint plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility


from plugin_types.client_gateway import plugin_type_token
import plugin_types.client_gateway.plugin_type_client_runtime_env

import components.database.server_common.database_api as database_api  


from components.communication.sim import sim_tunnel_endpoint
from components.database.server_common.connection_factory import ConnectionFactory


import plugin_modules.endpoint.server_gateway
import plugin_modules.endpoint.client_gateway
from plugin_types.common import plugin_type_token as database_model 
import plugin_modules.endpoint.client_gateway_common
import plugin_modules.endpoint.client_gateway

import components.management_message.server_gateway.session_send

PLUGIN_NAME = u'endpoint_token'


SERVERS = """
<?xml version='1.0'?>
<servers>
  <connection_group title='Default' selection_delay_sec='1'>
   <connection title='Direct-Connection' host='127.0.0.1' port='13945' timeout_sec='10' type='direct' />
   <connection title='Direct-Connection' host='127.0.0.1' port='13945' timeout_sec='10' type='direct' />
<!-- 
    <connection title='HTTP-Connection hej'   host='127.0.0.1' port='13946' timeout_sec='30' type='http' />
-->
  </connection_group>
</servers>
"""

class Deployment(unittest.TestCase):
    def setUp(self):
        self.client_knownsecret = "Hej du"
        
    def tearDown(self):
        pass

    def test_initialization_and_deploy(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)
        deploy_plugin = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), PLUGIN_NAME)
        deploy_plugin.initialize_token(installation_location)
        tokens = deploy_plugin.get_tokens([], runtime_env_instance)
        self.assertTrue(len(tokens) > 0)
        self.assertTrue(tokens[0].token_serial != None)
        token_serial = tokens[0].token_serial

        deploy_plugin.deploy_token(installation_location, self.client_knownsecret, SERVERS)
        tokens = deploy_plugin.get_tokens([], runtime_env_instance)
        self.assertTrue(len(tokens) > 0)
        self.assertEqual(tokens[0].token_serial, token_serial)

    def test_gets(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)
        deploy_plugin = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), PLUGIN_NAME)
        deploy_plugin.initialize_token(installation_location)
        self.assertRaises(plugin_type_token.Error, lambda: deploy_plugin.get_public_key(runtime_env_instance))
        self.assertRaises(plugin_type_token.Error, lambda: deploy_plugin.get_private_key(runtime_env_instance))
        
        deploy_plugin.deploy_token(installation_location, self.client_knownsecret, SERVERS)
        
        (knownsecret, servers) = deploy_plugin.get_knownsecret_and_servers(installation_location)
        self.assertNotEqual(knownsecret, None)
        self.assertNotEqual(servers, None)

        deploy_plugin.generate_keypair(installation_location)
        self.assertNotEqual(deploy_plugin.get_public_key(installation_location), deploy_plugin.get_private_key(installation_location))
        self.assertNotEqual(deploy_plugin.get_public_key(installation_location), '')
        self.assertNotEqual(deploy_plugin.get_private_key(installation_location), '')
        self.assertNotEqual(deploy_plugin.get_public_key(installation_location), None)
        self.assertNotEqual(deploy_plugin.get_private_key(installation_location), None)

        self.assertFalse(deploy_plugin.is_enrolled(installation_location))
        deploy_plugin.set_enrolled(installation_location)
        self.assertTrue(deploy_plugin.is_enrolled(installation_location))
        deploy_plugin.reset_enrolled(installation_location)
        self.assertFalse(deploy_plugin.is_enrolled(installation_location))

        (knownsecret_after, servers_after) = deploy_plugin.get_knownsecret_and_servers(installation_location)
        self.assertEqual(knownsecret, knownsecret_after)
        self.assertEqual(servers, servers_after)


    def test_set_and_servers(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)
        deploy_plugin = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), PLUGIN_NAME)

        deploy_plugin.initialize_token(installation_location)
        deploy_plugin.deploy_token(installation_location, self.client_knownsecret, SERVERS)

        (knownsecret, servers_before) = deploy_plugin.get_knownsecret_and_servers(installation_location)
        error_message = deploy_plugin.set_servers(installation_location,  '127.0.0.1, 8044')
        self.assertEqual(error_message, None)

        (knownsecret, servers_after) = deploy_plugin.get_knownsecret_and_servers(installation_location)
        self.assertNotEqual(servers_before, servers_after)


    def test_update_connection_info(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)

        knownsecret_base = "xxx"
        servers_base = "123"
        deploy_plugin = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), PLUGIN_NAME)
        deploy_plugin.initialize_token(installation_location)
        deploy_plugin.deploy_token(installation_location, knownsecret_base, servers_base)

        dictionary = None
        support_update = runtime_env_instance.support_update_connection_info(dictionary)
        self.assertTrue(support_update['supported'])

        knownsecret_change = "xxxx"
        servers_change = "1234"
        error_handler = None
        runtime_env_instance.update_connection_info(knownsecret_change, servers_change, error_handler)
        
        (knownsecret, servers) = runtime_env_instance.get_connection_info()
        self.assertEqual(knownsecret, knownsecret_change)
        self.assertEqual(servers, servers_change)
        self.assertNotEqual(knownsecret, knownsecret_base)
        self.assertNotEqual(servers, servers_base)


    def test_settings(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)

        value_set = 'hej'
        runtime_env_instance.set_settings_value('test', value_set)
        value_get = runtime_env_instance.get_settings_value('test', None)
        self.assertEqual(value_set, value_get)
        
        value_set = 1
        runtime_env_instance.set_settings_value('test', value_set)
        value_get = runtime_env_instance.get_settings_value_int('test', None)
        self.assertEqual(value_set, value_get)
        
        value_set = True
        runtime_env_instance.set_settings_value('test', value_set)
        value_get = runtime_env_instance.get_settings_value_int('test', None)
        self.assertEqual(value_set, value_get)

        value_get = runtime_env_instance.get_settings_value_int('testx', None)
        self.assertEqual(value_get, None)


class AuthCallbackStub(object):
    
    def plugin_event_state_change(self, name, predicate_info=[]):
        pass

class Auth_plugin_test(unittest.TestCase):
    def setUp(self):
        self.client_knownsecret = "Hej du"

        self.db_filename = os.tmpnam()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)
        
    def tearDown(self):
        self.db.dispose()
        try:
            os.remove(self.db_filename)
        except:
            pass

    def test_auth_plugin_simple(self):
        installation_location = giri_unittest.mkdtemp()
        runtime_env_instance = plugin_modules.endpoint.client_gateway.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), installation_location)

        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        self.plugin_client = plugin_modules.endpoint.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
        self.plugin_client.set_current_runtime_env(runtime_env_instance)
        
        management_message_session_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSendBlackHole()
        self.plugin_server = plugin_modules.endpoint.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, self.db, management_message_session_send, None)
        self.plugin_server.set_callback(AuthCallbackStub())

        self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
        self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
        
        deploy_plugin = plugin_modules.endpoint.client_gateway_common.EndpointToken(giri_unittest.get_checkpoint_handler(), PLUGIN_NAME)
        deploy_plugin.initialize_token(installation_location)
        deploy_plugin.deploy_token(installation_location, self.client_knownsecret, SERVERS)
        deploy_plugin.generate_keypair(installation_location)
        
        self.token_serial = deploy_plugin.get_serial(installation_location)
        with database_model.Transaction() as dbt:
            new_key =  dbt.add(database_model.Token())
            new_key.serial = deploy_plugin.get_serial(installation_location)
            new_key.plugin_type = PLUGIN_NAME
            new_key.public_key = deploy_plugin.get_public_key(installation_location)
            dbt.commit()

        self.plugin_server.start()
        giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((self.token_serial, self.token_serial)), 5000)
        self.assertEqual(self.plugin_server.check_predicate((self.token_serial, self.token_serial)), True)
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
