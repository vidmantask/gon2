"""
Endpoint plugin module for the Management Server 

This following datasets are available from this plugin module:
+ '1', All seen endpoints info
"""
from __future__ import with_statement

import os.path
import ConfigParser

import linecache

import lib.checkpoint

from plugin_modules import access_log
from plugin_types.server_management import plugin_type_report

from components.endpoint.server_common import database_schema
from components.database.server_common import schema_api
from components.database.server_common import database_api

from plugin_types.server_management import plugin_type_token




class Config(object):
    """
        The config file contains the report title entries and controls the number of report available e.g.:
        ...
         [report]
         report_1_title = Title for report 1
         report_2_title = Title for report 2
        ... 
    """
    def __init__(self):
        self.report_titles = {}
        self.max_rows = None
        self.load()
    
    def load(self):
        config = ConfigParser.ConfigParser()
        filename = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')
        config.read(filename)     

        if config.has_section('report'):
            for report_id in range(1, 99):
                report_title_tag = 'report_%s_title' % report_id
                if config.has_option('report', report_title_tag):
                    self.report_titles[report_id] =  config.get('report', report_title_tag)

        if config.has_section('restrictions'):
            if config.has_option('restrictions', 'max_rows'):
                self.max_rows = config.getint('restrictions', 'max_rows')


config = Config()    




class PluginEndpointToken(plugin_type_token.PluginToken):

    
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_token.PluginToken.__init__(self, checkpoint_handler, database, license_handler, u'endpoint_token')



class EndpointReport(plugin_type_report.PluginTypeReport):
    """
    """
    
    def __init__(self, checkpoint_handler, license_handler, database):
        plugin_type_report.PluginTypeReport.__init__(self, checkpoint_handler, database, license_handler, u'endpoint_reports')
        self.reports = self._load_reports()

    def _load_reports(self):
        """
        For each found title in the ini-file, a report specification file must be found in the folder of the plugin, e.g:
        ./report_1.rptdesign
        ./report_2.rptdesign
        
        """
        report_path = os.path.abspath(os.path.dirname(__file__))
        reports = {}
        for report_id in config.report_titles.keys():
            report_spec_filename = os.path.join(report_path, 'report_%s.rptdesign' % report_id)
            if os.path.exists(report_spec_filename):
                report = {'report_id':report_id, 'report_title':config.report_titles[report_id], 'report_spec_filename': report_spec_filename }
                reports[report_id] = report
            else:
                self.checkpoint_handler.Checkpoint("load_reports.not_found", self.plugin_name, lib.checkpoint.ERROR, message='Report specification not found', report_spec_filename=report_spec_filename)
        return reports

    def get_reports(self):
        reports = []
        if self.reports != None:
            for report in self.reports.values():
                reports.append(plugin_type_report.Report(self.plugin_name, report['report_id'], report['report_title']))
        return reports
    
    def get_report_spec(self, report_id):
        if self.reports.has_key(report_id):
            report_spec_filename = self.reports[report_id]['report_spec_filename']
            report_spec_lines = linecache.getlines(report_spec_filename)
            return ''.join(report_spec_lines)
        else:
            self.checkpoint_handler.Checkpoint("get_report_spec.not_found", self.plugin_name, lib.checkpoint.ERROR, message='Report specification not found', report_id="%r" % report_id)
            return ''
    
    def get_report_data(self, data_id, args):
        if data_id == u'1':
            return self._get_report_data_seen_endpoints()
        return []

    def _get_report_data_seen_endpoints(self):
        data = []
        with database_api.ReadonlySession() as dbs:
            endpoint_infos_select = database_schema.table_endpoint_info.outerjoin(database_schema.table_endpoint_attribute).select().apply_labels().order_by(database_schema.table_endpoint_info.c.id.desc())
            for db_row in dbs.select(endpoint_infos_select):
                assert len(db_row) == 9
                row = []
                row.append(unicode(db_row[0])) # id
                row.append(unicode(db_row[1])) # hash_key
                row.append(unicode(db_row[2].isoformat(' '))) # first_seen
                row.append(unicode(db_row[3].isoformat(' '))) # last_seen
                row.append(unicode(db_row[6])) # plugin_name
                row.append(unicode(db_row[7])) # attribute_name
                row.append(unicode(db_row[8])) # attribute_value
                data.append(row)
        return data
