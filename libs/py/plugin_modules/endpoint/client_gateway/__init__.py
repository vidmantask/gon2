"""
Endpoint plugin for Gateway Client
"""
from __future__ import with_statement

import tempfile
import sys
import subprocess
import StringIO

import os
import os.path
import uuid

import lib.checkpoint
import lib.utility
import lib.hardware.windows_security_center as windows_security_center 
import lib.appl.gon_os

from components.communication import message

import plugin_types.client_gateway.plugin_type_endpoint as pte
from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_client_runtime_env
import plugin_types.client_gateway.plugin_type_token

import plugin_modules.endpoint.client_gateway_common

import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_installer


# Got this from shutil.py (WindowsError is not defined on other platforms that windows)
try:
    WindowsError
except NameError:
    WindowsError = None


    
class PluginTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'endpoint_token'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        self._endpoint_tokens = plugin_modules.endpoint.client_gateway_common.EndpointToken(checkpoint_handler, self.plugin_name)

    def initialize_token(self, token_id):
        self._endpoint_tokens.initialize_token(token_id)

    def get_tokens(self, additional_device_roots):
        return self._endpoint_tokens.get_tokens(additional_device_roots, PluginClientRuntimeEnv.PLUGIN_NAME)

    def deploy_token(self, token_id, client_knownsecret, servers):
        self._endpoint_tokens.deploy_token(token_id, client_knownsecret, servers)

    def generate_keypair(self, token_id):
        self._endpoint_tokens.generate_keypair(token_id)

    def get_public_key(self, token_id):
        return self._endpoint_tokens.get_public_key(token_id)

    def set_serial(self, token_id, serial):
        return self._endpoint_tokens.set_serial(token_id, serial)
        
    def set_enrolled(self, token_id):
        self._endpoint_tokens.set_enrolled(token_id)

    def reset_enrolled(self, token_id):
        self._endpoint_tokens.reset_enrolled(token_id)

    def is_enrolled(self, token_id):
        return self._endpoint_tokens.is_enrolled(token_id)
    
    def get_knownsecret_and_servers(self, token_id):
        return self._endpoint_tokens.get_knownsecret_and_servers(token_id)

    def set_servers(self, token_id, servers):
        return self._endpoint_tokens.set_servers(token_id, servers)

#
# Implementation of PluginTypeAuth and PluginTypeToken
#
    def get_serial(self, token_id=None):
        if token_id is None:
            return  self.get_serial_authentication()
        return self._endpoint_tokens.get_serial(token_id)

#
# Implementation of PluginTypeAuth
#
    def get_serial_authentication(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            if self.current_runtime_env is not None:
                installation_location = self.current_runtime_env.get_info().id
                serial = self._endpoint_tokens.get_serial(installation_location, throw_on_error=False)
                cps.add_complete_attr(serial=serial)
                self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))
                
    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            if self.current_runtime_env is not None:
                try:
                    installation_location = self.current_runtime_env.get_info().id
                    private_key = self._endpoint_tokens.get_private_key(installation_location)
                    if private_key != None:
                        challenge_signature = lib.cryptfacility.pk_sign_challenge(private_key, challenge)
                        self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))
                    else:
                        self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=None))
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("get_challenge.failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                    self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=None))



class PluginClientRuntimeEnv(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv):
    PLUGIN_NAME = u'endpoint_token_client_runtime_env'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv.__init__(self, async_service, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, user_interface, additional_device_roots)
        self._endpoint_tokens = plugin_modules.endpoint.client_gateway_common.EndpointToken(checkpoint_handler, self.plugin_name)
        self._instances = None

    def _build_instances(self):
        if self._instances is not None:
            return
        self._instances = {}
        for installation_location in self._endpoint_tokens.get_installation_locations():
            self._instances[installation_location] =  PluginClientRuntimeEnvInstance(self.checkpoint_handler, installation_location)

    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin. 
        """
        self._build_instances()
        return self._instances.keys()

    def get_instance(self, runtime_env_id):
        """
        Return the instance. 
        """
        self._build_instances()
        if runtime_env_id in self._instances.keys():
            return self._instances[runtime_env_id]
        return None

    def create_instance(self, checkpoint_handler, root):
        return PluginClientRuntimeEnvInstance(checkpoint_handler, root)
    


class PluginClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase):
    def __init__(self, checkpoint_handler, token_id):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase.__init__(self, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, token_id, 'EndpointDesktop', []) 

    def set_knownsecret_and_servers(self, knownsecret, servers, generate_keypair):
        endpoint_token_deploy = plugin_modules.endpoint.client_gateway_common.EndpointToken(self.checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME)
        if generate_keypair:
            endpoint_token_deploy.initialize_token(self.get_root())
            endpoint_token_deploy.generate_keypair(self.get_root())
            endpoint_token_deploy.reset_enrolled(self.get_root())
        endpoint_token_deploy.deploy_token(self.get_root(), knownsecret, servers)

    def get_knownsecret_and_servers(self):
        endpoint_token_deploy = plugin_modules.endpoint.client_gateway_common.EndpointToken(self.checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME)
        return endpoint_token_deploy.get_knownsecret_and_servers(self.get_root())



class PluginClientEndpointPY(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for using platform independen python to collect information
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_py'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        infos = []
        infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_PLATFORM, sys.platform))
        if sys.platform == 'win32':
            infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_PLATFORM_GROUP, 'win'))
        elif sys.platform == 'darwin':
            infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_PLATFORM_GROUP, 'mac'))
        elif sys.platform == 'linux2':
            if lib.appl.gon_os.is_gon_os():
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_PLATFORM_GROUP, lib.appl.gon_os.VERSION_INFO_GON_OS))
            else:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_PLATFORM_GROUP, 'linux'))

        import socket
        hostname = socket.gethostname()
        if hostname is not None: 
            infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_NAME, hostname.lower()))
            infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_LOCAL_ACCOUNT, os.path.basename(lib.utility.get_home_folder())))

        try:
            ip_info = socket.gethostbyaddr(hostname)
            if len(ip_info) == 3 is not None: 
                for ip in ip_info[2]:
                    infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_IP, ip))
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginClientEndpointPY.do_run", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)

        self.report_some(infos)


class PluginClientEndpointWinAPI(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for the windows platform using wind32 api to collect information
    """
    MIB_IF_OPER_STATUS_NON_OPERATIONAL = 0
    MIB_IF_OPER_STATUS_UNREACHABLE = 1
    MIB_IF_OPER_STATUS_DISCONNECTED = 2
    MIB_IF_OPER_STATUS_CONNECTING = 3
    MIB_IF_OPER_STATUS_CONNECTED = 4
    MIB_IF_OPER_STATUS_OPERATIONAL = 5

    MIB_IF_TYPE_OTHER = 1
    MIB_IF_TYPE_ETHERNET = 6
    MIB_IF_TYPE_TOKENRING = 9
    MIB_IF_TYPE_FDDI = 15
    MIB_IF_TYPE_PPP = 23
    MIB_IF_TYPE_LOOPBACK = 24 
    
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_win_api'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        if sys.platform == 'win32':
            self.do_run_win()

    def do_run_win(self):
        self.report_some(self._get_net_info_win_api())

    def _get_net_info_win_api(self):
        info_nets = []
        mac_addrs = self._getMACAddrWin()
        for mac_addr in mac_addrs:
            info_nets.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_MAC, mac_addr))
        return info_nets

    def _getMACAddrWin(self):
        """
        Code from http://www.mailinglistarchive.com/python-dev@python.org/msg07330.html
        """
        try:
            from ctypes import Structure, windll, sizeof
            from ctypes import POINTER, byref, SetPointerType
            from ctypes import c_ulonglong, c_ulong, c_uint, c_ubyte, c_char
            MAX_ADAPTER_DESCRIPTION_LENGTH = 128
            MAX_ADAPTER_NAME_LENGTH = 256
            MAX_ADAPTER_ADDRESS_LENGTH = 8
            class IP_ADDR_STRING(Structure):
                pass
            LP_IP_ADDR_STRING = POINTER(IP_ADDR_STRING)
            IP_ADDR_STRING._fields_ = [
                ("next", LP_IP_ADDR_STRING),
                ("ipAddress", c_char * 16),
                ("ipMask", c_char * 16),
                ("context", c_ulong)]
            class IP_ADAPTER_INFO (Structure):
                pass
            LP_IP_ADAPTER_INFO = POINTER(IP_ADAPTER_INFO)
            IP_ADAPTER_INFO._fields_ = [
                ("next", LP_IP_ADAPTER_INFO),
                ("comboIndex", c_ulong),
                ("adapterName", c_char * (MAX_ADAPTER_NAME_LENGTH + 4)),
                ("description", c_char * (MAX_ADAPTER_DESCRIPTION_LENGTH + 4)),
                ("addressLength", c_uint),
                ("address", c_ubyte * MAX_ADAPTER_ADDRESS_LENGTH),
                ("index", c_ulong),
                ("type", c_uint),
                ("dhcpEnabled", c_uint),
                ("currentIpAddress", LP_IP_ADDR_STRING),
                ("ipAddressList", IP_ADDR_STRING),
                ("gatewayList", IP_ADDR_STRING),
                ("dhcpServer", IP_ADDR_STRING),
                ("haveWins", c_uint),
                ("primaryWinsServer", IP_ADDR_STRING),
                ("secondaryWinsServer", IP_ADDR_STRING),
                ("leaseObtained", c_ulong),
                ("leaseExpires", c_ulong)]
            GetAdaptersInfo = windll.iphlpapi.GetAdaptersInfo
            GetAdaptersInfo.restype = c_ulong
            GetAdaptersInfo.argtypes = [LP_IP_ADAPTER_INFO, POINTER(c_ulong)]
            adapterList = (IP_ADAPTER_INFO * 10)()
            buflen = c_ulong(sizeof(adapterList))
            rc = GetAdaptersInfo(byref(adapterList[0]), byref(buflen))
            mac_addrs = []
            if rc == 0:
                for a in range(10):
                    if adapterList[a].addressLength == 6:
                        adapter = adapterList[a]
                        mac_adr = "%02x:%02x:%02x:%02x:%02x:%02x" %(adapterList[a].address[0], adapterList[a].address[1], adapterList[a].address[2], adapterList[a].address[3], adapterList[a].address[4], adapterList[a].address[5])
                        mac_addrs.append(mac_adr)
            
            return mac_addrs
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginClientEndpointWinAPI._getMACAddrWin", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
            return []



class PluginClientEndpointWinWMI(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for the windows platform using wmi to collect information
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_win_wmi'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        if sys.platform == 'win32':
            self.do_run_win()

    def do_run_win(self):
        self.report_some(self._get_net_info_win_wmi())
        self.report_some(self._get_machine_info_win_wmi())
        self.report_some(self._get_os_info_win_wmi())
        self._get_security_info_win_wmi()

    def _get_net_info_win_wmi(self):
        info_nets = []
        try:
            import win32com.client
            import pythoncom
            pythoncom.CoInitialize()
            strComputer = "."
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer, "root\cimv2")
            colItems = objSWbemServices.ExecQuery("Select * from Win32_NetworkAdapterConfiguration")
            for objItem in colItems:
                if objItem.IPAddress is not None:
                    info_nets.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_IP, objItem.IPAddress[0]))
#                if objItem.MACAddress:
#                    info_nets.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_MAC, objItem.MACAddress.lower()))
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginClientEndpointWinWMI._get_net_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
            
        return info_nets
    
    def _get_security_info_win_wmi(self):
        strComputer = "."
        try:
            import win32com.client
            import pythoncom
            pythoncom.CoInitialize()
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer, "root\SecurityCenter")
            antivirusItems = objSWbemServices.ExecQuery("Select * from AntiVirusProduct")
            _wmi_antivirus = []
            count = 0
            for item in antivirusItems:
                _wmi_antivirus.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_ANTIVIRUS_COMPANY_NAME + str(count), item.companyName))
                _wmi_antivirus.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE + str(count), item.productUptoDate))
                _wmi_antivirus.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_ANTIVIRUS_VERSION + str(count), item.versionNumber))
                _wmi_antivirus.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_ANTIVIRUS_COMPANY_TITLE + str(count), item.displayName))
                count += 1
            self.report_some(_wmi_antivirus)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        try:
            import pythoncom
            pythoncom.CoInitialize()
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer, "root\SecurityCenter")
            firewallItems = objSWbemServices.ExecQuery("Select * from FireWallProduct")
            count = 0
            _wmi_firewall = []
            for item in firewallItems:
                _wmi_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_COMPANY_NAME + str(count), item.companyName))
                _wmi_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_ENABLED + str(count), item.enabled))
                _wmi_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_VERSION + str(count), item.versionNumber))
                _wmi_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_COMPANY_TITLE + str(count), item.displayName))
                count += 1
            self.report_some(_wmi_firewall)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)

    
    def _get_machine_info_win_wmi(self):
        info_machines = []
        try:
            import win32com.client
            strComputer = "."
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer,"root\cimv2")
            colItems = objSWbemServices.ExecQuery("Select * from Win32_ComputerSystem")
            if len(colItems) > 0 :
                objItem = colItems[0] 
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_NAME, objItem.Name.lower()))
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_MANUFACTURE, objItem.Manufacturer.strip()))
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_MODEL, objItem.Model.strip()))
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_DOMAIN, objItem.Domain))
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_WORKGROUP, objItem.Workgroup))
    
            colItems = objSWbemServices.ExecQuery("Select * from Win32_BIOS")
            if len(colItems) > 0 :
                objItem = colItems[0] 
                info_machines.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_SERIAL, objItem.SerialNumber))
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginClientEndpointWinWMI._get_machine_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        return info_machines    

    def _get_os_info_win_wmi(self):
        info_oss = []
        try:
            import win32com.client
            strComputer = "."
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer,"root\cimv2")
            colItems = objSWbemServices.ExecQuery("Select * from Win32_OperatingSystem")
            if len(colItems) > 0 :
                objItem = colItems[0] 
                info_oss.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_DISTRIBUTION, objItem.Manufacturer))
                info_oss.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_SERIAL, objItem.SerialNumber))
                info_oss.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_VERSION, objItem.Version))
                info_oss.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_VERSION_SERVICE_PACK, '%s.%s' % (objItem.ServicePackMajorVersion, objItem.ServicePackMinorVersion)))
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginClientEndpointWinWMI._get_os_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        return info_oss

 

class PluginClientEndpointHACKS(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin using differents hacks to collect infomation
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_hacks'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        """
        Implement this method to do the collection and report for information
        """
        if sys.platform == 'win32':
            self.do_run_win()
        elif sys.platform == 'linux2':
            self.do_run_linux()
        elif sys.platform == 'darwin':
            self.do_run_mac()

    def do_run_win(self):
        pass
     
    def do_run_mac(self):
        pass
    
    def do_run_linux(self):
        self.report_some(self._get_os_info_linux())
        self.report_some(self._get_net_info_linux_ifconfig())
    
    def _get_os_info_linux(self):
        info_oss = []
        redhat_ident_filename = '/etc/redhat-release'
        if os.path.exists(redhat_ident_filename):
            redhat_dist = open(redhat_ident_filename, 'rt').read().strip() 
            info_oss.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_DISTRIBUTION, redhat_dist))
        return info_oss
    
    def _get_net_info_linux_ifconfig(self):
        """
        eth0      Link encap:Ethernet  HWaddr 00:18:8B:18:4C:75  
                  inet addr:192.168.45.116  Bcast:192.168.45.255  Mask:255.255.255.0
                  inet6 addr: fe80::218:8bff:fe18:4c75/64 Scope:Link
                  UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
                  RX packets:1915954 errors:0 dropped:0 overruns:0 frame:0
                  TX packets:983548 errors:0 dropped:0 overruns:0 carrier:0
                  collisions:0 txqueuelen:1000 
                  RX bytes:1429329842 (1.3 GiB)  TX bytes:696641462 (664.3 MiB)
                  Interrupt:16 
        """
        info_nets = []
        command_args = ['ifconfig']
        process = subprocess.Popen(args=command_args, stdout=subprocess.PIPE, shell=True)
        process.wait()
        process_result = process.communicate()[0].splitlines()
        if process.returncode == 0 and len(process_result) > 0:
            net_interface = None
            net_mac = None
            net_ip = None
            for process_line in process_result:
                if process_line.find('Link') > -1 and process_line.find('HWaddr') > -1:
                    net_interface = process_line.split()[0]
                    net_mac = process_line.split()[4].lower()
                    net_ip = None
                if process_line.find('inet addr') > -1:
                    net_ip = process_line.split(':')[1].split()[0]
                    if net_interface is not None:
                        info_nets.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_IP, net_ip))
                        info_nets.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_MAC, net_mac))
                    net_interface = None
                    net_mac = None
                    net_ip = None
        return info_nets
       

class PluginClientEndpointMacSystemProfiler(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for the mac platform using system_profiler to collect information
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_mac_system_profiler'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        if sys.platform == 'darwin':
            self.report_some(self._get_machine_info())
            self.report_some(self._get_os_info())
            self.report_some(self._get_net_info())

    def _get_machine_info(self):
        import xml.etree.ElementTree
        infos = []
        command_args = ['system_profiler', '-xml', 'SPHardwareDataType']
        process = subprocess.Popen(args=command_args, stdout=subprocess.PIPE, shell=False)
        process.wait()
        process_result = process.communicate()[0]
        if process.returncode == 0:
            system_profiler_output = StringIO.StringIO(process_result)
            system_profiler_dom = xml.etree.ElementTree.ElementTree(file=system_profiler_output)
            
            machine_model = self._get_sp_value(system_profiler_dom, 'machine_model')
            if machine_model is not None:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_MODEL, machine_model))
            
            machine_serial = self._get_sp_value(system_profiler_dom, 'serial_number')
            if machine_serial is not None:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_SERIAL, machine_serial))
        return infos

    def _get_os_info(self):
        import xml.etree.ElementTree
        infos = []
        command_args = ['system_profiler', '-xml', 'SPSoftwareDataType']
        process = subprocess.Popen(args=command_args, stdout=subprocess.PIPE, shell=False)
        process.wait()
        process_result = process.communicate()[0]
        if process.returncode == 0:
            system_profiler_output = StringIO.StringIO(process_result)
            system_profiler_dom = xml.etree.ElementTree.ElementTree(file=system_profiler_output)
            
            os_version = self._get_sp_value(system_profiler_dom, 'os_version')
            if os_version is not None:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_VERSION, os_version))

            local_host_name = self._get_sp_value(system_profiler_dom, 'local_host_name')
            if local_host_name is not None:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_NAME, local_host_name))
        return infos

    def _get_net_info(self):
        import xml.etree.ElementTree
        infos = []
        command_args = ['system_profiler', '-xml', 'SPNetworkDataType']
        process = subprocess.Popen(args=command_args, stdout=subprocess.PIPE, shell=False)
        process.wait()
        process_result = process.communicate()[0]
        if process.returncode == 0:
            system_profiler_output = StringIO.StringIO(process_result)
            system_profiler_dom = xml.etree.ElementTree.ElementTree(file=system_profiler_output)
            
            macs = self._get_sp_value_all(system_profiler_dom, 'MAC Address')
            for mac in macs:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_MAC, mac))

            ips = self._get_sp_value_all(system_profiler_dom, 'ip_address')
            for ip in ips:
                infos.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_NET_IP, ip))


        return infos

    def _get_sp_value(self, dom, key):
        values = self._get_sp_value_all(dom, key)
        if len(values) == 0:
            return None
        return values[0]
        
    def _get_sp_value_all(self, dom, key):
        result = []
        iter = dom.getiterator()
        use_next = False
        for node in iter:
            if use_next:
                use_next = False
                if node.tag == 'string':
                    result.append(node.text)
                elif node.tag == 'array':
                    for node_string in node.findall('string'):
                        result.append(node_string.text)
            if node.tag == 'key' and node.text == key:
                use_next = True
        return result


class PluginClientEndpointGMID(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for collecting GMID(Giritech Machine ID)
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_gmid'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        """
        Implement this method to do the collection and report for information
        """
        if sys.platform == 'win32':
            self.do_run_win()
        elif sys.platform == 'linux2':
            self.do_run_unix()
        elif sys.platform == 'darwin':
            self.do_run_unix()

    def do_run_win(self):
        import _winreg as winreg 
        reg_handler = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)
        reg_gmid_folder_id = r'SOFTWARE\Giritech\gmid_user'
        try:
            reg_gmid_folder = winreg.OpenKey(reg_handler, reg_gmid_folder_id, 0, winreg.KEY_ALL_ACCESS)
            gmid = winreg.QueryValue(reg_handler, reg_gmid_folder_id)
            winreg.CloseKey(reg_gmid_folder)        
        except WindowsError:
            reg_gmid_folder = winreg.CreateKey(reg_handler, reg_gmid_folder_id)       
            gmid = self._generate_gmid()
            winreg.SetValue(reg_handler, reg_gmid_folder_id, winreg.REG_SZ, gmid)
            winreg.CloseKey(reg_gmid_folder)        
        winreg.CloseKey(reg_handler)        
        self.report(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_GMID_USER, gmid))
     
    def do_run_unix(self):
        reg_foldername = os.path.join(lib.utility.get_home_folder(), '.giritech')
        reg_gmid_filename = os.path.join(reg_foldername, 'gmid_user')
        if not os.path.isdir(reg_foldername):
            os.mkdir(reg_foldername)
        if not os.path.isfile(reg_gmid_filename):
            reg_gmid_file = open(reg_gmid_filename, 'wb')
            reg_gmid_file.write(self._generate_gmid())
            reg_gmid_file.close()
        reg_gmid_file = open(reg_gmid_filename, 'rb')
        gmid_user = reg_gmid_file.read()
        reg_gmid_file.close()
        self.report(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_MACHINE_GMID_USER, gmid_user))

    def _generate_gmid(self):
        gmid = uuid.uuid1()
        return "%s" % gmid



class PluginClientEndpointWinCOM(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for the windows platform using wmi to collect information
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_win_com'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        if sys.platform == 'win32':
            self.do_run_win()

    def do_run_win(self):
        self._get_windows_firewall_status()
        self.done_hash()
        self._get_windows_auto_update_status()
        self._get_windows_update_status()

    def _get_windows_firewall_status(self):
        try:
            import win32com.client
            import pythoncom
            pythoncom.CoInitialize()
            h=win32com.client.Dispatch("HNetCfg.FwMgr")
            p = h.LocalPolicy.CurrentProfile
            com_firewall = []
            com_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_COMPANY_NAME, "Windows"))
            com_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_ENABLED, p.FirewallEnabled))
            com_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_VERSION, "N/A"))
            com_firewall.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_FIREWALL_COMPANY_TITLE, "Windows"))
            self.report_some(com_firewall)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)


    def _get_windows_auto_update_status(self):
        try:
            import _winreg
            with _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update", 0, _winreg.KEY_READ | _winreg.KEY_WOW64_64KEY) as reg_key_auto_update:
                (au_setting, au_setting_type) = _winreg.QueryValueEx(reg_key_auto_update, "AUOptions")
                
            prop = pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_AUTO_UPDATE, au_setting)
            self.report_some([prop])
                
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        
        
        
    def _get_windows_update_status(self):
        try:
            import win32com.client
            import pythoncom
            pythoncom.CoInitialize()
            update = win32com.client.Dispatch('Microsoft.Update.Session')
            updateSearcher = update.CreateUpdateSearcher()
            searchResult = updateSearcher.Search("IsAssigned=1 and IsInstalled=0")
            _windows_updates = []
            if searchResult.Updates and searchResult.Updates.Count:
                _windows_updates.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_UP_TO_DATE, False))
                for u in searchResult.Updates:
                    title = u.title
                    if not title:
                        title = "Untitled update"
                    _windows_updates.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_MISSING_UPDATE, title))
            elif searchResult.ResultCode == 2:
                _windows_updates.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_OS_UP_TO_DATE, True))
            self.report_some(_windows_updates)
                
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_info_win_wmi", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        


class PluginClientEndpointWinWSC(pte.PluginTypeEndpoint):
    """
    Client endpoint plugin for the windows platform using wmi to collect information
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._plugin_id = u'endpoint_win_wsc'
        pte.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, self._plugin_id, user_interface, additional_device_roots)

    def do_run(self):
        self.done_hash()
        if sys.platform == 'win32':
            self.do_run_win()

    def do_run_win(self):
        self._get_security_center_status()

    def _get_security_center_status(self):
        try:
            _security_all_center_status = windows_security_center.get_security_all_center_status()
            _wsc_values = []
            for provider, value in _security_all_center_status.items():
                _wsc_values.append(pte.PluginTypeEndpointInfo(pte.PluginTypeEndpointInfo.ATTR_NAME_WSC_PROVIDER + str(provider), value))
            self.report_some(_wsc_values)
                
                
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_get_security_center_status", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)




if __name__ == '__main__':
    class PluginCallback(pte.IPluginTypeEndpointCallback):
        def report(self, endpoint_info):
            print "EndpointSession, report", endpoint_info
    
        def report_some(self, endpoint_infos):
            for endpoint_info in endpoint_infos:
                print "EndpointSession, report_some", endpoint_info
    
        def done(self):
            print "Done"

    plugin_callback = PluginCallback()
    
    plugin = PluginClientEndpointPY(None, None, None, [])
    plugin.set_callback(plugin_callback)
    plugin.run()
    
#    plugin = PluginClientEndpointWinWMI(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
#
#    plugin = PluginClientEndpointWinAPI(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
#
#    plugin = PluginClientEndpointHACKS(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
#
#    plugin = PluginClientEndpointLSHAL(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
#
#    plugin = PluginClientEndpointMacSystemProfiler(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
#    
#    plugin = PluginClientEndpointGMID(None, None, [])
#    plugin.set_callback(plugin_callback)
#    plugin.run()
