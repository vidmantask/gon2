"""
Unittest of certificate plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile
import random

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility
import lib.hardware.device
import lib.hardware.windows_security_center as windows_security_center

import datetime, time


import components.database.server_common.database_api as database_api 

from components.communication.sim import sim_tunnel_endpoint

from components.database.server_common.connection_factory import ConnectionFactory
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from components.auth.server_management import OperationNotAllowedException


import plugin_modules.endpoint_security.server_gateway
import plugin_modules.endpoint_security.server_management
from plugin_modules.endpoint_security.server_common import database_schema as database_model, version_check_spec
  

class AuthCallbackStub(object):
    
    def plugin_event_state_change(self, name, info=[]):
        pass
    
    def report_auth_results(self, report):
        pass

class Auth_plugin_test(unittest.TestCase):
    
    

    def setUp(self):
        self.db_filename = giri_unittest.mk_temp_file()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)
        self.element_count = 0
        self._create_test_data()

        
    def tearDown(self):
        self.db.dispose()
        try:
            os.remove(self.db_filename)
        except:
            pass
        
    def _create_test_data(self):
        plugin = plugin_modules.endpoint_security.server_management.PluginOsSecurity(giri_unittest.get_checkpoint_handler(), None, None)
        
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "default")
        config_spec.set_value("description", "default" * 1000)
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
        self.element_count += 1
        
        for os in version_check_spec.def_operating_systems:
            
            template = plugin.config_get_template(None, None)
            config_spec = ConfigTemplateSpec(template)
            config_spec.set_value("name", os)
            for os_name in version_check_spec.def_operating_systems:
                config_spec.set_value("%s_allowed" % os_name, os_name==os)
            with database_api.Transaction() as dbt:
                plugin.config_create_row(None, template, dbt)
            self.element_count += 1
            
            version_check = version_check_spec.def_version_check.get(os)
            if version_check:
                for version_check_dict in version_check:

                    version_name = version_check_dict.get("main_version")
                    template = plugin.config_get_template(None, None)
                    config_spec = ConfigTemplateSpec(template)
                    
                    config_spec.set_value("name", "%s_version_%s_False" % (os, version_name))
                    config_spec.set_value("%s_version_check" % os, True)
                    for os_name in version_check_spec.def_operating_systems:
                        config_spec.set_value("%s_allowed" % os_name, os_name==os)
                    config_spec.set_value("%s_version_check_field_main_%s" % (os, version_name), False)

                    with database_api.Transaction() as dbt:
                        plugin.config_create_row(None, template, dbt)
                    self.element_count += 1
                    
                    sub_versions = version_check_dict.get("sub_versions")
                    if sub_versions:
                        index = len(sub_versions)/2
                        template = plugin.config_get_template(None, None)
                        config_spec = ConfigTemplateSpec(template)
                        config_spec.set_value("name", "%s_version_%s_sub" % (os, version_name, ))
                        for os_name in version_check_spec.def_operating_systems:
                            config_spec.set_value("%s_allowed" % os_name, os_name==os)
                        config_spec.set_value("%s_version_check" % os, True)
                        config_spec.set_value("%s_version_check_field_main_%s" % (os, version_name), True)
                        config_spec.set_value("%s_version_check_field_sub_%s" % (os, version_name), sub_versions[index][0])

                        with database_api.Transaction() as dbt:
                            plugin.config_create_row(None, template, dbt)
                        self.element_count += 1
            
            security_check = version_check_spec.def_security_check.get(os)
            if security_check:
                for check_dict in security_check:
                    check = check_dict.get("name")
                    check_type = check_dict.get("security_type")

                    template = plugin.config_get_template(None, None)
                    config_spec = ConfigTemplateSpec(template)
                    config_spec.set_value("name", "%s_security_%s" % (os, check, ))
                    for os_name in version_check_spec.def_operating_systems:
                        config_spec.set_value("%s_allowed" % os_name, os_name==os)
                    config_spec.set_value("%s_security_check" % os, True)
                    
                    if check_type == 'wsc':
                        config_spec.set_value("%s_security_check_field_active_%s" % (os, check), True)
                        config_spec.set_value("%s_security_check_field_value_%s" % (os, check), plugin_modules.endpoint_security.server_management.wsc_values[0][1])
                    elif check_type == 'internal':
                        config_spec.set_value("%s_security_check_field_active_%s" % (os, check), True)
        
                    with database_api.Transaction() as dbt:
                        plugin.config_create_row(None, template, dbt)
                    self.element_count += 1
        
        

    def test_mgmt0(self):
        plugin = plugin_modules.endpoint_security.server_management.PluginOsSecurity(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)
        self.assertEquals(len(elements), self.element_count)
        
        plugin = plugin_modules.endpoint_security.server_management.PluginOsSecurity(giri_unittest.get_checkpoint_handler(), None, None)
        
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, plugin.config_create_row, None, template, dbt)

        config_spec.set_value("name", "yo")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)
        
        elements = plugin.get_elements(None)
        self.assertEquals(len(elements), self.element_count+1)

        element_id = None
        for e in elements:
            if e.get_label() == "yo":
                element_id = e.get_internal_id()
        self.assert_(element_id)
        
        spec_elements = plugin.get_specific_elements(None, [element_id])
        self.assertEquals(len(spec_elements), 1)
        
        
        with database_api.Transaction() as dbt:
            template = plugin.config_get_template(None, element_id, dbt)
        
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "yo1")
        for os_name in version_check_spec.def_operating_systems:
            config_spec.set_value("%s_allowed" % os_name, False)
        with database_api.Transaction() as dbt:
            new_name = plugin.config_update_row(None, template, dbt)
        self.assertEquals(new_name, "yo1")
        
        with database_api.Transaction() as dbt:
            template = plugin.config_get_template(None, element_id, dbt)
            config_spec = ConfigTemplateSpec(template)
            for os_name in version_check_spec.def_operating_systems:
                self.assertEquals(config_spec.get_value("%s_allowed" % os_name), False)
                
        with database_api.Transaction() as dbt:
            plugin.config_delete_row(None, element_id, dbt)
            
        spec_elements = plugin.get_specific_elements(None, [element_id])
        self.assertEquals(len(spec_elements), 0)

    def test_gateway0(self):

        os_platforms = [("win", "Windows"), 
                        ("mac", "Mac"),
                        ("linux", "Linux"),
                        (lib.appl.gon_os.VERSION_INFO_GON_OS, "G/On OS"),
                        ("ios", "iOS"),
                        ]
    

        
        for os_platform, os in os_platforms:
                
            plugin_gateway = plugin_modules.endpoint_security.server_gateway.PluginOsSecurity(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, None, None, None)
            plugin_gateway.set_callback(AuthCallbackStub())
    
            plugin_gateway.start()
            self.assertFalse(plugin_gateway.is_ready())
            
            plugin_gateway.endpoint_info_report_some("yo",[('os_platform_group',os_platform)])

            plugin_gateway.endpoint_info_all_done()
            self.assertTrue(plugin_gateway.is_ready())
            
            plugin = plugin_modules.endpoint_security.server_management.PluginOsSecurity(giri_unittest.get_checkpoint_handler(), None, None)
            elements = plugin.get_elements(None)
            
            for e in elements:
                name = e.get_label()
                value = plugin_gateway.check_predicate((e.get_internal_id(), e.get_label()))
#                print (name,value)
                if name=="default":
                    self.assertTrue(value is True)
                elif name.startswith(os):
                    if name==os:
                        self.assertTrue(value is True)
                    else:
                        self.assertTrue(value is None)
                else:
                    self.assertTrue(value is False)

            version_check = version_check_spec.def_version_check.get(os)
            if version_check:
                for version_check_dict in version_check:

                    sub_versions = version_check_dict.get("sub_versions")
                    if sub_versions:
                        n = len(sub_versions)
                        sub_version_index = -1
                        for sub_version in sub_versions:
                            sub_version_index += 1
                            
                            plugin_gateway = plugin_modules.endpoint_security.server_gateway.PluginOsSecurity(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, None, None, None)
                            plugin_gateway.set_callback(AuthCallbackStub())
                    
                            plugin_gateway.start()
                            self.assertFalse(plugin_gateway.is_ready())
                            
                            plugin_gateway.endpoint_info_report_some("yo",[('os_platform_group',os_platform)])
                
                            version_name = version_check_dict.get("main_version")
                            version_numbers = version_check_dict.get("values")
                            index = random.randint(0, len(version_numbers)-1)
                            
#                            print "Set version : %s" % version_name
#                            print "Set sub version : %s" % sub_version[0]
                            plugin_gateway.endpoint_info_report_some("yo",[('os_version',"%s.2" % version_numbers[index])])
                            plugin_gateway.endpoint_info_report_some("yo",[('os_version_service_pack',sub_version[0])])

                            plugin_gateway.endpoint_info_all_done()
                            self.assertTrue(plugin_gateway.is_ready())

                            for e in elements:
                                name = e.get_label()
                                value = plugin_gateway.check_predicate((e.get_internal_id(), e.get_label()))
#                                print (name, value)
                                if name=="default":
                                    self.assertTrue(value is True)
                                elif name.startswith(os):
                                    if name==os:
                                        self.assertTrue(value is True)
                                    elif name.startswith("%s_version" % os):
                                        if version_name in name:
                                            if name.endswith("_sub"): 
                                                if not sub_version[0]:
                                                    self.assertTrue(value is None)
                                                else:
                                                    self.assertTrue(value is (sub_version_index >= n/2))
                                        else:
                                            self.assertTrue(value is True)
                                    else:
                                        self.assertTrue(value is None)
                                else:
                                    self.assertTrue(value is False)
        

            security_check = version_check_spec.def_security_check.get(os)
            if security_check:
                for check_dict in security_check:
                    check = check_dict.get("name")
                    check_type = check_dict.get("security_type")

                    if check_type == 'wsc':
                        wsc_property = windows_security_center.wsc_property_map.get(check)
                        
                        for wsc_title, wsc_value in plugin_modules.endpoint_security.server_management.wsc_values: 
                            plugin_gateway = plugin_modules.endpoint_security.server_gateway.PluginOsSecurity(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, None, None, None)
                            plugin_gateway.set_callback(AuthCallbackStub())
                    
                            plugin_gateway.start()
                            self.assertFalse(plugin_gateway.is_ready())
                            
                            plugin_gateway.endpoint_info_report_some("yo",[('os_platform_group',os_platform)])
                            
                            plugin_gateway.endpoint_info_report_some("yo",[(plugin_modules.endpoint_security.server_gateway.PluginOsSecurity.ATTR_NAME_WSC_PROVIDER + str(wsc_property), wsc_value)])
                            plugin_gateway.endpoint_info_all_done()
                            
                            for e in elements:
                                name = e.get_label()
                                value = plugin_gateway.check_predicate((e.get_internal_id(), e.get_label()))
#                                print (name, value)
                                if name.startswith("%s_security" % os):
                                    if name.startswith("%s_security_%s" % (os, check, )):
                                        self.assertTrue(value is (wsc_value == windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_GOOD))
                                    else:
                                        self.assertTrue(value is None)
                            
                        
                        
                    elif check_type == 'internal':
                        if check == "security_updates":
                            property = plugin_modules.endpoint_security.server_gateway.PluginOsSecurity.ATTR_NAME_OS_UP_TO_DATE
                        else:
                            raise Exception("Unknown security check '%s'" % check)
                            
                        plugin_gateway = plugin_modules.endpoint_security.server_gateway.PluginOsSecurity(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, None, None, None)
                        plugin_gateway.set_callback(AuthCallbackStub())
                
                        plugin_gateway.start()
                        self.assertFalse(plugin_gateway.is_ready())
                        
                        plugin_gateway.endpoint_info_report_some("yo",[('os_platform_group',os_platform)])
                        
                        plugin_gateway.endpoint_info_report_some("yo",[(property, True)])
                        plugin_gateway.endpoint_info_all_done()
                        
                        for e in elements:
                            name = e.get_label()
                            value = plugin_gateway.check_predicate((e.get_internal_id(), e.get_label()))
#                            print (name, value)
                            if name.startswith("%s_security" % os):
                                if name.startswith("%s_security_%s" % (os, check, )):
                                    self.assertTrue(value is True)
                                else:
                                    self.assertTrue(value is None)
        

        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
