'''
Created on 19/05/2010

@author: pwl
'''

from lib.hardware.windows_security_center import *

wsc_values = [("Good", WSC_SECURITY_PROVIDER_HEALTH_GOOD),
              ("Unknown", WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED),              
              ("Poor", WSC_SECURITY_PROVIDER_HEALTH_POOR),              
              ("N/A", WSC_SECURITY_PROVIDER_HEALTH_SNOOZE),              
            ]

firewall_values = [("Enabled", 0),
                  ("Installed", 1),              
                  ("None", 2),              
                ] 

GT = ">"
GTE = ">="
EQ = "=="
LTE = "<="
LTE = "<"

def_security_checks = {
                        u"WSC_Antivirus" : dict( method=u"get_wsc_status", 
                                                 parameters = [WSC_SECURITY_PROVIDER_ANTIVIRUS], 
                                                 type="enum",
                                                 values=wsc_values,
                                                 title=u"WSC Antivirus state",
                                                 comparison_type=LTE,
                                                 ),
                        u"WSC_Firewall" : dict( method=u"get_wsc_status", 
                                                 parameters = [WSC_SECURITY_PROVIDER_FIREWALL], 
                                                 type="enum",
                                                 values=wsc_values,
                                                 title=u"WSC Firewall state",
                                                 comparison_type=LTE,
                                                 ),
                        u"WSC_Update" : dict( method=u"get_wsc_status", 
                                                 parameters = [WSC_SECURITY_PROVIDER_AUTOUPDATE_SETTINGS], 
                                                 type="enum",
                                                 values=wsc_values,
                                                 title=u"WSC Windows Update State",
                                                 comparison_type=LTE,
                                                 ),
                        u"WSC_All" :  dict( method=u"get_wsc_status", 
                                                 parameters = [WSC_SECURITY_PROVIDER_ALL], 
                                                 type="enum",
                                                 values=wsc_values,
                                                 title=u"WSC General State",
                                                 comparison_type=LTE,
                                                 ),

                        u"WMI_FirewallProduct" :  dict( method=u"get_wmi_firewall", 
                                                     parameters = [], 
                                                     type="enum",
                                                     values=firewall_values,
                                                     title=u"Firewall Product Status",
                                                     comparison_type=LTE,
                                                     ),

                        u"Windows_Firewall" :  dict( method=u"get_windows_firewall", 
                                                     parameters = [], 
                                                     type="enum",
                                                     values=firewall_values,
                                                     title=u"Windows Firewall Status",
                                                     comparison_type=LTE,
                                                     ),

                        u"WMI_AntivirusProduct" :  dict( method=u"get_wmi_antivirus", 
                                                         parameters = [], 
                                                         type="enum",
                                                         values=[("Up to Date", 0),
                                                                 ("Installed", 1),              
                                                                 ("None", 2),], 
                                                         title=u"Antivirus Product Status",
                                                         comparison_type=LTE,
                                                     ),

                        u"Windows_Update" :  dict( method=u"get_windows_updated", 
                                                         parameters = [], 
                                                         type="bool",
                                                         title=u"All Windows Updates Installed",
                                                     ),
                                                     
                      }