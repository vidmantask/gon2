'''
Created on 01/12/2010

@author: pwl
'''
def_operating_systems = ['Windows', 'Linux', 'Mac', 'iOS', 'Android', 'G/On OS']

def_version_check = {
                       'Windows' : [
                                       { 'main_version' : 'Windows XP',
                                         'sub_version_title' : "Required Service Pack",
                                         'sub_versions' : [('', ''), ('2.0', 'SP2'), ('3.0', 'SP3')],
                                         'values' : ["5.1", "5.2"]
                                       },
                                       { 'main_version' : 'Windows Vista',
                                         'sub_version_title' : "Required Service Pack",
                                        'sub_versions'  : [('', ''), ('1.0', 'SP1'), ('2.0', 'SP2')],
                                         'values' : ["6.0"]
                                        },
                                       { 'main_version' : 'Windows 7',
                                         'sub_version_title' : "Required Service Pack",
                                        'sub_versions'  : [('', ''), ('1.0', 'SP1')],
                                         'values' : ["6.1"]
                                         },
                                       { 'main_version' : 'Windows 8',
                                         'sub_version_title' : "Required Service Pack",
                                        'sub_versions'  : [('', '')],
                                         'values' : ["6.2"]
                                         },
                                       { 'main_version' : 'Windows 8.1',
                                         'sub_version_title' : "Required Service Pack",
                                        'sub_versions'  : [('', '')],
                                         'values' : ["6.3"]
                                         },
                                       { 'main_version' : 'Windows 10',
                                         'sub_version_title' : "Required Service Pack",
                                        'sub_versions'  : [('', '')],
                                         'values' : ["10.0"],
                                         'use_version_as_sub_version': True
                                         },
                                   ],

                       'Linux' :   [
                                   ],
                       'Mac' :     [
                                   ],
                       'iOS' :     [
                                   ],
                       'Android' :     [
                                   ],
                       'G/On OS' :     [
                                               ],
                    }


def_security_check = {
                       'Windows' : [
                                       {
                                          'name' : 'firewall',
                                          'title' : 'Firewall',
                                          'security_type': 'wsc'
                                        },
                                       {
                                          'name' : 'antivirus',
                                           'title' : 'AntiVirus',
                                           'security_type': 'wsc'
                                        },
                                       {
                                          'name' : 'windows_update',
                                           'title' : 'Windows Auto Update',
                                           'security_type': 'wsc'
                                        },
                                       {
                                          'name' : 'security_updates',
                                           'title' : 'All important updates installed',
                                           'security_type': 'internal'
                                        },
                                   ],

                       'Linux' :   [
                                   ],
                       'Mac' :     [
                                   ],
                    }


def_properties = {
                   "os"             : "os_platform_group",
                   "os_main_version" : {
                                          'Windows' : 'os_version',
                                       },
                   "os_sub_version" : {
                                          'Windows' : 'os_version_service_pack',
                                       },

                 }

if __name__ == '__main__':

    import lib.hardware.windows_security_center as windows_security_center

    def create_security_settings(os):
        config_list = []
        checks = def_security_check.get(os)
        for check in checks:
            check_type = check.get("security_type")
            check_name = check.get("name")
            if check_type=="wsc":
                for value in windows_security_center.wsc_value_title_map.keys():
                    security_dict = dict(check_name=value)
                    config_list.append(security_dict)
            elif check_type=="internal":
                if check_name == "security_updates":
                    pass





    f = file("test_configurations.py", "wt")

    config_list = []

    for os in def_operating_systems:
        base_dict = dict(os_platform_group=os)
        config_list.append(base_dict)

        versions = create_versions(os)
        security_settings = create_security_settings(os)

        n = max(len(versions), len(security_settings))

        for i in range(0,n):
            config_dict = base_dict.copy()
            try:
                config_dict.update(versions[i])
            except IndexError:
                pass
            try:
                config_dict.update(security_settings[i])
            except IndexError:
                pass
        config_list.append(config_dict)



    f.writelines([repr(c)+"\n" for c in config_list])

    f.close()
