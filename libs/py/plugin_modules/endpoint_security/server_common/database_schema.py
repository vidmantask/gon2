from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api



dbapi = schema_api.SchemaFactory.get_creator("plugin_endpoint_security")

table_element = dbapi.create_table("element",
                                        schema_api.Column('name', schema_api.String(256), nullable=False),
                                        schema_api.Column('description', schema_api.Text),
                                        schema_api.Column('linux_allowed', schema_api.Boolean(), default=True),
                                        schema_api.Column('mac_allowed', schema_api.Boolean(), default=True),
                                        schema_api.Column('windows_allowed', schema_api.Boolean(), default=False),
                                        schema_api.Column('windows_security', schema_api.Boolean(), default=False),
                                        schema_api.Column('linux_security', schema_api.Boolean(), default=False),
                                        schema_api.Column('mac_security', schema_api.Boolean(), default=False),
                                     )

table_os_check = dbapi.create_table("os_check",
                                schema_api.Column('element_id', schema_api.Integer(), nullable=False, index=True),
                                schema_api.Column('os_name', schema_api.String(256), nullable=False),
                                schema_api.Column('os_allowed', schema_api.Boolean(), default=True),
                                schema_api.Column('os_security_check', schema_api.Boolean(), default=False),
                                schema_api.Column('os_version_check', schema_api.Boolean(), default=False),
                             )

table_security_check = dbapi.create_table("security_check",
                                schema_api.Column('os_check_id', schema_api.Integer(), nullable=False, index=True),
                                schema_api.Column('type', schema_api.String(256), nullable=False),
                                schema_api.Column('name', schema_api.String(256), nullable=False),
                                schema_api.Column('int_value', schema_api.Integer()),
                                schema_api.Column('string_value', schema_api.String(1024)),
                               )

table_version_check = dbapi.create_table("version_check",
                                schema_api.Column('os_check_id', schema_api.Integer(), nullable=False, index=True),
                                schema_api.Column('main_version', schema_api.String(256), nullable=False),
                                schema_api.Column('sub_version', schema_api.String(256)),
                               )



dbapi.add_foreign_key_constraint(table_os_check, 'element_id', table_element)
dbapi.add_foreign_key_constraint(table_security_check, 'os_check_id', table_os_check)
dbapi.add_foreign_key_constraint(table_version_check, 'os_check_id', table_os_check)

schema_api.SchemaFactory.register_creator(dbapi)

class SecurityElement(object):
    
    def delete(self, transaction):
        for os_check in self.os_checks:
            os_check.delete(transaction)
        transaction.delete(self)

class SecurityOsCheck(object):

    def delete(self, transaction):
        for version_check in self.version_checks:
            transaction.delete(version_check)
        for security_check in self.security_checks:
            transaction.delete(security_check)
        transaction.delete(self)


class SecurityElementCheck(object):
    pass

class VersionCheck(object):
    pass


schema_api.mapper(SecurityElementCheck, table_security_check)
schema_api.mapper(VersionCheck, table_version_check)
schema_api.mapper(SecurityOsCheck, table_os_check, relations=[database_api.Relation(SecurityElementCheck, 'security_checks'),
                                                             database_api.Relation(VersionCheck, 'version_checks')])

schema_api.mapper(SecurityElement, table_element, relations=[database_api.Relation(SecurityOsCheck, 'os_checks')])
#schema_api.mapper(SecurityElement, table_element, relations=[database_api.Relation(SecurityElementCheck, 'ip_ranges', backref_property_name='address_element')])

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)
