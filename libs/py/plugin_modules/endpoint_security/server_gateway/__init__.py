"""
Endpoint MAC plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.windows_security_center as windows_security_center
import lib.appl.gon_os

from plugin_types.server_gateway import plugin_type_auth
from plugin_types.server_gateway import plugin_type_endpoint

from plugin_modules.endpoint_security.server_common import database_schema
from components.database.server_common import database_api
from plugin_modules.endpoint_security.server_common import *
from plugin_modules.endpoint_security.server_common.version_check_spec import def_version_check,def_security_check



class PluginOsSecurity(plugin_type_auth.PluginTypeAuth,
                        plugin_type_endpoint.PluginTypeEndpoint):


    ATTR_NAME_OS_UP_TO_DATE = 'os_up_to_date'
    ATTR_NAME_OS_MISSING_UPDATE = 'os_missing_update'
    ATTR_NAME_OS_AUTO_UPDATE = 'os_auto_update'

    ATTR_NAME_ANTIVIRUS_COMPANY_NAME = 'antivirus_company_name'
    ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE = 'antivirus_product_up_to_date'
    ATTR_NAME_ANTIVIRUS_VERSION = 'antivirus_version'
    ATTR_NAME_ANTIVIRUS_COMPANY_TITLE = 'antivirus_company_title'

    ATTR_NAME_FIREWALL_COMPANY_NAME = 'firewall_company_name'
    ATTR_NAME_FIREWALL_ENABLED = 'firewall_enabled'
    ATTR_NAME_FIREWALL_VERSION = 'firewall_version'
    ATTR_NAME_FIREWALL_COMPANY_TITLE = 'firewall_company_title'

    ATTR_NAME_WSC_PROVIDER = 'wsc_provider_'


    """
    Runtime server part of Endpoint authentication plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'endpoint_security', license_handler, session_info, database, management_message_session, access_log_server_session)
        plugin_type_endpoint.PluginTypeEndpoint.__init__(self, async_service, checkpoint_handler, u'endpoint_security', license_handler, session_info)
        self.all_done = False
        self.required_endpoint_properties = ['os_platform_group', 'os_version', 'os_version_service_pack' ]
        self.multi_endpoint_properties = [self.ATTR_NAME_FIREWALL_ENABLED, self.ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE, self.ATTR_NAME_OS_MISSING_UPDATE]
        self.other_endpoint_properties = [self.ATTR_NAME_OS_UP_TO_DATE, self.ATTR_NAME_OS_AUTO_UPDATE]
        self.endpoint_properties = dict()
        self.endpoint_check_elements = None
        self.endpoint_check_element_map = dict()
        self.auth_notified = False
        self.true_element_ids = []
        self.is_windows_xp = None

        self.false_results = dict()

        self._os_platform = None

    def reset(self):
        plugin_type_auth.PluginTypeAuth.reset(self)
        plugin_type_endpoint.PluginTypeEndpoint.reset(self)

    def start(self):
        """
        Initiates the process
        """
        self.set_started()
        if self.all_done:
            self._calculate_elements()

    def _check_element(self, param, internal_type=None):
        element_id = int(param[0])
        if element_id in self.true_element_ids:
            return True
        element = self.endpoint_check_element_map.get(element_id)
        if element in self.endpoint_check_elements:
            return None
        else:
            return False

    def _get_platform(self):
        if not self._os_platform:
            os_platform = self.endpoint_properties.get("os_platform_group")
            if os_platform == "win":
                self._os_platform = "Windows"
            elif os_platform == 'mac':
                self._os_platform = "Mac"
            elif os_platform == 'linux':
                self._os_platform = "Linux"
            elif os_platform == lib.appl.gon_os.VERSION_INFO_GON_OS:
                self._os_platform = "G/On OS"
            elif os_platform == "ios":
                self._os_platform = "iOS"
            elif os_platform == "android":
                self._os_platform = "Android"
            elif os_platform is None:
                self._os_platform = None
            else:
                self._os_platform = "Unknown"

        return self._os_platform

    def _is_windows_xp(self):
        if self.is_windows_xp is None:
            os_version = self.endpoint_properties.get("os_version")
            if not os_version:
                return None
            os = self._get_platform()
            self.is_windows_xp = False
            if os == "Windows":
                version_specs = def_version_check.get(os)
                for version_spec in version_specs:
                    if version_spec.get("main_version") == "Windows XP":
                        for value in version_spec.get("values", []):
                            if os_version.startswith(value):
                                self.is_windows_xp = True
                                break
                        break
        return self.is_windows_xp




    def _check_security_info(self, os, security_checks):
        for security_check in security_checks:
            if security_check.type == "wsc":
                wsc_type = security_check.name
                if self._is_windows_xp():
                    required_value = int(security_check.string_value)
                    reason = "Unknown"
                    if wsc_type == "firewall":
                        if required_value > windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_GOOD:
                            return True, ""
                        value_dict = self.endpoint_properties.get(self.ATTR_NAME_FIREWALL_ENABLED)
                        if value_dict:
                            for value in value_dict.values():
                                if value:
                                    return True, ""
                            reason = "Firewall not enabled"
                        else:
                            reason = "Firewall not detected (yet)"

                    elif wsc_type == "antivirus":
                        if required_value > windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED:
                            return True, ""
                        value_dict = self.endpoint_properties.get(self.ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE)
                        if value_dict:
                            if required_value == windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED:
                                return True, ""
                            for value in value_dict.values():
                                if value:
                                    return True, ""
                            reason = "Antivirus not up-to-date"
                        else:
                            reason = "Antivirus not detected (yet)"
                    elif wsc_type == "windows_update":
                        value = self.endpoint_properties.get(self.ATTR_NAME_OS_AUTO_UPDATE)
                        if not value is None:
                            if value==2:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_POOR
                            elif value==3:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED
                            elif value==4:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_GOOD
                            else:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_SNOOZE

                            return required_value >= wsc_value, "Required Windows Auto update setting not fulfilled"
                        else:
                            reason = "Windows Auto Update setting not (yet) found"
                    else:
                        self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown Windows Security Center property for Windows XP", wsc_property=wsc_type)
                        return False, "Internal Error"

                    return None, reason
                else:
                    wsc_property = windows_security_center.wsc_property_map.get(wsc_type)
                    if not wsc_property:
                        self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown Windows Security Center property", wsc_property=wsc_type)
                        return False, "Error : Unknown Windows Security Center property"
                    wsc_value = self.endpoint_properties.get(self.ATTR_NAME_WSC_PROVIDER + str(wsc_property))
                    if wsc_value is None:
                        return None, "Windows Security Center information not (yet) found"
                    if security_check.string_value < str(wsc_value):
                        title = windows_security_center.wsc_property_title_map.get(wsc_property)
                        return False, "Windows Security Center requirement for '%s' not met" % title
                    else:
                        return True, ""
            elif security_check.type == "internal":
                wsc_type = security_check.name
                if wsc_type == "security_updates":
                    value = self.endpoint_properties.get(self.ATTR_NAME_OS_UP_TO_DATE)
                    if value:
                        return True, ""
                    elif value is None:
                        return value, "Check for uninstalled important updates not completed"
                    else:
                        missing_update_dict = self.endpoint_properties.get(self.ATTR_NAME_OS_MISSING_UPDATE, dict())
                        if not missing_update_dict:
                            return True, ""
                        missing_updates = ",".join(missing_update_dict.values())
                        return value, "Missing Updates: %s" % missing_updates
                else:
                    self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown internal security check", check_type=wsc_type)
                    return False

        return True




    def _check_version_info(self, os, version_checks):

        os_version = self.endpoint_properties.get("os_version")
        if not os_version:
            return None, "Operating System version not yet retrieved"
        version_specs = def_version_check.get(os)
        version_found = None
        for version_check in version_checks:
            main_version = version_check.main_version
            version_found = None
            for version_spec in version_specs:
                if version_spec.get("main_version") == main_version:
                    version_found = version_spec
                    break
            if not version_found:
                continue

            value_found = False
            for value in version_found.get("values", []):
                if os_version.startswith(value):
                    value_found = True
                    break
            if value_found:
                break
            else:
                version_found = None

        if not version_found:
            return False, "Operating System version requirement not met"
        if version_check.sub_version:
            os_version_service_pack = self.endpoint_properties.get("os_version_service_pack")
            if not os_version_service_pack:
                return None, "Service Pack information not (yet) retrieved"
            if self._is_older_sub_version(os_version_service_pack, version_check.sub_version, os):
                return False, "Service Pack requirement not met"

        return True, ""

    def _check_security_element(self, security_element):
        os = self._get_platform()
        if os is None:
            return None, "Operating System information not yet retrieved"
        if os:
            for os_check in security_element.os_checks:
                if os_check.os_name == os:
                    if os_check.os_allowed:
                        if os_check.os_security_check:
                            if os_check.security_checks:
                                security_result, reason = self._check_security_info(os, os_check.security_checks)
                                if not security_result:
                                    return security_result, reason
                        elif os_check.os_version_check:
                            if os_check.version_checks:
                                version_result, reason = self._check_version_info(os, os_check.version_checks)
                                if not version_result:
                                    return version_result, reason
                        return True, ""
            return False, "Operating System requirement not met"
        return False, "Missing Operating system information"

    def _is_older_sub_version(self, current_version, check_version, os):
        return current_version < check_version

    def _calculate_elements(self):
        if self.endpoint_check_elements is None:
            with database_api.ReadonlySession() as db_session:
                self.endpoint_check_elements = dict((e, "Insufficient information available") for e in db_session.select(database_schema.SecurityElement))
                self.endpoint_check_element_map = dict((e.id, e) for e in self.endpoint_check_elements)
        true_results = []
        false_results = []
        for element in self.endpoint_check_elements.keys():
            result, reason = self._check_security_element(element)
            if result:
                true_results.append(element)
                self.true_element_ids.append(element.id)
            elif result is False:
                false_results.append((element, reason))
            else:
                self.endpoint_check_elements[element] = reason
            self.checkpoint_handler.Checkpoint("_calculate_elements", self.plugin_name, lib.checkpoint.DEBUG, element_name=element.name, result=result, reason=reason)
        for element, reason in false_results:
            self.endpoint_check_elements.pop(element)
            self.false_results[element.id] = reason
        if true_results:
            self.set_ready()
            predicate_list = []
            for element in true_results:
                self.endpoint_check_elements.pop(element)
                predicate_info = dict()
                predicate_info['value'] = str(element.id)
                predicate_list.append(predicate_info)
            self.callback.plugin_event_state_change(self.plugin_name, predicate_list)
            self.auth_notified = True
        if self.all_done and not self.auth_notified:
            self.set_ready()
            self.callback.plugin_event_state_change(self.plugin_name, [dict(value=None)])
            self.auth_notified = True

    def add_multi_attribute(self, name, prefix_name, value):
        props = self.endpoint_properties.get(prefix_name, dict())
        attr_number = name.replace(prefix_name, "", 1)
        if not attr_number:
            count = 0
            while props.has_key(str(count)):
                count += 1
            attr_number = str(count)
        props[attr_number] = value
        self.endpoint_properties[prefix_name] = props


    def is_used_attribute(self, name, value):
        if name in self.other_endpoint_properties:
            self.endpoint_properties[name] = value
            return True
        if name.startswith(self.ATTR_NAME_WSC_PROVIDER):
            self.endpoint_properties[name] = value
            return True
        for attr in self.multi_endpoint_properties:
            if name.startswith(attr):
                self.add_multi_attribute(name, attr, value)
                return True
        return False

    def _create_auth_report(self):
        results = []
        VERSION_TYPE = 100
        SECURITY_TYPE = 100
        os = self._get_platform()
        results.append((VERSION_TYPE, ("OS", os)))
        os_version = self.endpoint_properties.get("os_version")
        if os_version:
            version_specs = def_version_check.get(os)
            if version_specs:
                version_found = None
                for version_spec in version_specs:
                    value_found = False
                    for value in version_spec.get("values", []):
                        if os_version.startswith(value):
                            version_found = version_spec
                            break
                if version_found:
                    main_version = version_found.get("main_version")
                    results.append((VERSION_TYPE, ("OS Version", main_version)))
                    if 'use_version_as_sub_version' in version_spec and version_spec['use_version_as_sub_version']:
                        sub_version = os_version
                    else:
                        sub_version = self.endpoint_properties.get("os_version_service_pack")
                    if sub_version:
                        results.append((VERSION_TYPE, ("OS Service Pack", sub_version)))
                else:
                    results.append((VERSION_TYPE, ("OS Version", "Unknown")))


        security_checks = def_security_check.get(os, [])
        for security_check in security_checks:
            if security_check.get("security_type") == "wsc":
                wsc_type = security_check.get("name")
                if self._is_windows_xp():
                    if wsc_type == "firewall":
                        value_dict = self.endpoint_properties.get(self.ATTR_NAME_FIREWALL_ENABLED)
                        if value_dict:
                            value_found = False
                            for value in value_dict.values():
                                if value:
                                    results.append((SECURITY_TYPE, ("Firewall", "Enabled")))
                                    value_found = True
                                    break
                            if not value_found:
                                results.append((SECURITY_TYPE, ("Firewall", "Disabled")))
                        else:
                            results.append((SECURITY_TYPE, ("Firewall", "Not Detected")))

                    elif wsc_type == "antivirus":
                        value_dict = self.endpoint_properties.get(self.ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE)
                        if value_dict:
                            value_found = False
                            for value in value_dict.values():
                                if value:
                                    results.append((SECURITY_TYPE, ("AntiVirus", "Enabled")))
                                    value_found = True
                                    break
                            if not value_found:
                                results.append((SECURITY_TYPE, ("AntiVirus", "Disabled")))
                        else:
                            results.append((SECURITY_TYPE, ("AntiVirus", "Not Detected")))

                    elif wsc_type == "windows_update":
                        value = self.endpoint_properties.get(self.ATTR_NAME_OS_AUTO_UPDATE)
                        if not value is None:
                            if value==2:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_POOR
                            elif value==3:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED
                            elif value==4:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_GOOD
                            else:
                                wsc_value = windows_security_center.WSC_SECURITY_PROVIDER_HEALTH_SNOOZE

                            results.append((SECURITY_TYPE, ("AutoUpdate", windows_security_center.wsc_value_title_map.get(wsc_value))))
                        else:
                            results.append((SECURITY_TYPE, ("AutoUpdate", "Not Detected")))
                    else:
                        self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown Windows Security Center property for Windows XP", wsc_property=wsc_type)

                else:
                    wsc_property = windows_security_center.wsc_property_map.get(wsc_type)
                    if not wsc_property:
                        self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown Windows Security Center property", wsc_property=wsc_type)
                    else:
                        title = windows_security_center.wsc_property_title_map.get(wsc_property)
                        wsc_value = self.endpoint_properties.get(self.ATTR_NAME_WSC_PROVIDER + str(wsc_property))
                        if wsc_value is None:
                            results.append((SECURITY_TYPE, (title, "Not Detected")))
                        else:
                            value_str = windows_security_center.wsc_value_title_map.get(wsc_value, "Unknown value")
                            results.append((SECURITY_TYPE, (title, value_str)))
            elif security_check.get("security_type") == "internal":
                wsc_type = security_check.get("name")
                if wsc_type == "security_updates":
                    value = self.endpoint_properties.get(self.ATTR_NAME_OS_UP_TO_DATE)
                    if value:
                        results.append((SECURITY_TYPE, ("OS up-to-date", "Yes")))
                    elif value is None:
                        results.append((SECURITY_TYPE, ("OS up-to-date", "Not Detected")))
                    else:
                        results.append((SECURITY_TYPE, ("OS up-to-date", "No")))
                else:
                    self.checkpoint_handler.Checkpoint("_check_security_info", self.plugin_name, lib.checkpoint.ERROR, msg="Unknown internal security check", check_type=wsc_type)

        return results


    def endpoint_info_report_some(self, endpoint_info_plugin_name, endpoint_info_attributes):
        """
        Inform the plugin that some endpoint information is available from the given plugin
        """
        calculate = False
        for (name, value) in endpoint_info_attributes:
#            print name, value
            if self.required_endpoint_properties:
                try:
                    self.required_endpoint_properties.remove(name)
                    self.endpoint_properties[name] = value
                    calculate = True
                except ValueError:
                    pass
            if self.is_used_attribute(name, value):
                calculate = True
        if calculate and not self.required_endpoint_properties:
            self._calculate_elements()


    def endpoint_info_all_done(self):
        """
        Inform the plugin that all endpoint information has been collected
        """
        self.all_done = True
        if self.is_started():
            self._calculate_elements()

        try:
            report = self._create_auth_report()
            self.callback.report_auth_results(report)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_create_auth_report.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)



    def set_ready(self):
        if self.callback:
            self._ready = True
            self._started = True
