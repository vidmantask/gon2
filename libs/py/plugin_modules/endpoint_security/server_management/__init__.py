# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
from plugin_types.server_management import plugin_type_element, plugin_type_config
from components.database.server_common import database_api
from plugin_modules.endpoint_security.server_common import database_schema, version_check_spec 
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

# ??? allowed
from components.auth.server_management import OperationNotAllowedException

from lib.hardware.windows_security_center import *

wsc_values = [("Good", WSC_SECURITY_PROVIDER_HEALTH_GOOD),
#              ("Unknown", WSC_SECURITY_PROVIDER_HEALTH_NOTMONITORED),              
              ("Poor", WSC_SECURITY_PROVIDER_HEALTH_POOR),              
#              ("N/A", WSC_SECURITY_PROVIDER_HEALTH_SNOOZE),              
            ]

class PluginOsSecurity(plugin_type_config.PluginTypeConfig, plugin_type_element.PluginTypeElement):

    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_name = "endpoint_security"
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        
    element_types = ["security_element_id"]
        
    def get_elements(self, element_type):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.SecurityElement)
            element_list = []
            for e in elements:
                element_list.append(self.create_element(e.id, e.name, e.description))
            return element_list
             

    def get_element_count(self, element_type):
        with database_api.ReadonlySession() as dbs:
            return dbs.select_count(database_schema.SecurityElement) 
        
        
    def get_specific_elements(self, internal_element_type, element_ids):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.SecurityElement, database_api.in_(database_schema.SecurityElement.id, element_ids))
            return [self.create_element(e.id, e.name, e.description) for e in elements]
        

    

    def config_get_template(self, element_type, element_id, db_session=None):
        with database_api.SessionWrapper(db_session) as dbs:
            element = None
            if element_id:
                element = self._lookup_element(dbs, element_id)
            config_spec = self._get_config_spec(element)
            return config_spec.get_template()
        
    
    def get_config_enabling(self, element_type):
        return True, True
    
    

    def find_or_create_os_element(self, element, os):
        check_element = None
        for os_check in element.os_checks:
            if os_check.os_name == os:
                return os_check
        os_check = database_schema.SecurityOsCheck()
        os_check.os_name = os
        element.os_checks.append(os_check)
        return os_check

    def _get_config_spec(self, element):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (self.plugin_name,), "Os Security Status", "")

        field = config_spec.add_field("name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field = config_spec.add_field("id", 'ID', ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field("description", 'Description', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        
        if element:
            config_spec.set_values_from_object(element)
        else:
            default_check_element = database_schema.SecurityOsCheck()
            default_check_element.os_allowed = True

        for os in version_check_spec.def_operating_systems:
            if element:
                check_element = self.find_or_create_os_element(element, os)
            else:
                check_element = default_check_element
                
            config_spec.add_field("%s_allowed" % os, '%s Allowed' % os, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=check_element.os_allowed, section=os)
            version_check = version_check_spec.def_version_check.get(os)
            if version_check:
                config_spec.add_field("%s_version_check" % os, 'Check Version', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=check_element.os_version_check, section=os)
                for version_check_dict in version_check:
                    version_name = version_check_dict.get("main_version")
                    sub_versions = version_check_dict.get("sub_versions")
                    if element:
                        element_main_version = False
                        element_sub_version = None
                        for version_check_element in check_element.version_checks:
                            if version_check_element.main_version == version_name:
                                element_main_version = True
                                element_sub_version = version_check_element.sub_version
                                break
                    else:
                        element_main_version = True
                        element_sub_version = None
                        
                        
                    config_spec.add_field("%s_version_check_field_main_%s" % (os, version_name), version_name, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=element_main_version, section=os)
                    if sub_versions:
                        field = config_spec.add_field("%s_version_check_field_sub_%s" % (os, version_name), "Required Service Pack", ConfigTemplateSpec.VALUE_TYPE_STRING, value=element_sub_version, section=os)
                        for value, title in sub_versions:
                            config_spec.add_selection_choice(field, title, value)
            security_check = version_check_spec.def_security_check.get(os)
            if security_check:
                config_spec.add_field("%s_security_check" % os, 'Check Security', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=check_element.os_security_check, section=os)
                for check_dict in security_check:
                    check = check_dict.get("name")
                    check_type = check_dict.get("security_type")
                    existing_security_check_element = None
                    for security_check_element in check_element.security_checks:
                        if security_check_element.type == check_type and security_check_element.name == check:
                            existing_security_check_element = security_check_element
                            break
                    if check_type == 'wsc':
                        config_spec.add_field("%s_security_check_field_active_%s" % (os, check), check_dict.get("title"), ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value = not existing_security_check_element is None, section=os)
                        field = config_spec.add_field("%s_security_check_field_value_%s" % (os, check), "Required value", ConfigTemplateSpec.VALUE_TYPE_STRING, section=os)
                        if existing_security_check_element:
                            config_spec.set_field_value(field, existing_security_check_element.string_value)
                        else:
                            config_spec.set_field_value(field, wsc_values[-1][1])
                            
                        for title, value in wsc_values:
                            config_spec.add_selection_choice(field, title, value)
                    elif check_type == 'internal':
                        self._add_internal_check(config_spec, check, check_dict.get("title"), os, existing_security_check_element)
                        
            
        return config_spec

    def _add_internal_check(self, config_spec, check_name, check_title, os, element):
        if check_name == "security_updates":
            field = config_spec.add_field("%s_security_check_field_active_%s" % (os, check_name), check_title, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, section=os)
            if element:
                config_spec.set_field_value(field, True)
        
    


    def _update_obj_with_template(self, new_element, template, transaction):
#        PluginTypeConfig.self._update_obj_with_template(new_element, template, transaction)
        config_spec = ConfigTemplateSpec(template)
        if not config_spec.get_value("name"):
            raise OperationNotAllowedException("Name cannot be empty")
        row = config_spec.get_field_value_dict()
        self._update_obj_with_row(new_element, row, transaction)
             
        for os in version_check_spec.def_operating_systems:
            check_element = self.find_or_create_os_element(new_element, os)
            transaction.add(check_element)
            check_element.os_allowed = config_spec.get_value("%s_allowed" % os)
            
            version_check = version_check_spec.def_version_check.get(os)
            if version_check:
                check_element.os_version_check = config_spec.get_value("%s_version_check" % os)
                new_version_checks = []
                for version_check_dict in version_check:
                    version_name = version_check_dict.get("main_version")
                    sub_versions = version_check_dict.get("sub_versions")
                    new_main_version = config_spec.get_value("%s_version_check_field_main_%s" % (os, version_name))
                    if sub_versions:
                        new_sub_version = config_spec.get_value("%s_version_check_field_sub_%s" % (os, version_name))
                    else:
                        new_sub_version = None

                    old_element = None
                    for version_check_element in check_element.version_checks:
                        if version_check_element.main_version == version_name:
                            old_element = version_check_element
                            break
                    if old_element:
                        check_element.version_checks.remove(old_element)
                        if not new_main_version and not new_sub_version:
                            transaction.delete(old_element)
                        else:
                            old_element.main_version = version_name
                            old_element.sub_version = new_sub_version
                            new_version_checks.append(old_element)
                    elif new_main_version:
                        old_element = transaction.add(database_schema.VersionCheck())
                        old_element.main_version = version_name
                        old_element.sub_version = new_sub_version
                        new_version_checks.append(old_element)
                        
                for version_check_element in check_element.version_checks:
                    transaction.delete(version_check_element)
                check_element.version_checks = new_version_checks
                        
                        
            security_check = version_check_spec.def_security_check.get(os)
            if security_check:
                check_element.os_security_check = config_spec.get_value("%s_security_check" % os)
                new_security_checks = []
                for check_dict in security_check:
                    check = check_dict.get("name")
                    check_type = check_dict.get("security_type")

                    check_type_activated = config_spec.get_value("%s_security_check_field_active_%s" % (os, check))
                    try:
                        check_type_value = config_spec.get_value("%s_security_check_field_value_%s" % (os, check))
                    except ConfigTemplateSpec.FieldNotFoundException:
                        check_type_value = None
                    
                    existing_security_check_element = None
                    for security_check_element in check_element.security_checks:
                        if security_check_element.type == check_type and security_check_element.name == check:
                            existing_security_check_element = security_check_element
                            break
                    if existing_security_check_element:
                        check_element.security_checks.remove(existing_security_check_element)
                        if not check_type_activated:
                            transaction.delete(existing_security_check_element)
                        else:
                            existing_security_check_element.string_value = check_type_value
                            new_security_checks.append(existing_security_check_element)
                    elif check_type_activated:
                        existing_security_check_element = transaction.add(database_schema.SecurityElementCheck())
                        existing_security_check_element.type = check_type
                        existing_security_check_element.name = check
                        existing_security_check_element.string_value = check_type_value
                        new_security_checks.append(existing_security_check_element)
                
                for security_check_element in check_element.security_checks:
                    transaction.delete(security_check_element)
                check_element.security_checks = new_security_checks
                        
        
        
        
    
    
    def config_create_row(self, element_type, template, dbt):
        #new_key =  dbt.add(Token(**row))
        new_element = dbt.add(database_schema.SecurityElement())
        self._update_obj_with_template(new_element, template, dbt)
        dbt.flush()
        
        return "%s.%s" % (self.plugin_name, new_element.id) 
            
    
    def config_update_row(self, element_type, template, dbt):
        config_spec = ConfigTemplateSpec(template)
        element = self._lookup_element(dbt, config_spec.get_value("id"))
        if element:
            self._update_obj_with_template(element, template, dbt)
            return config_spec.get_value("name")
        else:
            raise Exception("The element was not found in the database")
    
    def config_delete_row(self, element_type, element_id, dbt):
        element = self._lookup_element(dbt, element_id)
        if element:
            element.delete(dbt)
            return True
        return False


    def _lookup_element(self, dbs, element_id):
        return dbs.get(database_schema.SecurityElement, element_id)


