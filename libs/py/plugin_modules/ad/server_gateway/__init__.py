"""
AD plugins for Gateway Server
"""
from __future__ import with_statement
import sys

if sys.platform == 'win32':

    import os.path
    import datetime
    
    import lib.checkpoint
    
    from plugin_modules import ad
    from plugin_modules.ad.server_common.ad_config import Config
    
    
    from plugin_types.server_gateway import plugin_type_traffic
    from plugin_types.server_gateway import plugin_type_user
    
    from plugin_modules.ad.server_common.login_module import LoginModule
    
    
    config = Config(os.path.abspath(os.path.dirname(__file__)))    
    
    
    
    
    class PluginAuthentication(plugin_type_traffic.PluginTypeTraffic, plugin_type_user.PluginTypeUser, LoginModule):
        """
        Server part of AD module, plugging into server
        """
        def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, domain, domain_options):
            plugin_name = domain_options.get("plugin name")
            plugin_type_traffic.PluginTypeTraffic.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
            plugin_type_user.PluginTypeUser.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info, database, management_message_session, access_log_server_session)
            LoginModule.__init__(self, checkpoint_handler, self.plugin_name, domain, domain_options)

            
        @classmethod
        def generator(cls, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
            domains = dict()
            if config.enabled:
                for domain in config.authorization_domains:
                    try:
                        domain_options = config.domain_options.get(domain)
                        plugin = PluginAuthentication(async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session, domain, domain_options)
                        domains[plugin.plugin_name] = plugin
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("load_ad_module.error", domain, lib.checkpoint.ERROR, etype, evalue, etrace)
            return domains
            
    
        def start(self):
            """
            Initiates the login process
            """
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::start", self.plugin_name, lib.checkpoint.DEBUG) as cps:
                self.request_login()
                self.set_started()
    
        def login_cancelled(self):
            self.authenticated = False
            self.set_ready()    
                
        def receive_login(self, login, password, internal_user_login=None):
            LoginModule.receive_login(self, login, password, internal_user_login)
            if not self.must_change_password:
                self.set_ready()
                    
                    
    
        def change_password_cancelled(self):
            if self.must_change_password:
                self.authenticated = False
                self.set_ready()
        
            
        def get_attribute(self, attribute_name):
            if attribute_name == 'domain':
                return self.domain
            elif attribute_name == 'netbios':
                return self.netbios
            else:
                try:
                    user_object = self.get_user_object()
                    return getattr(user_object, attribute_name, None)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("get_attribute", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                    return None
        
        def is_domain(self, dns=None):
            if not dns:
                return True
            return dns.lower() == self.domain.lower()

        
        
        def sso_authenticate_user(self, nt_user_name, user_sid, group_SIDs=None):
            return LoginModule.sso_authenticate_user(self, nt_user_name, user_sid, group_SIDs)
        
        
