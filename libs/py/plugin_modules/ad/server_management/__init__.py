# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys

if sys.platform == 'win32':
    
    import os.path
    
    import lib.checkpoint
    
    from components.database.server_common import schema_api
    from components.database.server_common import database_api
    
    from plugin_modules import ad
    from plugin_modules.ad.server_common.ad_config import Config
    from plugin_modules.ad.server_common.login_module import LoginModule
    import plugin_modules.ad.server_common.ad_util as ad_util
    from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
    
    from plugin_types.server_management import plugin_type_user
    from plugin_types.server_management import plugin_type_config
    from plugin_types.server_management import plugin_type_access_notification
    from plugin_types.server_management import plugin_type_element
   
   
    import win32com.client
    import win32security
    import pythoncom
    
    
    default_props = {
                        #Fetch all users which are not disabled"""
                       "user_query" : "(&(objectCategory=person)(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))",
                       "user_label" : "userPrincipalName,sAMAccountName",
                       "user_login" : "userPrincipalName",
                       "user_info"  : "displayName",
                       "user_name"  : "displayName",
                       
                       "group_query" : "(objectCategory=Group)",
                       "group_label" : "cn", 
                       "group_info"  : "description",
                       
                     } 
    
    
    config = Config(os.path.abspath(os.path.dirname(__file__)))    
    
    
    
    
    SAM_GROUP_OBJECT = 0x10000000 
    SAM_ALIAS_OBJECT = 0x20000000 
    
    
    
    def convert_to_guid (item):
        if item is None: return None
        guid = convert_to_hex (item)
        return "{%s-%s-%s-%s-%s}" % (guid[:8], guid[8:12], guid[12:16], guid[16:20], guid[20:])
    
    def convert_to_hex (item):
        if item is None: return None
        return "".join (["%x" % ord (i) for i in item])
    
    
    def convert_to_rid (py_sid):
        if py_sid is None: return None
        partition = str(py_sid).rpartition('-')
        return partition[2]
    
    
    
    
    class PluginAuthentication(plugin_type_user.PluginTypeUser):
        
        def __init__(self, checkpoint_handler, database, license_handler, domain, domain_options):
            name = domain_options.get("plugin name")
            plugin_type_user.PluginTypeUser.__init__(self, checkpoint_handler, database, license_handler, name)
            self.domain = domain
            self._computers = None
            self.query_prefix = None
            self.connection = None
    
            self.domain = domain
            
            self.options = default_props
            self.options.update(config.domain_options.get(domain))
            
            self._login_module = LoginModule(checkpoint_handler, self.plugin_name, domain, self.options)
            self._login_module.change_password_disabled = True

            self.netbios = self.options.get(config.KEY_NETBIOS_NAME, "")
            self.user_labels = Config.convert_to_list(self.options.get("user_label"))
            self.group_labels = Config.convert_to_list(self.options.get("group_label"))
            self.override_max_page_size = self.options.get(config.KEY_OVERRIDE_MAX_PAGE_SIZE, False)
            
        element_types = ["user", "group"]
    
        @classmethod
        def generator(cls, checkpoint_handler, database, license_handler):
            domains = dict()
            if config.enabled:
                for domain in config.authorization_domains:
                    try:
                        domain_options = config.domain_options.get(domain)
                        plugin = PluginAuthentication(checkpoint_handler, database, license_handler, domain, domain_options)
                        domains[plugin.plugin_name] = plugin
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("load_ad_module.error", domain, lib.checkpoint.ERROR, etype, evalue, etrace)
            return domains
            
            
    
    
        def _get_query_prefix(self):
            if not self.query_prefix:
    
                if self.domain:
                    root_DN = win32com.client.GetObject('LDAP://%s/rootDSE' % self.domain).Get('defaultNamingContext')  # "DC=giritech,DC=com"
                    self.query_prefix = "<LDAP://%s/%s>;" % (self.domain, root_DN)
                else:
                    root_DN = win32com.client.GetObject('LDAP://rootDSE').Get('defaultNamingContext')  # "DC=giritech,DC=com"
                    self.query_prefix = "<LDAP://%s>;" % root_DN
                    
            return self.query_prefix
                    
            
        def _get_connnection(self):
            if not self.connection:
                
                objConnection = win32com.client.Dispatch("ADODB.Connection")
                objConnection.Open("Provider=ADsDSOObject")
                self.connection = objConnection
            return self.connection
    
    
            
    
        def _query(self, query_string, override_max_page_size=False):
            """Auxiliary function to serve as a quick-and-dirty
               wrapper round an ADO query
            """
            try:
                
                #for some twisted reason you have to call this before the dispatch... see http://mail.python.org/pipermail/python-win32/2006-December/005421.html
                pythoncom.CoInitialize() 
                
                command = win32com.client.Dispatch ("ADODB.Command")
                command.ActiveConnection = self._get_connnection()
                #
                # Add any client-specified ADO command properties.
                # NB underscores in the keyword are replaced by spaces.
                #
                # Examples:
                #   "Cache_results" = False => Don't cache large result sets
                #   "Page_size" = 500 => Return batches of this size
                #   "Time Limit" = 30 => How many seconds should the search continue
                #
                if self.override_max_page_size or override_max_page_size:
                    command.Properties("Page size").Value = 50
                
                command.CommandText = self._get_query_prefix() + query_string
                with self.checkpoint_handler.CheckpointScope('query', self.plugin_name, lib.checkpoint.DEBUG, command=command.CommandText):
                    return command.Execute ()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("_query.error", self._get_domain_name(), lib.checkpoint.ERROR, etype, evalue, etrace)
                error_msg = "Error during Active Directory query (domain='%s') : %s" % (self._get_domain_name(), repr(evalue))
                raise Exception(error_msg) 
        
        def _get_domain_name(self):
            if self.domain:
                return self.domain
            else:
                return "default"

        def add_user_config_fields(self, config_spec):
            field = config_spec.add_field("ad_domain", 'Domain', ConfigTemplateSpec.VALUE_TYPE_STRING)
            field.set_attribute_read_only(True)
            
        def add_config_values(self, element_type, element_id, config_spec, db_session):
            config_spec.set_value("ad_domain", self.domain)
            
        def user_exists(self, user_id):
            user_object = ad_util.get_object_by_sid(user_id, self.domain)
            if user_object:
                return True
            return False
            
        def get_login_and_name_from_id(self, user_id, internal_user_login):
            user_object = ad_util.get_object_by_sid(user_id, self.domain)
            if user_object:
                internal_login_name = user_object.userPrincipalName
                if not internal_login_name:
                    internal_login_name = user_object.sAMAccountName
                    login_name = "%s@%s" % (internal_login_name, self.domain)
                else:
                    login_name = internal_login_name
                    
                if not internal_login_name:
                    self.checkpoint_handler.Checkpoint('get_login_and_name_from_id', self.plugin_name, lib.checkpoint.ERROR, message='No login name found for user', user_id=user_id)
                    raise Exception("Found no login name for user with SID '%s'" % (user_id) ) 

                user_name = getattr(user_object, self.options.get("user_name"), "")
                if not user_name:
                    user_name = user_id
                return internal_login_name, login_name, user_name
            else:
                raise Exception("Found no users with id %s" % (user_id) ) 
                
    
        def fetch_users(self, search_filter=None):
            """Get list of AD group names suitable as parameteres to is_logged_in_name"""
    
            import win32security
            
            user_info_field = self.options.get("user_info", "")
            user_properties = set(['name', 'member', 'userPrincipalName', 'displayName', 'objectGUID', 'objectSid', 'cn'])
            user_properties.update(self.user_labels)
            if user_info_field:
                user_properties.add(user_info_field)

            if search_filter:
                if search_filter[-1]!='*':
                    search_filter += '*'
                search_predicates = []
                for user_label_property in self.user_labels:
                    search_predicates.append("(%s=%s)" % (user_label_property, search_filter))
                    if user_info_field:
                        search_predicates.append("(%s=%s)" % (user_info_field, search_filter))
                search_predicate = "(|%s)" % "".join(search_predicates)
                query_str = "(&%s%s)" % (self.options.get("user_query"), search_predicate)
            else:
                query_str = self.options.get("user_query")
                
            query_str += ";%s;subtree" % ",".join(user_properties)
            (objRecordSet, dummy) = self._query(query_str)
            
            if objRecordSet.RecordCount > 0:
                count = 0
                objRecordSet.MoveFirst()
                while not objRecordSet.EOF:

#                        count += 1 
#                        if count % 100 == 0 :
#                            print "%s users fetched" % count

                    name = None
                    for property in self.user_labels:
                        try:
                            name = self._get_recordset_value(objRecordSet, property)
                            if name:
                                break
                        except:
                            pass
                    if name:
                        login_name = unicode(name)
                        if not '@' in login_name:
                            login_name = "%s(@%s)" % (login_name, self.domain)
                        sid = win32security.SID(objRecordSet.Fields["objectSid"].Value)
                        id = win32security.ConvertSidToStringSid(sid)
                        #print name, sid
                        #user.id = convert_to_guid(objRecordSet.Fields["objectGUID"].Value)
                        fullname = self._get_recordset_value(objRecordSet, user_info_field)
                        user = self.create_element(id, login_name, fullname)
                        yield user
                    objRecordSet.MoveNext()
    
    
    
        '''The valid group types are security groups, which can be universal, global or local. This is however not reflected
           in the way that you distinguish them...'''
        def valid_group_type(self, objRecordSet):
            group_type = objRecordSet.Fields["sAMAccountType"].Value
            return group_type == SAM_GROUP_OBJECT or group_type == SAM_ALIAS_OBJECT
        
        def _get_recordset_value(self, objRecordSet, name):
            if not name:
                return ""
            try:
                value = objRecordSet.Fields[name].Value
                if isinstance(value, tuple) or isinstance(value, list):
                    value = value[0]
                if isinstance(value, basestring):
                    value = unicode(value)
                return value
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("_get_recordset_value.error", self.domain, lib.checkpoint.ERROR, etype, evalue, etrace)
                return ""
                            
    
        def fetch_groups(self, search_filter=None):
            """Get list of AD group names suitable as parameteres to is_logged_in_member"""
    
            import win32security
            
            group_info_field = self.options.get("group_info")
            group_properties = set(['sAMAccountType', 'objectGUID', 'objectSid', 'cn'])
            group_properties.update(self.group_labels)
            if group_info_field:
                group_properties.add(group_info_field)

            if search_filter:
                if search_filter[-1]!='*':
                    search_filter += '*'
                search_predicates = []
                for group_label_property in self.group_labels:
                    search_predicates.append("(%s=%s)" % (group_label_property, search_filter))
                    if group_info_field:
                        search_predicates.append("(%s=%s)" % (group_info_field, search_filter))
                search_predicate = "(|%s)" % "".join(search_predicates)
                query_str = "(&%s%s)" % (self.options.get("group_query"), search_predicate)
            else:
                query_str = self.options.get("group_query")
                
            query_str += ";%s;subtree" % ",".join(group_properties)
            (objRecordSet, dummy) = self._query(query_str)
    
            if objRecordSet.RecordCount > 0:
                #count = 0
                objRecordSet.MoveFirst()
                while not objRecordSet.EOF:
                    #count += 1 
                    #if count % 100 == 0 :
                    #    print "%s groups fetched" % count
                    name = None
                    for property in self.group_labels:
                        try:
                            name = self._get_recordset_value(objRecordSet, property)
                            if name:
                                break
                        except:
                            pass
                    
                    if name and self.valid_group_type(objRecordSet):
                        
                        sid = win32security.SID(objRecordSet.Fields["objectSid"].Value)
                        id = win32security.ConvertSidToStringSid(sid)
                        yo = objRecordSet.Fields[group_info_field].Value
                        info = self._get_recordset_value(objRecordSet, group_info_field)
                        
                        #print name, sid
                        #group.id = convert_to_guid(objRecordSet.Fields["objectGUID"].Value)
                        #print name, group.id
                        group = self.create_element(id, "%s (%s)" % (name, self.domain), info)
                        yield group
                    objRecordSet.MoveNext()
                        
    
        def find_users_in_primary_group (self, group_rid):
            users = set()
            search_predicate = "(PrimaryGroupID=%s)" % (group_rid)
            query_str = "(&%s%s)" % (self.options.get("user_query"), search_predicate)
            user_properties = set(['name', 'objectSid'])
            query_str += ";%s;subtree" % ",".join(user_properties)
            (objRecordSet, dummy) = self._query(query_str, override_max_page_size=True)
            if objRecordSet.RecordCount > 0:
                count = 0
                objRecordSet.MoveFirst()
                while not objRecordSet.EOF:
                    user_name = self._get_recordset_value(objRecordSet, 'name')
                    sid = win32security.SID(objRecordSet.Fields["objectSid"].Value)
                    user_id = win32security.ConvertSidToStringSid(sid)
                    users.add((user_id, user_name))
                    objRecordSet.MoveNext()
            
            return users
    
        
        def _get_group_members(self, group_sid, users, seen_groups):
        
            if group_sid in seen_groups:
                return
            seen_groups.add(group_sid)
        
            ''' Add primary group members (refer to http://support.microsoft.com/kb/275523)'''
            group_rid = convert_to_rid(group_sid)
            primary_members = self.find_users_in_primary_group(group_rid)
              
            users.update(primary_members)
              
              
            # Add "normal" members
            try:
                pythoncom.CoInitialize() 
                group = win32com.client.GetObject("LDAP://%s/<SID=%s>" % (self.domain, group_sid))
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("GetObject.error", self.domain, lib.checkpoint.ERROR, etype, evalue, etrace)
                raise Exception("Group with SID='%s' not found" % group_sid)
              
            try:              
                members = group.Get("member")
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("GetMembers.error", self.domain, lib.checkpoint.ERROR, etype, evalue, etrace)
                try:
                    self.checkpoint_handler.Checkpoint("GetMembers.error", self.domain, lib.checkpoint.ERROR, group_name=group.Get("Name"), group_cn=group.Get("cn"), group_sid=group.Name)
                except:
                    pass
                members = []
             
            for member_dn in members:
                try:
#                   pythoncom.CoInitialize() 
                    member = win32com.client.GetObject("LDAP://%s/%s" % (self.domain, member_dn))
                    if member.Class=="user":
                        user_name = member.Name
                        user_sid = win32security.SID(member.objectSid)
                        user_id = win32security.ConvertSidToStringSid(user_sid)
                        users.add((user_id, user_name))
                    elif member.Class=="group":
                        group_sid = win32security.SID(member.objectSid)
                        group_sid = win32security.ConvertSidToStringSid(group_sid)
                        self._get_group_members(group_sid, users, seen_groups)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("GetObject.error", self.domain, lib.checkpoint.ERROR, etype, evalue, etrace)

        def get_group_members(self, group_id):
            users = set()
            seen_groups = set()
            self._get_group_members(group_id, users, seen_groups)
            return users
            
    
    
        def fetch_computers(self):
            """Get list of AD computer names suitable as parameteres to is_logged_in_member"""
    
            if sys.platform != 'win32':
                return # FIXME
    
    
            if self._computers is None:
                import win32security
                
                #(objRecordSet, dummy) = self._query('(objectCategory=computer);cn,objectGUID,objectSid;subtree')
                (objRecordSet, dummy) = self._query('(objectCategory=Computer);cn,objectGUID,objectSid,sAMAccountType;subtree')
        
                computers = []
                if objRecordSet.RecordCount > 0:
                    #count = 0
                    objRecordSet.MoveFirst()
                    while not objRecordSet.EOF:
                        #count += 1 
                        #if count % 100 == 0 :
                        #    print "%s computers fetched" % count
                        name = objRecordSet.Fields["cn"].Value
                        
                        if name:
                            computer = Computer(self.plugin_name)
                            computer.name = name
                            
                            
                            sid = win32security.SID(objRecordSet.Fields["objectSid"].Value)
                            computer.id = win32security.ConvertSidToStringSid(sid)
                            #print name, sid
                            #computer.id = convert_to_guid(objRecordSet.Fields["objectGUID"].Value)
                            #print name, computer.id
                            computers.append(computer)
                            yield computer
                        objRecordSet.MoveNext()
                        
                self._computers = computers
            else:
                for g in self._computers:
                    yield g
    
    
        def get_id_column(self):
            return plugin_type_config.Column(self.plugin_name, 'id',   plugin_type_config.Column.TYPE_STRING, 'SID', hidden=True, is_id=True)
    
    
    
        def get_email(self, user_sid):
            with self.checkpoint_handler.CheckpointScope('get_email', self.plugin_name, lib.checkpoint.DEBUG, user_sid=user_sid):

                # Get email address for user
                user_email = None
                pythoncom.CoInitialize() 
                try:
                    user_object = win32com.client.GetObject("LDAP://%s/<SID=%s>" % (self.domain, user_sid))
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("GetObject.error", self.domain, lib.checkpoint.ERROR, etype, evalue, etrace)
                return user_object.mail


        def get_login_module(self):
            return self._login_module
    

        def get_plugin_title(self):
            return self.domain
        
    #
    #
    #if __name__ == '__main__':
    #    
    #    import datetime
    #
    #    #
    #    # Code contributed by Stian Søiland <stian@soiland.no>
    #    #
    #    def i32(x):
    #      """Converts a long (for instance 0x80005000L) to a signed 32-bit-int.
    #    
    #      Python2.4 will convert numbers >= 0x80005000 to large numbers
    #      instead of negative ints.    This is not what we want for
    #      typical win32 constants.
    #    
    #      Usage:
    #          >>> i32(0x80005000L)
    #          -2147363168
    #      """
    #      # x > 0x80000000L should be negative, such that:
    #      # i32(0x80000000L) -> -2147483648L
    #      # i32(0x80000001L) -> -2147483647L     etc.
    #      return (x&0x80000000L and -2*0x40000000 or 0) + int(x&0x7fffffff)
    #
    #    BASE_TIME = datetime.datetime (1601, 1, 1)
    #    def ad_time_to_datetime (ad_time):
    #      if not ad_time:
    #          return "never"
    #      hi, lo = i32 (ad_time.HighPart), i32 (ad_time.LowPart)
    #      if (lo==-1):
    #          return "never"
    #      
    #      ns100 = (hi << 32) + lo
    #      delta = datetime.timedelta (microseconds=ns100 / 10)
    #      return BASE_TIME + delta
    #    
    #    auth_server = AuthenticationServerAdmin("yo", "giritech.com")
    #    (objRecordSet, dummy) = auth_server._query('(&(objectCategory=person)(objectClass=user)((userAccountControl:1.2.840.113556.1.4.803:=2)));name,member,userPrincipalName,displayName,objectGUID,objectSid,cn,accountExpires;subtree')
    #
    #    objRecordSet.MoveFirst()
    #    while not objRecordSet.EOF:
    #        userPrincipalName = objRecordSet.Fields["userPrincipalName"].Value
    #        accountExpires  = objRecordSet.Fields["accountExpires"].Value
    #        try:
    #            print userPrincipalName, repr(ad_time_to_datetime(accountExpires))
    #        except:
    #            pass
    #        objRecordSet.MoveNext()
        
    class Computer(plugin_type_element.ModuleElementType):
        
        def __init__(self, plugin_name):
            self.id = None
            self.name = ""
            self.long_name = ""
            plugin_type_element.ModuleElementType.__init__(self, "Computer", plugin_name)
            
        def get_id(self):
            return self.id
        
        def get_label(self):
            return self.name
        
        def get_info(self):
            return self.long_name


