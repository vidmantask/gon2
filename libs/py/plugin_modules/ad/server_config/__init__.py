"""
Server config specification plugin  

"""
from __future__ import with_statement

import os.path
import shutil
import sys

from plugin_modules.ad.server_common import ad_util

import lib.checkpoint
import lib.utility.support_file_filter as support_file_filter



import plugin_types.server_config.plugin_type_user_config_specification as plugin_type_user_config_specification

import plugin_modules.ad
import plugin_modules.ad.server_common.ad_config as ad_config

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec


class ConfigSpecificationPluginAD(plugin_type_user_config_specification.PluginTypeUserConfigSpecification):
    ID = 'plugin_ad'
    TITLE = 'Active Directory Plugin Configuration' 
    DESCRIPTION = 'Set up Active Directory Plugin' 

    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_user_config_specification.PluginTypeUserConfigSpecification.__init__(self, checkpoint_handler, server_configuration_all, __name__, "ad")
        self.server_configuration_all = server_configuration_all
        self.enabled = False
        self.template_list = []

    def _get_configurations(self):
        management_config = ad_config.Config(self.get_path_management_ini())
        gateway_config = ad_config.Config(self.get_path_gateway_ini())
        return gateway_config, management_config


    def get_path_management_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_management_plugin_root(server_configuration_all, 'ad')

    def get_path_gateway_ini(self, alternative_server_configuration_all=None):
        server_configuration_all = self.server_configuration_all
        if alternative_server_configuration_all is not None:
            server_configuration_all = alternative_server_configuration_all
        return self.get_server_gateway_plugin_root(server_configuration_all, 'ad')

    def get_version(self):
        return lib.version.Version.create_from_encode(plugin_modules.ad.plugin_module_version)

    def has_config(self):
        return False

    
    def get_type_and_priority(self):
        return ("User", 1)

    def get_config_specification(self):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        config_spec = ConfigTemplateSpec()
        config_spec.init(ConfigSpecificationPluginAD.ID, ConfigSpecificationPluginAD.TITLE, ConfigSpecificationPluginAD.DESCRIPTION)

        gateway_config, management_config = self._get_configurations()

        authorization_domain = dict()
        if management_config.authorization_domains:
            authorization_domain = management_config.domain_options.get(management_config.authorization_domains[0])
        if not authorization_domain and gateway_config.authorization_domains:
            authorization_domain = gateway_config.domain_options.get(gateway_config.authorization_domains[0])
        
        
        config_spec.add_field(ad_config.Config.KEY_ENABLED_NAME, 'Enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=False)
        config_spec.add_field(ad_config.Config.KEY_DNS_NAME, 'Domain (dns)', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
        config_spec.add_field(ad_config.Config.KEY_NETBIOS_NAME, 'Netbios', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)

        enabled = management_config.enabled and gateway_config.enabled
        config_spec.set_value(ad_config.Config.KEY_ENABLED_NAME, enabled)

        if authorization_domain:
            config_spec.set_values_from_dict(authorization_domain)
        elif enabled:
            try:
                netbios, dns = ad_util.get_dns_and_netbios("", self.checkpoint_handler)
                config_spec.set_value(ad_config.Config.KEY_DNS_NAME, dns)
                config_spec.set_value(ad_config.Config.KEY_NETBIOS_NAME, netbios)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_dns_and_netbios", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        
       
        return config_spec.get_template()
        
    def is_enabled(self):
        management_config = ad_config.Config(self.get_path_management_ini())
        return management_config.enabled
    
    def check_template(self, template):
        config_spec = ConfigTemplateSpec(template)
        name = config_spec.get_value(ad_config.Config.KEY_DNS_NAME)
        if not name:
            raise Exception("Domain (dns) must be set")
        config_spec.set_title(name)
        return name
        
    def get_title(self):
        return "Domain (dns)"
        

    def get_config_specifications(self, parent_module_id):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        config_specs = dict()
        
        management_config = ad_config.Config(self.get_path_management_ini())
        

        for authorization_domain_name in management_config.authorization_domains:  
            config_spec = ConfigTemplateSpec()
            config_spec.init(ConfigSpecificationPluginAD.ID, authorization_domain_name, ConfigSpecificationPluginAD.DESCRIPTION)
        
            authorization_domain = management_config.domain_options.get(authorization_domain_name)
        
            
            config_spec.add_field(ad_config.Config.KEY_PLUGIN_NAME, 'Plugin name', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False, read_only=True)
            field = config_spec.add_field(ad_config.Config.KEY_DNS_NAME, 'Domain (dns)', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
            config_spec.add_test_config_action(field, parent_module_id, sub_type=self.ID, title="Test Connection")
            config_spec.add_field(ad_config.Config.KEY_NETBIOS_NAME, 'Netbios', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
            config_spec.add_field(ad_config.Config.USE_QUERY_FOR_GROUP_MEMBERS, 'Query for group members', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True, tooltip="Alternative method for finding group membership, including local security groups.\nRequires access to group membership for service accounts")
            config_spec.add_field(ad_config.Config.KEY_OVERRIDE_MAX_PAGE_SIZE, 'Override Max Page size', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True, tooltip="Enable queries to fetch more than the 'Max Page size' property value (default 1000).\nEnable this in order to be able to show all users/groups in G/On Management.\nEnabling this will reduce performance G/On Management")


            config_spec.set_values_from_dict(authorization_domain)
            
            config_specs[config_spec.get_value(ad_config.Config.KEY_DNS_NAME)] = config_spec.get_template()
       
        return config_specs

    def get_create_config_specification(self, parent_module_id):
        """
        Get config specification from plugin. 
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        config_spec = ConfigTemplateSpec()
        config_spec.init(ConfigSpecificationPluginAD.ID, ConfigSpecificationPluginAD.TITLE, ConfigSpecificationPluginAD.DESCRIPTION)

        field = config_spec.add_field(ad_config.Config.KEY_DNS_NAME, 'Domain (dns)', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
        config_spec.add_test_config_action(field, parent_module_id, sub_type=self.ID, title="Test Connection")
        config_spec.add_field(ad_config.Config.KEY_NETBIOS_NAME, 'Netbios', ConfigTemplateSpec.VALUE_TYPE_STRING, advanced=False)
        config_spec.add_field(ad_config.Config.USE_QUERY_FOR_GROUP_MEMBERS, 'Query for group members', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True, tooltip="Alternative method for finding group membership, including local security groups.\nRequires access to group membership for service accounts")
        config_spec.add_field(ad_config.Config.KEY_OVERRIDE_MAX_PAGE_SIZE, 'Override Max Page size', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, advanced=True, tooltip="Enable queries to fetch more than the 'Max Page size' property value (default 1000).\nEnable this in order to be able to show all users/groups in G/On Management.\nEnabling this will reduce performance G/On Management")

        try:
            netbios, dns = ad_util.get_dns_and_netbios("", self.checkpoint_handler)
            config_spec.set_value(ad_config.Config.KEY_DNS_NAME, dns)
            config_spec.set_value(ad_config.Config.KEY_NETBIOS_NAME, netbios)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("get_dns_and_netbios", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
        
       
        return config_spec.get_template()

    def save(self, template):
        """
        If the config_specification does not match the current instance then None should be returned
        else should a instance of components.server_config_ws.ws.server_config_ws_services_types.ns0.SaveConfigSpecificationResponseType_Def(None).pyclass() be returned.
        """
        if template.get_attribute_name() != ConfigSpecificationPluginAD.ID:
            return None

        config_spec = ConfigTemplateSpec(template)
        
        other_values = config_spec.get_field_value_dict()
        enabled = other_values.pop(ad_config.Config.KEY_ENABLED_NAME)
        
        ad_config.Config.write(self.get_path_management_ini(), enabled, [other_values])
        ad_config.Config.write(self.get_path_gateway_ini(), enabled, [other_values])
        return self.create_save_response(True)

    def save_data(self, enabled, template_list):
        self.enabled = enabled
        self.template_list = template_list

    def finalize(self):
        authorization_domains = []
        for template in self.template_list:
            config_spec = ConfigTemplateSpec(template)
            values = config_spec.get_field_value_dict()
            directory_name = values.get(ad_config.Config.KEY_PLUGIN_NAME)
            if not directory_name:
                domain = values.get(ad_config.Config.KEY_DNS_NAME)
                values[ad_config.Config.KEY_PLUGIN_NAME] = "ad_%s" % (domain.replace(".","_"))
            
            authorization_domains.append(values)
        
        ad_config.Config.write(self.get_path_management_ini(), self.enabled, authorization_domains)
        ad_config.Config.write(self.get_path_gateway_ini(), self.enabled, authorization_domains)
        
    def test_config(self, template, test_type=None):
        try :
            config_spec = ConfigTemplateSpec(template)
            domain = config_spec.get_value(ad_config.Config.KEY_DNS_NAME)
            if not domain:
                return False, "Domain name cannot be empty"
            query_helper = ad_util.QueryHelper(self.checkpoint_handler, domain)
            query_helper.test()
            return True, "AD connection ok"
        except Exception, e:
            return False, e
    
        
        

    def generate_support_package(self, support_package_root):
        """
        This method is called with a folder where the plugin can dump files/info. The information dumped will be packaed to a support package for giritech.
        """
        support_log_filename_abs = os.path.join(support_package_root, 'ad.txt') 
        support_log = lib.checkpoint.CheckpointHandler(lib.checkpoint.CheckpointFilter_true(), lib.checkpoint.CheckpointOutputHandlerTEXT(support_log_filename_abs, False), None)

        support_log.Checkpoint('Hello from ad plugin to Giritech supporter, I am fine today how are you?', plugin_modules.ad.MODULE_ID, lib.checkpoint.DEBUG)
        self._copy_ini_files(support_log, support_package_root, remove_sensitive_info=True)


    def backup(self, backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all):
        """
        This method is called with a folder where the plugin can backup files/info. The information dumped will be included in the backup.
        """
        self._copy_ini_files(backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all)

    def _copy_ini_files(self, logger, dest_root, cb_config_backup=None, alternative_server_configuration_all=None, remove_sensitive_info=False):
        ini_filename_src = os.path.join(self.get_path_management_ini(alternative_server_configuration_all), ad_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'management_%s' % ad_config.Config.CONFIG_FILENAME)
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.ad.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("AD-Plugin ini file not found, see log file.")

        ini_filename_src = os.path.join(self.get_path_gateway_ini(alternative_server_configuration_all), ad_config.Config.CONFIG_FILENAME) 
        if os.path.exists(ini_filename_src):
            ini_filename_dst = os.path.join(dest_root, 'gateway_%s' % ad_config.Config.CONFIG_FILENAME) 
            if remove_sensitive_info:
                support_file_filter.sensitive_copy_file(ini_filename_src, ini_filename_dst)
            else:
                shutil.copyfile(ini_filename_src, ini_filename_dst) 
        else:
            logger.Checkpoint("copy_ini_files.file_not_found", plugin_modules.ad.MODULE_ID, lib.checkpoint.WARNING, ini_filename_src=ini_filename_src)
            if cb_config_backup is not None:
                cb_config_backup.cb_backup_warning("AD-Plugin ini file not found, see log file.")


    def restore(self, restore_log, restore_package_plugin_root, cb_restore):
        ini_filename_src = os.path.join(restore_package_plugin_root, 'management_%s' % ad_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_management_ini(), ad_config.Config.CONFIG_FILENAME)
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)
        
        ini_filename_src = os.path.join(restore_package_plugin_root, 'gateway_%s' % ad_config.Config.CONFIG_FILENAME) 
        ini_filename_dst = os.path.join(self.get_path_gateway_ini(), ad_config.Config.CONFIG_FILENAME) 
        dirname = os.path.dirname(ini_filename_dst)
        if not os.path.exists(dirname): 
            os.makedirs(os.path.dirname(ini_filename_dst))
        shutil.copy(ini_filename_src, ini_filename_dst)
