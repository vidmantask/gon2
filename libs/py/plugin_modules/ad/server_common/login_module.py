"""
AD plugin login module
"""
from __future__ import with_statement
import sys

if sys.platform == 'win32':

    import os.path
    import datetime
    
    import lib.checkpoint
    
    
    from components.communication import message
    
    from components.database.server_common import schema_api
    from components.database.server_common import database_api
    
    from plugin_modules import ad
    from plugin_modules.ad.server_common import ad_util
    
    from plugin_modules.ad.server_common.ad_config import Config
    
    import pywintypes
    import winerror
    import win32security
    import win32api
    import win32net
    import pythoncom
    from win32com.client import GetObject, Dispatch
    import win32com.adsi.adsicon
    
    #
    # Code contributed by Stian Soiland <stian@soiland.no>
    #
    def i32(x):
      """Converts a long (for instance 0x80005000L) to a signed 32-bit-int.
    
      Python2.4 will convert numbers >= 0x80005000 to large numbers
      instead of negative ints.    This is not what we want for
      typical win32 constants.
    
      Usage:
          >>> i32(0x80005000L)
          -2147363168
      """
      # x > 0x80000000L should be negative, such that:
      # i32(0x80000000L) -> -2147483648L
      # i32(0x80000001L) -> -2147483647L     etc.
      return (x&0x80000000L and -2*0x40000000 or 0) + int(x&0x7fffffff)
    
    BASE_TIME = datetime.datetime (1601, 1, 1)
    def ad_time_to_delta (ad_time):
      hi, lo = i32 (ad_time.HighPart), i32 (ad_time.LowPart)
      ns100 = abs((hi << 32) + lo)
      return datetime.timedelta (microseconds=ns100 / 10)
    
    ADS_NAME_TYPE_1779 = 1
    ADS_NAME_TYPE_CANONICAL = 2
    ADS_NAME_TYPE_NT4 = 3
    ADS_NAME_TYPE_DISPLAY = 4
    ADS_NAME_TYPE_DOMAIN_SIMPLE = 5
    ADS_NAME_TYPE_ENTERPRISE_SIMPLE = 6
    ADS_NAME_TYPE_GUID = 7
    ADS_NAME_TYPE_UNKNOWN = 8
    ADS_NAME_TYPE_USER_PRINCIPAL_NAME = 9
    ADS_NAME_TYPE_CANONICAL_EX = 10
    ADS_NAME_TYPE_SERVICE_PRINCIPAL_NAME = 11
    ADS_NAME_TYPE_SID_OR_SID_HISTORY_NAME = 12
    
    ADS_NAME_INITTYPE_DOMAIN = 1
    ADS_NAME_INITTYPE_SERVER = 2
    ADS_NAME_INITTYPE_GC = 3
    
    
    from plugin_types.common.plugin_type_user import LoginModuleBase
    
    
    class LoginModule(LoginModuleBase):
        """
        Server part of AD module, plugging into server
        """
        def __init__(self, checkpoint_handler, plugin_name, domain, domain_options):
            LoginModuleBase.__init__(self)
    
            self.plugin_name = plugin_name
            self.checkpoint_handler = checkpoint_handler
            self.domain = domain
            self.login_postfix = domain.lower()
            self.login = ''
            self.password = ''
            self.user_SID = None
            self.group_SIDs = []
            self.sAMAccountName = None
            self.email = ''
            self._query_helper = ad_util.QueryHelper(checkpoint_handler, self.domain)
            
            self.options = domain_options

            self.netbios = self.options.get(Config.KEY_NETBIOS_NAME, "")

            self.query_for_group_members = self.options.get(Config.USE_QUERY_FOR_GROUP_MEMBERS, False)
            
            if self.options.has_key("password_expiry_warning"):
                self.set_password_warning_days(self.options.get("password_expiry_warning"))
                
            self.change_password_disabled = self.options.get("change_password_disabled", False)                
            
            
    
        def is_logged_in_name(self, login):
            """
            True if the user is logged in as login
            """
            user_sid = login[0]
            user_name = login[1]
            if self.authenticated:
                if user_sid:
                    return self.user_SID == user_sid
            return False
    
        def is_logged_in_member(self, group):
            """
            True if the user is logged in as member of group
            """
            group_sid = group[0]
            name = group[1]
            if self.authenticated:
                if group_sid:
                    try:
                        self.group_SIDs.index(group_sid)
                        return True
                    except:
                        return False
            return False
    
        def get_group_ids_for_logged_in_user(self):
            if self.authenticated:
                return self.group_SIDs
            return []

        def is_authenticated(self):
            return self.authenticated
        
    
        def receive_login(self, login, password, internal_user_login=None):
            
            """
            Recieves result from login and password prompt and continues the authentication proces
            """
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::receive_login", self.plugin_name, lib.checkpoint.DEBUG, login=login):
                self.authenticated = False
                self.login = login
    
                if self.authenticated:
                    return
                if self._CheckPassword(self.login, password, internal_user_login):
                    self.checkpoint_handler.Checkpoint("PluginAuthentication::receive_login.entered", self.plugin_name, lib.checkpoint.DEBUG, login=login, user_sid=self.user_SID, user_email=self.email)
                    self.password = password  # FIXME: Security issue???
                    self.authenticated = True
                    
        def get_normalised_login(self, user_login):
            if '@' in user_login:
                return user_login
            elif self.login_postfix:
                return "%s@%s" % (user_login, self.login_postfix)
            else:
                return user_login
        
        def lookup_user(self, user_login):
            with self.checkpoint_handler.CheckpointScope("PluginAuthentication::lookup_user", self.plugin_name, lib.checkpoint.DEBUG, login=user_login):
                try:
                    user_data = self.lookup_user1(user_login)
                    if user_data:
                        userPrincipalName = user_data["userPrincipalName"].Value
                        if userPrincipalName:
                            return userPrincipalName
                        else:
                            return user_data["sAMAccountName"].Value
                    return None
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("lookup_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                    return None

        def lookup_user1(self, user_login):
            userPrincipalName = self.get_normalised_login(user_login)
            if userPrincipalName:
                (objRecordSet, dummy) = self._query_helper.query("(userPrincipalName=%s);userPrincipalName,sAMAccountName" % userPrincipalName)
                if objRecordSet.RecordCount > 0:
                    objRecordSet.MoveFirst()
                    return objRecordSet.Fields
            
            if '@' in user_login:
                # Only check sAMAccountName if we this is the domain the user has specified
                if self.login_postfix and user_login.lower().endswith("@" + self.login_postfix):
                    check_sAMAccountName = True
                else:
                    check_sAMAccountName = False
            else:
                # No domain suffix specified : we should check sAMAccountName
                check_sAMAccountName = True
                
            if check_sAMAccountName:
                sAMAccountName = user_login.rsplit("@")[0]
                (objRecordSet, dummy) = self._query_helper.query("(sAMAccountName=%s);userPrincipalName,sAMAccountName" % sAMAccountName)
                if objRecordSet.RecordCount > 0:
                    objRecordSet.MoveFirst()
                    return objRecordSet.Fields
            return None
                    
    

        def get_user_object(self):
            pythoncom.CoInitialize()
            if self.domain:
                #print "LDAP://%s/<SID=%s>" % (self.domain, self.user_SID)
                user_object = GetObject("LDAP://%s/<SID=%s>" % (self.domain, self.user_SID))
            else:
                user_object = GetObject("LDAP://<SID=%s>" % self.user_SID)
            return user_object

        def _CheckPassword(self, login, password, internal_user_login=None):
            """
            Private function for validating username and password
            """
            
            if not login:
                self.checkpoint_handler.Checkpoint("login_failed", self.plugin_name, lib.checkpoint.WARNING, msg="No login name specified")
                return False
            if not internal_user_login:
                user_data = self.lookup_user1(login)
                normalised_login = user_data["userPrincipalName"].Value
                domain = None
                if not normalised_login:
                    normalised_login = user_data["sAMAccountName"].Value
                    domain = self.domain
                
                if not normalised_login:
                    self.checkpoint_handler.Checkpoint("login_failed", self.plugin_name, lib.checkpoint.WARNING, msg="Could not find login name for user", login=login)
                    return False
            else:
                normalised_login = internal_user_login
                if '@' in normalised_login:
                    domain = None
                else:
                    domain = self.domain
                    
                
            handle = None
            try:
                handle = win32security.LogonUser(normalised_login, domain, password, win32security.LOGON32_LOGON_NETWORK, win32security.LOGON32_PROVIDER_DEFAULT)
                user_info = win32security.GetTokenInformation(handle, win32security.TokenUser)
                self.user_SID = win32security.ConvertSidToStringSid(user_info[0])
                user_object = None
                if not self.query_for_group_members:
                    group_SIDs = []
                    for sid in win32security.GetTokenInformation(handle, win32security.TokenGroups):
                        try:
                            group_SIDs.append(win32security.ConvertSidToStringSid(sid[0]))
                        except:
                            (etype, evalue, etrace) = sys.exc_info()
                            self.checkpoint_handler.CheckpointException("GetTokenInformation", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                    self.group_SIDs = group_SIDs
                    try:
                        user_object = self.get_user_object()
                        self.sAMAccountName = user_object.sAMAccountName
                    except:
                        self.checkpoint_handler.Checkpoint("login", self.plugin_name, lib.checkpoint.ERROR, msg="Unable to get samAccountname from AD")
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("login", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                else:
                    try:
                        user_object = self.get_user_object()
                        # The following was converted from http://www.tech-archive.net/Archive/Scripting/microsoft.public.scripting.vbscript/2006-10/msg00368.html
                        user_object.GetInfoEx(["tokenGroups"], 0)
                        user_groups = user_object.Get("tokenGroups")
                        group_sids = [ win32security.SID(g) for g in user_groups]
                        self.group_SIDs = [ win32security.ConvertSidToStringSid(sid) for sid in group_sids]
                        self.sAMAccountName = user_object.sAMAccountName
                    except:
                        self.checkpoint_handler.Checkpoint("login", self.plugin_name, lib.checkpoint.ERROR, msg="Unable to get group membership information from AD")
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("login", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)

                
                if not self.change_password_disabled and user_object:
                    self._check_password_expiration(user_object)
                    
                return True
            except pywintypes.error, (error_code, module, message):
#                error_code = win32api.GetLastError()
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("login_failed", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                

                # check if password has expired and/or needs to be changed
                if not self.change_password_disabled and (error_code==winerror.ERROR_PASSWORD_EXPIRED or error_code==winerror.ERROR_PASSWORD_MUST_CHANGE):
                    if '@' in normalised_login:
                        # use upn for login : have to lookup user in order to get sAMAccountName 
                        user_data = self.lookup_user1(normalised_login)
                        if user_data:
                            self.sAMAccountName = user_data["sAMAccountName"].Value
                    else:
                        # normalised_login is sAMAccountName
                        self.sAMAccountName = normalised_login
                        
                    if self.sAMAccountName:
                        self.password = password
                        self.must_change_password = True
                else:
                    self.login_error_code = error_code
                    self.login_error_message = message
                    
                # Because of the sheer number of Windows-specific errors that can
                # occur here, we have to assume any of them mean that the
                # credentials were not valid.
                
                return False
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("login", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                return False
            finally:
                if handle:
                    handle.Close()  
                    
        def receive_changed_password(self, old_password, new_password):
            if self.change_password_disabled:
                return "It is not possible to change password in this configuration"
            try:
                win32net.NetUserChangePassword(self.domain, self.sAMAccountName, old_password, new_password)
                self.password = new_password
                self.days_to_password_expiration = None
                if self.must_change_password:
                    self.must_change_password = False
                    return self.receive_login(self.login, new_password)
                return None
            except pywintypes.error, (error_code, module, message):
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("change_password_failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                
            return message
    
        def _check_password_expiration(self, user_object):
            if self.change_password_disabled or self._password_expiration_warning_days <= 0:
                return

            try:
                '''
                  Algorithm for checking password expiration is taken from http://msdn.microsoft.com/en-us/library/ms974598.aspx
                '''
                
                #Check if password expires
                intUserAccountControl = user_object.Get("userAccountControl")
                never_expires = intUserAccountControl & win32com.adsi.adsicon.ADS_UF_DONT_EXPIRE_PASSWD
                if never_expires:
                    return 

                #Determine When Password Was Changed
                last_changed_pytime = user_object.PasswordLastChanged
                if not last_changed_pytime:
                    return 
                
                
                # Find max password age
                pythoncom.CoInitialize() 
                root_DN = win32com.client.GetObject('LDAP://%s/rootDSE' % self.domain).Get('defaultNamingContext')  # "DC=giritech,DC=com"
                objDomain = win32com.client.GetObject("LDAP://%s" % root_DN)
                objMaxPwdAge = objDomain.Get("maxPwdAge")
                delta = ad_time_to_delta(objMaxPwdAge)
                
                if delta.days!=0:
                    last_changed = datetime.datetime (year=last_changed_pytime.year,
                                                      month=last_changed_pytime.month,
                                                      day=last_changed_pytime.day,
                                                      hour=last_changed_pytime.hour,
                                                      minute=last_changed_pytime.minute,
                                                      second=last_changed_pytime.second)
                    expire_date = last_changed + delta
                    
                    time_to_expiration = expire_date - datetime.datetime.now()
                    self.days_to_password_expiration = time_to_expiration.days
                     
                    
                    
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("check_password_expiration", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
                
            
        def get_current_user_id(self):
            if self.authenticated:
                return self.user_SID
        
            
        def sso_authenticate_user(self, nt_user_name, user_sid, group_SIDs=None):
            print "sso_authenticate_user"
            if user_sid:
                self.user_SID = user_sid
                try:
                    user_object = self.get_user_object()
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                    self.user_SID = None
                    return False
                
                if group_SIDs is None:
                    try:
                        # The following was converted from http://www.tech-archive.net/Archive/Scripting/microsoft.public.scripting.vbscript/2006-10/msg00368.html
                        user_object.GetInfoEx(["tokenGroups"], 0)
                        user_groups = user_object.Get("tokenGroups")
                        group_sids = [ win32security.SID(g) for g in user_groups]
                        self.group_SIDs = [ win32security.ConvertSidToStringSid(sid) for sid in group_sids]
                        self.sAMAccountName = user_object.sAMAccountName
                    except:
                        self.checkpoint_handler.Checkpoint("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, msg="Unable to get group membership information from AD")
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("sso_authenticate_user", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
                else:
                    self.group_SIDs = group_SIDs

                self.authenticated = True
                return True

            else:
                self.sAMAccountName = nt_user_name.rsplit("/")[-1]
            
            return False
