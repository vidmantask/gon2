import sys
if sys.platform == 'win32':

    ADS_NAME_TYPE_1779 = 1
    ADS_NAME_TYPE_CANONICAL = 2
    ADS_NAME_TYPE_NT4 = 3
    ADS_NAME_TYPE_DISPLAY = 4
    ADS_NAME_TYPE_DOMAIN_SIMPLE = 5
    ADS_NAME_TYPE_ENTERPRISE_SIMPLE = 6
    ADS_NAME_TYPE_GUID = 7
    ADS_NAME_TYPE_UNKNOWN = 8
    ADS_NAME_TYPE_USER_PRINCIPAL_NAME = 9
    ADS_NAME_TYPE_CANONICAL_EX = 10
    ADS_NAME_TYPE_SERVICE_PRINCIPAL_NAME = 11
    ADS_NAME_TYPE_SID_OR_SID_HISTORY_NAME = 12
    
    ADS_NAME_INITTYPE_DOMAIN = 1
    ADS_NAME_INITTYPE_SERVER = 2
    ADS_NAME_INITTYPE_GC = 3

    import win32com.client
    import pythoncom
    import lib.checkpoint
    
    
    def translate_name(distinguishedName, checkpoint_handler):
        try:
            pythoncom.CoInitialize() 
            objTrans = win32com.client.Dispatch("NameTranslate")
            objTrans.Init(ADS_NAME_INITTYPE_GC, "")
            objTrans.Set(ADS_NAME_TYPE_1779, distinguishedName)
            netbios_name = objTrans.Get(ADS_NAME_TYPE_NT4)
            canonical_name = objTrans.Get(ADS_NAME_TYPE_CANONICAL)
            return netbios_name.partition("\\")[0], canonical_name.partition("/")[0]
        except Exception, e:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("Translate distiguished name", "ad_util", lib.checkpoint.ERROR, etype, evalue, etrace)

            
    def get_dns_and_netbios(domain, checkpoint_handler):
        pythoncom.CoInitialize() 
        if domain:
            root_DN = win32com.client.GetObject('LDAP://%s/rootDSE' % domain).Get('defaultNamingContext')  # "DC=giritech,DC=com"
            root = win32com.client.GetObject ("LDAP://%s" % root_DN)
        else:
            root_DN = win32com.client.GetObject('LDAP://rootDSE').Get('defaultNamingContext')  # "DC=giritech,DC=com"
            root = win32com.client.GetObject ("LDAP://%s" % root_DN)
            
        return translate_name(root.distinguishedName, checkpoint_handler)

    def get_object_by_sid(sid, domain=None):
        pythoncom.CoInitialize() 
        if domain:
            user_object = win32com.client.GetObject("LDAP://%s/<SID=%s>" % (domain, sid))
        else:
            user_object = win32com.client.GetObject("LDAP://<SID=%s>" % sid)
        return user_object
    
    class QueryHelper(object):
        
        def __init__(self, checkpoint_handler, domain):
            self.domain = domain
            self.query_prefix = None
            self.connection = None
            self.checkpoint_handler = checkpoint_handler
            
            
        def _get_connnection(self):
            if not self.connection:
                objConnection = win32com.client.Dispatch("ADODB.Connection")
                objConnection.Open("Provider=ADsDSOObject")
                self.connection = objConnection
            return self.connection
            
        def _get_query_prefix(self):
            if not self.query_prefix:
    
                if self.domain:
                    root_DN = win32com.client.GetObject('LDAP://%s/rootDSE' % self.domain).Get('defaultNamingContext')  # "DC=giritech,DC=com"
                    self.query_prefix = "<LDAP://%s/%s>;" % (self.domain, root_DN)
                else:
                    root_DN = win32com.client.GetObject('LDAP://rootDSE').Get('defaultNamingContext')  # "DC=giritech,DC=com"
                    self.query_prefix = "<LDAP://%s>;" % root_DN
                    
            return self.query_prefix
            
        def test(self):
            self._get_query_prefix()
    
        def query(self, query_string):
            """Auxiliary function to serve as a quick-and-dirty
               wrapper round an ADO query
            """
            try:
                
                #for some twisted reason you have to call this before the dispatch... see http://mail.python.org/pipermail/python-win32/2006-December/005421.html
                pythoncom.CoInitialize() 
                
                command = win32com.client.Dispatch ("ADODB.Command")
                command.ActiveConnection = self._get_connnection()
                #
                # Add any client-specified ADO command properties.
                # NB underscores in the keyword are replaced by spaces.
                #
                # Examples:
                #   "Cache_results" = False => Don't cache large result sets
                #   "Page_size" = 500 => Return batches of this size
                #   "Time Limit" = 30 => How many seconds should the search continue
                #
                command.Properties("Page size").Value = 50
                command.CommandText = self._get_query_prefix() + query_string
                return command.Execute ()
            except Exception, e:
                error_msg = "Error during Active Directory query (domain='%s') (query_string='%s'): %s" % (self._get_domain_name(), query_string, repr(e))
                raise Exception(error_msg)
        
        def _get_domain_name(self):
            if self.domain:
                return self.domain
            else:
                return "default"
            
        
