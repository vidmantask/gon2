"""
Swissbit MicroSmart plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint
import lib.cryptfacility

import random


from components.communication import message

from plugin_modules import micro_smart_swissbit_2
from plugin_types.common import plugin_type_token as database_model
from plugin_types.server_gateway import plugin_type_auth

from components.database.server_common.database_api import *


F4 = "\x01\x00\x01" # 65537

def from_hex(s):
    """
    Utility for converting hex_string to integer string - the reverse of to_hex
    """
    return ''.join(chr(int(x, 16)) for x in s.strip().split())



class PluginAuthentication(plugin_type_auth.PluginTypeAuth):
    """
    Gateway Server part of MicroSmart authentication plugin module.
    """
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'micro_smart_swissbit_2', license_handler, session_info, database, management_message_session, access_log_server_session)

        self.serial = None
        self.serial_authenticated = False
        self.challenge_size = 10
        self.challenge = None

    def start(self):
        """
        Initiates the process
        """
        if self._tunnel_endpoint != None:
            self.set_started()
            self.tunnelendpoint_send(message.Message('get_serial'))
        else:
            raise plugin_type_auth.PluginWaitingForConnectionException()

    def get_predicate_info(self):
        if self.serial_authenticated:
            return [dict(value = self.serial)]
        else:
            return [dict(value =None)]

    def _check_element(self, param, internal_type=None):
        """
        Returns true if the the serial(label) is matching the found serial and the found serial is authenticated
        """
        token_id  = param[0]
        token_label = param[1]
        self.checkpoint_handler.Checkpoint('has_id', self.plugin_name, lib.checkpoint.DEBUG, token_id=token_id, token_label=token_label, serial=self.serial, serial_authenticated='%r' % self.serial_authenticated)
        return token_id == self.serial and self.serial_authenticated

    msg_receivers = ['get_serial_response', 'get_challenge_response']

    def get_serial_response(self, serial):
        with self.checkpoint_handler.CheckpointScope('get_serial_response', self.plugin_name, lib.checkpoint.DEBUG, serial=serial):
            if serial:
                self.serial = serial
                self.challenge = ''.join(chr(random.randrange(60, 127)) for x in range(self.challenge_size))
                self.tunnelendpoint_send(message.Message('get_challenge', challenge=self.challenge))
            else:
                self.set_ready()

    def get_challenge_response(self, challenge_signature):
        with self.checkpoint_handler.CheckpointScope('get_challenge_response', self.plugin_name, lib.checkpoint.DEBUG, serial=self.serial) as cps:
            self.serial_authenticated = False
            if self.challenge != None and challenge_signature != None:
                with database_model.ReadonlySession() as dbs:
                    tokens = dbs.select(database_model.Token, database_model.and_(database_model.Token.serial==self.serial))
                    if len(tokens) == 0:
                        self.checkpoint_handler.CheckpointScope('get_challenge_response.error', self.plugin_name, lib.checkpoint.ERROR, message='Requested serial not found in the database')
                        self.access_log_server_session.report_access_log_gateway_warning(self.plugin_name, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_ID, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_SUMMARY, plugin_type_auth.ACCESS_LOG_INVALID_SERIAL_DETAILS)
                    else:
                        public_key = from_hex(tokens[0].public_key.encode('ascii'))
                        self.serial_authenticated = lib.cryptfacility.verify_public_key_signature(public_key, F4, self.challenge, challenge_signature)
                        if not self.serial_authenticated:
                            self.access_log_server_session.report_access_log_gateway_warning(self.plugin_name, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_ID, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_SUMMARY, plugin_type_auth.ACCESS_LOG_INVALID_SIGNATURE_DETAILS)
                        else:
                            self.get_message_sender().send_message("sink_serial_authenticated", serial=self.serial)

            cps.add_complete_attr(serial_authenticated='%r' % self.serial_authenticated)
            self.set_ready()
