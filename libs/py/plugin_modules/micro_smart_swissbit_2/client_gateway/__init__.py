"""
MicroSmart Swissbit plugin for Gateway Client
"""
from __future__ import with_statement

import sys

import lib.checkpoint
import lib.cryptfacility
import lib.hardware.device
import lib_cpp.swissbit

from components.communication import message

from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_client_runtime_env

import plugin_modules.micro_smart_swissbit_2.client_gateway_common

import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_installer


class PluginTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'micro_smart_swissbit_2'
    """
    Token plugin
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        self._tokens = plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens(checkpoint_handler)
        self._token_id = None

    def get_tokens(self, additional_device_roots):
        return self._tokens.get_tokens(self.plugin_name, additional_device_roots)

    def initialize_token(self, device):
        self._tokens.initialize_token(device)

    def deploy_token(self, token_id, client_knownsecret, servers):
        self._tokens.deploy_token(token_id, client_knownsecret, servers)

    def generate_keypair(self, token_id):
        self._tokens.generate_keypair(token_id)

    def set_serial(self, token_id, serial):
        self._tokens.set_serial(token_id, serial)

    def get_public_key(self, token_id):
        return self._tokens.get_public_key(token_id)

    def set_enrolled(self, token_id):
        self._tokens.set_enrolled(token_id)

    def reset_enrolled(self, token_id):
        self._tokens.reset_enrolled(token_id)

    def is_enrolled(self, token_id):
        return self._tokens.is_enrolled(token_id)

    def get_knownsecret_and_servers(self, token_id):
        return self._tokens.get_knownsecret_and_servers(token_id)

    def set_servers(self, token_id, servers):
        return self._tokens.set_servers(token_id, servers)

    def get_serial(self, token_id = None):
        if token_id is None:
            return self.get_serial_auth()
        return self._tokens.get_serial(token_id)

    def _select_token(self):
        if self._token_id == None:
            tokens = self._tokens.get_tokens(self.plugin_name, self.additional_device_roots)
            if len(tokens) > 0:
                return tokens[0].token_id
            return None
        else:
            return self._token_id

    def _get_serial(self):
        token_id = self._select_token()
        if token_id != None:
            try:
                return unicode(self._tokens.get_serial(token_id))
            except:
                self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.DEBUG, message='No serial found')
                return None
        else:
            self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None

    def _get_challenge(self, challenge):
        token_id = self._select_token()
        if token_id != None:
            msc_filename = self._tokens._get_msc_filename(token_id)
            is_ok, error_message, signature = lib_cpp.swissbit.key_create_challenge_signature(msc_filename, challenge)
            if is_ok:
                return signature
            self.checkpoint_handler.Checkpoint('get_challenge.error', self.plugin_name, lib.checkpoint.ERROR, message='%s' % error_message)
            return None
        else:
            self.checkpoint_handler.Checkpoint('_get_challenge', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None

    msg_receivers = ['get_serial', 'get_challenge']

    def get_serial_auth(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            serial = self._get_serial()
            cps.add_complete_attr(serial=serial)
            self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))

    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            challenge_signature = self._get_challenge(challenge)
            self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))



class PluginClientRuntimeEnv(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv):
    """
    Client runtime envrionment plugin
    """
    PLUGIN_NAME = u'micro_smart_swissbit_2_client_runtime_env'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        self._user_interface = user_interface
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnv.__init__(self, async_service, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, user_interface, additional_device_roots)
        self._instances = {}

    def _build_instances(self):
        msc_tokens = plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        tokens = msc_tokens.get_tokens(PluginClientRuntimeEnv.PLUGIN_NAME, self.additional_device_roots)
        for token in tokens:
            self._instances[token.token_id] = PluginClientRuntimeEnvInstance(self.checkpoint_handler, token.token_id)

    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin.
        """
        self._build_instances()
        return self._instances.keys()

    def get_instance(self, runtime_env_id):
        """
        Return the instance.
        """
        self._build_instances()
        if runtime_env_id in self._instances.keys():
            return self._instances[runtime_env_id]
        return None


class PluginClientRuntimeEnvInstance(plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase):
    def __init__(self, checkpoint_handler, token_id):
        plugin_type_client_runtime_env.PluginTypeClientRuntimeEnvInstanceRWFileSystemBase.__init__(self, checkpoint_handler, PluginClientRuntimeEnv.PLUGIN_NAME, token_id, 'MicroSmartSwissbit2',
                                                                                                   [plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens.DEPLOY_FOLDERNAME, plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens.INIT_FOLDERNAME])
    def set_knownsecret_and_servers(self, knownsecret, servers, generate_keypair):
        endpoint_token_deploy = plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        if generate_keypair:
            endpoint_token_deploy.initialize_token(self.get_root())
            endpoint_token_deploy.generate_keypair(self.get_root())
            # Ignoring exceptions because token might not be deployed yet
            try:
                endpoint_token_deploy.reset_enrolled(self.get_root())
            except:
                self.checkpoint_handler.Checkpoint('reset_enrolled', PluginTokenAndAuth.PLUGIN_NAME, lib.checkpoint.WARNING, message='exception ignored')
        endpoint_token_deploy.deploy_token(self.get_root(), knownsecret, servers)

    def get_knownsecret_and_servers(self):
        endpoint_token_deploy = plugin_modules.micro_smart_swissbit_2.client_gateway_common.SmartCardTokens(self.checkpoint_handler)
        return endpoint_token_deploy.get_knownsecret_and_servers(self.get_root())

    def support_bootification(self, dictionary):
        return (True, "Ok")
