"""
Computer Token plugin module for the Management Server 
"""
from __future__ import with_statement

import os.path
import ConfigParser

import linecache

import lib.checkpoint

from plugin_modules import access_log
from plugin_types.server_management import plugin_type_report

from components.endpoint.server_common import database_schema
from components.database.server_common import schema_api
from components.database.server_common import database_api

from plugin_types.server_management import plugin_type_token


class PluginEndpointToken(plugin_type_token.PluginToken):
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_token.PluginToken.__init__(self, checkpoint_handler, database, license_handler, u'computer_token')
