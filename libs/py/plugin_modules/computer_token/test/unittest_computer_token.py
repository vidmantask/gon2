"""
Unittest of Computer TOken plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile
import time

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility

from components.communication.sim import sim_tunnel_endpoint

from components.database.server_common.connection_factory import ConnectionFactory
import components.database.server_common.database_api as database_api  
import components.management_message.server_gateway.session_send

from plugin_types.client_gateway import plugin_type_token
import plugin_types.client_gateway.plugin_type_client_runtime_env
from plugin_types.common import plugin_type_token as database_model 
import plugin_modules.computer_token.server_gateway
import plugin_modules.computer_token.client_gateway

import appl.gon_client_device_service.gon_client_device_main
import appl.gon_client_device_service.gon_client_device_info

PLUGIN_NAME = u'computer_token'



class AuthCallbackStub(object):
    def plugin_event_state_change(self, name, predicate_info=[]):
        pass


class Auth_plugin_test(unittest.TestCase):
    def setUp(self):
        self.db =  None
#        self.db = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)
        self.db_filename = giri_unittest.mk_temp_filename()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)

        appl.gon_client_device_service.gon_client_device_main.global_checkpoint_handler = giri_unittest.get_checkpoint_handler()
        appl.gon_client_device_service.gon_client_device_main.global_secure_store = appl.gon_client_device_service.gon_client_device_main.SecureStore()
        appl.gon_client_device_service.gon_client_device_main.global_secure_store.generate_serial()
        appl.gon_client_device_service.gon_client_device_main.global_secure_store.generate_keypair()
        
        self.device_http_server = appl.gon_client_device_service.gon_client_device_main.HttpServer(appl.gon_client_device_service.gon_client_device_main.global_checkpoint_handler, appl.gon_client_device_service.gon_client_device_main.global_secure_store)
        self.device_http_server.start()
        
        self.gon_device_info = appl.gon_client_device_service.gon_client_device_info.GOnClientDeviceInfo()
        self.gon_device_info.write_host_and_port(self.device_http_server.listen_host, self.device_http_server.listen_port)
        
    def tearDown(self):
        self.device_http_server.stop()
        self.gon_device_info.reset()
        time.sleep(5)
#        self.db.dispose()
#        try:
#            os.remove(self.db_filename)
#        except:
#            pass

    def test_auth_plugin_simple(self):
        installation_location = giri_unittest.mkdtemp()
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        management_message_session_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSendBlackHole()
        self.plugin_client = plugin_modules.computer_token.client_gateway.ComputerTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
        self.plugin_server = plugin_modules.computer_token.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, None, self.db, management_message_session_send, None)
        self.plugin_server.set_callback(AuthCallbackStub())

        self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
        self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
        
        
        if self.plugin_client.check_access():
            token_serial = self.plugin_client.get_serial('1')
            with database_model.Transaction() as dbt:
                new_key =  dbt.add(database_model.Token())
                new_key.serial = token_serial
                new_key.plugin_type = PLUGIN_NAME
                new_key.public_key = self.plugin_client.get_public_key('1')
                dbt.commit()
    
            self.plugin_server.start()
            giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((token_serial, token_serial)), 5000)
            self.assertEqual(self.plugin_server.check_predicate((token_serial, token_serial)), True)
        
        else:
            self.assertTrue(False, "No Computer Token Found")
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
