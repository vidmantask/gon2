"""
Computer Token Gateway Client
"""
from __future__ import with_statement

import tempfile
import sys
import subprocess
import StringIO

import os
import os.path
import uuid

import lib.checkpoint
import lib.utility

from components.communication import message

import plugin_types.client_gateway.plugin_type_endpoint as pte
from plugin_types.client_gateway import plugin_type_auth
from plugin_types.client_gateway import plugin_type_token
import plugin_types.client_gateway.plugin_type_token

import appl.gon_client_device_service.gon_client_device_service_proxy


class ComputerTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u'computer_token'
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, ComputerTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, ComputerTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        self.computer_token_proxy = appl.gon_client_device_service.gon_client_device_service_proxy.GOnClientDeviceServiceProxy()

    def initialize_token(self, token_id=None):
        raise plugin_type_token.Error("Unable to initialize a computer token")

    def get_tokens(self, additional_device_roots):
        tokens = []
        if self.check_access():
            token_id = "ComputerToken"
            token_title = "Computer Token"
            token = plugin_type_token.Token(self.plugin_name, token_id, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, None, token_type_title="ComputerToken", token_title=token_title, token_plugin_module_name='computer_token')
            token.token_serial = self.computer_token_proxy.get_serial()
            if self.is_enrolled():
                token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED
            tokens.append(token)
        return tokens

    def deploy_token(self, token_id, client_knownsecret, servers):
        raise plugin_type_token.Error("Unable to deploy a computer_token")

    def generate_keypair(self, token_id=None):
        raise plugin_type_token.Error("Unable to generate keypair for a computer_token")

    def get_public_key(self, token_id=None):
        return self.computer_token_proxy.get_public_key()

    def set_serial(self, token_id, serial):
        raise plugin_type_token.Error("Unable to set serial on a computer token")
        
    def set_enrolled(self, token_id=None):
        self.computer_token_proxy.set_enrolled()

    def reset_enrolled(self, token_id=None):
        self.computer_token_proxy.reset_enrolled()

    def is_enrolled(self, token_id=None):
        return self.computer_token_proxy.is_enrolled()
    
    def get_knownsecret_and_servers(self, token_id):
        raise plugin_type_token.Error("Unable to get knownsecret and servers from a computer_token")

    def set_servers(self, token_id, servers):
        raise plugin_type_token.Error("Unable to set servers on a computer token")

    def check_access(self, token_id=None):
        return self.computer_token_proxy.get_serial() is not None


#
# Implementation of PluginTypeAuth and PluginTypeToken
#
    def get_serial(self, token_id=None):
        if token_id is None:
            return  self.get_serial_authentication()
        return self.computer_token_proxy.get_serial()

#
# Implementation of PluginTypeAuth
#
    def get_serial_authentication(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            serial = self.computer_token_proxy.get_serial()
            if serial is not None:
                cps.add_complete_attr(serial=serial)
                self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))
                
    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            try:
                challenge_signature = self.computer_token_proxy.sign_challenge(challenge)
                self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("get_challenge.failed", self.plugin_name, lib.checkpoint.DEBUG, etype, evalue, etrace)
                self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=None))


