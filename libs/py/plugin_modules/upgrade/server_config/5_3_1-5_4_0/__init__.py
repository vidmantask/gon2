"""
Version upgrade plugin
"""
import os
import os.path

import lib.checkpoint

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade

            

class UpgradeAuth(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeAuth::upgrade', __name__, lib.checkpoint.DEBUG):
            
            dbapi = schema_api.SchemaFactory.get_creator("auth_server")
            
            
            
            table_criteria = dbapi.create_table("criteria",
                                 schema_api.Column('title', schema_api.String(100)),
                                 schema_api.Column('entity_type', schema_api.String(128)),
                                 schema_api.Column('type', schema_api.String(30), nullable=False),
                                 schema_api.Column('deactivated', schema_api.Boolean, default=False)
                                )
            
            table_aggregating_criteria = dbapi.create_table("aggregating_criteria",
                                                 schema_api.Column('aggregation_kind', schema_api.Integer),
                                                 schema_api.Column('internal_type', schema_api.Integer),
                                                 schema_api.Column('internal_type_name', schema_api.String(64)), # Dependent on internal_type this can be the name of the class the criteria belongs to or the name of the MAP Window which created it
                                                 schema_api.Column('is_true', schema_api.Boolean, default=False),
                                                 inherit_from = table_criteria
                                                )
            

    
            table_criteria_association_table = dbapi.create_table("aggregating_criteria_criteria", autoload=transaction)
            
            dbapi.add_foreign_key_constraint(table_criteria_association_table, "aggregating_criteria_id", table_aggregating_criteria)
            dbapi.add_foreign_key_constraint(table_criteria_association_table, "criteria_id", table_criteria)
            
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            class AggregatingCriteria(object):
                pass

            class CriteriaAssociation(object):
                pass
            
            schema_api.mapper(CriteriaAssociation, table_criteria_association_table)
            schema_api.mapper(AggregatingCriteria, table_aggregating_criteria, 
                              relations = [schema_api.Relation(CriteriaAssociation, 'child_criterias', 
                                                    primaryjoin=table_criteria_association_table.c.aggregating_criteria_id == table_aggregating_criteria.c.id)
                                            ])
            
            criterias = transaction.select(AggregatingCriteria)
            for criteria in criterias:
                if criteria.aggregation_kind == 0 and not criteria.child_criterias:
                    criteria.is_true = True
            

            updater.update_finished(self.to_version)


class UpgradeDialog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeDialog::upgrade', __name__, lib.checkpoint.DEBUG):
            
            dbapi_dialog_ro = schema_api.SchemaFactory.get_creator("dialog_component_ro")
            
            
            dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_statistics",
                                                                                schema_api.Column('launch_id', schema_api.String(256)),
                                                                                schema_api.Column('launch_key', schema_api.String(256)),
                                                                                schema_api.Column('launch_count', schema_api.Integer),
                                                                                schema_api.Column('last_date', schema_api.DateTime)
                                                                                )
            
            dbapi_dialog_table_dialog_tag_properties = dbapi_dialog_ro.create_table("dialog_tag_properties",
                                                                                schema_api.Column('user_id', schema_api.String(1024)),
                                                                                schema_api.Column('tag_name', schema_api.String(256)),
                                                                                schema_api.Column('tag_priority', schema_api.Integer), # sort, tag priority for sorting out contradictions 
                                                                                schema_api.Column('menu_show', schema_api.Boolean, default=False), # Boolean
                                                                                schema_api.Column('menu_caption', schema_api.String(256), default=''), # title
                                                                                schema_api.Column('menu_sortitems', schema_api.String(20)), # launch item property name for sorting. enum. "Sort order" 
                                                                                schema_api.Column('menu_maxitems', schema_api.Integer, default=0), # Max no of launch elements in menu. 0 == All
                                                                                schema_api.Column('menu_showinparent', schema_api.Integer), # Not implemented. Show in parent menu
                                                                                schema_api.Column('menu_removefromparent', schema_api.Integer), # Not implemented.
                                                                                schema_api.Column('menu_overwrite_enabled', schema_api.Boolean, default=False), # BOOL, Overwrie enabled on menu items
                                                                                schema_api.Column('menu_overwrite_show', schema_api.Boolean, default=False), # BOOL,Overwrie show on menu items
                                                                                schema_api.Column('item_enabled', schema_api.Boolean), # BOOL, ENABLED tag
                                                                                schema_api.Column('item_show', schema_api.Boolean), # Bool
                                                                                schema_api.Column('item_autolaunch_once', schema_api.Boolean, default=False), # Bool
                                                                                schema_api.Column('item_autolaunch_first_start', schema_api.Boolean, default=False), # Bool
                                                                                schema_api.Column('auto_menu_all', schema_api.Boolean, default=False), # Bool : if true, tag is automatically placed on all launch items
                                                                                )
            
            dbapi_dialog_table_parentmenutag = dbapi_dialog_ro.create_table("parentmenutag",
                                                                            schema_api.Column('tag_property_id', schema_api.Integer, nullable=False),
                                                                            schema_api.Column('tag_name', schema_api.String(256), nullable=False),
                                                                            )
            
            
            dbapi_dialog_table_dialog_launch_tags = dbapi_dialog_ro.create_table("dialog_launch_tags",
                                                                                schema_api.Column('launch_id', schema_api.String(256), nullable=False),
                                                                                schema_api.Column('user_id', schema_api.String(1024)),
                                                                                schema_api.Column('tag_name', schema_api.String(256))
                                                                                )
            
            dbapi_dialog_table_dialog_launch_tag_generators = dbapi_dialog_ro.create_table("dialog_launch_tag_generators",
                                                                                           schema_api.Column('launch_id', schema_api.Integer),
                                                                                           schema_api.Column('tag_generator', schema_api.String(1000))
                                                                                           )
            
            dbapi_dialog_ro.add_foreign_key_constraint("parentmenutag", "tag_property_id", "dialog_tag_properties")


            updater = schema_api.SchemaFactory.get_updater(dbapi_dialog_ro, transaction, restore_path)
            updater.update_schema()
            
            class DialogTagProperties(object):
                pass
            
            schema_api.mapper(DialogTagProperties, dbapi_dialog_table_dialog_tag_properties)
            
            rec = transaction.add(DialogTagProperties())
            rec.tag_name = "AUTOLAUNCH_FIRST_START"
            rec.user_id = "-1"
            rec.item_autolaunch_first_start = True

            updater.update_finished(self.to_version)


class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")
            
            
            table_launch_element = dbapi.create_table("launch_element",
                                                      schema_api.Column('action_id', schema_api.Integer), # reference to auth engine 
                                                      schema_api.Column('launch_type', schema_api.Integer), # 0=launch_portforward, 1=launch_citrix, 3=cpm, 4=wol
                                                      # Card pane:
                                                      schema_api.Column('command', schema_api.Text),
                                                      schema_api.Column('working_directory', schema_api.Text),
                                                      schema_api.Column('param_file_name', schema_api.Text, default=""),
                                                      schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),
                                                      #schema_api.Column('param_file_template', schema_api.Binary(), nullable=False, default=""),
                                                      schema_api.Column('param_file_template', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('close_with_process', schema_api.Boolean, nullable=False, default=False), 
                                                      schema_api.Column('kill_on_close', schema_api.Boolean, nullable=False, default=False), 
                                                      # For launch_type launch_citrix
                                                      schema_api.Column('citrix_command', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('citrix_metaframe_path', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('citrix_https', schema_api.Boolean, default=False),
                                                      schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                                      schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                                      schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),
                                                      
                                                      schema_api.Column('name', schema_api.String(40)),
                                                      schema_api.Column('menu_title', schema_api.String(256)),
                                                      )
            
            
            table_portforward =    dbapi.create_table("portforward",
                                                      schema_api.Column('launch_element_id', schema_api.Integer(), nullable=False),
                                                      schema_api.Column('line_no', schema_api.Integer(), nullable=False),
                                                      schema_api.Column('server_host', schema_api.Text()),
                                                      schema_api.Column('server_port', schema_api.Integer),
                                                      schema_api.Column('client_host', schema_api.Text(), nullable=False, default="127.0.0.1"),
                                                      schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                                      schema_api.Column('lock_to_process_pid', schema_api.Boolean, nullable=False, default=False),
                                                      schema_api.Column('sub_processes', schema_api.Boolean, nullable=False, default=False), 
                                                      schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                                      )
            
            table_launch_tag_generators = dbapi.create_table("launch_tag_generator",
                                                             schema_api.Column('launch_id', schema_api.Integer, nullable=False),
                                                             schema_api.Column('tag_generator', schema_api.String(1000))
                                                            )
            
            
            
            dbapi.add_foreign_key_constraint(table_portforward, "launch_element_id", table_launch_element)
            dbapi.add_unique_constraint(table_portforward, "line_no", "launch_element_id")
            
            dbapi.add_foreign_key_constraint(table_launch_tag_generators, "launch_id", table_launch_element)


            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            dbapi_dialog_ro = schema_api.SchemaFactory.get_creator("dialog_component_ro")
            updater_dialog = schema_api.SchemaFactory.get_updater(dbapi_dialog_ro, transaction, restore_path)
            
            dbapi_dialog_table_dialog_launch_tag_generators = dbapi_dialog_ro.create_table("dialog_launch_tag_generators", autoload=transaction)
            dbapi_dialog_table_dialog_launch_tag_generators = dbapi_dialog_ro.create_table("dialog_launch_tag_generators", autoload=transaction)
            updater_dialog.update_schema()

            # Move tag generators from dialog to traffic

            class LaunchElement(object):
                pass

            class DialogTagGenerators(object):
                pass

            class TrafficTagGenerators(object):
                pass

            schema_api.mapper(LaunchElement, table_launch_element)
            schema_api.mapper(DialogTagGenerators, dbapi_dialog_table_dialog_launch_tag_generators)
            schema_api.mapper(TrafficTagGenerators, table_launch_tag_generators)
                         
            dialog_tag_generators = transaction.select(DialogTagGenerators)
            for dialog_tag_generator in dialog_tag_generators:
                if transaction.get(LaunchElement, dialog_tag_generator.launch_id):
                    traffic_tag_generator = transaction.add(TrafficTagGenerators())
                    traffic_tag_generator.launch_id = dialog_tag_generator.launch_id 
                    traffic_tag_generator.tag_generator = dialog_tag_generator.tag_generator 

            # Move menu title to traffic - obliterate dialog_element
            
            dbapi_dialog_table_dialog_element = dbapi_dialog_ro.create_table("dialog_element", autoload=transaction)
            
            class DialogElement(object):
                pass
            
            schema_api.mapper(DialogElement, dbapi_dialog_table_dialog_element)

            
            dbapi_template = schema_api.SchemaFactory.get_creator("templates")
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path)
            
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)
            updater_template.update_schema()
            
            class TemplateUse(object):
                pass
            
            class TemplateValue(object):
                pass
            
            schema_api.mapper(TemplateValue, table_template_value)
            schema_api.mapper(TemplateUse, table_template_use)


            dialog_elements = transaction.select(DialogElement)
            for dialog_element in dialog_elements:
                launch_element = transaction.get(LaunchElement, dialog_element.launch_id)
                launch_element.name = launch_element.menu_title = dialog_element.title
                template_use = transaction.select_first(TemplateUse, filter=TemplateUse.element_id==launch_element.action_id)
                if template_use:
                    template_value = transaction.add(TemplateValue())
                    template_value.template_use_id = template_use.id
                    template_value.value = dialog_element.title
                    template_value.field_name = "menu_title" 
                    
                
            # Update param_file_lifetime for launch type 1 actions
            launch_elements = transaction.select(LaunchElement)
            for launch_element in launch_elements:
                if launch_element.launch_type == 1:
                    launch_element.param_file_lifetime = -1
                    launch_element.param_file_name = "%(run_env.temp)/launch_%(portforward.port).ica"
                    template_use = transaction.select_first(TemplateUse, filter=TemplateUse.element_id==launch_element.action_id)
                    if template_use:
                        template_value = transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.field_name=='param_file_lifetime',TemplateValue.template_use_id==template_use.id))
                        if template_value:
                            template_value.value = "-1"
                        
                    

            # Add field enrollment menu action
            
            title = u"Field Enrollment"
            existing_action = transaction.select(LaunchElement, LaunchElement.name==title)
            if not existing_action:
            
                dbapi_dialog_table_dialog_launch_tags = dbapi_dialog_ro.create_table("dialog_launch_tags", autoload=transaction)
    
                dbapi_auth = schema_api.SchemaFactory.get_creator("auth_server")
                updater_auth = schema_api.SchemaFactory.get_updater(dbapi_auth, transaction, restore_path)
                
                
                table_criteria = dbapi_auth.create_table("criteria",
                                     schema_api.Column('title', schema_api.String(100)),
                                     schema_api.Column('entity_type', schema_api.String(128)),
                                     schema_api.Column('type', schema_api.String(30), nullable=False),
                                     schema_api.Column('deactivated', schema_api.Boolean, default=False)
                                    )
                
                table_aggregating_criteria = dbapi_auth.create_table("aggregating_criteria",
                                                     schema_api.Column('aggregation_kind', schema_api.Integer),
                                                     schema_api.Column('internal_type', schema_api.Integer),
                                                     schema_api.Column('internal_type_name', schema_api.String(64)), # Dependent on internal_type this can be the name of the class the criteria belongs to or the name of the MAP Window which created it
                                                     schema_api.Column('is_true', schema_api.Boolean, default=False),
                                                     inherit_from = table_criteria
                                                    )
                updater_auth.update_schema()

                class Criteria(object):
                    pass
                    
                class AggregatingCriteria(Criteria):
                    pass
    
                class DialogLaunchTag(object):
                    pass

                updater_auth.update_schema()
    
                schema_api.mapper(Criteria, table_criteria, 
                                  polymorphic_on = table_criteria.c.type,
                                  polymorphic_identity = 'Criteria')
    
    
                schema_api.mapper(AggregatingCriteria, table_aggregating_criteria,
                                  inherits=Criteria,
                                  polymorphic_identity = 'AggregatingCriteria')
    
                schema_api.mapper(DialogLaunchTag, dbapi_dialog_table_dialog_launch_tags)
                                                  
                criteria = transaction.add(AggregatingCriteria())
                criteria.title = title
                criteria.aggregation_kind = 1
                criteria.internal_type = 3
                
                transaction.flush()
    
                launch_spec = transaction.add(LaunchElement())
                launch_spec.action_id = criteria.id
                launch_spec.launch_type = 2
                launch_spec.command = "endpoint_enrollment"
                launch_spec.name = title
                launch_spec.menu_title = title
                
                launch_spec.working_directory = ''
                launch_spec.close_with_process = False
                launch_spec.kill_on_close = False
                launch_spec.citrix_https = False
                launch_spec.citrix_metaframe_path = ''
                launch_spec.citrix_command = ''
                launch_spec.param_file_name = ''
                launch_spec.param_file_lifetime = 5
                launch_spec.param_file_template = ''
                launch_spec.sso_login = ''
                launch_spec.sso_password = ''
                launch_spec.sso_domain = ''
            
                transaction.flush()
                
                tags=set(['CLIENTOK','SERVEROK','AUTOLAUNCH_FIRST_START'])
    
                for tag in tags:
                    tag_rec = transaction.add(DialogLaunchTag())
                    tag_rec.launch_id = launch_spec.id
                    tag_rec.user_id = -1
                    tag_rec.tag_name = tag
                updater_auth.update_finished(self.to_version)

                updater_auth.update_finished(self.to_version)

            updater.update_finished(self.to_version)
            updater_dialog.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
                        
class UpgradeUser(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, database_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeUser::upgrade', __name__, lib.checkpoint.DEBUG):

            
            dbapi = database_api.SchemaFactory.get_creator("user_component")
            
            
            
            table_user_info = dbapi.create_table("user",
                                                 database_api.Column('internal_id', database_api.String(1024), nullable=False, index=True),
                                                 database_api.Column('external_user_id', database_api.String(1024), nullable=False, index=True),
                                                 database_api.Column('user_plugin', database_api.String(256), nullable=False, index=True),
                                                 database_api.Column('user_login', database_api.String(512), nullable=False, index=True),
                                                 database_api.Column('internal_user_login', database_api.String(1024)),
                                                 database_api.Column('user_name', database_api.String(512)),
                                                 database_api.Column('email', database_api.String(256)),
                                                 database_api.Column('old_my_pc_1', database_api.String(1000)),
                                                 database_api.Column('old_my_pc_2', database_api.String(1000)),
                                                 database_api.Column('last_login', database_api.DateTime),
                                                    )

            updater = database_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            class User(object):
                pass
            
            database_api.mapper(User, table_user_info)
            
            users = transaction.select(User)
            for user in users:
                if user.user_plugin.startswith("ldap"):
                    user.internal_user_login = user.external_user_id
                else:
                    user.internal_user_login = user.user_login
            
            

            updater.update_finished(self.to_version)


class UpgradeTokens(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTokens::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("plugin_type_token")


            table_token = dbapi.create_table("token",
                                             schema_api.Column('plugin_type', schema_api.String(200)),
                                             schema_api.Column('serial', schema_api.String(200)),
                                             schema_api.Column('description', schema_api.String(2000)),
                                             schema_api.Column('casing_number', schema_api.String(2000)),
                                             schema_api.Column('public_key', schema_api.String(2000)),
                                             schema_api.Column('name', schema_api.String(200)),
                                             schema_api.Column('endpoint', schema_api.Boolean, default=False),
                                           )
            
            dbapi.add_unique_constraint(table_token, 'serial')


            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            class Token(object):
                pass
            
            schema_api.mapper(Token, table_token)
            
            tokens = transaction.select(Token)
            for token in tokens:
                token.name = token.serial

            updater.update_finished(self.to_version)

class UpgradeEndpoint(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeEndpoint::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("endpoint_component")
            
            
            table_endpoint_info = dbapi.create_table("endpoint_info",
                                                     schema_api.Column('hash_key', schema_api.String(100)),
                                                     schema_api.Column('first_seen', schema_api.DateTime()),
                                                     schema_api.Column('last_seen', schema_api.DateTime()),
                                                     schema_api.Column('create_session_id', schema_api.String(100))
                                                     )
            
            table_registered_endpoint = dbapi.create_table("registered_endpoint",
                                                     schema_api.Column('rule_id', schema_api.Integer()),
                                                     schema_api.Column('token_serial', schema_api.String(200), nullable=False, index=True),
                                                     schema_api.Column('token_plugin', schema_api.String(100), nullable=False, index=True),
                                                     schema_api.Column('deployed', schema_api.DateTime()),
                                                     schema_api.Column('last_seen', schema_api.DateTime()),
                                                     )
            
            table_registered_endpoint_attribute = dbapi.create_table("registered_endpoint_attribute",
                                                          schema_api.Column('endpoint_id', schema_api.Integer(), nullable=False, index=True),
                                                          schema_api.Column('plugin_name', schema_api.String(100)),
                                                          schema_api.Column('attribute_name', schema_api.String(100)),
                                                          schema_api.Column('attribute_value', schema_api.Text())
                                                        )
            
            
            table_failed_enrollment = dbapi.create_table("failed_enrollment",
                                                          schema_api.Column('token_serial', schema_api.String(200)),
                                                          schema_api.Column('plugin_name', schema_api.String(100)),
                                                          schema_api.Column('unique_session_id', schema_api.String(100)),
                                                          schema_api.Column('error_message', schema_api.String(200)),
                                                          schema_api.Column('timestamp', schema_api.DateTime()),
                                                        )
                        

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            updater.update_finished(self.to_version)


class UpgradeInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_config(self, restore_path_config):
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_config', __name__, lib.checkpoint.DEBUG):
            print restore_path_config, self.from_version, self.to_version
        
            ini_root = os.path.join(restore_path_config, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
            configuration_server_management = components.config.common.ConfigServerManagement()
            configuration_server_management.read_config_file_from_folder(ini_root)

            db_connect_string = configuration_server_management.db_connect_string
            index = db_connect_string.find("://")
            db_type = db_connect_string[0:index]
#            if db_type != "mssql":
#                db_type = "sqlite"
#            configuration_server_management.db_type = db_type
#            configuration_server_management.write_config_file(ini_root)
        
            if db_type == "mssql":
                configuration_server_management.db_type = db_type
                configuration_server_management.write_config_file(ini_root)

if __name__ == '__main__':
    
    print "yo"
