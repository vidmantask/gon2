"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint
import components.database.server_common.database_api

from plugin_types.server_config import plugin_type_upgrade




class UpgradeAccessLog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    class AccessLogSession(object):
        pass

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG):
            dbapi = schema_api.SchemaFactory.get_creator("access_log_component")

            dbapi_session = dbapi.create_table("session",
                                   schema_api.Column('unique_session_id', schema_api.String(20)),
                                   schema_api.Column('server_id', schema_api.Integer),
                                   schema_api.Column('start_ts', schema_api.DateTime),
                                   schema_api.Column('close_ts', schema_api.DateTime),
                                   schema_api.Column('client_ip', schema_api.String(100)),
                                   schema_api.Column('login', schema_api.String(100)),
                                   schema_api.Column('user_sid', schema_api.String(100)),
                                   schema_api.Column('user_email', schema_api.String(100))
                                   )
    
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            schema_api.mapper(self.AccessLogSession, dbapi_session)

            sessions = transaction.select(self.AccessLogSession)
            for session in sessions:
                session.server_id = 1

            updater.update_finished()
            
