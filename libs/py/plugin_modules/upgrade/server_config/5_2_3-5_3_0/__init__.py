"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint

from plugin_types.server_config import plugin_type_upgrade


class UpgradeUsers(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, database_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeUsers::upgrade', __name__, lib.checkpoint.DEBUG):
            
            creator = database_api.SchemaFactory.get_creator("user_component")
            
            
            table_user_info = creator.create_table("user",
                                                 database_api.Column('internal_id', database_api.String(1024), nullable=False, index=True),
                                                 database_api.Column('external_user_id', database_api.String(1024), nullable=False, index=True),
                                                 database_api.Column('user_plugin', database_api.String(256), nullable=False, index=True),
                                                 database_api.Column('user_login', database_api.String(512), nullable=False, index=True),
                                                 database_api.Column('user_name', database_api.String(512)),
                                                 database_api.Column('email', database_api.String(256)),
                                                 database_api.Column('old_my_pc_1', database_api.String(1000)),
                                                 database_api.Column('old_my_pc_2', database_api.String(1000)),
                                                 database_api.Column('last_login', database_api.DateTime),
                                                    )
            
            table_user_pc = creator.create_table("user_pc",
                                                 database_api.Column('user_id', database_api.Integer, nullable=False),
                                                 database_api.Column('line_no', database_api.Integer, nullable=False),
                                                 database_api.Column('my_pc', database_api.String(256)),
                                                 database_api.Column('mac_address', database_api.String(64)),
                                                 database_api.Column('port', database_api.Integer),
                                                 database_api.Column('last_login', database_api.DateTime),
                                                    )
            
            
            
            table_group_info = creator.create_table("group",
                                                    database_api.Column('internal_id', database_api.String(1024), nullable=False, index=True),
                                                    database_api.Column('external_group_id', database_api.String(1024), nullable=False, index=True),
                                                    database_api.Column('group_plugin', database_api.String(256), nullable=False, index=True),
                                                    )
            
            
            creator.add_foreign_key_constraint(table_user_pc, "user_id", table_user_info)
            
            creator.add_unique_constraint(table_user_info, "external_user_id", "user_plugin")
            creator.add_unique_constraint(table_user_info, "user_login")
            creator.add_unique_constraint(table_user_info, "internal_id")
            creator.add_unique_constraint(table_user_pc, "user_id", "line_no")
            
            creator.add_unique_constraint(table_group_info, "external_group_id", "group_plugin")
            creator.add_unique_constraint(table_group_info, "internal_id")
            
            updater = database_api.SchemaFactory.get_updater(creator, transaction, restore_path)
            updater.rename_column("user", "my_pc_1", "old_my_pc_1")
            updater.rename_column("user", "my_pc_2", "old_my_pc_2")
            updater.update_schema()

            class User(object):
                pass

            class UserPC(object):
                pass
            
            database_api.mapper(UserPC, table_user_pc)
            database_api.mapper(User, table_user_info, 
                                relations = [database_api.Relation(UserPC, 'user_pcs')])
            
            my_pc_dict = dict()
            users = transaction.select(User)
            for user in users:
                # Check that the update has not already been made
                if not user.user_pcs:
                    # copy values to new relation
                    if user.old_my_pc_1 or user.old_my_pc_2:
                        user_pc = UserPC()
                        user_pc.my_pc = user.old_my_pc_1
                        user_pc.line_no = 1
                        user.user_pcs.append(user_pc)
                        
                        if user.old_my_pc_2:
                            user_pc = UserPC()
                            user_pc.my_pc = user.old_my_pc_2
                            user_pc.line_no = 2
                            user.user_pcs.append(user_pc)
                    
                

            updater.update_finished()
            

class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):
            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")

            table_launch_element = dbapi.create_table("launch_element",
                                          schema_api.Column('action_id', schema_api.Integer), # reference to auth engine 
                                          schema_api.Column('launch_type', schema_api.Integer), # 0=launch_portforward, 1=launch_citrix, 3=cpm, 4=wol
                                          # Card pane:
                                          schema_api.Column('command', schema_api.Text),
                                          schema_api.Column('working_directory', schema_api.Text, default=""),
                                          schema_api.Column('param_file_name', schema_api.Text, default=""),
                                          schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),
                                          #schema_api.Column('param_file_template', schema_api.Binary(), nullable=False, default=""),
                                          schema_api.Column('param_file_template', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('close_with_process', schema_api.Boolean, nullable=False, default=False), 
                                          schema_api.Column('kill_on_close', schema_api.Boolean, nullable=False, default=False), 
                                          # For launch_type launch_citrix
                                          schema_api.Column('citrix_command', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_metaframe_path', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_https', schema_api.Boolean, default=False),
                                          schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),
                                          )


            table_portforward =    dbapi.create_table("portforward",
                                          schema_api.Column('launch_element_id', schema_api.Integer(), nullable=False),
                                          schema_api.Column('line_no', schema_api.Integer(), nullable=False),
                                          schema_api.Column('server_host', schema_api.Text()),
                                          schema_api.Column('server_port', schema_api.Integer),
                                          schema_api.Column('client_host', schema_api.Text(), nullable=False, default="0.0.0.0"),
                                          schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                          schema_api.Column('lock_to_process_pid', schema_api.Boolean, nullable=False, default=False),
                                          schema_api.Column('sub_processes', schema_api.Boolean, nullable=False, default=False), 
                                          schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                          )

            dbapi.add_foreign_key_constraint(table_portforward, "launch_element_id", table_launch_element)
            dbapi.add_unique_constraint(table_portforward, "line_no", "launch_element_id")

            
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            updater.update_finished()

class UpgradeAccessLog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new columns (external_user_id, plugin_name) has been added and the columns (user_sid, user_email) have been deleted
    Wipe all data
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG):
            dbapi = schema_api.SchemaFactory.get_creator("access_log_component")
            
            
            dbapi_session = dbapi.create_table("session",
                                               schema_api.Column('unique_session_id', schema_api.String(100)),
                                               schema_api.Column('server_id', schema_api.Integer),
                                               schema_api.Column('start_ts', schema_api.DateTime),
                                               schema_api.Column('close_ts', schema_api.DateTime),
                                               schema_api.Column('client_ip', schema_api.String(100)),
                                               schema_api.Column('login', schema_api.String(100)),
                                               schema_api.Column('external_user_id', schema_api.String(1024)),
                                               schema_api.Column('user_plugin', schema_api.String(256)),
                                               )
            
            dbapi_traffic_proxy = dbapi.create_table("traffic_proxy",
                                               schema_api.Column('unique_session_id', schema_api.String(100)),
                                               schema_api.Column('proxy_id', schema_api.Integer()),
                                               schema_api.Column('proxy_id_sub', schema_api.Integer()),
                                               schema_api.Column('start_ts', schema_api.DateTime),
                                               schema_api.Column('close_ts', schema_api.DateTime),
                                               schema_api.Column('client_ip', schema_api.String(100)),
                                               schema_api.Column('client_port', schema_api.Integer()),
                                               schema_api.Column('server_ip', schema_api.String(100)),
                                               schema_api.Column('server_port', schema_api.Integer())
                                               )
            
            
            dbapi_session_error = dbapi.create_table("session_error",
                                               schema_api.Column('unique_session_id', schema_api.String(100)),
                                               schema_api.Column('error_type', schema_api.String(100)),
                                               schema_api.Column('error_source', schema_api.String(100)),
                                               schema_api.Column('error_code', schema_api.String(100)),
                                               schema_api.Column('error_message', schema_api.Text),
                                               schema_api.Column('timestamp', schema_api.DateTime),
                                               )

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            
            class Session(object):   
                @classmethod
                def map_to_table(cls):
                    schema_api.mapper(cls, dbapi_session)
            
            
            class TrafficProxy(object):
                @classmethod
                def map_to_table(cls):
                    schema_api.mapper(cls, dbapi_traffic_proxy)
            
            class SessionError(object):
                
                @classmethod
                def map_to_table(cls):
                    schema_api.mapper(cls, dbapi_session_error)
            
            Session.map_to_table()
            TrafficProxy.map_to_table()
            SessionError.map_to_table()
            
            transaction.delete_all(Session)
            transaction.delete_all(TrafficProxy)
            transaction.delete_all(SessionError)
            
            updater.update_finished()


if __name__ == '__main__':
    
    print "yo"
