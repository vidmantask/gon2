"""
Version upgrade plugin
"""
import os
import os.path

import lib.checkpoint

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade
from components.database.server_common import schema_api




class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")

            table_launch_element = dbapi.create_table("launch_element", autoload=transaction)
            table_port_forward = dbapi.create_table("portforward", autoload=transaction)
            table_launch_tag_generators = dbapi.create_table("launch_tag_generator", autoload=transaction)

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            # Update param_file_lifetime for launch type 1 actions

            dbapi_template = schema_api.SchemaFactory.get_creator("templates")
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path)

            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)

            updater_template.update_schema()

            dbapi_dialog = schema_api.SchemaFactory.get_creator("dialog_component_ro")
            updater_dialog = schema_api.SchemaFactory.get_updater(dbapi_dialog, transaction, restore_path)

            table_dialog_launch_tags = dbapi_dialog.create_table("dialog_launch_tags", autoload=transaction)

            updater_dialog.update_schema()

            class TemplateUse(object):
                pass

            class TemplateValue(object):
                pass

            class LaunchElement(object):
                pass

            class Portforward(object):
                pass

            class DialogLaunchTags(object):
                pass

            class LaunchTagGenerators(object):
                pass


            schema_api.mapper(TemplateValue, table_template_value)
            schema_api.mapper(TemplateUse, table_template_use)
            schema_api.mapper(LaunchElement, table_launch_element)
            schema_api.mapper(Portforward, table_port_forward)
            schema_api.mapper(LaunchTagGenerators, table_launch_tag_generators)
            schema_api.mapper(DialogLaunchTags, table_dialog_launch_tags)

            def get_template_value(field_name, template_element):
                return transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_element.id,
                                                                                      TemplateValue.field_name==field_name))

            def get_portforward(launch_element, line_no):
                return transaction.select_first(Portforward, filter=schema_api.and_(Portforward.launch_element_id == launch_element.id,
                                                                                    Portforward.line_no == line_no))
            def remove_command_option(command, option):
                index = command.find(option)
                if index<0:
                    return command
                end_index = index + len(option)
                while end_index < len(command) and command[end_index] != ' ':
                    end_index += 1
                return command[0:index] + command[end_index+1:]

            #########################################################################################################


            dme_app_templates = transaction.select(TemplateUse, TemplateUse.template_name=='DME_app')
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, template="DME_app", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("https_handling", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("command", template_element)
                if template_value:
                    template_value.value = template_value.value.replace("--url=http", "--url=%(custom_template.web_server_protocol)")
                    template_value.value = remove_command_option(template_value.value, "--handle_scheme_https")
                    template_value.value += " --manifest_url_opt %(custom_template.manifest_url) --keep_offline_button=%(custom_template.editing_saving) --edit_save=%(custom_template.editing_saving) "

                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = remove_command_option(launch_element.command, "--handle_scheme_https")
                launch_element.command += " --manifest_url_opt --keep_offline_button=on --edit_save=on"

            #########################################################################################################

            dme_app_templates = transaction.select(TemplateUse, TemplateUse.template_name=='DME_api_documentation')
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, template="DME_api_documentation", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("https_handling", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("web_server_address", template_element)
                if template_value:
                    template_value.value = "resources.excitor.com"

                template_value = get_template_value("web_server_port", template_element)
                if template_value:
                    template_value.value = "80"

                template_value = get_template_value("folder", template_element)
                if template_value:
                    template_value.value = "manuals/AppBoxDev/index.htm#welcome.htm"


                template_value = get_template_value("command", template_element)
                if template_value:
                    template_value.value = template_value.value.replace("--url=http", "--url=%(custom_template.web_server_protocol)")
                    template_value.value = remove_command_option(template_value.value, "--handle_scheme_https")
                    template_value.value += " --manifest_url_opt %(custom_template.manifest_url) --edit_save=%(custom_template.editing_saving)"

                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = remove_command_option(launch_element.command, "--handle_scheme_https")
                launch_element.command.replace("127.0.0.1:8075/dme/1.0.0/doc/index.html", "resources.excitor.com:80/manuals/AppBoxDev/index.htm#welcome.htm")
                launch_element.command += " --manifest_url_opt --edit_save=on"

                portforward1 = get_portforward(launch_element, 1)
                if portforward1:
                    portforward1.server_host = "resources.excitor.com"
                    portforward1.server_port = 80

                portforward2 = get_portforward(launch_element, 2)
                if portforward2:
                    transaction.delete(portforward2)


            #########################################################################################################

            dme_app_templates = transaction.select(TemplateUse, TemplateUse.template_name=='DME_browser')
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, template="DME_browser", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("https_handling", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("command", template_element)
                if template_value:
                    template_value.value = template_value.value.replace("--url=http", "--url=%(custom_template.web_server_protocol)")
                    template_value.value = remove_command_option(template_value.value, "--handle_scheme_https")
                    template_value.value += " --manifest_url_opt %(custom_template.manifest_url) --keep_offline_button=%(custom_template.editing_saving) --edit_save=%(custom_template.editing_saving) "

                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = remove_command_option(launch_element.command, "--handle_scheme_https")
                launch_element.command += " --manifest_url_opt --keep_offline_button=on --edit_save=on"

            #########################################################################################################

            dme_app_templates = transaction.select(TemplateUse, TemplateUse.template_name=='DME_handle_any_link')
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, template="DME_handle_any_link", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("https_handling", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("command", template_element)
                if template_value:
                    template_value.value = remove_command_option(template_value.value, "--handle_scheme_https")
                    template_value.value += " --keep_offline_button=off --edit_save=off "

                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = remove_command_option(launch_element.command, "--handle_scheme_https")
                launch_element.command += " --keep_offline_button=off --edit_save=off "


            #########################################################################################################

            dme_app_templates = transaction.select(TemplateUse, TemplateUse.template_name=='DME_launch_pad')
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, template="DME_launch_pad", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("command", template_element)
                if template_value:
                    template_value.value = "secure_browser --url=http://not-used:80/index.html --skin=app_fullscreen --skin_back_button=off"

                template_value = get_template_value("dialog_tags", template_element)
                if template_value:
                    template_value.value += ",APPBOX_AVAILABLE_OFFLINE,APPBOX_LAUNCH_DME_LAUNCH_PAD"

                template_value = get_template_value("dialog_tag_generators", template_element)
                if template_value:
                    template_value.value = 'client_ok::IfPlatformIs("DME-mobile")'

                template_value = get_template_value("web_server_address", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("web_server_port", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("folder", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("applicable_os", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("applicable_screen_size", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("progress_bar", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("orientation_lock_small", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("orientation_lock_normal", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("orientation_lock_large", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("orientation_lock_xlarge", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("browser_cache", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("client_side_browser_logging", template_element)
                if template_value:
                    transaction.delete(template_value)

                template_value = get_template_value("https_handling", template_element)
                if template_value:
                    transaction.delete(template_value)


                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = "secure_browser --url=http://not-used:80/index.html --skin=app_fullscreen --skin_back_button=off"

                portforward1 = get_portforward(launch_element, 1)
                if portforward1:
                    transaction.delete(portforward1)

                portforward2 = get_portforward(launch_element, 2)
                if portforward2:
                    transaction.delete(portforward2)

                portforward3 = get_portforward(launch_element, 3)
                if portforward3:
                    transaction.delete(portforward3)

                tag_obj = transaction.add(DialogLaunchTags())
                tag_obj.launch_id = launch_element.id
                tag_obj.user_id = "-1"
                tag_obj.tag_name = "APPBOX_AVAILABLE_OFFLINE"

                tag_obj = transaction.add(DialogLaunchTags())
                tag_obj.launch_id = launch_element.id
                tag_obj.user_id = "-1"
                tag_obj.tag_name = "APPBOX_LAUNCH_DME_LAUNCH_PAD"

                tag_generators = transaction.select(LaunchTagGenerators, LaunchTagGenerators.launch_id == launch_element.id)

                if tag_generators:
                    tag_generator = tag_generators.pop()
                else:
                    tag_generator = transaction.add(LaunchTagGenerators())
                    tag_generator.launch_id = launch_element.id
                tag_generator.tag_generator = 'client_ok::IfPlatformIs("DME-mobile")'

                for tag_generator in tag_generators:
                    transaction.delete(tag_generator)

            #########################################################################################################

            dme_app_templates = transaction.select(TemplateUse, filter=schema_api.or_(TemplateUse.template_name=='win_citrix_xml_service',
                                                                                      TemplateUse.template_name=='win_citrix_xml_service_auto_settings',
                                                                                      TemplateUse.template_name=='win_citrix_web'))

            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, templates="win_citrix_xml_service, win_citrix_xml_service_auto_settings, win_citrix_web", count=len(dme_app_templates))

            for template_element in dme_app_templates:
                template_value = get_template_value("portforward.0.client_host", template_element)
                if template_value:
                    template_value.value = "127.0.0.1"

                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))

                portforward1 = get_portforward(launch_element, 0)
                if portforward1:
                    portforward1.client_host = "127.0.0.1"



            updater.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
            updater_dialog.update_finished(self.to_version)


if __name__ == '__main__':

    print "yo"
