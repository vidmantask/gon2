import os.path
import ConfigParser

class Config(object):
    CONFIG_FILENAME = 'config.ini'

    SECTION_DOMAIN_PREFIX = 'domain'
    KEY_DNS_NAME = 'dns name'
    KEY_NETBIOS_NAME = 'netbios name'
    KEY_OVERRIDE_MAX_PAGE_SIZE = 'override_max_page_size'
    USE_QUERY_FOR_GROUP_MEMBERS = 'use_query_for_group_members'

    SECTION_PLUGIN = 'plugin'
    KEY_ENABLED_NAME = 'enabled'
    
    def __init__(self, path, filename):
        self.authorization_domains = None
        self.domain_options = dict()
        self.path = path
        self.filename = filename
        
        
        self.enabled = True
        
        self._config_ini = ConfigParser.ConfigParser()
        self.load()
        
    def load(self):
        filename = os.path.join(self.path, self.filename)
        if os.path.exists(filename):
            self._config_ini.read(filename)
            try:
                self.enabled = self._config_ini.getboolean(self.SECTION_PLUGIN, self.KEY_ENABLED_NAME)
            except:
                pass
            
            try:
                self.check_and_update_domain_sections()
                
            except Exception, e:
                if self.enabled:
                    raise e
                   
        #self.authorization_domain = self._config_ini.get('authorization', 'domain')

    def check_and_update_domain_sections(self):
        if self.authorization_domains is None:
            self.authorization_domains = []
            domain_sections = []
            sections = self._config_ini.sections()
            sections.sort()
            for section in sections:
                if section.lower().startswith(Config.SECTION_DOMAIN_PREFIX):
                    domain_sections.append(section)
            
            for section in domain_sections:
                try:
                    dns = self._config_ini.get(section, Config.KEY_DNS_NAME)
                except:
                    print "Configuration error. Missing option 'dns name' in section '%s'" % (section)
                    continue
                self.authorization_domains.append(dns)
                options = self._config_ini.options(section)
                config_dict = dict()
                for option in options:
                    config_dict[option] = self._config_ini.get(section, option)
                if not config_dict.get(Config.KEY_NETBIOS_NAME):
                    config_dict[Config.KEY_NETBIOS_NAME] = ""
                option = Config.USE_QUERY_FOR_GROUP_MEMBERS
                if self._config_ini.has_option(section, option):
                    config_dict[option] = self._config_ini.getboolean(section, option)
                else:
                    config_dict[option] = True # Semi hack: For versions older than 5.5, this setting was True and should remain so if not set explicitly
                option = Config.KEY_OVERRIDE_MAX_PAGE_SIZE
                if self._config_ini.has_option(section, option):
                    config_dict[option] = self._config_ini.getboolean(section, option)
                else:
                    config_dict[option] = False
                self.domain_options[dns] = config_dict 


    @classmethod
    def convert_to_list(cls, option_str):
        return [s.strip() for s in option_str.split(",") if s]        

    @classmethod
    def write(cls, path, enabled, authorization_domains, filename):
        config_ini = ConfigParser.ConfigParser()
        for authorization_domain in authorization_domains:
            dns_name = authorization_domain.get(Config.KEY_DNS_NAME)
            if dns_name:
                section_name = '%s %s' %(Config.SECTION_DOMAIN_PREFIX, dns_name)
                config_ini.add_section(section_name)
                for (key,value) in authorization_domain.items():
                    config_ini.set(section_name, key, value)
        
        if not os.path.exists(path):
            os.makedirs(path)
        
        config_filenameabs = os.path.join(path, filename)

        found_plugin_section = False
        if os.path.exists(config_filenameabs):
            config_ini_existing = ConfigParser.ConfigParser()
            config_ini_existing.read(config_filenameabs)
        
            for section in config_ini_existing.sections():
                if not section.startswith(Config.SECTION_DOMAIN_PREFIX):
                    config_ini.add_section(section)
                    for option in config_ini_existing.options(section):
                        config_ini.set(section, option, config_ini_existing.get(section, option))
                    if section==Config.SECTION_PLUGIN:
                        found_plugin_section = True
                        config_ini.set(section, Config.KEY_ENABLED_NAME, enabled)
                elif config_ini.has_section(section):
                    for option in config_ini_existing.options(section):
                        if not config_ini.has_option(section, option):
                            config_ini.set(section, option, config_ini_existing.get(section, option))
        
        if not found_plugin_section:
            config_ini.add_section(cls.SECTION_PLUGIN)
            config_ini.set(cls.SECTION_PLUGIN, Config.KEY_ENABLED_NAME, enabled)
            
        
        config_file = open(config_filenameabs, 'w')
        config_ini.write(config_file)
        config_file.close()


if __name__ == '__main__':
    import tempfile
    import shutil
    
    temp_folder = tempfile.mkdtemp()
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'w')
    test_init_file.write('[access_notification]\n') 
    test_init_file.write('enabled = True\n') 
    test_init_file.write('xxx = 10\n')
    test_init_file.write('[domain 1]\n') 
    test_init_file.write('dns name = domain_xxxxx\n') 
    test_init_file.close()
    

    Config.write(temp_folder, ['domain_a', 'domain_b'])
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'r')
    print test_init_file.read()
    
    test_init_file.close()
    shutil.rmtree(temp_folder)
