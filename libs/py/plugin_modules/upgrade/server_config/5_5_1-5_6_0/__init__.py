"""
Version upgrade plugin
"""
import os
import os.path
import datetime

import lib.checkpoint
import lib.cryptfacility
import lib.config
import lib.appl.upgrade

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade

import ldap_config
import ad_config
import dme_config
import rule_definitions
            

                    


class UpgradeToken(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade token where new columns have been added
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeToken::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("plugin_type_token")
            
            table_token = dbapi.create_table("token",
                                             schema_api.Column('plugin_type', schema_api.String(200)),
                                             schema_api.Column('internal_type', schema_api.String(200)),
                                             schema_api.Column('serial', schema_api.String(200)),
                                             schema_api.Column('description', schema_api.String(2000)),
                                             schema_api.Column('casing_number', schema_api.String(2000)),
                                             schema_api.Column('public_key', schema_api.String(2000)),
                                             schema_api.Column('name', schema_api.String(200)),
                                             schema_api.Column('endpoint', schema_api.Boolean, default=False),
                                             schema_api.Column('licensed', schema_api.Boolean, default=True),
                                             schema_api.Column('created', schema_api.DateTime(), default=datetime.datetime.now()),
                                             schema_api.Column('last_seen', schema_api.DateTime()),
                                           )
            
            
#            dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_element", autoload=transaction)
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater.update_schema()

            class Token(object):
                pass

            schema_api.mapper(Token, table_token)

            tokens = transaction.select(Token)
            for token in tokens:
                if token.plugin_type == 'mobile':
                    token.internal_type = 'ios'
                else:
                    token.internal_type = token.plugin_type
                
                self.checkpoint_handler.Checkpoint('UpgradeToken::upgrade', __name__, lib.checkpoint.DEBUG, name=token.name)
            

            updater.update_finished(self.to_version)

            
class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade traffic - add change password action
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")

            table_launch_element = dbapi.create_table("launch_element", autoload=transaction)
            
            class LaunchElement(object):
                pass

            schema_api.mapper(LaunchElement, table_launch_element) 

            
            title = u"Change Password"
            existing_action = transaction.select(LaunchElement, schema_api.and_(LaunchElement.name==title,
                                                                                LaunchElement.launch_type==2))
            if not existing_action:
                self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, msg="Add Change Password action")
                
                updater_traffic = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
                updater_traffic.update_schema()
                
                dbapi_dialog_ro = schema_api.SchemaFactory.get_creator("dialog_component_ro")
                updater_dialog = schema_api.SchemaFactory.get_updater(dbapi_dialog_ro, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
                dbapi_dialog_table_dialog_launch_tags = dbapi_dialog_ro.create_table("dialog_launch_tags", autoload=transaction)
                updater_dialog.update_schema()
    
                dbapi_auth = schema_api.SchemaFactory.get_creator("auth_server")
                updater_auth = schema_api.SchemaFactory.get_updater(dbapi_auth, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
                table_rule_object = dbapi_auth.create_table("rule_node", autoload=transaction)
                
                updater_auth.update_schema()
                
                class RuleObject(object):
                    pass

                schema_api.mapper(RuleObject, table_rule_object) 

                criteria = transaction.add(RuleObject())
                criteria.title = title
                criteria.description = ""
                criteria.entity_type = None
                criteria.aggregation_kind = 1
                criteria.internal_type_name = "Action"
                
                transaction.flush()

                class DialogLaunchTag(object):
                    pass

                schema_api.mapper(DialogLaunchTag, dbapi_dialog_table_dialog_launch_tags)
                                                  
                launch_spec = transaction.add(LaunchElement())
                launch_spec.action_id = criteria.id
                launch_spec.launch_type = 2
                launch_spec.command = "change_password"
                launch_spec.name = title
                launch_spec.menu_title = title
                
                launch_spec.working_directory = ''
                launch_spec.close_with_process = False
                launch_spec.kill_on_close = False
                launch_spec.citrix_https = False
                launch_spec.citrix_metaframe_path = ''
                launch_spec.citrix_command = ''
                launch_spec.param_file_name = ''
                launch_spec.param_file_lifetime = 5
                launch_spec.param_file_template = ''
                launch_spec.sso_login = ''
                launch_spec.sso_password = ''
                launch_spec.sso_domain = ''
            
                transaction.flush()
                
                tags=set(['CLIENTOK','SERVEROK','_MENU_ROOT'])
    
                for tag in tags:
                    tag_rec = transaction.add(DialogLaunchTag())
                    tag_rec.launch_id = launch_spec.id
                    tag_rec.user_id = -1
                    tag_rec.tag_name = tag

                updater_auth.update_finished(self.to_version)
                updater_dialog.update_finished(self.to_version)
                updater_traffic.update_finished(self.to_version)
            

class UpgradeLDAPInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)


        
    def upgrade_plugin(self, plugin_id, plugin_version, restore_path_plugin):
        if plugin_id != 'ldap':
            return
        
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_plugin', "ldap", lib.checkpoint.DEBUG):
            self.update_config_file(restore_path_plugin, "management_config.ini")
            self.update_config_file(restore_path_plugin, "gateway_config.ini")

    def update_config_file(self, restore_path_plugin, filename):
        management_config = ldap_config.Config(restore_path_plugin, raise_error=True, filename=filename)
        for domain in management_config.authorization_domains:
            dir_name = domain.get(ldap_config.Config.DIRECTORY_NAME)
            domain["plugin name"] = "ldap_%s" % dir_name.replace(".","_")
            
        ldap_config.Config.write(restore_path_plugin, management_config.enabled, management_config.authorization_domains, filename)

class UpgradeADInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)


        
    def upgrade_plugin(self, plugin_id, plugin_version, restore_path_plugin):
        if plugin_id != 'ad':
            return
        
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_plugin', "ad", lib.checkpoint.DEBUG):
            self.update_config_file(restore_path_plugin, "management_config.ini")
            self.update_config_file(restore_path_plugin, "gateway_config.ini")

    def update_config_file(self, restore_path_plugin, filename):
        management_config = ad_config.Config(restore_path_plugin, filename)
        for domain_name, domain in management_config.domain_options.items():
            domain["plugin name"] = "ad_%s" % domain_name.replace(".","_")
            
        ad_config.Config.write(restore_path_plugin, management_config.enabled, management_config.domain_options.values(), filename)


class UpgradeDMEInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        print "yo"
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)


    def upgrade_config(self, restore_path_config):
        restore_path = os.path.split(restore_path_config)[0]
        restore_path_plugin = os.path.join(restore_path, "plugin", "dme")
        if not os.path.exists(restore_path_plugin):
            os.makedirs(restore_path_plugin)
            
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_plugin', "dme", lib.checkpoint.DEBUG):
            value_dict = dict()
            value_dict["dme_server"] = "" 
            value_dict["port"] = "" 
            value_dict["https"] = True 
            
            dme_config.Config.write(restore_path_plugin, value_dict, "management_config.ini")
            dme_config.Config.write(restore_path_plugin, value_dict, "gateway_config.ini")


class UpgradeAccessLog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where new columns (user_login, user_name) has been added in order to improve reporting
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG):
            
            dbapi = schema_api.SchemaFactory.get_creator("access_log_component", checkpoint_handler=self.checkpoint_handler)
            
            dbapi_session = dbapi.create_table("session",
                                               schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                               schema_api.Column('server_id', schema_api.Integer),
                                               schema_api.Column('server_sid', schema_api.String(100)),
                                               schema_api.Column('start_ts', schema_api.DateTime),
                                               schema_api.Column('close_ts', schema_api.DateTime),
                                               schema_api.Column('client_ip', schema_api.String(100)),
                                               schema_api.Column('login', schema_api.String(256)),
                                               schema_api.Column('external_user_id', schema_api.String(1024)),
                                               schema_api.Column('user_plugin', schema_api.String(256)),
                                               schema_api.Column('user_name', schema_api.String(1024)),
                                               schema_api.Column('user_login', schema_api.String(256)),
                                               )
                    
            dbapi_traffic_proxy = dbapi.create_table("traffic_proxy",
                                               schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                               schema_api.Column('proxy_id', schema_api.Integer()),
                                               schema_api.Column('proxy_id_sub', schema_api.Integer()),
                                               schema_api.Column('start_ts', schema_api.DateTime),
                                               schema_api.Column('close_ts', schema_api.DateTime),
                                               schema_api.Column('client_ip', schema_api.String(100)),
                                               schema_api.Column('client_port', schema_api.Integer()),
                                               schema_api.Column('server_ip', schema_api.String(100)),
                                               schema_api.Column('server_port', schema_api.Integer())
                                               )
            
            dbapi_notification = dbapi.create_table("notification",
                                               schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                               schema_api.Column('server_sid', schema_api.String(100)),
                                               schema_api.Column('timestamp', schema_api.DateTime),
                                               schema_api.Column('type', schema_api.String(100)),
                                               schema_api.Column('severity', schema_api.String(100)),
                                               schema_api.Column('source', schema_api.String(100)),
                                               schema_api.Column('code', schema_api.String(100)),
                                               schema_api.Column('summary', schema_api.Text),
                                               schema_api.Column('details', schema_api.Text),
                                               )
            
            dbapi_auth_results = dbapi.create_table("auth_results",
                                               schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                               schema_api.Column('result_type', schema_api.Integer()),
                                               schema_api.Column('criteria_type', schema_api.String(100)),
                                               schema_api.Column('criteria_text', schema_api.String(256)),
                                               )

                    
            
#            dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_element", autoload=transaction)
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater.update_schema()
            
            user_dbapi = schema_api.SchemaFactory.get_creator("user_component")
            table_user_info = user_dbapi.create_table("user", autoload=transaction)

            endpoint_dbapi = schema_api.SchemaFactory.get_creator("endpoint_component")
            table_endpoint_access_log = endpoint_dbapi.create_table("endpoint_access_log", autoload=transaction)

#            updater_user = schema_api.SchemaFactory.get_updater(user_dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            class User(object):
                pass

            schema_api.mapper(User, table_user_info)   

            class EndpointAccessLog(object):
                pass

            schema_api.mapper(EndpointAccessLog, table_endpoint_access_log)   
            
            class Session(object):
                pass
            
            schema_api.mapper(Session, dbapi_session)
            
            class TrafficProxy(object):
                pass
            
            schema_api.mapper(TrafficProxy, dbapi_traffic_proxy)


            class Notification(object):
                pass
            
            schema_api.mapper(Notification, dbapi_notification)


            class AuthResult(object):
                pass
    
            schema_api.mapper(AuthResult, dbapi_auth_results)

            user_dict = dict()
            user_records = transaction.select(User)
            for user_record in user_records:
                user_dict[((user_record.user_plugin, user_record.external_user_id))] = user_record
                
            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, user_count=len(user_dict))                
            
            deletable_sessions = dict()
            sessions = transaction.select(Session)
            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, session_count=len(sessions))                
            for session in sessions:
                if session.user_plugin and session.external_user_id:
                    user = user_dict.get((session.user_plugin, session.external_user_id))
                    if user:
                        session.user_login = user.user_login
                        session.user_name = user.user_name
                    else:
                        session.user_login = session.login
                        session.user_name = "%s (Not Registered)" % session.login
                else:
                    if session.close_ts and not session.login:
                        session_session_duration_sec = (session.close_ts - session.start_ts).seconds
                        if session_session_duration_sec < 30:
                            deletable_sessions[session.unique_session_id] = session
            
            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, deletable_session_count=len(deletable_sessions))                
            not_deletable = set()
            if deletable_sessions:
                
                def update_non_deletable(cls, session_ids):
                    values = transaction.select(cls.unique_session_id, filter=schema_api.in_(cls.unique_session_id, session_ids))
                    not_deletable.update([x.unique_session_id for x in values])
                
                start_index = 0
                while start_index < len(deletable_sessions):
                    session_ids = deletable_sessions.keys()[start_index:start_index+999]
                    start_index += 999
                    
                    update_non_deletable(AuthResult, session_ids)
                    update_non_deletable(Notification, session_ids)
                    update_non_deletable(TrafficProxy, session_ids)
                    update_non_deletable(EndpointAccessLog, session_ids)

            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, not_deletable_session_count=len(not_deletable))
            
            count=0
            for session in deletable_sessions.values():
                if not session.unique_session_id in not_deletable:
                    transaction.delete(session)
                    count += 1                
            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, msg="Deleted %s sessions" % count)

            updater.update_finished(self.to_version)


class UpgradeAux(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where new columns (user_login, user_name) has been added in order to improve reporting
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeAux::upgrade', __name__, lib.checkpoint.DEBUG):
            
            deleted_tables = ["access_log_component_session_error", 
                               "auth_server_aggregating_criteria_criteria",
                               "auth_server_aggregating_criteria", 
                               "auth_server_module_criteria_parameter",
                               "auth_server_module_criteria",
                               "auth_server_module",
                               "auth_server_criteria",
                               "auth_server_action",
                            ]
            
            table_remover = schema_api.AuxTableRemover(transaction, restore_path, tables_to_delete=deleted_tables, checkpoint_handler=self.checkpoint_handler)
            table_remover.remove_tables(self.to_version)
            
            dbapi_auth = schema_api.SchemaFactory.get_creator("auth_server")
            updater_auth = schema_api.SchemaFactory.get_updater(dbapi_auth, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            updater_auth.drop_table("action")
            updater_auth.drop_table("criteria")
            updater_auth.drop_table("module")
            updater_auth.drop_table("module_criteria")
            updater_auth.drop_table("module_criteria_parameter")
            updater_auth.drop_table("aggregating_criteria_criteria")
            updater_auth.drop_table("aggregating_criteria")

            updater_auth.update_schema()
            updater_auth.update_finished(self.to_version)
            
            dbapi_access = schema_api.SchemaFactory.get_creator("access_log_component")
            updater_access = schema_api.SchemaFactory.get_updater(dbapi_access, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            updater_access.drop_table("session_error")

            updater_access.update_schema()
            updater_access.update_finished(self.to_version)
            
            


class UpgradeTemplates(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade traffic - add change password action
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component", checkpoint_handler=self.checkpoint_handler)

            table_launch_element = dbapi.create_table("launch_element", autoload=transaction)
            table_launch_tag_generators = dbapi.create_table("launch_tag_generator", autoload=transaction)
            table_portforward = dbapi.create_table("portforward", autoload=transaction) 
                                                             
            class LaunchElement(object):
                pass

            class LaunchTagGenerators(object):
                pass
            
            class Portforward(object):
                pass
                        

            schema_api.mapper(LaunchElement, table_launch_element) 
            schema_api.mapper(LaunchTagGenerators, table_launch_tag_generators)
            schema_api.mapper(Portforward, table_portforward)

            updater_traffic = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater_traffic.update_schema()
            
            dbapi_template = schema_api.SchemaFactory.get_creator("templates", checkpoint_handler=self.checkpoint_handler)
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)
#            updater_template.update_schema()
            
            class TemplateUse(object):
                pass

            class TemplateValue(object):
                pass
            
            schema_api.mapper(TemplateUse, table_template_use)
            schema_api.mapper(TemplateValue, table_template_value)
                
            template_names = ["built_in_browser_http_proxy", "ios_rdp_con", "ios_itap_rdp_con"]
            template_new_names = ["mobile_built_in_browser_http_proxy", "mobile_rdp_con", "mobile_url_itap_rdp_con"]

            self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, msg="Change ios templates to mobile")

            for index in range(0, len(template_names)):
                template_name = template_names[index]
                new_template_name = template_new_names[index]
                
                template_uses = transaction.select(TemplateUse, TemplateUse.template_name == template_name)

                self.checkpoint_handler.Checkpoint('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG, template_name=template_name, new_template_name=new_template_name, no_of_actions=len(template_uses))
                
                for template_use in template_uses:
                    template_use.template_name = new_template_name
                    
                    template_value_tag_properties = transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_use.id,
                                                                                                                   TemplateValue.field_name=="tag_generator"))
                    if not template_value_tag_properties:
                        self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', template_name, lib.checkpoint.ERROR, mgs="Missing field value for 'tag_generator' in database")
                        continue
                    
                    old_value = template_value_tag_properties.value
                    transaction.delete(template_value_tag_properties)
                    
                    applicable_os = transaction.add(TemplateValue())
                    applicable_os.template_use_id = template_use.id
                    applicable_os.field_name = "applicable_os"
                    applicable_os.value = "iOS"
                    
                    applicable_screen_size = transaction.add(TemplateValue())
                    applicable_screen_size.template_use_id = template_use.id
                    applicable_screen_size.field_name = "applicable_screen_size"
                    if '"iOS-iPhone"' in old_value or '"iOS-iPod"' in old_value:
                        applicable_screen_size.value = '-normal'
                    elif '"iOS-iPad"' in old_value:
                        applicable_screen_size.value = '-xlarge'
                    else:
                        applicable_screen_size.value = ''
                        
                    template_value_dialog_tag_generators = transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_use.id,
                                                                                                                          TemplateValue.field_name=="dialog_tag_generators"))
                    
                    if template_value_dialog_tag_generators:
                        template_value_dialog_tag_generators.value = template_value_dialog_tag_generators.value.replace('%(custom_template.tag_generator)', 'client_ok::IfPlatformIs("%(custom_template.applicable_os)%(custom_template.applicable_screen_size)")')
                    else:
                        self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', template_name, lib.checkpoint.ERROR, mgs="Missing field value for 'dialog_tag_generators' in database")
                        continue
                        
                    launch_spec = transaction.select_first(LaunchElement, filter=LaunchElement.action_id==int(template_use.element_id))
                    if not launch_spec:
                        self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', template_name, lib.checkpoint.WARNING, mgs="No launch spec found for template use with (id, name, element_id)=(%s,%s,%s)" % (template_use.id, template_use.template_name, template_use.element_id))
                    else:
                        self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG, launch_spec_name=launch_spec.name)
                        tag_generators = transaction.select(LaunchTagGenerators, LaunchTagGenerators.launch_id==launch_spec.id)
                        for tag_generator_row in tag_generators:
                            self._replace_ios_tag_generator_value(tag_generator_row)
            
            other_tag_generators = transaction.select(LaunchTagGenerators, schema_api.or_(LaunchTagGenerators.tag_generator.like('%client_ok::IfPlatformIs("iOS-iPhone")%'),
                                                                                          LaunchTagGenerators.tag_generator.like('%client_ok::IfPlatformIs("iOS-iPod")%'),
                                                                                          LaunchTagGenerators.tag_generator.like('%client_ok::IfPlatformIs("iOS-iPad")%'),
                                                                                          )
                                                      )            
            
            for tag_generator_row in other_tag_generators:
                #Find possible template use
                launch_element = transaction.select_first(LaunchElement, filter=LaunchElement.id==tag_generator_row.launch_id)
                if not launch_element:
                    self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', "data_error", lib.checkpoint.ERROR, mgs="No launch spec for tag generator '%s'" % tag_generator_row.launch_id)
                    continue
                template_use = transaction.select_first(TemplateUse, filter = TemplateUse.element_id == str(launch_element.action_id))
                if template_use:
                    template_value_dialog_tag_generators = transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_use.id,
                                                                                                                          TemplateValue.field_name=="dialog_tag_generators"))
                    if template_value_dialog_tag_generators:
                        if "%(custom_template." in template_value_dialog_tag_generators.value:
                            self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', template_use.template_name, lib.checkpoint.WARNING, mgs="Unable to upgrade template with new iOS tag generators:\n Tag Generators contains custom_template field information. Menu Action '%s' must be updated manually" % launch_element.name)                            
                            continue
                        
                        self._replace_ios_template_value(template_value_dialog_tag_generators)
                    
                self._replace_ios_tag_generator_value(tag_generator_row)
                
            # Update citrix launch specs for new ica client                
            citrix_template_names = ["win_citrix", "win_citrix_web", "win_citrix_xml_service_auto_settings", "win_citrix_xml_service"]

            template_uses = transaction.select(TemplateUse, schema_api.in_(TemplateUse.template_name, citrix_template_names))
            
            template_use_ids = [template.id for template in template_uses]
            action_ids = [int(template.element_id) for template in template_uses]

            self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG, msg="update %d win citrix menu actions" % len(action_ids))
            
            index = 0
            launch_specs = []
            template_values_working_directory = []
            template_values_param_file_name = []
            template_values_citrix_command = []
            template_values_citrix_client_program = []
            
            while index < len(template_uses):
                launch_specs.extend(transaction.select(LaunchElement, schema_api.in_(LaunchElement.action_id, action_ids[index:index + schema_api.IN_CLAUSE_LIMIT])))

                template_values_working_directory.extend(transaction.select(TemplateValue, 
                                                                            filter=schema_api.and_(TemplateValue.field_name=="working_directory",
                                                                                                   schema_api.in_(TemplateValue.template_use_id, template_use_ids[index:index + schema_api.IN_CLAUSE_LIMIT]))
                                                                            )
                                                        )
                
                template_values_param_file_name.extend(transaction.select(TemplateValue, 
                                                                            filter=schema_api.and_(TemplateValue.field_name=="param_file_name",
                                                                                                   schema_api.in_(TemplateValue.template_use_id, template_use_ids[index:index + schema_api.IN_CLAUSE_LIMIT]))
                                                                            )
                                                        )
                
                template_values_citrix_command.extend(transaction.select(TemplateValue, 
                                                                            filter=schema_api.and_(TemplateValue.field_name=="citrix_command",
                                                                                                   schema_api.in_(TemplateValue.template_use_id, template_use_ids[index:index + schema_api.IN_CLAUSE_LIMIT]))
                                                                            )
                                                        )
                
                template_values_citrix_client_program.extend(transaction.select(TemplateValue, 
                                                                            filter=schema_api.and_(TemplateValue.field_name=="citrix_client_program",
                                                                                                   schema_api.in_(TemplateValue.template_use_id, template_use_ids[index:index + schema_api.IN_CLAUSE_LIMIT]))
                                                                            )
                                                        )
                
                index = index + schema_api.IN_CLAUSE_LIMIT
                            
            for template_value_working_directory in template_values_working_directory:
                template_value_working_directory.value = "%(run_env.temp)" 
                
            for template_value_param_file_name in template_values_param_file_name:
                template_value_param_file_name.value = "launch_%(portforward.port).ica" 
                
            for template_value_citrix_command in template_values_citrix_command:
                template_value_citrix_command.value = '"%(custom_template.citrix_client_program)" "%(launch.param_file)"' 

            for template_value_citrix_client_program in template_values_citrix_client_program:
                template_value_citrix_client_program.value = "%(cpm.rw_root)/application_clients/win/ICA Client/wfcrun32.exe" 

            for launch_spec in launch_specs:
                self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG, msg="update win citrix menu action", name=launch_spec.name)
                launch_spec.working_directory = "%(run_env.temp)" 
                launch_spec.param_file_name = "launch_%(portforward.port).ica" 
                launch_spec.citrix_command = '"%(cpm.rw_root)/application_clients/win/ICA Client/wfcrun32.exe" "%(launch.param_file)"' 
            
            
            # Update G/On management template with new working folder                
            template_uses = transaction.select(TemplateUse, TemplateUse.template_name == "win_gon_management")
            
            template_use_ids = [template.id for template in template_uses]
            action_ids = [int(template.element_id) for template in template_uses]

            self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG, msg="update %d G/on management menu actions" % len(action_ids))

            index = 0
            launch_specs = []
            template_values_working_directory = []
            
            while index < len(template_uses):
                launch_specs.extend(transaction.select(LaunchElement, schema_api.in_(LaunchElement.action_id, action_ids[index:index + schema_api.IN_CLAUSE_LIMIT])))

                template_values_working_directory.extend(transaction.select(TemplateValue, 
                                                                            filter=schema_api.and_(TemplateValue.field_name=="working_directory",
                                                                                                   schema_api.in_(TemplateValue.template_use_id, template_use_ids[index:index + schema_api.IN_CLAUSE_LIMIT]))
                                                                            )
                                                        )
                index = index + schema_api.IN_CLAUSE_LIMIT
                
                
            for template_value_working_directory in template_values_working_directory:
                template_value_working_directory.value = "%(cpm.rw_root)\gon_client_management\win" 


            for launch_spec in launch_specs:
                self.checkpoint_handler.Checkpoint('UpgradeTemplates::upgrade', __name__, lib.checkpoint.DEBUG, msg="update win citrix menu action", name=launch_spec.name)
                launch_spec.working_directory = "%(cpm.rw_root)\gon_client_management\win" 
                
            # Finish update                
            updater_traffic.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
            
    def _replace_ios_tag_generator_value(self, tag_generator_row):
        tag_generator_row.tag_generator = tag_generator_row.tag_generator.replace('client_ok::IfPlatformIs("iOS-iPhone")', 'client_ok::IfPlatformIs("iOS-normal")') 
        tag_generator_row.tag_generator = tag_generator_row.tag_generator.replace('client_ok::IfPlatformIs("iOS-iPod")', 'client_ok::IfPlatformIs("iOS-normal")') 
        tag_generator_row.tag_generator = tag_generator_row.tag_generator.replace('client_ok::IfPlatformIs("iOS-iPad")', 'client_ok::IfPlatformIs("iOS-xlarge")') 


    def _replace_ios_template_value(self, template_value):
        template_value.value = template_value.value.replace('client_ok::IfPlatformIs("iOS-iPhone")', 'client_ok::IfPlatformIs("iOS-normal")') 
        template_value.value = template_value.value.replace('client_ok::IfPlatformIs("iOS-iPod")', 'client_ok::IfPlatformIs("iOS-normal")') 
        template_value.value = template_value.value.replace('client_ok::IfPlatformIs("iOS-iPad")', 'client_ok::IfPlatformIs("iOS-xlarge")') 



class UpgradeAccess(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade token where new columns have been added
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeAccess::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi_auth = schema_api.SchemaFactory.get_creator("auth_server")
            updater_auth = schema_api.SchemaFactory.get_updater(dbapi_auth, transaction, restore_path, self.checkpoint_handler)
            table_access_right = dbapi_auth.create_table("access_right", autoload=transaction)
            
            updater_auth.update_schema()
            
            class AccessRight(object):
                pass

            schema_api.mapper(AccessRight, table_access_right)
            
            access_rights = transaction.select(AccessRight, schema_api.like(AccessRight.name, "Rule.%"))
            auth_access_defs = rule_definitions.def_rule_types
            for access_right in access_rights:
                name = access_right.name.split(".")[-1]
                access_def = auth_access_defs.get(name) 
                if access_def and access_def.get("long_title"):
                    access_right.title = "%s (Rule)" % access_def.get("long_title")
                    self.checkpoint_handler.Checkpoint('UpgradeAccess::upgrade', __name__, lib.checkpoint.DEBUG, name=access_right.name)

            updater_auth.update_finished(self.to_version)



if __name__ == '__main__':
    
    pass
