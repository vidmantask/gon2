import os.path
import ConfigParser

class Config(object):
    CONFIG_FILENAME = 'config.ini'
    
    my_sections = ["dme_server"]
    

    def __init__(self, path):
        self.path = path
        self.dme_server_config_dict = dict()
        
        self.enabled = True
        
        self._config_ini = ConfigParser.ConfigParser()
        self.load()
        
    def load(self):
        filename = os.path.join(self.path, Config.CONFIG_FILENAME)
        if os.path.exists(filename):
            self._config_ini.read(filename)
            
            try:
                sections = self._config_ini.sections()
                for section in sections:
                    if section.lower() == "dme_server":
                    
                        self.dme_server_config_dict["dme_server"] = self._config_ini.get(section, "dme_server")
                        self.dme_server_config_dict["port"] = self._config_ini.get(section, "port")
                        if self._config_ini.has_option(section, "https"):
                            self.dme_server_config_dict["https"] = self._config_ini.getboolean(section, "https")
                        else:
                            self.dme_server_config_dict["https"] = True
                
            except Exception, e:
                if self.enabled:
                    raise e
                   
        #self.authorization_domain = self._config_ini.get('authorization', 'domain')


    @classmethod
    def convert_to_list(cls, option_str):
        return [s.strip() for s in option_str.split(",") if s]        

    @classmethod
    def write(cls, path, value_dict, filename=None):
        if not filename:
            filename = cls.CONFIG_FILENAME
            
        config_ini = ConfigParser.ConfigParser()
        
        if not os.path.exists(path):
            os.makedirs(path)
        
        config_filenameabs = os.path.join(path, filename)

        if os.path.exists(config_filenameabs):
            config_ini_existing = ConfigParser.ConfigParser()
            config_ini_existing.read(config_filenameabs)
        
            for section in config_ini_existing.sections():
                if not section in cls.my_sections:
                    config_ini.add_section(section)
                    for option in config_ini_existing.options(section):
                        config_ini.set(section, option, config_ini_existing.get(section, option))
        
        config_ini.add_section("dme_server")
        for key, value in value_dict.items():
            config_ini.set("dme_server", key, value)
            
        
        config_file = open(config_filenameabs, 'w')
        config_ini.write(config_file)
        config_file.close()


if __name__ == '__main__':
    import tempfile
    import shutil
    
    temp_folder = tempfile.mkdtemp()
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'w')
    test_init_file.write('[access_notification]\n') 
    test_init_file.write('enabled = True\n') 
    test_init_file.write('xxx = 10\n')
    test_init_file.write('[domain 1]\n') 
    test_init_file.write('dns name = domain_xxxxx\n') 
    test_init_file.close()
    

    Config.write(temp_folder, ['domain_a', 'domain_b'])
    
    test_init_file = open(os.path.join(temp_folder, Config.CONFIG_FILENAME), 'r')
    print test_init_file.read()
    
    test_init_file.close()
    shutil.rmtree(temp_folder)
