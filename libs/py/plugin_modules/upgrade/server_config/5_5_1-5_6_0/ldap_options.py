'''
Created on 07/07/2009

@author: pwl
'''
import ldap

ldap_options = {
                 ldap.OPT_PROTOCOL_VERSION : ldap.VERSION3,
                 ldap.OPT_REFERRALS : 0, # Needed for AD
                 ldap.OPT_DEREF : 1, # Don't fetch aliases                 
               }



