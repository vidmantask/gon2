"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint
import components.database.server_common.schema_api

from plugin_types.server_config import plugin_type_upgrade


class UpgradeUsers(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeUsers::upgrade', __name__, lib.checkpoint.DEBUG):
            dbapi = schema_api.SchemaFactory.get_creator("user_component")

            table_user_info = dbapi.create_table("user",
                                                 schema_api.Column('internal_id', schema_api.String(1024), nullable=False, index=True),
                                                 schema_api.Column('external_user_id', schema_api.String(1024), nullable=False, index=True),
                                                 schema_api.Column('user_plugin', schema_api.String(256), nullable=False, index=True),
                                                 schema_api.Column('user_login', schema_api.String(256), nullable=False, index=True),
                                                 schema_api.Column('email', schema_api.String(256)),
                                                 schema_api.Column('my_pc_1', schema_api.String(1000)),
                                                 schema_api.Column('my_pc_2', schema_api.String(1000)),
                                                 schema_api.Column('last_login', schema_api.DateTime),
                                                    )
             
            
            dbapi.add_unique_constraint(table_user_info, "external_user_id", "user_plugin")
            dbapi.add_unique_constraint(table_user_info, "user_login")
            dbapi.add_unique_constraint(table_user_info, "internal_id")
    
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            table_old_user = dbapi.create_table("user_info", autoload=transaction)
            
            class User(object):
                pass
            
            class OldUser(object):
                pass

            creator_auth = schema_api.SchemaFactory.get_creator("auth_server")
            table_module = creator_auth.create_table("module", autoload=transaction)
            table_module_criteria = creator_auth.create_table("module_criteria", autoload=transaction)
            table_module_criteria_parameter = creator_auth.create_table("module_criteria_parameter", autoload=transaction)
            
            schema_api.mapper(self.Module, table_module)
            schema_api.mapper(self.ModuleCriteria, table_module_criteria, 
                              relations=[schema_api.Relation(self.ModuleCriteriaParameter, 'parameters', backref_property_name = 'module_criteria'),
                                         schema_api.Relation(self.Module, 'module')])
            schema_api.mapper(self.ModuleCriteriaParameter, table_module_criteria_parameter)
            
            schema_api.mapper(User, table_user_info)
            schema_api.mapper(OldUser, table_old_user)
            
            
            old_users = schema_api.select(OldUser)
            for old_user in old_users:
                new_user = User()
                plugin_name, dot, external_user_id = old_user.user_id.partition(".")
                new_user.internal_id = old_user.user_id
                new_user.external_user_id = external_user_id
                new_user.user_plugin = plugin_name
                new_user.email = old_user.email
                new_user.my_pc_1 = old_user.my_pc_1
                new_user.my_pc_2 = old_user.my_pc_2
                new_user.last_login = old_user.last_login
                

            updater.update_finished()
            
