"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint
import components.database.server_common.database_api

from plugin_types.server_config import plugin_type_upgrade

class UpgradeTokenTables(plugin_type_upgrade.PluginTypeUpgradeVersion):
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    class Module(object):
        pass
    
    class ModuleCriteriaParameter(object):
        pass

    class Token(object):
        pass
    
    class ModuleCriteria(object):
        pass
    
    
    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('VersionUpgrade::upgrade', __name__, lib.checkpoint.DEBUG):
            creator = schema_api.SchemaFactory.get_creator("plugin_type_token")
            table_token = creator.create_table("token",
                                     schema_api.Column('plugin_type', schema_api.String(200)),
                                     schema_api.Column('serial', schema_api.String(200)),
                                     schema_api.Column('description', schema_api.String(2000)),
                                     schema_api.Column('casing_number', schema_api.String(2000)),
                                     schema_api.Column('public_key', schema_api.String(2000))
                                    )
            updater = schema_api.SchemaFactory.get_updater(creator, transaction, restore_path)
            updater.update_schema()

            schema_api.mapper(self.Token, table_token)
            
            creator_auth = schema_api.SchemaFactory.get_creator("auth_server")
            table_module = creator_auth.create_table("module", autoload=transaction)
            table_module_criteria = creator_auth.create_table("module_criteria", autoload=transaction)
            table_module_criteria_parameter = creator_auth.create_table("module_criteria_parameter", autoload=transaction)
            
            schema_api.mapper(self.Module, table_module)
            schema_api.mapper(self.ModuleCriteriaParameter, table_module_criteria_parameter)
            schema_api.mapper(self.ModuleCriteria, table_module_criteria, 
                              relations=[schema_api.Relation(self.ModuleCriteriaParameter, 'parameters', backref_property_name = 'module_criteria'),
                                         schema_api.Relation(self.Module, 'module')])
            
            
            try:
                creator_certificate = schema_api.SchemaFactory.get_creator("soft_token")
                table_key = creator_certificate.create_table("key", autoload=transaction)
                self.move_token_data(schema_api, transaction, table_key, "soft_token")
            except schema_api.NoSuchTableError:
                print "Warning: table certificate not found"

            try:
                creator_certificate = schema_api.SchemaFactory.get_creator("micro_smart")
                table_key = creator_certificate.create_table("token", autoload=transaction)
                self.move_token_data(schema_api, transaction, table_key, "micro_smart")
            except schema_api.NoSuchTableError:
                print "Warning: table micro_smart not found"

            try:
                creator_certificate = schema_api.SchemaFactory.get_creator("smart_card")
                table_key = creator_certificate.create_table("token", autoload=transaction)
                self.move_token_data(schema_api, transaction, table_key, "smart_card")
            except schema_api.NoSuchTableError:
                print "Warning: table smart_card not found"
            
            updater.update_finished()
            
            self.drop_token_table(schema_api, transaction, restore_path, "soft_token", "key")
            self.drop_token_table(schema_api, transaction, restore_path, "smart_card", "token")
            self.drop_token_table(schema_api, transaction, restore_path, "micro_smart", "token")
            
            

    def get_matching_module_criteria_parameters(self, schema_api, transaction, module_name):
        return_val = []
        parameters = transaction.select(self.ModuleCriteriaParameter,self.ModuleCriteriaParameter.parameter_name == "key")
        if parameters:
            module = transaction.select_one(self.Module, filter=self.Module.module_name == module_name)
            for p in parameters:
                criteria = p.module_criteria
                if criteria.module.id == module.id:
                    return_val.append(p)
        return return_val

    def move_token_data(self, schema_api, transaction, table, plugin_name):
        class Key(object):
            pass
        schema_api.mapper(Key, table)
        
        keys = transaction.select(Key)
        for key in keys:
            token = transaction.add(self.Token())
            token.plugin_type = plugin_name
            token.serial = key.serial
            token.description = key.description
            token.casing_number = key.casing_number
            token.public_key = key.public_key
            transaction.delete(key)
            
        parameters = self.get_matching_module_criteria_parameters(schema_api, transaction, plugin_name)
        if parameters:
            for p in parameters:
                p.parameter_value_id = p.parameter_value
            

    def drop_token_table(self, schema_api, transaction, restore_path, creator_name, table_name):
        creator = schema_api.SchemaFactory.get_creator(creator_name)
        #creator.create_table(table_name, autoload=transaction)
        updater = schema_api.SchemaFactory.get_updater(creator, transaction, restore_path)
        updater.drop_table(table_name)
        updater.update_schema()
        updater.update_finished()




        
class UpgradeTrafficCitrixParameters(plugin_type_upgrade.PluginTypeUpgradeVersion):
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def define_database_creator(self, schema_api):
        dbapi = schema_api.SchemaFactory.get_creator("traffic_component")

        table_launch_element = dbapi.create_table("launch_element",
                                          schema_api.Column('action_id', schema_api.Integer),
                                          schema_api.Column('launch_type', schema_api.Integer), # 0=launch_portforward, 1=launch_citrix
                                          schema_api.Column('command', schema_api.Text()),
                                          schema_api.Column('server_host', schema_api.Text()),
                                          schema_api.Column('server_port', schema_api.Integer),
                                          schema_api.Column('client_host', schema_api.Text(), nullable=False, default="0.0.0.0"),
                                          schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                          schema_api.Column('lock_to_process_pid', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('sub_processes', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('close_with_process', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('kill_on_close', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('citrix_command', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('citrix_metaframe_path', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('citrix_https', schema_api.Integer, default=0),
                                          schema_api.Column('param_file_name', schema_api.Text(), default=""),
                                          schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),
                                          schema_api.Column('param_file_template', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),
                                          )
        return dbapi


    def upgrade(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('VersionUpgrade::upgrade', __name__, lib.checkpoint.DEBUG):
            creator = self.define_database_creator(schema_api)
            updater = schema_api.SchemaFactory.get_updater(creator, transaction, restore_path)
            updater.update_schema()
            updater.update_finished()



class UpgradeDialogTags(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade dialog tags with same values as in demodata, and add the tags 'ENABLED' and 'SHOW' to all launches.
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    class DialogTagProperties(object):
        pass

    class DialogLaunchTags(object):
        pass

    class TrafficLaunchElement(object):
        pass

    def update_tag_property(self,
                            transaction, 
                            tag, 
                            user_id, 
                            menu_show=None,
                            menu_caption=None, 
                            menu_parentmenutags = None,
                            menu_sortitems = None,
                            menu_maxitems = None,
                            menu_showinparent = None,
                            item_enabled = None, 
                            item_show = None,
                            ):    
        tagrec = transaction.select(self.DialogTagProperties, 
                                    components.database.server_common.database_api.and_(self.DialogTagProperties.tag_name==tag, self.DialogTagProperties.user_id==user_id))
        if len(tagrec) == 0:
            rec = self.DialogTagProperties()
        else:
            rec = tagrec[0]
            
        #basic
        rec.tag_name = tag
        rec.user_id = user_id
        
        #sub menu related
        if menu_caption <> None:
            rec.menu_caption = menu_caption
        if menu_show <> None:
            rec.menu_show = menu_show
        if menu_parentmenutags <> None:
            rec.menu_parentmenutags = menu_parentmenutags
        if menu_sortitems <> None:
            rec.menu_sortitems = menu_sortitems
        if menu_maxitems <> None:
            rec.menu_maxitems = menu_maxitems
        if menu_showinparent <> None:
            rec.menu_showinparent = menu_showinparent
            
        #Item related    
        if item_enabled <> None: 
            rec.item_enabled = item_enabled
        if item_show <> None: 
            rec.item_show = item_show
            
        # add it if it's new    
        if len(tagrec) == 0:
            transaction.add(rec)    

    def update_tag_properties(self, schema_api, transaction):
        self.update_tag_property(transaction, 'ALL', -1, True, 'All Programs', '') #, item_show = True, item_enabled = True)
        self.update_tag_property(transaction, 'TOPX', -1, True, 'Top 3', '', menu_sortitems='launch_count', menu_maxitems=3 ) #, item_show = True, item_enabled = True)
        self.update_tag_property(transaction, 'WINDOWS', -1, True,'Windows programs')
        self.update_tag_property(transaction, 'LINUX', -1, True,'Linux programs')
        self.update_tag_property(transaction, 'BROWSER', -1, True,'Browser apps')
        self.update_tag_property(transaction, 'RDP', -1, True, 'Remote Desktops')
        self.update_tag_property(transaction, 'VNC', -1, True, 'VNC - Shared desktops')
        self.update_tag_property(transaction, 'GON', -1, True, 'G/Update')
        self.update_tag_property(transaction, 'SHOW', -1, item_show = True)
        self.update_tag_property(transaction, 'ENABLED', -1, item_enabled = True)

    def update_tag(self, transaction, launch_id, user_id, tag):
        dbrec = transaction.select(self.DialogLaunchTags, 
                                   components.database.server_common.database_api.and_(self.DialogLaunchTags.launch_id==launch_id, self.DialogLaunchTags.user_id==user_id, self.DialogLaunchTags.tag_name==tag))
        if len(dbrec) == 0:
            rec = self.DialogLaunchTags()
        else:
            rec = dbrec[0]

        #basic
        rec.launch_id = launch_id
        rec.user_id = user_id
        rec.tag_name = tag
        if len(dbrec)==0:
            transaction.add(rec)

    def update_tags(self, schema_api, transaction):
        try:
            creator_certificate = schema_api.SchemaFactory.get_creator("traffic_component")
            table_launch_element = creator_certificate.create_table("launch_element", autoload=transaction)

            schema_api.mapper(self.TrafficLaunchElement, table_launch_element)

            launch_elements = transaction.select(self.TrafficLaunchElement)
            for launch_element in launch_elements:
                self.update_tag(transaction, launch_element.id, -1, 'SHOW')
                self.update_tag(transaction, launch_element.id, -1, 'ENABLED')

        except schema_api.NoSuchTableError:
            print "Warning: table traffic_component.launc_element not found when updating dialog_launch_tags"

    def upgrade(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('VersionUpgrade::upgrade', __name__, lib.checkpoint.DEBUG):
            dbapi = schema_api.SchemaFactory.get_creator("dialog_component_ro")
    
            table_dialog_tag_properties = dbapi.create_table("dialog_tag_properties",
                                                             schema_api.Column('user_id', schema_api.Integer),
                                                             schema_api.Column('tag_name', schema_api.String(256)),
                                                             schema_api.Column('tag_priority', schema_api.Integer),
                                                             schema_api.Column('menu_show', schema_api.Integer),
                                                             schema_api.Column('menu_caption', schema_api.String(256)),
                                                             schema_api.Column('menu_parentmenutags', schema_api.String(256)),                                                                    
                                                             schema_api.Column('menu_sortitems', schema_api.String(20)),
                                                             schema_api.Column('menu_maxitems', schema_api.Integer),
                                                             schema_api.Column('menu_showinparent', schema_api.Integer),
                                                             schema_api.Column('menu_removefromparent', schema_api.Integer),
                                                             schema_api.Column('menu_overwrite_enabled', schema_api.Integer),
                                                             schema_api.Column('menu_overwrite_show', schema_api.Integer),
                                                             schema_api.Column('item_enabled', schema_api.Integer),
                                                             schema_api.Column('item_show', schema_api.Integer)
                                                             )
            
            table_dialog_launch_tags = dbapi.create_table("dialog_launch_tags",
                                                          schema_api.Column('launch_id', schema_api.Integer),
                                                          schema_api.Column('user_id', schema_api.Integer),
                                                          schema_api.Column('tag_name', schema_api.String(256))
                                                          )

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()

            schema_api.mapper(self.DialogTagProperties, table_dialog_tag_properties)
            schema_api.mapper(self.DialogLaunchTags, table_dialog_launch_tags)

            self.update_tag_properties(schema_api, transaction)
            self.update_tags(schema_api, transaction)

            updater.update_finished()
            
