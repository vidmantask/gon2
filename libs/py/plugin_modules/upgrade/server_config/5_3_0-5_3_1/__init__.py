"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint

from plugin_types.server_config import plugin_type_upgrade


            

class UpgradeTrafficAndDialog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):
            
            
            dbapi_dialog = schema_api.SchemaFactory.get_creator("dialog_component_ro")
            
            
            dbapi_dialog_table_dialog_statistics = dbapi_dialog.create_table("dialog_statistics",
                                                                                schema_api.Column('launch_id', schema_api.String(256)),
                                                                                schema_api.Column('launch_key', schema_api.String(256)),
                                                                                schema_api.Column('launch_count', schema_api.Integer),
                                                                                schema_api.Column('last_date', schema_api.DateTime)
                                                                                )
            
            dbapi_dialog_table_dialog_tag_properties = dbapi_dialog.create_table("dialog_tag_properties",
                                                                                schema_api.Column('user_id', schema_api.String(1024)),
                                                                                schema_api.Column('tag_name', schema_api.String(256)),
                                                                                schema_api.Column('tag_priority', schema_api.Integer), # sort, tag priority for sorting out contradictions 
                                                                                schema_api.Column('menu_show', schema_api.Boolean, default=False), # Boolean
                                                                                schema_api.Column('menu_caption', schema_api.String(256), default=''), # title
                                                                                schema_api.Column('menu_sortitems', schema_api.String(20)), # launch item property name for sorting. enum. "Sort order" 
                                                                                schema_api.Column('menu_maxitems', schema_api.Integer, default=0), # Max no of launch elements in menu. 0 == All
                                                                                schema_api.Column('menu_showinparent', schema_api.Integer), # Not implemented. Show in parent menu
                                                                                schema_api.Column('menu_removefromparent', schema_api.Integer), # Not implemented.
                                                                                schema_api.Column('menu_overwrite_enabled', schema_api.Boolean, default=False), # BOOL, Overwrie enabled on menu items
                                                                                schema_api.Column('menu_overwrite_show', schema_api.Boolean, default=False), # BOOL,Overwrie show on menu items
                                                                                schema_api.Column('item_enabled', schema_api.Boolean), # BOOL, ENABLED tag
                                                                                schema_api.Column('item_show', schema_api.Boolean), # Bool
                                                                                schema_api.Column('item_autolaunch_once', schema_api.Boolean), # Bool
                                                                                schema_api.Column('auto_menu_all', schema_api.Boolean, default=False), # Bool : if true, tag is automatically placed on all launch items
                                                                                )
            
            dbapi_dialog_table_parentmenutag = dbapi_dialog.create_table("parentmenutag",
                                                                            schema_api.Column('tag_property_id', schema_api.Integer, nullable=False),
                                                                            schema_api.Column('tag_name', schema_api.String(256), nullable=False),
                                                                            )
            
            
            dbapi_dialog_table_dialog_launch_tags = dbapi_dialog.create_table("dialog_launch_tags",
                                                                                schema_api.Column('launch_id', schema_api.String(256), nullable=False),
                                                                                schema_api.Column('user_id', schema_api.String(1024)),
                                                                                schema_api.Column('tag_name', schema_api.String(256))
                                                                                )
            
            dbapi_dialog_table_dialog_launch_tag_generators = dbapi_dialog.create_table("dialog_launch_tag_generators",
                                                                                           schema_api.Column('launch_id', schema_api.Integer),
                                                                                           schema_api.Column('tag_generator', schema_api.String(1000))
                                                                                           )
            
            dbapi_dialog.add_foreign_key_constraint("parentmenutag", "tag_property_id", "dialog_tag_properties")

            
            updater_dialog = schema_api.SchemaFactory.get_updater(dbapi_dialog, transaction, restore_path)
            updater_dialog.drop_table("dialog_element")
            updater_dialog.update_schema()
            
            
            dbapi_traffic = schema_api.SchemaFactory.get_creator("traffic_component")
            
            
            
            table_launch_element = dbapi_traffic.create_table("launch_element",
                                                      schema_api.Column('action_id', schema_api.Integer), # reference to auth engine 
                                                      schema_api.Column('launch_type', schema_api.Integer), # 0=launch_portforward, 1=launch_citrix, 3=cpm, 4=wol
                                                      # Card pane:
                                                      schema_api.Column('command', schema_api.Text),
                                                      schema_api.Column('working_directory', schema_api.Text),
                                                      schema_api.Column('param_file_name', schema_api.Text, default=""),
                                                      schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),
                                                      #schema_api.Column('param_file_template', schema_api.Binary(), nullable=False, default=""),
                                                      schema_api.Column('param_file_template', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('close_with_process', schema_api.Boolean, nullable=False, default=False), 
                                                      schema_api.Column('kill_on_close', schema_api.Boolean, nullable=False, default=False), 
                                                      # For launch_type launch_citrix
                                                      schema_api.Column('citrix_command', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('citrix_metaframe_path', schema_api.Text, nullable=False, default=""),
                                                      schema_api.Column('citrix_https', schema_api.Boolean, default=False),
                                                      schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                                      schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                                      schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),
                                                      
                                                      schema_api.Column('menu_title', schema_api.String(256)),
                                                      )
            
            
            table_portforward =    dbapi_traffic.create_table("portforward",
                                                      schema_api.Column('launch_element_id', schema_api.Integer(), nullable=False),
                                                      schema_api.Column('line_no', schema_api.Integer(), nullable=False),
                                                      schema_api.Column('server_host', schema_api.Text()),
                                                      schema_api.Column('server_port', schema_api.Integer),
                                                      schema_api.Column('client_host', schema_api.Text(), nullable=False, default="0.0.0.0"),
                                                      schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                                      schema_api.Column('lock_to_process_pid', schema_api.Boolean, nullable=False, default=False),
                                                      schema_api.Column('sub_processes', schema_api.Boolean, nullable=False, default=False), 
                                                      schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                                      )
            
            table_launch_tag_generators = dbapi_traffic.create_table("launch_tag_generator",
                                                             schema_api.Column('launch_id', schema_api.Integer, nullable=False),
                                                             schema_api.Column('tag_generator', schema_api.String(1000))
                                                            )
            
            
            
            dbapi_traffic.add_foreign_key_constraint(table_portforward, "launch_element_id", table_launch_element)
            dbapi_traffic.add_unique_constraint(table_portforward, "line_no", "launch_element_id")
            
            dbapi_traffic.add_foreign_key_constraint(table_launch_tag_generators, "launch_id", table_launch_element)
            
            
            updater_traffic = schema_api.SchemaFactory.get_updater(dbapi_traffic, transaction, restore_path)
            updater_traffic.update_schema()

            dbapi_auth = schema_api.SchemaFactory.get_creator("auth_server")
            table_criteria = dbapi_auth.create_table("criteria", autoload=transaction)
            
            class Criteria(object):
                pass

            class LaunchElement(object):
                pass
            
            class TrafficLaunchTagGenerator(object):
                pass
                        
            class DialogLaunchTagGenerator(object):
                pass
            
            schema_api.mapper(Criteria, table_criteria)
            schema_api.mapper(LaunchElement, table_launch_element)

            schema_api.mapper(TrafficLaunchTagGenerator, table_launch_tag_generators)
            schema_api.mapper(DialogLaunchTagGenerator, dbapi_dialog_table_dialog_launch_tag_generators)
            
            # Set menu title and name for all launch specs
            launch_elements = transaction.select(LaunchElement)
            for launch_element in launch_elements:
                criteria = transaction.get(Criteria, launch_element.action_id)
                title = criteria.title
                launch_element.name = title
                launch_element.menu_title = title
                
            # Move tag_generators to traffic table
            tag_generators = transaction.select(DialogLaunchTagGenerator)
            for tag_generator in tag_generators:
                new_tag_generator = TrafficLaunchTagGenerator()
                new_tag_generator.launch_id = new_tag_generator.launch_id 
                new_tag_generator.tag_generator = new_tag_generator.tag_generator
                transaction.delete(tag_generator) 

            updater_traffic.update_finished()
            updater_dialog.update_finished()



if __name__ == '__main__':
    
    print "yo"
