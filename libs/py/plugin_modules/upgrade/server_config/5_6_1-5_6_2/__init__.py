"""
Version upgrade plugin
"""
import os
import os.path

import lib.checkpoint

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade
from components.database.server_common import schema_api

            


class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade templates and launch elements according to new packages.
    """
    
    new_content = """
user_pref("datareporting.policy.dataSubmissionPolicyAccepted", true); 
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 1); 
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1365509001307"); 
user_pref("datareporting.policy.dataSubmissionPolicyResponseTime", "1365517336671"); 
user_pref("datareporting.policy.dataSubmissionPolicyResponseType", "accepted-info-bar-dismissed");
"""
    
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):

        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")
            
            table_launch_element = dbapi.create_table("launch_element", autoload=transaction)
            table_port_forward = dbapi.create_table("portforward", autoload=transaction)
            table_launch_tag_generators = dbapi.create_table("launch_tag_generator", autoload=transaction)
            
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            # Update param_file_lifetime for launch type 1 actions
            
            dbapi_template = schema_api.SchemaFactory.get_creator("templates")
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path)
            
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)

            updater_template.update_schema()

            class TemplateUse(object):
                pass
            
            class TemplateValue(object):
                pass

            class LaunchElement(object):
                pass
            
            class Portforward(object):
                pass

            
            class LaunchTagGenerators(object):
                pass
            

            schema_api.mapper(TemplateValue, table_template_value)
            schema_api.mapper(TemplateUse, table_template_use)
            schema_api.mapper(LaunchElement, table_launch_element)
            schema_api.mapper(Portforward, table_port_forward)
            schema_api.mapper(LaunchTagGenerators, table_launch_tag_generators)

            def get_template_value(field_name, template_element):
                return transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_element.id,
                                                                                      TemplateValue.field_name==field_name))
            
            def get_portforward(launch_element, line_no):
                return transaction.select_first(Portforward, filter=schema_api.and_(Portforward.launch_element_id == launch_element.id,
                                                                                    Portforward.line_no == line_no))
            def remove_command_option(command, option):
                index = command.find(option)
                if index<0:
                    return command
                end_index = index + len(option)
                while end_index < len(command) and command[end_index] != ' ':
                    end_index += 1
                return command[0:index] + command[end_index+1:]
                
            #########################################################################################################
            
            tempate_elements = transaction.select(TemplateUse, schema_api.or_(TemplateUse.template_name=='mac_firefox_http_proxy',
                                                                              TemplateUse.template_name=='g-on-os_browse_http_proxy ',
                                                                              TemplateUse.template_name=='win_firefox_http_proxy'))
            
            self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG, templates="mac_firefox_http_proxy + win_firefox_http_proxy + g-on-os_browse_http_proxy ", count=len(tempate_elements))
            
                
            for template_element in tempate_elements:
                
                template_value = get_template_value("param_file_template", template_element)
                if template_value:
                    template_value.value += self.new_content 
            
                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.param_file_template += self.new_content

                self.checkpoint_handler.Checkpoint('UpgradeTraffic::upgrade_launch_element', __name__, lib.checkpoint.DEBUG, name=launch_element.name)
                    
                

            updater.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
            
            
            

if __name__ == '__main__':
    
    print "yo"
