"""
Version upgrade plugin
"""
from __future__ import with_statement

import lib.checkpoint

from plugin_types.server_config import plugin_type_upgrade

class UpgradeTrafficCitrixParameters(plugin_type_upgrade.PluginTypeUpgradeVersion):
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def define_database_creator(self, schema_api):
        dbapi = schema_api.SchemaFactory.get_creator("traffic_component")

        table_launch_element = dbapi.create_table("launch_element",
                                          schema_api.Column('action_id', schema_api.Integer),
                                          schema_api.Column('launch_type', schema_api.Integer), # 0=launch_portforward, 1=launch_citrix
                                          schema_api.Column('command', schema_api.Text()),
                                          schema_api.Column('server_host', schema_api.Text()),
                                          schema_api.Column('server_port', schema_api.Integer),
                                          schema_api.Column('client_host', schema_api.Text(), nullable=False, default="0.0.0.0"),
                                          schema_api.Column('client_port', schema_api.Integer, nullable=False, default=0),
                                          schema_api.Column('lock_to_process_pid', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('sub_processes', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('lock_to_process_name', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('close_with_process', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('kill_on_close', schema_api.Integer, nullable=False, default=False), # FIXME: Bool
                                          schema_api.Column('citrix_command', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('citrix_metaframe_path', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('citrix_https', schema_api.Integer, default=0),
                                          schema_api.Column('param_file_template', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),
                                          )
        return dbapi


    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('VersionUpgrade::upgrade', __name__, lib.checkpoint.DEBUG):
            creator = self.define_database_creator(schema_api)
            updater = schema_api.SchemaFactory.get_updater(creator, transaction, restore_path)
            updater.rename_column("launch_element", "wfica_exe", "citrix_command")
            updater.update_schema()
            updater.update_finished()
        

