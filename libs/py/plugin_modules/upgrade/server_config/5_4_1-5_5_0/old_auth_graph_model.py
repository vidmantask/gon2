"""The database model and the interface and object-persistense to the database"""

import old_database_schema as schema


class Criteria(object):

    """
    Base class for all autograph nodes
    """
    
    testing = False
    
    def getDescendants(self):
        """Return transitive closure of criteria using this criteria"""

        result = set()
        for x in self.aggregating_use:
            result.add(x)
            result.update(x.getDescendants())
        for x in self.enabler_use:
            result.add(x)
            result.update(x.getDescendants())
        return result
    

    def add_parent_criteria(self, criteria, rule_entity_type=None):
        a = CriteriaAssociation()
        a.parent_criteria = criteria
        a.rule_entity_type = rule_entity_type
        a.child_criteria = self

    def get_parent_criterias(self):
        return [a.parent_criteria for a in self.parent_criterias]

    def get_parent_criteria(self):
        if self.parent_criterias:
            return self.parent_criterias[0].parent_criteria
        else:
            return None

    def criteria_description(self):
        """Virtual in base class: Return description of criteria"""
        raise NotImplementedError

    def delete(self, transaction):
        raise NotImplementedError

    def calculate(self, module_check_predicate, results):
        """Return current value of evaluation of self, 
        caching values in results so that it can be used for real and test and without storing state in model,
        parameterized with module_check_predicate so that it can be used for test without real modules"""

#        print "Calculate ", self.id

        try:
            return results[self.id]
        except:
            if self.deactivated:
                result = False
            else:
                results[self.id] = None  # will be returned in case of cyclic dependencies - will normally be set correctly after recursion
                result = self.calculate_recursive_helper(results, module_check_predicate)

            # print 'Calculated criteria', repr(self.criteria_description()), 'is', result

            results[self.id] = result
            return result

    def calculate_recursive_helper(self, results, module_check_predicate):
        """Virtual in base class: Calculate state of current criteria, possibly recursing through .calculate() on children"""
        raise NotImplementedError
    
    def add_active_plugins(self, plugin_set, visit_list):
        raise NotImplementedError
    
    def get_partial_results(self, results, partial_results):
        partial_results[self.id] = None
    
    def get_report_info(self):
        return None
    
    def set_true_bottom_up(self, results):
        if not self.deactivated:
            for parent in self.get_parent_criterias():
                parent.set_true_bottom_up(results)
        
class CriteriaAssociation(object):
    pass


class AggregatingCriteria(Criteria):

    """
        A criteria which designates a boolean operation aggregating other criteria: All, One or None
    """

    (AllOf,  # and, universal quantifier
     OneOf,  # or, existential quantifier
     NoneOf,  # and not, non-existential quantifier / universal non-quantifier
     ) = range(3)

    ''' Internal type values '''
    (Simple, # Standard - no semantics 
     RuleClass,  # Criteria is part of a class of criteria defined by internal_type_name - must be of kind OneOf
     WindowRules, # Criteria is head of basic rules defined in MAP Window - must be of kind OneOf. A WindowRules criteria must be part of a RuleClass criteria
     Action, # Criteria specifying acces for an action
     NonStandard, # Specific rule type for e.g. endpoint
     ) = range(5)

        
     
    def __init__(self, **kwargs):
        self.internal_type = self.Simple
        self.aggregation_kind = self.AllOf
        super(AggregatingCriteria,self).__init__(**kwargs)

#    @reconstructor
#    def __db__init__(self):
#        self.calc_result = None


    def add_criteria(self, criteria, rule_entity_type=None):
        a = CriteriaAssociation()
        a.child_criteria = criteria
        a.rule_entity_type = rule_entity_type
        a.parent_criteria = self
        
    def get_criterias(self):
        return [a.child_criteria for a in self.child_criterias]

    def get_entity_type_and_criterias(self):
        return [(a.rule_entity_type, a.child_criteria) for a in self.child_criterias]

    def delete(self, transaction):
        for a in self.child_criterias:
            transaction.delete(a)
            
        transaction.delete(self)

    def criteria_description(self):
        f =  {self.AllOf: "All Of", 
             self.OneOf: "One Of", 
             self.NoneOf: "None Of",
             }.get(self.aggregation_kind, None)
        prefix = ""
        if self.internal_type == self.RuleClass:
            prefix = "RuleClass"
        elif self.internal_type == self.Action:
            prefix = "Action"
        elif self.internal_type == self.Simple:
            prefix = "Simple"
        elif self.internal_type == self.WindowRules:
            prefix = "WindowRules"
        elif self.internal_type == self.NonStandard:
            prefix = "NonStandard"
        return "%s %s" % (prefix, f)

    def __str__(self):
        if self.title:
            return self.title
        if self.internal_type==self.Simple or self.internal_type==self.WindowRules:
            return self.get_parent_criteria().__str__()
        return "Unknown"

    def calculate_recursive_helper(self, results, module_check_predicate):
        """Calculate state of current criteria, recursing (with shortcut evaluation) through .calculate() on aggregated criteria"""
        if self.aggregation_kind == self.AllOf:
            for sub_criteria in self.criterias:
                if not sub_criteria.calculate(module_check_predicate, results):
                    return False
            return True
        elif self.aggregation_kind == self.OneOf:
            for sub_criteria in self.criterias:
                if sub_criteria.calculate(module_check_predicate, results):
                    return True
            return False
        elif self.aggregation_kind == self.NoneOf:
            for sub_criteria in self.criterias:
                if sub_criteria.calculate(module_check_predicate, results):
                    return False
            return True
        else:
            raise RuntimeError  # Bogus criteria type

    def has_no_conditions(self):
        if self.internal_type == self.Action:
            child_criterias = self.get_criterias()
            if not child_criterias:
                return False
            for child_criteria in child_criterias:
                grand_child_criterias = child_criteria.get_criterias()
                if not grand_child_criterias:
                    return True
        return False
            

    def calculate_recursive_helper(self, results, module_check_predicate):
        """Calculate state of current criteria, recursing (without shortcut evaluation) through .calculate() on aggregated criteria"""
        
#        if not self.calc_result is None:
#            return self.calc_result;
        if self.deactivated:
            return False
        
        sub_criteria_results = []
        for sub_criteria in self.get_criterias():
            result = sub_criteria.calculate(module_check_predicate, results)
            sub_criteria_results.append(result)
        
        if self.aggregation_kind == self.AllOf:
            and_result = True
            for result in sub_criteria_results:
                if result is None:
                    return None
                if result is False:
                    and_result = False
#            self.calc_result = and_result
            return and_result
        elif self.aggregation_kind == self.OneOf:
            or_result = False
            for result in sub_criteria_results:
                if result:
#                    self.calc_result = True
                    return True
                elif result is None:
                    or_result = None
#            if or_result is False:
#                self.calc_result = False
            return or_result
        elif self.aggregation_kind == self.NoneOf:
            not_result = True
            for result in sub_criteria_results:
                if result:
#                    self.calc_result = False
                    return False
                elif result is None:
                    not_result = None
#            if not_result:
#                self.calc_result = True
            return True
        else:
            raise RuntimeError  # Bogus criteria type


    def add_active_plugins(self, plugin_set, visit_list):
        if not self.id in visit_list:
            visit_list.append(self.id)
            for sub_criteria in self.get_criterias():
                sub_criteria.add_active_plugins(plugin_set, visit_list)
                
                
    def get_partial_results(self, results, partial_results):
        
        if partial_results.has_key(self.id):
            return
        partial_result = None
        if self.aggregation_kind == self.AllOf:
            found_true = []
            found_false = 0
            false_criteria = None
            for sub_criteria in self.get_criterias():
                result = results.get(sub_criteria.id)
                if result:
                    found_true.append(sub_criteria)
                else:
                    found_false += 1
                    false_criteria = sub_criteria
#                    sub_criteria.get_partial_results(results, partial_results)
            if found_false==1:
                # We are not interested in two "falses"
                if found_true:
                    partial_result = (self, false_criteria)
                false_criteria.get_partial_results(results, partial_results)
        if self.aggregation_kind == self.OneOf:
            for sub_criteria in self.get_criterias():
                sub_criteria.get_partial_results(results, partial_results)
        
        partial_results[self.id] = partial_result
    
    def get_report_info(self):
        if self.internal_type == self.RuleClass or self.internal_type == self.NonStandard:
            return self.title
        return None
                
    def check_true(self, results):
        
#        print self.id
        if self.deactivated:
            results[self.id] = False
            return False

        self_value = None
        if self.aggregation_kind == self.AllOf:
            
            and_result = True
            for sub_criteria in self.get_criterias():
#                print sub_criteria.id
                result = results.get(sub_criteria.id)
                if result is None:
                    return None
                if result is False:
                    and_result = False
                    break

            self_value = and_result
        elif self.aggregation_kind == self.OneOf:
            self_value = True
        elif self.aggregation_kind == self.NoneOf:
            self_value = False
        else:
            raise RuntimeError("Unknown criteria type")  # Bogus criteria type

        if not self_value is None:
            results[self.id] = self_value

        return self_value
                    

    def set_true_bottom_up(self, results):
        
#        print self.id
        if self.deactivated:
            return

#        self_value = results.get(self.id)
#        if self_value:
#            return
        
        self_value = None
        if self.aggregation_kind == self.AllOf:
            
            and_result = True
            for sub_criteria in self.get_criterias():
#                print sub_criteria.id
                result = results.get(sub_criteria.id)
                if result is None:
                    return
                if result is False:
                    and_result = False
                    break

            self_value = and_result
        elif self.aggregation_kind == self.OneOf:
            self_value = True
        elif self.aggregation_kind == self.NoneOf:
            self_value = False
        else:
            raise RuntimeError("Unknown criteria type")  # Bogus criteria type

        if not self_value is None:
            results[self.id] = self_value
            if self_value:
                for parent in self.get_parent_criterias():
                    parent.set_true_bottom_up(results)
                
            

class ModuleCriteria(Criteria):

    """
      A module function criteria
    """

    def delete(self, transaction):
        for p in self.parameters:
            p.delete(transaction)
        for a in self.parent_criterias:
            transaction.delete(a)
        transaction.delete(self)

    def parameter_dict(self):
        """Returns .parameters as a dict"""
        return dict((str(p.parameter_name), (p.parameter_value_id, p.parameter_value)) for p in self.parameters)

    def criteria_description(self):
        return self.module.module_name + "." + self.predicate_name + \
            "(" + ",".join(p.parameter_name + "=" + p.parameter_value for p in self.parameters) + ")"

    def calculate_recursive_helper(self, results, module_check_predicate):
        """Calculate state of current criteria, possibly recursing to enabler criteria, then invoking modules predicate with parameters"""
        if not self.testing and self.module.enabler_criteria and not self.module.enabler_criteria.calculate(module_check_predicate, results):
            return None
        return module_check_predicate(self.module.module_name, self.predicate_name, self.parameter_dict())  # will automatically start module on demand and check it's ready ready

    def add_active_plugins(self, plugin_set, visit_list):
        if not self.id in visit_list:
            visit_list.append(self.id)
            plugin_set.add(self.module.module_name)


    def get_report_info(self):
        parameter = self.parameters[0]
        return parameter.parameter_value


class Action_(object):
    pass

class Module(object):
    pass

class ModuleCriteriaParameter(object):

    def delete(self, transaction):
        transaction.delete(self)

def create_mappers(schema_api):
    schema.create_criteria_mapper(Criteria, schema_api)
    schema.create_action_mapper(Action_, schema_api, schema_api.Relation(Criteria, 'criteria', backref_property_name = 'actions'))
    schema.create_aggregating_criteria_mapper(AggregatingCriteria, schema_api,
                                              CriteriaAssociation,
                                              Criteria
                                       )
    schema.create_module_criteria_parameter_mapper(ModuleCriteriaParameter, schema_api)
    schema.create_module_mapper(Module, schema_api, schema_api.Relation(Criteria, 'enabler_criteria', backref_property_name = 'enabler_use'))
    schema.create_module_criteria_mapper(ModuleCriteria, schema_api,
                                         Criteria, 
                                  schema_api.Relation(ModuleCriteriaParameter, 'parameters', backref_property_name = 'module_criteria'),
                                  schema_api.Relation(Module, 'module'),
                                  )

    
