import os.path
import lib.variable_expansion
import ConfigParser
import ldap
import lib.utility.xor_crypt as xor_crypt


CONFIG_FILENAME = 'config.ini'

class Config(object):
    SECTION_DIRECTORY_PREFIX = 'directory'

    LDAP_SECTION = 'ldap'
    DIRECTORY_NAME = 'directory name'
    LOGIN_SUFFIX = 'login_suffix'
    
    SECTION_PLUGIN = 'plugin'
    KEY_ENABLED_NAME = 'enabled'
    
    encryption_fields = ['password']
    
    default_values = {
                       DIRECTORY_NAME : "<name for directory, e.g. the domain. This name is used to identify the directory, so it should never be changed>",
                       "dn" : "<the base dn, e.g. dc=example,dc=com>",
                       "password_expiry_warning" : 14,
                       "hosts" : "<list of hosts in the form server[:port], default port is 389 - e.g. myserver:636>",
                      }
    
    
    
    def __init__(self, path, raise_error=True, filename=CONFIG_FILENAME):
        self.authorization_domains = []
        self.path = path
        
        self._config_ini = ConfigParser.ConfigParser()
        self.enabled = False
        self.load(raise_error, filename=filename)
        
    def load(self, raise_error=True, filename=CONFIG_FILENAME):
        filename = os.path.join(self.path, filename)
        if os.path.exists(filename):
            self._config_ini.read(filename)
            self.enabled = self._config_ini.getboolean(self.SECTION_PLUGIN, self.KEY_ENABLED_NAME)  
            self.check_and_update_domain_sections(raise_error)
            
        
    _boolean_states = {'1': True, 'yes': True, 'true': True, 'on': True,
                       '0': False, 'no': False, 'false': False, 'off': False}

    @classmethod
    def get_boolean(cls, value):
        if isinstance(value, bool):
            return value
        if value.lower() not in cls._boolean_states:
            raise ValueError, 'Not a boolean: %s' % value
        return cls._boolean_states[value.lower()]
    
    @classmethod
    def get_list(cls, value):
        if value is None:
            return []
        return [name.strip() for name in value.split(",")]
            
    @classmethod
    def dir_name_error(cls, dir_name):
        if not dir_name.replace("_","").replace(".", "").isalnum(): 
            return "LDAP Configuration error. Directory name value '%s' contains illegal characters. Only alphanumeric characters, '_' and '.' are allowed" % (dir_name,)
        return None

    def check_and_update_domain_sections(self, raise_error=True):
        if not self.authorization_domains:
            try:
                self.authorization_domains = []
                domain_sections = []
                sections = self._config_ini.sections()
                sections.sort()
                for section in sections:
                    if section.lower().startswith(Config.SECTION_DIRECTORY_PREFIX):
                        domain_sections.append(section)
                
                for section in domain_sections:
                    try:
                        dir_name = self._config_ini.get(section, Config.DIRECTORY_NAME)
                        if raise_error and self.dir_name_error(dir_name):
                            raise Exception("LDAP Configuration error. Option '%s' in section '%s' contains illegal characters. Only alphanumeric characters and '_' are allowed" % (dir_name, section))
                    except ConfigParser.NoOptionError:
                        if raise_error:
                            raise Exception("LDAP Configuration error. Missing option '%s' in section '%s'" % (Config.DIRECTORY_NAME, section))
                    if dir_name:
                        options = self._config_ini.options(section)
                        option_dict = dict()
                        for option in options:
                            option_dict[option] = self._config_ini.get(section,option)
                            
                        login_suffix = option_dict.get(Config.LOGIN_SUFFIX)
                        if not login_suffix:
                            option_dict[Config.LOGIN_SUFFIX] = dir_name.lower()
                        else:
                            login_suffix = login_suffix.lower()
                        self.authorization_domains.append(option_dict)
            except Exception, e:
                if self.enabled:
                    raise e

    @classmethod
    def write(cls, path, enabled, authorization_domains, filename=CONFIG_FILENAME):
        config_ini = ConfigParser.ConfigParser()
        for authorization_domain in authorization_domains:
            dir_name = authorization_domain.get(Config.DIRECTORY_NAME)
            if dir_name:
                section_name = '%s %s' %(Config.SECTION_DIRECTORY_PREFIX, dir_name)
                config_ini.add_section(section_name)
                for (key,value) in authorization_domain.items():
                    if (key in cls.encryption_fields):
                        value = xor_crypt.xor_crypt(value)
                    config_ini.set(section_name, key, value)
        
        if not os.path.exists(path):
            os.makedirs(path)
        
        config_filenameabs = os.path.join(path, filename)

        found_plugin_section = False
        if os.path.exists(config_filenameabs):
            config_ini_existing = ConfigParser.ConfigParser()
            config_ini_existing.read(config_filenameabs)
        
            for section in config_ini_existing.sections():
                if not section.startswith(Config.SECTION_DIRECTORY_PREFIX):
                    config_ini.add_section(section)
                    for option in config_ini_existing.options(section):
                        config_ini.set(section, option, config_ini_existing.get(section, option))
                    if section==Config.SECTION_PLUGIN:
                        found_plugin_section = True
                        config_ini.set(section, Config.KEY_ENABLED_NAME, enabled)
                elif config_ini.has_section(section):
                    for option in config_ini_existing.options(section):
                        if not config_ini.has_option(section, option):
                            config_ini.set(section, option, config_ini_existing.get(section, option))
                    
        
        if not found_plugin_section:
            config_ini.add_section(cls.SECTION_PLUGIN)
            config_ini.set(cls.SECTION_PLUGIN, Config.KEY_ENABLED_NAME, enabled)
            
        
        config_file = open(config_filenameabs, 'w')
        config_ini.write(config_file)
        config_file.close()


if __name__ == '__main__':
    import tempfile
    import shutil
    
    temp_folder = tempfile.mkdtemp()
    
    test_init_file = open(os.path.join(temp_folder, CONFIG_FILENAME), 'w')
    test_init_file.write('[access_notification]\n') 
    test_init_file.write('enabled = True\n') 
    test_init_file.write('xxx = 10\n')
    test_init_file.write('[domain 1]\n') 
    test_init_file.write('dns name = domain_xxxxx\n') 
    test_init_file.close()
    

    Config.write(temp_folder, ['domain_a', 'domain_b'])
    
    test_init_file = open(os.path.join(temp_folder, CONFIG_FILENAME), 'r')
    print test_init_file.read()
    
    test_init_file.close()
    shutil.rmtree(temp_folder)
