
class ServerSchema:

    @classmethod
    def create_schema(cls, schema_api, transaction):
        dbapi = schema_api.SchemaFactory.get_creator("auth_server")
        cls.table_action = dbapi.create_table("action", autoload=transaction)
        cls.table_criteria = dbapi.create_table("criteria", autoload=transaction)
        cls.table_aggregating_criteria = dbapi.create_table("aggregating_criteria",  autoload=transaction)
        cls.table_criteria_association_table = dbapi.create_table("aggregating_criteria_criteria", autoload=transaction)
        cls.table_module_criteria = dbapi.create_table("module_criteria", autoload=transaction)
        cls.table_module_criteria_parameter = dbapi.create_table("module_criteria_parameter", autoload=transaction)
        cls.table_module = dbapi.create_table("module", autoload=transaction)
        cls.table_default_rule_criteria = dbapi.create_table("default_rule_criteria", autoload=transaction)
        
        dbapi.add_foreign_key_constraint(cls.table_action, "criteria_id", cls.table_criteria)
        dbapi.add_foreign_key_constraint(cls.table_criteria_association_table, "aggregating_criteria_id", cls.table_aggregating_criteria)
        dbapi.add_foreign_key_constraint(cls.table_criteria_association_table, "criteria_id", cls.table_criteria)
        dbapi.add_foreign_key_constraint(cls.table_module_criteria, "module_id", cls.table_module)
        dbapi.add_foreign_key_constraint(cls.table_aggregating_criteria, "id", cls.table_criteria)
        dbapi.add_foreign_key_constraint(cls.table_module_criteria, "id", cls.table_criteria)
        dbapi.add_foreign_key_constraint(cls.table_module_criteria_parameter, "module_criteria_id", cls.table_module_criteria)
        dbapi.add_foreign_key_constraint(cls.table_module, "enabler_criteria_id", cls.table_criteria)
        dbapi.add_foreign_key_constraint(cls.table_default_rule_criteria, "criteria_id", cls.table_aggregating_criteria)
        
    
    table_action = None
    table_criteria = None
    table_aggregating_criteria = None
    table_criteria_association_table = None
    table_module_criteria = None
    table_module_criteria_parameter = None
    table_module = None
    table_default_rule_criteria = None



def create_action_mapper(cls, schema_api, criteria_relation=None):
    relations = []
    if criteria_relation:
        relations = [criteria_relation]
    schema_api.mapper(cls, ServerSchema.table_action, relations = relations)


def create_criteria_mapper(cls, schema_api):
    schema_api.mapper(cls, ServerSchema.table_criteria, 
           polymorphic_on = ServerSchema.table_criteria.c.type,
           polymorphic_identity = 'Criteria')


def create_aggregating_criteria_mapper(cls, schema_api, association_class, inherits_from, criteria_relation=None):
    relations = [schema_api.Relation(association_class, 'child_criterias', backref_property_name = 'parent_criteria',
                          primaryjoin=ServerSchema.table_criteria_association_table.c.aggregating_criteria_id==ServerSchema.table_aggregating_criteria.c.id)]
    schema_api.mapper(association_class, ServerSchema.table_criteria_association_table, 
           relations=[schema_api.Relation(inherits_from, 'child_criteria', backref_property_name = 'parent_criterias',
                          primaryjoin=ServerSchema.table_criteria_association_table.c.criteria_id==ServerSchema.table_criteria.c.id)])
    schema_api.mapper(cls, 
           ServerSchema.table_aggregating_criteria, 
           inherits=inherits_from,
           polymorphic_identity = 'AggregatingCriteria', 
           relations = relations
          )



def create_module_criteria_mapper(cls, schema_api, inherits_from, parameter_relation = None, module_relation = None):
    relations = []
    if parameter_relation:
        relations.append(parameter_relation)
    if module_relation:
        relations.append(module_relation)
    schema_api.mapper(cls, 
           ServerSchema.table_module_criteria, 
           inherits=inherits_from,
           polymorphic_identity = 'ModuleCriteria', 
           relations = relations
          )


def create_module_criteria_parameter_mapper(cls, schema_api):
    schema_api.mapper(cls, ServerSchema.table_module_criteria_parameter)


def create_module_mapper(cls, schema_api, criteria_relation = None):
    relations = []
    if criteria_relation:
        relations = [criteria_relation]
    schema_api.mapper(cls, ServerSchema.table_module, relations = relations)


def create_criteria_association_mapper(cls, schema_api):
    schema_api.mapper(cls, ServerSchema.table_criteria_association_table)


def create_default_rule_criteria_mapper(cls, schema_api, criteria_relation = None):
    relations = []
    if criteria_relation:
        relations = [criteria_relation]
    schema_api.mapper(cls, ServerSchema.table_default_rule_criteria, relations = relations)


