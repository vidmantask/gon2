"""
Version upgrade plugin
"""
import os
import os.path
import datetime

import lib.checkpoint
import lib.cryptfacility
import lib.config
import lib.appl.upgrade

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade

import old_auth_graph_model as old_model
import old_database_schema as old_schema
import auth_graph_model as new_model
import endpoint_database_schema as endpoint_schema
            
import sqlalchemy.engine.url as sqlalchemy_url

import ldap_config
import lib.utility.xor_crypt as xor_crypt
import shutil
            

def _create_criteria(schema_api, transaction, a, rule_class_map):
#    if a.internal_type != old_model.AggregatingCriteria.RuleClass:
#        print "yo"
#    assert a.internal_type == old_model.AggregatingCriteria.RuleClass
    if a.entity_type in ["PersonalTokenAssignment"]:
        c = transaction.add(new_model.Association())
        c.title = a.title
        c.entity_type = a.entity_type
    elif a.entity_type in ["TokenGroupMembership", "GOnUserGroup"]:
        c = transaction.add(new_model.Group())
        c.title = a.title
        c.entity_type = a.entity_type
    elif a.entity_type in ["Endpoint"]:
        c = transaction.add(new_model.Association())
        c.title = a.title
        c.entity_type = "Endpoint"
        token_serial = None
        token_title = None
        hardware_check_found = False
        for (rule_entity_type, child) in a.get_entity_type_and_criterias():
            if child.entity_type == "EndpointMAC":
                parameter = child.parameters[0]
                c.add_criteria_value(schema_api,
                                     transaction, 
                                     "HardwareCheck", 
                                     parameter.parameter_value_id, 
                                     c.title, 
                                     "HardwareCheck")
                hardware_check_found = True
                endpoint = endpoint_schema.get_registered_endpoint(int(parameter.parameter_value_id), transaction)
                hardware_check = transaction.add(endpoint_schema.HardwareCheckPlugin())
                hardware_check.endpoint = endpoint
                hardware_check.plugin_name = "endpoint_mac"
                
            else:
                parameter = child.parameters[0]
                c.add_criteria_value(schema_api,
                                     transaction, 
                                     child.entity_type, 
                                     parameter.parameter_value_id, 
                                     parameter.parameter_value, 
                                     rule_entity_type)
                token_serial = parameter.parameter_value_id
                token_title = parameter.parameter_value

        if not token_serial:
            # no rule
            raise Exception("Endpoint rule error")
        endpoint = endpoint_schema.get_registered_endpoint_by_token_serial(token_serial, transaction)
        endpoint.rule_id = c.id
        if not hardware_check_found:
            c.add_criteria_value(schema_api,
                                 transaction, 
                                 "HardwareCheck", 
                                 endpoint.id, 
                                 token_title, 
                                 "HardwareCheck")
         
    else:
        c = transaction.add(new_model.Rule())
        c.title = a.title
        c.entity_type = a.entity_type

    transaction.flush()
    rule_class_map[a.id] = c
    
    children = a.get_criterias()
    if a.internal_type == old_model.AggregatingCriteria.RuleClass:
        assert len(children)==1
        child = children[0]
        assert child.internal_type == old_model.AggregatingCriteria.WindowRules
        children = child.get_criterias()
        
        for child in children:
            _create_rule(schema_api, transaction, child, c, rule_class_map)
            
    
    return c
        

def _create_rule(schema_api, transaction, old_criteria, parent, rule_class_map):
    assert old_criteria.internal_type == old_model.AggregatingCriteria.Simple
    association = []

    if not isinstance(parent, new_model.Group):
        if isinstance(parent, new_model.Rule):
            new_parent = transaction.add(new_model.RuleClause())
        elif isinstance(parent, new_model.Association):
            new_parent = transaction.add(new_model.AssociationClause())
        else:
            raise Exception("Unexpected type for rule parent '%s'" % parent.__class__)
        new_parent.title = parent.title
        new_parent.entity_type = parent.entity_type
        transaction.flush()
        parent.add_sub_criteria(new_parent, deactivated=old_criteria.deactivated)
        conditions_deactivated = False
    else:
        new_parent = parent
        conditions_deactivated = old_criteria.deactivated
        
    
    for (rule_entity_type, c) in old_criteria.get_entity_type_and_criterias():
        if isinstance(c, old_model.AggregatingCriteria):
            new_condition = rule_class_map.get(c.id)
            if not new_condition:
                new_condition = _create_criteria(schema_api, transaction, c, rule_class_map)
            new_parent.add_sub_criteria(new_condition, rule_entity_type, deactivated=conditions_deactivated)
        else:
            assert len(c.parameters)==1
            parameter = c.parameters[0]
            new_parent.add_criteria_value(schema_api,
                                          transaction,
                                          c.entity_type, 
                                          parameter.parameter_value_id, 
                                          parameter.parameter_value, 
                                          rule_entity_type=rule_entity_type,
                                          deactivated=conditions_deactivated)
    if association:
        assert len(association)==2
        transaction.add(new_parent.add_values(association))
            

class UpgradeAuth(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeAuth::upgrade', __name__, lib.checkpoint.INFO):

            self.checkpoint_handler.Checkpoint('Updating Schema', __name__, lib.checkpoint.INFO)
            
            dbapi = endpoint_schema.create_schema_and_mappers(schema_api, transaction)
            updater_endpoint = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater_endpoint.update_schema()
            
            
            old_schema.ServerSchema.create_schema(schema_api, transaction)
            old_model.create_mappers(schema_api)
            dbapi = new_model.create_schema_and_mappers(schema_api)

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater.rename_column("default_rule_criteria", "criteria_id", "node_id")
            updater.rename_column("default_rule_criteria", "element_type", "entity_type")
            updater.update_schema()
            
            rule_class_map = dict()
            action_id_map = dict()

            
            actions_and_rules = transaction.select(old_model.AggregatingCriteria)
            self.checkpoint_handler.Checkpoint('Convert Auth Start', __name__, lib.checkpoint.INFO, n=len(actions_and_rules))
            
            for a in actions_and_rules:
                if a.parent_criterias:
                    continue
                if a.internal_type != old_model.AggregatingCriteria.Action:
                    self.checkpoint_handler.Checkpoint('Convert Auth Criteria', __name__, lib.checkpoint.DEBUG, convert_criteria=a.title)
                    if not a.title or not a.entity_type:
                        self.checkpoint_handler.Checkpoint('Convert Auth Criteria', __name__, lib.checkpoint.WARNING, msg="Unable to convert criteria", criteria_id=a.id)
                        continue
                    child = a
                    action = None
                    rule_criteria = _create_criteria(schema_api, transaction, a, rule_class_map)
                else:
                    self.checkpoint_handler.Checkpoint('Convert Auth Action', __name__, lib.checkpoint.DEBUG, action_title=a.title)
                    action = transaction.add(new_model.Action())
                    action.title = a.title
                    action.entity_type = a.entity_type
                    
                    transaction.flush()
                    action_id_map[a.id] = action.id
            
                    children = a.get_criterias()
                    if not children:
                        continue
                    
                    assert len(children)==1
                    child = children[0]
                    assert child.internal_type == old_model.AggregatingCriteria.RuleClass
                    
                    rule_criteria = transaction.add(new_model.Rule())
                    rule_criteria.title = child.title
                    rule_criteria.entity_type = child.entity_type
                    action.add_sub_criteria(rule_criteria)
                    
                    
                if child.internal_type == old_model.AggregatingCriteria.RuleClass:
                    children = child.get_criterias()
                    assert len(children)==1
                    child = children[0]
                    assert child.internal_type == old_model.AggregatingCriteria.WindowRules
    
                    children = child.get_criterias()
                    for c in children:
                        _create_rule(schema_api, transaction, c, rule_criteria, rule_class_map)
                else:
                    print child.title, child.internal_type, child.entity_type
                    _create_criteria(schema_api, transaction, child, rule_class_map)
            

            self.checkpoint_handler.Checkpoint('Updating Traffic', __name__, lib.checkpoint.INFO)

            dbapi_traffic = schema_api.SchemaFactory.get_creator("traffic_component", checkpoint_handler=self.checkpoint_handler)
            updater_traffic = schema_api.SchemaFactory.get_updater(dbapi_traffic, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            table_launch_element = dbapi_traffic.create_table("launch_element",
                                          schema_api.Column('action_id', schema_api.Integer), # reference to auth engine
                                          # launch_types
                                          # 0=launch_portforward
                                          # 1=launch_citrix
                                          # 2=cpm
                                          # 3=wol
                                          # 4=citrix xml
                                          # 5=async
                                          # 6=rdp
                                          # 7=proxy(http/socks)
                                          # 8=i?
                                          schema_api.Column('launch_type', schema_api.Integer),
                                          # Card pane:
                                          schema_api.Column('command', schema_api.Text, default=""),
                                          schema_api.Column('close_command', schema_api.Text, default=""),
                                          schema_api.Column('working_directory', schema_api.Text, default=""),
                                          schema_api.Column('param_file_name', schema_api.Text, default=""),
                                          schema_api.Column('param_file_lifetime', schema_api.Integer, default=5),
                                          #schema_api.Column('param_file_template', schema_api.Binary(), nullable=False, default=""),
                                          schema_api.Column('param_file_template', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('close_with_process', schema_api.Boolean, nullable=False, default=False),
                                          schema_api.Column('kill_on_close', schema_api.Boolean, nullable=False, default=False),
                                          # For launch_type launch_citrix
                                          schema_api.Column('citrix_command', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_metaframe_path', schema_api.Text, nullable=False, default=""),
                                          schema_api.Column('citrix_https', schema_api.Boolean, default=False),
                                          schema_api.Column('sso_login', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_password', schema_api.Text(), nullable=False, default=""),
                                          schema_api.Column('sso_domain', schema_api.Text(), nullable=False, default=""),

                                          schema_api.Column('name', schema_api.String(40)),
                                          schema_api.Column('menu_title', schema_api.String(256)),
                                          schema_api.Column('image_id', schema_api.String(256)),
                                          )

            updater_traffic.update_schema()

            class LaunchElement(object):
                pass

            schema_api.mapper(LaunchElement, table_launch_element)

            launch_elements = transaction.select(LaunchElement)
            for launch_element in launch_elements:
                new_id = action_id_map[launch_element.action_id]
                self.checkpoint_handler.Checkpoint('Update launch spec', __name__, lib.checkpoint.DEBUG, launc_spec=launch_element.name, old_action_id=launch_element.action_id, new_action_id=new_id)
                launch_element.action_id = new_id
                
                
            dbapi_template = schema_api.SchemaFactory.get_creator("templates", checkpoint_handler=self.checkpoint_handler)
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)
#            updater_template.update_schema()
            
            class TemplateUse(object):
                pass

            class TemplateValue(object):
                pass
            
            schema_api.mapper(TemplateUse, table_template_use)
            schema_api.mapper(TemplateValue, table_template_value)
                
            elements = transaction.select(TemplateUse)
            self.checkpoint_handler.Checkpoint('Updating templates', __name__, lib.checkpoint.INFO, n=len(elements))
            
            for template_use in elements:
                new_id = action_id_map.get(int(template_use.element_id))
                self.checkpoint_handler.Checkpoint('Update template', __name__, lib.checkpoint.DEBUG, template=template_use.template_name, old_id=template_use.element_id, new_id=new_id)
                if new_id:
                    template_use.element_id = "%s" % new_id
                else:
                    self.checkpoint_handler.Checkpoint('Update template', __name__, lib.checkpoint.WARNING, msg="Template not used", template=template_use.template_name, old_id=template_use.element_id)
                    values = transaction.select(TemplateValue, TemplateValue.id==template_use.id)
                    transaction.delete_sequence(values)
                    transaction.delete(template_use)
                    
#            elements = transaction.select(TemplateUse)
#            for template_use in elements:
#                template_use.element_id = template_use.element_id[1:]

            with self.checkpoint_handler.CheckpointScope('update dialog_tag_generators template values', __name__, lib.checkpoint.DEBUG) as cps:
                template_names = ["linux_browser",
                                  "linux_citrix",
                                  "linux_citrix_web",
                                  "linux_citrix_xml_service",
                                  "linux_citrix_xml_service_auto_settings",
                                  "linux_fz",
                                  "linux_owa",
                                  "linux_rdesktop",
                                  "linux_rdesktop_my_pc",
                                  "linux_rdesktop_rdp_con",
                                  ]
                template_use_exp = transaction.select_expression(TemplateUse.id, schema_api.in_(TemplateUse.template_name, template_names))
                template_values = transaction.select(TemplateValue, schema_api.and_(schema_api.in_(TemplateValue.template_use_id, template_use_exp),
                                                                                   TemplateValue.field_name=="dialog_tag_generators"
                                                                                   ))
                cps.add_complete_attr(no_of_values=len(template_values))
                for template_value in template_values:
                    template_value.value = "%(custom_template.tag_generator)"
                

            
            self.checkpoint_handler.Checkpoint('Auth Conversion finished', __name__, lib.checkpoint.INFO)
            
            updater.update_finished(self.to_version)
            updater_endpoint.update_finished(self.to_version)
            updater_traffic.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
                


class UpgradeDialog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
            
        with self.checkpoint_handler.CheckpointScope('UpgradeDialog::upgrade', __name__, lib.checkpoint.DEBUG):
            
            dbapi_dialog_ro = schema_api.SchemaFactory.get_creator("dialog_component_ro", checkpoint_handler=self.checkpoint_handler)
#            dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_element", autoload=transaction)
            updater = schema_api.SchemaFactory.get_updater(dbapi_dialog_ro, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater.drop_table("dialog_element")
            updater.update_schema()

            updater.update_finished(self.to_version)


class UpgradeAccessLog(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        with self.checkpoint_handler.CheckpointScope('UpgradeAccessLog::upgrade', __name__, lib.checkpoint.DEBUG):
            
            dbapi = schema_api.SchemaFactory.get_creator("access_log_component", checkpoint_handler=self.checkpoint_handler)
            
            dbapi_session = dbapi.create_table("session",
                                               schema_api.Column('unique_session_id', schema_api.String(100)),
                                               schema_api.Column('server_id', schema_api.Integer),
                                               schema_api.Column('server_sid', schema_api.String(100)),
                                               schema_api.Column('start_ts', schema_api.DateTime),
                                               schema_api.Column('close_ts', schema_api.DateTime),
                                               schema_api.Column('client_ip', schema_api.String(100)),
                                               schema_api.Column('login', schema_api.String(100)),
                                               schema_api.Column('external_user_id', schema_api.String(1024)),
                                               schema_api.Column('user_plugin', schema_api.String(256)),
                                               )
                    
            
#            dbapi_dialog_table_dialog_statistics = dbapi_dialog_ro.create_table("dialog_element", autoload=transaction)
            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            updater.update_schema()
            
            class Session(object):
                pass
            
            schema_api.mapper(Session, dbapi_session)   
            
            unfinished_sessions = transaction.select(Session, Session.close_ts==None)
            for session in unfinished_sessions:
                session.close_ts = datetime.datetime.now() 

            updater.update_finished(self.to_version)


class UpgradeTemplates(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade templates win_mstsc and the experimental template win_mstsc_rdp_con with new custom fields
    """
    new_content = """
use multimon:i:%(custom_template.usemultimon)
displayconnectionbar:i:%(custom_template.displayconnectionbar)
disable wallpaper:i:%(custom_template.disablewallpaper)
allow font smoothing:i:%(custom_template.allowfontsmoothing)
allow desktop composition:i:%(custom_template.allowdesktopcomposition)
disable full window drag:i:%(custom_template.disablefullwindowdrag)
disable menu anims:i:%(custom_template.disablemenuanims)
disable themes:i:%(custom_template.disablethemes)
bitmapcachepersistenable:i:%(custom_template.bitmapcachepersistenable)
disable cursor setting:i:%(custom_template.disablecursorsetting)"""


    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi_template = schema_api.SchemaFactory.get_creator("templates", checkpoint_handler=self.checkpoint_handler)
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path, checkpoint_handler=self.checkpoint_handler)
            
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)
            updater_template.update_schema()
            
            class TemplateUse(object):
                pass
            
            class TemplateValue(object):
                pass

            schema_api.mapper(TemplateValue, table_template_value)
            schema_api.mapper(TemplateUse, table_template_use)
            
            tempate_elements = transaction.select(TemplateUse, schema_api.or_(TemplateUse.template_name=='win_mstsc',
                                                                              TemplateUse.template_name=='win_mstsc_rdp_con'))
                
            for template_element in tempate_elements:
                
                def get_template_value(field_name):
                    return transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_element.id,
                                                                                          TemplateValue.field_name==field_name))
                    
                template_value = get_template_value("param_file_template")
                if template_value:
                    template_value.value += self.new_content 
                

            updater_template.update_finished(self.to_version)


class UpgradeInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)


    def upgrade_config(self, restore_path_config):
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_config', __name__, lib.checkpoint.DEBUG):
            print restore_path_config, self.from_version, self.to_version
        
            ini_root = os.path.join(restore_path_config, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_INI_FOLDER)
            
            configuration_server_management = components.config.common.ConfigServerManagement()
            self.update_config(configuration_server_management, ini_root)

            configuration_server_gateway = components.config.common.ConfigServerGateway()
            self.update_config(configuration_server_gateway, ini_root)

            configuration_server_config = components.config.common.ConfigConfigService()
            self.update_config(configuration_server_config, ini_root)

    def update_config(self, configuration_server, ini_root):
        configuration_server.read_config_file_from_folder(ini_root)

        db_connect_string = configuration_server.db_connect_string
        if db_connect_string:
            db_url = sqlalchemy_url.make_url(db_connect_string)
    
            configuration_server.db_type = db_url.drivername
            configuration_server.db_host = db_url.host if db_url.host else ""
            configuration_server.db_port = str(db_url.port) if db_url.port else ""
            configuration_server.db_database = db_url.database if db_url.database else ""
            configuration_server.db_username = db_url.username if db_url.username else ""
            configuration_server.db_password = db_url.password if db_url.password else "" # encrypt ??
            configuration_server.db_query = ""
            if db_url.query:
                if isinstance(db_url.query, dict):
                    configuration_server.db_query = ",".join(["%s=%s" % (option,value) for option, value in db_url.query.items()])
                elif isinstance(db_url.query, basestring):
                    configuration_server.db_query = db_url.query
            configuration_server.db_connect_string = ""
            
            configuration_server.write_config_file(ini_root)
        

class UpgradeLDAPInifiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade ini-files ....
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)


        
    def upgrade_plugin(self, plugin_id, plugin_version, restore_path_plugin):
        if plugin_id != 'ldap':
            return
        
        with self.checkpoint_handler.CheckpointScope('UpgradeInifiles::upgrade_plugin', "ldap", lib.checkpoint.DEBUG):
            self.update_config_file(restore_path_plugin, "management_config.ini")
            self.update_config_file(restore_path_plugin, "gateway_config.ini")

    def update_config_file(self, restore_path_plugin, filename):
        management_config = ldap_config.Config(restore_path_plugin, raise_error=False, filename=filename)
        ldap_config.Config.write(restore_path_plugin, management_config.enabled, management_config.authorization_domains, filename)




class UpgradeGenerateSystemfiles(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Generate new system files for 5.5.0
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_config(self, restore_path_config):
        with self.checkpoint_handler.CheckpointScope('UpgradeGenerateSystemfiles::upgrade_config', __name__, lib.checkpoint.DEBUG):
            deploy_root = os.path.join(restore_path_config, components.server_config_ws.server_config.ServerConfigurationAll.BACKUP_DEPLOYED_FOLDER)

            # Knownsecrets for management service connections
            secrets = lib.cryptfacility.generate_secrets()
            client_ks_filename = os.path.join(deploy_root, components.config.common.SERVICE_MANAGEMENT_KS_CLIENT_FILENAME)
            ks_client_file = open(client_ks_filename, 'w')
            ks_client_file.write(secrets[0])
            ks_client_file.close()

            server_ks_filename = os.path.join(deploy_root, components.config.common.SERVICE_MANAGEMENT_KS_SERVER_FILENAME)
            ks_server_file = open(server_ks_filename, 'w')
            ks_server_file.write(secrets[1])
            ks_server_file.close()

            # Rename gon_firewall_whitelist.inf 
            old_filename = os.path.join(deploy_root, 'gon_firewall_whitelist.inf')
            new_filename = os.path.join(deploy_root, components.config.common.CLIENT_FIREWALL_WHITHELIST_FILENAME)
            if os.path.exists(old_filename):
                shutil.move(old_filename, new_filename)

            # Generate new connect_info files
            servers_filename = os.path.join(deploy_root, components.config.common.CLIENT_SERVERS_FILENAME) 
            knownsecret_filename = os.path.join(deploy_root, components.config.common.KS_CLIENT_FILENAME) 
            qr_image_filename = os.path.join(deploy_root, components.config.common.CONNECT_INFO_QR_IMAGE_FILENAME) 
            dat_filename = os.path.join(deploy_root, components.config.common.CONNECT_INFO_DAT_FILENAME) 
            lib.config.generate_gon_qr_image(self.checkpoint_handler, servers_filename, knownsecret_filename, qr_image_filename, dat_filename)

            # Upgrade checksums
            lib.appl.upgrade.update_checksum_for_folder(deploy_root, lib.appl.upgrade.CHECKSUMS_LOCAL_FILENAME)

if __name__ == '__main__':
    
    print "yo"
