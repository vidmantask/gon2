"""
This file contains the database schema for the endpoint component
"""
from __future__ import with_statement

def create_schema_and_mappers(schema_api, transaction):
    dbapi = schema_api.SchemaFactory.get_creator("endpoint_component")



#    table_endpoint_info = dbapi.create_table("endpoint_info", autoload=transaction)
#    table_endpoint_access_log = dbapi.create_table("endpoint_access_log", autoload=transaction)
#    table_endpoint_attribute = dbapi.create_table("endpoint_attribute", autoload=transaction)
#    
    table_registered_endpoint = dbapi.create_table("registered_endpoint", autoload=transaction)
    
#    table_registered_endpoint_attribute = dbapi.create_table("registered_endpoint_attribute", autoload=transaction)
    
    table_registered_endpoint_hardware_check = dbapi.create_table("registered_endpoint_hardware_check",
                                                  schema_api.Column('endpoint_id', schema_api.Integer(), nullable=False, index=True),
                                                  schema_api.Column('plugin_name', schema_api.String(100)),
                                                )
    
#    table_failed_enrollment = dbapi.create_table("failed_enrollment",
#                                                  schema_api.Column('token_serial', schema_api.String(200)),
#                                                  schema_api.Column('plugin_name', schema_api.String(100)),
#                                                  schema_api.Column('unique_session_id', schema_api.String(100)),
#                                                  schema_api.Column('error_message', schema_api.String(200)),
#                                                  schema_api.Column('timestamp', schema_api.DateTime()),
#                                                )
    
    dbapi.add_foreign_key_constraint(table_registered_endpoint_hardware_check, 'endpoint_id', table_registered_endpoint, ondelete="CASCADE")


    schema_api.mapper(HardwareCheckPlugin, table_registered_endpoint_hardware_check)
    schema_api.mapper(RegisteredEndpoint, table_registered_endpoint, 
                      relations = [schema_api.Relation(HardwareCheckPlugin, 'hardware_check_plugins', backref_property_name='endpoint')])
    
    return dbapi

class HardwareCheckPlugin(object):
    pass

class RegisteredEndpoint(object):

    def __init__(self, **kwargs):
        self.attribute_dict = None

        
            
            




#def get_endpoint_info_attributes(dbs, endpoint_info_id):
#    with database_api.ReadonlySession() as dbs:
#        info_attributes = []
#        db_attributes = dbs.select(EndpointAttribute, filter=EndpointAttribute.endpoint_info_id==endpoint_info_id)
#        for db_attribute in db_attributes:
#            pass

def get_registered_endpoint(endpoint_id, db_session):
    return db_session.get(RegisteredEndpoint, endpoint_id)

def get_registered_endpoint_by_token_serial(token_serial, db_session):
    return db_session.select_first(RegisteredEndpoint, filter=RegisteredEndpoint.token_serial==token_serial)



