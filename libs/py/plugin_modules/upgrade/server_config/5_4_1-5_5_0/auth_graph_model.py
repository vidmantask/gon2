"""The database model and the interface and object-persistense to the database"""

                
    

class RuleObject(object):

    """
    Base class for all autograph nodes
    """
    

    (AllOf,  # and, universal quantifier
     OneOf,  # or, existential quantifier
     NoneOf,  # and not, non-existential quantifier / universal non-quantifier
     ) = range(3)

    ''' Internal type values '''
    
    TYPE_CONDITION = "Condition"
    TYPE_ACTION = "Action"
    TYPE_RULE = "Rule"
    TYPE_RULE_CLAUSE = "RuleClause"
    TYPE_ASSSOCIATION = "Association"
    TYPE_ASSSOCIATION_CLAUSE = "AssociationClause"
    TYPE_GROUP = "Group"


    def __str__(self):
        if self.title:
            return self.title
        return "unknown"
    
    def update_title(self, title):
        self.title = title
    
    def add_criteria_value(self, database_api, transaction, entity_type, value_id, value_title, rule_entity_type=None, deactivated=False):
        existing = transaction.select(RuleObject, database_api.and_(RuleObject.value_id==value_id,
                                                                    RuleObject.entity_type==entity_type))
        
        if existing:
            c = existing[0]
        else:
            c = transaction.add(Condition())
            c.entity_type = entity_type
            c.value_id = value_id
            c.title = value_title
            
        self.add_sub_criteria(c, rule_entity_type, deactivated)
    
    @classmethod
    def get_internal_type_name(self, rule_object):
        if isinstance(rule_object, Group):
            return self.TYPE_GROUP
        elif isinstance(rule_object, Association):
            return self.TYPE_ASSSOCIATION
        elif isinstance(rule_object, AssociationClause):
            return self.TYPE_ASSSOCIATION_CLAUSE
        elif isinstance(rule_object, Rule):
            return self.TYPE_RULE
        elif isinstance(rule_object, RuleClause):
            return self.TYPE_RULE_CLAUSE
        elif isinstance(rule_object, Action):
            return self.TYPE_ACTION
        elif isinstance(rule_object, Condition):
            return self.TYPE_CONDITION
        else:
            raise Exception("Unknown rule type '%s'" % rule_object.__class__)
        
    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==result_id:
            return True
        for parent in self.get_parents():
            if parent.is_in_rule(result_id, exclude_rule_id):
                return True
        return False

    def add_sub_criteria(self, criteria, rule_entity_type=None, deactivated=False):
        a = RuleAssociation()
        a.parent_criteria = self
        a.deactivated = deactivated
        if self.internal_type_name:
            a.parent_internal_type_name = self.internal_type_name
        else:
            a.parent_internal_type_name = self.get_internal_type_name(self)
        a.child_criteria = criteria
        if criteria.internal_type_name:
            a.child_internal_type_name = criteria.internal_type_name
        else:
            a.child_internal_type_name = self.get_internal_type_name(criteria)
        a.rule_entity_type = rule_entity_type
        self.child_criterias.append(a)

    def get_id(self):
        return "%d" % self.id

    def get_parents(self):
        return [a.parent_criteria for a in self.parent_criterias]
    

    def get_children(self):
        return [a.child_criteria for a in self.child_criterias]
        
    def get_entity_type_and_children(self):
        return [(a.rule_entity_type, a.child_criteria) for a in self.child_criterias]


    def is_last_connection(self, rule_association):
        return False
            
    def set_true_bottom_up(self, db_session, nodes):
        if self.id in nodes:
            return
        nodes.add(self.id)
#        print self.title
        global count
        count += 1
        nodes.add(self.id)
        if self.aggregation_kind == RuleObject.AllOf:
            children = self.get_sub_criteria(db_session)
            children.extend(self.get_conditions(db_session))
            for child in children:
                b = child.id in nodes
#        with checkpoint_handler_demo.CheckpointScope("RuleCriteria.get_parents", "get_parents", lib.checkpoint.INFO, this=self.title):
        parents = self.get_parents(db_session)
        for parent in parents:
            parent.set_true_bottom_up(db_session, nodes)
        
        

        

        
     
#    def __init__(self, **kwargs):
#        self.aggregation_kind = self.OneOf

#    @reconstructor
#    def __db__init__(self):
#        self.calc_result = None


    def get_entity_type(self):
        return self.entity_type


    
    def description(self):
        f =  {self.AllOf: "All Of", 
             self.OneOf: "One Of", 
             self.NoneOf: "None Of",
             }.get(self.aggregation_kind, None)
        prefix = ""
#        if self.internal_type == self.RuleClass:
#            prefix = "RuleClass"
#        elif self.internal_type == self.Action:
#            prefix = "Action"
#        elif self.internal_type == self.Simple:
#            prefix = "Simple"
#        elif self.internal_type == self.WindowRules:
#            prefix = "WindowRules"
#        elif self.internal_type == self.NonStandard:
#            prefix = "NonStandard"
        return "%s %s %s" % (prefix, self.internal_type_name, f)
        


class Action(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf
        
    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)

    def delete(self, transaction):
        for child_edge in self.child_criterias:
            child_edge.child_criteria.delete(transaction)
            transaction.delete(child_edge)
        transaction.delete(self)
        

class Condition(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = None
    
    def is_last_connection(self, rule_association):
        if len(self.parent_criterias)==1 and self.parent_criterias[0].id == rule_association.id:
            return True 
        return False

    

class Rule(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf
        
    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)
    
        
    def get_new_clause_criteria(self, transaction, activated):
        clause_criteria = RuleClause()
        clause_criteria.deactivated = not activated
        clause_criteria.title = self.title
        self.add_sub_criteria(clause_criteria)
        return clause_criteria
    


class RuleClause(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.AllOf

    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==exclude_rule_id:
            return False
        return RuleObject.is_in_rule(self, result_id, exclude_rule_id)
        

class Association(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf

    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)

    def get_new_clause_criteria(self, transaction, activated):
        clause_criteria = AssociationClause()
        clause_criteria.deactivated = not activated
        clause_criteria.title = self.title
        self.add_sub_criteria(clause_criteria)
        return clause_criteria



class AssociationClause(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.AllOf

    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==exclude_rule_id:
            return False
        return RuleObject.is_in_rule(self, result_id, exclude_rule_id)

class Group(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf

    def get_new_clause_criteria(self, transaction, activated):
        a = RuleAssociation()
        a.parent_criteria = self
        a.parent_internal_type_name = self.TYPE_GROUP
        # set dummy - otherwise error if flush issued
        a.child_criteria = a.parent_criteria
        a.child_internal_type_name = a.parent_internal_type_name
        return a
    
    
    def is_in_rule(self, result_id, exclude_rule_id):
        raise NotImplementedError()
    
    def has_member(self, member_id, exclude_rule_id):
        for group_edge in self.child_criterias:
            if group_edge.id != exclude_rule_id and member_id == group_edge.child_id:
                return True

class RuleAssociation(object):

    def add_sub_criteria(self, criteria, rule_entity_type=None):
        self.child_criteria = criteria
        if criteria.internal_type_name:
            self.child_internal_type_name = criteria.internal_type_name
        else:
            self.child_internal_type_name = RuleObject.get_internal_type_name(criteria)
        self.rule_entity_type = rule_entity_type

    def delete(self, transaction):
        child_criteria = self.child_criteria
        if child_criteria.is_last_connection(self):
            transaction.delete(child_criteria)
        transaction.delete(self)

class DefaultRuleCriteria(object):
    
    pass        

class BuiltInCriteria(object):
    pass        



def create_schema_and_mappers(schema_api):
    
    dbapi = schema_api.SchemaFactory.get_creator("auth_server")
    
    
    table_criteria = dbapi.create_table("rule_node",
                                        schema_api.Column('title', schema_api.String(256), nullable=False),
                                        schema_api.Column('description', schema_api.Text()),
                                        schema_api.Column('aggregation_kind', schema_api.Integer),
                                        schema_api.Column('internal_type_name', schema_api.String(128), nullable=False),
                                        schema_api.Column('entity_type', schema_api.String(128), index=True),
                                        schema_api.Column('value_id', schema_api.String(256), index=True),
                                        schema_api.Column('is_true', schema_api.Boolean, default=False),
                                        )
    
    table_sub_criteria = dbapi.create_table("rule_node_association",
                                        schema_api.Column('rule_entity_type', schema_api.String(128)),
                                        schema_api.Column('parent_id', schema_api.Integer, nullable=False, index=True),
                                        schema_api.Column('child_id', schema_api.Integer, nullable=False, index=True),
                                        schema_api.Column('parent_internal_type_name', schema_api.String(128), nullable=False, index=True),
                                        schema_api.Column('child_internal_type_name', schema_api.String(128), nullable=False, index=True),
                                        schema_api.Column('deactivated', schema_api.Boolean, default=False),
                                        )
    
    table_default_rule_criteria = dbapi.create_table("default_rule_criteria",
                                         schema_api.Column('rule_type', schema_api.String(128), nullable=False),
                                         schema_api.Column('entity_type', schema_api.String(128), nullable=False),
                                         schema_api.Column('node_id', schema_api.Integer, nullable=False)
                                        )
    
    table_built_in_criteria = dbapi.create_table("built_in_criteria",
                                         schema_api.Column('internal_name', schema_api.String(128), nullable=False),
                                         schema_api.Column('node_id', schema_api.Integer, nullable=False)
                                        )
    
    table_access_right = dbapi.create_table("access_right",
                                     schema_api.Column('name', schema_api.String(128), nullable=False),
                                     schema_api.Column('title', schema_api.String(256), nullable=False),
                                     schema_api.Column('element_type', schema_api.String(128), nullable=False),
                                     )
    
    table_access_element_attribute = dbapi.create_table("access_element_attribute",
                                         schema_api.Column('access_right_id', schema_api.Integer, nullable=False),
                                         schema_api.Column('access_element_id', schema_api.Integer, nullable=False),
                                         schema_api.Column('access_level', schema_api.String(16), nullable=False),
                                         )
    
    table_rule_restriction = dbapi.create_table("rule_restriction",
                                        schema_api.Column('restriction_type', schema_api.String(128)),
                                        schema_api.Column('rule_id', schema_api.Integer, nullable=False, index=True),
                                        schema_api.Column('restriction_rule_id', schema_api.Integer, nullable=False, index=True),
                                        )
                                
    
    table_element_lifetime = dbapi.create_table("element_lifetime",
                                        schema_api.Column('entity_type', schema_api.String(128), index=True, nullable=False),
                                        schema_api.Column('value_id', schema_api.String(256), index=True, nullable=False),
                                        schema_api.Column('start', schema_api.DateTime),
                                        schema_api.Column('end', schema_api.DateTime),
                                        )
    
    
    
    dbapi.add_foreign_key_constraint(table_sub_criteria, "parent_id", table_criteria)
    dbapi.add_foreign_key_constraint(table_sub_criteria, "child_id", table_criteria)
    dbapi.add_foreign_key_constraint(table_default_rule_criteria, "node_id", table_criteria)
    dbapi.add_foreign_key_constraint(table_built_in_criteria, "node_id", table_criteria)
    
    dbapi.add_foreign_key_constraint(table_access_element_attribute, "access_right_id", table_access_right)
    dbapi.add_foreign_key_constraint(table_access_element_attribute, "access_element_id", table_criteria)
    
    dbapi.add_foreign_key_constraint(table_rule_restriction, "rule_id", table_criteria)
    dbapi.add_foreign_key_constraint(table_rule_restriction, "restriction_rule_id", table_criteria)
    
    dbapi.add_unique_constraint(table_access_element_attribute, "access_right_id", "access_element_id")
    dbapi.add_unique_constraint(table_element_lifetime, "entity_type", "value_id")
    
    
    schema_api.mapper(RuleObject, table_criteria, 
           polymorphic_identity = "BaseRule",
           polymorphic_on = table_criteria.c.internal_type_name)
    
    schema_api.mapper(Action, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ACTION)
    schema_api.mapper(Condition, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_CONDITION)
    schema_api.mapper(Association, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ASSSOCIATION)
    schema_api.mapper(AssociationClause, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ASSSOCIATION_CLAUSE)
    schema_api.mapper(Rule, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_RULE)
    schema_api.mapper(RuleClause, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_RULE_CLAUSE)
    schema_api.mapper(Group, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_GROUP)
    
    relations = [schema_api.Relation(RuleObject, 'parent_criteria', backref_property_name = 'child_criterias',
                          primaryjoin=table_sub_criteria.c.parent_id==table_criteria.c.id),
                 schema_api.Relation(RuleObject, 'child_criteria', backref_property_name = 'parent_criterias',
                          primaryjoin=table_sub_criteria.c.child_id==table_criteria.c.id)
                          ]
    
    schema_api.mapper(RuleAssociation, table_sub_criteria, relations=relations)
    
    schema_api.mapper(DefaultRuleCriteria, table_default_rule_criteria, relations=[schema_api.Relation(RuleObject, 'criteria', backref_property_name = 'default_in_rules')])
    schema_api.mapper(BuiltInCriteria, table_built_in_criteria, relations=[schema_api.Relation(RuleObject, 'criteria', backref_property_name = 'built_in')])

    return dbapi


