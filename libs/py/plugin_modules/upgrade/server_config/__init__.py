"""
Upgrade plugin

"""
from __future__ import with_statement

import lib.checkpoint

from plugin_types.server_config import plugin_type_upgrade

class Upgrade(plugin_type_upgrade.PluginTypeUpgrade):
    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_upgrade.PluginTypeUpgrade.__init__(self, checkpoint_handler, server_configuration_all, __name__, "SysUpgrade")

    def import_schema(self):
        import components.database.server_common.schema_api
        import components.auth.server_common.database_schema
        import components.dialog.server_common.database_schema
        import components.management_message.server_common.database_schema
        import components.traffic.server_common.database_schema
        import components.user.server_common.database_schema
        import components.access_log.server_common.database_schema
        import plugin_types.common.plugin_type_token
        import components.templates.server_common.database_schema
        import components.server_config_ws.database_schema
        import components.admin_ws.database_schema
