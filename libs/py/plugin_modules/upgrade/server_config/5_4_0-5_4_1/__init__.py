"""
Version upgrade plugin
"""
import os
import os.path

import lib.checkpoint

import components.server_config_ws.server_config
import components.config.common

from plugin_types.server_config import plugin_type_upgrade
from components.database.server_common import schema_api

            


class UpgradeTraffic(plugin_type_upgrade.PluginTypeUpgradeVersion):
    """
    Upgrade access-log where a new column (server_id) has been added in order to do proper cleanup
    """
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        plugin_type_upgrade.PluginTypeUpgradeVersion.__init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version)

    def upgrade_db(self, schema_api, transaction, restore_path):
        # TODO Finish this...
        with self.checkpoint_handler.CheckpointScope('UpgradeTraffic::upgrade', __name__, lib.checkpoint.DEBUG):

            dbapi = schema_api.SchemaFactory.get_creator("traffic_component")
            
            
            table_launch_element = dbapi.create_table("launch_element", autoload=transaction)

            updater = schema_api.SchemaFactory.get_updater(dbapi, transaction, restore_path)
            updater.update_schema()
            
            # Update param_file_lifetime for launch type 1 actions
            
            dbapi_template = schema_api.SchemaFactory.get_creator("templates")
            updater_template = schema_api.SchemaFactory.get_updater(dbapi_template, transaction, restore_path)
            
            table_template_use = dbapi_template.create_table("template_use", autoload=transaction)
            table_template_value = dbapi_template.create_table("template_value", autoload=transaction)
            updater_template.update_schema()
            
            class TemplateUse(object):
                pass
            
            class TemplateValue(object):
                pass

            class LaunchElement(object):
                pass
            
            schema_api.mapper(TemplateValue, table_template_value)
            schema_api.mapper(TemplateUse, table_template_use)
            schema_api.mapper(LaunchElement, table_launch_element)
            
            tempate_elements = transaction.select(TemplateUse, schema_api.or_(TemplateUse.template_name=='win_mstsc',
                                                                              TemplateUse.template_name=='win_mstsc_rdp_con'))
                
            for template_element in tempate_elements:
                
                def get_template_value(field_name):
                    return transaction.select_first(TemplateValue, filter=schema_api.and_(TemplateValue.template_use_id==template_element.id,
                                                                                          TemplateValue.field_name==field_name))
                    
                template_value = get_template_value("fullscreen")
                new_template_value = get_template_value("screenmode")
                if template_value:
                    if not new_template_value:
                        template_value.field_name = "screenmode"
                        template_value.value = "2"
                    else:
                        transaction.delete(template_value)
                template_value = get_template_value("command")
                if template_value:
                    template_value.value = template_value.value.replace("%(custom_template.fullscreen)","")
                template_value = get_template_value("param_file_template")
                if template_value:
                    template_value.value = template_value.value.replace("screen mode id:i:2","screen mode id:i:%(custom_template.screenmode)")
                launch_element = transaction.select_first(LaunchElement, filter=(LaunchElement.action_id==template_element.element_id))
                launch_element.command = launch_element.command.replace("/fullscreen", "") 


            updater.update_finished(self.to_version)
            updater_template.update_finished(self.to_version)
                        

if __name__ == '__main__':
    
    print "yo"
