"""
SmartCard plugin for the Management Server 
"""
from __future__ import with_statement

import lib.checkpoint
from plugin_types.server_management import plugin_type_token


class PluginSoftToken(plugin_type_token.PluginToken):
    
    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_type_token.PluginToken.__init__(self, checkpoint_handler, database, license_handler, u'smart_card')


