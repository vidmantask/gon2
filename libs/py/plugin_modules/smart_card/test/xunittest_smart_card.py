"""
Unittest of certificate plugin module
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility
import lib.hardware.device


from plugin_types.client_gateway import plugin_type_token

from components.database.server_common.connection_factory import ConnectionFactory


import plugin_modules.smart_card.client_gateway_common
import plugin_modules.smart_card.server_gateway
import plugin_modules.smart_card.client_gateway
from plugin_types.common import plugin_type_token as database_model 



class Deployment(unittest.TestCase):

    def setUp(self):
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        tokens = deploy_plugin.get_tokens(u'smart_card', [])
        self.assertTrue(len(tokens) > 0, "No smart_card token found, unittest aborted")
        self.token_device = tokens[0].token_id

        self.token_serial = 'my_smart_card_001'
        self.client_knownsecret = 'my_known_secret'
        self.servers = '127.0.0.1, 8043'
        
    def test_initialization_and_deploy(self):
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        deploy_plugin.initialize_token(self.token_device)
        deploy_plugin.set_serial(self.token_device, self.token_serial)
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)

        tokens = deploy_plugin.get_tokens(u'smart_card', [self.token_device])
        self.assertEqual(len(tokens), 1)
        self.assertEqual(tokens[0].token_serial, self.token_serial)
        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))

    def test_gets(self):
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
        self.assertEqual(deploy_plugin.get_serial(self.token_device), self.token_serial)
        self.assertNotEqual(deploy_plugin.get_public_key(self.token_device), '')
        self.assertNotEqual(deploy_plugin.get_public_key(self.token_device), None)

        (knownsecret, servers) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
        self.assertNotEqual(knownsecret, None)
        self.assertNotEqual(servers, None)

        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))
        deploy_plugin.set_enrolled(self.token_device)
        self.assertTrue(deploy_plugin.is_enrolled(self.token_device))
        deploy_plugin.reset_enrolled(self.token_device)
        self.assertFalse(deploy_plugin.is_enrolled(self.token_device))

        (knownsecret_after, servers_after) = deploy_plugin.get_knownsecret_and_servers(self.token_device)
        self.assertEqual(knownsecret, knownsecret_after)
        self.assertEqual(servers, servers_after)




class AuthCallbackStub(object):
    
    def plugin_event_state_change(self, name, info=[]):
        pass

class Auth_plugin_test(unittest.TestCase):

    def setUp(self):
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        tokens = deploy_plugin.get_tokens(u'smart_card', [])
        self.assertTrue(len(tokens) > 0, "No smart_card token found, unittest aborted")
        self.token_device = tokens[0].token_id

        self.token_serial = 'my_smart_card_001'
        self.client_knownsecret = 'my_known_secret'
        self.servers = '127.0.0.1, 8043'

        self.db_filename = os.tmpnam()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)
        

        
    def tearDown(self):
        self.db.dispose()
        try:
            os.remove(self.db_filename)
        except:
            pass

    def test_auth_plugin_simple(self):
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        self.plugin_client = plugin_modules.smart_card.client_gateway.PluginTokenAndAuth(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, [])
        self.plugin_server = plugin_modules.smart_card.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, self.db, None, None, None)
        self.plugin_server.set_callback(AuthCallbackStub())
        
        self.plugin_client._token_id = self.token_device
        
        self.plugin_client.set_tunnelendpoint(tunnel_endpoint_client)
        self.plugin_server.set_tunnelendpoint(tunnel_endpoint_server)
        
        deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
        deploy_plugin.initialize_token(self.token_device)
        deploy_plugin.set_serial(self.token_device, self.token_serial)
        deploy_plugin.deploy_token(self.token_device, self.client_knownsecret, self.servers)
        
        with database_model.Transaction() as dbt:
            new_token =  dbt.add(database_model.Token())
            new_token.serial = self.token_serial
            new_token.plugin_type = u'smart_card'
            new_token.public_key = deploy_plugin.get_public_key(self.token_device)
            dbt.commit()

        self.plugin_server.start()
        giri_unittest.wait_until_with_timeout(lambda:self.plugin_server.check_predicate((self.token_serial, self.token_serial)), 10000)
        self.assertEqual(self.plugin_server.check_predicate((self.token_serial, self.token_serial)), True)



        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

def smart_card_token_found():
    deploy_plugin = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(giri_unittest.get_checkpoint_handler())
    tokens = deploy_plugin.get_tokens(u'smart_card', [])
    return len(tokens) > 0
GIRI_UNITTEST_IGNORE = not smart_card_token_found()

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
