"""
Common functionality for SmartCard tokens on Gateway Client level
"""
from __future__ import with_statement

import os
import os.path
import shutil
import tempfile
import sys
import binascii
import traceback
import pickle
import zlib
import uuid

import lib.checkpoint
import lib.cryptfacility

import lib.config

import lib.smartcard
import lib.smartcard.pcsc
import lib.smartcard.pcsc_pkcs15
import lib.smartcard.common

import plugin_modules.smart_card
from plugin_types.client_gateway import plugin_type_token

CHUNK_ID_INIT = 0
CHUNK_ID_DEPLOY = 1


class SmartCardTokens(object):
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
        self._data_chunks_init = {}
        self._data_chunks_deploy = {}

    def _get_devices(self):
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                return pcsc_handler.get_readers()
        except lib.smartcard.common.SmartCardException, e:
            pass
        return []
   
    def initialize_token(self, device):
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            serial_new = '%s' % uuid.uuid4()
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    pcsc_pkcs15.generate_key_pair(keylength=2048)
                    self._put_data_chunk_init(device, pcsc_pkcs15, serial_new)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)


    def get_tokens(self, plugin_name, additional_device_roots):
        devices = self._get_devices()
        tokens = []
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                for device in devices:
                    with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                        token = plugin_type_token.Token(plugin_name, device, plugin_type_token.Token.TOKEN_STATUS_INITIALIZED, None, token_type_title="SmartCard", token_plugin_module_name='smart_card')
                        pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                        try:
                            (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(device, pcsc_pkcs15)
                            if  enrolled:
                                token.token_status = plugin_type_token.Token.TOKEN_STATUS_ENROLLED
                        except lib.smartcard.common.SmartCardException, e:
                            (etype, evalue, etrace) = sys.exc_info()
                            self.checkpoint_handler.CheckpointMultilineMessages('get_tokens', plugin_modules.smart_card.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
                        tokens.append(token)
        except lib.smartcard.common.SmartCardException, e:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointMultilineMessages('get_tokens.failed', plugin_modules.smart_card.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return tokens

    def deploy_token(self, token_id, client_knownsecret, servers):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    pcsc_pkcs15.generate_key_pair()
                    self._put_data_chunk_deploy(token_id, pcsc_pkcs15, client_knownsecret, servers, False)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def generate_keypair(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    pcsc_pkcs15.generate_key_pair()
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def get_public_key(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    public_key = pcsc_pkcs15.get_public_key()
                    return lib.smartcard.pkcs15.to_hex(public_key)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def set_serial(self, token_id, serial):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    self._put_data_chunk_init(device, pcsc_pkcs15, serial)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def get_serial(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (serial) = self._get_data_chunk_init(token_id, pcsc_pkcs15)
                    return serial
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)
    
    def get_knownsecret_and_servers(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(token_id, pcsc_pkcs15)
                    servers = lib.config.parse_servers(self.checkpoint_handler, servers_raw)
                    return (client_knownsecret, servers)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def set_enrolled(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(token_id, pcsc_pkcs15)
                    self._put_data_chunk_deploy(token_id, pcsc_pkcs15, client_knownsecret, servers_raw, True)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)
        
    def reset_enrolled(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(token_id, pcsc_pkcs15)
                    self._put_data_chunk_deploy(token_id, pcsc_pkcs15, client_knownsecret, servers_raw, False)
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)

    def is_enrolled(self, token_id):
        device = token_id
        if not device in self._get_devices():
            raise plugin_type_token.Error("'%s' is a invalid device'" % device)
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(token_id, pcsc_pkcs15)
                    return enrolled
        except lib.smartcard.common.SmartCardException, e:
            raise plugin_type_token.Error(e)
        return False

    def set_servers(self, token_id, servers):
        device = token_id
        if not device in self._get_devices():
            return "'%s' is a invalid device'" % device
        try:
            with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                with pcsc_handler.get_connected_transaction(device) as smart_card_transaction:
                    pcsc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                    (client_knownsecret, servers_raw, enrolled) = self._get_data_chunk_deploy(token_id, pcsc_pkcs15)
                    self._put_data_chunk_deploy(token_id, pcsc_pkcs15, client_knownsecret, servers, enrolled)
                    return None
        except lib.smartcard.common.SmartCardException, e:
            return str(e)

    def _put_data_chunk_init(self, token_id, pcsc_pkcs15, serial):
        chunk = (ord(c) for c in zlib.compress(pickle.dumps( (serial) ), 9))
        pcsc_pkcs15.write_file(chunk, CHUNK_ID_INIT)
        if self._data_chunks_init.has_key(token_id):
            del self._data_chunks_init[token_id]
    
    def _put_data_chunk_deploy(self, token_id, pcsc_pkcs15, client_knownsecret, servers, enrolled):
        chunk = (ord(c) for c in zlib.compress(pickle.dumps( (client_knownsecret, servers, enrolled) ), 9))
        pcsc_pkcs15.write_file(chunk, CHUNK_ID_DEPLOY)
        if self._data_chunks_deploy.has_key(token_id):
            del self._data_chunks_deploy[token_id]

    def _get_data_chunk_init(self, token_id, pcsc_pkcs15):
        if self._data_chunks_init.has_key(token_id):
            return self._data_chunks_init[token_id]
        else:
            try:
                chunk = ''.join(chr(x) for x in pcsc_pkcs15.read_file(CHUNK_ID_INIT))
                self._data_chunks_init[token_id] = pickle.loads(zlib.decompress(chunk))
                return self._data_chunks_init[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_init.failed', plugin_modules.smart_card.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None)
    
    def _get_data_chunk_deploy(self, token_id, pcsc_pkcs15):
        if self._data_chunks_deploy.has_key(token_id):
            return self._data_chunks_deploy[token_id]
        else:
            try:
                chunk_raw = ''.join(chr(x) for x in  pcsc_pkcs15.read_file(CHUNK_ID_DEPLOY))
                chunk = pickle.loads(zlib.decompress(chunk_raw))
                (client_knownsecret, servers, enrolled) = (None, None, False)
                if len(chunk) == 2:
                    (client_knownsecret, servers) = chunk
                if len(chunk) == 3:
                    (client_knownsecret, servers, enrolled) = chunk
                self._data_chunks_deploy[token_id] =  (client_knownsecret, servers, enrolled)
                return self._data_chunks_deploy[token_id]
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointMultilineMessages('_get_data_chunk_init.failed', plugin_modules.smart_card.plugin_name, lib.checkpoint.DEBUG, traceback.format_exception_only(etype, evalue))
        return (None, None, False)
