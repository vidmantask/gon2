"""
SmartCard plugin for Gateway Client
"""
from __future__ import with_statement

import os.path
import lib.checkpoint
import lib.cryptfacility
import lib.smartcard
import lib.smartcard.pcsc
import lib.smartcard.pcsc_pkcs15
import lib.smartcard.common


from components.communication import message

from plugin_types.client_gateway import plugin_type_token
from plugin_types.client_gateway import plugin_type_auth

import plugin_modules.smart_card.client_gateway_common


class PluginTokenAndAuth(plugin_type_token.PluginTypeToken, plugin_type_auth.PluginTypeAuth):
    PLUGIN_NAME = u"smart_card"
    """
    Token plugin 
    """
    def __init__(self, async_service, checkpoint_handler, user_interface, additional_device_roots):
        plugin_type_token.PluginTypeToken.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, PluginTokenAndAuth.PLUGIN_NAME, user_interface, additional_device_roots)
        self._tokens = plugin_modules.smart_card.client_gateway_common.SmartCardTokens(checkpoint_handler)    
        self._token_id = None
    
    def initialize_token(self, device):
        self._tokens.initialize_token(device)

    def get_tokens(self, additional_device_roots):
        return self._tokens.get_tokens(self.plugin_name, additional_device_roots)

    def deploy_token(self, token_id, client_knownsecret, servers):
        self._tokens.deploy_token(token_id, client_knownsecret, servers)

    def generate_keypair(self, token_id):
        self._tokens.generate_keypair(token_id)

    def set_serial(self, token_id, serial):
        self._tokens.set_serial(token_id, serial)

    def get_public_key(self, token_id):
        return self._tokens.get_public_key(token_id)

    def set_enrolled(self, token_id):
        self._tokens.set_enrolled(token_id)
        
    def reset_enrolled(self, token_id):
        self._tokens.reset_enrolled(token_id)

    def is_enrolled(self, token_id):
        return self._tokens.is_enrolled(token_id)

    def get_knownsecret_and_servers(self, token_id):
        return self._tokens.get_knownsecret_and_servers(token_id)

    def set_servers(self, token_id, servers):
        return self._tokens.set_servers(token_id, servers)

    def get_serial(self, token_id=None):
        if token_id is None:
            return self.get_serial_auth()
        return self._tokens.get_serial(token_id)

    def _select_token(self):
        if self._token_id == None:
            tokens = self._tokens.get_tokens(self.plugin_name, self.additional_device_roots)
            if len(tokens) > 0:
                return tokens[0].token_id
            return None
        else:
            return self._token_id
        
    def _get_serial(self):
        token_id = self._select_token() 
        if token_id != None:
            try:
                return unicode(self._tokens.get_serial(token_id))
            except:
                self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.DEBUG, message='No serial found')
                return None
        else:
            self.checkpoint_handler.Checkpoint('_get_serial', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None
        
    def _get_challenge(self, challenge):
        token_id = self._select_token() 
        if token_id != None:
            try:
                with lib.smartcard.pcsc.PcSc() as pcsc_handler:
                    with pcsc_handler.get_connected_transaction(token_id) as smart_card_transaction:
                        msc_pkcs15 = lib.smartcard.pkcs15.Pkcs15(smart_card_transaction)
                        return msc_pkcs15.create_challenge_signature(challenge)
            except lib.smartcard.common.SmartCardException, e:
                self.checkpoint_handler.Checkpoint('get_challenge.error', self.plugin_name, lib.checkpoint.ERROR, message='%s' % e)
        else:
            self.checkpoint_handler.Checkpoint('_get_challenge', self.plugin_name, lib.checkpoint.WARNING, message='No tokens available')
            return None
        
    msg_receivers = ['get_serial', 'get_challenge']
    
    def get_serial_auth(self):
        with self.checkpoint_handler.CheckpointScope('get_serial', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            serial = self._get_serial()
            cps.add_complete_attr(serial=serial)
            self.tunnelendpoint_send(message.Message('get_serial_response', serial=serial))
                
    def get_challenge(self, challenge):
        with self.checkpoint_handler.CheckpointScope('get_challenge', self.plugin_name, lib.checkpoint.DEBUG):
            challenge_signature = self._get_challenge(challenge) 
            self.tunnelendpoint_send(message.Message('get_challenge_response', challenge_signature=challenge_signature))

