from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api



dbapi = schema_api.SchemaFactory.get_creator("plugin_ip_address")

table_adress_element = dbapi.create_table("adress_element",
                                         schema_api.Column('name', schema_api.String(256), nullable=False),
                                         schema_api.Column('description', schema_api.Text()),
                                         )

CLIENT_SOURCE = 0
SERVER_SOURCE = 1

table_ip_range = dbapi.create_table("ip_range",
                                    schema_api.Column('source', schema_api.Integer(), nullable=False, default=0),
                                    schema_api.Column('from_address', schema_api.String(256), nullable=False),
                                    schema_api.Column('to_address', schema_api.String(256)),
                                    schema_api.Column('address_id', schema_api.Integer(), index=True),
                                   )




dbapi.add_foreign_key_constraint(table_ip_range, 'address_id', table_adress_element)

schema_api.SchemaFactory.register_creator(dbapi)

class AddressElement(object):
    pass

class IpRange(object):
    pass


schema_api.mapper(IpRange, table_ip_range)
schema_api.mapper(AddressElement, table_adress_element, relations=[database_api.Relation(IpRange, 'ip_ranges')])
#schema_api.mapper(AddressElement, table_adress_element, relations=[database_api.Relation(IpRange, 'ip_ranges', backref_property_name='address_element')])

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)
