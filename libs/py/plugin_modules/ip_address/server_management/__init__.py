# -*- coding: iso-8859-1 -*-
"""
Plugin for Management Server
"""
from __future__ import with_statement
import sys
from IPy import IP

import lib.checkpoint
from plugin_types.server_management import plugin_type_element, plugin_type_config
from components.database.server_common import database_api
from plugin_modules.ip_address.server_common import database_schema 
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

# ??? allowed
from components.auth.server_management import OperationNotAllowedException

class PluginIpAddress(plugin_type_config.PluginTypeConfig, plugin_type_element.PluginTypeElement):

    def __init__(self, checkpoint_handler, database, license_handler):
        plugin_name = "ip_address"
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        
    element_types = ["address"]
        
    def get_elements(self, element_type):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.AddressElement)
            element_list = []
            for e in elements:
                element_list.append(self._create_element(e))
            return element_list
             
    def _create_element(self, e):
        info_list = []
        for ip_range in e.ip_ranges:
            if ip_range.to_address:
                info_list.append("%s-%s" % (ip_range.from_address, ip_range.to_address))
            else:
                info_list.append(ip_range.from_address)
        info = "\n".join(info_list)
        element = self.create_element(e.id, e.name, info)
        config_spec = self._get_config_spec(e)
        element.template = config_spec.get_template()
        return element

    def get_element_count(self, element_type):
        with database_api.ReadonlySession() as dbs:
            return dbs.select_count(database_schema.AddressElement) 
        
        
    def get_specific_elements(self, internal_element_type, element_ids):
        with database_api.ReadonlySession() as dbs:
            elements = dbs.select(database_schema.AddressElement, database_api.in_(database_schema.AddressElement.id, element_ids))
            return [self._create_element(e) for e in elements]
        

    def config_get_template(self, element_type, element_id, db_session=None):
        with database_api.SessionWrapper(db_session) as dbs:
            address = None
            if element_id:
                address = self._lookup_address(dbs, element_id)
            config_spec = self._get_config_spec(address)
            return config_spec.get_template()
        
    
    def get_config_enabling(self, element_type):
        return True, True
    
    
    def _get_config_spec(self, address):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (self.plugin_name,), "IP Address", "")

        field = config_spec.add_field("name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
        field = config_spec.add_field("id", 'ID', ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        config_spec.add_field("description", 'Description', ConfigTemplateSpec.VALUE_TYPE_TEXT)
        config_spec.add_field("client_ip_ranges", 'Client IP Ranges', ConfigTemplateSpec.VALUE_TYPE_TEXT, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, tooltip="IP ranges separated by a ',',\ne.g. 127.0.0.1-127.0.0.255, 192.168.0.0/24")
        config_spec.add_field("server_ip_ranges", 'Server IP Ranges', ConfigTemplateSpec.VALUE_TYPE_TEXT, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT, tooltip="IP ranges separated by ',',\ne.g. 127.0.0.1-127.0.0.255, 192.168.0.0/24")

        if address:
            config_spec.set_values_from_object(address)
            field_value_dict = dict()
            field_values = []
            for ip_range in address.ip_ranges:
                field_values = field_value_dict.get(ip_range.source, [])
                if ip_range.to_address:
                    value = "%s - %s" % (ip_range.from_address, ip_range.to_address)
                else:
                    value = ip_range.from_address
                field_values.append(value)
                field_value_dict[ip_range.source] = field_values
            
            config_spec.set_value("client_ip_ranges", ", ".join(field_value_dict.get(database_schema.CLIENT_SOURCE, [])))
            config_spec.set_value("server_ip_ranges", ", ".join(field_value_dict.get(database_schema.SERVER_SOURCE, [])))
            
        return config_spec

#        count = 0
#        if address:
#            config_spec.set_values_from_object(address)
#            for ip_range in address.ip_ranges:
#                field_name = get_ip_range_field_name(count)
#                config_spec.add_field(field_name, 'IP Range %d' % (count+1), ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
#                if ip_range.to_address:
#                    field_value = "%s - %s" % (ip_range.from_address, ip_range.to_address)
#                else:
#                    field_value = ip_range.from_address
#                config_spec.set_value(field_name, field_value)
#                count += 1
#        empty_slots = (3-count) if count < 3 else 1
#        for i in range(0,empty_slots):
#            field_name = get_ip_range_field_name(count)
#            config_spec.add_field(field_name, 'IP Range %d' % (count+1), ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
#            count += 1
        
    
    def _check_constraints(self, new_address, db_session):
        addresses = db_session.select(database_schema.AddressElement, database_schema.AddressElement.name==new_address.name)
        if len(addresses)>1:
            raise OperationNotAllowedException("An IP Range with this name already exists")
    
    def _get_ip_address_object(self, address):
        try:
            return IP(address)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginIpAddress._get_ip_address_object", self.plugin_name, lib.checkpoint.WARNING, etype, evalue, etrace)
            raise OperationNotAllowedException("Invalid IP Range '%s'" % address)
    

    def _update_range_value(self, ip_range, source, new_range):
        values = [s.strip() for s in new_range.split("-")]
        ip_range.source = source
        if len(values)==1:
            range_value = self._get_ip_address_object(values[0])
            ip_range.from_address = range_value.strCompressed()
            ip_range.to_address = None
        elif len(values)==2:
            from_address = self._get_ip_address_object(values[0])
            to_address = self._get_ip_address_object(values[1])
            ip_range.from_address = from_address.strCompressed()
            ip_range.to_address = to_address.strCompressed()
            
        else:
            raise OperationNotAllowedException("Invalid IP Range '%s'" % new_range)

    def _update_obj_with_template(self, new_address, template, transaction):
#        PluginTypeConfig.self._update_obj_with_template(new_address, template, transaction)
        config_spec = ConfigTemplateSpec(template)
        if not config_spec.get_value("name"):
            raise OperationNotAllowedException("Name cannot be empty")
        row = config_spec.get_field_value_dict()
        self._update_obj_with_row(new_address, row, transaction)
        self._check_constraints(new_address, transaction)
        count = 0
        client_ip_range_values = row.get("client_ip_ranges")
        address_list = []
        if client_ip_range_values:
            address_list = [(database_schema.CLIENT_SOURCE, s.strip()) for s in client_ip_range_values.split(",") if s.strip()]
        server_ip_range_values = row.get("server_ip_ranges")
        if server_ip_range_values:
            address_list.extend([(database_schema.SERVER_SOURCE, s.strip()) for s in server_ip_range_values.split(",") if s.strip()])
        if not address_list:
            raise OperationNotAllowedException("No IP Ranges specified")
        for ip_range in new_address.ip_ranges:
            try:
                source, new_range = address_list.pop(0)
            except IndexError:
                transaction.delete(ip_range)
                continue
            self._update_range_value(ip_range, source, new_range)
        for source, new_range in address_list:
            ip_range = database_schema.IpRange()
            self._update_range_value(ip_range, source, new_range)
            new_address.ip_ranges.append(ip_range)
             
            
        
        
        
    
    
    def config_create_row(self, element_type, template, dbt):
        #new_key =  dbt.add(Token(**row))
        new_address = dbt.add(database_schema.AddressElement())
        self._update_obj_with_template(new_address, template, dbt)
        dbt.flush()
        
        return "%s.%s" % (self.plugin_name, new_address.id) 
            
    
    def config_update_row(self, element_type, template, dbt):
        config_spec = ConfigTemplateSpec(template)
        address = self._lookup_address(dbt, config_spec.get_value("id"))
        if address:
            self._update_obj_with_template(address, template, dbt)
            return config_spec.get_value("name")
        else:
            raise Exception("The IP Address was not found in the database")
    
    def config_delete_row(self, element_type, element_id, dbt):
        address = self._lookup_address(dbt, element_id)
        if address:
            for ip_range in address.ip_ranges:
                dbt.delete(ip_range)
            dbt.delete(address)
            return True
        return False


    def _lookup_address(self, dbs, address_id):
        return dbs.get(database_schema.AddressElement, address_id)


def get_ip_range_field_name(count):
    return "ip_address_%d" % count
