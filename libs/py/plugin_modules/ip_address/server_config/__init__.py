"""
Upgrade plugin  

"""
from __future__ import with_statement

import lib.checkpoint

from plugin_types.server_config import plugin_type_upgrade


class Upgrade(plugin_type_upgrade.PluginTypeUpgrade):
    def __init__(self, checkpoint_handler, server_configuration_all):
        plugin_type_upgrade.PluginTypeUpgrade.__init__(self, checkpoint_handler, server_configuration_all, __name__, "ip_address")

    def import_schema(self):
        import plugin_modules.ip_address.server_common.database_schema 

        