"""
Unittest of certificate plugin module
"""
import os
import os.path

import sys
import shutil
import tempfile
import datetime, time

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility
import lib.hardware.device

import components.database.server_common.database_api as database_api

from components.database.server_common.connection_factory import ConnectionFactory
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from components.auth.server_management import OperationNotAllowedException

import plugin_modules.ip_address.server_gateway
import plugin_modules.ip_address.server_management
from plugin_modules.ip_address.server_common import database_schema as database_model




class SessionInfoStub(object):

    def __init__(self):
        self.socket_info_remote = None
        self.socket_info_local = None

    def set_client_ip(self, address):
        self.socket_info_remote = [address]

    def set_server_ip(self, address):
        self.socket_info_local = [address]


class AuthCallbackStub(object):

    def plugin_event_state_change(self, name, info=[], internal_type=None):
        pass

class Auth_plugin_test(unittest.TestCase):

    def setUp(self):
        self.db_filename = giri_unittest.mk_temp_file()
        self.db = ConnectionFactory(None).create_connection('sqlite:///%s' % self.db_filename)
        database_model.connect_to_database_using_connection(self.db)



    def tearDown(self):
        self.db.dispose()
        try:
            os.remove(self.db_filename)
        except:
            pass

    def test_mgmt0(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        n = 0

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test1")
        config_spec.set_value("client_ip_ranges", "127.0.0.1")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 1)
        element_id = elements[0].get_internal_id()

        template = plugin.config_get_template(None, element_id, dbt)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("server_ip_ranges", "192.168.0.0/24")

        with database_api.Transaction() as dbt:
            plugin.config_update_row(None, template, dbt)


        elements = plugin.get_specific_elements(None, [element_id])
        self.assertEqual(len(elements), 1)

        with database_api.Transaction() as dbt:
            plugin.config_delete_row(None, element_id, dbt)


    def test_mgmt1(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_create_row(None, template, dbt) )
        dbt.close()

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test1")
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_create_row(None, template, dbt) )
        dbt.close()

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("client_ip_ranges", "127.0.0.1")
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_create_row(None, template, dbt) )
        dbt.close()

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test1")
        config_spec.set_value("client_ip_ranges", "abc")
        dbt = database_api.Transaction()
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_create_row(None, template, dbt) )
        dbt.close()

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test1")
        config_spec.set_value("client_ip_ranges", "127.0.0.1")
        dbt = database_api.Transaction()
        plugin.config_create_row(None, template, dbt)
        dbt.commit()

        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 1)
        element_id = elements[0].get_internal_id()

        dbt = database_api.Transaction()
        template = plugin.config_get_template(None, element_id, dbt)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "")
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_update_row(None, template, dbt) )
        config_spec.set_value("name", "yo")
        config_spec.set_value("client_ip_ranges", "abc")
        self.assertRaises(OperationNotAllowedException, lambda: plugin.config_update_row(None, template, dbt) )

        with database_api.Transaction() as dbt:
            plugin.config_delete_row(None, element_id, dbt)




    def test_gateway0(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "client")
        config_spec.set_value("client_ip_ranges", "192.16.0.192")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()
        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 1)
        address = (elements[0].get_internal_id(),  elements[0].get_label())

        session_info.set_client_ip("192.16.0.192")
        session_info.set_server_ip("192.16.0.192")

        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is True)

        session_info.set_client_ip("192.16.0.193")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        session_info.set_client_ip("192.16.0")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)


    def test_gateway1(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test0")
        config_spec.set_value("server_ip_ranges", "192.16.0.192")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 1)
        address = (elements[0].get_internal_id(),  elements[0].get_label())

        session_info.set_client_ip("192.16.0.192")
        session_info.set_server_ip("192.16.0.192")

        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is True)

        session_info.set_server_ip("192.16.0.193")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        session_info.set_server_ip("192.16.0")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

    def test_gateway2(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "test0")
        config_spec.set_value("client_ip_ranges", "192.16.0.192")
        config_spec.set_value("server_ip_ranges", "192.16.0.192")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        elements = plugin.get_elements(None)
        self.assertEqual(len(elements), 1)
        address = (elements[0].get_internal_id(),  elements[0].get_label())

        session_info.set_client_ip("192.16.0.192")
        session_info.set_server_ip("192.16.0.192")

        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is True)

        session_info.set_server_ip("192.16.0.193")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        session_info.set_server_ip("192.16.0.192")
        session_info.set_client_ip("192.16.0")
        value = plugin_gateway.check_predicate(address)
        self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

    def test_gateway3(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "8")
        config_spec.set_value("client_ip_ranges", "10/8")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "16")
        config_spec.set_value("client_ip_ranges", "10.0/16")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "24")
        config_spec.set_value("client_ip_ranges", "10.0.0/24")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        session_info.set_server_ip("192.16.0.192")
        elements = plugin.get_elements(None)

        session_info.set_client_ip("10.0.0.0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value)

        session_info.set_client_ip("10.0.0.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value)

        session_info.set_client_ip("10.0.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)<24))

        session_info.set_client_ip("10.255.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)<16))

        session_info.set_client_ip("11.0.0.0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        session_info.set_client_ip("9.255.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

    def test_gateway4(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "8")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.255.255.255")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "16")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.0.255.255")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "24")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.0.0.255")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        session_info.set_server_ip("192.16.0.192")
        elements = plugin.get_elements(None)

        session_info.set_client_ip("172.0.0.0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value)

        session_info.set_client_ip("172.0.0.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value)

        session_info.set_client_ip("172.0.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)<24))

        session_info.set_client_ip("172.255.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)<16))

        session_info.set_client_ip("173.0.0.0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        session_info.set_client_ip("171.255.255.255")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

    def test_gateway5(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "1")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.255.255.255")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "2")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.255.255.255, 10/8")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "3")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.255.255.255, 10/8, 192.168.0.0")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "4")
        config_spec.set_value("client_ip_ranges", "172.0.0.0-172.255.255.255, 10/8, 192.168.0.0, 2001:0db8:85a3:0000:0000:8a2e:0370:7334-2001:db8:85a3:0:0:8a2e:370:7336")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        session_info.set_server_ip("192.16.0.192")
        elements = plugin.get_elements(None)

        session_info.set_client_ip("172.1.1.2")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value)

        session_info.set_client_ip("10.12.14.16")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)>1))

        session_info.set_client_ip("192.168.0.0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)>2))

        session_info.set_client_ip("2001:0db8:85a3:0000:0000:8a2e:0370:7335")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)>3))

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)


    def test_gateway6(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "1")
        config_spec.set_value("client_ip_ranges", "2001:db8:85a3:0:0:8a2e:370:7334")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "2")
        config_spec.set_value("client_ip_ranges", "2001:0db8:85a3:0000:0000:8a2e:0370:7334")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "3")
        config_spec.set_value("client_ip_ranges", "2001:db8:85a3::8a2e:370:7334")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "4")
        config_spec.set_value("client_ip_ranges", "::1")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "5")
        config_spec.set_value("client_ip_ranges", "::")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "6")
        config_spec.set_value("client_ip_ranges", "::ffff:c000:280")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "7")
        config_spec.set_value("client_ip_ranges", "::ffff:192.0.2.128")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        session_info = SessionInfoStub()


        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        session_info.set_server_ip("192.16.0.192")
        elements = plugin.get_elements(None)

        session_info.set_client_ip("2001:db8:85a3:0:0:8a2e:370:7334")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)<4))

        session_info.set_client_ip("0:0:0:0:0:0:0:1")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)==4))

        session_info.set_client_ip("0:0:0:0:0:0:0:0")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)==5))

        session_info.set_client_ip("::ffff:192.0.2.128")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is (int(name)>5))

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

    def test_gateway7(self):
        plugin = plugin_modules.ip_address.server_management.PluginIpAddress(giri_unittest.get_checkpoint_handler(), None, None)
        elements = plugin.get_elements(None)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "1")
        config_spec.set_value("client_ip_ranges", "2001:db8:1234::/48")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)

        template = plugin.config_get_template(None, None)
        config_spec = ConfigTemplateSpec(template)
        config_spec.set_value("name", "2")
        config_spec.set_value("client_ip_ranges", "2001:db8:1234:0000:0000:0000:0000:0000-2001:db8:1234:ffff:ffff:ffff:ffff:ffff")
        with database_api.Transaction() as dbt:
            plugin.config_create_row(None, template, dbt)


        session_info = SessionInfoStub()

        plugin_gateway = plugin_modules.ip_address.server_gateway.PluginAuthentication(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), None, session_info, None, None, None)
        plugin_gateway.set_callback(AuthCallbackStub())
        plugin_gateway.start()

        session_info.set_server_ip("192.16.0.192")
        elements = plugin.get_elements(None)

        session_info.set_client_ip("2001:db8:1234:0000:0000:0000:0000:0000")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is True)


        session_info.set_client_ip("2001:db8:1234:ffff:ffff:ffff:ffff:ffff")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is True)

        session_info.set_client_ip("2001:db8:1235:0000:0000:0000:0000:0000")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        session_info.set_client_ip("2001:db8:1233:ffff:ffff:ffff:ffff:ffff")
        for e in elements:
            name = e.get_label()
            address = (e.get_internal_id(),  e.get_label())
            value = plugin_gateway.check_predicate(address)
            self.assertTrue(value is False)

        elements = plugin.get_elements(None)
        for e in elements:
            with database_api.Transaction() as dbt:
                plugin.config_delete_row(None, e.get_internal_id(), dbt)

#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
