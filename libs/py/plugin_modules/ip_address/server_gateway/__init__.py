"""
Gateway Server
"""
from __future__ import with_statement
import sys
from IPy import IP

import lib.checkpoint
from plugin_types.server_gateway import plugin_type_auth
from plugin_modules.ip_address.server_common import database_schema 
from components.database.server_common import database_api
    
class PluginAuthentication(plugin_type_auth.PluginTypeAuth):

    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, u'ip_address', license_handler, session_info, database, management_message_session, access_log_server_session)

    def start(self):
        # Get IP address some way...
        self._address_map = None
        self._session = None
        self.set_ready()
        
    def get_address(self, address_id):
        if not self._session:
            self._address_map = dict()
            self._session = database_api.ReadonlySession()
            address_list = self._session.select(database_schema.AddressElement)
            for address in address_list:
                self._address_map[address.id] = address
        return self._address_map.get(int(address_id))
                

    def _check_range(self, ip_range, source_ip_address_str):
        source_ip_address = IP(source_ip_address_str)
        if ip_range.to_address:
            from_ip = IP(ip_range.from_address)
            to_ip = IP(ip_range.to_address)
            ok = from_ip <= source_ip_address <= to_ip
        else:
            ip_net = IP(ip_range.from_address)
            ok = source_ip_address in ip_net
        return ok


    def _check_element(self, param, internal_type=None):
        address_id  = param[0]
        address_label = param[1]

        self.checkpoint_handler.Checkpoint('in_range', self.plugin_name, lib.checkpoint.DEBUG, address_id=address_id, address_label=address_label)
        address = self.get_address(address_id)
        if address:
            client_ok = None
            server_ok = None
            for ip_range in address.ip_ranges:
                
                if ip_range.source == database_schema.CLIENT_SOURCE:
                    if client_ok:
                        continue
                    source_ip_address_str = self.session_info.socket_info_remote[0]
                    client_ok = self._check_range(ip_range, source_ip_address_str)
                elif  ip_range.source == database_schema.SERVER_SOURCE:
                    if server_ok:
                        continue
                    source_ip_address_str = self.session_info.socket_info_local[0]
                    server_ok = self._check_range(ip_range, source_ip_address_str)
                else:
                    self.checkpoint_handler.Checkpoint('in_range', self.plugin_name, lib.checkpoint.ERROR, address_id=address_id, address_label=address_label, msg="Unknown IP address source", source=address.source)

                if client_ok and server_ok:
                    return True
                
            if client_ok and server_ok:
                return True
            if client_ok is None:
                return server_ok is True
            if server_ok is None:
                return client_ok is True
            return False
        self.checkpoint_handler.Checkpoint('in_range', self.plugin_name, lib.checkpoint.ERROR, address_id=address_id, address_label=address_label, msg="Unable to find IP Range in database")
        return False
                    
        