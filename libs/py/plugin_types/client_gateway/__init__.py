"""
Common functionality for all Gateway Client plugins
"""
from plugin_types import common


class PluginTypeGatewayClient(common.PluginType):
    """
    Base class for all Client Gateway plugins
    """
    def __init__(self, async_service, checkpoint_handler, plugin_name, user_interface, additional_device_roots):
        common.PluginType.__init__(self, checkpoint_handler, plugin_name)
        self.async_service = async_service
        self.user_interface = user_interface
        self.plugin_name = plugin_name
        self.additional_device_roots = additional_device_roots
        
