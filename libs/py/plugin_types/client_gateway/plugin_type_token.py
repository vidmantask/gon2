"""
The base class and common functionality for client Gateway token plugins.
"""
import os.path


from plugin_types import client_gateway




class Token(object):
    """
    Object representation of token
    """
    TOKEN_STATUS_INITIALIZED = 0
    TOKEN_STATUS_ENROLLED = 1
    TOKEN_STATUS_BURNED = 2
    TOKEN_STATUS_TEXTS = ['Initialized', 'Enrolled', 'Burned']

    TOKEN_DISTANCE_CURRENT = 0
    TOKEN_DISTANCE_CLOSE = 1
    TOKEN_DISTANCE_OTHER = 2
    TOKEN_DISTANCE_UNKNOWN = 3
     
    def __init__(self, token_type, token_id, token_status, runtime_env_id, token_type_title=None, token_title=None, token_plugin_module_name=None, token_internal_type=None):
        self.token_type = token_type
        self.token_internal_type = token_internal_type
        self.token_id = token_id
        self.token_status = token_status
        self.token_serial = None
        self.runtime_env_id = runtime_env_id
        self.token_title = token_title
        self.token_type_title = token_type_title
        self.token_plugin_module_name = token_plugin_module_name
        self.token_public_key = None
        self.token_is_endpoint = token_type=="endpoint_token"
        self.token_distance = Token.TOKEN_DISTANCE_UNKNOWN

    def get_token_status_text(self):
        return Token.TOKEN_STATUS_TEXTS[self.token_status]

    def get_token_title(self):
        if self.token_title is not None:
            return self.token_title
        token_type_title = self.get_token_type_title()
        return "%s (%s)" % (self.token_id, token_type_title)

    def get_token_type_title(self):
        token_type_title = self.token_type_title
        if token_type_title is None:
            token_type_title = self.token_type
        return token_type_title

    def to_dict(self):
        ret_val = dict()
        ret_val["token_id"] = self.token_id
        ret_val["token_type"] = self.token_type
        ret_val["token_internal_type"] = self.token_internal_type
        ret_val["token_status"] = self.token_status
        ret_val["token_serial"] = self.token_serial
        ret_val["runtime_env_id"] = self.runtime_env_id
        ret_val["token_title"] = self.get_token_title()
        ret_val["token_type_title"] = self.token_type_title
        ret_val["token_public_key"] = self.token_public_key
        ret_val["token_is_endpoint"] = self.token_is_endpoint
        return ret_val

    def get_image_filename(self, image_path=None, size_id='32x32'):
        return Token.get_token_image_filename(self.token_plugin_module_name, self.token_status, image_path, size_id)

    def calculate_distance(self, runtime_env):
        if self.runtime_env_id is None:
            self.token_distance = Token.TOKEN_DISTANCE_CLOSE
        elif self.runtime_env_id == runtime_env.get_info().get_id():
            self.token_distance = Token.TOKEN_DISTANCE_CURRENT
        else:
            self.token_distance = Token.TOKEN_DISTANCE_OTHER
        
    @classmethod
    def compare_distance(cls, token_a, token_b):
        if token_a.token_distance == token_b.token_distance:
            return 0
        elif token_a.token_distance < token_b.token_distance:
            return -1
        else:
            return 1

    @classmethod
    def sort_by_distance(cls, tokens, client_runtime_env):
        for token in tokens:
            token.calculate_distance(client_runtime_env)
        tokens.sort(cmp=Token.compare_distance)


    @classmethod
    def get_token_image_filename(cls, token_plugin_module_name, token_status, image_path=None, size_id='32x32'):
        filename = 'g_%s' % token_plugin_module_name
    
        if token_status in [Token.TOKEN_STATUS_ENROLLED]:
            filename += '_marked'
        filename += '_%s.bmp' % size_id
    
        if image_path is not None:
            filename_abs = os.path.join(image_path, filename) 
            if not os.path.exists(filename_abs):  
                filename = 'giritech_%s.bmp' % size_id
        return filename 


class Error(Exception):
    """
    Raise this excetption in case of an error in the plugin
    """
    def __init__(self, message):
        self._message = message

    def __str__(self):
        return self._message



class PluginTypeToken(client_gateway.PluginTypeGatewayClient):
    """
    Base class for all Gateway client token plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, user_interface, additional_device_roots):
        client_gateway.PluginTypeGatewayClient.__init__(self, async_service, checkpoint_handler, plugin_name, user_interface, additional_device_roots)

    def get_tokens(self, additional_device_roots):
        """
        Expected to return a list Token objects of currently attatched tokens.
        """
        raise NotImplementedError

    def initialize_token(self, device):
        """
        Expected to do the initial deploy of a token. The device is a reference to the device that holds the token. 
        A new serial should be generated, unles serial is static for the token.
        To initialize deployment means to make the token ready for the GOn system.
        """
        raise NotImplementedError

    def deploy_token(self, token_id, client_knownsecret, servers):
        """
        Expected to deploy the token referenced by token_id. token_id is from a priviosly call to get_tokens().

        To deploy a token means that the token should be 'personalized' to the current GOn installation.
        """
        raise NotImplementedError
    
    def generate_keypair(self, token_id):
        """
        Generate the keypair on the token, where the public_key should be returned get_public_key().
        """
        raise NotImplementedError
    
    def get_public_key(self, token_id):
        """
        Get the public key from the token. 
        If it do not make sense to return a public-key for the token then the plugin should return None
        """
        raise NotImplementedError

    def set_serial(self, token_id, serial):
        """
        Set the serial of the token. It might not be possible to set the serial, due to the
        nature of the token. 
        """
        raise NotImplementedError

    def get_serial(self, token_id):
        """
        Get the serial from the token. 
        """
        raise NotImplementedError

    def set_enrolled(self, token_id):
        """
        Set the status of the token to be enrolled. 
        """
        raise NotImplementedError
        
    def reset_enrolled(self, token_id):
        """
        Set the status of the token not to be enrolled. 
        """
        raise NotImplementedError

    def is_enrolled(self, token_id):
        """
        Get the enrolled status of the token. 
        """
        raise NotImplementedError

    def get_knownsecret_and_servers(self, token_id):
        """
        Expected to return the knownsecret found in one of the devices
        
        If device is not recognized the plugin, the None should be returned
        """
        raise NotImplementedError

    def set_servers(self, token_id, servers):
        """
        Set the servers infomation 

        Expected to return None if the servers was set, and a errormesagge if the set operation failed
        """
        raise NotImplementedError
    
    def check_access(self, token_id):
        """
        Check to see if there is access to the token  
        """
        raise NotImplementedError
        

