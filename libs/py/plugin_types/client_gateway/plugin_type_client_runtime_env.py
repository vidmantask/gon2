"""
The base class and common functionality for client runtime environment plugins. on the client Gateway level.
"""
from __future__ import with_statement
from plugin_types import client_gateway

import os
import os.path
import sys
import tempfile
import shutil

import lib.checkpoint
import lib.hardware.device

import lib.gpm.gpm_builder
import lib.gpm.gpm_installer
import lib.gpm.gpm_installer_base
import lib.gpm.post_hook

import lib.appl.in_use_mark

import lib.appl.gon_client
import lib.appl.settings


MODULE_ID = "plugin_type_client_runtime"

class ClientRuntimeEnvInstallCB(lib.gpm.gpm_installer_base.GpmInstallerCB):
    def client_runtime_env_install_cb_restart(self, restart_gon_client_install_state):
        pass

class Info(object):
    """
    Info object about a client runtime env
    """
    def __init__(self, type, id, title):
        self.type = type
        self.id = id
        self.title = title
 
    def get_id(self):
        return u"%s::%s" % (self.type, self.id) 


class PluginTypeClientRuntimeEnv(client_gateway.PluginTypeGatewayClient):
    """
    Base class for all runtime client token plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, user_interface, devices):
        client_gateway.PluginTypeGatewayClient.__init__(self, async_service, checkpoint_handler, plugin_name, user_interface, devices)
        self.client_runtime_env_root = None
   
    def set_client_runtime_env_root(self, client_runtime_env_root):
        self.client_runtime_env_root = client_runtime_env_root
   
    def get_runtime_env_ids(self):
        """
        Return a list of client_runtime_env ids available from this plugin. 
        """
        raise NotImplementedError

    def get_instance(self, runtime_env_id):
        """
        Return the instance. 
        """
        raise NotImplementedError
    
    def create_instance(self, checkpoint_handler, root):
        return None


class PluginTypeClientRuntimeEnvInstance(object):
    SETTINGS_FILENAME = 'gon_settings'
    UNITTEST_MARK_FILENAME = 'gon_unittest'

    def force_to_current(self):
        """
        Tell this instance that it has be chosen to be the current.
        """
        raise NotImplementedError
    
    def get_root(self):
        raise NotImplementedError
        
    def set_root(self, root):
        """
        Set the folder root for all envs of this plugin
        """
        raise NotImplementedError
   
    def get_info(self):
        """
        Return info object about the client runtime environment
        """
        raise NotImplementedError
    
    def is_current(self):
        """
        Expected to return true if this client runtime environment is the one that is currently being used.
        Only one plugin is expected to be the current one
        """
        raise NotImplementedError

    def is_current_relocated(self, installation_root):
        """
        Expected to return true if this client runtime environment is the one that is currently being used, 
        but currently  is running relocated. This happends during upgrade of gon_client.
        Only one plugin is expected to be the current one
        """
        raise NotImplementedError

    def is_in_use(self, timeout=None):
        """
        Expected to return true if this client runtime environment is in use at the moment.
        The timeout attribute indicate when a mark is no longer valid
        """
        raise NotImplementedError

    def mark_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return true.
        """
        raise NotImplementedError

    def mark_not_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return false.
        """
        raise NotImplementedError

    def support_download(self, size, dictionary):
        """
        Expected to return information about support for  download of packages, if True is returned
        the method 'get_download_root' is expected to return a valid folder with rw-rights, and has room for size bytes of data.

        Expected to return a dict with the following content:
          {'supported': bool, 'message' : String}
        """
        raise NotImplementedError

    def set_download_root(self, download_root): 
        """
        Used to set the download root to a differet location that the default used by the runtime env. 
        This is used when using a download_cache for installation of many runtime env's.
        """
        raise NotImplementedError

    def get_download_root(self):
        """
        See 'support_download'
        """
        raise NotImplementedError

    def support_install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gon_client_install_state, dictionary, knownsecret, servers):
        """
        Examine if the given packages can be installed. The gpms_meta holds meta-info for the listed packages.
        If servers and/or knownsecret is None the fields should not be updated
        
        Expected to return a dict with the following content:
          {'supported': bool, 'require_restart' : bool, 'message': String}
        """
        raise NotImplementedError

    def support_bootification(self, dictionary):
        """
        Examine if the given runtime env support bootification.

        Expected to return a tupple:
          (bool, error_message)
        """
        raise NotImplementedError

    def install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gpm_installer_cb, install_state, error_handler, dictionary, knownsecret, servers):
        """
        Expected to install the given packages. The install_state holds the data given in the 'install_restart' callback, and None if not called during restart.
        If servers and/or knownsecret is None the field shoul not be updated
        """
        raise NotImplementedError

    def install_gpms_clean(self, gpm_ids_install, gpm_installer_cb, error_handler, dictionary, knownsecret, servers, generate_keypair):
        """
        Expected to make a clean installation of the given packages. 
        """
        raise NotImplementedError

    def get_installed_gpm_meta_all(self, error_handler):
        raise NotImplementedError

    def get_installed_gpm_meta(self, gpm_id, error_handler):
        raise NotImplementedError

    def install_gpm_meta(self, gpm_filename, error_handler):
        raise NotImplementedError

    def remove_installed_gpm_meta(self, gpm_id, error_handler):
        raise NotImplementedError

    def get_ro_root(self):
        raise NotImplementedError

    def get_rw_root(self):
        raise NotImplementedError

    def get_temp_root(self):
        raise NotImplementedError

    def get_download_gpm_filename(self, gpm_id):
        raise NotImplementedError
    
    def support_update_connection_info(self, dictionary):
        """
        Examine if the connection info can be updated.
        
        Expected to return a dict with the following content:
          {'supported': bool, 'supported_by_installation':bool, 'message': String}
        """
        raise NotImplementedError()

    def update_connection_info(self, servers, knownsecret, error_handler):
        """
        Expected to update the connection info
        """
        raise NotImplementedError()

    def get_connection_info(self):
        """
        Expected to return connection information in the form (knownsecret, servers)
        """
        raise NotImplementedError()
        
    def is_unittest(self):
        """
        Expected to return true if this client runtime environment is marked for unittest only
        """
        unittest_filename_mark = os.path.join(self.get_rw_root(), 'gon_client', PluginTypeClientRuntimeEnvInstance.UNITTEST_MARK_FILENAME)
        return os.path.exists(unittest_filename_mark)

    def mark_as_unittest(self):
        unittest_filename_mark = os.path.join(self.get_rw_root(), 'gon_client', PluginTypeClientRuntimeEnvInstance.UNITTEST_MARK_FILENAME)
        unittest_file_mark = open(unittest_filename_mark, 'w')
        unittest_file_mark.write('x')
        unittest_file_mark.close()
        
    def get_settings_filename(self):
        """
        Expected to return filename of settings. None is returned if settings is not supported
        """
        return os.path.join(self.get_rw_root(), 'gon_client', PluginTypeClientRuntimeEnvInstance.SETTINGS_FILENAME)

    def load_settings(self):
        return lib.appl.settings.Settings.create_from_file(self.get_settings_filename())
        
    def save_settings(self, settings):
        return settings.save_to_file(self.get_settings_filename())
        
    def get_settings_value(self, key, default_value=None):
        return self.load_settings().get_value(key, default_value)
    
    def get_settings_value_int(self, key, default_value=None):
        return self.load_settings().get_value_int(key, default_value)

    def get_settings_value_bool(self, key, default_value=None):
        return self.load_settings().get_value_bool(key, default_value)
    
    def set_settings_value(self, key, value):
        settings = self.load_settings()
        settings.set_value(key, value)
        self.save_settings(settings)
        

class PluginTypeClientRuntimeEnvInstanceRWFileSystemBase(PluginTypeClientRuntimeEnvInstance):
    """
    This is a base class holding common functionlity for runtime environments using a rw-filesystem,
    eg. certificate or mobile-security-card 
    """
    def __init__(self, checkpoint_handler, token_type, token_id, token_title, clean_skip_folders):
        self.checkpoint_handler = checkpoint_handler
        self._token_type = token_type
        self._token_id = token_id
        self._token_title = token_title
        self.set_root(self._token_id)
        self._forced_to_current = False
        self.clean_skip_folders = clean_skip_folders

    def force_to_current(self):
        """
        Tell this instance that it has be chosen to be the current.
        """
        self._forced_to_current = True

    def get_info(self):
        """
        Return info object about the client runtime environment
        """
        info = Info(self._token_type, self._token_id, self._token_title)
        return info

    def set_root(self, root):
        """
        Set the folder root for all envs of this plugin
        """
        self._runtime_root = root
        self._ro_root = self._runtime_root
        self._rw_root = self._runtime_root
        self._gpm_meta_root = os.path.join(self._runtime_root, 'gon_client' ,'gpm_meta')
        self._download_root = os.path.join(self.get_temp_root(), 'download_cache')
    
    def get_root(self):
        return self._runtime_root
       
    def set_download_root(self, download_root): 
        self._download_root = download_root

    def is_current(self):
        """
        Expected to return true if this client runtime environment is the one that is currently being used.
        Only one plugin is expected to be the current one
        """
        if self._runtime_root is None:
            return self._forced_to_current
        current_installation_root_normalized = os.path.normpath(lib.appl.gon_client.get_installation_root())
        runtime_root_normalized = os.path.normpath(self._runtime_root)
        self.checkpoint_handler.Checkpoint("is_current", MODULE_ID, lib.checkpoint.DEBUG, token_type=self._token_type, current_installation_root_normalized=repr(current_installation_root_normalized), runtime_root_normalized=repr(runtime_root_normalized))

        #should use os.path.samefile but it only works on linux
        if os.path.normcase(current_installation_root_normalized) == os.path.normcase(runtime_root_normalized): 
            return True
        
        return self._forced_to_current

    def is_current_relocated(self, installation_root):
        """
        Expected to return true if this client runtime environment is the one that is currently being used, 
        but currently  is running relocated. This happends during upgrade of gon_client.
        Only one plugin is expected to be the current one
        """
        restart_state = lib.appl.gon_client.get_restart_state(self)
        if restart_state is not None:
            plugin_data = restart_state.get_plugin_data() 
            if plugin_data is not None and plugin_data.has_key('certificate_data'):
                certificate_data = plugin_data['certificate_data']
                if certificate_data.has_key('cleanup_root'):
                    cleanup_root = certificate_data['cleanup_root']
                    if os.path.normpath(cleanup_root) == os.path.normpath(installation_root):
                        return True
        return False

    def support_download(self, size, dictionary):
        if not self.is_current():
            return {'supported':False, 'message': dictionary._('This runtime environment is not the current one')}
        if not os.path.exists(self._download_root):
            os.mkdir(self._download_root)
        if not os.access(self._download_root, os.W_OK):
            return {'supported':False, 'message':dictionary._('No write-access to the folder %s') % self._download_root}
        free_space = lib.hardware.device.Device.get_free_space(self._download_root)
        if free_space != None and free_space < size: 
            return {'supported':False, 'message':dictionary._('Not enough space on device to download and install packages')}
        return {'supported':True}

    def get_download_root(self):
        return self._download_root

    def _is_install_state_none(self, gon_client_install_state):
        if gon_client_install_state == None:
            return True
        plugin_data = gon_client_install_state.get_plugin_data()
        return plugin_data == None or not plugin_data.has_key('certificate_data')

    def _is_install_state_relocated(self, gon_client_install_state):
        if gon_client_install_state == None:
            return False
        plugin_data = gon_client_install_state.get_plugin_data()
        if plugin_data != None and plugin_data.has_key('certificate_data'):
            certificate_data = plugin_data['certificate_data']
            if certificate_data.has_key('state') and  certificate_data['state']=='relocated':
                return True
        return False
    
    def _is_install_state_cleanup(self, gon_client_install_state):
        if gon_client_install_state == None:
            return False
        plugin_data = gon_client_install_state.get_plugin_data()
        if plugin_data != None and plugin_data.has_key('certificate_data'):
            certificate_data = plugin_data['certificate_data']
            if certificate_data.has_key('state') and  certificate_data['state']=='cleanup':
                return True
        return False

    def _set_install_state(self, state, org_ro_root, org_rw_root, cleanup_root=None):
        restart_gon_client_install_state = lib.appl.gon_client.GOnClientInstallState()
        certificate_data = {'state': state, 
                            'org_ro_root' : org_ro_root,
                            'org_rw_root' : org_rw_root,
                            'cleanup_root' : cleanup_root
                            }
        plugin_data = {'certificate_data': certificate_data}
        restart_gon_client_install_state.set_plugin_data(plugin_data)
        return restart_gon_client_install_state

    def _get_install_state_data(self, gon_client_install_state):
        if gon_client_install_state == None:
            return (None, None, None)
        plugin_data = gon_client_install_state.get_plugin_data()
        return (plugin_data['certificate_data']['org_ro_root'],
                plugin_data['certificate_data']['org_rw_root'],
                plugin_data['certificate_data']['cleanup_root'])

    def support_bootification(self, dictionary):
        return (False, dictionary._('Bootification not supported on this token type'))
    
    def support_install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, gon_client_install_state, dictionary, knownsecret, servers):
        if self._is_install_state_none(gon_client_install_state):
            
            if lib.gpm.post_hook.is_bootification_enabled(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary):
                (supported, error_message) = self.support_bootification(dictionary)
                if not supported:
                    return {'supported': False, 'message': error_message}
                    
                (possible, error_message) = lib.gpm.post_hook.is_bootification_possible(gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, dictionary)
                if not possible:
                    return {'supported': False, 'message': error_message}
            
            require_restart = lib.appl.gon_client.contains_current_instance(gpm_ids_update) != None
            if not os.access(self._ro_root, os.W_OK):
                return {'supported': False, 'message': dictionary._("No write-access to the folder '%s'") % self._ro_root}
            if not os.access(self._rw_root, os.W_OK):
                return {'supported': False, 'message': dictionary._("No write-access to the folder '%s'") % self._rw_root}
            return {'supported': True, 'message': '', 'require_restart':require_restart}
        elif self._is_install_state_relocated(gon_client_install_state):
            require_restart = True
            return {'supported': True, 'message': '', 'require_restart':require_restart}
        elif self._is_install_state_cleanup(gon_client_install_state):
            require_restart = True
            return {'supported': True, 'message': '', 'require_restart':require_restart}
        else:
            require_restart = False
            return {'supported': False, 'message': dictionary._('Unexpected installation state'), 'require_restart':require_restart}
    
    def install_gpms(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpms_meta, cb, gon_client_install_state, error_handler, dictionary, knownsecret, servers):
        if self._is_install_state_none(gon_client_install_state):
            if not (servers is None and knownsecret is None):
                knownsecret_current, servers_current = self.get_knownsecret_and_servers()
                if servers is None:
                    servers = servers_current
                if knownsecret is None:
                    knownsecret = knownsecret_current
                self.set_knownsecret_and_servers(knownsecret, servers, False)
            gon_client_update_package_id = lib.appl.gon_client.contains_current_instance(gpm_ids_update)
            if gon_client_update_package_id == None:
                task_cleanup = lib.gpm.gpm_installer.GpmInstallerTaskCleanup([self.get_download_root()], [], dictionary._('Cleaning up temporary files'), cb, error_handler, dictionary)
                installer = lib.gpm.gpm_installer.GpmInstaller(self, self._ro_root, self._rw_root, error_handler, dictionary, cb)
                installer.init_tasks(gpm_ids_install, gpm_ids_update, gpm_ids_remove, [], [task_cleanup])
                installer.execute()
            else:
                temp_root = tempfile.mkdtemp()
                installer = lib.gpm.gpm_installer.GpmInstaller(self, temp_root, temp_root, error_handler, dictionary, cb)
                installer.init_tasks([gon_client_update_package_id], [], [])
                installer.execute()
                restart_gon_client_install_state = self._set_install_state('relocated', self._ro_root, self._rw_root, temp_root)
                restart_gon_client_install_state.set_restart_root(temp_root)
                restart_gon_client_install_state.set_offline_operation(True)
                if not error_handler.error():
                    cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state)
        elif self._is_install_state_relocated(gon_client_install_state):
            (org_ro_root, org_rw_root, cleanup_root) = self._get_install_state_data(gon_client_install_state)
            installer = lib.gpm.gpm_installer.GpmInstaller(self, org_ro_root, org_rw_root, error_handler, dictionary, cb)
            installer.init_tasks(gpm_ids_install, gpm_ids_update, gpm_ids_remove)
            installer.execute()
            restart_gon_client_install_state = self._set_install_state('cleanup', org_ro_root, org_rw_root, cleanup_root)
            restart_gon_client_install_state.set_offline_operation(True)
            if not error_handler.error():
                cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state)
        elif self._is_install_state_cleanup(gon_client_install_state):
            (org_ro_root, org_rw_root, cleanup_root) = self._get_install_state_data(gon_client_install_state)

            cleanup_roots = [cleanup_root]
            download_root = self.get_download_root()
            if download_root != None:
                cleanup_roots.append(download_root)

            task_cleanup = lib.gpm.gpm_installer.GpmInstallerTaskCleanup(cleanup_roots, [], dictionary._('Cleaning up temporary files'), cb, error_handler, dictionary)
            installer = lib.gpm.gpm_installer.GpmInstaller(self, org_ro_root, org_rw_root, error_handler, dictionary, cb)
            installer.init_tasks([], [], [], [], [task_cleanup])
            installer.execute()
            
            restart_gon_client_install_state = lib.appl.gon_client.GOnClientInstallState(done=True)
            if not error_handler.error():
                cb.client_runtime_env_install_cb_restart(restart_gon_client_install_state)
        else:
            error_handler.emmit_error('Invalid restart state, try to run update again')

    def install_gpms_clean(self, gpm_ids_install, gpm_installer_cb, error_handler, dictionary, knownsecret, servers, generate_keypair):
        # gpm_ids to be removed must be found before changing root of runtime_env
        gpm_ids_remove = []
        try:
            gpm_ids_remove = lib.gpm.gpm_builder.query_meta_for_gpm_ids(self.get_installed_gpm_meta_all(error_handler), error_handler)
        except:
            self.error_handler.emmit_warning('Unexpected error looking for installed packages')

        cleanup_folders = [self.get_temp_root()]
        pre_task_cleanup = lib.gpm.gpm_installer.GpmInstallerTaskCleanup(cleanup_folders, [], 'Cleaning up', gpm_installer_cb, error_handler, dictionary)
        installer = lib.gpm.gpm_installer.GpmInstaller(self, self._ro_root, self._rw_root, error_handler, dictionary, gpm_installer_cb)
        installer.init_tasks(gpm_ids_install, [], gpm_ids_remove, [pre_task_cleanup], [])
        installer.execute()
        if knownsecret is not None and servers is not None:
            self.set_knownsecret_and_servers(knownsecret, servers, generate_keypair)

    def set_knownsecret_and_servers(self, knownsecret, servers, generate_keypair):
        """
        Intented for overwrite
        """
        pass

    def get_knownsecret_and_servers(self):
        """
        Intented for overwrite
        """
        return (None, None)

    def get_installed_gpm_meta_all(self, error_handler):
        return lib.gpm.gpm_builder.query_meta_all_from_files(self._gpm_meta_root, error_handler)

    def get_installed_gpm_meta(self, gpm_id, error_handler):
        return lib.gpm.gpm_builder.query_meta_from_from_file(self._gpm_meta_root, gpm_id, error_handler)

    def remove_installed_gpm_meta(self, gpm_id, error_handler):
        lib.gpm.gpm_builder.remove_meta(self._gpm_meta_root, gpm_id, error_handler)

    def install_gpm_meta(self, gpm_filename, error_handler):
        if not os.path.exists(self._gpm_meta_root):
            os.mkdir(self._gpm_meta_root)
        lib.gpm.gpm_builder.install_meta(self._gpm_meta_root, gpm_filename, error_handler)

    def get_download_gpm_filename(self, gpm_id):
        gpm_filename_abs = os.path.join(self._download_root, '%s.gpm' % gpm_id)
        return gpm_filename_abs
    
    def get_ro_root(self):
        return self._ro_root

    def get_rw_root(self):
        return self._rw_root

    def get_temp_root(self):
        temp_root = os.path.join(self.get_rw_root(), 'gon_client', 'gon_temp')
        if not os.path.exists(temp_root):
            os.makedirs(temp_root)
        return temp_root

    def is_in_use(self, timeout=None):
        """
        Expected to return true if this client runtime environment is in use at the moment.
        The timeout attribute indicate when a mark is no longer valid
        """
        temp_root = os.path.join(self.get_rw_root(), 'gon_client', 'gon_temp')
        if not os.path.exists(temp_root):
            return False
        
        in_use_mark = lib.appl.in_use_mark.InUseMark(temp_root)
        return in_use_mark.is_in_use(timeout)

    def mark_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return true.
        """
        try:
            in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
            in_use_mark.do_mark_in_use()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            print 'mark_in_use.exception', evalue
        
    def mark_not_in_use(self):
        """
        Mark this client runtime environment to be in use. After this call, is_in_use should return false.
        """
        try:
            in_use_mark = lib.appl.in_use_mark.InUseMark(self.get_temp_root())
            in_use_mark.do_mark_not_in_use()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            print 'mark_not_in_use.exception', evalue

    def support_update_connection_info(self, dictionary):
        """
        Examine if the connection info can be updated.
        
        Expected to return a dict with the following content:
          {'supported': bool, 'supported_by_installation':bool, 'message': String}
        """
        return {'supported':True, 'supported_by_installation':False, 'message':'Supported'}

    def update_connection_info(self, knownsecret, servers, error_handler):
        """
        Expected to update the connection info
        """
        self.set_knownsecret_and_servers(knownsecret, servers, False)

    def get_connection_info(self):
        """
        Expected to return connection information in the form (knownsecret, servers)
        """
        return self.get_knownsecret_and_servers()
