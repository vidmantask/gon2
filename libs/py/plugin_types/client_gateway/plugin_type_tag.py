"""
The base class and common functionality for client Gateway tag plugins.

A tag plugin's main functionlity is talk to its serverpart so that the serverpart can generate tags. 
"""
from __future__ import with_statement

from plugin_types import client_gateway
from components.communication import message
from components.communication import tunnel_endpoint_base 


class PluginTypeTag(client_gateway.PluginTypeGatewayClient, tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    """
    Base class for all Gateway client authorization plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, user_interface, devices):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler)
        client_gateway.PluginTypeGatewayClient.__init__(self, async_service, checkpoint_handler, plugin_name, user_interface, devices)

        