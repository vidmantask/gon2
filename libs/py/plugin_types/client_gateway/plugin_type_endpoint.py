"""
The base class and common functionality for client Gateway endpoint plugins.
"""
from __future__ import with_statement

import sys
import threading
import traceback

import lib.checkpoint
from plugin_types import client_gateway


class PluginTypeEndpointInfo(object):
    ATTR_NAME_OS_PLATFORM = 'os_platform'
    ATTR_NAME_OS_PLATFORM_GROUP = 'os_platform_group'
    ATTR_NAME_OS_DISTRIBUTION = 'os_distribution'
    ATTR_NAME_OS_SERIAL = 'os_serial'
    ATTR_NAME_OS_VERSION = 'os_version'
    ATTR_NAME_OS_VERSION_SERVICE_PACK = 'os_version_service_pack'
    ATTR_NAME_OS_AUTO_UPDATE = 'os_auto_update'
    ATTR_NAME_OS_UP_TO_DATE = 'os_up_to_date'
    ATTR_NAME_OS_MISSING_UPDATE = 'os_missing_update'
    
    ATTR_NAME_NET_IP = 'net_ip'
    ATTR_NAME_NET_MAC = 'net_mac'
    
    ATTR_NAME_ANTIVIRUS_COMPANY_NAME = 'antivirus_company_name'
    ATTR_NAME_ANTIVIRUS_PRODUCT_UP_TO_DATE = 'antivirus_product_up_to_date'
    ATTR_NAME_ANTIVIRUS_VERSION = 'antivirus_version'
    ATTR_NAME_ANTIVIRUS_COMPANY_TITLE = 'antivirus_company_title'

    ATTR_NAME_FIREWALL_COMPANY_NAME = 'firewall_company_name'
    ATTR_NAME_FIREWALL_ENABLED = 'firewall_enabled'
    ATTR_NAME_FIREWALL_VERSION = 'firewall_version'
    ATTR_NAME_FIREWALL_COMPANY_TITLE = 'firewall_company_title'
    
    ATTR_NAME_WSC_PROVIDER = 'wsc_provider_'
    
    
    ATTR_NAME_MACHINE_GMID_USER = 'machine_gmid_user'
    ATTR_NAME_MACHINE_GMID = 'machine_gmid'
    ATTR_NAME_MACHINE_NAME = 'machine_name'
    ATTR_NAME_MACHINE_UUID = 'machine_uuid'
    ATTR_NAME_MACHINE_DOMAIN = 'machine_domain'
    ATTR_NAME_MACHINE_WORKGROUP = 'machine_workgroup'
    ATTR_NAME_MACHINE_MANUFACTURE = 'machine_manufacture'
    ATTR_NAME_MACHINE_MODEL = 'machine_model'
    ATTR_NAME_MACHINE_SERIAL = 'machine_serial'
    ATTR_NAME_MACHINE_LOCAL_ACCOUNT = 'machine_local_account'
    
    def __init__(self, attribute_name, attribute_value):
        self.attribute_name = attribute_name
        self.attribute_value = attribute_value

    def __str__(self):
        return "(%s, %s)" % (self.attribute_name, self.attribute_value)
   
   
class IPluginTypeEndpointCallback(object):
    def report(self, endpoint_info):
        """
        The plugin can call this method to report a single endpoint_info
        """
        raise NotImplementedError

    def report_some(self, endpoint_infos):
        """
        The plugin can call this method to report a list of endpoint_info's
        """
        raise NotImplementedError
    
    def done_hash(self):
        """
        The plugin is expected to call this method when it has finished collecting information used in endpoint hash value
        """
        raise NotImplementedError
    

    def done(self):
        """
        The plugin is expected to call this method when it is finish collecting information
        """
        raise NotImplementedError
    
    
class PluginTypeEndpoint(client_gateway.PluginTypeGatewayClient, threading.Thread):
    """
    Base class for all Gateway client endpoint plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, user_interface, additional_device_roots):
        client_gateway.PluginTypeGatewayClient.__init__(self, async_service, checkpoint_handler, plugin_name, user_interface, additional_device_roots)
        threading.Thread.__init__(self, name=plugin_name)
        self.setDaemon(True)
        self._callback = None
        self._is_running = False

    def set_callback(self, callback):
        self._callback = callback

    def reset_callback(self):
        self._callback = None

    def run(self):
        try:
            self._is_running = True
            self.do_run()
        except:
            print traceback.print_exc()
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("PluginTypeEndpoint.run.error", self.plugin_name, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        self._is_running = False
        if self._callback is not None:
            self._callback.done()
            
    def done_hash(self):
        if self._callback:
            self._callback.done_hash()

    def is_running(self):
        return self._is_running

    def report(self, endpoint_info):
        if self._callback is not None and endpoint_info is not None:
            self._callback.report(endpoint_info)

    def report_some(self, endpoint_info):
        if self._callback is not None:
            self._callback.report_some(endpoint_info)
        

    def do_run(self):
        """
        Implement this method to do the collection and report for information
        """
        raise NotImplementedError
    
