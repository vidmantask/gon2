"""
The base class and common functionality for server Gateway authorization plugins.
"""
from __future__ import with_statement
from plugin_types import server_gateway

from components.communication import message
from components.communication import tunnel_endpoint_base 
from components.management_message.server_gateway.session_send import ManagementMessageSessionSender

import lib.checkpoint


ACCESS_LOG_INVALID_SERIAL_ID = 'auth:0001'
ACCESS_LOG_INVALID_SERIAL_SUMMARY = 'Serial not found'
ACCESS_LOG_INVALID_SERIAL_DETAILS = 'Requested serial not found in the database'

ACCESS_LOG_INVALID_SIGNATURE_ID = 'auth:0002'
ACCESS_LOG_INVALID_SIGNATURE_SUMMARY = 'Challenge verification failed'
ACCESS_LOG_INVALID_SIGNATURE_DETAILS = 'Challenge verification failed'



class PluginWaitingForConnectionException(Exception):
    pass

class PluginWaitingException(Exception):
    pass

class PluginTypeAuthCallback(object):
    """
    This abstract interface defines the auth plugin callback
    """
    def plugin_event_state_change(self, plugin_name):
        """
        With this event a plugin can tell its parent that its state has changed.
        """
        raise NotImplementedError 

class ManagmentMessageSender(object):
    """
    This class represent the interface to the Management Server
    """
    def __init__(self, plugin_name, management_message_session):
        self._management_message_sender = ManagementMessageSessionSender(plugin_name, management_message_session)
        
        
    def send_message(self, message_name, **kwargs):
        self._management_message_sender.message_remote(message_name, **kwargs)
        
 
class PluginTypeAuth(server_gateway.PluginTypeGatewayServer, tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    """
    Base class for all Gateway server authorization plugins 
    """        
    msg_receivers = None  # The functions exposed as receivers of messages from client
    
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info, database, management_message_session, access_log_server_session):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler)
        server_gateway.PluginTypeGatewayServer.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
        self.database = database
        self.management_message_session = management_message_session
        self.access_log_server_session = access_log_server_session
        self._management_message_sender = None
        self._started = False
        self._ready = False
        self.callback = None

    def set_callback(self, callback):
        self.callback = callback
        
    def reset(self):
        self.callback = None
        self.management_message_session = None
        self.reset_tunnelendpoint()
        
    def get_tunnelendpoint(self):
        """
        This method might be moved to TunnelendpointTunnelBaseWithMessageDispatch
        """
        if self._tunnel_endpoint != None:
            return self._tunnel_endpoint
        raise PluginWaitingForConnectionException()
    
    def start(self):
        """
        Start the plugin. 
        
        The method is expected to call self.set_started(), when the plugin has successfuly started,
        and eventualy end up calling self.set_ready()
        """
        raise NotImplementedError

    def set_started(self):
        self._started = True
        if self.callback:
            self.callback.plugin_event_state_change(self.plugin_name)
        
    def is_started(self):
        return self._started

    def set_ready(self):
        self._ready = True
        self._started = True
        if self.callback:
            self.callback.plugin_event_state_change(self.plugin_name, self.get_predicate_info(), internal_type=self.get_internal_type())

    def get_predicate_info(self):
        return []

    def get_internal_type(self):
        return None

    def is_ready(self):
        return self._ready
    
    def _check_element(self, param, internal_type=None):
        raise NotImplementedError()

    def check_predicate(self, params, internal_type=None):
        with self.checkpoint_handler.CheckpointScope("PluginTypeAuth::check_predicate", self.plugin_name, lib.checkpoint.DEBUG, internal_type=internal_type, params=params) as cps:
            if not self._started:
                assert not self._ready  # can't become ready before started
                self.start()
            
            if not self._ready:
                result = None
                cps.add_complete_attr(predicate='%r'%result)
                cps.add_complete_attr(ready='%r'%self._ready)
                cps.add_complete_attr(started='%r'%self._started)
                return result
            
            result = self._check_element(params, internal_type)  
            cps.add_complete_attr(predicate='%r'%result)
            return result
        
    def get_message_sender(self):
        if not self._management_message_sender:
            self._management_message_sender = ManagmentMessageSender(self.plugin_name, self.management_message_session)
        return self._management_message_sender
        