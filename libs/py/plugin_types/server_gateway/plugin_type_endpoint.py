"""
The base class and common functionality for server Gateway endpoint plugins.
"""
from __future__ import with_statement
from plugin_types import server_gateway


class EndpointSessionPluginCB(object):
    """
    The is the interface class for callbacks to endpoint session
    """
    def get_registered_endpoint_info(self, endpoint_id):
        """
        Expected to return information about a registered endpoint
        """
        raise NotImplementedError

        

class PluginTypeEndpoint(server_gateway.PluginTypeGatewayServer):
    """
    Base class for all Gateway endpoint plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info):
        server_gateway.PluginTypeGatewayServer.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
        self.endpoint_session_plugin_cb = None
    
    def set_endpoint_session(self, endpoint_session_plugin_cb):
        self.endpoint_session_plugin_cb = endpoint_session_plugin_cb
        
    def reset(self):
        self.endpoint_session_plugin_cb = None

    def endpoint_info_report_some(self, endpoint_info_plugin_name, endpoint_info_attributes):
        """
        Inform the plugin that some endpoint information is available from the given plugin
        """
        raise NotImplementedError
        
    def endpoint_info_all_done(self):
        """
        Inform the plugin that all endpoint information has been collected
        """
        raise NotImplementedError
