from __future__ import with_statement

import plugin_types.server_gateway.plugin_type_auth
from plugin_types.common.plugin_type_user import LoginModuleBase
import sys
import lib.checkpoint



class PluginTypeUser(plugin_types.server_gateway.plugin_type_auth.PluginTypeAuth, LoginModuleBase):
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_types.server_gateway.plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info, database, management_message_session, access_log_server_session)
        LoginModuleBase.__init__(self)
        self.user_session_callback = None
        self.login_requested = False
        
        
        
    def request_login(self):
        if self.user_session_callback:
            self.user_session_callback.request_login(self)
        else:
            self.login_requested = True

    def set_ready(self):
        """
        Override normal set_ready behaviour by not notifying auth_sesssion. User session will do it in stead
        """
        self._ready = True
        self._started = True

    def set_started(self):
        """
        Override normal set_ready behaviour by not notifying auth_sesssion. User session will do it in stead
        """
        self._started = True

    def login_cancelled(self):
        pass

    def set_user_session_callback(self, callback):
        self.user_session_callback = callback
        if self.login_requested:
            self.login_requested = False
            self.request_login()
    
    def reset_user_session_callback(self):
        self.user_session_callback = None
        
    def is_domain(self, dns=None):
        return False
    
    
    def get_auth_data(self):
        return None
    
    def sso_authenticate_user(self, nt_user_name, user_sid):
        return False
        

