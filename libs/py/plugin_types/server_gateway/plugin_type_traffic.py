"""
The base class and common functionality for server Gateway traffic plugins.
"""
from __future__ import with_statement
from plugin_types import server_gateway


class PluginTypeTraffic(server_gateway.PluginTypeGatewayServer):
    """
    Base class for all Gateway traffic plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info):
        server_gateway.PluginTypeGatewayServer.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
    
    def get_attribute(self, attribute_name):
        """
        Expected to return a value for the attribute having the given attribute_name. 
        If the attribute_name is not known to the plugin, None should be returned. 
        """
        raise NotImplementedError
