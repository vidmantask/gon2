"""
Common functionality for all Gateway server plugins
"""
from plugin_types import common


class PluginTypeGatewayServer(common.PluginType):
    """
    Base class for all Gateway server plugins
    """
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info):
        common.PluginType.__init__(self, checkpoint_handler, plugin_name)
        self.async_service = async_service
        self.license_handler = license_handler
        self.session_info = session_info

