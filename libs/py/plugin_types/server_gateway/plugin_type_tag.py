"""
The base class and common functionality for server Gateway tag plugins.

A tag plugin's main functionality is to generate new tags given a list of existing tags. 
It can not be assumed that one plugin is called before another, this is why the generated tags not are exposed to the other plugins. 
"""
from __future__ import with_statement

import types
from plugin_types import server_gateway

from components.communication import tunnel_endpoint_base 


class Tag(object):
    """
    Base class for all tags generated 
    """        
    def __init__(self, plugin_id, tag_name, tag_args=None):
        self.plugin_id = plugin_id
        self.tag_name = tag_name
        self.tag_args = tag_args
        
    def get_tag_id(self):
        tag_id = ''
        if self.plugin_id is not None:
            tag_id += '%s::' % (self.plugin_id)
        tag_id += '%s' % (self.tag_name)
        if self.tag_args is not None:
            if type(self.tag_args) is types.StringType:
                tag_id += "('%s')" % str(self.tag_args)
            else:
                tag_id += "(%s)" % str(self.tag_args)
        return tag_id

    def in_tags(self, tags):
        for tag in tags:
            if self.get_tag_id() == tag.get_tag_id():
                return True
        return False

    def in_tags_get(self, tags):
        return [tag for tag in tags if self.plugin_id == tag.plugin_id and self.tag_name == tag.tag_name]
#        for tag in tags:
#            if self.plugin_id == tag.plugin_id and self.tag_name == tag.tag_name:
#                return tag
#        return None

    @classmethod
    def create_tag_list_from_string_set(cls, arg_tags_strings):
        arg_tags = []
        for arg_tags_string in arg_tags_strings:
            plugin_id = None
            tag_name = arg_tags_string
            tag_args = None
            i =  arg_tags_string.find('::')
            j =  arg_tags_string.find('(', i+2)
            k =  arg_tags_string.find(')', j+1)
            if i > 0:
                plugin_id = arg_tags_string[:i]
                if j > 0 and k > 0:
                    tag_name = arg_tags_string[i+2:j]
                    tag_args_str = arg_tags_string[j+1:k]
                    try: 
                        tag_args = eval(tag_args_str, {}, {})
                    except:
                        tag_args_str
                        tag_args = 'ERROR %s' % tag_args_str 
                else:
                    tag_name = arg_tags_string[i+2:]

            arg_tags.append(Tag(plugin_id, tag_name, tag_args))
        return arg_tags
    
    @classmethod
    def create_string_set_from_tag_list(cls, arg_tags):
        arg_tags_strings = set()
        for arg_tag in arg_tags:
            arg_tags_strings.add(arg_tag.get_tag_id())
        return arg_tags_strings
        

class TagInfo(Tag):
    def __init__(self, plugin_id, tag_name, summary, description):
        Tag.__init__(self, plugin_id, tag_name)
        self.summary = summary
        self.description = description
        

class PluginTypeTagCB(object):
    """
    Definition of callbacks from the class PluginTypeTag
    """
    def tag_changed(self, plugin_name):
        pass

    def get_cpm_session(self):
        pass


class PluginTypeTag(server_gateway.PluginTypeGatewayServer, tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    """
    Base class for all Gateway traffic plugins 
    """        
    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler)
        server_gateway.PluginTypeGatewayServer.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
        self.cb = None

    def set_cb(self, cb):
        self.cb = cb

    def reset(self):
        self.cb = None
        self.reset_tunnelendpoint()
        
    def client_connected(self):
        """
        Called if a plugin on the Gateway client is connected
        """
        pass
    
    def generate_tags(self, taglist):
        """
        Expected to return a list of new tags. The taglist given as argument should not be included in the result. 
        """
        raise NotImplementedError
        
    def get_argument_taginfos(self):
        """
        Expected to return a list of new taginfos that the plugin react on. 
        """
        raise NotImplementedError

    def notify_tag_changed(self):
        if self.cb != None:
           self.cb.tag_changed(self.plugin_name)


if __name__ == '__main__':
    tags = Tag.create_tag_list_from_string_set(['BROWSER','client_ok::IfPlatformIs("win")','SERVEROK', 'WINDOWS', 'MAIL', 'package::PackageMissing(["app_client_gtsc","win"])'])
    for tag in tags:
        print tag.get_tag_id()
