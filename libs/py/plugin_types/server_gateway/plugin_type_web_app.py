from __future__ import with_statement

from plugin_types import server_gateway
import components.database.server_common.database_api as database_api



class PluginTypeWebApp(server_gateway.PluginTypeGatewayServer):


    def __init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info):
        server_gateway.PluginTypeGatewayServer.__init__(self, async_service, checkpoint_handler, plugin_name, license_handler, session_info)
        self.user_session_callback = None


    def set_user_session_callback(self, user_session_callback):
        self.user_session_callback = user_session_callback
        
    def do_GET(self, base_http_server):
        pass
    
    def do_POST(self, base_http_server):
        pass