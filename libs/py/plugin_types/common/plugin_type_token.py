from components.database.server_common.database_api import *
import datetime

dbapi = SchemaFactory.get_creator("plugin_type_token")


def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)

table_token = dbapi.create_table("token",
                                 Column('plugin_type', String(200)),
                                 Column('internal_type', String(200)),
                                 Column('serial', String(200)),
                                 Column('description', String(2000)),
                                 Column('casing_number', String(2000)),
                                 Column('public_key', String(2000)),
                                 Column('name', String(200)),
                                 Column('endpoint', Boolean, default=False),
                                 Column('licensed', Boolean, default=True),
                                 Column('created', DateTime(), default=datetime.datetime.now()),
                                 Column('last_seen', DateTime()),
                               )

dbapi.add_unique_constraint(table_token, 'serial')
dbapi.add_unique_constraint(table_token, 'name')

SchemaFactory.register_creator(dbapi)

class Token(PersistentObject):
    pass

mapper(Token, table_token)


def get_tokens_by_name(name, db_session=None):
    with SessionWrapper(db_session) as dbs:
        return dbs.select(Token, filter=Token.name==name)
    
def lookup_token(dbs, plugin_name, token_serial):
    return dbs.select_first(Token, filter=and_(Token.serial==token_serial, Token.plugin_type==plugin_name))

def get_endpoint_tokens(db_session):
    return db_session.select(Token, Token.endpoint==True)

def get_number_of_registered_tokens(db_session=None):
    with SessionWrapper(db_session) as dbs:
        return dbs.select_count(Token, filter=Token.licensed==True)
    
def create_and_check_token_name(dbt, name):
    count = 1
    while True:
        new_name = "%s (%d)" % (name, count)
        existing_tokens = dbt.select_count(Token, filter=Token.name == new_name)
        if existing_tokens==0:
            return new_name
        count += 1
    
def get_unique_token_name(dbt, name):
    existing_tokens = dbt.select_count(Token, filter=Token.name == name)
    if existing_tokens:
        return create_and_check_token_name(dbt, name)
    else:
        return name
    
    