"""
Common functionality for all plugins
"""

class PluginType(object):
    """
    Base class for all plugins
    """
    def __init__(self, checkpoint_handler, plugin_name):
        self.checkpoint_handler = checkpoint_handler
        self.plugin_name = plugin_name
    