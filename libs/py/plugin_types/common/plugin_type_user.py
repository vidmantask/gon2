from __future__ import with_statement

import sys
import lib.checkpoint



class LoginModuleBase():
    def __init__(self):
        self.user_session_callback = None
        self.login_requested = False
        self.authenticated = False
        self.must_change_password = False
        self.days_to_password_expiration = None
        self._password_expiration_warning_days = 14
        self.login_error_code = None
        self.login_error_message = None
        self.change_password_disabled = False
        
        
        
    def get_current_user_id(self):
        """
        Expected to return a an id for the user currently logged in. 
        If no user is logged in None should be returned  
        """
        raise NotImplementedError
    
    
    def request_login(self):
        if self.user_session_callback:
            self.user_session_callback.request_login(self)
        else:
            self.login_requested = True

    def is_change_password_disabled(self):
        return self.change_password_disabled

    def get_password_warning_days(self):
        return self._password_expiration_warning_days

    def set_password_warning_days(self, days):
        try:
            tmp = int(days)
            self._password_expiration_warning_days = tmp
        except ValueError:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("load_ad_module.password_expiry_warning.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)

    def get_days_to_password_expiration(self):
        return self.days_to_password_expiration


    # Return tuple (error_code, error_messge) for last login error 
    def get_login_error(self):
        if self.login_error_code or self.login_error_message:
            return (self.login_error_code, self.login_error_message)
        return None

    def clear_login_error(self):
        self.login_error_code = self.login_error_message = None

    def is_authenticated(self):
        return self.authenticated
            
    def change_password_needed(self):
        return self.must_change_password
            

    def receive_login(self, login, password, internal_user_login=None):
        pass
    
    def receive_changed_password(self, password):
        pass

    def get_normalised_login(self, user_login):
        pass
    
    def lookup_user(self, user_login):
        pass
    
