"""
The base class for upgrade plugins
"""
from __future__ import with_statement
from plugin_types import server_config


import os
import os.path
import sys
import types

import lib.checkpoint
import lib.appl.upgrade    


class PluginTypeUpgrade(server_config.PluginTypeServerConfig):
    """
    Base class for all upgrade plugins.
    """        
    def __init__(self, checkpoint_handler, server_configuration_all, plugin_module_name, plugin_name):
        server_config.PluginTypeServerConfig.__init__(self, checkpoint_handler, server_configuration_all, plugin_name)
        self.plugin_module_name = plugin_module_name

    def _create_version_string_from_tuple(self, tuple):
        strings = [str(t) for t in tuple]
        return "_".join(strings)
    
    def upgrade(self, from_version, to_version,  database_api, transaction, restore_path):
        """
        Load and create instances of relevant version upgrade plugins, and call objects 
        """ 
        py_objects = {}
        py_classes = []
        py_module_name = "%s.%s-%s" % (self.plugin_module_name, self._create_version_string_from_tuple(from_version), self._create_version_string_from_tuple(to_version))
        
        with self.checkpoint_handler.CheckpointScope('PluginTypeUpgrade::load_plugins', self.plugin_name, lib.checkpoint.DEBUG, py_module_name=py_module_name) as cps:
            try:
                py_module = __import__(py_module_name, fromlist=['.'])
                for py_plugin_name in py_module.__dict__:
                    py_plugin = getattr(py_module, py_plugin_name)

                    if isinstance(py_plugin, types.TypeType) and issubclass(py_plugin, PluginTypeUpgradeVersion):
                        self.checkpoint_handler.Checkpoint("load_plugins.load", self.plugin_name, lib.checkpoint.DEBUG, class_name=py_plugin.__name__)
                        py_classes.append(py_plugin)
                cps.add_complete_attr(found=repr(True))
            except ImportError, e:
                cps.add_complete_attr(found=repr(False))
                cps.add_complete_attr(error=e)
            except :
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("load_plugins.load.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)
            
        for py_plugin_class in py_classes:
            try:
                py_plugin_instance = py_plugin_class(self.plugin_name, self.checkpoint_handler, from_version, to_version)
                py_plugin_instance_name = py_plugin_class.__name__
                py_objects[py_plugin_instance_name] = py_plugin_instance
                self.checkpoint_handler.Checkpoint("load_plugins.instance", self.plugin_name, lib.checkpoint.DEBUG, plugin_name=py_plugin_instance_name, plugin_class=py_plugin_instance.__class__.__name__)
            except :
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("load_plugins.instance.error", self.plugin_name, lib.checkpoint.ERROR, etype, evalue, etrace)

        for py_object in py_objects.values():
            with self.checkpoint_handler.CheckpointScope('PluginTypeUpgrade::upgrade_db', self.plugin_name, lib.checkpoint.DEBUG):
                restore_path_database = os.path.join(restore_path, lib.appl.upgrade.DATABASE_FOLDER)
                py_object.upgrade_db(database_api, transaction, restore_path_database)
        
            with self.checkpoint_handler.CheckpointScope('PluginTypeUpgrade::upgrade_config', self.plugin_name, lib.checkpoint.DEBUG):
                restore_path_config = os.path.join(restore_path, lib.appl.upgrade.CONFIG_FOLDER)
                py_object.upgrade_config(restore_path_config)

            restore_path_plugins = os.path.join(restore_path, lib.appl.upgrade.PLUGIN_FOLDER)
            if  os.path.isdir(restore_path_plugins):
                for plugin_id in os.listdir(restore_path_plugins):
                    restore_path_plugin = os.path.join(restore_path_plugins, plugin_id)
                    if os.path.exists(restore_path_plugin):
                        with self.checkpoint_handler.CheckpointScope('PluginTypeUpgrade::upgrade_plugin', self.plugin_name, lib.checkpoint.DEBUG, plugin_id=plugin_id):
                            plugin_version = lib.appl.upgrade.load_version(restore_path_plugin)
                            py_object.upgrade_plugin(plugin_id, plugin_version, restore_path_plugin)
                    

class PluginTypeUpgradeVersion(object):
    """
    Base class for all upgrade plugins doing the actual version upgrade 
    """        
    def __init__(self, upgrade_plugin_name, checkpoint_handler, from_version, to_version):
        self.upgrade_plugin_name = upgrade_plugin_name
        self.checkpoint_handler = checkpoint_handler
        self.from_version = from_version
        self.to_version = to_version
        
    def upgrade_db(self, database_api, transaction, restore_path):
        """
        Do the actual version upgrade 
        """ 
        pass
    
    def upgrade_config(self, restore_path_config):
        """
        Do the actual version upgrade 
        """ 
        pass
        
    def upgrade_plugin(self, plugin_id, plugin_version, restore_path_plugin):
        """
        Do the actual version upgrade 
        """ 
        pass
    
