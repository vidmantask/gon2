"""
Module contain the base class for config specification plugins
"""
from __future__ import with_statement
from plugin_types import server_config

import sys
import types
import os.path

import lib.checkpoint

from components.rest.rest_api import RestApi


class PluginTypeConfigSpecification(server_config.PluginTypeServerConfig):
    """
    The base class for config specification plugins
    """

    ID = None

    def __init__(self, checkpoint_handler, server_configuration_all, plugin_module_name, plugin_name):
        server_config.PluginTypeServerConfig.__init__(self, checkpoint_handler, server_configuration_all, plugin_name)
        self.plugin_module_name = plugin_module_name

    def get_version(self):
        """
        This method is expected to return the version of the plugin.
        """
        raise NotImplementedError

    def get_config_specification(self):
        """
        Get config specification from plugin.
        Expected to return a instance of components.server_config_ws.ws.server_config_ws_services_types.ns1.ConfigurationTemplate_Def(None).pyclass()
        """
        raise NotImplementedError

    def save(self, config_specification):
        """
        If the config_specification does not match the current instance then None should be returned
        else should a instance of components.server_config_ws.ws.server_config_ws_services_types.ns0.SaveConfigSpecificationResponseType_Def(None).pyclass() be returned.
        """
        raise NotImplementedError

    def finalize(self):
        """
        If the config_specification does not match the current instance then None should be returned
        else should a instance of components.server_config_ws.ws.server_config_ws_services_types.ns0.SaveConfigSpecificationResponseType_Def(None).pyclass() be returned.
        """
        pass


    def get_template(self, element_id=None, sub_type=None):
        """
        test configuration and return rc, msg
        test_type can be used if a module has more than one test that can be performed
        """
        raise NotImplementedError

    def save_template(self, config_template, element_id=None, sub_type=None):
        """
        test configuration and return rc, msg
        test_type can be used if a module has more than one test that can be performed
        """
        raise NotImplementedError

    def delete_template(self, config_template, element_id=None, sub_type=None):
        """
        test configuration and return rc, msg
        test_type can be used if a module has more than one test that can be performed
        """
        raise NotImplementedError

    def test_config(self, config_specification, test_type=None, sub_type=None):
        """
        test configuration and return rc, msg
        test_type can be used if a module has more than one test that can be performed
        """
        raise NotImplementedError

    def generate_support_package(self, support_package_root):
        """
        This method is called with a folder where the plugin can dump files/info. The information dumped will be packaed to a support package for giritech.
        """
        raise NotImplementedError

    def backup(self, backup_log, backup_package_plugin_root, cb_config_backup, alternative_server_configuration_all):
        """
        This method is called with a folder where the plugin can backup files/info. The information dumped will be included in the backup.
        """
        raise NotImplementedError

    def restore(self, restore_log, restore_package_plugin_root, cb_restore):
        """
        This method is called with a folder where the plugin was backedup.
        """
        raise NotImplementedError

    def get_server_management_plugin_root(self, server_configuration_all, plugin_name):
        return os.path.join(server_configuration_all.configuration_server_management.plugin_modules_abspath, 'plugin_modules', plugin_name, 'server_management')

    def get_server_gateway_plugin_root(self, server_configuration_all, plugin_name):
        return os.path.join(server_configuration_all.configuration_server_gateway.plugin_modules_abspath, 'plugin_modules', plugin_name, 'server_gateway')


    def has_config(self):
        return True

    def has_user_config(self):
        return False


    def get_type_and_priority(self):
        return ("_Unknown", 100)

    @classmethod
    def create_save_response(cls, rc, field_name = None, message = None):
        response = RestApi.ServerConfig.SaveConfigSpecificationResponse()
        cls._update_save_response(response, rc, field_name, message)
        return response


    @classmethod
    def create_save_template_response(cls, rc, field_name = None, message = None, spec=None):
        response = RestApi.ServerConfig.SaveConfigTemplateResponse()
        cls._update_save_response(response, rc, field_name, message)
        response.set_config_spec(spec)
        return response

    @classmethod
    def _update_save_response(cls, response, rc, field_name = None, message = None):
        response.set_rc(rc)
        if not rc:
            error_location = RestApi.Admin.ConfigTemplate.ErrorLocation()
            error_location.set_field_name(field_name)
            error_location.set_message(message)
            response.set_error_location(error_location)
        return response


