"""
Common functionality for all Configuration plugins
"""
from plugin_types import common


class PluginTypeServerConfig(common.PluginType):
    """
    Base class for all Configuration plugins
    """
    def __init__(self, checkpoint_handler, server_configuration_all, plugin_name):
        common.PluginType.__init__(self, checkpoint_handler, plugin_name)
        self.server_configuration_all = server_configuration_all
