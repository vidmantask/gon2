"""
Module contain the base class for config specification plugins
"""
from __future__ import with_statement
from plugin_types import server_config

import sys
import types
import os.path

import lib.checkpoint

from plugin_types.server_config import plugin_type_config_specification


class PluginTypeUserConfigSpecification(plugin_type_config_specification.PluginTypeConfigSpecification):
    """
    The base class for config specification plugins
    """

    ID = None
    TITLE = "User Directory"

    def __init__(self, checkpoint_handler, server_configuration_all, plugin_module_name, plugin_name):
        plugin_type_config_specification.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration_all, plugin_module_name, plugin_name)

    def has_config(self):
        return False

    def has_user_config(self):
        return True


    def is_singleton(self):
        return False

    def get_title(self):
        return self.TITLE

    def get_config_specifications(self, parent_module_id):
        return dict()

    def get_create_config_specification(self, parent_module_id):
        return None

    def check_template(self, template):
        raise NotImplementedError()


    def is_enabled(self):
        return False

    def save_data(self, enabled, template_list):
        pass




