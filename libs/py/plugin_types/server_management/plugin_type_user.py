from __future__ import with_statement

import plugin_type_element
import plugin_type_config

import components.database.server_common.database_api as database_api
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec





class PluginTypeUser(plugin_type_element.PluginTypeElement, plugin_type_config.PluginTypeConfig):


    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        self._users = None
        self._groups = None
        self._user_columns = None
        self._user_admin_column_indices = set()
        self.user_admin = None


    ATTRIBUTE_ID = 'user_id'
    ATTRIBUTE_LOGIN = 'user_login'
    ATTRIBUTE_FULLNAME = 'user_fullname'
    ATTRIBUTE_PLUGIN = 'user_plugin'
    ATTRIBUTE_GON_USER = "registered_user"


    def supports_optional_attribute(self, name):
        return False

    element_types = ["user", "group"]


    def clear_element_cache(self, element_type):
        if element_type=="user":
            self._users = None
        elif element_type=="group":
            self._groups = None

    def get_elements(self, element_type, search_filter=None):
        if element_type=="user":
            return self.fetch_users(search_filter)
        elif element_type=="group":
            return self.fetch_groups(search_filter)
        
    def fetch_users(self, search_filter=None):
        raise NotImplementedError
        
    def fetch_groups(self, search_filter=None):
        raise NotImplementedError

    def get_group_members(self, group_id):
        raise NotImplementedError
        
    def get_element_count(self, element_type):
        if element_type=="user":
            return len(self._users) if self._users else -1
        elif element_type=="group":
            return len(self._groups) if self._groups else -1


    def get_email(self, user_id):
        raise NotImplementedError()
    
    def config_get_template(self, element_type, element_id, db_session=None):
        if element_type.lower()=="user" and element_id:
            config_spec = self._get_user_config_spec(self.plugin_name, element_id)
            config_spec.set_value(self.ATTRIBUTE_ID, element_id)

            internal_user_login, user_login, user_name = self.get_login_and_name_from_id(element_id, None)
            config_spec.set_value(self.ATTRIBUTE_LOGIN, user_login)
            
            config_spec.set_value(self.ATTRIBUTE_PLUGIN, self.plugin_name)

            self.add_config_values(element_type, element_id, config_spec, db_session)
            return config_spec.get_template()
        else:
            return None
        
    
    def _get_user_config_spec(self, plugin_name, element_id):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (self.plugin_name,), "Edit User", "")
        
        config_spec.add_field(self.ATTRIBUTE_ID, 'user_id', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field(self.ATTRIBUTE_LOGIN, 'Login', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field(self.ATTRIBUTE_PLUGIN, 'Plugin', ConfigTemplateSpec.VALUE_TYPE_STRING, value=False)
        field.set_attribute_read_only(True)
        field = config_spec.add_field(self.ATTRIBUTE_GON_USER, 'G/On User', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=False)
        field.set_attribute_read_only(True)
        self.add_user_config_fields(config_spec)
        
        return config_spec
    
    def add_user_config_fields(self, config_spec):
        pass
        
    def get_id_column(self):
        return plugin_type_config.Column(self.plugin_name, 'id', plugin_type_config.Column.TYPE_STRING, 'ID', hidden=True)

    def get_edit_columns(self, element_type):
        return []
    
    
    def config_get_columns(self, element_type):
        if self._user_columns is None:
            self._user_columns = [self.get_id_column()]
            self._user_columns.extend(self.get_edit_columns(element_type))
            user_admin_columns = self.user_admin.add_user_columns(self.plugin_name, self._user_columns)
        return self._user_columns
    
    def get_login_and_name_from_id(self, user_id, internal_user_login):
        pass
    
    def user_exists(self, user_id):
        return False

    def add_config_values(self, element_type, element_id, config_spec, db_session):
        pass
    
    def update_row(self, element_type, value_dict, transaction):
        pass

    def create_row(self, element_type, value_dict, transaction):
        pass
    
    def delete_row(self, element_type, element_id):
        pass
            
    def config_create_row(self, element_type, template, dbt):
        pass
            
        
    def config_delete_row(self, element_type, element_id, dbt):
        self.delete_row(element_type, element_id)
        self.user_admin.delete_user_values(self.plugin_name, element_type, element_id, dbt)
        
    def get_login_module(self):
        return None
    
    def get_plugin_title(self):
        return self.plugin_name
    