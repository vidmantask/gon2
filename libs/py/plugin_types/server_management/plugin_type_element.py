"""
The base class for element plugins
"""
from plugin_types import server_management

from components.auth.server_management import ElementType
from components.auth.server_common import rule_definitions


class Param(object):
    """
    Class for specification of modules Predicate Parameters
    """
    def __init__(self, name, title, select_list_lister=None):
        self.name = name
        self.title = title
        self.select_list_lister = select_list_lister

    def __str__(self):
        return str('%s/"%s"' % (self.name, self.title))

    __repr__ = __str__


class Profile(object):
    """
    Class for specification of modules Predicate Parameter Profile
    """
    def __init__(self, name, title, *params):
        self.name = name
        self.title = title
        self.params = params

    def __str__(self):
        return 'Profile %s "%s": %s' % (self.name, self.title, list(self.params))


def Profile_dict(*profile_list):
    return dict((profile.name, profile) for profile in profile_list)


class ModuleElementType(ElementType):
    
    def __init__(self, plugin_name, id, label, info):
        self.entity_type = ""
        self.internal_id = id
        self.external_id = "%s.%s" % (plugin_name, id)
        self._label = label
        self._info = info
        self.element_type = "module"
        self.template = None
    
    def get_id(self):
        return self.external_id

    def get_internal_id(self):
        return self.internal_id
    
    def get_info(self):
        return self._info
    
    def get_label(self):
        return self._label

    def get_entity_type(self):
        return self.entity_type
    
    def get_template(self):
        return self.template
    
    def get_element_type(self):
        return self.element_type

class PluginTypeElement(server_management.PluginTypeManagementServer):
    """
    Base class for all element plugins 
    """        

    predicates = None  # List of predicate profiles, used by Configurator
    
    element_types = []

    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        server_management.PluginTypeManagementServer.__init__(self, checkpoint_handler, database, license_handler, plugin_name)

    def get_elements(self, element_type):
        raise NotImplementedError()

    def get_specific_elements(self, internal_element_type, element_ids):
        raise NotImplementedError()

    def clear_element_cache(self, element_type):
        pass

    def get_element_count(self, element_type):
        return -1
    
    def has_element_type(self, element_type):
        return element_type.lower() in self.element_types
    
    def create_element(self, id, label, info=""):
        return ModuleElementType(self.plugin_name, id, label, info)

    
    

    