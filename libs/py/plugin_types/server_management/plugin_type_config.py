"""
The base class for cofiguration plugins.

The configuration plugin offers functionality for Get, Create, Update, Delete of plugin specific data
"""
import lib.checkpoint

from plugin_types import server_management

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec



class PluginTypeConfig(server_management.PluginTypeManagementServer):
    """
    Base class for all cofig plugins 
    """        
    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        server_management.PluginTypeManagementServer.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
    

    def config_get_template(self, element_type, element_id, db_session):
        """
        Expected to return a list of Column objects
        """
        raise NotImplementedError
    
    
    def config_create_row(self, element_type, template, transaction):
        """
        Create the row, return the created row id. The returned row's values can be updated.
        """
        raise NotImplementedError
    
    def config_update_row(self, element_type, template, transaction):
        """
        Update the row, return the updated rule element label. The returned row's values can be updated.
        """
        raise NotImplementedError
    
    def config_delete_row(self, element_type, element, transaction):
        """
        Delete the row, return true if the row was deleted
        """
        raise NotImplementedError
    
    def get_config_enabling(self, element_type):
        """
        Returns whether edit and delete, respectively, are enabled for this element_type
        """
        return False, False


    def _update_obj_with_row(self, obj, row, transaction): 
        for (name,value) in row.items():
            try:
                setattr(obj, name, value)
            except AttributeError, e:
                self.checkpoint_handler.CheckPoint("_update_db_with_row", self.plugin_name, lib.checkpoint.WARNING, message="Columh '%s' does not exist in database" % name)


    def _update_obj_with_template(self, obj, template, transaction):
        config_spec = ConfigTemplateSpec(template)
        row = config_spec.get_field_value_dict()
        self._update_obj_with_row(obj, row, transaction)
        return config_spec
