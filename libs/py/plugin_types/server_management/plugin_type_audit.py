'''
Created on 24/11/2009

@author: peter
'''
import lib.checkpoint
from plugin_types import server_management


class PluginTypeAudit(server_management.PluginTypeManagementServer):
    
    
    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        server_management.PluginTypeManagementServer.__init__(self, checkpoint_handler, database, license_handler, plugin_name)


    def save_entry(self, **value_dict):
        print "PluginTypeAudit save %s" % value_dict

    def set_version_string(self, version_string):
        pass