"""
The base class for access notification plugins
"""
from plugin_types import server_management


class PluginTypeAccessNotification(server_management.PluginTypeManagementServer):
    """
    Base class for all access notification plugins 
    """        
    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        server_management.PluginTypeManagementServer.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
    
    def notify_login(self, login, user_sid, user_email, ip, timestamp):
        """
        Called just after a user has logged in
        """ 
        raise NotImplementedError
