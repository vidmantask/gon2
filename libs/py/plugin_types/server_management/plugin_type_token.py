"""
Token plugin for the Management Server 

The main functionality for this plugin is to split token rows into the types soft_token and smart_card.



"""
from __future__ import with_statement

import uuid
import datetime

import lib.checkpoint
from components.database.server_common.database_api import *

from plugin_types.server_management import plugin_type_config
from plugin_types.server_management import plugin_type_element

from components.auth.server_management import OperationNotAllowedException
from components.management_message.server_management.session_recieve import ManagementMessageSessionRecieverBase
from plugin_types.common.plugin_type_token import *

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.auth.server_management.rule_api as rule_api

IDENT_NAME = 'name'
IDENT_TYPE = 'plugin_type'
IDENT_INTERNAL_TYPE = 'internal_type'
IDENT_SERIAL_ID = 'serial_id'
IDENT_SERIAL = 'serial'
IDENT_DESCRIPTION = 'description'
IDENT_CASING_NUMBER = 'casing_number'
IDENT_PUBLIC_KEY = 'public_key'
IDENT_CREATED = 'created'
IDENT_LAST_SEEN = 'last_seen'
IDENT_ENDPOINT = 'endpoint'
IDENT_LICENSED = 'licensed'
IDENT_PERSONAL = 'personal'


max_number_of_tokens = -1

def create_and_check_token_name(dbt, plugin_type, plugin_type_name, no_of_tokens):
    while True:
        name = '%s-%04d' % (plugin_type_name, no_of_tokens)
        old_name = '%s-%04d' % (plugin_type, no_of_tokens)
        existing_tokens = dbt.select_count(Token, filter=or_(Token.name == name, Token.name == old_name))
        if not existing_tokens:
            break
        no_of_tokens += 1
    return name

def get_unique_token_name(dbt, plugin_type, plugin_type_name):
    if not plugin_type_name:
        plugin_type_name = plugin_type
    no_of_tokens = dbt.select_count(Token, filter=Token.plugin_type == plugin_type) + 1
    
    name = create_and_check_token_name(dbt, plugin_type, plugin_type_name, no_of_tokens)
    
    return name



class PluginToken(plugin_type_config.PluginTypeConfig, plugin_type_element.PluginTypeElement, ManagementMessageSessionRecieverBase):

    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        ManagementMessageSessionRecieverBase.__init__(self, checkpoint_handler, plugin_name)


        
        
    def get_elements(self, element_type):
        keys = self.get_keys(element_type)
        elements = []
        for k in keys:
            token = self._create_element(k)
            yield token
#                elements.append(token)
#            return elements
        
    def get_specific_elements(self, internal_element_type, element_ids):
        dbs = QuerySession()
        tokens = dbs.select(Token, filter=and_(Token.plugin_type==self.plugin_name,
                                               Token.internal_type==internal_element_type,
                                               in_(Token.serial, element_ids)))
        elements = []
        for k in tokens:
            token = self._create_element(k)
            elements.append(token)
        return elements
        
    def get_info(self,db_token):
        if db_token.description and db_token.casing_number:
            info = "%s : %s" % (db_token.casing_number, db_token.description)
        elif db_token.description:
            info = db_token.description
        else:
            info = db_token.casing_number
        return info
        
        
    def _create_element(self, db_token):
        info = self.get_info(db_token)
        token = self.create_element(db_token.serial, db_token.name, info)
        
#        config_spec = self._get_config_spec()
#        config_spec.set_values_from_object(db_token)
#        config_spec.set_value(IDENT_SERIAL_ID, db_token.serial)
#        token.template = config_spec.get_template()
        
        return token
            
        
    
    def config_get_template(self, element_type, element_id, db_session=None):
        with SessionWrapper(db_session) as dbs:
            config_spec = self._get_config_spec()
            if element_id:
                db_token = self._lookup_token(dbs, element_id)
                if db_token:
                    config_spec = self._get_config_spec()
                    config_spec.set_values_from_object(db_token)
                    config_spec.set_value(IDENT_SERIAL_ID, db_token.serial)
                    
#                    personal_rule_count = rule_api.is_in_personal_token_rule(element_type, element_id, plugin_name=self.plugin_name)
#                    config_spec.set_value(IDENT_PERSONAL, personal_rule_count>0)
                    
            return config_spec.get_template()
        return None
        
    
    def get_config_enabling(self, element_type):
        return True, True
    
    def _get_config_spec(self):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (self.plugin_name,), "Token deployment", "")

        field = config_spec.add_field(IDENT_NAME, 'Name (unique)', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_EDIT)
        field.set_attribute_read_only(True)
        config_spec.add_field(IDENT_TYPE, 'Token Type', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        config_spec.add_field(IDENT_INTERNAL_TYPE, 'Token Internal Type', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field(IDENT_SERIAL_ID, 'Serial Number', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field(IDENT_SERIAL, 'New Serial Number', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field(IDENT_DESCRIPTION, 'Description', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(2000)
        field = config_spec.add_field(IDENT_CASING_NUMBER, 'Casing Number', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(2000)
        field = config_spec.add_field(IDENT_CREATED, 'Created', ConfigTemplateSpec.VALUE_TYPE_DATETIME)
        field.set_attribute_read_only(True)
        field = config_spec.add_field(IDENT_LAST_SEEN, 'Last seen', ConfigTemplateSpec.VALUE_TYPE_DATETIME)
        field.set_attribute_read_only(True)
        field = config_spec.add_field(IDENT_PUBLIC_KEY, 'Public Key', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)

        config_spec.add_field(IDENT_ENDPOINT, 'Used in device', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, read_only=True, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)

        config_spec.add_field(IDENT_LICENSED, 'Licensed', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, read_only=True, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN, value=True)
#        config_spec.add_field(IDENT_PERSONAL, 'Personal Token', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, read_only=True) #field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)

        
        return config_spec
    
    def _check_constraints(self, new_key, db_session):
        tokens = get_tokens_by_name(new_key.name, db_session)
        if len(tokens)>1:
            raise OperationNotAllowedException("A token with this name already exists")
    
    
    
    def config_create_row(self, element_type, template, dbt):
        #new_key =  dbt.add(Token(**row))
        license = self.license_handler.get_license()
        if not license.valid:
            raise OperationNotAllowedException("Invalid license")
                
        new_key = Token()
        config_spec = self._update_obj_with_template(new_key, template, dbt)
        
        if not new_key.serial:
            new_key.serial = str(uuid.uuid1())
        else:
            existing_token = self._lookup_token(dbt, new_key.serial)
            if (existing_token):
                raise OperationNotAllowedException("This token is already enrolled in the system as '%s'" % existing_token.name)

        new_key.name = get_unique_token_name(dbt, new_key.plugin_type, new_key.name)
        
        if not new_key.internal_type:
            new_key.internal_type = new_key.plugin_type

        dbt.add(new_key)
        self._check_constraints(new_key, dbt)
        
        if new_key.licensed:
            max_number_of_tokens = license.get_int('Number of Tokens') 
            if not max_number_of_tokens < 0:
                no_of_tokens = get_number_of_registered_tokens(dbt)
                if no_of_tokens > max_number_of_tokens:
                    if config_spec.has_field("unique_session_id") and config_spec.get_value("unique_session_id"):
                        #TODO: Create warning event
                        self.checkpoint_handler.Checkpoint("add_token", self.plugin_name, lib.checkpoint.WARNING, message="Maximum number of licensed tokens reached", max_number_of_tokens=max_number_of_tokens, token_serial=new_key.serial)                        
                    else:
                        raise OperationNotAllowedException("Maximum number of licensed tokens (%d) reached" % max_number_of_tokens)
        
        return "%s.%s" % (self.plugin_name, new_key.serial) 
            
    
    def config_update_row(self, element_type, template, dbt):
        config_spec = ConfigTemplateSpec(template)
        key = self._lookup_token(dbt, config_spec.get_value(IDENT_SERIAL_ID))
        if key:
            row = config_spec.get_field_value_dict()
            self._update_obj_with_row(key, row, dbt)
            self._check_constraints(key, dbt)
            return key.name
        else:
            raise Exception("Token '%s' not found" % config_spec.get_value(IDENT_SERIAL_ID))
    
    def config_delete_row(self, element_type, element_id, dbt):
        db_token = self._lookup_token(dbt, element_id)
        if db_token != None:
            dbt.delete(db_token)
            return True
        return False

    def _lookup_token(self, dbs, token_id):
        return dbs.select_first(Token, filter=and_(Token.serial==token_id, Token.plugin_type==self.plugin_name))



    def get_keys(self, element_type):
        with self.checkpoint_handler.CheckpointScope('get_keys', self.plugin_name, lib.checkpoint.DEBUG) as cps:
            dbs = QuerySession()
            return dbs.select(Token, filter=and_(Token.plugin_type==self.plugin_name,
                                                 Token.internal_type==element_type))

    def sink_serial_authenticated(self, serial, server_sid):
        with Transaction() as dbt:
            token = self._lookup_token(dbt, serial)
            if not token:
                self.checkpoint_handler.Checkpoint("sink_serial_authenticated", self.plugin_name, lib.checkpoint.ERROR, message="Unable to find authenticated token", token_serial=serial)
            else:
                token.last_seen = datetime.datetime.now()


