"""
Common functionality for all Management Server plugins
"""
from plugin_types import common


class PluginTypeManagementServer(common.PluginType):
    """
    Base class for all Management Server plugins
    """
    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        common.PluginType.__init__(self, checkpoint_handler, plugin_name)
        self.database = database
        self.license_handler = license_handler
