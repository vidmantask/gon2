'''
Created on 24/11/2009

@author: peter
'''
import plugin_type_element
import plugin_type_config
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
from components.auth.server_common import rule_definitions


class PluginTypeEndpoint(plugin_type_element.PluginTypeElement, plugin_type_config.PluginTypeConfig):
    
    
    def __init__(self, checkpoint_handler, database, license_handler, plugin_name):
        plugin_type_element.PluginTypeElement.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        plugin_type_config.PluginTypeConfig.__init__(self, checkpoint_handler, database, license_handler, plugin_name)
        self.add_element_definitions()

    def add_element_definitions(self):
        rule_definitions.def_element_types.update(self.get_element_definitions())

    def get_element_definitions(self):
        return dict()

    def supports_device_status(self):
        return False

    def supports_endpoint_authentication(self):
        return False
    
    def get_default_entity_type(self):
        element_defs = self.get_element_definitions()
        if element_defs:
            return element_defs.keys()[0]
        else:
            raise Exception("No device rule definitions for plugin '%s'" % self.plugin_name)

    def get_rule_entity_type(self, config_spec_value):
        if not config_spec_value:
            return None
        element_defs = self.get_element_definitions()
        if element_defs:
            if len(element_defs) == 1:
                return element_defs.keys()[0]
            else:
                return config_spec_value
        else:
            raise Exception("No device rule definitions for plugin '%s'" % self.plugin_name)
    
    def get_attribute_filter(self):
        return None    
    
    def add_config(self, config_spec, endpoint_id, enabled):
        element_defs = self.get_element_definitions()
        if element_defs:
            if len(element_defs) == 1:
                element_def = element_defs.values()[0]
                name = element_def.get("plugin")
                title = element_def.get("title")
                config_spec.add_field(name, title, ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=enabled)
            else:
                # Selection - not yet implemented
                raise NotImplementedError()


