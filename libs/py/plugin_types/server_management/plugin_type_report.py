"""
The base class for report plugins.

A report plugin offers a list of BIRT reports and offers data-sources for the reports.
"""
import ConfigParser
import os.path


import lib.checkpoint
from plugin_types import server_management


class Report(object):
    def __init__(self, plugin_name, report_id, report_title, report_specification_filename=None):
        self.module_name = plugin_name
        self.report_id = report_id
        self.report_title = report_title
        self.report_specification_filename = report_specification_filename
        self.sub_specification_filenames = []
        self.data_ids = []

    def get_specification(self, checkpoint_handler):
        specification = ""
        if os.path.exists(self.report_specification_filename):
            file = open(self.report_specification_filename, 'r')
            specification = file.read()
        else:
            checkpoint_handler.Checkpoint("Report.get_specification.not_found", self.module_name, lib.checkpoint.ERROR, report_id=self.report_id, report_specification_filename=self.report_specification_filename)
        return specification

    @classmethod
    def find_by_id(cls, reports, report_id):
        for report in reports:
            if report.report_id == report_id:
                return report
        return None


def convert_stringlist_to_string(value_list):
    return ', '.join(value_list)

def convert_string_to_stringlist(value_list_string):
    return [name.strip() for name in value_list_string.split(",")]


class ReportConfig(object):
    SECTION_REPORT_PREFIX = 'report_'
    
    def __init__(self, plugin_name, config_folder):
        self.config_folder = config_folder
        self.plugin_name = plugin_name
        self.config = ConfigParser.ConfigParser()
        config_filename_abs = os.path.join(config_folder, 'config.ini')
        self.config.read(config_filename_abs)    
        
         
   
    def get_reports(self):
        reports = []
        for section in self.config.sections():
            if section.startswith(ReportConfig.SECTION_REPORT_PREFIX):
                report_id = section.replace(ReportConfig.SECTION_REPORT_PREFIX, "")
                report_title = ''
                if self.config.has_option(section,"title"):
                    report_title = self.config.get(section, "title")
                report_specification_filename = None
                if self.config.has_option(section,"specification_filename"):
                    report_specification_filename = self.config.get(section, "specification_filename")
                report = Report(self.plugin_name, report_id, report_title, os.path.join(self.config_folder, report_specification_filename))
                if report_specification_filename:
                    file_name = os.path.split(report_specification_filename)[1]
                    if file_name:
                        report.sub_specification_filenames.append(file_name)
                if self.config.has_option(section,"sub_specification_filenames"):
                    report.sub_specification_filenames.extend(convert_string_to_stringlist(self.config.get(section, "sub_specification_filenames")))
                if self.config.has_option(section,"data_ids"):
                    report.data_ids = convert_string_to_stringlist(self.config.get(section, "data_ids"))
                reports.append(report)
        return reports
                



class PluginTypeReport(server_management.PluginTypeManagementServer):
    """
    Base class for all report plugins 
    """        
    def __init__(self, checkpoint_handler, database, license_handler, name):
        server_management.PluginTypeManagementServer.__init__(self, checkpoint_handler, database, license_handler, name)
    
    
    def get_reports(self):
        """
        Expected to return a list of Report objects
        """
        raise NotImplementedError
    
    def get_report_specification(self, report_id):
        """
        Expected to return a BIRT report specification(a .rptdesign file) for the given report
        """
        raise NotImplementedError

    def find_report_specification_by_specification_filename(self, specification_filename):
        """
        Expected to return a BIRT report specification(a .rptdesign file) for the given filename
        If the specification is not found None should be returned 
        """
        return None
    
    def get_report_data(self, data_source_id, args):
        """
        Expected to return a list of rows, where each row contains a list of string values, for the given data source
        """
        raise NotImplementedError
    
