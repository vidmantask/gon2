#!c:\python27\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'pastescript==1.7.4.2','console_scripts','paster'
__requires__ = 'pastescript==1.7.4.2'
import sys
from pkg_resources import load_entry_point

if __name__ == '__main__':
    sys.exit(
        load_entry_point('pastescript==1.7.4.2', 'console_scripts', 'paster')()
    )
