#!c:\Python27\python.exe
# EASY-INSTALL-ENTRY-SCRIPT: 'zsi==2.0-rc3','console_scripts','wsdl2py'
__requires__ = 'zsi==2.0-rc3'
import sys
from pkg_resources import load_entry_point

sys.exit(
   load_entry_point('zsi==2.0-rc3', 'console_scripts', 'wsdl2py')()
)
