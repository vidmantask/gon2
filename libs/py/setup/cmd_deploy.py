"""
This file contains a python script to use for deployment of the current inserted keys.
The script uses the admin_server and admin_clinet_server which must be running on demo ports
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import os
import os.path
import tempfile
import shutil

import components.communication.async_service

import components.admin_deploy_ws.ws as ws
import components.admin_deploy_ws.admin_deploy_ws_service
import components.admin_ws.admin_ws_service


def _login(gon_deploy_service):
    request = ws.admin_deploy_ws_services.LoginRequest()
    request_content = ws.admin_deploy_ws_services.ns0.LoginRequestType_Def(None).pyclass()
    request_content.set_element_username("ib")
    request_content.set_element_password("FidoTheDog")
    request.set_element_content(request_content)
    response = gon_deploy_service.Login(request)
    return response.get_element_content().get_element_session_id()
 
def _logout(gon_deploy_service, session_id):
    request = ws.admin_deploy_ws_services.LogoutRequest()
    request_content = ws.admin_deploy_ws_services.ns0.LogoutRequestType_Def(None).pyclass()
    request_content.set_element_session_id(session_id)
    request.set_element_content(request_content)
    response = gon_deploy_service.Logout(request)
    rc = response.get_element_content().get_element_rc()


def main():
    admin_deploy_ws_host = '127.0.0.1'
    management_deploy_ws_port = 8081
    gon_deploy_service = ws.admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (admin_deploy_ws_host, management_deploy_ws_port))

    session_id = _login(gon_deploy_service)

    #
    # Get available tokens
    #
    request = ws.admin_deploy_ws_services.GetTokensRequest()
    request_c = ws.admin_deploy_ws_services.ns0.GetTokensRequestType_Def(None).pyclass()
    request_c.set_element_session_id(session_id)
    request.set_element_content(request_c)
    response = gon_deploy_service.GetTokens(request)
    for token in response.get_element_content().get_element_tokens():
        if token.get_element_runtime_env_id() != None:
            print "Found token :", token.get_element_token_label()
            print "Initialize token, begin"
            request = ws.admin_deploy_ws_services.InitTokenRequest()
            request_c = ws.admin_deploy_ws_services.ns0.InitTokenRequestType_Def(None).pyclass()
            request_c.set_element_session_id(session_id)
            request_c.set_element_token_type_id(token.get_element_token_type_id())
            request_c.set_element_token_id(token.get_element_token_id())
            request_c.set_element_token_serial('1234')
            request.set_element_content(request_c)
            response = gon_deploy_service.InitToken(request)
            print "Initialize token, end"

            print "Deploy token, begin"
            request = ws.admin_deploy_ws_services.DeployTokenRequest()
            request_c = ws.admin_deploy_ws_services.ns0.DeployTokenRequestType_Def(None).pyclass()
            request_c.set_element_session_id(session_id)
            request_c.set_element_token_type_id(token.get_element_token_type_id())
            request_c.set_element_token_id(token.get_element_token_id())
            request.set_element_content(request_c)
            response = gon_deploy_service.DeployToken(request)
            print "Deploy token, done"

            print "Install collection 'gon_client_all', begin"
            request = ws.admin_deploy_ws_services.InstallGPMCollectionRequest()
            request_c = ws.admin_deploy_ws_services.ns0.InstallGPMCollectionRequestType_Def(None).pyclass()
            request_c.set_element_session_id(session_id)
            request_c.set_element_runtime_env_id(token.get_element_runtime_env_id())
            request_c.set_element_gpm_collection_id('gon_client_all')
            request.set_element_content(request_c)
            response = gon_deploy_service.InstallGPMCollection(request)
            job_id = response.get_element_content().get_element_job_id()

            job_more = True
            while(job_more):
                request = ws.admin_deploy_ws_services.GetJobInfoRequest()
                request_c = ws.admin_deploy_ws_services.ns0.GetJobInfoRequestType_Def(None).pyclass()
                request_c.set_element_session_id(session_id)
                request_c.set_element_job_id(job_id)
                request.set_element_content(request_c)
                response = gon_deploy_service.GetJobInfo(request)
                
                job_info = response.get_element_content().get_element_job_info()
                job_more = job_info.get_element_job_more()
                job_status = job_info.get_attribute_job_status()
                job_header = job_info.get_element_job_header()
                job_sub_header = job_info.get_element_job_sub_header()
                job_progress = job_info.get_element_job_progress()
                print job_status, job_header, job_sub_header, job_progress
                time.sleep(1)

            job_done_status = job_info.get_attribute_job_status()
            job_header = job_info.get_element_job_header()
            job_sub_header = job_info.get_element_job_sub_header()
            print "Install collection 'gon_client_all', done", job_done_status, job_header, job_sub_header

#        print "Token:", token.get_element_token_id(), token.get_element_token_label(), token.get_element_token_type_id(), token.get_element_token_status(), token.get_element_token_serial(), token.get_element_runtime_env_id()

    _logout(gon_deploy_service, session_id)

if __name__ == '__main__':
    main()
