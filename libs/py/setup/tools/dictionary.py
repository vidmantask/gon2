"""
This module contains functionality  for generating and maintaing dictionaries
"""
import sys
import os.path
dev_env_root = os.path.join(os.path.dirname(__file__), '..', '..', '..')
dev_env_py_root = os.path.join(dev_env_root, 'py')
dev_env_root_auto_dicts = os.path.join(dev_env_root, 'setup', 'release', 'dictionary')

#dev_env_py_root = os.path.join(dev_env_root, 'py')
#sys.path.append(dev_env_py_root)

import optparse


import lib.checkpoint

import lib.dictionary
import lib.dictionary.dictionary_spec
import lib.dictionary.auto_translate
import components.dictionary.common

import lib.launch_process



DICTIONARY_DEFAULT_NAME = 'en'
DICTIONARY_DEFAULT_SUB_NAME = 'source'

checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()


def build_filelist(root, extensions):
    py_filelist = []
    for (dirpath, dirnames, filenames) in os.walk(root):
        for filename in filenames:
            filename_abs = os.path.join(dirpath, filename)
            (root, ext) = os.path.splitext(filename_abs)
            if ext in extensions:
                py_filelist.append(os.path.normpath(filename_abs))
    return py_filelist
        
        
STATE_LOOKING_FOR_MSGID = 0
STATE_LOOKING_FOR_MSGSTR = 1


LOOK_FOR_gettext = 0
LOOK_FOR_gettext_id = 1
LOOK_FOR_deftext_id = 2


def build_messagelist(temp_folder, source_filename_list, look_for):
    message_filename = os.path.join(temp_folder, 'xgettext.txt')
    if os.path.exists(message_filename):
        os.remove(message_filename)
    message_file = open(message_filename, 'w')
    message_file.close()

    for source_filename in source_filename_list:
        if look_for == LOOK_FOR_gettext:
            command_xgettext = ['/usr/bin/xgettext', '--omit-header', '-j', '-o', message_filename, source_filename]
        elif look_for == LOOK_FOR_gettext_id:
            command_xgettext = ['/usr/bin/xgettext', '--omit-header', '-j', '-o', message_filename, '--keyword=gettext_id', '--keyword', source_filename]
        elif look_for == LOOK_FOR_deftext_id:
            command_xgettext = ['/usr/bin/xgettext', '--omit-header', '-j', '-o', message_filename, '--keyword=deftext_id', '--keyword', source_filename]
            
        (rc, stdout_result) = lib.launch_process.launch_command(command_xgettext, shell=True)
        if rc != 0:
            print "Error found when building messagelist"
            print stdout_result
            
    message_file = open(message_filename, 'r')
    message_file_lines = message_file.readlines()
    message_file.close()
    messages_and_locations = []
    
    message_state = STATE_LOOKING_FOR_MSGID
    message_work = ''
    message_work_locations = []
    for message_file_line in message_file_lines:
        if message_file_line.startswith('#:'):
            message_file_line_elements = message_file_line.split('#:')
            message_work_locations.append(message_file_line_elements[1].strip())

        if message_file_line.startswith('msgid'):
            message_state = STATE_LOOKING_FOR_MSGSTR
        if message_file_line.startswith('msgstr'):
            message_state = STATE_LOOKING_FOR_MSGID
            
        if message_state == STATE_LOOKING_FOR_MSGSTR:
            if message_file_line.startswith('msgid'):
                message_file_line_elements = message_file_line.split('msgid ')
                message = message_file_line_elements[1].strip()
            else:
                message = message_file_line
            if message.endswith('\n'):
                message = message [:len(message)-1]
            if message.endswith('"'):
                message = message [1:len(message)-1]
            message_work += message

        if message_state == STATE_LOOKING_FOR_MSGID:
            if message_work != '':
                messages_and_locations.append((message_work, message_work_locations))
                message_work = ''
                message_work_locations = []
    return messages_and_locations

def _extract_filenames(locations):
    result = []
    for location in locations:
        location_line = location.find(':')
        if location_line > 0:
            result.append(location[:location_line])
        else:
            result.append(location)
    return result

def _filenames_to_modulenames(filenames_abs):
    modulenames = []
    for filename_abs in filenames_abs:
        modulename = os.path.relpath(filename_abs, dev_env_py_root)
        modulename = modulename.replace(os.path.sep, '.')
        
        if modulename.endswith('.__init__.py'):
            modulename__elements = modulename.split('.__init__.py')
            modulename = modulename__elements[0].strip()
        
        if modulename.endswith('.py'):
            modulename__elements = modulename.split('.py')
            modulename = modulename__elements[0].strip()
        modulenames.append(modulename)
    return modulenames
    
    
def _extract_messages_from_defs(def_filenames):
    message_defs = {}
    def_modulenames = _filenames_to_modulenames(def_filenames)
    for def_modulename in def_modulenames:
        py_module = __import__(def_modulename, fromlist=[''], globals=globals(), locals=locals())
    for (message_id, message, message_args_description) in lib.dictionary._deftexts:
        message_defs[message_id] = message
    return message_defs
    
    
    
    
    

def command_generate_default(dictionary_folder):
    print "Extracting en_source from source files"
    temp_folder = os.path.join(dev_env_root, 'GTesting')
    py_filelist = []
    py_filelist.extend(build_filelist(os.path.join(dev_env_py_root, 'components'), ['.py']))
    py_filelist.extend(build_filelist(os.path.join(dev_env_py_root, 'lib'), ['.py']))
    py_filelist.extend(build_filelist(os.path.join(dev_env_py_root, 'appl'), ['.py']))
    py_filelist.extend(build_filelist(os.path.join(dev_env_py_root, 'plugin_modules'), ['.py']))
    py_filelist.extend(build_filelist(os.path.join(dev_env_py_root, 'plugin_types'), ['.py']))
    
    messagelist = build_messagelist(temp_folder, py_filelist, look_for=LOOK_FOR_gettext)
    messagelist_by_id = build_messagelist(temp_folder, py_filelist, look_for=LOOK_FOR_gettext_id)
    messagelist_by_id_def = build_messagelist(temp_folder, py_filelist, look_for=LOOK_FOR_deftext_id)

    dictionary = lib.dictionary.dictionary_spec.Dictionary()
    dictionary_header = lib.dictionary.dictionary_spec.DictionaryHeader()
    dictionary_header.lang_name = DICTIONARY_DEFAULT_NAME
    dictionary_header.lang_sub_name = DICTIONARY_DEFAULT_SUB_NAME
    dictionary.header = dictionary_header
    
    dictionary_translations = lib.dictionary.dictionary_spec.DictionaryTranslations()
    for (message, locations) in messagelist:
        dictionary_translation = lib.dictionary.dictionary_spec.DictionaryTranslation()
        dictionary_translation.message = message
        dictionary_translation.translation = message
        dictionary_translations.translations.append(dictionary_translation)

    def_filenames = set()
    for (message_id, locations) in messagelist_by_id_def:
        def_filenames.update(_extract_filenames(locations))
    message_defs = _extract_messages_from_defs(def_filenames)

    for (message_id, locations) in messagelist_by_id:
        dictionary_translation = lib.dictionary.dictionary_spec.DictionaryTranslationById()
        dictionary_translation.set_message_id(message_id)
        if message_defs.has_key(message_id):
            dictionary_translation.translation = message_defs[message_id]
        else:
            dictionary_translation.translation = "No translation available"
        dictionary_translations.translations_by_id.append(dictionary_translation)
    dictionary.translations = dictionary_translations

    dictionary_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(DICTIONARY_DEFAULT_NAME, DICTIONARY_DEFAULT_SUB_NAME)) 
    dictionary_file = open(dictionary_filename_abs, 'w')
    dictionary.to_tree().write(dictionary_file, encoding='utf-8')
    dictionary_file.close()
    print "Done"



def command_generate_auto(dictionary_folder, dictionary_from_filename_abs, lang_name_to, lang_sub_name_to, translator):
    print "  ", lang_name_to, lang_sub_name_to

    dictionary_from_file = open(dictionary_from_filename_abs, 'r')
    dictionary_from_spec = lib.dictionary.dictionary_spec.Dictionary.parse_from_file(dictionary_from_file)
    dictionary_from_file.close()
    
    dictionary_from_spec.header.lang_name = lang_name_to
    dictionary_from_spec.header.lang_sub_name = lang_sub_name_to
    dictionary_from_spec.header.description = translator.description
    
    for translation in dictionary_from_spec.translations.translations:
        translation.translation = translator.translate(translation.message)

    for translation_by_id in dictionary_from_spec.translations.translations_by_id:
        translation_by_id.translation = translator.translate(translation_by_id.translation)

    dictionary_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(lang_name_to, lang_sub_name_to)) 
    dictionary_file = open(dictionary_filename_abs, 'w')
    dictionary_from_spec.to_tree().write(dictionary_file, encoding='utf-8')
    dictionary_file.close()


def command_generate_auto_dev(dictionary_folder):
    print "Generating auto translated xx dictionary from en_source"

    dictionary_from_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(DICTIONARY_DEFAULT_NAME, DICTIONARY_DEFAULT_SUB_NAME)) 
    command_pp_xml_file(dictionary_from_filename_abs)

    translator = lib.dictionary.auto_translate.AutoTranslatorDev()
    command_generate_auto(dictionary_folder, dictionary_from_filename_abs, 'dev', None, translator)

    dictionary_dev_from_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename('dev')) 
    command_pp_xml_file(dictionary_dev_from_filename_abs)

    print "Done"
    

def command_pp_xml_file(xml_filename):
    command = ['xmllint', '--format', xml_filename, '--output', xml_filename]
    (rc, stdout_result) = lib.launch_process.launch_command(command, shell=True)
    if rc != 0:
        print "Error found when building messagelist"
        print stdout_result


def command_generate_autos(dictionary_folder):
    print "Generating auto translated dictionaries from en_source"
    auto_langs = [('da','auto'), ('de', 'auto'), ('es', 'auto')]
    auto_langs = [('da','auto')]

    dictionary_from_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(DICTIONARY_DEFAULT_NAME, DICTIONARY_DEFAULT_SUB_NAME)) 

    for (lang_name_to, lang_sub_name_to) in auto_langs:
        translator = lib.dictionary.auto_translate.AutoTranslatorBabelfish(lang_name_to)
        command_generate_auto(dictionary_folder, dictionary_from_filename_abs, lang_name_to, lang_sub_name_to, translator)
    print "Done"
    

def command_generate_bi(dictionary_folder, filename_first, filename_second, filename_result):

    dictionary_first_file = open(filename_first, 'r')
    dictionary_first = lib.dictionary.Dictionary.create_from_spec_fp(None, dictionary_first_file)
    dictionary_first_file.close()

    dictionary_second_file = open(filename_second, 'r')
    dictionary_second = lib.dictionary.Dictionary.create_from_spec_fp(None, dictionary_second_file)
    dictionary_second_file.close()


    dictionary_base_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(DICTIONARY_DEFAULT_NAME, DICTIONARY_DEFAULT_SUB_NAME)) 
    dictionary_base_file = open(filename_first, 'r')
    dictionary_base_spec = lib.dictionary.dictionary_spec.Dictionary.parse_from_file(dictionary_base_filename_abs)
    dictionary_base_file.close()

    for translation in dictionary_base_spec.translations.translations:
        translation.translation = "%s / %s" % (dictionary_first.gettext(translation.message), dictionary_second.gettext(translation.message))

    dictionary_file = open(filename_result, 'w')
    dictionary_base_spec.to_tree().write(dictionary_file, encoding='utf-8')
    dictionary_file.close()
    
    command_pp_xml_file(filename_result)
    


class CommandOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--generate_en_source',  action='store_true', help='Generate default dictionary from source')
        self._parser.add_option('--generate_autos', action='store_true', help='Generate auto dictionary from en_source')
        self._parser.add_option('--generate_bi', action='store_true', help='Generate bi-language dictionary from dictionaries given by --dict_first and --dict_second')

        self._parser.add_option('--dict_first', default="missing", help='Filename of first dictionary')
        self._parser.add_option('--dict_second', default="missing", help='Filename of second dictionary')
        self._parser.add_option('--dict_result', default="missing", help='Filename of result dictionary')

        (self.options, self.args) = self._parser.parse_args()

if __name__ == '__main__':
    command_options = CommandOptions()
    if command_options.options.generate_en_source:
        command_generate_default(dev_env_root_auto_dicts)
        command_generate_auto_dev(dev_env_root_auto_dicts)

    elif command_options.options.generate_autos:
        command_generate_autos(dev_env_root_auto_dicts)

    elif command_options.options.generate_bi:
        filename_first = command_options.options.dict_first
        filename_second = command_options.options.dict_second
        filename_result = command_options.options.dict_result
        if not os.path.exists(filename_first):
            print "filename given with argument --dict_first %s not found" % filename_first
            exit(1)
        if not os.path.exists(filename_second):
            print "filename given with argument --dict_second %s not found" % filename_second
            exit(1)
        command_generate_bi(dev_env_root_auto_dicts, filename_first, filename_second, filename_result)
        
        
        
    else:
        print "Invallid command"
    
    