import os.path
import pstats

if __name__ == '__main__':
    
    filename = 'client_cb_menu_item_selected-2009-10-12_12_17_09.102000.prof'
    filename_abs = os.path.join(os.path.dirname(__file__), 'prof', filename)
    
    p_data = pstats.Stats(filename_abs)
    p_data.sort_stats('cumulative').print_stats(30)
