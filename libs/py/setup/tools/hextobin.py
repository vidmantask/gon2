import binascii
import sys

filename = sys.argv[1]

file_hex = open(filename, 'r');

print binascii.unhexlify(file_hex.read())
file_hex.close()
