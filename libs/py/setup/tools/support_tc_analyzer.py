'''
Demo to show use of the engineering Formatter.
'''
import xml.dom.minidom
import tempfile
import os
import optparse
from duplicity.tempdir import default


class CommandOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--filename',  help='Filename of file holding tc-data')
        self._parser.add_option('--session_id',  default=None, help='Filter on session id')
        (self.options, self.args) = self._parser.parse_args()

    def get_filename(self):
        return self.options.filename
    
    def get_session_id(self):
        return self.options.session_id
    



def load_dom(filename):
    cp_file = open(filename, 'r')
    
    (_, temp_filename) = tempfile.mkstemp()
    temp_file = open(temp_filename, 'w')
    
    temp_file.write('<?xml version="1.0" encoding="UTF-8"?>')
    temp_file.write('<gon_traffic_control>')
    temp_file.write(cp_file.read())
    temp_file.write('</gon_traffic_control>')
    cp_file.close()
    temp_file.close()

    dom = xml.dom.minidom.parse(temp_filename)
    os.unlink(temp_filename)
        
    return dom
    

class TCTick(object):
    counter = 0
    
    def __init__(self):
        self.in_read_size = 0
        self.in_buffer_size = 0
        self.in_buffer_transit_min_ms = 0
        self.in_buffer_transit_max_ms = 0
        self.in_write_size = 0
        self.out_read_size = 0
        self.out_buffer_size = 0
        self.out_buffer_transit_min_ms = 0
        self.out_buffer_transit_max_ms = 0
        self.out_write_size = 0
        
        self.in_bandwidth_throttling_state = 0
        self.in_current_ping_durration = 0
        self.out_bandwidth_throttling_state = 0
        self.out_current_ping_durration = 0
        
        self.in_interval_read_size_sec = 0
        self.out_interval_read_size_sec = 0
        
        self.in_interval_read_bucket_size = 0
        self.in_interval_read_bucket_count = 0
        self.in_interval_read_delay_ms = 0
        self.out_interval_read_bucket_size = 0
        self.out_interval_read_bucket_count = 0
        self.out_interval_read_delay_ms = 0
        
        self.session_id = ""
        self.ts = ""
        self.id = TCTick.counter
        TCTick.counter += 1

    def parse_interval_in(self, cp_tag):
        self.in_read_size = int(cp_tag.getAttribute('read_size'))
        self.in_buffer_size = int(cp_tag.getAttribute('buffer_size'))
        self.in_buffer_transit_min_ms = int(cp_tag.getAttribute('buffer_transit_min_ms'))
        self.in_buffer_transit_max_ms = int(cp_tag.getAttribute('buffer_transit_max_ms'))
        self.in_write_size = int(cp_tag.getAttribute('write_size'))
        
    def parse_interval_out(self, cp_tag):
        self.out_read_size = int(cp_tag.getAttribute('read_size'))
        self.out_buffer_size = int(cp_tag.getAttribute('buffer_size'))
        self.out_buffer_transit_min_ms = int(cp_tag.getAttribute('buffer_transit_min_ms'))
        self.out_buffer_transit_max_ms = int(cp_tag.getAttribute('buffer_transit_max_ms'))
        self.out_write_size = int(cp_tag.getAttribute('write_size'))
        
    def parse_interval_state(self, cp_tag):
        self.in_bandwidth_throttling_state = int(cp_tag.getAttribute('in_bandwidth_throttling_state'))
        self.out_bandwidth_throttling_state = int(cp_tag.getAttribute('out_bandwidth_throttling_state'))

    def parse_interval_future(self, cp_tag):
        self.in_interval_read_size_sec = int(cp_tag.getAttribute('in_interval_read_size_sec'))
        self.out_interval_read_size_sec = int(cp_tag.getAttribute('out_interval_read_size_sec'))
        
    def parse_interval_future_bucket(self, cp_tag):
        self.in_interval_read_bucket_size = int(cp_tag.getAttribute('in_interval_read_bucket_size'))
        self.in_interval_read_bucket_count = int(cp_tag.getAttribute('in_interval_read_bucket_count'))
        self.in_interval_read_delay_ms = int(cp_tag.getAttribute('in_interval_read_delay_ms'))
        self.out_interval_read_bucket_size = int(cp_tag.getAttribute('out_interval_read_bucket_size'))
        self.out_interval_read_bucket_count = int(cp_tag.getAttribute('out_interval_read_bucket_count'))
        self.out_interval_read_delay_ms = int(cp_tag.getAttribute('out_interval_read_delay_ms'))
        
        

def parse_cps(dom, session_id):
    tc_ticks = []
    current_tc_tick = None
    
    for cp_tag in dom.getElementsByTagName('cp'):
        attr_cp_id = cp_tag.getAttribute('checkpoint_id')
        attr_session_id = cp_tag.getAttribute('session_id')
        
        if session_id is not None and session_id != attr_session_id:
            continue
        
        if attr_cp_id == 'tick.interval_in':
            if current_tc_tick is not None:
                tc_ticks.append(current_tc_tick)
            current_tc_tick = TCTick()
            current_tc_tick.parse_interval_in(cp_tag)
        
        elif attr_cp_id == 'tick.interval_out':
            current_tc_tick.parse_interval_out(cp_tag)
        
        elif attr_cp_id == 'tick.interval_state':
            current_tc_tick.parse_interval_state(cp_tag)

        elif attr_cp_id == 'tick.interval_future':
            current_tc_tick.parse_interval_future(cp_tag)

        elif attr_cp_id == 'tick.interval_future_bucket':
            current_tc_tick.parse_interval_future_bucket(cp_tag)
        
    if current_tc_tick is not None:
        tc_ticks.append(current_tc_tick)
        
    return tc_ticks



import matplotlib.pyplot as plt




def analyse_and_show(filename, session_id):
    
    dom = load_dom(filename)
    tc_ticks = parse_cps(dom, session_id)
    
    
    xs = [ tc_tick.id for tc_tick in tc_ticks ]
    
    y_in_read   = [ tc_tick.in_read_size   for tc_tick in tc_ticks ]
    y_in_write  = [ tc_tick.in_write_size  for tc_tick in tc_ticks ]
    y_in_buffer = [ tc_tick.in_buffer_size for tc_tick in tc_ticks ]
    y_in_current_ping_durration  = [ tc_tick.in_current_ping_durration for tc_tick in tc_ticks ]
    y_in_interval_read_size_sec  = [ tc_tick.in_interval_read_size_sec  for tc_tick in tc_ticks ]
    y_in_buffer_transit_min_ms  = [ tc_tick.in_buffer_transit_min_ms  for tc_tick in tc_ticks ]
    y_in_buffer_transit_max_ms  = [ tc_tick.in_buffer_transit_max_ms  for tc_tick in tc_ticks ]
    

    y_in_interval_read_bucket_size  = [ tc_tick.in_interval_read_bucket_size  for tc_tick in tc_ticks ]
    y_in_interval_read_bucket_count  = [ tc_tick.in_interval_read_bucket_count  for tc_tick in tc_ticks ]
    y_in_interval_read_delay_ms  = [ tc_tick.in_interval_read_delay_ms  for tc_tick in tc_ticks ]
    y_out_interval_read_bucket_size  = [ tc_tick.out_interval_read_bucket_size  for tc_tick in tc_ticks ]
    y_out_interval_read_bucket_count  = [ tc_tick.out_interval_read_bucket_count  for tc_tick in tc_ticks ]
    y_out_interval_read_delay_ms  = [ tc_tick.out_interval_read_delay_ms  for tc_tick in tc_ticks ]
    
    
    y_out_read   = [ tc_tick.out_read_size   for tc_tick in tc_ticks ]
    y_out_write  = [ tc_tick.out_write_size  for tc_tick in tc_ticks ]
    y_out_buffer = [ tc_tick.out_buffer_size for tc_tick in tc_ticks ]
    y_out_current_ping_durration  = [ tc_tick.out_current_ping_durration for tc_tick in tc_ticks ]
    y_out_interval_read_size_sec  = [ tc_tick.out_interval_read_size_sec for tc_tick in tc_ticks ]
    y_out_buffer_transit_min_ms  = [ tc_tick.out_buffer_transit_min_ms  for tc_tick in tc_ticks ]
    y_out_buffer_transit_max_ms  = [ tc_tick.out_buffer_transit_max_ms  for tc_tick in tc_ticks ]

    
    plt.figure(1)
    plt.plot(xs, y_out_read, "r", xs, y_out_write, "g", xs, y_out_buffer, "b", xs, y_out_interval_read_size_sec, 'c')
    plt.legend( ('A-Read', 'G-Write', 'buffer', 'limit-read') )
    plt.title('Out Flow G <-- A')

    plt.figure(2)
    plt.plot(xs, y_in_read, "r", xs, y_in_write, "g", xs, y_in_buffer, "b", xs, y_in_interval_read_size_sec, 'c')
    plt.legend( ('A-Read', 'G-Write', 'buffer', 'limit-read') )
    plt.title('Out Flow A <-- G')

    plt.figure(3)
    plt.plot(xs, y_out_buffer_transit_min_ms, "r", xs, y_out_buffer_transit_max_ms)
    plt.legend( ('Min', 'Max') )
    plt.title('Buffer transit G <-- A')

    plt.figure(4)
    plt.plot(xs, y_in_buffer_transit_min_ms, "r", xs, y_in_buffer_transit_max_ms)
    plt.legend( ('Min', 'Max') )
    plt.title('Buffer transit A <-- G')


    plt.figure(6)
    plt.plot(xs, y_out_interval_read_bucket_size, "r", xs, y_out_interval_read_bucket_count, "g", xs, y_out_interval_read_delay_ms, 'b')
    plt.title('Out Bucket')
    plt.legend( ('bucket-size', 'bucket-count', 'delay') )

    plt.figure(7)
    plt.plot(xs, y_in_interval_read_bucket_size, "r", xs, y_in_interval_read_bucket_count, "g", xs, y_in_interval_read_delay_ms, 'b')
    plt.title('In Bucket')
    plt.legend( ('bucket-size', 'bucket-count', 'delay') )





def main():
    command_options = CommandOptions()

    filename = command_options.get_filename()
    
    session_id = command_options.get_session_id()

    if session_id is None:
        print "No session_id specified, the following session_ids are available:"
        dom = load_dom(filename)
        
        session_ids = set()
        for cp_tag in dom.getElementsByTagName('cp'):
            session_ids.add(cp_tag.getAttribute('session_id'))
        for session_id in session_ids:
            print "--session_id %s" % session_id
        
    else:
        analyse_and_show(filename, session_id)
    


#    ax = plt.subplot(112)
#    ax.plot(xs, yss)

#
#    ax = subplot(211)
#l = plot(t1, f(t1), 'bo', t2, f(t2), 'k--', markerfacecolor='green')
#grid(True)
#title('A tale of 2 subplots')
#ylabel('Damped oscillation')


    plt.show()
    
if __name__ == '__main__':
    main()
