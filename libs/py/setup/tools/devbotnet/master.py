"""
"""
import sys
import os.path
import optparse
import threading
import socket
import tempfile
import shutil
import ConfigParser
import json

import BaseHTTPServer
import mimetypes
import time
import datetime
import tarfile

#
#
#
dev_env_root = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..', '..'))
www_root = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'www_root'))



def gon_log(*argv):
    print('DevBotnet Master info : ' + ('%s'*len(argv)).lstrip() % argv)

def gon_log_error(*argv):
    print('DevBotnet Master error: ' + ('%s'*len(argv)).lstrip() % argv)

#
#
#
class CommandOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--listen_host', default="0.0.0.0", help='Listen host')
        self._parser.add_option('--listen_port', type=int, default=8091, help='Listen port')
        self._parser.add_option('--slave_win',    type='string', default=None, help='host:port of devbotnet slave for building windows')
        self._parser.add_option('--slave_mac',    type='string', default=None, help='host:port of devbotnet slave for building mac')
        self._parser.add_option('--slave_linux',  type='string', default=None, help='host:port of devbotnet slave for building linux')
        self._parser.add_option('--skip_clean',  action='store_true', default=False, help='Skip signing')
        self._parser.add_option('--skip_sign',  action='store_true', default=False, help='Skip signing')
        (self.options, self.args) = self._parser.parse_args()

    def get_listen(self):
        return (self.options.listen_host, self.options.listen_port)

    def devbotnet_slave_win(self):
        return self.options.slave_win

    def devbotnet_slave_mac(self):
        return self.options.slave_mac

    def devbotnet_slave_linux(self):
        return self.options.slave_linux

    def skip_sign(self):
        return self.options.skip_sign

    def skip_clean(self):
        return self.options.skip_clean

    def get_phases(self):
        return  [
           {'id': 0, 'name': 'get info',                'class': CommandGetInfo,                  'auto_next':True},
           {'id': 1, 'name': 'git pull',                'class': CommandGitPull,                  'auto_next':True},
           {'id': 2, 'name': 'git revision',            'class': CommandGitRevision,              'auto_next':True},
           {'id': 3, 'name': 'build cpp',               'class': CommandCmdBuildCpp,              'auto_next':True},
           {'id': 4, 'name': 'build',                   'class': CommandCmdBuild,                 'auto_next':True},
           {'id': 5, 'name': 'distribute',              'class': CommandDistributeBuild,          'auto_next':True},
           {'id': 6, 'name': 'build release',           'class': CommandCmdBuildRelease,          'auto_next':True},
         ]
    def create_builder(self, arg, id, phases=[0, 1, 2, 3, 4, 5]):
        (host, port) = arg.split(':')
        return {'id': id, 'host': host, 'port':port, 'phases':phases}

    def get_builders(self):
        id = 0
        builders = []

        if self.devbotnet_slave_mac() is not None:
            builders.append(self.create_builder(self.devbotnet_slave_mac(), id))
        if self.devbotnet_slave_linux() is not None:
            id += 1
            builders.append(self.create_builder(self.devbotnet_slave_linux(), id))
        if self.devbotnet_slave_win() is not None:
            id += 1
            builders.append(self.create_builder(self.devbotnet_slave_win(), id, [0, 1, 2, 3, 4]))
            id += 1
            builders.append(self.create_builder(self.devbotnet_slave_win(), id, [0, 6]))
        return builders



command_options = CommandOptions()


#
# Command classes for slaves
#
class CommandProxy(threading.Thread):
    def __init__(self, builder_id, builder_phase_id, host, port, command, do_load_response=True):
        threading.Thread.__init__(self)
        self.builder_id = builder_id
        self.builder_phase_id = builder_phase_id
        self.host = host
        self.port = port
        self.command = command
        self.done = False
        self.started = False
        self.do_load_response = do_load_response

        self.tempfolder = tempfile.mkdtemp()
        self.response_filename = os.path.join(self.tempfolder, 'response')
        self.response = None
        self.ts_start = None
        self.ts_stop = None
        self.error = False
        self.error_message = ''
        self.rc = 0

    def run(self):
        self.ts_start = datetime.datetime.now()
        self.started = True
        self.run_pre()
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            response_file = open(self.response_filename, 'wb')
            sock.connect((self.host, int(self.port)))
            sock.sendall(self.command)
            while True:
                data = sock.recv(1024)
                if not data:
                    break
                response_file.write(data)
        except Exception as error:
            print(error)
            self.rc = 100
            self.error = True
            self.error_message = 'Unable to connect'

        finally:
            sock.close()
            response_file.close()

        if not self.error:
            if self.do_load_response:
                self.response = self.get_response()

        if not self.error:
            self.run_post()

        self.cleanup()
        self.done = True
        self.ts_stop = datetime.datetime.now()


    def get_time_info(self):
        ts_stop = datetime.datetime.now()

        if self.ts_start is None:
            return ('', '')

        if not self.ts_stop is None:
            ts_stop = self.ts_stop

        start_str = self.ts_start.strftime('%Y.%m.%d %H:%M:%S')

        durration = (ts_stop - self.ts_start)

        durration_str = '0'
        if durration.total_seconds() > 0:
            durration_str = '%s' % durration

        return (start_str, durration_str)

    def run_pre(self):
        pass

    def run_post(self):
        pass

    def cleanup(self):
        shutil.rmtree(self.tempfolder)

    def is_done(self):
        return self.done

    def is_started(self):
        return self.started

    def get_response(self):
        response_file = open(self.response_filename)
        response = response_file.read()
        response_file.close()
        return response

    def get_response_note(self):
        if self.error:
            return self.error_message
        return ''

    def get_response_rc(self):
        return self.rc

    def has_additional_info(self):
        return False

    def get_additional_info(self):
        return ''



class CommandProxyExec(CommandProxy):
    def __init__(self, builder_id, builder_phase_id, host, port, command):
        CommandProxy.__init__(self, builder_id, builder_phase_id, host, port, 'exec %s' % command, True)

    def get_response(self):
        response_file = open(self.response_filename)
        response = response_file.read()
        response_file.close()
        return json.loads(response)

    def get_response_rc(self):
        if self.response is not None:
            return self.response['rc']
        return self.rc

    def get_std_out(self):
        if self.response is not None:
            return self.response['std_out']
        return None

    def get_std_err(self):
        if self.response is not None:
            return self.response['std_err']
        return None

    def has_additional_info(self):
        return self.is_done() and not self.error

    def get_additional_info(self):
        info = ''
        info += self.command + '\n\n'
        info += ('std_out '*10) + '\n'
        info += self.get_std_out() + '\n\n'
        info += ('std_err '*10) + '\n'
        info += self.get_std_err() + '\n\n'
        return info


class CommandGetInfo(CommandProxy):
    def __init__(self, builder_id, builder_phase_id, host, port):
        CommandProxy.__init__(self, builder_id, builder_phase_id, host, port, "get_file config_local.ini")
        self.devbotnet_name = None
        self.devbotnet_arch_target = None

    def run_post(self):
        try:
            config_local = ConfigParser.ConfigParser()
            config_local.read(self.response_filename)
            self.devbotnet_name = config_local.get('devbotnet', 'name')
            self.devbotnet_arch_target = config_local.get('devbotnet', 'arch_target')
        except:
            pass

    def get_response_note(self):
        if self.error:
            return self.error_message

        if self.is_done():
            return '%s(%s)' % (self.devbotnet_name, self.devbotnet_arch_target)
        return ''

class CommandGitRevision(CommandProxyExec):
    def __init__(self, builder_id, builder_phase_id, host, port):
        CommandProxyExec.__init__(self, builder_id, builder_phase_id, host, port, "git rev-parse HEAD")
        self.git_revision = None

    def run_post(self):
        try:
            self.git_revision = self.response['std_out'].split(' ')[0].strip()
        except:
            pass

    def get_response_note(self):
        if self.error:
            return self.error_message
        if self.is_done() and self.git_revision is not None:
            return self.git_revision
        return ''


class CommandGitPull(CommandProxyExec):
    def __init__(self, builder_id, builder_phase_id, host, port):
        CommandProxyExec.__init__(self, builder_id, builder_phase_id, host, port, "git pull")
        self.git_revision = None

    def run_post(self):
        try:
            self.git_revision = self.response['std_out'].split(' ')[0].strip()
        except:
            pass

    def get_response_note(self):
        if self.error:
            return self.error_message
        if self.is_done() and self.git_revision is not None:
            return self.git_revision
        return ''


class CommandCmdBuildCpp(CommandProxyExec):
    def __init__(self, builder_id, builder_phase_id, host, port):
        command = "python ../gsetup.py --cmd_build_cpp"
        if command_options.skip_clean():
            command += " --skip_clean"
        CommandProxyExec.__init__(self, builder_id, builder_phase_id, host, port, command)

class CommandCmdBuild(CommandProxyExec):
    def __init__(self, builder_id, builder_phase_id, host, port):
        command = "python ../gsetup.py --cmd_build"
        if command_options.skip_clean():
            command += " --skip_clean"
        if command_options.skip_sign():
            command += " --skip_sign"
        CommandProxyExec.__init__(self, builder_id, builder_phase_id, host, port, command)

class CommandCmdBuildRelease(CommandProxyExec):
    def __init__(self, builder_id, builder_phase_id, host, port):
        CommandProxyExec.__init__(self, builder_id, builder_phase_id, host, port, "python ../gsetup.py --cmd_build_release")

class CommandDistributeBuild(CommandProxy):
    def __init__(self, builder_id, builder_phase_id, host, port):
        CommandProxy.__init__(self, builder_id, builder_phase_id, host, port, "get_build_store")
        self.file_size = None
        self.files_note = ''

    def run_post(self):
        try:
            statinfo = os.stat(self.response_filename)
            self.file_size = statinfo.st_size
            gon_log("Dowloaded build_store file with size %d" % self.file_size)
            tarfilee = tarfile.TarFile(name=self.response_filename)
            tarfilee.extractall(path=os.path.join(dev_env_root, '..', 'gon_build', 'build_store'))
            for member in tarfilee.getmembers():
                self.files_note += "extracted %s size: %d from build_store\n" % (member.name, member.size)
                gon_log("extracted %s size: %d from build_store" % (member.name, member.size))
                tarfilee.close()
        except Exception as error:
            print(error)
            gon_log_error(error)
            self.error = True
            self.error_message = "File not downloaded"

    def get_response_note(self):
        if self.error:
            return self.error_message

        if self.is_done() and self.file_size is not None:
            return 'Filesize %d downloaded' % self.file_size
        return 'No file downloaded'

    def has_additional_info(self):
        return self.is_done() and self.file_size is not None

    def get_additional_info(self):
        return self.files_note



#
# Status handling
#
#
# Configuration of build
#



class CommandController(threading.Thread):
    JOBSTATE_READY = 'ready'
    JOBSTATE_RUNNING = 'running'
    JOBSTATE_RUNNING_WAIT = 'running_wait'
    JOBSTATE_DONE = 'done'

    def __init__(self, options):
        threading.Thread.__init__(self)
        self.done = False
        self.state = []
        self.current_phase = 0
        self.goto_next_phase = False
        self.do_stop = False
        self.job_state = CommandController.JOBSTATE_READY
        self.options = options

    def get_setup_phase(self, phase_id):
        for setup_phase in self.options.get_phases():
            if phase_id == setup_phase['id']:
                return setup_phase
        return None

    def get_setup_phase_last_id(self):
        return self.options.get_phases()[len(self.options.get_phases())-1]['id']

    def stop(self):
        self.do_stop = True

    def run(self):
        self._run_init_commands()
        while not self.do_stop and not self.goto_next_phase:
            time.sleep(2)

        self.job_state = CommandController.JOBSTATE_RUNNING

        self._start_current_phase()
        while not self.do_stop:
            if self._all_done_current_phase():
                current_phase = self.get_setup_phase(self.current_phase)
                if current_phase is None or current_phase['auto_next'] or self.goto_next_phase:
                    self.current_phase += 1
                    self._start_current_phase()
                    self.goto_next_phase = False
                    self.job_state = CommandController.JOBSTATE_RUNNING
                else:
                    self.job_state = CommandController.JOBSTATE_RUNNING_WAIT

            if self._all_done_current_phase() and self.current_phase > self.get_setup_phase_last_id():
                self.do_stop = True

            time.sleep(2)

        self.job_state = CommandController.JOBSTATE_DONE
        self.done = True

    def _run_init_commands(self):
        for builder in self.options.get_builders():
            builder_id = builder['id']
            host = builder['host']
            port = builder['port']

            state_builder_phases = []
            for builder_phase in builder['phases']:
                setup_phase = self.get_setup_phase(builder_phase)
                state_builder_phases.append( {'id':builder_phase, 'instance': setup_phase['class'](builder_id, builder_phase, host, port)})

            state_builder = {'id': builder['id'], 'host_port':'%s:%s' % (host, port), 'phases': state_builder_phases}
            self.state.append(state_builder)


    def _all_done_current_phase(self):
        for state_builder in self.state:
            for state_builder_phase in state_builder['phases']:
                if state_builder_phase['id'] == self.current_phase:
                    if not state_builder_phase['instance'].is_done():
                        return False
        return True

    def _start_current_phase(self):
        for state_builder in self.state:
            for state_builder_phase in state_builder['phases']:
                if state_builder_phase['id'] == self.current_phase:
                    state_builder_phase['instance'].start()


    def is_done(self):
        return self.done


    def get_bulder_phase(self, builder_id, phase_id):
        for state_builder in self.state:
            if state_builder['id'] == builder_id:
                for state_builder_phase in state_builder['phases']:
                    if state_builder_phase['id'] == phase_id:
                        return state_builder_phase
        return None


    def get_status(self):
        status_phases = []
        for setup_phase in self.options.get_phases():
            status = 'xx'

            if self.job_state == CommandController.JOBSTATE_DONE:
                status = 'cancled'
                if setup_phase['id'] <= self.current_phase:
                    status = 'done'
            elif self.job_state == CommandController.JOBSTATE_READY:
                status = 'waiting'
                pass
            else:
                if setup_phase['id'] < self.current_phase:
                    status = 'done'
                elif setup_phase['id'] == self.current_phase:
                    if self._all_done_current_phase():
                        status = "waiting_to_proceed"
                    else:
                        status = 'running'
                else:
                    status = 'waiting'

            status_phases.append({'id':setup_phase['id'], 'name':setup_phase['name'], 'status':status})

        status_builders = []
        for builder in self.options.get_builders():
            status_builder_phases = []
            for setup_phase in self.options.get_phases():
                builder_phase = self.get_bulder_phase(builder['id'], setup_phase['id'])
                status = 'na'
                note = ''
                has_additional_info = False

                time_info_start = ''
                time_info_durration = ''

                if builder_phase is not None:
                    (time_info_start, time_info_durration) = builder_phase['instance'].get_time_info()

                    if builder_phase['instance'].is_done():
                        status = 'done'
                        note = builder_phase['instance'].get_response_note()
                        has_additional_info = builder_phase['instance'].has_additional_info()
                        if builder_phase['instance'].get_response_rc() != 0:
                            status = 'error'

                    elif builder_phase['instance'].is_started():
                        status = 'running'
                    else:
                        status = 'waiting'
                status_builder_phases.append({'id':setup_phase['id'], 'status':status, 'note':note, 'time_info_start':time_info_start, 'time_info_durration':time_info_durration, 'has_additional_info':has_additional_info})
            status_builder = {'id': builder['id'], 'name':builder['host'], 'phases':status_builder_phases}
            status_builders.append(status_builder)
        status = {'phases': status_phases, 'builders':status_builders, 'state':self.job_state}
        return status


    def get_additional_info(self, builder_id, builder_phase_id):
        builder_phase = self.get_bulder_phase(builder_id, builder_phase_id)
        if builder_phase is None:
            return 'no additional info'
        return builder_phase['instance'].get_additional_info()


#
# WebServer
#
command_controller = CommandController(command_options)


class WebServerRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path.startswith('/devbotnet/data/status'):
            self.do_GET_data_status()
            return
        if self.path.startswith('/devbotnet/command/start'):
            self.do_GET_command_start()
            return
        if self.path.startswith('/devbotnet/command/continue'):
            self.do_GET_command_continue()
            return
        if self.path.startswith('/devbotnet/command/stop'):
            self.do_GET_command_stop()
            return

        if self.path.startswith('/devbotnet/command/get_additional_info'):
            self.do_GET_command_get_additional_info()
            return

        if self.path.startswith('/devbotnet/css'):
            url = os.path.join(www_root, 'devbotnet.css')
            mime_type = 'text/css'
        else:
            url = os.path.join(www_root, 'index.html')
            mime_type = 'text/html'

        if os.path.isfile(url):
            url_file = open(url, 'rb')
            url_file_content = url_file.read()
            url_file.close()

            self.send_response(200)
            self.send_header('Content-Type', mime_type)
            self.send_header('Content-Length', str(len(url_file_content)))
            self.end_headers()

            self.wfile.write(url_file_content)
            self.wfile.flush()
            return
        self.send_error(404, 'File Not Found: %s' % url)


    def do_GET_data_status(self):
        global command_controller
        response = command_controller.get_status()
        self._send_response(response)

    def do_GET_command_start(self):
        global command_controller
        if command_controller.is_done():
            command_controller = CommandController(command_options)
            command_controller.start()
            response = "ok"
            self._send_response(response)
        else:
            response = "ignored"
            self._send_response(response)

    def do_GET_command_continue(self):
        global command_controller
        command_controller.goto_next_phase = True
        response = "ok"
        self._send_response(response)

    def do_GET_command_stop(self):
        global command_controller
        command_controller.do_stop = True
        response = "ok"
        self._send_response(response)


    def do_GET_command_get_additional_info(self):
        path_elements = self.path.split('/')
        builder_id = int(path_elements[len(path_elements)-2])
        builder_phase_id = int(path_elements[len(path_elements)-1])
        global command_controller
        response = command_controller.get_additional_info(builder_id, builder_phase_id)
        self._send_response(response)

    def log_message(self, format, *args):
        gon_log(format % args)

    def _send_callback_response(self, callback, arg):
        response = '%s(%s)' % (callback,json.dumps(arg))
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', str(len(response)))
        self.send_header('Cache-Control', 'max-age=0')
        self.end_headers()
        self.wfile.write(response)
        self.wfile.flush()

    def _send_response(self, arg):
        response = '%s' % (json.dumps(arg))
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', str(len(response)))
        self.send_header('Cache-Control', 'max-age=0')
        self.end_headers()
        self.wfile.write(response)
        self.wfile.flush()



def main():
    global command_controller
    command_controller.start()

    try:
        gon_log("listen on %s:%d " % command_options.get_listen())
        web_server = BaseHTTPServer.HTTPServer(command_options.get_listen(), WebServerRequestHandler)
        web_server.serve_forever()
    finally:
        command_controller.stop()
        command_controller.join(10)


if __name__ == '__main__':
    main()
