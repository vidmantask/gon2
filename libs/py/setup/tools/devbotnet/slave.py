"""
"""
import sys
import os.path
import optparse
import SocketServer
import socket
import subprocess
import datetime
import traceback
import json
import tarfile
import os
import glob

dev_env_root = os.path.normpath(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..', '..', '..'))

def gon_log(*argv):
    print('DevBotnet Slave  info : ' + ('%s'*len(argv)).lstrip() % argv)

def gon_log_error(*argv):
    print('DevBotnet Slave  error: ' + ('%s'*len(argv)).lstrip() % argv)


def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP


TARGET_PLATFORM_MAC    = "mac_64"
TARGET_PLATFORM_LINUX  = "linux_64"
TARGET_PLATFORM_WIN    = "win_32"
def detect_platform():
    if sys.platform == "win32":
        return TARGET_PLATFORM_WIN
    elif sys.platform in [ "linux2", "linux" ]:
        return TARGET_PLATFORM_LINUX
    elif sys.platform == "darwin":
        return TARGET_PLATFORM_MAC
    return "unknown"


def read_content(filename):
    the_file = open(filename, 'rb')
    content = the_file.read()
    the_file.close()
    return content

class CommandOptions(object):
    def __init__(self):
        self._parser = optparse.OptionParser()
        self._parser.add_option('--listen_host', default="0.0.0.0", help='Listen host')
        self._parser.add_option('--listen_port', type=int, default=6090, help='Listen port')
        (self.options, self.args) = self._parser.parse_args()

    def get_listen(self):
        return (self.options.listen_host, self.options.listen_port)


class DevBotNetSlaveTCPHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        command = self.request.recv(1024).strip()
        if command.startswith('get_file'):
            return self.handle_command_get_file(command)
        if command.startswith('exec'):
            return self.handle_command_exec(command)
        if command.startswith('get_build_store'):
            return self.handle_command_get_build_store(command)
        gon_log_error("ERROR: Invalid command '%s'" % command)

    def handle_command_get_file(self, command):
        filename = command[command.find(' ')+1:]
        filename_abs = os.path.join(dev_env_root, filename)
        gon_log("%s begin get_file %s" % (datetime.datetime.now(), filename_abs))
        if os.path.exists(filename_abs):
            thefile = open(filename_abs, 'r')
            self.request.sendall(thefile.read())
            thefile.close()
            gon_log("%s done get_file %s" % (datetime.datetime.now(), filename_abs))
        else:
            gon_log_error("ERROR: get_file, invalid filename %s" % filename_abs)

    def handle_command_exec(self, command):
        std_out_filename = os.path.join(dev_env_root, 'devbotnet_exec.std_out')
        std_err_filename = os.path.join(dev_env_root, 'devbotnet_exec.std_err')

        command = command[command.find(' ')+1:]
        gon_log("%s begin exec %s" % (datetime.datetime.now(), command))
        try:
            command = command + ' > %s 2> %s' % (std_out_filename, std_err_filename)
            process = subprocess.Popen(command, cwd=dev_env_root, close_fds=True, shell=True)
            returncode = process.wait()
            std_out = read_content(std_out_filename)
            std_err = read_content(std_err_filename)
            self._send_command_exec_result(std_out, std_err, returncode)
            gon_log("%s rc:%d" % (datetime.datetime.now(), returncode))
        except Exception as error:
            gon_log_error(error)
            self._send_command_exec_result('', traceback.format_exc(), 99)
        gon_log("%s done" % datetime.datetime.now())
        gon_log("%s done exec %s" % (datetime.datetime.now(), command))

    def handle_command_get_build_store(self, command):
        gon_log("%s begin get_build_store" % (datetime.datetime.now()))
        tarfilename = os.path.join(dev_env_root, '..', 'gon_build', 'ditribution.tar.gz')
        tarfilee = tarfile.TarFile(name=tarfilename, mode='w')
        build_store_root = os.path.join(dev_env_root, '..', 'gon_build', 'build_store')
        for root, dirs, files in os.walk(build_store_root):
            path = root.split(os.sep)
            for filename in files:
                arcname = os.path.relpath(os.path.join(root, filename), build_store_root)
                gon_log(filename, arcname)
                tarfilee.add(os.path.join(root, filename), arcname=arcname)
        tarfilee.close()
        if os.path.exists(tarfilename):
            thefile = open(tarfilename, 'rb')
            self.request.sendall(thefile.read())
            thefile.close()
            gon_log("%s done get_build_store" % (datetime.datetime.now()))
        else:
            gon_log_error("ERROR: get_file, invalid filename %s" % filename_abs)

    def _send_command_exec_result(self, std_out, std_err, rc):
        result = {'std_out': std_out, 'std_err':std_err, 'rc':rc}
        self.request.sendall(json.dumps(result))

def main():
    options = CommandOptions()
    gon_log("platform %s" % detect_platform())
    gon_log("ip %s " % get_ip())
    gon_log("listen on %s:%d " % options.get_listen())

    try:
        server = SocketServer.TCPServer(options.get_listen(), DevBotNetSlaveTCPHandler)
        server.serve_forever()
    except Exception as error:
        gon_log_error(error)
    gon_log("stopped")
    return 1

if __name__ == '__main__':
    main()
