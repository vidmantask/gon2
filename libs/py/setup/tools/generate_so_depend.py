'''
This module holds functionality for generating a list of dependend so files
'''
import os.path
import glob
import subprocess
import sys

FILENAME_GON_DO_DEPENDENCY  = 'generate_so_dependency_data.txt'

CHECK_PATH = ['/lib', '/usr/lib']

IGNORE_MODULES = ['ld-linux.so', 'linux-gate.so']


def generate_dependency(base_folder):
    print "generate_dependency, looking for all *.so files in '%s'" % base_folder
    
    base_folder_with_glob = os.path.join(base_folder, '*.so')
    
    ldd_base_modules = set()
    ldd_modules = set()
    for filename in glob.glob(base_folder_with_glob):
        ldd_base_modules.add(os.path.basename(filename))
        
        args = ['ldd', filename]
        process = subprocess.Popen(args=args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
        process.wait()
        if process.returncode != 0:
            print "ERROR", process.returncode,process.communicate()[1] 
            return 

        ldd_output_lines = process.communicate()[0]
        for ldd_output_line in ldd_output_lines.splitlines():
            ldd_output_line_parts = ldd_output_line.split('=>')
            ldd_module = ldd_output_line_parts[0].strip()
            ldd_modules.add(ldd_module)

    ldd_modules_before = ldd_modules.copy()
    for ldd_module in ldd_modules_before:
        for ignore_module in IGNORE_MODULES:
            if ldd_module.find(ignore_module) != -1:
                ldd_modules.remove(ldd_module)

    ldd_base_modules.difference_update(ldd_base_modules)
    print "generate_dependency, %d dependencies to other modules found" % len(ldd_modules)
    ldd_modules_filename  =  os.path.join(base_folder, FILENAME_GON_DO_DEPENDENCY)
    ldd_modules_file = open(ldd_modules_filename, 'w')
    for ldd_module in ldd_modules:
        ldd_modules_file.write("%s\n" % ldd_module)

    ldd_modules_file.close()
    print "generate_dependency, done"


def check_dependency(base_folder):
    print "check_dependency"
    ldd_modules = set()
    
    ldd_modules_filename  =  os.path.join(base_folder, FILENAME_GON_DO_DEPENDENCY)
    ldd_modules_file = open(ldd_modules_filename, 'r')
    for ldd_modules_file_line in ldd_modules_file.readlines():
        ldd_modules.add(ldd_modules_file_line.rstrip("\n"))

    missing_module_found = False
    for module_filename in ldd_modules:
        module_found = False
        for check_folder in CHECK_PATH:
            module_filename_abs = os.path.join(check_folder, module_filename)
            if os.path.exists(module_filename_abs):
                module_found = True
                break
        if not module_found:
            missing_module_found = True
            print "ERROR: Module not found '%s'" % module_filename

    if not missing_module_found:
        print "check_dependency, no missing modules found"
    print "check_dependency, done"

        
if __name__ == '__main__':
    base_folder = os.getcwd()
    if '--generate' in sys.argv:
        generate_dependency(base_folder)
        exit(0) 
    
    if '--check' in sys.argv:
        check_dependency(base_folder)
        exit(0) 

    print "Help, options are:"
    print "  --generate"
    print "  --check"
 