"""
This tool generates the first start file used by gon_client in dev_env to been able to connect to server
"""
import sys
import os.path
import base64
import pickle

dev_env_root = os.path.join(os.path.dirname(__file__), '..', '..', '..')
dev_env_py_plugin_modules_root = os.path.join(dev_env_root, 'py', 'plugin_modules')
dev_env_root_client  = os.path.join(dev_env_root, 'setup', 'dev_env', 'gon_installation')


import lib.checkpoint
import lib.appl.gon_client

import plugin_modules.endpoint.client_gateway_common

checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()

FILENAME_KNOWNSECRET  = 'gon_client.ks'
FILENAME_SERVERS = 'gon_client.servers' 

def read_file_content(file_root, filename):
    filename_abs = os.path.abspath(os.path.join(file_root, filename))
    file = open(filename_abs, 'rb')
    content = file.read()
    file.close()
    return content 

def generate_first_start(file_root):
    first_start_data = {}
    first_start_data['knownsecret'] = read_file_content(file_root, FILENAME_KNOWNSECRET)
    first_start_data['servers'] = read_file_content(file_root, FILENAME_SERVERS)

    restart_state = {}
    restart_state['first_start_operation'] = True
    restart_state['first_start_data'] = first_start_data
    

    restart_filename_folder = os.path.abspath(os.path.join('..', 'gon_temp'))
    if not os.path.exists(restart_filename_folder):
        os.makedirs(restart_filename_folder)
    
    restart_filename = os.path.abspath(os.path.join(restart_filename_folder, lib.appl.gon_client.GOnClientRestartHandler.GON_CLIENT_RESTART_FILENAME))
    restart_state_file = file(restart_filename, 'wb')
    restart_state_file.write(base64.standard_b64encode(pickle.dumps(restart_state)))
    restart_state_file.close()

    print "Files need for first start of gon_client generated"


def generate_token_entry(file_root):
    client_root = os.path.normpath(dev_env_root_client)
    
    endpoint_token = plugin_modules.endpoint.client_gateway_common.EndpointToken(checkpoint_handler, 'dev_endpoint')
    endpoint_token.initialize_token(client_root)
    endpoint_token.deploy_token(client_root,  read_file_content(file_root, FILENAME_KNOWNSECRET), read_file_content(file_root, FILENAME_SERVERS))

if __name__ == '__main__':
    
    if len(sys.argv) != 2:
        print "Invalid arguments"
        exit()

    root_of_files = sys.argv[1]
#    generate_first_start(root_of_files)
    generate_token_entry(root_of_files)
