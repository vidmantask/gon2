"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'], always_scan=config['debug'])
    map.minimization = False

    # The ErrorController route (handles 404/500 error pages); it should
    # likely stay at the top, ensuring it can always be resolved
    map.connect('/error/{action}', controller='error')
    map.connect('/error/{action}/{id}', controller='error')

    # CUSTOM ROUTES HERE
    map.connect('/', controller='pages', action='giri_repo')
    map.connect('/index', controller='pages', action='giri_repo')
    map.connect('/pages/index', controller='pages')
    map.connect('/pages/repo_summary/{repo}', controller='pages', action='repo_summary')
    map.connect('/pages/changeset_summary/{repo}/{changeset}', controller='pages', action='changeset_summary')
    map.connect('/pages/changeset/{repo}/{changeset}/{module_group}', controller='pages', action='changeset')
    map.connect('/pages/module/{repo}/{changeset}/{module}', controller='pages', action='module')
    map.connect('/pages/release_info/{repo}/{changeset}/{release_info_key}', controller='pages', action='release_info')

    map.connect('/pages/giri_repo/{repo}', controller='pages', action='giri_repo')
    map.connect('/pages/giri_repo_setup', controller='pages', action='giri_repo_setup')
    map.connect('/pages/giri_repo_job/{run_uid}', controller='pages', action='giri_repo_job')
    map.connect('/pages/giri_repo_job_part/{run_part_uid}', controller='pages', action='giri_repo_job_part')
    map.connect('/pages/giri_repo_job_part_detail_download/{run_part_detail_uid}/{filename}', controller='pages', action='giri_repo_job_part_detail_download')
    map.connect('/pages/giri_repo_job_part_detail_show/{run_part_detail_uid}/{filename}', controller='pages', action='giri_repo_job_part_detail_show')

    map.connect('/{controller}/{action}')
    map.connect('/{controller}/{action}/{uid}')

    return map
