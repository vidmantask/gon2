<%inherit file="/giri_base.mako"/>\

<%def name="mako_tag_header_links()">
  <a href="/index">home</a> 
  &#8594;
  <a href="/pages/giri_repo_job/${c.db_run.uid}">job</a> 
  &#8594;
  ${c.db_run_part.module} 
</%def>


<%def name="mako_tag_col_1()">
<div class="giri_box_with_margin_and_background" style="height:140px;">
<div class="giri_box_headline">
repo
</div>
<div class="giri_box_text giri_font_huge">
${c.repo}
</div>

<div class="giri_box_text">
<div class="giri_float_left giri_font_bold">
${c.hg_change.rev}
</div>
<div class="giri_float_left">
:${c.hg_change.node}
</div>
<div class="giri_float_end"></div>
${c.hg_change.desc}
</div>
%if c.hg_change_compare is not None:
	<div class="giri_box_text giri_font_small">
    <div class="giri_font_bold">compare with</div>
	<div class="giri_float_left giri_font_bold">
	${c.hg_change_compare.rev}
	</div>
	<div class="giri_float_left">
	:${c.hg_change_compare.node}
	</div>
	<div class="giri_float_end"></div>
	${c.hg_change_compare.desc}
	</div>
%endif
</div>

<div class="giri_box_with_margin_and_background" style="height:200px;">
<div class="giri_box_headline">
job part
</div>
<div class="giri_box_with_margin" id="gui_job_part">
</div>
</div>

</%def>

<%def name="mako_tag_col_2()">

<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
new job part details
</div>
<div class="giri_box_with_margin" id="gui_job_part_details_new">
</div>
</div>

<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
job part details
</div>
<div class="giri_box_with_margin" id="gui_job_part_details">
</div>
</div>

<div id="gui_job_part_details_module">
</div>

</%def>

<%def name="mako_tag_script()">
YUI().use("anim-base", "datasource-get", "datasource-jsonschema", "datasource-xmlschema", "datasource-polling", "datasource-function", "io-form", function(Y) {

	//
	// Functionality for job_part
	//
    job_part_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_job_part?run_part_uid=${c.db_run_part.uid}&"}),
    job_part_datasource_callback = {
        success: function(e) {
        	job_part_datasource_load_sucess(e.response.results[0]);
        },
        failure: function(e) {
        	job_part_datasource_load_error();
        }
    };
    function job_part_datasource_generate_html(job_part) {
    	var html = '';
    	html += '<div class="giri_box_text">';
    	html += '  ${c.db_run.computer} (${c.db_run.arch_target})';
    	html += '</div>';
    	html += help_format_generate_html_sum_boxes_with_ok(job_part);
    	html += '<div class="giri_float_end"></div>';
    	Y.one('#gui_job_part').setContent(html);
    }
    
    function job_part_datasource_load_sucess(data) {
    	job_part_datasource_generate_html(data);
    }
    
    function job_part_datasource_load_error() {
    }

    function gui_update_job_part() {
    	job_part_datasource.sendRequest({callback:job_part_datasource_callback});
	}

    
    
    //
	// Functionality for job_part_details
	//
    job_part_details_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_job_part_details?run_part_uid=${c.db_run_part.uid}&"}),
    job_part_details_datasource_callback = {
        success: function(e) {
        	job_part_details_datasource_load_sucess(e.response.results[0]);
        },
        failure: function(e) {
        	job_part_details_datasource_load_error();
        }
    };

    function job_part_details_datasource_generate_html_job_part_count_box(style) {
    	var html = '';
    	html += '<div class="giri_box_sum giri_float_right '+style+'" style="height:15px;">';
        html += '</div>';
    	return html;
    };

    function job_part_details_datasource_generate_html_detail(job_part_detail) {
    	var html = '';
    	
		html += '<div class="yui3-g">';
		html += '<div class="yui3-u-1-12">';
    	if(job_part_detail.line_begin > 0) {
        	html += '<div class="giri_text_box giri_font_small giri_font_bold">';
    		html += 'line ' + job_part_detail.line_begin;
        	if(job_part_detail.line_end != job_part_detail.line_begin) {
        		html += ' - ' + job_part_detail.line_end;
        	}
        	html += '</div>';
    	}
    	html += '</div>';
		html += '<div class="yui3-u-11-12">';
    	switch (job_part_detail.status) {
    	case 0:
    		html += help_format_generate_html_box_info();
    		break;
    	case 1:
    		html += help_format_generate_html_box_warning();
    		break;
    	case 2:
    		html += help_format_generate_html_box_error();
    		break;
    	case 3:
    		html += help_format_generate_html_box_ok();
    		break;
    	};
    	if(job_part_detail.name != "") {
        	html += '<div class="giri_text_box giri_font_small giri_font_bold">';
			html += job_part_detail.name;
	    	html += '</div>';
    	}
    	if(job_part_detail.data != "") {
	    	html += '<div class="giri_text_box">';
	    	html += '<pre class="giri_font_small">';
			html += job_part_detail.data;
	    	html += '</pre>';
	    	html += '</div>';
    	}
		html += '</div>';
		html += '</div>';
    	return html;
    }

    function job_part_details_datasource_generate_html(data) {
    	var job_part_details = data.details
    	var html = '';
    	var job_part_detail_count = 0;
    	html += '<div class="giri_table_no_select">';
    	html += '  <ul>'; 
		for (var i=0; i<job_part_details.length; i++) {
			var job_part_detail = job_part_details[i];
			if(job_part_detail.is_new) {
				job_part_detail_count = job_part_detail_count + 1;
				html += '<li class="giri_table_row_box">';
				html += job_part_details_datasource_generate_html_detail(job_part_detail);
				html += '</li>';
			}
    	}
    	html += '  </ul>'; 
    	html += '</div>';
    	if(job_part_detail_count == 0) {
    		html = "No new job part details";
    	}
    	Y.one('#gui_job_part_details_new').setContent(html);
    	
    	if(job_part_detail_count == job_part_details.length) {
        	Y.one('#gui_job_part_details').setContent('All is new');
    	}
    	else {
	    	var html = '';
	    	html += '<div class="giri_table_no_select">';
	    	html += '  <ul>'; 
			for (var i=0; i<job_part_details.length; i++) {
				var job_part_detail = job_part_details[i];
				html += '<li class="giri_table_row_box">';
				html += job_part_details_datasource_generate_html_detail(job_part_detail);
				html += '</li>';
	    	}
	    	html += '  </ul>'; 
	    	html += '</div>';
	    	Y.one('#gui_job_part_details').setContent(html);
    	}
    }

    function job_part_details_datasource_generate_html_module_data(data) {
    	var html = '';
    	var job_part_module_details = data.module_details
		for (var i=0; i<job_part_module_details.length; i++) {
			var job_part_module_detail = job_part_module_details[i];
	    	html += '<div class="giri_box_with_margin_and_background">';
	    	html += '<div class="giri_box_headline">';
	    	html += job_part_module_detail.name;
	    	html += ' (' + job_part_module_detail.data_size_kb +' kb)';
	    	
			html += '</div>';
	    	html += '<div class="giri_box_with_margin giri_float_right">';
    		html += '<a href="/pages/giri_repo_job_part_detail_download/'+job_part_module_detail.uid+'/'+job_part_module_detail.name+'">download</a>'; 
	    	html += '</div>';
	    	html += '<div class="giri_box_with_margin giri_float_right">';
    		html += '<a href="/pages/giri_repo_job_part_detail_show/'+job_part_module_detail.uid+'/'+job_part_module_detail.name+'">show</a>'; 
	    	html += '</div>';
	    	html += '<div class="giri_flow_end"></div>';
	    	if (job_part_module_detail.data_included) {
		    	html += '<div class="giri_box_with_margin">';
		    	html += '<pre class="giri_font_small">'
		    	html += job_part_module_detail.data_text;
		    	html += '</pre>'
		    	html += '</div>';
	    	}
	    	else {
	    	}
	    	html += '</div>';
    	}
    	Y.one('#gui_job_part_details_module').setContent(html);
    }
    
    function job_part_details_datasource_load_sucess(data) {
    	job_part_details_datasource_generate_html(data);
    	job_part_details_datasource_generate_html_module_data(data);
    }
    
    function job_part_details_datasource_load_error() {
    }

    function gui_update_job_part_details() {
    	job_part_details_datasource.sendRequest({callback:job_part_details_datasource_callback});
	}


    
	//
	// Main
	//
	function main() {
		gui_update_job_part();
		gui_update_job_part_details();
	}
	Y.on("domready", main);
});
</%def>

