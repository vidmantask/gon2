<%inherit file="/base.mako"/>\

<%def name="td_create(value, class_true, class_false)">
  % if value > 0:
    <td class="${class_true}">${value}</td>
  % else:
    <td class="${class_false}">${value}</td>
  % endif
</%def>

<%def name="header()">${c.title}</%def>

<h1>
Unittest
</h1>

<table class="stats" cellspacing="0">
<tr>
<td class="header" colspan="1" rowspan='2'>Module</td>
% for (slave_name, slave_arch_target) in c.ut_slaves:
  <td class="header" colspan="3">Unittest ${slave_name}(${slave_arch_target})</td>
% endfor
</tr>

<tr>
% for (slave_name, slave_arch_target) in c.ut_slaves:
  <td class="header_sub" colspan="1">Case Count</td>
  <td class="header_sub" colspan="1">Warnings</td>
  <td class="header_sub" colspan="1">Errors</td>
% endfor
</tr>

% for modulename in c.ut_modules:
<tr>
  <td>
    ${h.link_to(modulename, url(controller='pages', action='module', repo=c.repo, changeset=c.changeset, module=modulename))}
  </td>
% for (slave_name, slave_arch_target) in c.ut_slaves:
  ${td_create(c.ut_changeset_modules_summary[modulename][slave_name].counters_unittest.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(c.ut_changeset_modules_summary[modulename][slave_name].counters_unittest.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(c.ut_changeset_modules_summary[modulename][slave_name].counters_unittest.counter_error, 'higlight_error', 'higlight_fine')}
% endfor
</tr>
% endfor
</table>



<h1>
Static Analysis
</h1>

<table class="stats" cellspacing="0">
<tr>
<td class="header" colspan="1" rowspan='2'>Module</td>
% for (slave_name, slave_arch_target) in c.sa_slaves:
  <td class="header" colspan="3">Unittest ${slave_name}(${slave_arch_target})</td>
% endfor
</tr>

<tr>
% for (slave_name, slave_arch_target) in c.sa_slaves:
  <td class="header_sub" colspan="1">Line Count</td>
  <td class="header_sub" colspan="1">Warnings</td>
  <td class="header_sub" colspan="1">Errors</td>
% endfor
</tr>

% for modulename in c.sa_modules:
<tr>
  <td>
    ${h.link_to(modulename, url(controller='pages', action='module', repo=c.repo, changeset=c.changeset, module=modulename))}
  </td>
% for (slave_name, slave_arch_target) in c.sa_slaves:
  ${td_create(c.sa_changeset_modules_summary[modulename][slave_name].counters_unittest.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(c.sa_changeset_modules_summary[modulename][slave_name].counters_unittest.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(c.sa_changeset_modules_summary[modulename][slave_name].counters_unittest.counter_error, 'higlight_error', 'higlight_fine')}
% endfor
</tr>
% endfor
</table>


