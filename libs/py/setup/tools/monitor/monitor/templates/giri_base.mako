<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>

<title>Giritech Development</title>
  <link href="http://yui.yahooapis.com/3.3.0/build/cssreset/reset-min.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://yui.yahooapis.com/3.3.0/build/cssbase/base-min.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://yui.yahooapis.com/3.3.0/build/cssfonts/fonts-min.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="http://yui.yahooapis.com/3.3.0/build/cssgrids/grids-min.css" media="screen" rel="stylesheet" type="text/css" />
  ${h.stylesheet_link('/css/giri_base.css')}

  <script src="http://yui.yahooapis.com/3.3.0/build/yui/yui-min.js" type="text/javascript"/></script>
  <script src="/js/help_format.js" type="text/javascript"/></script>
  <script type="text/javascript">
    ${self.mako_tag_script()}
  </script>
</head>

<body>
  <div class="giri_header giri_float_left">
  Giritech Development
  </div>
  <div class="giri_header_small giri_float_left" style="width:10px;">
  </div>
  <div class="giri_header_small giri_float_left">
    ${self.mako_tag_header_links()}
  </div>
  <div class="giri_float_end">
  </div>

  
  <div class="yui3-g">
    <div class="yui3-u-1-4">
    <div class="giri_box_with_margin">
      ${self.mako_tag_col_1()}
    </div>
    </div>
    <div class="yui3-u-3-4">
    <div class="giri_box_with_margin">
      ${self.mako_tag_col_2()}
    </div>
    </div>
  </div>
</body>
</html>
