<%inherit file="/base.mako"/>\

<%def name="header()">${c.title}</%def>

% for repo in c.repos:
<h2>${h.link_to(repo, url(controller='pages', action='repo_summary', repo=repo))}</h2>
% endfor
