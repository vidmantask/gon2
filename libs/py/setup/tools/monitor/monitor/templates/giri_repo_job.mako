<%inherit file="/giri_base.mako"/>\

<%def name="mako_tag_header_links()">
<a href="/index">home</a>
&#8594;
job
</%def>

<%def name="mako_tag_col_1()">
<div class="giri_box_with_margin_and_background" style="height:140px;">
<div class="giri_box_headline">
repo
</div>
<div class="giri_box_text giri_font_huge">
${c.repo}
</div>

<div class="giri_box_text">
<div class="giri_float_left giri_font_bold">
${c.hg_change.rev}
</div>
<div class="giri_float_left">
:${c.hg_change.node}
</div>
<div class="giri_float_end"></div>
${c.hg_change.desc}
</div>
%if c.hg_change_compare is not None:
	<div class="giri_box_text giri_font_small">
    <div class="giri_font_bold">compare with</div>
	<div class="giri_float_left giri_font_bold">
	${c.hg_change_compare.rev}
	</div>
	<div class="giri_float_left">
	:${c.hg_change_compare.node}
	</div>
	<div class="giri_float_end"></div>
	${c.hg_change_compare.desc}
	</div>
%endif
</div>

<div class="giri_box_with_margin_and_background" style="height:200px;">
<div class="giri_box_headline">
job
</div>
<div class="giri_box_with_margin" id="gui_job">
</div>
</div>

<div class="giri_box_with_margin_and_background" style="height:200px;">
<div class="giri_box_headline">
actions
</div>
<div class="giri_box_with_margin">
  <div class="giri_button_box">
    <input class="giri_button" id="gui_delete_job" type="button" value="delete job" />
  </div>
</div>
</div>

</%def>

<%def name="mako_tag_col_2()">
<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
job parts with diffs
</div>
<div class="giri_box_with_margin" id="gui_job_parts_diff">
</div>
</div>

<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
all job parts
</div>
<div class="giri_box_with_margin" id="gui_job_parts">
</div>
</div>
</%def>

<%def name="mako_tag_script()">
YUI().use("anim-base", "datasource-get", "datasource-jsonschema", "datasource-xmlschema", "datasource-polling", "datasource-function", "io-form", function(Y) {

	//
	// Functionality for job
	//
    job_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_job?run_uid=${c.run_uid}&"}),
    job_datasource_callback = {
        success: function(e) {
          job_datasource_load_sucess(e.response.results);
        },
        failure: function(e) {
        	job_datasource_load_error();
        }
    };
    
    function job_datasource_generate_html(data) {
    	var html = '';
    	var run_info = data[0];
    	html += '<div class="giri_box_text">';
    	html += run_info.computer + "(" + run_info.arch_target + ")";
    	html += '</div>';
    	html += '<div class="giri_box_text">';
    	html += help_format_generate_html_sum_boxes_with_ok(run_info);
    	html += '<div class="giri_float_end"></div>';
    	html += '<div class="giri_box_text">';
    	html += run_info.begin_ts + " (" + run_info.durration + ")";
    	html += '</div>';
    	html += '</div>';
    	Y.one('#gui_job').setContent(html);
    }
    
    function job_datasource_load_sucess(data) {
    	job_datasource_generate_html(data);    
    }
    
    function job_datasource_load_error() {
    }

    function gui_update_job() {
    	job_datasource.sendRequest({callback:job_datasource_callback});
	}

    
	//
	// Functionality for job_parts
	//
    job_parts_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_job_parts?run_uid=${c.run_uid}&"}),
    job_parts_datasource_callback = {
        success: function(e) {
          job_parts_datasource_load_sucess(e.response.results);
        },
        failure: function(e) {
        	job_parts_datasource_load_error();
        }
    };

    function job_parts_datasource_generate_html_job_part(job_part) {
    	var html = '';
    	html += '<div class="giri_float_left">';
		html += job_part.module;
    	html += '</div>';
    	html += help_format_generate_html_sum_boxes_with_ok(job_part);
    	html += '<div class="giri_float_end"></div>';
    	return html;
    }

    function job_parts_datasource_generate_html(job_parts) {
    	var html = '';
    	
    	var job_part_counter = 0;
    	html += '<div class="giri_table">';
    	html += '  <ul>'; 
		for (var i=0; i<job_parts.length; i++) {
			var job_part = job_parts[i];
			if (!job_part.has_diff) {
				job_part_counter = job_part_counter + 1;
	    		var select_node_id = 'run_part_select_' + job_part.uid;
				html += '<li class="giri_table_row_box" id="'+select_node_id+'">';
				html += job_parts_datasource_generate_html_job_part(job_part);
				html += '</li>';
	    	}
    	}
    	html += '  </ul>'; 
    	html += '</div>';
    	if(job_part_counter == 0) {
    		html = "No job parts";
    	}
    	Y.one('#gui_job_parts').setContent(html);
    	
    	html = ""
    	var job_part_diff_counter = 0;
    	html += '<div class="giri_table">';
    	html += '  <ul>'; 
		for (var i=0; i<job_parts.length; i++) {
			var job_part = job_parts[i];
			if (job_part.has_diff) {
				job_part_diff_counter = job_part_diff_counter + 1;
	    		var select_node_id = 'run_part_select_' + job_part.uid;
				html += '<li class="giri_table_row_box" id="'+select_node_id+'">';
				html += job_parts_datasource_generate_html_job_part(job_part);
				html += '</li>';
			}
    	}
    	html += '  </ul>'; 
    	html += '</div>';
    	if(job_part_diff_counter == 0) {
    		html = "No job parts with diff";
    	}
    	Y.one('#gui_job_parts_diff').setContent(html);
    	
    }

    function job_parts_datasource_generate_html_post(job_parts) {
    	Y.detach('giri_run_part_handlers|*');
    	for (var i=0; i<job_parts.length; i++) {
			var job_part = job_parts[i];
    		var select_node_id = 'run_part_select_' + job_part.uid;
    		Y.on("giri_run_part_handlers|click", Y.bind(action_job_part_clicked, "dummy", job_part.uid), "#"+select_node_id);
    	}
    }
    function action_job_part_clicked(job_part_uid) {
    	window.open('/pages/giri_repo_job_part/'+job_part_uid, '_self'); 
    }

    function job_parts_datasource_load_sucess(data) {
    	job_parts_datasource_generate_html(data);    
    	job_parts_datasource_generate_html_post(data);    
    }
    
    function job_parts_datasource_load_error() {
    }

    function gui_update_job_parts() {
    	job_parts_datasource.sendRequest({callback:job_parts_datasource_callback});
	}
    
    //
    //
    // 
    function gui_delete_job(event) {
    	var url = '/ajax/giri_repo_delete_job?run_uid=${c.run_uid}';
    	var cfg = {
    		method: "POST",
    		on: {
    			success: gui_delete_job_success
    		}
    	};
    	var request = Y.io(url, cfg);
    };
	
    function gui_delete_job_success() {
    	window.open('/pages/giri_repo', '_self'); 
    };


    
	//
	// Main
	//
	function main() {
		gui_update_job();
		gui_update_job_parts();
		
		Y.on("click", gui_delete_job, "#gui_delete_job");

	}
	Y.on("domready", main);
});
</%def>

