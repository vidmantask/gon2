<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <title>QuickWiki</title>
    ${h.stylesheet_link('/base.css')}
  </head>

  <body>
    <div class="content">
      <h1 class="main">${self.header()}</h1>
      ${next.body()}\
      <p class="footer">
        Return to ${h.link_to('Home', url(controller='pages'))}
      </p>
    </div>
  </body>
</html>
