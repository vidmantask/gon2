
<%inherit file="/base.mako"/>\

<%def name="header()">${c.title}</%def>

<%def name="td_create(value, class_true, class_false)">
  % if value > 0:
    <td class="${class_true}">${value}</td>
  % else:
    <td class="${class_false}">${value}</td>
  % endif
</%def>


% if len(c.releases) != 0:
<table class="stats" cellspacing="0">
<tr>
<td class="header" colspan="1">Release</td>
<td class="header" colspan="1">Status</td>
</tr>

% for release_info_key in c.releases.keys():
<tr>
  <td>
  ${h.link_to(c.releases[release_info_key].slave_name+'('+c.releases[release_info_key].slave_arch_target+')', url(controller='pages', action='release_info', repo=c.repo, changeset=c.changeset, release_info_key=release_info_key))}
  </td>
</tr>
% endfor
</table>
<hr/>
% endif



<table class="stats" cellspacing="0">
<tr>
<td class="header" colspan="1" rowspan='2'>Module groups</td>
<td class="header" colspan="3">Unittest</td>
<td class="header" colspan="3">Static Analysis</td>
</tr>
<tr>
<td class="header_sub" colspan="1">Case Count</td>
<td class="header_sub" colspan="1">Warnings</td>
<td class="header_sub" colspan="1">Errors</td>
<td class="header_sub" colspan="1">Line Count</td>
<td class="header_sub" colspan="1">Warnings</td>
<td class="header_sub" colspan="1">Errors</td>
</tr>


% for module_summary in c.changeset_modules_summary:
<tr>
  <td>
  ${h.link_to(module_summary.modulename, url(controller='pages', action='changeset', repo=c.repo, changeset=c.changeset, module_group=module_summary.modulename))}
  </td>
  ${td_create(module_summary.counters_unittest.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(module_summary.counters_unittest.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(module_summary.counters_unittest.counter_error, 'higlight_error', 'higlight_fine')}
  ${td_create(module_summary.counters_static_analysis.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(module_summary.counters_static_analysis.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(module_summary.counters_static_analysis.counter_error, 'higlight_error', 'higlight_fine')}
</tr>
% endfor
</table>

