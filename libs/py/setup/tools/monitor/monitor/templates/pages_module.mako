<%inherit file="/base.mako"/>\

<%def name="header()">${c.title}</%def>

% for module_detail in c.module_details:
<h1>
	${module_detail.type} - ${module_detail.slave_name}(${module_detail.slave_arch_target})
</h1>
<pre>${module_detail.output_out}</pre>
<pre>${module_detail.output_stdout}</pre>
<pre>${module_detail.output_stderr}</pre>

% endfor
