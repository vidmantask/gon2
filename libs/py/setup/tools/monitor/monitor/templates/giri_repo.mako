<%inherit file="/giri_base.mako"/>\

<%def name="mako_tag_header_links()">
home
</%def>

<%def name="mako_tag_col_1()">
<div class="giri_box_with_margin_and_background" style="height:50px;">
<div class="giri_box_headline">
repo
</div>
<div class="giri_box_with_margin giri_font_huge">
${c.repo}
</div>
</div>
<div class="giri_box_with_margin_and_background" style="height:250px;">
<div class="giri_box_headline">
builders
</div>
<div class="giri_box_with_margin" id="gui_builders">
</div>
</div>


<div class="giri_box_with_margin_and_background" style="height:300px;" id="gui_filters">
<div class="giri_box_headline">
filters
</div>
<div class="giri_box_with_margin" id="gui_filters_content">
</div>
</div>

<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
actions
</div>
<div class="giri_box_with_margin" id="gui_actions">
</div>
</div>

</%def>

<%def name="mako_tag_col_2()">
<div class="giri_box_with_margin_and_background">
<div class="giri_box_headline">
changes
</div>
<div class="giri_box_with_margin" id="gui_changes" >
</div>
</div>
</%def>

<%def name="mako_tag_script()">
YUI().use("anim-base", "datasource-get", "datasource-jsonschema", "datasource-xmlschema", "datasource-polling", "datasource-function", "io-form", function(Y) {

	//
	// Functionality for changes
	//
    changes_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_changes?"}),
    changes_datasource_callback = {
        success: function(e) {
          changes_datasource_load_sucess(e.response.results);
        },
        failure: function(e) {
        	changes_datasource_load_error();
        }
    };
    changes_datasource.plug(Y.Plugin.DataSourceJSONSchema, {
        schema: {
            resultListLocator: "changes",
            resultFields: ["node", "date", "rev", "desc", "job_sums_unittest", "job_sums_static_analysis", "job_sums_profile", "job_sums_coverage"]
        }
    });
    
    function changes_datasource_generate_html_job_sum(job_sum) {
    	html = "";
		html += '<div class="yui3-g">';
		html += '  <div class="yui3-u-2-5">';
		html += '  <div class="giri_float_left giri_font_small">';
		html += job_sum.computer +'(' + job_sum.arch_target + ')';
		html += '  </div>';
		html += '  <div class="giri_float_right giri_font_small">';
		html += job_sum.begin_ts;
		html += '  </div>';
		html += '  <div class="giri_float_end"></div>';
		html += '  </div>';
		html += '  <div class="yui3-u-3-5">';
		html += help_format_generate_html_sum_boxes(job_sum); 
    	html += '<div class="giri_float_end"></div>';
		html += '</div>';
		html += '</div>';
    	return html;
    }

    function changes_datasource_generate_html_job_sums(job_sums, title) {
    	html = "";
		html += '<div class="yui3-g">';
		html += '  <div class="yui3-g">';
		html += '  <div class="yui3-u-1-5">';
		html += '  <div class="giri_font_small giri_font_bold">';
		html += title;
		html += '  </div>';
		html += '  </div>';
		html += '  <div class="yui3-u-4-5">';
		html += '  </div>';
		html += '  </div>';
		html += '</div>';
    	html += '<div class="giri_table">';
    	html += '  <ul>'; 
		for (var i=0; i<job_sums.length; i++) {
			var job_sum = job_sums[i];
    		var select_node_id = 'select_' + job_sum.uid;
			html += '<li class="giri_table_row_box" id="'+select_node_id+'">';
			html += changes_datasource_generate_html_job_sum(job_sum);
			html += '</li>';
		}
    	html += '  </ul>'; 
    	html += '</div>';
    	return html;
    };
    
    function changes_datasource_generate_html(data) {
		var html = '';
    	html += '<div class="giri_table_no_select">';
    	html += '    <ul>'; 
    	for (change_id in data) {
    		var change = data[change_id];
    		var select_node_id = 'change_select_' + change.node;
			html += '<li class="giri_table_row_box" id="'+select_node_id+'">';
			html += '<div class="yui3-g">';
			html += '<div class="yui3-u-1-2">';
			html += '  <div class="giri_table_row_text giri_float_left giri_font_small giri_font_bold">' + change.rev + '</div>';
			html += '  <div class="giri_table_row_text giri_float_left giri_font_small">' + ':' + change.node + '</div>';
			html += '  <div class="giri_float_end"></div>';
			html += '  <div class="giri_table_row_text giri_float_left">' + change.desc + '</div>';
			html += '  <div class="giri_float_end"></div>';
			html += '</div>';
			html += '<div class="yui3-u-1-2">';
			if (change.job_sums_unittest.length != 0) {
				html += changes_datasource_generate_html_job_sums(change.job_sums_unittest, "Unittest");
			}
			if (change.job_sums_static_analysis.length != 0) {
				html += changes_datasource_generate_html_job_sums(change.job_sums_static_analysis, "Static Analysis");
			}
			if (change.job_sums_profile.length != 0) {
				html += changes_datasource_generate_html_job_sums(change.job_sums_profile, "Profiling");
			}
			if (change.job_sums_coverage.length != 0) {
				html += changes_datasource_generate_html_job_sums(change.job_sums_coverage, "Coverage");
			}
			if (change.job_sums_profile.length != 0) {
				html += '<div class="giri_box_with_margin"></div>';
			}
			html += '</div>';
			html += '</div>';
			html += '</li>';
    	}
    	html += '    </ul>'; 
    	html += '  </div>';
    	html += '</div>';
    	Y.one('#gui_changes').setContent(html);
    }

    
    function changes_datasource_generate_html_post(data) {
    	Y.detach('giri_changes_handlers|*');
    	for (change_id in data) {
    		var change = data[change_id];
    		for (var i=0; i<change.job_sums_unittest.length; i++) {
    			var job_sum = change.job_sums_unittest[i];
        		var select_node_id = 'select_' + job_sum.uid;
        		Y.on("giri_changes_handlers|click", Y.bind(action_change_clicked, "dummy", job_sum.uid), "#"+select_node_id);
    		}
    		for (var i=0; i<change.job_sums_static_analysis.length; i++) {
    			var job_sum = change.job_sums_static_analysis[i];
        		var select_node_id = 'select_' + job_sum.uid;
        		Y.on("giri_changes_handlers|click", Y.bind(action_change_clicked, "dummy", job_sum.uid), "#"+select_node_id);
    		}
    		for (var i=0; i<change.job_sums_profile.length; i++) {
    			var job_sum = change.job_sums_profile[i];
        		var select_node_id = 'select_' + job_sum.uid;
        		Y.on("giri_changes_handlers|click", Y.bind(action_change_clicked, "dummy", job_sum.uid), "#"+select_node_id);
    		}
    		for (var i=0; i<change.job_sums_coverage.length; i++) {
    			var job_sum = change.job_sums_coverage[i];
        		var select_node_id = 'select_' + job_sum.uid;
        		Y.on("giri_changes_handlers|click", Y.bind(action_change_clicked, "dummy", job_sum.uid), "#"+select_node_id);
    		}
    	}
    }
    function action_change_clicked(job_sum_uid) {
    	window.open('/pages/giri_repo_job/'+job_sum_uid, '_self'); 
    }

    function changes_datasource_load_sucess(data) {
    	changes_datasource_generate_html(data);
    	changes_datasource_generate_html_post(data);
    }
    
    function changes_datasource_load_error() {
    }
    
	function gui_update_changes() {
		changes_datasource.sendRequest({callback:changes_datasource_callback});
	}

	
	

	//
	// Functionality for builders
	//
    builders_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_builders?"}),
    builders_datasource_callback = {
        success: function(e) {
          builders_datasource_load_sucess(e.response.results);
        },
        failure: function(e) {
        	builders_datasource_load_error();
        }
    };
    
    function builders_datasource_generate_html(data) {
		var html = '';

		if(!data.job_is_running) {
			html += '<form id="gui_form_builders" onSubmit="return false;">';
			html += '  <div class="giri_form_field">';
			html += '    <div class="giri_form_field_label">builder list ([ip:port, ip:port])</div>';
			html += '    <div class="giri_form_field_input_box">';
			html += '      <input type="text" name="form_builders_string" value="'+data.builders_string+'"></input>';
			html += '    </div>';
			html += '  </div>';
			html += '</div>';
			html += '</form>';
			html += '<div class="giri_button_box">';
			html += '  <input class="giri_button" id="gui_action_builders_update" type="button" value="update builder list" />';
			html += '</div>';
		}
		
    	html += '<div class="giri_table_no_select">';
    	html += '  <ul>'; 
    	for (builder_id in data.builders) {
    		var builder = data.builders[builder_id];
    		var select_node_id = 'select_' + builder.uid;
			html += '<li class="giri_table_row_box" id="'+select_node_id+'">';
			html += '<div class="yui3-g">';
			html += '<div class="yui3-u-1-2">';
			html += '  <div class="giri_table_row_text giri_float_left giri_font_small giri_font_bold">' + builder.name +'(' + builder.arch_target + ')' + '</div>';
			html += '  <div class="giri_float_end"></div>';
			html += '  <div class="giri_table_row_text giri_float_left giri_font_small">' + builder.hg_id + '</div>';
			html += '  <div class="giri_float_end"></div>';
			html += '</div>';
			html += '<div class="yui3-u-1-2">';
			html += '  <div class="giri_table_row_text giri_float_right">' + builder.status + '</div>';
			html += '  <div class="giri_float_end"></div>';
			html += '</div>';
			html += '</div>';
			html += '</li>';
    	}
    	html += '  </ul>'; 
    	html += '</div>';
    	Y.one('#gui_builders').setContent(html);
    	
		if(!data.job_is_running) {
			Y.detach('giri_builders_handlers|*');
			Y.on("giri_builders_handlers|click", action_save_builders_execute, "#gui_action_builders_update");
		}
    }
    
    function builders_datasource_load_sucess(data) {
    	builders_datasource_generate_html(data[0]);    
    }
    
    function builders_datasource_load_error() {
    }

    function gui_update_builders() {
    	builders_datasource.sendRequest({callback:builders_datasource_callback});
	}

    
	//
	// Functionality for actions
	//
    actions_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_actions?"}),
    actions_datasource_callback = {
        success: function(e) {
          actions_datasource_load_sucess(e.response.results);
        },
        failure: function(e) {
        	actions_datasource_load_error();
        }
    };
    
    function actions_datasource_generate_html(data) {
    	var html = '';

		var job_info = data[0];
		if(job_info.job_is_running) {
    		html += '<div class="giri_button_box_message">';
			html += '    <div class="giri_form_field_label giri_box_sum_ok">job is running</div>';
			html += '    <div class="giri_float_left">';
    		html += job_info.job_title; 
    		html += '    </div>';
			html += '    <div class="giri_float_right">';
    		html += job_info.job_begin_ts; 
    		html += '    </div>';
    		html += '    <div class="giri_float_end"></div>';
			html += '    <div class="giri_float_right giri_font_small">';
    		html += job_info.job_durration; 
    		html += '    </div>';
    		html += '    <div class="giri_float_end"></div>';
    		html += '</div>';
		}

    	
    	var data_actions = data[0].actions;
    	if(data_actions.length > 0) {
	    	for (action_id in data_actions) {
	    		var action = data_actions[action_id];
	    		var select_node_id = 'select_' + action.uid;
	    		html += '<div class="giri_button_box">';
	    		html += '  <input class="giri_button" id="'+select_node_id+'" type="button" value="'+action.title+'" />';
	    		html += '</div>';
	    	}
    	}
    	Y.one('#gui_actions').setContent(html);
    	
    	if(data_actions.length > 0) {
			Y.detach('giri_action_handlers|*');
			for (action_id in data_actions) {
	    		var action = data_actions[action_id];
	    		var select_node_id = 'select_' + action.uid;
	    		Y.on("giri_action_handlers|click", Y.bind(action_execute, "dummy", action.uid), "#"+select_node_id);
	    	}
    	}
    }
    
    function actions_datasource_load_sucess(data) {
    	actions_datasource_generate_html(data);
    	if (data[0].job_is_running) {
    		gui_update_actions_and_builders_delayed();
    		gui_update_changes();
    	}
    	else {
    		gui_update_changes();
    	}
    }
    
    function actions_datasource_load_error() {
    }

   
    function gui_update_actions() {
    	actions_datasource.sendRequest({callback:actions_datasource_callback});
	}

    var gui_update_actions_and_builders_delayed_timer_id = null;
    function gui_update_actions_and_builders_delayed() {
    	if (gui_update_actions_and_builders_delayed_timer_id != null) {
    		clearTimeout(self.gui_update_actions_and_builders_delayed_timer_id);
    		gui_update_actions_and_builders_delayed_timer_id = null;
    	}
    	gui_update_actions_and_builders_delayed_timer_id = setTimeout(gui_update_actions_and_builders, 10000);	
    }
    
    function gui_update_actions_and_builders() {
    	gui_update_actions();
    	gui_update_builders();
		gui_update_filters(false);

    }


	//
	// Functionality for filters
	//
    filters_datasource = new Y.DataSource.Get({source:"/ajax/giri_repo_request_filters?"}),
    filters_datasource_callback = {
        success: function(e) {
        	filters_datasource_load_sucess(e.response.results[0]);
        },
        failure: function(e) {
        	filters_datasource_load_error();
        }
    };
    
    function filters_datasource_generate_html(data) {
    	var html = '';    	
		if(! data.job_is_running) {
    		var ifilter_value =  data.ifilter;
    		var efilter_value =  data.efilter;
    		
			html += '<form id="gui_form_filters" onSubmit="return false;">';
			html += '  <div class="giri_form_field">';
			html += '    <div class="giri_form_field_label">include filter(regexp)</div>';
			html += '    <div class="giri_form_field_input_box">';
			html += '      <input type="text" id="form_ifilter" name="form_ifilter" value="' + ifilter_value + '"></input>';
			html += '    </div>';
			html += '  </div>';
			html += '  <div class="giri_form_field">';
			html += '    <div class="giri_form_field_label">exclude filter(regexp)</div>';
			html += '    <div class="giri_form_field_input_box">';
			html += '      <input type="text" id="form_efilter" name="form_efilter" value="' + efilter_value + '"></input>';
			html += '    </div>';
			html += '  </div>';
			html += '</div>';
			html += '</form>';
    		html += '<div class="giri_button_box">';
    		html += '  <input class="giri_button" id="form_filters_update" type="button" value="update filters" />';
    		html += '</div>';
    		
        	html += '<div class="giri_table_no_select giri_scroll" style="height:150px;">';
        	html += '  <ul>'; 
        	for (filter_id in data.filters) {
        		var filter = data.filters[filter_id];
    			html += '<li class="giri_table_row_box">';
    			html += '  <div class="giri_table_row_text giri_font_small giri_font_bold">';
    			html += filter.modulename;
    			html += '  </div>';
    			html += '</li>';
        	}
        	html += '  </ul>'; 
    		html += '</div>';
		}
		else {
			html += '<form id="gui_form_filters" onSubmit="return false;">';
			html += '</form>';
		}
    	Y.one('#gui_filters_content').setContent(html);
    	
    	if(!data.job_is_running) {
        	Y.one('#gui_filters').setStyle("display", "block");
			Y.detach('giri_filters_handlers|*');
			Y.on("giri_filters_handlers|click", gui_update_filters_with_save, "#form_filters_update");
    	}
    	else {
        	Y.one('#gui_filters').setStyle("display", "none");
    	}
    }
    
    function filters_datasource_load_sucess(data) {
    	filters_datasource_generate_html(data);
    }
    
    function filters_datasource_load_error() {
    }
    
    function gui_update_filters_with_save() {
    	gui_update_filters(true);
    }
    
    function gui_update_filters(save_filters) {
    	var form_ifilter = "";
    	var form_efilter = "";
    	if(save_filters) {
        	form_ifilter = Y.one('#form_ifilter').get('value');
        	form_efilter = Y.one('#form_efilter').get('value');
    	}
    	var request = "";
    	if(save_filters) {
        	request += "save_filters=yes" + "&";
    	}
    	request += "form_ifilter=" + form_ifilter + "&";
    	request += "form_efilter=" + form_efilter;
    	
    	cfg = {
    		callback : filters_datasource_callback,
    		request  : request
    	};
    	filters_datasource.sendRequest(cfg);
	}

    //
    //
    //
    function action_execute(action_uid, event) {
    	var url = '/ajax/'+action_uid+'/&';
    	var cfg = {
    		method: "POST",
    		on: {
    			success: function () {
    				action_execute_success();
    			}
    		}
    	};
    	var request = Y.io(url, cfg);
    };
	
    function action_execute_success() {
		gui_update_builders();
		gui_update_actions();
		gui_update_filters(false);
    };
    

    //
    //
    //
    function action_save_builders_execute() {
    	var url = '/ajax/giri_repo_action_save_builders/&';
    	var cfg = {
    		method: "POST",
    		form: {
    			id: Y.Node.one('#gui_form_builders'),
    			useDisabled: true
    		},
    		on: {
    			success: function () {
    				action_save_builders_execute_success();
    			}
    		}
    	};
    	var request = Y.io(url, cfg);
    };
	
    function action_save_builders_execute_success() {
    	gui_update_builders();
    };

    
	//
	// Main
	//
	function main() {
		gui_update_builders();
		gui_update_actions();
		gui_update_filters(false);
	}
	Y.on("domready", main);
});
</%def>

