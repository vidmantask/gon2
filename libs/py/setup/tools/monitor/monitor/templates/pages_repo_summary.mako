<%inherit file="/base.mako"/>\

<%def name="header()">${c.title}</%def>

<%def name="td_create(value, class_true, class_false)">
  % if value > 0:
    <td class="${class_true}">${value}</td>
  % else:
    <td class="${class_false}">${value}</td>
  % endif
</%def>


<table class="stats" cellspacing="0">
<tr>
<td class="header" colspan="1" rowspan='2'>Changeset</td>
<td class="header" colspan="3">Unittest</td>
<td class="header" colspan="3">Static Analysis</td>
</tr>
<tr>
<td class="header_sub" colspan="1">Case Count</td>
<td class="header_sub" colspan="1">Warnings</td>
<td class="header_sub" colspan="1">Errors</td>
<td class="header_sub" colspan="1">Line Count</td>
<td class="header_sub" colspan="1">Warnings</td>
<td class="header_sub" colspan="1">Errors</td>
</tr>


% for changeset_summary in c.repo_summary.chaneset_summaries:
<tr>
  <td>
  ${h.link_to("%s:%s" % (changeset_summary.changeset_local,changeset_summary.changeset), url(controller='pages', action='changeset_summary', repo=c.repo, changeset=changeset_summary.changeset))}
  </td>
  ${td_create(changeset_summary.counters_unittest.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(changeset_summary.counters_unittest.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(changeset_summary.counters_unittest.counter_error, 'higlight_error', 'higlight_fine')}
  ${td_create(changeset_summary.counters_static_analysis.counter, 'higlight_normal', 'higlight_normal')}
  ${td_create(changeset_summary.counters_static_analysis.counter_warning, 'higlight_warning', 'higlight_normal')}
  ${td_create(changeset_summary.counters_static_analysis.counter_error, 'higlight_error', 'higlight_fine')}
</tr>
% endfor
</table>