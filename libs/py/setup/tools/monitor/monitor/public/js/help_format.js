function help_format_generate_html_sum_box(count, count_diff, style, is_pct) {
	html = "";
	if(count > 0 || count_diff != 0) {
		html += '<div class="giri_float_right">';
		html += '<div class="'+style+'">';
		html += '<div class="yui3-g">';
		html += '  <div class="yui3-u-1-2">';
    	if(count > 0) {
    		html += count;
    		if(is_pct) {
    			html += "%";
    		}
    	}
		html += '  </div>';
		html += '  <div class="yui3-u-1-2">';
    	if(count_diff > 0) {
    		html += '+' + count_diff;
    		if(is_pct) {
    			html += "%";
    		}
    	}
    	else if(count_diff < 0) {
    		html += count_diff;
    		if(is_pct) {
    			html += "%";
    		}
    	}
		html += '  </div>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
	}
	return html;
};

function help_format_generate_html_box_ok() {
	html = "";
	html += '<div class="giri_box_sum giri_box_sum_ok giri_float_right" style="height:12px;">';
	html += '</div>';
	return html;
};

function help_format_generate_html_box_info() {
	var html = '';
	html += '<div class="giri_box_sum giri_box_sum_info giri_float_right" style="height:12px;">';
    html += '</div>';
	return html;
};

function help_format_generate_html_box_warning() {
	var html = '';
	html += '<div class="giri_box_sum giri_box_sum_warning giri_float_right" style="height:12px;">';
    html += '</div>';
	return html;
};

function help_format_generate_html_box_error() {
	var html = '';
	html += '<div class="giri_box_sum giri_box_sum_error giri_float_right" style="height:12px;">';
    html += '</div>';
	return html;
};

function help_format_generate_html_sum_boxes(job_sum) {
	html = "";
	if(job_sum.count > 0) {
		html += help_format_generate_html_sum_box(job_sum.count, job_sum.diff_count, 'giri_box_sum_normal', false)
	}
	if('ok_pct' in job_sum) {
		html += help_format_generate_html_sum_box(job_sum.ok_pct, job_sum.diff_ok_pct, 'giri_box_sum giri_box_sum_ok', true)
	}
	else if('info_pct' in job_sum) {
		html += help_format_generate_html_sum_box(job_sum.info_pct, job_sum.diff_info_pct, 'giri_box_sum giri_box_sum_info', true)
	}
	else if('warning_pct' in job_sum) {
		html += help_format_generate_html_sum_box(job_sum.warning_pct, job_sum.diff_warning_pct, 'giri_box_sum giri_box_sum_warning', true)
	}
	else if('error_pct' in job_sum) {
		html += help_format_generate_html_sum_box(job_sum.error_pct, job_sum.diff_error_pct, 'giri_box_sum giri_box_sum_error', true)
	}
	else if(job_sum.count > 0) {
		html += help_format_generate_html_sum_box(job_sum.info, job_sum.diff_info, 'giri_box_sum giri_box_sum_info', false)
		html += help_format_generate_html_sum_box(job_sum.warning, job_sum.diff_warning, 'giri_box_sum giri_box_sum_warning', false)
		html += help_format_generate_html_sum_box(job_sum.error, job_sum.diff_error, 'giri_box_sum giri_box_sum_error', false)
	}
	return html;
};


function help_format_generate_html_sum_boxes_with_ok(job_sum) {
	html = "";
	html += help_format_generate_html_sum_boxes(job_sum);
	if(job_sum.warning == 0 && job_sum.error == 0) {
    	html += help_format_generate_html_box_ok();
	}
	return html;
};
