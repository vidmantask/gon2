'''
Created on Jun 15, 2011

@author: thwang
'''

def query_repo_changes(repo, count_max=14):
    return repo.get_changes(count_max)
    
def query_repo_builders(repo, slave_connect_list=None):
    return repo.get_builders(slave_connect_list)
    
