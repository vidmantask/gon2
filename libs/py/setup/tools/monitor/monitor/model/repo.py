'''
Created on Jun 15, 2011

@author: thwang
'''
import datetime 
import time
import traceback
import pstats
import os
import StringIO

import lib.dev_env
import lib.dev_env.hg
import lib.dev_env.devbotnet
import lib.dev_env.devbotnet_jobs
import lib.dev_env.devbotnet_db
import lib.dev_env.devbotnet_db_update

import threading

from pylons import config


def get_connect_string(): 
    return config['sqlalchemy.url']




class RepoBuilder(object):
    def __init__(self, dev_env_slave_info, hg_id):
        self.dev_env_slave_info = dev_env_slave_info
        self.hg_id = hg_id


class RepoFilter(object):
    def __init__(self, name, selected):
        self.name = name
        self.selected = selected
    

class Repo(object):
    def __init__(self):
        self.dev_env = lib.dev_env.DevEnv.create_current()
        self.dev_env_slave_info = lib.dev_env.devbotnet.DevbotnetSlaveInfo.create_from_local_config_ini(self.dev_env)
        self.hg_repo = self.dev_env_slave_info.hg_repo
        self.hg_root = self.dev_env.hg_root
        
    def get_changes(self, count_max):
        return lib.dev_env.hg.HGChange.create_from_repo(count_max=count_max)

    def get_builders(self, slave_connect_list):
        builders = []

        dev_env = lib.dev_env.DevEnv.create_current()
        dev_env_slave_info = lib.dev_env.devbotnet.DevbotnetSlaveInfo.create_from_local_config_ini(dev_env)

        devbotnet_master = lib.dev_env.devbotnet.DevbootnetMaster(dev_env_slave_info, dev_env, slave_connect_list=slave_connect_list)
        for slave in devbotnet_master.lookup_slaves():
            (hg_repo, hg_revision) = slave.cmd_get_hg_revision()
            builder = RepoBuilder(slave.cmd_get_info(), hg_revision)
            builders.append(builder)
        return builders 

    def get_module_list_from_filters(self, ifilter, efilter):
        result = []
        list_of_modules = self.dev_env.aply_ifilter_to_modulename(self.dev_env.select_files_py(), ifilter, efilter)
        for (filename, module) in list_of_modules:
            result.append( (filename, module) )
        result.sort()
        return result

    def generate_default_profile(self, profile_data):
        temp_filename = self.dev_env.generate_temp_filename()
            
        temp_file = open(temp_filename, 'wb')
        temp_file.write(profile_data)
        temp_file.close()

        result_stream = StringIO.StringIO()
        p_data = pstats.Stats(temp_filename, stream=result_stream)
        p_data.sort_stats('cumulative').print_stats(100)
        
        os.unlink(temp_filename)
        return result_stream.getvalue()

    @classmethod
    def create_current(cls):
        return Repo()
    



global_repo_job = None

class RepoJob(threading.Thread):
    def __init__(self,  job_title, can_cancel_job=False):
        threading.Thread.__init__(self, name='RepoJob')
        self.daemon = True
        self._done = False

        self.dev_env = lib.dev_env.DevEnv.create_current()
        self.dev_env_slave_info = lib.dev_env.devbotnet.DevbotnetSlaveInfo.create_from_local_config_ini(self.dev_env)

        db_session = lib.dev_env.devbotnet_db.create_session(get_connect_string())
        db_config = lib.dev_env.devbotnet_db_update.get_or_create_config(db_session)
        self.db_config_ifilter = db_config.ifilter
        self.db_config_efilter = db_config.efilter
        self.db_config_connect_list = db_config.get_slave_connect_list()
        db_session.close()
        
        self.devbotnet_master = lib.dev_env.devbotnet.DevbootnetMaster(self.dev_env_slave_info, self.dev_env, get_connect_string(), self.db_config_connect_list)

        self.job_begin_ts = None
        self.job_title = job_title
        self.can_cancel_job = can_cancel_job
        
    def is_done(self):
        return self._done
        
    def repo_job_begin(self):
        global global_repo_job
        global_repo_job = self
        self._done = False
        self.job_begin_ts = datetime.datetime.utcnow()
        
    def repo_job_end(self):
        self._done = True
        global global_repo_job
        global_repo_job = None

    def run(self):
        self.repo_job_begin()
        try:
            self.do_run()
        except:
            traceback.print_exc()
            print "x" * 80
        self.repo_job_end()

    def do_run(self):
        pass
    
    def cancel(self):
        pass

    def get_running_slave_infos(self):
        return self.devbotnet_master.get_running_slave_infos()

    @classmethod
    def is_job_running(cls):
        global global_repo_job
        if global_repo_job is None:
            return False
        return not global_repo_job.is_done()

    @classmethod
    def get_job_running(cls):
        global global_repo_job
        if global_repo_job is not None:
            return global_repo_job
        return None


class RepoJobUnittest(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "unittest")
        
    def do_run(self):
        print "RepoJobUnittest begin"
        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter
        self.devbotnet_master.do_unittest(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        print "RepoJobUnittest end"



class RepoJobStaticAnalysis(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "static analysis")
        
    def do_run(self):
        print "RepoJobStaticAnalysis begin"
        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter
        self.devbotnet_master.do_static_analysis(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        print "RepoJobStaticAnalysis end"


class RepoJobUnittestAndStaticAnalysis(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "unittest and static analysis")
        
    def do_run(self):
        print "RepoJobUnittestAndStaticAnalysis begin"
        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter
        self.devbotnet_master.do_unittest(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        self.devbotnet_master.do_static_analysis(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        print "RepoJobUnittestAndStaticAnalysis end"


class RepoJobProfile(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "profiling")
        
    def do_run(self):
        print "RepoJobProfile begin"
        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter
        self.devbotnet_master.do_profile(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        print "RepoJobProfile end"


class RepoJobCoverage(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "coverage")
        
    def do_run(self):
        print "RepoJobCoverage begin"
        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter
        self.devbotnet_master.do_coverage(self.dev_env_slave_info.hg_repo, ifilter, efilter)
        print "RepoJobCoverage end"


class RepoJobCont(RepoJob):
    def __init__(self):
        RepoJob.__init__(self,  "continuous", can_cancel_job=True)
        self._do_stop = False
    
    def do_run(self):
        print "RepoJobUnittestAndStaticAnalysis begin"

        update_interval_min = 1

        ifilter = self.db_config_ifilter
        efilter = self.db_config_efilter

        while not self._do_stop:
            print 'do_run_inc, checking' , datetime.datetime.now()
            if lib.dev_env.hg.hg_check_for_news():
                self.devbotnet_master.do_pull_and_update()
                self.devbotnet_master.do_restart(self.dev_env_slave_info.hg_repo)
                self.devbotnet_master.do_unittest(self.dev_env_slave_info.hg_repo, ifilter, efilter)
                self.devbotnet_master.do_static_analysis(self.dev_env_slave_info.hg_repo, ifilter, efilter)
            try:
                time.sleep(update_interval_min * 60)
            except KeyboardInterrupt:
                self._do_stop = True
        
        print "RepoJobUnittestAndStaticAnalysis end"

    def cancel(self):
        self._do_stop = True
        
        
        
        
        