'''
'''
import os
import os.path
import pickle
import copy
import sys
import subprocess

ROOT = os.path.abspath(os.path.dirname(__file__))
HG_ROOT = os.path.normpath(os.path.join(ROOT, '..', '..', '..', '..', '..', '..'))

ROOT_DEVBOTNET = os.path.join(HG_ROOT, 'setup', 'dev_env', 'devbotnet')
ROOT_DEVBOTNET_TEMP = os.path.join(ROOT_DEVBOTNET, 'temp')

ROOT_PY = os.path.join(HG_ROOT, 'py')
sys.path.append(ROOT_PY)

import lib.dev_env.devbotnet_jobs



def get_changeset_local(changeset='tip'):
    if changeset.endswith('+'):
        changeset = changeset[:len(changeset)-1]

    command = ["hg", "id", "-n", '-r', changeset]
    process = subprocess.Popen(command, cwd=HG_ROOT, stdout=subprocess.PIPE)
    process.wait()
    process_result = process.communicate()
    return process_result[0].strip()


def load_job_infos(root, job_info_type_suffix):
    if not os.path.isdir(root):
        return []
    job_infos = []
    for folder_item in os.listdir(root):
        folder_item_abs = os.path.join(root, folder_item)
        job_info_filename_abs = os.path.join(folder_item_abs, 'job_info_%s' % job_info_type_suffix)
        if os.path.exists(job_info_filename_abs):
            job_info_file = open(job_info_filename_abs, 'r')
            job_info = pickle.load(job_info_file)
            job_info_file.close()
            job_infos.append(job_info)
    return job_infos

def filter_job_parts_by_groups(job_infos, module_groups, default_group_name):
    module_groups_info = {}
    for module_group in module_groups:
        module_groups_info[module_group] = []
    module_groups_info[default_group_name] = []

    for job_info in job_infos:
        for job_part_info in job_info.part_infos:
            module_info_matched = False
            for module_group in module_groups:
                if job_part_info.modulename.startswith(module_group):
                    module_groups_info[module_group].append(job_part_info)
                    module_info_matched = True
                    break
            if not module_info_matched:
                module_groups_info[default_group_name].append(job_part_info)
    return module_groups_info


class DevBotNetRepo(object):
    def __init__(self):
        self.repo = ''
        self.repo_path = ''
    
    @classmethod
    def create_repos(cls):
        repos = {}
        if not os.path.isdir(ROOT_DEVBOTNET):
            return repos
        
        for folder_item in os.listdir(ROOT_DEVBOTNET):
            folder_item_abs = os.path.join(ROOT_DEVBOTNET, folder_item)
            if os.path.isdir(folder_item_abs) and folder_item != 'temp':
                repo_obj = DevBotNetRepo()
                repo_obj.repo = folder_item
                repo_obj.repo_path = folder_item_abs
                repos[folder_item] = repo_obj
        return repos

    @classmethod
    def create_repo(cls, repo):
        repo_obj = DevBotNetRepo()
        repo_path = os.path.join(ROOT_DEVBOTNET, repo)
        if os.path.isdir(repo_path):
            repo_obj.repo = repo
            repo_obj.repo_path = repo_path
        return repo_obj


class DevBotNetRepoSummary(object):
    def __init__(self, repo_obj):
        self.repo_obj = repo_obj
        self.chaneset_summaries = []

    @classmethod
    def create_repo_summary(cls, repo):
        repo_obj = DevBotNetRepo.create_repo(repo)
        repo_summary = DevBotNetRepoSummary(repo_obj)
        
        changeset_summarys = {}
        for folder_item in os.listdir(repo_obj.repo_path):
            folder_item_abs = os.path.join(repo_obj.repo_path, folder_item)
            changeset_summary = DevBotNetChangesetSummary.create(repo_obj, folder_item, folder_item_abs)
            changeset_summarys[changeset_summary.changeset_local+':'+changeset_summary.changeset] = changeset_summary

        changeset_summarys_keys = changeset_summarys.keys()
        changeset_summarys_keys.sort(reverse=True)
        for changeset_summarys_key in changeset_summarys_keys:
            repo_summary.chaneset_summaries.append(changeset_summarys[changeset_summarys_key])
        return repo_summary


class DevBotNetCounters(object):
    def __init__(self, name=''):
        self.name = name
        self.counter = 0
        self.counter_error = 0
        self.counter_warning = 0
    
    def calc_from_job_infos(self, job_infos):
        for job_info in job_infos:
            self.calc_from_job_parts(job_info.part_infos)

    def calc_from_job_parts(self, job_parts):
        for jop_part_info in job_parts:
            self.add_from_job_part(jop_part_info)

    def add_from_job_part(self, job_part):
        self.counter += job_part.counter
        self.counter_error += job_part.counter_error
        self.counter_warning += job_part.counter_warning

    @classmethod
    def create_summary_unittest(cls, changeset_path):
        counters = DevBotNetCounters('Unittest from all slaves')
        counters.calc_from_job_infos(load_job_infos(changeset_path, 'unittest'))
        return counters

    @classmethod
    def create_summary_static_analysis(cls, changeset_path):
        counters = DevBotNetCounters('Static Analysis from all slaves')
        counters.calc_from_job_infos(load_job_infos(changeset_path, 'static_analysis'))
        return counters
        

class DevBotNetChangesetSummary(object):
    def __init__(self, repo_obj, changeset, changeset_path):
        self.repo_obj = repo_obj
        self.changeset = changeset
        self.changeset_local = get_changeset_local(changeset)
        self.changeset_path = changeset_path
        self.counters_unittest = DevBotNetCounters()
        self.counters_static_analysis = DevBotNetCounters()
        
    @classmethod
    def create(cls, repo_obj, changeset, changeset_path):
        changeset_summary = DevBotNetChangesetSummary(repo_obj, changeset, changeset_path)
        changeset_summary.counters_unittest = DevBotNetCounters.create_summary_unittest(changeset_path)
        changeset_summary.counters_static_analysis = DevBotNetCounters.create_summary_static_analysis(changeset_path)
        return changeset_summary


class DevBotNetChangeset(object):
    def __init__(self, repo_obj, changeset):
        self.repo_obj = repo_obj
        self.changeset = changeset
        self.changeset_local = get_changeset_local(changeset)
        self.changeset_path = os.path.join(repo_obj.repo_path, changeset)
        self.module_summary = []

    @classmethod
    def create(cls, repo, changeset, module_groups):
        repo_obj = DevBotNetRepo.create_repo(repo)
        changeset_obj = DevBotNetChangeset(repo_obj, changeset)
        changeset_obj.module_summary = DevBotNetModuleSummary.create_modules_by_groups(changeset_obj.changeset_path, module_groups)
        return changeset_obj

        
class DevBotNetModuleSummary(object):
    def __init__(self, modulename):
        self.modulename = modulename
        self.counters_unittest = DevBotNetCounters()
        self.counters_static_analysis = DevBotNetCounters()
        
    @classmethod
    def create_modules_by_groups(cls, changeset_path, module_groups):
        default_group_name = 'Others' 
        job_infos_unittest = load_job_infos(changeset_path, 'unittest')
        modules_summary_unittest = filter_job_parts_by_groups(job_infos_unittest, module_groups, default_group_name)
        
        job_infos_static_analysis = load_job_infos(changeset_path, 'static_analysis')
        modules_summary_static_analysis = filter_job_parts_by_groups(job_infos_static_analysis, module_groups, default_group_name)

        modules_summary = []
        module_groups = copy.copy(module_groups)
        module_groups.append(default_group_name)
        for module_group in module_groups:
            module_summary = DevBotNetModuleSummary(module_group)
            module_summary.counters_unittest.calc_from_job_parts(modules_summary_unittest[module_group])
            module_summary.counters_static_analysis.calc_from_job_parts(modules_summary_static_analysis[module_group])
            modules_summary.append(module_summary)
        return modules_summary
        
    @classmethod
    def create_modules_for_group(cls, repo, changeset, module_group, job_filter='unittest'):
        repo_obj = DevBotNetRepo.create_repo(repo)
        changeset_obj = DevBotNetChangeset(repo_obj, changeset)

        job_infos_unittest = load_job_infos(changeset_obj.changeset_path, job_filter)
        modules_set = set()
        slave_set = set()
        slaves = {}
        modules_summary = {}
        for job_info in job_infos_unittest:
            slave_set.add(job_info.slave_name)
            slaves[job_info.slave_name] = (job_info.slave_name, job_info.slave_arch_target)
            for job_part in job_info.part_infos:
                if job_part.modulename.startswith(module_group):
                    modules_set.add(job_part.modulename)
                    module_summary_obj = DevBotNetModuleSummary(job_part.modulename)
                    module_summary_obj.counters_unittest.add_from_job_part(job_part)
                    module_summary_obj.counters_static_analysis.add_from_job_part(job_part)
                    if not modules_summary.has_key(job_part.modulename):
                        modules_summary[job_part.modulename] = {}
                    modules_summary[job_part.modulename][job_info.slave_name] = module_summary_obj

        for module in modules_set:
            if not modules_summary.has_key(module):
                modules_summary[module] = {}
            for slave in slave_set:
                if not modules_summary[module].has_key(slave):
                    modules_summary[module][slave] = DevBotNetModuleSummary(module)

        keys = modules_summary.keys()
        keys.sort()
        return (slaves.values(), keys, modules_summary)


class DevBotNetModuleDetailPart(object):
    def __init__(self):
        self.slave_name = ''
        self.slave_arch_target = ''
        self.type = ''
        self.output_stdout = ''
        self.output_stderr = ''
        self.counters = DevBotNetCounters()
        
    @classmethod
    def create_for_module(cls, repo, changeset, module):
        repo_obj = DevBotNetRepo.create_repo(repo)
        changeset_obj = DevBotNetChangeset(repo_obj, changeset)

        module_detail_parts = []
        job_infos = load_job_infos(changeset_obj.changeset_path, 'unittest')
        for job_info in job_infos:
            for job_part in job_info.part_infos:
                if job_part.modulename == module:
                    module_detail_part = DevBotNetModuleDetailPart()
                    module_detail_part.slave_name = job_info.slave_name
                    module_detail_part.slave_arch_target = job_info.slave_arch_target
                    module_detail_part.type = 'Unittest'
                    module_detail_part.output_out = job_part.exec_out
                    module_detail_part.output_stdout = job_part.exec_stdout
                    module_detail_part.output_stderr = job_part.exec_stderr
                    module_detail_parts.append(module_detail_part)

        job_infos = load_job_infos(changeset_obj.changeset_path, 'static_analysis')
        for job_info in job_infos:
            for job_part in job_info.part_infos:
                if job_part.modulename == module:
                    module_detail_part = DevBotNetModuleDetailPart()
                    module_detail_part.slave_name = job_info.slave_name
                    module_detail_part.slave_arch_target = job_info.slave_arch_target
                    module_detail_part.type = 'Static Analysis'
                    module_detail_part.output_out = job_part.exec_out
                    module_detail_part.output_stdout = job_part.exec_stdout
                    module_detail_part.output_stderr = job_part.exec_stderr
                    module_detail_parts.append(module_detail_part)
        return module_detail_parts
            
            
            
            
            

class DevBotNetReleaseInfo(object):
    def __init__(self):
        self.slave_name = ''
        self.slave_arch_target = ''
        self.output_stdout = ''
        self.output_stderr = ''
    
    @classmethod
    def create_for_changeset(cls, repo, changeset):
        repo_obj = DevBotNetRepo.create_repo(repo)
        changeset_obj = DevBotNetChangeset(repo_obj, changeset)

        release_infos = {}
        job_infos = load_job_infos(changeset_obj.changeset_path, 'release')
        idx = 0
        for job_info in job_infos:
            release_info = DevBotNetReleaseInfo()
            release_info.slave_name = job_info.slave_name
            release_info.slave_arch_target = job_info.slave_arch_target
            release_info.output_stdout = job_info.exec_stdout
            release_info.output_stderr = job_info.exec_stderr
            release_infos[u'%d'% idx] = release_info
            idx += 1
        return release_infos
            
        
        
        