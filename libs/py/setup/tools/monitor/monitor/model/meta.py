"""SQLAlchemy Metadata and Session object"""
from sqlalchemy import MetaData

from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from lib.dev_env.devbotnet_db import Base as DevbotnetDBBase

__all__ = ['Session', 'engine', 'metadata']

# SQLAlchemy database engine. Updated by model.init_model()
engine = None

# SQLAlchemy session manager. Updated by model.init_model()
Session = scoped_session(sessionmaker())


Base = DevbotnetDBBase

# Global metadata. If you have multiple databases with overlapping table
# names, you'll need a metadata for each database
#metadata = MetaData()
metadata = Base.metadata
