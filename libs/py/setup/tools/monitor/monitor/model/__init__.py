"""The application's model objects"""
import sqlalchemy as sa
from sqlalchemy import orm

from monitor.model import meta

from lib.dev_env.devbotnet_db import DevBotnetDBChangeset
from lib.dev_env.devbotnet_db import DevBotnetDBRun
from lib.dev_env.devbotnet_db import DevBotnetDBRunPart
from lib.dev_env.devbotnet_db import DevBotnetDBRunPartDetail


def init_model(engine):
    meta.Session.configure(bind=engine)
    meta.engine = engine

    # Create the tables if they don't already exist
    meta.metadata.create_all(bind=meta.engine)
