
import logging


from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort

from monitor.lib.base import BaseController, render

import monitor.model.devbotnet
import monitor.model.repo

import lib.dev_env.devbotnet_db_query

log = logging.getLogger(__name__)

class PagesController(BaseController):

    def index(self):
        c.title = "Home"
        c.repos = monitor.model.devbotnet.DevBotNetRepo.create_repos()
        return render('/pages.mako')


    def giri_repo(self):
        repo = monitor.model.repo.Repo.create_current() 
        c.repo = repo.hg_repo
        return render('giri_repo.mako')

    def giri_repo_setup(self):
        repo = monitor.model.repo.Repo.create_current() 
        c.repo = repo.hg_repo
        return render('giri_repo_setup.mako')

    def giri_repo_job(self, run_uid):
        repo = monitor.model.repo.Repo.create_current() 

        c.repo = repo.hg_repo
        
        db_run = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, run_uid)
        db_changeset = lib.dev_env.devbotnet_db_query.get_changeset_for_run(monitor.model.meta.Session, db_run)
        c.hg_change =  lib.dev_env.hg.HGChange.create_from_repo_changeset(repo.hg_root, changeset=db_changeset.changeset)
        c.run_uid = run_uid
        
        c.hg_change_compare = None
        if db_run.compare_run_uid is not None:
            db_run_compare = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, db_run.compare_run_uid)
            db_changeset_compare = lib.dev_env.devbotnet_db_query.get_changeset_for_run(monitor.model.meta.Session, db_run_compare)
            c.hg_change_compare =  lib.dev_env.hg.HGChange.create_from_repo_changeset(repo.hg_root, changeset=db_changeset_compare.changeset)
            
        return render('giri_repo_job.mako')

    def giri_repo_job_part(self, run_part_uid):
        repo = monitor.model.repo.Repo.create_current() 

        db_run_part = lib.dev_env.devbotnet_db_query.get_run_part_by_id(monitor.model.meta.Session, run_part_uid)
        db_run = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, db_run_part.run_uid)
        db_changeset = lib.dev_env.devbotnet_db_query.get_changeset_for_run(monitor.model.meta.Session, db_run)
        c.hg_change =  lib.dev_env.hg.HGChange.create_from_repo_changeset(repo.hg_root, changeset=db_changeset.changeset)
        c.repo = repo.hg_repo
        c.db_run = db_run
        c.db_run_part = db_run_part

        c.hg_change_compare = None
        c.db_run_part_compare = None
        if db_run.compare_run_uid is not None:
            db_run_compare = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, db_run.compare_run_uid)
            db_changeset_compare = lib.dev_env.devbotnet_db_query.get_changeset_for_run(monitor.model.meta.Session, db_run_compare)
            c.hg_change_compare =  lib.dev_env.hg.HGChange.create_from_repo_changeset(repo.hg_root, changeset=db_changeset_compare.changeset)
            c.db_run_part_compare = db_run_compare
        
        (c.diff_count, c.diff_info, c.diff_warning, c.diff_error) = lib.dev_env.devbotnet_db_query.get_run_part_compare_diff(monitor.model.meta.Session, db_run_part)
        return render('giri_repo_job_part.mako')

    def giri_repo_job_part_detail_download(self, run_part_detail_uid, filename):
        db_run_part_detail = lib.dev_env.devbotnet_db_query.get_run_part_detail_by_id(monitor.model.meta.Session, run_part_detail_uid)
        return db_run_part_detail.data

    def giri_repo_job_part_detail_show(self, run_part_detail_uid, filename):
        db_run_part_detail = lib.dev_env.devbotnet_db_query.get_run_part_detail_by_id(monitor.model.meta.Session, run_part_detail_uid)
        c.data = db_run_part_detail.data
        return render('giri_repo_job_part_detail_show.mako')
    