import logging
import datetime 

from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect

from monitor.lib.base import BaseController, render
import monitor.lib.decorators

import monitor.model.meta
import monitor.model.repo
import monitor.model.query


import lib.dev_env.devbotnet_db_query
import lib.dev_env.devbotnet_db_update
import lib.dev_env.devbotnet_db
log = logging.getLogger(__name__)




class AjaxController(BaseController):

    def index(self):
        # Return a rendered template
        #return render('/ajax.mako')
        # or, return a string
        return 'Hello World'


#
# Calls for the page giri_repo
#

    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_changes(self):
        callback = request.params['callback']

        repo = monitor.model.repo.Repo.create_current() 
        
        result = {}
        result_changes = []
        for change in monitor.model.query.query_repo_changes(repo):
            result_change = {}
            result_change['node'] = change.node
            result_change['desc'] = change.desc
            result_change['date'] = change.date
            result_change['author'] = unicode(change.author, errors='ignore')
            result_change['rev'] = change.rev
            result_change['job_sums_unittest'] = lib.dev_env.devbotnet_db_query.get_job_summary_for_revision(monitor.model.meta.Session, repo.hg_repo, change.node, lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_UNITTEST)
            result_change['job_sums_static_analysis'] = lib.dev_env.devbotnet_db_query.get_job_summary_for_revision(monitor.model.meta.Session, repo.hg_repo, change.node, lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_STATIC_ANALYSIS)
            result_change['job_sums_profile'] = lib.dev_env.devbotnet_db_query.get_job_summary_for_revision(monitor.model.meta.Session, repo.hg_repo, change.node, lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_PROFILE)
            result_change['job_sums_coverage'] = lib.dev_env.devbotnet_db_query.get_job_summary_for_revision(monitor.model.meta.Session, repo.hg_repo, change.node, lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE)
            result_changes.append(result_change)
        result['changes'] = result_changes
        return (callback, result)


    def _get_builder_status(self, job_infos, name, arch_target):
        for job_info in job_infos:
            if job_info['name'] == name and job_info['arch_target'] == arch_target:
                if job_info['is_running']:
                    return "running"
                else:
                    return "done"
        return "ready"
    
    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_builders(self):
        callback = request.params['callback']

        repo = monitor.model.repo.Repo.create_current() 
        
        result_builders = []

        db_config = lib.dev_env.devbotnet_db_update.get_or_create_config(monitor.model.meta.Session)

        idx = 0
        if monitor.model.repo.RepoJob.is_job_running():
            for job_info in monitor.model.repo.RepoJob.get_job_running().get_running_slave_infos():
                result_builder = {}
                result_builder['uid'] = idx
                result_builder['name'] = job_info['name']
                result_builder['arch_target'] = job_info['arch_target']
                result_builder['hg_id'] = job_info['hg_id']
                if job_info['is_running']:
                    result_builder['status'] = 'running'
                else:
                    result_builder['status'] = 'done'
                result_builders.append(result_builder)
                idx += 1
        else:
            for builder in monitor.model.query.query_repo_builders(repo, db_config.get_slave_connect_list()):
                result_builder = {}
                result_builder['uid'] = idx
                result_builder['name'] = builder.dev_env_slave_info['name']
                result_builder['arch_target'] = builder.dev_env_slave_info['arch_target']
                result_builder['hg_id'] = builder.hg_id
                result_builder['status'] = 'ready'
                result_builders.append(result_builder)
                idx += 1
            
        result = {}
        result['builders'] = result_builders
        result['builders_string'] = db_config.builders
        
        result['job_is_running'] = monitor.model.repo.RepoJob.is_job_running()
        return (callback, result)


    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_actions(self):
        callback = request.params['callback']

        result = {}
        result_actions = []
        job_is_running = False
        job_title = "job title"
        job_begin_ts = "begin_ts"
        job_durration = "durration"
        
        if not monitor.model.repo.RepoJob.is_job_running():
            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_unittest'
            result_action['title'] = 'run unittest'
            result_actions.append(result_action)
    
            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_static_analysis'
            result_action['title'] = 'run static analysis'
            result_actions.append(result_action)
    
            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_unittest_static_analysis'
            result_action['title'] = 'run all above'
            result_actions.append(result_action)

            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_coverage'
            result_action['title'] = 'run coverage'
            result_actions.append(result_action)

            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_profile'
            result_action['title'] = 'run profiling'
            result_actions.append(result_action)


            result_action = {}
            result_action['uid'] = 'giri_repo_action_run_cont'
            result_action['title'] = 'continuous'
            result_actions.append(result_action)
    
        else:

            job = monitor.model.repo.RepoJob.get_job_running()

            if job.can_cancel_job:
                result_action = {}
                result_action['uid'] = 'giri_repo_action_run_cancel'
                result_action['title'] = 'cancel job'
                result_actions.append(result_action)

            job_is_running = True
            job_title = job.job_title
            job_begin_ts = job.job_begin_ts.strftime('%Y.%m.%d %H:%M')
            job_durration = str(datetime.datetime.utcnow() - job.job_begin_ts)
        
        
        result['actions'] = result_actions
        result['job_is_running'] = job_is_running
        result['job_title'] = job_title
        result['job_begin_ts'] = job_begin_ts
        result['job_durration'] = job_durration
        
        
        db_config = lib.dev_env.devbotnet_db_update.get_or_create_config(monitor.model.meta.Session)
        result['ifilter'] = db_config.ifilter
        result['efilter'] = db_config.efilter
        return (callback, result)


    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_filters(self):
        callback = request.params['callback']
        form_ifilter = request.params['form_ifilter']
        form_efilter = request.params['form_efilter']
        save_filters = request.params.has_key('save_filters')
        
        if save_filters:
            lib.dev_env.devbotnet_db_update.set_config_filters(monitor.model.meta.Session, form_ifilter, form_efilter)

        result = {}
        db_config = lib.dev_env.devbotnet_db_update.get_or_create_config(monitor.model.meta.Session)
        result['ifilter'] = db_config.ifilter
        result['efilter'] = db_config.efilter

        
        result_filters = []
        job_is_running = monitor.model.repo.RepoJob.is_job_running()
        if not job_is_running:
            repo = monitor.model.repo.Repo.create_current() 
            for (filename, modulename) in repo.get_module_list_from_filters(db_config.ifilter, db_config.efilter):
                result_filter = {}
                result_filter['filename'] = filename
                result_filter['modulename'] = modulename
                result_filters.append(result_filter)

        result['job_is_running'] = job_is_running
        result['filters'] = result_filters
        return (callback, result)


    def giri_repo_action_run_unittest(self):    
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobUnittest()
            repo_job.start()
        return "ok"

    def giri_repo_action_run_static_analysis(self):
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobStaticAnalysis()
            repo_job.start()
        return "ok"

    def giri_repo_action_run_unittest_static_analysis(self):
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobUnittestAndStaticAnalysis()
            repo_job.start()
        return "ok"

    def giri_repo_action_run_profile(self):
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobProfile()
            repo_job.start()
        return "ok"
        
    def giri_repo_action_run_coverage(self):
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobCoverage()
            repo_job.start()
        return "ok"
        

    def giri_repo_action_run_cont(self):
        if not monitor.model.repo.RepoJob.is_job_running():
            repo_job = monitor.model.repo.RepoJobCont()
            repo_job.start()
        return "ok"

    def giri_repo_action_run_cancel(self):
        job = monitor.model.repo.RepoJob.get_job_running()
        if job is not None:
            job.cancel()
            return "ok"
        return "error"

    def giri_repo_action_save_builders(self):
        form_builders = request.params['form_builders_string']
        lib.dev_env.devbotnet_db_update.set_config_builders(monitor.model.meta.Session, form_builders)

        return "ok"

#
# Calls for the page giri_repo_job
#
    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_job(self):
        callback = request.params['callback']
        run_uid = request.params['run_uid']
        
        result = {}
        db_run = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, run_uid)

        result['computer'] = db_run.computer
        result['arch_target'] = db_run.arch_target
        result['count'] = db_run.sum_count
        result['info'] = db_run.sum_count_info
        result['warning'] = db_run.sum_count_warning
        result['error'] = db_run.sum_count_error

        if db_run.begin_ts is not None and db_run.end_ts is not None:
            result['begin_ts'] = db_run.begin_ts.strftime('%Y.%m.%d %H:%M')
            result['end_ts'] = db_run.end_ts.strftime('%Y.%m.%d %H:%M')
            durration = db_run.end_ts - db_run.begin_ts
            result['durration'] = str(durration)
        else:
            result['begin_ts'] = ""
            result['end_ts'] = ""
            result['durration'] = ""

        (diff_count, diff_info, diff_warning, diff_error) = lib.dev_env.devbotnet_db_query.get_run_compare_diff(monitor.model.meta.Session, db_run)
        result['diff_count'] = diff_count
        result['diff_info'] = diff_info
        result['diff_warning'] = diff_warning
        result['diff_error'] = diff_error
        
        if db_run.type == lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE:
            lib.dev_env.devbotnet_db_query.add_pct_threshold(db_run, result)
        
        return (callback, result)

    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_job_parts(self):
        callback = request.params['callback']
        run_uid = request.params['run_uid']

        result_parts = []
        db_run = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, run_uid)
        for db_part in lib.dev_env.devbotnet_db_query.get_run_parts(monitor.model.meta.Session, db_run):
            result_part = {}
            result_part['uid'] = db_part.uid
            result_part['module'] = db_part.module
            result_part['run_type'] = db_run.type
            
            result_part['count'] = db_part.sum_count
            result_part['info'] = db_part.sum_count_info
            result_part['warning'] = db_part.sum_count_warning
            result_part['error'] = db_part.sum_count_error

            (diff_count, diff_info, diff_warning, diff_error) = lib.dev_env.devbotnet_db_query.get_run_part_compare_diff(monitor.model.meta.Session, db_part)
            result_part['diff_count'] = diff_count
            result_part['diff_info'] = diff_info
            result_part['diff_warning'] = diff_warning
            result_part['diff_error'] = diff_error

            if db_run.type == lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE:
                lib.dev_env.devbotnet_db_query.add_pct_threshold(db_part, result_part)

            result_part['has_diff'] = diff_count > 0 or diff_info > 0 or diff_warning > 0 or diff_error > 0

            result_parts.append(result_part)
        return (callback, result_parts)


    def giri_repo_delete_job(self):
        run_uid = request.params['run_uid']

        if lib.dev_env.devbotnet_db_update.delete_run_job(monitor.model.meta.Session, run_uid):
            return "ok" 
        
        return "error"


#
# Calls for the page giri_repo_job_part
#
    def _build_part_detail(self, db_part_detail, type):
        result_part_detail = {}
        result_part_detail['uid'] = db_part_detail.uid
        result_part_detail['name'] = db_part_detail.name
        result_part_detail['data'] = str(db_part_detail.data)
        result_part_detail['status'] = db_part_detail.status
        result_part_detail['line_begin'] = db_part_detail.line_begin
        result_part_detail['line_end'] = db_part_detail.line_end
        result_part_detail['is_new'] = lib.dev_env.devbotnet_db_query.get_run_part_detail_is_new(monitor.model.meta.Session, db_part_detail, type)
        return result_part_detail


    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_job_part(self):
        callback = request.params['callback']
        run_part_uid = request.params['run_part_uid']

        db_part = lib.dev_env.devbotnet_db_query.get_run_part_by_id(monitor.model.meta.Session, run_part_uid)
        db_run = lib.dev_env.devbotnet_db_query.get_run_by_id(monitor.model.meta.Session, db_part.run_uid)
        
        result_part = {}
        result_part['uid'] = db_part.uid
        result_part['module'] = db_part.module
        result_part['run_type'] = db_run.type
        
        result_part['count'] = db_part.sum_count
        result_part['info'] = db_part.sum_count_info
        result_part['warning'] = db_part.sum_count_warning
        result_part['error'] = db_part.sum_count_error

        (diff_count, diff_info, diff_warning, diff_error) = lib.dev_env.devbotnet_db_query.get_run_part_compare_diff(monitor.model.meta.Session, db_part)
        result_part['diff_count'] = diff_count
        result_part['diff_info'] = diff_info
        result_part['diff_warning'] = diff_warning
        result_part['diff_error'] = diff_error

        if db_run.type == lib.dev_env.devbotnet_db.DevBotnetDBRun.TYPE_COVERAGE:
            lib.dev_env.devbotnet_db_query.add_pct_threshold(db_part, result_part)

        result_part['has_diff'] = diff_count > 0 or diff_info > 0 or diff_warning > 0 or diff_error > 0

        return (callback, result_part)
    
    @monitor.lib.decorators.jsonify_with_yui3_callback
    def giri_repo_request_job_part_details(self):
        callback = request.params['callback']
        run_part_uid = request.params['run_part_uid']

        db_run_part = lib.dev_env.devbotnet_db_query.get_run_part_by_id(monitor.model.meta.Session, run_part_uid)

        result = {}
        result_details = []
        for db_part_detail in lib.dev_env.devbotnet_db_query.get_run_part_details(monitor.model.meta.Session, db_run_part, lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_LINE):
            result_details.append(self._build_part_detail(db_part_detail, lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_LINE))

        for db_part_detail in lib.dev_env.devbotnet_db_query.get_run_part_details(monitor.model.meta.Session, db_run_part, lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE):
            result_details.append(self._build_part_detail(db_part_detail, lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE))
        result['details'] =  result_details

        result_module_details = []
        for db_part_detail in lib.dev_env.devbotnet_db_query.get_run_part_details_module(monitor.model.meta.Session, db_run_part):
            result_module_detail = {}
            result_module_detail['uid'] = db_part_detail.uid
            result_module_detail['name'] = db_part_detail.name
            result_module_detail['type'] = db_part_detail.type
            result_module_detail['status'] = db_part_detail.status

            result_module_detail['data_included'] = False
            result_module_detail['data_size_kb'] = int(len(db_part_detail.data) / 1024)
            if len(db_part_detail.data) < 10 * 1024:
                result_module_detail['data_text'] = str(db_part_detail.data)
                result_module_detail['data_included'] = True

            if db_part_detail.type == lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_PROFILE:
                repo = monitor.model.repo.Repo.create_current() 
                result_module_detail['data_text'] = repo.generate_default_profile(db_part_detail.data)
                result_module_detail['data_included'] = True
            elif db_part_detail.type == lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDERROR:
                if db_part_detail.name == '':
                    result_module_detail['name'] = 'std_error.txt'
            elif db_part_detail.type == lib.dev_env.devbotnet_db.DevBotnetDBRunPartDetail.TYPE_MODULE_STDOUT:
                if db_part_detail.name == '':
                    result_module_detail['name'] = 'std_out.txt'

            result_module_details.append(result_module_detail)
        result['module_details'] =  result_module_details
        
        return (callback, result)
