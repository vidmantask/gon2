'''
Created on Jun 15, 2011

@author: thwang
'''

import logging
import sys
import warnings

import formencode
import simplejson
from decorator import decorator
from formencode import api, htmlfill, variabledecode
from webob.multidict import UnicodeMultiDict

from pylons.decorators.util import get_pylons


@decorator
def jsonify_with_yui3_callback(func, *args, **kwargs):
    """Action decorator that formats output for JSON

    Given a function that will return content, this decorator will turn
    the result into JSON, with a content-type of 'application/json' and
    output it.
    
    """
    pylons = get_pylons(args)
    pylons.response.headers['Content-Type'] = 'application/json'
    (callback, data) = func(*args, **kwargs)
    if isinstance(data, (list, tuple)):
        msg = "JSON responses with Array envelopes are susceptible to " \
              "cross-site data leak attacks, see " \
              "http://pylonshq.com/warnings/JSONArray"
        warnings.warn(msg, Warning, 2)
    return str(callback) + "(" + simplejson.dumps(data) + ")"
