"""
This file constains the functionality related to a authorization session on the client
"""
from __future__ import with_statement

from lib import checkpoint 

from components.communication import tunnel_endpoint_base

from components.plugin.client_gateway import plugin_socket_auth


class AuthorizationSession(tunnel_endpoint_base.TunnelendpointSession):
    """
    TODO: missing documentation
    A session with a set of module instances with a state independent of other sessions
    """

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, plugin_manager, plugins, com_session_api, current_runtime_env):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.com_session_api = com_session_api
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.auth_plugin_socket = plugin_socket_auth.PluginSocket(plugin_manager, plugins, current_runtime_env) 
 
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        child_id = 1
        for plugin_name in self.auth_plugin_socket.plugins_filtered.keys():
            self.auth_plugin_socket.set_tunnelendpoint(plugin_name, self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id))
            self.tunnelendpoint_remote('auth_child_created_on_client', plugin_name=plugin_name, child_id=child_id)
            child_id += 1
        self.tunnelendpoint_remote('all_auth_children_created_on_client')

    def session_close(self):
        """Hard close of session. Called from main session when communication is terminated"""
        with self.checkpoint_handler.CheckpointScope("session_close", "auth_session", checkpoint.DEBUG):
            pass
 
    def remote_call_show_message(self, message):
        self.user_interface.message.set_message(self.dictionary._("Authorization information"), message)
        self.user_interface.message.display()
    
    def remote_authorization_failed(self, message):
        self.user_interface.message.set_message(self.dictionary._("Authorization information"), message)
        self.user_interface.message.display()
    
    