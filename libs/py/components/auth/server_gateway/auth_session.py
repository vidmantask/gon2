"""
This file constains the functionality related to a authorization session on the server
"""
from __future__ import with_statement
import sys
import datetime
import weakref
from lib import checkpoint 

from components.communication import tunnel_endpoint_base

from components.plugin.server_gateway import plugin_socket_auth
from plugin_types.server_gateway import plugin_type_auth
from components.management_message.server_gateway.session_send import ManagementMessageSessionSender


import components.auth.server_common.auth_graph_model as model
from components.auth.server_common.auth_analysis import AuthorizationAnalysis
from components.database.server_common.database_api import and_, or_, not_, QuerySession
import components.auth

from components.auth.server_common.rule_definitions import *
from components.auth.server_common import rule_definitions, database_schema
from components.auth.server_common.auth_graph_model import RuleCriteria
from components.database.server_common import database_api


def safe_remove(obj, list_):
    try:
        list_.remove(obj)
    except ValueError:
        pass
    except KeyError:
        pass
    



class AuthorizationManagmentMessageSender(object):
    """
    This class represent the interface to the Management Server
    """
    def __init__(self, management_message_session, unique_session_id):
        self._management_message_sender = ManagementMessageSessionSender(components.auth.component_id, management_message_session)
        self.unique_session_id = unique_session_id
        
        
    def send_auth_report(self, auth_success, report):
        self._management_message_sender.message_remote('auth_report', unique_session_id=self.unique_session_id, auth_success=auth_success, report=report)
        
    def report_auth_results(self, report):
        self._management_message_sender.message_remote('report_auth_results', unique_session_id=self.unique_session_id, report=report)
        

    
class AuthorizationSessionOptions(object):
    
    def __init__(self, always_allow_access=False, timeout=-1, authentication_rule_name=None, use_tri_state_logic=True, require_full_login=False):
        self.always_allow_access = always_allow_access
        self.timeout = int(timeout)
        self.authentication_rule_name = authentication_rule_name
        self.use_tri_state_logic = use_tri_state_logic
        self.require_full_login = require_full_login

class ReversableDict():
    
    def __init__(self):
        self.normal_dict = dict()
        self.reverse_dict = dict()
        
    def put(self, key, value):
        prev_value = self.normal_dict.get(key)
        self.normal_dict[key] = value
        if prev_value:
            self.reverse_dict.get(prev_value).remove(key)
        reverse_list = self.reverse_dict.get(value, [])
        reverse_list.append(key)
        self.reverse_dict[value] = reverse_list
        
    def update(self, key_list, value):
        for key in key_list:
            self.put(key, value)
        
    def get(self, key, default=None):
        return self.normal_dict.get(key, default)
    
    def clear(self):
        self.normal_dict.clear()
        self.reverse_dict.clear()
    
    def items(self):
        return self.normal_dict.items()
        
    def get_reverse(self, value):
        return self.reverse_dict.get(value, [])
    
    def has_reverse_not_in(self, value_list):
        for (value, key_list) in self.reverse_dict.items():
            if not value in value_list and key_list:
                return True
        return False 

    def has_reverse_in(self, value_list):
        for (value, key_list) in self.reverse_dict.items():
            if value in value_list and key_list:
                return True
        return False 
    
    def __str__(self):
        return repr(self)
    
    def __repr__(self):
        return repr(self.reverse_dict)

class AuthorizationSession(plugin_type_auth.PluginTypeAuthCallback, tunnel_endpoint_base.TunnelendpointSession):
    """
    TODO: missing documentation
    """
    def __init__(self, 
                 environment, 
                 async_service, 
                 checkpoint_handler, 
                 tunnelendpoint_tunnel, 
                 management_message_session,
                 session_info, 
                 plugins, 
                 server_session_callback, 
                 user_session_api, 
                 endpoint_session_api, 
                 options,
                 dictionary):
        
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnelendpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.plugins = plugins
        self.server_session_callback = weakref.ref(server_session_callback)
        self.async_service = async_service
        self._management_message_sender = AuthorizationManagmentMessageSender(management_message_session, session_info.unique_session_id)

        self.dictionary = dictionary
        
        #self.plugin_socket_auth = plugin_socket_auth.PluginSocket(environment.plugin_manager, self.plugins)
        self.plugin_socket_auth = plugin_socket_auth.PluginSocket(None, self.plugins)
        self.plugin_socket_auth.set_callback_all(self)
        self.traffic_callback = None
        self.state = self.STATE_STARTING
        self.calculating = False
        self.block_access = not options.always_allow_access
        self.block_access_criteria_ids = None
        self.options = options
        self.plugin_set = set()
        self.firstAccess = False
        
        self.plugins_state = ReversableDict()
        self.components_state = ReversableDict()
        
        self.possible_change = False
        
#        self.plugins_waiting_for_connection = []
#        self.plugins_starting = set()
#        self.plugins_started = set()
#        self.plugins_ready = set()
#        self.plugins_not_working = []

#        self.components_starting = set()
#        self.components_started = set()
#        self.components_ready = set()
        
        self.load_graph = True
        self.load_full_graph = False
        self.graph = model.Graph(self.checkpoint_handler)
        self.event_queue = []
        
        self._calculate_failure = False
        
        self.results = {}
        self.authorized_actions = []
        self.restricted_actions = dict()
        self.terminated = False
        
        self._auth_analysis = None
        self._auth_report_count = 0

        self.uncalculated_criterias = set()
        self.module_criteria_values = dict()
        
        self._possible_true_conditions = set()
        
        self._criteria_cache = dict()
        self._parent_map = dict()
        
        self.preload_criteria = []
        
        self.user_session_api = user_session_api
        self.user_session_active = False
        self.user_session_api.set_auth_callback(self)
        self.endpoint_session_api = endpoint_session_api
        self.endpoint_session_active = False
        self.endpoint_session_api.set_auth_callback(self)

        self.component_handlers = dict()
        self.component_handlers["user"] = self.user_session_api 
        self.component_handlers["endpoint"] = self.endpoint_session_api 
        
                 
    STATE_STARTING = "starting"
    STATE_STARTED = "started"
    STATE_READY = "ready"
    STATE_CLOSED = "closed"
    STATE_WAITING_FOR_CONNECTION = "waiting_for_connection"
    STATE_NOT_WORKING = "not_working"

         
         
    def session_start_and_connected(self):
#        import pdb; pdb.set_trace()
#        import gc;
#        gc.collect()
#        print gc.garbage
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        self.checkpoint_handler.Checkpoint("session_start_and_connected", "auth_session", checkpoint.DEBUG)
        try:
            self.set_state(self.STATE_STARTING)
            
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_preload_graph', 0)        

            self._start_plugins()

#            self._preload_graph(0)

            if self.options.timeout > 0:
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, self.options.timeout, 0, self, 'auth_timeout', 10)        
                
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("session_start_and_connected.error", "auth_session", checkpoint.CRITICAL, etype, evalue, etrace)
            self.terminate_session("Error during authorization - details have been logged on server")


    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
#        import pdb; pdb.set_trace()
        if self._calculate_failure:
            with self.checkpoint_handler.CheckpointScope("report_auth", "auth_session", checkpoint.DEBUG):
                with QuerySession() as db_session:
                    analysis_obj = AuthorizationAnalysis(db_session, self.graph, self.restricted_actions)
                self._management_message_sender.send_auth_report(False, analysis_obj.create_failure_report())
        
        with self.checkpoint_handler.CheckpointScope("session_close", "auth_session", checkpoint.DEBUG):
            self.state = self.STATE_CLOSED
            self.server_session_callback = None
                
            self.user_session_api.reset_auth_callback()
            self.user_session_api = None
            
            self.graph.clear()
            self.graph = None

            self.endpoint_session_api.reset_auth_callback()
            self.components_state.clear()
            self.plugins_state.clear()
            self.endpoint_session_api = None
            self.plugin_socket_auth.reset_all()
            self.reset_tunnelendpoint()
            
            

    def _log_report(self, report):
        try:
            for e in report:
                self.checkpoint_handler.Checkpoint("send_auth_report", "auth_session", checkpoint.DEBUG, line_entry=repr(e))
        
        except:
            etype, evalue, etrace = sys.exc_info()
            self.checkpoint_handler.CheckpointException("send_auth_report", "auth_session", checkpoint.ERROR, etype, evalue, etrace)

    def report_auth_results(self, report):
        self._log_report(report)
        self._management_message_sender.report_auth_results(report)

    def send_auth_report(self, auth_success, report):
        self._log_report(report)
        self._management_message_sender.send_auth_report(auth_success, report)

    
    def _terminate_session(self, a="dummy"):
        if not self.terminated:
            with self.checkpoint_handler.CheckpointScope("_terminate_session", "auth_session", checkpoint.WARNING):
                if self.server_session_callback and self.server_session_callback() is not None:
                    self.server_session_callback().child_close()
                self.terminated = True
                
        
    def terminate_session(self, message=None):
        if not self.terminated:
            with self.checkpoint_handler.CheckpointScope("terminate_session", "auth_session", checkpoint.WARNING, message=message):
                if message:
                    try:
                        self.tunnelendpoint_remote('remote_authorization_failed', message=message)
                    except:
                        pass
                    self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 2, 0, self, '_terminate_session', 10)        
                else:
                    self._terminate_session()
                    

    def create_auth_report(self, auth_report_count):
        if self._auth_report_count == auth_report_count:
            report = self._auth_analysis.create_auth_report()
            self.send_auth_report(True, report)

    def set_traffic_callback(self, traffic_callback):
        self.traffic_callback = weakref.ref(traffic_callback)

    def component_result_ready(self, component_name, element_type_name, result_dicts=[]):
        with self.checkpoint_handler.CheckpointScope("component_result_ready", "auth_session", checkpoint.DEBUG, component_name=component_name):
#            print "plugin_event_state_change %s" % plugin_name
            if self.state == self.STATE_STARTING:
                self.event_queue.append((component_name, element_type_name, result_dicts))
            elif self.state == self.STATE_STARTED or self.state == self.STATE_READY:
                entity_type = get_element_type_for_component_type(component_name, element_type_name)
                self._calculate(entity_type, result_dicts)


    
    def plugin_event_state_change(self, plugin_name, predicate_info=[], internal_type=None):
        with self.checkpoint_handler.CheckpointScope("plugin_event_state_change", "auth_session", checkpoint.DEBUG, plugin_name=plugin_name) as cps:
#            print "plugin_event_state_change %s" % plugin_name
            self._plugin_state_changed(plugin_name)
            if self.state == self.STATE_STARTING:
                self.event_queue.append((plugin_name, predicate_info))
            else:
                if self.plugin_socket_auth.is_ready(plugin_name):
                    entity_type = get_element_type_for_plugin_type(plugin_name, element_type=internal_type)
                    cps.add_complete_attr(entity_type=entity_type)
                    self._calculate(entity_type, predicate_info)
                        
                self.update_state()                    

                    
    def auth_timeout(self, a):
        if self.state == self.STATE_CLOSED:
            return

        if self.state != self.STATE_READY:
            self.set_state(self.STATE_READY)
            self.checkpoint_handler.Checkpoint("auth_timeout", "auth_session", checkpoint.DEBUG, 
                                               plugin_state = repr(self.plugins_state), 
                                               components_state = repr(self.components_state),
                                               )
            self._calculate_step(0)
            
                
                
                
    def _plugin_start(self, a):
        if self.state == self.STATE_CLOSED:
            return
        if self.components_state.get_reverse(self.STATE_STARTING):
            try:
                component_name = self.components_state.get_reverse(self.STATE_STARTING)[0]
                component = self.component_handlers.get(component_name)
                if not component:
                    raise Exception("Component '%s' not found" % component_name) 
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_plugin_start', 0)
                self.components_state.put(component_name, self.STATE_STARTED)
                plugin_names = component.start_auth()
                if plugin_names:
                    self.checkpoint_handler.Checkpoint("_plugin_start", "auth_session", checkpoint.DEBUG, component_name=component_name, component_plugin_name=plugin_names)
                    self.plugins_state.update(plugin_names, self.STATE_STARTING)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("component_start.error", "auth_session", checkpoint.CRITICAL, etype, evalue, etrace)
                self.terminate_session("Error during authorization - details have been logged on server")
                
        elif self.plugins_state.get_reverse(self.STATE_STARTING):
            plugin_name = self.plugins_state.get_reverse(self.STATE_STARTING)[0]
            self.checkpoint_handler.Checkpoint("_plugin_start", "auth_session", checkpoint.DEBUG, plugin_name=plugin_name)
            self._attempt_plugin_start(plugin_name)
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_plugin_start', 0)
        
    
    def _attempt_plugin_start(self, plugin_name):
        try:
            if self.plugin_socket_auth.is_plugin_loaded(plugin_name):
                self.checkpoint_handler.Checkpoint("attempt_plugin_start", "auth_session", checkpoint.DEBUG, plugin_name=plugin_name)
#                safe_remove(plugin_name, self.plugins_starting)
                self.plugin_socket_auth.start(plugin_name)
            else:
                self.checkpoint_handler.Checkpoint("_attempt_plugin_start.error", "auth_session", checkpoint.ERROR, plugin_name=plugin_name, msg="plugin not found")
                self.plugins_state.put(plugin_name, self.STATE_NOT_WORKING)
                
        except plugin_type_auth.PluginWaitingForConnectionException:
            self.plugins_state.put(plugin_name, self.STATE_WAITING_FOR_CONNECTION)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("_attempt_plugin_start.error", "auth_session", checkpoint.CRITICAL, etype, evalue, etrace)
            self.plugins_state.put(plugin_name, self.STATE_NOT_WORKING)

    
    def _plugin_state_changed(self, plugin_name):
        with self.checkpoint_handler.CheckpointScope("_plugin_state_changed", "auth_session", checkpoint.DEBUG, plugin_name=plugin_name) as cps:
        
            is_ready = self.plugin_socket_auth.is_ready(plugin_name)
            is_started = self.plugin_socket_auth.is_started(plugin_name)
            cps.add_complete_attr(is_ready=is_ready)
            cps.add_complete_attr(is_started=is_started)
            
            if is_ready:
                self.plugins_state.put(plugin_name, self.STATE_READY)
            elif is_started:
                self.plugins_state.put(plugin_name, self.STATE_STARTED)

#        if recalculate:
#            self._calculate()
            
    def set_state(self, new_state):
        # Here it id possible to do something when changing state
        old_state = self.state
        self.state = new_state
        if new_state==self.STATE_READY and old_state!=self.STATE_READY:
            self._calculate_step(0)
        if new_state==self.STATE_STARTED:
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_handle_events', 0)
        
        
    def update_state(self):
        self.checkpoint_handler.Checkpoint("update_state", "auth_session", checkpoint.DEBUG, 
                                           plugin_state = repr(self.plugins_state), 
                                           components_state = repr(self.components_state),
                                           )
        
        
        if (self.state==self.STATE_STARTED 
            and not self.plugins_state.has_reverse_in([self.STATE_STARTING, self.STATE_STARTED, self.STATE_WAITING_FOR_CONNECTION]) 
            and not self.components_state.has_reverse_in([self.STATE_STARTING, self.STATE_STARTED])): 
            self.set_state(self.STATE_READY)

    def _start_plugins(self):
        with QuerySession() as db_session:
            entity_types = [rec[0] for rec in db_session.select(database_schema.table_criteria.c.entity_type, distinct=True) if rec[0]]
            for entity_type in entity_types:
                element_type_def = def_element_types.get(entity_type)
                if element_type_def:
                    element_type = element_type_def.get("type_")
                    if element_type == "module": 
                        plugin_name = element_type_def.get("plugin") 
                        if not self.plugins_state.get(plugin_name):
                            self.plugins_state.put(element_type_def.get("plugin"), self.STATE_STARTING)
                    elif element_type == "component":
                        component_name = element_type_def.get("component_name")
                        if not self.components_state.get(component_name): 
                            self.components_state.put(element_type_def.get("component_name"), self.STATE_STARTING)
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_plugin_start', 0)
                    
#        for plugin_name in self.plugin_socket_auth.get_plugin_names():
#            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_attempt_plugin_start', plugin_name)
#            self._attempt_plugin_start(plugin_name)
            
        
        
            
        
    def _preload_graph(self, a):
        with self.checkpoint_handler.CheckpointScope("_preload_graph", "auth_session", checkpoint.DEBUG):
            if self.state == self.STATE_CLOSED:
                return
            
            try:
                self.graph = model.Graph(self.checkpoint_handler)
                self.graph.load_full_graph = self.load_full_graph
                self.graph.load_graph = self.load_graph
                self.graph.preload_graph()
                
                self._calculate_from_true_criteria(0)
            finally:
                self.set_state(self.STATE_STARTED)
    #        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_from_true_criteria', 0)
        

    def _handle_events(self, a):
        if self.event_queue:
            args = self.event_queue.pop(0)
#            print "handle_events", args
            if len(args)==3:
                self.component_result_ready(*args)
            elif len(args)==2:
                self.plugin_event_state_change(*args)
            elif len(args)==1:
                self.component_ready(*args)
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_handle_events', 0)
                

    def recalculate(self):
        try:
            # set authorized_actions to None so that traffic session is always notified  
            self.authorized_actions = None
            # reset traffic 
            self.traffic_callback().initialize_data()
            
            self._preload_graph(0)
            with QuerySession() as db_session:
                criterias = []
                for (entity_type, value_id) in self._possible_true_conditions:
                    if value_id:
                        criterias.extend(db_session.select(model.RuleCriteria, and_(model.RuleCriteria.value_id == value_id,
                                                                                    model.RuleCriteria.entity_type==entity_type,
                                                                                    model.RuleCriteria.internal_type_name=="Condition"), result_class=model.RuleNode))
                    else:
                        criterias.extend(db_session.select(model.RuleCriteria, and_(model.RuleCriteria.entity_type==entity_type,
                                                                                    model.RuleCriteria.internal_type_name=="Condition"), result_class=model.RuleNode))
            
            for criteria in criterias:
                self.uncalculated_criterias.add(criteria.id)
    
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("recalculate.error", "auth_session", checkpoint.CRITICAL, etype, evalue, etrace)
                                
                
        
    def check_element_lifetime(self, entity_type, value_id):
        with database_api.QuerySession() as dbs:
            element_lifetime = self.get_element_lifetime(entity_type, value_id, dbs)
            if element_lifetime:
                now = datetime.datetime.now()
                ok = not element_lifetime.start or element_lifetime.start < now
                if ok:
                    ok =  not element_lifetime.end or now < element_lifetime.end
                    if ok:
                        return True 
                self.checkpoint_handler.Checkpoint("check_element_lifetime", "auth_session", checkpoint.DEBUG, entity_type=entity_type, value_id=value_id, msg="Check failed")
                return False
        return True
    
    
    def get_element_lifetime(self, entity_type, value_id, dbs):
        return dbs.select_first(model.ElementLifetime, filter=database_api.and_(model.ElementLifetime.entity_type==entity_type,
                                                                                model.ElementLifetime.value_id==value_id))

    
    def _calculate_from_true_criteria(self, a):
        with self.checkpoint_handler.CheckpointScope("calculate_true_criteria", "auth_session", checkpoint.DEBUG):
            true_criteria = self.graph.get_always_true_criteria()
            
            for criteria in true_criteria:
                self.uncalculated_criterias.add(criteria.id)
                
            if self.uncalculated_criterias:
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)                    


    def _calculate_step(self, a):
        if self.state == self.STATE_CLOSED:
            return

#        if not self.user_session_api.is_ready():
#            return

        def module_check_predicate(plugin_name, internal_type, params):
            try:
                return self.plugin_socket_auth.check_predicate(plugin_name, params, internal_type)
            except plugin_type_auth.PluginWaitingForConnectionException:
                return None
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("module_check_predicate.error", "auth_session", checkpoint.CRITICAL, etype, evalue, etrace)
                return None
        
        if self.uncalculated_criterias:

            with self.checkpoint_handler.CheckpointScope("_calculate_step", "auth_session", checkpoint.DEBUG):        
                criteria_id = self.uncalculated_criterias.pop()
                
                try:
                        
                    with QuerySession() as db_session:
                        criteria = db_session.get(model.RuleCriteria, criteria_id, result_class=model.RuleNode)
                        criteria_node = self.graph.get_or_create_node(criteria)
                        result = False
                        if self.options.use_tri_state_logic:
                            result = criteria_node.value
                            if not result is None:
                                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)
                        
                        element_def = def_element_types.get(criteria.entity_type)
                        if not element_def:
                            raise Exception("No element def for type '%s'" % criteria.entity_type)
                        if element_def.get("type_") == "module":
    #                        params = dict()
    #                        params[str(element_def.get("parameter_name"))] = (criteria.value_id, criteria.title)
                            result = module_check_predicate(element_def.get("plugin"), element_def.get("internal_element_type"), (criteria.value_id, criteria.title))
                        elif element_def.get("type_") == "component":
                            component_name = element_def.get("component_name")
                            handler = self.component_handlers.get(component_name)
                            if handler:
                                internal_type_name = element_def.get("internal_element_type")
                                result = handler.check_condition(internal_type_name, criteria.value_id, criteria.title)   
                        else:
                            if criteria_node.aggregation_kind == RuleCriteria.AllOf and len(criteria_node.get_children())==0:
                                result = True                 
            
                        
                        with self.checkpoint_handler.CheckpointScope("set_true_bottom_up", "auth_session", checkpoint.DEBUG, criteria_title=criteria.title):        
                            if result:
                                criteria_node.set_true_bottom_up(db_session)
                                self.possible_change = True
                            else:
                                criteria_node.value = False
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("calculate_step.error", "auth_session", checkpoint.ERROR, etype, evalue, etrace)
                
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)
        else:
            
            with self.checkpoint_handler.CheckpointScope("calculate", "auth_session", checkpoint.DEBUG):        

                action_ids = [a.id for a in self.graph.enabled_actions]
                
                deactivated_actions = self._get_zone_restricted_actions(action_ids)
                
                action_ids.sort()
                change = action_ids != self.authorized_actions or self.restricted_actions.keys() != deactivated_actions.keys() 
                self.authorized_actions = action_ids
                self.restricted_actions = deactivated_actions
                        
                self.checkpoint_handler.Checkpoint("calculate.enabled_actions", "auth_session", checkpoint.DEBUG, action_ids = repr(action_ids), deactivated_actions=deactivated_actions)
                
                if not self.firstAccess and self.state==self.STATE_READY and self.block_access and not action_ids:
                    self.checkpoint_handler.Checkpoint("calculate.acces_denied", "auth_session", checkpoint.DEBUG, msg = "Insufficient Authorization")
                    self._calculate_failure = True
                    self.terminate_session(self.dictionary._("Insufficient Authorization"))
                    return
                    
                if change and self.traffic_callback():
                    if action_ids and not self.firstAccess:
                        self.firstAccess = True
                        self.first_access_given()
                    self.traffic_callback().traffic_update_ids(action_ids, deactivated_actions)
                    for action_id in action_ids:
                        node = self.graph.get_node(action_id)
#                        print node.title

                if self.possible_change:
                    self.possible_change = False
                                        
                if action_ids:
                    if not self._auth_analysis:
                            with QuerySession() as db_session:
                                self._auth_analysis = AuthorizationAnalysis(db_session, self.graph, self.restricted_actions)
                    else:
                        self._auth_analysis.graph = self.graph
                    
                    self._auth_report_count +=1
                    self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 2, 0, self, 'create_auth_report', self._auth_report_count)


    def get_user_message_for_zone_restriction(self, zone_element):
        if not zone_element.description:
            return "Zone restriction '%s' not met" % zone_element.title
        else:
            return zone_element.description

    def _get_zone_restricted_actions(self, action_ids):
        restricted_actions = dict() 
        with database_api.QuerySession() as dbs:
            for action_id in action_ids:
                restrictions = dbs.select(model.RuleRestriction, filter=database_api.and_(model.RuleRestriction.restriction_type=="Zone",
                                                                                          model.RuleRestriction.rule_id==action_id))
                zone_ids = [e.restriction_rule_id for e in restrictions]
                false_zone_info = []
                for zone_id in zone_ids:
                    zone_element = self.graph.get_node(zone_id)
                    if not zone_element:
                        self.checkpoint_handler.Checkpoint("_get_zone_restricted_actions", "auth_session", checkpoint.DEBUG, msg="Zone element not found", zone_id=zone_id)
                        zone_element = dbs.get(model.RuleCriteria, zone_id)
                        self.graph.add_node(zone_element)
                        false_zone_info.append(self.get_user_message_for_zone_restriction(zone_element))
                    if not zone_element.value:
                        info = self.get_user_message_for_zone_restriction(zone_element)
                        false_zone_info.append(info)
                    else:
                        false_zone_info = []
                        break
                if false_zone_info:
                    restricted_actions[action_id] = false_zone_info
            return restricted_actions
                    
                
    
        
        
    def _calculate(self, entity_type, result_dicts=[]):
        """Perform calculations based on current state in modules. 
        
        The calculation might change module state and trig events which might cause messages to be sent to client.
        """
        
        with self.checkpoint_handler.CheckpointScope("calculate_init", "auth_session", checkpoint.DEBUG):
            try:        
        
        
#        if not self.user_session_api.is_ready():
#            return

#        self.uncalculated_criterias = set()
                
                plugin_info = []
                if result_dicts:
                    for result_dict in result_dicts:
                        if result_dict:
                            value = result_dict.get("value")
                        else:
                            value = None
                            
                        if not value:
                            self.checkpoint_handler.Checkpoint("_calculate", "auth_session", checkpoint.DEBUG, entity_type=entity_type, msg="No true results for this type")
                            return
                        
                        plugin_info.append((entity_type, value))
                            
                        if value and result_dict.has_key("alert"):
                            alert_dict = result_dict.get("alert")
                            component_name = alert_dict.get("component")
                            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_alert_component', (component_name, alert_dict))
                            self.components_state.put(component_name, self.STATE_STARTING)
                            
                else:                    
                    plugin_info.append((entity_type, None))
                        
                criterias = []
                with QuerySession() as db_session:
                    for (element_def_name, value) in plugin_info:
                        if value:
                            criterias.extend(db_session.select(model.RuleCriteria, and_(model.RuleCriteria.value_id == value,
                                                                                        model.RuleCriteria.entity_type==element_def_name,
                                                                                        model.RuleCriteria.internal_type_name=="Condition"), result_class=model.RuleNode))
                        else:
                            criterias.extend(db_session.select(model.RuleCriteria, and_(model.RuleCriteria.entity_type==element_def_name,
                                                                                     model.RuleCriteria.internal_type_name=="Condition"), result_class=model.RuleNode))
                    
                for criteria in criterias:
                    self.uncalculated_criterias.add(criteria.id)
    
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)                    
    
                # for recalculation
                self._possible_true_conditions.update(plugin_info)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("calculate.error", "auth_session", checkpoint.ERROR, etype, evalue, etrace)
        
#        with self.checkpoint_handler.CheckpointScope("calculate_init", "auth_session", checkpoint.DEBUG):        
#            db_session = QuerySession()
#            parameters = db_session.select(model.ModuleCriteriaParameter)
#            for parameter in parameters:
#                self.uncalculated_criterias.add(parameter.module_criteria_id)
#            self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_calculate_step', 0)                    
            
    def component_ready(self, component):
        if self.state == self.STATE_STARTING:
            self.event_queue.append((component,))
        else:
            with self.checkpoint_handler.CheckpointScope("component_ready", "auth_session", checkpoint.DEBUG) as cps:        
                for (name, comp) in self.component_handlers.items():
                    if comp==component:
                        self.components_state.put(name, self.STATE_READY)
                        cps.add_complete_attr(component=name)
                        self.update_state()
                        break
                                
            
                
    def _alert_component(self, (component_name, params)):
        component = self.component_handlers.get(component_name)
        if component:
            component.alert(**params)
        
        

    def auth_child_created_on_client(self, plugin_name, child_id):
        with self.checkpoint_handler.CheckpointScope("auth_child_created_on_client", "auth_session", checkpoint.DEBUG, plugin_name=plugin_name, child_id=child_id):
            if self.plugin_socket_auth.is_plugin_loaded(plugin_name):
                plugin_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, child_id)
                self.plugin_socket_auth.set_tunnelendpoint(plugin_name, plugin_tunnelendpoint)
                if plugin_name in self.plugins_state.get_reverse(self.STATE_WAITING_FOR_CONNECTION):
                    self.plugins_state.put(plugin_name, self.STATE_STARTING)
                    self._attempt_plugin_start(plugin_name)
                else:
                    # This plugin is not used for authorization 
                    pass
            else:
                self.checkpoint_handler.Checkpoint("auth_child_created_on_client.plugin_not_loaded", "auth_session", checkpoint.WARNING, pluginname=plugin_name)
                    
    def first_access_given(self):
        try:
            self.user_session_api.auth_access_given()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("first_access_given.error", "auth_session", checkpoint.ERROR, etype, evalue, etrace)

    def all_auth_children_created_on_client(self):
        plugins_waiting_for_connection = self.plugins_state.get_reverse(self.STATE_WAITING_FOR_CONNECTION)
        self.plugins_state.update(plugins_waiting_for_connection, self.STATE_NOT_WORKING)
        for plugin_name in plugins_waiting_for_connection:
            self.checkpoint_handler.Checkpoint("auth_session.plugin_not_connected", "auth_session", checkpoint.WARNING, name=plugin_name)

    def _get_assignment_status(self, criteria):
        activated = False
        user_id = None
        parents = criteria.get_parents(only_active=False)
        for parent in parents:
            if parent.internal_type_name == model.RuleCriteria.TYPE_ASSSOCIATION_CLAUSE:
                sub_criteria = parent.get_children()
                for child in sub_criteria:
                    if child.id == criteria.id:
                        continue
                    if child.entity_type == "User":
                        user_id = child.value_id
                        if parent.get_parents(only_active=True):
                            activated = True
                        break
                if not user_id:
                    activated, user_id = self._get_assignment_status(parent)
            if user_id:
                break
        return activated, user_id


        
    def get_enrollment_status(self, plugin_name, element_id, is_endpoint, db_session):
        activated = False
        current_user = False
        element_type = get_element_type_for_plugin_type(plugin_name)
        matching_conditions = db_session.select(model.RuleCriteria, and_(model.RuleCriteria.value_id == element_id,
                                                                        model.RuleCriteria.entity_type==element_type,
                                                                        model.RuleCriteria.internal_type_name=="Condition"), result_class=model.RuleNode)
        
        matching_conditions = [self.graph.get_or_create_node(c) for c in matching_conditions]
        current_user_id = self.user_session_api.get_user_id()
        for condition in matching_conditions:
            tmp_activated, tmp_user_id = self._get_assignment_status(condition)
            if tmp_user_id and current_user_id == tmp_user_id:
                current_user = True
                if tmp_activated:
                    activated = True
                    break

        return activated, current_user
    
    def require_full_login(self):
        return self.options.require_full_login
    