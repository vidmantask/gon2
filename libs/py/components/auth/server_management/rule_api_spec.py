


_element_types = { 
                    u"User" : dict( title=u"User", 
                                    title_plural=u"Users", 
                                    rule_title = u"Users",
                                    type_=u"user", 
                                    plugin_element_type=u"user",
                                    ),
                    u"Group" : dict( title=u"Group", title_plural=u"Groups", rule_title = u"Groups",
                                     type_=u"user", plugin_element_type=u"group"),
                    u"Key" :         dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens", 
                                             elements = [u"SoftToken", u"MicroSmart"], 
                                             
                                             ),
                                     
                    u"SoftToken" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                   type_=u"module", plugin=u"soft_token", predicate=u"has_id", parameter_name = u"key"),
                    u"MicroSmart" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                   type_=u"module", plugin_providers=[u"soft_token", u"micro_smart", u"smart_card", u"hagiwara"], predicate=u"has_id", parameter_name = u"key"),
                    u"SmartCard" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                   type_=u"module", plugin_providers=[u"soft_token", u"micro_smart", u"smart_card", u"hagiwara"], predicate=u"has_id", parameter_name = u"key"),
                    u"Hagiwara" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                   type_=u"module", plugin_providers=[u"soft_token", u"micro_smart", u"smart_card", u"hagiwara"], predicate=u"has_id", parameter_name = u"key"),
                    u"KeyAssignment" : dict( title=u"Authentication Strength", 
                                             title_plural=u"Authentication Strength", 
                                             rule_title = u"Verified Authentication Strength",
                                             type_=u"class",
                                             rule = dict( conditions = [u"Key", u"User", u"Group"], 
                                                          unique_conditions = [u"Key"], 
                                                          mandatory_conditions = [[u"Key", u"User"], [ u"Key",  u"Group"]], 
                                                          title=u"Rules for Auth. Strength", 
                                                          long_title=u"Rules for Authentication Strength" ),
                                             
                                             ),
                    u"ApplicationAccess" : dict( title=u"Application Class", 
                                                 title_plural=u"Application Classes", 
                                                 rule_title = u"Authorized Application Class", 
                                                 type_=u"class", 
                                                 rule = dict( conditions = [u"User", u"Group"], 
                                                              title=u"Authorization Rules", 
                                                              long_title=u"Application Authorization Rules"  ),
                                                 ),
                    u"ProgramAccess" : dict( title=u"Launch Spec", 
                                             title_plural=u"Launch specs", 
                                             rule_title = u"Allowed Launch Spec",
                                             type_=u"action", 
                                             rule = dict( conditions = [u"ApplicationAccess", u"TokenGroupMix"], 
                                                          title=u"Launch Rules", 
                                                          long_title=u"Launch Rules"  ),
                                             
                                             ),
                                             
                    u"TokenGroups" : dict( title=u"Token groups", 
                                           title_plural=u"Token groups", 
                                             rule_title = u"Token groups",
                                             type_=u"class", 
                                             rule = dict( conditions = [u"Key"], 
                                                          title=u"Token Groups", 
                                                          long_title=u"Token Groups"  ),
                                             
                                             ),
                                             
                    u"TokenGroupMix" : dict( title=u"Token groups", 
                                             title_plural=u"Token groups", 
                                             type_=u"composite",
                                             elements = [u"TokenGroups", u"KeyAssignment"], 
                                             
                                             ),

                    
                    u"TestResult" : dict( title=u"Test", 
                                          type_=u"class", 
                                          info=u"TestInfo",
                                          rule = dict( conditions = [u"TestElement1", u"TestElement2", u"TestElement3"] 
                                                      ),
                                          ),
                    u"TestAction" : dict( title=u"Test1", 
                                          type_=u"action", 
                                          info=u"TestInfo1",
                                          rule = dict( conditions = [u"TestElement1", u"TestResult"] ),
                                          
                                         ),
                    u"TestElement1" : dict( title=u"TestElement1", type_=u"module", plugin_providers =[u"test_plugin1"], predicate=u"test", parameter_name = u"test"),
                    u"TestElement2" : dict( title=u"TestElement2", type_=u"module", plugin_providers=[u"test_plugin2"],  predicate=u"test", parameter_name = u"test"),
                    u"TestElement3" : dict( title=u"TestElement3", type_=u"module", plugin_providers=[u"test_plugin3", u"test_plugin4"],  predicate=u"test3", parameter_name = u"test3"),
                    #u"TstElement4" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement1", u"TestElement2", u"TestElement3"]),
                    #u"TestElement3" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement4", u"TestElement5"]),
                    #u"TestElement4" : dict( title=u"TestElement4", type_=u"module", name=u"test_plugin4",  predicate=u"test4", parameter_name = u"test4"),
                    #u"TestElement5" : dict( title=u"TestElement5", type_=u"module", name=u"test_plugin5",  predicate=u"test5", parameter_name = u"test5"),
                 }

# Still needs refactoring
_built_in_elements = [
                       dict( title=u"Personal Token", 
                             element_type = u"KeyAssignment", 
                             mandatory_conditions = [[u"Key", u"User"]], 
                             default_in_rules=[u"KeyAssignment"]) 
                     ]
