'''
Created on 14/10/2010

@author: pwl
'''
import sys

from components.admin_ws.config_api import ComponentConfigAPI
from components.management_message.server_management.session_recieve import ManagementMessageSessionRecieverBase
from components.auth.server_common import sink_component_id
from components.database.server_common import database_api as database_api 
from components.auth.server_common import database_schema as database_schema
from components.auth.server_management import auth_graph_model as auth_graph_model
from components.auth.server_common import rule_definitions
from components.auth.server_management import ElementType, OperationNotAllowedException
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.user.server_common.database_schema as user_database
import components.plugin.server_management.plugin_socket_user

from components.auth.server_common import auth_graph_model as common_model
import rule_api
import lib.checkpoint


class AdminAccessElement(ElementType):

    def __init__(self, admin_access):
        self.entity_type = AuthAdmin.entity_type
        self.element_id = admin_access.child_id
        self._label = admin_access.title
        
        
    def get_id(self):
        return self.element_id
    
    def get_label(self):
        return self._label

    def get_entity_type(self):
        return self.entity_type

class AccessElement(object):
    
    def __init__(self, name, title):
        self.element_type, dot, self.name = name.partition(".")
        self.title = title
        self.read = True
        self.create = False
        self.update = False
        self.delete = False
        
        
    def set_crud_enabling(self, access_level):
        if access_level == AuthAdmin.ACCESS_LEVEL_NONE:
            self.read = False
        elif  access_level == AuthAdmin.ACCESS_LEVEL_RW:
            self.create = True
            self.update = True
            self.delete = True
            

class LoginException(Exception):
    
    def __init__(self, error_msg, error_code=-1):
        Exception.__init__(self, error_msg)
        self.error_code = error_code
        self.error_msg = error_msg    

class AuthAdmin(ManagementMessageSessionRecieverBase, ComponentConfigAPI):
    
    def __init__(self, environment, user_admin, plugin_socket_report, license_handler, activity_log_manager):
        ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, sink_component_id)
        self.license_handler = license_handler
        self.activity_log_manager = activity_log_manager
        self._user_admin = user_admin
        self._plugin_socket_report = plugin_socket_report
        self._reports =  None
        self._update_built_in_elements()
        self.graph = None
        self._admin_access = None
        self._calculate_initial()
        self._access_map = dict()
        
        self.plugin_socket_user = components.plugin.server_management.plugin_socket_user.PluginSocket(environment.plugin_manager)
        self._login_module_dict = None
        self.authentication_plugin = None        
            
        self._activity_log_manager = activity_log_manager
        
    ACCESS_LEVEL_NONE = ""
    ACCESS_LEVEL_R = "r"
    ACCESS_LEVEL_RW = "rw"
    
    ACCESS_TYPE_RULE = "Rule"
    ACCESS_TYPE_ELEMENT = "Entity"
    ACCESS_TYPE_REPORT = "Report"
    ACCESS_TYPE_STANDARD = "Standard"
    
    ACCESS_DEF_ADMINISTRATOR = u"Administrator"     
    ACCESS_DEF_TOKEN_ASSIGNMENT = u"Token Manager"

    entity_type = "AdminAccess"
    
    STANDARD_ACCESS_GATEWAY_CONFIG = "GatewayConfig"
    
    _built_in_ids = None
    
    standard_access_defs = [ { "name" : STANDARD_ACCESS_GATEWAY_CONFIG,
                               "title" : "Gateway server configuration",
                               "crud_enabled" : True 
                              } 
                           ]
    
    
    for access_def in standard_access_defs:
        access_def["name"] = ACCESS_TYPE_STANDARD + ".%s" % access_def.get("name")
        access_def["type"] = ACCESS_TYPE_STANDARD
        
    
     

    def get_elements(self, element_type, search_filter=None):
        with database_api.QuerySession() as dbs:
            elements = dbs.select([database_schema.table_criteria, database_schema.table_sub_criteria], 
                                   filter=database_api.and_(database_schema.table_criteria.c.internal_type_name == auth_graph_model.AdminAccess.TYPE_ADMIN_ACCESS,
                                                            database_schema.table_criteria.c.id == database_schema.table_sub_criteria.c.parent_id,
                                                            )
                                   )
            return [AdminAccessElement(a) for  a in elements]
        
        
    def get_specific_elements(self, internal_element_type, element_ids):
        with database_api.QuerySession() as dbs:
            elements = dbs.select([database_schema.table_criteria, database_schema.table_sub_criteria], 
                                   filter=database_api.and_(database_schema.table_criteria.c.internal_type_name == auth_graph_model.AdminAccess.TYPE_ADMIN_ACCESS,
                                                            database_schema.table_criteria.c.id == database_schema.table_sub_criteria.c.parent_id,
                                                            database_api.in_(database_schema.table_sub_criteria.c.child_id, element_ids)
                                                            
                                                            )
                                   )
            return [AdminAccessElement(a) for  a in elements]
           


    def get_create_templates(self, internal_element_type):
        config_spec = self._get_config_spec(None)
        return [config_spec.get_template()]
    
    _built_in_access_def = { ACCESS_DEF_ADMINISTRATOR : dict( title=u"Administrator"), 
                             ACCESS_DEF_TOKEN_ASSIGNMENT : dict( title=u"Token Manager", 
                                                                 write_access = ["Rule.PersonalTokenAssignment", "Rule.TokenGroupMembership", "Entity.Token", "Entity.TokenGroupMembership", "Entity.Endpoint"],
                                                                 read_access = ["Entity.User", "Entity.PersonalTokenAssignment"]), 
                         }
    
    def _calculate_initial(self):
        with database_api.QuerySession() as dbs:
            self.graph = common_model.Graph(self.checkpoint_handler)
            self.graph.load_full_graph = False
            self.graph.load_graph = True
            self.graph.action_type = common_model.RuleCriteria.TYPE_ADMIN_ACCESS
            self.graph.preload_graph()
            
            criteria = self.graph.get_always_true_criteria()
            for c in criteria:
                c.set_true_bottom_up(dbs)
            self._calculate_admin_acces()
            
            
    def _calculate(self, user_id, plugin):
        internal_user_id = "%s.%s" % (plugin.plugin_name, user_id)
        criterias = []
        with database_api.QuerySession() as db_session: 
            criterias.extend(db_session.select(common_model.RuleCriteria, 
                                               database_api.and_(common_model.RuleCriteria.value_id == internal_user_id,
                                                    common_model.RuleCriteria.entity_type=="User",
                                                    common_model.RuleCriteria.internal_type_name==common_model.RuleCriteria.TYPE_CONDITION), 
                                                result_class=common_model.RuleNode))
    
            user_groups = None
            try:
                user_groups = self.authentication_plugin.get_group_ids_for_logged_in_user()
#                if not user_groups is None:
#                    with QuerySession() as dbs:
#                        n_groups = dbs.select_count(database.GroupInfo)
#                    if n_groups < len(user_groups):
#                        user_groups = None
                        
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("_calculate", "auth_admin", lib.checkpoint.WARNING, etype, evalue, etrace)
            
            if user_groups is None:
                group_criterias = db_session.select(common_model.RuleCriteria, 
                                                   database_api.and_(common_model.RuleCriteria.entity_type=="Group",
                                                                     common_model.RuleCriteria.internal_type_name==common_model.RuleCriteria.TYPE_CONDITION), 
                                                    result_class=common_model.RuleNode)
                
                
                for group_criteria in group_criterias:
                    plugin_name, dot, external_group_id = group_criteria.value_id.partition(".")
                    if plugin_name == self.authentication_plugin.plugin_name:
                        if self.authentication_plugin.is_logged_in_member((external_group_id, group_criteria.title)):
                            criterias.append(group_criteria)
                
                
            elif user_groups:
                user_group_ids = ["%s.%s" % (plugin.plugin_name, group_id) for group_id in user_groups]
                index = 0
                
                while index<len(user_group_ids):
                    criterias.extend(db_session.select(common_model.RuleCriteria, 
                                                       database_api.and_(database_api.in_(common_model.RuleCriteria.value_id, user_group_ids[index:index + database_api.IN_CLAUSE_LIMIT]),
                                                                         common_model.RuleCriteria.entity_type=="Group",
                                                                         common_model.RuleCriteria.internal_type_name==common_model.RuleCriteria.TYPE_CONDITION), 
                                                        result_class=common_model.RuleNode))
                    index = index + database_api.IN_CLAUSE_LIMIT
                    
            criterias = [self.graph.get_or_create_node(c) for c in criterias]
            for c in criterias:
                c.set_true_bottom_up(db_session)
                    
        self._calculate_admin_acces()

    @classmethod
    def gives_more_access(cls, new_access_level, old_access_level):
        if old_access_level != new_access_level:
            if new_access_level == cls.ACCESS_LEVEL_RW:
                return True
            if new_access_level == cls.ACCESS_LEVEL_R and old_access_level == cls.ACCESS_LEVEL_NONE:
                return True
            
        return False
    
    def give_access(self, access_def_name):
        with database_api.QuerySession() as dbs:
            elements = dbs.select(database_schema.table_criteria, 
                                   filter=database_api.and_(database_schema.table_criteria.c.internal_type_name == auth_graph_model.AdminAccess.TYPE_ADMIN_ACCESS,
                                                            database_schema.table_criteria.c.title == access_def_name,
                                                            ),
                                    result_class = common_model.RuleNode 
                                   )
            if not elements:
                self.checkpoint_handler.Checkpoint('give_access', "auth_admin", lib.checkpoint.WARNING, msg="Unable to find access definition '%s'" % access_def_name)
            else:
                self.graph.enabled_admin_access.add(elements[0])
                self._calculate_admin_acces()
        
                
    def _calculate_admin_acces(self):
        self._access_map = dict()
        self._admin_access = dict()
        with database_api.QuerySession() as dbs:
            admin_access_ids = [a.id for a in self.graph.enabled_admin_access]
            assert len(admin_access_ids) < 1000
            access_list = dbs.select([database_schema.table_access_element_attribute, database_schema.table_access_right],
                                     filter=database_api.and_(database_api.in_(database_schema.table_access_element_attribute.c.access_element_id, admin_access_ids),
                                                              database_schema.table_access_element_attribute.c.access_right_id == database_schema.table_access_right.c.id,
                                                              )
                      )
            tmp_dict = dict()
            for access_def in access_list:
                existing = tmp_dict.get(access_def.name)
                if not existing or (existing and self.gives_more_access(access_def.access_level, existing.access_level)):
                    tmp_dict[access_def.name] = access_def
                    
            for access_def in tmp_dict.values():
                access_element = AccessElement(access_def.name, access_def.title)
                access_element.set_crud_enabling(access_def.access_level)
                print "Access to %s : %s" % (access_def.name, access_def.access_level)
                self._admin_access[access_def.name] = access_element
                
    def get_admin_access(self):
        if self._admin_access is None:
            self._calculate_admin_acces()
        return self._admin_access
    
    def get_report_access(self):
        admin_access = self.get_admin_access()
        return dict([(a.name,a) for a in admin_access.values() if a.element_type==self.ACCESS_TYPE_REPORT])
    
    def _get_reports(self):
        if not self._reports:
            self._reports = self._plugin_socket_report.get_reports()
        return self._reports
    
    def _find_report_spec(self, report_name):
        reports = self._get_reports()
        for report in reports:
            if report.report_id == report_name:
                return report
        return None

    def _find_report_specs(self, specification_filename=None, data_id=None):
        result = []
        if specification_filename or data_id:
            reports = self._get_reports()
            for report_spec in reports:
                if specification_filename and specification_filename not in report_spec.sub_specification_filenames:
                    continue
                if data_id and data_id not in report_spec.data_ids:
                    continue
                result.append(report_spec)
                
        return result
    
    def check_report_access(self, report_name=None, specification_filename=None, data_id=None):
        report_access = self.get_report_access()
        if report_name:
            access = report_access.get(report_name)
            if not access or not access.read:
                return report_name
            if specification_filename or data_id:
                report_spec = self._find_report_spec(report_name)
                assert(report_spec)
                if specification_filename and specification_filename not in report_spec.sub_specification_filenames:
                    return report_name
                if data_id and data_id not in report_spec.data_ids:
                    return report_name
            return None

        report_specs = self._find_report_specs(specification_filename, data_id)
        report_name = "<not found>"
        for report_spec in report_specs:
            report_name = report_spec.report_id 
            access = report_access.get(report_spec.report_id)
            if access and access.read:
                return None
        return report_name
        

    def get_all_admin_access(self):
        admin_access = self.get_admin_access().copy()
        with database_api.QuerySession() as dbs:
            access_right_elements = dbs.select(auth_graph_model.AccessRight)
            for a in access_right_elements:
                if not admin_access.has_key(a.name):
                    admin_access[a.name] = None
        return admin_access
        
            
    def get_accesss_types_for_element_type(self, element_type):
        access_mapping = rule_definitions.get_access_mapping()
        
    def get_rule_access(self, rule_type):
        admin_access = self.get_admin_access()
        rule_type_name = "Rule.%s" % rule_type
        return admin_access.get(rule_type_name)


    def get_standard_access(self, access_type):
        admin_access = self.get_admin_access()
        access_type_name = "%s.%s" % (self.ACCESS_TYPE_STANDARD, access_type)
        return admin_access.get(access_type_name)


    
    def merge_access(self, access_list, element_name):
        access_element = AccessElement("Entity.%s" % element_name, element_name)
        access_element.create = True
        access_element.update = True
        access_element.delete = True
        for a in access_list:
            if not a.create:
                access_element.create = False
            if not a.update:
                access_element.update = False
            if not a.delete:
                access_element.delete = False
        return access_element
                 
            
    def _lookup_element_access(self, element_type):
        element_type_name = "Entity.%s" % element_type
        admin_access = self.get_admin_access()
        return admin_access.get(element_type_name)

    def get_element_access(self, element_type):
        access_list = self._access_map.get(element_type)
        if access_list is None:
            element_access_map = rule_definitions.get_access_mapping()
            mapping = element_access_map.get(element_type)
            assert mapping
            access_list = []
            if isinstance(mapping, basestring):
                access = self._lookup_element_access(mapping)
                if access:
                    access_list.append(access)
            else:
                for name, access_element_type in mapping:
                    access = self._lookup_element_access(access_element_type)
                    if access:
                        access_list.append(access)
            self._access_map[element_type] = access_list

        return access_list
#        if access_list:
#            if len(access_list==1):
#                return access_list[0]
#            else:
#                return self.merge_access(access_list, element_type)
#        else:
#            return None
    
    def get_access_error_message(self, access_element_name, element_type, access_type):
        with database_api.QuerySession() as dbs:
            name = "%s.%s" % (element_type, access_element_name)
            access_right = dbs.select_first(auth_graph_model.AccessRight, filter=auth_graph_model.AccessRight.name == name)
            if access_right:
                title = access_right.title
            else:
                self.checkpoint_handler.Checkpoint('get_access_error_message', "auth_admin", lib.checkpoint.WARNING, msg="Unable to find access right named '%s'" % name)
                title = name
            return  "Missing Authorization to %s data of type '%s'" % (access_type, title) 
        
        

    @classmethod
    def _create_access_attribute(cls, dbt, access_def, access_right, access_level):
        new_attribute = dbt.add(auth_graph_model.AdminAccessAttribute())
        new_attribute.access_right = access_right
        new_attribute.access_element = access_def
        new_attribute.access_level = access_level
        print "_create_access_attribute", access_def.title, access_right.name
        


    @classmethod
    def _create_rule_object(cls, dbt, access_def):
        access_rule = dbt.add(auth_graph_model.Rule())
        access_rule.title = access_def.title
        access_rule.entity_type = cls.entity_type
        access_def.add_sub_criteria(access_rule)
        return access_rule

    def _update_built_in_elements(self):
        if not self._built_in_ids is None:
            return
        
        with database_api.Transaction() as dbt:
            
            auth_access_defs = rule_definitions.get_access_defs()
            auth_access_defs.extend(self.standard_access_defs)
            access_right_elements = dbt.select(auth_graph_model.AccessRight)
            
            read_only_object_names = set()    
            
            access_right_dict = dict([(a.name,a) for a in access_right_elements])
            for access_def in auth_access_defs:
                name = access_def.get("name")
                access_right = access_right_dict.get(name) 
                if not access_def.get("crud_enabled"):
                    read_only_object_names.add(name)
                if not access_right:
                    access_right = dbt.add(auth_graph_model.AccessRight())
                    access_right.name = name
                    access_right.title = access_def.get("title")
                    access_right.element_type = access_def.get("type")
                    access_right_dict[name] = access_right
                    
            reports = self._plugin_socket_report.get_reports()
            for report in reports:
                report_name = "Report.%s" % report.report_id
                read_only_object_names.add(report_name)
                access_right = access_right_dict.get(report_name) 
                if not access_right:
                    access_right = dbt.add(auth_graph_model.AccessRight())
                    access_right.name = report_name
                    access_right.title = "%s (Report)" % report.report_title  
                    access_right.element_type = self.ACCESS_TYPE_REPORT
                    access_right_dict[report_name] = access_right
                
            access_defs = dbt.select(auth_graph_model.AdminAccess)
            access_defs_dict = dict([(a.title, a) for a in access_defs])
            for name, access_spec in self._built_in_access_def.items():
                access_def = access_defs_dict.get(name)
                if not access_def:
                    access_def = dbt.add(auth_graph_model.AdminAccess())
                    access_def.title = access_spec.get("title")
                    
                    access_rule = self._create_rule_object(dbt, access_def)
                    access_def_created = access_rule
                else:
                    access_def_created = None
                
                existing_attribute_dict = dict([(a.access_right.name, a) for a in access_def.access_attributes])
                if name == self.ACCESS_DEF_ADMINISTRATOR:
                    for name, access_right in access_right_dict.items():
                        existing_attribute = existing_attribute_dict.get(name)
                        if name in read_only_object_names:
                            access_level = self.ACCESS_LEVEL_R
                        else:
                            access_level = self.ACCESS_LEVEL_RW
                        if not existing_attribute:
                            self._create_access_attribute(dbt, access_def, access_right, access_level)
                        else:
                            existing_attribute.access_level = access_level
                    if access_def_created:
                        # Create default rule_clause
                        rule_clause = dbt.add(auth_graph_model.RuleClause())
                        rule_clause.title = access_def.title 
                        rule_clause.entity_type = access_def_created.entity_type 
                        access_def_created.add_sub_criteria(rule_clause)
                else:
                    element_access_list = [(a, self.ACCESS_LEVEL_RW) for a in access_spec.get("write_access", [])]
                    element_access_list.extend([(a, self.ACCESS_LEVEL_R) for a in access_spec.get("read_access", [])])
                    for element_access, level in element_access_list:
                        existing_attribute = existing_attribute_dict.get(element_access)
                        if existing_attribute:
                            existing_attribute.access_level = level
                            existing_attribute_dict.pop(element_access)
                        else:
                            access_right = access_right_dict.get(element_access)
                            self._create_access_attribute(dbt, access_def, access_right, level)
                    for existing_attribute in existing_attribute_dict.values():
                        dbt.delete(existing_attribute)

        with database_api.ReadonlySession() as dbt:
                
            built_access_defs = dbt.select(auth_graph_model.AdminAccess, 
                                           database_api.in_(auth_graph_model.AdminAccess.title, self._built_in_access_def.keys()))
            
            self._built_in_ids = []
            for a in built_access_defs:
                if a.title == self.ACCESS_DEF_ADMINISTRATOR:
                    child_list = a.get_children()
                    assert len(child_list)==1
                    rule_def = rule_definitions.def_rule_types.get(self.entity_type)
                    rule_def["mandatory_result"] = child_list[0].id
                self._built_in_ids.append(a.id)
                
                
        
        

    def get_config_enabling(self, internal_element_type):
        return True, True
    
    def _get_config_spec(self, access_element):
        config_spec = ConfigTemplateSpec()
        config_spec.init("AdminAccess", "Edit Access Specification", "")
        
        if access_element and access_element.id in self._built_in_ids:
            built_in = True
            config_spec.set_read_only()
        else:
            built_in = False
        
        id_field = config_spec.add_field('element_id', 'element_id', ConfigTemplateSpec.VALUE_TYPE_INTEGER, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        config_spec.add_field("title", 'Title', ConfigTemplateSpec.VALUE_TYPE_STRING)
        if access_element:
            config_spec.add_field("built_in", 'Built-in', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, read_only=True, value=built_in)
        
        with database_api.QuerySession() as db_session:
            access_defs = db_session.select(auth_graph_model.AccessRight)
        for access_def in access_defs:
            name = access_def.name
            title = access_def.title
            field = config_spec.add_field(name, title, ConfigTemplateSpec.VALUE_TYPE_STRING)
            config_spec.add_selection_choice(field, "No Access", self.ACCESS_LEVEL_NONE)
            config_spec.add_selection_choice(field, "Read Access", self.ACCESS_LEVEL_R)
            
            if access_def.element_type in [self.ACCESS_TYPE_REPORT]:
                read_only = True
            else:
                read_only = not rule_definitions.is_crud_enabled(name)
            if not read_only:
                config_spec.add_selection_choice(field, "Read/Write Access", self.ACCESS_LEVEL_RW)
        
        if access_element:
            config_spec.set_values_from_object(access_element)
            config_spec.set_field_value(id_field, access_element.id)
            config_spec.set_value("built_in", built_in)
            for a in access_element.access_attributes:
                if a.access_level:
                    name = a.access_right.name
                    config_spec.set_value(name, a.access_level)
        
        return config_spec
            
    def _get_access_element_from_rule_id(self, element_id, db_session):
        rule_element = db_session.get(auth_graph_model.Rule, element_id)
        if not rule_element:
            raise Exception("Access element not found")
        parents = rule_element.get_parents()
        if not parents:
            raise Exception("Access element not found")
        assert len(parents)==1
        return parents[0]

    def config_get_template(self, internal_element_type, element_id, custom_template, db_session):
        if element_id:
            access_element = self._get_access_element_from_rule_id(element_id, db_session)
        else:
            access_element = None
        
        config_spec = self._get_config_spec(access_element)
        return config_spec.get_template()
    
    def _check_title(self, element_id, config_spec):
        
        title = config_spec.get_value("title")
        with database_api.ReadonlySession() as db_session:
            existing_elements = db_session.select(auth_graph_model.AdminAccess, 
                                                  auth_graph_model.AdminAccess.title == title)
        for existing_element in existing_elements:
            if existing_element.id != element_id:
                raise OperationNotAllowedException("An element with this name already exists")
        
        
        return title
    
    def _update_element(self, element_for_update, element_id, config_spec, dbt):
        title = self._check_title(element_id, config_spec)
        element_for_update.title = title
        
        existing_attributes = dict([(a.access_right.name, a) for a in element_for_update.access_attributes])
        access_rights = dbt.select(auth_graph_model.AccessRight)
        for access_right in access_rights:
            name = access_right.name
            try:
                existing_attribute = existing_attributes.pop(name)
            except KeyError:
                existing_attribute = None
                
            new_value = config_spec.get_value(name)
            if not new_value or new_value==self.ACCESS_LEVEL_NONE:
                if existing_attribute:
                    dbt.delete(existing_attribute)
            else:
                if existing_attribute:
                    existing_attribute.access_level = new_value
                else:
                    self._create_access_attribute(dbt, element_for_update, access_right, new_value)
        
        for attribute in existing_attributes.values():
            dbt.delete(attribute)
        
        return title
            
    # update element     
    def config_update_row(self, internal_element_type, element_id, template, dbt):
        
        config_spec = ConfigTemplateSpec(template)
        
        access_element = self._get_access_element_from_rule_id(element_id, dbt)
        if access_element.id in self._built_in_ids:
            title = config_spec.get_value("title")
            self.checkpoint_handler.Checkpoint("config_update_row", "auth_admin", lib.checkpoint.DEBUG, message="update built-in element ignored", title=title)
            return title
        
        
        title = self._update_element(access_element, access_element.id, config_spec, dbt)
        
        return title
            
            
        
        
    # create element
    def config_create_row(self, internal_element_type, template, dbt):
        config_spec = ConfigTemplateSpec(template)
        new_element = dbt.add(auth_graph_model.AdminAccess())
        self._update_element(new_element, None, config_spec, dbt)
        access_rule = self._create_rule_object(dbt, new_element)
        dbt.flush()
        return access_rule.id

            
        
    # delete element - return True if delete succeded, False otherwise
    def config_delete_row(self, internal_element_type, element_id, dbt):
        access_element = self._get_access_element_from_rule_id(element_id, dbt)
        if access_element.id in self._built_in_ids:
            raise OperationNotAllowedException("Built-in elements cannot be deleted")
        
        rule_api.delete_criteria(element_id, dbt)
        for a in access_element.access_attributes:
            dbt.delete(a)
        dbt.delete(access_element)
        return True

    def get_login_modules(self):
        if self._login_module_dict is None:
            self._login_module_dict = dict()
            all_plugins = self.plugin_socket_user.get_plugins()
            for plugin in all_plugins:
                login_module = plugin.get_login_module()
                if login_module:
                    self._login_module_dict[plugin.plugin_name] = login_module
        return self._login_module_dict
            

    def attempt_login(self, plugin_name, internal_user_login):
        with self.checkpoint_handler.CheckpointScope("attempt_login", "auth_admin", lib.checkpoint.DEBUG, plugin_name=plugin_name, internal_user_login=internal_user_login) as cps:
            login_module_dict = self.get_login_modules()
            plugin = login_module_dict.get(plugin_name)
            if plugin:
                plugin.clear_login_error()
                plugin.receive_login(self.login, self.password, internal_user_login)
                login_error_info = plugin.get_login_error()
                if login_error_info:
                    cps.add_complete_attr(login_error_info=repr(login_error_info))
                    (error_code, error_message) = login_error_info
                    raise LoginException(error_message, error_code)
                else:
                    self.authentication_plugin = plugin
                return plugin
            else: 
                raise LoginException("No login module available for plugin '%s'" % plugin_name)

    def get_user(self, dbs, login):
        with self.checkpoint_handler.CheckpointScope("get_user", "auth_admin", lib.checkpoint.DEBUG, login=login):
            login_lower = login.lower()
            return dbs.select_first(user_database.UserInfo, filter=database_api.and_(database_api.lower(user_database.UserInfo.user_login)==login_lower))

    def receive_login(self, login, password):
        with self.checkpoint_handler.CheckpointScope("receive_login", "auth_admin", lib.checkpoint.INFO, login=login):
            if not self.authentication_plugin and login:

                self.login = login
                self.password = password
                login_module_dict = self.get_login_modules() 
                
                with database_api.QuerySession() as dbs:
                    all_plugins = login_module_dict.values()
                    if '@' in login:
                        user = self.get_user(dbs, login)
                        if user:
                            users_found = [user]
                        else:
                            users_found = []
                    else:
                        # get normalized user login from each plugin and see if that matches
                        users_found = []    
                        for plugin in all_plugins:
                            normalised_login = plugin.get_normalised_login(login)
                            if normalised_login:
                                user = self.get_user(dbs, normalised_login)
                                if user:
                                    users_found.append(user)
                    if users_found:
                        if len(users_found)>1:
                            self.checkpoint_handler.Checkpoint("receive_login", "auth_admin", lib.checkpoint.ERROR, message="Found %s matches for user login '%s'" % (len(users_found), login))
                            raise LoginException("Found %s matches for user login" % len(users_found))
                        else:
                            user = users_found[0]
                            plugin = self.attempt_login(user.user_plugin, user.internal_user_login)
                            if not self.authentication_plugin:
                                # Check to see if user login have changed since last visit
                                internal_user_login = plugin.lookup_user(login)
                                if internal_user_login and internal_user_login != user.internal_user_login:
                                    self.attempt_login(user.user_plugin, internal_user_login)
                    else:
                        # Now get each plugin to try and find the user. If exactly one user is found we try to login
                        for plugin in all_plugins:
                            internal_user_login = plugin.lookup_user(login)
                            if internal_user_login:
                                users_found.append((plugin.plugin_name, internal_user_login))
                        if users_found:
                            if len(users_found)>1:
                                self.checkpoint_handler.Checkpoint("receive_login", "auth_admin", lib.checkpoint.ERROR, message="Found %s matches for user login '%s'" % (len(users_found), login))
                                for (plugin_name, internal_user_login) in users_found:
                                    self.checkpoint_handler.Checkpoint("UserSession::receive_login:user_match", "auth_admin", lib.checkpoint.ERROR, plugin_name=plugin_name, internal_user_login=internal_user_login)
                                raise LoginException("Found %s matches for user login" % len(users_found))
                            else:
                                (plugin_name, internal_user_login) = users_found[0]
                                self.attempt_login(plugin_name, internal_user_login)
                        else:
                            raise LoginException("The user name could not be found")
                            
                
                    assert self.authentication_plugin
                    
                    user_id = self.authentication_plugin.get_current_user_id()
                    self._calculate(user_id, self.authentication_plugin)
                    
                    self._user_admin.user_mgmt_login_succeded(self.authentication_plugin.plugin_name, user_id, login)
            


#            for a in access_right_elements:
#                print ''' 
#                          <activityPatternBinding
#                                activityId="java_gon_client_management.%s"
#                                pattern="gon_client_management/gon_client_management.UserView">
#                          </activityPatternBinding>''' % a.name
#            
#            for a in access_right_elements:
#                print "<variable"
#                print "name=\"%s\"" % a.name
#                print "priorityLevel=\"workbench\">"
#                print "</variable>"
#            for a in access_right_elements:
#                print '''<activity 
#                            id="java_gon_client_management.%s"
#                            name="%s">
#                            <enabledWhen>
#                            <with
#                            variable="%s">
#                            <equals
#                                 value="ENABLED">
#                                 </equals>
#                                 </with></enabledWhen>
#                        </activity>''' % (a.name, a.title, a.name)
