
class OperationNotAllowedException(Exception):
    pass

class DataErrorException(Exception):
    pass

def split_entity_type(entity_type):
    entity_type, dot, module_name = entity_type.partition(".")
    return (entity_type, module_name)

def _str(s):
    if isinstance(s, basestring):
        return s
    else:
        return str(s)
    


class ElementType(object):
    
    def get_id(self):
        return ""
    
    def get_entity_type(self):
        return "Element"
    
    def get_entity_module_name(self):
        return self.get_entity_type().partition(".")[2]
        
    def get_main_entity_type(self):
        return self.get_entity_type().partition(".")[0]
    
    def split_entity_type(self):
        return split_entity_type(self.get_entity_type())

    def get_element_type(self):
        return "module"
    
    def get_info(self):
        return ""
    
    def get_label(self):
        return ""
    
    def __repr__(self):
        try:
            return "'%s' (%s)" % (self.get_label().encode("ascii", "replace"), self.get_entity_type())
        except:
            return "Error creating string"

    def same_type(self, element_type):
        if (self.get_entity_type() == element_type.get_entity_type()):
            return True
        entity_type = self.get_main_entity_type()
        if element_type.get_entity_type().startswith("%s." % entity_type):
            return True
        return False
    
    def equals(self, element_type):
        if (_str(self.get_id()) == _str(element_type.get_id()) and 
           self.get_entity_type() == element_type.get_entity_type()):
            return True
        return False
    
    def get_template(self):
        return None
        
class EmptyElement(ElementType):

    def __init__(self, entity_type):
        self.entity_type = entity_type

    def get_id(self):
        return ""

    def get_entity_type(self):
        return self.entity_type
    
    def get_info(self):
        return ""
    
    def get_label(self):
        return ""


class RuleType(object):
    
    def get_id(self):
        raise NotImplementedError

    def get_result_element(self):
        raise NotImplementedError
        
    def get_condition_elements(self):
        raise NotImplementedError
    
    def is_active(self):
        raise NotImplementedError

    def __repr__(self):
        try:
            return "Rule %s : %s -> %s" % (self.get_id(), self.get_condition_elements(), self.get_result_element())
        except:
            return "Error creating string"
    
    def get_conditions_in_order(self, rule_element_types):
        tmp_list = list(self.get_condition_elements())
        conditions = []
        for name in rule_element_types:
            found = None
            for element in tmp_list:
                entity_type = element.get_main_entity_type()
                if entity_type == name:
                    found = element
                    break
            if found:
                tmp_list.remove(found)
                conditions.append(found)
            else:
                conditions.append(EmptyElement(name))
        
        if tmp_list:
            raise DataErrorException("Rule %s is invalid : Contains too many conditions" % self.get_id())
             
        return conditions



class RuleSpecInterface(object):
    
    def get_title(self):        
        raise NotImplementedError
        
    def get_title_long(self):        
        raise NotImplementedError

    def get_unique_element_types(self):        
        raise NotImplementedError
    
    def get_mandatory_element_types(self):
        raise NotImplementedError
        
    def get_template_rule(self):
        raise NotImplementedError

    def get_header_rule(self):
        raise NotImplementedError
