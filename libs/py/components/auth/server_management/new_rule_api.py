from __future__ import with_statement
import auth_graph_model as model
import components.auth.server_common.database_schema as database_schema
import components.database.server_common.database_api as database_api
import components.auth
from components.auth.server_management import ElementType, RuleType, OperationNotAllowedException, RuleSpecInterface, DataErrorException, split_entity_type
from components.access_log.server_common import database_schema as access_log_db_schema
from components.auth.server_common.auth_analysis import AuthorizationAnalysis
import lib.checkpoint

import ConfigParser
import datetime

import components.management_message.server_management.session_recieve
import os.path
import sys

from components.auth.server_common.rule_definitions import *



rule_module_id = "new_rule_api"

def log(checkpoint_handler, name, **kwargs):
    checkpoint_handler.Checkpoint(name, rule_module_id, lib.checkpoint.DEBUG, **kwargs)
    
def log_scope(checkpoint_handler, name, **kwargs):
    return checkpoint_handler.CheckpointScope(name, rule_module_id, lib.checkpoint.DEBUG, **kwargs)    

condition_dict = dict()
rule_condition_dict = dict()
rule_restult_dict = dict()
all_rules = dict()
checkpoint_handler = None

class Condition(object):
    
    def __init__(self, condition_id, entity_type, rule_entity_type):
        self.condition_id = condition_id
        self.entity_type = entity_type
        self.rule_entity_type = rule_entity_type
        
    def __repr__(self):
        return "%s.%s" % (self.entity_type, self.condition_id)

class Rule(object):
    
    def __init__(self, rule_id, result, entity_type, rule_type):
        self.result = "%s" % result
        self.rule_id = rule_id
        self.conditions = []
        self.entity_type = entity_type
        self.rule_type = rule_type
        self.active = True
        
def add_rule_condition(condition, rule, rule_entity_type, rule_condition_dict):
    if condition.internal_type_name == "Condition":
        element_def = def_element_types.get(condition.entity_type)
#        if element_def["type_"] == "module":
#            module_name = element_def.get("plugin") 
#            condition_id = "%s.%s.%s" % (condition.entity_type, module_name, condition.value_id)
#        else:
#            condition_id = "%s.%s" % (condition.entity_type, condition.value_id)
        condition_id = "%s.%s" % (condition.entity_type, condition.value_id)
        if element_def["type_"] == "module":
            module_name = element_def.get("plugin")
            condition_element = Condition("%s.%s" % (module_name, condition.value_id), condition.entity_type, rule_entity_type)
        else:
            condition_element = Condition(condition.value_id, condition.entity_type, rule_entity_type)
    else:
        condition_id = "%s" % condition.id
        condition_element = Condition(condition.id, condition.entity_type, rule_entity_type)
#    print "Add condition id : %s" % condition_id
    condition_rules = rule_condition_dict.get(condition_id, [])
    condition_rules.append(rule)
    rule_condition_dict[condition_id] = condition_rules
    rule.conditions.append(condition_element)
    return condition_id

def rule_deleted(rule_id):
    global all_rules
    all_rules = dict()

def rule_updated(rule_id):
    global all_rules
    all_rules = dict()

def rule_created(rule_id):
    global all_rules
    all_rules = dict()

def read_all_rules():
    global checkpoint_handler
    with log_scope(checkpoint_handler, "read_all_rules"):
        global all_rules
        if all_rules:
            return
        global condition_dict
        global rule_condition_dict
        global rule_restult_dict
        condition_dict = dict()
        rule_condition_dict = dict()
        rule_restult_dict = dict()
        with database_api.QuerySession() as db_session:
            all_criteria = db_session.select(model.RuleObject)
            all_assoc = db_session.select(model.RuleAssociation)
            all_criteria_dict = dict()
            all_criteria_dict.update([(c.id,c) for c in all_criteria])
            all_assoc_dict = dict()
            all_assoc_dict.update([(c.id,c) for c in all_assoc])
            for a in all_assoc:
                if a.parent_internal_type_name == "Group":
                    rule_id = a.id
                    result = all_criteria_dict.get(a.parent_id)
                    rule_type = result.internal_type_name
                    rule = Rule(rule_id, result.id, result.entity_type, rule_type)
                    rule.active = not a.deactivated
                    all_rules[rule_id] = rule
                    condition = all_criteria_dict.get(a.child_id)
                    add_rule_condition(condition, rule, a.rule_entity_type, rule_condition_dict)
                elif a.parent_internal_type_name in ["RuleClause", "AssociationClause"]:
                    rule_id = a.parent_id
                    rule = all_rules.get(rule_id)
                    if not rule:
                        rule_type = a.parent_internal_type_name.replace("Clause", "")
                        rule = Rule(rule_id, None, None, rule_type)
                        all_rules[rule_id] = rule
                    condition = all_criteria_dict.get(a.child_id)
                    condition_id = add_rule_condition(condition, rule, a.rule_entity_type, rule_condition_dict)
                elif a.parent_internal_type_name in ["Rule", "Association"]:
                    rule_id = a.child_id
                    rule = all_rules.get(rule_id)
                    result = all_criteria_dict.get(a.parent_id)
                    if not rule:
                        rule_type = a.parent_internal_type_name
                        rule = Rule(rule_id, result.id, result.entity_type, rule_type)
                        all_rules[rule_id] = rule
                    else:
                        rule.result = result.id
                        rule.entity_type = result.entity_type
                    rule.active = not a.deactivated
                
                
                
        


def get_rules_for_element(element_id, entity_type, known_rules):
    new_rules = []
    deleted_rules = []
    element_def = def_element_types.get(entity_type)
    rule_element_type = element_def.get("type_")
    read_all_rules()
#    with database_api.QuerySession() as dbs:
        
#        if rule_element_type in ["module", "component"]:
#            conditions = dbs.select(model.Condition, filter=database_api.and_(model.Condition.value_id==element_id,
#                                                                              model.Condition.entity_type==entity_type,
#                                                                              model.Condition.internal_type_name=="Condition"))
#        else:
#            conditions = [dbs.get(model.RuleObject, element_id)]
#        print [a.id for a in conditions]

#        if rule_element_type in ["module", "component"]:
#            id_select = dbs.select_expression(model.Condition.id, filter=database_api.and_(model.Condition.value_id==element_id,
#                                                                                           model.Condition.entity_type==entity_type,
#                                                                                           model.Condition.internal_type_name=="Condition"))
#            rule_associations = dbs.select(model.RuleAssociation, model.RuleAssociation.child_id.in_(id_select))
#        else:
#            rule_associations = dbs.select(model.RuleAssociation, model.RuleAssociation.child_id==element_id)
#        
#        print  [a.id for a in rule_associations]
#        rule_ids = [a.parent_id for a in rule_associations]
#        rule_clauses = dbs.select_expression(model.RuleObject, model.RuleObject.id.in_(rule_ids))
    global all_rules
    global condition_dict
    global rule_condition_dict
    global rule_restult_dict
    
    if rule_element_type == "component":
        condition_id = "%s.%s" % (entity_type, element_id)
    elif rule_element_type == "module":
        plugin_name, dot, plugin_element_id = element_id.partition(".")
        condition_id = "%s.%s" % (entity_type, plugin_element_id)
#        condition_id = "%s.%s" % (entity_type, element_id)
    else:
        condition_id = "%s" % element_id
        
    rules = rule_condition_dict.get(condition_id, [])
    print "%s : %d" % (condition_id, len(rules))
    
    for rule in rules:
        str = "%s <= " % rule.result
        for c in rule.conditions:
            str += "%s " % c
        print str
        
    new_rules = rules
        
    while known_rules:
        known_rule = known_rules.pop()
        rule = all_rules.get(known_rule.rule_id)
        same_rule = True
        if rule:
            if rule.result == known_rule.result:
                if len(rule.conditions) ==  len(known_rule.conditions):
                    for condition in rule.conditions:
                        if not condition in known_rule.conditions:
                            same_rule = False
                            break
                    if same_rule:
                        continue
            deleted_rules.append(known_rule)
            new_rules.append(rule)
        else:
            deleted_rules.append(known_rule)
        
    return new_rules, deleted_rules


