from __future__ import with_statement
import components.auth.server_common.auth_graph_model as model
import components.database.server_common.database_api as database
import random
count = 0

class RuleNode(object):
    
    def __init__(self, criteria):
        self.children = []
        self.parents = []
        self.criteria = criteria
        self.id = criteria.id
        if isinstance(criteria, model.RuleCriteria):
            self.title = criteria.title
        elif isinstance(criteria, model.Condition):
            self.title = criteria.value_title
        else:
            self.title = "Unknown type"
        self.mark = None
    
    def set_true_bottom_up(self, seen):
#        print self.title
        for c in self.children:
            c.id in seen
        for p in self.parents:
            p.set_true_bottom_up(seen)
        
def dfs(node):
    global count
    count += 1
    node.mark = True
    for c in node.children:
        if not c.mark:
            dfs(c)

def load_graph(all=False, simulate=False):
    with database.ReadonlySession() as db_session:

        nodes = dict()
        actions = []
        with checkpoint_handler_demo.CheckpointScope("select_all_RuleCriteria", "select_all_RuleCriteria", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.RuleCriteria)
            for c in all_group_criteria:
                rule_node = RuleNode(c)
                nodes[c.id] = rule_node
                if c.internal_type_name == "Action":
                    actions.append(rule_node)
                
        

        with checkpoint_handler_demo.CheckpointScope("select_all_RuleAssociations", "select_all_RuleAssociations", lib.checkpoint.INFO):
            all_assoc = db_session.select(model.RuleAssociation)

        with checkpoint_handler_demo.CheckpointScope("connect_graph", "connect_graph", lib.checkpoint.INFO):
            for a in all_assoc:
                parent = nodes.get(a.parent_id)
                child = nodes.get(a.child_id)
                parent.children.append(child)
                child.parents.append(parent)
                
        if all:
            with checkpoint_handler_demo.CheckpointScope("select_all_Conditions", "select_all_Conditions", lib.checkpoint.INFO):
                conditions = db_session.select(model.Condition)
                for c in conditions:
                    rule_node = RuleNode(c)
                    nodes["%s" % c.id] = rule_node
            
            with checkpoint_handler_demo.CheckpointScope("select_all_ConditionAssociations", "select_all_ConditionAssociations", lib.checkpoint.INFO):
                condition_assoc = db_session.select(model.ConditionAssociation)

            with checkpoint_handler_demo.CheckpointScope("connect_graph", "connect_graph", lib.checkpoint.INFO):
                for a in condition_assoc:
                    parent = nodes.get(a.parent_id)
                    child = nodes.get("%s" % a.child_id)
                    parent.children.append(child)
                    child.parents.append(parent)
                
        if not simulate:
            with checkpoint_handler_demo.CheckpointScope("dfs", "dfs", lib.checkpoint.INFO):
                global count
                count = 0
                for a in actions:
                    dfs(a)
                print "Count %d" % count
        else:
            max = db_session.select_count(model.Condition)
            n = 3
            cids = []
            while len(cids)<n:
                id = random.randint(0,max-1)
                if not id in cids:
                    cids.append(id)
        
            if simulate==1:
                with checkpoint_handler_demo.CheckpointScope("simulate_steps", "simulatesteps", lib.checkpoint.INFO):
                    seen = set()
                    for id in cids:
                        with checkpoint_handler_demo.CheckpointScope("simulate_step", "simulatestep", lib.checkpoint.INFO):
                            condition = db_session.get(model.Condition, id)
                            condition.set_true_bottom_up(db_session, seen)
            else:
                with checkpoint_handler_demo.CheckpointScope("simulate_steps_all", "simulatesteps_all", lib.checkpoint.INFO):
                    for id in cids:
                        condition = db_session.get(model.Condition, id)
                    
                        parents = condition.get_parents(db_session)
                        for parent in parents:
                            parent_obj = nodes.get(parent.id)
                            parent_obj.set_true_bottom_up(set())
                    
        
            
        

def test_rules():
    
    with database.ReadonlySession() as db_session:
        with checkpoint_handler_demo.CheckpointScope("AggregatingCriteriaAggregatingCriteria", "AggregatingCriteria", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.AggregatingCriteria)
            print len(all_group_criteria)

    with database.ReadonlySession() as db_session:
        with checkpoint_handler_demo.CheckpointScope("ModuleCriteria", "ModuleCriteria", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.ModuleCriteria)
            print len(all_group_criteria)
        
    with database.ReadonlySession() as db_session:
        with checkpoint_handler_demo.CheckpointScope("Criteria", "Criteria", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.Criteria)
            print len(all_group_criteria)

    with database.ReadonlySession() as db_session:
        with checkpoint_handler_demo.CheckpointScope("CriteriaAssociation", "CriteriaAssociation", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.CriteriaAssociation)
            print len(all_group_criteria)
        
#    with checkpoint_handler_demo.CheckpointScope("load_graph", "load_graph", lib.checkpoint.INFO):
#        load_graph()
#
#    with checkpoint_handler_demo.CheckpointScope("load_graph_all", "load_graph_all", lib.checkpoint.INFO):
#        load_graph(all=True)
#
#    with checkpoint_handler_demo.CheckpointScope("load_graph_simulate_all", "load_graph_simulate_all", lib.checkpoint.INFO):
#        load_graph(simulate=2)
#    
#    with checkpoint_handler_demo.CheckpointScope("load_graph_simulate", "load_graph_simulate", lib.checkpoint.INFO):
#        load_graph(simulate=1)
#    


if __name__ == '__main__':
    
    import components.environment
    import lib.checkpoint


    checkpoint_handler_demo = lib.checkpoint.CheckpointHandler.get_cout_all()
    component_env = components.environment.Environment(checkpoint_handler_demo, "sqlite:///../../../../../setup/dev_env/gon_installation_active/gon_server_management_service/win/gon_server_db.sqlite",
                                                       db_logging=False)
#    component_env.set_log_database_output(True)
    #component_env = components.environment.Environment(checkpoint_handler_demo, "mssql://@localhost\SQLEXPRESS/gon52")
    model.schema.ServerSchema.connect(component_env.get_default_database())
    model.checkpoint_handler_demo = checkpoint_handler_demo

    with checkpoint_handler_demo.CheckpointScope("yo", "no", lib.checkpoint.INFO):
        test_rules()
