from __future__ import with_statement
import components.auth.server_common.auth_graph_model as model
import components.database.server_common.database_api as database

written_criteria = []

def write_criteria(db_session, file, c):
    title = c.title if c.title else ""
    active = "deactivated" if c.deactivated else "active"
    title = title + ("(%s) :" % c.id) + c.description() + " (%s)" % active
    try:
        file.write("<li>%s</li>" % title.decode("UTF-8",'ignore'))
    except:
        file.write("<li>UnicodeError</li>")
    file.write("<ul>")
    if c.id in written_criteria:
        file.write("...")
    else:
        for child in c.get_sub_criteria(db_session):
            with checkpoint_handler_demo.CheckpointScope("write_criteria", "write_criteria", lib.checkpoint.INFO):
                write_criteria(db_session, file, child)
    file.write("</ul>")
    written_criteria.append(c.id)

def write_condition(db_session, file, c):
    title = ("(%s) :" % c.value_title) + c.value_entity_type + "(%s)" % c.value_id
    try:
        file.write("<li>%s</li>" % title.decode("UTF-8",'ignore'))
    except:
        file.write("<li>UnicodeError</li>")
        
        

def generate_html_tree_view(checkpoint_handler, filename):
    
    file = open(filename, "w")
    file.write('''
                <HTML>
                <HEAD>
                <TITLE>Tree view of rules</TITLE>
                <SCRIPT LANGUAGE="JavaScript" SRC="mktree.js"></SCRIPT>
                <LINK REL="stylesheet" HREF="mktree.css">
                </HEAD>
                <BODY BGCOLOR=#FFFFFF LINK="#00615F" VLINK="#00615F" ALINK="#00615F">
                
                <A href="#" onClick="expandTree('tree1'); return false;">Expand All</A>&nbsp;&nbsp;&nbsp;
                <A href="#" onClick="collapseTree('tree1'); return false;">Collapse All</A>&nbsp;&nbsp;&nbsp;
                
                <ul class="mktree" id="tree1">
                '''
              )
    
    with database.ReadonlySession() as db_session:
        with checkpoint_handler_demo.CheckpointScope("select_all_RuleCriteria", "select_all_RuleCriteria", lib.checkpoint.INFO):
            all_group_criteria = db_session.select(model.RuleCriteria)
        base_criteria = []
        for c in all_group_criteria:
            with checkpoint_handler_demo.CheckpointScope("find actions", "find actions", lib.checkpoint.INFO):
                parent_criterias = db_session.select(model.RuleAssociation, database.and_(model.RuleAssociation.child_id==c.id))
            if len(parent_criterias)==0:
                base_criteria.append(c)
                
        for c in base_criteria:
            write_criteria(db_session, file, c)
        
    file.write(''' 
                </ul>
                </BODY>
                </HTML>
               '''
               )
    file.close()
        

if __name__ == '__main__':
    
    import components.environment
    import lib.checkpoint


    checkpoint_handler_demo = lib.checkpoint.CheckpointHandler.get_cout_all()
    db_connect_url = database.get_connect_url_from_string("sqlite:///../../../../../setup/dev_env/gon_installation/config/gon_server_db.sqlite")
    component_env = components.environment.Environment(checkpoint_handler_demo, db_connect_url)
    #component_env = components.environment.Environment(checkpoint_handler_demo, "mssql://@localhost\SQLEXPRESS/gon52")
    model.schema.dbapi.bind(component_env.get_default_database())

    with checkpoint_handler_demo.CheckpointScope("yo", "no", lib.checkpoint.INFO):
        generate_html_tree_view(checkpoint_handler_demo, "rules.html")