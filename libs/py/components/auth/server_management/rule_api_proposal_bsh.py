_rule_types = {
                 u"PersonalTokenAssignment" : 
                    dict( result_class = u"PersonalTokenStatus", 
                          conditions = [u"Token", u"User"], 
                          unique_conditions = [u"Token"], 
                          mandatory_conditions = [[u"Token", u"User"]], 
                          title = u"Personal Tokens", 
                          long_title = u"Personal Token Assignment" ),
                 u"TokenGroupMembership" : 
                    dict( result_class = u"TokenGroup", 
                          conditions = [u"Token"], 
                          mandatory_conditions = [[u"Token"]], 
                          title = u"Token Groups", 
                          long_title = u"Token Group Membership" ),
                 u"AuthenticationPolicy" : 
                    dict( result_class = u"AuthenticationStatus", 
                          conditions = [u"GOnUserGroup", u"UserGroup", u"TokenGroup", u"PersonalTokenStatus"], 
                          title = u"Authentication", 
                          long_title = u"Authentication Policy"  ),
                 u"AuthorizationPolicy" : 
                    dict( result_class = u"ProgramAccess", 
                          conditions = [u"AuthenticationStatus", u"GOnUserGroup", u"UserGroup"], 
                          title = u"Authorization", 
                          long_title = u"Authorization Policy"  ),
                 u"GOnUserGroupMembership" : 
                    dict( result_class = u"GOnUserGroup", 
                          conditions = [u"User", u"Group"], 
                          title = u"G/On User Groups", 
                          long_title = u"G/On User Group Membership"  ),
                 
                 u"TestRule" : dict( result_class=u"TestResult", conditions = [u"TestElement1", u"TestElement2", u"TestElement3"] ),
                 u"TestRule1" : dict( result_class=u"TestAction", conditions = [u"TestElement1", u"TestResult"] ),
              }


_element_types = { 
                    u"User" : 
                        dict( title = u"User", 
                              title_plural = u"Users", 
                              rule_title = u"User",
                              type_ = u"user", 
                              plugin_element_type = u"user" ),
                    u"UserGroup" : 
                        dict( title = u"User Group",
                              title_plural = u"User Groups",
                              rule_title = u"In User Group",
                              type_ = u"user",
                              plugin_element_type = u"group"), 
                    u"Token" : 
                        dict( title = u"Token", 
                              title_plural = u"Tokens", 
                              rule_title = u"Has Token",
                              type_ = u"module", 
                              plugin_providers = [u"soft_token", u"micro_smart", u"smart_card", u"hagiwara"], 
                              predicate = u"has_id", 
                              parameter_name = u"key"),
                    u"PersonalTokenStatus" : 
                        dict( title = u"Personal Token Status", 
                              title_plural = u"Personal Token Status", 
                              rule_title = u"Personal Token Status",
                              type_ = u"class", 
                              name = u"personal_token_status"),
                    u"AuthenticationStatus" : 
                        dict( title = u"Authentication Status", 
                              title_plural = u"Authentication Status", 
                              rule_title = u"Authentication Status",
                              type_ = u"class", 
                              name = u"authentication_status"),
                    u"GOnUserGroup" : 
                        dict( title = u"G/On User Group", 
                              title_plural = u"G/On User Groups", 
                              rule_title = u"In G/On User Group", 
                              type_ = u"class", 
                              name = u"gon_user_group"),
                    u"ProgramAccess" : 
                        dict( title = u"Menu Action", 
                              title_plural = u"Menu Actions", 
                              rule_title = u"Authorized Menu Action",
                              type_ = u"action", 
                              name = u"program_access"),

                    
                    u"TestResult" : dict( title=u"Test", type_=u"class", name=u"test_result", info=u"TestInfo"),
                    u"TestAction" : dict( title=u"Test1", type_=u"action", name=u"test_action", info=u"TestInfo1"),
                    u"TestElement1" : dict( title=u"TestElement1", type_=u"module", plugin_providers =[u"test_plugin1"], predicate=u"test", parameter_name = u"test"),
                    u"TestElement2" : dict( title=u"TestElement2", type_=u"module", plugin_providers=[u"test_plugin2"],  predicate=u"test", parameter_name = u"test"),
                    u"TestElement3" : dict( title=u"TestElement3", type_=u"module", plugin_providers=[u"test_plugin3", u"test_plugin4"],  predicate=u"test3", parameter_name = u"test3"),
                    #u"TstElement4" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement1", u"TestElement2", u"TestElement3"]),
                    #u"TestElement3" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement4", u"TestElement5"]),
                    #u"TestElement4" : dict( title=u"TestElement4", type_=u"module", name=u"test_plugin4",  predicate=u"test4", parameter_name = u"test4"),
                    #u"TestElement5" : dict( title=u"TestElement5", type_=u"module", name=u"test_plugin5",  predicate=u"test5", parameter_name = u"test5"),
                 }

_built_in_elements = [
                       dict( title=u"Has Personal Token", 
                             element_type = u"PersonalTokenStatus", 
                             mandatory_conditions = [[u"Token", u"User"]], 
                             default_in_rules=[u"PersonalTokenStatus"]), 
                             
                       dict( title=u"Authenticated", 
                             element_type = u"AuthenticationStatus", 
                             default_in_rules=[u"AuthenticationPolicy"]) 
                     ]

