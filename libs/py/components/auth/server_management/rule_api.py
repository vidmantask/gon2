from __future__ import with_statement
import auth_graph_model as model
import components.auth.server_common.database_schema as schema
import components.database.server_common.database_api as database
import components.database.server_common.schema_api as schema_api
import components.auth
from components.auth.server_management import ElementType, RuleType, OperationNotAllowedException, RuleSpecInterface, DataErrorException, split_entity_type
from components.access_log.server_common import database_schema as access_log_db_schema
from components.auth.server_common.auth_analysis import AuthorizationAnalysis
from components.plugin.server_common.config_api_lib import ConfigTemplateSpec
import lib.checkpoint

import ConfigParser
import datetime
from components.database.server_common.database_api import QuerySession,\
    ReadonlySession

import components.management_message.server_management.session_recieve
import components.traffic.server_common.database_schema as traffic_schema

import new_rule_api
from components.auth.server_common import database_schema
from components.admin_ws.activity_log import ActivityLogCreator, ActivityLog


rule_module_id = "rule_api"

RESTRICTED_ACTION = "restricted_action"

import os.path
import elementtree.ElementTree as elementtree
import sys

from components.auth.server_common.rule_definitions import *

def setup(base_path, checkpoint_handler):

    if base_path and False:
        def get_content_list(xml_element, tag_name):
            return [element.text for element in xml_element.findall(tag_name)]

        # disabled in version 5.1.x
        try:
            file_path = os.path.join(base_path,"rule_customization","rule_specifications.xml")
            if os.path.exists(file_path):
                try:
                    rule_spec_file = file(file_path, "r")
                    rule_spec_xml = elementtree.ElementTree(None,rule_spec_file)
                    rule_spec_file.close()
                    rule_spec_sections = rule_spec_xml.getiterator('RuleSpecification')
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    checkpoint_handler.Checkpoint("setup", "rule_api", lib.checkpoint.WARNING, warning="Error parsing rule_spec file '%s' : %s" % (file_path, evalue))
                    rule_spec_sections = []


                for rule_spec in rule_spec_sections:
                    try:
                        name = rule_spec.get("name")
                        if not name:
                            raise Exception("Missing name for rule_spec")
                        rule_spec_dict = def_rule_types.get(name)
                        if not rule_spec_dict:
                            rule_spec_dict = dict()
                            def_rule_types[name] = rule_spec_dict

                        rule_spec_dict["title"] = rule_spec.findtext("title")
                        rule_spec_dict["long_title"] = rule_spec.findtext("long_title")
                        rule_spec_dict["result_class"] = rule_spec.findtext("result_class")

                        rule_spec_dict["conditions"] = get_content_list(rule_spec, "condition")
                        rule_spec_dict["unique_conditions"] = get_content_list(rule_spec, "unique_condition")

                        mandatory_specs = rule_spec.findall("mandatory_condition")
                        rule_spec_dict["mandatory_conditions"] = [get_content_list(mandatory_spec, "condition") for mandatory_spec in mandatory_specs]
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("setup", "rule_api", lib.checkpoint.WARNING, etype, evalue, etrace)


            file_path = os.path.join(base_path,"rule_customization","rule_element_specifications.xml")
            if os.path.exists(file_path):
                try:
                    element_spec_file = file(file_path, "r")
                    element_spec_xml = elementtree.ElementTree(None,element_spec_file)
                    element_spec_file.close()
                    elements = element_spec_xml.findall("class")
                    elements.extend(element_spec_xml.findall("module"))
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    checkpoint_handler.Checkpoint("setup", "rule_api", lib.checkpoint.WARNING, warning="Error parsing rule_element_spec file '%s' : %s" % (file_path, evalue))
                    elements = []

                for element_spec in elements:
                    name = "Unknown"
                    try:
                        name = element_spec.get("name")
                        if not name:
                            raise Exception("Missing name for rule_element_spec")
                        element_spec_dict = def_element_types.get(name)
                        if not element_spec_dict:
                            element_spec_dict = dict()
                            def_element_types[name] = element_spec_dict

                        if element_spec.tag=="class":
                            if element_spec.get("action"):
                                element_spec_dict["type_"] = "action"
                            else:
                                element_spec_dict["type_"] = "class"
                            element_spec_dict["internal_name"] = element_spec.findtext("internal_name")
                        elif element_spec.tag=="module":
                            element_spec_dict["type_"] = "module"
                            element_spec_dict["internal_element_type"] = element_spec.findtext("internal_element_type")
                            element_spec_dict["plugin_providers"] = get_content_list(element_spec, "plugin_provider")
                        elif element_spec.tag=="user":
                            element_spec_dict["type_"] = "user"
                            element_spec_dict["internal_element_type"] = element_spec.findtext("internal_element_type")
                        else:
                            raise Exception("Unknown element type '%s'" % element_spec.tag)

                        element_spec_dict["title"] = element_spec.findtext("title")
                        element_spec_dict["title_plural"] = element_spec.findtext("title_plural")
                        element_spec_dict["rule_title"] = element_spec.findtext("rule_title")

                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        checkpoint_handler.CheckpointException("setup", "rule_api", lib.checkpoint.WARNING, etype, evalue, etrace)

        except:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("setup", "rule_api", lib.checkpoint.WARNING, etype, evalue, etrace)

    _init_element_types()
    _built_in_elements.load()

def element2string(element):
    try:
        return "'%s','%s' (%s)" % (element.get_label(), element.get_id(), get_entity_type_title(element.get_entity_type()))
    except Exception, e:
        return "Error getting element values: %s" % repr(e)


def rule2logstring(rule):
    try:
        rule.get_condition_elements()
        conditions = " + ".join([element2string(e) for e in rule.get_condition_elements() if e.get_id()])
        arrow = "-->" if rule.is_active() else "-/->"
        return "%s %s %s" % (conditions, arrow, element2string(rule.get_result_element()))
    except Exception, e:
        return "Error getting rule details: %s" % repr(e)




def parse_list(str):
    if str:
        return [s.strip() for s in str.split(",")]
    return []


class RuleCopy(RuleType):

    def __init__(self, rule):
        self.result = rule.get_result_element()
        self.id = rule.get_id()
        self.conditions = [r for r in rule.get_condition_elements()]
        self.active = rule.is_active()

    def get_id(self):
        return self.id

    def get_result_element(self):
        return self.result

    def get_condition_elements(self):
        return self.conditions

    def set_result_element(self, result_element):
        self.result = result_element

    def is_active(self):
        return self.active

class Rule1(RuleType):

    def __init__(self, rule_id, result):
        self.rule_id = rule_id
        self.result = result
        self.conditions = []
        self.active = True

    def __repr__(self):
        return repr(self.result) + ":" + repr(self.conditions)


    def get_id(self):
        return self.rule_id

    def set_condition_order(self, rule_element_types):
        self.conditions = self.get_conditions_in_order(rule_element_types)

    def add_condition(self, row):
        if row.internal_type_name=="Condition":
            entity_type = "%s.%s" % (row.rule_entity_type, row.entity_type)
    #        print "_create_condition_element_from_criteria.entity_type", entity_type
            element =  BasicConditionElement(entity_type, row)
        else:
            element = CriteriaElement1(row)
        self.conditions.append(element)


    def get_result_element(self):
        return self.result

    def get_condition_elements(self):
        return self.conditions

    def is_active(self):
        return self.active


class Rule(RuleType):

    def __init__(self, result_criteria=None, criteria=None):
        if result_criteria:
            self.result = CriteriaElement(result_criteria)
        else:
            self.result = None
        self.criteria = criteria
        self.conditions = []
        self.active = True

    def __repr__(self):
        return repr(self.result) + ":" + repr(self.conditions)


    def get_id(self):
        if self.criteria:
            return self.criteria.id
        else:
            return -1

    def set_condition_order(self, rule_element_types):
        self.conditions = self.get_conditions_in_order(rule_element_types)

    def get_result_element(self):
        return self.result

    def get_condition_elements(self):
        return self.conditions

    def is_active(self):
        return self.active

class RuleSpec(RuleSpecInterface):

    def __init__(self, class_name):
        if not def_rule_types.has_key(class_name):
            raise Exception("Unknown rule type '%s'" % class_name)
        self.rule_type = def_rule_types[class_name]
        self.template_rule = None
        self.header_rule = None
        self.entity_type_map = None


    def get_title(self):
        return self.rule_type["title"]

    def get_title_long(self):
        return self.rule_type["long_title"]

    def get_unique_element_types(self):
        return self.rule_type.get("unique_conditions", [])

    def get_mandatory_element_types(self):
        return self.rule_type.get("mandatory_conditions", [])

    def get_template_rule(self):
        return self.template_rule

    def get_header_rule(self):
        return self.header_rule

    def get_type_map(self):
        return self.entity_type_map

class RuleException(Exception):
    pass





class TemplateElement(ElementType):

    def __init__(self, name, rule_element_type, nameless=False, rule_info=False):
        if not nameless:
            self.title = rule_element_type["title"]
            if not rule_info:
                self.info = rule_element_type.get("title_plural","")
            else:
                self.info = rule_element_type.get("rule_title","")
        else:
            self.title = self.info = ""
        self.element_type = rule_element_type["type_"]
        self.name = name
        self.id = ""

    def get_id(self):
        return self.id

    def get_entity_type(self):
        #return "%s.%s" % (self.name, self.name)
        return self.name

    def get_info(self):
        return self.info

    def get_label(self):
        return self.title

    def get_element_type(self):
        return self.element_type



class CriteriaElement1(ElementType):

    def __init__(self, row):
        self.entity_type = "%s.%s" % (row.rule_entity_type, row.entity_type)
        self.element_id = row.child_id
        self.title = row.title


    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return self.entity_type

    def get_info(self):
        return ""

    def get_label(self):
        return self.title


class CriteriaElement(ElementType):

    def __init__(self, criteria, main_entity_type=None):
        self.criteria = criteria
        self.element_id = criteria.id
        self.title = criteria.title
        self.description = criteria.description
        if main_entity_type:
            self.entity_type = "%s.%s" % (main_entity_type, criteria.entity_type)
        else:
            self.entity_type = criteria.entity_type
        self.element_type = ""

    def get_id(self):
        return self.element_id

    def get_entity_type(self):
        return self.entity_type

    def get_info(self):
        return self.description

    def get_label(self):
        return self.title

    def get_element_type(self):
        return self.element_type


class BuiltInCriteriaElement(CriteriaElement):

    def __init__(self, criteria, meta_info):
        CriteriaElement.__init__(self, criteria)
        self.meta_info = meta_info

    def get_meta_info(self):
        return self.meta_info

class BasicConditionElement(ElementType):

    def __init__(self, rule_entity_type, criteria):
        element_def = def_element_types.get(criteria.entity_type)
        if element_def["type_"] == "module":
            module_name = element_def.get("plugin")
            self.id = "%s.%s" % (module_name, criteria.value_id)
        else:
            self.id = criteria.value_id
        self.entity_type = rule_entity_type
        self.label = criteria.title
        self.criteria = criteria
#        print "BasicConditionElement.id", self.id


    def get_id(self):
        return self.id

    def get_entity_type(self):
        return self.entity_type

    def get_info(self):
        return ""

    def get_label(self):
        return self.label



class BuiltInElements(object):

    _built_in_elements = { u"personal_token" : dict( title=u"Personal Token", element_type = u"PersonalTokenAssignment", default_in_rules=[u"PersonalTokenAssignment"]),
                           u"authenticated" : dict( title=u"Authenticated", element_type = u"AuthenticationStatus", default_in_rules=[u"AuthenticationStatus", u"ProgramAccess"]),
#                           dict( title=u"Personal Device", element_type = u"PersonalEndpointAssignment", default_in_rules=[u"PersonalEndpointAssignment"]),
                           u"one_time_enrollers" : dict( title=u"One-Time Enrollers", element_type = u"GOnUserGroup"),
                         }


    built_in_rules =   [

                            dict( rule_type=u"AuthenticationStatus",
                                  rules = [
                                            dict(result = u"authenticated", conditions=[(u"personal_token", u"TokenGroup")]),
#                                            dict( result = u"Authenticated", conditions=[(u"Personal Device",u"EndpointGroup")]),
                                          ]),
                            dict( rule_type=u"ProgramAccess",
                                  rules = [
                                            dict(result = u"Field Enrollment", conditions=[(u"one_time_enrollers", u"UserGroup")]),
                                          ]),
                        ]


    def __init__(self):
        self.elements = None

    def is_loaded(self):
        return self.elements != None

    def load(self, dbt=None):
        with database.SessionWrapper(dbt, read_only=False) as transaction:

            self.elements = dict()

            for name, e in self._built_in_elements.items():
                built_in = transaction.select_first(model.BuiltInCriteria, filter = model.BuiltInCriteria.internal_name == name)
                if not built_in:
                    entity_type = e["element_type"]
                    label = e["title"]
                    element_type_def = def_element_types.get(entity_type)
                    rule_cls = _get_rule_class(element_type_def.get("type_"))
                    criteria_list = _get_matching_criteria(transaction, rule_cls, entity_type, label)
                    if len(criteria_list)==0:
                        # create it!
                        criteria_element = _create_class_criteria(rule_cls, element_type_def, label, dbt=transaction)
                        criteria = criteria_element.criteria

                    elif len(criteria_list) > 1:
                        raise Exception("Inconsistency in database : more than one representation of built-in element '%s' of type '%s' exists" % (label, e["element_type"]))
                    else:
                        criteria = criteria_list[0]

                    built_in = transaction.add(model.BuiltInCriteria())
                    built_in.internal_name = name
                    built_in.criteria = criteria
                else:
                    criteria = built_in.criteria

                default_in_rules = e.get("default_in_rules", [])
                if default_in_rules:
                    _set_criteria_as_default(transaction, criteria, e["element_type"], default_in_rules)
                self.elements[name] = BuiltInCriteriaElement(criteria, e)






    def _check_loaded(self):
        if not self.is_loaded():
            self.load()

    def get_element(self, name):
        self._check_loaded()
        return self.elements.get(name)



_built_in_elements = BuiltInElements()


def create_build_in_elements(environment, transaction):
    _init_element_types()
    _built_in_elements.load(transaction)

    checkpoint_handler = environment.checkpoint_handler
    activity_log = ActivityLog(environment)
    activity_log_manager = ActivityLogCreator(activity_log, session_id = "__setup__", user_name="gon_config", severity=ActivityLog.INFO)
    for rule_def in BuiltInElements.built_in_rules:
        rule_type = rule_def.get("rule_type")
        rule_handler = create_rule_handler(checkpoint_handler, None, rule_type, activity_log_manager)
        for rule in rule_def.get("rules"):
            result_entity_type = rule_handler.rule_type.get("result_class")
            result_element_type_def = def_element_types.get(result_entity_type)
            result_cls = _get_rule_class(result_element_type_def.get("type_"))
            if result_cls==model.Action:
                result_criteria = transaction.select_first(model.Action, filter=model.Action.title==rule.get("result"))
                result_element = CriteriaElement(result_criteria)
                result_element.entity_type = result_entity_type
            else:
                result_element = _built_in_elements.get_element(rule.get("result"))
            if not result_element:
                print "Default rule result '%s' not found" % rule.get("result")
                continue

            new_rule = Rule()
            new_rule.result = result_element
            for (condition_name, condition_type) in rule.get("conditions"):
                element = _built_in_elements.get_element(condition_name)
                element.entity_type = "%s.%s" % (condition_type, element.entity_type)
                new_rule.conditions.append(element)
            rule_handler.add_rule(new_rule, transaction)


def get_basic_types(entity_type, checkpoint_handler):
    return _get_basic_types1(entity_type, entity_type, checkpoint_handler)


def _get_basic_types1(main_entity_type, entity_type, checkpoint_handler):
    module_type_dict = get_element_type_dict(entity_type)
    if not module_type_dict:
        if checkpoint_handler:
            checkpoint_handler.Checkpoint("get_basic_type_names", "rule_api", lib.checkpoint.ERROR, msg="Unknown element type '%s'" % entity_type)
        return []
    if module_type_dict["type_"] == "composite":
        result = []
        for sub_entity_type in module_type_dict["elements"]:
            result.extend(_get_basic_types1(main_entity_type, sub_entity_type, checkpoint_handler))
        return result
    else:
        return [(entity_type, module_type_dict)]



def get_rule_entity_type(entity_type, result_entity_type):
    result_entity_type = result_entity_type.rpartition(".")[2]
    for rule_dict in def_rule_types.values():
        if rule_dict.get("result_class") == result_entity_type:
            for condition in rule_dict.get("conditions"):
                if is_type_of(entity_type, condition):
                    return condition
    return None


def _get_rule_class(element_type):
    if element_type == "action":
        return model.Action
    elif element_type == "rule":
        return model.Rule
    elif element_type == "association":
        return model.Association
    elif element_type == "group":
        return model.Group
    raise Exception("Unknown rule element '%s'" % element_type)



def _create_elements(result_criteria, dbs):
    restricted_ids = get_restricted_action_ids()
    elements = []
    action_elements = []
    for c in result_criteria:
        if c.internal_type_name == model.RuleObject.TYPE_ACTION:
            action_elements.append(c)
        else:
            element = CriteriaElement(c)
            elements.append(element)

    launch_spec_dict = traffic_schema.get_launch_title_and_categories(dbs, [action.id for action in action_elements])
    for action_element in action_elements:
        element = CriteriaElement(action_element)
        element.description, element.element_type = launch_spec_dict.get(action_element.id, ("N/A", "Unknown"))
        if action_element.id in restricted_ids:
            element.element_type += ".%s" % RESTRICTED_ACTION
        elements.append(element)


    return elements


def get_specific_criteria_elements(element_ids):
    """ returns sequence of criteria elements of the given type.
        element_type should be an object of type components.auth.server_management.ElementType
    """
    with database.QuerySession() as db_session:
        result_criteria = db_session.select(model.RuleObject, filter=database.in_(model.RuleObject.id, element_ids))
        elements = _create_elements(result_criteria, db_session)

    return elements



def get_criteria_elements(element_type_def):
    """ returns sequence of criteria elements of the given type.
        element_type should be an object of type components.auth.server_management.ElementType
    """
    element_type = element_type_def["type_"]
    if element_type == "action":
        with database.QuerySession() as db_session:
            result_criteria = db_session.select(model.RuleObject, model.RuleObject.internal_type_name == model.RuleObject.TYPE_ACTION)
            elements = _create_elements(result_criteria, db_session)
    else:
        cls = None
        if element_type == "rule":
            cls = model.Rule
            model_type = model.RuleObject.TYPE_RULE
        elif element_type == "association":
            cls = model.Association
            model_type = model.RuleObject.TYPE_ASSSOCIATION
        elif element_type == "group":
            cls = model.Group
            model_type = model.RuleObject.TYPE_GROUP
        with database.QuerySession() as db_session:
            result_criteria = db_session.select(model.RuleObject,
                                                database.and_(model.RuleObject.entity_type == element_type_def["name"],
                                                              model.RuleObject.internal_type_name == model_type))
            elements =  [ CriteriaElement(c) for c in result_criteria]



    for e in elements:
        yield e


def create_rule_handler(check_point_handler, user_admin, class_name, activity_log_scope):
    """ returns a rule handler object for the given rule class name
        plugin socket element is used to get elements from plugins
    """
    return MapRuleWindowHandler(check_point_handler, user_admin, class_name, activity_log_scope)


def _find_default_rule_criteria(db_session, rule_type, entity_type):
    return db_session.select_first(model.DefaultRuleCriteria,
                                   filter=database.and_(model.DefaultRuleCriteria.entity_type == entity_type,
                                                        model.DefaultRuleCriteria.rule_type == rule_type)
                                   )


def _set_criteria_as_default(transaction, criteria, entity_type, rule_names):
    for rule_type in rule_names:
        existing_entry = _find_default_rule_criteria(transaction, rule_type, entity_type)
        if not existing_entry:
            newDefault = model.DefaultRuleCriteria()
            newDefault.criteria = criteria
            newDefault.rule_type = rule_type
            newDefault.entity_type = entity_type
            transaction.add(newDefault)
        else:
            existing_entry.criteria = criteria



class MapRuleWindowHandler(object):

    element_types = None


    def __init__(self, checkpoint_handler, user_admin, class_name, activity_log_scope):
        if not def_rule_types.has_key(class_name):
            raise Exception("Could not find rule class '%s'" % class_name)
        rule_type = def_rule_types[class_name]

        self.checkpoint_handler = checkpoint_handler
        self.user_admin = user_admin
        self.activity_log_scope = activity_log_scope
        self.class_name = class_name

        self.component_handlers = dict()
        self.add_component_handler("user", user_admin)


        self.rule_type = rule_type
        result_class_name = rule_type["result_class"]
        self.result_element_type = def_element_types[result_class_name]
        rule_element_types = rule_type["conditions"]

        self.rule_element_types = self._create_element_types(rule_element_types)

        # create rules for types and headlines
        self.rule_spec = RuleSpec(class_name)
        template_rule = Rule()
        header_rule = Rule()
        template_rule.result, header_rule.result = self._create_template_and_header_element(class_name, result_class_name, self.result_element_type)

        entity_type_map = dict()
        for (name, rule_element_type) in self.rule_element_types:
            template_element, header_element = self._create_template_and_header_element(class_name, name, rule_element_type)
            template_rule.conditions.append(template_element)
            header_rule.conditions.append(header_element)

            basic_types = get_basic_types(name, self.checkpoint_handler)
            entity_types = []
            for (entity_type, element_def) in basic_types:
                if name != entity_type:
                    entity_types.append(entity_type)
            if entity_types:
                entity_type_map[name] = entity_types

        self.rule_spec.header_rule = header_rule
        self.rule_spec.template_rule = template_rule
        self.rule_spec.entity_type_map = entity_type_map

    def add_component_handler(self, component_name, component):
        self.component_handlers[component_name] = component



    def log(self, name, **kwargs):
        self.checkpoint_handler.Checkpoint(name, rule_module_id, lib.checkpoint.DEBUG, **kwargs)

    def log_scope(self, name, **kwargs):
        return self.checkpoint_handler.CheckpointScope(name, rule_module_id, lib.checkpoint.DEBUG, **kwargs)


    def _get_rule_class(self, element_type):
        if element_type == "rule" or element_type == "action":
            return model.Rule
        elif element_type == "association":
            return model.Association
        elif element_type == "group":
            return model.Group
        raise Exception("Unknown rule element '%s'" % element_type)


    def _get_rules_from_db(self):
        with self.log_scope("_get_rules_from_db1", rule_type=self.result_element_type["name"]):
            db_session = database.QuerySession()
            rule_class = self._get_rule_class(self.result_element_type["type_"])
            result_criteria = db_session.select(rule_class, database.and_(rule_class.entity_type == self.result_element_type["name"],
#                                                                          database.lower(rule_class.internal_type_name) == self.result_element_type["type_"]))
                                                                          rule_class.internal_type_name == rule_class.__name__))
            rules = []
            if rule_class==model.Group:
                count = 0
                for rule_criteria in result_criteria:
                    rule_rows = db_session.select([database_schema.table_criteria,
                                                   database_schema.table_sub_criteria.c.id.label('rule_id'),
                                                   database_schema.table_sub_criteria.c.child_id,
                                                   database_schema.table_sub_criteria.c.deactivated,
                                                   database_schema.table_sub_criteria.c.rule_entity_type],
                                              filter=database.and_(database_schema.table_sub_criteria.c.parent_id==rule_criteria.id,
                                                                   database_schema.table_sub_criteria.c.child_id == database_schema.table_criteria.c.id))
                    result_element = CriteriaElement(rule_criteria)
                    for r in rule_rows:
                        count += 1
                        if count % 1000 == 0:
                            self.log("_get_rules_from_db", info="Found %d rules" % count)

                        rule_id = r.rule_id
                        rule = Rule1(rule_id, result_element)
                        rule.active = not r.deactivated
                        rule.add_condition(r)
                        rules.append(rule)
            else:
                rule_dict = dict()
                deactivated_rule_ids = db_session.select(database_schema.table_sub_criteria.c.child_id,
                                                         filter=model.RuleAssociation.deactivated == True)
                if self.result_element_type["type_"]=="action":
                    restricted_rules = db_session.select([database_schema.table_rule_restriction, database_schema.table_sub_criteria],
                                                        filter=database_schema.table_rule_restriction.c.rule_id==database_schema.table_sub_criteria.c.parent_id)
                    restricted_ids = [r.child_id for r in restricted_rules]
                else:
                    restricted_ids = []
                for rule_criteria in result_criteria:
                    rule_query_expr = db_session.select_expression(model.RuleAssociation.child_id, filter=model.RuleAssociation.parent_id == rule_criteria.id)
                    rule_rows = db_session.select([database_schema.table_criteria, database_schema.table_sub_criteria], use_labels=True,
                                              filter=database.and_(database.in_(database_schema.table_sub_criteria.c.parent_id, rule_query_expr),
                                                                   database_schema.table_sub_criteria.c.child_id == database_schema.table_criteria.c.id))
                    rule_clauses = db_session.select(database_schema.table_criteria, database.in_(database_schema.table_criteria.c.id, rule_query_expr))
                    rule_clause_dict = dict([(c.id,c) for c in rule_clauses])

                    result_element = CriteriaElement(rule_criteria)
                    if rule_criteria.id in restricted_ids:
                        result_element.element_type = RESTRICTED_ACTION
                    for r in rule_rows:
                        rule_id = r.parent_id
                        rule = rule_dict.get(rule_id)
                        if not rule:
                            rule = Rule1(rule_id, result_element)
                            if (rule_id,) in deactivated_rule_ids:
                                rule.active = False
                            rule_dict[rule_id] = rule
                            if len(rule_dict) % 1000 == 0:
                                self.log("_get_rules_from_db", info="Found %d rules" % len(rule_dict))
                        rule.add_condition(r)
                        rule_clause_dict.pop(rule_id, None)

#                    empty_rule_rows = db_session.select(database_schema.table_criteria,
#                                                        filter=database.and_(database.in_(database_schema.table_criteria.c.id, rule_query_expr),
#                                                        database.not_exists([database_schema.table_sub_criteria.c.id],
#                                                                            database_schema.table_sub_criteria.c.child_id == database_schema.table_criteria.c.id)))
                    if rule_clause_dict:
                        empty_rule_rows = rule_clause_dict.values()
                        for r in empty_rule_rows:
                            print r.title
                            rule_id = r.id
                            rule = Rule1(rule_id, result_element)
                            if (rule_id,) in deactivated_rule_ids:
                                rule.active = False
                            rule_dict[rule_id] = rule
                rules = rule_dict.values()

            for r in rules:
                r.set_condition_order(self.rule_type["conditions"])
            return rules


    def _get_rule_from_db(self, rule_id, db_session):
        rule_class = self._get_rule_class(self.result_element_type["type_"])
        with self.log_scope("_get_rule_from_db", rule_class=repr(rule_class), rule_id=rule_id):
            if rule_class == model.Group:
                rule_association = db_session.get(model.RuleAssociation, rule_id)
                if rule_association:
                    rule_criteria = rule_association.parent_criteria
                    criteria =  rule_association.child_criteria
                    return self._create_group_rule_from_criteria(rule_association, rule_criteria)
            else:
                criteria = db_session.get(model.RuleObject, rule_id)
                if criteria:
                    parent_association = criteria.parent_criterias[0]
                    return self._create_rule_from_criteria(criteria, parent_association)


            return None


    def _create_group_rule_from_criteria(self, rule_association, result_criteria):
        main_criteria = result_criteria

        rule = Rule(main_criteria, rule_association)
        rule.active = not rule_association.deactivated
        rule_entity_type = rule_association.rule_entity_type
        rule.conditions.append(self._create_condition_element_from_criteria(rule_entity_type, rule_association.child_criteria))
        rule.set_condition_order(self.rule_type["conditions"])
        return rule


    def _create_rule_from_criteria(self, criteria, parent_association):
        result_criteria = parent_association.parent_criteria

        rule = Rule(result_criteria, criteria)
        for rule_entity_type, c in criteria.get_entity_type_and_children():
            rule.conditions.append(self._create_condition_element_from_criteria(rule_entity_type, c))

        rule.active = not parent_association.deactivated
        rule.set_condition_order(self.rule_type["conditions"])
        return rule


    def _create_template_and_header_element(self, rule_type, element_type, element_type_dict):
        with database.ReadonlySession() as dbs:
            default_criteria = _find_default_rule_criteria(dbs, rule_type, element_type)
            if default_criteria:
                template_element = CriteriaElement(default_criteria.criteria)
            else:
                template_element = TemplateElement(element_type, element_type_dict, nameless=True)
            header_element = TemplateElement(element_type, element_type_dict, rule_info=True)
            return template_element, header_element


    def _add_condition_rule_element(self, transaction, parent_criteria, element):
        return _add_condition_rule_element(transaction, parent_criteria, element, self.component_handlers)

    def _create_element_types(self, element_type_names):
        return_val = []
        for name in element_type_names:
            return_val.append(self.element_types[name])
        return return_val


    def _create_condition_element_from_criteria(self, rule_entity_type, criteria):
        return _create_condition_element_from_criteria(rule_entity_type, criteria)


    def _get_result_criteria_by_id(self, transaction, id):
        result = transaction.get(model.RuleObject, id)
        if isinstance(result, model.Action):

            result_entity_type = self.rule_type["result_class"]
            result_criteria = None
            for c in result.get_children():
                if c.entity_type == result_entity_type:
                    result_criteria = c
                    break

            if not result_criteria:
                result_criteria = transaction.add(model.Rule())
                result_criteria.title = result.title
                result_criteria.entity_type = result_entity_type

                transaction.add(result_criteria)
                result.add_sub_criteria(result_criteria)
                transaction.flush()

            result = result_criteria

        return result
        '''
        for c in self.result_criteria:
            if int(c.id) == int(id):
                return c
        return None
        '''

    def get_rules(self):
        """Return all the rules of the handlers type
        """
        return self._get_rules_from_db()

    def get_rule_spec(self):
        return self.rule_spec

    def _check_mandatory_result(self, rule_to_remove_or_update, is_group):
        mandatory_result_id = self.rule_type.get("mandatory_result")
        if not mandatory_result_id is None:
            result_id = rule_to_remove_or_update.get_result_element().get_id()
            if result_id == mandatory_result_id:
                with database.ReadonlySession() as dbs:
                    mandatory_result = dbs.get(model.RuleObject, mandatory_result_id)
                    for child in mandatory_result.child_criterias:
                        # skip the rule about to be deleted or changed
                        rule_id = rule_to_remove_or_update.get_id()
                        if is_group and child.id == rule_id or not is_group and child.child_id == rule_id:
                            continue
                        if not child.deactivated:
                            return
                    raise OperationNotAllowedException("There should be at least one active rule with the result '%s'" % mandatory_result.title)



    def delete_rule(self, rule, dbt=None):
        """ deletes the rule which the rule object represents.
            rule should be an object of type components.auth.server_management.RuleType
        """
        with self.log_scope("delete_rule", rule=repr(rule)):
            with database.SessionWrapper(dbt, read_only=False) as transaction:
                rule_to_remove = self._get_rule_from_db(rule.get_id(), transaction)

                if rule_to_remove:
                    if not _rules_identical(rule, rule_to_remove):
                        raise OperationNotAllowedException("Action canceled: The rule has been changed by another user")
                    rule_class = self._get_rule_class(self.result_element_type["type_"])
                    self._check_mandatory_result(rule_to_remove, rule_class == model.Group)
                    if rule_class == model.Group:
                        rule_association = transaction.get(model.RuleAssociation, rule.get_id())
                        rule_association.delete(transaction)
                        new_rule_api.rule_deleted(rule.get_id())
                    else:
                        delete_rule(rule.get_id(), transaction, check_if_used=False)

                    self.activity_log_scope.copyUpdateAndAdd(transaction,
                                                           type="Delete",
                                                           summary="Delete '%s' Rule" % self._get_rule_class_title(),
                                                           details = "Deleted Rule: %s" % (rule2logstring(rule),),
                                                           )

                else:
                    self.log("delete_rule", info="Rule already deleted")


    def _get_rule_class_title(self):
        return get_rule_type_title(self.class_name)

    def _find_non_empty_matching_condition(self, mandatory_condition, conditions):
        for condition in conditions:
            if condition.get_id() and condition.get_main_entity_type() == mandatory_condition:
                return True
        return False


    def _check_mandatory_conditions(self, mandatory_conditions, conditions):
        if not mandatory_conditions:
            return True
        all_missing_conditions = []
        for mandatory_condition_list in mandatory_conditions:
            missing_conditions = []
            for mandatory_condition in mandatory_condition_list:
                if not self._find_non_empty_matching_condition(mandatory_condition, conditions):
                    name = def_element_types[mandatory_condition]["title"]
                    missing_conditions.append(name)
            if not missing_conditions:
                return True
            all_missing_conditions.append(missing_conditions)

        message = " or ".join(["+".join([def_element_types[x]["title"] for x in list]) for list in mandatory_conditions])
        raise OperationNotAllowedException("Invalid rule conditions. Valid rule conditions are %s" % (message,))


    def _get_condition_element_of_type(self, rule, type_name):
        for condition in rule.get_condition_elements():
            if condition.get_main_entity_type() == type_name:
                return condition
        return None

    def _check_unique_condition(self, new_rule, unique_condition_type, db_session):
        new_condition = self._get_condition_element_of_type(new_rule, unique_condition_type)
        if new_condition:
            rule_entity_type, basic_entity_type = new_condition.split_entity_type()
            type_def = get_element_type_dict(basic_entity_type)
            type_ = type_def["type_"]
            if type_ in ["module", "component"]:
                module_name, dot, condition_id = new_condition.get_id().partition(".")
                matching_criterias = db_session.select(model.Condition, filter=database.and_(model.Condition.value_id==condition_id,
                                                                                             model.Condition.entity_type==basic_entity_type))
            else:
                matching_criterias = []
                criteria = db_session.get(model.RuleObject, new_condition.get_id())
                if criteria:
                    matching_criterias.append(criteria)

            for criteria in matching_criterias:
                result_id = int(new_rule.get_result_element().get_id())
                result_criteria = db_session.get(model.RuleObject, result_id)
                if isinstance(result_criteria, model.Group):
                    if result_criteria.has_member(criteria.id, exclude_rule_id=new_rule.get_id()):
                        name = def_element_types[unique_condition_type]["title"]
                        raise OperationNotAllowedException("%s is already a member of this group" % name)
                else:
                    if criteria.is_in_rule(result_id, exclude_rule_id=new_rule.get_id()):
                        name = def_element_types[unique_condition_type]["title"]
                        raise OperationNotAllowedException("%s has already been used in another rule" % name)





    def _check_conditions(self, db_session, rule):
        for unique_condition in self.rule_type.get("unique_conditions", []):
            self._check_unique_condition(rule, unique_condition, db_session)
        result_label = rule.get_result_element().get_label()
        result_criteria = db_session.get(model.RuleObject, rule.get_result_element().get_id())
        if result_criteria.built_in:
            built_in_name = result_criteria.built_in[0].internal_name
            built_in_element = _built_in_elements.get_element(built_in_name)
            built_in_meta = built_in_element.get_meta_info()
            mandatory_conditions = built_in_meta.get("mandatory_conditions", [])
            self._check_mandatory_conditions(mandatory_conditions, rule.get_condition_elements())
        mandatory_conditions = self.rule_type.get("mandatory_conditions", [])
        self._check_mandatory_conditions(mandatory_conditions, rule.get_condition_elements())


    def add_rule(self, rule, db_transaction=None):
        """ adds the rule which the rule object represents.
            rule should be an object of type components.auth.server_management.RuleType
            returns a RuleType representation of the rule
        """
        with self.log_scope("add_rule", rule=repr(rule)):
            with database.SessionWrapper(db_transaction, read_only=False) as transaction:
                ok = self._check_conditions(transaction, rule)

                result_element = rule.get_result_element()
                if not result_element.get_id():
                    raise OperationNotAllowedException("No result specified in rule")

                result_criteria = self._get_result_criteria_by_id(transaction, result_element.get_id())
#                rule_criteria = _get_window_rules_criteria(result_criteria)

                #"create rule criteria"

                clause_criteria = result_criteria.get_new_clause_criteria(transaction, rule.is_active())
                clause_criteria.entity_type = result_criteria.entity_type
                has_conditions = False

                result_rule = Rule(result_criteria, clause_criteria)
                result_rule.active = rule.is_active()
                result_rule.get_result_element().element_type = result_element.get_element_type()

                #"add conditions"
                for element in rule.get_condition_elements():
                    if element.get_id():
                        has_conditions = True
                    rule_element = self._add_condition_rule_element(transaction, clause_criteria, element)
                    result_rule.conditions.append(rule_element)
                    self.log("add_rule", info="Add condition", new_condition=repr(rule_element))

                clause_criteria.is_true = not has_conditions

                self.activity_log_scope.copyUpdateAndAdd(transaction,
                                                       type="Create",
                                                       summary="Create '%s' Rule" % self._get_rule_class_title(),
                                                       details = "New Rule: %s" % (rule2logstring(rule))
                                                       )

            result_rule.set_condition_order(self.rule_type["conditions"])
            new_rule_api.rule_created(result_rule.get_id())



            return RuleCopy(result_rule)



    def update_rule(self, old_rule, new_rule):
        """ new_rule should be an object of type components.auth.server_management.RuleType with get_id() returning the id of
            the rule which should be updated. Otherwise the object should contain the data which the new rule should
            consist of.
            old_rule should either be None or an object of type omponents.auth.server_management.RuleType representing the rule before update as viewed
            by the client.
            returns a RuleType representation of the updated rule
        """
        with self.log_scope("update_rule", rule=repr(new_rule)):
            with database.Transaction() as transaction:
                rule_to_replace = self._get_rule_from_db(new_rule.get_id(), transaction)

                if not rule_to_replace:
                    raise OperationNotAllowedException("Action canceled: The rule has been deleted by another user")

                if old_rule and not _rules_identical(old_rule, rule_to_replace):
                    raise OperationNotAllowedException("Action canceled: The rule has been changed by another user")

                self._check_conditions(transaction, new_rule)
                criteria_id = new_rule.get_id()

                rule_class = self._get_rule_class(self.result_element_type["type_"])
                if rule_class == model.Group:

                    rule_association = transaction.get(model.RuleAssociation, criteria_id)


                    new_result = new_rule.get_result_element()
                    old_result = rule_to_replace.get_result_element()
                    if int(new_result.get_id()) != int(old_result.get_id()):
                        self.log("update_rule", info="New result element")
                        self._check_mandatory_result(rule_to_replace, is_group=True)
                        new_main_criteria = self._get_result_criteria_by_id(transaction, new_result.get_id())
                        rule_association.parent_criteria = new_main_criteria
                        rule_association.parent_internal_type_name = new_main_criteria.internal_type_name
                        result_rule = Rule(new_main_criteria, rule_association)
                    else:
                        if not new_rule.is_active():
                            self._check_mandatory_result(rule_to_replace, is_group=True)
                        result_rule = Rule()
                        result_rule.criteria = rule_to_replace.criteria
                        result_rule.result = rule_to_replace.result

                    condition_elements = new_rule.get_condition_elements()
                    if len(condition_elements)==0:
                        rule_association.delete(transaction)
                    else:
                        for condition in condition_elements:
                            rule_element = self._add_condition_rule_element(transaction, rule_association, condition)
                            result_rule.conditions.append(rule_element)
                        rule_association.deactivated = not new_rule.is_active()
                        result_rule.active = new_rule.is_active()


                    if old_rule:
                        for element in old_rule.get_condition_elements():
                            rule_entity_type, basic_entity_type = element.split_entity_type()
                            rule_element_type_def = def_element_types.get(basic_entity_type)
                            rule_element_type = rule_element_type_def["type_"]

                            if rule_element_type in ["module", "component"]:
                                condition = transaction.select_first(model.Condition, filter=database.and_(model.Condition.value_id==element.get_id(),
                                                                                       model.Condition.entity_type==basic_entity_type))
                                if condition and not condition.parent_criterias:
                                    transaction.delete(condition)



                else:
                    update_criteria = transaction.get(model.RuleObject, criteria_id)
                    if not update_criteria:
                        raise Exception("Could not find rule to update")

                    new_result = new_rule.get_result_element()
                    old_result = rule_to_replace.get_result_element()
                    if int(new_result.get_id()) != int(old_result.get_id()):
                        self.log("update_rule", info="New result element")
                        self._check_mandatory_result(rule_to_replace, is_group=False)
                        child_association = update_criteria.parent_criterias.pop()
                        transaction.delete(child_association)
                        new_main_criteria = self._get_result_criteria_by_id(transaction, new_result.get_id())
#                        child_association.parent_criteria = new_main_criteria
#                        child_association.parent_internal_type_name = new_main_criteria.internal_type_name
                        new_main_criteria.add_sub_criteria(update_criteria, deactivated=not new_rule.is_active())
                        result_rule = Rule(new_main_criteria, update_criteria)
                    else:
                        if not new_rule.is_active():
                            self._check_mandatory_result(rule_to_replace, is_group=False)
                        result_rule = Rule()
                        result_rule.criteria = rule_to_replace.criteria
                        result_rule.result = rule_to_replace.result
                        parent_association = update_criteria.parent_criterias[0]
                        parent_association.deactivated = not new_rule.is_active()
                    result_rule.active = new_rule.is_active()

                    new_conditions = []
                    deleted_conditions = []
                    for new_condition in new_rule.get_condition_elements():
                        found = None
                        for old_condition in rule_to_replace.get_condition_elements():
                            if new_condition.same_type(old_condition):
                                if new_condition.equals(old_condition):
                                    result_rule.conditions.append(old_condition)
                                else:
                                    if old_condition.get_id():
                                        ''' Old condition is not a template '''
                                        deleted_conditions.append(old_condition)
                                    if new_condition.get_id():
                                        ''' New condition is not a template '''
                                        new_conditions.append(new_condition)
                                found = old_condition
                                break
                        if not found:
                            new_conditions.append(new_condition)
                        else:
                            rule_to_replace.conditions.remove(found)

                    deleted_conditions.extend(rule_to_replace.conditions)

                    for new_condition in new_conditions:
                        self.log("update_rule", info="Add condition", new_condition=repr(new_condition))
                        rule_element = self._add_condition_rule_element(transaction, update_criteria, new_condition)
                        result_rule.conditions.append(rule_element)
                    for old_condition in deleted_conditions:
                        if not old_condition.get_id():
                            ''' Old condition is template '''
                            continue
                        self.log("update_rule", info="Delete condition", old_condition=repr(old_condition))
                        _delete_condition_criteria(old_condition.criteria, update_criteria, transaction)

                    is_true = True
                    for condition in result_rule.get_condition_elements():
                        if condition.get_id():
                            is_true = False
                            break

                    update_criteria.is_true = is_true

                self.activity_log_scope.copyUpdateAndAdd(transaction,
                                                       type="Update",
                                                       summary="Update '%s' Rule" % self._get_rule_class_title(),
                                                       details = "Old rule: %s\nNew Rule: %s" % (rule2logstring(old_rule), rule2logstring(new_rule)),
                                                       )

            self.log("update_rule", info="Rule updated")
            new_rule_api.rule_updated(new_rule.get_id())
            result_rule.set_condition_order(self.rule_type["conditions"])


            return RuleCopy(result_rule)




class AuthAdminReceiver(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
    def __init__(self, checkpoint_handler):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, checkpoint_handler, components.auth.component_id)

    def auth_report(self, unique_session_id, server_sid, auth_success, report):
#        print report

        if not auth_success:
            error_messages = []
            for report_line in report:
                try:
                    line_type = report_line[0]
                    if line_type == AuthorizationAnalysis.PASSED_CRITERIA_TYPE:
                        error_message = "Check for %s '%s' passed" % report_line[1]
                    elif line_type == AuthorizationAnalysis.NO_REPLY_TYPE:
                        error_message = "Checking %s failed. Please check log files for further information" % report_line[1]
                    elif line_type == AuthorizationAnalysis.PARTIAL_RESULT_TYPE:
                        if isinstance(report_line[1], basestring):
                            enabled = report_line[1]
                        else:
                            enabled = "%s '%s'" % report_line[1]
                        error_message = "Possibly missing %s '%s'" % report_line[2] + ", which would enable %s" % enabled
                    else:
                        error_message = "Unknown error '%s'" % repr(report_line)
                    error_messages.append(error_message)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("auth_report", "rule_api", lib.checkpoint.ERROR, etype, evalue, etrace)
                    self.checkpoint_handler.Checkpoint("auth_report.error", "rule_api", lib.checkpoint.ERROR, report_line=repr(report_line))

            full_error_message = "\n".join(error_messages)

            with database.Transaction() as t:
                notification = t.add(access_log_db_schema.Notification())
                notification.unique_session_id = unique_session_id
                notification.type = access_log_db_schema.Notification.TYPE_AUTH
                notification.severity = access_log_db_schema.Notification.SEVERITY_WARNING
                notification.source = ""
                notification.code = -1
                notification.summary = "Authorization failure"
                notification.details = full_error_message
                notification.timestamp =  datetime.datetime.now()
#                session_error = t.add(access_log_db_schema.SessionError())
#                session_error.unique_session_id = unique_session_id
#                session_error.error_type = access_log_db_schema.SessionError.ERROR_TYPE_AUTH
#                session_error.error_source = ""
#                session_error.error_code = "Authorization failure"
#                session_error.error_message = full_error_message
#                session_error.timestamp = datetime.datetime.now()
        else:
            with database.Transaction() as t:
                for report_line in report:
#                    print report_line
                    auth_result = t.add(access_log_db_schema.AuthResult(unique_session_id=unique_session_id))
                    auth_result.result_type = int(report_line[0])
                    criteria_type, criteria_text = report_line[1]
                    auth_result.criteria_type = criteria_type
                    auth_result.criteria_text = criteria_text



#    def report_auth_results(self, unique_session_id, server_sid, auth_success, report):
    def report_auth_results(self, unique_session_id, report, server_sid):
        with database.Transaction() as t:
            for report_line in report:
#                    print report_line
                auth_result = t.add(access_log_db_schema.AuthResult(unique_session_id=unique_session_id))
                auth_result.result_type = int(report_line[0])
                criteria_type, criteria_text = report_line[1]
                auth_result.criteria_type = criteria_type
                auth_result.criteria_text = criteria_text




def _get_element_type_dict(element_def):
    entity_type = element_def.get_main_entity_type()

    if not def_element_types.has_key(entity_type):
        raise Exception("Unknown entity type '%s'" % entity_type)

    element_type = def_element_types[entity_type]
    return element_type

def _get_element_type(element_def):
    entity_type = element_def.get_main_entity_type()
    if not def_element_types.has_key(entity_type):
        raise Exception("Unknown entity type '%s'" % entity_type)

    element_type = def_element_types[entity_type]
    return element_type["type_"]



def _get_matching_criteria(transaction, cls, entity_type, label):
    return  transaction.select(cls,
                               database.and_(cls.entity_type == entity_type,
                                             cls.title == label)
                               )



def check_can_delete_criteria(existing_criteria, transaction):
    if existing_criteria.built_in:
        raise OperationNotAllowedException("This element cannot be deleted")
    restrictions = transaction.select(model.RuleRestriction, filter=model.RuleRestriction.restriction_rule_id==existing_criteria.id)
    if restrictions:
        raise OperationNotAllowedException("This rescriction is in use")
    if len(existing_criteria.parent_criterias)>0:
        raise OperationNotAllowedException("This element is used in one or more rules")




def delete_criteria(element_id, db_transaction=None):
    """ deletes the criteria with id element_id
    """
    return_val = True
    with database.SessionWrapper(db_transaction, read_only=False) as transaction:
        existing_criteria = transaction.get(model.RuleObject, element_id)
        if existing_criteria:
            check_can_delete_criteria(existing_criteria, transaction)
#            for parent_assoc in existing_criteria.parent_criterias:
#                if not parent_assoc.parent_internal_type_name in [model.RuleObject.TYPE_GROUP, model.RuleObject.TYPE_ADMIN_ACCESS]:
#                    raise OperationNotAllowedException("This element is used in one or more rules")
#                transaction.delete(parent_assoc)

            restrictions = transaction.select(model.RuleRestriction, filter=model.RuleRestriction.rule_id==existing_criteria.id)
            for restriction in restrictions:
                transaction.delete(restriction)

            existing_criteria.delete(transaction)
    return return_val

def delete_criteria_for_element(element_type, element_id, db_session):
    type_def = get_element_type_dict(element_type)
    type_ = type_def["type_"]
    if type_ == "module" or type_=="component":
        conditions = get_matching_conditions(db_session, element_id, element_type)
        for condition in conditions:
            delete_criteria(condition.id, db_session)
#            for assoc in condition.parent_criterias:
#                if assoc.parent_internal_type_name != model.RuleObject.TYPE_GROUP:
#                    raise OperationNotAllowedException("%s is used in one or more rules" % type_def["title"])
#                else:
#                    db_session.delete(assoc)
#            db_session.delete(condition)


def config_create_row(element_type, template, dbt):
    element_type_def = def_element_types.get(element_type)
    rule_class = _get_rule_class(element_type_def["type_"])
    config_spec = ConfigTemplateSpec(template)
    label = config_spec.get_value("label")
    description = config_spec.get_value("description")

    return _create_class_criteria(rule_class, element_type_def, label, description=description, dbt=dbt)

def config_update_row(element_type, template, dbt):
    config_spec = ConfigTemplateSpec(template)
    element_id = config_spec.get_value("id")
    label = config_spec.get_value("label")
    description = config_spec.get_value("description")
    update_criteria(element_id, label, description, dbt)

def config_get_template(element_type, element_id, db_session):
    config_spec = ConfigTemplateSpec()
    element_def = def_element_types.get(element_type)
    if not element_def.get("crud_enabled", True):
        return None
    config_spec.init(element_type, element_def["title"], "")
    config_spec.add_field('id', 'ID', ConfigTemplateSpec.VALUE_TYPE_STRING, ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
    field = config_spec.add_field('label', 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
    field.set_max_length(100)
    description_caption = element_def.get("description_caption", 'Description')
    config_spec.add_field('description', description_caption, ConfigTemplateSpec.VALUE_TYPE_TEXT)
    if element_id:
        criteria = get_class_criteria(db_session, element_id)
        config_spec.set_values_from_object(criteria)
        config_spec.set_field_value(field, criteria.title)
        if criteria.built_in:
            config_spec.set_read_only()
    return config_spec.get_template()


def _add_condition_rule_element(transaction, parent_criteria, element, component_handlers):
    # Ignore dummy element
    if not element.get_id():
        return element

    rule_entity_type, basic_entity_type = element.split_entity_type()
    rule_element_type_def = def_element_types.get(basic_entity_type)
    rule_element_type = rule_element_type_def["type_"]

    if rule_element_type in ["module", "component"]:
        condition = transaction.select_first(model.Condition, filter=database.and_(model.Condition.value_id==element.get_id(),
                                                                                   model.Condition.entity_type==basic_entity_type))

        if not condition:
            new_condition = model.Condition()
            new_condition.title = element.get_label()
            new_condition.entity_type = basic_entity_type
            if rule_element_type  == "module":
                plugin_name, element_id = split_entity_type(element.get_id())
                new_condition.value_id = element_id
            else:
                new_condition.value_id = element.get_id()
                if component_handlers:
                    component_handler = component_handlers.get(rule_element_type_def["component_name"])
                    if component_handler:
                        (new_condition.value_id, new_condition.title) = component_handler.register_element(transaction, element, rule_element_type_def["internal_element_type"])

            condition = new_condition


        parent_criteria.add_sub_criteria(condition, rule_entity_type)

        rule_element = element
    else:
        #sub_criteria = model.Criteria.get(transaction, element.get_id())
        condition = transaction.get(model.RuleObject, element.get_id())
        parent_criteria.add_sub_criteria(condition, rule_entity_type)
        rule_element = element

    return rule_element


def create_simple_rule(transaction, rule, component_handlers=None):

    result_element = rule.get_result_element()
#    element_type_def = def_element_types.get(result_element.get_entity_type())
#    rule_cls = _get_rule_class(element_type_def.get("type_"))
#    if rule_cls==model.Rule:
#        cls = model.RuleClause
#    elif rule_cls==model.Association:
#        cls = model.AssociationClause
#    else:
#        raise Exception("Cannot create simple rule with result of entity type '%s'" % result_element.get_entity_type())

    cls = model.AssociationClause

    clause_criteria = cls()
    clause_criteria.entity_type=result_element.get_entity_type()
    clause_criteria.title=result_element.get_label()
    transaction.add(clause_criteria)

    for element in rule.get_condition_elements():
        rule_element = _add_condition_rule_element(transaction, clause_criteria, element, component_handlers)

    transaction.flush()
    new_rule_api.rule_created(clause_criteria.id)
    return clause_criteria.id

def update_simple_rule(transaction, rule_id, new_title):

    criteria_for_update = transaction.get(model.RuleObject, rule_id)
    if not criteria_for_update:
        raise Exception("Element not found")

    criteria_for_update.title = new_title
    for child in criteria_for_update.get_children():
        child.title = new_title

    new_rule_api.rule_updated(rule_id)



def _create_condition_element_from_criteria(rule_entity_type, criteria):
    if isinstance(criteria, model.Condition):
        entity_type = "%s.%s" % (rule_entity_type, criteria.entity_type)
#        print "_create_condition_element_from_criteria.entity_type", entity_type
        element =  BasicConditionElement(entity_type, criteria)
        return element
    else:
        return CriteriaElement(criteria, rule_entity_type)


def get_simple_rule(db_session, rule_id):
    criteria = db_session.get(model.RuleObject, rule_id)
    rule = Rule(criteria, criteria)
    for rule_entity_type, c in criteria.get_entity_type_and_criterias():
        rule.conditions.append(_create_condition_element_from_criteria(rule_entity_type, c, None))
    return rule




def _delete_condition_criteria(criteria, parent_criteria, transaction):
    for a in criteria.parent_criterias:
        c = a.parent_criteria
        if c == parent_criteria:
            a.delete(transaction)
            return

def set_rule_activation(rule_criteria_id, active, transaction):
    rule_criteria = transaction.get(model.RuleObject, rule_criteria_id)
    if rule_criteria:
        assert len(rule_criteria.parent_criterias)==1, "Error in rule structure"
        rule_criteria.parent_criterias[0].deactivated = not active



def delete_rule(rule_criteria_id, transaction, check_if_used=True):
    rule_criteria = transaction.get(model.RuleObject, rule_criteria_id)
    if rule_criteria:

        if check_if_used:
            check_can_delete_criteria(rule_criteria, transaction)

        for a in rule_criteria.child_criterias:
            a.delete(transaction)

        for a in rule_criteria.parent_criterias:
            transaction.delete(a)

        transaction.delete(rule_criteria)
        new_rule_api.rule_deleted(rule_criteria_id)



def _create_class_criteria(rule_class, element_type_def, label, description=None, dbt=None):
    if not label:
        if element_type_def:
            entity_type = element_type_def["title"]
        else:
            entity_type = "Element"
        raise OperationNotAllowedException("%s must have a name" % entity_type)

    if element_type_def:
        entity_type = element_type_def["name"]
    else:
        entity_type = None

    with database.SessionWrapper(dbt, read_only=False) as transaction:
        existing_criteria = _get_matching_criteria(transaction, rule_class, entity_type, label)
        if len(existing_criteria)>0:
            raise OperationNotAllowedException("An element with this name already exists")

        criteria = transaction.add(rule_class())
        criteria.title = label
        criteria.description = description
        criteria.entity_type = entity_type

        transaction.flush()
        return CriteriaElement(criteria)

def get_class_criteria(db_session, id):
    return db_session.get(model.RuleObject, id)


def create_action_criteria(label, dbt=None):

    '''
        TODO: We flush in order to be able to access criteria id. So creation is possibly not transactionally safe on sqlite(?).
        We should in stead have the id as a field on criteria and create it uniquely ourselves in some way
    '''

    with database.SessionWrapper(dbt, False) as transaction:
        element = _create_class_criteria(model.Action, None, label, dbt=transaction)
        transaction.flush()
        return element




def create_criteria(element_def, transaction=None):
    """ creates a criteria with the data from element_def.
        element_def should be of type components.auth.server_management.ElementType
        returns an ElementType represntation of the created element
    """
    label = element_def.get_label()

    element_type_def = _get_element_type_dict(element_def)
#    if element_type_def["config_type"] == "class":
    rule_class = _get_rule_class(element_type_def["type_"])

    return _create_class_criteria(rule_class, element_type_def, label, dbt=transaction)
#    else:
#        raise Exception("Unable to create element of type '%s'" % element_def.get_element_type())


def _check_existing_criteria(existing_criteria, id):
    if len(existing_criteria)>0:
        for c in existing_criteria:
            if int(c.id) != int(id):
                return False
    return True

def _ids_equal(id1, id2):
    if not isinstance(id1, basestring):
        id1 = str(id1)
    if not isinstance(id2, basestring):
        id2 = str(id2)
    return id1==id2

def _rules_identical(rule1, rule2):
    if rule1.get_id()!=rule2.get_id():
        return False
    if not _ids_equal(rule1.get_result_element().get_id(), rule2.get_result_element().get_id()):
        return False
    rule1_conditions = rule1.get_condition_elements()
    rule2_conditions = rule2.get_condition_elements()
    if len(rule1_conditions)!=len(rule2_conditions):
        return False
    condition_pairs = zip(rule1_conditions, rule2_conditions)
    for c1,c2 in condition_pairs:
        entity_type1 = c1.get_entity_type().rpartition(".")[2]
        entity_type2 = c2.get_entity_type().rpartition(".")[2]
        if entity_type1!=entity_type2:
            return False
#        print c1.get_id(), c2.get_id()
        if not _ids_equal(c1.get_id(), c2.get_id()):
            return False
    return True





def update_action_criteria(element_id, label, dbt=None):
    update_criteria(element_id, label, dbt=dbt)


def update_criteria(element_id, label, description=None, dbt=None):
    """ updates a criteria with label.
    """
    with database.SessionWrapper(dbt, False) as transaction:
        criteria_for_update = transaction.get(model.RuleObject, element_id)
        if not criteria_for_update:
            raise Exception("Element not found")

        existing_criteria = _get_matching_criteria(transaction, criteria_for_update.__class__, criteria_for_update.entity_type, label)
        if not _check_existing_criteria(existing_criteria, element_id):
            raise OperationNotAllowedException("An element with this name already exists")

        if criteria_for_update.built_in:
            raise OperationNotAllowedException("This element cannot be updated")

        if not label:
            element_type_def = def_element_types.get(criteria_for_update.entity_type)
            raise OperationNotAllowedException("%s must have a name" % element_type_def["title"])

        criteria_for_update.update_title(label)
        criteria_for_update.description = description
        return CriteriaElement(criteria_for_update)

def get_matching_conditions(db_session, value_id, entity_type):
        return db_session.select(model.Condition, filter=database.and_(model.Condition.value_id==value_id,
                                                                       model.Condition.entity_type==entity_type))

def get_all_personal_token_conditions():
    personal_token_criteria = _built_in_elements.get_element("personal_token")
    if not personal_token_criteria:
        raise Exception("get_all_personal_token_conditions : Personal Token criteria does not exist")
    personal_token_criteria_id = personal_token_criteria.get_id()
    with database.QuerySession() as db_session:
        personal_token_rule_ids = db_session.select_expression(model.RuleAssociation.child_id, model.RuleAssociation.parent_id==personal_token_criteria_id)
        rule_rows = db_session.select([database_schema.table_criteria, database_schema.table_sub_criteria],
                                      filter=database.and_(database_schema.table_sub_criteria.c.child_id == database_schema.table_criteria.c.id,
                                                           database.in_(database_schema.table_sub_criteria.c.parent_id, personal_token_rule_ids)
                                                           )

                                      )
        result_dict = dict()
        for r in rule_rows:
            element_ids = result_dict.get(r.entity_type, set())
            if r.internal_type_name == model.RuleObject.TYPE_CONDITION:
                element_def = def_element_types.get(r.entity_type)
                if element_def["type_"] == "module":
                    module_name = element_def.get("plugin")
                    element_id = "%s.%s" % (module_name, r.value_id)
                else:
                    element_id = r.value_id

                element_ids.add(element_id)
            else:
                element_ids.add(r.child_id)
            result_dict[r.entity_type] = element_ids
        return result_dict

personal_token_types = None

def _add_basic_types(entity_type, type_name_set):
    module_type_dict = get_element_type_dict(entity_type)
    if not module_type_dict:
        return
    type_name_set.add(entity_type)
    if module_type_dict["type_"] == "composite":
        for sub_entity_type in module_type_dict["elements"]:
            _add_basic_types(sub_entity_type, type_name_set)


def is_personal_token_type(entity_type):
    global personal_token_types
    if personal_token_types is None:
        personal_token_types = set()
        personal_token_criteria = _built_in_elements.get_element("personal_token")
        if not personal_token_criteria:
            raise Exception("is_personal_token_type : Personal Token criteria does not exist")
        built_in_meta = personal_token_criteria.get_meta_info()
        element_type = built_in_meta.get("element_type")
        rule_def = def_rule_types.get(element_type)
        conditions = rule_def.get("conditions")
        for entity_type in conditions:
            _add_basic_types(entity_type, personal_token_types)

    return entity_type in personal_token_types



def is_in_personal_token_rule(element_type, element_id, plugin_name=None, component_name=None):
    personal_token_criteria = _built_in_elements.get_element("personal_token")
    if not personal_token_criteria:
        raise Exception("is_in_personal_token_rule : Personal Token criteria does not exist")
    if plugin_name:
        entity_type = get_element_type_for_plugin_type(plugin_name, element_type)
    elif component_name:
        entity_type = get_element_type_for_component_type(component_name, element_type)
    else:
        raise Exception("is_in_personal_token_rule : missing plugin or component name")

    personal_token_criteria_id = personal_token_criteria.get_id()
    with database.QuerySession() as db_session:
        rule_rows = db_session.select([database_schema.table_criteria, database_schema.table_sub_criteria],
                                  filter=database.and_(database_schema.table_sub_criteria.c.child_id == database_schema.table_criteria.c.id,
                                                       database_schema.table_criteria.c.value_id==element_id,
                                                       database_schema.table_criteria.c.entity_type==entity_type))
        rule_ids = [r.parent_id for r in rule_rows]
        personal_token_rule_rows = db_session.select(model.RuleAssociation,
                                                     database.and_(database.in_(model.RuleAssociation.child_id, rule_ids),
                                                                   model.RuleAssociation.parent_id==personal_token_criteria_id)
                                                     )
        return len(personal_token_rule_rows)



def element_updated(element_type, element_id, label, dbt=None):
    type_def = get_element_type_dict(element_type)
    type_ = type_def["type_"]
    if type_ == "module" or type_=="component":
        with database.SessionWrapper(dbt, read_only=False) as transaction:
            matching_conditions = get_matching_conditions(transaction, element_id, element_type)
            for condition in matching_conditions:
                condition.title = label

def get_element_lifetime(entity_type, value_id, dbs):
    return dbs.select_first(model.ElementLifetime, filter=database.and_(model.ElementLifetime.entity_type==entity_type,
                                                                        model.ElementLifetime.value_id==value_id))


def get_or_create_element_lifetime(entity_type, value_id, dbt):
    element = get_element_lifetime(entity_type, value_id, dbt)
    if not element:
        element = dbt.add(model.ElementLifetime())
        element.entity_type = entity_type
        element.value_id = value_id
    return element


def create_personal_device_rule(rule_handler, user_id, element_id, element_name, entity_type, activate_rule, transaction):
    rule = RuleCopy(rule_handler.get_rule_spec().get_template_rule())
    for condition in rule.conditions:
        if is_type_of(entity_type, condition.get_entity_type()):
            device_rule_entity_type = condition.get_entity_type()
    rule.conditions = []

    device_element_type = get_element_type_dict(entity_type)
    device_element = TemplateElement("%s.%s" % (device_rule_entity_type, entity_type), device_element_type)
    device_element.id = element_id
    device_element.title = element_name
    rule.conditions.append(device_element)
    user_element_type = get_element_type_dict("User")
    user_element = TemplateElement("User.User", user_element_type)
    user_element.id = user_id
    rule.conditions.append(user_element)
    rule.active = activate_rule

    rule_handler.add_rule(rule, transaction)

    return True, "Rule created"

def create_device_group_rule(rule_handler, group_name, element_id, element_name, activate_rule, transaction):
    entity_type = "Device"
    rule = RuleCopy(rule_handler.get_rule_spec().get_template_rule())
    for condition in rule.conditions:
        if is_type_of(entity_type, condition.get_entity_type()):
            device_rule_entity_type = condition.get_entity_type()
    rule.conditions = []

    device_element_type = get_element_type_dict(entity_type)
    device_element = TemplateElement("%s.%s" % (device_rule_entity_type, entity_type), device_element_type)
    device_element.id = element_id
    device_element.title = element_name
    rule.conditions.append(device_element)
    rule.active = activate_rule

    groups = _get_device_groups_by_name(group_name, transaction)

    assert len(groups)==1, "Found %d device groups named '%s'" % (len(groups), group_name)
    group = groups[0]
    rule.result = CriteriaElement(group)

    rule_handler.add_rule(rule, transaction)

    return True, "Rule created"

def _get_device_groups_by_name(group_name, db_session):
    return db_session.select(model.Group, filter=database.and_(model.Group.entity_type=="DeviceGroupMembership",
                                                               model.Group.title == group_name)
                                                                )

def device_group_exists(group_name):
    with database.QuerySession() as db_session:
        groups = _get_device_groups_by_name(group_name, db_session)
        return len(groups)>0

class TmpElement(ElementType):

    def __init__(self, element_id):
        self.element_id = element_id

    def get_id(self):
        return self.element_id

def get_gon_group_rule_type():
    gon_group_criteria = _built_in_elements.get_element("one_time_enrollers")
    if not gon_group_criteria:
        raise Exception("One-Time Enrollers group does not exist")

    return gon_group_criteria.get_meta_info().get("element_type")

def create_gon_group_rules(checkpoint_handler, user_admin, users, transaction):

    gon_group_criteria = _built_in_elements.get_element("one_time_enrollers")
    if not gon_group_criteria:
        raise Exception("One-Time Enrollers group does not exist")
        return

    result_criteria = gon_group_criteria.criteria
    transaction.add(result_criteria)

    with checkpoint_handler.CheckpointScope("get_existing_conditions", "rule_api", lib.checkpoint.DEBUG):
        existing_conditions = transaction.select(model.Condition, filter=model.Condition.entity_type=="User")
        existing_condition_dict = dict()
        existing_condition_dict.update([(condition.value_id, condition) for condition in existing_conditions])



    for user_element in users:
        user_id = user_element.get_id()
        user_label = user_element.get_label()
        with checkpoint_handler.CheckpointScope("create_gon_group_rule", "rule_api", lib.checkpoint.DEBUG, user_id=user_id, user_label=user_label):
            matching_criteria = existing_condition_dict.get(user_id)

            if matching_criteria:
                condition = matching_criteria
            else:
                condition = transaction.add(model.Condition())
                condition.entity_type = "User"
                condition.value_id = user_id
                condition.title = user_label
            result_criteria.add_sub_criteria(condition, "User")



def delete_gon_group_rule(get_rule_handler, user_id, transaction):

    gon_group_criteria = _built_in_elements.get_element("one_time_enrollers")
    if not gon_group_criteria:
        return
    gon_group_criteria_id = gon_group_criteria.criteria.id

    matching_criterias = transaction.select(model.Condition, filter=database.and_(model.Condition.value_id==user_id,
                                                                                  model.Condition.entity_type=="User"))

    for criteria in matching_criterias:
        for parent_edge in criteria.parent_criterias:
            if parent_edge.parent_id == gon_group_criteria_id:
                parent_edge.delete(transaction)
                return


def _init_element_types():
    if not MapRuleWindowHandler.element_types:
        MapRuleWindowHandler.element_types = dict()
        for item in def_element_types.items():
            element_name = item[0]
            element_dict = item[1]
            MapRuleWindowHandler.element_types[element_name] = item

            element_dict["name"] = element_name
            if element_dict["type_"] == "composite":
                for sub_element_name in element_dict["elements"]:
                    sub_element = def_element_types[sub_element_name]
                    parent_list = sub_element.get("_parent_list", [])
                    #parent_list.append(element_dict)
                    parent_list.append(element_name)
                    sub_element["_parent_list"] = parent_list


#        element_type = dict()
#        element_type["type_"] = "action"
#        element_type["name"] = ""
#        MapRuleWindowHandler.element_types["action"] = ("action", element_type)

def get_restricted_action_ids(restriction_type="Zone"):
    with database.QuerySession() as dbs:
        restrictions = dbs.select(model.RuleRestriction, filter=database.and_(model.RuleRestriction.restriction_type==restriction_type))
        return [r.rule_id for r in restrictions]



def get_restriction_elements(element, restriction_type):
    with database.QuerySession() as dbs:
        action_id = element.get_id()
        restrictions = dbs.select(model.RuleRestriction, filter=database.and_(model.RuleRestriction.restriction_type==restriction_type,
                                                                               model.RuleRestriction.rule_id==action_id))
        restriction_ids = [r.restriction_rule_id for r in restrictions]

        restriction_elements = dbs.select(model.RuleObject, database.in_(model.RuleObject.id, restriction_ids))

        return [CriteriaElement(e) for e in restriction_elements]

def update_restriction_elements(element, restriction_type, new_elements):
    with database.Transaction() as dbt:
        action_id = element.get_id()
        new_restriction_ids = [int(e.get_id()) for e in new_elements]
        restrictions = dbt.select(model.RuleRestriction, filter=database.and_(model.RuleRestriction.restriction_type==restriction_type,
                                                                               model.RuleRestriction.rule_id==action_id))
        for restriction in restrictions:
            if restriction.restriction_rule_id in new_restriction_ids:
                new_restriction_ids.remove(restriction.restriction_rule_id)
            else:
                dbt.delete(restriction)
        for restriction_rule_id in new_restriction_ids:
            restriction = dbt.add(model.RuleRestriction())
            restriction.rule_id = int(element.get_id())
            restriction.restriction_rule_id = restriction_rule_id
            restriction.restriction_type = restriction_type


        return True

