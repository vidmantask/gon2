from __future__ import with_statement
import unittest

from lib import giri_unittest
import rule_api
from components.auth.server_management import ElementType, RuleType, OperationNotAllowedException
import components.auth.server_common.database_schema
from components.database.server_common.connection_factory import ConnectionFactory
from components.database.server_common.database_api import Transaction, ReadonlySession
import components.auth.server_common.auth_graph_model as model

import lib.checkpoint
import random
from components.database.server_common import database_api

global_count = 1

class ActivityLogCreatorStub():

    def __init__(self):
        pass
        
    def save(self, **kwargs):
        pass
        
    def addValues(self, **kwargs):
        pass
    
    def copy(self):
        return self
    
    def copyUpdateAndAdd(self, dbt, **kwargs):
        pass


_test_rule_types = {
                 u"TestRule" : dict( result_class=u"TestResult", conditions = [u"TestElement1", u"TestElement2", u"TestElement3"] ),
                 u"TestRule1" : dict( result_class=u"TestAction", conditions = [u"TestElement1", u"TestResult"] ),

                 u"TestRule2" : dict( result_class=u"TestResult2", conditions = [u"TestComposite1", u"TestComposite2"] ),
                 u"TestRule3" : dict( result_class=u"TestResult3", conditions = [u"TestComposite3"] ),
                 u"TestRule4" : dict( result_class=u"TestResult4", conditions = [u"TestComposite4", u"TestElement1", u"TestComposite3"] ),
                 u"TestRule5" : dict( result_class=u"TestResult5", conditions = [u"TestComposite1", u"TestComposite2", u"TestComposite3", u"TestComposite4"],
                                      unique_conditions = [u"TestComposite1"], 
                                      mandatory_conditions = [[u"TestComposite2", u"TestComposite3"], [ u"TestComposite2",  u"TestComposite4"]],
                                       ),
                 
                 
              }


_test_element_types = { 

#                    
#                 u"KeyAssignment" : dict( result_class=u"KeyAssignment", conditions = [u"Key", u"User", u"Group"], unique_conditions = [u"Key"], 
#                                          mandatory_conditions = [[u"Key", u"User"], [ u"Key",  u"Group"]], title=u"Rules for Auth. Strength", long_title=u"Rules for Authentication Strength" ),
#                 u"SecurityRule" : dict( result_class=u"ApplicationAccess", conditions = [u"User", u"Group"], title=u"Authorization Rules", long_title=u"Application Authorization Rules"  ),
#
#                 u"AccessRule" : dict( result_class=u"Test0", conditions = [u"TestComp1", u"TestComp2"], title=u"Authorization Rules", long_title=u"Application Authorization Rules"  ),
#                    
                    u"TestResult" : dict( title=u"Test", type_=u"rule", info=u"TestInfo"),
                    u"TestAction" : dict( title=u"Test1", type_=u"action",info=u"TestInfo1"),

                    u"TestResult2" : dict( title=u"Test2", type_=u"association", info=u"TestInfo"),
                    u"TestResult3" : dict( title=u"Test2", type_=u"group", info=u"TestInfo"),
                    u"TestResult4" : dict( title=u"Test2", type_=u"rule", info=u"TestInfo"),
                    u"TestResult5" : dict( title=u"Test2", type_=u"rule", info=u"TestInfo"),
                    u"TestResult6" : dict( title=u"Test2", type_=u"rule", info=u"TestInfo"),
                    
                    u"TestElement1" : dict( title=u"TestElement1", type_=u"module", 
                                            plugin=u"test_plugin1", internal_element_type = u"testelement1"),
                    u"TestElement2" : dict( title=u"TestElement2", type_=u"module", 
                                            plugin=u"test_plugin2",  internal_element_type = u"testelement1"),
                    u"TestElement3" : dict( title=u"TestElement3", type_=u"module", 
                                            plugin=u"test_plugin3",  internal_element_type = u"testelement3"),
                    u"TestElement4" : dict( title=u"TestElement4", type_=u"module", 
                                            plugin=u"test_plugin4",  internal_element_type = u"testelement4"),
                    
                    u"TestComposite1" : dict( title=u"TestElement 3 and 4", type_=u"composite", 
                                             elements = ["TestElement3", "TestElement4"]),

                    u"TestComposite2" : dict( title=u"Class composite", type_=u"composite", 
                                             elements = ["TestResult", "TestResult2"]),

                    u"TestComposite3" : dict( title=u"Mix composite", type_=u"composite", 
                                             elements = ["TestResult", "TestElement2"]),

                    u"TestComposite4" : dict( title=u"Multi level composite", type_=u"composite", 
                                             elements = ["TestComposite1", "TestComposite3", "TestElement1"]),
                    #u"TstElement4" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement1", u"TestElement2", u"TestElement3"]),
                    #u"TestElement3" : dict( title=u"TestElement4", type_=u"multi", element_types=[u"TestElement4", u"TestElement5"]),
                    #u"TestElement4" : dict( title=u"TestElement4", type_=u"module", name=u"test_plugin4",  predicate=u"test4", parameter_name = u"test4"),
                    #u"TestElement5" : dict( title=u"TestElement5", type_=u"module", name=u"test_plugin5",  predicate=u"test5", parameter_name = u"test5"),
                 }

rule_api.def_element_types.update(_test_element_types)
rule_api.def_rule_types.update(_test_rule_types)

    

def _get_non_empty_conditions(conditions):
    return_val = []
    for c in conditions:
        if c.get_id():
            return_val.append(c)
    return return_val


class TestModuleElementType(ElementType):
    
    
    def __init__(self, element_type_name, id=None):
        if not id:
            global global_count
            self.id = global_count
            global_count += 1
        else:
            self.id = id
        self.element_type_name = "%s.%s" % (element_type_name, element_type_name)
        element_type_def = rule_api.MapRuleWindowHandler.element_types[element_type_name][1]
        self.element_type = element_type_def["type_"]
        if not id and self.element_type=="module":
            plugin_name = element_type_def["plugin"]
            self.id = "%s.%s" % (plugin_name, self.id)
        self.label = "label%s" % self.id

    def get_id(self):
        return self.id
    
    def get_entity_type(self):
        return self.element_type_name

    def get_element_type(self):
        return self.element_type
    
    def get_info(self):
        return "info"
    
    def get_label(self):
        return self.label
    
class TestRuleType(RuleType):
    
    def __init__(self, rule_type, pluginSocketElement, add_condtions=True):
        self.id = -1
        if rule_type:
            rule_type_def = rule_api.def_rule_types[rule_type]
            result_element = TestModuleElementType(rule_type_def.get("result_class"))
            if result_element.get_element_type()=="action":
                self.result_element = rule_api.create_action_criteria(result_element.get_label(), False)
            else:
                self.result_element = rule_api.create_criteria(result_element)
            if add_condtions:
                self.conditions = [pluginSocketElement.create_element(element_name) for element_name in rule_type_def.get("conditions") ]
            else:
                self.conditions = []
        else:
            self.conditions = []
            self.result_element = None
            

    def get_id(self):
        return self.id

    def get_result_element(self):
        return self.result_element


    def set_result_element(self, result_element):
        self.result_element = result_element
        
    def get_condition_elements(self):
        return self.conditions
    
    def is_active(self):
        return True
    
def create_class_element(rule_type, element_type):
    result_element = TestModuleElementType(element_type)
    if result_element.get_element_type()=="action":
        element = rule_api.create_action_criteria(result_element.get_label(), False)
    else:
        element = rule_api.create_criteria(result_element)
    
    element.entity_type = "%s.%s" % (rule_type, element_type)
    return element
    

class PluginSocketElementStub(object):
    
    def __init__(self):
        self.elements = dict()
    
    def get_module_header_element(self, element_type):
        pass

    def get_module_elements(self, element_type, search_filter=None):
        pass

    def create_element(self, element_name):
        element = TestModuleElementType(element_name)
        self.elements[element.get_id()]  = element
        return element

    def create_composite_element(self, rule_type, element_type):
        element = TestModuleElementType(element_type)
        element.element_type_name = "%s.%s" % (rule_type, element_type)
        self.elements[element.get_id()]  = element
        return element
    
class UserAdminStub(object):

    def __init__(self):
        pass

rule_type_name = "TestRule"
rule_type_name1 = "TestRule1"

class RuleAPITest(unittest.TestCase):

    connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', False)
    #connection = ConnectionFactory(None).create_connection('sqlite:///./unittest_rule_api.sqlite', True)
    components.auth.server_common.database_schema.connect_to_database(connection)
    
    handler = None 
    handler1 = None 
    pluginSocketElement = None
    
    global_setup = False

    def dummy(self):
        h = rule_api.MapRuleWindowHandler()
        
        
    def delete_all_rules(self):
        for rule_type in _test_rule_types.keys():
            rule_handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, rule_type, ActivityLogCreatorStub())
            for rule in rule_handler.get_rules():
                rule_handler.delete_rule(rule)
        
        
    def setUp(self):
        self.connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', False)
        #self.connection = ConnectionFactory(None).create_connection('sqlite:///./unittest_rule_api.sqlite', True)
        #self.connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)
        #self.connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/m2test', False)

        self.pluginSocketElement = PluginSocketElementStub()
        self.user_admin = UserAdminStub()
        rule_api._init_element_types()
        self.handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, rule_type_name, ActivityLogCreatorStub())
        self.handler1 = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, rule_type_name1, ActivityLogCreatorStub())
        


    def test_element_crud(self):

        # create element
        rule_type_def = rule_api.def_rule_types[rule_type_name]
        element = TestModuleElementType(rule_type_def.get("result_class"))
        new_element = rule_api.create_criteria(element)

        # create element with empty label
        rule_type_def = rule_api.def_rule_types[rule_type_name]
        element = TestModuleElementType(rule_type_def.get("result_class"))
        element.label = ""
        self.assertRaises(OperationNotAllowedException, rule_api.create_criteria, element)
        
        # create element that already exists
        label = new_element.get_label()
        element = TestModuleElementType(rule_type_def.get("result_class"))
        element.label = label
        self.assertRaises(OperationNotAllowedException, rule_api.create_criteria, element)
        
        # update element
        old_label = new_element.get_label()
        element = TestModuleElementType(rule_type_def.get("result_class"))
        element.id = new_element.get_id()
        new_label = element.get_label()
        self.assertNotEqual(old_label, new_label)
        new_element = rule_api.update_criteria(element.get_id(), element.get_label())
        
        
        # update element with label of already existing element
        rule_type_def = rule_api.def_rule_types[rule_type_name]
        other_element = TestModuleElementType(rule_type_def.get("result_class"))
        other_new_element = rule_api.create_criteria(other_element)

        element = TestModuleElementType(rule_type_def.get("result_class"))
        element.id = new_element.get_id()
        element.label = other_new_element.get_label()
        self.assertRaises(OperationNotAllowedException, rule_api.update_criteria, element.get_id(), element.get_label())
        
        # delete element
        id = other_new_element.get_id()
        db_session = ReadonlySession()
        class_criteria = db_session.get(model.RuleCriteria, id)
        self.assertNotEquals(class_criteria, None)
        db_session.close()

        ok = rule_api.delete_criteria(other_new_element.get_id())
        self.assertEquals(ok, True)
        
        db_session = ReadonlySession()
        class_criteria = db_session.get(model.RuleCriteria, id)
        self.assertEquals(class_criteria, None)
        db_session.close()
        
        # delete element that doesn't exist
        ok = rule_api.delete_criteria(other_new_element.get_id())
        self.assertEquals(ok, True)
        
        # delete element used in a rule
        rule = TestRuleType(rule_type_name1, self.pluginSocketElement)
        action = rule.get_result_element()
        new_element.entity_type = "%s.%s" % (new_element.entity_type, new_element.entity_type)
        rule.conditions[1] = new_element
        rule = self.handler1.add_rule(rule)
        self.assertRaises(OperationNotAllowedException,rule_api.delete_criteria, new_element.get_id())
        
        
        # update action
        action_copy = TestModuleElementType("TestAction", action.get_id())
        action_copy.label = "Yo"
        action = rule_api.update_criteria(action_copy.get_id(), action_copy.get_label())
        self.assertEquals(action_copy.get_label(), "Yo")

        # update action with label already used
        other_action = rule_api.create_action_criteria("Action", False)
        action_copy.label = other_action.get_label()
        self.assertRaises(OperationNotAllowedException,rule_api.update_criteria, action_copy.get_id(), action_copy.get_label())
        
        # delete action with rules assigned
        self.assertRaises(OperationNotAllowedException,rule_api.delete_criteria, action.get_id())
        
        # delete element and action in deleted rule
        self.handler1.delete_rule(rule)
        rule_api.delete_criteria(new_element.get_id())
        rule_api.delete_criteria(action.get_id())


        # delete element with rules assigned
        rule = TestRuleType(rule_type_name, self.pluginSocketElement)
        rule = self.handler.add_rule(rule)
        tmp_element = rule.get_result_element()
        self.assertRaises(OperationNotAllowedException,rule_api.delete_criteria, tmp_element.get_id())

    
    def test_crud(self):
        
        self.delete_all_rules()
        
        # all conditions
        rule = TestRuleType(rule_type_name, self.pluginSocketElement)
        rule = self.handler.add_rule(rule)
        result = rule.get_result_element()
        conditions = rule.get_condition_elements()
        self.assertEqual(len(conditions),3)
        
        # some conditons
        rule1 = TestRuleType(rule_type_name, self.pluginSocketElement)
        rule1.conditions.pop()
        rule1 = self.handler.add_rule(rule1)
        result = rule1.get_result_element()
        conditions = _get_non_empty_conditions(rule1.get_condition_elements())
        self.assertEqual(len(conditions),2)
        
        # no conditons
        rule2 = TestRuleType(rule_type_name, self.pluginSocketElement)
        rule2.conditions = []
        rule2 = self.handler.add_rule(rule2)
        result = rule2.get_result_element()
        conditions = _get_non_empty_conditions(rule2.get_condition_elements())
        self.assertEqual(len(conditions),0)
        
        # conditons in wrong order
        rule3 = TestRuleType(rule_type_name, self.pluginSocketElement)
        rule3.conditions.reverse()
        rule3 = self.handler.add_rule(rule3)
        result = rule3.get_result_element()
        conditions = rule3.get_condition_elements()
        self.assertEqual(len(conditions),3)
        self.assertEqual(conditions[0].get_entity_type(), "TestElement1.TestElement1")
        
        # update - add condition
        rule2.conditions[2] = rule.conditions[2]
        entity_type = rule2.conditions[2].get_entity_type() 
        rule2 = self.handler.update_rule(None, rule2)
        conditions = _get_non_empty_conditions(rule2.get_condition_elements())
        self.assertEqual(len(conditions),1)
        self.assertEqual(conditions[0].get_entity_type(), entity_type)


        # update - remove condition
        rule3.conditions.pop()
        rule3 = self.handler.update_rule(None, rule3)
        conditions = _get_non_empty_conditions(rule3.get_condition_elements())
        self.assertEqual(len(conditions),2)

        # update - remove and add condition
        rule3.conditions.pop()
        rule3.conditions.pop()
        rule3.conditions.append(rule.conditions[2])
        element3_entity_type = rule.conditions[2].get_entity_type()
        rule3 = self.handler.update_rule(None, rule3)
        conditions = _get_non_empty_conditions(rule3.get_condition_elements())
        self.assertEqual(len(conditions),2)
        conditions = rule3.get_condition_elements()
        self.assertEqual(conditions[0].get_entity_type(), "TestElement1.TestElement1")
        self.assertEqual(conditions[2].get_entity_type(), element3_entity_type)


        #update - change module condition
        conditions = rule3.get_condition_elements()
        old_condition = conditions[0]
        new_condition = rule.get_condition_elements()[0]
        conditions[0] = new_condition
        self.assertEqual(new_condition.get_entity_type(), old_condition.get_entity_type())
        self.assertNotEqual(new_condition.get_label(), old_condition.get_label())
        rule3 = self.handler.update_rule(None, rule3)
        self.assertEqual(new_condition.get_label(), rule3.get_condition_elements()[0].get_label())
        
        #update - change multiple modules condition
        element_type_def = rule_api.MapRuleWindowHandler.element_types["TestElement3"][1]
        element3condition = rule3.get_condition_elements()[2]
        entity_type_prefix, module_name = element3condition.split_entity_type()

        module_name = element_type_def["plugin"]
        entity_type = ".".join([entity_type_prefix, entity_type_prefix])
            
        new_element = TestModuleElementType(entity_type_prefix)
        #new_element.element_type_name = entity_type
        #new_element.id = element3condition.get_id()
        rule3.conditions[2] = new_element
        rule3 = self.handler.update_rule(None, rule3)
        self.assertEquals(rule3.get_condition_elements()[2].get_entity_type(), entity_type)
        self.assertEqual(rule3.get_condition_elements()[2].get_label(), new_element.get_label())
        
        #update - change class condition
        action_rule = TestRuleType(rule_type_name1, self.pluginSocketElement)
        result_element = rule1.get_result_element()
        result_element.entity_type = "%s.%s" % (result_element.entity_type, result_element.entity_type)
        action_rule.conditions[1] = result_element
        action_rule = self.handler1.add_rule(action_rule)
        conditions = action_rule.get_condition_elements()
        old_condition = conditions[1]
        new_condition = rule.get_result_element()
        self.assertEqual(new_condition.get_entity_type().rpartition(".")[2], old_condition.get_entity_type().rpartition(".")[2])
        self.assertNotEqual(new_condition.get_label(), old_condition.get_label())
        conditions[1] = new_condition
        new_condition.entity_type = "%s.%s" % (new_condition.entity_type, new_condition.entity_type)
        action_rule = self.handler1.update_rule(None, action_rule)
        old_condition = conditions[1]
        self.assertEqual(new_condition.get_entity_type(), old_condition.get_entity_type())
        self.assertEqual(new_condition.get_label(), old_condition.get_label())
        
        
        
        
        #update - change result
        rule_type_def = rule_api.def_rule_types[rule_type_name]
        result_element = TestModuleElementType(rule_type_def.get("result_class"))
        label = result_element.get_label()
        self.assertNotEqual(label, rule.get_result_element().get_label())
        new_result = rule_api.create_criteria(result_element)
        rule.set_result_element(new_result)
        rule = self.handler.update_rule(None, rule)
        self.assertEqual(label, rule.get_result_element().get_label())

        #delete rule
        db_session = ReadonlySession()
        class_criteria = db_session.get(model.RuleCriteria, rule1.get_id())
        self.assertNotEqual(class_criteria,None)
        db_session.close()
        self.handler.delete_rule(rule1)
        db_session = ReadonlySession()
        class_criteria = db_session.get(model.RuleCriteria, rule1.get_id())
        self.assertEquals(class_criteria, None)
        db_session.close()
        
        #delete deleted rule
        self.handler.delete_rule(rule1)
        
        #delete all rules
        self.handler.delete_rule(rule)
        self.handler.delete_rule(rule2)
        self.handler.delete_rule(rule3)
        self.handler1.delete_rule(action_rule)
        db_session = ReadonlySession()
        class_criteria = db_session.select(model.RuleCriteria, 
                                           database_api.in_(model.RuleCriteria.internal_type_name,[model.RuleCriteria.TYPE_ASSSOCIATION_CLAUSE,
                                                                                                   model.RuleCriteria.TYPE_RULE_CLAUSE]))
        self.assertEqual(len(class_criteria),0)


    def test_composite_rules(self):

        rule_handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, "TestRule2", ActivityLogCreatorStub())
        
        rule = TestRuleType("TestRule2", self.pluginSocketElement, False)
        rule_result = rule.result_element
        
        # Create rule 
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite1", "TestElement3"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),1)
        
        # Update with other composite
        new_rule = TestRuleType(None, self.pluginSocketElement)
        new_rule.result_element = rule_result
        new_rule.id = rule.get_id()
        new_rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite1", "TestElement4"))
        rule_handler.update_rule(rule, new_rule)
        
        # Two conditions
        rule = TestRuleType("TestRule2", self.pluginSocketElement, False)
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite1", "TestElement3"))
        rule.conditions.append(create_class_element("TestComposite2", "TestResult"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),2)

        rule.conditions.pop()
        
        # delete fail
        self.assertRaises(OperationNotAllowedException, rule_handler.delete_rule, rule)

        # add and remove condition
        rule.conditions.pop()
        rule.conditions.append(create_class_element("TestComposite2", "TestResult"))
        rule = rule_handler.update_rule(None, rule)
        
        
        # delete
        rule_handler.delete_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),1)
        
        

    def test_composite_rules1(self):

        
        rule_handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, "TestRule3", ActivityLogCreatorStub())
        
        rule = TestRuleType("TestRule3", self.pluginSocketElement, False)
        rule_result = rule.result_element
        
        # Create rule 
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite3", "TestElement2"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),1)
        
        # Update with other composite
        new_rule = TestRuleType(None, self.pluginSocketElement)
        new_rule.result_element = rule_result
        new_rule.id = rule.get_id()
        new_rule.conditions.append(create_class_element("TestComposite3", "TestResult"))
        rule_handler.update_rule(rule, new_rule)

        rule.conditions.pop()
        
        # delete fail
        self.assertRaises(OperationNotAllowedException, rule_handler.delete_rule, rule)

        # remove condition
        rule = rule_handler.update_rule(None, rule)
        
        # delete
        rule_handler.delete_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),0)
        

    def test_composite_rules2(self):

        rule_handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, "TestRule4", ActivityLogCreatorStub())
        
        rule = TestRuleType("TestRule4", self.pluginSocketElement, False)
        rule_result = rule.result_element
        
        # Create rule 
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite4", "TestElement3"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),1)
        
        # Update with other composite
        new_rule = TestRuleType(None, self.pluginSocketElement)
        new_rule.result_element = rule_result
        new_rule.id = rule.get_id()
        new_rule.conditions.append(create_class_element("TestComposite4", "TestResult"))
        rule_handler.update_rule(rule, new_rule)

        # Two conditions
        rule = TestRuleType("TestRule4", self.pluginSocketElement, False)
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite4", "TestElement1"))
        rule.conditions.append(self.pluginSocketElement.create_element("TestElement1"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),2)
        
        self.assertEqual(len(rule.conditions),3)
        self.assertEqual(rule.conditions[0].get_entity_type(), "TestComposite4.TestElement1")
        self.assertEqual(rule.conditions[1].get_entity_type(), "TestElement1.TestElement1")
        
        # Three conditions
        rule = TestRuleType("TestRule4", self.pluginSocketElement, False)
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite4", "TestElement3"))
        rule.conditions.append(self.pluginSocketElement.create_element("TestElement1"))
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite3", "TestElement2"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),3)
        
        self.assertEqual(len(rule.conditions),3)
        
        # Three conditions - shared class criteria
        element = create_class_element("TestComposite4", "TestResult")
        element_def = rule_api.get_element_type_dict("TestResult")
        elements = rule_api.get_criteria_elements(element_def)
        element_copy = None
        for e in elements:
            if e.get_id()==element.get_id():
                element_copy = e
                break
        
        self.assertFalse(element_copy is None)
        element_copy.entity_type = "TestComposite3.TestResult"

        rule = TestRuleType("TestRule4", self.pluginSocketElement, False)
        rule.conditions.append(element)
        rule.conditions.append(element_copy)
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),4)
        
        self.assertEqual(len(rule.conditions),3)
        self.assertEqual(rule.conditions[0].get_id(), rule.conditions[2].get_id())
        
        # delete all
        for rule in rule_handler.get_rules():
            rule_handler.delete_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),0)

    def test_composite_rules3(self):

        rule_handler = rule_api.create_rule_handler(giri_unittest.get_checkpoint_handler(), self.user_admin, "TestRule5", ActivityLogCreatorStub())
        
        rule = TestRuleType("TestRule5", self.pluginSocketElement, False)
        rule_result = rule.result_element
        
        # Create rule 
        self.assertRaises(OperationNotAllowedException, rule_handler.add_rule, rule)

        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite3", "TestElement2"))
        self.assertRaises(OperationNotAllowedException, rule_handler.add_rule, rule)

        rule.conditions.append(create_class_element("TestComposite2", "TestResult"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),1)
        
        
        element = self.pluginSocketElement.create_composite_element("TestComposite1", "TestElement3")
        rule.conditions[0] = element
        rule = rule_handler.update_rule(None, rule)

        element_copy = TestModuleElementType("TestElement3", element.get_id())
        element_copy.element_type_name = "TestComposite1.TestElement3"
        result_element = rule.get_result_element()
        
        rule = TestRuleType("TestRule5", self.pluginSocketElement, False)
        rule.result_element = result_element
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite3", "TestElement2"))
        rule.conditions.append(create_class_element("TestComposite2", "TestResult"))
        rule.conditions.append(element_copy)
        self.assertRaises(OperationNotAllowedException, rule_handler.add_rule, rule)
        
        rule = TestRuleType("TestRule5", self.pluginSocketElement, False)
        rule.result_element = result_element
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite3", "TestElement2"))
        rule.conditions.append(create_class_element("TestComposite2", "TestResult"))
        rule.conditions.append(self.pluginSocketElement.create_composite_element("TestComposite1", "TestElement3"))
        rule = rule_handler.add_rule(rule)
        self.assertEqual(len(rule_handler.get_rules()),2)
    
        rule.conditions[0] = element_copy
        self.assertRaises(OperationNotAllowedException, rule_handler.update_rule, None, rule)
        
        old_conditions = rule.conditions
        
        rule.conditions = []
        rule.conditions.append(old_conditions[2])
        self.assertRaises(OperationNotAllowedException, rule_handler.update_rule, None, rule)

        rule.conditions = []
        rule.conditions.append(old_conditions[1])
        self.assertRaises(OperationNotAllowedException, rule_handler.update_rule, None, rule)

        rule.conditions.append(create_class_element("TestComposite4", "TestResult"))
        rule_handler.update_rule(None, rule)
        

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
