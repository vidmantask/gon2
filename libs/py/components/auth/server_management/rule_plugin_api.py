'''
  Part of rule api exposed to plugins. How plugins get this api has not been resolved - now they just call directly 
'''
import rule_api

def is_element_used(element, db_session=None):
    '''
        Check if element is used in a rule
    '''
    return rule_api.is_element_used(element, db_session)


def element_updated(element, transaction=None):
    '''
        Reports that an element has been updated. The element id is assumed to be the same
    '''
    return rule_api.element_updated(element, transaction)