"""The database model and the interface and object-persistense to the database"""

import components.auth.server_common.database_schema as schema

from components.database.server_common.schema_api import Relation, mapper
from components.auth.server_management import OperationNotAllowedException

import components.database.server_common.database_api as database_api 


import lib.checkpoint 


                
    

class RuleObject(object):

    """
    Base class for all autograph nodes
    """
    

    (AllOf,  # and, universal quantifier
     OneOf,  # or, existential quantifier
     NoneOf,  # and not, non-existential quantifier / universal non-quantifier
     ) = range(3)

    ''' Internal type values '''
    
    TYPE_CONDITION = "Condition"
    TYPE_ACTION = "Action"
    TYPE_RULE = "Rule"
    TYPE_RULE_CLAUSE = "RuleClause"
    TYPE_ASSSOCIATION = "Association"
    TYPE_ASSSOCIATION_CLAUSE = "AssociationClause"
    TYPE_GROUP = "Group"
    TYPE_ADMIN_ACCESS = "AdminAccess"


    def __str__(self):
        if self.title:
            return self.title
        return "unknown"
    
    def update_title(self, title):
        self.title = title
    
    def add_criteria_value(self, transaction, entity_type, value_id, value_title, rule_entity_type=None):
        existing = transaction.select(RuleObject, database_api.and_(RuleObject.value_id==value_id,
                                                                   RuleObject.entity_type==entity_type))
        
        if existing:
            c = existing[0]
        else:
            c = transaction.add(RuleObject())
            c.entity_type = entity_type
            c.internal_type_name = "Condition"
            c.value_id = value_id
            c.title = value_title
            transaction.flush()
        a = transaction.add(RuleAssociation())
        a.parent_id = self.id
        a.parent_internal_type_name = self.internal_type_name
        a.child_id = c.id
        a.child_internal_type_name = c.internal_type_name
        a.rule_entity_type = rule_entity_type
        return a
    
    @classmethod
    def get_internal_type_name(self, rule_object):
        if isinstance(rule_object, Group):
            return self.TYPE_GROUP
        elif isinstance(rule_object, Association):
            return self.TYPE_ASSSOCIATION
        elif isinstance(rule_object, AssociationClause):
            return self.TYPE_ASSSOCIATION_CLAUSE
        elif isinstance(rule_object, Rule):
            return self.TYPE_RULE
        elif isinstance(rule_object, RuleClause):
            return self.TYPE_RULE_CLAUSE
        elif isinstance(rule_object, Action):
            return self.TYPE_ACTION
        elif isinstance(rule_object, Condition):
            return self.TYPE_CONDITION
        elif isinstance(rule_object, AdminAccess):
            return self.TYPE_ADMIN_ACCESS
        else:
            raise Exception("Unknown rule type '%s'" % rule_object.__class__)
        
    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==result_id:
            return True
        for parent in self.get_parents():
            if parent.is_in_rule(result_id, exclude_rule_id):
                return True
        return False

    def add_sub_criteria(self, criteria, rule_entity_type=None, deactivated=False):
        a = RuleAssociation()
        a.parent_criteria = self
        if self.internal_type_name:
            a.parent_internal_type_name = self.internal_type_name
        else:
            a.parent_internal_type_name = self.get_internal_type_name(self)
        a.child_criteria = criteria
        if criteria.internal_type_name:
            a.child_internal_type_name = criteria.internal_type_name
        else:
            a.child_internal_type_name = self.get_internal_type_name(criteria)
        a.rule_entity_type = rule_entity_type
        a.deactivated = deactivated
        self.child_criterias.append(a)

    def delete(self, transaction):
        if self.child_criterias:
            raise OperationNotAllowedException("One or more rules exists for this element")
        transaction.delete(self)

    def get_id(self):
        return "%d" % self.id

    def get_parents(self):
        return [a.parent_criteria for a in self.parent_criterias]
    

    def get_children(self):
        return [a.child_criteria for a in self.child_criterias]
        
    def get_entity_type_and_children(self):
        return [(a.rule_entity_type, a.child_criteria) for a in self.child_criterias]


    def is_last_connection(self, rule_association):
        return False
            
    def set_true_bottom_up(self, db_session, nodes):
        if self.id in nodes:
            return
        nodes.add(self.id)
#        print self.title
        global count
        count += 1
        nodes.add(self.id)
        if self.aggregation_kind == RuleObject.AllOf:
            children = self.get_sub_criteria(db_session)
            children.extend(self.get_conditions(db_session))
            for child in children:
                b = child.id in nodes
#        with checkpoint_handler_demo.CheckpointScope("RuleCriteria.get_parents", "get_parents", lib.checkpoint.INFO, this=self.title):
        parents = self.get_parents(db_session)
        for parent in parents:
            parent.set_true_bottom_up(db_session, nodes)
        
        

        

        
     
#    def __init__(self, **kwargs):
#        self.aggregation_kind = self.OneOf

#    @reconstructor
#    def __db__init__(self):
#        self.calc_result = None


    def get_entity_type(self):
        return self.entity_type


    
    def description(self):
        f =  {self.AllOf: "All Of", 
             self.OneOf: "One Of", 
             self.NoneOf: "None Of",
             }.get(self.aggregation_kind, None)
        prefix = ""
#        if self.internal_type == self.RuleClass:
#            prefix = "RuleClass"
#        elif self.internal_type == self.Action:
#            prefix = "Action"
#        elif self.internal_type == self.Simple:
#            prefix = "Simple"
#        elif self.internal_type == self.WindowRules:
#            prefix = "WindowRules"
#        elif self.internal_type == self.NonStandard:
#            prefix = "NonStandard"
        return "%s %s %s" % (prefix, self.internal_type_name, f)
        


class Action(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf
        
    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)

    def delete(self, transaction):
        for child_edge in self.child_criterias:
            child_edge.child_criteria.delete(transaction)
            transaction.delete(child_edge)
        transaction.delete(self)
        

        

class Condition(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = None
    
    def is_last_connection(self, rule_association):
        if len(self.parent_criterias)==1 and self.parent_criterias[0].id == rule_association.id:
            return True 
        return False

    

class Rule(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf
        
    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)
    
        
    def get_new_clause_criteria(self, transaction, activated):
        clause_criteria = RuleClause()
        clause_criteria.title = self.title
        self.add_sub_criteria(clause_criteria, deactivated = not activated)
        return clause_criteria
    


class RuleClause(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.AllOf

    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==exclude_rule_id:
            return False
        return RuleObject.is_in_rule(self, result_id, exclude_rule_id)
        

class Association(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf

    def update_title(self, title):
        self.title = title
        for child in self.get_children():
            child.update_title(title)

    def get_new_clause_criteria(self, transaction, activated):
        clause_criteria = AssociationClause()
        clause_criteria.title = self.title
        self.add_sub_criteria(clause_criteria, deactivated = not activated)
        return clause_criteria



class AssociationClause(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.AllOf

    def is_in_rule(self, result_id, exclude_rule_id):
        if self.id==exclude_rule_id:
            return False
        return RuleObject.is_in_rule(self, result_id, exclude_rule_id)

class Group(RuleObject):
    
    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf

    def get_new_clause_criteria(self, transaction, activated):
        a = RuleAssociation()
        a.parent_criteria = self
        a.parent_internal_type_name = self.TYPE_GROUP
        # set dummy - otherwise error if flush issued
        a.child_criteria = a.parent_criteria
        a.child_internal_type_name = a.parent_internal_type_name
        a.deactivated = not activated
        return a
    
    def delete(self, transaction):
        if self.child_criterias:
            raise OperationNotAllowedException("The group is not empty")
        transaction.delete(self)
    
    def is_in_rule(self, result_id, exclude_rule_id):
        return False
    
    def has_member(self, member_id, exclude_rule_id):
        for group_edge in self.child_criterias:
            if group_edge.id != exclude_rule_id and member_id == group_edge.child_id:
                return True

class RuleAssociation(object):

    def add_sub_criteria(self, criteria, rule_entity_type=None):
        self.child_criteria = criteria
        if criteria.internal_type_name:
            self.child_internal_type_name = criteria.internal_type_name
        else:
            self.child_internal_type_name = RuleObject.get_internal_type_name(criteria)
        self.rule_entity_type = rule_entity_type

    def delete(self, transaction):
        child_criteria = self.child_criteria
        if child_criteria.is_last_connection(self):
            transaction.delete(child_criteria)
        transaction.delete(self)

class DefaultRuleCriteria(object):
    pass        

class BuiltInCriteria(object):
    pass

class AccessRight(object):
    pass

class AdminAccess(RuleObject):

    def __init__(self, **kwargs):
        self.aggregation_kind = self.OneOf

class AdminAccessAttribute(object):
    pass        

class RuleRestriction(object):
    pass

class ElementLifetime(object):
    pass


mapper(RuleObject, schema.table_criteria, 
       polymorphic_identity = "BaseRule",
       polymorphic_on = schema.table_criteria.c.internal_type_name)

mapper(Action, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ACTION)
mapper(Condition, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_CONDITION)
mapper(Association, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ASSSOCIATION)
mapper(AssociationClause, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ASSSOCIATION_CLAUSE)
mapper(Rule, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_RULE)
mapper(RuleClause, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_RULE_CLAUSE)
mapper(Group, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_GROUP)

relations = [Relation(RuleObject, 'parent_criteria', backref_property_name = 'child_criterias',
                      primaryjoin=schema.table_sub_criteria.c.parent_id==schema.table_criteria.c.id),
             Relation(RuleObject, 'child_criteria', backref_property_name = 'parent_criterias',
                      primaryjoin=schema.table_sub_criteria.c.child_id==schema.table_criteria.c.id)
                      ]

mapper(RuleAssociation, schema.table_sub_criteria, relations=relations)

mapper(DefaultRuleCriteria, schema.table_default_rule_criteria, relations=[Relation(RuleObject, 'criteria', backref_property_name = 'default_in_rules')])
mapper(BuiltInCriteria, schema.table_built_in_criteria, relations=[Relation(RuleObject, 'criteria', backref_property_name = 'built_in')])

mapper(AccessRight, schema.table_access_right)
#mapper(AdminAccess, schema.table_access_element, relations=[Relation(Rule, 'rule')])
mapper(AdminAccess, None, inherits=RuleObject, polymorphic_identity=RuleObject.TYPE_ADMIN_ACCESS)
mapper(AdminAccessAttribute, schema.table_access_element_attribute, relations=[Relation(AccessRight, 'access_right', backref_property_name = 'access_given'),
                                                                               Relation(AdminAccess, 'access_element', backref_property_name = 'access_attributes')]) 

mapper(RuleRestriction, schema.table_rule_restriction)
mapper(ElementLifetime, schema.table_element_lifetime)

#mapper(cls, 
#       ServerSchema.table_aggregating_criteria, 
#       inherits=inherits_from,
#       polymorphic_identity = 'AggregatingCriteria', 
#       relations = relations)
#
#employee_mapper = mapper(Employee, employees_table, \
#    polymorphic_on=employees_table.c.type, polymorphic_identity='employee')
#manager_mapper = mapper(Manager, inherits=employee_mapper, polymorphic_identity='manager')
#engineer_mapper = mapper(Engineer, inherits=employee_mapper, polymorphic_identity='engineer')


checkpoint_handler_demo = None
