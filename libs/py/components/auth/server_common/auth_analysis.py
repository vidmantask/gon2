'''
Created on 19/01/2010

@author: peter
'''
import auth_graph_model as model
import rule_definitions

def get_criteria_report_element(criteria):
    while not criteria.entity_type:
        criteria = criteria.get_parent_criteria()
    return criteria

def get_criteria_report_info(criteria):
    element_type_def = rule_definitions.def_element_types.get(criteria.entity_type)
    if not element_type_def:
        return ("Unknown rule type id=%d" % criteria.id, criteria.title)
    else:
        return (element_type_def.get("title"), criteria.title)
    

def get_partial_result_criteria_info(criteria, results):
    return get_criteria_report_info(criteria)



class AuthorizationAnalysis(object):
    
    PASSED_CRITERIA_TYPE    = 0
    PASSED_ACTION_TYPE      = 1
    NO_REPLY_TYPE           = 2
    PARTIAL_RESULT_TYPE     = 3
    RESTRICTED_ACTION_TYPE  = 4
    
    
    def __init__(self, db_session, graph, restricted_actions):
        self.db_session = db_session
        self.graph = graph
        self.actions = []
        self.restricted_actions = restricted_actions
        self.reported_results = set()

        
        
    def analyse_result(self, passed):
        analysed_ids = set()
        partial_results = {} 
        for action in self.graph.get_actions():
            action.get_partial_results(partial_results, self.db_session)
        partial_results = [value for value in partial_results.values() if value]
        return partial_results
                    
        
        
    def get_result_report(self):
        passed_criteria = []
        for node in self.graph.get_nodes():
            if node.value: 
                passed_criteria.append(node)
        return passed_criteria
                
    def add_unknown_to_report(self, unknown, report):
        for c in unknown:
            report.append((AuthorizationAnalysis.NO_REPLY_TYPE, c))

    def add_passed_to_report(self, passed, report):
        unique_passed = set()
        for c in passed:
            info = c.get_report_info()
            if info: 
                if c.is_action():
                    if self.restricted_actions.has_key(c.id):
                        pass_type = AuthorizationAnalysis.RESTRICTED_ACTION_TYPE
                    else:
                        pass_type = AuthorizationAnalysis.PASSED_ACTION_TYPE
                    pass_type_text = "Menu Action"
                elif c.is_admin_access():
                    continue
                else:
                    element_type_def = rule_definitions.def_element_types.get(c.entity_type)
                    pass_type = AuthorizationAnalysis.PASSED_CRITERIA_TYPE
                    if not element_type_def:
                        pass_type_text = "Unknown Type"
                    else:
                        if element_type_def.get("type_") == "action":
                            continue
                        pass_type_text = element_type_def.get("title")
                unique_passed.add((pass_type, (pass_type_text, info)))
#            print "%s passed" % get_criteria_report_info(c)
        report.extend(unique_passed)
    
    def remove_duplicates(self, report_list):
        new_list = []
        for x in report_list:
            if not x in new_list:
                new_list.append(x)
        return new_list
                
    def create_failure_report(self):

        report = list()
        passed = self.get_result_report()
        self.add_passed_to_report(passed, report)

        partial_results = self.analyse_result(passed)
        action_results = dict()
        for (result, condition) in partial_results:
#            print "%s would pass if %s" % (get_criteria_report_info(result), get_partial_result_criteria_info(condition, self.calc_results))
#            report_criteria = get_criteria_report_element(result)
            if not result.entity_type:
                continue
            # Avoid report on multiple menu actions with same enabler - code could be improved...
            element_def = rule_definitions.def_element_types.get(result.entity_type)
            if element_def["type_"]=="action":
                other_actions = action_results.get(condition)
                info = ("Menu Action", result.title)
                if other_actions:
                    other_actions.add(info)
                else:
                    other_actions = set([info])
                action_results[condition] = other_actions
            else:
                report.append((AuthorizationAnalysis.PARTIAL_RESULT_TYPE, get_criteria_report_info(result), get_criteria_report_info(condition)))
        for (condition, action_set) in action_results.items():
            if len(action_set)==1:
                report.append((AuthorizationAnalysis.PARTIAL_RESULT_TYPE, action_set.pop(), get_criteria_report_info(condition)))
            else:
                report.append((AuthorizationAnalysis.PARTIAL_RESULT_TYPE, "multiple Menu Actions", get_criteria_report_info(condition)))
        
        return self.remove_duplicates(report)
                

    def create_auth_report(self):

        report = list()
        passed = self.get_result_report()
        self.add_passed_to_report(passed, report)

        filtered_report = []
        for item in report:
            if not item in self.reported_results:
                filtered_report.append(item)
                self.reported_results.add(item)
                
        return filtered_report
                
    
        
    