'''
Created on 11/11/2009

@author: peter
'''
def_rule_types = {
                 u"PersonalTokenAssignment" : dict( result_class=u"PersonalTokenAssignment", conditions = [u"Token", u"User"], unique_conditions = [u"Token"], 
                                          mandatory_conditions = [[u"Token", u"User"]],
                                          title=u"Personal Token Assignment", long_title=u"Personal Token Assignment" ),
#                 u"PersonalEndpointAssignment" : dict( result_class=u"PersonalEndpointAssignment", conditions = [u"Endpoint", u"User"], unique_conditions = [u"Endpoint"], 
#                                          mandatory_conditions = [[u"Endpoint", u"User"]], warn_if_single_condition=[u"Endpoint"],
#                                          title=u"Personal Devices", long_title=u"Personal Device Assignment" ),
                 u"TokenGroupMembership" : dict( result_class = u"TokenGroupMembership", 
                                                  conditions = [u"Token"], 
                                                  mandatory_conditions = [[u"Token"]], 
                                                  title = u"Token Groups", 
                                                  long_title = u"Token Group Membership" ),
#                 u"EndpointGroupMembership" : dict( result_class = u"EndpointGroupMembership", 
#                                                  conditions = [u"Endpoint"], 
#                                                  mandatory_conditions = [[u"Endpoint"]], 
#                                                  title = u"Computer Group", 
#                                                  long_title = u"Computer Group Membership" ),
                 u"AuthenticationStatus" : dict( result_class = u"AuthenticationStatus", 
                                                  conditions = [u"UserGroup", u"TokenGroup"], 
                                                  title = u"Authentication", 
                                                  long_title = u"User Authentication Policy"  ),
                 u"ProgramAccess" : dict( result_class=u"ProgramAccess", 
                                          conditions = [u"AuthenticationStatus", u"UserGroup", u"TokenGroupMembership"], 
                                          title = u"Authorization", 
                                          long_title = u"Action Authorization Policy"  ),
                 u"GOnUserGroup" : dict( result_class = u"GOnUserGroup", 
                                          conditions = [u"User", u"Group"],
                                          mandatory_conditions = [[u"User"], [u"Group"]], 
                                          title = u"G/On User Groups", 
                                          long_title = u"G/On User Group Membership"  ),
#                 u"Endpoint" : dict( result_class = u"Endpoint", 
#                                     conditions = [u"EndpointToken"], 
#                                          title = u"Devices", 
#                                          long_title = u"Devices"  ),

                 u"Zone" : dict( result_class = u"Zone", 
                                          conditions = [u"IpRange", u"SecurityStatus", u"LoginInterval"],
                                          title = u"Zone", 
                                          long_title = u"Zone Detection Rule",
                                          ),
                                          
                
                                          
                 u"AdminAccess" : dict( result_class = u"AdminAccess", 
                                          conditions = [u"UserOrGroup"],
                                          title = u"Management Role Assignment", 
                                          long_title = u"Management Role Assignment"  ),
                                          

#                 u"DeviceGroupMembership" :   dict( result_class = u"DeviceGroupMembership", 
#                                              conditions = [u"Device"],
#                                              mandatory_conditions = [[u"Device"]], 
#                                              title = u"Device Group Management", 
#                                              long_title = u"Device Group Management"  ),
              }


def_element_types = { 
                    u"User" : dict( title=u"User", 
                                    title_plural=u"Users", 
                                    rule_title = u"Users",
                                    type_=u"component",
                                    config_type=u"component",
                                    component_name=u"user", 
                                    internal_element_type=u"user",
                                    access_dependency = True
                                    ),
                    u"Group" : dict( title=u"Directory User Group", 
                                     title_plural=u"Directory User Groups", 
                                     rule_title = u"In User Group",
                                     type_=u"component",
                                     config_type=u"component",
                                     component_name=u"user", 
                                     access_dependency = u"User",
                                     internal_element_type=u"group"),

                    u"Token" :         dict( title=u"Token", title_plural=u"Tokens", 
                                             rule_title = u"Has Token",
                                           type_=u"composite", 
                                           access_dependency = True,
                                           elements = [u"SoftToken", u"MicroSmart", u"MicroSmartSwiss", u'MicroSmartSwissbit2', u'MicroSmartSwissbitPE', u"SmartCard", u'Endpoint', u'Mobile', u'Android'], 
                                         ),
                                         

                    u"UserGroup" :   dict( title=u"User Group", title_plural=u"User Groups", 
                                             rule_title = u"In User Group",
                                           type_=u"composite", 
                                           access_dependency = u"elements",
                                           elements = [u"Group", u"GOnUserGroup"], 
                                         ),
                                         
                    u"UserOrGroup" :   dict( title=u"User or Group", title_plural=u"Users/Groups", 
                                             rule_title = u"User/In User Group",
                                           type_=u"composite", 
                                           access_dependency = u"elements",
                                           elements = [u"User",  u"UserGroup"], 
                                         ),
                                         

                    u"TokenGroup" :   dict( title=u"Token Group", title_plural=u"Token Groups", 
                                             rule_title = u"In Token Group",
                                           type_=u"composite", 
                                           access_dependency = u"elements",
                                           elements = [u"PersonalTokenAssignment", u"TokenGroupMembership", u"DMEToken"], 
                                         ),

#                    u"EndpointGroup" :   dict( title=u"Device Group", title_plural=u"Device Groups", 
#                                             rule_title = u"In Device Group",
#                                           type_=u"composite", 
#                                           elements = [ u"PersonalEndpointAssignment", u"EndpointGroupMembership"], 
#                                         ),
                                     
                    u"SoftToken" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                         access_dependency = u"Token",
                                         config_type=u"module",
                                              type_=u"module", plugin=u"soft_token", internal_element_type = u"soft_token"),
                    u"MicroSmart" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                         access_dependency = u"Token",
                                         config_type=u"module",
                                                  type_=u"module", plugin=u"micro_smart", internal_element_type = u"micro_smart"),
                    u"MicroSmartSwiss" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                         access_dependency = u"Token",
                                         config_type=u"module",
                                                  type_=u"module", plugin=u"micro_smart_swissbit", internal_element_type = u"micro_smart_swissbit"),
                    u"MicroSmartSwissbit2" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                            access_dependency = u"Token", config_type=u"module",
                                            type_=u"module", plugin=u"micro_smart_swissbit_2", internal_element_type = u"micro_smart_swissbit_2"),
                    u"MicroSmartSwissbitPE" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                                     access_dependency = u"Token", config_type=u"module", type_=u"module", 
                                                     plugin=u"micro_smart_swissbit_pe", internal_element_type = u"micro_smart_swissbit_pe"),
                    u"SmartCard" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                         access_dependency = u"Token", config_type=u"module", type_=u"module", 
                                         plugin=u"smart_card", internal_element_type = u"smart_card"),
                     u"Hagiwara" : dict( title=u"Token", title_plural=u"Tokens", rule_title = u"Tokens",
                                                             access_dependency = u"Token",
                                                             config_type=u"module",
                                                            type_=u"module", plugin=u"hagiwara", internal_element_type = u"hagiwara"),
                    u"Mobile" : dict( title=u"Mobile", title_plural=u"Mobiles", rule_title = u"Mobiles",
                                         access_dependency = u"Token",
                                         config_type=u"module",
                                         type_=u"module", plugin=u"mobile", internal_element_type = u"ios"),
                    u"Android" : dict( title=u"Android mobile", title_plural=u"Android mobiles", rule_title = u"Android mobiles",
                                         access_dependency = u"Token",
                                         config_type=u"module",
                                         type_=u"module", plugin=u"mobile", internal_element_type = u"android"),
                    u"EndpointToken" : dict( title=u"Private/public key pair", title_plural=u"Private/public key pairs", rule_title = u"Private/public key pair",
                                         access_dependency = u"Endpoint",
                                         config_type=u"module",
                                        type_=u"module", plugin=u"endpoint_token", 
                                        internal_element_type = u"key"
                                        ),
                    u"DeviceToken" : dict( title=u"Private/public key pair", title_plural=u"Private/public key pairs", rule_title = u"Private/public key pair",
                                         access_dependency = u"Device",
                                         config_type=u"module",
                                        type_=u"module", plugin=u"computer_token", 
                                        internal_element_type = u"key"
                                        ),

                    u"DMEToken" : dict( title=u"DME Approved Device", title_plural=u"DME Approved Devices", rule_title = u"DME Approved Device",
                                         access_dependency = True,
                                         config_type=u"module",
                                        type_=u"module", plugin=u"dme", 
                                        internal_element_type = u"dme_token"
                                        ),

                    u"Zone" : dict( title=u"Zone", title_plural=u"Zones", rule_title = u"Zone",
                                      access_dependency = True,
                                       config_type=u"class",
                                       type_ = u"rule",
                                       description_caption = u"User Message",  
                                       
                                    ),



                    u"IpRange" : dict( title=u"IP Range", title_plural=u"IP Ranges", rule_title = u"IP Range",
                                      access_dependency = True,
                                       config_type=u"module",
                                        type_=u"module", plugin=u"ip_address", internal_element_type = u"address"),

                    u"SecurityStatus" : dict( title=u"Operating System State", title_plural=u"Operating System States", rule_title = u"Operating System State",
                                                access_dependency = True,
                                                config_type=u"module",
                                                type_=u"module", plugin=u"endpoint_security", 
                                                internal_element_type = u"security_element_id"),

                    u"LoginInterval" : dict( title=u"Login Interval", title_plural=u"Login Intervals", rule_title = u"Login Interval",
                                                access_dependency = True,
                                                config_type=u"module",
                                                type_=u"module", plugin=u"login_interval", internal_element_type = u"login_interval"),

                    u"PersonalTokenAssignment" : 
                        dict( title = u"Personal Token Status", 
                              title_plural = u"Personal Token Status", 
                              rule_title = u"Personal Token Status",
                              type_ = u"association",
                              config_type=u"class",
                              crud_enabled = False,
                              access_dependency = True,
                             ),
                             
                             
#                    u"DeviceGroupMembership" : 
#                        dict( title = u"Device Group", 
#                              title_plural = u"Device Groups", 
#                              rule_title = u"In Device Group",
#                              type_ = u"group",
#                              config_type=u"class",
#                              access_dependency = True,
#                             ),
                             
#                    u"PersonalEndpointAssignment" : 
#                        dict( title = u"Personal Device Status", 
#                              title_plural = u"Personal Device Status", 
#                              rule_title = u"Personal Device Status",
#                              type_ = u"class",
#                              crud_enabled = False,
#                             ),
                             
                    u"TokenGroupMembership" : 
                        dict( title = u"Token Group", 
                              title_plural = u"Token Groups", 
                              rule_title = u"In Token Group",
                              type_ = u"group",
                              config_type=u"class",
                              access_dependency = True,
                             ),
#                    u"EndpointGroupMembership" : 
#                        dict( title = u"Computer Group", 
#                              title_plural = u"Computer Groups", 
#                              rule_title = u"In Computer Group",
#                              type_ = u"group",
#                              config_type=u"class",
#                             ),
                    u"AuthenticationStatus" : 
                        dict( title = u"Authentication Status", 
                              title_plural = u"Authentication Status", 
                              rule_title = u"Authentication Status",
                              type_ = u"rule", 
                              config_type=u"class",
                              access_dependency = True,
                              ),
                    u"ComputerStatus" : 
                        dict( title = u"Computer Status", 
                              title_plural = u"Computer Status", 
                              rule_title = u"Computer Status",
                              type_ = u"rule", 
                              access_dependency = True,
                              config_type=u"class",
                              ),
                    u"GOnUserGroup" : 
                        dict( title = u"G/On User Group", 
                              title_plural = u"G/On User Groups", 
                              rule_title = u"In G/On User Group", 
                              type_ = u"group", 
                              access_dependency = True,
                              config_type=u"class",
                              ),
                    u"Endpoint" : 
                        dict( title = u"Computer User Token", 
                              title_plural = u"Computer User Tokens", 
                               type_=u"association",
                               config_type=u"component",
                               component_name=u"endpoint",
                               access_dependency = True,
                               internal_element_type=u"endpoint",
                              ),
#                    u"Device" : 
#                        dict( title = u"Device", 
#                              title_plural = u"Devices", 
#                               type_=u"component",
#                               config_type=u"component",
#                               component_name=u"endpoint",
#                               access_dependency = True,
#                               internal_element_type=u"device",
#                              ),
                    u"HardwareCheck" : 
                        dict( title = u"Hardware Check", 
                              title_plural = u"Hardware Checks", 
                               type_=u"component",
                               config_type=u"component",
                               component_name=u"endpoint",
                               access_dependency = "Endpoint",
                               internal_element_type=u"hardware_check",
                              ),
                    u"ProgramAccess" : 
                        dict( title = u"Menu Action", 
                              title_plural = u"Menu Actions", 
                              rule_title = u"Authorized Menu Action",
                              type_ = u"action", 
                              config_type=u"action",
                              component_name=u"traffic",
                              access_dependency = True,
                              internal_element_type = u"action", 
                              alias = "MenuAction",
                              ),
                              

#                    u"Yo" : 
#                        dict( title = u"Yo", 
#                              title_plural = u"Yo's", 
#                              rule_title = u"Yo rules", 
#                              type_ = u"class", 
#                              ),


                    u"Tag" : dict( title=u"Tag", 
                                   title_plural=u"Tags", 
                                   config_type=u"component",
                                   type_=u"component",
                                   component_name=u"dialog", 
                                   access_dependency = True,
                                   internal_element_type=u"tag",
                                   ),
                    
                    u"MenuAction" :  
                        dict( title = u"Menu Action", 
                              title_plural = u"Menu Actions", 
                              config_type=u"component",
                              type_=u"component",
                              access_dependency = u"ProgramAccess",
                              component_name=u"traffic", 
                              internal_element_type=u"menu_action",
                              alias = "ProgramAccess",
                              ),

                    u"AdminAccess" :  
                        dict( title = u"Management Role", 
                              title_plural = u"Management Roles", 
                              rule_title = u"Management Role",
                              config_type=u"component",
                              type_=u"rule",
                              component_name=u"auth", 
                              access_dependency = True,
                              internal_element_type=u"access_element",
                              ),
                    
                    
                    # Some test/demo types:

                        
#                    u"Test0" : dict( title=u"Test0", title_plural=u"Test0", rule_title = u"Test0", 
#                                                 type_=u"class", ),
#                    
#                    u"TestComp1" :         dict( title=u"TestComp1", title_plural=u"TestComp1", rule_title = u"TestComp1",
#                                                 type_=u"composite", 
#                                                 elements = [u"Key", u"KeyAssignment"], 
#
#                                         ),
#
#                    u"TestComp2" :         dict( title=u"TestComp2", title_plural=u"TestComp2", rule_title = u"TestComp2",
#                                                 type_=u"composite", 
#                                                 elements = [u"User", u"Group", "ApplicationAccess"], 
#                                             
#                                         ),
                    
                 }

for name, element_def in def_element_types.items():
    element_def["name"] = name

for name, element_def in def_rule_types.items():
    element_def["name"] = name
    
def_enrollment_map = {
                    u"Token" : u"PersonalTokenAssignment",
                    u"Endpoint" : u"PersonalTokenAssignment",
                  }


def is_type_of(sub_type, main_type):
    if sub_type == main_type:
        return True
    element_def = def_element_types.get(main_type)
    if element_def and element_def.get("type_")=="composite":
        for element in element_def.get("elements"):
            if is_type_of(sub_type, element):
                return True
        
    return False


def get_element_type_for_plugin_type(plugin_name, element_type=None):
    for name, element_type_def in def_element_types.items():
        if element_type_def.get("type_") == "module":
            if element_type_def.get("plugin") == plugin_name:
                if not element_type or element_type_def.get("internal_element_type") == element_type:
                    return name
    raise Exception("Unable to find entity type for plugin '%s' (element_type='%s')" % (plugin_name, element_type if element_type else "Not set"))

def get_element_type_for_component_type(component_name, element_type=None):
    for name, element_type_def in def_element_types.items():
        if element_type_def.get("type_") == "component":
            if element_type_def.get("component_name") == component_name:
                if not element_type or element_type_def.get("internal_element_type") == element_type:
                    return name
    raise Exception("Unable to find entity type for component '%s' (element_type='%s')" % (component_name, element_type if element_type else "Not set"))

def get_element_type_dict(type_name):
    return def_element_types.get(type_name, None)    

def get_plugin_element_type(type_name):
    element_def = def_element_types.get(type_name, None)
    if not element_def:
        raise Exception("Element def '%s' not found" % type_name)
    
    type_ = element_def["config_type"]
    if type_ == "module":
        return element_def.get("internal_element_type")
    elif type_ == "component":
        return element_def.get("internal_element_type")
    elif type_ == "action":
        return "_action"
    else:
        raise Exception("rule_api.get_plugin_element_type called with non applicable type '%s'" % type_)

def is_instance_of(entity_type, composite_type):
    if entity_type==composite_type:
        return True
    composite_def = def_element_types.get(composite_type)
    if composite_def.get("type_")=="composite":
        for child_type in composite_def.get("elements"):
            if is_instance_of(entity_type, child_type):
                return True
    return False
             

_access_defs = None

def _create_access_def(element_dict, element_type):
    access_def = dict()
    access_def["name"] = "%s.%s" % (element_type, element_dict.get("name"))
    title = element_dict.get("long_title") if element_dict.get("long_title") else element_dict.get("title")
    access_def["title"] = "%s (%s)" % (title, element_type)
    access_def["type"] = element_type
    access_def["crud_enabled"]= element_dict.get("crud_enabled", True)
    return access_def

def get_access_defs():
    global _access_defs
    if _access_defs is None:
        _access_defs = []
        for rule_def in def_rule_types.values():
            _access_defs.append(_create_access_def(rule_def, "Rule"))
        for element_def in def_element_types.values():
            access_dependency = element_def.get("access_dependency")
            if access_dependency is True:
                _access_defs.append(_create_access_def(element_def, "Entity"))
        _access_defs.sort()
    return _access_defs

_access_mapping = None

def _get_accesss_types_for_element_type(element_type, access_mapping_dict):
    access_mapping = access_mapping_dict.get(element_type)
    if access_mapping:
        return access_mapping
    element_def = def_element_types.get(element_type)
    if not element_def:
        return None
    assert element_def
    access_dependency = element_def.get("access_dependency")
    if access_dependency is True:
        access_mapping_dict[element_type] = element_type
        return element_type
    if access_dependency == "elements":
        elements = element_def.get("elements")
        element_types = []
        for e in elements:
            parent_access_mapping = _get_accesss_types_for_element_type(e, access_mapping_dict)
            if not parent_access_mapping:
                return None
            if isinstance(parent_access_mapping, list):
                element_types.extend(parent_access_mapping)
            else:
                element_types.append((e,parent_access_mapping)) 
        access_mapping_dict[element_type] = element_types
        return element_types
    else:
        parent_access_mapping = _get_accesss_types_for_element_type(access_dependency, access_mapping_dict)
        if not parent_access_mapping:
            return None
        access_mapping_dict[element_type] = parent_access_mapping
        return parent_access_mapping

def get_access_mapping():
    global _access_mapping
    if _access_mapping is None:
        _access_mapping = dict()
        for name, element_def in def_element_types.items():
            name = element_def.get("name")
            _get_accesss_types_for_element_type(name, _access_mapping)
        
        
    return _access_mapping

def is_crud_enabled(access_type_name):
    element_type, dot, element_type_name = access_type_name.partition(".")
    if element_type=="Rule":
        rule_def = def_rule_types.get(element_type_name)
        return rule_def and rule_def.get("crud_enabled", True)
    elif element_type=="Entity":
        element_def = def_element_types.get(element_type_name)
        return element_def and element_def.get("crud_enabled", True)
    return True

def get_entity_type_title(entity_type):
    entity_type = entity_type.rsplit(".")[-1]
    type_def = def_element_types.get(entity_type)
    if type_def:
        return type_def.get("title")
    else:
        return entity_type

def get_rule_type_title(rule_class):
    type_def = def_rule_types.get(rule_class)
    if type_def:
        return type_def.get("title")
    else:
        return entity_type
            
get_access_mapping()