from components.database.server_common.schema_api import *


dbapi = SchemaFactory.get_creator("auth_server")


def connect_to_database(db_connection):
    dbapi.bind(db_connection)



table_criteria = dbapi.create_table("rule_node",
                                    Column('title', String(256), nullable=False),
                                    Column('description', Text()),
                                    Column('aggregation_kind', Integer),
                                    Column('internal_type_name', String(128), nullable=False),
                                    Column('entity_type', String(128), index=True),
                                    Column('value_id', String(256), index=True),
                                    Column('is_true', Boolean, default=False),
                                    )

table_sub_criteria = dbapi.create_table("rule_node_association",
                                    Column('rule_entity_type', String(128)),
                                    Column('parent_id', Integer, nullable=False, index=True),
                                    Column('child_id', Integer, nullable=False, index=True),
                                    Column('parent_internal_type_name', String(128), nullable=False, index=True),
                                    Column('child_internal_type_name', String(128), nullable=False, index=True),
                                    Column('deactivated', Boolean, default=False),
                                    )

table_default_rule_criteria = dbapi.create_table("default_rule_criteria",
                                     Column('rule_type', String(128), nullable=False),
                                     Column('entity_type', String(128), nullable=False),
                                     Column('node_id', Integer, nullable=False)
                                    )

table_built_in_criteria = dbapi.create_table("built_in_criteria",
                                     Column('internal_name', String(128), nullable=False),
                                     Column('node_id', Integer, nullable=False)
                                    )


table_access_right = dbapi.create_table("access_right",
                                 Column('name', String(128), nullable=False),
                                 Column('title', String(256), nullable=False),
                                 Column('element_type', String(128), nullable=False),
                                 )

table_access_element_attribute = dbapi.create_table("access_element_attribute",
                                     Column('access_right_id', Integer, nullable=False),
                                     Column('access_element_id', Integer, nullable=False),
                                     Column('access_level', String(16), nullable=False),
                                     )

table_rule_restriction = dbapi.create_table("rule_restriction",
                                    Column('restriction_type', String(128)),
                                    Column('rule_id', Integer, nullable=False, index=True),
                                    Column('restriction_rule_id', Integer, nullable=False, index=True),
                                    )
                            

table_element_lifetime = dbapi.create_table("element_lifetime",
                                    Column('entity_type', String(128), index=True, nullable=False),
                                    Column('value_id', String(256), index=True, nullable=False),
                                    Column('start', DateTime),
                                    Column('end', DateTime),
                                    )


dbapi.add_foreign_key_constraint(table_sub_criteria, "parent_id", table_criteria)
dbapi.add_foreign_key_constraint(table_sub_criteria, "child_id", table_criteria)
dbapi.add_foreign_key_constraint(table_default_rule_criteria, "node_id", table_criteria)
dbapi.add_foreign_key_constraint(table_built_in_criteria, "node_id", table_criteria)

dbapi.add_foreign_key_constraint(table_access_element_attribute, "access_right_id", table_access_right)
dbapi.add_foreign_key_constraint(table_access_element_attribute, "access_element_id", table_criteria)

dbapi.add_foreign_key_constraint(table_rule_restriction, "rule_id", table_criteria)
dbapi.add_foreign_key_constraint(table_rule_restriction, "restriction_rule_id", table_criteria)

dbapi.add_unique_constraint(table_access_element_attribute, "access_right_id", "access_element_id")
dbapi.add_unique_constraint(table_element_lifetime, "entity_type", "value_id")



SchemaFactory.register_creator(dbapi)
