"""The database model and the interface and object-persistense to the database"""

import database_schema as schema

from components.database.server_common.schema_api import Relation, mapper
import components.database.server_common.database_api as database_api 

from sqlalchemy.orm.session import Session
import lib.checkpoint 
from components.database.server_common.database_api import QueryResultBase,\
    QuerySession, and_, or_, not_
from components.auth.server_common import database_schema
from lib import checkpoint 


count = 0

class Graph(object):
    
    def __init__(self, checkpoint_handler):
        self.nodes = dict()
        self.enabled_actions = set()
        self.enabled_admin_access = set()
        self._actions = None
        self._admin_access = None
        self.load_graph = True
        self.load_full_graph = False
        self.checkpoint_handler = checkpoint_handler
        self.action_type = RuleCriteria.TYPE_ACTION

        self.preload_types = [RuleCriteria.TYPE_RULE, RuleCriteria.TYPE_RULE_CLAUSE]

    def preload_graph(self):
        if self.load_graph:
            
            if not self.action_type in self.preload_types:
                self.preload_types.append(self.action_type)
            
            with QuerySession() as db_session:
                self._actions = []
                
                
                
                if self.load_full_graph:
                    with self.checkpoint_handler.CheckpointScope("select_all_RuleCriteria", "select_all_RuleCriteria", checkpoint.DEBUG):
                        all_group_criteria = db_session.select(RuleCriteria, result_class=RuleNode )
                    with self.checkpoint_handler.CheckpointScope("select_all_RuleAssociations", "select_all_RuleAssociations", checkpoint.DEBUG):
                        all_assoc = db_session.select(RuleAssociation)
                else:
                    with self.checkpoint_handler.CheckpointScope("select_all_RuleCriteria", "select_all_RuleCriteria", checkpoint.DEBUG):
                        all_group_criteria = db_session.select(RuleCriteria, database_api.in_(RuleCriteria.internal_type_name, self.preload_types), result_class=RuleNode)
    
                    with self.checkpoint_handler.CheckpointScope("select_all_RuleAssociations", "select_all_RuleAssociations", checkpoint.DEBUG):
                        node_ids_expression = db_session.select_expression(RuleCriteria.id, RuleCriteria.internal_type_name.in_(self.preload_types))
                        all_assoc = db_session.select(RuleAssociation, or_(RuleAssociation.parent_id.in_(node_ids_expression),
                                                                                      RuleAssociation.child_id.in_(node_ids_expression)))
                
                for c in all_group_criteria:
                    rule_node = self.add_node(c)
                    rule_node.all_parents_loaded = self.load_full_graph
    #                if self.load_full_graph:
                    rule_node.parents = []
                    rule_node.children = []
                    if c.internal_type_name == "Action":
                        self._actions.append(rule_node)
                        
                with self.checkpoint_handler.CheckpointScope("connect_graph", "connect_graph", checkpoint.DEBUG):
                    for a in all_assoc:
                        if a.deactivated:
                            continue
                        parent = self.get_node(a.parent_id)
                        child = self.get_node(a.child_id)
    #                    parent.add_child(child)
    #                    child.add_parent(parent)
                        if parent:
                            parent.child_ids.append(a.child_id)
                        if child:
                            child.parent_ids.append(a.parent_id)
        
    def get_always_true_criteria(self):
        true_criteria = []
        if not self.load_graph:
            raise NotImplementedError()
        else:
            for node in self.get_nodes():
                if node.aggregation_kind == RuleCriteria.AllOf and not node.child_ids:
                    true_criteria.append(node)
        return true_criteria
        
#    def check_possible_actions(self):
#        if self.enabled_actions:
#            return True
#        for action in self.get_actions():
#            rule_nodes = self._get_rule_nodes_for_action(action)
#            action_possible = True
#            for rule_node in rule_nodes:
#                for child in rule_node.get_children():
#                    if child.value is False:
#                        action_possible = False
#                        break
#                if action_possible:
#                    return True
#        return False
#    
#    def _get_rule_nodes_for_action(self, action):
#        children = action.get_children()
#        rule_nodes = []
#        for child in children:
#            rule_nodes.extend(child.get_children())
#            
#        return rule_nodes
#     
                    
        
    def add_node(self, new_node):
        node_id = new_node.id
        new_node.graph = self
        self.nodes[node_id] = new_node
        
        return new_node
    
    def get_nodes(self):
        return self.nodes.values()
    
    def get_actions(self):
        if self._actions is None:
            self._actions = [node for node in self.get_nodes() if node.is_action()]
            
        return self._actions

    def get_admin_access(self):
        if self._admin_access is None:
            self._admin_access = [node for node in self.get_nodes() if node.is_admin_access()]
            
        return self._admin_access
    
    def get_node(self, node_id):
        return self.nodes.get(node_id)
        
    def get_condition_node(self, node_id):
        return self.nodes.get("%s" % node_id)
    
    def get_or_create_node(self, criteria):
        node = self.get_node(criteria.id)
        if not node:
            node = self.add_node(criteria)
        return node
        
    def clear(self):
        self.nodes = dict()
        self.enabled_actions = set()
        self.enabled_admin_access = set()
        self._actions = None
        self._admin_access = None

        self.preload_types = [RuleCriteria.TYPE_RULE, RuleCriteria.TYPE_RULE_CLAUSE]
        for node in self.nodes.values():
            node.init()
        

class RuleNode(QueryResultBase):
    
    def init(self):
        self.children = None
        self.parents = None
        self.parent_ids = []
        self.child_ids = []
        self.graph = None
        self.value = None
        self.mark = None
        self.all_parents_loaded = False


    def add_child(self, child):
        if not self.children:
            self.children = []
        self.children.append(child)

    def add_parent(self, parent):
        if not self.parents:
            self.parents = []
        self.parents.append(parent)
        
    def _get_parents_from_db(self, only_active=True):
        clauses = [database_schema.table_sub_criteria.c.child_id == self.id,
                   database_schema.table_criteria.c.id == database_schema.table_sub_criteria.c.parent_id]
        if only_active:
            clauses.append(database_schema.table_sub_criteria.c.deactivated==False)
        clauses = tuple(clauses)
        
        db_session = QuerySession()
        return db_session.select([database_schema.table_criteria, database_schema.table_sub_criteria.c.deactivated], 
                                 database_api.and_(*clauses),
                                 result_class=RuleNode)
#        if only_active:
#            id_select = db_session.select_expression(RuleAssociation.parent_id, database_api.and_(RuleAssociation.child_id == self.id,
#                                                                                                 RuleAssociation.deactivated==False))
#        else:
#            id_select = db_session.select_expression(RuleAssociation.parent_id, RuleAssociation.child_id == self.id)
#        return db_session.select(RuleCriteria, RuleCriteria.id.in_(id_select), result_class=RuleNode)
    
        
    def get_parents(self, only_active=True):
        if self.parents is None:
            self.parents = []
#            print "get_parents %s" % self.id
            parent_criteria = self._get_parents_from_db(only_active)
            for c in parent_criteria:
                parent = self.graph.get_or_create_node(c)
                self.parents.append(parent)
        elif self.parent_ids and len(self.parent_ids)>len(self.parents):
            self.parents = []
            ids_not_found = []
            for parent_id in self.parent_ids:
                parent_node = self.graph.get_node(parent_id)
                if not parent_node:
                    ids_not_found.append(parent_id)
                else:
                    self.parents.append(parent_node)
            if ids_not_found:
                db_session = QuerySession()
                criterias = db_session.select(RuleCriteria, RuleCriteria.id.in_(ids_not_found), result_class=RuleNode)
                for c in criterias:
                    node = self.graph.get_or_create_node(c)
                    self.parents.append(node)
        return self.parents
    
    def _get_children_from_db(self, only_active=True):
        clauses = [database_schema.table_sub_criteria.c.parent_id == self.id,
                   database_schema.table_criteria.c.id == database_schema.table_sub_criteria.c.child_id]
        if only_active:
            clauses.append(database_schema.table_sub_criteria.c.deactivated==False)
        clauses = tuple(clauses)
        
        db_session = QuerySession()
        return db_session.select([database_schema.table_criteria, database_schema.table_sub_criteria.c.deactivated], 
                                 database_api.and_(*clauses),
                                 result_class=RuleNode)
 
    def get_children(self, only_active=True):
        if self.children is None:
            self.children = []
            sub_criteria = self._get_children_from_db(only_active)
            for c in sub_criteria:
                child = self.graph.get_or_create_node(c)
                self.children.append(child)
        elif self.child_ids and len(self.child_ids)>len(self.children):
            self.children = []
            ids_not_found = []
            for child_id in self.child_ids:
                child_node = self.graph.get_node(child_id)
                if not child_node:
                    ids_not_found.append(child_id)
                else:
                    self.children.append(child_node)
            if ids_not_found:
                db_session = QuerySession()
                criterias = db_session.select(RuleCriteria, RuleCriteria.id.in_(ids_not_found), result_class=RuleNode)
                for c in criterias:
                    node = self.graph.get_or_create_node(c)
                    self.children.append(node)
            
        return self.children
    
    def is_action(self):
        return self.internal_type_name == RuleCriteria.TYPE_ACTION

    def is_admin_access(self):
        return self.internal_type_name == RuleCriteria.TYPE_ADMIN_ACCESS

    def get_partial_results(self, partial_results, db_session):
        
        if partial_results.has_key(self.id):
            return
        
        partial_result = None
        if self.aggregation_kind == RuleCriteria.AllOf:
            found_true = []
            found_false = 0
            false_criteria = None
            for sub_criteria in self.get_children():
                result = sub_criteria.value
                if result:
                    found_true.append(sub_criteria)
                else:
                    found_false += 1
                    false_criteria = sub_criteria
#                    sub_criteria.get_partial_results(results, partial_results)
            if found_false==1:
                # We are not interested in two "falses"
                if found_true:
                    partial_result = (self, false_criteria)
                false_criteria.get_partial_results(partial_results, db_session)
        else:
            for sub_criteria in self.get_children():
                sub_criteria.get_partial_results(partial_results, db_session)
        
        partial_results[self.id] = partial_result
        

    
    def get_partial(self):
        partial_results = []
        for parent in self.parents:
            if not parent.value and parent.rule_type == RuleCriteria.AllOf and len(parent.children)==2:
                partial_results.append(parent)
        return partial_results
                    

    
    def set_true_bottom_up(self, db_session):
        
#        if self.deactivated:
#            self.value = False
#            return
        
        if self.value is None:
            rule_type = self.aggregation_kind
            if self.internal_type_name != RuleCriteria.TYPE_CONDITION and rule_type == RuleCriteria.AllOf:
                for child in self.get_children():
                    if not child.value:
                        if child.value is False:
                            self.value = False
                        return
            self.value = True
            if self.is_action():
                self.graph.enabled_actions.add(self)
            elif self.is_admin_access():
                self.graph.enabled_admin_access.add(self)
            for parent in self.get_parents():
                parent.set_true_bottom_up(db_session)
                    
    def get_report_info(self):
#        if self.internal_type in [RuleCriteria.TYPE_ACTION, RuleCriteria.TYPE_CONDITION, RuleCriteria.TYPE_GROUP, RuleCriteria.TYPE_ASSSOCIATION]:
#            return self.title
#        return ""
        return self.title
    
    def __str__(self):
        if self.title:
            return self.title
        return "unknown"
                
    

class RuleCriteria(object):


    (AllOf,  # and, universal quantifier
     OneOf,  # or, existential quantifier
     NoneOf,  # and not, non-existential quantifier / universal non-quantifier
     ) = range(3)

    ''' Internal type values '''
    
    TYPE_CONDITION = "Condition"
    TYPE_ACTION = "Action"
    TYPE_RULE = "Rule"
    TYPE_RULE_CLAUSE = "RuleClause"
    TYPE_ASSSOCIATION = "Association"
    TYPE_ASSSOCIATION_CLAUSE = "AssociationClause"
    TYPE_GROUP = "Group"
    TYPE_ADMIN_ACCESS = "AdminAccess"

        
        


class RuleAssociation(object):
    pass

class RuleRestriction(object):
    pass

class ElementLifetime(object):
    pass

mapper(RuleCriteria, schema.table_criteria)
mapper(RuleAssociation, schema.table_sub_criteria)
mapper(RuleRestriction, schema.table_rule_restriction)
mapper(ElementLifetime, schema.table_element_lifetime)

#mapper(RuleAssociation, schema.table_sub_criteria)
#mapper(ConditionAssociation, schema.table_condition_association)
#mapper(Condition, schema.table_condition, relations=[Relation(ConditionAssociation, "parents", "conditions")])
#mapper(RuleCriteria, schema.table_criteria, relations=[Relation(RuleAssociation, "children", "rules")])


checkpoint_handler_demo = None
