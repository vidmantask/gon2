from __future__ import with_statement
import unittest
import time

import lib.giri_unittest as giri_unittest


from components.auth.server_common.database_schema import *
from components.auth.server_gateway.auth_session import *
import components.environment
from components.database.server_common.schema_api import SchemaFactory, clear_database
import components.database.server_common.database_api as database_api



from components.communication.sim import sim_tunnel_endpoint
import components.auth.server_management.rule_api as rule_api
import components.auth.server_management.auth_graph_model as model
from plugin_types.server_gateway import plugin_type_auth


checkpoint_handler = lib.giri_unittest.get_checkpoint_handler()
checkpoint_handler_null = lib.giri_unittest.get_checkpoint_handler_null()

import components.communication.tunnel_endpoint_base


database_connect_string = 'sqlite:///%s/test.sqlite' % (lib.giri_unittest.mkdtemp())
environment = components.environment.Environment(checkpoint_handler, database_connect_string, 'management_message_unittest')
#ServerSchema.connect(environment.get_default_database())



class Dummy(object):

    def dummy(self, *args, **kwargs):
        pass

    def __getattr__(self, arg):
        return self.dummy

class UserSessionStub(object):

    def all_plugins_started(self):
        pass

    def set_auth_callback(self, callback):
        pass

user_session_stub = UserSessionStub()

class EndpointSessionStub(object):

    def set_auth_callback(self, callback):
        pass

endpoint_session_stub = EndpointSessionStub()


class SessionInfo(object):
    def __init__(self):
        self.unique_session_id = 'xxxxx1'


class AuthTestPlugin(plugin_type_auth.PluginTypeAuth):
    def __init__(self, async_service, plugin_name, plugin_started_callback, licence_handler=None, database=None, management_message_session=None, access_log_server_session=None):
        plugin_type_auth.PluginTypeAuth.__init__(self, async_service, checkpoint_handler, plugin_name, licence_handler, SessionInfo(), database, management_message_session, access_log_server_session)
        self.value = False
        self.plugin_started_callback = plugin_started_callback

    def start(self):
        self.plugin_started_callback(self)

    def _check_element(self, internal_type, param):
        return self.value

def create_plugin_criteria(transaction, module_name, value_id, value):
    sub_criteria = model.Condition()
    sub_criteria.entity_type = module_name
    sub_criteria.title = value
    sub_criteria.value_id = value_id
    element_def = rule_definitions.def_element_types.get(module_name)
    if not element_def:
        element_dict = dict( title=u"Title " + module_name, title_plural=u"Title p " + module_name, rule_title = u"Title " + module_name,
                             config_type=u"module",
                             type_=u"module", plugin=module_name, internal_element_type = u"parm")
        rule_definitions.def_element_types[module_name] = element_dict

    return sub_criteria

class ComSessionAPIStub(object):

    def __init__(self):
        self.close_called = False

    def child_close(self):
        self.close_called = True



class AuthSessionTest(unittest.TestCase):

    def setUp(self):
        clear_database(environment.get_default_database())
        SchemaFactory.connect(environment.get_default_database())
        self.raise_error = True

    def test_auth_session1(self):
        with database_api.Transaction() as t:
            action_element = rule_api.create_action_criteria("action0", t)
            action = action_element.criteria
            t.add(action)
            criteria = create_plugin_criteria(t, "plugin0", 1, "1")
            action.add_sub_criteria(criteria)

        def plugin_started(plugin):
            plugin.set_started()

        plugin = AuthTestPlugin(giri_unittest.get_async_service(), "plugin0", plugin_started)

        options = AuthorizationSessionOptions(True)
        com_session_api = ComSessionAPIStub()
        tunnel_endpoint_server = components.communication.tunnel_endpoint_base.TunnelendpointSessionStub(giri_unittest.get_async_service())
        session = AuthorizationSession(None, giri_unittest.get_async_service(), checkpoint_handler, tunnel_endpoint_server, Dummy(), SessionInfo(),  { plugin.plugin_name : plugin}, com_session_api, user_session_stub, endpoint_session_stub, options)

        session = AuthorizationSession(
        None,
        giri_unittest.get_async_service(),
        checkpoint_handler,
        tunnel_endpoint_server,
        Dummy(),
        SessionInfo(),
        { plugin.plugin_name : plugin},
        com_session_api,
        user_session_stub,
        endpoint_session_stub,
        options)

        def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, plugin_manager, plugins, com_session_api, current_runtime_env):


        session.session_start_and_connected()

        self.assertEquals(session.state, session.STATE_STARTING)
        self.assertEquals(session.authorized_actions, [])

        def auth_session_is_started():
            return session.state == session.STATE_STARTED

        def auth_session_has_result(min_size=1):
            return len(session.authorized_actions) >= min_size

        plugin.value = True
        plugin.set_ready()
        lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000)

        self.assertEquals(session.authorized_actions, [1])


    def test_auth_session2(self):
        def plugin_started(plugin):
            plugin.set_started()

        with database_api.Transaction() as t:
            plugins = dict()
            for i in range(0,3):
                plugin = AuthTestPlugin(giri_unittest.get_async_service(), "plugin%s" %i, plugin_started)
                plugins[plugin.plugin_name] = plugin

            actions = []
            for i in range(0,3):
                action_element = rule_api.create_action_criteria("action%s" % i, t)
                actions.append(action_element.criteria)
            t.add_list(actions)

            for i in range(0,2):
                criteria = create_plugin_criteria(t, "plugin%s" % i, 1, "1")
                t.add(criteria)
                actions[i].add_sub_criteria(criteria)

        options = AuthorizationSessionOptions(False)
        com_session_api = ComSessionAPIStub()
        tunnel_endpoint_server = components.communication.tunnel_endpoint_base.TunnelendpointSessionStub(giri_unittest.get_async_service())
        session = AuthorizationSession(None, giri_unittest.get_async_service(), checkpoint_handler, tunnel_endpoint_server, Dummy(), SessionInfo(), plugins, com_session_api, user_session_stub, endpoint_session_stub, options)
        session.session_start_and_connected()

        def auth_session_is_started():
            return session.state == session.STATE_STARTED

        def auth_session_has_result(min_size=1):
            return len(session.authorized_actions) >= min_size

        lib.giri_unittest.wait_until_with_timeout(auth_session_is_started, 10000)

        self.assertEquals(session.state, session.STATE_STARTED)
        self.assertEquals(session.authorized_actions, [])

        auth_actions = []
        for i in range(0,2):
            self.assertEquals(session.state, session.STATE_STARTED)
            plugins["plugin%s" % i].value = True
            plugins["plugin%s" % i].set_ready()
            auth_actions.append(i+1)
            lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000, min_size=len(auth_actions))
            self.assertEquals(session.authorized_actions, auth_actions)

#            lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000, min_size=len(auth_actions))

        self.assertEquals(session.state, session.STATE_READY)
        self.assertEquals(session.authorized_actions, [1,2])



    def test_auth_session3(self):

        def plugin_started(plugin):
            if self.raise_error and plugin.plugin_name=="plugin1":
                self.raise_error = False
                raise plugin_type_auth.PluginWaitingForConnectionException()
            plugin.set_started()

        with database_api.Transaction() as t:
            plugins = dict()
            for i in range(0,3):
                plugin = AuthTestPlugin(giri_unittest.get_async_service(), "plugin%s" %i, plugin_started)
                plugins[plugin.plugin_name] = plugin

            actions = []
            for i in range(0,3):
                action_element = rule_api.create_action_criteria("action%s" % i, t)
                actions.append(action_element.criteria)
            t.add_list(actions)

            for i in range(0,2):
                criteria = create_plugin_criteria(t, "plugin%s" % i, 1, "1")
                t.add(criteria)
                actions[i].add_sub_criteria(criteria)

        options = AuthorizationSessionOptions(False)
        com_session_api = ComSessionAPIStub()
        tunnelendpoint = components.communication.tunnel_endpoint_base.TunnelendpointSessionStub(giri_unittest.get_async_service())
        session = AuthorizationSession(None, giri_unittest.get_async_service(), checkpoint_handler, tunnelendpoint, Dummy(), SessionInfo(), plugins, com_session_api, user_session_stub, endpoint_session_stub, options)
        session.session_start_and_connected()

        def auth_session_is_started():
            return session.state == session.STATE_STARTED

        def auth_session_is_ready():
            return session.state == session.STATE_READY

        def auth_session_has_result(min_size=2):
            return len(session.authorized_actions) >= min_size

        lib.giri_unittest.wait_until_with_timeout(auth_session_is_started, 10000)
        self.assertEquals(session.authorized_actions, [])

        plugins["plugin0"].value = True
        plugins["plugin0"].set_ready()
        session.auth_child_created_on_client("plugin1", 1)

        lib.giri_unittest.wait_until_with_timeout(auth_session_is_ready, 10000)
        self.assertEquals(session.authorized_actions, [1])

        plugins["plugin1"].value = True
        plugins["plugin1"].set_ready()

        lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000)
        self.assertEquals(len(session.authorized_actions), 2)



    def test_auth_session4(self):

        print "test4, started"
        def plugin_started(plugin):
            if self.raise_error and plugin.plugin_name=="plugin1":
                self.raise_error = False
                raise plugin_type_auth.PluginWaitingForConnectionException()
            plugin.set_started()

        with database_api.Transaction() as t:
            plugins = dict()
            for i in range(0,3):
                plugin = AuthTestPlugin(giri_unittest.get_async_service(), "plugin%s" %i, plugin_started)
                plugins[plugin.plugin_name] = plugin

            actions = []
            for i in range(0,3):
                action_element = rule_api.create_action_criteria("action%s" % i, t)
                actions.append(action_element.criteria)
            t.add_list(actions)

            for i in range(0,2):
                criteria = create_plugin_criteria(t, "plugin%s" % i, 1, "1")
                t.add(criteria)
                actions[i].add_sub_criteria(criteria)

            options = AuthorizationSessionOptions(True)
            com_session_api = ComSessionAPIStub()
            tunnel_endpoint_server = components.communication.tunnel_endpoint_base.TunnelendpointSessionStub(giri_unittest.get_async_service())
            session = AuthorizationSession(None, giri_unittest.get_async_service(), checkpoint_handler, tunnel_endpoint_server, Dummy(), SessionInfo(), plugins, com_session_api, user_session_stub, endpoint_session_stub, options)
            session.session_start_and_connected()

            def auth_session_is_started():
                return session.state == session.STATE_STARTED

            def auth_session_is_ready():
                return session.state == session.STATE_READY

            def auth_session_has_result(min_size=1):
                return len(session.authorized_actions) >= min_size

        self.assertEquals(session.authorized_actions, [])

        plugins["plugin0"].value = True
        plugins["plugin0"].set_ready()

        lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000)
        self.assertEquals(session.authorized_actions, [1])

        session.auth_child_created_on_client("plugin1", 1)

        self.assertEquals(session.authorized_actions, [1])

        plugins["plugin1"].value = True
        plugins["plugin1"].set_ready()

        lib.giri_unittest.wait_until_with_timeout(auth_session_has_result, 10000, min_size=2)
        lib.giri_unittest.wait_until_with_timeout(auth_session_is_ready, 10000)

        self.assertEquals(len(session.authorized_actions), 2)


    def test_auth_session5(self):
        with database_api.Transaction() as t:
            action_element = rule_api.create_action_criteria("action0", t)
            action = action_element.criteria
            t.add(action)
            criteria = create_plugin_criteria(t, "plugin0", 1, "1")
            action.add_sub_criteria(criteria)

        def plugin_started(plugin):
            plugin.set_started()

        plugin = AuthTestPlugin(giri_unittest.get_async_service(), "plugin0", plugin_started)

        options = AuthorizationSessionOptions(always_allow_access = False)
        com_session_api = ComSessionAPIStub()
        tunnel_endpoint_server = components.communication.tunnel_endpoint_base.TunnelendpointSessionStub(giri_unittest.get_async_service())
        session = AuthorizationSession(None, giri_unittest.get_async_service(), checkpoint_handler, tunnel_endpoint_server, Dummy(), SessionInfo(), { plugin.plugin_name : plugin}, com_session_api,  user_session_stub, endpoint_session_stub, options)
        session.session_start_and_connected()

        def auth_session_is_started():
            return session.state == session.STATE_STARTED

        def auth_session_is_ready():
            return session.terminated


        self.assertEquals(session.state, session.STATE_STARTING)
        self.assertEquals(session.authorized_actions, [])

        plugin.value = False
        plugin.set_ready()

        lib.giri_unittest.wait_until_with_timeout(auth_session_is_ready, 10000)

        self.assertEquals(com_session_api.close_called, True)

#    def disabled_test_auth_session6(self):
#
#
#        with database_api.Transaction() as t:
#            t.add(model.Module(module_name="plugin0"))
#
#            action_element = rule_api.create_action_criteria("action0", t)
#            action = action_element.criteria
#            t.add(action)
#            criteria = create_plugin_criteria(t, "plugin0", 1, "1")
#            action.add_criteria(criteria)
#
#        def plugin_started(plugin):
#            plugin.set_started()
#
#        plugin = AuthTestPlugin("plugin0", plugin_started)
#
#        options = AuthorizationSessionOptions(always_allow_access = False, timeout=1)
#        com_session_api = ComSessionAPIStub()
#        session = AuthorizationSession(None, checkpoint_handler, Dummy(), { plugin.plugin_name : plugin}, com_session_api, async_servcice_handler, user_session_stub, options)
#        session.start_session()
#
#        self.assertEquals(session.state, session.STATE_STARTED)
#        self.assertEquals(session.authorized_actions, [])
#
#        time.sleep(2)
#        async_servcice_handler.run()
#
#        self.assertEquals(com_session_api.close_called, True)

#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'
GIRI_UNITTEST_TIMEOUT = 120
GIRI_UNITTEST_IGNORE = False

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
