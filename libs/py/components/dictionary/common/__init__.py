"""
This module holds the common functionality for the dictionary compoent
"""
import glob
import os.path
import lib.dictionary


def load_dictionary(checkpoint_handler, dictionary_folder='', lang_name=None, lang_sub_name=None):
    dictionary = None
    if lang_name is not None:
        dictionary_filename_abs = os.path.join(dictionary_folder, lib.dictionary.generate_dictionary_filename(lang_name, lang_sub_name))
        dictionary = lib.dictionary.Dictionary.create_from_spec_fp(checkpoint_handler, dictionary_filename_abs)
        if dictionary is not None:
            if checkpoint_handler is not None:
                checkpoint_handler.Checkpoint("load_dictionary", "Dictionary", lib.checkpoint.DEBUG, dictionary_filename=dictionary_filename_abs)

    if dictionary is None:
        for dictionary_filename_abs in glob.glob(os.path.join(dictionary_folder, '*' + lib.dictionary.DICTIONARY_SUFIX)):
            if checkpoint_handler is not None:
                checkpoint_handler.Checkpoint("load_dictionary", "Dictionary", lib.checkpoint.DEBUG, dictionary_filename=dictionary_filename_abs)
            dictionary = lib.dictionary.Dictionary.create_from_spec_fp(checkpoint_handler, dictionary_filename_abs)
            break
                                              
    if dictionary is None:
        if checkpoint_handler is not None:
            checkpoint_handler.Checkpoint("load_dictionary.using_default", "Dictionary", lib.checkpoint.DEBUG)
        dictionary = lib.dictionary.Dictionary(checkpoint_handler)
    return dictionary
