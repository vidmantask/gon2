"""
This file contains ...
"""

import lib.checkpoint

import components.management_message.server_common.session_server
import components.communication.message

from components.management_message import MODULE_ID


class ManagementMessage(object):
    def __init__(self, sender_server_sid, (reciever_id, package)):
        self.id = 0
        self.sender_server_sid = sender_server_sid
        self.reciever_id = reciever_id
        self.package = package


class ManagementMessageSessionRecieverBase(object):
    def __init__(self, checkpoint_handler, reciever_id):
        self.checkpoint_handler = checkpoint_handler
        self.reciever_id = reciever_id
        
    def dipatch(self, server_sid, package):
        try:
            msg = components.communication.message.Message.from_package(package)
            msg.add_arg('server_sid', server_sid)
            msg.call_receiver(self)
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("ManagementMessageSessionRecieverBase", MODULE_ID, lib.checkpoint.ERROR)


class ManagementMessageSessionRecieve(components.management_message.server_common.session_server.ManagementMessageRecieveInterface):
    def __init__(self, checkpoint_handler):
        self.checkpoint_handler = checkpoint_handler
        self._recievers = {}

    def recieve_message(self, session_id, server_sid, message_raw):
        try:
            message = ManagementMessage(server_sid, message_raw)
            with self.checkpoint_handler.CheckpointScope("recieve_message", MODULE_ID, lib.checkpoint.DEBUG, id=message.id, reciever_id=message.reciever_id):
                if self._recievers.has_key(message.reciever_id):
                    self._recievers[message.reciever_id].dipatch(server_sid, message.package)
                else:
                    self.checkpoint_handler.Checkpoint("recieve_message", MODULE_ID, lib.checkpoint.ERROR, message="Receiver not found")
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("recieve_message.error", MODULE_ID, lib.checkpoint.CRITICAL)

    def add_reciever(self, reciever):
        self._recievers[reciever.reciever_id] = reciever
    
