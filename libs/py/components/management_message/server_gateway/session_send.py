"""
This file contains classes for sending messages from gateway server to management server
"""
from components.management_message import MODULE_ID

import components.communication.message


class ManagementMessageSessionSender(object):
    def __init__(self, reciever_id, session_send):
        self.reciever_id = reciever_id
        self.session_send = session_send
        
    def message_send(self, message):
        self.session_send.send(self.reciever_id,  message.to_package())

    def message_remote(self, receiver_id, **values):
        message = components.communication.message.Message(receiver_id, **values)
        self.session_send.send(self.reciever_id,  message.to_package())


class ManagementMessageSessionSend(object):
    def __init__(self, checkpoint_handler, management_message_client_session):
        self.checkpoint_handler = checkpoint_handler
        self.management_message_client_session = management_message_client_session
    
    def send(self, reciever_id, package):
        message = (reciever_id, package)
        self.management_message_client_session.send_message(message)
        
    def create_sender(self, receiver_id):
        return ManagementMessageSessionSender(receiver_id, self)


class ManagementMessageSessionSendBlackHole(object):
    def __init__(self):
        pass
    
    def send(self, reciever_id, package):
        pass