"""
This file contains the classes for handling server side of communication between 
management server and gatewayservers.
"""
import lib.checkpoint
import time
import uuid

from  components.management_message import MODULE_ID
import components.communication.session
import components.communication.session_manager
import session_element_control
import session_element_message
import session_element_filedist


class ManagementMessageServerSession(components.communication.session.APISessionEventhandlerReady):
    def __init__(self, checkpoint_handler, async_service, com_session, dictionary, recieve_callback, filedist_manager, auth_callback, gateway_server_path):
        components.communication.session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.async_service = async_service
        self.com_session = com_session
        self.com_session.set_eventhandler_ready(self)
        self.recieve_callback = recieve_callback
        self.auth_callback = auth_callback
        self.gateway_server_path = gateway_server_path
        self.socket_info_remote = com_session.get_ip_remote()
        self._element_control = session_element_control.ManagementMessageSessionElementControlServer(com_session, async_service, checkpoint_handler, 1, self.auth_callback)
        self._element_message = session_element_message.ManagementMessageSessionElementMessageServer(com_session, async_service, checkpoint_handler, 2, self.recieve_callback, self._element_control)
        self._element_filedist = session_element_filedist.ManagementMessageSessionElementFiledistServer(com_session, async_service, checkpoint_handler, 3, self.recieve_callback, filedist_manager, self.gateway_server_path)

    def session_state_ready(self):
        remote_ip = self.com_session.get_ip_remote()[0]
        unique_session_id = self.com_session.get_unique_session_id()
        self.checkpoint_handler.CheckpointScope("ManagementMessageServerSession::session_ready", MODULE_ID, lib.checkpoint.INFO, remote_ip=remote_ip, unique_session_id=unique_session_id)
        self.com_session.read_start_state_ready()
        self._element_control.start_when_connected()
        self._element_message.start_when_connected()
        self._element_filedist.start_when_connected()

    def session_state_closed(self):
        unique_session_id = self.com_session.get_unique_session_id()
        self.checkpoint_handler.CheckpointScope("ManagementMessageServerSession::session_closed", MODULE_ID, lib.checkpoint.INFO, unique_session_id=unique_session_id)
        self._element_control.reset()
        self._element_message.reset()
        self._element_filedist.reset()

    def session_state_key_exchange(self):
        pass
    
    def session_user_signal(self, signal_id, message):
        pass
    
    def is_closed(self):
        return self._com_session.is_closed()
                
    def get_remote_service_info(self):
        return self._element_control.get_remote_service_info()

    def ping(self):
        self._element_control.ping()

    def service_restart(self, when_no_sessions):
        self._element_control.service_restart(when_no_sessions)

    def service_stop(self, when_no_sessions):
        self._element_control.service_stop(when_no_sessions)

    def session_close(self, unique_session_id):
        self._element_control.session_close(unique_session_id)

    def session_recalculate_menu(self, unique_session_id):
        self._element_control.session_recalculate_menu(unique_session_id)



class ManagementMessageRecieveInterface(object):
    def recieve_message(self, session_id, server_sid, message):
        pass



class ManagementMessageServerSessionManager(components.communication.session_manager.APISessionManagerEventhandler, session_element_control.AuthCallback):
    def __init__(self, async_service, checkpoint_handler, server_knownsecret, dictionary, internal_ws_connector, listen_ip, listen_port, listen_id, recieve_callback, filedist_root, tickets_max, check_interval_sec, gateway_server_path):
        components.communication.session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.server_knownsecret = server_knownsecret
        self.dictionary = dictionary
        self.internal_ws_connector = internal_ws_connector
        self._com_session_manager = components.communication.session_manager.APISessionManagerServer_create(self.async_service.get_com_async_service(), 
                                                                                                            self,
                                                                                                            self.checkpoint_handler.checkpoint_handler,
                                                                                                            server_knownsecret, 
                                                                                                            listen_ip, listen_port, str(listen_id))
        self._com_session_manager.set_config_idle_timeout_sec(0, 60) # Disable session idle timeout
        self._sessions = {}
        self.recieve_callback = recieve_callback
        self.gateway_server_path = gateway_server_path
        self.filedist_manager = session_element_filedist.FiledistManager(async_service, checkpoint_handler, filedist_root, self._com_session_manager.get_mutex(), tickets_max, check_interval_sec, self.gateway_server_path)

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_connecting(self, connection_id, connection_title):
        pass 
    
    def session_manager_connecting_failed(self, connection_id):
        pass

    def session_manager_failed(self, message):
        pass

    def session_manager_session_created(self, connection_id, com_session):
        session_id = com_session.get_session_id()
        unique_session_id = com_session.get_unique_session_id()
        self.checkpoint_handler.Checkpoint("ManagementMessageServerSessionManager.session_created", MODULE_ID, lib.checkpoint.DEBUG, unique_session_id=unique_session_id)
        self._sessions[session_id] = ManagementMessageServerSession(self.checkpoint_handler, self.async_service, com_session, self.dictionary, self.recieve_callback, self.filedist_manager, auth_callback=self, gateway_server_path=self.gateway_server_path)

    def session_manager_session_closed(self, session_id):
        self.checkpoint_handler.Checkpoint("ManagementMessageServerSessionManager.session_closed", MODULE_ID, lib.checkpoint.DEBUG, session_id=session_id)
        if session_id in self._sessions.keys():
            del self._sessions[session_id]

    def session_manager_closed(self):
        pass
    
    def is_closed(self):
        return self._com_session_manager.is_closed()

    def start(self):
        try:
            self.filedist_manager.start()
            self._com_session_manager.start()
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("ManagementMessageServerSessionManager.start.error", MODULE_ID)
        
    def stop_and_wait(self):
        try:
            self.filedist_manager.stop_and_wait()
            self._com_session_manager.close_start()
            while not self.is_closed():
                time.sleep(0.5)
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("ManagementMessageServerSessionManager.stop.error", MODULE_ID)

    def get_remote_service_infos(self):
        result = []
        for session_id in self._sessions.keys():
            session = self._sessions[session_id]
            result.append(session.get_remote_service_info())
        return result
    
    def service_restart(self, sid, when_no_sessions):
        session = self._lookup_by_sid(sid)
        if session is not None:
            session.service_restart(when_no_sessions)
            return True
        return False

    def service_stop(self, sid, when_no_sessions):
        session = self._lookup_by_sid(sid)
        if session is not None:
            session.service_stop(when_no_sessions)
            return True
        return False

    def session_close(self, sid, unique_session_id):
        session = self._lookup_by_sid(sid)
        if session is not None:
            session.session_close(unique_session_id)
            return True
        return False

    def session_recalculate_menu(self, sid, unique_session_id):
        session = self._lookup_by_sid(sid)
        if session is not None:
            session.session_recalculate_menu(unique_session_id)
            return True
        return False

    def _lookup_by_sid(self, sid):
        for session in self._sessions.values():
            service_info = session.get_remote_service_info()
            if service_info.sid == sid:
                return session
        return None
    
    def auth_verify_server_sid(self, server_sid, session_count):
        new_sid = False
        if server_sid in [None, '']:
            new_sid = True 
        else:
            other_session = self._lookup_by_sid(server_sid)
            if other_session:
                # There is another session with same sid.
                try:
                    other_session.ping()
                    new_sid = True
                except:
                    # We have lost connection to the other session - delete it from the list and let the "new" one use the sid
                    self.checkpoint_handler.Checkpoint("ManagementMessageServerSessionManager.lost_connection", MODULE_ID, lib.checkpoint.INFO, server_sid=server_sid)
                    for session_id, session in self._sessions.items():
                        if session == other_session:
                            del self._sessions[session_id]
                            break
        if new_sid:
            old_server_sid = server_sid
            server_sid = str(uuid.uuid1()) 
            self.checkpoint_handler.Checkpoint("ManagementMessageServerSessionManager.server_sid.generated", MODULE_ID, lib.checkpoint.INFO, server_sid=server_sid, old_server_sid=old_server_sid)
        return server_sid
    
    def verify_internal_login(self, login_index, login):
        return self.internal_ws_connector.verify_internal_login(login_index, login)
