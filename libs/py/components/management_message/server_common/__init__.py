"""
Message queue between Gateway Server and the Management Server.
Currently implemented with one database relation, could be local async message queues in the future. 
"""
import datetime

# Utiility functions

def from_timestamp_to_sink(d):
    if not d is None:
        return (d.year, d.month, d.day, d.hour, d.minute, d.second, d.microsecond)
    else:
        return None


def from_sink_to_timestamp(ts):
    if not ts is None:
        return datetime.datetime(ts[0],ts[1],ts[2],ts[3],ts[4],ts[5],ts[6])
    else:
        return None
