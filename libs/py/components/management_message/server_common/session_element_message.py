"""
This file contains the client and server side of message session element
"""
import session_element


class ManagementMessageSessionElementMessageClient(session_element.ManagementMessageSessionElement):
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id)
    
    def start(self):
        """
        Called when connected and session is ready
        """
        pass

    def send_message(self, message):
        self.tunnelendpoint_remote('remote_send_mesage', message=message)
    
    
class ManagementMessageSessionElementMessageServer(session_element.ManagementMessageSessionElement):
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, recieve_callback, element_control):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id)
        self.recieve_callback = recieve_callback
        self.element_control = element_control
    
    def reset(self):
        self.recieve_callback = None
        self.element_control = None
        session_element.ManagementMessageSessionElement.reset(self)
    
    def start(self):
        """
        Called when connected and session is ready
        """
        pass

    def remote_send_mesage(self, message):
        if self.recieve_callback is not None:
            self.recieve_callback.recieve_message(self.get_session_id(), self.element_control.get_server_sid(), message)
