"""
This file contains the client and server side of control session element
"""
import session_element

GATEWAY_STATUS_UNKNOWN = 0
GATEWAY_STATUS_RUNNING = 1
GATEWAY_STATUS_RESTART_WHEN_NO_SESSIONS = 2
GATEWAY_STATUS_STOP_WHEN_NO_SESSIONS = 3
GATEWAY_STATUS_STOPPING = 4
GATEWAY_STATUS_RESTARTING = 5


GATEWAY_STATUS_STR = {0:"unknown", 1:"running", 2:"restart_when_no_sessions", 3:"stop_when_no_sessions", 4:"stopping", 5:"restarting"}


class GOnService(object):
    def __init__(self, sid, title, ip, port, status):
        self.sid = sid
        self.title = title
        self.ip = ip
        self.port = port
        self.status = status
        
    def get_status_as_string(self):
        return GATEWAY_STATUS_STR[self.status]
        


class ManagementMessageSessionElementControlClient(session_element.ManagementMessageSessionElement):
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, gateway_sid, gateway_title, gateway_listen_ip, gateway_listen_port, service_recieve_callback):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id)
        self.server_sid_not_verifyed = gateway_sid
        self.server_sid = None
        self.server_title = gateway_title
        self.server_listen_ip = gateway_listen_ip
        self.server_listen_port = gateway_listen_port
        self.service_recieve_callback = service_recieve_callback
        self._status = GATEWAY_STATUS_UNKNOWN
    
    def start(self):
        """
        Called when connected and session is ready
        """
        if self.service_recieve_callback:
            self.server_sid_not_verifyed = self.service_recieve_callback.get_server_sid()
        session_count = self.service_recieve_callback.get_session_count()
        self.tunnelendpoint_remote('remote_verify_server_sid', server_sid=self.server_sid_not_verifyed, server_title=self.server_title, server_listen_ip=self.server_listen_ip, server_listen_port=self.server_listen_port, session_count=session_count)
        self.set_status(GATEWAY_STATUS_RUNNING)
    
    def reset(self):
        self.service_recieve_callback = None
        session_element.ManagementMessageSessionElement.reset(self)

    def remote_verify_server_sid_response(self, server_sid):
        self.server_sid = server_sid
        if self.server_sid != self.server_sid_not_verifyed:
            self.service_recieve_callback.server_sid_changed(self.server_sid_not_verifyed, self.server_sid)

    def get_server_sid(self):
        return self.server_sid

    def set_status(self, status):
        self._status = status
        self.tunnelendpoint_remote('remote_set_status', status=self._status)
        
    def remote_service_stop(self, when_no_sessions):
        if self.service_recieve_callback is not None:
            self.service_recieve_callback.management_service_stop(when_no_sessions)

    def ping(self):
        pass
    
    def remote_service_restart(self, when_no_sessions):
        if self.service_recieve_callback is not None:
            self.service_recieve_callback.management_service_restart(when_no_sessions)
        
    def remote_session_close(self, unique_session_id):
        if self.service_recieve_callback is not None:
            self.service_recieve_callback.management_service_session_close(unique_session_id)

    def remote_session_recalculate_menu(self, unique_session_id):
        if self.service_recieve_callback is not None:
            self.service_recieve_callback.management_service_session_recalculate_menu(unique_session_id)

    def remote_accept_sessions(self, do_accept):
        if self.service_recieve_callback is not None:
            self.service_recieve_callback.management_service_accept_sessions(do_accept)



class AuthCallback(object):
    def auth_verify_server_sid(self, server_sid, session_count):
        """
        Expected to return a valid server_sid
        """
        return server_sid


class ManagementMessageSessionElementControlServer(session_element.ManagementMessageSessionElement):
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, auth_callback):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id)
        self._server_sid = None
        self._server_title = "N/A"
        self._server_listen_ip = ""
        self._server_listen_port = 0
        self.com_session = com_session
        self._status = GATEWAY_STATUS_UNKNOWN
        self.auth_callback = auth_callback
        ip_remote = self.com_session.get_ip_remote()
        if ip_remote is None:
            ip_remote = ('', 0)
        (self._remote_ip, self._remote_port) = ip_remote
    
    def start(self):
        """
        Called when connected and session is ready
        """
        pass

    def reset(self):
        self.auth_callback = None
        session_element.ManagementMessageSessionElement.reset(self)

    def remote_verify_server_sid(self, server_sid, server_title, server_listen_ip, server_listen_port, session_count):
        self._server_title = server_title
        self._server_listen_ip = server_listen_ip
        self._server_listen_port = server_listen_port
        self._server_sid = self.auth_callback.auth_verify_server_sid(server_sid, session_count)
        self.tunnelendpoint_remote('remote_verify_server_sid_response', server_sid=self._server_sid)

    def remote_set_status(self, status):
        self._status = status

    def get_remote_service_info(self):
        return GOnService(self._server_sid, self._server_title, self._remote_ip, self._server_listen_port, self._status)

    def get_server_sid(self):
        return self._server_sid

    def ping(self):
        self.tunnelendpoint_remote('ping')

    def service_stop(self, when_no_sessions):
        self.tunnelendpoint_remote('remote_service_stop', when_no_sessions=when_no_sessions)
        if when_no_sessions:
            self._status = GATEWAY_STATUS_STOP_WHEN_NO_SESSIONS
        else:
            self._status = GATEWAY_STATUS_STOPPING
    
    def service_restart(self, when_no_sessions):
        self.tunnelendpoint_remote('remote_service_restart', when_no_sessions=when_no_sessions)
        if when_no_sessions:
            self._status = GATEWAY_STATUS_RESTART_WHEN_NO_SESSIONS
        else:
            self._status = GATEWAY_STATUS_RESTARTING
        
    def session_close(self, unique_session_id):
        self.tunnelendpoint_remote('remote_session_close', unique_session_id=unique_session_id)

    def session_recalculate_menu(self, unique_session_id):
        self.tunnelendpoint_remote('remote_session_recalculate_menu', unique_session_id=unique_session_id)

    def accept_sessions(self, do_accept):
        self.tunnelendpoint_remote('remote_accept_sessions', do_accept=do_accept)
