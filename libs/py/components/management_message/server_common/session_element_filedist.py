"""
This file contains the client and server side of file distribution session element
"""
import os.path
import StringIO
import time
import string

import lib.checkpoint
import lib.utility
import lib.utility.lockfile

import lib.gpm.gpm_env
import lib.gpm.gpm_spec
import lib.gpm.gpm_verify

import session_element
import components.cpm.client_gateway.cpm_file_downloader
import components.cpm.server_gateway.cpm_file_downloader
import components.cpm.server_gateway.cpm_download_manager

from components.management_message import *



class ManagementMessageSessionElementFiledistClient(session_element.ManagementMessageSessionElement, components.cpm.client_gateway.cpm_file_downloader.CPMFileDownloaderCB):
    """
    Cerver side (Gateway) of filedist element
    """
    STATE_INIT = 0
    STATE_RUNNING = 1
    STATE_CLOSED = 2

    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, filedist_root, filedist_enabled, service_recieve_callback, server_gateway_root):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, )
        self.filedist_root = filedist_root
        self.filedist_enabled = filedist_enabled
        self.service_recieve_callback = service_recieve_callback
        self._download_tunnel_id = None
        self._state = ManagementMessageSessionElementFiledistClient.STATE_INIT
        
        self.gpm_build_env = lib.gpm.gpm_env.GpmBuildEnv()
        self.gpm_build_env.temp_root = None
        self.gpm_build_env.build_root = self.filedist_root
        self.gpm_build_env.dest_root = self.filedist_root
        self.gpm_build_env.add_path_to_subst_dict('gon_server_gateway_root', server_gateway_root)

    def start(self):
        self._state = ManagementMessageSessionElementFiledistClient.STATE_RUNNING

    def reset(self):
        self._state = ManagementMessageSessionElementFiledistClient.STATE_CLOSED
        session_element.ManagementMessageSessionElement.reset(self)

    def remote_verify_files(self, filedist_files):
        if self._state not in [ManagementMessageSessionElementFiledistClient.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("FiledistClient.remote_verify_files.invalid_state", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return 
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        
        gpm_files_file = StringIO.StringIO(filedist_files)
        gpm_files = lib.gpm.gpm_spec.GpmFiles.parse_from_file(gpm_files_file)

        verifyer = lib.gpm.gpm_verify.GpmVerify(self.gpm_build_env, gpm_error_handler, gpm_files)
        filelist = verifyer.verify()
        if len(filelist) > 0:
            self.checkpoint_handler.Checkpoint("FiledistClient.files_need_update", MODULE_ID, lib.checkpoint.DEBUG, count=len(filelist))
            self.tunnelendpoint_remote('remote_verify_files_response', filelist=filelist)

    def remote_download_start(self):
        if not self.filedist_enabled:
            self.checkpoint_handler.Checkpoint("FiledistClient.remote_download_start.not_enabled", MODULE_ID, lib.checkpoint.INFO)
            return

        self.service_recieve_callback.management_service_accept_sessions(do_accept=False)
            
        if self._state not in [ManagementMessageSessionElementFiledistClient.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("FiledistClient.remote_download_start.invalid_state", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return 

        self._download_tunnel_id = 1
        self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, self._download_tunnel_id)
        self._downloader = components.cpm.client_gateway.cpm_file_downloader.CPMFileDownloader(self.async_service, self.checkpoint_handler, self._download_tunnelendpoint, self._download_tunnel_id, self.filedist_root, self)
        self._downloader.start()

    def file_download_all_start(self, download_id, number_of_files, tick_value_max_total):
        self.checkpoint_handler.Checkpoint("FiledistClient.downloading.all_start", MODULE_ID, lib.checkpoint.INFO, number_of_files=number_of_files)

    def file_download_start(self, download_id, filename, tick_value_max):
        self.checkpoint_handler.Checkpoint("FiledistClient.downloading.file", MODULE_ID, lib.checkpoint.INFO, filename=filename)

    def file_download_tick(self, download_id, tick_value):
        pass
    
    def file_download_done(self, download_id):
        pass

    def file_download_verifying(self, download_id):
        pass

    def file_download_all_done(self, download_id):
        self.checkpoint_handler.Checkpoint("FiledistClient.downloading.all_done", MODULE_ID, lib.checkpoint.INFO)
        self._download_cleanup()

    def file_download_error(self, download_id, message, tell_remote=True):
        self.checkpoint_handler.Checkpoint("FiledistClient.downloading.error", MODULE_ID, lib.checkpoint.ERROR, message=message)
        self._download_cleanup()
    
    def file_download_canceling(self, download_id):
        self._download_cleanup()

    def file_download_canceled(self, download_id):
        self._download_cleanup()

    def _download_cleanup(self):
        self.service_recieve_callback.management_service_accept_sessions(do_accept=True)
        if self._download_tunnel_id != None:
            self.remove_tunnelendpoint_tunnel(self._download_tunnel_id)
            self._downloader = None
            self._download_tunnel_id = None


class FiledistManager(object):
    """
    The Filedist manager calculate filelist based on filedefs specification file found in root of dist folder.
    
    If the fildist file is missing it is generated, and if the checksum of the file is different from cashed version
    all callback_clients are notified.
    """
    FILENAME_FILEDEFS = 'gon_server_gateway.filedefs.xml'
    FILENAME_FILES = 'gon_server_gateway.files.xml'
    FILENAME_FILES_LOCK = 'gon_server_gateway.files.xml.lock'
    
    STATE_INIT = 0
    STATE_RUNNING = 1
    STATE_CLOSING = 2
    STATE_CLOSED = 3
    
    def __init__(self, async_service, checkpoint_handler, filedist_root, session_mutex, tickets_max, check_interval_sec, gateway_server_path):
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.filedist_root = filedist_root
        self.download_manager = components.cpm.server_gateway.cpm_download_manager.DownloadManager(async_service, session_mutex, checkpoint_handler, tickets_max)
        self._callback_clients = set()
        self._files_file_content = None
        self._files_file_checksum = None
        self.session_mutex = session_mutex
        self.check_interval_sec = check_interval_sec
        self._state = FiledistManager.STATE_INIT
        self._lock = lib.utility.lockfile.FileLock(os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES_LOCK))
        self.gateway_server_path = gateway_server_path

    def _generate_files_from_filedefs(self):
        try:
            self._lock.acquire()
            gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
            gpm_build_env = lib.gpm.gpm_env.GpmBuildEnv()
            gpm_build_env.temp_root = None
            gpm_build_env.build_root = self.filedist_root
            gpm_build_env.dest_root = self.filedist_root
            gpm_build_env.add_path_to_subst_dict('gon_server_gateway_root', self.gateway_server_path)
            
            filedefs_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILEDEFS)
            if os.path.isfile(filedefs_filename_abs):
                
                filedefs_file = open(filedefs_filename_abs, 'r')
                filedefs = lib.gpm.gpm_spec.GpmFileDefs.parse_from_file(filedefs_file)
                filedefs_file.close()
                
                filelist = filedefs.generate_filelist(gpm_build_env, gpm_error_handler)
                files = lib.gpm.gpm_spec.GpmFiles.build_from_filelist(filelist)
    
                files_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES)
                files_file = open(files_filename_abs, 'w')
                files.to_tree().write(files_file)
                files_file.close()
            else:
                self.checkpoint_handler.Checkpoint("FiledistManager.file_not_found", MODULE_ID, lib.checkpoint.ERROR, filename=filedefs_filename_abs)
            if not gpm_error_handler.ok():
                self.checkpoint_handler.CheckpointMultilineMessage("FiledistManager.not_ok", MODULE_ID, lib.checkpoint.WARNING, gpm_error_handler.dump_as_string())
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("FiledistManager", MODULE_ID, lib.checkpoint.CRITICAL)
        else:
            self._lock.release()

    def start(self):
        if self._state not in [FiledistManager.STATE_INIT]:
            self.checkpoint_handler.Checkpoint("FiledistManager.start.invalid_state", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return
        self._state = FiledistManager.STATE_RUNNING
        files_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES)
        if not os.path.isfile(files_filename_abs):
            self._generate_files_from_filedefs()
        self._janitor_start()
    
    def stop_and_wait(self):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("FiledistManager.stop_and_wait.invalid_state", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return
        self._state = FiledistManager.STATE_CLOSING
        self.download_manager.stop_and_wait()
        while self._state != FiledistManager.STATE_CLOSED:
            time.sleep(1)

    def get_filedist_files(self):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("FiledistManager.get_filedist_files.invalid_state", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return None
        files_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES)
        if not os.path.isfile(files_filename_abs):
            self._generate_files_from_filedefs()
            
        reread_files_file = False
        if self._files_file_content is None:
            reread_files_file = True
        else:
            if self._files_file_checksum != lib.utility.calculate_checksum_for_file(files_filename_abs):
                reread_files_file = True
        
        if reread_files_file:
            self._files_file_content = None
            self._files_file_checksum = None
            files_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES)
            if os.path.isfile(files_filename_abs):
                files_file = open(files_filename_abs, 'r')
                self._files_file_content = files_file.read()
                files_file.close()
                self._files_file_checksum = lib.utility.calculate_checksum_for_file(files_filename_abs)
        return self._files_file_content

    def add_callback_client(self, callback_client):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            return
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_add_callback_client', callback_client)        

    def _add_callback_client(self, callback_client):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            return
        self._callback_clients.add(callback_client)

    def remove_callback_client(self, callback_client):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            return
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_remove_callback_client', callback_client)        

    def _remove_callback_client(self, callback_client):
        if self._state not in [FiledistManager.STATE_RUNNING]:
            return None
        self._callback_clients.discard(callback_client)
        
    def _janitor_start(self): 
        if self._state not in [FiledistManager.STATE_RUNNING]:
            return None
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, self.check_interval_sec, 0, self, '_janitor')        

    def _janitor(self): 
        if self._state in [FiledistManager.STATE_CLOSING]:
            self._state = FiledistManager.STATE_CLOSED
            return
        files_filename_abs = os.path.join(self.filedist_root, FiledistManager.FILENAME_FILES)
        if not os.path.isfile(files_filename_abs):
            self._generate_files_from_filedefs()
            
            for callback_client in self._callback_clients:
                callback_client.verify_files()
        self._janitor_start()
        
    @classmethod
    def files_update(cls, checkpoint_handler, filedist_root):
        checkpoint_handler.Checkpoint("FiledistManager.files_update", MODULE_ID, lib.checkpoint.DEBUG)
        try:
            lock = lib.utility.lockfile.FileLock(os.path.join(filedist_root, FiledistManager.FILENAME_FILES_LOCK))
            lock.acquire()
        
            files_filename_abs = os.path.join(filedist_root, FiledistManager.FILENAME_FILES)
            if os.path.isfile(files_filename_abs):
                os.unlink(files_filename_abs)
        except:
            checkpoint_handler.CheckpointExceptionCurrent("FiledistManager", MODULE_ID, lib.checkpoint.DEBUG)
        else:
            lock.release()

    
class ManagementMessageSessionElementFiledistServer(session_element.ManagementMessageSessionElement, components.cpm.server_gateway.cpm_download_manager.DownloadManagerCB, components.cpm.server_gateway.cpm_file_downloader.CPMFileDownloaderCB):
    """
    Server side (Managment) of filedist element
    """
    STATE_INIT = 0
    STATE_READY_TO_VERIFY = 1
    STATE_DOWNLOADING = 2
    STATE_DONE = 3
    
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id, recieve_callback, filedist_manager, server_gateway_root=None):
        session_element.ManagementMessageSessionElement.__init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id)
        self.recieve_callback = recieve_callback
        self.filedist_manager = filedist_manager
        self.server_gateway_root = server_gateway_root
        self._state = ManagementMessageSessionElementFiledistServer.STATE_INIT
        self._download_tunnel_id = None
    
    def start(self):
        if self._state not in [ManagementMessageSessionElementFiledistServer.STATE_INIT]:
            self.checkpoint_handler.Checkpoint("FiledistServer.start.invalid_state", MODULE_ID, lib.checkpoint.DEBUG, state=self._state)
            return
        self.filedist_manager.add_callback_client(self)
        self._state = ManagementMessageSessionElementFiledistServer.STATE_READY_TO_VERIFY
        self.verify_files()

    def reset(self):
        self.filedist_manager.remove_callback_client(self)
        self._state = ManagementMessageSessionElementFiledistServer.STATE_DONE
        self.recieve_callback = None
        session_element.ManagementMessageSessionElement.reset(self)

    def verify_files(self):
        if self._state not in [ManagementMessageSessionElementFiledistServer.STATE_READY_TO_VERIFY]:
            self.checkpoint_handler.Checkpoint("FiledistServer.verify_files.not_ready_to_verify", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return
        filedist_files = self.filedist_manager.get_filedist_files()
        if filedist_files is not None:
            self.tunnelendpoint_remote('remote_verify_files', filedist_files=filedist_files)
        
    def _expand_filename(self, filename):
        subst_dict = {}
        if self.server_gateway_root is not None:
            subst_dict['gon_server_gateway_root'] = self.server_gateway_root
        return string.Template(filename).safe_substitute(subst_dict)
    
    def remote_verify_files_response(self, filelist):
        """
        Called as response to remote_verify_files with a list of files needed to be downloaded
        """
        if self._state not in [ManagementMessageSessionElementFiledistServer.STATE_READY_TO_VERIFY]:
            self.checkpoint_handler.Checkpoint("FiledistServer.remote_verify_files_response.not_ready_to_verify", MODULE_ID, lib.checkpoint.WARNING, state=self._state)
            return
        self._state = ManagementMessageSessionElementFiledistServer.STATE_DOWNLOADING

        self._download_filenames = []
        self._download_filenames_client = []
        for (why, abs_filename_client, rel_filename) in filelist:
            self._download_filenames_client.append(abs_filename_client)
            rel_filename_server = self._expand_filename(rel_filename)
            if rel_filename_server.startswith('/') or rel_filename_server.startswith('\\'):
                rel_filename_server = rel_filename_server[1:]
            self._download_filenames.append(rel_filename_server)
        self.filedist_manager.download_manager.request_ticket(self)

    def download_manager_ticket_granted_from_other_thread(self, download_ticket):
        if self._state not in [ManagementMessageSessionElementFiledistServer.STATE_DOWNLOADING]:
            self.checkpoint_handler.Checkpoint("FiledistServer.ticket_granted.invalid_state", MODULE_ID, lib.checkpoint.ERROR, state=self._state)
            return
        self._download_tunnel_id = 1
        self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, self._download_tunnel_id)
        self._downloader = components.cpm.server_gateway.cpm_file_downloader.CPMFileDownloader(self.async_service, self.checkpoint_handler, self._download_tunnelendpoint, download_ticket, self._download_tunnel_id, self, self.filedist_manager.download_manager, self.filedist_manager.filedist_root, self._download_filenames, self._download_filenames_client)
        self.tunnelendpoint_remote('remote_download_start')

    def download_manager_ticket_timeout_from_other_thread(self, download_ticket):
        self._download_cleanup()
        
    def file_download_all_done(self, download_id):
        self._download_cleanup()

    def file_download_cancled(self, download_id):
        self._download_cleanup()

    def file_download_error(self, download_id, message, tell_remote=True):
        self.checkpoint_handler.Checkpoint("FiledistServer.downloading.error", MODULE_ID, lib.checkpoint.ERROR, message=message)
        self._download_cleanup()
    
    def file_download_ticket_expired(self, download_id):
        self.checkpoint_handler.Checkpoint("FiledistServer.ticket_expired", MODULE_ID, lib.checkpoint.WARNING)
        self._download_cleanup()

    def _download_cleanup(self):
        if self._download_tunnel_id is not None:
            self.remove_tunnelendpoint_tunnel(self._download_tunnel_id)
            self._downloader = None
            self._download_tunnel_id = None
        self._state = ManagementMessageSessionElementFiledistServer.STATE_READY_TO_VERIFY




