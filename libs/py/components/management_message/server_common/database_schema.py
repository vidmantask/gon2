"""
This file contains the database schema for the Management message component
"""
from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api

dbapi = schema_api.SchemaFactory.get_creator("management_message_component")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)


table_message = dbapi.create_table("message",
                                   schema_api.Column('sender_id', schema_api.String(100)),
                                   schema_api.Column('sink_id', schema_api.String(100)),
                                   schema_api.Column('package', schema_api.LargeBinary()))



schema_api.SchemaFactory.register_creator(dbapi)


class Message(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_message)
Message.map_to_table()


def create_demo_data():
    with database_api.Transaction() as t:
        t.delete_all(Message)
