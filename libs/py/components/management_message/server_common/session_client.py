"""
This file contains the classes for handling client side of communication between 
management server and gateway servers.
"""
import Queue
import time
import os
import os.path
import pickle
import base64
import sys

import lib.checkpoint
import lib_cpp.communication

from  components.management_message import MODULE_ID
import components.communication.session
import components.communication.session_manager

import components.communication.tunnel_endpoint_base

import session_element_control
import session_element_message
import session_element_filedist



class ManagementMessageClientSession(components.communication.session.APISessionEventhandlerReady):
    """
    This class represent the client side of communication between gateway servers and management server.
    """
    def __init__(self, checkpoint_handler, com_session, async_service, dictionary, gateway_sid, gateway_title, gateway_listen_ip, gateway_listen_port, filedist_root, service_recieve_callback, filedist_enabled, server_gateway_root):
        components.communication.session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self.dictionary = dictionary
        self.com_session = com_session
        self.com_session.set_eventhandler_ready(self)
        self.filedist_enabled = filedist_enabled
        self._connected = False
        self._element_control = session_element_control.ManagementMessageSessionElementControlClient(com_session, async_service, checkpoint_handler, 1, gateway_sid, gateway_title, gateway_listen_ip, gateway_listen_port, service_recieve_callback)
        self._element_message = session_element_message.ManagementMessageSessionElementMessageClient(com_session, async_service, checkpoint_handler, 2)
        self._element_filedist = session_element_filedist.ManagementMessageSessionElementFiledistClient(com_session, async_service, checkpoint_handler, 3, filedist_root, filedist_enabled, service_recieve_callback, server_gateway_root)

    def session_state_ready(self):
        unique_session_id = self.com_session.get_unique_session_id()
        remote_unique_session_id = self.com_session.get_remote_unique_session_id()
        self.checkpoint_handler.Checkpoint("ManagementMessageClientSession.session_ready", MODULE_ID, lib.checkpoint.INFO, remote_unique_session_id=remote_unique_session_id)
        self.com_session.read_start_state_ready()

        self._element_control.start_when_connected();
        self._element_message.start_when_connected();
        self._element_filedist.start_when_connected();
        self._connected = True

    def session_state_closed(self):
        remote_unique_session_id = self.com_session.get_remote_unique_session_id()
        self.checkpoint_handler.Checkpoint("ManagementMessageClientSession.session_closed", MODULE_ID, lib.checkpoint.INFO, remote_unique_session_id=remote_unique_session_id)
        self._connected = False
        self._element_control.reset()
        self._element_message.reset()
        self._element_filedist.reset()
    
    def session_state_key_exchange(self):
        pass
    
    def session_user_signal(self, signal_id, message):
        pass

    def is_closed(self):
        return self.com_session.is_closed()

    def is_connected(self):
        return self._connected

    def is_connected_and_authorized(self):
        return self._connected and self._element_control.get_server_sid() is not None

    def send_message(self, message):
        self._element_message.send_message(message)



class ManagementMessageSendInterface(object):
    def send_message(self, message):
        pass


class ManagementServiceRecieveCalllback(object):
    def management_service_stop(self, when_no_sessions):
        pass    
    
    def management_service_restart(self, when_no_sessions):
        pass    
        
    def management_service_session_close(self, unique_session_id):
        pass
        
    def management_service_session_recalculate_menu(self, unique_session_id):
        pass    

    def management_service_accept_sessions(self, do_accept):
        pass    
    
    def server_sid_changed(self, old_server_sid, new_server_sid):
        pass


class ManagementMessageClientSessionManager(components.communication.session_manager.APISessionManagerEventhandler, ManagementMessageSendInterface):
    def __init__(self, async_service, checkpoint_handler, dictionary, license_handler, connect_ip, connect_port, client_knownsecret, gateway_sid, gateway_title, gateway_listen_ip, gateway_listen_port, filedist_enabled, filedist_root, message_queue_offline_filename, server_gateway_root):
        components.communication.session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.dictionary = dictionary
        self.license_handler = license_handler
        self.connect_ip = connect_ip
        self.connect_port = connect_port
        self._com_session_manager = components.communication.session_manager.APISessionManagerClient_create(async_service.get_com_async_service(), self, checkpoint_handler.checkpoint_handler, client_knownsecret, lib_cpp.communication.ApplProtocolType.PYTHON)
        self._session = None
        self._is_stopping = False    
        self._reconnect_janitor_is_running = False
        self._send_message_queue_janitor_is_running = False
        self._send_message_queue = Queue.Queue()
        self.gateway_sid = gateway_sid
        self.gateway_title = gateway_title
        self.gateway_listen_ip = gateway_listen_ip
        self.gateway_listen_port = gateway_listen_port
        self.filedist_root = filedist_root
        self.service_recieve_callback = None
        self.filedist_enabled = filedist_enabled
        self.message_queue_offline_filename = message_queue_offline_filename
        self.server_gateway_root = server_gateway_root

    def set_service_recieve_callback(self, service_recieve_callback):
        self.service_recieve_callback = service_recieve_callback
       
    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info
    
    def session_manager_connecting(self, connection_id, connection_title):
        pass

    def session_manager_connecting_failed(self, connection_id):
        pass
    
    def session_manager_failed(self, message):
        self.checkpoint_handler.Checkpoint("ManagementMessageClientSessionManager::session_manager_failed", MODULE_ID, lib.checkpoint.WARNING, message=message)
            
    def session_manager_session_created(self, connection_id, com_session):
        session_id = com_session.get_session_id()
        self._session = ManagementMessageClientSession(self.checkpoint_handler, com_session, self.async_service, self.dictionary, self.gateway_sid, self.gateway_title, self.gateway_listen_ip, self.gateway_listen_port, self.filedist_root, self.service_recieve_callback, self.filedist_enabled, self.server_gateway_root)
                
    def session_manager_session_closed(self, session_id):
        self._session = None
            
    def session_manager_closed(self):
        with self.checkpoint_handler.CheckpointScope("ManagementMessageClientSessionManager::session_manager_closed", MODULE_ID, lib.checkpoint.DEBUG):
            self._session = None
 
    def is_connected(self):
        return self._session != None and self._session.is_connected()
    
    def is_connected_and_authorized(self):
        return self._session != None and self._session.is_connected_and_authorized()
        
    def start(self):
        self._reconnect_janitor_start(janitor_sleep_sec=0)
   
    def _reconnect_janitor_start(self, janitor_sleep_sec=20):
        if not self._reconnect_janitor_is_running:
            self._reconnect_janitor_is_running = True
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session_manager.get_mutex(), 0, janitor_sleep_sec, 0, self, '_reconnect_janitor')
        
    def _reconnect_janitor(self):
        self.checkpoint_handler.Checkpoint("ManagementMessageClientSessionManager::_reconnect_janitor", MODULE_ID, lib.checkpoint.DEBUG)
        self._reconnect_janitor_is_running = False
        if self._is_stopping:
            return
        if not self.is_connected():
            self._com_session_manager.set_eventhandler(self)
            self._com_session_manager.add_server_connection(self.connect_ip, self.connect_port)
            self._com_session_manager.start()
        self._reconnect_janitor_start()
   
    def stop_and_wait(self):
        with self.checkpoint_handler.CheckpointScope("ManagementMessageClientSessionManager::stop", MODULE_ID, lib.checkpoint.DEBUG):
            if not self._is_stopping:
                self._is_stopping = True
                
                if not self._com_session_manager.is_closed():
                    self._com_session_manager.close_start()
                
                while not self._com_session_manager.is_closed():
                    self.checkpoint_handler.CheckpointScope("ManagementMessageClientSessionManager::stop.waiting", MODULE_ID, lib.checkpoint.DEBUG)
                    time.sleep(0.5)

    def send_message(self, message):
        self._send_message_queue.put_nowait(message)
        if self._is_stopping:
            self._send_message_queue_janitor_append_to_file()
            return
        self._send_message_queue_janitor_start()
             
    def _send_message_queue_janitor_start(self, janitor_sleep_sec=0):
        if not self._send_message_queue_janitor_is_running:
            self._send_message_queue_janitor_is_running = True
            self.async_service.sleep_start_mutex(self.checkpoint_handler, self._com_session_manager.get_mutex(), 0, janitor_sleep_sec, 0, self, '_send_message_queue_janitor')

    def _send_message_queue_janitor(self):
        self._send_message_queue_janitor_is_running = False
        if self._is_stopping:
            self._send_message_queue_janitor_append_to_file()
            return
        if self.is_connected_and_authorized():
            self._send_message_queue_janitor_send_from_file()
            while not self._send_message_queue.empty():
                self._session.send_message(self._send_message_queue.get())
                self._send_message_queue.task_done()
        else:
            self._send_message_queue_janitor_append_to_file()
            self._send_message_queue_janitor_start(janitor_sleep_sec=10)

    def _send_message_queue_janitor_append_to_file(self):
        if self._send_message_queue.empty():
            return
        try:
            self.checkpoint_handler.CheckpointScope("send_message_queue_janitor_append_to_file", MODULE_ID, lib.checkpoint.DEBUG, message_count=self._send_message_queue.qsize())
            with open(self.message_queue_offline_filename, 'a') as queue_offline_file:
                while not self._send_message_queue.empty():
                    message = self._send_message_queue.get()
                    message_encoded = base64.encodestring(pickle.dumps(message)).replace("\n", "")
                    queue_offline_file.write('%s\n' % message_encoded)
                    self._send_message_queue.task_done()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self._checkpoint_handler.CheckpointException("send_message_queue_janitor_append_to_file.failed", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            

    def _send_message_queue_janitor_send_from_file(self):
        try:
            if os.path.isfile(self.message_queue_offline_filename):
                self.checkpoint_handler.CheckpointScope("send_message_queue_janitor_send_from_file", MODULE_ID, lib.checkpoint.DEBUG)
                with open(self.message_queue_offline_filename, 'r') as queue_offline_file:
                    message_encoded = queue_offline_file.readline()
                    while message_encoded != "":
                        message = pickle.loads(base64.decodestring(message_encoded))
                        self._session.send_message(message)
                        message_encoded = queue_offline_file.readline()
                os.remove(self.message_queue_offline_filename)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self._checkpoint_handler.CheckpointException("send_message_queue_janitor_send_from_file", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
