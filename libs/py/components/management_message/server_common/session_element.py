"""
This file contains common functionality for session elements
"""
import components.communication.tunnel_endpoint_base


class ManagementMessageSessionElement(components.communication.tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    def __init__(self, com_session, async_service, checkpoint_handler, tunnel_endpoint_id):
        tunnel_endpoint = components.communication.tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, checkpoint_handler)
        com_session.add_tunnelendpoint_tunnel(tunnel_endpoint_id, tunnel_endpoint)
        components.communication.tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.com_session = com_session
        self.tunnel_endpoint_id = tunnel_endpoint_id
        self._start_allowed_from_session = False
        self._tunnelendpoint_connected = False
    
    def get_session_id(self):
        return self.com_session.get_unique_session_id()
    
    def tunnelendpoint_connected(self):
        self._tunnelendpoint_connected = True
        if self._start_allowed_from_session:
            self.start()
    
    def start_when_connected(self):
        self._start_allowed_from_session = True
        if self._tunnelendpoint_connected:
            self.start()

    def start(self):
        """
        Called when connected and session is ready
        """
        pass

    def reset(self):
#TODO: not implemented in API yet 
#        self.com_session.remove_tunnelendpoint_tunnel(self.tunnel_endpoint_id)
        self.com_session = None
        self.reset_tunnelendpoint()
