"""
Unittest of management_message component
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import os.path

import components.management_message.server_common.session_client as session_client
import components.management_message.server_common.session_server as session_server
import components.management_message.server_gateway.session_send 
import components.management_message.server_management.session_recieve

import components.management_message.server_common.session_element_filedist



class ManagementMessageTest(unittest.TestCase):
    def setUp(self):
        self.server_knownsecret = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        self.client_knownsecret = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.client_filedist_root = giri_unittest.mkdtemp()

        gpm_filedefs_str = """<?xml version="1.0" encoding="UTF-8"?>
<filedefs>
  <include source="${gpm_build_root}/" dest="/"/>
</filedefs>
"""
        filedefs_filename = os.path.join(self.dev_installation.get_installation_root(), components.management_message.server_common.session_element_filedist.FiledistManager.FILENAME_FILEDEFS)
        filedefs_file = open(filedefs_filename, 'w')
        filedefs_file.write(gpm_filedefs_str)
        filedefs_file.close()
    
    
    def test_001(self):
        class TestReciever(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
            def __init__(self, reciever_id):
                components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, giri_unittest.get_checkpoint_handler(), reciever_id)
                self.called = 0

            def my_fun_call(self, server_sid, arg1, arg2):
                print "my_fun_call called", server_sid, arg1, arg2
                self.called += 1
                
            def got_all_messages(self):
                return self.called == 2

        
        message_reciever = components.management_message.server_management.session_recieve.ManagementMessageSessionRecieve(giri_unittest.get_checkpoint_handler())
        test_reciever = TestReciever('my_reciever')
        message_reciever.add_reciever(test_reciever) 
        
        server_filedist_root = self.dev_installation.get_installation_root()
        client_filedist_root = self.client_filedist_root
        tickets_max = 2
        check_interval_sec = 10
        
        license_handler = None
        message_queue_offline_filename = self.dev_installation.get_message_queue_offline_filename()
        internal_ws_connector = None        
        
        management_message_server_manager = session_server.ManagementMessageServerSessionManager(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), self.server_knownsecret, giri_unittest.get_dictionary(), internal_ws_connector, '127.0.0.1', 13946, 1, message_reciever, server_filedist_root, tickets_max, check_interval_sec)
        gateway_sid = 42
        gateway_title = "Gateway-42"
        gateway_listen_ip = '127.0.0.1'
        gateway_listen_port = 13947
        management_message_client_manager = session_client.ManagementMessageClientSessionManager (giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), giri_unittest.get_dictionary(), license_handler, '127.0.0.1', 13946, self.client_knownsecret, gateway_sid, gateway_title, gateway_listen_ip, gateway_listen_port, False, client_filedist_root, message_queue_offline_filename)

        management_message_server_manager.start()
        management_message_client_manager.start()

        management_message_session_send = components.management_message.server_gateway.session_send.ManagementMessageSessionSend(giri_unittest.get_checkpoint_handler(), management_message_client_manager)
        management_message_session_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender('my_reciever', management_message_session_send)


        time.sleep(3)
        management_message_session_sender.message_remote('my_fun_call', arg1='hej', arg2='davs')
        management_message_session_sender.message_remote('my_fun_call', arg1='hej', arg2='davs igen')
        
        giri_unittest.wait_until_with_timeout(test_reciever.got_all_messages, 10000)
        self.assertEqual(test_reciever.got_all_messages(), True)

        management_message_client_manager.stop_and_wait()
        management_message_server_manager.stop_and_wait()


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
