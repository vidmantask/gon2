# -*- coding: latin-1 -*-
from __future__ import with_statement
import unittest
import os
import math
import datetime
import binascii
from lib import giri_unittest
from components.database.server_common.schema_api import *
from components.database.server_common.database_api import *
from components.database.server_common.connection_factory import ConnectionFactory
from components.database.server_common import schema_api



class SchemaAPITest(unittest.TestCase):
    
    
    def setUp(self):
        self.work_folder = giri_unittest.mkdtemp()
        
#        self.connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)
        self.connection = ConnectionFactory(None).create_connection('sqlite:///%s/unittest_schema_api.sqlite' % self.work_folder, True)
#        self.connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/test', True)
        #self.connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/atest_upgrade', True)
#        self.connection = ConnectionFactory(None).create_connection('mysql://root@192.168.42.228/gon_56_1?charset=latin1', True)
#        self.connection = ConnectionFactory(None).create_connection('mysql://gon_db_user:GiriGiri007@192.168.42.238/test', True, encoding="latin1")

        
        clear_database(self.connection)
        sqlalchemy.orm.clear_mappers()
        
    def tearDown(self):
        try:
            backup_path = self.work_folder
            backup_files = os.listdir(backup_path)
            for backup_file in backup_files:
                (name, ext) = os.path.splitext(backup_file)
                if ext == '.xml' and name.startswith("schema_test"):
                    filename = os.path.join(backup_path, backup_file)
                    os.remove(filename)
        except:
            pass
    
        
    def test_upgrade2(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
        
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

        class T3(PersistentObject):
            pass
            
        
        mapper(T1, table1)
        mapper(T2, table2)
        mapper(T3, table3, [Relation(T2, 'owner', backref_property_name = 'email_adresses')])
        
        with Transaction() as t:
            t.add(T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int=200000))
            t.add(T2(name="Frodo"))
            p = t.add(T2(name="Saruman"))
            p.email_adresses.append(T3(email="saruman@microsoft.com"))
            t.add(T3(email="unwanted@unwanted.dk"))
            t3 = T3(email="frodo@mordor.org")
            t3.owner = t.get(T2, 1)
            
        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        table1 = creator_new.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                             Column('new_column', Integer, nullable=False, default=0)
                            )
    
        table2 = creator_new.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )
    
    
        table3 = creator_new.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             Column('yo', Text, default="Yo"),
                             )

        table4 = creator_new.create_table("table4", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
         
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        creator.add_foreign_key_constraint(table2, 'new_ref', table1)

        conn = self.connection.connect()
        #t = conn.begin()
        transaction = Transaction(connection=conn) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.update_schema()
        
        class T1a(PersistentObject):
            pass

        mapper(T1a, updater.get_table("table3"))
        
        
        t1 = T1a(email="email@email.com")
        transaction.add(t1)
        
        
        transaction.commit()
        
        t3s = conn.execute(table3.select()).fetchall()
        print t3s


        
    def test_upgrade4(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
        
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        creator.add_foreign_key_constraint(table2, 'new_ref', table1)
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

        class T3(PersistentObject):
            pass
            
        
        mapper(T1, table1)
        mapper(T2, table2, [Relation(T1, 't1', backref_property_name = 't2s')])
        mapper(T3, table3, [Relation(T2, 'owner', backref_property_name = 'email_adresses')])
        
        with Transaction() as t:
            t1 = T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int=200000)
            t.add(t1)
            t.add(T2(name="Frodo", t1=t1))
            p = t.add(T2(name="Saruman"))
            p.email_adresses.append(T3(email="saruman@microsoft.com"))
            t.add(T3(email="unwanted@unwanted.dk"))
            t3 = T3(email="frodo@mordor.org")
            t3.owner = t.get(T2, 1)
            
        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        table1 = creator_new.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
    
        table3 = creator_new.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             Column('yo', Text, default="Yo"),
                             Column('new_column', Integer)
                             )

         
        creator.add_foreign_key_constraint(table3, 'new_column', table1)

        #t = conn.begin()
        transaction = Transaction(connection=self.connection) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.drop_table("table2")
        updater.update_schema()
        
        class T1a(PersistentObject):
            pass

        mapper(T1a, updater.get_table("table3"))
        
        
        t1 = T1a(email="email@email.com")
        transaction.add(t1)
        
        transaction.commit()
        
        conn = self.connection.connect()

        table_name = creator.create_table_name("table1")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),1)

        table_name = creator.create_table_name("table3")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),4)
        
        table_name = creator.create_table_name("table2")
        self.assertRaises(Exception, conn.execute, "select * from %s" % table_name)
        
        
        
    def test_upgrade5(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )
    
    
        creator.add_foreign_key_constraint(table2, 'new_ref', table1, ondelete="CASCADE")
        
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

            
        
        mapper(T1, table1)
        mapper(T2, table2, [Relation(T1, 't1', backref_property_name = 't2s')])
        
        with Transaction() as t:
            t1 = T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int=200000)
            t.add(t1)
            t.add(T2(name="Frodo", t1=t1))
            p = t.add(T2(name="Saruman"))
            
        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        #backup_path = None
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        table1 = creator_new.create_table("table1_new", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
    
        table2 = creator_new.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )

        creator_new.add_foreign_key_constraint(table2, 'new_ref', table1)
         

        conn = self.connection.connect()
        #t = conn.begin()
        transaction = Transaction(connection=conn) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.rename_table("table1", "table1_new")
        updater.update_schema()
        
        transaction.commit()
        
        conn = self.connection.connect()

        table_name = creator.create_table_name("table1_new")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),1)

        table_name = creator.create_table_name("table2")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),2)
        

    def test_upgrade6(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )
    
    
        creator.add_foreign_key_constraint(table2, 'new_ref', table1, ondelete="CASCADE")
        
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

            
        
        mapper(T1, table1)
        mapper(T2, table2, [Relation(T1, 't1', backref_property_name = 't2s')])
        
        with Transaction() as t:
            t1 = T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int=200000)
            t.add(t1)
            t.add(T2(name="Frodo", t1=t1))
            p = t.add(T2(name="Saruman"))
            
        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        #backup_path = None
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        table1 = creator_new.create_table("table1_new", 
                             Column('s20', String(20)),
                             Column('int_new', Integer),
                             Column('text', Text),
                            )
    
    
        table2 = creator_new.create_table("table2", 
                             Column('name', String(100)),
                             Column('ref_table1', Integer)
                             )

        creator_new.add_foreign_key_constraint(table2, 'ref_table1', table1)
         

        conn = self.connection.connect()
        #t = conn.begin()
        transaction = Transaction(connection=conn) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.rename_table("table1", "table1_new")
        updater.rename_column("table1_new", "int", "int_new")
        updater.rename_column("table2", "new_ref", "ref_table1")
        updater.rename_column("table1_new", "s10", "s20")
        updater.update_schema()
        
        transaction.commit()
        
        conn = self.connection.connect()

        table_name = creator.create_table_name("table1_new")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),1)

        table_name = creator.create_table_name("table2")
        result = conn.execute("select * from %s" % table_name)
        self.assertEquals(len(result.fetchall()),2)

    def test_upgrade7(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             Column('new_ref', Integer)
                             )

        table3 = creator.create_table("table3", 
                             Column('name', String(100), default="Yo"),
                             Column('ref', Integer, nullable=False)
                             )

        table0 = creator.create_table("table0", 
                             Column('name', String(100), default="Yo"),
                             Column('ref', Integer, nullable=False)
                             )
    
    
        creator.add_foreign_key_constraint(table1, 'int', table1, ondelete="CASCADE")
        creator.add_foreign_key_constraint(table3, 'ref', table1)
        creator.add_foreign_key_constraint(table2, 'new_ref', table1, ondelete="CASCADE")
        creator.add_unique_constraint(table3, "ref")
#        creator.add_unique_constraint(table1, "int", "text")
        
        
        creator.bind(self.connection)
        
        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        #backup_path = None
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        table1 = creator_new.create_table("table1_new", 
                             Column('s20', String(20)),
                             Column('int_new', Integer),
                            )
    
    
        table2 = creator_new.create_table("table2", 
                             Column('name', String(100)),
                             Column('ref_table1', Integer),
                             Column('new_column', LargeBinary)
                             )

        table3 = creator_new.create_table("table3", 
                             Column('name', String(100)),
                             Column('ref1', Integer)
                             )

        table4 = creator_new.create_table("table4")

        creator_new.add_foreign_key_constraint(table1, 'int_new', table1)
        creator_new.add_foreign_key_constraint(table2, 'ref_table1', table1)
        creator_new.add_unique_constraint(table3, "ref1")
        creator_new.add_unique_constraint(table1, "int_new")
         
        conn = self.connection.connect()
        #t = conn.begin()
        transaction = Transaction(connection=conn) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.rename_table("table1", "table1_new")
        updater.rename_column("table1_new", "int", "int_new")
        updater.rename_column("table2", "new_ref", "ref_table1")
        updater.rename_column("table3", "ref", "ref1")
        
        updater.update_schema()
        
        updater.update_finished((2,))
        
        transaction.commit()
    
    def test_upgrade8(self):

        creator = SchemaFactory.get_creator(self._testMethodName)

        users_table =  creator.create_table("user", 
                                             Column('name', String(40)),
                                             Column('fullname', String(100)),
                                             Column('password', String(15))
                                            )
    

        emails_table = creator.create_table("email_address", 
                                            Column('email_address', String(100), nullable=False),
                                            Column('user_id', Integer, nullable=False)
                                            )

        address_table = creator.create_table("address", 
                                            Column('address', String(100), nullable=False),
                                            Column('user_id', Integer, nullable=False)
                                            )
        
        user_log_table =   creator.create_table("user_log", 
                                                Column('info', String(100), nullable=False),
                                                Column('user_id', Integer)
                                                )
        
    
    
        creator.add_foreign_key_constraint(emails_table, 'user_id', users_table, ondelete="CASCADE")
        creator.add_foreign_key_constraint(address_table, 'user_id', users_table, ondelete="CASCADE")
        creator.add_foreign_key_constraint(user_log_table, 'user_id', users_table)
        
        
        creator.bind(self.connection)
        
        class User0(PersistentObject):
            pass
        
        class EmailAddress(PersistentObject):
            pass
        
        class Address(PersistentObject):
            pass
        
        class UserLog(PersistentObject):
            pass
        
        mapper(User0, users_table)
        mapper(EmailAddress, emails_table, [Relation(User0, 'user', backref_property_name = 'email_adresses')])
        mapper(Address, address_table, [Relation(User0, 'user', backref_property_name = 'adresses')])
        mapper(UserLog, user_log_table)
        
        with Transaction() as t:
            u1 = User0(name="pwl", fullname="Peter W. Lauridsen", password="secret")
            t.add(u1)
            u2 = User0(name="mwl", fullname="Michelle W. Lauridsen", password="very secret")
            t.add(u2)
            u3 = User0(name="tw", fullname="Tinky Winky", password="red bag")
            t.add(u3)
            u4 = User0(name="po", fullname="Po", password="scooter")
            t.add(u4)
            
            email1 = EmailAddress(email_address="pwl@giritech.com")
            t.add(email1)
            email2 = EmailAddress(email_address="pwl@djaman.dk")
            t.add(email2)
            email3 = EmailAddress(email_address="teletubbies@bbc.co.uk")
            t.add(email3)
            email4 = EmailAddress(email_address="michelle@djaman.dk")
            t.add(email4)
            
            email1.user = u1
            email2.user = u1
            email3.user = u3
            email4.user = u2
            
            
            address1 = Address(address="Dianavej 17J")
            t.add(address1)
            address2 = Address(address="Dianavej 17J")
            t.add(address2)
            address3 = Address(address="Over the hills and far away")
            t.add(address3)

            address1.user = u1
            address2.user = u2
            address3.user = u3
            
            for i in range(0,500):
                t.add(UserLog(info="Info %s" %i))

        
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        #backup_path = None
        
        creator_new = SchemaFactory.get_creator(self._testMethodName)

        users_table =  creator_new.create_table("user", 
                                             Column('name', String(40)),
                                             Column('fullname', String(100)),
                                             Column('password', String(56))
                                            )
    

        user_details_table = creator_new.create_table("user_details", 
                                                Column('detail_type', Integer, nullable=False, default=1),
                                                Column('detail_value', String(100), nullable=False),
                                                Column('user_id', Integer, nullable=False)
                                                )

        address_table = creator_new.create_table("address", 
                                            Column('address', String(100), nullable=False),
                                            Column('user_id', Integer, nullable=False)
                                            )
        
        user_details_types_table = creator_new.create_table("user_details_types", 
                                                            Column('detail_type', Integer, nullable=False),
                                                            Column('detail_name', String(100), nullable=False),
                                                            )
        

        creator_new.add_foreign_key_constraint(user_details_table, 'user_id', users_table, ondelete="CASCADE")
        creator_new.add_foreign_key_constraint(address_table, 'user_id', users_table, ondelete="CASCADE")
        creator_new.add_unique_constraint(user_details_types_table, "detail_type")
         
        conn = self.connection.connect()
        #t = conn.begin()
        transaction = Transaction(connection=conn) 
        
        updater = SchemaFactory.get_updater(creator_new, transaction, restore_path=backup_path)
        updater.rename_table("email_address", "user_details")
        updater.rename_column("user_details", "email_address", "detail_value")
        updater.drop_table("user_log")
        
        updater.update_schema()


        class User1(PersistentObject):
            pass
        
        class UserDetail(PersistentObject):
            pass
        
        class UserDetailsType(PersistentObject):
            pass

        class Address1(PersistentObject):
            pass
        
        mapper(User1, users_table)
        mapper(UserDetail, user_details_table, [Relation(User1, 'user', backref_property_name = 'user_details')])
        mapper(Address1, address_table, [Relation(User1, 'user', backref_property_name = 'adresses')])
        mapper(UserDetailsType, user_details_types_table)
        
        with Transaction() as t:
            # Create user detail types
            email_type = t.select_first(UserDetailsType, filter=UserDetailsType.detail_type==1)
            if not email_type:
                email_type = UserDetailsType(detail_type=1, detail_name="email address")
                t.add(email_type)

            address_type = t.select_first(UserDetailsType, filter=UserDetailsType.detail_type==2)
            if not address_type:
                address_type = UserDetailsType(detail_type=2, detail_name="address")
                t.add(address_type)

            # Move addresses to user_details
            addresses = t.select(Address1)
            for address in addresses:
                address_detail = UserDetail(detail_type=address_type.detail_type, detail_value=address.address, user=address.user)
                t.add(address_detail)
                t.delete(address)
                
            # Change a password
            user_pwl = t.select_one(User1, filter = User1.name=="pwl")
            user_pwl.password = "very very very very secret"
        
        updater.update_finished((2,))
        
        transaction.commit()
        
        
#
#    TEST OF SELF-REFERENCING TABLES - no longer supported 
#
#    def test_restore1a(self):
#        self._test_restore1(False)
#
#    def test_restore1b(self):
#        self._test_restore1(True)
#        
#    def _test_restore1(self, schema_update):
#
#        creator = SchemaFactory.get_creator(self._testMethodName)
#        
#        table1 = creator.create_table("self_ref", 
#                             Column('name', String(1)),
#                             Column('ref1', Integer),
#                             Column('ref2', Integer),
#                            )
#    
#    
#    
#        creator.add_foreign_key_constraint(table1, 'ref1', table1)
#        creator.add_foreign_key_constraint(table1, 'ref2', table1)
#        creator.add_unique_constraint(table1, "name")
#        
#        
#        creator.bind(self.connection)
#        
#        class Node(PersistentObject):
#            pass
#        
#        
#        mapper(Node, table1,[Relation(Node,'children1',primaryjoin=table1.c.id==table1.c.ref1), 
#                             Relation(Node,'children2',primaryjoin=table1.c.id==table1.c.ref2),
#                             ]
#                             )
#        
#        t = Transaction()                      
#        
#        node_A = Node(name="A")
#        node_B = Node(name="B")
#        node_C = Node(name="C")
#        node_D = Node(name="D")
#        node_E = Node(name="E")
#        node_F = Node(name="F")
#        node_G = Node(name="G")
#        node_H = Node(name="H")
#        node_I = Node(name="I")
#        
#        t.add(node_A)
#        t.add(node_B)
#        t.add(node_C)
#        t.add(node_D)
#        t.add(node_E)
#        t.add(node_F)
#        t.add(node_G)
#        t.add(node_H)
#        t.add(node_I)
#        
#        t.commit()
#        ''' 
#
#        node_H.ref1 = node_G.id
#        node_B.ref1 = node_A.id
#        node_C.ref1 = node_A.id
#        node_D.ref1 = node_C.id
#        node_E.ref1 = node_C.id
#        ''' 
#        
#        node_G.children1.append(node_H)
#        node_A.children1.append(node_B)
#        node_A.children1.append(node_C)
#        node_C.children1.append(node_D)
#        node_C.children1.append(node_E)
#
#
#        node_C.children2.append(node_H)
#        node_B.children2.append(node_F)
#        node_B.children2.append(node_D)
#        node_G.children2.append(node_A)
#
#        t.commit()
#        t.close()
#        ''' 
#
#        node_H.parent2 = node_C
#        node_F.parent2 = node_B
#        node_D.parent2 = node_B
#        node_A.parent2 = node_G
#        '''
#                  
#        backup_path = self.work_folder
#        creator.backup(backup_path, schema_only=False, data_only=False)
#        creator.restore(backup_path, self.connection, schema_update=schema_update, data_update=True)



    def dfs(self, group, marked):
        marked[group.id] = 0
        for obj in group.members:
            if isinstance(obj, Group):
                mark = marked.get(obj.id)
                if mark==0:
                    raise CycleDetectedException(obj.name)
                elif mark!=1:
                    self.dfs(obj, marked)
        marked[group.id] = 1
            
    
    def check_cycles(self, known_groups):
        marked = dict()
        start_nodes = []
        for g in known_groups:
            if isinstance(g, Group):
                start_nodes.append(g)
                
        
        for g in start_nodes:
            if not marked.has_key(g.id):
                self.dfs(g, marked)
        

#    def test_asimple(self):
#
#        creator = SchemaFactory.get_creator(self._testMethodName)
#        
#        member_object = creator.create_table("test", 
#                             Column('name', String(100)),
#                            )
#        
#        creator.bind(self.connection)
        

    def test_restore2a(self):
        self._test_restore2(False)

    def test_restore2b(self):
        self._test_restore2(True)
        
    def _test_restore2(self, schema_update):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        member_object = creator.create_table("MemberObject", 
                             Column('name', String(100)),
                             Column('type', String(30), nullable=False)
                            )
        
        user = creator.create_table("User", 
                                     inherit_from = member_object
                                    )
    
        group = creator.create_table("Group", 
                                     Column('name', String(100)),
                                     inherit_from = member_object
                            )
        
    
        group_membership = creator.create_table("group_membership", 
                             Column('parent_id', Integer),
                             Column('child_id', Integer),
                            )
    
        creator.add_foreign_key_constraint(group_membership, 'parent_id', group)
        creator.add_foreign_key_constraint(group_membership, 'child_id', member_object)
        
        
        creator.bind(self.connection)
        
        
        
        
        mapper(MemberObject, member_object, 
               polymorphic_on = member_object.c.type,
               polymorphic_identity = 'Object')
        
        
        mapper(Group, 
               group, 
               inherits=MemberObject,
               polymorphic_identity = 'Group', 
               relations = [Relation(MemberObject,'members', intermediate_table=group_membership, backref_property_name="memberof")]
              )
        
        mapper(User, 
               user, 
               inherits=MemberObject,
               polymorphic_identity = 'User'
              )
        
        t = Transaction()
        
        user1 = User(name="Kurt")
        user2 = User(name="Ole")
        user3 = User(name="Karen")
        user4 = User(name="Birte")
        
        group1 = Group(name="Players")                      
        group2 = Group(name="Managers")
        group3 = Group(name="Employees")
        
        group3.members.append(group1)
        group3.members.append(group2)
        group3.members.append(user3)
        
        group1.members.append(user1)
        group2.members.append(user2)
        group1.members.append(user4)
        group2.members.append(user4)
                              
        
        t.add(group1)
        t.add(group2)
        t.add(group3)

        t.add(user1)
        t.add(user2)
        t.add(user3)

        t.commit()
        t.close()
                  
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        conn = self.connection.connect()
        trans = conn.begin()
        creator.restore(backup_path, conn, schema_update=schema_update, data_update=True)
        trans.commit()
        trans.close()

    def test_restore3a(self):
        self._test_restore3(False)

    def test_restore3b(self):
        self._test_restore3(True)
        
    def _test_restore3(self, schema_update):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        member_object = creator.create_table("MemberObject", 
                             Column('name', String(100)),
                             Column('type', String(30), nullable=False)
                            )
        
        user = creator.create_table("User", 
                                     inherit_from = member_object
                                    )
    
        group = creator.create_table("Group", 
                                     Column('name', String(100)),
                                     inherit_from = member_object
                            )
        
    
        group_membership = creator.create_table("group_membership", 
                             Column('parent_id', Integer),
                             Column('child_id', Integer),
                            )
    
        creator.add_foreign_key_constraint(group_membership, 'parent_id', group)
        creator.add_foreign_key_constraint(group_membership, 'child_id', member_object)
        
        
        creator.bind(self.connection)
        
        
        
        
        mapper(MemberObject, member_object, 
               polymorphic_on = member_object.c.type,
               polymorphic_identity = 'Object')
        
        
        mapper(Group, 
               group, 
               inherits=MemberObject,
               polymorphic_identity = 'Group', 
               relations = [Relation(MemberObject,'members', intermediate_table=group_membership, backref_property_name="memberof")]
              )
        
        mapper(User, 
               user, 
               inherits=MemberObject,
               polymorphic_identity = 'User'
              )
        
        t = Transaction()
        
        user1 = User(name="Kurt")
        user2 = User(name="Ole")
        user3 = User(name="Karen")
        
        group1 = Group(name="group1")                      
        group2 = Group(name="group2")
        group3 = Group(name="group3")
        group4 = Group(name="group4")
        group5 = Group(name="group5")
        group6 = Group(name="group6")
        group7 = Group(name="group7")
        
        group3.members.append(group1)
        group3.members.append(group2)
        group3.members.append(user3)
        
        group1.members.append(user1)
        group2.members.append(user2)
        
        
        t.add(group1)
        t.add(group2)
        t.add(group3)
        t.add(group4)
        t.add(group5)
        t.add(group6)
        t.add(group7)

        t.add(user1)
        t.add(user2)
        t.add(user3)

        group1.members.append(group1)
        t._session.flush()
        with ReadonlySession() as dbs:
            self.assertRaises(CycleDetectedException, self.check_cycles, t.select(Group))

        group1.members.pop()
        group1.members.append(group4)
        self.check_cycles(t.select(Group))
        
        group4.members.append(group3)
        self.assertRaises(CycleDetectedException, self.check_cycles, t.select(Group))
        
        group4.members.pop()
        group4.members.append(group5)
        group4.members.append(group6)
        group3.members.append(group7)
        group7.members.append(group6)
        group5.members.append(group7)
        self.check_cycles(t.select(Group))
        
        group5.members.append(group3)
        self.assertRaises(CycleDetectedException, self.check_cycles, t.select(Group))

        group5.members.pop()

        t.commit()
                  
        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        conn = self.connection.connect()
        trans = conn.begin()
        creator.restore(backup_path, conn, schema_update=schema_update, data_update=True)
        trans.commit()
        
#    def test_restore5(self):
#        path = "C:/Users/pwl/main2/setup/dev_env/gon_installation/gon_config_service/win/backup/backup_5.6.0-0_2012-01-30_120444.370000/database/aux_schema.xml"
#        path = "C:/Users/pwl/main2/setup/dev_env/gon_installation/gon_config_service/win/backup/backup_5.6.0-0_2012-01-31_103430.698000/database/aux_schema.xml"
##        SchemaFactory.restore(path, self.connection, schema_update=True)
#        restore(self.connection, path, schema_update=True)


    def test_restore4a(self):
        self._test_restore4(False)

    def test_restore4b(self):
        self._test_restore4(True)


    def _test_restore4(self, schema_update):
        
        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('str1', String(1)),
                             Column('str8000', String(8000)),
                             Column('text', Text),
                             Column('int', Integer),
                             Column('float', Float),
                             Column('dt', DateTime),
                             Column('dd', Date),
                             Column('tt', Time),
                             Column('bin', LargeBinary),
                             Column('bool', Boolean),
                            )
        
        class T(object):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        hexstr = "1f2e3d4c4b5a6978"
        with Transaction() as dbt:
            
            t = dbt.add(T())
            t.str1 = u"�"
            t.str8000 = "100000"
            t.text = "text\ntext"
            t.int = 30000
            t.float = math.pi
            t.dt = datetime.datetime(1967, 4, 16, 8, 5, 20)
            t.dd = datetime.datetime(2005, 12, 14, 20, 5, 20).date()
            t.tt = datetime.datetime(2005, 12, 14, 20, 5, 20).time()
            t.bin = binascii.unhexlify(hexstr)
            t.bool = True
            
            dbt.flush()
            first_id = t.id

            t = dbt.add(T())
            dbt.flush()
            second_id = t.id
            
            t = dbt.add(T())
            t.str1 = ""
            t.str8000 = ""
            t.text = ""
            t.int = 0
            t.float = 0
            t.dt = datetime.datetime.fromtimestamp(0)
            t.dd = datetime.datetime.fromtimestamp(0).date()
            t.tt = datetime.datetime.fromtimestamp(0).time()
            t.bin = binascii.unhexlify("")
            t.bool = False
            
            dbt.flush()
            third_id = t.id


        backup_path = self.work_folder
        creator.backup(backup_path, schema_only=False, data_only=False)
        
        conn = self.connection.connect()
        trans = conn.begin()
        creator.restore(backup_path, conn, schema_update=schema_update, data_update=True)
        trans.commit()
        trans.close()
            
        with ReadonlySession() as dbt:
            first = dbt.get(T, first_id)
            second = dbt.select_one(T, filter=T.id==second_id)
            third = dbt.select_first(T, filter=T.id==third_id)
            
            self.assertEquals(first.str1, u"�")
            self.assertEquals(first.str8000, "100000")
            self.assertEquals(first.text, "text\ntext")
            self.assertEquals(first.int, 30000)
            self.assertAlmostEquals(first.float, math.pi, places=5)
            self.assertEquals(first.dt, datetime.datetime(1967, 4, 16, 8, 5, 20))
            self.assertEquals(first.dd, datetime.datetime(2005, 12, 14, 20, 5, 20).date())
            self.assertEquals(first.tt, datetime.datetime(2005, 12, 14, 20, 5, 20).time())
            self.assertEquals(binascii.hexlify(first.bin), hexstr)
            self.assertEquals(first.bool, True)
            
            self.assertEquals(second.str1, None)
            self.assertEquals(second.str8000, None)
            self.assertEquals(second.text, None)
            self.assertEquals(second.int, None)
            self.assertEquals(second.float, None)
            self.assertEquals(second.dt, None)
            self.assertEquals(second.dd, None)
            self.assertEquals(second.tt, None)
            self.assertEquals(second.bin, None)
            self.assertEquals(second.bool, None)
            
            self.assertEquals(third.str1, "")
            self.assertEquals(third.str8000, "")
            self.assertEquals(third.text, "")
            self.assertEquals(third.int, 0)
            self.assertAlmostEquals(third.float, 0, places=6)
            self.assertEquals(third.dt, datetime.datetime.fromtimestamp(0))
            self.assertEquals(third.dd, datetime.datetime.fromtimestamp(0).date())
            self.assertEquals(third.tt, datetime.datetime.fromtimestamp(0).time())
            self.assertEquals(binascii.hexlify(third.bin), "")
            self.assertEquals(third.bool, False)

    def test_inhteritance1(self):
        
        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('type', String(100), nullable=False),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             inherit_from = table1
                             )
    
    
        table3 = creator.create_table("table3", 
                             inherit_from = table1
                             )
        

        table4 = creator.create_table("table4",
                             Column('int', Integer),
                             inherit_from = table1
                             )

        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(T1):
            pass

        class T3(T1):
            pass
            
        class T4(T1):
            pass
        
        mapper(T1, table1, polymorphic_on = table1.c.type, polymorphic_identity = 'T1')
        mapper(T2, table2, inherits=T1, polymorphic_identity = 'T2') 
        mapper(T3, table3, inherits=T1, polymorphic_identity = 'T3') 
        mapper(T4, table4, inherits=T1, polymorphic_identity = 'T4') 
        
        with Transaction() as t:
            t.add(T1(s10="yo"))
            t.add(T2(s10="yo2"))
            t.add(T3(s10="yo3"))
            t.add(T4(s10="yo2", int=2))
            
        conn = self.connection.connect()
            
        table_name = creator.create_table_name("table1")
        result = conn.execute("select * from %s" % table_name).fetchall()
        
        self.assertEquals(len(result),4)
        for r in result:
            if r[3] == "T4":
                self.assertEquals(r[2],2)
            else:
                self.assertEquals(r[2],None)

        table_name = creator.create_table_name("table4")
        result = conn.execute("select * from %s" % table_name).fetchall()
        self.assertEquals(len(result),1)
        self.assertEquals(result[0][1],2)
            
    def _test_backup1(self):
        db_path = "C:\\Program Files\\Giritech\\gon_5.4.1-6\\gon_server_management_service\\win\\gon_server_db.sqlite"
        path = cmake_unittest.mkdtemp()
        db_connection = ConnectionFactory(None).create_connection('sqlite:///%s' % db_path, True)
        
        import components.database.server_common.schema_api
        import components.auth.server_common.database_schema
        import components.dialog.server_common.database_schema
        import components.management_message.server_common.database_schema
        import components.endpoint.server_common.database_schema
        import components.traffic.server_common.database_schema
        import components.user.server_common.database_schema
        import components.access_log.server_common.database_schema
        import plugin_types.common.plugin_type_token
        import components.templates.server_common.database_schema
        import components.server_config_ws.database_schema  
        import components.admin_ws.database_schema      
        
        SchemaFactory.backup_reflect(path, db_connection)

    def _test_backup2(self):
        path = cmake_unittest.mkdtemp()
        db_connection = ConnectionFactory(None, db_encoding="cp1252").create_connection('mssql://sa:GiriGiri@dev-pwl\SQLEXPRESS/gon541', True)
#        db_connection = ConnectionFactory(None, db_encoding="cp1252").create_connection('mssql://sa:GiriGiri@dev-pwl\SQLEXPRESS/gon540', True)
        
        import components.database.server_common.schema_api
        import components.auth.server_common.database_schema
        import components.dialog.server_common.database_schema
        import components.management_message.server_common.database_schema
        import components.endpoint.server_common.database_schema
        import components.traffic.server_common.database_schema
        import components.user.server_common.database_schema
        import components.access_log.server_common.database_schema
        import plugin_types.common.plugin_type_token
        import components.templates.server_common.database_schema
        import components.server_config_ws.database_schema
        import components.admin_ws.database_schema        
        
        SchemaFactory.backup_reflect(path, db_connection)
        
    

class MemberObject(PersistentObject):
    pass
    

class User(MemberObject):
    pass

#''' 
class Group(MemberObject):
    pass
'''

class Group(MemberObject):
    
    def __init__(self, **kwargs):
        self     = None
        super(Group,self).__init__(**kwargs)
'''
        
class CycleDetectedException(Exception):
    pass
 
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
