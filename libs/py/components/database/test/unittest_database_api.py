from __future__ import with_statement
import unittest
import os
import math
import datetime
import binascii

from lib import giri_unittest
from components.database.server_common.schema_api import *
from components.database.server_common.database_api import *
from components.database.server_common.connection_factory import ConnectionFactory


class DatabaseAPITest(unittest.TestCase):
    
    connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)
    #connection = ConnectionFactory(None).create_connection('sqlite:///test.sqlite', True)
#    connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/m2test', True)
#    connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/test', True)
#    connection = ConnectionFactory(None).create_connection('mysql://gon_db_user:GiriGiri007@192.168.42.238/gon_db_56_1', True)
    
    
    def setUp(self):
        self.work_folder = giri_unittest.mkdtemp()
        
        clear_database(self.connection)
        sqlalchemy.orm.clear_mappers()

        
    
    def test_api(self):

        creator = SchemaFactory.get_creator("test_api")
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int_field', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
        
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

        class T3(PersistentObject):
            pass
            
        
        mapper(T1, table1)
        mapper(T2, table2)
        mapper(T3, table3, [Relation(T2, 'owner', backref_property_name = 'email_adresses')])
        
        with Transaction() as t:
            t.add(T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int_field=200000))
            t.add(T2(name="Frodo"))
            p = t.add(T2(name="Saruman"))
            p.email_adresses.append(T3(email="saruman@microsoft.com"))
            t.add(T3(email="unwanted@unwanted.dk"))
            t3 = T3(email="frodo@mordor.org")
            t3.owner = t.get(T2, 1)
            

        s = ReadonlySession()
        t1s = T1.select(s)
        self.assertEqual(len(t1s), 1)
        t1 = t1s[0]
        self.assertEqual(t1.s10, "1234567890")
        
        t2s = T2.select(s)
        self.assertEqual(len(t2s), 2)
        t2 = t2s[0]
        self.assertEqual(t2.name, "Frodo")
        mail_list = t2.email_adresses
        self.assertEqual(len(mail_list), 1)
        self.assertEqual(mail_list[0].email, "frodo@mordor.org")
        
        
        t3s = T3.select(s)
        self.assertEqual(len(t3s), 3)
        
        for t3 in t3s:
            if t3.email == "saruman@microsoft.com":
                self.assertEqual(t3.owner.name, "Saruman")
            elif t3.email == "unwanted@unwanted.dk":
                self.assertEqual(t3.owner, None)
                
        s.close()
        

    def test_api1(self):

        creator = SchemaFactory.get_creator("test_api1")
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int_field', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )

        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        
        creator.bind(self.connection)

        class T2(PersistentObject):
            pass
        
        class T3(PersistentObject):
            pass
        
        mapper(T2, table2, column_name_map = { 'club_name' : 'name' })
        mapper(T3, table3, column_name_map = { 'player_name' : table3.c.email }, relations = [Relation(T2, 'plays_for', backref_property_name = 'players')])
        
        with Transaction() as t:
            club = t.create(T2, club_name = "Liverpool")
            club.players.append(T3(player_name = "Stephen Gerrard"))
            club.players.append(T3(player_name = "Jamie Carragher"))
            
        session = ReadonlySession()
        print T2.select(session)
        players = T3.select(session)
        print players
        self.assertEqual(len(players), 2)
        for p in players:
            print p.plays_for
            self.assertEqual(p.plays_for.club_name, "Liverpool")
            
        class T1(object):
            pass

        class T1a(object):
            pass
        
        mapper(T1a, table1, include_properties=["s10"])
        mapper(T1, table1)
        
        with Transaction() as t:
            t1 = T1()
            t1.s10 = "hello"
            t1.int_field = 10
            t.add(t1)
        
        session.close()
        
        s = ReadonlySession()
        t1s = s.select(T1a)
        print t1s
        self.assertEqual(len(t1s), 1)
        t1 = t1s[0]
        self.assertEqual(t1.s10, "hello")
        self.assertEqual(getattr(t1, "int_field", None), None)
        s.close()
        
        
    def _create_disc_connection(self, name):
        
        path = os.path.join(self.work_folder, "%s.sqlite" % name)
        try:
            os.remove(path)
        except:
            pass
        if os.path.exists(path):
            raise Exception("Could not clear database file %s" % path)
        
        return ConnectionFactory(None).create_connection('sqlite:///%s' % path, True)
    
    
    def test_api2(self):

        creator = SchemaFactory.get_creator("test_api1")
        other_creator = SchemaFactory.get_creator("test_api1")
        
        table_mem = creator.create_table("table_mem", 
                             Column('s5', String(5), nullable=False),
                            )
    
        table_disc = other_creator.create_table("table_disc", 
                             Column('name', String(10)),
                             )

        other_connection = self._create_disc_connection(self._testMethodName)

        creator.bind(self.connection)
        other_creator.bind(other_connection)
        
        class Mem(object):
            pass
        
        class Disc(object):
            pass
        
        mapper(Mem, table_mem) 
        mapper(Disc, table_disc)
        
        try:
            with Transaction() as t:
                m = t.add(Mem())
                m.s5 = "yo1"
                d = t.add(Disc())
                d.name = "1234567890"
                
        except:
            pass
            
        s = ReadonlySession()
        print [m.s5 for m in s.select(Mem)]
        print s.select(Disc)
        
        
            
        
    def test_api3(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('yo', String(5)),
                            )
        
        class T(object):
            pass
        
        mapper(T, table)
        
        disc_connection = self._create_disc_connection(self._testMethodName)


        creator.bind(disc_connection)
        creator.bind(self.connection)
        
        
        with Transaction() as t:
            obj = T()
            obj.yo = "yo"
            t.add(obj)
            
        s = ReadonlySession()
        list = s.select(T)
        self.assertEqual(len(list), 1)
        print [t.yo for t in list]
            
        
        creator.bind(disc_connection)
    
        s = ReadonlySession()
        list = s.select(T)
        self.assertEqual(len(list), 0)
        print [t.yo for t in list]
            

    def test_api4(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('yo', String(5)),
                             mysql_engine='InnoDB'                             
                            )
        
        class T(object):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        
        t = Transaction()
        t.add(T())
        t.add(T())
        t.commit()
        
        self.assertEqual(len(t.select(T)), 2)
        t.delete_all(T)
        #t.rollback()
        self.assertEqual(len(t.select(T)), 0)
        t.add(T())
        self.assertEqual(len(t.select(T)), 1)
        t.commit()
        
        self.assertEqual(len(t.select(T)), 1)

        t.delete_all(T)
        t.rollback()
    
        self.assertEqual(len(t.select(T)), 1)


    def test_api5(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('yo0', String(5)),
                             Column('yo1', String(5)),
                             Column('yo2', String(5)),
                             Column('yo3', String(5)),
                            )
        
        class T(PersistentObject):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        
        t = Transaction()
        t.add(T(yo0="0", yo1="1", yo2="2",yo3="3") )
        t.add(T(yo0="0", yo1="0", yo2="3",yo3="2") )
        t.add(T(yo0="3", yo1="1", yo2="2",yo3="0") )
        t.add(T(yo0="0", yo1="2", yo2="1",yo3="3") )
        t.add(T(yo0="1", yo1="3", yo2="0",yo3="1") )
        t.add(T(yo0="11", yo1="33", yo2="01",yo3="12") )

        list = t.select(T, and_(T.yo0=='0', T.yo3 < 3))
        self.assertEqual(len(list),1)

        list = t.select(T, and_(T.yo0=='0', or_(T.yo1 =='1', T.yo2 =='1')))
        self.assertEqual(len(list),2)

        list = t.select(T, or_(T.yo0.like('1%'), T.yo1 =='0', not_(T.yo3<>'0')))
        self.assertEqual(len(list),4)
        
        list =  t.select(T, order_by=T.yo3)
        self.assertNotEqual(len(list),0)
        self.assertEqual(list[0].yo0, "3")
        
        list =  t.select(T, order_by=[T.yo0, T.yo1])
        self.assertNotEqual(len(list),0)
        self.assertEqual(list[0].yo3, "2")
        

    def test_api6(self):

        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('yo0', String(5)),
                             Column('yo1', String(5)),
                             Column('yo2', String(5)),
                             Column('yo3', String(5)),
                            )
        
        class T(PersistentObject):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        
        t = Transaction()
        t.add(T(yo0="0", yo1="1", yo2="2",yo3="3") )
        t.add(T(yo0="0", yo1="0", yo2="3",yo3="2") )
        t.add(T(yo0="3", yo1="1", yo2="2",yo3="0") )
        t.add(T(yo0="0", yo1="2", yo2="1",yo3="3") )
        t.add(T(yo0="1", yo1="3", yo2="0",yo3="1") )
        t.add(T(yo0="11", yo1="33", yo2="01",yo3="12") )

        e = t.select_one(T, filter = and_(T.yo0=='0', T.yo3 < 3))
        self.assertEqual(e.yo3,"2")
        self.assertRaises(InvalidRequestException, t.select_one, T, filter = and_(T.yo0=='0', or_(T.yo1 =='1', T.yo2 =='1')))
        self.assertRaises(InvalidRequestException, t.select_one, T, filter = T.yo0=="42")
        
        e = t.select_first(T, filter = and_(T.yo0=='0', T.yo3 < 3))
        self.assertEqual(e.yo3,"2")
        e = t.select_first(T, filter = and_(T.yo0=='0', or_(T.yo1 =='1', T.yo2 =='1')))
        self.assertEqual(e.yo0, "0")
        e = t.select_first( T, filter = T.yo0=="42")
        self.assertEqual(e, None)

        count = t.select_count(T, filter = or_(T.yo0.like('1%'), T.yo1 =='0', not_(T.yo3<>'0')))
        self.assertEqual(count,4)
        count = t.select_count( T, filter = T.yo0=="42")
        self.assertEqual(count,0)
        

    def test_api7(self):

        creator = SchemaFactory.get_creator("test_api")
        
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
        
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        
        creator.bind(self.connection)
        
        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

        class T3(PersistentObject):
            pass
            
        
        mapper(T2, table2)
        mapper(T3, table3, [Relation(T2, 'owner', backref_property_name = 'email_adresses')])
        
        with Transaction() as t:
            t.add(T2(name="NoMail"))
            t.add(T2(name="Frodo"))
            p = t.add(T2(name="Saruman"))
            p.email_adresses.append(T3(email="saruman@microsoft.com"))
            t.add(T3(email="unwanted@unwanted.dk"))
            t3 = T3(email="frodo@mordor.org")
            t3.owner = t.get(T2, 2)
            
        s = ReadonlySession()
        
        t2s = s.select(T2, outerjoin="email_adresses")
        self._print_objects(t2s)

        t2s = s.select(T2, join="email_adresses")
        self._print_objects(t2s)

        t2 = s.select_first(T2, join="email_adresses")
        print t2

        t3s = s.select(T3, outerjoin="owner")
        self._print_objects(t3s)

        t3s = s.select(T3, join="owner")
        self._print_objects(t3s)
        
        t3 = s.select_first(T3, join="owner")
        print t3
        
        s.close()

        with Transaction() as t:
            t.add(T2())
            
        s = ReadonlySession()
        
        t2s = s.select(T2, T2.name==None)
        self.assertEquals(len(t2s), 1)
        
        s.close()

    def test_api8(self):
        
        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('str1', String(1)),
                             Column('str8000', String(8000)),
                             Column('text', Text),
                             Column('int_field', Integer),
                             Column('float', Float),
                             Column('dt', DateTime),
                             Column('bin', LargeBinary),
                             Column('bool', Boolean),
                            )
        
        class T(object):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        
        hexstr = "1f2e3d4c4b5a6978"
        with Transaction() as dbt:
            
            t = dbt.add(T())
            t.str1 = "a"
            t.str8000 = "100000"
            t.text = "text\ntext"
            t.int_field = 30000
            t.float = math.pi
            t.dt = datetime.datetime(1967, 4, 16, 8, 5, 20)
            t.bin = binascii.unhexlify(hexstr)
            t.bool = True
            
            dbt.flush()
            first_id = t.id

            t = dbt.add(T())
            dbt.flush()
            second_id = t.id
            
            t = dbt.add(T())
            t.str1 = ""
            t.str8000 = ""
            t.text = ""
            t.int_field = 0
            t.float = 0
            t.dt = datetime.datetime.fromtimestamp(0)
            t.bin = binascii.unhexlify("")
            t.bool = False
            
            dbt.flush()
            third_id = t.id
            
        with ReadonlySession() as dbt:
            first = dbt.get(T, first_id)
            second = dbt.select_one(T, filter=T.id==second_id)
            third = dbt.select_first(T, filter=T.id==third_id)
            
            self.assertEquals(first.str1, "a")
            self.assertEquals(first.str8000, "100000")
            self.assertEquals(first.text, "text\ntext")
            self.assertEquals(first.int_field, 30000)
            self.assertAlmostEquals(first.float, math.pi, places=5)
            self.assertEquals(first.dt, datetime.datetime(1967, 4, 16, 8, 5, 20))
            self.assertEquals(binascii.hexlify(first.bin), hexstr)
            self.assertEquals(first.bool, True)
            
            self.assertEquals(second.str1, None)
            self.assertEquals(second.str8000, None)
            self.assertEquals(second.text, None)
            self.assertEquals(second.int_field, None)
            self.assertEquals(second.float, None)
            self.assertEquals(second.dt, None)
            self.assertEquals(second.bin, None)
            self.assertEquals(second.bool, None)
            
            self.assertEquals(third.str1, "")
            self.assertEquals(third.str8000, "")
            self.assertEquals(third.text, "")
            self.assertEquals(third.int_field, 0)
            self.assertAlmostEquals(third.float, 0, places=6)
            self.assertEquals(third.dt, datetime.datetime.fromtimestamp(0))
            self.assertEquals(binascii.hexlify(third.bin), "")
            self.assertEquals(third.bool, False)
            
    def test_api9(self):
        
        creator = SchemaFactory.get_creator(self._testMethodName)
        
        table = creator.create_table("table1", 
                             Column('str1', String(1)),
                             Column('str8000', String(8000)),
                             Column('text', Text),
                             Column('int_field', Integer),
                             Column('float', Float),
                             Column('dt', DateTime),
                             Column('bin', LargeBinary),
                             Column('bool', Boolean),
                            )
        
        class T(object):
            pass
        
        mapper(T, table)
        
        creator.bind(self.connection)
        
        hexstr = "1f2e3d4c4b5a6978"
        with Transaction() as dbt:
            
            t = dbt.add(T())
            t.str1 = "a"
            t.str8000 = "100000"
            t.text = "text\ntext"
            t.int_field = 30000
            t.float = math.pi
            t.dt = datetime.datetime(1967, 4, 16, 8, 5, 20)
            t.bin = binascii.unhexlify(hexstr)
            t.bool = True
            
            
        with ReadonlySession() as dbt:
            first = dbt.select_first(T)
            
            self.assertEquals(first.str1, "a")
            self.assertEquals(first.str8000, "100000")
            self.assertEquals(first.text, "text\ntext")
            self.assertEquals(first.int_field, 30000)
            self.assertAlmostEquals(first.float, math.pi, places=5)
            self.assertEquals(first.dt, datetime.datetime(1967, 4, 16, 8, 5, 20))
            self.assertEquals(binascii.hexlify(first.bin), hexstr)
            self.assertEquals(first.bool, True)
            
            first.str8000 = "Not saved"
            
        with Transaction() as dbt:
            first = dbt.select_first(T)
            first.text = "Saved"
            
        with ReadonlySession() as dbt:
            first = dbt.select_first(T)
            
            self.assertEquals(first.str8000, "100000")
            self.assertEquals(first.text, "Saved")

        with Transaction() as dbt:
            first = dbt.select_first(T)
            first.text = "Saved again"
            
            with ReadonlySession() as dbt:
                first_ro = dbt.select_first(T)
                
                first_ro.str8000 = "Not saved"


        with ReadonlySession() as dbt:
            first = dbt.select_first(T)
            
            self.assertEquals(first.str8000, "100000")
            self.assertEquals(first.text, "Saved again")


    def test_query_api1(self):

        creator = SchemaFactory.get_creator("test_api")
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int_field', Integer),
                             Column('text', Text),
                            )
    
        table2 = creator.create_table("table2", 
                             Column('name', String(100)),
                             )
    
    
        table3 = creator.create_table("table3", 
                             Column('email', String(100)),
                             Column('owner_id', Integer),
                             )
        
        creator.add_foreign_key_constraint(table3, 'owner_id', table2, ondelete="CASCADE")
        
        creator.bind(self.connection)
        
        class T1(PersistentObject):
            pass

        class T2(PersistentObject):
            def __init__(self, **kwargs):
                self.email_adresses = kwargs.pop('email_adresses', [])
                super(T2, self).__init__(**kwargs)

        class T3(PersistentObject):
            pass
            
        
        mapper(T1, table1)
        mapper(T2, table2)
        mapper(T3, table3, [Relation(T2, 'owner', backref_property_name = 'email_adresses')])
        
        
        with Transaction() as t:
            t.add(T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int_field=200000))
            t.add(T1(s10="1234567890", text="This is a text, which is written with \n and other stuff", int_field=100000))
            t.add(T2(name="Frodo"))
            p = t.add(T2(name="Saruman"))
            p.email_adresses.append(T3(email="saruman@microsoft.com"))
            t.add(T3(email="unwanted@unwanted.dk"))
            t3 = T3(email="frodo@mordor.org")
            t3.owner = t.get(T2, 1)

        class T1r(QueryResultBase):
            pass

        class T2r(QueryResultBase):
            pass

        class T3r(QueryResultBase):
            pass

        s = QuerySession()
        t1s = s.select(T1)
        self.assertEqual(len(t1s), 2)
        t1 = t1s[0]
        self.assertEqual(t1.s10, "1234567890")
        
        count = s.select_count(T1)
        self.assertEqual(count, 2)
        
        count_int = s.select_count(T1, group_by="int_field")
        self.assertEqual(len(count_int), 2)
        t1 = count_int[0]
        self.assertTrue(t1.int_field==200000 or t1.int_field==100000)
        self.assertEqual(t1.count_1, 1)
        
        
        t2s = s.select(T2)
        self.assertEqual(len(t2s), 2)
        t2 = t2s[0]
        self.assertEqual(t2.name, "Frodo")
        
        
        t3s = s.select(T3)
        self.assertEqual(len(t3s), 3)
        
        for t3 in t3s:
            if t3.email == "saruman@microsoft.com":
                self.assertEqual(t3.owner_id, 2)
            elif t3.email == "unwanted@unwanted.dk":
                self.assertEqual(t3.owner_id, None)
                
        t1s = s.select(T1, T1.id==1)
        self.assertEqual(len(t1s), 1)
        t1 = t1s[0]
        self.assertEqual(t1.s10, "1234567890")

        t3s = s.select(T3, filter=and_(T3.email=="saruman@microsoft.com", T3.owner_id==2))
        self.assertEqual(len(t3s), 1)

        t3s = s.select(T3, filter=and_(T3.email.like("Saruman@%"), T3.owner_id==2))
        self.assertEqual(len(t3s), 1)

        t3s = s.select(T3, filter=or_(T3.email=="saruman@microsoft.com", T3.owner_id==1))
        self.assertEqual(len(t3s), 2)
        
        t1s = s.select(table1, table1.c.id>0, order_by=table1.c.int_field)
        self.assertEqual(len(t1s), 2)
        t1 = t1s[0]
        self.assertEqual(t1.int_field, 100000)

        t1s = s.select([table1.c.int_field, table1.c.s10], distinct=True, order_by=[table1.c.int_field, table1.c.s10])
        t1s = s.select(table1.c.int_field, distinct=True, order_by=table1.c.int_field)
        self.assertEqual(len(t1s), 2)
        t1 = t1s[0]
        self.assertEqual(t1.int_field, 100000)

        t1s = s.select(table1.c.s10, distinct=True)
        self.assertEqual(len(t1s), 1)

        t1s = s.select([table1.c.s10, table1.c.int_field], distinct=True)
        self.assertEqual(len(t1s), 2)

            

    def _print_objects(self, list):
        for obj in list:
            print obj

        


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
