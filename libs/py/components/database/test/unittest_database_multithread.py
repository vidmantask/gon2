from __future__ import with_statement
import unittest
import os
import math
import datetime
import binascii
import threading
import lib.checkpoint
import time

from lib import giri_unittest
from components.database.server_common.schema_api import *
from components.database.server_common.database_api import *
from components.database.server_common.connection_factory import ConnectionFactory
import sqlalchemy.orm 
import sqlalchemy.sql

class T1(object):
    pass

class T2(object):
    
    def __init__(self):
        self.id = None
        self.s10 = None
        self.int = None
        self.text = None
        

use_session = 1

class ReadFromDB(threading.Thread):
    
    def __init__(self, checkpoint_handler, n):
        threading.Thread.__init__(self, name="%s" % n)
        self.checkpoint_handler = checkpoint_handler
        self.running = False
    
    def run(self):
        self.running = True
        global use_session
        try:
            for i in range(0,10):
                with self.checkpoint_handler.CheckpointScope("run %s.%d" % (self.name, i), "ReadFromDB", lib.checkpoint.INFO):
                    dbs = QuerySession()
                    all = dbs.select(T1)
                    print "%s.%d found %d" % (self.name, i, len(all))

                time.sleep(0.2)
        except Exception, e:
            print "Exception in %s" % self.name
            print e
        finally:
            self.running = False
    #            print e

class WriteToDB(threading.Thread):
    
    def __init__(self, checkpoint_handler, n, test_class):
        threading.Thread.__init__(self, name="Write%s" % n)
        self.checkpoint_handler = checkpoint_handler
        self.test_class = test_class
        self.running = False
        self.n = n
    
    def run(self):
        self.running = True
        try:
            n = 200
            with self.checkpoint_handler.CheckpointScope("populate_database", "WriteToDB", lib.checkpoint.INFO):
                t = Transaction()
                for i in range(0,n):
                    s10 = "%d" %i 
                    text = "This is a text, which is written with \n and other stuff %d" % i
                    t.add(T1())
                    t.s10 = s10
                    t.text = text
                    t.int = i
            
            with self.checkpoint_handler.CheckpointScope("commit", "WriteToDB", lib.checkpoint.INFO):
                print "commiting"
                t.commit()
        except Exception, e:
            print "Exception in %s" % self.name
            print e
        finally:
       
            if self.n > 1:
                time.sleep(1)
                self.n = self.n-1
                self.run()
#                thread = WriteToDB(self.checkpoint_handler, self.n-1, self)
#                thread.start()
#                while thread.running:
#                    time.sleep(1)
            
            else:        
                self.running = False
        
        

reset_all = True   

class DatabaseAPITest(unittest.TestCase):
    
#    connection = ConnectionFactory(None).create_connection('sqlite:///:memory:', True)
    connection = ConnectionFactory(None).create_connection('sqlite:///test.sqlite', False)
#    connection = ConnectionFactory(None).create_connection('mssql://sa:GiriGiri@localhost\SQLEXPRESS/test_1', False)
    #connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/test_upgrade', True)
    
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    
    def setUp(self):
        if reset_all:
            clear_database(self.connection)
#            sqlalchemy.orm.clear_mappers()
            self.populate_database()

    def populate_database(self):
#        pass
#    
#    def test_populate_database(self):

        creator = SchemaFactory.get_creator("test_api")
        
        table1 = creator.create_table("table1", 
                             Column('s10', String(10)),
                             Column('int', Integer),
                             Column('text', Text),
                            )

        
        creator.bind(self.connection)
        

        mapper(T1, table1)

        n = 200
        
        
#        with self.checkpoint_handler.CheckpointScope("populate_database", "populate_database", lib.checkpoint.INFO):
#            with Transaction() as t:
#                for i in range(0,n):
#                    s10 = "%d" %i 
#                    text = "This is a text, which is written with \n and other stuff %d" % i
#                    t.add(T1(s10=s10, text=text, int=i))

        with self.checkpoint_handler.CheckpointScope("populate_database", "populate_database", lib.checkpoint.INFO):
            t = Transaction()
            for i in range(0,n):
                s10 = "%d" %i 
                text = "This is a text, which is written with \n and other stuff %d" % i
                t.add(T1())
                t.s10 = s10
                t.text = text
                t.int = i
        
        with self.checkpoint_handler.CheckpointScope("commit", "populate_database", lib.checkpoint.INFO):
            t.commit()
                        
                        
#                for i in range(n, n+100):
#                    s10 = "%d" %i 
#                    text = "This is a text, which is written with \n and other stuff %d" % i
#                    t.add(T2(s10=s10, text=text, int=i, type=1))
#                    t.add(T3(s10=s10, text=text, int=i, type="RuleElement"))
        global reset_all
        reset_all = False                    

    
    def test_api(self):
        threads = []

        write_thread_no = 10
        thread = WriteToDB(self.checkpoint_handler, write_thread_no, self)
        threads.append(thread)
        
        thread_no = 5
        for i in range(0,thread_no):
            thread = ReadFromDB(self.checkpoint_handler, i)
            threads.append(thread)
            

        for thread in threads:
            thread.start()
            time.sleep(1)
        
        thread_alive = True
        while thread_alive:
            thread_alive = False
            for thread in threads:
                if thread.running:
                    time.sleep(1)
                    thread_alive = True
                    break
                    
            
        

            

    def _print_objects(self, list):
        for obj in list:
            print obj

        


if __name__ == '__main__':
    gut_py = giri_unittest.CMakeUnittest()
    
    if gut_py.do_run():
        unittest.main()    
