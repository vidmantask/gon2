import sqlalchemy
import sqlalchemy.orm as orm
#from sqlalchemy.types import *
import types as python_types
import sqlalchemy.types as types
import os
import elementtree.ElementTree as element_tree
from elementtree.SimpleXMLWriter import XMLWriter
from elementtree.ElementTree import iterparse
import datetime
import base64
import shutil

import connection_factory
import lib.checkpoint

import lib.commongon

from sqlalchemy.orm.interfaces import SessionExtension

#import setup.lib3p.sqlalchemy_migrate.migrate.changeset as changeset


#import logging
#logging.basicConfig()
#logging.getLogger('sqlalchemy.pool').setLevel(logging.DEBUG)
#logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)



''' Column types - shadow classes for sqlalchemy types'''

class Text(types.Text):
    pass

class String(types.String):
    pass

class Integer(types.Integer):
    pass

class Float(types.Float):
    pass

class DateTime(types.DateTime):
    pass

class Date(types.Date):
    pass

class Time(types.Time):
    pass

def deprecated(func):
    """This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emmitted
    when the function is used."""
    def newFunc(*args, **kwargs):
        warnings.warn("Call to deprecated function %s." % func.__name__,
                      category=DeprecationWarning)
        return func(*args, **kwargs)
    newFunc.__name__ = func.__name__
    newFunc.__doc__ = func.__doc__
    newFunc.__dict__.update(func.__dict__)
    return newFunc


class Binary(types.Binary):
    """Deprecated.  Renamed to LargeBinary."""
    pass

class LargeBinary(types.LargeBinary):
    pass

class Boolean(types.Boolean):
    pass


class NoSuchTableError(Exception):
    pass

class SchemaFactory(object):
    
    #databases = { None : sqlalchemy.MetaData()}
    _unbound_creators = []
    _bound_creators = []
    _db_connection = None
    _restore_path = None
    
    _AUX_BACKUP_FILENAME = "aux_schema.xml"

    @classmethod
    def get_creator(cls, name, checkpoint_handler=None):
        return SchemaCreator(name, checkpoint_handler=checkpoint_handler)
        
    @classmethod
    def unbind(cls):
        if cls._bound_creators and not cls._unbound_creators:
            cls._unbound_creators = SchemaFactory._bound_creators 
            cls._bound_creators = [] 
    
    @classmethod
    def get_all_existing_tables(cls, db_connection):
        metadata = sqlalchemy.MetaData()
        metadata.bind = db_connection
        metadata.reflect()
        return [table.name for table in metadata.sorted_tables]
        
    @classmethod
    def delete_all_tables(cls):
        for creator in cls._bound_creators:
            creator.delete_all()

    @classmethod
    def _backup_reflected_tables(cls, db_connection, table_names, file_name, schema_only, data_only):
        new_metadata = sqlalchemy.MetaData()
        new_metadata.bind = db_connection
        new_metadata.reflect(only=table_names)
        
        return _backup(new_metadata, filename=file_name, schema_only=schema_only, data_only=data_only)
    

    @classmethod
    def backup(cls, path, db_connection=None, schema_only=False, data_only=False, callback=None, checkpoint_handler=None, log_diff=False):
        if db_connection:
            cls.connect(db_connection)

        if callback is not None:
            callback.cb_db_backup_begin(len(cls._bound_creators))

        backed_up_tables = []
        for creator in cls._bound_creators:
            creator.checkpoint_handler = checkpoint_handler
            if callback is not None:
                callback.cb_db_backup_step(creator.name)
            table_names = creator.backup(path, schema_only=schema_only, data_only=data_only)
            
            backed_up_tables.extend(table_names)

        if callback is not None:
            callback.cb_db_backup_end()
        
        if checkpoint_handler:
            checkpoint_handler.Checkpoint("backup.backed_up_tables", "SchemaFactory.backup", lib.checkpoint.DEBUG, backed_up_tables=",".join(backed_up_tables))
            
        if log_diff:
            all_db_tables = set(cls.get_all_existing_tables(db_connection))
            checkpoint_handler.Checkpoint("backup.db_tables", "SchemaFactory.backup", lib.checkpoint.DEBUG, db_tables=",".join(all_db_tables))
            remaining_tables = all_db_tables.difference(set(backed_up_tables))
            if remaining_tables:
                file_name = os.path.join(path, cls._AUX_BACKUP_FILENAME)
                table_names = cls._backup_reflected_tables(db_connection, remaining_tables, file_name, schema_only, data_only)
                checkpoint_handler.Checkpoint("backup.diff", "SchemaFactory.backup", lib.checkpoint.WARNING, tables_not_backed_up=",".join(remaining_tables))
                
        return backed_up_tables

    @classmethod
    def backup_reflect(cls, path, db_connection, schema_only=False, callback=None, checkpoint_handler=None):

        if callback is not None:
            callback.cb_db_backup_begin(len(cls._bound_creators))

        all_creator = SchemaCreator("all", checkpoint_handler)
        all_creator.metadata.reflect(bind=db_connection)
        
        all_creators = cls._bound_creators + cls._unbound_creators

        backed_up_tables = []
        for current_creator in all_creators:
            creator = SchemaCreator(current_creator.name, checkpoint_handler)
            checkpoint_handler.Checkpoint("backup_reflect", "schema_api", lib.checkpoint.DEBUG, creator=creator.name)
            if callback is not None:
                callback.cb_db_backup_step(creator.name)
            for table_name in all_creator.metadata.tables:
                if table_name.startswith(creator.name):
                    table = all_creator.metadata.tables.get(table_name)
                    table.tometadata(creator.metadata)
                    checkpoint_handler.Checkpoint("backup_reflect", "schema_api", lib.checkpoint.DEBUG, table_added=table_name)
            creator.metadata.bind = db_connection
            table_names = creator.backup(path, schema_only=schema_only)
            
            backed_up_tables.extend(table_names)

        if callback is not None:
            callback.cb_db_backup_end()
        
        if checkpoint_handler:
            checkpoint_handler.Checkpoint("backup.backed_up_tables", "SchemaFactory.backup", lib.checkpoint.DEBUG, backed_up_tables=",".join(backed_up_tables))
            
        return backed_up_tables

    @classmethod
    def restore_data(cls, path, db_connection, callback=None, checkpoint_handler=None):
        if callback is not None:
            callback.cb_db_restore_begin(len(cls._bound_creators))
        cls.connect(db_connection)
        conn = db_connection.connect()
        t = conn.begin()

        try:
            files = os.listdir(path)
            for creator in cls._bound_creators:
                full_filename = creator._create_backup_file_name(path)
                if callback is not None:
                    callback.cb_db_restore_step(full_filename)
                creator.metadata.bind = conn
                _restore(creator.metadata, full_filename, create_constraints=False, checkpoint_handler=checkpoint_handler)
                try:
                    filename = creator.get_backup_file_name()
                    files.remove(filename)
                except ValueError, e:
                    checkpoint_handler.Checkpoint("restore", "SchemaFactory.restore1", lib.checkpoint.WARNING, msg="restore file not found %s" % filename)
                
            for filename in files:
                if filename.endswith(".xml"):
                    if callback is not None:
                        callback.cb_db_restore_step(filename)
                    metadata = sqlalchemy.MetaData()
                    metadata.bind = conn
                    full_filename = os.path.join(path, filename)
                    _restore(metadata, full_filename, create_constraints=False, checkpoint_handler=checkpoint_handler)
            t.commit()
        except:
            t.rollback()
            raise
        if callback is not None:
            callback.cb_db_restore_end()


    @classmethod
    def restore(cls, path, db_connection, schema_update=False, data_update=True, create_constraints=True, callback=None, checkpoint_handler=None):
        if callback is not None:
            callback.cb_db_restore_begin(len(cls._bound_creators))
        cls.connect(db_connection)
        conn = db_connection.connect()
        t = conn.begin()
        try:
            if schema_update:
                clear_database(conn)
                
            files = os.listdir(path)
            for filename in files:
                if filename.endswith(".xml"):
                    if callback is not None:
                        callback.cb_db_restore_step(filename)
                    metadata = sqlalchemy.MetaData()
                    metadata.bind = conn
                    full_filename = os.path.join(path, filename)
                    _restore(metadata, full_filename, schema_update, data_update, create_constraints=create_constraints, checkpoint_handler=checkpoint_handler)
            t.commit()
        except:
            t.rollback()
            raise
            
        if callback is not None:
            callback.cb_db_restore_end()

    @classmethod
    def register_creator(cls, creator):
        if cls._db_connection:
            creator.bind(cls._db_connection)
            cls._bound_creators.append(creator)
        else:
            cls._unbound_creators.append(creator)
            
    @classmethod
    def connect(cls, db_connection):
        cls._db_connection = db_connection
        for creator in cls._unbound_creators:
            creator.bind(db_connection)
            cls._bound_creators.append(creator)
        cls._unbound_creators = []
    
    @classmethod
    def disconnect(cls):
        cls._db_connection = None
        cls._unbound_creators.extend(cls._bound_creators)
        

    @classmethod
    def get_updater(cls, creator, transaction, restore_path, checkpoint_handler=None):
        if not restore_path:
            restore_path = cls._restore_path
        return SchemaUpdater(creator, transaction, restore_path, checkpoint_handler=checkpoint_handler)


_type_map = dict([('BIT', 'Boolean')])

def _get_type_name(data_type):
    type_name = data_type.__class__.__name__
    if _type_map.has_key(type_name):
        return _type_map[type_name]
    else:
        if type_name in types.__dict__:
            _type_map[type_name] = type_name
        else:
            for types_type_name in types.__dict__:
                types_type = getattr(types, types_type_name)
                if isinstance(types_type, python_types.TypeType) and types_type!=types.TypeEngine and issubclass(data_type.__class__, types_type) and issubclass(types_type, types.TypeEngine):
                    _type_map[type_name] = types_type_name
                    break
                        
        return _type_map[type_name]
    return "Text"
    

def get_sorted_tables(metadata, reverse=False):
    result = metadata.sorted_tables
    if result is None:
        return []
    if reverse:
        result.reverse()
    return result

def _add_type_info(element, data_type):
    element.set("type", _get_type_name(data_type))
    #element.set("type", repr(data_type))
    if isinstance(data_type, types.String) and data_type.length:
        element.set("length", str(data_type.length))
        
def _serialize_value(value, col_type):
    if value is None:
        return None
    elif isinstance(col_type, types.String):
        return value
    elif isinstance(col_type, types.DateTime):
        # HACK!!! the column default value datetime.now() is backed up as a string and therefore also restored as one, so if it is a string we accept it
        if isinstance(value, basestring):
            return value
        return lib.commongon.datetime2str(value)
    elif isinstance(col_type, types.Date):
        return lib.commongon.date2str(value)
    elif isinstance(col_type, types.Time):
        return lib.commongon.time2str(value)
    elif isinstance(col_type, types.LargeBinary):
        return base64.b64encode(value)
    elif isinstance(col_type, types.Binary):
        return base64.b64encode(value)
    elif isinstance(col_type, types.Boolean):
        return "1" if value else "0"
    else:
        return str(value)
    
    
def create_backup_root(name):
    return element_tree.Element('database_backup', version = "1.0", name=name)

def create_tables_root(parent):    
    return element_tree.SubElement(parent, "tables")

def write_backup_to_file(root, filename):
    tree = element_tree.ElementTree(root)
    tree.write(filename, "UTF-8")

def _backup1(metadata, filename=None, connection = None, tables_element=None, schema_only=False, data_only=False, name="", checkpoint_handler=None):
    
    backed_up_tables = []
    
    _restore_log(checkpoint_handler, filename=filename, schema_only=schema_only, data_only=data_only, name=name)
    
    
    if filename:
        root = create_backup_root(name)
        tables_element = create_tables_root(root)

    if not connection:
        conn = metadata.bind.connect()
    else:
        conn = connection
    for table in get_sorted_tables(metadata):
        backed_up_tables.append(table.name)
        _restore_log(checkpoint_handler, table_name=table.name, msg="backup table")
        table_element = element_tree.SubElement(tables_element, "table", name=table.name)
        if not data_only:
            fields_element = element_tree.SubElement(table_element, "fields")
            for col in table.c:
                field_element = element_tree.SubElement(fields_element, "field", name=col.name)
                _add_type_info(field_element, col.type)
                
                if not col.nullable:
                    field_element.set("not_nullable", "")
                if col.default:
                    field_element.set("default", _serialize_value(col.default.arg, col.type))
                if col.unique:
                    field_element.set("unique", "")
                if col.primary_key:
                    field_element.set("primary_key", "")
            constraints_element = element_tree.SubElement(table_element, "constraints")
            for constraint in table.constraints:
                if isinstance(constraint, sqlalchemy.ForeignKeyConstraint):
                    fk_element = element_tree.SubElement(constraints_element, "foreign_key")
                    if constraint.name:
                        fk_element.set("name", constraint.name)
                    if constraint.ondelete:
                        fk_element.set("ondelete", constraint.ondelete)
                    if constraint.onupdate:
                        fk_element.set("ondelete", constraint.onupdate)
                    fks = constraint.elements
                    for fk in fks:
                        field_element = element_tree.SubElement(fk_element, "field")
                        field_element.set("name", fk.parent.name)
                        field_element.set("referred_name", fk.column.name)
                        fk_element.set("referred_table", fk.column.table.name)
                elif isinstance(constraint, sqlalchemy.UniqueConstraint):
                    fk_element = element_tree.SubElement(constraints_element, "unique")
                    if constraint.name:
                        fk_element.set("name", constraint.name)
                    fks = constraint.columns
                    for fk in fks:
                        field_element = element_tree.SubElement(fk_element, "field")
                        field_element.set("name", fk.name)
                        
                        
        if not schema_only:
            data_element = element_tree.SubElement(table_element, "data")
            rows = conn.execute(table.select())
            count = 0
            for row in rows:
                row_element = element_tree.SubElement(data_element, "row")
                for col in table.c:
                    value_element = element_tree.SubElement(row_element, col.name)
                    value = row[col.name]
                    col_type = col.type
                    str_value = _serialize_value(value, col_type)
                    if not str_value is None:
                        value_element.set("null", "0")
                        value_element.text = str_value
                    else:
                        value_element.set("null", "1")
                count += 1
                if count % 10000 == 0:
                    _restore_log(checkpoint_handler, msg="Prepared %d rows for backup" % count)
                        
                    

    if filename:
        _restore_log(checkpoint_handler, msg="writing file %s" % filename)
        write_backup_to_file(root, filename)
        
    return backed_up_tables
    
def _backup(metadata, filename=None, connection = None, tables_element=None, schema_only=False, data_only=False, name="", checkpoint_handler=None):
    
    backed_up_tables = []
    
    _restore_log(checkpoint_handler, filename=filename, schema_only=schema_only, data_only=data_only, name=name)
    
    
    writer = XMLWriter(filename, encoding="UTF-8")

    writer.declaration()
    writer.start('database_backup', version = "1.0", name=name)
    writer.start('tables')
    
        

    if not connection:
        conn = metadata.bind.connect()
    else:
        conn = connection
        
    for table in get_sorted_tables(metadata):
        backed_up_tables.append(table.name)
        _restore_log(checkpoint_handler, table_name=table.name, msg="backup table")
        writer.start('table', name=table.name)

        if not data_only:
            writer.start('fields')
            for col in table.c:
                attrib = { "name" : col.name }

                data_type = col.type
                attrib["type"] = _get_type_name(data_type)
                #element.set("type", repr(data_type))
                if isinstance(data_type, types.String) and data_type.length:
                    attrib["length"] = str(data_type.length)

                
                if not col.nullable:
                    attrib["not_nullable"] = ""
                if col.default:
                    if hasattr(col.default, "arg"):
                        attrib["default"] = _serialize_value(col.default.arg, col.type)
                if col.unique:
                    attrib["unique"] = ""
                if col.primary_key:
                    attrib["primary_key"] = ""
                writer.element("field", attrib=attrib)
            writer.end('fields')
                
            writer.start('constraints')
            for constraint in table.constraints:
                if isinstance(constraint, sqlalchemy.ForeignKeyConstraint):
                    attrib = {}
                    if constraint.name:
                        attrib["name"] = str(constraint.name)
                    if constraint.ondelete:
                        attrib["ondelete"] = constraint.ondelete
                    if constraint.onupdate:
                        attrib["onupdate"] = constraint.onupdate
                    fields = []
                    fks = constraint.elements
                    for fk in fks:
                        field_attrib = {}
                        field_attrib["name"] =  str(fk.parent.name)
                        field_attrib["referred_name"] = fk.column.name
                        fields.append(field_attrib)
                        attrib["referred_table"] = fk.column.table.name
                    if fields:
                        writer.start('foreign_key', attrib)
                        for field_attrib in fields:
                            writer.element("field", attrib=field_attrib)
                        writer.end('foreign_key')
                elif isinstance(constraint, sqlalchemy.UniqueConstraint):
                    attrib = {}
                    if constraint.name:
                        attrib["name"] = constraint.name
                    writer.start('unique', attrib)
                    fks = constraint.columns
                    for fk in fks:
                        field_attrib = {}
                        field_attrib["name"] =  fk.name
                        writer.element("field", attrib=field_attrib)
                    writer.end('unique')
            writer.end('constraints')
                        
                        
        if not schema_only:
            writer.start("data")
            rows = conn.execute(table.select())
            count = 0
            for row in rows:
                writer.start("row")
                for col in table.c:
                    attrib = dict()
                    value = row[col.name]
                    col_type = col.type
                    str_value = _serialize_value(value, col_type)
                    
                    if not str_value is None:
                        text = str_value
                    else:
                        attrib["null"] = "1"
                        text = None
                    writer.element(col.name, text ,attrib)
                count += 1
                if count % 10000 == 0:
                    _restore_log(checkpoint_handler, msg="Prepared %d rows for backup" % count)
                writer.end("row")
            writer.end("data")
                        
        writer.end('table')
                    
    writer.end('tables')
    writer.end('database_backup')

        
    return backed_up_tables
        

def clear_database(connection):
    metadata = sqlalchemy.MetaData()
    metadata.bind = connection
    metadata.reflect()
    metadata.drop_all()
    SchemaFactory.unbind()
    


def _deserialize_value(value, col_type, col_name):
    if value is None:
        return ""
    if isinstance(col_type, types.DateTime):
        return lib.commongon.str2datetime(value)
    if isinstance(col_type, types.Date):
        return lib.commongon.str2date(value)
    if isinstance(col_type, types.Time):
        return lib.commongon.str2time(value)
    if isinstance(col_type, types.Binary):
        return base64.b64decode(value)
    if isinstance(col_type, types.LargeBinary):
        return base64.b64decode(value)
    if isinstance(col_type, types.Boolean):
        return False if value=="0" else True
    if isinstance(col_type, types.Integer):
        return int(value)
    if isinstance(col_type, types.Float):
        return float(value)
    if isinstance(col_type, types.String):
        if col_type.length and len(value) > col_type.length:
            raise Exception("Length of string field exceeds limit. Field name: '%s', length: '%d', value: '%s'" % (col_name, col_type.length, value))
        
    
    return value

def _restore_log(checkpoint_handler, **kwargs):
    if checkpoint_handler:
        checkpoint_handler.Checkpoint("restore", "schema_api._restore", lib.checkpoint.DEBUG, **kwargs)

def _restore_get_table_for_update(metadata, table_name):
    db_table = metadata.tables.get(table_name)
    if db_table is None:
        try:
            metadata.reflect(only=[table_name])
        except:
            return None
        db_table = metadata.tables.get(table_name)
    return db_table


class RestoreParser(object):
    
    def __init__(self, metadata, create_schema=False, data_update=True, create_not_defined=True, create_constraints=True, renamed_tables=None, renamed_columns=None, checkpoint_handler=None):
        self._state = [""]
        self._parent_stack = []
        self.current_table_object = None
        self.current_table_data = None
        self.current_table_columns = None
        self.current_table_col_names = None
        self.current_row_data = None
        
        self.metadata = metadata
        self.create_schema = create_schema
        self.data_update = data_update
        self.create_not_defined = create_not_defined
        self.create_constraints = create_constraints
        self.checkpoint_handler = checkpoint_handler
        self.map_old_table_name_to_new = renamed_tables if renamed_tables else dict()
        self.renamed_columns = renamed_columns if renamed_columns else dict()
        
        self.fk_referred_table = None
        self.fk_column_names = []
        self.fk_referred_column_names = []
        
        self.table_col_names = dict()
        self.restore_data = dict()
        
        self.event_change_tags = ["table", "data", "fields", "constraints", "unique", "foreign_key", "row"]
        
    def get_state(self):
        if len(self._state)>0:
            return self._state[-1]
        return ""

    def get_parent_state(self):
        if len(self._state)>1:
            return self._state[-2]
        return ""
    
    def parse(self, filename):
        f = file(filename, "r")
        #print "file: %s" % filename
        parser = iterparse(f, events = ["start", "end"])
        
#        context = iter(parser)
#        event, root = context.next()
        
        for event, elem in parser:
            #print "%s: %s" % (event, elem.tag)
            if event == "start":
                self._change_state(elem) 
                self._parent_stack.append(elem)
                
#                a = len(elem.getchildren())
#                if a > 100:
#                    print "%s : %d" % (elem.tag, a)
            else:
                #print "value : %s" % elem.text
                if elem.tag in self.event_change_tags:
                    self._state.pop()
                self._save_element(elem)
                elem.clear()
                self._parent_stack.pop()
                if self._parent_stack:
                    self._parent_stack[-1].remove(elem)
#                    a = len(self._parent_stack[-1].getchildren())
#                    if a > 0 and a % 100 == 0:
#                        print "%s : %d" % (self._parent_stack[-1].tag, a)
                #root.clear()
                
    def _change_state(self, elem):
        if elem.tag in self.event_change_tags:
            self._state.append(elem.tag)
        if elem.tag == "table":
            self.current_table_data = []
            self.current_table_columns = []
            self.current_table_constraints = []
            self.current_table_col_names = []

        elif elem.tag == "unique":
            self.uk_column_names = []
        elif elem.tag == "foreign_key":
            self.fk_column_names = []
            self.fk_referred_column_names = []
            self.fk_referred_table = elem.attrib.get("referred_table")
        elif elem.tag == "row" and self.get_parent_state()=="data":
            self.current_row_data = dict()
            
    def _save_element(self, elem):
        if self.get_state() == "fields":
            if elem.tag == "field":
                field = elem
                col_name = field.attrib.pop("name")
                self.current_table_col_names.append(col_name)
                if self.create_schema or self.create_not_defined:
                    col_type_name = field.attrib.pop("type")
                    try:
                        length = int(field.attrib.pop("length", 0))
                    except ValueError:
                        length = 0
                    col_type = types.__dict__[col_type_name]
                    
                    kwargs = dict()
                    if field.attrib.has_key("not_nullable"):
                        kwargs["nullable"] = False
                    if field.attrib.has_key("default"):
                        kwargs["default"] = _deserialize_value(field.attrib["default"], col_type, col_name)
                    if field.attrib.has_key("unique"):
                        kwargs["unique"] = True
                    if field.attrib.has_key("primary_key"):
                        kwargs["primary_key"] = True
    
                    if issubclass(col_type, types.String):
                        col = sqlalchemy.Column(col_name, col_type(length), **kwargs)
                    else:
                        col = sqlalchemy.Column(col_name, col_type, **kwargs)
                    self.current_table_columns.append(col)

        elif self.get_state() == "constraints":
            if not self.create_schema or self.create_not_defined or not self.create_constraints:
                return
            if elem.tag == "unique":
                uk = elem
                kwargs = dict()
                if uk.attrib.has_key("name"):
                    kwargs["name"] = uk.attrib["name"]
                self.current_table_constraints.append(sqlalchemy.UniqueConstraint(*self.uk_column_names,**kwargs))
            elif elem.tag == "foreign_key":
                fk = elem
                kwargs = dict()
                if fk.attrib.has_key("name"):
                    kwargs["name"] = fk.attrib["name"]
                if fk.attrib.has_key("ondelete"):
                    kwargs["ondelete"] = fk.attrib["ondelete"]
                if fk.attrib.has_key("ondupdate"):
                    kwargs["ondupdate"] = fk.attrib["ondupdate"]
                    
                self.current_table_constraints.append(sqlalchemy.ForeignKeyConstraint(self.fk_column_names, self.fk_referred_column_names, **kwargs))
        elif self.get_state() == "unique":
            if not self.create_schema or self.create_not_defined or not self.create_constraints:
                return
            if elem.tag == "field":
                self.uk_column_names.append(elem.attrib["name"])
            
        elif self.get_state() == "foreign_key":
            if not self.create_schema or self.create_not_defined or not self.create_constraints:
                return
            if elem.tag == "field":
                field = elem
                self.fk_column_names.append(field.attrib["name"])
                self.fk_referred_column_names.append("%s.%s" % (self.fk_referred_table, field.attrib["referred_name"]))
        elif self.get_state() == "row" and self.get_parent_state() == "data":
            if self.data_update:
                value_dict = dict()
                value_dict["name"] = elem.tag
                is_null_str = elem.get("null", "0")
                is_null = False if is_null_str=="0" else True
                value_dict["is_null"] = is_null
                
                if not is_null:
                    value = elem.text
                else:
                    value = None
                value_dict["value"] = value
                self.current_row_data[elem.tag] =  value_dict
        elif self.get_state() == "data":
            if self.data_update:
                if elem.tag == "row":
                    self.current_table_data.append(self.current_row_data)
                    n = len(self.current_row_data)
                    if n and (n % 10000 == 0):
                        _restore_log(self.checkpoint_handler, msg="Read %d rows" % n)
            
        elif elem.tag == "table":
            name = elem.get("name")
            self.table_col_names[name] = self.current_table_col_names
            
            create_schema = self.create_schema or (self.create_not_defined and not name in self.metadata.tables)
            
            _restore_log(self.checkpoint_handler, msg="Parsed table '%s'" % name)
            if create_schema:
                db_table = sqlalchemy.Table(name, self.metadata, useexisting=True, *self.current_table_columns)
                for c in self.current_table_constraints:
                    db_table.append_constraint(c)                
            
            if self.data_update:
                _restore_log(self.checkpoint_handler, msg="Updating data")
                
                if self.map_old_table_name_to_new.has_key(name):
                    new_table_name = self.map_old_table_name_to_new.get(name)
                    db_table = _restore_get_table_for_update(self.metadata, new_table_name)
                else:
                    db_table = _restore_get_table_for_update(self.metadata, name)
                if db_table is not None:
                    
                    _restore_log(self.checkpoint_handler, table=name, rows=len(self.current_table_data))

                    table_data = []
                    col_names = self.table_col_names.get(name)
                    column_map_tmp = self.renamed_columns.get(db_table.name, dict())
                    column_map = dict()
                    for item in column_map_tmp.items():
                        column_map[item[1]] = item[0]
                    restore_columns = []
                    for col in db_table.c:
                        if column_map.has_key(col.name):
                            old_name = column_map.get(col.name)
                        else:
                            old_name = col.name
                        if old_name in col_names:
                            restore_columns.append((col, old_name))
                    
                    for row in self.current_table_data:
                        values = dict()
                        for col, old_name in restore_columns:
                            value_tag = row.get(old_name)
                            if value_tag and not value_tag.get("is_null"):
                                value = value_tag.get("value")
                                # Am empty string in content is returned as None so we must change it manually
                                if value is None:
                                    value = ""
                                else:
                                    value = _deserialize_value(value, col.type, col.name)
                            else:
                                value = None
                            values[col.name] = value
                        table_data.append(values)
                    
                    self.restore_data[db_table.name] = table_data
                    
class RestoreParser1(object):
    
    def __init__(self, checkpoint_handler=None):
        self._state = [""]
        self.current_row_data = None
        self._parent_stack = []
        
        self.checkpoint_handler = checkpoint_handler
        
        self.table_name = ""
        self.event_change_tags = ["tables", "table", "data"]
        self.next_event = 0
        self.done = False
        
    def get_state(self):
        if len(self._state)>0:
            return self._state[-1]
        return ""

    def get_parent_state(self):
        if len(self._state)>1:
            return self._state[-2]
        return ""
    
    def get_data(self, filename, table_name):
        f = file(filename, "r")
        self.table_name = table_name
        parser = iterparse(f, events = ["start", "end"])
        for event, elem in parser:
            if self.done:
                break
            if event == "start":
                self._state.append(elem.tag)
                self._change_state(elem) 
                self._parent_stack.append(elem)
            else:
                data = self._save_element(elem)
                self._state.pop()
                elem.clear()
                self._parent_stack.pop()
                if self._parent_stack:
                    self._parent_stack[-1].remove(elem)
                
                if data:
                    yield data
                
    def _change_state(self, elem):
        if self.next_event < len(self.event_change_tags):
            if elem.tag == self.event_change_tags[self.next_event]:
                if elem.tag == "table":
                    name = elem.attrib["name"]
                    if name == self.table_name:
                        self.next_event += 1
                else:
                    self.next_event += 1
        elif elem.tag == "row":
            self.current_row_data = dict()                

            
    def _save_element(self, elem):
        if self.next_event == len(self.event_change_tags):
            if elem.tag == "row":
                return self.current_row_data
            elif elem.tag == "data" and self.get_parent_state() == "table":
                self.next_event = 0
                self.done = True
            else:
                value_dict = dict()
                value_dict["name"] = elem.tag
                is_null_str = elem.get("null", "0")
                is_null = False if is_null_str=="0" else True
                value_dict["is_null"] = is_null
                
                if not is_null:
                    value = elem.text
                else:
                    value = None
                value_dict["value"] = value
                self.current_row_data[elem.tag] =  value_dict
        

def reverse_dict(d):
    reverse = dict()
    for item in d.items():
        reverse[item[1]] = item[0]
    return reverse
    

def _restore(metadata, filename, schema_update=False, data_update=True, create_not_defined=True, create_constraints=True, renamed_tables=None, renamed_columns=None, checkpoint_handler=None):
    conn = metadata.bind
    
    _restore_log(checkpoint_handler, filename=filename, schema_update=schema_update, 
                 data_update=data_update, create_not_defined=create_not_defined, create_constraints=create_constraints)
    
    if not os.path.exists(filename):
        _restore_log(checkpoint_handler, msg="Skip restore - file does not exist", filename=filename) 
        return

    if schema_update:
        _restore_log(checkpoint_handler, msg="Updating schema")
        
        metadata.drop_all()
        metadata.clear()
    else:
        _restore_log(checkpoint_handler, msg="Not updating schema")
    
    parser = RestoreParser(metadata, create_schema=schema_update, data_update=False, create_not_defined=create_not_defined, create_constraints=create_constraints, renamed_tables=renamed_tables, renamed_columns=renamed_columns, checkpoint_handler=checkpoint_handler)
    parser.parse(filename)
        
    
    #print "Done parsing ******************************************************************************"

    metadata.create_all()

    if data_update:

        for table in get_sorted_tables(metadata, reverse=True):
            conn.execute(table.delete())
                
                
        map_new_table_name_to_old = reverse_dict(parser.map_old_table_name_to_new)    
        for table_object in get_sorted_tables(metadata):
                
            old_table_name = map_new_table_name_to_old.get(table_object.name, table_object.name)
            col_names = parser.table_col_names.get(old_table_name)
            if col_names is None:
                continue
                
            data_parser = RestoreParser1(checkpoint_handler)
            table_rows = data_parser.get_data(filename, old_table_name)
            _restore_log(checkpoint_handler, msg="Updating table %s" % table_object.name)
            start_index = 0
            insert_count = 1000
            
            column_map_tmp = parser.renamed_columns.get(table_object.name, dict())
            column_map = dict()
            for item in column_map_tmp.items():
                column_map[item[1]] = item[0]
            restore_columns = []
            for col in table_object.c:
                if column_map.has_key(col.name):
                    old_name = column_map.get(col.name)
                else:
                    old_name = col.name
                if old_name in col_names:
                    restore_columns.append((col, old_name))
            
            table_data = []
            count = 0
            for row in table_rows:
                
                values = dict()
                for col, old_name in restore_columns:
                    value_tag = row.get(old_name)
                    if value_tag and not value_tag.get("is_null"):
                        value = value_tag.get("value")
                        # Am empty string in content is returned as None so we must change it manually
                        if value is None:
                            value = ""
                        else:
                            value = _deserialize_value(value, col.type, col.name)
                    else:
                        value = None
                    values[col.name] = value
                table_data.append(values)
                
                if len(table_data) % insert_count == 0:
                    conn.execute(table_object.insert(), table_data)
                    table_data = []
                count += 1
                if count % 10000 == 0:
                    _restore_log(checkpoint_handler, msg="Inserted %d rows" % count)

            if table_data:
                conn.execute(table_object.insert(), table_data)

            
class SortTableDataNode(object):
    
    def __init__(self, row):
        self.row = row
        self.count = 0
        self.pred_nodes = []
        
    def __repr__(self):
        return repr(self.row)
    
def sort_table_data(table_object, table_data):
    self_referencing_keys = []
    for constraint in table_object.constraints:
        if isinstance(constraint, sqlalchemy.ForeignKeyConstraint):
            fks = constraint.elements
            for fk in fks:
                if fk.column.table.name == table_object.name:
                    self_referencing_keys.append(fk)
    if not self_referencing_keys:
        return table_data
    else:
        nodes = [SortTableDataNode(row) for row in table_data]
        start_nodes = [n for n in nodes] 
        fk_maps = []
        for fk in self_referencing_keys:
            field_name = fk.parent.name
            referred_name = fk.column.name
            referred_map = dict()
            for node in nodes:
                value = node.row.get(referred_name)
                if value:
                    referred_map[value] = node
            for node in nodes:
                value = node.row.get(field_name)
                if value:
                    referred_node = referred_map.get(value)
                    referred_node.pred_nodes.append(node)
                    node.count += 1
                    try:
                        start_nodes.remove(node)
                    except ValueError:
                        pass
        
        sorted_nodes = []
        while len(start_nodes)>0:
            node = start_nodes.pop(0)
            sorted_nodes.append(node)
            if node.count!=0:
                raise Exception("sort_table_data : Error in partial order algorithm")
            for pred_node in node.pred_nodes:
                pred_node.count -= 1
                if pred_node.count == 0:
                    start_nodes.append(pred_node)
        
        
        return [node.row for node in sorted_nodes]
    


def backup(db_connection, filename, schema_only=False):
    metadata = sqlalchemy.MetaData()
    metadata.bind = db_connection
    return _backup(metadata, filename=filename, schema_only=schema_only, checkpoint_handler=None)

def restore(db_connection, filename, schema_update=False, data_update=True):
    metadata = sqlalchemy.MetaData()
    metadata.bind = db_connection
    metadata.reflect()
    conn = metadata.bind.connect()
    t = conn.begin()
    try:
        metadata.bind = conn
        _restore(metadata, filename, schema_update=schema_update, data_update=data_update)
        t.commit()
    except:
        t.rollback()
        raise

class SchemaCreator(object):
    
    _id_col = sqlalchemy.Column('id', Integer, primary_key=True)
    

    def __init__(self, name, checkpoint_handler=None):
        self.name = name
        self.checkpoint_handler = checkpoint_handler
        self.metadata = sqlalchemy.MetaData()

    def create_table_name(self, name):
        return self.name + "_" + name
    
    def create_table(self, name, *args, **kwargs):
        autoload = kwargs.pop('autoload', False)
        if autoload:
            try:
                if autoload==True:
                    return sqlalchemy.Table(self.create_table_name(name), self.metadata, autoload=True, **kwargs)
                else:
                    return sqlalchemy.Table(self.create_table_name(name), self.metadata, autoload=True, autoload_with=autoload.get_connection(), **kwargs)
            except sqlalchemy.exceptions.NoSuchTableError:
                raise NoSuchTableError()
        
        inherit_from = kwargs.pop('inherit_from', None)
        no_primary = kwargs.pop('no_primary', False)
        cols = []
        id_col = self._id_col.copy()
        if not no_primary:
            cols.append(id_col)
        cols.extend([a.column() for a in args])
        columns = tuple(cols)
        table = sqlalchemy.Table(self.create_table_name(name), self.metadata, *columns, mysql_engine='InnoDB')
        if inherit_from is not None:
            if no_primary:
                raise "DBAPI::create_table : inherit_from and no_primary cannot both be set"
            inherit_table = self.get_table_object(inherit_from)
            table.append_constraint(sqlalchemy.ForeignKeyConstraint([id_col.name], [inherit_table.name +  "." + id_col.name]))
        #print self.metadata.tables
        return table

    def get_table_object(self, table):
        if isinstance(table, basestring):
            return self.metadata.tables[self.create_table_name(table)]
        return table
    
    def add_foreign_key_constraint(self, from_table, from_table_column_name, to_table, **kwargs):
        '''
        if from_table==to_table:
            kwargs['use_alter'] = True
            kwargs['name'] = 'forign_key_%s' % from_table
        '''
        self.get_table_object(from_table).append_constraint(sqlalchemy.ForeignKeyConstraint([from_table_column_name], 
                                                                                            [self.get_table_object(to_table).name +  "." + self._id_col.name],
                                                                                            **kwargs))


    def add_unique_constraint(self, table, *columns, **kwargs):
        self.get_table_object(table).append_constraint(sqlalchemy.UniqueConstraint(*columns,**kwargs))
        
    def create_tables(self):
        if self.metadata.bind:
            self.metadata.create_all()
            
    
    def delete_all(self, conn=None):
        if not conn:
            conn = self.metadata.bind.connect()
            t = conn.begin()
        else:
            t = None
        try:
            for table in get_sorted_tables(self.metadata, reverse=True):
                conn.execute(table.delete())
            if t:
                t.commit()
        except:
            if t:
                t.rollback()
            raise
            
        
        

    def drop_tables(self):
        if self.metadata.bind:
            self.metadata.drop_all()
            
    def get_backup_file_name(self):
        return "schema_" + self.name + ".xml"
            
    def _create_backup_file_name(self, path, version=None):
        if not version:
            return os.path.join(path,"schema_" + self.name + ".xml")
        else:
            version_str = "_".join(["%s" % v for v in version])
            return os.path.join(path,"schema_" + self.name + "_" + version_str + ".bak")
            
    def backup(self, path, schema_only=False, data_only=False):
        file_name = self._create_backup_file_name(path)
        return _backup(self.metadata, filename=file_name, schema_only=schema_only, data_only=data_only, checkpoint_handler=self.checkpoint_handler)
            
            
    def restore(self, path, db_connection, schema_update=False, data_update=True, create_constraints=True):
        filename = self._create_backup_file_name(path)
        if self.checkpoint_handler:
            self.checkpoint_handler.Checkpoint("SchemaCreator::restore", "schema_api", lib.checkpoint.DEBUG, filename=filename)
        if os.path.exists(filename):
            old_connection = self.metadata.bind
            self.metadata.bind = db_connection
            _restore(self.metadata, filename, schema_update, data_update, create_constraints=create_constraints, create_not_defined=False, checkpoint_handler=self.checkpoint_handler)
            self.metadata.bind = old_connection
            return True
        print "Unable to find file %s" % filename
        return False
            
            
    def bind(self, engine):
        self.metadata.bind = engine
        self.create_tables()

class SchemaUpdater(SchemaCreator):
    
    def __init__(self, creator, transaction, restore_path, checkpoint_handler=None):
        SchemaCreator.__init__(self, creator.name, checkpoint_handler)
        self.creator = creator
        self.rename_table_map = dict()
        self.rename_column_map = dict()
        self.checkpoint_handler = checkpoint_handler
        self.dropped_tables = []
        self.restore_path = restore_path
        self.engine = transaction.get_connection()
        self.transaction = transaction
        self.metadata.bind = self.engine
        self._update_log(msg="SchemaUpdater::init", restore_path=restore_path)
        restored = self.restore(restore_path, self.engine, schema_update=True, data_update=False, create_constraints=False)
        if not restored:
            raise Exception("Error restoring...")
#        restored = None
#        if not restored:
#            table_names = [table.name for table in get_sorted_tables(self.creator.metadata)]
#            self._reflect_tables(table_names)
    
    def _update_log(self, **kwargs):
        if self.checkpoint_handler:
            self.checkpoint_handler.Checkpoint("SchemaUpdater", "schema_api._update", lib.checkpoint.DEBUG, **kwargs)
        
    def get_table(self, name):
        table_name = self.create_table_name(name)
        return self.creator.metadata.tables.get(table_name, None)
        '''
        if isinstance(name_or_obj, basestring):
            table_name = self.create_table_name(name_or_obj)
            return self.tables.get(table_name)
        else:
            return name_or_obj
        '''
        
    def _reflect_tables(self, table_names):
        
        ''' create temporary connection, because refection can cause problems with the transaction, e.g. some of the transaction is 'forgotten' 
        '''
        tmp_engine = connection_factory.ConnectionFactory().create_connection(self.engine.engine.url, True)
        tmp_metadata = sqlalchemy.MetaData()
        tmp_metadata.bind = tmp_engine
        
        existing_table_names = [table for table in self.metadata.tables]
        
        def relect_table(name, metadata):
            if name in table_names:
                return True
            return False
        
        tmp_metadata.reflect(only=relect_table)
        '''note that sqlalchemy also retrieves all associated tables to the ones specified''' 

        for table in get_sorted_tables(tmp_metadata):
            if table.name in table_names and not table.name in existing_table_names:
                table.tometadata(self.metadata)
            
    def rename_table(self, table_name, new_table_name):
        self._update_log(msg="rename_table", table_name=table_name, new_table_name=new_table_name)
        self.rename_table_map[self.create_table_name(table_name)] = self.create_table_name(new_table_name)
        self.drop_table(table_name)
        
    def rename_column(self, new_table_name, old_column_name, new_column_name):
        self._update_log(msg="rename_column", old_column_name=old_column_name, new_table_name=new_table_name, new_column_name=new_column_name)
        table_name = self.create_table_name(new_table_name)
        if not self.rename_column_map.has_key(table_name):
            self.rename_column_map[table_name] = dict()
        column_map = self.rename_column_map.get(table_name)
        column_map[old_column_name] = new_column_name 
        
        
    def drop_table(self, table_name):
        self._update_log(msg="drop_table", table_name=table_name)
        name = self.create_table_name(table_name)
        self.dropped_tables.append(name)
        table = self.creator.metadata.tables.get(name)
        if table is not None:
            self.creator.metadata.remove(table)

        
    def _find_changed_elements(self, old_fields, new_fields, name_attrib, rename_map):
        changed_elements = []
        dropped_elements = []
        for old_field in old_fields:
            field_name = old_field.attrib.get(name_attrib)
            if rename_map.has_key(field_name):
                field_name = rename_map.get(field_name)
            new_field = _find_in_elementlist(new_fields, name_attrib, field_name)
            if not new_field is None:
                changed = not elements_equal(old_field, new_field, [name_attrib])
                if changed:
                    print "Field %s changed" % field_name
                    changed_elements.append(new_field)
                else:
                    new_fields.remove(new_field)
            else:
                print "Field %s dropped" % field_name
                dropped_elements.append(old_field)
                
        return dropped_elements, changed_elements, new_fields
    
        
    def _diff(self, tables_element_old, tables_element_new):
        tables_old = tables_element_old.findall('table')
        tables_new = tables_element_new.findall('table')
        
        
        for old_table in tables_old:
            table_name = old_table.attrib.get("name")
            if self.rename_table_map.has_key(table_name):
                table_name = self.rename_table_map.get(table_name) 
            new_table = _find_in_elementlist(tables_new, "name", table_name)
            if new_table:
                tables_new.remove(new_table)
                old_fields = old_table.find("fields").findall("field")
                new_fields = new_table.find("fields").findall("field")
                renamed_columns = self.rename_column_map.get(table_name, dict())
                dropped_fields, changed_fields, new_fields = self._find_changed_elements(old_fields, new_fields, "name", renamed_columns)
            
                old_constraints = old_table.find("constraints")
                new_constraints = new_table.find("constraints")
                old_foreign_keys = old_constraints.findall("foreign_key")
                new_foreign_keys = new_constraints.findall("foreign_key")
                for old_fk in old_foreign_keys:
                    referred_table_name = old_fk.attrib.get("referred_table")
                    if self.rename_table_map.has_key(referred_table_name):
                        referred_table_name = self.rename_table_map.get(referred_table_name)
                    new_fk = _find_in_elementlist(new_foreign_keys, "referred_table", referred_table_name) 
                    if new_fk:
                        changed = not elements_equal(old_fk, new_fk, ["referred_table", "name"])
                        if changed:
                            print "fk to '%s' def changed" % referred_table_name
                            continue
                        else:
                            old_fields = old_fk.findall("field")
                            new_fields = new_fk.findall("field")
                            dropped_fields, changed_fields, new_fields = self._find_changed_elements(old_fields, new_fields, "name", renamed_columns)
                            if dropped_fields or changed_fields or new_fields:
                                print "fk to '%s' fields changed" % referred_table_name
                    else:
                        print "fk to %s dropped" % referred_table_name
                old_unique_keys = old_constraints.findall("unique")
                new_unique_keys = new_constraints.findall("unique")
                for old_uk in old_unique_keys:
                    old_fields = old_fk.findall("field")
                
                        
                
            
    
    def calc_diff(self):
        
        new_tables = []
        changed_tables = []
        
        old_tables_element = element_tree.Element('old')
        _backup(self.metadata, filename=None, connection=self.engine, tables_element=old_tables_element, schema_only=True)
        
        new_tables_element = element_tree.Element('new')
        _backup(self.creator.metadata, filename=None, connection=self.engine, tables_element=new_tables_element, schema_only=True)
        
        self._diff(old_tables_element, new_tables_element)
                    
        
    def update_schema(self, schema_update=True):
        self._update_log(msg="update_schema", schema_update=schema_update)

        conn = self.engine
        backup_filename = self._create_backup_file_name(self.restore_path)
        if not os.path.exists(backup_filename):
            raise Exception("Unable to find backup file '%s'" % backup_filename)

        
        
        ''' Now for the schema update:'''
        
        ''' First we drop the tables using the restored (or reflected) definition. This ensures (hopefully) that the drop order is correct '''
        for table_name in self.dropped_tables:
            table = self.metadata.tables.get(table_name)
            if table is not None:
                table.drop(checkfirst=True)
                self._update_log(msg="drop_dropped_table", table_name=table_name)

        for table_name in self.creator.metadata.tables:
            table = self.metadata.tables.get(table_name)
            if table is not None:
                table.drop(checkfirst=True)
                self._update_log(msg="drop_old_table", table_name=table_name)
        
        ''' Finally we create the tables using the new schema '''
        self.creator.metadata.bind = conn
        self.creator.create_tables()


        self._update_log(msg="created_tables", table_names=[name for name in self.creator.metadata.tables])
        
        ''' Now the data is restored '''

        # Add all non-dropped tables to the old metadata in order to restore the data
        for table_name in self.metadata.tables:
            if not table_name in self.dropped_tables and not self.creator.metadata.tables.has_key(table_name):
                table = self.metadata.tables.get(table_name)
                table.tometadata(self.creator.metadata)
            
        self._update_log(msg="call _restore", backup_filename=backup_filename)
        
        _restore(self.creator.metadata, 
                 backup_filename, 
                 schema_update=False, 
                 data_update=True, 
                 create_not_defined=False,
                 renamed_tables = self.rename_table_map,
                 renamed_columns = self.rename_column_map,
                 checkpoint_handler=self.checkpoint_handler
                 )
        
    def update_finished(self, to_version):
        ''' Transaction needs to be flushed in order for the changes to be visible when using the connection '''
        if not self.creator.metadata.bind:
            self.creator.metadata.bind = self.engine
        self._update_log(msg="Update finished - flush data")
        self.transaction._session.flush()
        filename = self._create_backup_file_name(self.restore_path)
        self._update_log(msg="call _backup", backup_filename=filename)
        _backup(self.creator.metadata, filename)

        # Create version specific backup file so that data can be recovered
        filename_version = self._create_backup_file_name(self.restore_path, to_version)
        shutil.copy(filename, filename_version)
#            _backup(self.creator.metadata, filename)

        
        


class Relation(object):
    '''
        Define a foreign key relation to a class. Used as a parameter to "mapper". 
        Note that specifying the "backref_property_name" will create a property on the related class and that changing content of
        the property on the mapped class will affect the contents of the backref property on the related class and vice versa. 
        If you don't want this behaviour you can just create the relation on both mapped classes without setting the "backref_property_name"
        
        related_class : The related class
        
        property_name : Name of the property which will contain the related object(s)
        
        backref_property_name : Name of the property on the related class, which will contain the related object(s)
        
        intermediate_table : Name of intermediate table if this is a many-to-may relationsship
        
    '''
    
    def __init__(self, related_class, property_name, backref_property_name = None, one_to_one=None, intermediate_table = None, primaryjoin=None, circular=False, remote_side=None):
        self.related_class = related_class
        self.property_name = property_name
        self.backref_property_name = backref_property_name
        self.intermediate_table = intermediate_table
        self.primaryjoin = primaryjoin
        if one_to_one is None:
            self.uselist = None
        else:
            self.uselist = not one_to_one
        self.remote_side = remote_side
    
        self.post_update = circular
        
class Column(object):
    '''
        Interface to SQLAlchemy's Column class
    '''
    
    def __init__(self, name, type, **kwargs):
        if kwargs.pop("primary_key", None):
            print "Warning"
        self.name = name
        self.type = type
        self.kwargs = kwargs
        
    def column(self):
        return sqlalchemy.Column(self.name, self.type, **self.kwargs)



    


class MyMapperExtension(orm.MapperExtension):
    '''
         Extension we use in mappers in order to ensure that a session is available when selecting from the database
    '''
    
    def get_session(self):
        return SessionFactory.create_contextual_session()
        

def _get_table_column(table, string_or_column):
    if isinstance(string_or_column, basestring):
        return getattr(table.c, string_or_column)
    else:
        return string_or_column
    
'''
   mapper: create mapping from table to a class
   
   class_ : The class which will get properties from the table
   
   local_table : The table to be mapped

   relations  :  List of (foreign key) relations which should be mapped. Each element should be an object of class Relation.
                 Note that adding a relation means that the objects are loaded from the database, so only use it if you need it.
                 
   column_name_map : By default the properties created on the class get the name of their respective column. This can be overridden
                     with this map. The map should be of type : Dict(<property_name> = <table_column>), e.g. { 'my_id' : my_table.c.id }
                     or just { 'my_id' : 'id' }
                     
   polymorphic_on : Used with mappers in an inheritance relationship, a Column (name) which will identify the class/mapper combination 
                   to be used with a particular row. requires the polymorphic_identity value to be set for all mappers in the inheritance hierarchy.
   
   inherits : Super class with which the mapped class will have an inheritance relationship with.
   
   polymorphic_identity : A value which will be stored in the Column denoted by polymorphic_on, in order to denote the polymorphic type.
   
   no_primary : Indicate that the mapped table doesn't have a primary key. As a consequence all columns are used as primary key. Note
                that this will not work if table contains NULL values
                
   include_properties : An inclusive list of properties to map. Columns present in the mapped table but not present in this list 
                        will not be automatically converted into properties.
                        
   exclude_properties : A list of properties not to map. Columns present in the mapped table and present in this list will not be 
                        automatically converted into properties. Note that neither this option nor include_properties will 
                        allow an end-run around Python inheritance. If mapped class B inherits from mapped class A, no combination 
                        of includes or excludes will allow B to have fewer properties than its superclass, A.
   
'''

def mapper(class_, local_table,
           relations = None,
           column_name_map = None,
           polymorphic_on=None,
           inherits = None,
           polymorphic_identity = None,
           no_primary = False,
           include_properties = None,
           exclude_properties = None,
          ):
    kwargs = dict()
    if polymorphic_on is not None or inherits is not None:
        kwargs['polymorphic_identity'] = polymorphic_identity
        kwargs['polymorphic_on'] = _get_table_column(local_table, polymorphic_on)
        kwargs['inherits'] = inherits
    properties = dict()
    if relations:
        for r in relations:
            backref_property = None
            if r.backref_property_name:
                backref_property = orm.backref(r.backref_property_name, uselist=r.uselist, primaryjoin=r.primaryjoin)
                    
            if r.related_class!=class_:
                related_table = orm.class_mapper(r.related_class).mapped_table
            else:
                related_table = local_table
            properties[r.property_name] = orm.relation(r.related_class,
                                                       lazy=False, 
                                                       #order_by=related_table.c.id,
                                                       backref=backref_property, 
                                                       secondary = r.intermediate_table,
                                                       uselist = r.uselist, 
                                                       primaryjoin = r.primaryjoin,
                                                       post_update=r.post_update,
                                                       remote_side = r.remote_side
                                                       )
    kwargs['properties'] = properties
    if no_primary:
        kwargs['primary_key'] = local_table.columns
    
    if column_name_map:
        for key in column_name_map.keys():
            value = column_name_map[key]
            column_name_map[key] = _get_table_column(local_table, value)
        properties.update(column_name_map)
    
    if include_properties:
        if not no_primary and SchemaCreator._id_col.name not in include_properties:
            include_properties.append(SchemaCreator._id_col.name)
        kwargs['include_properties'] = include_properties
    if exclude_properties:
        kwargs['exclude_properties'] = exclude_properties
    
    kwargs['extension'] = MyMapperExtension() 
    orm.mapper(class_, local_table, **kwargs)


def _find_in_elementlist(elements, attrib_name, attrib_value):
    for e in elements:
        value = e.attrib.get(attrib_name)
        if value==attrib_value:
            return e
    return None
    
def elements_equal(e1, e2, ignorelist=[]):
    for item in e1.attrib.items():
        if item[0] in ignorelist:
            continue
        e2value = e2.attrib.get(item[0])
        if e2value!=item[1]:
            print "element match conflict : attrib '%s', '%s' vs '%s'" % (item[0], item[1], e2value)
            return False
    return True

class AuxTableRemover(object):
    
    def __init__(self, transaction, restore_path, tables_to_delete = [], checkpoint_handler=None):
        self.checkpoint_handler = checkpoint_handler
        self.transaction = transaction
        self.restore_path = restore_path
        self.deleted_tables = tables_to_delete
        
        
    def remove_tables(self, to_version):
        if self.checkpoint_handler:
            self.checkpoint_handler.Checkpoint("AuxTableRemover", "schema_api", lib.checkpoint.DEBUG, msg="copy aux file")
        
        aux_file_name = os.path.join(self.restore_path, SchemaFactory._AUX_BACKUP_FILENAME)
        if not os.path.exists(aux_file_name):
            self.checkpoint_handler.Checkpoint("AuxTableRemover", "schema_api", lib.checkpoint.WARNING, msg="Aux schema file '%s' not found" % aux_file_name)
            return
        
        version_str = "_".join(["%s" % v for v in to_version]) 
        aux_file_bak_name = os.path.join(self.restore_path, "%s.%s.bak" % (SchemaFactory._AUX_BACKUP_FILENAME, version_str))
        shutil.copy(aux_file_name, aux_file_bak_name)
        
        f = file(aux_file_bak_name, "r")
        parser = iterparse(f, events = ["start", "end"])
        
#        # turn it into an iterator
#        context = iter(parser)
#
#        # get the root element
#        event, root = context.next()
        out_filename = aux_file_name
        writer = XMLWriter(out_filename, encoding="UTF-8")
        writer.declaration()

        write = True
        for event, elem in parser:
            if event == "start":
                if elem.tag=="table":
                    if elem.get("name") in self.deleted_tables:
                        print "Skip %s" % elem.get("name")
                        write = False
                    else:
                        print "Don't Skip %s" % elem.get("name")
                if write:
                    writer.start(elem.tag, elem.attrib)
                
            if event == "end":
                if write:
                    if not elem.getchildren() and elem.text:
                        writer.data(elem.text)
                    writer.end(elem.tag)
                if elem.tag=="table" and elem.get("name") in self.deleted_tables:
                    print "End Skip %s" % elem.get("name")
                    write = True
                    
        



if __name__ == '__main__':

    from connection_factory import ConnectionFactory

    connection = ConnectionFactory(None).create_connection('mssql://@localhost\SQLEXPRESS/test_upgrade', True)
    #connection = ConnectionFactory(None).create_connection("sqlite:///../../setup/demo/gon_server_db.sqlite", True)
    
    filename = "C:\\Temp\\TestUpgrade\\all1.xml"
    #filename = "C:\\Temp\\TestUpgrade\\schema_auth_server.xml"
    
    #backup(connection, filename)
    restore(connection, filename, schema_update=True)
    #restore(connection, filename)
    
        
        

#    p.get_data("C:\\Temp\\holland_upgrade\\work_backup\\database\\aux_schema.xml")
    
                            
