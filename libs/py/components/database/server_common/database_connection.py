"""
Database configuration module
"""
import sqlalchemy
import sqlalchemy.pool
import sqlalchemy.engine.url


class DatabaseConnection(object):

    #Session = sessionmaker(autoflush=True, transactional=True)
    engines = {}
        
    
    @classmethod
    def get_engine(cls, config, echo=False, logger=None, encoding=None, pool_size=5):
        is_sqlite = True
        if type(config) != sqlalchemy.engine.url.URL:
            is_sqlite = "sqlite" in config
        else:
            is_sqlite = config.drivername.lower() == "sqlite"
        engine = cls.engines.get(config, None)
        if not engine:
            #print "Database : connecting to " + config
            kwargs = dict()
            kwargs["echo"] = echo  
            kwargs["convert_unicode"] = True  
            kwargs["strategy"] = 'threadlocal'
            if not is_sqlite:  
                kwargs["max_overflow"] = -1
                kwargs["pool_size"] = pool_size
            if encoding:
                kwargs["encoding"] = str(encoding)

            engine = sqlalchemy.create_engine(config, **kwargs)
            cls.engines[config] = engine
            if logger:
                engine.logger = logger
#        else:
#            if engine.echo!=echo:
#                engine.echo = echo
#                if echo and logger:
#                    engine.logger = logger
                
        return engine
    


                            

        