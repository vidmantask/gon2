'''
Created on 19/06/2012

Database API for config specific tasks 

@author: pwl
'''
import sys

import pyodbc
#import MySQLdb
import database_api
import database_connection
import lib.checkpoint as checkpoint


class PyODBCConnection(object):

    def __init__(self, db_connect_info):
        driver = "{SQL Server}"
        query = db_connect_info.query
        if query and query.has_key("Driver"):
            driver = query.pop("Driver")
        pyodbc_connect_str = "DRIVER=%s;SERVER=%s" % (driver, db_connect_info.host, )
        if db_connect_info.port:
            pyodbc_connect_str += ",%s" % (db_connect_info.port, )
        if db_connect_info.username:
            pyodbc_connect_str += ";UID=%s;PWD=%s" % (db_connect_info.username, db_connect_info.password)
#        if db_connect_info.port:
#            pyodbc_connect_str += ";PORT=%s" % (db_connect_info.port, )
        if query:
            for (key,value) in query.items():
                pyodbc_connect_str += ";%s=%s" % (key, value)
            
        self.cnxn = pyodbc.connect(pyodbc_connect_str, autocommit=True)
    
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self.close()
        
    def cursor(self):
        return self.cnxn.cursor()
    
    def close(self):
        if self.cnxn:
            self.cnxn.close()

def get_database_encoding(db_connect_info):    
    if db_connect_info.db_type=="mssql":
        
        cnxn = PyODBCConnection(db_connect_info)
        cursor = cnxn.cursor()
        try:
            result = cursor.execute("select collation_name from sys.databases where name='%s'" % db_connect_info.database)
            collation_name_rec = result.fetchone()
            if collation_name_rec and collation_name_rec[0]:
                collation_name = collation_name_rec[0]
                result = cursor.execute("SELECT CAST(collationproperty('%s', 'CodePage') AS varchar(255))" % collation_name)
                codepage_rec = result.fetchone()
                if codepage_rec:
                    codepage = codepage_rec[0]
                    return "cp%s" % codepage
                else:
                    raise Exception("No codepage found for collation '%s'" % collation_name)
            else:
                raise Exception("No collation name found for database '%s'" % db_connect_info.database)
        finally:
            cursor.close()
    elif db_connect_info.db_type=="mysql":
        db = MySQLdb.connect(host=db_connect_info.host, 
                             user=db_connect_info.username, 
                             passwd=db_connect_info.password)

        db.query("SELECT default_character_set_name FROM information_schema.SCHEMATA WHERE schema_name = '%s'" % (db_connect_info.database,))
        result = db.store_result()
        charset_name_rec = result.fetch_row()
        if charset_name_rec and charset_name_rec[0]:
            return charset_name_rec[0][0]
        else:
            raise Exception("No character set name found for database '%s'" % db_connect_info.database)
        
#        cursor = db.cursor()
#        try:
#            result = cursor.execute("SELECT default_character_set_name FROM information_schema.SCHEMATA WHERE schema_name = %s" % (db_connect_info.database,))
#            charset_name_rec = cursor.fetchone()
#            if charset_name_rec and charset_name_rec[0]:
#                return charset_name_rec[0]
#            else:
#                raise Exception("No character set name found for database '%s'" % db_connect_info.database)
#                
#        finally:
#            cursor.close() 
    else:
        return "utf-8"


def check_database_exists(db_connect_info):    
    if db_connect_info.db_type=="mssql":
        cnxn = PyODBCConnection(db_connect_info)
        cursor = cnxn.cursor()
        try:
            count = cursor.execute("select count(*) from sys.databases where name = '%s'" % (db_connect_info.database)).fetchone()[0]
            if count == 0:
                raise Exception("The database '%s' does not exist" % db_connect_info.database)
        finally: 
            cursor.close()
    elif db_connect_info.db_type=="mysql":
        db = MySQLdb.connect(host=db_connect_info.host, 
                             user=db_connect_info.username, 
                             passwd=db_connect_info.password,
                             db=db_connect_info.database)


def create_database(db_connect_info):    
    if db_connect_info.db_type=="mssql":
        cnxn = PyODBCConnection(db_connect_info)
        cursor = cnxn.cursor()
        cursor.execute("if not exists(select * from sys.databases where name = '%s') create database [%s]" % (db_connect_info.database, db_connect_info.database))
        cursor.execute("ALTER DATABASE [%s] SET AUTO_CLOSE OFF" % db_connect_info.database)         
        cursor.close()
    elif db_connect_info.db_type=="mysql":
        db = MySQLdb.connect(host=db_connect_info.host, 
                             user=db_connect_info.username, 
                             passwd=db_connect_info.password)
        cursor = db.cursor()
        try:
            cursor.execute("create database if not exists %s" % (db_connect_info.database,))
        finally:
            cursor.close() 


def test_connection(db_connect_info, checkpoint_handler=None):
    
    
    connect_str = database_api.get_connect_url(db_connect_info)
    try:
        engine = database_connection.DatabaseConnection.get_engine(connect_str, encoding=db_connect_info.encoding)
        creator = database_api.SchemaCreator("test")
        
        table = creator.create_table("table_gon_test1", 
                             database_api.Column('str1', database_api.String(1)),
                             database_api.Column('str8000', database_api.String(8000)),
                             database_api.Column('text', database_api.Text),
                             database_api.Column('int', database_api.Integer),
                             database_api.Column('float', database_api.Float),
                             database_api.Column('dt', database_api.DateTime),
                             database_api.Column('bin', database_api.LargeBinary),
                             database_api.Column('bool', database_api.Boolean),
                            )
        creator.bind(engine)
        
        creator.drop_tables()
        
        return True, "Database connection ok"

    except Exception, e:
        if checkpoint_handler:
            (etype, evalue, etrace) = sys.exc_info()
            checkpoint_handler.CheckpointException("test_connection", "database_api", checkpoint.WARNING, etype, evalue, etrace)
        return False, repr(e)
