from __future__ import with_statement
import sqlalchemy
import sqlalchemy.orm as orm
import lib.checkpoint as checkpoint
import database_connection
from schema_api import SchemaFactory
from schema_api import mapper
import threading
import sqlalchemy.sql as sql
import sqlalchemy.engine.url as sqlalchemy_url

import sys

from schema_api import *
from sqlalchemy.orm import reconstructor

reconstructor_method = reconstructor

def and_(*clauses):
    return sqlalchemy.and_(*clauses)

def or_(*clauses):
    return sqlalchemy.or_(*clauses)

def not_(*clauses):
    return sqlalchemy.not_(*clauses)

def in_(column, clause_or_list):
    return column.in_(clause_or_list)

def like(column, clause_or_list):
    return column.like(clause_or_list)

def exists(*clauses):
    return sql.exists(*clauses)

def not_exists(*clauses):
    return ~sql.exists(*clauses)

def lower(*expr):
    return sqlalchemy.func.lower(*expr)

def desc(*clauses):
    return sqlalchemy.desc(*clauses)

IN_CLAUSE_LIMIT = 999

def max_(column, label=None, type_= None):
    if type_:
        function = sql.func.max(column, type_=type_)
    else:
        function = sql.func.max(column)
    if label:
        return function.label(label)
    else:
        return function

reconstructor = orm.reconstructor

class InvalidRequestException(Exception):
    pass

class QueryResultBase(object):
    
    def __init__(self, *attribute_dict):
        self.__dict__.update(*attribute_dict)
        self.init()
        
    def init(self):
        pass


class QuerySession(object):
    
    def __init__(self):
        self.result = None


#    def _query(self, cls, filter):
#        #print issubclass(mapper_or_class, PersistentObject)
#        class_mapper = orm.class_mapper(cls)
#        select = sqlalchemy.sql.select([class_mapper.local_table], filter)
#        result = class_mapper.local_table.bind.execute(select)
#        return self._session.query(mapper_or_class, *addtl_entities, **kwargs)

    
    def _select(self, table_or_cls, filter=None, execute=True, **kwargs):
        if type(table_or_cls)==list:
            query = sqlalchemy.sql.select(table_or_cls)
            local_table = kwargs.get("from_")
            if not local_table:
                local_table = table_or_cls[0]
            else:
                if type(local_table)==type:
                    class_mapper = orm.class_mapper(local_table)
                    local_table = class_mapper.local_table
        else:
            if type(table_or_cls)!=type:
                local_table = table_or_cls
            else:
                class_mapper = orm.class_mapper(table_or_cls)
                local_table = class_mapper.local_table
            if kwargs.get("count"):
                if kwargs.has_key("group_by"):
                    arg = kwargs.get("group_by")
                    arg_list = [sqlalchemy.func.count(local_table.c.id)]
                    if isinstance(arg, list) or isinstance(arg, tuple):
                        arg_list.extend(arg)
                    else:
                        arg_list.append(arg)
                    query = sqlalchemy.sql.select(arg_list)
                else:
                    query = sqlalchemy.sql.select([sqlalchemy.func.count(local_table.c.id)])
            else:
                query = sqlalchemy.sql.select([local_table])
        if filter is not None:
            query = query.where(filter)
        if kwargs.get("distinct"):
            query = query.distinct()
        if kwargs.has_key("order_by"):
            arg = kwargs.get("order_by")
            if isinstance(arg, list) or isinstance(arg, tuple):
                tmp = tuple(arg)
                query = query.order_by(*tmp)
            else:
                query = query.order_by(arg)
                
        if kwargs.has_key("group_by"):
            arg = kwargs.get("group_by")
            if isinstance(arg, list) or isinstance(arg, tuple):
                tmp = tuple(arg)
                query = query.group_by(*tmp)
            else:
                query = query.group_by(arg)
        if kwargs.has_key("outerjoin"):
            query = query.outerjoin(kwargs.get("outerjoin"))
        elif kwargs.has_key("join"):
            onclause = kwargs.get("onclause")
            query = query.join(kwargs.get("join"), onclause=onclause)
            
        if execute:
            if type(local_table)==sqlalchemy.Column:
                local_table = local_table.table
            self.result = local_table.bind.execute(query)
            return self.result
        else:
            return query
        

    def _get_object(self, row, result_class=None):
        if result_class:
            return result_class(row)
        else:
            return row
    
    def select(self, table_or_cls, filter=None, result_class=None, **kwargs):
        result = self._select(table_or_cls, filter, **kwargs)
        return [self._get_object(row, result_class) for row in result]
        
        
    def select_one(self, table_or_cls, filter=None, result_class=None, **kwargs):
        result = self._select(table_or_cls, filter, **kwargs)
        row = result.fetchone()
        return self._get_object(row, result_class)
        
    def select_first(self, table_or_cls, filter=None, result_class=None, **kwargs):
        result = self._select(table_or_cls, filter, **kwargs)
        try:
            row = result.fetchone()
            return self._get_object(row, result_class)
        except Exception, e:
            print e
            return None

    # TODO : Optimize this...
    def select_count(self, table_or_cls, filter=None, **kwargs):
        kwargs["count"] = True
        result = self.select(table_or_cls, filter, **kwargs)
        if len(result)==1 and len(result[0])==1:
            return result[0][0]
        else:
            return result
            
    
    def select_expression(self, table_or_cls, filter=None, **kwargs):
        return self._select(table_or_cls, filter, execute=False, **kwargs)
    
    def get(self, table_or_cls, id, result_class=None):
        if type(table_or_cls)!=type:
            filter=table_or_cls.c.id==id
        else:
            filter=table_or_cls.id==id
        return self.select_one(table_or_cls, filter=filter, result_class=result_class)
        
            
    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        if self.result:
            self.result.close()
    
    def close(self):
        if self.result:
            self.result.close()
    
class AfterFlushExtension(SessionExtension):
    
    def _before_commit(self, session):
        for obj in session:
            if isinstance(obj, PersistentObject):
                try:
                    obj.before_commit()
                except:
                    pass
                
#    def after_commit(self, session):
#        print "> after_commit"
#
#    def before_flush(self, session, flush_context, instances):
#        print '> before_flush'
#
#    def after_flush(self, session, flush_context):
#        print '> after_flush'
#
#    def after_flush_postexec(self, session, flush_context):
#        print '> after_flush_postexec'

    
class SessionFactory(object):
    
    ReadSession = orm.sessionmaker(autoflush=False, autocommit=True, expire_on_commit=False, extension=AfterFlushExtension())
    ScopedSession = orm.scoped_session(orm.sessionmaker(autoflush=True, autocommit=False, expire_on_commit=False, extension=AfterFlushExtension()))
    WriteSession = orm.sessionmaker(autoflush=True, autocommit=False, expire_on_commit=False, extension=AfterFlushExtension())
    _sessions = []
    
    
    @classmethod
    def create_write_session(cls, connection=None):
        #return cls.ScopedSession()
        return cls.WriteSession(bind=connection)
    
    @classmethod
    def create_read_session(cls):
        return cls.ReadSession()
        #return cls.ScopedSession()

    @classmethod
    def create_contextual_session(cls):
        #cls.ScopedSession.close()
        return cls.ScopedSession()
    
    @classmethod
    def bind(cls, engine):
        cls.ReadSession.configure(bind = engine)
        cls.WriteSession.configure(bind = engine)
        cls.ScopedSession.configure(bind = engine)
        

class ReadonlySession(object):
    
    def __init__(self):
        self._session = self._create_session()
#        print "ReadonlySession"
        
    def _create_session(self):
        return SessionFactory.create_read_session()
    
    def _readonly(self):
        return True

    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        self._session.close()
        
    def _query(self, mapper_or_class, *addtl_entities, **kwargs):
        query =  self._session.query(mapper_or_class, *addtl_entities, **kwargs)
#        if not self._readonly():
#            return query.with_lockmode('update')
#        else:
#            return query
        return query
        
    def close(self):
        self._session.close()


    def select(self, mapped_class, filter=None, order_by=None, **kwargs):
        return DBAPI.select(mapped_class, self, filter, order_by, **kwargs)

    def select_count(self, mapped_class, **kwargs):
        return DBAPI.select_count(mapped_class, self, **kwargs)

    def select_one(self, mapped_class, **kwargs):
        return DBAPI.select_one(mapped_class, self, **kwargs)
        
    
    def select_first(self, mapped_class, filter=None, order_by=None, **kwargs):
        return DBAPI.select_first(mapped_class, self, filter, order_by, **kwargs)
            
    def select_expression(self, mapped_class, filter=None, order_by=None, **kwargs):
        return DBAPI.select_expression(mapped_class, self, filter, order_by, **kwargs)
    
    def get(self, mapped_class, id):
        return DBAPI.get(mapped_class, id, self)
    


class Transaction(ReadonlySession):
    
    commit_lock = threading.Lock()
    
    def __init__(self, connection=None, autocommit=True):
        self.autocommit = autocommit
        self.connection = connection
        super(Transaction,self).__init__()
        
    
    def _create_session(self):
        return SessionFactory.create_write_session(self.connection)

    def _readonly(self):
        return False

    def __enter__(self):
        return self
        
    def __exit__(self, type, value, traceback):
        if self.autocommit and type is None:
            self.commit()
        else:
            self.rollback()
        self._session.close()


    def add(self, obj):
        obj_session = self._session.object_session(obj)
        if obj_session: 
            if obj_session == self._session:
                return obj
            obj_session.expunge(obj)    
            #raise "Error : object already loaded in transaction"
        self._session.add(obj)
        return obj
    
    def add_list(self, list):
        for obj in list:
            obj_session = self._session.object_session(obj)
            if obj_session and obj_session != self._session:
                obj_session.expunge(obj)
            self._session.merge(obj)
            #delattr(obj, "_instance_key")
                
    def get_connection(self):
        return self._session.connection()

    def commit(self):
        with self.commit_lock:
            self._session.commit()
        
    def flush(self):
        self._session.flush()
        
    def rollback(self):
        self._session.flush()
        self._session.rollback()
        
    def remove(self, obj):
        self._session.expunge(obj)

    def create(self, cls, **kwargs):
        obj = cls(**kwargs)
        self.add(obj)
        return obj
    
    def delete(self, obj):
        self.add(obj)
        self._session.delete(obj)

    def delete_sequence(self, l):
        for obj in l:
            self.delete(obj)
            
    def delete_direct(self, mapped_class, filter=None):
        class_mapper = orm.class_mapper(mapped_class)
        local_table = class_mapper.local_table
        query = local_table.delete()
        if filter is not None:
            query = query.where(filter)
        result = self._session.execute(query)
        return result.rowcount

        
    def execute(self, clause, mapped_class):
        result = self._session.execute(clause, mapper = mapped_class)
        result.close()
        
    def delete_all_tables(self, creator):
        try:
            conn = self._session.connection()
        except:
            table = None
            for t in get_sorted_tables(creator.metadata):
                table = t
                break
            class Temp(object):
                pass
            
            m = orm.mapper(Temp, table)
            conn = self._session.connection(mapper=m)
            
        self._session.bind = conn
        creator.delete_all(conn)
        
    def delete_all(self, class_):
        
        for obj in self.select(class_):
            self.delete(obj)
        
        '''
        class_mapper =  orm.class_mapper(class_)
        if class_mapper:
            self.execute(class_mapper.local_table.delete(), class_)
        '''
    

class SessionWrapper(Transaction):
    '''
        Utility class used when receiving a database session which may be null. Wrapping this session in an object
        of this class means that you can   
    '''
    
    def __init__(self, db_session, read_only=True):
        self.read_only = read_only
        if not db_session:
            self.local_session = True
            if read_only:
                self.db_session = ReadonlySession()
            else:
                self.db_session = Transaction()
        else:
            self.local_session = False
            self.db_session = db_session
            
        self._session = self.db_session._session
            
    
    def close(self):
        if self.local_session:
            self.db_session.close()
    
    def __exit__(self, type, value, traceback):
        if self.local_session:
            if not self.read_only and type is None:
                self.commit()
            self.close()

    def _readonly(self):
        return self.read_only
             


class PersistentObject(object):
    
    def __init__(self, **kwargs):
        for k in kwargs.keys():
            setattr(self, k, kwargs[k])
        
    
    @classmethod
    def select(cls, db_session, filter=None):
        """Return list of all instances of this class in the database. 
        """
        return DBAPI.select(cls, db_session, filter)
            
    
    
    @classmethod
    def get(cls, db_session, id):
        return DBAPI.get(cls, id, db_session)
    
    def __repr__(self):
        try:
            s = "< " + self.__class__.__name__  + ": "
            for key in self.c.keys():
                 s += key + "=" + repr(getattr(self, key, 'N/A')) + ", "
            s += ">"
            return s
        except:
            return "Error creating string"
    
    def __json__(self):
        props = {}
        for key in self.c.keys():
           props[key] = getattr(self, key)
        return props

    def before_commit(self):
        pass





class DBAPI(object):
    

    @classmethod
    def _exec_query(cls, query, filter, order_by, **kwargs):
        if kwargs.get("distinct"):
            query = query.distinct()
        if kwargs.has_key("outerjoin"):
            query = query.outerjoin(kwargs.get("outerjoin"), )
        elif kwargs.has_key("join"):
            query = query.join(kwargs.get("join"))
        if filter is not None:
            query = query.filter(filter)
        if order_by is not None:
            if type(order_by)==list: 
                query = query.order_by(*order_by)
            else:
                query = query.order_by(order_by)
        return query
        

    @classmethod
    def _select(cls, mapped_class, transaction=None, filter=None, order_by=None, **kwargs):
        db_session = SessionWrapper(transaction, True)
        query = db_session._query(mapped_class)
        return_val = cls._exec_query(query, filter, order_by, **kwargs)
        db_session.close()
        return return_val
            


    @classmethod
    def select(cls, mapped_class, transaction=None, filter=None, order_by=None, **kwargs):
        return cls._select(mapped_class, transaction, filter, order_by, **kwargs).all()

    #query = transaction._query(mapped_class).with_lockmode('read') ???

    @classmethod
    def select_one(cls, mapped_class, transaction=None, **kwargs):
        try:
            return cls._select(mapped_class, transaction, **kwargs).one()
        except sqlalchemy.exceptions.InvalidRequestError, e:
            raise InvalidRequestException(e)
        

    @classmethod
    def select_first(cls, mapped_class, transaction=None, filter=None, order_by=None, **kwargs):
        return cls._select(mapped_class, transaction, filter, order_by, **kwargs).first()
    
    @classmethod
    def select_count(cls, mapped_class, transaction=None, **kwargs):
        return cls._select(mapped_class, transaction, **kwargs).count()

    @classmethod
    def select_expression(cls, mapped_class, transaction=None, filter=None, order_by=None, **kwargs):
        return cls._select(mapped_class, transaction, filter, order_by, **kwargs)
    
    @classmethod
    def get(cls, mapped_class, id, transaction=None):
        if transaction:
            return transaction._query(mapped_class).get(id)
        else:
            with ReadonlySession() as transaction:
                return transaction._query(mapped_class).get(id)


def create_default_value_map(table):
    map = {}
    for col in table.c:
        name = col.name
        if col.default:
            value = col.default.arg
            map[name] = value
    return map

def get_connect_url(db_connect_info):
    # SEMIHACK - sqlalchemy 1.2 needs explicit driver specification:
    if db_connect_info.db_type == "mssql":
        if not (db_connect_info.query and ("DRIVER" in db_connect_info.query or "driver" in db_connect_info.query)):
            db_connect_info.query["driver"] = "SQL Server"
    return sqlalchemy_url.URL(db_connect_info.db_type, 
                              username=db_connect_info.username, 
                              password=db_connect_info.password, 
                              host=db_connect_info.host, 
                              port=int(db_connect_info.port) if db_connect_info.port else None, 
                              database=db_connect_info.database,
                              query=db_connect_info.query if db_connect_info.query else None)

def get_connect_url_from_string(db_connect_string):
    return sqlalchemy_url.make_url(db_connect_string)

def get_connect_url_info(db_connect_url):
    connect_str = "%s://" % (db_connect_url.drivername )
    if db_connect_url.username:
        connect_str += "%s:<password>" % (db_connect_url.username,)
    connect_str += "@%s" % (db_connect_url.host,)
    if db_connect_url.port:
        connect_str += ":%s" % (db_connect_url.port,)
    connect_str += "/%s" % (db_connect_url.database,)
    if db_connect_url.query:
        connect_str += "?%s=%s" % db_connect_url.query.popitem()
        for (key,value) in db_connect_url.query.items():
            connect_str += "&%s=%s" % (key, value)
    return connect_str


            
class DB_ConnectInfo(object):
    
    def __init__(self):
        self.db_type = self.username = self.password = self.host = self.port = self.database = self.query = self.encoding = None 
    
                
        


