"""
Server lib for database handling, available for Management and Gateway Server.
Environment creates one DatabaseFactory which modules/components uses to 
get DatabaseConnection with datatabase_api for local schema.
"""