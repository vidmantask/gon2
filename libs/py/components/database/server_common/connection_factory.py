from database_connection import DatabaseConnection

class ConnectionFactory(object):
    
    def __init__(self, db_connect_string = None, db_encoding= None):
        self.db_connect_string = db_connect_string
        self.db_encoding = db_encoding
        if self.db_connect_string is None:
            self.db_connect_string = 'sqlite:///:memory:'
    
    def get_default_connection(self, echo=False, logger=None, pool_size=5):
        return DatabaseConnection.get_engine(self.db_connect_string, echo, logger, self.db_encoding, pool_size) 

    def create_connection(self, connect_string, echo=False, encoding=None):
        if not encoding:
            encoding = self.db_encoding
        return DatabaseConnection.get_engine(connect_string, echo, encoding=encoding)



