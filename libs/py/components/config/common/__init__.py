"""
This module contains definition of configurations used around in the system.
"""
import sys
import os.path
import lib.appl.config
import lib.commongon
import components.database.server_common.database_api as database_api
import components.license.gonlicense
import lib.utility.xor_crypt

KS_SERVER_FILENAME = 'gon_server.ks'
KS_CLIENT_FILENAME = 'gon_client.ks'
CLIENT_SERVERS_FILENAME = 'gon_client.servers'
CLIENT_FIREWALL_WHITHELIST_FILENAME = 'gon_os_firewall_whitelist.inf'
CONNECT_INFO_QR_IMAGE_FILENAME = 'gon_connect_info.html'
CONNECT_INFO_DAT_FILENAME = 'gon_connect_info.dat'
SCRAMBLE_SALT_FILENAME = "gon_scramble_salt"

SERVICE_MANAGEMENT_KS_SERVER_FILENAME = 'gon_service_management_server.ks'
SERVICE_MANAGEMENT_KS_CLIENT_FILENAME = 'gon_service_management_client.ks'



class ConfigLogging(lib.appl.config.ConfigBase):
    """
    This Class represent common logging configuration
    """
    SECTION_LOG = 'log'
    OPTION_LOG_ENABLED = 'enabled'
    OPTION_LOG_VERBOSE = 'verbose'
    OPTION_LOG_TYPE = 'type'
    OPTION_LOG_TYPE_VALUE_text = 'text'
    OPTION_LOG_TYPE_VALUE_xml = 'xml'
    OPTION_LOG_FILE = 'file'
    OPTION_LOG_SESSION_ROTATE = 'rotate'
    OPTION_LOG_IN_FOLDER_ENABLED = 'in_folder_enabled'
    OPTION_LOG_IN_FOLDER_PATH = 'in_folder_path'

    def __init__(self, config_filename_prefix):
        lib.appl.config.ConfigBase.__init__(self, config_filename_prefix)
        self._set_defaults_loggging()

    def _set_defaults_loggging(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_TYPE, ConfigLogging.OPTION_LOG_TYPE_VALUE_text)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_FILE, '%s.log' % self.config_filename_prefix)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_SESSION_ROTATE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_IN_FOLDER_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_IN_FOLDER_PATH, 'cp_logs')

    log_enabled = lib.appl.config.def_property_boolean(SECTION_LOG, OPTION_LOG_ENABLED)
    log_verbose = lib.appl.config.def_property_int(SECTION_LOG, OPTION_LOG_VERBOSE)
    log_type = lib.appl.config.def_property(SECTION_LOG, OPTION_LOG_TYPE)
    log_file = lib.appl.config.def_property(SECTION_LOG, OPTION_LOG_FILE)
    log_absfile = lib.appl.config.def_property_abspath(SECTION_LOG, OPTION_LOG_FILE)
    log_rotate = lib.appl.config.def_property_boolean(SECTION_LOG, OPTION_LOG_SESSION_ROTATE)
    log_in_folder_enabled = lib.appl.config.def_property_boolean(SECTION_LOG, OPTION_LOG_IN_FOLDER_ENABLED)
    log_in_folder_path = lib.appl.config.def_property(SECTION_LOG, OPTION_LOG_IN_FOLDER_PATH)
    log_in_folder_abspath = lib.appl.config.def_property_abspath(SECTION_LOG, OPTION_LOG_IN_FOLDER_PATH)

    def is_log_type_text(self):
        return self.log_type == ConfigLogging.OPTION_LOG_TYPE_VALUE_text

    def is_log_type_xml(self):
        return self.log_type == ConfigLogging.OPTION_LOG_TYPE_VALUE_xml

class ConfigDatabase(lib.appl.config.ConfigBase):
    """
    This Class represent common database configuration
    """
    SECTION_DB = 'db'
    OPTION_DB_TYPE = 'type'
    OPTION_DB_HOST = 'host'
    OPTION_DB_PORT = 'port'
    OPTION_DB_DATABASE = 'database'
    OPTION_DB_USERNAME = 'username'
    OPTION_DB_PASSWORD = 'password'
    OPTION_DB_ENCODING = 'encoding'
    OPTION_DB_QUERY = 'query'
    OPTION_DB_LOG_ENABLED = 'log_enabled'
    OPTION_DB_CONNECT_STRING = 'connect_string'

    def __init__(self, config_filename_prefix=None):
        if config_filename_prefix:
            lib.appl.config.ConfigBase.__init__(self, config_filename_prefix)
        self._db_connect_url = None
        self._set_defaults_database()

    def _set_defaults_database(self):
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_ENCODING, 'utf8')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_TYPE, 'sqlite')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_HOST, '')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PORT, '')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_DATABASE, '../../config/gon_server_db.sqlite')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_USERNAME, '')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PASSWORD, '')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_QUERY, '')
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_LOG_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_CONNECT_STRING, '')

    def reset_to_default_when_restore_durring_upgrade(self):
        if self.db_type == 'sqlite':
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_ENCODING)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_HOST)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PORT)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_DATABASE)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_USERNAME)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PASSWORD)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_QUERY)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_LOG_ENABLED)
            self.reset_to_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_CONNECT_STRING)

    db_type = lib.appl.config.def_property(SECTION_DB, OPTION_DB_TYPE)
    db_host = lib.appl.config.def_property(SECTION_DB, OPTION_DB_HOST)
    db_port = lib.appl.config.def_property(SECTION_DB, OPTION_DB_PORT)
    db_database_absfilename = lib.appl.config.def_property_abspath(SECTION_DB, OPTION_DB_DATABASE)
    db_database = lib.appl.config.def_property(SECTION_DB, OPTION_DB_DATABASE)
    db_username = lib.appl.config.def_property(SECTION_DB, OPTION_DB_USERNAME)
    db_password = lib.appl.config.def_property_password(SECTION_DB, OPTION_DB_PASSWORD)
    db_log_enabled = lib.appl.config.def_property_boolean(SECTION_DB, OPTION_DB_LOG_ENABLED)
    db_encoding = lib.appl.config.def_property(SECTION_DB, OPTION_DB_ENCODING)
    db_query = lib.appl.config.def_property(SECTION_DB, OPTION_DB_QUERY)
    db_connect_string = lib.appl.config.def_property(SECTION_DB, OPTION_DB_CONNECT_STRING)

    def _get_attribute(self, name, kwargs):
        if kwargs.has_key(name):
            return kwargs.get(name)
        else:
            return getattr(self, name)

    def get_db_connect_url(self, **kwargs):
        db_connect_string = self._get_attribute("db_connect_string", kwargs)
        if db_connect_string:
            self._db_connect_url = database_api.get_connect_url_from_string(db_connect_string)
        else:
            db_connect_info = database_api.DB_ConnectInfo()
            db_connect_info.db_type = self._get_attribute("db_type", kwargs)
            db_connect_info.username = self._get_attribute("db_username", kwargs)
            db_connect_info.password = self._get_attribute("db_password", kwargs)
            db_connect_info.host = self._get_attribute("db_host", kwargs)
            db_connect_info.port = self._get_attribute("db_port", kwargs)
            if db_connect_info.db_type == 'sqlite':
                db_connect_info.database = self._get_attribute("db_database_absfilename", kwargs)
            else:
                db_connect_info.database = self._get_attribute("db_database", kwargs)
            db_connect_info.query = lib.commongon.string_to_dict(self._get_attribute("db_query", kwargs))
            self._db_connect_url = database_api.get_connect_url(db_connect_info)
        return self._db_connect_url

    def remove_password(self, connect_string):
        type_part, mark, rest = connect_string.partition("://")
        return "".join([type_part, mark, "<removed info>"])
#        user_name, colon, rest = rest.partition(":")
#        rest, q, extras = rest.rpartition("?")
#
#        password_and_server, slash, db_name = rest.rpartition("/")
#        password, at, server = password_and_server.rpartition("@")
#        if password:
#            password = "<password>"
#        parts = [type_part, mark, user_name, colon, password, at, server, slash, db_name, q, extras]
#        return "".join(parts)

    def get_db_connect_info(self):
        try:
            connect_url = self.get_db_connect_url()
            return database_api.get_connect_url_info(connect_url)
        except Exception, e:
            return "Error getting db connect info: %s" % e

    def set_to_default(self):
        self.db_type = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_TYPE)
        self.db_host = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_HOST)
        self.db_port = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PORT)
        self.db_database = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_DATABASE)
        self.db_username = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_USERNAME)
        self.db_password = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_PASSWORD)
        self.db_encoding = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_ENCODING)
        self.db_query = self.get_default(ConfigDatabase.SECTION_DB, ConfigDatabase.OPTION_DB_QUERY)




class ConfigServerManagement(ConfigLogging, ConfigDatabase):
    """
    This class represent the configuration for the Management Server
    """
    SECTION_TEMPLATES = 'templates'
    OPTION_TEMPLATES_PATH = 'path'

    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_DEPLOYMENT = 'deployment'
    OPTION_DEPLOYMENT_PATH = 'path'

    SECTION_MANAGEMENT_WS = 'management_ws'
    OPTION_MANAGEMENT_WS_IP = 'ip'
    OPTION_MANAGEMENT_WS_PORT = 'port'
    OPTION_MANAGEMENT_WS_WWW_HTML_PATH = 'www_html_path'
    OPTION_MANAGEMENT_WS_HTTPS_ENABLED = 'https_enabled'
    OPTION_MANAGEMENT_WS_HTTPS_CERTIFICATE_FILENAME = 'https_certificate_filename'
    OPTION_MANAGEMENT_WS_HTTPS_KEY_FILENAME = 'https_key_filename'

    SECTION_CPM = 'cpm'
    OPTION_CPM_GPMS_PATH = 'gpms_path'
    OPTION_CPM_GPMC_DEFS_PATH = 'gpmc_defs_path'

    SECTION_PORTSCAN = 'portscan'
    OPTION_PORTSCAN_ENABLED = 'enabled'
    OPTION_PORTSCAN_IP_RANGES = 'ip_ranges'
    OPTION_PORTSCAN_TIMEOUT = 'timeout'
    OPTION_PORTSCAN_PARALLEL_CONNECTIONS = 'parallel_connections'
    OPTION_PORTSCAN_DELAY_SEC = 'delay_sec'

    SECTION_LICENSE = 'license'
    OPTION_LICENSE_FILENAME = 'filename'

    SECTION_ACCESS_NOTIFICATION = 'access_notification'
    OPTION_ACCESS_NOTIFICATION_ENABLED = 'enabled'
    OPTION_ACCESS_NOTIFICATION_SMTP_SERVER = 'smtp_server'
    OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_USER = 'smtp_server_user'
    OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_PASSWORD = 'smtp_server_password'
    OPTION_ACCESS_NOTIFICATION_SENDER = 'sender'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    SECTION_ENROLLMENT = 'enrollment'
    OPTION_ACTIVATE_RULE = 'activate_assignment_rule'
    OPTION_DEVICE_CHECK_HARDWARE = 'hardware_check'

    SECTION_WS_SESSION = 'ws_session'
    OPTION_USER_LIMIT = 'users_returned_limit'
    OPTION_GROUP_LIMIT = 'groups_returned_limit'

    SECTION_SERVICE_MANAGEMENT = 'service_management'
    OPTION_SERVICE_MANAGEMENT_LISTEN_IP = 'listen_ip'
    OPTION_SERVICE_MANAGEMENT_LISTEN_PORT = 'listen_port'
    OPTION_SERVICE_MANAGEMENT_NUM_OF_THREADS = 'num_of_threads'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_TICKETS_MAX = 'filedist_tickets_max'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_CHECK_INTERVAL_SEC = 'filedist_check_interval_sec'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH = 'filedist_path'

    LAUNCH_COMMAND_WIN = 'gon_server_management_service.exe'
    LAUNCH_COMMAND_MAC = 'gon_server_management_service.exe'
    LAUNCH_COMMAND_LINUX = 'gon_server_management_service.exe'



    def __init__(self):
        ConfigLogging.__init__(self, 'gon_server_management')
        ConfigDatabase.__init__(self)
        self._set_defaults_server_management()

    def _set_defaults_server_management(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(1))
        self.set_default(ConfigServerManagement.SECTION_TEMPLATES, ConfigServerManagement.OPTION_TEMPLATES_PATH, '../../config/templates')
        self.set_default(ConfigServerManagement.SECTION_PLUGIN_MODULES, ConfigServerManagement.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigServerManagement.SECTION_DEPLOYMENT, ConfigServerManagement.OPTION_DEPLOYMENT_PATH, '../../config/deployed')
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_IP, '127.0.0.1')
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_PORT, lib.appl.config.ConfigBase.INT_VALUE(8072))
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_WWW_HTML_PATH, './www/html')
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_HTTPS_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_HTTPS_CERTIFICATE_FILENAME, '../../config/certificates/gon_https_management_server.crt')
        self.set_default(ConfigServerManagement.SECTION_MANAGEMENT_WS, ConfigServerManagement.OPTION_MANAGEMENT_WS_HTTPS_KEY_FILENAME, '../../config/certificates/gon_https_management_server.key.noencrypt.pem')
        self.set_default(ConfigServerManagement.SECTION_CPM, ConfigServerManagement.OPTION_CPM_GPMS_PATH, '../../config/gpm/gpms')
        self.set_default(ConfigServerManagement.SECTION_CPM, ConfigServerManagement.OPTION_CPM_GPMC_DEFS_PATH, '../../config/gpm/gpmcdefs')
        self.set_default(ConfigServerManagement.SECTION_PORTSCAN, ConfigServerManagement.OPTION_PORTSCAN_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerManagement.SECTION_PORTSCAN, ConfigServerManagement.OPTION_PORTSCAN_IP_RANGES, '192.168.45.0-192.168.45.255, 172.16.0.0-172.16.0.255')
        self.set_default(ConfigServerManagement.SECTION_PORTSCAN, ConfigServerManagement.OPTION_PORTSCAN_TIMEOUT, lib.appl.config.ConfigBase.FLOAT_VALUE(0.1))
        self.set_default(ConfigServerManagement.SECTION_PORTSCAN, ConfigServerManagement.OPTION_PORTSCAN_PARALLEL_CONNECTIONS, lib.appl.config.ConfigBase.INT_VALUE(256))
        self.set_default(ConfigServerManagement.SECTION_PORTSCAN, ConfigServerManagement.OPTION_PORTSCAN_DELAY_SEC, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigServerManagement.SECTION_LICENSE, ConfigServerManagement.OPTION_LICENSE_FILENAME, '../../config/deployed/gon_license.lic')
        self.set_default(ConfigServerManagement.SECTION_ACCESS_NOTIFICATION, ConfigServerManagement.OPTION_ACCESS_NOTIFICATION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerManagement.SECTION_ACCESS_NOTIFICATION, ConfigServerManagement.OPTION_ACCESS_NOTIFICATION_SMTP_SERVER, 'smtp.example.com')
        self.set_default(ConfigServerManagement.SECTION_ACCESS_NOTIFICATION, ConfigServerManagement.OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_USER, '')
        self.set_default(ConfigServerManagement.SECTION_ACCESS_NOTIFICATION, ConfigServerManagement.OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_PASSWORD, '')
        self.set_default(ConfigServerManagement.SECTION_ACCESS_NOTIFICATION, ConfigServerManagement.OPTION_ACCESS_NOTIFICATION_SENDER, '')
        self.set_default(ConfigServerManagement.SECTION_ENROLLMENT, ConfigServerManagement.OPTION_ACTIVATE_RULE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerManagement.SECTION_ENROLLMENT, ConfigServerManagement.OPTION_DEVICE_CHECK_HARDWARE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerManagement.SECTION_DICTIONARY, ConfigServerManagement.OPTION_DICTIONARY_PATH, '../../config/dictionary')
        self.set_default(ConfigServerManagement.SECTION_WS_SESSION, ConfigServerManagement.OPTION_GROUP_LIMIT, lib.appl.config.ConfigBase.INT_VALUE(500))
        self.set_default(ConfigServerManagement.SECTION_WS_SESSION, ConfigServerManagement.OPTION_USER_LIMIT, lib.appl.config.ConfigBase.INT_VALUE(500))
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_LISTEN_IP, '0.0.0.0')
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_LISTEN_PORT, lib.appl.config.ConfigBase.INT_VALUE(8074))
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_NUM_OF_THREADS, lib.appl.config.ConfigBase.INT_VALUE(5))
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_FILEDIST_TICKETS_MAX, lib.appl.config.ConfigBase.INT_VALUE(2))
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_FILEDIST_CHECK_INTERVAL_SEC, lib.appl.config.ConfigBase.INT_VALUE(20))
        self.set_default(ConfigServerManagement.SECTION_SERVICE_MANAGEMENT, ConfigServerManagement.OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH, "../../config")

    def get_launch_command(self):
        if sys.platform == 'darwin':
            return ConfigServerManagement.LAUNCH_COMMAND_WIN
        elif sys.platform == 'win32':
            return ConfigServerManagement.LAUNCH_COMMAND_WIN
        elif sys.platform == 'linux2':
            return ConfigServerManagement.LAUNCH_COMMAND_WIN
        elif sys.platform == 'linux2':
            return ConfigServerManagement.LAUNCH_COMMAND_WIN
        return None

    def get_license_handler(self):
        return components.license.gonlicense.GonLicenseHandler(self.license_absfilename)

    def load_and_set_scramble_salt(self):
        scramble_salt_filename = os.path.join(self.deployment_abspath, SCRAMBLE_SALT_FILENAME)
        lib.utility.xor_crypt.load_and_set_scramble_salt(scramble_salt_filename)

    def reset_to_default_when_restore_durring_upgrade(self):
        self.reset_to_default(ConfigServerManagement.SECTION_TEMPLATES, ConfigServerManagement.OPTION_TEMPLATES_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_PLUGIN_MODULES, ConfigServerManagement.OPTION_PLUGIN_MODULES_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_DEPLOYMENT, ConfigServerManagement.OPTION_DEPLOYMENT_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_CPM, ConfigServerManagement.OPTION_CPM_GPMS_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_CPM, ConfigServerManagement.OPTION_CPM_GPMC_DEFS_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_DICTIONARY, ConfigServerManagement.OPTION_DICTIONARY_PATH)
        self.reset_to_default(ConfigServerManagement.SECTION_LICENSE, ConfigServerManagement.OPTION_LICENSE_FILENAME)
        ConfigDatabase.reset_to_default_when_restore_durring_upgrade(self)

    templates_abspath = lib.appl.config.def_property_abspath(SECTION_TEMPLATES, OPTION_TEMPLATES_PATH)
    plugin_modules_abspath = lib.appl.config.def_property_abspath(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    deployment_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_PATH)
    management_ws_ip = lib.appl.config.def_property(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_IP)
    management_ws_port = lib.appl.config.def_property_int(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_PORT)
    management_ws_www_html_path = lib.appl.config.def_property(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_WWW_HTML_PATH)
    management_ws_https_enabled = lib.appl.config.def_property_boolean(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_HTTPS_ENABLED)
    management_ws_https_certificate_filename = lib.appl.config.def_property(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_HTTPS_CERTIFICATE_FILENAME)
    management_ws_https_certificate_filename_abspath = lib.appl.config.def_property_abspath(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_HTTPS_CERTIFICATE_FILENAME)
    management_ws_https_key_filename = lib.appl.config.def_property(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_HTTPS_KEY_FILENAME)
    management_ws_https_key_filename_abspath = lib.appl.config.def_property_abspath(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_HTTPS_KEY_FILENAME)

    cpm_gpms_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPMS_PATH)
    cpm_gpmc_defs_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPMC_DEFS_PATH)
    portscan_enabled = lib.appl.config.def_property_boolean(SECTION_PORTSCAN, OPTION_PORTSCAN_ENABLED)
    portscan_ip_ranges = lib.appl.config.def_property(SECTION_PORTSCAN, OPTION_PORTSCAN_IP_RANGES)
    portscan_timeout = lib.appl.config.def_property(SECTION_PORTSCAN, OPTION_PORTSCAN_TIMEOUT)
    portscan_parallel_connections = lib.appl.config.def_property(SECTION_PORTSCAN, OPTION_PORTSCAN_PARALLEL_CONNECTIONS)
    portscan_delay_sec = lib.appl.config.def_property_int(SECTION_PORTSCAN, OPTION_PORTSCAN_DELAY_SEC)
    license_absfilename = lib.appl.config.def_property_abspath(SECTION_LICENSE, OPTION_LICENSE_FILENAME)

    access_notification_enabled = lib.appl.config.def_property_boolean(SECTION_ACCESS_NOTIFICATION, OPTION_ACCESS_NOTIFICATION_ENABLED)
    access_notification_smtp_server = lib.appl.config.def_property(SECTION_ACCESS_NOTIFICATION, OPTION_ACCESS_NOTIFICATION_SMTP_SERVER)
    access_notification_smtp_server_user = lib.appl.config.def_property(SECTION_ACCESS_NOTIFICATION, OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_USER)
    access_notification_smtp_server_password = lib.appl.config.def_property(SECTION_ACCESS_NOTIFICATION, OPTION_ACCESS_NOTIFICATION_SMTP_SERVER_PASSWORD)
    access_notification_sender = lib.appl.config.def_property(SECTION_ACCESS_NOTIFICATION, OPTION_ACCESS_NOTIFICATION_SENDER)

    enrollment_activate_assignment_rule = lib.appl.config.def_property_boolean(SECTION_ENROLLMENT, OPTION_ACTIVATE_RULE)
    enrollment_device_hardware_check = lib.appl.config.def_property_boolean(SECTION_ENROLLMENT, OPTION_DEVICE_CHECK_HARDWARE)

    dictionary_abspath = lib.appl.config.def_property_abspath(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)

    ws_group_limit = lib.appl.config.def_property_int(SECTION_WS_SESSION, OPTION_GROUP_LIMIT)
    ws_user_limit = lib.appl.config.def_property_int(SECTION_WS_SESSION, OPTION_USER_LIMIT)

    service_management_listen_ip = lib.appl.config.def_property(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_LISTEN_IP)
    service_management_listen_port = lib.appl.config.def_property_int(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_LISTEN_PORT)
    service_management_num_of_threads = lib.appl.config.def_property_int(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_NUM_OF_THREADS)
    service_management_filedist_tickets_max = lib.appl.config.def_property_int(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_TICKETS_MAX)
    service_management_filedist_check_interval_sec = lib.appl.config.def_property_int(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_CHECK_INTERVAL_SEC)
    service_management_filedist_abspath = lib.appl.config.def_property_abspath(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH)


class ConfigServerGateway(ConfigLogging, ConfigDatabase):
    """
    This class represent the configuration for the Gateway Server
    """
    OPTION_LOG_SESSION_ENABLED = 'session_enabled'
    OPTION_LOG_SESSION_ENABLED_BY_REMOTE = 'session_enabled_by_remote'
    OPTION_LOG_SESSION_PATH = 'session_path'
    OPTION_LOG_SETTINGS_TRAFFIC = 'enabled_traffic_logs'
    OPTION_LOG_CLIENT_PATH = 'client_path'

    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_DEPLOYMENT = 'deployment'
    OPTION_DEPLOYMENT_PATH = 'path'

    SECTION_SERVICE = 'service'
    OPTION_SERVICE_IP = 'ip'
    OPTION_SERVICE_PORT = 'port'
    OPTION_SERVICE_CLIENT_CONNECT_ADDRESSES = 'client_connect_addresses'
    OPTION_SERVICE_CLIENT_CONNECT_PORTS = 'client_connect_ports'
    OPTION_SERVICE_NUM_OF_THREADS = 'num_of_threads'

    SECTION_SERVICE_HTTP = 'service_http'
    OPTION_SERVICE_HTTP_ENABLED = 'enabled'
    OPTION_SERVICE_HTTP_IP = 'ip'
    OPTION_SERVICE_HTTP_PORT = 'port'
    OPTION_SERVICE_HTTP_LOG_2_ENABLED = 'log_2_enabled'
    OPTION_SERVICE_HTTP_LOG_3_ENABLED = 'log_3_enabled'
    OPTION_SERVICE_HTTP_CLIENT_CONNECT_PORTS = 'client_connect_ports'

    SECTION_SESSION = 'session'
    OPTION_SESSION_TIMEOUT_MIN = 'timeout_min'
    OPTION_SESSION_KEYEXCHANGE_TIMEOUT_SEC = 'keyexchange_timeout_sec'
    OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC = 'keep_alive_ping_interval_sec'
    OPTION_SESSION_OFFLINE_CREDENTIAL_TIMEOUT_MIN = 'offline_credential_timeout_min'
    OPTION_SESSION_CLOSE_WHEN_ENTERING_BACKGROUND = 'close_when_entering_background'

    SECTION_AUTHORIZATION = 'authorization'
    OPTION_AUTHORIZATION_ALWAYS_ALLOW_ACCESS = 'always_allow_access'
    OPTION_AUTHORIZATION_TIMEOUT_SEC = 'timeout_sec'
    OPTION_AUTHORIZATION_ACCESS_RULE = 'access_rule'
    OPTION_AUTHORIZATION_TRI_STATE_LOGIC = 'use_tri_state_logic'
    OPTION_AUTHORIZATION_REQUIRE_FULL_LOGIN = 'require_full_login'

    SECTION_LOGIN = 'login'
    OPTION_LOGIN_SHOW_PASSWORD_PROMPT = 'show_password_prompt'
    OPTION_LOGIN_ENABLE_SSO = 'enable_sso'
    OPTION_LOGIN_LOCAL_GATEWAY_SSO = 'local_gateway_sso'
    OPTION_LOGIN_SECURITY_PACKAGE = 'security_package'
    OPTION_LOGIN_TARGET_SPN = 'target_spn'
    OPTION_LOGIN_SHOW_LAST_USER_DIRECTORY = 'show_last_user_directory'
    OPTION_LOGIN_SHOW_LAST_LOGIN = 'show_last_login'

    SECTION_CPM = 'cpm'
    OPTION_CPM_GPMS_PATH = 'gpms_path'
    OPTION_CONCURENT_DOWNLOADS = 'concurent_downloads'

    SECTION_DIALOG = 'dialog'
    OPTION_DIALOG_WELCOME_MESSAGE_ENABLED = 'welcome_message_enabled'
    OPTION_DIALOG_WELCOME_MESSAGE_FILENAME = 'welcome_message_filename'
    OPTION_DIALOG_WELCOME_MESSAGE_CLOSE_ON_CANCEL = 'welcome_message_close_on_cancel'
    OPTION_CREATE_CITRIX_MENU = "create_citrix_menu"

    SECTION_LICENSE = 'license'
    OPTION_LICENSE_FILENAME = 'filename'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    SECTION_IMAGE = 'images'
    OPTION_IMAGE_PATH = 'path'

    SECTION_SERVICE_MANAGEMENT = 'service_management'
    OPTION_SERVICE_MANAGEMENT_CONNECT_IP = 'connect_ip'
    OPTION_SERVICE_MANAGEMENT_CONNECT_PORT = 'connect_port'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED = 'filedist_enabled'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH = 'filedist_path'
    OPTION_SERVICE_MANAGEMENT_MESSAGE_QUEUE_OFFLINE_FILENAME = 'message_queue_offline_filename'

    SECTION_SECURITY = 'security'
    OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_HIGH = 'dos_attack_keyexchange_phase_one_high'
    OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_LOW = 'dos_attack_keyexchange_phase_one_low'
    OPTION_SECURITY_SECURITY_CLOSED_HIGH = 'dos_attack_security_closed_high'
    OPTION_SECURITY_SECURITY_CLOSED_LOW = 'dos_attack_security_closed_low'
    OPTION_SECURITY_BAN_ATTACKER_IPS = 'dos_attack_ban_attacker_ips'

    SECTION_WEB_SERVER = 'web_server'
    OPTION_WEB_SERVER_ENABLED = 'enabled'
    OPTION_WEB_SERVER_HOST = 'host'
    OPTION_WEB_SERVER_PORT = 'port'
    OPTION_WEB_SERVER_WWW_ROOT = 'www_root'

    SECTION_SSL_AS_CLIENT = 'ssl_as_client'
    OPTION_SSL_AS_CLIENT_CERTIFICATE_PATH = 'certificate_path'
    OPTION_SSL_AS_CLIENT_CERTIFICATE_VERIFICATION_ENABLED = 'certificate_verification_enabled'
    OPTION_SSL_AS_CLIENT_HOSTNAME_VERIFICATION_ENABLED = 'hostname_verification_enabled'
    OPTION_SSL_AS_CLIENT_SNI_ENABLED = 'sni_enabled'
    OPTION_SSL_AS_CLIENT_METHOD = 'method'
    OPTION_SSL_AS_CLIENT_OP_NO_SSLv2 = 'option_no_sslv2'
    OPTION_SSL_AS_CLIENT_OP_NO_SSLv3 = 'option_no_sslv3'
    OPTION_SSL_AS_CLIENT_OP_NO_TLSV1 = 'option_no_tlsv1'
    OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_1 = 'option_no_tlsv1_1'
    OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_2 = 'option_no_tlsv1_2'

    LAUNCH_COMMAND_WIN = 'gon_server_gateway_service.exe'
    LAUNCH_COMMAND_MAC = 'gon_server_gateway_service.exe'
    LAUNCH_COMMAND_LINUX = 'gon_server_gateway_service.exe'

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_server_gateway')
        ConfigDatabase.__init__(self)
        self._set_defaults_server_gateway()

    def _set_defaults_server_gateway(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(1))
        self.set_default(ConfigLogging.SECTION_LOG, ConfigServerGateway.OPTION_LOG_SESSION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigServerGateway.OPTION_LOG_SESSION_ENABLED_BY_REMOTE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigServerGateway.OPTION_LOG_SESSION_PATH, './session_logs')
        self.set_default(ConfigLogging.SECTION_LOG, ConfigServerGateway.OPTION_LOG_SETTINGS_TRAFFIC, '')
        self.set_default(ConfigLogging.SECTION_LOG, ConfigServerGateway.OPTION_LOG_CLIENT_PATH, './client_logs')

        self.set_default(ConfigServerGateway.SECTION_PLUGIN_MODULES, ConfigServerGateway.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigServerGateway.SECTION_DEPLOYMENT, ConfigServerGateway.OPTION_DEPLOYMENT_PATH, '../../config/deployed')
        self.set_default(ConfigServerGateway.SECTION_SERVICE, ConfigServerGateway.OPTION_SERVICE_IP, '0.0.0.0')
        self.set_default(ConfigServerGateway.SECTION_SERVICE, ConfigServerGateway.OPTION_SERVICE_PORT, lib.appl.config.ConfigBase.INT_VALUE(443))
        self.set_default(ConfigServerGateway.SECTION_SERVICE, ConfigServerGateway.OPTION_SERVICE_CLIENT_CONNECT_ADDRESSES, '127.0.0.1')
        self.set_default(ConfigServerGateway.SECTION_SERVICE, ConfigServerGateway.OPTION_SERVICE_CLIENT_CONNECT_PORTS, '443')
        self.set_default(ConfigServerGateway.SECTION_SERVICE, ConfigServerGateway.OPTION_SERVICE_NUM_OF_THREADS, lib.appl.config.ConfigBase.INT_VALUE(5))
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_IP, '0.0.0.0')
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_PORT, lib.appl.config.ConfigBase.INT_VALUE(80))
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_CLIENT_CONNECT_PORTS, '80')
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_LOG_2_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SERVICE_HTTP, ConfigServerGateway.OPTION_SERVICE_HTTP_LOG_3_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SESSION, ConfigServerGateway.OPTION_SESSION_TIMEOUT_MIN, lib.appl.config.ConfigBase.INT_VALUE(120))
        self.set_default(ConfigServerGateway.SECTION_SESSION, ConfigServerGateway.OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC, lib.appl.config.ConfigBase.INT_VALUE(29))
        self.set_default(ConfigServerGateway.SECTION_SESSION, ConfigServerGateway.OPTION_SESSION_KEYEXCHANGE_TIMEOUT_SEC, lib.appl.config.ConfigBase.INT_VALUE(60))
        self.set_default(ConfigServerGateway.SECTION_SESSION, ConfigServerGateway.OPTION_SESSION_OFFLINE_CREDENTIAL_TIMEOUT_MIN, lib.appl.config.ConfigBase.INT_VALUE(60))
        self.set_default(ConfigServerGateway.SECTION_SESSION, ConfigServerGateway.OPTION_SESSION_CLOSE_WHEN_ENTERING_BACKGROUND, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_AUTHORIZATION, ConfigServerGateway.OPTION_AUTHORIZATION_ALWAYS_ALLOW_ACCESS, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_AUTHORIZATION, ConfigServerGateway.OPTION_AUTHORIZATION_TIMEOUT_SEC, lib.appl.config.ConfigBase.INT_VALUE(150))
        self.set_default(ConfigServerGateway.SECTION_AUTHORIZATION, ConfigServerGateway.OPTION_AUTHORIZATION_ACCESS_RULE, '')
        self.set_default(ConfigServerGateway.SECTION_AUTHORIZATION, ConfigServerGateway.OPTION_AUTHORIZATION_TRI_STATE_LOGIC, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_AUTHORIZATION, ConfigServerGateway.OPTION_AUTHORIZATION_REQUIRE_FULL_LOGIN, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_CPM, ConfigServerGateway.OPTION_CPM_GPMS_PATH, '../../config/gpm/gpms')
        self.set_default(ConfigServerGateway.SECTION_CPM, ConfigServerGateway.OPTION_CONCURENT_DOWNLOADS, lib.appl.config.ConfigBase.INT_VALUE(2))
        self.set_default(ConfigServerGateway.SECTION_DIALOG, ConfigServerGateway.OPTION_DIALOG_WELCOME_MESSAGE_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_DIALOG, ConfigServerGateway.OPTION_DIALOG_WELCOME_MESSAGE_FILENAME, '../../config/gon_welcome_message.txt')
        self.set_default(ConfigServerGateway.SECTION_DIALOG, ConfigServerGateway.OPTION_DIALOG_WELCOME_MESSAGE_CLOSE_ON_CANCEL, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_DIALOG, ConfigServerGateway.OPTION_CREATE_CITRIX_MENU, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_LICENSE, ConfigServerGateway.OPTION_LICENSE_FILENAME, '../../config/deployed/gon_license.lic')
        self.set_default(ConfigServerGateway.SECTION_DICTIONARY, ConfigServerGateway.OPTION_DICTIONARY_PATH, '../../config/dictionary')
        self.set_default(ConfigServerGateway.SECTION_IMAGE, ConfigServerGateway.OPTION_IMAGE_PATH, '../../config/images')

        self.set_default(ConfigServerGateway.SECTION_SERVICE_MANAGEMENT, ConfigServerGateway.OPTION_SERVICE_MANAGEMENT_CONNECT_IP, '127.0.0.1')
        self.set_default(ConfigServerGateway.SECTION_SERVICE_MANAGEMENT, ConfigServerGateway.OPTION_SERVICE_MANAGEMENT_CONNECT_PORT, lib.appl.config.ConfigBase.INT_VALUE(8074))
        self.set_default(ConfigServerGateway.SECTION_SERVICE_MANAGEMENT, ConfigServerGateway.OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SERVICE_MANAGEMENT, ConfigServerGateway.OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH, '../../config')
        self.set_default(ConfigServerGateway.SECTION_SERVICE_MANAGEMENT, ConfigServerGateway.OPTION_SERVICE_MANAGEMENT_MESSAGE_QUEUE_OFFLINE_FILENAME, './message_queue_offline.dat')

        self.set_default(ConfigServerGateway.SECTION_SECURITY, ConfigServerGateway.OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_HIGH, lib.appl.config.ConfigBase.INT_VALUE(20))
        self.set_default(ConfigServerGateway.SECTION_SECURITY, ConfigServerGateway.OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_LOW, lib.appl.config.ConfigBase.INT_VALUE(5))
        self.set_default(ConfigServerGateway.SECTION_SECURITY, ConfigServerGateway.OPTION_SECURITY_SECURITY_CLOSED_HIGH, lib.appl.config.ConfigBase.INT_VALUE(4))
        self.set_default(ConfigServerGateway.SECTION_SECURITY, ConfigServerGateway.OPTION_SECURITY_SECURITY_CLOSED_LOW, lib.appl.config.ConfigBase.INT_VALUE(2))
        self.set_default(ConfigServerGateway.SECTION_SECURITY, ConfigServerGateway.OPTION_SECURITY_BAN_ATTACKER_IPS, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)

        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_SECURITY_PACKAGE, 'NTLM')
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_SHOW_PASSWORD_PROMPT, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_TARGET_SPN, '')
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_ENABLE_SSO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_LOCAL_GATEWAY_SSO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_SHOW_LAST_USER_DIRECTORY, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_LOGIN, ConfigServerGateway.OPTION_LOGIN_SHOW_LAST_LOGIN, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)

        self.set_default(ConfigServerGateway.SECTION_WEB_SERVER, ConfigServerGateway.OPTION_WEB_SERVER_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_WEB_SERVER, ConfigServerGateway.OPTION_WEB_SERVER_HOST, '127.0.0.1')
        self.set_default(ConfigServerGateway.SECTION_WEB_SERVER, ConfigServerGateway.OPTION_WEB_SERVER_PORT, lib.appl.config.ConfigBase.INT_VALUE(8075))
        self.set_default(ConfigServerGateway.SECTION_WEB_SERVER, ConfigServerGateway.OPTION_WEB_SERVER_WWW_ROOT, '../../config/www_root')

        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_CERTIFICATE_PATH, '../../config/certificates/ssl_as_client')
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_CERTIFICATE_VERIFICATION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_HOSTNAME_VERIFICATION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_SNI_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_METHOD, 'sslv23')
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_OP_NO_SSLv2, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_OP_NO_SSLv3, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_OP_NO_TLSV1, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_1, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigServerGateway.SECTION_SSL_AS_CLIENT, ConfigServerGateway.OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_2, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)

    def get_launch_command(self):
        if sys.platform == 'darwin':
            return ConfigServerGateway.LAUNCH_COMMAND_WIN
        elif sys.platform == 'win32':
            return ConfigServerGateway.LAUNCH_COMMAND_WIN
        elif sys.platform == 'linux2':
            return ConfigServerGateway.LAUNCH_COMMAND_WIN
        return None

    def get_license_handler(self):
        return components.license.gonlicense.GonLicenseHandler(self.license_absfilename)

    def load_and_set_scramble_salt(self):
        scramble_salt_filename = os.path.join(self.deployment_abspath, SCRAMBLE_SALT_FILENAME)
        lib.utility.xor_crypt.load_and_set_scramble_salt(scramble_salt_filename)

    def reset_to_default_when_restore_durring_upgrade(self):
        self.reset_to_default(ConfigServerGateway.SECTION_PLUGIN_MODULES, ConfigServerGateway.OPTION_PLUGIN_MODULES_PATH)
        self.reset_to_default(ConfigServerGateway.SECTION_DEPLOYMENT, ConfigServerGateway.OPTION_DEPLOYMENT_PATH)
        self.reset_to_default(ConfigServerGateway.SECTION_CPM, ConfigServerGateway.OPTION_CPM_GPMS_PATH)
        self.reset_to_default(ConfigServerGateway.SECTION_DICTIONARY, ConfigServerGateway.OPTION_DICTIONARY_PATH)
        self.reset_to_default(ConfigServerGateway.SECTION_IMAGE, ConfigServerGateway.OPTION_IMAGE_PATH)
        self.reset_to_default(ConfigServerGateway.SECTION_LICENSE, ConfigServerGateway.OPTION_LICENSE_FILENAME)
        ConfigDatabase.reset_to_default_when_restore_durring_upgrade(self)

    log_session_enabled = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED)
    log_session_enabled_by_remote = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED_BY_REMOTE)
    log_session_abspath = lib.appl.config.def_property_abspath(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_PATH)
    log_settings_traffic = lib.appl.config.def_property_list(ConfigLogging.SECTION_LOG, OPTION_LOG_SETTINGS_TRAFFIC)
    log_client_path = lib.appl.config.def_property_abspath(ConfigLogging.SECTION_LOG, OPTION_LOG_CLIENT_PATH)

    plugin_modules_abspath = lib.appl.config.def_property_abspath(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    deployment_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_PATH)
    service_ip = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_IP)
    service_port = lib.appl.config.def_property_int(SECTION_SERVICE, OPTION_SERVICE_PORT)
    service_client_connect_addresses = lib.appl.config.def_property_list(SECTION_SERVICE, OPTION_SERVICE_CLIENT_CONNECT_ADDRESSES)
    service_client_connect_ports = lib.appl.config.def_property_intlist(SECTION_SERVICE, OPTION_SERVICE_CLIENT_CONNECT_PORTS)
    service_num_of_threads = lib.appl.config.def_property_int(SECTION_SERVICE, OPTION_SERVICE_NUM_OF_THREADS)
    service_http_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_ENABLED)
    service_http_ip = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_IP)
    service_http_port = lib.appl.config.def_property_int(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PORT)
    service_http_client_connect_ports = lib.appl.config.def_property_intlist(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_CLIENT_CONNECT_PORTS)
    service_http_log_2_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_2_ENABLED)
    service_http_log_3_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_3_ENABLED)
    session_timeout_min = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_TIMEOUT_MIN)
    session_keep_alive_ping_interval_sec = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC)
    session_keyexchange_timeout_sec = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_KEYEXCHANGE_TIMEOUT_SEC)
    session_offline_credential_timeout_min = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_OFFLINE_CREDENTIAL_TIMEOUT_MIN)
    session_close_when_entering_background = lib.appl.config.def_property_boolean(SECTION_SESSION, OPTION_SESSION_CLOSE_WHEN_ENTERING_BACKGROUND)

    authorization_always_allow_access = lib.appl.config.def_property_boolean(SECTION_AUTHORIZATION, OPTION_AUTHORIZATION_ALWAYS_ALLOW_ACCESS)
    authorization_timeout_sec = lib.appl.config.def_property_int(SECTION_AUTHORIZATION, OPTION_AUTHORIZATION_TIMEOUT_SEC)
    authorization_access_rule = lib.appl.config.def_property(SECTION_AUTHORIZATION, OPTION_AUTHORIZATION_ACCESS_RULE)
    authorization_use_tri_state_logic = lib.appl.config.def_property_boolean(SECTION_AUTHORIZATION, OPTION_AUTHORIZATION_TRI_STATE_LOGIC)
    authorization_require_full_login = lib.appl.config.def_property_boolean(SECTION_AUTHORIZATION, OPTION_AUTHORIZATION_REQUIRE_FULL_LOGIN)
    cpm_gpms_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPMS_PATH)
    cpm_concurent_downloads = lib.appl.config.def_property_int(SECTION_CPM, OPTION_CONCURENT_DOWNLOADS)
    dialog_welcome_message_enabled = lib.appl.config.def_property_boolean(SECTION_DIALOG, OPTION_DIALOG_WELCOME_MESSAGE_ENABLED)
    dialog_welcome_message_filename = lib.appl.config.def_property_abspath(SECTION_DIALOG, OPTION_DIALOG_WELCOME_MESSAGE_FILENAME)
    dialog_welcome_message_close_on_cancel = lib.appl.config.def_property_boolean(SECTION_DIALOG, OPTION_DIALOG_WELCOME_MESSAGE_CLOSE_ON_CANCEL)
    dialog_create_citrix_menu = lib.appl.config.def_property_boolean(SECTION_DIALOG, OPTION_CREATE_CITRIX_MENU)
    license_absfilename = lib.appl.config.def_property_abspath(SECTION_LICENSE, OPTION_LICENSE_FILENAME)
    dictionary_abspath = lib.appl.config.def_property_abspath(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)
    image_abspath = lib.appl.config.def_property_abspath(SECTION_IMAGE, OPTION_IMAGE_PATH)

    service_management_connect_ip = lib.appl.config.def_property(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_CONNECT_IP)
    service_management_connect_port = lib.appl.config.def_property_int(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_CONNECT_PORT)
    service_management_filedist_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED)
    service_management_filedist_abspath = lib.appl.config.def_property_abspath(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH)
    service_management_message_queue_offline_absfilename = lib.appl.config.def_property_abspath(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_MESSAGE_QUEUE_OFFLINE_FILENAME)

    security_dos_attack_keyexchange_phase_one_high = lib.appl.config.def_property_int(SECTION_SECURITY, OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_HIGH)
    security_dos_attack_keyexchange_phase_one_low = lib.appl.config.def_property_int(SECTION_SECURITY, OPTION_SECURITY_DOS_ATTACK_KEYEXCHANGE_PHASE_ONE_LOW)
    security_dos_attack_security_closed_high = lib.appl.config.def_property_int(SECTION_SECURITY, OPTION_SECURITY_SECURITY_CLOSED_HIGH)
    security_dos_attack_security_closed_low = lib.appl.config.def_property_int(SECTION_SECURITY, OPTION_SECURITY_SECURITY_CLOSED_LOW)
    security_dos_attack_ban_attacker_ips = lib.appl.config.def_property_boolean(SECTION_SECURITY, OPTION_SECURITY_BAN_ATTACKER_IPS)

    login_target_spn = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_TARGET_SPN)
    login_security_package = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_SECURITY_PACKAGE)
    login_enable_sso = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_ENABLE_SSO)
    login_local_gateway_sso = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_LOCAL_GATEWAY_SSO)
    login_show_password_prompt = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_SHOW_PASSWORD_PROMPT)
    login_show_last_user_directory = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_SHOW_LAST_USER_DIRECTORY)
    login_show_last_login = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_SHOW_LAST_LOGIN)

    web_server_enable = lib.appl.config.def_property_boolean(SECTION_WEB_SERVER, OPTION_WEB_SERVER_ENABLED)
    web_server_host = lib.appl.config.def_property(SECTION_WEB_SERVER, OPTION_WEB_SERVER_HOST)
    web_server_port = lib.appl.config.def_property_int(SECTION_WEB_SERVER, OPTION_WEB_SERVER_PORT)
    web_server_www_absroot = lib.appl.config.def_property_abspath(SECTION_WEB_SERVER, OPTION_WEB_SERVER_WWW_ROOT)

    ssl_as_client_certificate_path = lib.appl.config.def_property_abspath(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_CERTIFICATE_PATH)
    ssl_as_client_certificate_verification_enabled = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_CERTIFICATE_VERIFICATION_ENABLED)
    ssl_as_client_hostname_verification_enabled = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_HOSTNAME_VERIFICATION_ENABLED)
    ssl_as_client_sni_enabled = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_SNI_ENABLED)
    ssl_as_client_method = lib.appl.config.def_property(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_METHOD)
    ssl_as_client_op_no_sslv2 = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_OP_NO_SSLv2)
    ssl_as_client_op_no_sslv3 = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_OP_NO_SSLv3)
    ssl_as_client_op_no_tlsv1 = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_OP_NO_TLSV1)
    ssl_as_client_op_no_tlsv1_1 = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_1)
    ssl_as_client_op_no_tlsv1_2 = lib.appl.config.def_property_boolean(SECTION_SSL_AS_CLIENT, OPTION_SSL_AS_CLIENT_OP_NO_TLSV1_2)


class ConfigServerGatewayLocal(lib.appl.config.ConfigBase):
    """
    This class represent the configuration for the Gateway Server local part
    """
    SECTION_SERVICE = 'service'
    OPTION_SERVICE_SID = 'sid'
    OPTION_SERVICE_TITLE = 'title'
    OPTION_SERVICE_IP = 'ip'
    OPTION_SERVICE_PORT = 'port'

    SECTION_SERVICE_MANAGEMENT = 'service_management'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED = 'filedist_enabled'

    def __init__(self):
        lib.appl.config.ConfigBase.__init__(self, 'gon_server_gateway_local')
        self._set_defaults_server_gateway_local()

    def _set_defaults_server_gateway_local(self):
        self.set_default(ConfigServerGatewayLocal.SECTION_SERVICE, ConfigServerGatewayLocal.OPTION_SERVICE_SID, "")
        self.set_default(ConfigServerGatewayLocal.SECTION_SERVICE, ConfigServerGatewayLocal.OPTION_SERVICE_TITLE, "Not defined")
        # no default value for OPTION_SERVICE_IP
        # no default value for OPTION_SERVICE_PORT
        # no default value for OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED

    service_sid = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_SID)
    service_title = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_TITLE)
    service_ip = lib.appl.config.def_property_with_none(SECTION_SERVICE, OPTION_SERVICE_IP)
    service_port = lib.appl.config.def_property_int_with_none(SECTION_SERVICE, OPTION_SERVICE_PORT)
    service_management_filedist_enabled = lib.appl.config.def_property_boolean_with_none(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_ENABLED)



class ConfigConfigService(ConfigLogging, ConfigDatabase):
    """
    This class represent the configuration for the Config Service tool
    """
    SECTION_INSTALLATION = 'installation'
    OPTION_INSTALLATION_PATH = 'path'
    OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH = 'server_management_path'
    OPTION_INSTALLATION_SERVER_GATEWAY_PATH = 'server_gateway_path'
    OPTION_INSTALLATION_CONFIG_SERVICE_PATH = 'config_service_path'
    OPTION_INSTALLATION_CLIENT_MANAGEMENT_SERVICE_PATH = 'client_management_service_path'

    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_DEPLOYMENT = 'deployment'
    OPTION_DEPLOYMENT_PATH = 'path'

    SECTION_BACKUP = 'backup'
    OPTION_BACKUP_PATH = 'path'

    SECTION_CPM = 'cpm'
    OPTION_CPM_GPM_BUILD_PATH = 'gpm_build_path'
    OPTION_CPM_GPM_DEFS_PATH = 'gpm_defs_path'
    OPTION_CPM_GPMC_DEFS_PATH = 'gpmc_defs_path'
    OPTION_CPM_GPMS_PATH = 'gpms_path'
    OPTION_CPM_DIST_PATH = 'dist_path'

    SECTION_SERVICE = 'service'
    OPTION_SERVICE_IP = 'ip'
    OPTION_SERVICE_PORT = 'port'
    OPTION_SERVICE_HTTPS_ENABLED = 'https_enabled'
    OPTION_SERVICE_HTTPS_CERTIFICATE_FILENAME = 'https_certificate_filename'
    OPTION_SERVICE_HTTPS_KEY_FILENAME = 'https_key_filename'

    SECTION_SERVICES = 'services'
    OPTION_SERVICES_ENABLED = 'enabled'

    SECTION_SUPPORT = 'support'
    OPTION_SUPPORT_PACKAGE_PATH = 'package_path'

    SECTION_SETUP = 'setup'
    OPTION_SETUP_GENERATE_SERVERS = 'generate_servers'

    SECTION_LICENSE = 'license'
    OPTION_LICENSE_FILENAME = 'filename'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    SECTION_SERVICE_MANAGEMENT = 'service_management'
    OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH = 'filedist_path'


    def __init__(self):
        ConfigLogging.__init__(self, 'gon_config_service')
        ConfigDatabase.__init__(self)

        self._set_defaults_config_service()
        if sys.platform == 'linux2':
            self._set_defaults_config_service_linux()

    def _set_defaults_config_service(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(9))
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_PATH, '../..')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH, '../../gon_server_management_service/win')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_GATEWAY_PATH, '../../gon_server_gateway_service/win')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_CONFIG_SERVICE_PATH, '../../gon_config_service/win')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_CLIENT_MANAGEMENT_SERVICE_PATH, '../../gon_client_management_service/win')
        self.set_default(ConfigConfigService.SECTION_PLUGIN_MODULES, ConfigConfigService.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigConfigService.SECTION_DEPLOYMENT, ConfigConfigService.OPTION_DEPLOYMENT_PATH, '../../config/deployed')
        self.set_default(ConfigConfigService.SECTION_BACKUP, ConfigConfigService.OPTION_BACKUP_PATH, './backup')
        self.set_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPM_BUILD_PATH, '../..')
        self.set_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPM_DEFS_PATH, '../../config/gpm/gpmdefs')
        self.set_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPMC_DEFS_PATH, '../../config/gpm/gpmcdefs')
        self.set_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPMS_PATH, '../../config/gpm/gpms')
        self.set_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_DIST_PATH, '../../distribution')
        self.set_default(ConfigConfigService.SECTION_SERVICE, ConfigConfigService.OPTION_SERVICE_IP, '127.0.0.1')
        self.set_default(ConfigConfigService.SECTION_SERVICE, ConfigConfigService.OPTION_SERVICE_PORT, lib.appl.config.ConfigBase.INT_VALUE(8071))
        self.set_default(ConfigConfigService.SECTION_SERVICE, ConfigConfigService.OPTION_SERVICE_HTTPS_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigConfigService.SECTION_SERVICE, ConfigConfigService.OPTION_SERVICE_HTTPS_CERTIFICATE_FILENAME, '../../config/certificates/gon_https_config_service.crt')
        self.set_default(ConfigConfigService.SECTION_SERVICE, ConfigConfigService.OPTION_SERVICE_HTTPS_KEY_FILENAME, '../../config/certificates/gon_https_config_service.key.noencrypt.pem')
        self.set_default(ConfigConfigService.SECTION_SUPPORT, ConfigConfigService.OPTION_SUPPORT_PACKAGE_PATH, '../../support_packages')
        self.set_default(ConfigConfigService.SECTION_SERVICES, ConfigConfigService.OPTION_SERVICES_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigConfigService.SECTION_LICENSE, ConfigConfigService.OPTION_LICENSE_FILENAME, '../../config/deployed/gon_license.lic')
        self.set_default(ConfigConfigService.SECTION_SETUP, ConfigConfigService.OPTION_SETUP_GENERATE_SERVERS, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigConfigService.SECTION_DICTIONARY, ConfigConfigService.OPTION_DICTIONARY_PATH, '../../config/dictionary')
        self.set_default(ConfigConfigService.SECTION_SERVICE_MANAGEMENT, ConfigConfigService.OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH, '../../config')

    def _set_defaults_config_service_linux(self):
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH, '../../gon_server_management_service/linux')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_GATEWAY_PATH, '../../gon_server_gateway_service/linux')
        self.set_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_CONFIG_SERVICE_PATH, '../../gon_config_service/linux')

    def get_license_handler(self):
        return components.license.gonlicense.GonLicenseHandler(self.license_absfilename)

    def load_and_set_scramble_salt(self, root=None, set_if_not_found=True):
        scramble_salt_filename = os.path.join(self.deployment_abspath, SCRAMBLE_SALT_FILENAME)
        if root is not None:
            scramble_salt_filename = os.path.join(root, scramble_salt_filename)
        lib.utility.xor_crypt.load_and_set_scramble_salt(scramble_salt_filename, set_if_not_found=set_if_not_found)

    def reset_to_default_when_restore_durring_upgrade(self):
        self.reset_to_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_SERVER_GATEWAY_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_CONFIG_SERVICE_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_INSTALLATION, ConfigConfigService.OPTION_INSTALLATION_CLIENT_MANAGEMENT_SERVICE_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_PLUGIN_MODULES, ConfigConfigService.OPTION_PLUGIN_MODULES_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_DEPLOYMENT, ConfigConfigService.OPTION_DEPLOYMENT_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPM_BUILD_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPM_DEFS_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPMC_DEFS_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_GPMS_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_CPM, ConfigConfigService.OPTION_CPM_DIST_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_DICTIONARY, ConfigConfigService.OPTION_DICTIONARY_PATH)
        self.reset_to_default(ConfigConfigService.SECTION_LICENSE, ConfigConfigService.OPTION_LICENSE_FILENAME)
        ConfigDatabase.reset_to_default_when_restore_durring_upgrade(self)

    installation_abspath = lib.appl.config.def_property_abspath(SECTION_INSTALLATION, OPTION_INSTALLATION_PATH)
    installation_path = lib.appl.config.def_property(SECTION_INSTALLATION, OPTION_INSTALLATION_PATH)
    installation_server_management_abspath = lib.appl.config.def_property_abspath(SECTION_INSTALLATION, OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH)
    installation_server_management_path = lib.appl.config.def_property(SECTION_INSTALLATION, OPTION_INSTALLATION_SERVER_MANAGEMENT_PATH)
    installation_server_gateway_abspath = lib.appl.config.def_property_abspath(SECTION_INSTALLATION, OPTION_INSTALLATION_SERVER_GATEWAY_PATH)
    installation_server_gateway_path = lib.appl.config.def_property(SECTION_INSTALLATION, OPTION_INSTALLATION_SERVER_GATEWAY_PATH)
    installation_config_service_abspath = lib.appl.config.def_property_abspath(SECTION_INSTALLATION, OPTION_INSTALLATION_CONFIG_SERVICE_PATH)
    installation_config_service_path = lib.appl.config.def_property(SECTION_INSTALLATION, OPTION_INSTALLATION_CONFIG_SERVICE_PATH)
    installation_client_management_service_abspath = lib.appl.config.def_property_abspath(SECTION_INSTALLATION, OPTION_INSTALLATION_CLIENT_MANAGEMENT_SERVICE_PATH)
    installation_client_management_service_path = lib.appl.config.def_property(SECTION_INSTALLATION, OPTION_INSTALLATION_CLIENT_MANAGEMENT_SERVICE_PATH)


    plugin_modules_abspath = lib.appl.config.def_property_abspath(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    deployment_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_PATH)
    backup_abspath = lib.appl.config.def_property_abspath(SECTION_BACKUP, OPTION_BACKUP_PATH)
    cpm_gpm_build_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPM_BUILD_PATH)
    cpm_gpm_defs_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPM_DEFS_PATH)
    cpm_gpmc_defs_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPMC_DEFS_PATH)
    cpm_gpms_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_GPMS_PATH)
    cpm_dist_abspath = lib.appl.config.def_property_abspath(SECTION_CPM, OPTION_CPM_DIST_PATH)
    service_ip = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_IP)
    service_port = lib.appl.config.def_property_int(SECTION_SERVICE, OPTION_SERVICE_PORT)
    service_https_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE, OPTION_SERVICE_HTTPS_ENABLED)
    service_https_certificate_filename = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_HTTPS_CERTIFICATE_FILENAME)
    service_https_certificate_filename_abspath = lib.appl.config.def_property_abspath(SECTION_SERVICE, OPTION_SERVICE_HTTPS_CERTIFICATE_FILENAME)
    service_https_key_filename = lib.appl.config.def_property(SECTION_SERVICE, OPTION_SERVICE_HTTPS_KEY_FILENAME)
    service_https_key_filename_abspath = lib.appl.config.def_property_abspath(SECTION_SERVICE, OPTION_SERVICE_HTTPS_KEY_FILENAME)
    support_package_abspath = lib.appl.config.def_property_abspath(SECTION_SUPPORT, OPTION_SUPPORT_PACKAGE_PATH)
    services_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICES, OPTION_SERVICES_ENABLED)
    license_absfilename = lib.appl.config.def_property_abspath(SECTION_LICENSE, OPTION_LICENSE_FILENAME)
    setup_generate_servers = lib.appl.config.def_property_boolean(SECTION_SETUP, OPTION_SETUP_GENERATE_SERVERS)
    dictionary_abspath = lib.appl.config.def_property_abspath(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)
    service_management_filedist_abspath = lib.appl.config.def_property_abspath(SECTION_SERVICE_MANAGEMENT, OPTION_SERVICE_MANAGEMENT_FILEDIST_PATH)



class ConfigConfigServiceLocal(lib.appl.config.ConfigBase):
    """
    This class represent the local configurations for Config Service
    """
    SECTION_SERVICE = 'service'
    OPTION_SERVICE_MANAGEMENT_FEATURES_ENABLED = 'management_features_enabled'

    def __init__(self):
        lib.appl.config.ConfigBase.__init__(self, 'gon_config_service_local')
        self._set_defaults_server_gateway_local()

    def _set_defaults_server_gateway_local(self):
        self.set_default(ConfigConfigServiceLocal.SECTION_SERVICE, ConfigConfigServiceLocal.OPTION_SERVICE_MANAGEMENT_FEATURES_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)

    management_features_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE, OPTION_SERVICE_MANAGEMENT_FEATURES_ENABLED)


class ConfigClientGateway(ConfigLogging):
    """
    This class represent the configuration for the Gateway Client
    """
    OPTION_LOG_SESSION_ENABLED = 'session_enabled'
    OPTION_LOG_SESSION_ENABLED_BY_REMOTE = 'session_enabled_by_remote'
    OPTION_LOG_SESSION_PATH = 'session_path'

    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_DEPLOYMENT = 'deployment'
    OPTION_DEPLOYMENT_PATH = 'path'
    OPTION_DEPLOYMENT_AUTO_CLOSE = 'auto_close'

    SECTION_GUI = 'gui'
    OPTION_GUI_IMAGE_PATH= 'image_path'
    OPTION_GUI_LOGIN_DEFAULT_BUTTON = 'login_default_button'
    OPTION_GUI_DEFAULT_BUTTON_VALUE_NEXT = 'next'
    OPTION_GUI_DEFAULT_BUTTON_VALUE_CANCEL = 'cancel'

    SECTION_SERVICE = 'service'
    OPTION_SERVICE_NUM_OF_THREADS = 'num_of_threads'

    SECTION_SERVICE_HTTP = 'service_http'
    OPTION_SERVICE_HTTP_IP = 'ip'
    OPTION_SERVICE_HTTP_PORT = 'port'
    OPTION_SERVICE_HTTP_PROXY_ENABLED = 'proxy_enabled'
    OPTION_SERVICE_HTTP_PROXY_AUTO = 'proxy_auto'
    OPTION_SERVICE_HTTP_PROXY_IP = 'proxy_ip'
    OPTION_SERVICE_HTTP_PROXY_PORT = 'proxy_port'
    OPTION_SERVICE_HTTP_LOG_2_ENABLED = 'log_2_enabled'
    OPTION_SERVICE_HTTP_LOG_3_ENABLED = 'log_3_enabled'
    OPTION_SERVICE_HTTP_USER_AGENT = 'user_agent'

    SECTION_SESSION = 'session'
    OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC = 'keep_alive_ping_interval_sec'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    SECTION_LOGIN = 'login'
    OPTION_LOGIN_SHOW_PASSWORD_PROMPT = 'show_password_prompt'
    OPTION_LOGIN_TRY_SSO = 'try_sso'
    OPTION_LOGIN_SECURITY_PACKAGE = 'security_package'
    OPTION_LOGIN_TARGET_SPN = 'target_spn'


    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client')
        self._set_defaults_client_gateway()

    def _set_defaults_client_gateway(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientGateway.SECTION_LOG, ConfigClientGateway.OPTION_LOG_SESSION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientGateway.SECTION_LOG, ConfigClientGateway.OPTION_LOG_SESSION_ENABLED_BY_REMOTE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientGateway.SECTION_LOG, ConfigClientGateway.OPTION_LOG_SESSION_PATH, './session_logs')
        self.set_default(ConfigClientGateway.SECTION_PLUGIN_MODULES, ConfigClientGateway.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientGateway.SECTION_DEPLOYMENT, ConfigClientGateway.OPTION_DEPLOYMENT_PATH, './deployed')
        self.set_default(ConfigClientGateway.SECTION_DEPLOYMENT, ConfigClientGateway.OPTION_DEPLOYMENT_AUTO_CLOSE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientGateway.SECTION_GUI, ConfigClientGateway.OPTION_GUI_IMAGE_PATH, './images')
        self.set_default(ConfigClientGateway.SECTION_GUI, ConfigClientGateway.OPTION_GUI_LOGIN_DEFAULT_BUTTON, ConfigClientGateway.OPTION_GUI_DEFAULT_BUTTON_VALUE_NEXT)
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_IP, '127.0.0.1')
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_PORT, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_PROXY_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_PROXY_AUTO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_PROXY_IP, '')
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_PROXY_PORT, lib.appl.config.ConfigBase.INT_VALUE(8080))
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_LOG_2_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_LOG_3_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientGateway.SECTION_SERVICE_HTTP, ConfigClientGateway.OPTION_SERVICE_HTTP_USER_AGENT, '')
        self.set_default(ConfigClientGateway.SECTION_SESSION, ConfigClientGateway.OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientGateway.SECTION_SERVICE, ConfigClientGateway.OPTION_SERVICE_NUM_OF_THREADS, lib.appl.config.ConfigBase.INT_VALUE(5))
        self.set_default(ConfigClientGateway.SECTION_DICTIONARY, ConfigClientGateway.OPTION_DICTIONARY_PATH, '../dictionary')

        self.set_default(ConfigClientGateway.SECTION_LOGIN, ConfigClientGateway.OPTION_LOGIN_SECURITY_PACKAGE, 'NTLM')
        self.set_default(ConfigClientGateway.SECTION_LOGIN, ConfigClientGateway.OPTION_LOGIN_SHOW_PASSWORD_PROMPT, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientGateway.SECTION_LOGIN, ConfigClientGateway.OPTION_LOGIN_TARGET_SPN, '')
        self.set_default(ConfigClientGateway.SECTION_LOGIN, ConfigClientGateway.OPTION_LOGIN_TRY_SSO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)

    log_session_enabled = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED)
    log_session_enabled_by_remote = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED_BY_REMOTE)
    log_session_path = lib.appl.config.def_property(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_PATH)
    plugin_modules_path = lib.appl.config.def_property(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    deployment_path = lib.appl.config.def_property(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_PATH)
    deployment_auto_close = lib.appl.config.def_property_boolean(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_AUTO_CLOSE)
    gui_image_path = lib.appl.config.def_property(SECTION_GUI, OPTION_GUI_IMAGE_PATH)
    gui_login_default_button = lib.appl.config.def_property(SECTION_GUI, OPTION_GUI_LOGIN_DEFAULT_BUTTON)
    service_http_ip = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_IP)
    service_http_port = lib.appl.config.def_property_int(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PORT)
    service_http_proxy_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_ENABLED)
    service_http_proxy_auto = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_AUTO)
    service_http_proxy_ip = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_IP)
    service_http_proxy_port = lib.appl.config.def_property_int(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_PORT)
    service_http_log_2_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_2_ENABLED)
    service_http_log_3_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_3_ENABLED)
    service_http_user_agent = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_USER_AGENT)
    session_keep_alive_ping_interval_sec = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC)
    service_num_of_threads = lib.appl.config.def_property_int(SECTION_SERVICE, OPTION_SERVICE_NUM_OF_THREADS)
    dictionary_path = lib.appl.config.def_property(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)

    login_target_spn = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_TARGET_SPN)
    login_security_package = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_SECURITY_PACKAGE)
    login_try_sso = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_TRY_SSO)
    login_show_password_prompt = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_SHOW_PASSWORD_PROMPT)



class ConfigClientDeviceService(ConfigLogging):
    """
    This class represent the configuration for the Gateway Client Device Service
    """
    SECTION_SERVICE = 'service'

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client_device_service')
        self._set_defaults_client_device_service()

    def _set_defaults_client_device_service(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(0))



class ConfigKeyMaker(ConfigLogging):
    """
    This class represent the configuration for the Key Maker
    """
    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_GUI = 'gui'
    OPTION_GUI_IMAGE_PATH= 'image_path'

    SECTION_DEVICES = 'devices'
    OPTION_DEVICE_LIST= 'device_list'


    def __init__(self):
        ConfigLogging.__init__(self, 'gon_key_maker')
        self._set_defaults()

    def _set_defaults(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(1))
        self.set_default(ConfigClientGateway.SECTION_PLUGIN_MODULES, ConfigClientGateway.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientGateway.SECTION_GUI, ConfigClientGateway.OPTION_GUI_IMAGE_PATH, './images')
        self.set_default(ConfigKeyMaker.SECTION_DEVICES, ConfigKeyMaker.OPTION_DEVICE_LIST, "")

    plugin_modules_path = lib.appl.config.def_property(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    gui_image_path = lib.appl.config.def_property(SECTION_GUI, OPTION_GUI_IMAGE_PATH)

    device_list = lib.appl.config.def_property(SECTION_DEVICES, OPTION_DEVICE_LIST)


class ConfigClientManagementService(ConfigLogging):
    """
    This class represent the configuration for the Management Client Service
    """
    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_DEPLOYMENT = 'deployment'
    OPTION_DEPLOYMENT_PATH = 'path'
    OPTION_DEPLOYMENT_SOFT_TOKEN_PATH = 'soft_token_path'
    OPTION_DEPLOYMENT_DOWNLOAD_CACHE_PATH = 'download_cache'

    SECTION_MANAGEMENT_DEPLOY_WS = 'management_deploy_ws'
    OPTION_MANAGEMENT_DEPLOY_WS_IP = 'ip'
    OPTION_MANAGEMENT_DEPLOY_WS_PORT = 'port'
    OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_ENABLED = 'https_enabled'
    OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_CERTIFICATE_FILENAME = 'https_certificate_filename'
    OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_KEY_FILENAME = 'https_key_filename'
    OPTION_MANAGEMENT_DEPLOY_TIMEOUT_SEC = 'timeout_sec'

    SECTION_MANAGEMENT_WS = 'management_ws'
    OPTION_MANAGEMENT_WS_IP = 'ip'
    OPTION_MANAGEMENT_WS_PORT = 'port'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client_management_service')
        self._set_defaults()

    def _set_defaults(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientManagementService.SECTION_PLUGIN_MODULES, ConfigClientManagementService.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientManagementService.SECTION_DEPLOYMENT, ConfigClientManagementService.OPTION_DEPLOYMENT_PATH, '../../config/deployed')
        self.set_default(ConfigClientManagementService.SECTION_DEPLOYMENT, ConfigClientManagementService.OPTION_DEPLOYMENT_SOFT_TOKEN_PATH, './soft_token_root')
        self.set_default(ConfigClientManagementService.SECTION_DEPLOYMENT, ConfigClientManagementService.OPTION_DEPLOYMENT_DOWNLOAD_CACHE_PATH, './download_cache')
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_WS_IP, '127.0.0.1')
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_WS_PORT,  lib.appl.config.ConfigBase.INT_VALUE(8073))
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_CERTIFICATE_FILENAME, './certificates/gon_https_management_client_service.crt')
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_KEY_FILENAME, './certificates/gon_https_management_client_service.key.noencrypt.pem')
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_DEPLOY_WS, ConfigClientManagementService.OPTION_MANAGEMENT_DEPLOY_TIMEOUT_SEC, lib.appl.config.ConfigBase.INT_VALUE(5*60))
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_WS, ConfigClientManagementService.OPTION_MANAGEMENT_WS_IP, '127.0.0.1')
        self.set_default(ConfigClientManagementService.SECTION_MANAGEMENT_WS, ConfigClientManagementService.OPTION_MANAGEMENT_WS_PORT,  lib.appl.config.ConfigBase.INT_VALUE(8072))
        self.set_default(ConfigClientManagementService.SECTION_DICTIONARY, ConfigClientManagementService.OPTION_DICTIONARY_PATH, './dictionary')

    plugin_modules_abspath = lib.appl.config.def_property_abspath(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)

    deployment_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_PATH)
    deployment_soft_token_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_SOFT_TOKEN_PATH)
    deployment_download_cache_abspath = lib.appl.config.def_property_abspath(SECTION_DEPLOYMENT, OPTION_DEPLOYMENT_DOWNLOAD_CACHE_PATH)
    management_deploy_ws_ip = lib.appl.config.def_property(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_IP)
    management_deploy_ws_port = lib.appl.config.def_property_int(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_PORT)
    management_deploy_ws_https_enabled = lib.appl.config.def_property_boolean(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_ENABLED)
    management_deploy_ws_https_certificate_filename = lib.appl.config.def_property(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_CERTIFICATE_FILENAME)
    management_deploy_ws_https_certificate_filename_abspath = lib.appl.config.def_property_abspath(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_CERTIFICATE_FILENAME)
    management_deploy_ws_https_key_filename = lib.appl.config.def_property(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_KEY_FILENAME)
    management_deploy_ws_https_key_filename_abspath = lib.appl.config.def_property_abspath(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_WS_HTTPS_KEY_FILENAME)
    management_deploy_timeout_sec = lib.appl.config.def_property_int(SECTION_MANAGEMENT_DEPLOY_WS, OPTION_MANAGEMENT_DEPLOY_TIMEOUT_SEC)
    management_ws_ip = lib.appl.config.def_property(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_IP)
    management_ws_port = lib.appl.config.def_property_int(SECTION_MANAGEMENT_WS, OPTION_MANAGEMENT_WS_PORT)
    dictionary_abspath = lib.appl.config.def_property_abspath(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)


class ConfigClientInstaller(ConfigLogging):
    """
    This class represent the configuration for the Client Installer
    """
    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_GUI = 'gui'
    OPTION_GUI_IMAGE_PATH= 'image_path'

    SECTION_DISCOVER = 'discover'
    OPTION_DISCOVER_DESKTOP_ENABLED = 'desktop_enabled'
    OPTION_DISCOVER_TOKEN_ENABLED = 'token_enabled'

    SECTION_CPM = 'cpm'
    OPTION_CPM_GPMS_PATH = 'gpms_path'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    SECTION_KEYS = 'keys'
    OPTION_GENERATE_KEYPAIR_ENABLED = 'generate_keypair_enabled'

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client_installer')
        self._set_defaults()

    def _set_defaults(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(9))
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_FILE, '../%s.log' % self.config_filename_prefix)
        self.set_default(ConfigClientInstaller.SECTION_PLUGIN_MODULES, ConfigClientGateway.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientInstaller.SECTION_GUI, ConfigClientGateway.OPTION_GUI_IMAGE_PATH, './images')
        self.set_default(ConfigClientInstaller.SECTION_DISCOVER, ConfigClientInstaller.OPTION_DISCOVER_DESKTOP_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientInstaller.SECTION_DISCOVER, ConfigClientInstaller.OPTION_DISCOVER_TOKEN_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientInstaller.SECTION_CPM, ConfigClientInstaller.OPTION_CPM_GPMS_PATH, './gpms')
        self.set_default(ConfigClientInstaller.SECTION_DICTIONARY, ConfigClientInstaller.OPTION_DICTIONARY_PATH, './dictionary')
        self.set_default(ConfigClientInstaller.SECTION_KEYS, ConfigClientInstaller.OPTION_GENERATE_KEYPAIR_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)

    plugin_modules_path = lib.appl.config.def_property(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    gui_image_path = lib.appl.config.def_property(SECTION_GUI, OPTION_GUI_IMAGE_PATH)
    dicover_desktop_enabled = lib.appl.config.def_property_boolean(SECTION_DISCOVER, OPTION_DISCOVER_DESKTOP_ENABLED)
    dicover_tokens_enabled = lib.appl.config.def_property_boolean(SECTION_DISCOVER, OPTION_DISCOVER_TOKEN_ENABLED)
    cpm_gpms_path = lib.appl.config.def_property(SECTION_CPM, OPTION_CPM_GPMS_PATH)
    dictionary_path = lib.appl.config.def_property(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)
    keys_generate_keypair_enabled = lib.appl.config.def_property_boolean(SECTION_KEYS, OPTION_GENERATE_KEYPAIR_ENABLED)


class ConfigClientUninstaller(ConfigLogging):
    """
    This class represent the configuration for the Client Uninstaller
    """
    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_GUI = 'gui'
    OPTION_GUI_IMAGE_PATH= 'image_path'

    SECTION_DICTIONARY = 'dictionary'
    OPTION_DICTIONARY_PATH = 'dictionary_path'

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client_uninstaller')
        self._set_defaults()

    def _set_defaults(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigLogging.OPTION_LOG_VERBOSE, lib.appl.config.ConfigBase.INT_VALUE(9))
        self.set_default(ConfigClientInstaller.SECTION_PLUGIN_MODULES, ConfigClientGateway.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientInstaller.SECTION_GUI, ConfigClientGateway.OPTION_GUI_IMAGE_PATH, './images')
        self.set_default(ConfigClientInstaller.SECTION_DICTIONARY, ConfigClientInstaller.OPTION_DICTIONARY_PATH, './dictionary')

    plugin_modules_path = lib.appl.config.def_property(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    gui_image_path = lib.appl.config.def_property(SECTION_GUI, OPTION_GUI_IMAGE_PATH)
    dictionary_path = lib.appl.config.def_property(SECTION_DICTIONARY, OPTION_DICTIONARY_PATH)



class ConfigClientStealth(ConfigLogging):
    """
    This class represent the configuration for the Gateway Client
    """
    OPTION_LOG_SESSION_ENABLED = 'session_enabled'
    OPTION_LOG_SESSION_ENABLED_BY_REMOTE = 'session_enabled_by_remote'
    OPTION_LOG_SESSION_PATH = 'session_path'

    SECTION_PLUGIN_MODULES = 'plugin_modules'
    OPTION_PLUGIN_MODULES_PATH = 'path'

    SECTION_SERVICE = 'service'
    OPTION_SERVICE_NUM_OF_THREADS = 'num_of_threads'

    SECTION_SERVICE_HTTP = 'service_http'
    OPTION_SERVICE_HTTP_IP = 'ip'
    OPTION_SERVICE_HTTP_PORT = 'port'
    OPTION_SERVICE_HTTP_PROXY_ENABLED = 'proxy_enabled'
    OPTION_SERVICE_HTTP_PROXY_AUTO = 'proxy_auto'
    OPTION_SERVICE_HTTP_PROXY_IP = 'proxy_ip'
    OPTION_SERVICE_HTTP_PROXY_PORT = 'proxy_port'
    OPTION_SERVICE_HTTP_LOG_2_ENABLED = 'log_2_enabled'
    OPTION_SERVICE_HTTP_LOG_3_ENABLED = 'log_3_enabled'
    OPTION_SERVICE_HTTP_USER_AGENT = 'user_agent'

    SECTION_ACTION = 'action'
    OPTION_ACTION_MENU_ACTION_TO_LAUNCH = 'menu_action_to_launch'

    SECTION_CPM = 'cpm'
    OPTION_CPM_GPMS_PATH = 'gpms_path'

    SECTION_LOGIN = 'login'
    OPTION_LOGIN_SHOW_PASSWORD_PROMPT = 'show_password_prompt'
    OPTION_LOGIN_TRY_SSO = 'try_sso'
    OPTION_LOGIN_SECURITY_PACKAGE = 'security_package'
    OPTION_LOGIN_TARGET_SPN = 'target_spn'

    SECTION_SESSION = 'session'
    OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC = 'keep_alive_ping_interval_sec'
    TIMEOUT_SEC = "timeout_sec"

    def __init__(self):
        ConfigLogging.__init__(self, 'gon_client_stealth')
        self._set_defaults_client_stealth()

    def _set_defaults_client_stealth(self):
        self.set_default(ConfigLogging.SECTION_LOG, ConfigClientStealth.OPTION_LOG_SESSION_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigClientStealth.OPTION_LOG_SESSION_ENABLED_BY_REMOTE, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigLogging.SECTION_LOG, ConfigClientStealth.OPTION_LOG_SESSION_PATH, './session_logs')

        self.set_default(ConfigClientStealth.SECTION_PLUGIN_MODULES, ConfigClientStealth.OPTION_PLUGIN_MODULES_PATH, '.')
        self.set_default(ConfigClientStealth.SECTION_SERVICE, ConfigClientStealth.OPTION_SERVICE_NUM_OF_THREADS, lib.appl.config.ConfigBase.INT_VALUE(5))
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_IP, '127.0.0.1')
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_PORT, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_PROXY_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_PROXY_AUTO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_PROXY_IP, '')
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_PROXY_PORT, lib.appl.config.ConfigBase.INT_VALUE(8080))
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_LOG_2_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_LOG_3_ENABLED, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientStealth.SECTION_SERVICE_HTTP, ConfigClientStealth.OPTION_SERVICE_HTTP_USER_AGENT, '')

        self.set_default(ConfigClientStealth.SECTION_ACTION, ConfigClientStealth.OPTION_ACTION_MENU_ACTION_TO_LAUNCH, '')
        self.set_default(ConfigClientStealth.SECTION_CPM, ConfigClientStealth.OPTION_CPM_GPMS_PATH, './gpms')
        self.set_default(ConfigClientStealth.SECTION_LOGIN, ConfigClientStealth.OPTION_LOGIN_SHOW_PASSWORD_PROMPT, lib.appl.config.ConfigBase.BOOLEAN_VALUE_TRUE)
        self.set_default(ConfigClientStealth.SECTION_LOGIN, ConfigClientStealth.OPTION_LOGIN_SECURITY_PACKAGE, 'NTLM')
        self.set_default(ConfigClientStealth.SECTION_LOGIN, ConfigClientStealth.OPTION_LOGIN_TARGET_SPN, '')
        self.set_default(ConfigClientStealth.SECTION_LOGIN, ConfigClientStealth.OPTION_LOGIN_TRY_SSO, lib.appl.config.ConfigBase.BOOLEAN_VALUE_FALSE)
        self.set_default(ConfigClientStealth.SECTION_SESSION, ConfigClientStealth.OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC, lib.appl.config.ConfigBase.INT_VALUE(0))
        self.set_default(ConfigClientStealth.SECTION_SESSION, ConfigClientStealth.TIMEOUT_SEC, lib.appl.config.ConfigBase.INT_VALUE(60))

    log_session_enabled = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED)
    log_session_enabled_by_remote = lib.appl.config.def_property_boolean(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_ENABLED_BY_REMOTE)
    log_session_path = lib.appl.config.def_property(ConfigLogging.SECTION_LOG, OPTION_LOG_SESSION_PATH)
    plugin_modules_path = lib.appl.config.def_property(SECTION_PLUGIN_MODULES, OPTION_PLUGIN_MODULES_PATH)
    service_num_of_threads = lib.appl.config.def_property_int(SECTION_SERVICE, OPTION_SERVICE_NUM_OF_THREADS)
    service_http_ip = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_IP)
    service_http_port = lib.appl.config.def_property_int(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PORT)
    service_http_proxy_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_ENABLED)
    service_http_proxy_auto = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_AUTO)
    service_http_proxy_ip = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_IP)
    service_http_proxy_port = lib.appl.config.def_property_int(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_PROXY_PORT)
    service_http_log_2_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_2_ENABLED)
    service_http_log_3_enabled = lib.appl.config.def_property_boolean(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_LOG_3_ENABLED)
    service_http_user_agent = lib.appl.config.def_property(SECTION_SERVICE_HTTP, OPTION_SERVICE_HTTP_USER_AGENT)
    action_menu_action_to_launhch = lib.appl.config.def_property(SECTION_ACTION, OPTION_ACTION_MENU_ACTION_TO_LAUNCH)
    cpm_gpms_path = lib.appl.config.def_property(SECTION_CPM, OPTION_CPM_GPMS_PATH)
    login_show_password_prompt = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_SHOW_PASSWORD_PROMPT)
    login_security_package = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_SECURITY_PACKAGE)
    login_target_spn = lib.appl.config.def_property(SECTION_LOGIN, OPTION_LOGIN_TARGET_SPN)
    login_try_sso = lib.appl.config.def_property_boolean(SECTION_LOGIN, OPTION_LOGIN_TRY_SSO)
    session_keep_alive_ping_interval_sec = lib.appl.config.def_property_int(SECTION_SESSION, OPTION_SESSION_KEEP_ALIVE_PING_INTERVAL_SEC)
    session_timeout_sec = lib.appl.config.def_property_int(SECTION_SESSION, TIMEOUT_SEC)
