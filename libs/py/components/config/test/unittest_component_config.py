"""
Unittest of config component
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import ConfigParser
import os.path

import lib.appl.config
import components.config.common
import lib.utility.xor_crypt as xor_crypt

class ConfigServerManagementTest(unittest.TestCase):
    def setUp(self):
        pass
            
    def tearDown(self):
        pass

    def test_default(self):
        
        config_server_management = components.config.common.ConfigServerManagement() 
        default_log_type = config_server_management.log_type
        default_log_verbose = config_server_management.log_verbose
        default_log_enabled = config_server_management.log_enabled
        
        config_server_management.log_type = 'xml'
        config_server_management.log_enabled = False
        config_server_management.log_verbose = 10

        self.assertTrue(default_log_type != config_server_management.log_type)
        self.assertTrue(default_log_enabled != config_server_management.log_enabled)
        self.assertTrue(default_log_verbose != config_server_management.log_verbose)


        temp_root = giri_unittest.mkdtemp()
        config_server_management.write_config_file(temp_root)
        file = open(os.path.join(temp_root, 'gon_server_management.ini'))
        print file.read()
        file.close()


    def test_read_write(self):
        config_server_management = components.config.common.ConfigServerManagement() 
        config_server_management.log_type = 'xml'
        config_server_management.log_enabled = False
        config_server_management.log_verbose = 10

        temp_root = giri_unittest.mkdtemp()
        config_server_management.write_config_file(temp_root)
        config_server_management_restore = components.config.common.ConfigServerManagement() 
        
        config_filename = os.path.join(temp_root, '%s.ini' % config_server_management.config_filename_prefix)
        config_server_management_restore.read_config_file(config_filename)

        self.assertTrue(config_server_management_restore.log_type == config_server_management.log_type)
        self.assertTrue(config_server_management_restore.log_enabled == config_server_management.log_enabled)
        self.assertTrue(config_server_management_restore.log_verbose == config_server_management.log_verbose)

        config = ConfigParser.ConfigParser()
        config.read(config_filename)
        self.assertTrue(config.has_section(components.config.common.ConfigServerManagement.SECTION_LOG))
        self.assertFalse(config.has_section(config_server_management_restore._get_default_section_name(components.config.common.ConfigServerManagement.SECTION_LOG)))


        file = open(config_filename)
        print file.read()
        file.close()
        
    def test_list_set_and_get(self):
        config_server_management = components.config.common.ConfigServerGateway() 
        
        ports = config_server_management.service_client_connect_ports
        ports.append(42)
        config_server_management.service_client_connect_ports = ports
        self.assertEqual(lib.appl.config.convert_string_to_intlist(lib.appl.config.convert_intlist_to_string(config_server_management.service_client_connect_ports)), ports)

        addresses = config_server_management.service_client_connect_addresses
        addresses.append('172.0.0.2')
        config_server_management.service_client_connect_addresses = addresses
        self.assertEqual(lib.appl.config.convert_string_to_stringlist(lib.appl.config.convert_stringlist_to_string(config_server_management.service_client_connect_addresses)), addresses)


    def test_list_set_and_get_default(self):
        config_server_management = components.config.common.ConfigServerManagement() 
        config_server_management.db_database = 'hej'
        config_server_management.set_to_default()
        self.assertNotEqual(config_server_management.db_database, 'hej')


    def test_password_with_salt(self):
        config_server_management = components.config.common.ConfigServerManagement() 
        password = "test_password"
        config_server_management.db_password = password
        self.assertEqual(config_server_management.db_password, password)

        xor_crypt.xor_salt = "changed"
        self.assertNotEqual(password, config_server_management.db_password)
        self.assertEqual(config_server_management.db_password, lib.appl.config.ConfigBase.PASSWORD_SALT_SAFE_ERROR)
        
        
    def test_none(self):
        temp_folder = giri_unittest.mkdtemp()
        
        config_file = open(os.path.join(temp_folder, 'gon_server_gateway_local.ini'), 'w')
        config_file.write("""
[service]
port = 9999
""")
        config_file.close()
        
        config_local = components.config.common.ConfigServerGatewayLocal()
        config_local.read_config_file_from_folder(temp_folder)
        config_local.service_sid = "1234567890"
        config_local.write_config_file(temp_folder)

        config_local2 = components.config.common.ConfigServerGatewayLocal()
        config_local2.read_config_file_from_folder(temp_folder)

        self.assertEqual(config_local.service_port, config_local2.service_port)
        self.assertEqual(config_local.service_sid, config_local2.service_sid)
        self.assertEqual(config_local.service_ip, config_local2.service_ip)

        
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
