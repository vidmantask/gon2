"""
This file contains the interfaces to the session functionality. 
"""
class ISessionEventhandlerReady(object):
    """
    This interface defines the events that can be recived from a session.
    """
    def session_state_ready(self):
        """
        The session is ready for use
        """
        raise NotImplementedError
    
    def session_state_closed(self):
        """
        The session is closed
        """
        raise NotImplementedError

    def session_state_key_exchange(self):
        """
        The session switche to key exchange state
        """
        raise NotImplementedError

    def session_read_continue_state_ready(self):
        """
        Continue reading packages
        """
        raise NotImplementedError


class ISession(object):
    """
    This is the interface of a session
    """
    def set_eventhandler_ready(self, eventhandler):
        """ 
        Set the eventhandler of the session. 
        The event handler is expected to have the ISessionEventhandler interface.
        """
        raise NotImplementedError
        
    def close(self):
        """
        Tell the session to close
        """ 
        raise NotImplementedError

    def is_closed(self):
        """
        Returns True if the session is closed
        """ 
        raise NotImplementedError

    def get_session_id(self):
        """
        Returns the session id
        """ 
        raise NotImplementedError
        
    def read_start_state_ready(self):
        """
        Tell the session to start a read operation 
        """
        raise NotImplementedError

    def add_tunnelendpoint(self, child_id, tunnelendpoint):
        """
        Add a tunnelendpoint
        """ 
        raise NotImplementedError
    
    def add_tunnelendpoint_tunnel(self, child_id, tunnelendpoint):
        """
        Add a tunnel tunnelendpoint
        """ 
        raise NotImplementedError
    