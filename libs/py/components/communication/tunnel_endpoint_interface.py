"""
This file contains the interfaces to the tunnel endpoint functionality. 

A tunnel endpoint represent a communication abstraction for the endpoints of a tunnel.

An example of two tunnel endpoints could be the 'Authorization Component on the Client' and 
'Authorization Component on the Server' whitch need a tunnel for communication.

It is the caller responsibility to setup the tunnel endpoints.
"""


class ITunnelendpointEventhandler(object):
    """
    This interface defines the events that can be received from a session.
    """
    def tunnelendpoint_recieve(self, package):
        """
        The tunnel endpoint has received a package
        This call is executed by a communication-work-thread and should return as fast as possible.
        """
        raise NotImplementedError


class ITunnelendpoint(object):
    """
    This abstract interface represent a tunnel endpoint.
    """
    def set_eventhandler(self, eventhandler):
        """ 
        Set the eventhandler of the tunnel endpoint. 
        The event handler is expected to have the ITunnelendpointEventhandler interface.
        """
        raise NotImplementedError

    def tunnelendpoint_send(self, package):
        """
        Sends a package to the matching endpoint on the other side.
        This call is executed by the caller-thread, but the work is transfered to a communication-work-thread.  
        """
        raise NotImplementedError
        

class ITunnelendpointReliableCrypted(ITunnelendpoint):
    """
    This interface represent a reliable crypted tunnel endpoint.

    Packages are encrypted and guaranteed to be delivered,
    and guaranteed to be delivered in the right order.
    """
    pass


class ITunnelendpointReliableCryptedTunnel(ITunnelendpointReliableCrypted):
    """
    This interface represent a reliable crypted tunnel endpoint, 
    where additional child tunnel endpoints can be attached.
    """

    def add_tunnelendpoint_tunnel(self, child_id, tunnel_endpoint):
        """
        Add a child tunnel endpoint to this tunnel
        """
        raise NotImplementedError

    def close_tunnelendpoint_tunnel(self, child_id):
        """
        Remove / unregister a previously created child_id 
        """
        raise NotImplementedError


class ITunnelendpointFactory(object):
    """
    This is a abstract interface and represent a endpoint factory.
    
    It is intended to be used for creation of child endpoints where,
    the child itself knows its identification and the parent just follow that.
    """    

    def create_tunnelendpoint_tunnel(self, child_id):
        """
        Create a tunnel endpoint and return the endpoint. 
        """
        raise NotImplementedError
    
    def close_tunnelendpoint_tunnel(self, child_id):
        """
        Remove / unregister a previously created child_id 
        """
        raise NotImplementedError

