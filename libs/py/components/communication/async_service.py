"""
This file contains the interfaces to the async socket(raw_tunnel_endpoint) functionality. 
"""
import sys
import time

import lib_cpp.communication

import threading
import lib.checkpoint


def init(checkpoint_handler):
    """
    Initialization of the communication and cryptation framework.
    If any selftest are assiociated with the cryptation framwork the are runned during this call.
    """
    lib_cpp.communication.init_communication(checkpoint_handler.checkpoint_handler)


class AsyncServiceThread(threading.Thread):
    """
    Helper class used by AsyncService for handling a single communication core thread
    """ 
    def __init__(self, com_async_service, id):
        threading.Thread.__init__(self, name='AsyncServiceThread#%s' % id)
        self.com_async_service = com_async_service
    
    def run(self):
        self.com_async_service.run()
    
        
class AsyncService(threading.Thread):
    def __init__(self, checkpoint_handler, module_id, num_of_threads, keep_running=False):
        threading.Thread.__init__(self, name='AsyncService')
        self.com_async_service = lib_cpp.communication.APIAsyncService_create(checkpoint_handler.checkpoint_handler)
        self.num_of_threads = num_of_threads
        self.checkpoint_handler = checkpoint_handler
        self.module_id = module_id
        self.threads = []
        self.running = False
        self._has_started = False
        self.keep_running = keep_running
        
    def run(self):
        with self.checkpoint_handler.CheckpointScope("AsyncService.run", self.module_id, lib.checkpoint.DEBUG): 
            try:
                self.running = True
                self._has_started = True
                while True:
                    for i in range(self.num_of_threads):
                        thread = AsyncServiceThread(self.com_async_service, id)
                        thread.start()
                        self.threads.append(thread)
                    for thread in self.threads:
                        thread.join()
                    if not self.keep_running:
                        break
                    else:
                        self.com_async_service.reset()
                        time.sleep(1)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("AsyncService.error", self.module_id, lib.checkpoint.ERROR, etype, evalue, etrace)
            self.running = False

    def get_com_async_service(self):
        return self.com_async_service

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("AsyncService.stop", self.module_id, lib.checkpoint.DEBUG): 
            self.keep_running = False
            self.com_async_service.stop()

    def stop_and_wait(self):
        if self.is_running():
            self.stop()
        while (self.is_running()):
            time.sleep(1)

    def is_running(self):
        return self.running

    def has_started(self):
        return self._has_started

    def sleep_start(self, checkpoint_handler, min, sec, ms, callback_self, callback_method_name, callback_arg=None):
        self.com_async_service.sleep_start(checkpoint_handler.checkpoint_handler, min, sec, ms, callback_self, callback_method_name, callback_arg)

    def sleep_start_mutex(self, checkpoint_handler, mutex, min, sec, ms, callback_self, callback_method_name, callback_arg=None):
        self.com_async_service.sleep_start_mutex(checkpoint_handler.checkpoint_handler, mutex, min, sec, ms, callback_self, callback_method_name, callback_arg)
    
    def create_mutex(self, mutex_id):
        return self.com_async_service.create_mutex(mutex_id)
    
    def memory_guard_cleanup(self):
        self.com_async_service.memory_guard_cleanup()
        
    def memory_guard_is_empty(self):
        return self.com_async_service.memory_guard_is_empty()
  

class MutexScopeLock(object):
    def __init__(self, mutex):
        self.mutex = mutex
        self._got_lock = False
        
    def __enter__(self):
        print "MutexScopeLock.__enter__.waiting"
        self._got_lock = self.mutex.lock()
        print "MutexScopeLock.__enter__", self._got_lock

    def __exit__(self, type, value, traceback):
        print "MutexScopeLock.__exit__.waiting", self._got_lock
        if self._got_lock:
            self.mutex.unlock()
            self._got_lock = False
        print "MutexScopeLock.__exit__"



