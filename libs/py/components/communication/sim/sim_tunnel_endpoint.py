"""
Simullation of connected tunnel endpoints.
"""
import time
import sys

import unittest
import lib.checkpoint
import lib.config

import lib_cpp.communication

from components.communication import session_manager
from components.communication import session
from components.communication import tunnel_endpoint
from components.communication import tunnel_endpoint_base

module_id = "SimTunnelEndpoint"
server_ip = '127.0.0.1'
server_port = 8014


import components.communication.async_service
components.communication.async_service.init(lib.checkpoint.CheckpointHandler.get_cout_all())


class SimSession(session.APISessionEventhandlerReady):
    """
    """
    def __init__(self, async_service, checkpoint_handler, new_session):
        session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        self._session = new_session
        self._session.set_eventhandler_ready(self)
        self._tunnel_endpoint = lib_cpp.communication.APITunnelendpointReliableCryptedTunnel_create(async_service.get_com_async_service(), checkpoint_handler.checkpoint_handler)
        self._session.add_tunnelendpoint_tunnel(1, self._tunnel_endpoint)
        self.ready = False

    def session_state_ready(self):
        self.ready = True

    def session_state_closed(self):
        self.ready = False
        self._session = None
        self.tunnel_endpoint = None

    def session_state_key_exchange(self):
        pass

    def session_read_continue_state_ready(self):
        return True

    def is_ready(self):
        return self.ready

    def get_tunnel_endpoint(self):
        return self._tunnel_endpoint

    def session_user_signal(self, signal_id, message):
        print "SimSession.session_user_signal", signal_id, message


class SimSessionManagerEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self, async_service, checkpoint_handler):
        session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.session = None
        self._is_closed = False
        self._checkpoint_handler = checkpoint_handler
        self.async_service = async_service

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_connecting(self, connection_id, connection_titler):
        pass # signal ignored for now

    def session_manager_failed(self, message):
        print "session_manager_failed", message

    def session_manager_connecting_failed(self, connection_id):
        pass # signal ignored for now

    def session_manager_session_created(self, connection_id, new_session):
        self.session = SimSession(self.async_service, self._checkpoint_handler, new_session)

    def session_manager_session_closed(self, session_id):
        self._is_closed = True

    def session_manager_closed(self):
        self._is_closed = True

    def is_closed(self):
        return self._is_closed

    def is_ready(self):
        if self.session != None:
            return self.session.is_ready()
        return False


class SimThread(object):
    def __init__(self, checkpoint_handler, async_service):
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service

        known_secret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        known_secret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

        self.session_manager_eventhandler_server = SimSessionManagerEventhandler(self.async_service, self.checkpoint_handler)
        self.session_manager_eventhandler_client = SimSessionManagerEventhandler(self.async_service, self.checkpoint_handler)
        self.session_manager_server = session_manager.APISessionManagerServer_create(self.async_service.get_com_async_service(), self.session_manager_eventhandler_server, self.checkpoint_handler.checkpoint_handler, known_secret_server, server_ip, server_port, "1")
        self.session_manager_server.set_option_reuse_address(True)
        self.session_manager_client = session_manager.APISessionManagerClient_create(self.async_service.get_com_async_service(), self.session_manager_eventhandler_client, self.checkpoint_handler.checkpoint_handler, known_secret_client, lib_cpp.communication.ApplProtocolType.PYTHON)
        self.session_manager_client.set_servers(lib.config.parse_servers(checkpoint_handler, '%s, %d' %(server_ip, server_port)))

    def is_ready(self):
        return self.session_manager_eventhandler_server.is_ready() and self.session_manager_eventhandler_client.is_ready()

    def start(self):
        try:
            self.session_manager_server.start()
            self.session_manager_client.start()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("SimThread.run.error", module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)

    def start_and_wait_until_ready(self):
        self.start()
        while not self.is_ready():
            time.sleep(1)

    def is_closed(self):
        return self.session_manager_eventhandler_server.is_closed() and self.session_manager_eventhandler_client.is_closed()

    def stop(self):
        self.session_manager_server.close_start()
        self.session_manager_client.close_start()

    def stop_and_wait_until_closed(self):
        self.stop()
        while not self.is_closed():
            time.sleep(1)

    def get_tunnelendpoint_client(self):
        return self.session_manager_eventhandler_client.session.get_tunnel_endpoint()

    def get_tunnelendpoint_server(self):
        return self.session_manager_eventhandler_server.session.get_tunnel_endpoint()

    def get_com_session_client(self):
        return self.session_manager_eventhandler_client.session._session

    def get_com_session_server(self):
        return self.session_manager_eventhandler_server.session._session

    def get_async_service(self):
        return self.async_service

    def sleep_start_mutex(self, min, sec, ms, callback_self, callback_method_name, callback_arg):
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_com_session_server().get_mutex(), min, sec, ms, callback_self, callback_method_name, callback_arg)
