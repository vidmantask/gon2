"""
This file contains the API to the C++ implementation of the tcp-tunnel endpoint functionality. 
"""
import sys

import lib_cpp.communication

APITunnelendpointTCPEventhandler = lib_cpp.communication.APITunnelendpointTCPEventhandler
APITunnelendpointTCPTunnelClientEventhandler = lib_cpp.communication.APITunnelendpointTCPTunnelClientEventhandler
APITunnelendpointTCPTunnelServerEventhandler = lib_cpp.communication.APITunnelendpointTCPTunnelServerEventhandler
APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler = lib_cpp.communication.APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler;

import lib.encode


class PortforwardTCPConnection(APITunnelendpointTCPEventhandler):
    """
    Class representing a tcp-connection created by a PortforwardClient
    """
    def __init__(self, checkpoint_handler, connection):
        APITunnelendpointTCPEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.connection_ = connection
        
    def __del__(self):
        if self.connection_ is not None:
            self.connection_.reset_tcp_eventhandler()
            self.close(True)
            
    def tunnelendpoint_tcp_eh_ready(self):
        """ Internal callback """
        self.ready()
        
    def tunnelendpoint_tcp_eh_closed(self):
        """ Internal callback """
        self.closed()

    def close(self, force):
        """ 
        Close the connection. 
        
        The force argument is a bool-flag that indicate if the close should be forced (without empty buffers).
        """
        self.connection_.close(force)

    def read_start(self):
        """ 
        Start the asio read. 
        """
        self.connection_.read_start()

    def get_ip_local(self):
        """ 
        Returns the local part of the socket.
        """
        return self.connection_.get_ip_local()

    def get_ip_remote(self):
        """ 
        Returns the remote part of the socket.
        """
        return self.connection_.get_ip_remote()

    def ready(self):
        """
        Callback that indicate that the portforward has been connected to the server-side and is ready to read/write data.
        """ 
        self.read_start()
    
    def closed(self):
        """
        Callback that indicate that the portforward has been closed.
        """ 
        pass


class TCPPortforwardClient(APITunnelendpointTCPTunnelClientEventhandler):
    """ 
    Class representing the client side of a tcp-portforward'er
    """
    def __init__(self, checkpoint_handler, async_service, listen_ip, listen_port, tcp_options=[]):
        APITunnelendpointTCPTunnelClientEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = checkpoint_handler
        self.tunnelendpoint_ = lib_cpp.communication.APITunnelendpointTCPTunnelClient_create(async_service.get_com_async_service(), self.checkpoint_handler.checkpoint_handler, listen_ip, listen_port)
        self.tunnelendpoint_.set_tcp_eventhandler(self)
        self.portforward_connections = []
        if "SO_REUSEADDR" in tcp_options:
            self.tunnelendpoint_.set_option_reuse_address(True)
    
    def __del__(self):
        if self.tunnelendpoint_ is not None:
            self.tunnelendpoint_.reset_tcp_eventhandler()
            self.tunnelendpoint_.close(True)
    
    def as_tunnelendpoint(self):
        return self.tunnelendpoint_
        
    def tunnelendpoint_tcp_eh_ready(self):
        """ Internal callback """
        self.ready()
    
    def tunnelendpoint_tcp_eh_closed(self):
        """ Internal callback """
        self.closed()

    def tunnelendpoint_tcp_eh_connected(self, connection):
        """ Internal callback """
        return self.accepted(connection)

    def tunnelendpoint_tcp_eh_acceptor_error(self, message):
        """ Internal callback """
        self.tunnelendpoint_.reset_tcp_eventhandler()
        self.tunnelendpoint_.close(True)
        self.error(message.decode(lib.encode.get_current_encoding(), 'ignore'))

    def tunnelendpoint_tcp_eh_accept_start_continue(self):
        return True

    def tunnelendpoint_tcp_eh_recieve(self, message):
        """ Internal callback """
        pass

    def tunnelendpoint_send(self, message):
        self.tunnelendpoint_.tunnelendpoint_send(message)
        
    def get_ip_local(self):
        """ 
        Returns the local part of the socket.
        """
        return self.tunnelendpoint_.get_ip_local()

    def close(self, force = False, close_and_forget = False):
        """ 
        Close the tunnelendpoint. 
        The force argument is a bool-flag that indicate if the close should be forced (without empty buffers).
        """
        if close_and_forget:
            self.tunnelendpoint_.reset_tcp_eventhandler()
        self.tunnelendpoint_.close(force)
        
    def is_closed(self):
        """ 
        Returns true if the tunnelendpoint is closed.
        """
        return self.tunnelendpoint_.is_closed()
    
    def ready(self):
        """
        Callback that indicate that the portforward has been connected to the server-side and is ready to read/write data.
        """ 
        self.tunnelendpoint_.accept_start()
        
    def accepted(self, connection):
        """
        Callback that indicate that connection on the listen port has been accepted. If true is returned the connection is accepted,
        else is the connection ignored.
        """ 
        self.portforward_connections.append(PortforwardTCPConnection(self.checkpoint_handler, connection))
        return True

    def closed(self):
        """
        Callback that indicate that the portforward has been closed. 
        """ 
        pass

    def error(self, message):
        """
        Callback that indicate that an error has occurred 
        """ 
        pass
        

class TCPPortforwardServer(APITunnelendpointTCPTunnelServerEventhandler):
    """ 
    Class representing the server side of a tcp-portforward'er
    """
    def __init__(self, async_service, checkpoint_handler, server_ip, server_port):
        APITunnelendpointTCPTunnelServerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = checkpoint_handler
        self.tunnelendpoint_ = lib_cpp.communication.APITunnelendpointTCPTunnelServer_create(async_service.get_com_async_service(), self.checkpoint_handler.checkpoint_handler, server_ip, server_port)
        self.tunnelendpoint_.set_tcp_eventhandler(self)
    
    def __del__(self):
        if self.tunnelendpoint_ is not None:
            self.tunnelendpoint_.reset_tcp_eventhandler()
            self.tunnelendpoint_.close(True)

    def as_tunnelendpoint(self):
        return self.tunnelendpoint_
        
    def tunnelendpoint_send(self, message):
        self.tunnelendpoint_.tunnelendpoint_send(message)
    
    def tunnelendpoint_tcp_eh_ready(self):
        """ Internal callback """
        self.ready()
    
    def tunnelendpoint_tcp_eh_closed(self):
        """ Internal callback """
        self.closed()

    def tunnelendpoint_tcp_eh_recieve(self, message):
        pass

    def close(self, force = False, close_and_forget = False):
        """ 
        Close the tunnelendpoint. 
        The force argument is a bool-flag that indicate if the close should be forced (without empty buffers).
        """
        if close_and_forget:
            self.tunnelendpoint_.reset_tcp_eventhandler()
        self.tunnelendpoint_.close(force)
        
    def is_closed(self):
        """ 
        Returns true if the tunnelendpoint is closed.
        """
        return self.tunnelendpoint_.is_closed()

    def ready(self):
        """
        Callback that indicate that the portforward has been connected to the server-side and is ready to read/write data.
        """ 
        pass
    
    def closed(self):
        """
        Callback that indicate that the portforward has been closed. 
        """ 
        pass

    def error(self, message):
        """
        Callback that indicate that an error has occurred 
        """ 
        pass




class TCPPortforwardServerWithConnectionEngine(APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler):
    """ 
    Class representing the server side of a tcp-portforward'er
    """
    def __init__(self, async_service, checkpoint_handler):
        APITunnelendpointTCPTunnelServerWithConnectionEngineEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.checkpoint_handler = checkpoint_handler
        self.tunnelendpoint_ = lib_cpp.communication.APITunnelendpointTCPTunnelServerWithConnectionEngine_create(async_service.get_com_async_service(), self.checkpoint_handler.checkpoint_handler)
        self.tunnelendpoint_.set_tcp_eventhandler(self)
        self.async_service = async_service

    def __del__(self):
        if self.tunnelendpoint_ is not None:
            self.tunnelendpoint_.reset_tcp_eventhandler()
            self.tunnelendpoint_.close(True)
    
    def create_and_add_tunnelendpoint(self, checkpoint_handler, child_id):
        new_tunnel_endpoint = lib_cpp.communication.APITunnelendpointReliableCrypted_create(self.async_service.get_com_async_service(), checkpoint_handler.checkpoint_handler)
        new_tunnel_endpoint.set_skip_connection_handshake()
        self.tunnelendpoint_.add_tunnelendpoint(child_id, new_tunnel_endpoint)
        return new_tunnel_endpoint

    def remove_tunnelendpoint_tunnel(self, child_id):
        self.tunnelendpoint_.remove_tunnelendpoint(child_id)

    def as_tunnelendpoint(self):
        return self.tunnelendpoint_
        
    def tunnelendpoint_send(self, message):
        self.tunnelendpoint_.tunnelendpoint_send(message)
    
    def tunnelendpoint_tcp_eh_ready(self):
        """ Internal callback """
        self.ready()
    
    def tunnelendpoint_tcp_eh_closed(self):
        """ Internal callback """
        self.closed()

    def tunnelendpoint_tcp_eh_recieve(self, message):
        pass

    def tunnelendpoint_tcp_eh_recieve_remote_connect(self, connection_id):
        """ Internal callback """
        self.remote_accepted(connection_id)

    def tunnelendpoint_tcp_eh_recieve_remote_close(self, connection_id):
        """ Internal callback """
        self.remote_close(connection_id)

    def tunnelendpoint_tcp_eh_recieve_remote_eof(self, connection_id):
        """ Internal callback """
        self.remote_eof(connection_id)

    def tunnelendpoint_tcp_eh_recieve_remote_closed(self, connection_id):
        """ Internal callback """
        self.remote_closed(connection_id)

    def close(self, force = False, close_and_forget = False):
        """ 
        Close the tunnelendpoint. 
        The force argument is a bool-flag that indicate if the close should be forced (without empty buffers).
        """
        if close_and_forget:
            self.tunnelendpoint_.reset_tcp_eventhandler()
        self.tunnelendpoint_.close(force)
        
    def is_closed(self):
        """ 
        Returns true if the tunnelendpoint is closed.
        """
        return self.tunnelendpoint_.is_closed()

    def is_done(self):
        """ 
        Returns true if the tunnelendpoint is done with all, and is ready for destruction.
        """
        return self.tunnelendpoint_.is_done()

    def remote_send_eof(self, connection_id):
        """ 
        Send eof to remote child
        """
        self.tunnelendpoint_.tunnelendpoint_send_remote_eof(connection_id)
        
    def remote_send_closed(self, connection_id):
        self.tunnelendpoint_.tunnelendpoint_send_remote_closed(connection_id)

    def ready(self):
        """
        Callback that indicate that the portforward has been connected to the server-side and is ready to read/write data.
        """ 
        pass
    
    def closed(self):
        """
        Callback that indicate that the portforward has been closed. 
        """ 
        pass

    def error(self, message):
        """
        Callback that indicate that an error has occurred 
        """ 
        pass

    def remote_accepted(self, connection_id):
        """
        Callback that indicate that a remote child has been created  
        """ 
        pass
    
    def remote_eof(self, connection_id):
        """
        Callback that indicate that a remote child has recived eof 
        """ 
        pass

    def remote_close(self, connection_id):
        """
        Callback that indicate that a remote child request a close 
        """ 
        pass

    def remote_closed(self, connection_id):
        """
        Callback that indicate that a remote child has closed 
        """ 
        pass

    def remote_recieve_connect_ok(self, connection_id): 
        self.tunnelendpoint_.tunnelendpoint_send_remote_recieve_connect_ok(connection_id)

    def remote_recieve_connect_error(self, connection_id): 
        self.tunnelendpoint_.tunnelendpoint_send_remote_recieve_connect_error(connection_id)
        
    def enable_checkpoint_tunnel(self, log_client_path):
        self.tunnelendpoint_.enable_checkpoint_tunnel(log_client_path)
        
