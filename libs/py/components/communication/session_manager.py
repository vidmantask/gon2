"""
This file contains the interfaces to the session manager functionality. 
"""
import lib_cpp.communication

#
# API implementation of the interface ISessionEventhandlerReady
#
APISessionManagerEventhandler = lib_cpp.communication.APISessionManagerEventhandler


#
# API implementation of the interface ISession
#
APISessionManagerClient_create = lib_cpp.communication.APISessionManagerClient_create
APISessionManagerServer_create = lib_cpp.communication.APISessionManagerServer_create


