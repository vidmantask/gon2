# coding=iso-8859-1

"""
Messages are invocations of remote procedures, 
targeted for a specific module instance and a specific receiver with named parameters
"""
#import marshal
import cPickle
import types
import sys

import lib_cpp.communication


class Message(object):

    """Signals/events/messages sent between client and server modules"""

    def __init__(self, receiver_id, **values):
        self.receiver_id = receiver_id
        self.values = values

    def __str__(self):
        return '%s: %r' % (self.receiver_id, self.values)

    def __repr__(self):
        return self.to_package()

    def to_package(self):
        return cPickle.dumps((self.receiver_id, self.values))

    def call_receiver(self, self_module):
        # print 'Message to %s.%s: %s' % (self_module, self.receiver_id, self.values)
        try:
            receiver_method = getattr(self_module, self.receiver_id)
        except KeyError: # FIXME: is this the right exception?
            print 'Error finding %r in %r' % (self.receiver_id, self_module)
            raise
        try:
            receiver_method(**self.values)
        except TypeError:
            print 'Error calling %r in %r with args %r' % (self.receiver_id, self_module, sorted(self.values.keys()))
            raise

    def add_arg(self, arg_name, arg_value):
        self.values[arg_name] = arg_value

    @classmethod
    def from_package(cls, s):
        """Factory for decoding network format string s to message"""
        try:
            (receiver_id, values) = cPickle.loads(s)
        except TypeError, e:
            print 's is length %s: %r' % (len(s), s)
            raise # "TypeError: expected string without null bytes" has been seen...
        return cls(receiver_id, **values)

    @classmethod
    def from_xml_package(cls, s):
        (rc, message, reciever_id, args) = lib_cpp.communication.rpc_xml_decode(s)
        return cls(reciever_id, **Message._8tou_dict(args))

    def to_xml_package(self):
        return lib_cpp.communication.rpc_xml_encode(self.receiver_id, Message._uto8_dict(self.values))

    @classmethod
    def _uto8(cls, value):
        if type(value) == types.UnicodeType:
            return value.encode('utf-8')
        if type(value) == type(set()):
            return cls._uto8_list(value)
        if type(value) == types.ListType:
            return cls._uto8_list(value)
        if type(value) == types.DictType:
            return cls._uto8_dict(value)
        else:
            return value

    @classmethod
    def _uto8_list(cls, values):
        result = []
        for element in values:
            result.append(cls._uto8(element))
        return result

    @classmethod
    def _uto8_dict(cls, values):
        result = {}
        for key in values:
            if type(key) == types.UnicodeType:
                result_key = key.encode('utf-8')
            elif type(key) == types.IntType:
                result_key = str(key)
            else:
                result_key = key
            result[result_key] = cls._uto8(values[key])
        return result

    @classmethod
    def _8tou(cls, value):
        if type(value) == types.StringType:
            return value.decode('utf-8')
        if type(value) == type(set()):
            return cls._8tou_list(value)
        if type(value) == types.ListType:
            return cls._8tou_list(value)
        if type(value) == types.DictType:
            return cls._8tou_dict(value)
        else:
            return value

    @classmethod
    def _8tou_list(cls, values):
        result = []
        for element in values:
            result.append(cls._8tou(element))
        return result

    @classmethod
    def _8tou_dict(cls, values):
        result = {}
        for key in values:
            if type(key) == types.UnicodeType:
                result_key = key.decode('utf-8')
            elif type(key) == types.IntType:
                result_key = str(key)
            else:
                result_key = key
            result[result_key] = cls._8tou(values[key])
        return result

            
        
