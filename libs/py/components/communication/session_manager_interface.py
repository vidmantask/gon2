"""
This file contains the interfaces to the session manager functionality. 
"""

class ISessionManagerEventhandler(object):
    """
    This interface defines the events that can be recived from a session manager.
    """
    def session_manager_session_created(self, connection_id, session):
        """
        Signals that a session has been created
        """
        raise NotImplementedError

    def session_manager_session_closed(self, session_id):
        """
        Signals that a session has been closed
        """
        raise NotImplementedError
    

class ISessionManagerServer(object):
    def start(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError
     