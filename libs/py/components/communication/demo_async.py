"""
This module demonstrate the use of the async_socket functionality combined
with tunnelendpoints.
The demo implements a proxy to www.dr.dk where some words are changed.
The proxy is shutdown from the server-sider  after 30 sec, to
illustrate closedown handling.

Threading:
By default each instance is guarded by a mutex(recursive mutex),
meaning that only one thread can operate on the instance at a time.

All callbacks from the instance are also guarded by the mutex,
this means that two collaborating instances eventually will end up in a deadlock situation.

In this demo the instance mutex is changed to use a session-mutex
using the set_mutex methods.
The session-mutex is available from a tunnelendpoint instance, and
have the effect that only one thread is operating on all instances
at a time.

"""
import time

import lib.checkpoint
import components.communication.async_socket as async_socket
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base
import components.communication.sim.sim_tunnel_endpoint as sim_tunnel_endpoint


class AsyncConnectionWithRemoteCallback(object):
    """
    Interface for AsyncConnectionWithRemote callback
    """
    def async_connection_with_remote_closed(self, id):
        pass


class AsyncConnectionWithRemote(tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch, async_socket.AsyncSocketTCPConnection):
    """
    Represent a socket connection, connected to a remote tunnelendpoint.

    In this demo it is used on both client and server side.
    """
    def __init__(self, async_service, checkpoint_handler, raw_connection, tunnel_endpoint, id, callback):
        async_socket.AsyncSocketTCPConnection.__init__(self, checkpoint_handler, raw_connection)
        tunnel_endpoint_base.TunnelendpointBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.id = id
        self.callback = callback
        self._remote_is_ready = False
        self.async_service = async_service

    def tunnelendpoint_connected(self):
        """
        Callback from TunnelendpointBaseWithMessageDispatch when connection between the two tunnel_endpoints has been established.
        """
        self.read_start()
        self._remote_is_ready = True

    def read(self, data_chunk):
        data_chunk = data_chunk.replace("Nyheder", "Nyheddder")
        data_chunk = data_chunk.replace("Sport", "Spooort")
        self.tunnelendpoint_remote('remote_read', data_chunk=data_chunk)
        self.read_start()

    def remote_read(self, data_chunk):
        self.write(data_chunk)

    def closed(self):
        if self._remote_is_ready:
            self.tunnelendpoint_remote('remote_closed')
        self.callback.async_connection_with_remote_closed(self.id)

    def remote_closed(self):
        self.close()

    def closed_eof(self):
        if self._remote_is_ready:
            self.tunnelendpoint_remote('remote_closed_eof')

    def remote_closed_eof(self):
        self.close_eof()

    def write_buffer_empty(self):
        pass


class AsyncProxyClient(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch, async_socket.AsyncSocketTCPAcceptor):
    """
    Represent the client side of the proxy.

    Listen on a ip:port and for each connection a new sub tunnelendpoint is created and a message is send to the server proxy.
    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, listen_ip, listen_port):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        async_socket.AsyncSocketTCPAcceptor.__init__(self, async_service, checkpoint_handler, (listen_ip, listen_port))
        async_socket.AsyncSocketTCPAcceptor.set_mutex(self, tunnel_endpoint.get_mutex())
        self._connection_ids_in_use = []
        self._connection_ids_not_in_use = range(1, 255)
        self._connections = {}
        self._running  = True

    def accepted(self, raw_connection):
        connection_id = self._connection_ids_not_in_use[0]
        self._connection_ids_in_use.append(connection_id)
        self._connection_ids_not_in_use.remove(connection_id)
        connection_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, connection_id)
        self._connections[connection_id] = AsyncConnectionWithRemote(self.async_service, self.checkpoint_handler, raw_connection, connection_tunnel_endpoint, connection_id, self)
        self._connections[connection_id].set_mutex(self.get_mutex())
        self.tunnelendpoint_remote('remote_accepted',  connection_id=connection_id)
        self.accept()
        return True

    def accept_failed(self, error_message):
        pass

    def async_connection_with_remote_closed(self, connection_id):
        self.remove_tunnelendpoint_tunnel(connection_id)
        del self._connections[connection_id]
        self._connection_ids_in_use.remove(connection_id)
        self._connection_ids_not_in_use.append(connection_id)

    def remote_close(self):
        self._running  = False
        self.close() # Acceptor, no more connections are accepted

    def is_done(self):
        return not self._running and len(self._connection_ids_in_use) == 0


class AsyncProxyServer(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch, async_socket.AsyncSocketTCPConnectorCallback):
    """
    Represent the server side of the proxy.

    On request from client proxy a connection (created using a connector) is established to ip:port.
    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint, connect_ip, connect_port):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self.connect_ip = connect_ip
        self.connect_port = connect_port
        self._connectors = {}
        self._connections = {}
        self._running = True

    def remote_accepted(self, connection_id):
        if self._running:
            self._connectors[connection_id] = async_socket.AsyncSocketTCPConnector(self.checkpoint_handler, self.async_service, self.connect_ip, self.connect_port, self, connection_id)
            self._connectors[connection_id].set_mutex(self.get_mutex())
            self._connectors[connection_id].connect()

    def async_socket_tcp_connector_connected(self, connection_id, raw_connection):
        del self._connectors[connection_id]
        connection_tunnel_endpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, connection_id)
        self._connections[connection_id] = AsyncConnectionWithRemote(self.async_service, self.checkpoint_handler, raw_connection, connection_tunnel_endpoint, connection_id, self)
        self._connections[connection_id].set_mutex(self.get_mutex())

    def async_socket_tcp_connect_failed(self, connection_id, message):
        del self._connectors[connection_id]

    def async_connection_with_remote_closed(self, connection_id):
        self.remove_tunnelendpoint_tunnel(connection_id)
        del self._connections[connection_id]

    def close(self, dummy_arch):
        self._running = False
        self.tunnelendpoint_remote('remote_close')
        for connection_id in self._connectors.keys():
            self._connectors[connection_id].close()
        for connection_id in self._connections.keys():
            self._connections[connection_id].close()

    def is_done(self):
        return not self._running and len(self._connectors) == 0 and len(self._connections) == 0


def main():
    checkpoint_handler_proxy = lib.checkpoint.CheckpointHandler.get_cout_all()
    checkpoint_handler_sim = checkpoint_handler_proxy

    sim_tunnel_endpoint_connector = sim_tunnel_endpoint.SimThread(checkpoint_handler_sim)
    sim_tunnel_endpoint_connector.start_and_wait_until_ready()

    tunnel_endpoint_client = sim_tunnel_endpoint_connector.get_tunnelendpoint_client()
    tunnel_endpoint_server = sim_tunnel_endpoint_connector.get_tunnelendpoint_server()

    async_service = sim_tunnel_endpoint_connector.async_service
    proxy_client = AsyncProxyClient(async_service, checkpoint_handler_proxy, tunnel_endpoint_client, '127.0.0.1', 8090)
    proxy_server = AsyncProxyServer(async_service, checkpoint_handler_proxy, tunnel_endpoint_server, 'www.dr.dk', 80)

    async_service.sleep_start_mutex(checkpoint_handler_proxy, proxy_server.get_mutex(), 0, 20, 0, proxy_server, 'close')
    proxy_client.accept()
    print "Listen on",  proxy_client.get_listen_info()

    while not (proxy_server.is_done() and proxy_client.is_done()):
        time.sleep(1)
    sim_tunnel_endpoint_connector.stop_and_waith_until_done()

if __name__ == '__main__':
    main()
