"""
Unitest of tcp-tunnelendpoint functionality
"""
import threading
import time
import sys

import lib.config
import unittest
from lib import giri_unittest
from lib import checkpoint 
from components.communication import session_manager
from components.communication import session
from components.communication import tunnel_endpoint
from components.communication import tunnel_endpoint_tcp

tunnelendpoint_create_policy = tunnel_endpoint.TunnelendpointCreatePolicy()

import components.communication.async_service
components.communication.async_service.init(checkpoint.CheckpointHandler.get_cout_all())

class ServerSession(session.APISessionEventhandlerReady):
    """
    Demo server session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, new_session, checkpoint_handler):
        session.APISessionEventhandlerReady.__init__(self)

        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerTEXT("/tmp/gon_portforward_server.log", False)
        self.checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)

        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)
        
        self.tunnelendpoint_ = tunnelendpoint_create_policy.create_tunnelendpoint_tunnel()
        self.portforward_web = tunnel_endpoint_tcp.TCPPortforwardServer(self.checkpoint_handler, self.session_, 'www.giritech.com', 80)
        self.portforward_rd = tunnel_endpoint_tcp.TCPPortforwardServer(self.checkpoint_handler, self.session_, 'x-men', 3389)
        self.portforward_ftp = tunnel_endpoint_tcp.TCPPortforwardServer(self.checkpoint_handler, self.session_, 'dev-test-gx270', 1080)
        self.portforward_gon_admin = tunnel_endpoint_tcp.TCPPortforwardServer(self.checkpoint_handler, self.session_, '127.0.0.1', 8080)

        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(1, self.portforward_web.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(2, self.portforward_rd.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(3, self.portforward_ftp.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(4, self.portforward_gon_admin.as_tunnelendpoint())

        
    def session_state_ready(self):
        self.session_.read_start_state_ready()
        
    def session_state_closed(self):
        print 'python ServerSession::session_state_closed'
    
    def session_state_key_exchange(self):
        print 'python ServerSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        print 'python ServerSession::session_read_continue_state_ready'
        return True


        

class ClientSession(session.APISessionEventhandlerReady, tunnel_endpoint_tcp.APITunnelendpointTCPTunnelClientEventhandler):
    """
    Demo client session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, new_session, checkpoint_handler):
        session.APISessionEventhandlerReady.__init__(self )
        tunnel_endpoint_tcp.APITunnelendpointTCPTunnelClientEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerTEXT("/tmp/gon_portforward_client.log", False)
        self.checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)

        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)

        self.tunnelendpoint_ = tunnelendpoint_create_policy.create_tunnelendpoint_tunnel()

        self.portforward_web = tunnel_endpoint_tcp.TCPPortforwardClient(self.checkpoint_handler, self.session_, '0.0.0.0', 8088)
        self.portforward_rd = tunnel_endpoint_tcp.TCPPortforwardClient(self.checkpoint_handler, self.session_, '0.0.0.0', 8089)
        self.portforward_ftp = tunnel_endpoint_tcp.TCPPortforwardClient(self.checkpoint_handler, self.session_, '0.0.0.0', 1080)
        self.portforward_gon_admin = tunnel_endpoint_tcp.TCPPortforwardClient(self.checkpoint_handler, self.session_, '0.0.0.0', 8090)

        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(1, self.portforward_web.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(2, self.portforward_rd.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(3, self.portforward_ftp.as_tunnelendpoint())
        self.tunnelendpoint_.add_tunnelendpoint_tunnel(4, self.portforward_gon_admin.as_tunnelendpoint())
                     
    def session_state_ready(self):
        print 'python ClientSession::session_state_ready'
        self.session_.read_start_state_ready()

    def session_state_closed(self):
        print 'python ClientSession::session_state_closed'
    
    def session_state_key_exchange(self):
        print 'python ClientSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        return True

    def tunnelendpoint_recieve(self, message):
        print 'python ClientSession::tunnelendpoint_recieve, message:', message


class SessionManagerServerEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self, checkpoint_handler):
        session_manager.APISessionManagerEventhandler.__init__(self)
        self.sessions_ = {}
        self.checkpoint_handler = checkpoint_handler
    
    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerServerEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.sessions_[new_session.get_session_id()] = ServerSession(new_session, self.checkpoint_handler)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerServerEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id

    def check(self, testcase):
        pass


class SessionManagerClientEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self, checkpoint_handler):
        session_manager.APISessionManagerEventhandler.__init__(self)
        self.checkpoint_handler = checkpoint_handler

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerClientEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.session_ = ClientSession(new_session, self.checkpoint_handler)
        
    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerClientEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id

    def check(self, testcase):
        pass
        


def server_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()
        
    except RuntimeError, value:
            checkpoint_handler.Checkpoint("main", "unittest_session_manager", checkpoint.ERROR, message= '%s' % value)


def client_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()
    
    except RuntimeError, value:
        checkpoint_handler.Checkpoint("main", "unittest_session_manager", checkpoint.ERROR, message= '%s' % value)
   



class SessionManagerTester(unittest.TestCase):

    def test_client_server_connect(self):
        known_secret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        known_secret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

        checkpoint_filter = checkpoint.CheckpointFilter_true()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerTEXT()
        checkpoint_handler_ignore = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)

        
        checkpoint_filter = checkpoint.CheckpointFilter_false()
        checkpoint_output_handler = checkpoint.CheckpointOutputHandlerTEXT()
        checkpoint_handler = checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)

        session_manager_server_eventhandler = SessionManagerServerEventhandler(checkpoint_handler_ignore)
        session_manager_client_eventhandler = SessionManagerClientEventhandler(checkpoint_handler_ignore)
    
        session_manager_server = session_manager.APISessionManagerServer_create(session_manager_server_eventhandler, 
                                                                                checkpoint_handler.checkpoint_handler, 
                                                                                known_secret_server, 
                                                                                "127.0.0.1", 
                                                                                8014,
                                                                                1)
        session_manager_client = session_manager.APISessionManagerClient_create(session_manager_client_eventhandler, 
                                                                                checkpoint_handler.checkpoint_handler, 
                                                                                known_secret_client)

        session_manager_server_thread = threading.Thread(target=server_session_manager_thread_start, args=(checkpoint_handler, session_manager_server))
        session_manager_server_thread.start()

        session_manager_client.set_servers(lib.config.parse_servers(checkpoint_handler, "127.0.0.1, 8014"))
        session_manager_client_thread = threading.Thread(target=client_session_manager_thread_start, args=(checkpoint_handler, session_manager_client))
        session_manager_client_thread.start()

        giri_unittest.wait_until_with_timeout(session_manager_client.is_closed, 1400000)
        session_manager_server_eventhandler.check(self)
        session_manager_client_eventhandler.check(self)
        session_manager_server.close()

        giri_unittest.wait_until_with_timeout(session_manager_server.is_closed, 20000)

 
if __name__ == '__main__':
    gut_py = giri_unittest.CMakeUnittest()

    if gut_py.do_run():
        unittest.main()    
