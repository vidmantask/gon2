"""
Demo of session manager
"""
import threading
import thread
import time
import sys


import lib_cpp.communication
import lib.config
import unittest
from lib import giri_unittest
from components.communication import session_manager
from components.communication import session
from components.communication import tunnel_endpoint_base


class ServerSession(session.APISessionEventhandlerReady, tunnel_endpoint_base.APITunnelendpointEventhandler):
    """
    Demo server session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, async_service, checkpoint_handler, new_session):
        session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        tunnel_endpoint_base.APITunnelendpointEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, checkpoint_handler)
        self.tunnelendpoint_.set_eventhandler(self)
        self.tunnelendpoint_child_01_ = tunnel_endpoint_base.create_tunnelendpoint(async_service, checkpoint_handler)
        self.tunnelendpoint_child_01_.set_eventhandler(self)
        self.tunnelendpoint_child_02_ = tunnel_endpoint_base.create_tunnelendpoint(async_service, checkpoint_handler)
        self.tunnelendpoint_child_02_.set_eventhandler(self)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint(1, self.tunnelendpoint_child_01_)
        self.tunnelendpoint_.add_tunnelendpoint(2, self.tunnelendpoint_child_02_)
        self.got_ping = False
        self.send_pong = False
        self.got_pings = 0

    def session_state_ready(self):
        print 'python ServerSession::session_state_ready'
        self.session_.read_start_state_ready()

    def session_state_closed(self):
        self.tunnelendpoint_.remove_tunnelendpoint(1)
        self.tunnelendpoint_.remove_tunnelendpoint(2)
        print 'python ServerSession::session_state_closed'

    def session_state_key_exchange(self):
        print 'python ServerSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        print 'python ServerSession::session_read_continue_state_ready'
        return True

    def tunnelendpoint_recieve(self, message):
        print 'python ServerSession::tunnelendpoint_recieve', message
        if(message == 'ping'):
            self.got_ping = True
            self.got_pings += 1
            self.tunnelendpoint_.tunnelendpoint_send("pong")
            self.tunnelendpoint_child_01_.tunnelendpoint_send("pong")
            self.tunnelendpoint_child_02_.tunnelendpoint_send("pong")
            self.send_pong = True

    def tunnelendpoint_connected(self):
        pass


class ClientSession(session.APISessionEventhandlerReady, tunnel_endpoint_base.APITunnelendpointEventhandler):
    """
    Demo client session with one tunnelendpoint having twa child tunnelendpoints
    """
    def __init__(self, async_service, checkpoint_handler, new_session):
        session.APISessionEventhandlerReady.__init__(self, checkpoint_handler.checkpoint_handler)
        tunnel_endpoint_base.APITunnelendpointEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.session_ = new_session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = tunnel_endpoint_base.create_tunnelendpoint_tunnel(async_service, checkpoint_handler)
        self.tunnelendpoint_.set_eventhandler(self)
        self.tunnelendpoint_child_01_ = tunnel_endpoint_base.create_tunnelendpoint(async_service, checkpoint_handler)
        self.tunnelendpoint_child_01_.set_eventhandler(self)
        self.tunnelendpoint_child_02_ = tunnel_endpoint_base.create_tunnelendpoint(async_service, checkpoint_handler)
        self.tunnelendpoint_child_02_.set_eventhandler(self)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint(1, self.tunnelendpoint_child_01_)
        self.tunnelendpoint_.add_tunnelendpoint(2, self.tunnelendpoint_child_02_)
        self.send_ping = False
        self.got_pongs = 0
        self._ready = False
        self._connected = False

    def session_state_ready(self):
        print 'python ClientSession::session_state_ready'
        self._ready = True
        self._connected_and_ready()

    def session_state_closed(self):
        print 'python ClientSession::session_state_closed'

    def session_state_key_exchange(self):
        print 'python ClientSession::session_state_key_exchange'

    def session_read_continue_state_ready(self):
        return True

    def tunnelendpoint_recieve(self, message):
        print 'python ClientSession::tunnelendpoint_recieve, message:', message
        if(message == 'pong'):
            self.got_pongs += 1
            if(self.got_pongs == 3):
                self.tunnelendpoint_.remove_tunnelendpoint(1)
                self.tunnelendpoint_.remove_tunnelendpoint(2)
                self.session_.close(False)

    def tunnelendpoint_connected(self):
        self._connected = True
        self._connected_and_ready()

    def _connected_and_ready(self):
        if not self._ready:
            return
        if not self._connected:
            return

        self.session_.read_start_state_ready()
        self.tunnelendpoint_.tunnelendpoint_send("ping")
        self.send_ping = True


class SessionManagerServerEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self, async_service, checkpoint_handler):
        session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.sessions_ = {}
        self.session_manager = None
        self._closed = False
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service

    def session_manager_failed(self, message):
        print "session_manager_failed", message

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerServerEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.sessions_[new_session.get_session_id()] = ServerSession(self.async_service, self.checkpoint_handler, new_session)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerServerEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id
        self.session_manager.close_start()

    def session_manager_closed(self):
        print 'python SessionManagerServerEventhandler::session_manager_closed'
        self._closed = True

    def is_closed(self):
        return self._closed

    def check(self, testcase):
        for session_id in self.sessions_.keys():
            testcase.assertEqual(self.sessions_[session_id].got_ping, True)


class SessionManagerClientEventhandler(session_manager.APISessionManagerEventhandler):
    def __init__(self, async_service, checkpoint_handler):
        session_manager.APISessionManagerEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.session_manager = None
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_failed(self, message):
        print "session_manager_failed", message

    def session_manager_connecting_failed(self, connection_id):
        pass # signal ignored for now

    def session_manager_connecting(self, connection_id, connection_title):
        pass # signal ignored for now

    def session_manager_session_created(self, connection_id, new_session):
        print 'python SessionManagerClientEventhandler::session_manager_session_created'
        print 'session_id:' , new_session.get_session_id()
        self.session_ = ClientSession(self.async_service, self.checkpoint_handler, new_session)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerClientEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id
        self.session_manager.close_start()

    def session_manager_closed(self):
        print 'python SessionManagerClientEventhandler::session_manager_closed'

    def check(self, testcase):
        testcase.assertEqual(self.session_.got_pongs, 3)




class SessionManagerTester(unittest.TestCase):

    def test_client_server_connect(self):
        known_secret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
        known_secret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

        session_manager_server_eventhandler = SessionManagerServerEventhandler(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler())
        session_manager_client_eventhandler = SessionManagerClientEventhandler(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler())

        session_manager_server = session_manager.APISessionManagerServer_create(giri_unittest.get_async_service().get_com_async_service(),
                                                                                session_manager_server_eventhandler,
                                                                                giri_unittest.get_checkpoint_handler().checkpoint_handler,
                                                                                known_secret_server,
                                                                                "127.0.0.1",
                                                                                8045,
                                                                                "1")
        session_manager_server.set_option_reuse_address(True)
        session_manager_server_eventhandler.session_manager = session_manager_server

        session_manager_server.start()

        session_manager_client = session_manager.APISessionManagerClient_create(giri_unittest.get_async_service().get_com_async_service(),
                                                                                session_manager_client_eventhandler,
                                                                                giri_unittest.get_checkpoint_handler().checkpoint_handler,
                                                                                known_secret_client,
                                                                                lib_cpp.communication.ApplProtocolType.PYTHON)
        session_manager_client_eventhandler.session_manager = session_manager_client

        session_manager_client.set_servers(lib.config.parse_servers(giri_unittest.get_checkpoint_handler(), "127.0.0.1, 8045"))
        session_manager_client.start()

        giri_unittest.wait_until_with_timeout(session_manager_client.is_closed, 2000)
        time.sleep(5)

        session_manager_server_eventhandler.check(self)
        session_manager_client_eventhandler.check(self)
        giri_unittest.wait_until_with_timeout(session_manager_server_eventhandler.is_closed, 3000)

        giri_unittest.get_async_service().stop_and_wait()


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
#GIRI_UNITTEST_IGNORE = False

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
