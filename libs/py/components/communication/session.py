"""
This file contains the interfaces to the session functionality. 
"""
import lib_cpp.communication

#
# API implementation of the interface ISessionEventhandlerReady
#
APISessionEventhandlerReady = lib_cpp.communication.APISessionEventhandlerReady

