"""
This file contains the interfaces to the async session functionality.

Threading:
By default each instance is guarded by a mutex(recursive mutex), meaning that only one thread can operate on the instance at a time.

All callbacks from the instance are also guarded by the mutex, this means that two collaborating instances eventually will end up in a deadlock situation.
There are two solutions for this:
* Let the two instance use the same mutex, using get_mutex/set_mutex
* Manualy separating the collaboration with thread aware code

"""
import lib.encode
import lib_cpp.communication


class AsyncSocketTCPConnection(lib_cpp.communication.APIRawTunnelendpointEventhandler):

    # Memory leak testing
#    COUNT = 0
#    OBJECTS = set()
    """
    Async TCP connection using async_service. Instances are created by Connector and Acceptor.
    """
    def __init__(self, checkpoint_handler, raw_connection=None):
        lib_cpp.communication.APIRawTunnelendpointEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.set_raw_connection(raw_connection)
        self.checkpoint_handler = checkpoint_handler
        # Memory leak testing
#        AsyncSocketTCPConnection.COUNT += 1
#        print "__init__ AsyncSocketTCPConnection %d, (%s,%s)" % (AsyncSocketTCPConnection.COUNT, self.__class__, self.__hash__())

    def __del__(self):
        # Memory leak testing
#        AsyncSocketTCPConnection.COUNT -= 1
#        print "__del__ AsyncSocketTCPConnection %d, (%s,%s)" % (AsyncSocketTCPConnection.COUNT, self.__class__, self.__hash__())
        if self._raw_connection is not None:
            self._raw_connection.reset_tcp_eventhandler()
            self._raw_connection.aio_close_start(True)
            #print "reset AsyncSocketTCPConnection"
        
    def set_raw_connection(self, raw_connection):
        if raw_connection is not None:
            self._raw_connection = raw_connection
            self._raw_connection.set_tcp_eventhandler(self)

    def com_raw_tunnelendpoint_close(self, connection_id):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self._raw_connection.reset_tcp_eventhandler()
        self.closed()

    def com_raw_tunnelendpoint_close_eof(self, connection_id):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.closed_eof()

    def com_raw_tunnelendpoint_read(self, connection_id, data):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.read(data)

    def com_raw_tunnelendpoint_write_buffer_empty(self, connection_id):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.write_buffer_empty()

    def get_remote(self):
        """
        Get remote-host and remote-port when connected
        """
        return self._raw_connection.get_ip_remote()

    def get_local(self):
        """
        Get local-host and local-port when connected
        """
        return self._raw_connection.get_ip_local()

    def read_start(self):
        """
        Start a async read-operation. When data is read, the it delevered through a callback to "read".
        """
        self._raw_connection.aio_read_start()

    def read(self, data_chunk):
        """
        A data_chunk has been read.
        This method is intented for overwrite.
        """
        raise NotImplementedError

    def write(self, data_chunk, start_ssl_host=None, disable_certificate_verification=False):
        if start_ssl_host:
            self.ssl_start(start_ssl_host, disable_certificate_verification)
            return
        """
        Start a async write operation.
        """
        if data_chunk:
            self._raw_connection.aio_write_start(data_chunk)
        else:   
            self._raw_connection.aio_close_write_start()

    def write_buffer_empty(self):
        """
        Called as when the write buffer is empty.
        This method is intented for overwrite.
        """
        raise NotImplementedError

    def close(self):
        """
        Start a async close operation, when close is done "closed" is called.
        """
        self._raw_connection.aio_close_start(False)

    def closed(self):
        """
        Called as a response to 'close' when the connection has been closed.
        This method is intented for overwrite.
        """
        raise NotImplementedError

    def close_eof(self):
        """
        Indicate that no more data a written to the connection.
        """
        self._raw_connection.aio_close_write_start()

    def closed_eof(self):
        """
        Called when the remote indicate that no-more data are comming.
        This method is intented for overwrite.
        """
        raise NotImplementedError

    def set_mutex(self, mutex):
        """
        By default every operation (and  callback) are  guarded by a instance mutex.
        This  method chagnge the used mutex.
        """
        self._raw_connection.set_mutex(mutex)

    def get_mutex(self):
        return self._raw_connection.get_mutex()

    def ssl_start(self, hostname, disable_certificate_verification=False):
        """
        Start ssl handshake on existing connection.
        """
        return self._raw_connection.aio_ssl_start(hostname, disable_certificate_verification)


class AsyncSocketTCPConnectorCallback(object):
    def async_socket_tcp_connector_connected(self, connection_id, raw_connection):
        raise NotImplementedError

    def async_socket_tcp_connect_failed(self, connection_id, message):
        raise NotImplementedError

    def async_socket_tcp_closed(self):
        raise NotImplementedError


class AsyncSocketTCPConnector(lib_cpp.communication.APIRawTunnelendpointConnectorTCPEventhandler):
    """
    Async TCP connector using async_service.
    """
    
    # Memory leak testing
#    COUNT = 0
#    OBJECTS = set()
    
    def __init__(self, checkpoint_handler, async_service, remote_ip, remote_port, callback=None, callback_param=None):
        lib_cpp.communication.APIRawTunnelendpointConnectorTCPEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self._checkpoint_handler = checkpoint_handler
        self._remote_ip = remote_ip
        self._remote_port = remote_port
        self._callback = callback
        self._callback_param = callback_param
        self._raw_tunnelendpoint_tcp_connector = lib_cpp.communication.APIRawTunnelendpointConnectorTCP_create(checkpoint_handler.checkpoint_handler, async_service.get_com_async_service(), self._remote_ip, self._remote_port)
        self._raw_tunnelendpoint_tcp_connector.set_tcp_eventhandler(self)

        # Memory leak testing
#        AsyncSocketTCPConnector.COUNT += 1
#        print "__init__ AsyncSocketTCPConnector %d, (%s,%s)" % (AsyncSocketTCPConnector.COUNT, self.__class__, self.__hash__())

    def __del__(self):
        # Memory leak testing
#        AsyncSocketTCPConnector.COUNT -= 1
#        print "__del__ AsyncSocketTCPConnector %d, (%s,%s)" % (AsyncSocketTCPConnector.COUNT, self.__class__, self.__hash__())

        if self._raw_tunnelendpoint_tcp_connector is not None:
            self._raw_tunnelendpoint_tcp_connector.reset_tcp_eventhandler()
            self._raw_tunnelendpoint_tcp_connector.close()
            #print "reset AsyncSocketTCPConnector"

    def set_connect_timeout_sec(self, sec):
        """
        Set connect timeout in seconds. 
        
        In case of timeout connect_failed is called
        """
        self._raw_tunnelendpoint_tcp_connector.set_connect_timeout_sec(sec)

    def com_tunnelendpoint_connected(self, raw_connection):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.connected(raw_connection)

    def com_tunnelendpoint_connection_failed_to_connect(self, message):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.connect_failed(message)

    def connect(self):
        """
        Start async connect
        """
        self._raw_tunnelendpoint_tcp_connector.aio_connect_start()

    def connected(self, raw_connection):
        """
        This method is called when we have a valid connection to the remote endpoint.
        This method is intented for overwrite.
        """
        if self._callback is not None:
            self._callback.async_socket_tcp_connector_connected(self._callback_param, raw_connection)

    def connect_failed(self, message):
        """
        This method is called if we conuld not connect to remote endpoint.
        This method is intented for overwrite.
        """
        if self._callback is not None:
            self._callback.async_socket_tcp_connect_failed(self._callback_param, message)

    def close(self):
        self._raw_tunnelendpoint_tcp_connector.close()
        self._raw_tunnelendpoint_tcp_connector.reset_tcp_eventhandler()
        if self._callback:
            work_callback = self._callback 
            self._callback = None
            self._callback_param = None
            work_callback.async_socket_tcp_closed()

    def close_no_callback(self):
        self._raw_tunnelendpoint_tcp_connector.close()
        self._raw_tunnelendpoint_tcp_connector.reset_tcp_eventhandler()
        self._callback = None
        self._callback_param = None

    def set_mutex(self, mutex):
        """
        By default every operation (and callback) are guarded by a instance mutex.
        This method changes the used mutex.
        """
        self._raw_tunnelendpoint_tcp_connector.set_mutex(mutex)

    def get_mutex(self):
        return self._raw_tunnelendpoint_tcp_connector.get_mutex()

    def enable_ssl(self, disable_certificate_verification=False):
        self._raw_tunnelendpoint_tcp_connector.enable_ssl(disable_certificate_verification)
        

class AsyncSocketTCPAcceptor(lib_cpp.communication.APIRawTunnelendpointAcceptorTCPEventhandler):
    """
    Async TCP acceptor using async_service.
    """
    
    # Memory leak testing
#    COUNT = 0
#    OBJECTS = set()
    
    def __init__(self, async_service, checkpoint_handler, (local_host, local_port), reuse=None):
        lib_cpp.communication.APIRawTunnelendpointAcceptorTCPEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self._checkpoint_handler = checkpoint_handler
        self._local_host = local_host
        self._local_port = local_port
        self._raw_tunnelendpoint_tcp_acceptor = lib_cpp.communication.APIRawTunnelendpointAcceptorTCP_create(checkpoint_handler.checkpoint_handler, async_service.get_com_async_service(), self._local_host, self._local_port)
        self._raw_tunnelendpoint_tcp_acceptor.set_tcp_eventhandler(self)
        if reuse is not None:
            self._raw_tunnelendpoint_tcp_acceptor.set_option_reuse_address(reuse)
        # Memory leak testing
#        AsyncSocketTCPAcceptor.COUNT += 1
#        print "__init__ AsyncSocketTCPAcceptor %d, (%s,%s)" % (AsyncSocketTCPAcceptor.COUNT, self.__class__, self.__hash__())

    def __del__(self):
        # Memory leak testing
#        AsyncSocketTCPAcceptor.COUNT -= 1
#        print "__del__ AsyncSocketTCPAcceptor %d, (%s,%s)" % (AsyncSocketTCPAcceptor.COUNT, self.__class__, self.__hash__())
        if self._raw_tunnelendpoint_tcp_acceptor is not None:
            self._raw_tunnelendpoint_tcp_acceptor.reset_tcp_eventhandler()
            self._raw_tunnelendpoint_tcp_acceptor.aio_close_start()
            #print "reset AsyncSocketTCPAcceptor"

    def com_tunnelendpoint_accepted(self, raw_connection):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        if not self.accepted(raw_connection):
            raw_connection.aio_close_start(True)

    def com_tunnelendpoint_acceptor_error(self, error_message):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        self.accept_failed(error_message.decode(lib.encode.get_current_encoding(), 'ignore'))

    def com_accept_continue(self):
        """
        Implementation of eventhandler, internal method don't overwrite.
        """
        return False

    def accept(self):
        """
        Start async accept of a connection, could be more that one connection if accept_contunue is returning True.
        """
        self._raw_tunnelendpoint_tcp_acceptor.aio_accept_start()

    def accepted(self, connection):
        """
        This method is called when we have a valid connection to the remote endpoint.
        This method is intented for overwrite.
        
        Expected to return true if the connection is used, otherwise the connection 
        will be closed.
        """
        raise NotImplementedError

    def accept_failed(self, error_message):
        """
        This method is called when we failed to accept. E.g. could be if local endpoint is already used.
        This method is intented for overwrite.
        """
        raise NotImplementedError

    def close(self):
        """
        Shutdown of the acceptor.
        """
        self._raw_tunnelendpoint_tcp_acceptor.aio_close_start()

    def set_mutex(self, mutex):
        """
        By default every operation (and  callback) are  guarded by a instance mutex.
        This  method chagnge the used mutex.
        """
        self._raw_tunnelendpoint_tcp_acceptor.set_mutex(mutex)

    def get_mutex(self):
        return self._raw_tunnelendpoint_tcp_acceptor.get_mutex()

    def get_listen_info(self):
        """
        Get the ip and port that the acceptor curretnliy  is listen on.
        In no info is available the tupple ("", 0) returned
        """
        return self._raw_tunnelendpoint_tcp_acceptor.get_ip_local()
