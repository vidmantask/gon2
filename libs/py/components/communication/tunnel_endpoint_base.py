"""
This file contains base classes for the tunnel endpoint functionality.
"""
import sys
import datetime

import cProfile

import lib.version
import lib.checkpoint
import thread

import lib_cpp.communication

APITunnelendpointEventhandler = lib_cpp.communication.APITunnelendpointEventhandler

from components.communication import tunnel_endpoint_interface
from components.communication import message

MODULE_ID = 'tunnel_endpoint'

class VersionError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class VersionErrorRemote(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class Atest(object):
    
    def __init__(self):
        pass


class TunnelendpointBase(APITunnelendpointEventhandler):
    """
    A tunnel endpoint implementing the ITunnelendpointEventhandler.
    """
    USER_SIGNAL_VERSION_ERROR = 0
    USER_SIGNAL_VERSION_ERROR_REMOTE = 1
    USER_SIGNAL_DISPATCH_ERROR = 2

    # Memory leak testing
#    COUNT = 0
#    OBJECTS = set()

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint=None):
        APITunnelendpointEventhandler.__init__(self, checkpoint_handler.checkpoint_handler)
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self._tunnel_endpoint = None
        self._mutex = None
        if tunnel_endpoint is not None:
            self.set_tunnelendpoint(tunnel_endpoint)

        # Memory leak testing
#        TunnelendpointBase.COUNT += 1
#        print "__init__ TunnelendpointBase %d, (%s,%s)" % (TunnelendpointBase.COUNT, self.__class__, self.__hash__())
#        TunnelendpointBase.OBJECTS.add("%s.%s" % (self.__class__, self.__hash__())) 

    def __del__(self):
        # Memory leak testing
#        TunnelendpointBase.COUNT -= 1
#        print "__del__ TunnelendpointBase %d, (%s,%s)" % (TunnelendpointBase.COUNT, self.__class__, self.__hash__())
#        obj = "%s.%s" % (self.__class__, self.__hash__())
#        if obj in TunnelendpointBase.OBJECTS:
#            TunnelendpointBase.OBJECTS.remove(obj)
#            for o in TunnelendpointBase.OBJECTS:
#                print o 
#        else:
#            print "Object %s unknown" % obj

        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.reset_eventhandler()
            self._tunnel_endpoint.close()

    def set_tunnelendpoint(self, tunnel_endpoint):
        self._tunnel_endpoint = tunnel_endpoint
        self._tunnel_endpoint.set_eventhandler(self)
        self._mutex = self._tunnel_endpoint.get_mutex()

    def reset_tunnelendpoint(self):
        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.reset_eventhandler()
        self._tunnel_endpoint = None

    def tunnelendpoint_recieve(self, package):
        pass

    def tunnelendpoint_send(self, package):
        self._tunnel_endpoint.tunnelendpoint_send(package)

    def tunnelendpoint_connected(self):
        """
        Callback from tunnelendpoint framework telling that the two tunnelendpoints are connected
        """
        pass

    def tunnelendpoint_recieve_user_signal(self, child_id, signale_id, message):
        pass

    def get_version(self):
        return lib.version.Version.create_from_encode(self._tunnel_endpoint.get_version())

    def get_version_remote(self):
        return lib.version.Version.create_from_encode(self._tunnel_endpoint.get_version_remote())

    def set_version(self, version):
        self._tunnel_endpoint.set_version(version.encode())

    def get_mutex(self):
        return self._mutex

    def is_connected(self):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.is_connected()
        return False

    def get_appl_protocol_type(self):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.get_appl_protocol_type()
        return None

    def get_tc_read_delay_ms(self):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.get_tc_read_delay_ms()
        return 100
        
    def get_tc_read_size(self):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.get_tc_read_size()
        return 15000

    def tc_report_read_begin(self):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.tc_report_read_begin()
        
    def tc_report_read_end(self, size):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.tc_report_read_end(size)

    def tc_report_push_buffer_size(self, size):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.tc_report_push_buffer_size(size)
    
    def tc_report_pop_buffer_size(self, size):
        if self._tunnel_endpoint is not None:
            return self._tunnel_endpoint.tc_report_pop_buffer_size(size)


class TunnelendpointBaseWithMessageDispatch(TunnelendpointBase):
    """
    A tunnel endpoint for sending messages and dispatch messages to method invocation.
    """
    VERSION_CHECK_NONE = 0
    VERSION_CHECK_THROW_WHEN_NOT_EQUAL = 1

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint = None, version_check=None):
        TunnelendpointBase.__init__(self, async_service, checkpoint_handler, tunnel_endpoint)
        self._package_buffer = []
        self._version_check = version_check
        if self._version_check is None:
            self._version_check = TunnelendpointBaseWithMessageDispatch.VERSION_CHECK_THROW_WHEN_NOT_EQUAL

    def tunnelendpoint_recieve(self, package):
        try:
            if self.get_appl_protocol_type() == lib_cpp.communication.ApplProtocolType.XML:
                msg = message.Message.from_xml_package(package)
            else:
                msg = message.Message.from_package(package)
            msg.call_receiver(self)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("dispatch.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            if self._tunnel_endpoint is not None:
                self._tunnel_endpoint.tunnelendpoint_user_signal(TunnelendpointBase.USER_SIGNAL_DISPATCH_ERROR, "unexpected error")

    def tunnelendpoint_send(self, msg):
        if self.get_appl_protocol_type() == lib_cpp.communication.ApplProtocolType.XML:
            package = msg.to_xml_package()
        else:
            package = msg.to_package()
        TunnelendpointBase.tunnelendpoint_send(self, package)

    def tunnelendpoint_remote_decoupled(self, async_service, receiver_id, **values):
        async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_tunnelendpoint_remote_decoupled', (receiver_id, values))

    def _tunnelendpoint_remote_decoupled(self, (receiver_id, values)):
        self.tunnelendpoint_remote(receiver_id, **values)

    def tunnelendpoint_remote(self, receiver_id, **values):
        """Remote invocation of method - like self.tunnelendpoint_send but encapsulates message"""
        msg = message.Message(receiver_id, **values)
        self.tunnelendpoint_send(msg)

    def tunnelendpoint_connected(self):
        self.check_version()

    def trick_check_version_failed(self, error_message):
        self.checkpoint_handler.Checkpoint("trick_check_version_failed", MODULE_ID, lib.checkpoint.DEBUG, error_message=error_message)
        self.tunnelendpoint_remote('_remote_check_version_failed', error_message=error_message)
        self._tunnel_endpoint.tunnelendpoint_user_signal(TunnelendpointBase.USER_SIGNAL_VERSION_ERROR, error_message)

    def check_version(self):
#        version check is moved to cmp_endpoint_manager_session as of 5.5
        pass
    
    def _remote_check_version_failed(self, error_message):
        self.checkpoint_handler.Checkpoint("remote_check_version_failed", MODULE_ID, lib.checkpoint.DEBUG, error_message=error_message)
        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.tunnelendpoint_user_signal(TunnelendpointBase.USER_SIGNAL_VERSION_ERROR_REMOTE, error_message)


class TunnelendpointTunnelBaseWithMessageDispatch(TunnelendpointBaseWithMessageDispatch, tunnel_endpoint_interface.ITunnelendpointFactory):
    """
    A tunnel endpoint for sending messages and dispatch messages to method invocation.
    The tunnel endpoint can act as parent of child tunnel endpoints.
    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel=None, version_check=None):
        TunnelendpointBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, version_check)

    def create_and_add_tunnelendpoint_tunnel(self, checkpoint_handler, child_id):
        new_tunnel_endpoint = create_tunnelendpoint_tunnel(self.async_service, checkpoint_handler)
        self._tunnel_endpoint.add_tunnelendpoint_tunnel(child_id, new_tunnel_endpoint)
        return new_tunnel_endpoint

    def add_tunnelendpoint(self, child_id, new_tunnel_endpoint):
        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.add_tunnelendpoint_tunnel(child_id, new_tunnel_endpoint)

    def close_tunnelendpoint_tunnel(self, child_id):
        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.remove_tunnelendpoint(child_id)

    def remove_tunnelendpoint_tunnel(self, child_id):
        if self._tunnel_endpoint is not None:
            self._tunnel_endpoint.remove_tunnelendpoint(child_id)

    def tunnelendpoint_recieve_user_signal(self, child_id, signale_id, message):
        self.tunnelendpoint_user_signal(signale_id, message)

    def tunnelendpoint_user_signal(self, signale_id, message):
        pass

    @classmethod
    def create(cls, async_service, checkpoint_handler, version_check=None):
        tunnel_endpoint = create_tunnelendpoint_tunnel(async_service, checkpoint_handler)
        return TunnelendpointTunnelBaseWithMessageDispatch(async_service, checkpoint_handler, tunnel_endpoint, version_check)

    def mutex_method(self, method_name, args=None):
        '''schedule a call of something like self.method_name(*args) on worker thread with mutex'''
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, method_name, args)

class TunnelendpointSession(TunnelendpointTunnelBaseWithMessageDispatch):
    """
    This class is entented to be used a base class for session tunnelendpoints that needs to cordinate the
    connection of the tunneelendpoint with a session_start from the owner of the session.
    """
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel=None, version_check=None):
        TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, version_check)
        self._session_start = False
        self._session_connected = False

    def session_start(self):
        self._session_start = True
        if self._session_connected:
            self.session_start_and_connected()

    def session_close(self):
        pass

    def tunnelendpoint_connected(self):
        self._session_connected = True
        if self._session_start:
            self.session_start_and_connected()

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """
        raise NotImplementedError()




def create_tunnelendpoint(async_service, checkpoint_handler):
    return lib_cpp.communication.APITunnelendpointReliableCrypted_create(async_service.get_com_async_service(), checkpoint_handler.checkpoint_handler)

def create_tunnelendpoint_tunnel(async_service, checkpoint_handler):
    return  lib_cpp.communication.APITunnelendpointReliableCryptedTunnel_create(async_service.get_com_async_service(), checkpoint_handler.checkpoint_handler)



class TunnelendpointSessionStub(object):
    def __init__(self,  async_service):
        self.async_service = async_service
        self._mutex = self.async_service.create_mutex('TunnelendpointSessionStub')

    def get_mutex(self):
        return self._mutex

    def set_eventhandler(self, callback):
        pass

    def add_tunnelendpoint_tunnel(self, child_id, new_tunnel_endpoint):
        pass
