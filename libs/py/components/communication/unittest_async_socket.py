"""
Unittest of async_socket module
"""
import unittest
import time

from lib import giri_unittest
from lib import checkpoint
import components.communication.async_service
from components.communication import async_socket


class MyAsyncServerConnection(async_socket.AsyncSocketTCPConnection):
    def __init__(self, checkpoint_handler, raw_connection, delay_write_sec=0):
        async_socket.AsyncSocketTCPConnection.__init__(self, checkpoint_handler, raw_connection)
        self.delay_write_sec = delay_write_sec
        self._done = False

    def read(self, data_chunk):
        print "MyAsyncConnection::read", data_chunk

        if self.delay_write_sec > 0:
            print "MyAsyncConnection:sleeping"
            time.sleep(self.delay_write_sec)
            print "MyAsyncConnection:sleeping_done"

        self.write("xxxxx %s xxxxx" % data_chunk)
        self.read_start()

    def closed(self):
        print "MyAsyncConnection::closed"
        self._done = True

    def closed_eof(self):
        print "MyAsyncConnection::closed_eof"

    def write_buffer_empty(self):
        print "MyAsyncConnection::write_buffer_empty"

    def is_done(self):
        return self._done


class MyAsyncServer(async_socket.AsyncSocketTCPAcceptor):
    def __init__(self, checkpoint_handler, async_service, local, num_of_connections, delay_write_sec):
        async_socket.AsyncSocketTCPAcceptor.__init__(self, async_service, checkpoint_handler, local)
        self._connections = []
        self.num_of_connections = num_of_connections
        self.checkpoint_handler = checkpoint_handler
        self.delay_write_sec = delay_write_sec
        self.accept()

    def accepted(self, raw_connection):
        connection =  MyAsyncServerConnection(self.checkpoint_handler, raw_connection, self.delay_write_sec)
        print "MyAsyncServer::accepted", connection.get_local(), connection.get_remote()
        self._connections.append(connection)
        connection.read_start()
        if len(self._connections) < self.num_of_connections:
            self.accept()
        return True

    def accept_failed(self, error_message):
        print "MyAsyncServer::accept_failed"

    def is_done(self):
        for connection in self._connections:
            if not connection.is_done():
                return False
        return True

    def close_and_wait(self):
        for connection in self._connections:
            connection.close()
        while not self.is_done():
            time.sleep(1)



class MyAsyncClientConnection(async_socket.AsyncSocketTCPConnection):
    def __init__(self, checkpoint_handler, raw_connection, ):
        async_socket.AsyncSocketTCPConnection.__init__(self, checkpoint_handler, raw_connection)
        self._done = False

    def read(self, data_chunk):
        print "MyAsyncClientConnection::read", data_chunk
        self.close()

    def closed(self):
        print "MyAsyncClientConnection::closed"
        self._done = True

    def closed_eof(self):
        print "MyAsyncClientConnection::closed_eof"

    def is_done(self):
        return self._done

    def write_buffer_empty(self):
        print "MyAsyncClientConnection::write_buffer_empty"


class MyAsyncClient(async_socket.AsyncSocketTCPConnector):
    def __init__(self, checkpoint_handler, async_service, (remote_ip, remote_port)):
        async_socket.AsyncSocketTCPConnector.__init__(self, checkpoint_handler, async_service, remote_ip, remote_port)
        self._done = False
        self.connection = None
        print "MyAsyncClient.__init__.connect"
        self.set_connect_timeout_sec(10)
        self.connect()
        print "MyAsyncClient.__init__.connect.done"
        self._connect_failed = False
        self.checkpoint_handler = checkpoint_handler
        self.is_connected = False

    def connected(self, raw_connection):
        self.connection = MyAsyncClientConnection(self.checkpoint_handler, raw_connection)
        self.connection.set_mutex(self.get_mutex())
        print "MyAsyncClient::connected", self.connection.get_local(), self.connection.get_remote()
        self.connection.write("Ping")
        self.connection.read_start()
        self._done = True
        self.is_connected = True

    def connect_failed(self, message):
        print "Connection failed %s" % message
        self._connect_failed = True
        self._done = True

    def is_done(self):
        print "is_done", self._done
        return self._done

    def close_and_wait(self):
        if self.connection is not None:
            self.connection.close()
            while not self.connection.is_done():
                time.sleep(1)



class AsyncSocketTester(unittest.TestCase):

    def test_accept_and_connect(self):
        server = ("127.0.0.1", 0)
        async_socket_server = MyAsyncServer(giri_unittest.get_checkpoint_handler(), giri_unittest.get_async_service(), server, 3, 0)
        server = async_socket_server.get_listen_info()

        async_socket_client_1 = MyAsyncClient(giri_unittest.get_checkpoint_handler(), giri_unittest.get_async_service(), server)
        async_socket_client_2 = MyAsyncClient(giri_unittest.get_checkpoint_handler(), giri_unittest.get_async_service(), server)
        async_socket_client_3 = MyAsyncClient(giri_unittest.get_checkpoint_handler(), giri_unittest.get_async_service(), server)

        def wait_for_done():
            return async_socket_client_1.is_done() and async_socket_client_2.is_done() and async_socket_client_2.is_done()
        giri_unittest.wait_until_with_timeout(wait_for_done, 5000)

        self.assertTrue(async_socket_client_1.is_done())
        self.assertTrue(async_socket_client_2.is_done())
        self.assertTrue(async_socket_client_3.is_done())

        async_socket_server.close_and_wait()
        async_socket_client_1.close_and_wait()
        async_socket_client_2.close_and_wait()
        async_socket_client_3.close_and_wait()


    def test_connect_error(self):
        server = ("127.0.0.1", 0)
        async_socket_client = MyAsyncClient(giri_unittest.get_checkpoint_handler(), giri_unittest.get_async_service(), server)


        def wait_for_done():
            return async_socket_client.is_done()
        giri_unittest.wait_until_with_timeout(wait_for_done, 5000)

        self.assertTrue(async_socket_client._connect_failed)

        async_socket_client.close_and_wait()

#
#    def test_accept_and_connect_crash(self):
#        server = ("127.0.0.1", 8047)
#        async_socket_server = MyAsyncServer(checkpoint_handler, async_service, server, 1, 10)
#        async_socket_client = MyAsyncClient(checkpoint_handler, async_service, server)
#
#        def wait_for_connect():
#            return async_socket_client.is_connected()
#        giri_unittest.wait_until_with_timeout(wait_for_connect, 5000)
#        async_socket_server.close_and_wait()
#        async_socket_client.close_and_wait()
#
#
#        while(not async_service.memory_guard_is_empty()):
#            print "memory_guard_cleanup"
#            async_service.memory_guard_cleanup()
#            time.sleep(2)
#


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
