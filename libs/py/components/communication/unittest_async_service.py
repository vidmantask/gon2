"""
Unittest of async_service module
"""
import unittest
import time

from lib import giri_unittest
from lib import cryptfacility


class AsyncServiceTester(unittest.TestCase):
    def setUp(self):
        pass
    
    def my_sleeper(self, counter):
        print "my_sleeper", counter
        key_pair_1 = cryptfacility.pk_generate_keys()
        print key_pair_1[0]
        print key_pair_1[1]
        time.sleep(1)
        if counter > 1:
            giri_unittest.get_async_service().sleep_start(giri_unittest.get_checkpoint_handler(), 0, 0, 500, self, 'my_sleeper', counter - 1)        
            return
        self.my_sleeper_slow_running = False
        

    def my_sleeper_fast(self, counter):
        print "my_sleeper_fast", counter
        if counter > 1:
            giri_unittest.get_async_service().sleep_start(giri_unittest.get_checkpoint_handler(), 0, 0, 100, self, 'my_sleeper_fast', counter - 1)        
            return
        self.my_sleeper_fast_running = False
        
    def my_no_arg(self):
        print "my_no_arg"

    def test_sleep(self):
        self.my_sleeper_fast_running = True
        self.my_sleeper_slow_running = True

        giri_unittest.get_async_service().sleep_start(giri_unittest.get_checkpoint_handler(), 0, 0, 500, self, 'my_no_arg')        
        giri_unittest.get_async_service().sleep_start(giri_unittest.get_checkpoint_handler(), 0, 0, 500, self, 'my_sleeper', 10)        
        giri_unittest.get_async_service().sleep_start(giri_unittest.get_checkpoint_handler(), 0, 0, 100, self, 'my_sleeper_fast', 100)        

        while(self.my_sleeper_fast_running or self.my_sleeper_slow_running):
            time.sleep(1)
        
 
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
