"""
Demo of session manager
"""
import threading
import time
import sys

import sys
import lib_cpp.communication

import lib.checkpoint 
import lib.giri_unittest
import lib.config

import components.communication.tunnel_endpoint
import components.communication.session
import components.communication.session_manager



class ServerSession(components.communication.session.APISessionEventhandlerReady, components.communication.tunnel_endpoint.APITunnelendpointEventhandler):
    def __init__(self, session):
        components.communication.session.APISessionEventhandlerReady.__init__(self)
        components.communication.tunnel_endpoint.APITunnelendpointEventhandler.__init__(self)
        self.session_ = session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = lib_cpp.communication.APITunnelendpointReliableCryptedTunnel_create()
        self.tunnelendpoint_.set_eventhandler(self)
        self.tunnelendpoint_child_01_ = lib_cpp.communication.APITunnelendpointReliableCrypted_create()
        self.tunnelendpoint_child_01_.set_eventhandler(self)
        self.tunnelendpoint_child_02_ = lib_cpp.communication.APITunnelendpointReliableCrypted_create()
        self.tunnelendpoint_child_02_.set_eventhandler(self)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint(1, self.tunnelendpoint_child_01_)
        self.tunnelendpoint_.add_tunnelendpoint(2, self.tunnelendpoint_child_02_)
        self.got_ping = False
        self.send_pong = False
        
    def session_state_ready(self):
        print 'python ServerSession::session_state_ready iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'
        self.session_.read_start_state_ready()
        
    def session_state_closed(self):
        print 'python ServerSession::session_state_closed xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    
    def session_state_key_exchange(self):
        print 'python ServerSession::session_state_key_exchange xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

    def session_read_continue_state_ready(self):
        print 'python ServerSession::session_read_continue_state_ready xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        return True
#
    def tunnelendpoint_recieve(self, message):
        print 'python ServerSession::tunnelendpoint_recieve tttttttttttttttttttttttttttttttt', message
        if(message == 'ping'):
            self.got_ping = True
            self.tunnelendpoint_.tunnelendpoint_send("pong")
            self.tunnelendpoint_child_01_.tunnelendpoint_send("pong")
            self.tunnelendpoint_child_02_.tunnelendpoint_send("pong")
            self.send_pong = True
        


class ClientSession(components.communication.session.APISessionEventhandlerReady, components.communication.tunnel_endpoint.APITunnelendpointEventhandler):
    def __init__(self, session):
        components.communication.session.APISessionEventhandlerReady.__init__(self)
        components.communication.tunnel_endpoint.APITunnelendpointEventhandler.__init__(self)
        self.session_ = session
        self.session_.set_eventhandler_ready(self)
        self.tunnelendpoint_ = lib_cpp.communication.APITunnelendpointReliableCryptedTunnel_create()
        self.tunnelendpoint_.set_eventhandler(self)
        self.tunnelendpoint_child_01_ = lib_cpp.communication.APITunnelendpointReliableCrypted_create()
        self.tunnelendpoint_child_01_.set_eventhandler(self)
        self.tunnelendpoint_child_02_ = lib_cpp.communication.APITunnelendpointReliableCrypted_create()
        self.tunnelendpoint_child_02_.set_eventhandler(self)
        self.session_.add_tunnelendpoint_tunnel(1, self.tunnelendpoint_)
        self.tunnelendpoint_.add_tunnelendpoint(1, self.tunnelendpoint_child_01_)
        self.tunnelendpoint_.add_tunnelendpoint(2, self.tunnelendpoint_child_02_)
        self.send_ping = False
        self.got_pongs = 0
                     
    def session_state_ready(self):
        print 'python ClientSession::session_state_ready xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        self.session_.read_start_state_ready()
        self.tunnelendpoint_.tunnelendpoint_send("ping")
        self.send_ping = True

    def session_state_closed(self):
        print 'python ClientSession::session_state_closed xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    
    def session_state_key_exchange(self):
        print 'python ClientSession::session_state_key_exchange xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

    def session_read_continue_state_ready(self):
        print 'python ClientSession::session_read_continue_state_ready xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
        return True

    def tunnelendpoint_recieve(self, message):
        print 'python ClientSession::tunnelendpoint_recieve xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ----------------------------------- message:', message
        try:
            if(message == 'pong'):
                self.got_pongs += 1
            if(self.got_pongs == 3):
                self.session_.close(False)
        except:
            print sys.exc_info()



class SessionManagerServerEventhandler(components.communication.session_manager.APISessionManagerEventhandler):
    def __init__(self):
        components.communication.session_manager.APISessionManagerEventhandler.__init__(self)
        self.sessions_ = {}

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;
    
    def session_manager_session_created(self, connection_id, session):
        print 'python SessionManagerServerEventhandler::session_manager_session_created'
        print 'session_id:' , session.get_session_id()
        self.sessions_[session.get_session_id()] = ServerSession(session)

    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerServerEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id
        del self.sessions_[session_id]


class SessionManagerClientEventhandler(components.communication.session_manager.APISessionManagerEventhandler):

    def session_manager_resolve_connection(self, resolve_info):
        return resolve_info;

    def session_manager_session_created(self, session):
        print 'python SessionManagerClientEventhandler::session_manager_session_created'
        print 'session_id:' , session.get_session_id()
        self.session_ = ClientSession(session)
        
    def session_manager_session_closed(self, session_id):
        print 'python SessionManagerClientEventhandler::session_manager_session_closed'
        print 'session_id:' , session_id
        self.session_ = None



def killer():
    print "KILLER.begin"
    session_manager_server.close()
    print "KILLER.end"
    
    
def server_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()
        
    except RuntimeError, value:
            checkpoint_handler.Checkpoint("main", "demo_session_manager", lib.checkpoint.ERROR, message= '%s' % value)


def client_session_manager_thread_start(checkpoint_handler, communication_session_manager):
    try:
        communication_session_manager.start()
    
    except RuntimeError, value:
        checkpoint_handler.Checkpoint("main", "demo_session_manager", lib.checkpoint.ERROR, message= '%s' % value)
   



lib_cpp.communication.init_communication()

known_secret_server = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004002E3741F0BE067C63BEF553E21C73CF928EB70123CF6817975254252385010AEF60D5894E8734B955689C929FEBB396F9269BA648132F24DB0AD6183081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D1A875AF53854318F52DEC8CD21CAF2DE7BD5187CAB39DA9C757F887747'
known_secret_client = '0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E0004018BA68CE7B267CD97ED181BD294EC8860399A0E7D2B2356A356C3D5A4A6001E4D1ED593DB5BA8AE8F79CE6C943F2C7BF87BD6642CBBE6934F76E5363081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D4ACDE35A55D995A65C050975890AEDF825C6ABA4DC67D31741D1B71B96'

checkpoint_filter = lib.checkpoint.CheckpointFilter_true()
checkpoint_output_handler = lib.checkpoint.CheckpointOutputHandlerTEXT()
checkpoint_handler = lib.checkpoint.CheckpointHandler(checkpoint_filter, checkpoint_output_handler)

session_manager_server_eventhandler = SessionManagerServerEventhandler()
session_manager_client_eventhandler = SessionManagerClientEventhandler()
    
session_manager_server = components.communication.session_manager.APISessionManagerServer_create(session_manager_server_eventhandler, checkpoint_handler.checkpoint_handler, known_secret_server, "127.0.0.1", 8012)
session_manager_client = components.communication.session_manager.APISessionManagerClient_create(session_manager_client_eventhandler, checkpoint_handler.checkpoint_handler, known_secret_client, lib_cpp.communication.ApplProtocolType.PYTHON);


session_manager_server_thread = threading.Thread(target=server_session_manager_thread_start, args=(checkpoint_handler, session_manager_server))
session_manager_server_thread.start()

#print "hej1"
#session_manager_server.start()
print "hej2"


session_manager_client.set_servers(lib.config.parse_servers(checkpoint_handler, "127.0.0.1, 8012"))
session_manager_client_thread = threading.Thread(target=client_session_manager_thread_start, args=(checkpoint_handler, session_manager_client))
session_manager_client_thread.start()





#lib.giri_unittest.wait_until_with_timeout(session_manager_client.session_.got_pong, 2000)
#session_manager_server.close()
#time.sleep(20)
t = threading.Timer(20.0, killer)
t.start()
