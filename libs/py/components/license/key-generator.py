#!/usr/bin/env python
"""
Generate new signing keys
"""

import sys

import licensefile

def main():
    assert len(sys.argv) == 2
    keypair_name = sys.argv[1]
    
    licensefile.generate_keypair(keypair_name)
    print 'pub:'
    print open(licensefile.pub_name(keypair_name)).read()
    print 'priv:'
    print open(licensefile.priv_name(keypair_name)).read()
        
if __name__ == '__main__':
    main()
