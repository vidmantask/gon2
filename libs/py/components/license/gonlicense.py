"""
G/On Licensing
See http://giriwiki/G5_License_Implementation#License_File_format
"""

import datetime
import os.path

import lib.version

import licensefile


class GonLicense(licensefile.LicenseFile):

    def __init__(self, license_filename, default_license_filename=None, public_key=None):
        if default_license_filename is None:
            default_license_filename = os.path.join(os.path.dirname(license_filename), 'default.lic')
        dme_license_filename = os.path.join(os.path.dirname(license_filename), 'dme_license.txt')
        
        current_ver = lib.version.Version.create_current()
        if public_key is None:
            public_key = current_ver.get_license_stamp_key()
        licensefile.LicenseFile.__init__(self, pub=public_key)

        self.errors = [] # Should be shown if license fails
        self.warnings = [] # Could be shown somewhere - but the following flags might be better
        self.license_expired = True # Mgmt lockdown and nagging
        self.maintenance_expired = True # Ok - but be careful!
        self.maintenance_violation = True # Mgmt lockdown and nagging

        self.default_lic = False
        if os.path.exists(license_filename):
            self.read(license_filename)
        elif os.path.exists(dme_license_filename):
            self.read(dme_license_filename)
        elif os.path.exists(default_license_filename):
            self.warnings.append("License file '%s' not found - using default license" % (license_filename))
            self.read(default_license_filename)
            self.default_lic = True
        else:
            self.errors.append("License file '%s' not found" % (license_filename))
            self.invalidate()
            return

        # No license if invalid license
        if not self.valid:
            self.errors.append("Invalid license file")

        # Some mandatory fields
        for key in ['License Number', 'License File Number', 'Licensed To']:
            value = self.get_one(key)
            if not value:
                self.errors.append("Mandatory field %r is missing" % key)

        if self.errors:
            self.invalidate()
            return

        # No license if license expired
        lic_exp_date = self.get_date('License Expiration Date')
        if lic_exp_date and lic_exp_date.date() < datetime.date.today():
            self.warnings.append("License expired on %s" % lic_exp_date)
        else:
            self.license_expired = False

        # No license to software build after maintenance expiration
        maint_exp_date = self.get_date('Maintenance Expiration Date')
        if maint_exp_date and maint_exp_date.date() < datetime.date.today():
            self.warnings.append("Maintenance expired on %s" % maint_exp_date)
        else:
            self.maintenance_expired = False
        if maint_exp_date and maint_exp_date < current_ver.get_build_date():
            self.warnings.append("No license to software released on %s after Maintenance Expiration Date %s" %
                                 (current_ver.get_build_date(), maint_exp_date))
        else:
            self.maintenance_violation = False

    def check_public_ips(self, addresses, ports=None, http_enc_ports=None):
        """Return string with message if usage of IP info is unlicensed"""
        if self.get_one('Client Connect Address') == '*':
            return None
        lic_addresses = self.get_all('Client Connect Address')
        if sorted(x.lower() for x in lic_addresses) != sorted(x.lower() for x in addresses):
            return "License for IP addresses %s doesn't match %s" % (', '.join(lic_addresses), ', '.join(addresses))
        if not ports: ports = ()
        lic_ports = self.get_all('Client Connect Port')
        if sorted(x.lower() for x in lic_ports) != sorted(x.lower() for x in ports):
            return "License for IP ports %s doesn't match %s" % (', '.join(lic_ports), ', '.join(ports))
        if not http_enc_ports: http_enc_ports = ()
        lic_http_enc_ports = self.get_all('HTTP Encapsulation Client Connect Port')
        if sorted(x.lower() for x in lic_http_enc_ports) != sorted(x.lower() for x in http_enc_ports):
            return "License for HTTP Encapsulation port %s doesn't match %s" % (', '.join(lic_http_enc_ports), ', '.join(http_enc_ports))
        return None

    def format_lines(self):
        """Return relevant license info in a few lines"""
        result = []
        if self.valid:
            result.append("%s#%s" % (self.get_one('License Number'), self.get_one('License File Number')))
            result.append(self.get_one('Licensed To'))
        return result


def gon_licensefile_stamper(pub, priv, filename):
    license = licensefile.LicenseFileWriter(priv=priv, pub=pub)
    license.read(filename)
    license.set_all('License Format Version', '2', pos=0)
    license.set_all('License Signature Key', pub[:16], pos=0)
    license.set_all('License File Timestamp', datetime.datetime.utcnow().replace(microsecond=0).isoformat() + 'Z', pos=0)
    license.write(filename)


class GonLicenseHandler(object):
    """
    Wrapper class for rereading the licensefile when requesting the license
    """
    def __init__(self, license_filename):
        self.license_filename = license_filename

    def get_license(self):
        current_ver = lib.version.Version.create_current()
        gon_license = GonLicense(self.license_filename, public_key=current_ver.get_license_stamp_key())
        if not gon_license.valid:
            gon_license = GonLicense(self.license_filename, public_key=current_ver.get_license_stamp_key_rsa())
        return gon_license

    def set_license(self, license):
        license_file = open(self.license_filename, 'w')
        license_file.write(license.encode("utf-8"))
        license_file.close()
        return True


def example(filename, public_key):
    # show how "other" things should be checked by rest of system

    license_file = GonLicense(filename, public_key=public_key)
    print 'errors:', license_file.errors
    print 'warnings:', license_file.warnings
    print 'valid:', license_file.valid

    # If configured then it must be checked by giving it as constructor to license_file
    # Alternatively it can be read and used instead of configuration
    print 'Client Connect Address', license_file.get_all('Client Connect Address')
    public_ips = ['127.0.0.1']
    print 'Ip check:', license_file.check_public_ips(public_ips)

    # Should be shown prominently in Management interface and client - for example in
    print 'Licensed To %r %s' % (license_file.get_one('Licensed To'), license_file.get_one('Licensed To'))
    # And if it is set a clear notification of
    print 'License Expiration Date', license_file.get_date('License Expiration Date')
    # In about or splash box we could show it together with
    print 'License Number', license_file.get_one('License Number')
    print 'License File Number', license_file.get_one('License File Number')
    print 'Maintenance Expiration Date', license_file.get_date('Maintenance Expiration Date')

    # Must be checked before adding users:
    print 'Number of Users', license_file.get_int('Number of Users')
    # Must be checked before adding tokens:
    print 'Number of Tokens', license_file.get_int('Number of Tokens')

    # Must be checked to enable auth factor in Management - and perhaps also by Gateway server
    print 'USB key file', license_file.has('Authentication Factor', 'USB key file')
    print 'MSC', license_file.has('Authentication Factor', 'MSC')

    # Must be checked before adding new menu item based on stamped template
    print 'Number of Menu Items', license_file.get_int('Number of Menu Items')

    # To enable stamped template make sure access to it or its group
    template_name = 'citrix template'
    template_group = 'Default' # all licenses should contain this
    print 'Launch Template', template_name, license_file.has('Launch Template', template_name)
    print 'Launch Template', template_group, license_file.has('Launch Template', template_group)

    # To enable stamped package make sure access to it or its group
    # (or perhaps: referenced from licensed stamped template)
    package_name = 'GTSC'
    package_group = 'Default' # all licenses should contain this
    print 'Package', package_name, license_file.has('Package', package_name)
    print 'Package', package_group, license_file.has('Package', package_group)

    # To enable stamped package make sure access to it or its group
    report_name = 'Time of day access'
    report_group = 'Default' # all licenses should contain this
    print 'Report', report_name, license_file.has('Report', report_name)
    print 'Report', report_group, license_file.has('Report', report_group)

    # To enable Management and Gateway use of a feature check access
    print 'Multiple Client Connect Addresses', license_file.has('Feature', 'Multiple Client Connect Addresses')
    print 'HTTP Encapsulation', license_file.has('Feature', 'HTTP Encapsulation')

def main():
    import lib.checkpoint
    import components.communication.async_service
    components.communication.async_service.init(lib.checkpoint.CheckpointHandler.get_cout_all())
    import sys
    filename = sys.argv[1]
    pub = sys.argv[2]
    example(filename, pub)

if __name__ == '__main__':
    main()
