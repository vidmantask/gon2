import unittest
from lib import giri_unittest

import os
import datetime
import licensefile

import lib.checkpoint
import components.communication.async_service
components.communication.async_service.init(lib.checkpoint.CheckpointHandler.get_cout_all())


class LicenseTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_compatible(self):
        """Test basic types and backwards compatibility"""
        pub = '308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E00040160CFFBA53809BDA96B3D560178DDEC5882F9C1057AA90CF842EDAFEA2801C2ECB00B90C93C2159FE855393420884E223975EF42FA1D45ABA91427F'
        priv = '3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D27403D82DDCE0827656917A73AA446B9EC41525127A55FD46EF2F4B4EC'
        file('org.lic', 'w').write('''# comment
i: 7
colon::
empty:
date: 2009-12-24
l 1:1
l 1: 3
l 1:2
triton: 1
triton: 2
triton: 3
uni:bl\xc3\xa5b\xc3\xa6rgr\xc3\xb8d

0F0B4F7FC55D2E3A044834F9D49F59499649AB5987452532E30481F84F07CD920C50E29B5AAA295A39FEDE9E553AD5948C9634F8A80A7C256DA2
''')

        def check(lf):
            #print repr(lf.stable_repr())
            self.assertEqual(lf.stable_repr(), "[('colon', [':']), ('date', ['2009-12-24']), ('empty', ['']), ('i', ['7']), ('l 1', ['1', '3', '2']), ('triton', ['1', '2', '3']), ('uni', ['bl\\xc3\\xa5b\\xc3\\xa6rgr\\xc3\\xb8d'])]")
            # basic functionality
            self.assertTrue(lf.valid) # correct stamp
            self.assertEqual(lf.get_int('i'), 7) # get_int
            self.assertEqual(lf.get_date('date'), datetime.datetime(2009, 12, 24, 0, 0))
            self.assertEqual(lf.get_one('uni'), 'bl\xc3\xa5b\xc3\xa6rgr\xc3\xb8d'.decode('utf8')) # get_one unicode
            self.assertEqual(type(lf.get_one('i')), unicode) # get_one always returns unicode
            self.assertEqual(lf.get_all('triton'), ['1', '2', '3']) # get_all gives strings
            self.assertEqual(lf.get_all('l 1'), ['1', '3', '2']) # in right order
            # format details
            self.assertEqual(lf.get_one('colon'), ':') # format handles colons
            self.assertEqual(lf.get_one('empty'), '') # format handles empty
            # has
            self.assertTrue(lf.has('triton', '1')) # works on strings
            self.assertFalse(lf.has('triton', 2)) # works on strings
            self.assertFalse(lf.has('Triton', '3')) # casing
            self.assertFalse(lf.has('triton', '4')) # not set
            # getters handling of special values
            self.assertEqual(lf.get_date('empty'), None) # date can be empty
            self.assertEqual(lf.get_int('empty'), 0) # int can be empty
            self.assertEqual(lf.get_int('colon'), 0) # int can be anything non-int
            # getters handling of non-existing values - defaults
            self.assertEqual(lf.get_all('I'), []) # case sensitive
            self.assertEqual(lf.get_one('not'), None)
            self.assertEqual(lf.get_date('not'), None)
            self.assertEqual(lf.get_int('not'), 0)
            self.assertEqual(lf.get_all('not'), [])
            self.assertEqual(lf.get_one('not'), None)
            # getters handling of non-existing values - custom defaults
            self.assertEqual(lf.get_all('I', 117), 117) # case sensitive
            self.assertEqual(lf.get_one('not', 117), 117)
            self.assertEqual(lf.get_date('not', 117), 117)
            self.assertEqual(lf.get_int('not', 117), 117)
            self.assertEqual(lf.get_all('not', 117), 117)
            self.assertEqual(repr(lf.get_one('not', 117)), "117") # get_one returns default as is
            self.assertEqual(repr(lf.get_one('not', 117L)), "117L") # get_one returns default as is
            self.assertEqual(repr(lf.get_one('not', '117')), "'117'") # get_one returns default as is
            self.assertEqual(repr(lf.get_one('not', u'117')), "u'117'") # get_one returns default as is
            # getters handling of empty values
            self.assertEqual(lf.get_one('empty'), '') # format handles empty
            self.assertEqual(lf.get_date('empty'), None) # date can be empty
            self.assertEqual(lf.get_int('empty'), 0) # int can be empty
            # get_one when many
            self.assertEqual(lf.get_one('triton'), None)
            self.assertEqual(lf.get_one('triton', 117), 117)
            self.assertEqual(lf.get_one('triton', default='x'), 'x')

        # Exercise read/writer
        z = licensefile.LicenseFileWriter(pub=pub, priv=priv)
        z.read('org.lic')
        z.write('stamped1.lic') # write a restamped file early - nice for debugging and test maintenance
        check(z)

        # Exercise readonly on old licensefile
        z = licensefile.LicenseFile(pub=pub)
        z.read('org.lic')
        check(z)

        # Exercise our new rewritten and restamped file
        z = licensefile.LicenseFile(pub=pub)
        z.read('stamped1.lic')
        check(z)

        # Check that slight editing doesn't hurt
        lines = file('stamped1.lic').readlines()
        lines2 = [' ' + l.strip() + ' \n\r\n' for l in lines if l[0].isalnum()]
        lines2[1:4] = reversed(lines2[1:4]) # change order - but no broken multi-line values
        file('stamped-whitespaced.lic', 'w').writelines(lines2)
        # Exercise our new rewritten and restamped file
        z = licensefile.LicenseFile(pub=pub)
        z.read('stamped-whitespaced.lic')
        check(z)

        # Test stamping with new keypair
        licensefile.generate_keypair('tmp')
        w = licensefile.LicenseFileWriter('tmp')
        w.read('stamped-whitespaced.lic')
        self.assertFalse(w.valid)
        w.write('stamped2.lic')
        z = licensefile.LicenseFile('tmp')
        z.read('stamped2.lic')
        check(z)

        # Test case preservation
        w.set_all('empty', 'void')
        w.set_all('Empty', 'Void')
        w.write('stamped3.lic')
        z.read('stamped3.lic')
        self.assertTrue(z.valid)
        self.assertEqual(z.get_all('empty'), ['void'])
        self.assertEqual(z.get_all('Empty'), ['Void'])


        # Check that slight editing hurts
        file('caseerror1.lic', 'w').write(file('stamped-whitespaced.lic').read().replace('empty', 'Empty'))
        z = licensefile.LicenseFile(pub=pub)
        z.read('caseerror1.lic')
        self.assertFalse(z.valid)

        file('caseerror2.lic', 'w').write(file('stamped-whitespaced.lic').read().replace('bl', 'gr'))
        z = licensefile.LicenseFile(pub=pub)
        z.read('caseerror2.lic')
        self.assertFalse(z.valid)

        os.remove('org.lic')
        os.remove('stamped1.lic')
        os.remove('stamped-whitespaced.lic')
        os.remove('stamped2.lic')
        os.remove('stamped3.lic')
        os.remove('caseerror1.lic')
        os.remove('caseerror2.lic')


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
GIRI_UNITTEST_IGNORE = True
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'mk'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
