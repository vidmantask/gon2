#!/usr/bin/env python
"""
Stamp a file
"""

import sys
import datetime

import licensefile

def main():
    assert len(sys.argv) == 4
    keys = sys.argv[1]
    input_file = sys.argv[2]
    license_file = sys.argv[3]
    
    z = licensefile.LicenseFileWriter(keys)
    z.read(input_file)
    z.set_all('License File Timestamp', datetime.datetime.utcnow().replace(microsecond=0).isoformat(), pos=0)
    z.write(license_file)

    x = licensefile.LicenseFile(keys)
    x.read(license_file)
    assert x.valid
        
if __name__ == '__main__':
    main()
