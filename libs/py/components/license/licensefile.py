"""
Generic license file format
"""

import datetime
import collections

from lib import cryptfacility



def pub_name(keypair_name):
    return keypair_name + '.pub'
def priv_name(keypair_name):
    return keypair_name + '.priv'

def generate_keypair(keypair_name):
    pub, priv = cryptfacility.pk_generate_keys_rsa()
    open(pub_name(keypair_name), 'w').write(pub)
    open(priv_name(keypair_name), 'w').write(priv)


class LicenseFile(object):

    def __init__(self, keypair_name=None, pub=None):
        """
        keypair_name.pub (and .priv) will be used for key files.
        pub key can also be specified directly.
        """

        if keypair_name:
            assert not pub
            self.pub = open(pub_name(keypair_name)).read().strip()
        else:
            self.pub = pub
        #print 'pub:', self.pub

        self.valid = False # Flag indicating if license file is technically valid
        self.value_lines = [] # license file lines - used for writing back
        self.values = collections.defaultdict(list) # accessible values
        
        self.raw_lines = [] # raw license file lines

    def stable_repr(self):
        """Return string representation of current values, independent of line ordering (except when it matters) and whitespace
        """
        return repr(sorted(self.values.items()))

    def read(self, license_filename):
        self.values.clear()
        self.value_lines = []
        self.raw_lines = []
        self.valid = False
        signature_str = ''
        for line in open(license_filename):
            line = line.strip()
            self.raw_lines.append(line)
            if not line or line.startswith('#'):
                self.value_lines.append(line)
                continue
            if line:
                if ':' in line:
                    self.value_lines.append(line)
                    k, v = line.split(':', 1)
                    self.values[k].append(v.strip())
                else:
                    signature_str += line

        if signature_str:
            #print 'read signature', signature_str
            
            # Signature is verifyed first using ecdsa used in gon, in order to accept old licenses.
            # New licenses is using rsa.
            self.valid = cryptfacility.pk_verify_challenge(self.pub, self.stable_repr(), signature_str)
            if not self.valid:
                self.valid = cryptfacility.pk_verify_challenge_rsa(self.pub, self.stable_repr(), signature_str)
                

        #print dict(self.values)

    def invalidate(self):
        """Mark the licensefile as invalid.
        Value getters returns nothing, but lines are preserved and can be written back
        """
        self.valid = False

    def has(self, key, value):
        # Note: self.values[key] is a list, so this will make a slow search, not dict lookup
        return self.valid and (value in self.values.get(key, []))

    def get_all(self, key, default=None, filt=None):
        if self.valid and key in self.values:
            return [v.decode('utf-8') for v in self.values[key] if not filt or filt(v)
                    ] or default or []
        return default or []

    def get_one(self, key, default=None, filt=None):
        hits = self.get_all(key, default=[default], filt=filt)
        if len(hits) == 1:
            return hits[0]
        return default

    def get_int(self, key, default=0):
        return int(self.get_one(key, default=default, filt=str.isdigit))

    def get_date(self, key, default=None):
        s = self.get_one(key, '')
        if s.count('-') == 2:
            try:
                t = s.split('-')
                return datetime.datetime(int(t[0]), int(t[1]), int(t[2]))
            except ValueError:
                pass
        if isinstance(default, tuple) and len(default) == 3:
            return datetime.datetime(default[0], default[1], default[2])
        return default
    
    def to_string(self):
        return '\r\n'.join([s.decode("utf-8") for s in self.raw_lines])


class LicenseFileWriter(LicenseFile):

    def __init__(self, keypair_name=None, pub=None, priv=None):
        LicenseFile.__init__(self, keypair_name, pub)
        if keypair_name:
            assert not priv
            self.priv = open(priv_name(keypair_name)).read().strip()
        else:
            self.priv = priv
        #print 'priv:', self.priv

    def write(self, license_filename):
        new_signature = cryptfacility.pk_sign_challenge_rsa(self.priv, self.stable_repr())
        #print 'new signature', new_signature
        open(license_filename, 'w').write('\r\n'.join(self.value_lines) + '\r\n\r\n' + new_signature + '\r\n')
        # Verify
        verify_lf = LicenseFile(pub=self.pub)
        verify_lf.read(license_filename)
        assert verify_lf.valid

    def set_all(self, key, value, pos=None):
        # replace all entries of key with the value, replacing first or inserting at pos or appending
        for i in reversed(range(len(self.value_lines))):
            if self.value_lines[i].startswith(key + ':'):
                pos = i
                del self.value_lines[i]
        value_line = key + ': ' + value
        if pos is None:
            self.value_lines.append(value_line)
        else:
            self.value_lines.insert(pos, value_line)
        self.values[key.strip()] = [value.strip()]



def test():
    import lib.checkpoint
    import components.communication.async_service
    checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
    components.communication.async_service.init(checkpoint_handler)

    generate_keypair('tmp')
    z = LicenseFileWriter('tmp')
    z.read('simple.lic')
    assert not z.valid # doesn't match new key pair
    z.write('tmp.lic')
    print 'reading...'
    x = LicenseFile('tmp')
    x.read('tmp.lic')
    assert '.valid', x.valid
    print 'Package', 'Default', x.has('Package', 'Default')
    print 'Package', 'yipie', x.has('Package', 'yipie')
    print 'Launch Template', x.get_int('Launch Template')
    print 'Token Type', x.get_int('Token Type')
    print 'Token Type', x.get_one('Token Type')
    print 'Licensed To', x.get_one('Licensed To')
    print 'default date', x.get_date('Installation Name', (1975,8,6))
    print 'Maintenance Expiration Date', x.get_date('Maintenance Expiration Date')
    print 'Number of Menu Items', x.get_int('Number of Menu Items')
    print 'Launch Template', x.get_all('Launch Template')
    print 'Launch Template', 'x', x.has('Launch Template', 'x')
    print 'Number of Users', x.get_all('Number of Users', filt=str.isalnum)
    print 'Number of Users', x.get_all('Number of Users', filt=str.isalpha, default='nada')
    print 'Launch Template', x.get_all('Launch Template', filt=str.isalpha)
    print 'multi', x.get_all('a')

if __name__ == '__main__':
    test()
