import json
from flask import (
    Flask,
    request,
    make_response
)
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token,
    get_jwt_identity,
    decode_token

)
from rest_api import RestApi


class RestService(object):
    def __init__(self):  # type: () -> None
        pass

    @staticmethod
    def create_response(data, code):
        try:
            response = make_response(json.dumps(data, default=lambda o: o.__dict__,
                                                sort_keys=False,
                                                indent=4), code)
        except Exception:
            # TODO: Add exception to the log
            response = make_response({"message": "Internal server error"}, 500)
        return response

    @staticmethod
    def deserialize(class_):
        def wrap(f):
            def decorator(*args, **kwargs):
                try:
                    if request.data:
                        obj = class_()(**request.get_json())
                    else:
                        obj = class_()(**dict())
                except Exception as e:
                    # TODO: Add exception to the log
                    return RestService.create_response({"message": e.message, "details": e}, 400)
                return f(obj)

            return decorator

        return wrap

    @staticmethod
    def start_admin_wsgi(admin_ws_service):
        app = Flask("Admin")
        app.config["admin_ws_service"] = admin_ws_service

        # Setup the Flask-JWT-Extended extension
        app.config[
            "JWT_SECRET_KEY"] = "C1vqmjq24g9IPOAjz5xvApiOhBGBonP4W7rzQtX7pQcBOCXnmn3jvAwHlM5ccyw6tF5zUHOnF58b3DAhISaA46JVuZmAiDjX9L71390nRVpvnywjaZEdfaHISizx62GxReMDYEORXMIuqyKJUCZtoBqX1em4vtukCvkE1keIOQtzs8K6tATNpK0hED4EkK17gROYQPVDoI8l25tyehB5otY7HQdkfgW76N3OQ9fbT6Vrjv2ApY4yq1SpT1PazRQqCMnokM1eD4l6IIByWs3qh3EkKYPWHRGDHOUNQzwTfht45X2V7EKRRJrbPHoT5lycMQb2vPBXAgXb3MYJZobmbcZo7XDEHMRrXJ9h1xa44ED9LBJEz1ytRqj7vjIwRSHd96IhUZgynYjkr13hfTbDK99CKnUCa6VOxTEWFCDNAJQZ5a6FS41f7Kq26P6uIuKCb7DlD89xfwm0IhGvUOJF7UJU3qIk91DUDWY4ruCoaZ0ZFLLBksTDii0XABZeG7UiLUahq4kISsKV52ywGP9M1qqxD3pp6M1aut39c137a3epwyyei4UI3MwjJ1L9ovHBeP0ceL0CVx1zCKe60LuRupgKdhGRZ44QoWpzFShE2CnxZQEcsWnJz1QCC1T3GyqXOgngkSNMx9bcxXSTY35U5exYMKmGpnUcqNNv8sPnznZC3KjXKEq006fmahRvHbMWqaPB5s8JjAZ6YjD8069tHQDbs51bYww3qksPxmG3pYUZpdIcEvPUIBJSKLR7cE0DJ27baADdpnPqlPXNllH3wcNNpcc4CXDyMBEvAUjKky49U3mb7Vg7c9soBJDwsYKCQm6Kw8vRuFIyouMJ0XONeQVuU50oNPqby4Z5c5yPuP0zkzrOoeTUnyFWtdET1VeGbZ7dhQTPKzxTttu7NEMrGuNt0TurQmOAd86AV7gPD12ZLV1EKPcXKlNA0Ifv5ZFxCVNxQ7cazuXtOuyvhBpMzHFsIUTi4RN0jmjt15J7hbzVi9MfWKcwtchfk0sllWfydUGs9YlupAmoZVlxcaCQ3Yj5AL320fxTtcaTp3TSrwsgmzsY9Is0xJGYRDt1MMLWl2S1fDQGQ63PvNfQnYbZQDQaSrXgKD80ma50OssSPQItf9UD4uvCgLJ9POrR1guqZ4sIdJpjsiuoIXWM7CA6viOSSIxV076j7aljPuK8gp0R3KX6eoWBodQTfPltYBlXbd70loi1nnuMEO1pDKmzdSbt1OIfdimIS5RdolJkG2x6MTRYacKRuXqvMFu8jt74"

        jwt = JWTManager(app)

        @app.after_request
        def finalize_response(response):
            response.headers.set("Content-Type", "application/json")
            response.headers.set("Access-Control-Allow-Origin", "http://localhost:4200")
            response.headers.set("Access-Control-Allow-Headers", "Content-Type, Authorization")
            response.headers.set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
            return response

        @app.errorhandler(404)
        def not_found():
            """Page not found."""
            return RestService.create_response({"error": "Unknown request"}, 404)

        @app.errorhandler(400)
        def bad_request():
            """Bad request."""
            return RestService.create_response({"error": "Bad request"}, 400)

        @app.errorhandler(500)
        def server_error():
            """Internal server error."""
            return RestService.create_response({"error": "Internal server error"}, 500)

        @app.route("/admin/openSession", endpoint="open_session", methods=["POST", "PUT", "DELETE"])
        @RestService.deserialize(RestApi.Admin.OpenSessionRequest)
        def open_session(open_session_request):
            try:
                if open_session_request.get_old_session_id:
                    # TODO: Investigate expired tokens and close expired session
                    old_session_id = decode_token(encoded_token=open_session_request.get_old_session_id,
                                                  allow_expired=True).get("jti")
                    open_session_request.set_old_session_id(old_session_id)
                open_session_response = app.config["admin_ws_service"].rest_open_session(open_session_request)
                if open_session_response.get_session_id:
                    open_session_response.set_session_id(create_access_token(
                        identity=open_session_response.get_session_id,
                        expires_delta=app.config["admin_ws_service"].get_session_timeout))
                    return RestService.create_response(open_session_response, 200)
                else:
                    raise Exception("Internal server error")
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/closeSession", endpoint="close_session", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.CloseSessionRequest)
        def close_session(close_session_request):
            try:
                session_id = get_jwt_identity()
                close_session_request.set_session_id(session_id)
                close_session_response = app.config["admin_ws_service"].rest_close_session(
                    close_session_request)
                return RestService.create_response(close_session_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/login", endpoint="login", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.LoginRequest)
        def login(login_request):
            try:
                session_id = get_jwt_identity()
                login_request.set_session_id(session_id)
                login_response = app.config["admin_ws_service"].rest_login(
                    login_request)
                if 0 == login_response.get_error_code:
                    response_code = 200
                else:
                    response_code = 401
                return RestService.create_response(login_response, response_code)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/internalLogin", endpoint="internal_login", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.InternalLoginRequest)
        def internal_login(internal_login_request):
            try:
                session_id = get_jwt_identity()
                internal_login_request.set_session_id(session_id)
                internal_login_response = app.config["admin_ws_service"].rest_internal_login(
                    internal_login_request)
                if internal_login_response.get_rc:
                    response_code = 200
                else:
                    response_code = 401
                return RestService.create_response(internal_login_response, response_code)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getAccessDefintion", endpoint="get_access_definition", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetAccessDefintionRequest)
        def get_access_definition(get_access_definition_request):
            try:
                session_id = get_jwt_identity()
                get_access_definition_request.set_session_id(session_id)
                get_access_definition_response = app.config["admin_ws_service"].rest_get_access_definition(
                    get_access_definition_request)
                return RestService.create_response(get_access_definition_response, 200)
            except Exception as e:
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getModuleElements", endpoint="get_module_elements", methods=["POST", "PUT",
                                                                                        "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetModuleElementsRequest)
        def get_module_elements(get_module_elements_request):
            try:
                session_id = get_jwt_identity()
                get_module_elements_request.set_session_id(session_id)
                get_module_elements_response = app.config["admin_ws_service"].rest_get_module_elements(
                    get_module_elements_request)
                return RestService.create_response(get_module_elements_response, 200)
            except Exception as e:
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getSpecificElements", endpoint="get_specific_elements", methods=["POST", "PUT",
                                                                                            "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetSpecificElementsRequest)
        def get_specific_elements(get_specific_elements_request):
            try:
                session_id = get_jwt_identity()
                get_specific_elements_request.set_session_id(session_id)
                get_specific_elements_response = app.config["admin_ws_service"].rest_get_specific_elements(
                    get_specific_elements_request)
                return RestService.create_response(get_specific_elements_response, 200)
            except Exception as e:
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getAuthRestrictionElements", endpoint="get_auth_restriction_elements",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetAuthRestrictionElementsRequest)
        def get_auth_restriction_elements(get_auth_restriction_elements_request):
            try:
                session_id = get_jwt_identity()
                get_auth_restriction_elements_request.set_session_id(session_id)
                get_auth_restriction_elements_response = app.config[
                    "admin_ws_service"].rest_get_auth_restriction_elements(get_auth_restriction_elements_request)
                return RestService.create_response(get_auth_restriction_elements_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/updateAuthRestrictionElements", endpoint="update_auth_restriction_elements",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.UpdateAuthRestrictionElementsRequest)
        def update_auth_restriction_elements(update_auth_restriction_elements_request):
            try:
                session_id = get_jwt_identity()
                update_auth_restriction_elements_request.set_session_id(session_id)
                update_auth_restriction_elements_response = app.config[
                    "admin_ws_service"].rest_update_auth_restriction_elements(update_auth_restriction_elements_request)
                return RestService.create_response(update_auth_restriction_elements_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/removeUserFromLaunchTypeCategory", endpoint="remove_user_from_launch_type_category",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest)
        def remove_user_from_launch_type_category(remove_user_from_launch_type_category_request):
            try:
                session_id = get_jwt_identity()
                remove_user_from_launch_type_category_request.set_session_id(session_id)
                remove_user_from_launch_type_category_response = app.config[
                    "admin_ws_service"].rest_remove_user_from_launch_type_category(
                    remove_user_from_launch_type_category_request)
                return RestService.create_response(remove_user_from_launch_type_category_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRestrictedLaunchTypeCategories", endpoint="get_restricted_launch_type_categories",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRestrictedLaunchTypeCategoriesRequest)
        def get_restricted_launch_type_categories(get_restricted_launch_type_categories_request):
            try:
                session_id = get_jwt_identity()
                get_restricted_launch_type_categories_request.set_session_id(session_id)
                get_restricted_launch_type_categories_response = app.config[
                    "admin_ws_service"].rest_get_restricted_launch_type_categories(
                    get_restricted_launch_type_categories_request)
                return RestService.create_response(get_restricted_launch_type_categories_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getModuleHeaderElement", endpoint="get_module_header_element",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetModuleHeaderElementRequest)
        def get_module_header_element(get_module_header_element_request):
            try:
                session_id = get_jwt_identity()
                get_module_header_element_request.set_session_id(session_id)
                get_module_header_element_response = app.config["admin_ws_service"].rest_get_module_header_element(
                    get_module_header_element_request)
                return RestService.create_response(get_module_header_element_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getElementSeachCategories", endpoint="get_element_seach_categories",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetElementSeachCategoriesRequest)
        def get_element_seach_categories(get_element_seach_categories_request):
            try:
                session_id = get_jwt_identity()
                get_element_seach_categories_request.set_session_id(session_id)
                get_element_seach_categories_response = app.config[
                    "admin_ws_service"].rest_get_element_seach_categories(get_element_seach_categories_request)
                return RestService.create_response(get_element_seach_categories_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getModuleElementsFirst", endpoint="get_module_elements_first",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetModuleElementsFirstRequest)
        def get_module_elements_first(get_module_elements_first_request):
            try:
                session_id = get_jwt_identity()
                get_module_elements_first_request.set_session_id(session_id)
                get_module_elements_first_response = app.config["admin_ws_service"].rest_get_module_elements_first(
                    get_module_elements_first_request)
                return RestService.create_response(get_module_elements_first_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getModuleElementsNext", endpoint="get_module_elements_next",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetModuleElementsNextRequest)
        def get_module_elements_next(get_module_elements_next_request):
            try:
                session_id = get_jwt_identity()
                get_module_elements_next_request.set_session_id(session_id)
                get_module_elements_next_response = app.config["admin_ws_service"].rest_get_module_elements_next(
                    get_module_elements_next_request)
                return RestService.create_response(get_module_elements_next_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRulesFirst", endpoint="get_rules_first", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRulesFirstRequest)
        def get_rules_first(get_rules_first_request):
            try:
                session_id = get_jwt_identity()
                get_rules_first_request.set_session_id(session_id)
                get_rules_first_response = app.config["admin_ws_service"].rest_get_rules_first(get_rules_first_request)
                return RestService.create_response(get_rules_first_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRulesNext", endpoint="get_rules_next", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRulesNextRequest)
        def get_rules_next(get_rules_next_request):
            try:
                session_id = get_jwt_identity()
                get_rules_next_request.set_session_id(session_id)
                get_rules_next_response = app.config["admin_ws_service"].rest_get_rules_next(get_rules_next_request)
                return RestService.create_response(get_rules_next_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/addMembersToFirstTimeEnrollers", endpoint="add_members_to_first_time_enrollers",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.AddMembersToFirstTimeEnrollersRequest)
        def add_members_to_first_time_enrollers(add_members_to_first_time_enrollers_request):
            try:
                session_id = get_jwt_identity()
                add_members_to_first_time_enrollers_request.set_session_id(session_id)
                add_members_to_first_time_enrollers_response = app.config[
                    "admin_ws_service"].rest_add_members_to_first_time_enrollers(
                    add_members_to_first_time_enrollers_request)
                return RestService.create_response(add_members_to_first_time_enrollers_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRules", endpoint="get_rules", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRulesRequest)
        def get_rules(get_rules_request):
            try:
                session_id = get_jwt_identity()
                get_rules_request.set_session_id(session_id)
                get_rules_response = app.config["admin_ws_service"].rest_get_rules(get_rules_request)
                return RestService.create_response(get_rules_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/createRule", endpoint="create_rule", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.CreateRuleRequest)
        def create_rule(create_rule_request):
            try:
                session_id = get_jwt_identity()
                create_rule_request.set_session_id(session_id)
                create_rule_response = app.config["admin_ws_service"].rest_create_rule(create_rule_request)
                return RestService.create_response(create_rule_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/updateRule", endpoint="update_rule", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.UpdateRuleRequest)
        def update_rule(update_rule_request):
            try:
                session_id = get_jwt_identity()
                update_rule_request.set_session_id(session_id)
                update_rule_response = app.config["admin_ws_service"].rest_update_rule(update_rule_request)
                return RestService.create_response(update_rule_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/deleteRule", endpoint="delete_rule", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DeleteRuleRequest)
        def delete_rule(delete_rule_request):
            try:
                session_id = get_jwt_identity()
                delete_rule_request.set_session_id(session_id)
                delete_rule_response = app.config["admin_ws_service"].rest_delete_rule(delete_rule_request)
                return RestService.create_response(delete_rule_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRuleSpec", endpoint="get_rule_spec", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRuleSpecRequest)
        def get_rule_spec(get_rule_spec_request):
            try:
                session_id = get_jwt_identity()
                get_rule_spec_request.set_session_id(session_id)
                get_rule_spec_response = app.config["admin_ws_service"].rest_get_rule_spec(get_rule_spec_request)
                return RestService.create_response(get_rule_spec_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getRulesForElement", endpoint="get_rules_for_element", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetRulesForElementRequest)
        def get_rules_for_element(get_rules_for_element_request):
            try:
                session_id = get_jwt_identity()
                get_rules_for_element_request.set_session_id(session_id)
                get_rules_for_element_response = app.config["admin_ws_service"].rest_get_rules_for_element(
                    get_rules_for_element_request)
                return RestService.create_response(get_rules_for_element_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getReports", endpoint="get_reports", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetReportsRequest)
        def get_reports(get_reports_request):
            try:
                session_id = get_jwt_identity()
                get_reports_request.set_session_id(session_id)
                get_reports_response = app.config["admin_ws_service"].rest_get_reports(get_reports_request)
                return RestService.create_response(get_reports_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getReportData", endpoint="get_report_data", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetReportDataRequest)
        def get_report_data(get_report_data_request):
            try:
                session_id = get_jwt_identity()
                get_report_data_request.set_session_id(session_id)
                get_report_data_response = app.config["admin_ws_service"].rest_get_report_data(get_report_data_request)
                return RestService.create_response(get_report_data_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getConfigSpec", endpoint="get_config_spec", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetConfigSpecRequest)
        def get_config_spec(get_config_spec_request):
            try:
                session_id = get_jwt_identity()
                get_config_spec_request.set_session_id(session_id)
                get_config_spec_response = app.config["admin_ws_service"].rest_get_config_spec(get_config_spec_request)
                return RestService.create_response(get_config_spec_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getConfigTemplate", endpoint="get_config_template", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetConfigTemplateRequest)
        def get_config_template(get_config_template_request):
            try:
                session_id = get_jwt_identity()
                get_config_template_request.set_session_id(session_id)
                get_config_template_response = app.config["admin_ws_service"].rest_get_config_template(
                    get_config_template_request)
                return RestService.create_response(get_config_template_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getElementConfigTemplate", endpoint="get_element_config_template",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetElementConfigTemplateRequest)
        def get_element_config_template(get_element_config_template_request):
            try:
                session_id = get_jwt_identity()
                get_element_config_template_request.set_session_id(session_id)
                get_element_config_template_response = app.config["admin_ws_service"].rest_get_element_config_template(
                    get_element_config_template_request)
                return RestService.create_response(get_element_config_template_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getReportSpec", endpoint="get_report_spec", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetReportSpecRequest)
        def get_report_spec(get_report_spec_request):
            try:
                session_id = get_jwt_identity()
                get_report_spec_request.set_session_id(session_id)
                get_report_spec_response = app.config["admin_ws_service"].rest_get_report_spec(get_report_spec_request)
                return RestService.create_response(get_report_spec_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/createConfigTemplateRow", endpoint="create_config_template_row",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.CreateConfigTemplateRowRequest)
        def create_config_template_row(create_config_template_row_request):
            try:
                session_id = get_jwt_identity()
                create_config_template_row_request.set_session_id(session_id)
                create_config_template_row_response = app.config["admin_ws_service"].rest_create_config_template_row(
                    create_config_template_row_request)
                return RestService.create_response(create_config_template_row_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/updateConfigTemplateRow", endpoint="update_config_template_row",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.UpdateConfigTemplateRowRequest)
        def update_config_template_row(update_config_template_row_request):
            try:
                session_id = get_jwt_identity()
                update_config_template_row_request.set_session_id(session_id)
                update_config_template_row_response = app.config["admin_ws_service"].rest_update_config_template_row(
                    update_config_template_row_request)
                return RestService.create_response(update_config_template_row_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/deleteConfigElementRow", endpoint="delete_config_element_row",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DeleteConfigElementRowRequest)
        def delete_config_element_row(delete_config_element_row_request):
            try:
                session_id = get_jwt_identity()
                delete_config_element_row_request.set_session_id(session_id)
                delete_config_element_row_response = app.config["admin_ws_service"].rest_delete_config_element_row(
                    delete_config_element_row_request)
                return RestService.create_response(delete_config_element_row_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/enrollDevice", endpoint="enroll_device", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.EnrollDeviceRequest)
        def enroll_device(enroll_device_request):
            try:
                session_id = get_jwt_identity()
                enroll_device_request.set_session_id(session_id)
                enroll_device_response = app.config["admin_ws_service"].rest_enroll_device(enroll_device_request)
                return RestService.create_response(enroll_device_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/enrollDeviceToUser", endpoint="enroll_device_to_user", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.EnrollDeviceToUserRequest)
        def enroll_device_to_user(enroll_device_to_user_request):
            try:
                session_id = get_jwt_identity()
                enroll_device_to_user_request.set_session_id(session_id)
                enroll_device_to_user_response = app.config["admin_ws_service"].rest_enroll_device_to_user(
                    enroll_device_to_user_request)
                return RestService.create_response(enroll_device_to_user_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getEnrollmentRequests", endpoint="get_enrollment_requests",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetEnrollmentRequestsRequest)
        def get_enrollment_requests(get_enrollment_requests_request):
            try:
                session_id = get_jwt_identity()
                get_enrollment_requests_request.set_session_id(session_id)
                get_enrollment_requests_response = app.config["admin_ws_service"].rest_get_enrollment_requests(
                    get_enrollment_requests_request)
                return RestService.create_response(get_enrollment_requests_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/respondToEnrollmentRequest", endpoint="respond_to_enrollment_request",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.RespondToEnrollmentRequestRequest)
        def respond_to_enrollment_request(respond_to_enrollment_request_request):
            try:
                session_id = get_jwt_identity()
                respond_to_enrollment_request_request.set_session_id(session_id)
                respond_to_enrollment_request_response = app.config[
                    "admin_ws_service"].rest_respond_to_enrollment_request(respond_to_enrollment_request_request)
                return RestService.create_response(respond_to_enrollment_request_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/deployGetKnownSecretClient", endpoint="deploy_get_known_secret_client",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DeployGetKnownSecretClientRequest)
        def deploy_get_known_secret_client(deploy_get_known_secret_client_request):
            try:
                session_id = get_jwt_identity()
                deploy_get_known_secret_client_request.set_session_id(session_id)
                deploy_get_known_secret_client_response = app.config[
                    "admin_ws_service"].rest_deploy_get_known_secret_client(deploy_get_known_secret_client_request)
                return RestService.create_response(deploy_get_known_secret_client_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/deployGetServers", endpoint="deploy_get_servers", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DeployGetServersRequest)
        def deploy_get_servers(deploy_get_servers_request):
            try:
                session_id = get_jwt_identity()
                deploy_get_servers_request.set_session_id(session_id)
                deploy_get_servers_response = app.config["admin_ws_service"].rest_deploy_get_servers(
                    deploy_get_servers_request)
                return RestService.create_response(deploy_get_servers_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/deployGenerateKeyPair", endpoint="deploy_generate_key_pair",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DeployGenerateKeyPairRequest)
        def deploy_generate_key_pair(deploy_generate_key_pair_request):
            try:
                session_id = get_jwt_identity()
                deploy_generate_key_pair_request.set_session_id(session_id)
                deploy_generate_key_pair_response = app.config["admin_ws_service"].rest_deploy_generate_key_pair(
                    deploy_generate_key_pair_request)
                return RestService.create_response(deploy_generate_key_pair_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getGPMCollections", endpoint="get_gpm_collections", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetGPMCollectionsRequest)
        def get_gpm_collections(get_gpm_collections_request):
            try:
                session_id = get_jwt_identity()
                get_gpm_collections_request.set_session_id(session_id)
                get_gpm_collections_response = app.config["admin_ws_service"].rest_get_gpm_collections(
                    get_gpm_collections_request)
                return RestService.create_response(get_gpm_collections_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/downloadGPMStart", endpoint="download_gpm_start", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DownloadGPMStartRequest)
        def download_gpm_start(download_gpm_start_request):
            try:
                session_id = get_jwt_identity()
                download_gpm_start_request.set_session_id(session_id)
                download_gpm_start_response = app.config["admin_ws_service"].rest_download_gpm_start(
                    download_gpm_start_request)
                return RestService.create_response(download_gpm_start_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/downloadGPMGetChunk", endpoint="download_gpm_get_chunk", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DownloadGPMGetChunkRequest)
        def download_gpm_get_chunk(download_gpm_get_chunk_request):
            try:
                session_id = get_jwt_identity()
                download_gpm_get_chunk_request.set_session_id(session_id)
                download_gpm_get_chunk_response = app.config["admin_ws_service"].rest_download_gpm_get_chunk(
                    download_gpm_get_chunk_request)
                return RestService.create_response(download_gpm_get_chunk_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/downloadGPMStop", endpoint="download_gpm_stop", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.DownloadGPMStopRequest)
        def download_gpm_stop(download_gpm_stop_request):
            try:
                session_id = get_jwt_identity()
                download_gpm_stop_request.set_session_id(session_id)
                download_gpm_stop_response = app.config["admin_ws_service"].rest_download_gpm_stop(
                    download_gpm_stop_request)
                return RestService.create_response(download_gpm_stop_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/generateGPMSignature", endpoint="generate_gpm_signature", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GenerateGPMSignatureRequest)
        def generate_gpm_signature(generate_gpm_signature_request):
            try:
                session_id = get_jwt_identity()
                generate_gpm_signature_request.set_session_id(session_id)
                generate_gpm_signature_response = app.config["admin_ws_service"].rest_generate_gpm_signature(
                    generate_gpm_signature_request)
                return RestService.create_response(generate_gpm_signature_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/portScan", endpoint="port_scan", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.PortScanRequest)
        def port_scan(port_scan_request):
            try:
                session_id = get_jwt_identity()
                port_scan_request.set_session_id(session_id)
                port_scan_response = app.config["admin_ws_service"].rest_port_scan(port_scan_request)
                return RestService.create_response(port_scan_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getEntityTypeForPluginType", endpoint="get_entity_type_for_plugin_type",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetEntityTypeForPluginTypeRequest)
        def get_entity_type_for_plugin_type(get_entity_type_for_plugin_type_request):
            try:
                session_id = get_jwt_identity()
                get_entity_type_for_plugin_type_request.set_session_id(session_id)
                get_entity_type_for_plugin_type_response = app.config[
                    "admin_ws_service"].rest_get_entity_type_for_plugin_type(get_entity_type_for_plugin_type_request)
                return RestService.create_response(get_entity_type_for_plugin_type_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getBasicEntityTypes", endpoint="get_basic_entity_types", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetBasicEntityTypesRequest)
        def get_basic_entity_types(get_basic_entity_types_request):
            try:
                session_id = get_jwt_identity()
                get_basic_entity_types_request.set_session_id(session_id)
                get_basic_entity_types_response = app.config["admin_ws_service"].rest_get_basic_entity_types(
                    get_basic_entity_types_request)
                return RestService.create_response(get_basic_entity_types_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getTokenInfo", endpoint="get_token_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetTokenInfoRequest)
        def get_token_info(get_token_info_request):
            try:
                session_id = get_jwt_identity()
                get_token_info_request.set_session_id(session_id)
                get_token_info_response = app.config["admin_ws_service"].rest_get_token_info(get_token_info_request)
                return RestService.create_response(get_token_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getMenuItems", endpoint="get_menu_items", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetMenuItemsRequest)
        def get_menu_items(get_menu_items_request):
            try:
                session_id = get_jwt_identity()
                get_menu_items_request.set_session_id(session_id)
                get_menu_items_response = app.config["admin_ws_service"].rest_get_menu_items(get_menu_items_request)
                return RestService.create_response(get_menu_items_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getMenu", endpoint="get_menu", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetMenuRequest)
        def get_menu(get_menu_request):
            try:
                session_id = get_jwt_identity()
                get_menu_request.set_session_id(session_id)
                get_menu_response = app.config["admin_ws_service"].rest_get_menu(get_menu_request)
                return RestService.create_response(get_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/addItemToMenu", endpoint="add_item_to_menu", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.AddItemToMenuRequest)
        def add_item_to_menu(add_item_to_menu_request):
            try:
                session_id = get_jwt_identity()
                add_item_to_menu_request.set_session_id(session_id)
                add_item_to_menu_response = app.config["admin_ws_service"].rest_add_item_to_menu(
                    add_item_to_menu_request)
                return RestService.create_response(add_item_to_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/addElementToMenu", endpoint="add_element_to_menu", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.AddElementToMenuRequest)
        def add_element_to_menu(add_element_to_menu_request):
            try:
                session_id = get_jwt_identity()
                add_element_to_menu_request.set_session_id(session_id)
                add_element_to_menu_response = app.config["admin_ws_service"].rest_add_element_to_menu(
                    add_element_to_menu_request)
                return RestService.create_response(add_element_to_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/moveItemToMenu", endpoint="move_item_to_menu", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.MoveItemToMenuRequest)
        def move_item_to_menu(move_item_to_menu_request):
            try:
                session_id = get_jwt_identity()
                move_item_to_menu_request.set_session_id(session_id)
                move_item_to_menu_response = app.config["admin_ws_service"].rest_move_item_to_menu(
                    move_item_to_menu_request)
                return RestService.create_response(move_item_to_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/removeItemFromMenu", endpoint="remove_item_from_menu", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.RemoveItemFromMenuRequest)
        def remove_item_from_menu(remove_item_from_menu_request):
            try:
                session_id = get_jwt_identity()
                remove_item_from_menu_request.set_session_id(session_id)
                remove_item_from_menu_response = app.config["admin_ws_service"].rest_remove_item_from_menu(
                    remove_item_from_menu_request)
                return RestService.create_response(remove_item_from_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/ping", endpoint="ping", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.PingRequest)
        def ping(ping_request):
            try:
                session_id = get_jwt_identity()
                ping_request.set_session_id(session_id)
                ping_response = app.config["admin_ws_service"].rest_ping(ping_request)
                return RestService.create_response(ping_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gOnServiceGetList", endpoint="g_on_service_get_list", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GOnServiceGetListRequest)
        def g_on_service_get_list(g_on_service_get_list_request):
            try:
                session_id = get_jwt_identity()
                g_on_service_get_list_request.set_session_id(session_id)
                g_on_service_get_list_response = app.config["admin_ws_service"].rest_g_on_service_get_list(
                    g_on_service_get_list_request)
                return RestService.create_response(g_on_service_get_list_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gOnServiceRestart", endpoint="g_on_service_restart", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GOnServiceRestartRequest)
        def g_on_service_restart(g_on_service_restart_request):
            try:
                session_id = get_jwt_identity()
                g_on_service_restart_request.set_session_id(session_id)
                g_on_service_restart_response = app.config["admin_ws_service"].rest_g_on_service_restart(
                    g_on_service_restart_request)
                return RestService.create_response(g_on_service_restart_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gOnServiceStop", endpoint="g_on_service_stop", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GOnServiceStopRequest)
        def g_on_service_stop(g_on_service_stop_request):
            try:
                session_id = get_jwt_identity()
                g_on_service_stop_request.set_session_id(session_id)
                g_on_service_stop_response = app.config["admin_ws_service"].rest_g_on_service_stop(
                    g_on_service_stop_request)
                return RestService.create_response(g_on_service_stop_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gatewayIsUserOnline", endpoint="gateway_is_user_online", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GatewayIsUserOnlineRequest)
        def gateway_is_user_online(gateway_is_user_online_request):
            try:
                session_id = get_jwt_identity()
                gateway_is_user_online_request.set_session_id(session_id)
                gateway_is_user_online_response = app.config["admin_ws_service"].rest_gateway_is_user_online(
                    gateway_is_user_online_request)
                return RestService.create_response(gateway_is_user_online_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getGatewaySessions", endpoint="get_gateway_sessions", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetGatewaySessionsRequest)
        def get_gateway_sessions(get_gateway_sessions_request):
            try:
                session_id = get_jwt_identity()
                get_gateway_sessions_request.set_session_id(session_id)
                get_gateway_sessions_response = app.config["admin_ws_service"].rest_get_gateway_sessions(
                    get_gateway_sessions_request)
                return RestService.create_response(get_gateway_sessions_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getGatewaySessionsNext", endpoint="get_gateway_sessions_next", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetGatewaySessionsNextRequest)
        def get_gateway_sessions_next(get_gateway_sessions_next_request):
            try:
                session_id = get_jwt_identity()
                get_gateway_sessions_next_request.set_session_id(session_id)
                get_gateway_sessions_next_response = app.config["admin_ws_service"].rest_get_gateway_sessions_next(
                    get_gateway_sessions_next_request)
                return RestService.create_response(get_gateway_sessions_next_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gatewaySessionClose", endpoint="gateway_session_close", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GatewaySessionCloseRequest)
        def gateway_session_close(gateway_session_close_request):
            try:
                session_id = get_jwt_identity()
                gateway_session_close_request.set_session_id(session_id)
                gateway_session_close_response = app.config["admin_ws_service"].rest_gateway_session_close(
                    gateway_session_close_request)
                return RestService.create_response(gateway_session_close_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/gatewaySessionRecalculateMenu", endpoint="gateway_session_recalculate_menu",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GatewaySessionRecalculateMenuRequest)
        def gateway_session_recalculate_menu(gateway_session_recalculate_menu_request):
            try:
                session_id = get_jwt_identity()
                gateway_session_recalculate_menu_request.set_session_id(session_id)
                gateway_session_recalculate_menu_response = app.config[
                    "admin_ws_service"].rest_gateway_session_recalculate_menu(gateway_session_recalculate_menu_request)
                return RestService.create_response(gateway_session_recalculate_menu_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getElementsStop", endpoint="get_elements_stop", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetElementsStopRequest)
        def get_elements_stop(get_elements_stop_request):
            try:
                session_id = get_jwt_identity()
                get_elements_stop_request.set_session_id(session_id)
                get_elements_stop_response = app.config["admin_ws_service"].rest_get_elements_stop(
                    get_elements_stop_request)
                return RestService.create_response(get_elements_stop_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/getLicenseInfo", endpoint="get_license_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetLicenseInfoRequest)
        def get_license_info(get_license_info_request):
            try:
                session_id = get_jwt_identity()
                get_license_info_request.set_session_id(session_id)
                get_license_info_response = app.config["admin_ws_service"].rest_get_license_info(
                    get_license_info_request)
                return RestService.create_response(get_license_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/admin/setLicense", endpoint="set_license", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.SetLicenseRequest)
        def set_license(set_license_request):
            try:
                session_id = get_jwt_identity()
                set_license_request.set_session_id(session_id)
                set_license_response = app.config["admin_ws_service"].rest_set_license(set_license_request)
                return RestService.create_response(set_license_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        app.run(threaded=False, debug=True, use_reloader=False, host=admin_ws_service.get_management_ws_ip_host,
                port=admin_ws_service.get_management_ws_ip_port)

    @staticmethod
    def start_server_config_wsgi(server_config_ws_service):
        app = Flask("ServerConfig")
        app.config["server_config_ws_service"] = server_config_ws_service

        # Setup the Flask-JWT-Extended extension
        app.config[
            "JWT_SECRET_KEY"] = "C1vqmjq24g9IPOAjz5xvApiOhBGBonP4W7rzQtX7pQcBOCXnmn3jvAwHlM5ccyw6tF5zUHOnF58b3DAhISaA46JVuZmAiDjX9L71390nRVpvnywjaZEdfaHISizx62GxReMDYEORXMIuqyKJUCZtoBqX1em4vtukCvkE1keIOQtzs8K6tATNpK0hED4EkK17gROYQPVDoI8l25tyehB5otY7HQdkfgW76N3OQ9fbT6Vrjv2ApY4yq1SpT1PazRQqCMnokM1eD4l6IIByWs3qh3EkKYPWHRGDHOUNQzwTfht45X2V7EKRRJrbPHoT5lycMQb2vPBXAgXb3MYJZobmbcZo7XDEHMRrXJ9h1xa44ED9LBJEz1ytRqj7vjIwRSHd96IhUZgynYjkr13hfTbDK99CKnUCa6VOxTEWFCDNAJQZ5a6FS41f7Kq26P6uIuKCb7DlD89xfwm0IhGvUOJF7UJU3qIk91DUDWY4ruCoaZ0ZFLLBksTDii0XABZeG7UiLUahq4kISsKV52ywGP9M1qqxD3pp6M1aut39c137a3epwyyei4UI3MwjJ1L9ovHBeP0ceL0CVx1zCKe60LuRupgKdhGRZ44QoWpzFShE2CnxZQEcsWnJz1QCC1T3GyqXOgngkSNMx9bcxXSTY35U5exYMKmGpnUcqNNv8sPnznZC3KjXKEq006fmahRvHbMWqaPB5s8JjAZ6YjD8069tHQDbs51bYww3qksPxmG3pYUZpdIcEvPUIBJSKLR7cE0DJ27baADdpnPqlPXNllH3wcNNpcc4CXDyMBEvAUjKky49U3mb7Vg7c9soBJDwsYKCQm6Kw8vRuFIyouMJ0XONeQVuU50oNPqby4Z5c5yPuP0zkzrOoeTUnyFWtdET1VeGbZ7dhQTPKzxTttu7NEMrGuNt0TurQmOAd86AV7gPD12ZLV1EKPcXKlNA0Ifv5ZFxCVNxQ7cazuXtOuyvhBpMzHFsIUTi4RN0jmjt15J7hbzVi9MfWKcwtchfk0sllWfydUGs9YlupAmoZVlxcaCQ3Yj5AL320fxTtcaTp3TSrwsgmzsY9Is0xJGYRDt1MMLWl2S1fDQGQ63PvNfQnYbZQDQaSrXgKD80ma50OssSPQItf9UD4uvCgLJ9POrR1guqZ4sIdJpjsiuoIXWM7CA6viOSSIxV076j7aljPuK8gp0R3KX6eoWBodQTfPltYBlXbd70loi1nnuMEO1pDKmzdSbt1OIfdimIS5RdolJkG2x6MTRYacKRuXqvMFu8jt74"

        jwt = JWTManager(app)

        @app.after_request
        def finalize_response(response):
            response.headers.set("Content-Type", "application/json")
            response.headers.set("Access-Control-Allow-Origin", "http://localhost:4200")
            response.headers.set("Access-Control-Allow-Headers", "Content-Type, Authorization")
            response.headers.set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
            return response

        @app.route("/serverConfig/login", endpoint="login", methods=["POST", "PUT", "DELETE"])
        @RestService.deserialize(RestApi.ServerConfig.LoginRequest)
        def login(login_request):
            try:
                login_response = app.config["server_config_ws_service"].rest_login(
                    login_request)
                if login_response.get_session_id is not None:
                    response_code = 200
                    login_response.set_session_id(create_access_token(
                        identity=login_response.get_session_id,
                        expires_delta=app.config["server_config_ws_service"].get_session_timeout))
                else:
                    response_code = 401
                return RestService.create_response(login_response, response_code)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/logout", endpoint="logout", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.LogoutRequest)
        def logout(logout_request):
            try:
                session_id = get_jwt_identity()
                logout_request.set_session_id(session_id)
                logout_response = app.config["server_config_ws_service"].rest_logout(logout_request)
                return RestService.create_response(logout_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getJobInfo", endpoint="get_job_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetJobInfoRequest)
        def get_job_info(get_job_info_request):
            try:
                session_id = get_jwt_identity()
                get_job_info_request.set_session_id(session_id)
                get_job_info_response = app.config["server_config_ws_service"].rest_get_job_info(get_job_info_request)
                return RestService.create_response(get_job_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/cancelJob", endpoint="cancel_job", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.CancelJobRequest)
        def cancel_job(cancel_job_request):
            try:
                session_id = get_jwt_identity()
                cancel_job_request.set_session_id(session_id)
                cancel_job_response = app.config["server_config_ws_service"].rest_cancel_job(cancel_job_request)
                return RestService.create_response(cancel_job_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getStatus", endpoint="get_status", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetStatusRequest)
        def get_status(get_status_request):
            try:
                session_id = get_jwt_identity()
                get_status_request.set_session_id(session_id)
                get_status_response = app.config["server_config_ws_service"].rest_get_status(get_status_request)
                return RestService.create_response(get_status_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobGenerateKnownsecrets", endpoint="start_job_generate_knownsecrets",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobGenerateKnownsecretsRequest)
        def start_job_generate_knownsecrets(start_job_generate_knownsecrets_request):
            try:
                session_id = get_jwt_identity()
                start_job_generate_knownsecrets_request.set_session_id(session_id)
                start_job_generate_knownsecrets_response = app.config[
                    "server_config_ws_service"].rest_start_job_generate_knownsecrets(
                    start_job_generate_knownsecrets_request)
                return RestService.create_response(start_job_generate_knownsecrets_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobGenerateGPMS", endpoint="start_job_generate_gpms", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobGenerateGPMSRequest)
        def start_job_generate_gpms(start_job_generate_gpms_request):
            try:
                session_id = get_jwt_identity()
                start_job_generate_gpms_request.set_session_id(session_id)
                start_job_generate_gpms_response = app.config["server_config_ws_service"].rest_start_job_generate_gpms(
                    start_job_generate_gpms_request)
                return RestService.create_response(start_job_generate_gpms_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobGenerateDemodata", endpoint="start_job_generate_demodata",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobGenerateDemodataRequest)
        def start_job_generate_demodata(start_job_generate_demodata_request):
            try:
                session_id = get_jwt_identity()
                start_job_generate_demodata_request.set_session_id(session_id)
                start_job_generate_demodata_response = app.config[
                    "server_config_ws_service"].rest_start_job_generate_demodata(start_job_generate_demodata_request)
                return RestService.create_response(start_job_generate_demodata_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobInstallServices", endpoint="start_job_install_services", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobInstallServicesRequest)
        def start_job_install_services(start_job_install_services_request):
            try:
                session_id = get_jwt_identity()
                start_job_install_services_request.set_session_id(session_id)
                start_job_install_services_response = app.config[
                    "server_config_ws_service"].rest_start_job_install_services(start_job_install_services_request)
                return RestService.create_response(start_job_install_services_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobGenerateSupportPackage", endpoint="start_job_generate_support_package",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobGenerateSupportPackageRequest)
        def start_job_generate_support_package(start_job_generate_support_package_request):
            try:
                session_id = get_jwt_identity()
                start_job_generate_support_package_request.set_session_id(session_id)
                start_job_generate_support_package_response = app.config[
                    "server_config_ws_service"].rest_start_job_generate_support_package(
                    start_job_generate_support_package_request)
                return RestService.create_response(start_job_generate_support_package_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobPrepareInstallation", endpoint="start_job_prepare_installation",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobPrepareInstallationRequest)
        def start_job_prepare_installation(start_job_prepare_installation_request):
            try:
                session_id = get_jwt_identity()
                start_job_prepare_installation_request.set_session_id(session_id)
                start_job_prepare_installation_response = app.config[
                    "server_config_ws_service"].rest_start_job_prepare_installation(
                    start_job_prepare_installation_request)
                return RestService.create_response(start_job_prepare_installation_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobFinalizeInstallation", endpoint="start_job_finalize_installation",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobFinalizeInstallationRequest)
        def start_job_finalize_installation(start_job_finalize_installation_request):
            try:
                session_id = get_jwt_identity()
                start_job_finalize_installation_request.set_session_id(session_id)
                start_job_finalize_installation_response = app.config[
                    "server_config_ws_service"].rest_start_job_finalize_installation(
                    start_job_finalize_installation_request)
                return RestService.create_response(start_job_finalize_installation_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobPrepareChange", endpoint="start_job_prepare_change", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobPrepareChangeRequest)
        def start_job_prepare_change(start_job_prepare_change_request):
            try:
                session_id = get_jwt_identity()
                start_job_prepare_change_request.set_session_id(session_id)
                start_job_prepare_change_response = app.config[
                    "server_config_ws_service"].rest_start_job_prepare_change(start_job_prepare_change_request)
                return RestService.create_response(start_job_prepare_change_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobFinalizeChange", endpoint="start_job_finalize_change", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobFinalizeChangeRequest)
        def start_job_finalize_change(start_job_finalize_change_request):
            try:
                session_id = get_jwt_identity()
                start_job_finalize_change_request.set_session_id(session_id)
                start_job_finalize_change_response = app.config[
                    "server_config_ws_service"].rest_start_job_finalize_change(start_job_finalize_change_request)
                return RestService.create_response(start_job_finalize_change_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobPrepareUpgrade", endpoint="start_job_prepare_upgrade", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobPrepareUpgradeRequest)
        def start_job_prepare_upgrade(start_job_prepare_upgrade_request):
            try:
                session_id = get_jwt_identity()
                start_job_prepare_upgrade_request.set_session_id(session_id)
                start_job_prepare_upgrade_response = app.config[
                    "server_config_ws_service"].rest_start_job_prepare_upgrade(start_job_prepare_upgrade_request)
                return RestService.create_response(start_job_prepare_upgrade_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/startJobFinalizeUpgrade", endpoint="start_job_finalize_upgrade", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.StartJobFinalizeUpgradeRequest)
        def start_job_finalize_upgrade(start_job_finalize_upgrade_request):
            try:
                session_id = get_jwt_identity()
                start_job_finalize_upgrade_request.set_session_id(session_id)
                start_job_finalize_upgrade_response = app.config[
                    "server_config_ws_service"].rest_start_job_finalize_upgrade(start_job_finalize_upgrade_request)
                return RestService.create_response(start_job_finalize_upgrade_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getFirstConfigSpecification", endpoint="get_first_config_specification",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetFirstConfigSpecificationRequest)
        def get_first_config_specification(get_first_config_specification_request):
            try:
                session_id = get_jwt_identity()
                get_first_config_specification_request.set_session_id(session_id)
                get_first_config_specification_response = app.config[
                    "server_config_ws_service"].rest_get_first_config_specification(
                    get_first_config_specification_request)
                return RestService.create_response(get_first_config_specification_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getNextConfigSpecification", endpoint="get_next_config_specification",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetNextConfigSpecificationRequest)
        def get_next_config_specification(get_next_config_specification_request):
            try:
                session_id = get_jwt_identity()
                get_next_config_specification_request.set_session_id(session_id)
                get_next_config_specification_response = app.config[
                    "server_config_ws_service"].rest_get_next_config_specification(
                    get_next_config_specification_request)
                return RestService.create_response(get_next_config_specification_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/saveConfigSpecification", endpoint="save_config_specification", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.SaveConfigSpecificationRequest)
        def save_config_specification(save_config_specification_request):
            try:
                session_id = get_jwt_identity()
                save_config_specification_request.set_session_id(session_id)
                save_config_specification_response = app.config[
                    "server_config_ws_service"].rest_save_config_specification(save_config_specification_request)
                return RestService.create_response(save_config_specification_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/reloadConfigSpecification", endpoint="reload_config_specification",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.ReloadConfigSpecificationRequest)
        def reload_config_specification(reload_config_specification_request):
            try:
                session_id = get_jwt_identity()
                reload_config_specification_request.set_session_id(session_id)
                reload_config_specification_response = app.config[
                    "server_config_ws_service"].rest_reload_config_specification(reload_config_specification_request)
                return RestService.create_response(reload_config_specification_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getConfigTemplate", endpoint="get_config_template", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetConfigTemplateRequest)
        def get_config_template(get_config_template_request):
            try:
                session_id = get_jwt_identity()
                get_config_template_request.set_session_id(session_id)
                get_config_template_response = app.config["server_config_ws_service"].rest_get_config_template(
                    get_config_template_request)
                return RestService.create_response(get_config_template_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/saveConfigTemplate", endpoint="save_config_template", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.SaveConfigTemplateRequest)
        def save_config_template(save_config_template_request):
            try:
                session_id = get_jwt_identity()
                save_config_template_request.set_session_id(session_id)
                save_config_template_response = app.config["server_config_ws_service"].rest_save_config_template(
                    save_config_template_request)
                return RestService.create_response(save_config_template_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/deleteConfigTemplate", endpoint="delete_config_template", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.DeleteConfigTemplateRequest)
        def delete_config_template(delete_config_template_request):
            try:
                session_id = get_jwt_identity()
                delete_config_template_request.set_session_id(session_id)
                delete_config_template_response = app.config["server_config_ws_service"].rest_delete_config_template(
                    delete_config_template_request)
                return RestService.create_response(delete_config_template_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/testConfigSpecification", endpoint="test_config_specification", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.TestConfigSpecificationRequest)
        def test_config_specification(test_config_specification_request):
            try:
                session_id = get_jwt_identity()
                test_config_specification_request.set_session_id(session_id)
                test_config_specification_response = app.config[
                    "server_config_ws_service"].rest_test_config_specification(test_config_specification_request)
                return RestService.create_response(test_config_specification_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getServices", endpoint="get_services", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetServicesRequest)
        def get_services(get_services_request):
            try:
                session_id = get_jwt_identity()
                get_services_request.set_session_id(session_id)
                get_services_response = app.config["server_config_ws_service"].rest_get_services(get_services_request)
                return RestService.create_response(get_services_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getGOnSystems", endpoint="get_g_on_systems", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GetGOnSystemsRequest)
        def get_g_on_systems(get_g_on_systems_request):
            try:
                session_id = get_jwt_identity()
                get_g_on_systems_request.set_session_id(session_id)
                get_g_on_systems_response = app.config["server_config_ws_service"].rest_get_g_on_systems(
                    get_g_on_systems_request)
                return RestService.create_response(get_g_on_systems_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/getLicenseInfo", endpoint="get_license_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.GetLicenseInfoRequest)
        def get_license_info(get_license_info_request):
            try:
                session_id = get_jwt_identity()
                get_license_info_request.set_session_id(session_id)
                get_license_info_response = app.config["server_config_ws_service"].rest_get_license_info(
                    get_license_info_request)
                return RestService.create_response(get_license_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/setLicense", endpoint="set_license", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.SetLicenseRequest)
        def set_license(set_license_request):
            try:
                session_id = get_jwt_identity()
                set_license_request.set_session_id(session_id)
                set_license_response = app.config["server_config_ws_service"].rest_set_license(set_license_request)
                return RestService.create_response(set_license_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/ping", endpoint="ping", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.Admin.PingRequest)
        def ping(ping_request):
            try:
                session_id = get_jwt_identity()
                ping_request.set_session_id(session_id)
                ping_response = app.config["server_config_ws_service"].rest_ping(ping_request)
                return RestService.create_response(ping_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/gOnManagementServiceGetStatus", endpoint="g_on_management_service_get_status",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GOnManagementServiceGetStatusRequest)
        def g_on_management_service_get_status(g_on_management_service_get_status_request):
            try:
                session_id = get_jwt_identity()
                g_on_management_service_get_status_request.set_session_id(session_id)
                g_on_management_service_get_status_response = app.config[
                    "server_config_ws_service"].rest_g_on_management_service_get_status(
                    g_on_management_service_get_status_request)
                return RestService.create_response(g_on_management_service_get_status_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/serverConfig/gOnManagementServiceControl", endpoint="g_on_management_service_control",
                   methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.ServerConfig.GOnManagementServiceControlRequest)
        def g_on_management_service_control(g_on_management_service_control_request):
            try:
                session_id = get_jwt_identity()
                g_on_management_service_control_request.set_session_id(session_id)
                g_on_management_service_control_response = app.config[
                    "server_config_ws_service"].rest_g_on_management_service_control(
                    g_on_management_service_control_request)
                return RestService.create_response(g_on_management_service_control_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        app.run(threaded=False, debug=True, use_reloader=False,
                host=server_config_ws_service.get_server_config_ws_ip_host,
                port=server_config_ws_service.get_server_config_ws_ip_port)

    @staticmethod
    def start_admin_deploy_wsgi(admin_deploy_ws_service):
        app = Flask("AdminDeploy")
        app.config["admin_deploy_ws_service"] = admin_deploy_ws_service

        # Setup the Flask-JWT-Extended extension
        app.config[
            "JWT_SECRET_KEY"] = "C1vqmjq24g9IPOAjz5xvApiOhBGBonP4W7rzQtX7pQcBOCXnmn3jvAwHlM5ccyw6tF5zUHOnF58b3DAhISaA46JVuZmAiDjX9L71390nRVpvnywjaZEdfaHISizx62GxReMDYEORXMIuqyKJUCZtoBqX1em4vtukCvkE1keIOQtzs8K6tATNpK0hED4EkK17gROYQPVDoI8l25tyehB5otY7HQdkfgW76N3OQ9fbT6Vrjv2ApY4yq1SpT1PazRQqCMnokM1eD4l6IIByWs3qh3EkKYPWHRGDHOUNQzwTfht45X2V7EKRRJrbPHoT5lycMQb2vPBXAgXb3MYJZobmbcZo7XDEHMRrXJ9h1xa44ED9LBJEz1ytRqj7vjIwRSHd96IhUZgynYjkr13hfTbDK99CKnUCa6VOxTEWFCDNAJQZ5a6FS41f7Kq26P6uIuKCb7DlD89xfwm0IhGvUOJF7UJU3qIk91DUDWY4ruCoaZ0ZFLLBksTDii0XABZeG7UiLUahq4kISsKV52ywGP9M1qqxD3pp6M1aut39c137a3epwyyei4UI3MwjJ1L9ovHBeP0ceL0CVx1zCKe60LuRupgKdhGRZ44QoWpzFShE2CnxZQEcsWnJz1QCC1T3GyqXOgngkSNMx9bcxXSTY35U5exYMKmGpnUcqNNv8sPnznZC3KjXKEq006fmahRvHbMWqaPB5s8JjAZ6YjD8069tHQDbs51bYww3qksPxmG3pYUZpdIcEvPUIBJSKLR7cE0DJ27baADdpnPqlPXNllH3wcNNpcc4CXDyMBEvAUjKky49U3mb7Vg7c9soBJDwsYKCQm6Kw8vRuFIyouMJ0XONeQVuU50oNPqby4Z5c5yPuP0zkzrOoeTUnyFWtdET1VeGbZ7dhQTPKzxTttu7NEMrGuNt0TurQmOAd86AV7gPD12ZLV1EKPcXKlNA0Ifv5ZFxCVNxQ7cazuXtOuyvhBpMzHFsIUTi4RN0jmjt15J7hbzVi9MfWKcwtchfk0sllWfydUGs9YlupAmoZVlxcaCQ3Yj5AL320fxTtcaTp3TSrwsgmzsY9Is0xJGYRDt1MMLWl2S1fDQGQ63PvNfQnYbZQDQaSrXgKD80ma50OssSPQItf9UD4uvCgLJ9POrR1guqZ4sIdJpjsiuoIXWM7CA6viOSSIxV076j7aljPuK8gp0R3KX6eoWBodQTfPltYBlXbd70loi1nnuMEO1pDKmzdSbt1OIfdimIS5RdolJkG2x6MTRYacKRuXqvMFu8jt74"

        jwt = JWTManager(app)

        @app.after_request
        def finalize_response(response):
            response.headers.set("Content-Type", "application/json")
            response.headers.set("Access-Control-Allow-Origin", "http://localhost:4200")
            response.headers.set("Access-Control-Allow-Headers", "Content-Type, Authorization")
            response.headers.set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
            return response

        @app.route("/adminDeploy/login", endpoint="login", methods=["POST", "PUT", "DELETE"])
        @RestService.deserialize(RestApi.AdminDeploy.LoginRequest)
        def login(login_request):
            try:
                login_response = app.config["admin_deploy_ws_service"].rest_login(
                    login_request)
                if login_response.get_session_id is not None:
                    response_code = 200
                    login_response.set_session_id(create_access_token(
                        identity=login_response.get_session_id,
                        expires_delta=app.config["admin_deploy_ws_service"].get_session_timeout))
                else:
                    response_code = 401
                return RestService.create_response(login_response, response_code)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/logout", endpoint="logout", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.LogoutRequest)
        def logout(logout_request):
            try:
                session_id = get_jwt_identity()
                logout_request.set_session_id(session_id)
                logout_response = app.config["admin_deploy_ws_service"].rest_logout(logout_request)
                return RestService.create_response(logout_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/getTokenTypes", endpoint="get_token_types", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.GetTokenTypesRequest)
        def get_token_types(get_token_types_request):
            try:
                session_id = get_jwt_identity()
                get_token_types_request.set_session_id(session_id)
                get_token_types_response = app.config["admin_deploy_ws_service"].rest_get_token_types(get_token_types_request)
                return RestService.create_response(get_token_types_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/getTokens", endpoint="get_tokens", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.GetTokensRequest)
        def get_tokens(get_tokens_request):
            try:
                session_id = get_jwt_identity()
                get_tokens_request.set_session_id(session_id)
                get_tokens_response = app.config["admin_deploy_ws_service"].rest_get_tokens(get_tokens_request)
                return RestService.create_response(get_tokens_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/initToken", endpoint="init_token", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.InitTokenRequest)
        def init_token(init_token_request):
            try:
                session_id = get_jwt_identity()
                init_token_request.set_session_id(session_id)
                init_token_response = app.config["admin_deploy_ws_service"].rest_init_token(init_token_request)
                return RestService.create_response(init_token_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/deployToken", endpoint="deploy_token", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.DeployTokenRequest)
        def deploy_token(deploy_token_request):
            try:
                session_id = get_jwt_identity()
                deploy_token_request.set_session_id(session_id)
                deploy_token_response = app.config["admin_deploy_ws_service"].rest_deploy_token(deploy_token_request)
                return RestService.create_response(deploy_token_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/getRuntimeEnvInfo", endpoint="get_runtime_env_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.GetRuntimeEnvInfoRequest)
        def get_runtime_env_info(get_runtime_env_info_request):
            try:
                session_id = get_jwt_identity()
                get_runtime_env_info_request.set_session_id(session_id)
                get_runtime_env_info_response = app.config["admin_deploy_ws_service"].rest_get_runtime_env_info(get_runtime_env_info_request)
                return RestService.create_response(get_runtime_env_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/installGPMCollection", endpoint="install_gpm_collection", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.InstallGPMCollectionRequest)
        def install_gpm_collection(install_gpm_collection_request):
            try:
                session_id = get_jwt_identity()
                install_gpm_collection_request.set_session_id(session_id)
                install_gpm_collection_response = app.config["admin_deploy_ws_service"].rest_install_gpm_collection(install_gpm_collection_request)
                return RestService.create_response(install_gpm_collection_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/getGPMCollections", endpoint="get_gpm_collections", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.GetGPMCollectionsRequest)
        def get_gpm_collections(get_gpm_collections_request):
            try:
                session_id = get_jwt_identity()
                get_gpm_collections_request.set_session_id(session_id)
                get_gpm_collections_response = app.config["admin_deploy_ws_service"].rest_get_gpm_collections(get_gpm_collections_request)
                return RestService.create_response(get_gpm_collections_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/getJobInfo", endpoint="get_job_info", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.GetJobInfoRequest)
        def get_job_info(get_job_info_request):
            try:
                session_id = get_jwt_identity()
                get_job_info_request.set_session_id(session_id)
                get_job_info_response = app.config["admin_deploy_ws_service"].rest_get_job_info(get_job_info_request)
                return RestService.create_response(get_job_info_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/cancelJob", endpoint="cancel_job", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.CancelJobRequest)
        def cancel_job(cancel_job_request):
            try:
                session_id = get_jwt_identity()
                cancel_job_request.set_session_id(session_id)
                cancel_job_response = app.config["admin_deploy_ws_service"].rest_cancel_job(cancel_job_request)
                return RestService.create_response(cancel_job_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        @app.route("/adminDeploy/ping", endpoint="ping", methods=["POST", "PUT", "DELETE"])
        @jwt_required
        @RestService.deserialize(RestApi.AdminDeploy.PingRequest)
        def ping(ping_request):
            try:
                session_id = get_jwt_identity()
                ping_request.set_session_id(session_id)
                ping_response = app.config["admin_deploy_ws_service"].rest_ping(ping_request)
                return RestService.create_response(ping_response, 200)
            except Exception as e:
                # TODO: Add exception to the log
                return RestService.create_response({"message": e.message, "details": e}, 500)

        app.run(threaded=False, debug=True, use_reloader=False,
                host=admin_deploy_ws_service.get_admin_deploy_ws_ip_host,
                port=admin_deploy_ws_service.get_admin_deploy_ws_port)
