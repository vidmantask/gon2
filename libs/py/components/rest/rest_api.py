from typing import Type
from typing import List
from typing import Optional
from base64 import decodestring as b64decode, encodestring as b64encode


class Base64String(object):
    def __init__(self):
        # type: () -> None
        self.data = None  # type: str

    def __call__(self,
                 chunk,  # type: str
                 ):
        # type: (...) -> Type[Base64String]
        self.set_data(chunk)
        return self

    @property
    def get_data(self):
        # type: () -> str
        if not isinstance(self.data, basestring):
            raise RestApi.Admin.FaultServerElement()(
                "Base64String.get_data type {type_name} - erroneous type/subtype. Expected type: str".format(
                    type_name=type(self.data)), -1)
        return b64decode(self.data)

    def set_data(self,
                 chunk  # type: str
                 ):
        # type: (...) -> None
        if not isinstance(chunk, basestring):
            raise RestApi.Admin.FaultServerElement()(
                "Base64String.set_data type {type_name} - erroneous type/subtype. Expected type: str".format(
                    type_name=type(chunk)), -1)
        self.data = b64encode(chunk).strip()


class RestApi(object):
    def __init__(self):  # type: () -> None
        pass

    class Admin(object):
        def __init__(self):  # type: () -> None
            pass

        class Gpm(object):
            def __init__(self):  # type: () -> None
                pass

            class GPMInfo(object):
                def __init__(self):
                    # type: () -> None
                    self.gpmId = None  # type: str
                    self.gpmSummary = None  # type: str
                    self.gpmDescription = None  # type: str
                    self.gpmVersion = None  # type: str
                    self.gpmArch = None  # type: str
                    self.gpmChecksum = None  # type: str

                def __call__(self,
                             gpmId,  # type: str
                             gpmSummary,  # type: str
                             gpmDescription,  # type: str
                             gpmVersion,  # type: str
                             gpmArch,  # type: str
                             gpmChecksum,  # type: str
                             ):
                    # type: (...) -> Type[RestApi.Admin.Gpm.GPMInfo]
                    self.set_gpm_id(gpmId)
                    self.set_gpm_summary(gpmSummary)
                    self.set_gpm_description(gpmDescription)
                    self.set_gpm_version(gpmVersion)
                    self.set_gpm_arch(gpmArch)
                    self.set_gpm_checksum(gpmChecksum)
                    return self

                @property
                def get_gpm_id(self):
                    # type: () -> str
                    if not isinstance(self.gpmId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmId)), -1)
                    return self.gpmId

                @property
                def get_gpm_summary(self):
                    # type: () -> str
                    if not isinstance(self.gpmSummary, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_summary type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmSummary)), -1)
                    return self.gpmSummary

                @property
                def get_gpm_description(self):
                    # type: () -> str
                    if not isinstance(self.gpmDescription, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmDescription)), -1)
                    return self.gpmDescription

                @property
                def get_gpm_version(self):
                    # type: () -> str
                    if not isinstance(self.gpmVersion, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmVersion)), -1)
                    return self.gpmVersion

                @property
                def get_gpm_arch(self):
                    # type: () -> str
                    if not isinstance(self.gpmArch, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_arch type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmArch)), -1)
                    return self.gpmArch

                @property
                def get_gpm_checksum(self):
                    # type: () -> str
                    if not isinstance(self.gpmChecksum, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.get_gpm_checksum type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmChecksum)), -1)
                    return self.gpmChecksum

                def set_gpm_id(self,
                               gpm_id  # type: str
                               ):
                    # type: (...) -> None
                    if not isinstance(gpm_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_id)), -1)
                    self.gpmId = gpm_id

                def set_gpm_summary(self,
                                    gpm_summary  # type: str
                                    ):
                    # type: (...) -> None
                    if not isinstance(gpm_summary, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_summary type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_summary)), -1)
                    self.gpmSummary = gpm_summary

                def set_gpm_description(self,
                                        gpm_description  # type: str
                                        ):
                    # type: (...) -> None
                    if not isinstance(gpm_description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_description)), -1)
                    self.gpmDescription = gpm_description

                def set_gpm_version(self,
                                    gpm_version  # type: str
                                    ):
                    # type: (...) -> None
                    if not isinstance(gpm_version, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_version)), -1)
                    self.gpmVersion = gpm_version

                def set_gpm_arch(self,
                                 gpm_arch  # type: str
                                 ):
                    # type: (...) -> None
                    if not isinstance(gpm_arch, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_arch type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_arch)), -1)
                    self.gpmArch = gpm_arch

                def set_gpm_checksum(self,
                                     gpm_checksum  # type: str
                                     ):
                    # type: (...) -> None
                    if not isinstance(gpm_checksum, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMInfo.set_gpm_checksum type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_checksum)), -1)
                    self.gpmChecksum = gpm_checksum

            class GPMCollectionInfo(object):
                def __init__(self):
                    # type: () -> None
                    self.gpmCollectionId = None  # type: str
                    self.gpmCollectionSummary = None  # type: str
                    self.gpmCollectionDescription = None  # type: str
                    self.gpmCollectionGpms = list()  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]

                def __call__(self,
                             gpmCollectionId,  # type: str
                             gpmCollectionSummary,  # type: str
                             gpmCollectionDescription,  # type: str
                             gpmCollectionGpms=None,  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]
                             ):
                    # type: (...) -> Type[RestApi.Admin.Gpm.GPMCollectionInfo]
                    self.set_gpm_collection_id(gpmCollectionId)
                    self.set_gpm_collection_summary(gpmCollectionSummary)
                    self.set_gpm_collection_description(gpmCollectionDescription)
                    if gpmCollectionGpms is not None:
                        if isinstance(gpmCollectionGpms, list):
                            for index, item in enumerate(gpmCollectionGpms):
                                if isinstance(item, dict):
                                    gpmCollectionGpms[index] = RestApi.Admin.Gpm.GPMInfo()(**item)
                        else:
                            raise TypeError(
                                "RestApi.Admin.Gpm.GPMCollectionInfo.__call__ gpmCollectionGpms type {type_name} - erroneous type. Expected type: list".format(
                                    type_name=type(gpmCollectionGpms)))
                    self.set_gpm_collection_gpms(gpmCollectionGpms)
                    return self

                @property
                def get_gpm_collection_id(self):
                    # type: () -> str
                    if not isinstance(self.gpmCollectionId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.get_gpm_collection_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmCollectionId)), -1)
                    return self.gpmCollectionId

                @property
                def get_gpm_collection_summary(self):
                    # type: () -> str
                    if not isinstance(self.gpmCollectionSummary, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.get_gpm_collection_summary type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmCollectionSummary)), -1)
                    return self.gpmCollectionSummary

                @property
                def get_gpm_collection_description(self):
                    # type: () -> str
                    if not isinstance(self.gpmCollectionDescription, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.get_gpm_collection_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.gpmCollectionDescription)), -1)
                    return self.gpmCollectionDescription

                @property
                def get_gpm_collection_gpms(self):
                    # type: () -> Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]
                    if self.gpmCollectionGpms is not None and not (isinstance(self.gpmCollectionGpms, list) and all(
                        isinstance(item, RestApi.Admin.Gpm.GPMInfo) for item in self.gpmCollectionGpms)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.get_gpm_collection_gpms type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]".format(
                                type_name=type(self.gpmCollectionGpms)), -1)
                    if self.gpmCollectionGpms is None:
                        return list()
                    else:
                        return self.gpmCollectionGpms

                def set_gpm_collection_id(self,
                                          gpm_collection_id  # type: str
                                          ):
                    # type: (...) -> None
                    if not isinstance(gpm_collection_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.set_gpm_collection_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_collection_id)), -1)
                    self.gpmCollectionId = gpm_collection_id

                def set_gpm_collection_summary(self,
                                               gpm_collection_summary  # type: str
                                               ):
                    # type: (...) -> None
                    if not isinstance(gpm_collection_summary, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.set_gpm_collection_summary type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_collection_summary)), -1)
                    self.gpmCollectionSummary = gpm_collection_summary

                def set_gpm_collection_description(self,
                                                   gpm_collection_description  # type: str
                                                   ):
                    # type: (...) -> None
                    if not isinstance(gpm_collection_description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.set_gpm_collection_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(gpm_collection_description)), -1)
                    self.gpmCollectionDescription = gpm_collection_description

                def set_gpm_collection_gpms(self,
                                            gpm_collection_gpms
                                            # type: Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]
                                            ):
                    # type: (...) -> None
                    if gpm_collection_gpms is not None and not (isinstance(gpm_collection_gpms, list) and all(
                        isinstance(item, RestApi.Admin.Gpm.GPMInfo) for item in gpm_collection_gpms)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.Gpm.GPMCollectionInfo.set_gpm_collection_gpms type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMInfo]]]".format(
                                type_name=type(gpm_collection_gpms)), -1)
                    if gpm_collection_gpms is None:
                        self.gpmCollectionGpms = list()
                    else:
                        self.gpmCollectionGpms = gpm_collection_gpms

        class ConfigTemplate(object):
            def __init__(self):  # type: () -> None
                pass

            class ValueSelectionChoice(object):
                def __init__(self):
                    # type: () -> None
                    self.value = None  # type: Optional[str]
                    self.title = None  # type: Optional[str]

                def __call__(self,
                             value=None,  # type: Optional[str]
                             title=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]
                    self.set_value(value)
                    self.set_title(title)
                    return self

                @property
                def get_value(self):
                    # type: () -> Optional[str]
                    if self.value is not None and not isinstance(self.value, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelectionChoice.get_value type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.value)), -1)
                    return self.value

                @property
                def get_title(self):
                    # type: () -> Optional[str]
                    if self.title is not None and not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelectionChoice.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.title)), -1)
                    return self.title

                def set_value(self,
                              value  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if value is not None and not isinstance(value, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelectionChoice.set_value type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(value)), -1)
                    self.value = value

                def set_title(self,
                              title  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if title is not None and not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelectionChoice.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(title)), -1)
                    self.title = title

            class ValueSelection(object):
                def __init__(self):
                    # type: () -> None
                    self.selectionChoice = list()  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                    self.editAllowed = None  # type: Optional[bool]
                    self.emptyAllowed = None  # type: Optional[bool]

                def __call__(self,
                             selectionChoice=None,
                             # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                             editAllowed=None,  # type: Optional[bool]
                             emptyAllowed=None,  # type: Optional[bool]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.ValueSelection]
                    if selectionChoice is not None:
                        if isinstance(selectionChoice, list):
                            for index, item in enumerate(selectionChoice):
                                if isinstance(item, dict):
                                    selectionChoice[index] = RestApi.Admin.ConfigTemplate.ValueSelectionChoice()(
                                        **item)
                        else:
                            raise TypeError(
                                "RestApi.Admin.ConfigTemplate.ValueSelection.__call__ selectionChoice type {type_name} - erroneous type. Expected type: list".format(
                                    type_name=type(selectionChoice)))
                    self.set_selection_choice(selectionChoice)
                    self.set_edit_allowed(editAllowed)
                    self.set_empty_allowed(emptyAllowed)
                    return self

                @property
                def get_selection_choice(self):
                    # type: () -> Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                    if self.selectionChoice is not None and not (isinstance(self.selectionChoice, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ValueSelectionChoice) for item in
                        self.selectionChoice)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.get_selection_choice type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]".format(
                                type_name=type(self.selectionChoice)), -1)
                    if self.selectionChoice is None:
                        return list()
                    else:
                        return self.selectionChoice

                @property
                def get_edit_allowed(self):
                    # type: () -> Optional[bool]
                    if self.editAllowed is not None and not isinstance(self.editAllowed, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.get_edit_allowed type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.editAllowed)), -1)
                    return self.editAllowed

                @property
                def get_empty_allowed(self):
                    # type: () -> Optional[bool]
                    if self.emptyAllowed is not None and not isinstance(self.emptyAllowed, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.get_empty_allowed type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.emptyAllowed)), -1)
                    return self.emptyAllowed

                def set_selection_choice(self,
                                         selection_choice
                                         # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                                         ):
                    # type: (...) -> None
                    if selection_choice is not None and not (isinstance(selection_choice, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ValueSelectionChoice) for item in
                        selection_choice)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.set_selection_choice type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]".format(
                                type_name=type(selection_choice)), -1)
                    if selection_choice is None:
                        self.selectionChoice = list()
                    else:
                        self.selectionChoice = selection_choice

                def set_edit_allowed(self,
                                     edit_allowed  # type: Optional[bool]
                                     ):
                    # type: (...) -> None
                    if edit_allowed is not None and not isinstance(edit_allowed, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.set_edit_allowed type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(edit_allowed)), -1)
                    self.editAllowed = edit_allowed

                def set_empty_allowed(self,
                                      empty_allowed  # type: Optional[bool]
                                      ):
                    # type: (...) -> None
                    if empty_allowed is not None and not isinstance(empty_allowed, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ValueSelection.set_empty_allowed type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(empty_allowed)), -1)
                    self.emptyAllowed = empty_allowed

            class PortscanAction(object):
                def __init__(self):
                    # type: () -> None
                    self.title = None  # type: Optional[str]
                    self.portField = None  # type: Optional[str]

                def __call__(self,
                             title=None,  # type: Optional[str]
                             portField=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.PortscanAction]
                    self.set_title(title)
                    self.set_port_field(portField)
                    return self

                @property
                def get_title(self):
                    # type: () -> Optional[str]
                    if self.title is not None and not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.PortscanAction.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_port_field(self):
                    # type: () -> Optional[str]
                    if self.portField is not None and not isinstance(self.portField, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.PortscanAction.get_port_field type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.portField)), -1)
                    return self.portField

                def set_title(self,
                              title  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if title is not None and not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.PortscanAction.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_port_field(self,
                                   port_field  # type: Optional[str]
                                   ):
                    # type: (...) -> None
                    if port_field is not None and not isinstance(port_field, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.PortscanAction.set_port_field type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(port_field)), -1)
                    self.portField = port_field

            class TestConfigAction(object):
                def __init__(self):
                    # type: () -> None
                    self.title = None  # type: Optional[str]
                    self.tooltip = None  # type: Optional[str]
                    self.moduleId = None  # type: Optional[str]
                    self.subType = None  # type: Optional[str]
                    self.testType = None  # type: Optional[str]

                def __call__(self,
                             title=None,  # type: Optional[str]
                             tooltip=None,  # type: Optional[str]
                             moduleId=None,  # type: Optional[str]
                             subType=None,  # type: Optional[str]
                             testType=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.TestConfigAction]
                    self.set_title(title)
                    self.set_tooltip(tooltip)
                    self.set_module_id(moduleId)
                    self.set_sub_type(subType)
                    self.set_test_type(testType)
                    return self

                @property
                def get_title(self):
                    # type: () -> Optional[str]
                    if self.title is not None and not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_tooltip(self):
                    # type: () -> Optional[str]
                    if self.tooltip is not None and not isinstance(self.tooltip, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.get_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.tooltip)), -1)
                    return self.tooltip

                @property
                def get_module_id(self):
                    # type: () -> Optional[str]
                    if self.moduleId is not None and not isinstance(self.moduleId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.get_module_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.moduleId)), -1)
                    return self.moduleId

                @property
                def get_sub_type(self):
                    # type: () -> Optional[str]
                    if self.subType is not None and not isinstance(self.subType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.get_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.subType)), -1)
                    return self.subType

                @property
                def get_test_type(self):
                    # type: () -> Optional[str]
                    if self.testType is not None and not isinstance(self.testType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.get_test_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.testType)), -1)
                    return self.testType

                def set_title(self,
                              title  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if title is not None and not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_tooltip(self,
                                tooltip  # type: Optional[str]
                                ):
                    # type: (...) -> None
                    if tooltip is not None and not isinstance(tooltip, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.set_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(tooltip)), -1)
                    self.tooltip = tooltip

                def set_module_id(self,
                                  module_id  # type: Optional[str]
                                  ):
                    # type: (...) -> None
                    if module_id is not None and not isinstance(module_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.set_module_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(module_id)), -1)
                    self.moduleId = module_id

                def set_sub_type(self,
                                 sub_type  # type: Optional[str]
                                 ):
                    # type: (...) -> None
                    if sub_type is not None and not isinstance(sub_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.set_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(sub_type)), -1)
                    self.subType = sub_type

                def set_test_type(self,
                                  test_type  # type: Optional[str]
                                  ):
                    # type: (...) -> None
                    if test_type is not None and not isinstance(test_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.TestConfigAction.set_test_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(test_type)), -1)
                    self.testType = test_type

            class FieldAction(object):
                def __init__(self):
                    # type: () -> None
                    self.portscan = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]
                    self.testConfig = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]

                def __call__(self,
                             portscan=None,  # type: Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]
                             testConfig=None,
                             # type: Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.FieldAction]
                    if portscan is not None:
                        if isinstance(portscan, dict):
                            portscan = RestApi.Admin.ConfigTemplate.PortscanAction()(**portscan)
                    self.set_portscan(portscan)
                    if testConfig is not None:
                        if isinstance(testConfig, dict):
                            testConfig = RestApi.Admin.ConfigTemplate.TestConfigAction()(**testConfig)
                    self.set_test_config(testConfig)
                    return self

                @property
                def get_portscan(self):
                    # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]
                    if self.portscan is not None and not isinstance(self.portscan,
                                                                    RestApi.Admin.ConfigTemplate.PortscanAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldAction.get_portscan type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]".format(
                                type_name=type(self.portscan)), -1)
                    return self.portscan

                @property
                def get_test_config(self):
                    # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]
                    if self.testConfig is not None and not isinstance(self.testConfig,
                                                                      RestApi.Admin.ConfigTemplate.TestConfigAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldAction.get_test_config type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]".format(
                                type_name=type(self.testConfig)), -1)
                    return self.testConfig

                def set_portscan(self,
                                 portscan  # type: Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]
                                 ):
                    # type: (...) -> None
                    if portscan is not None and not isinstance(portscan,
                                                               RestApi.Admin.ConfigTemplate.PortscanAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldAction.set_portscan type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.PortscanAction]]".format(
                                type_name=type(portscan)), -1)
                    self.portscan = portscan

                def set_test_config(self,
                                    test_config
                                    # type: Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]
                                    ):
                    # type: (...) -> None
                    if test_config is not None and not isinstance(test_config,
                                                                  RestApi.Admin.ConfigTemplate.TestConfigAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldAction.set_test_config type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.TestConfigAction]]".format(
                                type_name=type(test_config)), -1)
                    self.testConfig = test_config

            class FieldElement(object):
                def __init__(self):
                    # type: () -> None
                    self.name = None  # type: str
                    self.defaultValue = None  # type: Optional[str]
                    self.title = None  # type: Optional[str]
                    self.tooltip = None  # type: Optional[str]
                    self.description = None  # type: Optional[str]
                    self.section = None  # type: Optional[str]
                    self.selection = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                    self.action = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]
                    self.fieldType = None  # type: Optional[str]
                    self.valueType = None  # type: Optional[str]
                    self.advanced = None  # type: Optional[bool]
                    self.maxLength = None  # type: Optional[int]
                    self.secret = None  # type: Optional[bool]
                    self.readOnly = None  # type: Optional[bool]
                    self.mandatory = None  # type: Optional[bool]

                def __call__(self,
                             name,  # type: str
                             defaultValue=None,  # type: Optional[str]
                             title=None,  # type: Optional[str]
                             tooltip=None,  # type: Optional[str]
                             description=None,  # type: Optional[str]
                             section=None,  # type: Optional[str]
                             selection=None,  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                             action=None,  # type: Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]
                             fieldType=None,  # type: Optional[str]
                             valueType=None,  # type: Optional[str]
                             advanced=None,  # type: Optional[bool]
                             maxLength=None,  # type: Optional[int]
                             secret=None,  # type: Optional[bool]
                             readOnly=None,  # type: Optional[bool]
                             mandatory=None,  # type: Optional[bool]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.FieldElement]
                    self.set_name(name)
                    self.set_default_value(defaultValue)
                    self.set_title(title)
                    self.set_tooltip(tooltip)
                    self.set_description(description)
                    self.set_section(section)
                    if selection is not None:
                        if isinstance(selection, dict):
                            selection = RestApi.Admin.ConfigTemplate.ValueSelection()(**selection)
                    self.set_selection(selection)
                    if action is not None:
                        if isinstance(action, dict):
                            action = RestApi.Admin.ConfigTemplate.FieldAction()(**action)
                    self.set_action(action)
                    self.set_field_type(fieldType)
                    self.set_value_type(valueType)
                    self.set_advanced(advanced)
                    self.set_max_length(maxLength)
                    self.set_secret(secret)
                    self.set_read_only(readOnly)
                    self.set_mandatory(mandatory)
                    return self

                @property
                def get_name(self):
                    # type: () -> str
                    if not isinstance(self.name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.name)), -1)
                    return self.name

                @property
                def get_default_value(self):
                    # type: () -> Optional[str]
                    if self.defaultValue is not None and not isinstance(self.defaultValue, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_default_value type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.defaultValue)), -1)
                    return self.defaultValue

                @property
                def get_title(self):
                    # type: () -> Optional[str]
                    if self.title is not None and not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_tooltip(self):
                    # type: () -> Optional[str]
                    if self.tooltip is not None and not isinstance(self.tooltip, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.tooltip)), -1)
                    return self.tooltip

                @property
                def get_description(self):
                    # type: () -> Optional[str]
                    if self.description is not None and not isinstance(self.description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.description)), -1)
                    return self.description

                @property
                def get_section(self):
                    # type: () -> Optional[str]
                    if self.section is not None and not isinstance(self.section, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_section type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.section)), -1)
                    return self.section

                @property
                def get_selection(self):
                    # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                    if self.selection is not None and not isinstance(self.selection,
                                                                     RestApi.Admin.ConfigTemplate.ValueSelection):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_selection type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]".format(
                                type_name=type(self.selection)), -1)
                    return self.selection

                @property
                def get_action(self):
                    # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]
                    if self.action is not None and not isinstance(self.action,
                                                                  RestApi.Admin.ConfigTemplate.FieldAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_action type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]".format(
                                type_name=type(self.action)), -1)
                    return self.action

                @property
                def get_field_type(self):
                    # type: () -> Optional[str]
                    if self.fieldType is not None and not isinstance(self.fieldType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_field_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.fieldType)), -1)
                    return self.fieldType

                @property
                def get_value_type(self):
                    # type: () -> Optional[str]
                    if self.valueType is not None and not isinstance(self.valueType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_value_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.valueType)), -1)
                    return self.valueType

                @property
                def get_advanced(self):
                    # type: () -> Optional[bool]
                    if self.advanced is not None and not isinstance(self.advanced, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_advanced type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.advanced)), -1)
                    return self.advanced

                @property
                def get_max_length(self):
                    # type: () -> Optional[int]
                    if self.maxLength is not None and not isinstance(self.maxLength, (int, long)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_max_length type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                                type_name=type(self.maxLength)), -1)
                    return self.maxLength

                @property
                def get_secret(self):
                    # type: () -> Optional[bool]
                    if self.secret is not None and not isinstance(self.secret, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_secret type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.secret)), -1)
                    return self.secret

                @property
                def get_read_only(self):
                    # type: () -> Optional[bool]
                    if self.readOnly is not None and not isinstance(self.readOnly, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_read_only type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.readOnly)), -1)
                    return self.readOnly

                @property
                def get_mandatory(self):
                    # type: () -> Optional[bool]
                    if self.mandatory is not None and not isinstance(self.mandatory, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.get_mandatory type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.mandatory)), -1)
                    return self.mandatory

                def set_name(self,
                             name  # type: str
                             ):
                    # type: (...) -> None
                    if not isinstance(name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(name)), -1)
                    self.name = name

                def set_default_value(self,
                                      default_value  # type: Optional[str]
                                      ):
                    # type: (...) -> None
                    if default_value is not None and not isinstance(default_value, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_default_value type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(default_value)), -1)
                    self.defaultValue = default_value

                def set_title(self,
                              title  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if title is not None and not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_tooltip(self,
                                tooltip  # type: Optional[str]
                                ):
                    # type: (...) -> None
                    if tooltip is not None and not isinstance(tooltip, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(tooltip)), -1)
                    self.tooltip = tooltip

                def set_description(self,
                                    description  # type: Optional[str]
                                    ):
                    # type: (...) -> None
                    if description is not None and not isinstance(description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(description)), -1)
                    self.description = description

                def set_section(self,
                                section  # type: Optional[str]
                                ):
                    # type: (...) -> None
                    if section is not None and not isinstance(section, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_section type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(section)), -1)
                    self.section = section

                def set_selection(self,
                                  selection  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                                  ):
                    # type: (...) -> None
                    if selection is not None and not isinstance(selection,
                                                                RestApi.Admin.ConfigTemplate.ValueSelection):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_selection type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]".format(
                                type_name=type(selection)), -1)
                    self.selection = selection

                def set_action(self,
                               action  # type: Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]
                               ):
                    # type: (...) -> None
                    if action is not None and not isinstance(action, RestApi.Admin.ConfigTemplate.FieldAction):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_action type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.FieldAction]]".format(
                                type_name=type(action)), -1)
                    self.action = action

                def set_field_type(self,
                                   field_type  # type: Optional[str]
                                   ):
                    # type: (...) -> None
                    if field_type is not None and not isinstance(field_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_field_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(field_type)), -1)
                    self.fieldType = field_type

                def set_value_type(self,
                                   value_type  # type: Optional[str]
                                   ):
                    # type: (...) -> None
                    if value_type is not None and not isinstance(value_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_value_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(value_type)), -1)
                    self.valueType = value_type

                def set_advanced(self,
                                 advanced  # type: Optional[bool]
                                 ):
                    # type: (...) -> None
                    if advanced is not None and not isinstance(advanced, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_advanced type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(advanced)), -1)
                    self.advanced = advanced

                def set_max_length(self,
                                   max_length  # type: Optional[int]
                                   ):
                    # type: (...) -> None
                    if max_length is not None and not isinstance(max_length, (int, long)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_max_length type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                                type_name=type(max_length)), -1)
                    self.maxLength = max_length

                def set_secret(self,
                               secret  # type: Optional[bool]
                               ):
                    # type: (...) -> None
                    if secret is not None and not isinstance(secret, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_secret type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(secret)), -1)
                    self.secret = secret

                def set_read_only(self,
                                  read_only  # type: Optional[bool]
                                  ):
                    # type: (...) -> None
                    if read_only is not None and not isinstance(read_only, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_read_only type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(read_only)), -1)
                    self.readOnly = read_only

                def set_mandatory(self,
                                  mandatory  # type: Optional[bool]
                                  ):
                    # type: (...) -> None
                    if mandatory is not None and not isinstance(mandatory, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.FieldElement.set_mandatory type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(mandatory)), -1)
                    self.mandatory = mandatory

            class ConfigurationTemplateSpec(object):
                def __init__(self):
                    # type: () -> None
                    self.title = None  # type: str
                    self.entityType = None  # type: str
                    self.templateName = None  # type: str
                    self.subType = None  # type: Optional[str]
                    self.id_ = None  # type: Optional[str]

                def __call__(self,
                             title,  # type: str
                             entityType,  # type: str
                             templateName,  # type: str
                             subType=None,  # type: Optional[str]
                             id_=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                    self.set_title(title)
                    self.set_entity_type(entityType)
                    self.set_template_name(templateName)
                    self.set_sub_type(subType)
                    self.set_id(id_)
                    return self

                @property
                def get_title(self):
                    # type: () -> str
                    if not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.get_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_entity_type(self):
                    # type: () -> str
                    if not isinstance(self.entityType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.entityType)), -1)
                    return self.entityType

                @property
                def get_template_name(self):
                    # type: () -> str
                    if not isinstance(self.templateName, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.get_template_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.templateName)), -1)
                    return self.templateName

                @property
                def get_sub_type(self):
                    # type: () -> Optional[str]
                    if self.subType is not None and not isinstance(self.subType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.get_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.subType)), -1)
                    return self.subType

                @property
                def get_id(self):
                    # type: () -> Optional[str]
                    if self.id_ is not None and not isinstance(self.id_, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.get_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.id_)), -1)
                    return self.id_

                def set_title(self,
                              title  # type: str
                              ):
                    # type: (...) -> None
                    if not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.set_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_entity_type(self,
                                    entity_type  # type: str
                                    ):
                    # type: (...) -> None
                    if not isinstance(entity_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(entity_type)), -1)
                    self.entityType = entity_type

                def set_template_name(self,
                                      template_name  # type: str
                                      ):
                    # type: (...) -> None
                    if not isinstance(template_name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.set_template_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(template_name)), -1)
                    self.templateName = template_name

                def set_sub_type(self,
                                 sub_type  # type: Optional[str]
                                 ):
                    # type: (...) -> None
                    if sub_type is not None and not isinstance(sub_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.set_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(sub_type)), -1)
                    self.subType = sub_type

                def set_id(self,
                           id_  # type: Optional[str]
                           ):
                    # type: (...) -> None
                    if id_ is not None and not isinstance(id_, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec.set_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(id_)), -1)
                    self.id_ = id_

            class Details(object):
                def __init__(self):
                    # type: () -> None
                    self.title = None  # type: str
                    self.internalName = None  # type: Optional[str]
                    self.configEdit = list()  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                    self.configNew = list()  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                    self.detailsType = None  # type: Optional[str]
                    self.editEnabled = None  # type: Optional[bool]
                    self.deleteEnabled = None  # type: Optional[bool]

                def __call__(self,
                             title,  # type: str
                             internalName=None,  # type: Optional[str]
                             configEdit=None,
                             # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                             configNew=None,
                             # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                             detailsType=None,  # type: Optional[str]
                             editEnabled=None,  # type: Optional[bool]
                             deleteEnabled=None,  # type: Optional[bool]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.Details]
                    self.set_title(title)
                    self.set_internal_name(internalName)
                    if configEdit is not None:
                        if isinstance(configEdit, list):
                            for index, item in enumerate(configEdit):
                                if isinstance(item, dict):
                                    configEdit[index] = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()(
                                        **item)
                        else:
                            raise TypeError(
                                "RestApi.Admin.ConfigTemplate.Details.__call__ configEdit type {type_name} - erroneous type. Expected type: list".format(
                                    type_name=type(configEdit)))
                    self.set_config_edit(configEdit)
                    if configNew is not None:
                        if isinstance(configNew, list):
                            for index, item in enumerate(configNew):
                                if isinstance(item, dict):
                                    configNew[index] = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()(
                                        **item)
                        else:
                            raise TypeError(
                                "RestApi.Admin.ConfigTemplate.Details.__call__ configNew type {type_name} - erroneous type. Expected type: list".format(
                                    type_name=type(configNew)))
                    self.set_config_new(configNew)
                    self.set_details_type(detailsType)
                    self.set_edit_enabled(editEnabled)
                    self.set_delete_enabled(deleteEnabled)
                    return self

                @property
                def get_title(self):
                    # type: () -> str
                    if not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_internal_name(self):
                    # type: () -> Optional[str]
                    if self.internalName is not None and not isinstance(self.internalName, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_internal_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.internalName)), -1)
                    return self.internalName

                @property
                def get_config_edit(self):
                    # type: () -> Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                    if self.configEdit is not None and not (isinstance(self.configEdit, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec) for item in
                        self.configEdit)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_config_edit type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]".format(
                                type_name=type(self.configEdit)), -1)
                    if self.configEdit is None:
                        return list()
                    else:
                        return self.configEdit

                @property
                def get_config_new(self):
                    # type: () -> Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                    if self.configNew is not None and not (isinstance(self.configNew, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec) for item in
                        self.configNew)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_config_new type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]".format(
                                type_name=type(self.configNew)), -1)
                    if self.configNew is None:
                        return list()
                    else:
                        return self.configNew

                @property
                def get_details_type(self):
                    # type: () -> Optional[str]
                    if self.detailsType is not None and not isinstance(self.detailsType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_details_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.detailsType)), -1)
                    return self.detailsType

                @property
                def get_edit_enabled(self):
                    # type: () -> Optional[bool]
                    if self.editEnabled is not None and not isinstance(self.editEnabled, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_edit_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.editEnabled)), -1)
                    return self.editEnabled

                @property
                def get_delete_enabled(self):
                    # type: () -> Optional[bool]
                    if self.deleteEnabled is not None and not isinstance(self.deleteEnabled, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.get_delete_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.deleteEnabled)), -1)
                    return self.deleteEnabled

                def set_title(self,
                              title  # type: str
                              ):
                    # type: (...) -> None
                    if not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_internal_name(self,
                                      internal_name  # type: Optional[str]
                                      ):
                    # type: (...) -> None
                    if internal_name is not None and not isinstance(internal_name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_internal_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(internal_name)), -1)
                    self.internalName = internal_name

                def set_config_edit(self,
                                    config_edit
                                    # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                                    ):
                    # type: (...) -> None
                    if config_edit is not None and not (isinstance(config_edit, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec) for item in
                        config_edit)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_config_edit type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]".format(
                                type_name=type(config_edit)), -1)
                    if config_edit is None:
                        self.configEdit = list()
                    else:
                        self.configEdit = config_edit

                def set_config_new(self,
                                   config_new
                                   # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]
                                   ):
                    # type: (...) -> None
                    if config_new is not None and not (isinstance(config_new, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec) for item in
                        config_new)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_config_new type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]]".format(
                                type_name=type(config_new)), -1)
                    if config_new is None:
                        self.configNew = list()
                    else:
                        self.configNew = config_new

                def set_details_type(self,
                                     details_type  # type: Optional[str]
                                     ):
                    # type: (...) -> None
                    if details_type is not None and not isinstance(details_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_details_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(details_type)), -1)
                    self.detailsType = details_type

                def set_edit_enabled(self,
                                     edit_enabled  # type: Optional[bool]
                                     ):
                    # type: (...) -> None
                    if edit_enabled is not None and not isinstance(edit_enabled, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_edit_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(edit_enabled)), -1)
                    self.editEnabled = edit_enabled

                def set_delete_enabled(self,
                                       delete_enabled  # type: Optional[bool]
                                       ):
                    # type: (...) -> None
                    if delete_enabled is not None and not isinstance(delete_enabled, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.Details.set_delete_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(delete_enabled)), -1)
                    self.deleteEnabled = delete_enabled

            class ConfigurationTemplate(object):
                def __init__(self):
                    # type: () -> None
                    self.field = list()  # type: List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]
                    self.details = list()  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]
                    self.description = None  # type: Optional[str]
                    self.title = None  # type: Optional[str]
                    self.name = None  # type: Optional[str]
                    self.entityType = None  # type: Optional[str]
                    self.useAsTemplate = None  # type: Optional[bool]
                    self.readOnly = None  # type: Optional[bool]

                def __call__(self,
                             field,  # type: List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]
                             details=None,  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]
                             description=None,  # type: Optional[str]
                             title=None,  # type: Optional[str]
                             name=None,  # type: Optional[str]
                             entityType=None,  # type: Optional[str]
                             useAsTemplate=None,  # type: Optional[bool]
                             readOnly=None,  # type: Optional[bool]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                    if isinstance(field, list):
                        for index, item in enumerate(field):
                            if isinstance(item, dict):
                                field[index] = RestApi.Admin.ConfigTemplate.FieldElement()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.__call__ field type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(field)))
                    self.set_field(field)
                    if details is not None:
                        if isinstance(details, list):
                            for index, item in enumerate(details):
                                if isinstance(item, dict):
                                    details[index] = RestApi.Admin.ConfigTemplate.Details()(**item)
                        else:
                            raise TypeError(
                                "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.__call__ details type {type_name} - erroneous type. Expected type: list".format(
                                    type_name=type(details)))
                    self.set_details(details)
                    self.set_description(description)
                    self.set_title(title)
                    self.set_name(name)
                    self.set_entity_type(entityType)
                    self.set_use_as_template(useAsTemplate)
                    self.set_read_only(readOnly)
                    return self

                @property
                def get_field(self):
                    # type: () -> List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]
                    if not (isinstance(self.field, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.FieldElement) for item in self.field)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_field type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]".format(
                                type_name=type(self.field)), -1)
                    return self.field

                @property
                def get_details(self):
                    # type: () -> Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]
                    if self.details is not None and not (isinstance(self.details, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.Details) for item in self.details)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_details type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]".format(
                                type_name=type(self.details)), -1)
                    if self.details is None:
                        return list()
                    else:
                        return self.details

                @property
                def get_description(self):
                    # type: () -> Optional[str]
                    if self.description is not None and not isinstance(self.description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.description)), -1)
                    return self.description

                @property
                def get_title(self):
                    # type: () -> Optional[str]
                    if self.title is not None and not isinstance(self.title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.title)), -1)
                    return self.title

                @property
                def get_name(self):
                    # type: () -> Optional[str]
                    if self.name is not None and not isinstance(self.name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.name)), -1)
                    return self.name

                @property
                def get_entity_type(self):
                    # type: () -> Optional[str]
                    if self.entityType is not None and not isinstance(self.entityType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_entity_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.entityType)), -1)
                    return self.entityType

                @property
                def get_use_as_template(self):
                    # type: () -> Optional[bool]
                    if self.useAsTemplate is not None and not isinstance(self.useAsTemplate, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_use_as_template type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.useAsTemplate)), -1)
                    return self.useAsTemplate

                @property
                def get_read_only(self):
                    # type: () -> Optional[bool]
                    if self.readOnly is not None and not isinstance(self.readOnly, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.get_read_only type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.readOnly)), -1)
                    return self.readOnly

                def set_field(self,
                              field  # type: List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]
                              ):
                    # type: (...) -> None
                    if not (isinstance(field, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.FieldElement) for item in field)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_field type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.ConfigTemplate.FieldElement]]".format(
                                type_name=type(field)), -1)
                    self.field = field

                def set_details(self,
                                details  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]
                                ):
                    # type: (...) -> None
                    if details is not None and not (isinstance(details, list) and all(
                        isinstance(item, RestApi.Admin.ConfigTemplate.Details) for item in details)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_details type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.Details]]]".format(
                                type_name=type(details)), -1)
                    if details is None:
                        self.details = list()
                    else:
                        self.details = details

                def set_description(self,
                                    description  # type: Optional[str]
                                    ):
                    # type: (...) -> None
                    if description is not None and not isinstance(description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(description)), -1)
                    self.description = description

                def set_title(self,
                              title  # type: Optional[str]
                              ):
                    # type: (...) -> None
                    if title is not None and not isinstance(title, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(title)), -1)
                    self.title = title

                def set_name(self,
                             name  # type: Optional[str]
                             ):
                    # type: (...) -> None
                    if name is not None and not isinstance(name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(name)), -1)
                    self.name = name

                def set_entity_type(self,
                                    entity_type  # type: Optional[str]
                                    ):
                    # type: (...) -> None
                    if entity_type is not None and not isinstance(entity_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_entity_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(entity_type)), -1)
                    self.entityType = entity_type

                def set_use_as_template(self,
                                        use_as_template  # type: Optional[bool]
                                        ):
                    # type: (...) -> None
                    if use_as_template is not None and not isinstance(use_as_template, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_use_as_template type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(use_as_template)), -1)
                    self.useAsTemplate = use_as_template

                def set_read_only(self,
                                  read_only  # type: Optional[bool]
                                  ):
                    # type: (...) -> None
                    if read_only is not None and not isinstance(read_only, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ConfigurationTemplate.set_read_only type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(read_only)), -1)
                    self.readOnly = read_only

            class ErrorLocation(object):
                def __init__(self):
                    # type: () -> None
                    self.fieldName = None  # type: Optional[str]
                    self.message = None  # type: Optional[str]

                def __call__(self,
                             fieldName=None,  # type: Optional[str]
                             message=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.Admin.ConfigTemplate.ErrorLocation]
                    self.set_field_name(fieldName)
                    self.set_message(message)
                    return self

                @property
                def get_field_name(self):
                    # type: () -> Optional[str]
                    if self.fieldName is not None and not isinstance(self.fieldName, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ErrorLocation.get_field_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.fieldName)), -1)
                    return self.fieldName

                @property
                def get_message(self):
                    # type: () -> Optional[str]
                    if self.message is not None and not isinstance(self.message, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ErrorLocation.get_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.message)), -1)
                    return self.message

                def set_field_name(self,
                                   field_name  # type: Optional[str]
                                   ):
                    # type: (...) -> None
                    if field_name is not None and not isinstance(field_name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ErrorLocation.set_field_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(field_name)), -1)
                    self.fieldName = field_name

                def set_message(self,
                                message  # type: Optional[str]
                                ):
                    # type: (...) -> None
                    if message is not None and not isinstance(message, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.Admin.ConfigTemplate.ErrorLocation.set_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(message)), -1)
                    self.message = message

        class FaultServerElement(Exception):
            def __init__(self):
                # type: () -> None
                super(RestApi.Admin.FaultServerElement, self).__init__()
                self.errorMessage = None  # type: str
                self.errorCode = None  # type: Optional[int]

            def __call__(self,
                         errorMessage,  # type: str
                         errorCode=None,  # type: Optional[int]
                         ):
                # type: (...) -> Type[RestApi.Admin.FaultServerElement]
                self.set_error_message(errorMessage)
                self.set_error_code(errorCode)
                return self

            @property
            def get_error_message(self):
                # type: () -> str
                if not isinstance(self.errorMessage, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerElement.get_error_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.errorMessage)), -1)
                return self.errorMessage

            @property
            def get_error_code(self):
                # type: () -> Optional[int]
                if self.errorCode is not None and not isinstance(self.errorCode, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerElement.get_error_code type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.errorCode)), -1)
                return self.errorCode

            def set_error_message(self,
                                  error_message  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(error_message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerElement.set_error_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(error_message)), -1)
                self.errorMessage = error_message

            def set_error_code(self,
                               error_code  # type: Optional[int]
                               ):
                # type: (...) -> None
                if error_code is not None and not isinstance(error_code, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerElement.set_error_code type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(error_code)), -1)
                self.errorCode = error_code

        class FaultServerOperationNotAllowedElement(FaultServerElement):
            def __init__(self):
                # type: () -> None
                super(RestApi.Admin.FaultServerOperationNotAllowedElement, self).__init__()
                self.errorMessage = None  # type: str
                self.errorCode = None  # type: Optional[int]

            def __call__(self,
                         errorMessage,  # type: str
                         errorCode=None,  # type: Optional[int]
                         ):
                # type: (...) -> Type[RestApi.Admin.FaultServerOperationNotAllowedElement]
                self.set_error_message(errorMessage)
                self.set_error_code(errorCode)
                return self

            @property
            def get_error_message(self):
                # type: () -> str
                if not isinstance(self.errorMessage, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerOperationNotAllowedElement.get_error_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.errorMessage)), -1)
                return self.errorMessage

            @property
            def get_error_code(self):
                # type: () -> Optional[int]
                if self.errorCode is not None and not isinstance(self.errorCode, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerOperationNotAllowedElement.get_error_code type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.errorCode)), -1)
                return self.errorCode

            def set_error_message(self,
                                  error_message  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(error_message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerOperationNotAllowedElement.set_error_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(error_message)), -1)
                self.errorMessage = error_message

            def set_error_code(self,
                               error_code  # type: Optional[int]
                               ):
                # type: (...) -> None
                if error_code is not None and not isinstance(error_code, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.FaultServerOperationNotAllowedElement.set_error_code type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(error_code)), -1)
                self.errorCode = error_code

        class OpenSessionRequest(object):
            def __init__(self):
                # type: () -> None
                self.oldSessionId = None  # type: Optional[str]

            def __call__(self,
                         oldSessionId=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.OpenSessionRequest]
                self.set_old_session_id(oldSessionId)
                return self

            @property
            def get_old_session_id(self):
                # type: () -> Optional[str]
                if self.oldSessionId is not None and not isinstance(self.oldSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.OpenSessionRequest.get_old_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.oldSessionId)), -1)
                return self.oldSessionId

            def set_old_session_id(self,
                                   old_session_id  # type: Optional[str]
                                   ):
                # type: (...) -> None
                if old_session_id is not None and not isinstance(old_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.OpenSessionRequest.set_old_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(old_session_id)), -1)
                self.oldSessionId = old_session_id

        class OpenSessionResponse(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.OpenSessionResponse]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.OpenSessionResponse.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.OpenSessionResponse.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class CloseSessionRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.CloseSessionRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CloseSessionRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CloseSessionRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class CloseSessionResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.CloseSessionResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CloseSessionResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CloseSessionResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class LoginRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.username = None  # type: str
                self.password = None  # type: str

            def __call__(self,
                         username,  # type: str
                         password,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.LoginRequest]
                self.set_username(username)
                self.set_password(password)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_username(self):
                # type: () -> str
                if not isinstance(self.username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.get_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.username)), -1)
                return self.username

            @property
            def get_password(self):
                # type: () -> str
                if not isinstance(self.password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.get_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.password)), -1)
                return self.password

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_username(self,
                             username  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.set_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(username)), -1)
                self.username = username

            def set_password(self,
                             password  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginRequest.set_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(password)), -1)
                self.password = password

        class LoginResponse(object):
            def __init__(self):
                # type: () -> None
                self.errorCode = None  # type: int
                self.errorMsg = None  # type: str

            def __call__(self,
                         errorCode,  # type: int
                         errorMsg,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.LoginResponse]
                self.set_error_code(errorCode)
                self.set_error_msg(errorMsg)
                return self

            @property
            def get_error_code(self):
                # type: () -> int
                if not isinstance(self.errorCode, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginResponse.get_error_code type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.errorCode)), -1)
                return self.errorCode

            @property
            def get_error_msg(self):
                # type: () -> str
                if not isinstance(self.errorMsg, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginResponse.get_error_msg type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.errorMsg)), -1)
                return self.errorMsg

            def set_error_code(self,
                               error_code  # type: int
                               ):
                # type: (...) -> None
                if not isinstance(error_code, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginResponse.set_error_code type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(error_code)), -1)
                self.errorCode = error_code

            def set_error_msg(self,
                              error_msg  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(error_msg, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LoginResponse.set_error_msg type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(error_msg)), -1)
                self.errorMsg = error_msg

        class InternalLoginRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.loginIndex = None  # type: int
                self.login = None  # type: str

            def __call__(self,
                         loginIndex,  # type: int
                         login,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.InternalLoginRequest]
                self.set_login_index(loginIndex)
                self.set_login(login)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_login_index(self):
                # type: () -> int
                if not isinstance(self.loginIndex, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.get_login_index type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.loginIndex)), -1)
                return self.loginIndex

            @property
            def get_login(self):
                # type: () -> str
                if not isinstance(self.login, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.get_login type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.login)), -1)
                return self.login

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_login_index(self,
                                login_index  # type: int
                                ):
                # type: (...) -> None
                if not isinstance(login_index, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.set_login_index type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(login_index)), -1)
                self.loginIndex = login_index

            def set_login(self,
                          login  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(login, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginRequest.set_login type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(login)), -1)
                self.login = login

        class InternalLoginResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.InternalLoginResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.InternalLoginResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetAccessDefintionRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetAccessDefintionRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAccessDefintionRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAccessDefintionRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class AccessDef(object):
            def __init__(self):
                # type: () -> None
                self.name = None  # type: str
                self.readAccess = None  # type: bool

            def __call__(self,
                         name,  # type: str
                         readAccess,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.AccessDef]
                self.set_name(name)
                self.set_read_access(readAccess)
                return self

            @property
            def get_name(self):
                # type: () -> str
                if not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AccessDef.get_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_read_access(self):
                # type: () -> bool
                if not isinstance(self.readAccess, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AccessDef.get_read_access type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.readAccess)), -1)
                return self.readAccess

            def set_name(self,
                         name  # type: str
                         ):
                # type: (...) -> None
                if not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AccessDef.set_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_read_access(self,
                                read_access  # type: bool
                                ):
                # type: (...) -> None
                if not isinstance(read_access, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AccessDef.set_read_access type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(read_access)), -1)
                self.readAccess = read_access

        class GetAccessDefintionResponse(object):
            def __init__(self):
                # type: () -> None
                self.accessDefs = list()  # type: Optional[List[Type[RestApi.Admin.AccessDef]]]

            def __call__(self,
                         accessDefs=None,  # type: Optional[List[Type[RestApi.Admin.AccessDef]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetAccessDefintionResponse]
                if accessDefs is not None:
                    if isinstance(accessDefs, list):
                        for index, item in enumerate(accessDefs):
                            if isinstance(item, dict):
                                accessDefs[index] = RestApi.Admin.AccessDef()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetAccessDefintionResponse.__call__ accessDefs type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(accessDefs)))
                self.set_access_defs(accessDefs)
                return self

            @property
            def get_access_defs(self):
                # type: () -> Optional[List[Type[RestApi.Admin.AccessDef]]]
                if self.accessDefs is not None and not (isinstance(self.accessDefs, list) and all(
                    isinstance(item, RestApi.Admin.AccessDef) for item in self.accessDefs)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAccessDefintionResponse.get_access_defs type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.AccessDef]]]".format(
                            type_name=type(self.accessDefs)), -1)
                if self.accessDefs is None:
                    return list()
                else:
                    return self.accessDefs

            def set_access_defs(self,
                                access_defs  # type: Optional[List[Type[RestApi.Admin.AccessDef]]]
                                ):
                # type: (...) -> None
                if access_defs is not None and not (isinstance(access_defs, list) and all(
                    isinstance(item, RestApi.Admin.AccessDef) for item in access_defs)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAccessDefintionResponse.set_access_defs type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.AccessDef]]]".format(
                            type_name=type(access_defs)), -1)
                if access_defs is None:
                    self.accessDefs = list()
                else:
                    self.accessDefs = access_defs

        class GetModuleElementsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementType = None  # type: str
                self.refreshCache = None  # type: bool
                self.searchFilter = None  # type: Optional[str]

            def __call__(self,
                         elementType,  # type: str
                         refreshCache,  # type: bool
                         searchFilter=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsRequest]
                self.set_element_type(elementType)
                self.set_refresh_cache(refreshCache)
                self.set_search_filter(searchFilter)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_type(self):
                # type: () -> str
                if not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            @property
            def get_refresh_cache(self):
                # type: () -> bool
                if not isinstance(self.refreshCache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.get_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.refreshCache)), -1)
                return self.refreshCache

            @property
            def get_search_filter(self):
                # type: () -> Optional[str]
                if self.searchFilter is not None and not isinstance(self.searchFilter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.get_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.searchFilter)), -1)
                return self.searchFilter

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_type(self,
                                 element_type  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

            def set_refresh_cache(self,
                                  refresh_cache  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(refresh_cache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.set_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(refresh_cache)), -1)
                self.refreshCache = refresh_cache

            def set_search_filter(self,
                                  search_filter  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if search_filter is not None and not isinstance(search_filter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsRequest.set_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(search_filter)), -1)
                self.searchFilter = search_filter

        class SearchField(object):
            def __init__(self):
                # type: () -> None
                self.name = None  # type: str
                self.value = None  # type: str
                self.title = None  # type: str
                self.tooltip = None  # type: Optional[str]
                self.valueType = None  # type: Optional[str]

            def __call__(self,
                         name,  # type: str
                         value,  # type: str
                         title,  # type: str
                         tooltip=None,  # type: Optional[str]
                         valueType=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.SearchField]
                self.set_name(name)
                self.set_value(value)
                self.set_title(title)
                self.set_tooltip(tooltip)
                self.set_value_type(valueType)
                return self

            @property
            def get_name(self):
                # type: () -> str
                if not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.get_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_value(self):
                # type: () -> str
                if not isinstance(self.value, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.get_value type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.value)), -1)
                return self.value

            @property
            def get_title(self):
                # type: () -> str
                if not isinstance(self.title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.get_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.title)), -1)
                return self.title

            @property
            def get_tooltip(self):
                # type: () -> Optional[str]
                if self.tooltip is not None and not isinstance(self.tooltip, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.get_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.tooltip)), -1)
                return self.tooltip

            @property
            def get_value_type(self):
                # type: () -> Optional[str]
                if self.valueType is not None and not isinstance(self.valueType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.get_value_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.valueType)), -1)
                return self.valueType

            def set_name(self,
                         name  # type: str
                         ):
                # type: (...) -> None
                if not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.set_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_value(self,
                          value  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(value, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.set_value type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(value)), -1)
                self.value = value

            def set_title(self,
                          title  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.set_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(title)), -1)
                self.title = title

            def set_tooltip(self,
                            tooltip  # type: Optional[str]
                            ):
                # type: (...) -> None
                if tooltip is not None and not isinstance(tooltip, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.set_tooltip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(tooltip)), -1)
                self.tooltip = tooltip

            def set_value_type(self,
                               value_type  # type: Optional[str]
                               ):
                # type: (...) -> None
                if value_type is not None and not isinstance(value_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SearchField.set_value_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(value_type)), -1)
                self.valueType = value_type

        class Element(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: str
                self.entityType = None  # type: str
                self.info = None  # type: Optional[str]
                self.label = None  # type: str
                self.elementType = None  # type: Optional[str]

            def __call__(self,
                         id_,  # type: str
                         entityType,  # type: str
                         label,  # type: str
                         info=None,  # type: Optional[str]
                         elementType=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.Element]
                self.set_id(id_)
                self.set_entity_type(entityType)
                self.set_label(label)
                self.set_info(info)
                self.set_element_type(elementType)
                return self

            @property
            def get_id(self):
                # type: () -> str
                if not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.get_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_info(self):
                # type: () -> Optional[str]
                if self.info is not None and not isinstance(self.info, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.get_info type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.info)), -1)
                return self.info

            @property
            def get_label(self):
                # type: () -> str
                if not isinstance(self.label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.get_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.label)), -1)
                return self.label

            @property
            def get_element_type(self):
                # type: () -> Optional[str]
                if self.elementType is not None and not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.get_element_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            def set_id(self,
                       id_  # type: str
                       ):
                # type: (...) -> None
                if not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.set_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_info(self,
                         info  # type: Optional[str]
                         ):
                # type: (...) -> None
                if info is not None and not isinstance(info, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.set_info type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(info)), -1)
                self.info = info

            def set_label(self,
                          label  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.set_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(label)), -1)
                self.label = label

            def set_element_type(self,
                                 element_type  # type: Optional[str]
                                 ):
                # type: (...) -> None
                if element_type is not None and not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Element.set_element_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

        class ConfigElement(Element):
            def __init__(self):
                # type: () -> None
                super(RestApi.Admin.ConfigElement, self).__init__()
                self.configTemplate = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configTemplate=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         **kwargs
                         ):
                # type: (...) -> Type[RestApi.Admin.ConfigElement]
                if configTemplate is not None:
                    if isinstance(configTemplate, dict):
                        configTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**configTemplate)
                self.set_config_template(configTemplate)
                return self

            @property
            def get_config_template(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configTemplate is not None and not isinstance(self.configTemplate,
                                                                      RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ConfigElement.get_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configTemplate)), -1)
                return self.configTemplate

            def set_config_template(self,
                                    config_template
                                    # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                    ):
                # type: (...) -> None
                if config_template is not None and not isinstance(config_template,
                                                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ConfigElement.set_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_template)), -1)
                self.configTemplate = config_template

        class GetModuleElementsResponse(object):
            def __init__(self):
                # type: () -> None
                self.elements = list()  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                self.resultTruncated = None  # type: Optional[bool]

            def __call__(self,
                         elements=None,  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                         resultTruncated=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsResponse]
                if elements is not None:
                    if isinstance(elements, list):
                        for index, item in enumerate(elements):
                            if isinstance(item, dict):
                                elements[index] = RestApi.Admin.ConfigElement()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetModuleElementsResponse.__call__ elements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(elements)))
                self.set_elements(elements)
                self.set_result_truncated(resultTruncated)
                return self

            @property
            def get_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ConfigElement]]]
                if self.elements is not None and not (isinstance(self.elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsResponse.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            @property
            def get_result_truncated(self):
                # type: () -> Optional[bool]
                if self.resultTruncated is not None and not isinstance(self.resultTruncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsResponse.get_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.resultTruncated)), -1)
                return self.resultTruncated

            def set_elements(self,
                             elements  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                             ):
                # type: (...) -> None
                if elements is not None and not (isinstance(elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsResponse.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

            def set_result_truncated(self,
                                     result_truncated  # type: Optional[bool]
                                     ):
                # type: (...) -> None
                if result_truncated is not None and not isinstance(result_truncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsResponse.set_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(result_truncated)), -1)
                self.resultTruncated = result_truncated

        class ElementSpec(object):
            def __init__(self):
                # type: () -> None
                self.ids = list()  # type: List[str]
                self.entityType = None  # type: str

            def __call__(self,
                         ids,  # type: List[str]
                         entityType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.ElementSpec]
                self.set_ids(ids)
                self.set_entity_type(entityType)
                return self

            @property
            def get_ids(self):
                # type: () -> List[str]
                if not (isinstance(self.ids, list) and all(isinstance(item, basestring) for item in self.ids)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ElementSpec.get_ids type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(self.ids)), -1)
                return self.ids

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ElementSpec.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            def set_ids(self,
                        ids  # type: List[str]
                        ):
                # type: (...) -> None
                if not (isinstance(ids, list) and all(isinstance(item, basestring) for item in ids)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ElementSpec.set_ids type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(ids)), -1)
                self.ids = ids

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ElementSpec.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

        class GetSpecificElementsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementSpecs = list()  # type: List[Type[RestApi.Admin.ElementSpec]]

            def __call__(self,
                         elementSpecs,  # type: List[Type[RestApi.Admin.ElementSpec]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetSpecificElementsRequest]
                if isinstance(elementSpecs, list):
                    for index, item in enumerate(elementSpecs):
                        if isinstance(item, dict):
                            elementSpecs[index] = RestApi.Admin.ElementSpec()(**item)
                else:
                    raise TypeError(
                        "RestApi.Admin.GetSpecificElementsRequest.__call__ elementSpecs type {type_name} - erroneous type. Expected type: list".format(
                            type_name=type(elementSpecs)))
                self.set_element_specs(elementSpecs)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_specs(self):
                # type: () -> List[Type[RestApi.Admin.ElementSpec]]
                if not (isinstance(self.elementSpecs, list) and all(
                    isinstance(item, RestApi.Admin.ElementSpec) for item in self.elementSpecs)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsRequest.get_element_specs type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.ElementSpec]]".format(
                            type_name=type(self.elementSpecs)), -1)
                return self.elementSpecs

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_specs(self,
                                  element_specs  # type: List[Type[RestApi.Admin.ElementSpec]]
                                  ):
                # type: (...) -> None
                if not (isinstance(element_specs, list) and all(
                    isinstance(item, RestApi.Admin.ElementSpec) for item in element_specs)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsRequest.set_element_specs type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.ElementSpec]]".format(
                            type_name=type(element_specs)), -1)
                self.elementSpecs = element_specs

        class GetSpecificElementsResponse(object):
            def __init__(self):
                # type: () -> None
                self.elements = list()  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]

            def __call__(self,
                         elements=None,  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetSpecificElementsResponse]
                if elements is not None:
                    if isinstance(elements, list):
                        for index, item in enumerate(elements):
                            if isinstance(item, dict):
                                elements[index] = RestApi.Admin.ConfigElement()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetSpecificElementsResponse.__call__ elements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(elements)))
                self.set_elements(elements)
                return self

            @property
            def get_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ConfigElement]]]
                if self.elements is not None and not (isinstance(self.elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsResponse.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            def set_elements(self,
                             elements  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                             ):
                # type: (...) -> None
                if elements is not None and not (isinstance(elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetSpecificElementsResponse.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

        class GetAuthRestrictionElementsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.restrictionType = None  # type: str

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         restrictionType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetAuthRestrictionElementsRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                self.set_restriction_type(restrictionType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_restriction_type(self):
                # type: () -> str
                if not isinstance(self.restrictionType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.get_restriction_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.restrictionType)), -1)
                return self.restrictionType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_restriction_type(self,
                                     restriction_type  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(restriction_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsRequest.set_restriction_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(restriction_type)), -1)
                self.restrictionType = restriction_type

        class GetAuthRestrictionElementsResponse(object):
            def __init__(self):
                # type: () -> None
                self.elements = list()  # type: Optional[List[Type[RestApi.Admin.Element]]]

            def __call__(self,
                         elements=None,  # type: Optional[List[Type[RestApi.Admin.Element]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetAuthRestrictionElementsResponse]
                if elements is not None:
                    if isinstance(elements, list):
                        for index, item in enumerate(elements):
                            if isinstance(item, dict):
                                elements[index] = RestApi.Admin.Element()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetAuthRestrictionElementsResponse.__call__ elements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(elements)))
                self.set_elements(elements)
                return self

            @property
            def get_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Element]]]
                if self.elements is not None and not (isinstance(self.elements, list) and all(
                    isinstance(item, RestApi.Admin.Element) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsResponse.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            def set_elements(self,
                             elements  # type: Optional[List[Type[RestApi.Admin.Element]]]
                             ):
                # type: (...) -> None
                if elements is not None and not (
                    isinstance(elements, list) and all(isinstance(item, RestApi.Admin.Element) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetAuthRestrictionElementsResponse.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

        class UpdateAuthRestrictionElementsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.restrictionType = None  # type: str
                self.restrictionElements = list()  # type: Optional[List[Type[RestApi.Admin.Element]]]

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         restrictionType,  # type: str
                         restrictionElements=None,  # type: Optional[List[Type[RestApi.Admin.Element]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateAuthRestrictionElementsRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                self.set_restriction_type(restrictionType)
                if restrictionElements is not None:
                    if isinstance(restrictionElements, list):
                        for index, item in enumerate(restrictionElements):
                            if isinstance(item, dict):
                                restrictionElements[index] = RestApi.Admin.Element()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.UpdateAuthRestrictionElementsRequest.__call__ restrictionElements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(restrictionElements)))
                self.set_restriction_elements(restrictionElements)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_restriction_type(self):
                # type: () -> str
                if not isinstance(self.restrictionType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.get_restriction_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.restrictionType)), -1)
                return self.restrictionType

            @property
            def get_restriction_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Element]]]
                if self.restrictionElements is not None and not (isinstance(self.restrictionElements, list) and all(
                    isinstance(item, RestApi.Admin.Element) for item in self.restrictionElements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.get_restriction_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(self.restrictionElements)), -1)
                if self.restrictionElements is None:
                    return list()
                else:
                    return self.restrictionElements

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_restriction_type(self,
                                     restriction_type  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(restriction_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.set_restriction_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(restriction_type)), -1)
                self.restrictionType = restriction_type

            def set_restriction_elements(self,
                                         restriction_elements  # type: Optional[List[Type[RestApi.Admin.Element]]]
                                         ):
                # type: (...) -> None
                if restriction_elements is not None and not (isinstance(restriction_elements, list) and all(
                    isinstance(item, RestApi.Admin.Element) for item in restriction_elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsRequest.set_restriction_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(restriction_elements)), -1)
                if restriction_elements is None:
                    self.restrictionElements = list()
                else:
                    self.restrictionElements = restriction_elements

        class UpdateAuthRestrictionElementsResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateAuthRestrictionElementsResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateAuthRestrictionElementsResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class RemoveUserFromLaunchTypeCategoryRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.launchTypeCategory = None  # type: str

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         launchTypeCategory,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                self.set_launch_type_category(launchTypeCategory)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_launch_type_category(self):
                # type: () -> str
                if not isinstance(self.launchTypeCategory, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.get_launch_type_category type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.launchTypeCategory)), -1)
                return self.launchTypeCategory

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_launch_type_category(self,
                                         launch_type_category  # type: str
                                         ):
                # type: (...) -> None
                if not isinstance(launch_type_category, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryRequest.set_launch_type_category type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(launch_type_category)), -1)
                self.launchTypeCategory = launch_type_category

        class RemoveUserFromLaunchTypeCategoryResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.RemoveUserFromLaunchTypeCategoryResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveUserFromLaunchTypeCategoryResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetRestrictedLaunchTypeCategoriesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetRestrictedLaunchTypeCategoriesRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRestrictedLaunchTypeCategoriesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRestrictedLaunchTypeCategoriesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetRestrictedLaunchTypeCategoriesResponse(object):
            def __init__(self):
                # type: () -> None
                self.launchTypeCategory = list()  # type: Optional[List[str]]

            def __call__(self,
                         launchTypeCategory=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRestrictedLaunchTypeCategoriesResponse]
                self.set_launch_type_category(launchTypeCategory)
                return self

            @property
            def get_launch_type_category(self):
                # type: () -> Optional[List[str]]
                if self.launchTypeCategory is not None and not (isinstance(self.launchTypeCategory, list) and all(
                    isinstance(item, basestring) for item in self.launchTypeCategory)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRestrictedLaunchTypeCategoriesResponse.get_launch_type_category type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.launchTypeCategory)), -1)
                if self.launchTypeCategory is None:
                    return list()
                else:
                    return self.launchTypeCategory

            def set_launch_type_category(self,
                                         launch_type_category  # type: Optional[List[str]]
                                         ):
                # type: (...) -> None
                if launch_type_category is not None and not (isinstance(launch_type_category, list) and all(
                    isinstance(item, basestring) for item in launch_type_category)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRestrictedLaunchTypeCategoriesResponse.set_launch_type_category type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(launch_type_category)), -1)
                if launch_type_category is None:
                    self.launchTypeCategory = list()
                else:
                    self.launchTypeCategory = launch_type_category

        class GetModuleHeaderElementRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementType = None  # type: str

            def __call__(self,
                         elementType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleHeaderElementRequest]
                self.set_element_type(elementType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_type(self):
                # type: () -> str
                if not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_type(self,
                                 element_type  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

        class GetModuleHeaderElementResponse(object):
            def __init__(self):
                # type: () -> None
                self.element = None  # type: Type[RestApi.Admin.Element]

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleHeaderElementResponse]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                return self

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementResponse.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleHeaderElementResponse.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

        class GetElementSeachCategoriesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementType = None  # type: str

            def __call__(self,
                         elementType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementSeachCategoriesRequest]
                self.set_element_type(elementType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_type(self):
                # type: () -> str
                if not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_type(self,
                                 element_type  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

        class GetElementSeachCategoriesResponse(object):
            def __init__(self):
                # type: () -> None
                self.searchCategories = list()  # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]

            def __call__(self,
                         searchCategories=None,
                         # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementSeachCategoriesResponse]
                if searchCategories is not None:
                    if isinstance(searchCategories, list):
                        for index, item in enumerate(searchCategories):
                            if isinstance(item, dict):
                                searchCategories[index] = RestApi.Admin.ConfigTemplate.ValueSelectionChoice()(
                                    **item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetElementSeachCategoriesResponse.__call__ searchCategories type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(searchCategories)))
                self.set_search_categories(searchCategories)
                return self

            @property
            def get_search_categories(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                if self.searchCategories is not None and not (isinstance(self.searchCategories, list) and all(
                    isinstance(item, RestApi.Admin.ConfigTemplate.ValueSelectionChoice) for item in
                    self.searchCategories)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesResponse.get_search_categories type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]".format(
                            type_name=type(self.searchCategories)), -1)
                if self.searchCategories is None:
                    return list()
                else:
                    return self.searchCategories

            def set_search_categories(self,
                                      search_categories
                                      # type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]
                                      ):
                # type: (...) -> None
                if search_categories is not None and not (isinstance(search_categories, list) and all(
                    isinstance(item, RestApi.Admin.ConfigTemplate.ValueSelectionChoice) for item in
                    search_categories)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementSeachCategoriesResponse.set_search_categories type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigTemplate.ValueSelectionChoice]]]".format(
                            type_name=type(search_categories)), -1)
                if search_categories is None:
                    self.searchCategories = list()
                else:
                    self.searchCategories = search_categories

        class GetModuleElementsFirstRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementType = None  # type: str
                self.count = None  # type: int
                self.refreshCache = None  # type: bool
                self.searchFilter = None  # type: Optional[str]

            def __call__(self,
                         elementType,  # type: str
                         count,  # type: int
                         refreshCache,  # type: bool
                         searchFilter=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsFirstRequest]
                self.set_element_type(elementType)
                self.set_count(count)
                self.set_refresh_cache(refreshCache)
                self.set_search_filter(searchFilter)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_type(self):
                # type: () -> str
                if not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            @property
            def get_count(self):
                # type: () -> int
                if not isinstance(self.count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.get_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.count)), -1)
                return self.count

            @property
            def get_refresh_cache(self):
                # type: () -> bool
                if not isinstance(self.refreshCache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.get_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.refreshCache)), -1)
                return self.refreshCache

            @property
            def get_search_filter(self):
                # type: () -> Optional[str]
                if self.searchFilter is not None and not isinstance(self.searchFilter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.get_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.searchFilter)), -1)
                return self.searchFilter

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_type(self,
                                 element_type  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

            def set_count(self,
                          count  # type: int
                          ):
                # type: (...) -> None
                if not isinstance(count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.set_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(count)), -1)
                self.count = count

            def set_refresh_cache(self,
                                  refresh_cache  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(refresh_cache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.set_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(refresh_cache)), -1)
                self.refreshCache = refresh_cache

            def set_search_filter(self,
                                  search_filter  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if search_filter is not None and not isinstance(search_filter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstRequest.set_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(search_filter)), -1)
                self.searchFilter = search_filter

        class GetModuleElementsFirstResponse(object):
            def __init__(self):
                # type: () -> None
                self.nextSessionId = None  # type: Optional[str]
                self.countMax = None  # type: Optional[int]
                self.resultTruncated = None  # type: Optional[bool]
                self.elements = list()  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]

            def __call__(self,
                         nextSessionId=None,  # type: Optional[str]
                         countMax=None,  # type: Optional[int]
                         resultTruncated=None,  # type: Optional[bool]
                         elements=None,  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsFirstResponse]
                self.set_next_session_id(nextSessionId)
                self.set_count_max(countMax)
                self.set_result_truncated(resultTruncated)
                if elements is not None:
                    if isinstance(elements, list):
                        for index, item in enumerate(elements):
                            if isinstance(item, dict):
                                elements[index] = RestApi.Admin.ConfigElement()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetModuleElementsFirstResponse.__call__ elements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(elements)))
                self.set_elements(elements)
                return self

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            @property
            def get_count_max(self):
                # type: () -> Optional[int]
                if self.countMax is not None and not isinstance(self.countMax, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.get_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.countMax)), -1)
                return self.countMax

            @property
            def get_result_truncated(self):
                # type: () -> Optional[bool]
                if self.resultTruncated is not None and not isinstance(self.resultTruncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.get_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.resultTruncated)), -1)
                return self.resultTruncated

            @property
            def get_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ConfigElement]]]
                if self.elements is not None and not (isinstance(self.elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

            def set_count_max(self,
                              count_max  # type: Optional[int]
                              ):
                # type: (...) -> None
                if count_max is not None and not isinstance(count_max, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.set_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(count_max)), -1)
                self.countMax = count_max

            def set_result_truncated(self,
                                     result_truncated  # type: Optional[bool]
                                     ):
                # type: (...) -> None
                if result_truncated is not None and not isinstance(result_truncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.set_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(result_truncated)), -1)
                self.resultTruncated = result_truncated

            def set_elements(self,
                             elements  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                             ):
                # type: (...) -> None
                if elements is not None and not (isinstance(elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsFirstResponse.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

        class GetModuleElementsNextRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementType = None  # type: str
                self.count = None  # type: int
                self.nextSessionId = None  # type: str

            def __call__(self,
                         elementType,  # type: str
                         count,  # type: int
                         nextSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsNextRequest]
                self.set_element_type(elementType)
                self.set_count(count)
                self.set_next_session_id(nextSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_type(self):
                # type: () -> str
                if not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            @property
            def get_count(self):
                # type: () -> int
                if not isinstance(self.count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.get_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.count)), -1)
                return self.count

            @property
            def get_next_session_id(self):
                # type: () -> str
                if not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_type(self,
                                 element_type  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

            def set_count(self,
                          count  # type: int
                          ):
                # type: (...) -> None
                if not isinstance(count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.set_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(count)), -1)
                self.count = count

            def set_next_session_id(self,
                                    next_session_id  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextRequest.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

        class GetModuleElementsNextResponse(object):
            def __init__(self):
                # type: () -> None
                self.nextSessionId = None  # type: Optional[str]
                self.countMax = None  # type: Optional[int]
                self.resultTruncated = None  # type: Optional[bool]
                self.elements = list()  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]

            def __call__(self,
                         nextSessionId=None,  # type: Optional[str]
                         countMax=None,  # type: Optional[int]
                         resultTruncated=None,  # type: Optional[bool]
                         elements=None,  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetModuleElementsNextResponse]
                self.set_next_session_id(nextSessionId)
                self.set_count_max(countMax)
                self.set_result_truncated(resultTruncated)
                if elements is not None:
                    if isinstance(elements, list):
                        for index, item in enumerate(elements):
                            if isinstance(item, dict):
                                elements[index] = RestApi.Admin.ConfigElement()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetModuleElementsNextResponse.__call__ elements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(elements)))
                self.set_elements(elements)
                return self

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            @property
            def get_count_max(self):
                # type: () -> Optional[int]
                if self.countMax is not None and not isinstance(self.countMax, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.get_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.countMax)), -1)
                return self.countMax

            @property
            def get_result_truncated(self):
                # type: () -> Optional[bool]
                if self.resultTruncated is not None and not isinstance(self.resultTruncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.get_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.resultTruncated)), -1)
                return self.resultTruncated

            @property
            def get_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ConfigElement]]]
                if self.elements is not None and not (isinstance(self.elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

            def set_count_max(self,
                              count_max  # type: Optional[int]
                              ):
                # type: (...) -> None
                if count_max is not None and not isinstance(count_max, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.set_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(count_max)), -1)
                self.countMax = count_max

            def set_result_truncated(self,
                                     result_truncated  # type: Optional[bool]
                                     ):
                # type: (...) -> None
                if result_truncated is not None and not isinstance(result_truncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.set_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(result_truncated)), -1)
                self.resultTruncated = result_truncated

            def set_elements(self,
                             elements  # type: Optional[List[Type[RestApi.Admin.ConfigElement]]]
                             ):
                # type: (...) -> None
                if elements is not None and not (isinstance(elements, list) and all(
                    isinstance(item, RestApi.Admin.ConfigElement) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetModuleElementsNextResponse.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ConfigElement]]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

        class GetRulesFirstRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.ruleType = None  # type: str
                self.count = None  # type: int
                self.searchFilter = None  # type: Optional[str]

            def __call__(self,
                         ruleType,  # type: str
                         count,  # type: int
                         searchFilter=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesFirstRequest]
                self.set_rule_type(ruleType)
                self.set_count(count)
                self.set_search_filter(searchFilter)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_rule_type(self):
                # type: () -> str
                if not isinstance(self.ruleType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.get_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleType)), -1)
                return self.ruleType

            @property
            def get_count(self):
                # type: () -> int
                if not isinstance(self.count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.get_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.count)), -1)
                return self.count

            @property
            def get_search_filter(self):
                # type: () -> Optional[str]
                if self.searchFilter is not None and not isinstance(self.searchFilter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.get_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.searchFilter)), -1)
                return self.searchFilter

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_rule_type(self,
                              rule_type  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(rule_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.set_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_type)), -1)
                self.ruleType = rule_type

            def set_count(self,
                          count  # type: int
                          ):
                # type: (...) -> None
                if not isinstance(count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.set_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(count)), -1)
                self.count = count

            def set_search_filter(self,
                                  search_filter  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if search_filter is not None and not isinstance(search_filter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstRequest.set_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(search_filter)), -1)
                self.searchFilter = search_filter

        class GetRulesFirstResponse(object):
            def __init__(self):
                # type: () -> None
                self.nextSessionId = None  # type: Optional[str]
                self.countMax = None  # type: Optional[int]
                self.resultTruncated = None  # type: Optional[bool]
                self.rules = list()  # type: Optional[List[Type[RestApi.Admin.Rule]]]

            def __call__(self,
                         nextSessionId=None,  # type: Optional[str]
                         countMax=None,  # type: Optional[int]
                         resultTruncated=None,  # type: Optional[bool]
                         rules=None,  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesFirstResponse]
                self.set_next_session_id(nextSessionId)
                self.set_count_max(countMax)
                self.set_result_truncated(resultTruncated)
                if rules is not None:
                    if isinstance(rules, list):
                        for index, item in enumerate(rules):
                            if isinstance(item, dict):
                                rules[index] = RestApi.Admin.Rule()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesFirstResponse.__call__ rules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(rules)))
                self.set_rules(rules)
                return self

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            @property
            def get_count_max(self):
                # type: () -> Optional[int]
                if self.countMax is not None and not isinstance(self.countMax, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.get_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.countMax)), -1)
                return self.countMax

            @property
            def get_result_truncated(self):
                # type: () -> Optional[bool]
                if self.resultTruncated is not None and not isinstance(self.resultTruncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.get_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.resultTruncated)), -1)
                return self.resultTruncated

            @property
            def get_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Rule]]]
                if self.rules is not None and not (
                    isinstance(self.rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in self.rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.get_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(self.rules)), -1)
                if self.rules is None:
                    return list()
                else:
                    return self.rules

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

            def set_count_max(self,
                              count_max  # type: Optional[int]
                              ):
                # type: (...) -> None
                if count_max is not None and not isinstance(count_max, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.set_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(count_max)), -1)
                self.countMax = count_max

            def set_result_truncated(self,
                                     result_truncated  # type: Optional[bool]
                                     ):
                # type: (...) -> None
                if result_truncated is not None and not isinstance(result_truncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.set_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(result_truncated)), -1)
                self.resultTruncated = result_truncated

            def set_rules(self,
                          rules  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                          ):
                # type: (...) -> None
                if rules is not None and not (
                    isinstance(rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesFirstResponse.set_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(rules)), -1)
                if rules is None:
                    self.rules = list()
                else:
                    self.rules = rules

        class GetRulesNextRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.ruleType = None  # type: str
                self.count = None  # type: int
                self.nextSessionId = None  # type: str

            def __call__(self,
                         ruleType,  # type: str
                         count,  # type: int
                         nextSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesNextRequest]
                self.set_rule_type(ruleType)
                self.set_count(count)
                self.set_next_session_id(nextSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_rule_type(self):
                # type: () -> str
                if not isinstance(self.ruleType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.get_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleType)), -1)
                return self.ruleType

            @property
            def get_count(self):
                # type: () -> int
                if not isinstance(self.count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.get_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.count)), -1)
                return self.count

            @property
            def get_next_session_id(self):
                # type: () -> str
                if not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_rule_type(self,
                              rule_type  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(rule_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.set_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_type)), -1)
                self.ruleType = rule_type

            def set_count(self,
                          count  # type: int
                          ):
                # type: (...) -> None
                if not isinstance(count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.set_count type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(count)), -1)
                self.count = count

            def set_next_session_id(self,
                                    next_session_id  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextRequest.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

        class GetRulesNextResponse(object):
            def __init__(self):
                # type: () -> None
                self.nextSessionId = None  # type: Optional[str]
                self.countMax = None  # type: Optional[int]
                self.resultTruncated = None  # type: Optional[bool]
                self.rules = list()  # type: Optional[List[Type[RestApi.Admin.Rule]]]

            def __call__(self,
                         nextSessionId=None,  # type: Optional[str]
                         countMax=None,  # type: Optional[int]
                         resultTruncated=None,  # type: Optional[bool]
                         rules=None,  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesNextResponse]
                self.set_next_session_id(nextSessionId)
                self.set_count_max(countMax)
                self.set_result_truncated(resultTruncated)
                if rules is not None:
                    if isinstance(rules, list):
                        for index, item in enumerate(rules):
                            if isinstance(item, dict):
                                rules[index] = RestApi.Admin.Rule()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesNextResponse.__call__ rules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(rules)))
                self.set_rules(rules)
                return self

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            @property
            def get_count_max(self):
                # type: () -> Optional[int]
                if self.countMax is not None and not isinstance(self.countMax, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.get_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.countMax)), -1)
                return self.countMax

            @property
            def get_result_truncated(self):
                # type: () -> Optional[bool]
                if self.resultTruncated is not None and not isinstance(self.resultTruncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.get_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.resultTruncated)), -1)
                return self.resultTruncated

            @property
            def get_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Rule]]]
                if self.rules is not None and not (
                    isinstance(self.rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in self.rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.get_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(self.rules)), -1)
                if self.rules is None:
                    return list()
                else:
                    return self.rules

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

            def set_count_max(self,
                              count_max  # type: Optional[int]
                              ):
                # type: (...) -> None
                if count_max is not None and not isinstance(count_max, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.set_count_max type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(count_max)), -1)
                self.countMax = count_max

            def set_result_truncated(self,
                                     result_truncated  # type: Optional[bool]
                                     ):
                # type: (...) -> None
                if result_truncated is not None and not isinstance(result_truncated, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.set_result_truncated type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(result_truncated)), -1)
                self.resultTruncated = result_truncated

            def set_rules(self,
                          rules  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                          ):
                # type: (...) -> None
                if rules is not None and not (
                    isinstance(rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesNextResponse.set_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(rules)), -1)
                if rules is None:
                    self.rules = list()
                else:
                    self.rules = rules

        class AddMembersToFirstTimeEnrollersRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.noWarning = None  # type: Optional[bool]

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         noWarning=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddMembersToFirstTimeEnrollersRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                self.set_no_warning(noWarning)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_no_warning(self):
                # type: () -> Optional[bool]
                if self.noWarning is not None and not isinstance(self.noWarning, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.get_no_warning type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.noWarning)), -1)
                return self.noWarning

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_no_warning(self,
                               no_warning  # type: Optional[bool]
                               ):
                # type: (...) -> None
                if no_warning is not None and not isinstance(no_warning, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersRequest.set_no_warning type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(no_warning)), -1)
                self.noWarning = no_warning

        class AddMembersToFirstTimeEnrollersResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.elementsAdded = None  # type: Optional[int]
                self.warningMsg = None  # type: Optional[str]

            def __call__(self,
                         rc,  # type: bool
                         elementsAdded=None,  # type: Optional[int]
                         warningMsg=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddMembersToFirstTimeEnrollersResponse]
                self.set_rc(rc)
                self.set_elements_added(elementsAdded)
                self.set_warning_msg(warningMsg)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_elements_added(self):
                # type: () -> Optional[int]
                if self.elementsAdded is not None and not isinstance(self.elementsAdded, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.get_elements_added type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.elementsAdded)), -1)
                return self.elementsAdded

            @property
            def get_warning_msg(self):
                # type: () -> Optional[str]
                if self.warningMsg is not None and not isinstance(self.warningMsg, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.get_warning_msg type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.warningMsg)), -1)
                return self.warningMsg

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_elements_added(self,
                                   elements_added  # type: Optional[int]
                                   ):
                # type: (...) -> None
                if elements_added is not None and not isinstance(elements_added, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.set_elements_added type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(elements_added)), -1)
                self.elementsAdded = elements_added

            def set_warning_msg(self,
                                warning_msg  # type: Optional[str]
                                ):
                # type: (...) -> None
                if warning_msg is not None and not isinstance(warning_msg, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddMembersToFirstTimeEnrollersResponse.set_warning_msg type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(warning_msg)), -1)
                self.warningMsg = warning_msg

        class Rule(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: int
                self.resultElement = None  # type: Type[RestApi.Admin.Element]
                self.conditionElements = list()  # type: Optional[List[Type[RestApi.Admin.Element]]]
                self.active = None  # type: Optional[bool]

            def __call__(self,
                         id_,  # type: int
                         resultElement,  # type: Type[RestApi.Admin.Element]
                         conditionElements=None,  # type: Optional[List[Type[RestApi.Admin.Element]]]
                         active=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.Rule]
                self.set_id(id_)
                if isinstance(resultElement, dict):
                    resultElement = RestApi.Admin.Element()(**resultElement)
                self.set_result_element(resultElement)
                if conditionElements is not None:
                    if isinstance(conditionElements, list):
                        for index, item in enumerate(conditionElements):
                            if isinstance(item, dict):
                                conditionElements[index] = RestApi.Admin.Element()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.Rule.__call__ conditionElements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(conditionElements)))
                self.set_condition_elements(conditionElements)
                self.set_active(active)
                return self

            @property
            def get_id(self):
                # type: () -> int
                if not isinstance(self.id_, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.get_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_result_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.resultElement, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.get_result_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.resultElement)), -1)
                return self.resultElement

            @property
            def get_condition_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Element]]]
                if self.conditionElements is not None and not (isinstance(self.conditionElements, list) and all(
                    isinstance(item, RestApi.Admin.Element) for item in self.conditionElements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.get_condition_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(self.conditionElements)), -1)
                if self.conditionElements is None:
                    return list()
                else:
                    return self.conditionElements

            @property
            def get_active(self):
                # type: () -> Optional[bool]
                if self.active is not None and not isinstance(self.active, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.get_active type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.active)), -1)
                return self.active

            def set_id(self,
                       id_  # type: int
                       ):
                # type: (...) -> None
                if not isinstance(id_, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.set_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_result_element(self,
                                   result_element  # type: Type[RestApi.Admin.Element]
                                   ):
                # type: (...) -> None
                if not isinstance(result_element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.set_result_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(result_element)), -1)
                self.resultElement = result_element

            def set_condition_elements(self,
                                       condition_elements  # type: Optional[List[Type[RestApi.Admin.Element]]]
                                       ):
                # type: (...) -> None
                if condition_elements is not None and not (isinstance(condition_elements, list) and all(
                    isinstance(item, RestApi.Admin.Element) for item in condition_elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.set_condition_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Element]]]".format(
                            type_name=type(condition_elements)), -1)
                if condition_elements is None:
                    self.conditionElements = list()
                else:
                    self.conditionElements = condition_elements

            def set_active(self,
                           active  # type: Optional[bool]
                           ):
                # type: (...) -> None
                if active is not None and not isinstance(active, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Rule.set_active type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(active)), -1)
                self.active = active

        class GetRulesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.className = None  # type: str
                self.refreshCache = None  # type: bool

            def __call__(self,
                         className,  # type: str
                         refreshCache,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesRequest]
                self.set_class_name(className)
                self.set_refresh_cache(refreshCache)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_class_name(self):
                # type: () -> str
                if not isinstance(self.className, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.get_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.className)), -1)
                return self.className

            @property
            def get_refresh_cache(self):
                # type: () -> bool
                if not isinstance(self.refreshCache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.get_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.refreshCache)), -1)
                return self.refreshCache

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_class_name(self,
                               class_name  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(class_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.set_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(class_name)), -1)
                self.className = class_name

            def set_refresh_cache(self,
                                  refresh_cache  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(refresh_cache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesRequest.set_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(refresh_cache)), -1)
                self.refreshCache = refresh_cache

        class GetRulesResponse(object):
            def __init__(self):
                # type: () -> None
                self.rules = list()  # type: Optional[List[Type[RestApi.Admin.Rule]]]

            def __call__(self,
                         rules=None,  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesResponse]
                if rules is not None:
                    if isinstance(rules, list):
                        for index, item in enumerate(rules):
                            if isinstance(item, dict):
                                rules[index] = RestApi.Admin.Rule()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesResponse.__call__ rules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(rules)))
                self.set_rules(rules)
                return self

            @property
            def get_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Rule]]]
                if self.rules is not None and not (
                    isinstance(self.rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in self.rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesResponse.get_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(self.rules)), -1)
                if self.rules is None:
                    return list()
                else:
                    return self.rules

            def set_rules(self,
                          rules  # type: Optional[List[Type[RestApi.Admin.Rule]]]
                          ):
                # type: (...) -> None
                if rules is not None and not (
                    isinstance(rules, list) and all(isinstance(item, RestApi.Admin.Rule) for item in rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesResponse.set_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Rule]]]".format(
                            type_name=type(rules)), -1)
                if rules is None:
                    self.rules = list()
                else:
                    self.rules = rules

        class CreateRuleRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.className = None  # type: str
                self.rule = None  # type: Type[RestApi.Admin.Rule]

            def __call__(self,
                         className,  # type: str
                         rule,  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> Type[RestApi.Admin.CreateRuleRequest]
                self.set_class_name(className)
                if isinstance(rule, dict):
                    rule = RestApi.Admin.Rule()(**rule)
                self.set_rule(rule)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_class_name(self):
                # type: () -> str
                if not isinstance(self.className, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.get_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.className)), -1)
                return self.className

            @property
            def get_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.get_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.rule)), -1)
                return self.rule

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_class_name(self,
                               class_name  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(class_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.set_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(class_name)), -1)
                self.className = class_name

            def set_rule(self,
                         rule  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> None
                if not isinstance(rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleRequest.set_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(rule)), -1)
                self.rule = rule

        class CreateRuleResponse(object):
            def __init__(self):
                # type: () -> None
                self.rule = None  # type: Type[RestApi.Admin.Rule]

            def __call__(self,
                         rule,  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> Type[RestApi.Admin.CreateRuleResponse]
                if isinstance(rule, dict):
                    rule = RestApi.Admin.Rule()(**rule)
                self.set_rule(rule)
                return self

            @property
            def get_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleResponse.get_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.rule)), -1)
                return self.rule

            def set_rule(self,
                         rule  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> None
                if not isinstance(rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateRuleResponse.set_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(rule)), -1)
                self.rule = rule

        class UpdateRuleRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.className = None  # type: str
                self.oldRule = None  # type: Type[RestApi.Admin.Rule]
                self.newRule = None  # type: Type[RestApi.Admin.Rule]

            def __call__(self,
                         className,  # type: str
                         oldRule,  # type: Type[RestApi.Admin.Rule]
                         newRule,  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateRuleRequest]
                self.set_class_name(className)
                if isinstance(oldRule, dict):
                    oldRule = RestApi.Admin.Rule()(**oldRule)
                self.set_old_rule(oldRule)
                if isinstance(newRule, dict):
                    newRule = RestApi.Admin.Rule()(**newRule)
                self.set_new_rule(newRule)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_class_name(self):
                # type: () -> str
                if not isinstance(self.className, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.get_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.className)), -1)
                return self.className

            @property
            def get_old_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.oldRule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.get_old_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.oldRule)), -1)
                return self.oldRule

            @property
            def get_new_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.newRule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.get_new_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.newRule)), -1)
                return self.newRule

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_class_name(self,
                               class_name  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(class_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.set_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(class_name)), -1)
                self.className = class_name

            def set_old_rule(self,
                             old_rule  # type: Type[RestApi.Admin.Rule]
                             ):
                # type: (...) -> None
                if not isinstance(old_rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.set_old_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(old_rule)), -1)
                self.oldRule = old_rule

            def set_new_rule(self,
                             new_rule  # type: Type[RestApi.Admin.Rule]
                             ):
                # type: (...) -> None
                if not isinstance(new_rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleRequest.set_new_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(new_rule)), -1)
                self.newRule = new_rule

        class UpdateRuleResponse(object):
            def __init__(self):
                # type: () -> None
                self.rule = None  # type: Type[RestApi.Admin.Rule]

            def __call__(self,
                         rule,  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateRuleResponse]
                if isinstance(rule, dict):
                    rule = RestApi.Admin.Rule()(**rule)
                self.set_rule(rule)
                return self

            @property
            def get_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleResponse.get_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.rule)), -1)
                return self.rule

            def set_rule(self,
                         rule  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> None
                if not isinstance(rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateRuleResponse.set_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(rule)), -1)
                self.rule = rule

        class DeleteRuleRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.className = None  # type: str
                self.rule = None  # type: Type[RestApi.Admin.Rule]

            def __call__(self,
                         className,  # type: str
                         rule,  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> Type[RestApi.Admin.DeleteRuleRequest]
                self.set_class_name(className)
                if isinstance(rule, dict):
                    rule = RestApi.Admin.Rule()(**rule)
                self.set_rule(rule)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_class_name(self):
                # type: () -> str
                if not isinstance(self.className, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.get_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.className)), -1)
                return self.className

            @property
            def get_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.get_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.rule)), -1)
                return self.rule

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_class_name(self,
                               class_name  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(class_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.set_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(class_name)), -1)
                self.className = class_name

            def set_rule(self,
                         rule  # type: Type[RestApi.Admin.Rule]
                         ):
                # type: (...) -> None
                if not isinstance(rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleRequest.set_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(rule)), -1)
                self.rule = rule

        class DeleteRuleResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.DeleteRuleResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteRuleResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class StingList(object):
            def __init__(self):
                # type: () -> None
                self.elements = list()  # type: Optional[List[str]]

            def __call__(self,
                         elements=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.StingList]
                self.set_elements(elements)
                return self

            @property
            def get_elements(self):
                # type: () -> Optional[List[str]]
                if self.elements is not None and not (
                    isinstance(self.elements, list) and all(isinstance(item, basestring) for item in self.elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StingList.get_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.elements)), -1)
                if self.elements is None:
                    return list()
                else:
                    return self.elements

            def set_elements(self,
                             elements  # type: Optional[List[str]]
                             ):
                # type: (...) -> None
                if elements is not None and not (
                    isinstance(elements, list) and all(isinstance(item, basestring) for item in elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StingList.set_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(elements)), -1)
                if elements is None:
                    self.elements = list()
                else:
                    self.elements = elements

        class StringMap(object):
            def __init__(self):
                # type: () -> None
                self.values = list()  # type: List[str]
                self.key = None  # type: Optional[str]

            def __call__(self,
                         values,  # type: List[str]
                         key=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.StringMap]
                self.set_values(values)
                self.set_key(key)
                return self

            @property
            def get_values(self):
                # type: () -> List[str]
                if not (isinstance(self.values, list) and all(isinstance(item, basestring) for item in self.values)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StringMap.get_values type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(self.values)), -1)
                return self.values

            @property
            def get_key(self):
                # type: () -> Optional[str]
                if self.key is not None and not isinstance(self.key, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StringMap.get_key type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.key)), -1)
                return self.key

            def set_values(self,
                           values  # type: List[str]
                           ):
                # type: (...) -> None
                if not (isinstance(values, list) and all(isinstance(item, basestring) for item in values)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StringMap.set_values type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(values)), -1)
                self.values = values

            def set_key(self,
                        key  # type: Optional[str]
                        ):
                # type: (...) -> None
                if key is not None and not isinstance(key, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.StringMap.set_key type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(key)), -1)
                self.key = key

        class RuleSpec(object):
            def __init__(self):
                # type: () -> None
                self.ruleTitle = None  # type: str
                self.ruleTitleLong = None  # type: str
                self.uniqueElements = list()  # type: Optional[List[str]]
                self.mandatoryElements = list()  # type: Optional[List[Type[RestApi.Admin.StingList]]]
                self.headerRule = None  # type: Type[RestApi.Admin.Rule]
                self.templateRule = None  # type: Type[RestApi.Admin.Rule]
                self.entityTypeMaps = list()  # type: Optional[List[Type[RestApi.Admin.StringMap]]]
                self.createEnabled = None  # type: Optional[bool]
                self.editEnabled = None  # type: Optional[bool]
                self.deleteEnabled = None  # type: Optional[bool]

            def __call__(self,
                         ruleTitle,  # type: str
                         ruleTitleLong,  # type: str
                         headerRule,  # type: Type[RestApi.Admin.Rule]
                         templateRule,  # type: Type[RestApi.Admin.Rule]
                         uniqueElements=None,  # type: Optional[List[str]]
                         mandatoryElements=None,  # type: Optional[List[Type[RestApi.Admin.StingList]]]
                         entityTypeMaps=None,  # type: Optional[List[Type[RestApi.Admin.StringMap]]]
                         createEnabled=None,  # type: Optional[bool]
                         editEnabled=None,  # type: Optional[bool]
                         deleteEnabled=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.RuleSpec]
                self.set_rule_title(ruleTitle)
                self.set_rule_title_long(ruleTitleLong)
                if isinstance(headerRule, dict):
                    headerRule = RestApi.Admin.Rule()(**headerRule)
                self.set_header_rule(headerRule)
                if isinstance(templateRule, dict):
                    templateRule = RestApi.Admin.Rule()(**templateRule)
                self.set_template_rule(templateRule)
                self.set_unique_elements(uniqueElements)
                if mandatoryElements is not None:
                    if isinstance(mandatoryElements, list):
                        for index, item in enumerate(mandatoryElements):
                            if isinstance(item, dict):
                                mandatoryElements[index] = RestApi.Admin.StingList()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.RuleSpec.__call__ mandatoryElements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(mandatoryElements)))
                self.set_mandatory_elements(mandatoryElements)
                if entityTypeMaps is not None:
                    if isinstance(entityTypeMaps, list):
                        for index, item in enumerate(entityTypeMaps):
                            if isinstance(item, dict):
                                entityTypeMaps[index] = RestApi.Admin.StringMap()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.RuleSpec.__call__ entityTypeMaps type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(entityTypeMaps)))
                self.set_entity_type_maps(entityTypeMaps)
                self.set_create_enabled(createEnabled)
                self.set_edit_enabled(editEnabled)
                self.set_delete_enabled(deleteEnabled)
                return self

            @property
            def get_rule_title(self):
                # type: () -> str
                if not isinstance(self.ruleTitle, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_rule_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleTitle)), -1)
                return self.ruleTitle

            @property
            def get_rule_title_long(self):
                # type: () -> str
                if not isinstance(self.ruleTitleLong, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_rule_title_long type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleTitleLong)), -1)
                return self.ruleTitleLong

            @property
            def get_unique_elements(self):
                # type: () -> Optional[List[str]]
                if self.uniqueElements is not None and not (isinstance(self.uniqueElements, list) and all(
                    isinstance(item, basestring) for item in self.uniqueElements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_unique_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.uniqueElements)), -1)
                if self.uniqueElements is None:
                    return list()
                else:
                    return self.uniqueElements

            @property
            def get_mandatory_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.StingList]]]
                if self.mandatoryElements is not None and not (isinstance(self.mandatoryElements, list) and all(
                    isinstance(item, RestApi.Admin.StingList) for item in self.mandatoryElements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_mandatory_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.StingList]]]".format(
                            type_name=type(self.mandatoryElements)), -1)
                if self.mandatoryElements is None:
                    return list()
                else:
                    return self.mandatoryElements

            @property
            def get_header_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.headerRule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_header_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.headerRule)), -1)
                return self.headerRule

            @property
            def get_template_rule(self):
                # type: () -> Type[RestApi.Admin.Rule]
                if not isinstance(self.templateRule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_template_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(self.templateRule)), -1)
                return self.templateRule

            @property
            def get_entity_type_maps(self):
                # type: () -> Optional[List[Type[RestApi.Admin.StringMap]]]
                if self.entityTypeMaps is not None and not (isinstance(self.entityTypeMaps, list) and all(
                    isinstance(item, RestApi.Admin.StringMap) for item in self.entityTypeMaps)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_entity_type_maps type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.StringMap]]]".format(
                            type_name=type(self.entityTypeMaps)), -1)
                if self.entityTypeMaps is None:
                    return list()
                else:
                    return self.entityTypeMaps

            @property
            def get_create_enabled(self):
                # type: () -> Optional[bool]
                if self.createEnabled is not None and not isinstance(self.createEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_create_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.createEnabled)), -1)
                return self.createEnabled

            @property
            def get_edit_enabled(self):
                # type: () -> Optional[bool]
                if self.editEnabled is not None and not isinstance(self.editEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_edit_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.editEnabled)), -1)
                return self.editEnabled

            @property
            def get_delete_enabled(self):
                # type: () -> Optional[bool]
                if self.deleteEnabled is not None and not isinstance(self.deleteEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.get_delete_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.deleteEnabled)), -1)
                return self.deleteEnabled

            def set_rule_title(self,
                               rule_title  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(rule_title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_rule_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_title)), -1)
                self.ruleTitle = rule_title

            def set_rule_title_long(self,
                                    rule_title_long  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(rule_title_long, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_rule_title_long type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_title_long)), -1)
                self.ruleTitleLong = rule_title_long

            def set_unique_elements(self,
                                    unique_elements  # type: Optional[List[str]]
                                    ):
                # type: (...) -> None
                if unique_elements is not None and not (isinstance(unique_elements, list) and all(
                    isinstance(item, basestring) for item in unique_elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_unique_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(unique_elements)), -1)
                if unique_elements is None:
                    self.uniqueElements = list()
                else:
                    self.uniqueElements = unique_elements

            def set_mandatory_elements(self,
                                       mandatory_elements  # type: Optional[List[Type[RestApi.Admin.StingList]]]
                                       ):
                # type: (...) -> None
                if mandatory_elements is not None and not (isinstance(mandatory_elements, list) and all(
                    isinstance(item, RestApi.Admin.StingList) for item in mandatory_elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_mandatory_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.StingList]]]".format(
                            type_name=type(mandatory_elements)), -1)
                if mandatory_elements is None:
                    self.mandatoryElements = list()
                else:
                    self.mandatoryElements = mandatory_elements

            def set_header_rule(self,
                                header_rule  # type: Type[RestApi.Admin.Rule]
                                ):
                # type: (...) -> None
                if not isinstance(header_rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_header_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(header_rule)), -1)
                self.headerRule = header_rule

            def set_template_rule(self,
                                  template_rule  # type: Type[RestApi.Admin.Rule]
                                  ):
                # type: (...) -> None
                if not isinstance(template_rule, RestApi.Admin.Rule):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_template_rule type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Rule]".format(
                            type_name=type(template_rule)), -1)
                self.templateRule = template_rule

            def set_entity_type_maps(self,
                                     entity_type_maps  # type: Optional[List[Type[RestApi.Admin.StringMap]]]
                                     ):
                # type: (...) -> None
                if entity_type_maps is not None and not (isinstance(entity_type_maps, list) and all(
                    isinstance(item, RestApi.Admin.StringMap) for item in entity_type_maps)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_entity_type_maps type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.StringMap]]]".format(
                            type_name=type(entity_type_maps)), -1)
                if entity_type_maps is None:
                    self.entityTypeMaps = list()
                else:
                    self.entityTypeMaps = entity_type_maps

            def set_create_enabled(self,
                                   create_enabled  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if create_enabled is not None and not isinstance(create_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_create_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(create_enabled)), -1)
                self.createEnabled = create_enabled

            def set_edit_enabled(self,
                                 edit_enabled  # type: Optional[bool]
                                 ):
                # type: (...) -> None
                if edit_enabled is not None and not isinstance(edit_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_edit_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(edit_enabled)), -1)
                self.editEnabled = edit_enabled

            def set_delete_enabled(self,
                                   delete_enabled  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if delete_enabled is not None and not isinstance(delete_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleSpec.set_delete_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(delete_enabled)), -1)
                self.deleteEnabled = delete_enabled

        class GetRuleSpecRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.className = None  # type: str

            def __call__(self,
                         className,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRuleSpecRequest]
                self.set_class_name(className)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_class_name(self):
                # type: () -> str
                if not isinstance(self.className, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecRequest.get_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.className)), -1)
                return self.className

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_class_name(self,
                               class_name  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(class_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecRequest.set_class_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(class_name)), -1)
                self.className = class_name

        class GetRuleSpecResponse(object):
            def __init__(self):
                # type: () -> None
                self.ruleSpec = None  # type: Type[RestApi.Admin.RuleSpec]

            def __call__(self,
                         ruleSpec,  # type: Type[RestApi.Admin.RuleSpec]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRuleSpecResponse]
                if isinstance(ruleSpec, dict):
                    ruleSpec = RestApi.Admin.RuleSpec()(**ruleSpec)
                self.set_rule_spec(ruleSpec)
                return self

            @property
            def get_rule_spec(self):
                # type: () -> Type[RestApi.Admin.RuleSpec]
                if not isinstance(self.ruleSpec, RestApi.Admin.RuleSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecResponse.get_rule_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.RuleSpec]".format(
                            type_name=type(self.ruleSpec)), -1)
                return self.ruleSpec

            def set_rule_spec(self,
                              rule_spec  # type: Type[RestApi.Admin.RuleSpec]
                              ):
                # type: (...) -> None
                if not isinstance(rule_spec, RestApi.Admin.RuleSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRuleSpecResponse.set_rule_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.RuleSpec]".format(
                            type_name=type(rule_spec)), -1)
                self.ruleSpec = rule_spec

        class Condition(object):
            def __init__(self):
                # type: () -> None
                self.conditionId = None  # type: str
                self.entityType = None  # type: str
                self.ruleEntityType = None  # type: str

            def __call__(self,
                         conditionId,  # type: str
                         entityType,  # type: str
                         ruleEntityType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.Condition]
                self.set_condition_id(conditionId)
                self.set_entity_type(entityType)
                self.set_rule_entity_type(ruleEntityType)
                return self

            @property
            def get_condition_id(self):
                # type: () -> str
                if not isinstance(self.conditionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.get_condition_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.conditionId)), -1)
                return self.conditionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_rule_entity_type(self):
                # type: () -> str
                if not isinstance(self.ruleEntityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.get_rule_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleEntityType)), -1)
                return self.ruleEntityType

            def set_condition_id(self,
                                 condition_id  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(condition_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.set_condition_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(condition_id)), -1)
                self.conditionId = condition_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_rule_entity_type(self,
                                     rule_entity_type  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(rule_entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Condition.set_rule_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_entity_type)), -1)
                self.ruleEntityType = rule_entity_type

        class RuleType1(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: str
                self.entityType = None  # type: str
                self.resultId = None  # type: str
                self.ruleType = None  # type: str
                self.conditionElements = list()  # type: Optional[List[Type[RestApi.Admin.Condition]]]
                self.active = None  # type: Optional[bool]

            def __call__(self,
                         id_,  # type: str
                         entityType,  # type: str
                         resultId,  # type: str
                         ruleType,  # type: str
                         conditionElements=None,  # type: Optional[List[Type[RestApi.Admin.Condition]]]
                         active=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.RuleType1]
                self.set_id(id_)
                self.set_entity_type(entityType)
                self.set_result_id(resultId)
                self.set_rule_type(ruleType)
                if conditionElements is not None:
                    if isinstance(conditionElements, list):
                        for index, item in enumerate(conditionElements):
                            if isinstance(item, dict):
                                conditionElements[index] = RestApi.Admin.Condition()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.RuleType1.__call__ conditionElements type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(conditionElements)))
                self.set_condition_elements(conditionElements)
                self.set_active(active)
                return self

            @property
            def get_id(self):
                # type: () -> str
                if not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_result_id(self):
                # type: () -> str
                if not isinstance(self.resultId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_result_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.resultId)), -1)
                return self.resultId

            @property
            def get_rule_type(self):
                # type: () -> str
                if not isinstance(self.ruleType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.ruleType)), -1)
                return self.ruleType

            @property
            def get_condition_elements(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Condition]]]
                if self.conditionElements is not None and not (isinstance(self.conditionElements, list) and all(
                    isinstance(item, RestApi.Admin.Condition) for item in self.conditionElements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_condition_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Condition]]]".format(
                            type_name=type(self.conditionElements)), -1)
                if self.conditionElements is None:
                    return list()
                else:
                    return self.conditionElements

            @property
            def get_active(self):
                # type: () -> Optional[bool]
                if self.active is not None and not isinstance(self.active, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.get_active type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.active)), -1)
                return self.active

            def set_id(self,
                       id_  # type: str
                       ):
                # type: (...) -> None
                if not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_result_id(self,
                              result_id  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(result_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_result_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(result_id)), -1)
                self.resultId = result_id

            def set_rule_type(self,
                              rule_type  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(rule_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_rule_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(rule_type)), -1)
                self.ruleType = rule_type

            def set_condition_elements(self,
                                       condition_elements  # type: Optional[List[Type[RestApi.Admin.Condition]]]
                                       ):
                # type: (...) -> None
                if condition_elements is not None and not (isinstance(condition_elements, list) and all(
                    isinstance(item, RestApi.Admin.Condition) for item in condition_elements)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_condition_elements type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Condition]]]".format(
                            type_name=type(condition_elements)), -1)
                if condition_elements is None:
                    self.conditionElements = list()
                else:
                    self.conditionElements = condition_elements

            def set_active(self,
                           active  # type: Optional[bool]
                           ):
                # type: (...) -> None
                if active is not None and not isinstance(active, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RuleType1.set_active type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(active)), -1)
                self.active = active

        class GetRulesForElementRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.elementId = None  # type: str
                self.entityType = None  # type: str
                self.knownRules = list()  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]

            def __call__(self,
                         elementId,  # type: str
                         entityType,  # type: str
                         knownRules=None,  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesForElementRequest]
                self.set_element_id(elementId)
                self.set_entity_type(entityType)
                if knownRules is not None:
                    if isinstance(knownRules, list):
                        for index, item in enumerate(knownRules):
                            if isinstance(item, dict):
                                knownRules[index] = RestApi.Admin.RuleType1()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesForElementRequest.__call__ knownRules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(knownRules)))
                self.set_known_rules(knownRules)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element_id(self):
                # type: () -> str
                if not isinstance(self.elementId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.get_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementId)), -1)
                return self.elementId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_known_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.RuleType1]]]
                if self.knownRules is not None and not (isinstance(self.knownRules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in self.knownRules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.get_known_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(self.knownRules)), -1)
                if self.knownRules is None:
                    return list()
                else:
                    return self.knownRules

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element_id(self,
                               element_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(element_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.set_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_id)), -1)
                self.elementId = element_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_known_rules(self,
                                known_rules  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                                ):
                # type: (...) -> None
                if known_rules is not None and not (isinstance(known_rules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in known_rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementRequest.set_known_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(known_rules)), -1)
                if known_rules is None:
                    self.knownRules = list()
                else:
                    self.knownRules = known_rules

        class GetRulesForElementResponse(object):
            def __init__(self):
                # type: () -> None
                self.newRules = list()  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                self.deletedRules = list()  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]

            def __call__(self,
                         newRules=None,  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                         deletedRules=None,  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetRulesForElementResponse]
                if newRules is not None:
                    if isinstance(newRules, list):
                        for index, item in enumerate(newRules):
                            if isinstance(item, dict):
                                newRules[index] = RestApi.Admin.RuleType1()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesForElementResponse.__call__ newRules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(newRules)))
                self.set_new_rules(newRules)
                if deletedRules is not None:
                    if isinstance(deletedRules, list):
                        for index, item in enumerate(deletedRules):
                            if isinstance(item, dict):
                                deletedRules[index] = RestApi.Admin.RuleType1()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetRulesForElementResponse.__call__ deletedRules type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(deletedRules)))
                self.set_deleted_rules(deletedRules)
                return self

            @property
            def get_new_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.RuleType1]]]
                if self.newRules is not None and not (isinstance(self.newRules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in self.newRules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementResponse.get_new_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(self.newRules)), -1)
                if self.newRules is None:
                    return list()
                else:
                    return self.newRules

            @property
            def get_deleted_rules(self):
                # type: () -> Optional[List[Type[RestApi.Admin.RuleType1]]]
                if self.deletedRules is not None and not (isinstance(self.deletedRules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in self.deletedRules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementResponse.get_deleted_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(self.deletedRules)), -1)
                if self.deletedRules is None:
                    return list()
                else:
                    return self.deletedRules

            def set_new_rules(self,
                              new_rules  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                              ):
                # type: (...) -> None
                if new_rules is not None and not (isinstance(new_rules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in new_rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementResponse.set_new_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(new_rules)), -1)
                if new_rules is None:
                    self.newRules = list()
                else:
                    self.newRules = new_rules

            def set_deleted_rules(self,
                                  deleted_rules  # type: Optional[List[Type[RestApi.Admin.RuleType1]]]
                                  ):
                # type: (...) -> None
                if deleted_rules is not None and not (isinstance(deleted_rules, list) and all(
                    isinstance(item, RestApi.Admin.RuleType1) for item in deleted_rules)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetRulesForElementResponse.set_deleted_rules type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.RuleType1]]]".format(
                            type_name=type(deleted_rules)), -1)
                if deleted_rules is None:
                    self.deletedRules = list()
                else:
                    self.deletedRules = deleted_rules

        class ReportRef(object):
            def __init__(self):
                # type: () -> None
                self.moduleName = None  # type: str
                self.reportId = None  # type: str
                self.reportTitle = None  # type: str

            def __call__(self,
                         moduleName,  # type: str
                         reportId,  # type: str
                         reportTitle,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.ReportRef]
                self.set_module_name(moduleName)
                self.set_report_id(reportId)
                self.set_report_title(reportTitle)
                return self

            @property
            def get_module_name(self):
                # type: () -> str
                if not isinstance(self.moduleName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.get_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.moduleName)), -1)
                return self.moduleName

            @property
            def get_report_id(self):
                # type: () -> str
                if not isinstance(self.reportId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.get_report_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.reportId)), -1)
                return self.reportId

            @property
            def get_report_title(self):
                # type: () -> str
                if not isinstance(self.reportTitle, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.get_report_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.reportTitle)), -1)
                return self.reportTitle

            def set_module_name(self,
                                module_name  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(module_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.set_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(module_name)), -1)
                self.moduleName = module_name

            def set_report_id(self,
                              report_id  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(report_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.set_report_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(report_id)), -1)
                self.reportId = report_id

            def set_report_title(self,
                                 report_title  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(report_title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.ReportRef.set_report_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(report_title)), -1)
                self.reportTitle = report_title

        class GetReportsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetReportsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetReportsResponse(object):
            def __init__(self):
                # type: () -> None
                self.reports = list()  # type: Optional[List[Type[RestApi.Admin.ReportRef]]]

            def __call__(self,
                         reports=None,  # type: Optional[List[Type[RestApi.Admin.ReportRef]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetReportsResponse]
                if reports is not None:
                    if isinstance(reports, list):
                        for index, item in enumerate(reports):
                            if isinstance(item, dict):
                                reports[index] = RestApi.Admin.ReportRef()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetReportsResponse.__call__ reports type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(reports)))
                self.set_reports(reports)
                return self

            @property
            def get_reports(self):
                # type: () -> Optional[List[Type[RestApi.Admin.ReportRef]]]
                if self.reports is not None and not (isinstance(self.reports, list) and all(
                    isinstance(item, RestApi.Admin.ReportRef) for item in self.reports)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportsResponse.get_reports type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ReportRef]]]".format(
                            type_name=type(self.reports)), -1)
                if self.reports is None:
                    return list()
                else:
                    return self.reports

            def set_reports(self,
                            reports  # type: Optional[List[Type[RestApi.Admin.ReportRef]]]
                            ):
                # type: (...) -> None
                if reports is not None and not (
                    isinstance(reports, list) and all(isinstance(item, RestApi.Admin.ReportRef) for item in reports)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportsResponse.set_reports type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.ReportRef]]]".format(
                            type_name=type(reports)), -1)
                if reports is None:
                    self.reports = list()
                else:
                    self.reports = reports

        class GetReportSpecRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.moduleName = None  # type: str
                self.reportId = None  # type: str
                self.specificationFilename = None  # type: str

            def __call__(self,
                         moduleName,  # type: str
                         reportId,  # type: str
                         specificationFilename,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetReportSpecRequest]
                self.set_module_name(moduleName)
                self.set_report_id(reportId)
                self.set_specification_filename(specificationFilename)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_module_name(self):
                # type: () -> str
                if not isinstance(self.moduleName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.get_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.moduleName)), -1)
                return self.moduleName

            @property
            def get_report_id(self):
                # type: () -> str
                if not isinstance(self.reportId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.get_report_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.reportId)), -1)
                return self.reportId

            @property
            def get_specification_filename(self):
                # type: () -> str
                if not isinstance(self.specificationFilename, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.get_specification_filename type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.specificationFilename)), -1)
                return self.specificationFilename

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_module_name(self,
                                module_name  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(module_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.set_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(module_name)), -1)
                self.moduleName = module_name

            def set_report_id(self,
                              report_id  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(report_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.set_report_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(report_id)), -1)
                self.reportId = report_id

            def set_specification_filename(self,
                                           specification_filename  # type: str
                                           ):
                # type: (...) -> None
                if not isinstance(specification_filename, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecRequest.set_specification_filename type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(specification_filename)), -1)
                self.specificationFilename = specification_filename

        class GetReportSpecResponse(object):
            def __init__(self):
                # type: () -> None
                self.reportSpec = None  # type: str

            def __call__(self,
                         reportSpec,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetReportSpecResponse]
                self.set_report_spec(reportSpec)
                return self

            @property
            def get_report_spec(self):
                # type: () -> str
                if not isinstance(self.reportSpec, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecResponse.get_report_spec type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.reportSpec)), -1)
                return self.reportSpec

            def set_report_spec(self,
                                report_spec  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(report_spec, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportSpecResponse.set_report_spec type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(report_spec)), -1)
                self.reportSpec = report_spec

        class GetReportDataRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.moduleName = None  # type: str
                self.dataId = None  # type: str
                self.argInt01 = None  # type: Optional[int]
                self.argInt02 = None  # type: Optional[int]
                self.argInt03 = None  # type: Optional[int]
                self.argInt04 = None  # type: Optional[int]
                self.argInt05 = None  # type: Optional[int]
                self.argString01 = None  # type: Optional[str]
                self.argString02 = None  # type: Optional[str]
                self.argString03 = None  # type: Optional[str]
                self.argString04 = None  # type: Optional[str]
                self.argString05 = None  # type: Optional[str]
                self.argBoolean01 = None  # type: Optional[bool]
                self.argBoolean02 = None  # type: Optional[bool]
                self.argBoolean03 = None  # type: Optional[bool]
                self.argBoolean04 = None  # type: Optional[bool]
                self.argBoolean05 = None  # type: Optional[bool]

            def __call__(self,
                         moduleName,  # type: str
                         dataId,  # type: str
                         argInt01=None,  # type: Optional[int]
                         argInt02=None,  # type: Optional[int]
                         argInt03=None,  # type: Optional[int]
                         argInt04=None,  # type: Optional[int]
                         argInt05=None,  # type: Optional[int]
                         argString01=None,  # type: Optional[str]
                         argString02=None,  # type: Optional[str]
                         argString03=None,  # type: Optional[str]
                         argString04=None,  # type: Optional[str]
                         argString05=None,  # type: Optional[str]
                         argBoolean01=None,  # type: Optional[bool]
                         argBoolean02=None,  # type: Optional[bool]
                         argBoolean03=None,  # type: Optional[bool]
                         argBoolean04=None,  # type: Optional[bool]
                         argBoolean05=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetReportDataRequest]
                self.set_module_name(moduleName)
                self.set_data_id(dataId)
                self.set_arg_int_01(argInt01)
                self.set_arg_int_02(argInt02)
                self.set_arg_int_03(argInt03)
                self.set_arg_int_04(argInt04)
                self.set_arg_int_05(argInt05)
                self.set_arg_string_01(argString01)
                self.set_arg_string_02(argString02)
                self.set_arg_string_03(argString03)
                self.set_arg_string_04(argString04)
                self.set_arg_string_05(argString05)
                self.set_arg_boolean_01(argBoolean01)
                self.set_arg_boolean_02(argBoolean02)
                self.set_arg_boolean_03(argBoolean03)
                self.set_arg_boolean_04(argBoolean04)
                self.set_arg_boolean_05(argBoolean05)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_module_name(self):
                # type: () -> str
                if not isinstance(self.moduleName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.moduleName)), -1)
                return self.moduleName

            @property
            def get_data_id(self):
                # type: () -> str
                if not isinstance(self.dataId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_data_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.dataId)), -1)
                return self.dataId

            @property
            def get_arg_int_01(self):
                # type: () -> Optional[int]
                if self.argInt01 is not None and not isinstance(self.argInt01, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_int_01 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.argInt01)), -1)
                return self.argInt01

            @property
            def get_arg_int_02(self):
                # type: () -> Optional[int]
                if self.argInt02 is not None and not isinstance(self.argInt02, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_int_02 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.argInt02)), -1)
                return self.argInt02

            @property
            def get_arg_int_03(self):
                # type: () -> Optional[int]
                if self.argInt03 is not None and not isinstance(self.argInt03, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_int_03 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.argInt03)), -1)
                return self.argInt03

            @property
            def get_arg_int_04(self):
                # type: () -> Optional[int]
                if self.argInt04 is not None and not isinstance(self.argInt04, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_int_04 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.argInt04)), -1)
                return self.argInt04

            @property
            def get_arg_int_05(self):
                # type: () -> Optional[int]
                if self.argInt05 is not None and not isinstance(self.argInt05, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_int_05 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.argInt05)), -1)
                return self.argInt05

            @property
            def get_arg_string_01(self):
                # type: () -> Optional[str]
                if self.argString01 is not None and not isinstance(self.argString01, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_string_01 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.argString01)), -1)
                return self.argString01

            @property
            def get_arg_string_02(self):
                # type: () -> Optional[str]
                if self.argString02 is not None and not isinstance(self.argString02, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_string_02 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.argString02)), -1)
                return self.argString02

            @property
            def get_arg_string_03(self):
                # type: () -> Optional[str]
                if self.argString03 is not None and not isinstance(self.argString03, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_string_03 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.argString03)), -1)
                return self.argString03

            @property
            def get_arg_string_04(self):
                # type: () -> Optional[str]
                if self.argString04 is not None and not isinstance(self.argString04, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_string_04 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.argString04)), -1)
                return self.argString04

            @property
            def get_arg_string_05(self):
                # type: () -> Optional[str]
                if self.argString05 is not None and not isinstance(self.argString05, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_string_05 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.argString05)), -1)
                return self.argString05

            @property
            def get_arg_boolean_01(self):
                # type: () -> Optional[bool]
                if self.argBoolean01 is not None and not isinstance(self.argBoolean01, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_boolean_01 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.argBoolean01)), -1)
                return self.argBoolean01

            @property
            def get_arg_boolean_02(self):
                # type: () -> Optional[bool]
                if self.argBoolean02 is not None and not isinstance(self.argBoolean02, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_boolean_02 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.argBoolean02)), -1)
                return self.argBoolean02

            @property
            def get_arg_boolean_03(self):
                # type: () -> Optional[bool]
                if self.argBoolean03 is not None and not isinstance(self.argBoolean03, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_boolean_03 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.argBoolean03)), -1)
                return self.argBoolean03

            @property
            def get_arg_boolean_04(self):
                # type: () -> Optional[bool]
                if self.argBoolean04 is not None and not isinstance(self.argBoolean04, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_boolean_04 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.argBoolean04)), -1)
                return self.argBoolean04

            @property
            def get_arg_boolean_05(self):
                # type: () -> Optional[bool]
                if self.argBoolean05 is not None and not isinstance(self.argBoolean05, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.get_arg_boolean_05 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.argBoolean05)), -1)
                return self.argBoolean05

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_module_name(self,
                                module_name  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(module_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_module_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(module_name)), -1)
                self.moduleName = module_name

            def set_data_id(self,
                            data_id  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(data_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_data_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(data_id)), -1)
                self.dataId = data_id

            def set_arg_int_01(self,
                               arg_int_01  # type: Optional[int]
                               ):
                # type: (...) -> None
                if arg_int_01 is not None and not isinstance(arg_int_01, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_int_01 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(arg_int_01)), -1)
                self.argInt01 = arg_int_01

            def set_arg_int_02(self,
                               arg_int_02  # type: Optional[int]
                               ):
                # type: (...) -> None
                if arg_int_02 is not None and not isinstance(arg_int_02, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_int_02 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(arg_int_02)), -1)
                self.argInt02 = arg_int_02

            def set_arg_int_03(self,
                               arg_int_03  # type: Optional[int]
                               ):
                # type: (...) -> None
                if arg_int_03 is not None and not isinstance(arg_int_03, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_int_03 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(arg_int_03)), -1)
                self.argInt03 = arg_int_03

            def set_arg_int_04(self,
                               arg_int_04  # type: Optional[int]
                               ):
                # type: (...) -> None
                if arg_int_04 is not None and not isinstance(arg_int_04, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_int_04 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(arg_int_04)), -1)
                self.argInt04 = arg_int_04

            def set_arg_int_05(self,
                               arg_int_05  # type: Optional[int]
                               ):
                # type: (...) -> None
                if arg_int_05 is not None and not isinstance(arg_int_05, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_int_05 type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(arg_int_05)), -1)
                self.argInt05 = arg_int_05

            def set_arg_string_01(self,
                                  arg_string_01  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if arg_string_01 is not None and not isinstance(arg_string_01, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_string_01 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(arg_string_01)), -1)
                self.argString01 = arg_string_01

            def set_arg_string_02(self,
                                  arg_string_02  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if arg_string_02 is not None and not isinstance(arg_string_02, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_string_02 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(arg_string_02)), -1)
                self.argString02 = arg_string_02

            def set_arg_string_03(self,
                                  arg_string_03  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if arg_string_03 is not None and not isinstance(arg_string_03, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_string_03 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(arg_string_03)), -1)
                self.argString03 = arg_string_03

            def set_arg_string_04(self,
                                  arg_string_04  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if arg_string_04 is not None and not isinstance(arg_string_04, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_string_04 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(arg_string_04)), -1)
                self.argString04 = arg_string_04

            def set_arg_string_05(self,
                                  arg_string_05  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if arg_string_05 is not None and not isinstance(arg_string_05, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_string_05 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(arg_string_05)), -1)
                self.argString05 = arg_string_05

            def set_arg_boolean_01(self,
                                   arg_boolean_01  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if arg_boolean_01 is not None and not isinstance(arg_boolean_01, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_boolean_01 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(arg_boolean_01)), -1)
                self.argBoolean01 = arg_boolean_01

            def set_arg_boolean_02(self,
                                   arg_boolean_02  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if arg_boolean_02 is not None and not isinstance(arg_boolean_02, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_boolean_02 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(arg_boolean_02)), -1)
                self.argBoolean02 = arg_boolean_02

            def set_arg_boolean_03(self,
                                   arg_boolean_03  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if arg_boolean_03 is not None and not isinstance(arg_boolean_03, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_boolean_03 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(arg_boolean_03)), -1)
                self.argBoolean03 = arg_boolean_03

            def set_arg_boolean_04(self,
                                   arg_boolean_04  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if arg_boolean_04 is not None and not isinstance(arg_boolean_04, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_boolean_04 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(arg_boolean_04)), -1)
                self.argBoolean04 = arg_boolean_04

            def set_arg_boolean_05(self,
                                   arg_boolean_05  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if arg_boolean_05 is not None and not isinstance(arg_boolean_05, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataRequest.set_arg_boolean_05 type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(arg_boolean_05)), -1)
                self.argBoolean05 = arg_boolean_05

        class Values(object):
            def __init__(self):
                # type: () -> None
                self.values = list()  # type: Optional[List[str]]

            def __call__(self,
                         values=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.Values]
                self.set_values(values)
                return self

            @property
            def get_values(self):
                # type: () -> Optional[List[str]]
                if self.values is not None and not (
                    isinstance(self.values, list) and all(isinstance(item, basestring) for item in self.values)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Values.get_values type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.values)), -1)
                if self.values is None:
                    return list()
                else:
                    return self.values

            def set_values(self,
                           values  # type: Optional[List[str]]
                           ):
                # type: (...) -> None
                if values is not None and not (
                    isinstance(values, list) and all(isinstance(item, basestring) for item in values)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Values.set_values type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(values)), -1)
                if values is None:
                    self.values = list()
                else:
                    self.values = values

        class GetReportDataResponse(object):
            def __init__(self):
                # type: () -> None
                self.rows = list()  # type: Optional[List[Type[RestApi.Admin.Values]]]

            def __call__(self,
                         rows=None,  # type: Optional[List[Type[RestApi.Admin.Values]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetReportDataResponse]
                if rows is not None:
                    if isinstance(rows, list):
                        for index, item in enumerate(rows):
                            if isinstance(item, dict):
                                rows[index] = RestApi.Admin.Values()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetReportDataResponse.__call__ rows type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(rows)))
                self.set_rows(rows)
                return self

            @property
            def get_rows(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Values]]]
                if self.rows is not None and not (
                    isinstance(self.rows, list) and all(isinstance(item, RestApi.Admin.Values) for item in self.rows)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataResponse.get_rows type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Values]]]".format(
                            type_name=type(self.rows)), -1)
                if self.rows is None:
                    return list()
                else:
                    return self.rows

            def set_rows(self,
                         rows  # type: Optional[List[Type[RestApi.Admin.Values]]]
                         ):
                # type: (...) -> None
                if rows is not None and not (
                    isinstance(rows, list) and all(isinstance(item, RestApi.Admin.Values) for item in rows)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetReportDataResponse.set_rows type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Values]]]".format(
                            type_name=type(rows)), -1)
                if rows is None:
                    self.rows = list()
                else:
                    self.rows = rows

        class TemplateRef(object):
            def __init__(self):
                # type: () -> None
                self.entityType = None  # type: str
                self.templateType = None  # type: int
                self.templateName = None  # type: str
                self.templateTitle = None  # type: str
                self.templateDescription = None  # type: Optional[str]

            def __call__(self,
                         entityType,  # type: str
                         templateType,  # type: int
                         templateName,  # type: str
                         templateTitle,  # type: str
                         templateDescription=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.TemplateRef]
                self.set_entity_type(entityType)
                self.set_template_type(templateType)
                self.set_template_name(templateName)
                self.set_template_title(templateTitle)
                self.set_template_description(templateDescription)
                return self

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_template_type(self):
                # type: () -> int
                if not isinstance(self.templateType, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.get_template_type type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.templateType)), -1)
                return self.templateType

            @property
            def get_template_name(self):
                # type: () -> str
                if not isinstance(self.templateName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.get_template_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.templateName)), -1)
                return self.templateName

            @property
            def get_template_title(self):
                # type: () -> str
                if not isinstance(self.templateTitle, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.get_template_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.templateTitle)), -1)
                return self.templateTitle

            @property
            def get_template_description(self):
                # type: () -> Optional[str]
                if self.templateDescription is not None and not isinstance(self.templateDescription, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.get_template_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.templateDescription)), -1)
                return self.templateDescription

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_template_type(self,
                                  template_type  # type: int
                                  ):
                # type: (...) -> None
                if not isinstance(template_type, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.set_template_type type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(template_type)), -1)
                self.templateType = template_type

            def set_template_name(self,
                                  template_name  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(template_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.set_template_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(template_name)), -1)
                self.templateName = template_name

            def set_template_title(self,
                                   template_title  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(template_title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.set_template_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(template_title)), -1)
                self.templateTitle = template_title

            def set_template_description(self,
                                         template_description  # type: Optional[str]
                                         ):
                # type: (...) -> None
                if template_description is not None and not isinstance(template_description, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.TemplateRef.set_template_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(template_description)), -1)
                self.templateDescription = template_description

        class GetConfigSpecRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str

            def __call__(self,
                         entityType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetConfigSpecRequest]
                self.set_entity_type(entityType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

        class GetConfigSpecResponse(object):
            def __init__(self):
                # type: () -> None
                self.createEnabled = None  # type: bool
                self.editEnabled = None  # type: bool
                self.editPossible = None  # type: bool
                self.deleteEnabled = None  # type: bool
                self.configTemplates = list()  # type: Optional[List[Type[RestApi.Admin.TemplateRef]]]
                self.errorTemplates = list()  # type: Optional[List[str]]

            def __call__(self,
                         createEnabled,  # type: bool
                         editEnabled,  # type: bool
                         editPossible,  # type: bool
                         deleteEnabled,  # type: bool
                         configTemplates=None,  # type: Optional[List[Type[RestApi.Admin.TemplateRef]]]
                         errorTemplates=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetConfigSpecResponse]
                self.set_create_enabled(createEnabled)
                self.set_edit_enabled(editEnabled)
                self.set_edit_possible(editPossible)
                self.set_delete_enabled(deleteEnabled)
                if configTemplates is not None:
                    if isinstance(configTemplates, list):
                        for index, item in enumerate(configTemplates):
                            if isinstance(item, dict):
                                configTemplates[index] = RestApi.Admin.TemplateRef()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetConfigSpecResponse.__call__ configTemplates type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(configTemplates)))
                self.set_config_templates(configTemplates)
                self.set_error_templates(errorTemplates)
                return self

            @property
            def get_create_enabled(self):
                # type: () -> bool
                if not isinstance(self.createEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_create_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.createEnabled)), -1)
                return self.createEnabled

            @property
            def get_edit_enabled(self):
                # type: () -> bool
                if not isinstance(self.editEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_edit_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.editEnabled)), -1)
                return self.editEnabled

            @property
            def get_edit_possible(self):
                # type: () -> bool
                if not isinstance(self.editPossible, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_edit_possible type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.editPossible)), -1)
                return self.editPossible

            @property
            def get_delete_enabled(self):
                # type: () -> bool
                if not isinstance(self.deleteEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_delete_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.deleteEnabled)), -1)
                return self.deleteEnabled

            @property
            def get_config_templates(self):
                # type: () -> Optional[List[Type[RestApi.Admin.TemplateRef]]]
                if self.configTemplates is not None and not (isinstance(self.configTemplates, list) and all(
                    isinstance(item, RestApi.Admin.TemplateRef) for item in self.configTemplates)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_config_templates type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.TemplateRef]]]".format(
                            type_name=type(self.configTemplates)), -1)
                if self.configTemplates is None:
                    return list()
                else:
                    return self.configTemplates

            @property
            def get_error_templates(self):
                # type: () -> Optional[List[str]]
                if self.errorTemplates is not None and not (isinstance(self.errorTemplates, list) and all(
                    isinstance(item, basestring) for item in self.errorTemplates)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.get_error_templates type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.errorTemplates)), -1)
                if self.errorTemplates is None:
                    return list()
                else:
                    return self.errorTemplates

            def set_create_enabled(self,
                                   create_enabled  # type: bool
                                   ):
                # type: (...) -> None
                if not isinstance(create_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_create_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(create_enabled)), -1)
                self.createEnabled = create_enabled

            def set_edit_enabled(self,
                                 edit_enabled  # type: bool
                                 ):
                # type: (...) -> None
                if not isinstance(edit_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_edit_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(edit_enabled)), -1)
                self.editEnabled = edit_enabled

            def set_edit_possible(self,
                                  edit_possible  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(edit_possible, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_edit_possible type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(edit_possible)), -1)
                self.editPossible = edit_possible

            def set_delete_enabled(self,
                                   delete_enabled  # type: bool
                                   ):
                # type: (...) -> None
                if not isinstance(delete_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_delete_enabled type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(delete_enabled)), -1)
                self.deleteEnabled = delete_enabled

            def set_config_templates(self,
                                     config_templates  # type: Optional[List[Type[RestApi.Admin.TemplateRef]]]
                                     ):
                # type: (...) -> None
                if config_templates is not None and not (isinstance(config_templates, list) and all(
                    isinstance(item, RestApi.Admin.TemplateRef) for item in config_templates)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_config_templates type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.TemplateRef]]]".format(
                            type_name=type(config_templates)), -1)
                if config_templates is None:
                    self.configTemplates = list()
                else:
                    self.configTemplates = config_templates

            def set_error_templates(self,
                                    error_templates  # type: Optional[List[str]]
                                    ):
                # type: (...) -> None
                if error_templates is not None and not (isinstance(error_templates, list) and all(
                    isinstance(item, basestring) for item in error_templates)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigSpecResponse.set_error_templates type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(error_templates)), -1)
                if error_templates is None:
                    self.errorTemplates = list()
                else:
                    self.errorTemplates = error_templates

        class GetConfigTemplateRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str
                self.templateName = None  # type: Optional[str]

            def __call__(self,
                         entityType,  # type: str
                         templateName=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetConfigTemplateRequest]
                self.set_entity_type(entityType)
                self.set_template_name(templateName)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_template_name(self):
                # type: () -> Optional[str]
                if self.templateName is not None and not isinstance(self.templateName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.get_template_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.templateName)), -1)
                return self.templateName

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_template_name(self,
                                  template_name  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if template_name is not None and not isinstance(template_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateRequest.set_template_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(template_name)), -1)
                self.templateName = template_name

        class GetConfigTemplateResponse(object):
            def __init__(self):
                # type: () -> None
                self.configTemplate = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configTemplate=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetConfigTemplateResponse]
                if configTemplate is not None:
                    if isinstance(configTemplate, dict):
                        configTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**configTemplate)
                self.set_config_template(configTemplate)
                return self

            @property
            def get_config_template(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configTemplate is not None and not isinstance(self.configTemplate,
                                                                      RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateResponse.get_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configTemplate)), -1)
                return self.configTemplate

            def set_config_template(self,
                                    config_template
                                    # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                    ):
                # type: (...) -> None
                if config_template is not None and not isinstance(config_template,
                                                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetConfigTemplateResponse.set_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_template)), -1)
                self.configTemplate = config_template

        class GetElementConfigTemplateRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.bypassCustomTemplate = None  # type: bool
                self.refreshCache = None  # type: bool

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         bypassCustomTemplate,  # type: bool
                         refreshCache,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementConfigTemplateRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                self.set_bypass_custom_template(bypassCustomTemplate)
                self.set_refresh_cache(refreshCache)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_bypass_custom_template(self):
                # type: () -> bool
                if not isinstance(self.bypassCustomTemplate, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.get_bypass_custom_template type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.bypassCustomTemplate)), -1)
                return self.bypassCustomTemplate

            @property
            def get_refresh_cache(self):
                # type: () -> bool
                if not isinstance(self.refreshCache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.get_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.refreshCache)), -1)
                return self.refreshCache

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_bypass_custom_template(self,
                                           bypass_custom_template  # type: bool
                                           ):
                # type: (...) -> None
                if not isinstance(bypass_custom_template, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.set_bypass_custom_template type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(bypass_custom_template)), -1)
                self.bypassCustomTemplate = bypass_custom_template

            def set_refresh_cache(self,
                                  refresh_cache  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(refresh_cache, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateRequest.set_refresh_cache type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(refresh_cache)), -1)
                self.refreshCache = refresh_cache

        class GetElementConfigTemplateResponse(object):
            def __init__(self):
                # type: () -> None
                self.configTemplate = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configTemplate=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementConfigTemplateResponse]
                if configTemplate is not None:
                    if isinstance(configTemplate, dict):
                        configTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**configTemplate)
                self.set_config_template(configTemplate)
                return self

            @property
            def get_config_template(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configTemplate is not None and not isinstance(self.configTemplate,
                                                                      RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateResponse.get_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configTemplate)), -1)
                return self.configTemplate

            def set_config_template(self,
                                    config_template
                                    # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                    ):
                # type: (...) -> None
                if config_template is not None and not isinstance(config_template,
                                                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementConfigTemplateResponse.set_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_template)), -1)
                self.configTemplate = config_template

        class CreateConfigTemplateRowRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str
                self.template = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         entityType,  # type: str
                         template,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.Admin.CreateConfigTemplateRowRequest]
                self.set_entity_type(entityType)
                if isinstance(template, dict):
                    template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**template)
                self.set_template(template)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.get_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.template)), -1)
                return self.template

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_template(self,
                             template  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                             ):
                # type: (...) -> None
                if not isinstance(template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowRequest.set_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(template)), -1)
                self.template = template

        class CreateConfigTemplateRowResponse(object):
            def __init__(self):
                # type: () -> None
                self.elementId = None  # type: str
                self.template = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         elementId,  # type: str
                         template,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.Admin.CreateConfigTemplateRowResponse]
                self.set_element_id(elementId)
                if isinstance(template, dict):
                    template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**template)
                self.set_template(template)
                return self

            @property
            def get_element_id(self):
                # type: () -> str
                if not isinstance(self.elementId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowResponse.get_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementId)), -1)
                return self.elementId

            @property
            def get_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowResponse.get_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.template)), -1)
                return self.template

            def set_element_id(self,
                               element_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(element_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowResponse.set_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_id)), -1)
                self.elementId = element_id

            def set_template(self,
                             template  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                             ):
                # type: (...) -> None
                if not isinstance(template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.CreateConfigTemplateRowResponse.set_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(template)), -1)
                self.template = template

        class UpdateConfigTemplateRowRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str
                self.elementId = None  # type: str
                self.template = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         entityType,  # type: str
                         elementId,  # type: str
                         template,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateConfigTemplateRowRequest]
                self.set_entity_type(entityType)
                self.set_element_id(elementId)
                if isinstance(template, dict):
                    template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**template)
                self.set_template(template)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_element_id(self):
                # type: () -> str
                if not isinstance(self.elementId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.get_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementId)), -1)
                return self.elementId

            @property
            def get_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.get_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.template)), -1)
                return self.template

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_element_id(self,
                               element_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(element_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.set_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_id)), -1)
                self.elementId = element_id

            def set_template(self,
                             template  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                             ):
                # type: (...) -> None
                if not isinstance(template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowRequest.set_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(template)), -1)
                self.template = template

        class UpdateConfigTemplateRowResponse(object):
            def __init__(self):
                # type: () -> None
                self.template = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         template,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.Admin.UpdateConfigTemplateRowResponse]
                if isinstance(template, dict):
                    template = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**template)
                self.set_template(template)
                return self

            @property
            def get_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowResponse.get_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.template)), -1)
                return self.template

            def set_template(self,
                             template  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                             ):
                # type: (...) -> None
                if not isinstance(template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.UpdateConfigTemplateRowResponse.set_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(template)), -1)
                self.template = template

        class DeleteConfigElementRowRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str
                self.elementId = None  # type: str

            def __call__(self,
                         entityType,  # type: str
                         elementId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.DeleteConfigElementRowRequest]
                self.set_entity_type(entityType)
                self.set_element_id(elementId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_element_id(self):
                # type: () -> str
                if not isinstance(self.elementId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.get_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.elementId)), -1)
                return self.elementId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_element_id(self,
                               element_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(element_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowRequest.set_element_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(element_id)), -1)
                self.elementId = element_id

        class DeleteConfigElementRowResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.DeleteConfigElementRowResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeleteConfigElementRowResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class EnrollDeviceRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.uniqueSessionId = None  # type: str
                self.deviceTemplate = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                self.groupName = None  # type: Optional[str]
                self.groupPlugin = None  # type: Optional[str]
                self.activateRule = None  # type: bool

            def __call__(self,
                         uniqueSessionId,  # type: str
                         deviceTemplate,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         activateRule,  # type: bool
                         groupName=None,  # type: Optional[str]
                         groupPlugin=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.EnrollDeviceRequest]
                self.set_unique_session_id(uniqueSessionId)
                if isinstance(deviceTemplate, dict):
                    deviceTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**deviceTemplate)
                self.set_device_template(deviceTemplate)
                self.set_activate_rule(activateRule)
                self.set_group_name(groupName)
                self.set_group_plugin(groupPlugin)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_unique_session_id(self):
                # type: () -> str
                if not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            @property
            def get_device_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.deviceTemplate, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_device_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.deviceTemplate)), -1)
                return self.deviceTemplate

            @property
            def get_group_name(self):
                # type: () -> Optional[str]
                if self.groupName is not None and not isinstance(self.groupName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_group_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.groupName)), -1)
                return self.groupName

            @property
            def get_group_plugin(self):
                # type: () -> Optional[str]
                if self.groupPlugin is not None and not isinstance(self.groupPlugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_group_plugin type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.groupPlugin)), -1)
                return self.groupPlugin

            @property
            def get_activate_rule(self):
                # type: () -> bool
                if not isinstance(self.activateRule, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.get_activate_rule type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.activateRule)), -1)
                return self.activateRule

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_unique_session_id(self,
                                      unique_session_id  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

            def set_device_template(self,
                                    device_template
                                    # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                    ):
                # type: (...) -> None
                if not isinstance(device_template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_device_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(device_template)), -1)
                self.deviceTemplate = device_template

            def set_group_name(self,
                               group_name  # type: Optional[str]
                               ):
                # type: (...) -> None
                if group_name is not None and not isinstance(group_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_group_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(group_name)), -1)
                self.groupName = group_name

            def set_group_plugin(self,
                                 group_plugin  # type: Optional[str]
                                 ):
                # type: (...) -> None
                if group_plugin is not None and not isinstance(group_plugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_group_plugin type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(group_plugin)), -1)
                self.groupPlugin = group_plugin

            def set_activate_rule(self,
                                  activate_rule  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(activate_rule, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceRequest.set_activate_rule type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(activate_rule)), -1)
                self.activateRule = activate_rule

        class EnrollDeviceResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.message = None  # type: str

            def __call__(self,
                         rc,  # type: bool
                         message,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.EnrollDeviceResponse]
                self.set_rc(rc)
                self.set_message(message)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_message(self):
                # type: () -> str
                if not isinstance(self.message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceResponse.get_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.message)), -1)
                return self.message

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_message(self,
                            message  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceResponse.set_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(message)), -1)
                self.message = message

        class EnrollDeviceToUserRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.externalUserId = None  # type: str
                self.userPlugin = None  # type: str
                self.deviceTemplate = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                self.activateRule = None  # type: bool
                self.enrollAsEndpoint = None  # type: bool
                self.uniqueSessionId = None  # type: Optional[str]

            def __call__(self,
                         externalUserId,  # type: str
                         userPlugin,  # type: str
                         deviceTemplate,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         activateRule,  # type: bool
                         enrollAsEndpoint,  # type: bool
                         uniqueSessionId=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.EnrollDeviceToUserRequest]
                self.set_external_user_id(externalUserId)
                self.set_user_plugin(userPlugin)
                if isinstance(deviceTemplate, dict):
                    deviceTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**deviceTemplate)
                self.set_device_template(deviceTemplate)
                self.set_activate_rule(activateRule)
                self.set_enroll_as_endpoint(enrollAsEndpoint)
                self.set_unique_session_id(uniqueSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_external_user_id(self):
                # type: () -> str
                if not isinstance(self.externalUserId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_external_user_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.externalUserId)), -1)
                return self.externalUserId

            @property
            def get_user_plugin(self):
                # type: () -> str
                if not isinstance(self.userPlugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_user_plugin type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.userPlugin)), -1)
                return self.userPlugin

            @property
            def get_device_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.deviceTemplate, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_device_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.deviceTemplate)), -1)
                return self.deviceTemplate

            @property
            def get_activate_rule(self):
                # type: () -> bool
                if not isinstance(self.activateRule, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_activate_rule type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.activateRule)), -1)
                return self.activateRule

            @property
            def get_enroll_as_endpoint(self):
                # type: () -> bool
                if not isinstance(self.enrollAsEndpoint, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_enroll_as_endpoint type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.enrollAsEndpoint)), -1)
                return self.enrollAsEndpoint

            @property
            def get_unique_session_id(self):
                # type: () -> Optional[str]
                if self.uniqueSessionId is not None and not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_external_user_id(self,
                                     external_user_id  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(external_user_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_external_user_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(external_user_id)), -1)
                self.externalUserId = external_user_id

            def set_user_plugin(self,
                                user_plugin  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(user_plugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_user_plugin type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(user_plugin)), -1)
                self.userPlugin = user_plugin

            def set_device_template(self,
                                    device_template
                                    # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                    ):
                # type: (...) -> None
                if not isinstance(device_template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_device_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(device_template)), -1)
                self.deviceTemplate = device_template

            def set_activate_rule(self,
                                  activate_rule  # type: bool
                                  ):
                # type: (...) -> None
                if not isinstance(activate_rule, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_activate_rule type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(activate_rule)), -1)
                self.activateRule = activate_rule

            def set_enroll_as_endpoint(self,
                                       enroll_as_endpoint  # type: bool
                                       ):
                # type: (...) -> None
                if not isinstance(enroll_as_endpoint, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_enroll_as_endpoint type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(enroll_as_endpoint)), -1)
                self.enrollAsEndpoint = enroll_as_endpoint

            def set_unique_session_id(self,
                                      unique_session_id  # type: Optional[str]
                                      ):
                # type: (...) -> None
                if unique_session_id is not None and not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserRequest.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

        class EnrollDeviceToUserResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.message = None  # type: str

            def __call__(self,
                         rc,  # type: bool
                         message,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.EnrollDeviceToUserResponse]
                self.set_rc(rc)
                self.set_message(message)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_message(self):
                # type: () -> str
                if not isinstance(self.message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserResponse.get_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.message)), -1)
                return self.message

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_message(self,
                            message  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollDeviceToUserResponse.set_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(message)), -1)
                self.message = message

        class EnrollmentRequest(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: str
                self.tokenInfo = None  # type: str
                self.userInfo = None  # type: str

            def __call__(self,
                         id_,  # type: str
                         tokenInfo,  # type: str
                         userInfo,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.EnrollmentRequest]
                self.set_id(id_)
                self.set_token_info(tokenInfo)
                self.set_user_info(userInfo)
                return self

            @property
            def get_id(self):
                # type: () -> str
                if not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.get_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_token_info(self):
                # type: () -> str
                if not isinstance(self.tokenInfo, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.get_token_info type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenInfo)), -1)
                return self.tokenInfo

            @property
            def get_user_info(self):
                # type: () -> str
                if not isinstance(self.userInfo, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.get_user_info type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.userInfo)), -1)
                return self.userInfo

            def set_id(self,
                       id_  # type: str
                       ):
                # type: (...) -> None
                if not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.set_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_token_info(self,
                               token_info  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(token_info, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.set_token_info type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_info)), -1)
                self.tokenInfo = token_info

            def set_user_info(self,
                              user_info  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(user_info, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.EnrollmentRequest.set_user_info type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(user_info)), -1)
                self.userInfo = user_info

        class GetEnrollmentRequestsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetEnrollmentRequestsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEnrollmentRequestsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEnrollmentRequestsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetEnrollmentRequestsResponse(object):
            def __init__(self):
                # type: () -> None
                self.enrollmentRequests = list()  # type: Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]

            def __call__(self,
                         enrollmentRequests=None,  # type: Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetEnrollmentRequestsResponse]
                if enrollmentRequests is not None:
                    if isinstance(enrollmentRequests, list):
                        for index, item in enumerate(enrollmentRequests):
                            if isinstance(item, dict):
                                enrollmentRequests[index] = RestApi.Admin.EnrollmentRequest()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetEnrollmentRequestsResponse.__call__ enrollmentRequests type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(enrollmentRequests)))
                self.set_enrollment_requests(enrollmentRequests)
                return self

            @property
            def get_enrollment_requests(self):
                # type: () -> Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]
                if self.enrollmentRequests is not None and not (isinstance(self.enrollmentRequests, list) and all(
                    isinstance(item, RestApi.Admin.EnrollmentRequest) for item in self.enrollmentRequests)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEnrollmentRequestsResponse.get_enrollment_requests type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]".format(
                            type_name=type(self.enrollmentRequests)), -1)
                if self.enrollmentRequests is None:
                    return list()
                else:
                    return self.enrollmentRequests

            def set_enrollment_requests(self,
                                        enrollment_requests
                                        # type: Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]
                                        ):
                # type: (...) -> None
                if enrollment_requests is not None and not (isinstance(enrollment_requests, list) and all(
                    isinstance(item, RestApi.Admin.EnrollmentRequest) for item in enrollment_requests)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEnrollmentRequestsResponse.set_enrollment_requests type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.EnrollmentRequest]]]".format(
                            type_name=type(enrollment_requests)), -1)
                if enrollment_requests is None:
                    self.enrollmentRequests = list()
                else:
                    self.enrollmentRequests = enrollment_requests

        class RespondToEnrollmentRequestRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.enrollmentRequestId = None  # type: str
                self.accept = None  # type: bool

            def __call__(self,
                         enrollmentRequestId,  # type: str
                         accept,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.RespondToEnrollmentRequestRequest]
                self.set_enrollment_request_id(enrollmentRequestId)
                self.set_accept(accept)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_enrollment_request_id(self):
                # type: () -> str
                if not isinstance(self.enrollmentRequestId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.get_enrollment_request_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.enrollmentRequestId)), -1)
                return self.enrollmentRequestId

            @property
            def get_accept(self):
                # type: () -> bool
                if not isinstance(self.accept, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.get_accept type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.accept)), -1)
                return self.accept

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_enrollment_request_id(self,
                                          enrollment_request_id  # type: str
                                          ):
                # type: (...) -> None
                if not isinstance(enrollment_request_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.set_enrollment_request_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(enrollment_request_id)), -1)
                self.enrollmentRequestId = enrollment_request_id

            def set_accept(self,
                           accept  # type: bool
                           ):
                # type: (...) -> None
                if not isinstance(accept, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestRequest.set_accept type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(accept)), -1)
                self.accept = accept

        class RespondToEnrollmentRequestResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.RespondToEnrollmentRequestResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RespondToEnrollmentRequestResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class DeployGetKnownSecretClientRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.DeployGetKnownSecretClientRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetKnownSecretClientRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetKnownSecretClientRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class DeployGetKnownSecretClientResponse(object):
            def __init__(self):
                # type: () -> None
                self.knownsecretClient = None  # type: str

            def __call__(self,
                         knownsecretClient,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.DeployGetKnownSecretClientResponse]
                self.set_knownsecret_client(knownsecretClient)
                return self

            @property
            def get_knownsecret_client(self):
                # type: () -> str
                if not isinstance(self.knownsecretClient, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetKnownSecretClientResponse.get_knownsecret_client type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.knownsecretClient)), -1)
                return self.knownsecretClient

            def set_knownsecret_client(self,
                                       knownsecret_client  # type: str
                                       ):
                # type: (...) -> None
                if not isinstance(knownsecret_client, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetKnownSecretClientResponse.set_knownsecret_client type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(knownsecret_client)), -1)
                self.knownsecretClient = knownsecret_client

        class DeployGetServersRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.DeployGetServersRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetServersRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetServersRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class DeployGetServersResponse(object):
            def __init__(self):
                # type: () -> None
                self.servers = None  # type: str

            def __call__(self,
                         servers,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.DeployGetServersResponse]
                self.set_servers(servers)
                return self

            @property
            def get_servers(self):
                # type: () -> str
                if not isinstance(self.servers, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetServersResponse.get_servers type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.servers)), -1)
                return self.servers

            def set_servers(self,
                            servers  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(servers, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGetServersResponse.set_servers type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(servers)), -1)
                self.servers = servers

        class DeployGenerateKeyPairRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.DeployGenerateKeyPairRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class DeployGenerateKeyPairResponse(object):
            def __init__(self):
                # type: () -> None
                self.publicKey = None  # type: str
                self.privateKey = None  # type: str

            def __call__(self,
                         publicKey,  # type: str
                         privateKey,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.DeployGenerateKeyPairResponse]
                self.set_public_key(publicKey)
                self.set_private_key(privateKey)
                return self

            @property
            def get_public_key(self):
                # type: () -> str
                if not isinstance(self.publicKey, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairResponse.get_public_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.publicKey)), -1)
                return self.publicKey

            @property
            def get_private_key(self):
                # type: () -> str
                if not isinstance(self.privateKey, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairResponse.get_private_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.privateKey)), -1)
                return self.privateKey

            def set_public_key(self,
                               public_key  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(public_key, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairResponse.set_public_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(public_key)), -1)
                self.publicKey = public_key

            def set_private_key(self,
                                private_key  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(private_key, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DeployGenerateKeyPairResponse.set_private_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(private_key)), -1)
                self.privateKey = private_key

        class GetGPMCollectionsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetGPMCollectionsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGPMCollectionsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGPMCollectionsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetGPMCollectionsResponse(object):
            def __init__(self):
                # type: () -> None
                self.gpmCollections = list()  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]

            def __call__(self,
                         gpmCollections=None,  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetGPMCollectionsResponse]
                if gpmCollections is not None:
                    if isinstance(gpmCollections, list):
                        for index, item in enumerate(gpmCollections):
                            if isinstance(item, dict):
                                gpmCollections[index] = RestApi.Admin.Gpm.GPMCollectionInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetGPMCollectionsResponse.__call__ gpmCollections type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gpmCollections)))
                self.set_gpm_collections(gpmCollections)
                return self

            @property
            def get_gpm_collections(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                if self.gpmCollections is not None and not (isinstance(self.gpmCollections, list) and all(
                    isinstance(item, RestApi.Admin.Gpm.GPMCollectionInfo) for item in self.gpmCollections)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGPMCollectionsResponse.get_gpm_collections type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]".format(
                            type_name=type(self.gpmCollections)), -1)
                if self.gpmCollections is None:
                    return list()
                else:
                    return self.gpmCollections

            def set_gpm_collections(self,
                                    gpm_collections
                                    # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                                    ):
                # type: (...) -> None
                if gpm_collections is not None and not (isinstance(gpm_collections, list) and all(
                    isinstance(item, RestApi.Admin.Gpm.GPMCollectionInfo) for item in gpm_collections)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGPMCollectionsResponse.set_gpm_collections type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]".format(
                            type_name=type(gpm_collections)), -1)
                if gpm_collections is None:
                    self.gpmCollections = list()
                else:
                    self.gpmCollections = gpm_collections

        class DownloadGPMStartRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.gpmId = None  # type: str

            def __call__(self,
                         gpmId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMStartRequest]
                self.set_gpm_id(gpmId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_gpm_id(self):
                # type: () -> str
                if not isinstance(self.gpmId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartRequest.get_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmId)), -1)
                return self.gpmId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_gpm_id(self,
                           gpm_id  # type: str
                           ):
                # type: (...) -> None
                if not isinstance(gpm_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartRequest.set_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_id)), -1)
                self.gpmId = gpm_id

        class DownloadGPMStartResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.rcMessage = None  # type: Optional[str]
                self.downloadId = None  # type: Optional[int]
                self.downloadSize = None  # type: Optional[int]
                self.downloadChecksum = None  # type: Optional[str]

            def __call__(self,
                         rc,  # type: bool
                         rcMessage=None,  # type: Optional[str]
                         downloadId=None,  # type: Optional[int]
                         downloadSize=None,  # type: Optional[int]
                         downloadChecksum=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMStartResponse]
                self.set_rc(rc)
                self.set_rc_message(rcMessage)
                self.set_download_id(downloadId)
                self.set_download_size(downloadSize)
                self.set_download_checksum(downloadChecksum)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_rc_message(self):
                # type: () -> Optional[str]
                if self.rcMessage is not None and not isinstance(self.rcMessage, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.get_rc_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.rcMessage)), -1)
                return self.rcMessage

            @property
            def get_download_id(self):
                # type: () -> Optional[int]
                if self.downloadId is not None and not isinstance(self.downloadId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.get_download_id type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.downloadId)), -1)
                return self.downloadId

            @property
            def get_download_size(self):
                # type: () -> Optional[int]
                if self.downloadSize is not None and not isinstance(self.downloadSize, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.get_download_size type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.downloadSize)), -1)
                return self.downloadSize

            @property
            def get_download_checksum(self):
                # type: () -> Optional[str]
                if self.downloadChecksum is not None and not isinstance(self.downloadChecksum, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.get_download_checksum type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.downloadChecksum)), -1)
                return self.downloadChecksum

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_rc_message(self,
                               rc_message  # type: Optional[str]
                               ):
                # type: (...) -> None
                if rc_message is not None and not isinstance(rc_message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.set_rc_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(rc_message)), -1)
                self.rcMessage = rc_message

            def set_download_id(self,
                                download_id  # type: Optional[int]
                                ):
                # type: (...) -> None
                if download_id is not None and not isinstance(download_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.set_download_id type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(download_id)), -1)
                self.downloadId = download_id

            def set_download_size(self,
                                  download_size  # type: Optional[int]
                                  ):
                # type: (...) -> None
                if download_size is not None and not isinstance(download_size, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.set_download_size type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(download_size)), -1)
                self.downloadSize = download_size

            def set_download_checksum(self,
                                      download_checksum  # type: Optional[str]
                                      ):
                # type: (...) -> None
                if download_checksum is not None and not isinstance(download_checksum, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStartResponse.set_download_checksum type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(download_checksum)), -1)
                self.downloadChecksum = download_checksum

        class DownloadGPMGetChunkRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.downloadId = None  # type: int
                self.downloadChunkSize = None  # type: int

            def __call__(self,
                         downloadId,  # type: int
                         downloadChunkSize,  # type: int
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMGetChunkRequest]
                self.set_download_id(downloadId)
                self.set_download_chunk_size(downloadChunkSize)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_download_id(self):
                # type: () -> int
                if not isinstance(self.downloadId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.get_download_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.downloadId)), -1)
                return self.downloadId

            @property
            def get_download_chunk_size(self):
                # type: () -> int
                if not isinstance(self.downloadChunkSize, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.get_download_chunk_size type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.downloadChunkSize)), -1)
                return self.downloadChunkSize

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_download_id(self,
                                download_id  # type: int
                                ):
                # type: (...) -> None
                if not isinstance(download_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.set_download_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(download_id)), -1)
                self.downloadId = download_id

            def set_download_chunk_size(self,
                                        download_chunk_size  # type: int
                                        ):
                # type: (...) -> None
                if not isinstance(download_chunk_size, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkRequest.set_download_chunk_size type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(download_chunk_size)), -1)
                self.downloadChunkSize = download_chunk_size

        class DownloadGPMGetChunkResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.rcMessage = None  # type: Optional[str]
                self.downloadChunk = None  # type: Optional[Type[Base64String]]
                self.more = None  # type: Optional[bool]

            def __call__(self,
                         rc,  # type: bool
                         rcMessage=None,  # type: Optional[str]
                         downloadChunk=None,  # type: Optional[Type[Base64String]]
                         more=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMGetChunkResponse]
                self.set_rc(rc)
                self.set_rc_message(rcMessage)
                if downloadChunk is not None:
                    if isinstance(downloadChunk, dict):
                        downloadChunk = Base64String()(**downloadChunk)
                self.set_download_chunk(downloadChunk)
                self.set_more(more)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_rc_message(self):
                # type: () -> Optional[str]
                if self.rcMessage is not None and not isinstance(self.rcMessage, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.get_rc_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.rcMessage)), -1)
                return self.rcMessage

            @property
            def get_download_chunk(self):
                # type: () -> Optional[Type[Base64String]]
                if self.downloadChunk is not None and not isinstance(self.downloadChunk, Base64String):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.get_download_chunk type {type_name} - erroneous type/subtype. Expected type: Optional[Type[Base64String]]".format(
                            type_name=type(self.downloadChunk)), -1)
                return self.downloadChunk

            @property
            def get_more(self):
                # type: () -> Optional[bool]
                if self.more is not None and not isinstance(self.more, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.get_more type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.more)), -1)
                return self.more

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_rc_message(self,
                               rc_message  # type: Optional[str]
                               ):
                # type: (...) -> None
                if rc_message is not None and not isinstance(rc_message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.set_rc_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(rc_message)), -1)
                self.rcMessage = rc_message

            def set_download_chunk(self,
                                   download_chunk  # type: Optional[Type[Base64String]]
                                   ):
                # type: (...) -> None
                if download_chunk is not None and not isinstance(download_chunk, Base64String):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.set_download_chunk type {type_name} - erroneous type/subtype. Expected type: Optional[Type[Base64String]]".format(
                            type_name=type(download_chunk)), -1)
                self.downloadChunk = download_chunk

            def set_more(self,
                         more  # type: Optional[bool]
                         ):
                # type: (...) -> None
                if more is not None and not isinstance(more, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMGetChunkResponse.set_more type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(more)), -1)
                self.more = more

        class DownloadGPMStopRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.downloadId = None  # type: int

            def __call__(self,
                         downloadId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMStopRequest]
                self.set_download_id(downloadId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_download_id(self):
                # type: () -> int
                if not isinstance(self.downloadId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopRequest.get_download_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.downloadId)), -1)
                return self.downloadId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_download_id(self,
                                download_id  # type: int
                                ):
                # type: (...) -> None
                if not isinstance(download_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopRequest.set_download_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(download_id)), -1)
                self.downloadId = download_id

        class DownloadGPMStopResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.DownloadGPMStopResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.DownloadGPMStopResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GenerateGPMSignatureRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.updateChallenge = None  # type: str
                self.gpmIds = list()  # type: List[str]

            def __call__(self,
                         updateChallenge,  # type: str
                         gpmIds,  # type: List[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GenerateGPMSignatureRequest]
                self.set_update_challenge(updateChallenge)
                self.set_gpm_ids(gpmIds)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_update_challenge(self):
                # type: () -> str
                if not isinstance(self.updateChallenge, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.get_update_challenge type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.updateChallenge)), -1)
                return self.updateChallenge

            @property
            def get_gpm_ids(self):
                # type: () -> List[str]
                if not (isinstance(self.gpmIds, list) and all(isinstance(item, basestring) for item in self.gpmIds)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.get_gpm_ids type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(self.gpmIds)), -1)
                return self.gpmIds

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_update_challenge(self,
                                     update_challenge  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(update_challenge, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.set_update_challenge type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(update_challenge)), -1)
                self.updateChallenge = update_challenge

            def set_gpm_ids(self,
                            gpm_ids  # type: List[str]
                            ):
                # type: (...) -> None
                if not (isinstance(gpm_ids, list) and all(isinstance(item, basestring) for item in gpm_ids)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureRequest.set_gpm_ids type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(gpm_ids)), -1)
                self.gpmIds = gpm_ids

        class GenerateGPMSignatureResponse(object):
            def __init__(self):
                # type: () -> None
                self.gpmSignature = None  # type: str

            def __call__(self,
                         gpmSignature,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GenerateGPMSignatureResponse]
                self.set_gpm_signature(gpmSignature)
                return self

            @property
            def get_gpm_signature(self):
                # type: () -> str
                if not isinstance(self.gpmSignature, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureResponse.get_gpm_signature type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmSignature)), -1)
                return self.gpmSignature

            def set_gpm_signature(self,
                                  gpm_signature  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(gpm_signature, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GenerateGPMSignatureResponse.set_gpm_signature type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_signature)), -1)
                self.gpmSignature = gpm_signature

        class PortScanRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.name = None  # type: Optional[str]
                self.portNumbers = list()  # type: List[str]

            def __call__(self,
                         portNumbers,  # type: List[str]
                         name=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.PortScanRequest]
                self.set_port_numbers(portNumbers)
                self.set_name(name)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_name(self):
                # type: () -> Optional[str]
                if self.name is not None and not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.get_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_port_numbers(self):
                # type: () -> List[str]
                if not (isinstance(self.portNumbers, list) and all(
                    isinstance(item, basestring) for item in self.portNumbers)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.get_port_numbers type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(self.portNumbers)), -1)
                return self.portNumbers

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_name(self,
                         name  # type: Optional[str]
                         ):
                # type: (...) -> None
                if name is not None and not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.set_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_port_numbers(self,
                                 port_numbers  # type: List[str]
                                 ):
                # type: (...) -> None
                if not (isinstance(port_numbers, list) and all(isinstance(item, basestring) for item in port_numbers)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanRequest.set_port_numbers type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(port_numbers)), -1)
                self.portNumbers = port_numbers

        class PortScanResponse(object):
            def __init__(self):
                # type: () -> None
                self.selectionValues = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]

            def __call__(self,
                         selectionValues=None,  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                         ):
                # type: (...) -> Type[RestApi.Admin.PortScanResponse]
                if selectionValues is not None:
                    if isinstance(selectionValues, dict):
                        selectionValues = RestApi.Admin.ConfigTemplate.ValueSelection()(**selectionValues)
                self.set_selection_values(selectionValues)
                return self

            @property
            def get_selection_values(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                if self.selectionValues is not None and not isinstance(self.selectionValues,
                                                                       RestApi.Admin.ConfigTemplate.ValueSelection):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanResponse.get_selection_values type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]".format(
                            type_name=type(self.selectionValues)), -1)
                return self.selectionValues

            def set_selection_values(self,
                                     selection_values
                                     # type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]
                                     ):
                # type: (...) -> None
                if selection_values is not None and not isinstance(selection_values,
                                                                   RestApi.Admin.ConfigTemplate.ValueSelection):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PortScanResponse.set_selection_values type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ValueSelection]]".format(
                            type_name=type(selection_values)), -1)
                self.selectionValues = selection_values

        class GetEntityTypeForPluginTypeRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.pluginName = None  # type: str
                self.elementType = None  # type: Optional[str]

            def __call__(self,
                         pluginName,  # type: str
                         elementType=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetEntityTypeForPluginTypeRequest]
                self.set_plugin_name(pluginName)
                self.set_element_type(elementType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_plugin_name(self):
                # type: () -> str
                if not isinstance(self.pluginName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.get_plugin_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.pluginName)), -1)
                return self.pluginName

            @property
            def get_element_type(self):
                # type: () -> Optional[str]
                if self.elementType is not None and not isinstance(self.elementType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.get_element_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.elementType)), -1)
                return self.elementType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_plugin_name(self,
                                plugin_name  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(plugin_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.set_plugin_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(plugin_name)), -1)
                self.pluginName = plugin_name

            def set_element_type(self,
                                 element_type  # type: Optional[str]
                                 ):
                # type: (...) -> None
                if element_type is not None and not isinstance(element_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeRequest.set_element_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(element_type)), -1)
                self.elementType = element_type

        class GetEntityTypeForPluginTypeResponse(object):
            def __init__(self):
                # type: () -> None
                self.entityType = None  # type: str

            def __call__(self,
                         entityType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetEntityTypeForPluginTypeResponse]
                self.set_entity_type(entityType)
                return self

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeResponse.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetEntityTypeForPluginTypeResponse.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

        class GetBasicEntityTypesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str

            def __call__(self,
                         entityType,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetBasicEntityTypesRequest]
                self.set_entity_type(entityType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

        class GetBasicEntityTypesResponse(object):
            def __init__(self):
                # type: () -> None
                self.entityTypes = list()  # type: List[str]

            def __call__(self,
                         entityTypes,  # type: List[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetBasicEntityTypesResponse]
                self.set_entity_types(entityTypes)
                return self

            @property
            def get_entity_types(self):
                # type: () -> List[str]
                if not (isinstance(self.entityTypes, list) and all(
                    isinstance(item, basestring) for item in self.entityTypes)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesResponse.get_entity_types type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(self.entityTypes)), -1)
                return self.entityTypes

            def set_entity_types(self,
                                 entity_types  # type: List[str]
                                 ):
                # type: (...) -> None
                if not (isinstance(entity_types, list) and all(isinstance(item, basestring) for item in entity_types)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetBasicEntityTypesResponse.set_entity_types type {type_name} - erroneous type/subtype. Expected type: List[str]".format(
                            type_name=type(entity_types)), -1)
                self.entityTypes = entity_types

        class GetTokenInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.tokens = list()  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]

            def __call__(self,
                         tokens=None,  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetTokenInfoRequest]
                if tokens is not None:
                    if isinstance(tokens, list):
                        for index, item in enumerate(tokens):
                            if isinstance(item, dict):
                                tokens[index] = RestApi.AdminDeploy.Token.TokenInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetTokenInfoRequest.__call__ tokens type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(tokens)))
                self.set_tokens(tokens)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_tokens(self):
                # type: () -> Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                if self.tokens is not None and not (isinstance(self.tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in self.tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoRequest.get_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(self.tokens)), -1)
                if self.tokens is None:
                    return list()
                else:
                    return self.tokens

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_tokens(self,
                           tokens  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                           ):
                # type: (...) -> None
                if tokens is not None and not (isinstance(tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoRequest.set_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(tokens)), -1)
                if tokens is None:
                    self.tokens = list()
                else:
                    self.tokens = tokens

        class GetTokenInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.tokens = list()  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]

            def __call__(self,
                         tokens=None,  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetTokenInfoResponse]
                if tokens is not None:
                    if isinstance(tokens, list):
                        for index, item in enumerate(tokens):
                            if isinstance(item, dict):
                                tokens[index] = RestApi.AdminDeploy.Token.TokenInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetTokenInfoResponse.__call__ tokens type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(tokens)))
                self.set_tokens(tokens)
                return self

            @property
            def get_tokens(self):
                # type: () -> Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                if self.tokens is not None and not (isinstance(self.tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in self.tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoResponse.get_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(self.tokens)), -1)
                if self.tokens is None:
                    return list()
                else:
                    return self.tokens

            def set_tokens(self,
                           tokens  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                           ):
                # type: (...) -> None
                if tokens is not None and not (isinstance(tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetTokenInfoResponse.set_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(tokens)), -1)
                if tokens is None:
                    self.tokens = list()
                else:
                    self.tokens = tokens

        class MenuItem(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: str
                self.label = None  # type: str
                self.maxItems = None  # type: Optional[int]
                self.itemType = None  # type: Optional[str]

            def __call__(self,
                         id_,  # type: str
                         label,  # type: str
                         maxItems=None,  # type: Optional[int]
                         itemType=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.MenuItem]
                self.set_id(id_)
                self.set_label(label)
                self.set_max_items(maxItems)
                self.set_item_type(itemType)
                return self

            @property
            def get_id(self):
                # type: () -> str
                if not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.get_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_label(self):
                # type: () -> str
                if not isinstance(self.label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.get_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.label)), -1)
                return self.label

            @property
            def get_max_items(self):
                # type: () -> Optional[int]
                if self.maxItems is not None and not isinstance(self.maxItems, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.get_max_items type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.maxItems)), -1)
                return self.maxItems

            @property
            def get_item_type(self):
                # type: () -> Optional[str]
                if self.itemType is not None and not isinstance(self.itemType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.get_item_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.itemType)), -1)
                return self.itemType

            def set_id(self,
                       id_  # type: str
                       ):
                # type: (...) -> None
                if not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.set_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_label(self,
                          label  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.set_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(label)), -1)
                self.label = label

            def set_max_items(self,
                              max_items  # type: Optional[int]
                              ):
                # type: (...) -> None
                if max_items is not None and not isinstance(max_items, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.set_max_items type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(max_items)), -1)
                self.maxItems = max_items

            def set_item_type(self,
                              item_type  # type: Optional[str]
                              ):
                # type: (...) -> None
                if item_type is not None and not isinstance(item_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItem.set_item_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(item_type)), -1)
                self.itemType = item_type

        class MenuItemRef(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: str
                self.children = list()  # type: Optional[List[str]]

            def __call__(self,
                         id_,  # type: str
                         children=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.MenuItemRef]
                self.set_id(id_)
                self.set_children(children)
                return self

            @property
            def get_id(self):
                # type: () -> str
                if not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItemRef.get_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_children(self):
                # type: () -> Optional[List[str]]
                if self.children is not None and not (
                    isinstance(self.children, list) and all(isinstance(item, basestring) for item in self.children)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItemRef.get_children type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.children)), -1)
                if self.children is None:
                    return list()
                else:
                    return self.children

            def set_id(self,
                       id_  # type: str
                       ):
                # type: (...) -> None
                if not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItemRef.set_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_children(self,
                             children  # type: Optional[List[str]]
                             ):
                # type: (...) -> None
                if children is not None and not (
                    isinstance(children, list) and all(isinstance(item, basestring) for item in children)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MenuItemRef.set_children type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(children)), -1)
                if children is None:
                    self.children = list()
                else:
                    self.children = children

        class Menu(object):
            def __init__(self):
                # type: () -> None
                self.rootId = None  # type: str
                self.menuItems = list()  # type: List[Type[RestApi.Admin.MenuItemRef]]

            def __call__(self,
                         rootId,  # type: str
                         menuItems,  # type: List[Type[RestApi.Admin.MenuItemRef]]
                         ):
                # type: (...) -> Type[RestApi.Admin.Menu]
                self.set_root_id(rootId)
                if isinstance(menuItems, list):
                    for index, item in enumerate(menuItems):
                        if isinstance(item, dict):
                            menuItems[index] = RestApi.Admin.MenuItemRef()(**item)
                else:
                    raise TypeError(
                        "RestApi.Admin.Menu.__call__ menuItems type {type_name} - erroneous type. Expected type: list".format(
                            type_name=type(menuItems)))
                self.set_menu_items(menuItems)
                return self

            @property
            def get_root_id(self):
                # type: () -> str
                if not isinstance(self.rootId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Menu.get_root_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.rootId)), -1)
                return self.rootId

            @property
            def get_menu_items(self):
                # type: () -> List[Type[RestApi.Admin.MenuItemRef]]
                if not (isinstance(self.menuItems, list) and all(
                    isinstance(item, RestApi.Admin.MenuItemRef) for item in self.menuItems)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Menu.get_menu_items type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.MenuItemRef]]".format(
                            type_name=type(self.menuItems)), -1)
                return self.menuItems

            def set_root_id(self,
                            root_id  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(root_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Menu.set_root_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(root_id)), -1)
                self.rootId = root_id

            def set_menu_items(self,
                               menu_items  # type: List[Type[RestApi.Admin.MenuItemRef]]
                               ):
                # type: (...) -> None
                if not (isinstance(menu_items, list) and all(
                    isinstance(item, RestApi.Admin.MenuItemRef) for item in menu_items)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.Menu.set_menu_items type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.MenuItemRef]]".format(
                            type_name=type(menu_items)), -1)
                self.menuItems = menu_items

        class GetMenuItemsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetMenuItemsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuItemsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuItemsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetMenuItemsResponse(object):
            def __init__(self):
                # type: () -> None
                self.items = list()  # type: Optional[List[Type[RestApi.Admin.MenuItem]]]

            def __call__(self,
                         items=None,  # type: Optional[List[Type[RestApi.Admin.MenuItem]]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetMenuItemsResponse]
                if items is not None:
                    if isinstance(items, list):
                        for index, item in enumerate(items):
                            if isinstance(item, dict):
                                items[index] = RestApi.Admin.MenuItem()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetMenuItemsResponse.__call__ items type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(items)))
                self.set_items(items)
                return self

            @property
            def get_items(self):
                # type: () -> Optional[List[Type[RestApi.Admin.MenuItem]]]
                if self.items is not None and not (isinstance(self.items, list) and all(
                    isinstance(item, RestApi.Admin.MenuItem) for item in self.items)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuItemsResponse.get_items type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.MenuItem]]]".format(
                            type_name=type(self.items)), -1)
                if self.items is None:
                    return list()
                else:
                    return self.items

            def set_items(self,
                          items  # type: Optional[List[Type[RestApi.Admin.MenuItem]]]
                          ):
                # type: (...) -> None
                if items is not None and not (
                    isinstance(items, list) and all(isinstance(item, RestApi.Admin.MenuItem) for item in items)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuItemsResponse.set_items type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.MenuItem]]]".format(
                            type_name=type(items)), -1)
                if items is None:
                    self.items = list()
                else:
                    self.items = items

        class GetMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetMenuRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.menu = None  # type: Type[RestApi.Admin.Menu]

            def __call__(self,
                         menu,  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetMenuResponse]
                if isinstance(menu, dict):
                    menu = RestApi.Admin.Menu()(**menu)
                self.set_menu(menu)
                return self

            @property
            def get_menu(self):
                # type: () -> Type[RestApi.Admin.Menu]
                if not isinstance(self.menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuResponse.get_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(self.menu)), -1)
                return self.menu

            def set_menu(self,
                         menu  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> None
                if not isinstance(menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetMenuResponse.set_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(menu)), -1)
                self.menu = menu

        class AddItemToMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.item = None  # type: Type[RestApi.Admin.MenuItem]
                self.menuItem = None  # type: Type[RestApi.Admin.MenuItem]

            def __call__(self,
                         item,  # type: Type[RestApi.Admin.MenuItem]
                         menuItem,  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddItemToMenuRequest]
                if isinstance(item, dict):
                    item = RestApi.Admin.MenuItem()(**item)
                self.set_item(item)
                if isinstance(menuItem, dict):
                    menuItem = RestApi.Admin.MenuItem()(**menuItem)
                self.set_menu_item(menuItem)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.get_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.item)), -1)
                return self.item

            @property
            def get_menu_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.menuItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.get_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.menuItem)), -1)
                return self.menuItem

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_item(self,
                         item  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> None
                if not isinstance(item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.set_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(item)), -1)
                self.item = item

            def set_menu_item(self,
                              menu_item  # type: Type[RestApi.Admin.MenuItem]
                              ):
                # type: (...) -> None
                if not isinstance(menu_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuRequest.set_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(menu_item)), -1)
                self.menuItem = menu_item

        class AddItemToMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.menu = None  # type: Type[RestApi.Admin.Menu]

            def __call__(self,
                         menu,  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddItemToMenuResponse]
                if isinstance(menu, dict):
                    menu = RestApi.Admin.Menu()(**menu)
                self.set_menu(menu)
                return self

            @property
            def get_menu(self):
                # type: () -> Type[RestApi.Admin.Menu]
                if not isinstance(self.menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuResponse.get_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(self.menu)), -1)
                return self.menu

            def set_menu(self,
                         menu  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> None
                if not isinstance(menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddItemToMenuResponse.set_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(menu)), -1)
                self.menu = menu

        class AddElementToMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.element = None  # type: Type[RestApi.Admin.Element]
                self.menuItem = None  # type: Type[RestApi.Admin.MenuItem]

            def __call__(self,
                         element,  # type: Type[RestApi.Admin.Element]
                         menuItem,  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddElementToMenuRequest]
                if isinstance(element, dict):
                    element = RestApi.Admin.Element()(**element)
                self.set_element(element)
                if isinstance(menuItem, dict):
                    menuItem = RestApi.Admin.MenuItem()(**menuItem)
                self.set_menu_item(menuItem)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_element(self):
                # type: () -> Type[RestApi.Admin.Element]
                if not isinstance(self.element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.get_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(self.element)), -1)
                return self.element

            @property
            def get_menu_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.menuItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.get_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.menuItem)), -1)
                return self.menuItem

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_element(self,
                            element  # type: Type[RestApi.Admin.Element]
                            ):
                # type: (...) -> None
                if not isinstance(element, RestApi.Admin.Element):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.set_element type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Element]".format(
                            type_name=type(element)), -1)
                self.element = element

            def set_menu_item(self,
                              menu_item  # type: Type[RestApi.Admin.MenuItem]
                              ):
                # type: (...) -> None
                if not isinstance(menu_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuRequest.set_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(menu_item)), -1)
                self.menuItem = menu_item

        class AddElementToMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.newItem = None  # type: Type[RestApi.Admin.MenuItem]
                self.menu = None  # type: Type[RestApi.Admin.Menu]

            def __call__(self,
                         newItem,  # type: Type[RestApi.Admin.MenuItem]
                         menu,  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> Type[RestApi.Admin.AddElementToMenuResponse]
                if isinstance(newItem, dict):
                    newItem = RestApi.Admin.MenuItem()(**newItem)
                self.set_new_item(newItem)
                if isinstance(menu, dict):
                    menu = RestApi.Admin.Menu()(**menu)
                self.set_menu(menu)
                return self

            @property
            def get_new_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.newItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuResponse.get_new_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.newItem)), -1)
                return self.newItem

            @property
            def get_menu(self):
                # type: () -> Type[RestApi.Admin.Menu]
                if not isinstance(self.menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuResponse.get_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(self.menu)), -1)
                return self.menu

            def set_new_item(self,
                             new_item  # type: Type[RestApi.Admin.MenuItem]
                             ):
                # type: (...) -> None
                if not isinstance(new_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuResponse.set_new_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(new_item)), -1)
                self.newItem = new_item

            def set_menu(self,
                         menu  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> None
                if not isinstance(menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.AddElementToMenuResponse.set_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(menu)), -1)
                self.menu = menu

        class MoveItemToMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.item = None  # type: Type[RestApi.Admin.MenuItem]
                self.fromMenuItem = None  # type: Type[RestApi.Admin.MenuItem]
                self.toMenuItem = None  # type: Type[RestApi.Admin.MenuItem]

            def __call__(self,
                         item,  # type: Type[RestApi.Admin.MenuItem]
                         fromMenuItem,  # type: Type[RestApi.Admin.MenuItem]
                         toMenuItem,  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> Type[RestApi.Admin.MoveItemToMenuRequest]
                if isinstance(item, dict):
                    item = RestApi.Admin.MenuItem()(**item)
                self.set_item(item)
                if isinstance(fromMenuItem, dict):
                    fromMenuItem = RestApi.Admin.MenuItem()(**fromMenuItem)
                self.set_from_menu_item(fromMenuItem)
                if isinstance(toMenuItem, dict):
                    toMenuItem = RestApi.Admin.MenuItem()(**toMenuItem)
                self.set_to_menu_item(toMenuItem)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.get_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.item)), -1)
                return self.item

            @property
            def get_from_menu_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.fromMenuItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.get_from_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.fromMenuItem)), -1)
                return self.fromMenuItem

            @property
            def get_to_menu_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.toMenuItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.get_to_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.toMenuItem)), -1)
                return self.toMenuItem

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_item(self,
                         item  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> None
                if not isinstance(item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.set_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(item)), -1)
                self.item = item

            def set_from_menu_item(self,
                                   from_menu_item  # type: Type[RestApi.Admin.MenuItem]
                                   ):
                # type: (...) -> None
                if not isinstance(from_menu_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.set_from_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(from_menu_item)), -1)
                self.fromMenuItem = from_menu_item

            def set_to_menu_item(self,
                                 to_menu_item  # type: Type[RestApi.Admin.MenuItem]
                                 ):
                # type: (...) -> None
                if not isinstance(to_menu_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuRequest.set_to_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(to_menu_item)), -1)
                self.toMenuItem = to_menu_item

        class MoveItemToMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.menu = None  # type: Type[RestApi.Admin.Menu]

            def __call__(self,
                         menu,  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> Type[RestApi.Admin.MoveItemToMenuResponse]
                if isinstance(menu, dict):
                    menu = RestApi.Admin.Menu()(**menu)
                self.set_menu(menu)
                return self

            @property
            def get_menu(self):
                # type: () -> Type[RestApi.Admin.Menu]
                if not isinstance(self.menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuResponse.get_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(self.menu)), -1)
                return self.menu

            def set_menu(self,
                         menu  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> None
                if not isinstance(menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.MoveItemToMenuResponse.set_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(menu)), -1)
                self.menu = menu

        class RemoveItemFromMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.item = None  # type: Type[RestApi.Admin.MenuItem]
                self.menuItem = None  # type: Type[RestApi.Admin.MenuItem]

            def __call__(self,
                         item,  # type: Type[RestApi.Admin.MenuItem]
                         menuItem,  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> Type[RestApi.Admin.RemoveItemFromMenuRequest]
                if isinstance(item, dict):
                    item = RestApi.Admin.MenuItem()(**item)
                self.set_item(item)
                if isinstance(menuItem, dict):
                    menuItem = RestApi.Admin.MenuItem()(**menuItem)
                self.set_menu_item(menuItem)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.get_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.item)), -1)
                return self.item

            @property
            def get_menu_item(self):
                # type: () -> Type[RestApi.Admin.MenuItem]
                if not isinstance(self.menuItem, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.get_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(self.menuItem)), -1)
                return self.menuItem

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_item(self,
                         item  # type: Type[RestApi.Admin.MenuItem]
                         ):
                # type: (...) -> None
                if not isinstance(item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.set_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(item)), -1)
                self.item = item

            def set_menu_item(self,
                              menu_item  # type: Type[RestApi.Admin.MenuItem]
                              ):
                # type: (...) -> None
                if not isinstance(menu_item, RestApi.Admin.MenuItem):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuRequest.set_menu_item type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.MenuItem]".format(
                            type_name=type(menu_item)), -1)
                self.menuItem = menu_item

        class RemoveItemFromMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.menu = None  # type: Type[RestApi.Admin.Menu]

            def __call__(self,
                         menu,  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> Type[RestApi.Admin.RemoveItemFromMenuResponse]
                if isinstance(menu, dict):
                    menu = RestApi.Admin.Menu()(**menu)
                self.set_menu(menu)
                return self

            @property
            def get_menu(self):
                # type: () -> Type[RestApi.Admin.Menu]
                if not isinstance(self.menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuResponse.get_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(self.menu)), -1)
                return self.menu

            def set_menu(self,
                         menu  # type: Type[RestApi.Admin.Menu]
                         ):
                # type: (...) -> None
                if not isinstance(menu, RestApi.Admin.Menu):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.RemoveItemFromMenuResponse.set_menu type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.Menu]".format(
                            type_name=type(menu)), -1)
                self.menu = menu

        class PingRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: Optional[str]

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.PingRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> Optional[str]
                if self.sessionId is not None and not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PingRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: Optional[str]
                               ):
                # type: (...) -> None
                if session_id is not None and not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PingRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class PingResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.PingResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PingResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.PingResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GOnService(object):
            def __init__(self):
                # type: () -> None
                self.sid = None  # type: Optional[str]
                self.title = None  # type: Optional[str]
                self.ip = None  # type: Optional[str]
                self.port = None  # type: Optional[int]
                self.sessionCount = None  # type: Optional[int]
                self.status = None  # type: Optional[str]

            def __call__(self,
                         sid=None,  # type: Optional[str]
                         title=None,  # type: Optional[str]
                         ip=None,  # type: Optional[str]
                         port=None,  # type: Optional[int]
                         sessionCount=None,  # type: Optional[int]
                         status=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnService]
                self.set_sid(sid)
                self.set_title(title)
                self.set_ip(ip)
                self.set_port(port)
                self.set_session_count(sessionCount)
                self.set_status(status)
                return self

            @property
            def get_sid(self):
                # type: () -> Optional[str]
                if self.sid is not None and not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_sid type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_title(self):
                # type: () -> Optional[str]
                if self.title is not None and not isinstance(self.title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.title)), -1)
                return self.title

            @property
            def get_ip(self):
                # type: () -> Optional[str]
                if self.ip is not None and not isinstance(self.ip, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_ip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.ip)), -1)
                return self.ip

            @property
            def get_port(self):
                # type: () -> Optional[int]
                if self.port is not None and not isinstance(self.port, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_port type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.port)), -1)
                return self.port

            @property
            def get_session_count(self):
                # type: () -> Optional[int]
                if self.sessionCount is not None and not isinstance(self.sessionCount, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_session_count type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(self.sessionCount)), -1)
                return self.sessionCount

            @property
            def get_status(self):
                # type: () -> Optional[str]
                if self.status is not None and not isinstance(self.status, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.get_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.status)), -1)
                return self.status

            def set_sid(self,
                        sid  # type: Optional[str]
                        ):
                # type: (...) -> None
                if sid is not None and not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_sid type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_title(self,
                          title  # type: Optional[str]
                          ):
                # type: (...) -> None
                if title is not None and not isinstance(title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(title)), -1)
                self.title = title

            def set_ip(self,
                       ip  # type: Optional[str]
                       ):
                # type: (...) -> None
                if ip is not None and not isinstance(ip, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_ip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(ip)), -1)
                self.ip = ip

            def set_port(self,
                         port  # type: Optional[int]
                         ):
                # type: (...) -> None
                if port is not None and not isinstance(port, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_port type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(port)), -1)
                self.port = port

            def set_session_count(self,
                                  session_count  # type: Optional[int]
                                  ):
                # type: (...) -> None
                if session_count is not None and not isinstance(session_count, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_session_count type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                            type_name=type(session_count)), -1)
                self.sessionCount = session_count

            def set_status(self,
                           status  # type: Optional[str]
                           ):
                # type: (...) -> None
                if status is not None and not isinstance(status, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnService.set_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(status)), -1)
                self.status = status

        class GOnServiceGetListRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GOnServiceGetListRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GOnServiceGetListResponse(object):
            def __init__(self):
                # type: () -> None
                self.gonServices = list()  # type: Optional[List[Type[RestApi.Admin.GOnService]]]
                self.updateEnabled = None  # type: Optional[bool]

            def __call__(self,
                         gonServices=None,  # type: Optional[List[Type[RestApi.Admin.GOnService]]]
                         updateEnabled=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnServiceGetListResponse]
                if gonServices is not None:
                    if isinstance(gonServices, list):
                        for index, item in enumerate(gonServices):
                            if isinstance(item, dict):
                                gonServices[index] = RestApi.Admin.GOnService()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GOnServiceGetListResponse.__call__ gonServices type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gonServices)))
                self.set_gon_services(gonServices)
                self.set_update_enabled(updateEnabled)
                return self

            @property
            def get_gon_services(self):
                # type: () -> Optional[List[Type[RestApi.Admin.GOnService]]]
                if self.gonServices is not None and not (isinstance(self.gonServices, list) and all(
                    isinstance(item, RestApi.Admin.GOnService) for item in self.gonServices)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListResponse.get_gon_services type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnService]]]".format(
                            type_name=type(self.gonServices)), -1)
                if self.gonServices is None:
                    return list()
                else:
                    return self.gonServices

            @property
            def get_update_enabled(self):
                # type: () -> Optional[bool]
                if self.updateEnabled is not None and not isinstance(self.updateEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListResponse.get_update_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.updateEnabled)), -1)
                return self.updateEnabled

            def set_gon_services(self,
                                 gon_services  # type: Optional[List[Type[RestApi.Admin.GOnService]]]
                                 ):
                # type: (...) -> None
                if gon_services is not None and not (isinstance(gon_services, list) and all(
                    isinstance(item, RestApi.Admin.GOnService) for item in gon_services)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListResponse.set_gon_services type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnService]]]".format(
                            type_name=type(gon_services)), -1)
                if gon_services is None:
                    self.gonServices = list()
                else:
                    self.gonServices = gon_services

            def set_update_enabled(self,
                                   update_enabled  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if update_enabled is not None and not isinstance(update_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceGetListResponse.set_update_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(update_enabled)), -1)
                self.updateEnabled = update_enabled

        class GOnServiceRestartRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.sid = None  # type: str
                self.whenNoSessions = None  # type: bool

            def __call__(self,
                         sid,  # type: str
                         whenNoSessions,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnServiceRestartRequest]
                self.set_sid(sid)
                self.set_when_no_sessions(whenNoSessions)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_sid(self):
                # type: () -> str
                if not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.get_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_when_no_sessions(self):
                # type: () -> bool
                if not isinstance(self.whenNoSessions, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.get_when_no_sessions type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.whenNoSessions)), -1)
                return self.whenNoSessions

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_sid(self,
                        sid  # type: str
                        ):
                # type: (...) -> None
                if not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.set_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_when_no_sessions(self,
                                     when_no_sessions  # type: bool
                                     ):
                # type: (...) -> None
                if not isinstance(when_no_sessions, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartRequest.set_when_no_sessions type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(when_no_sessions)), -1)
                self.whenNoSessions = when_no_sessions

        class GOnServiceRestartResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnServiceRestartResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceRestartResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GOnServiceStopRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.sid = None  # type: str
                self.whenNoSessions = None  # type: bool

            def __call__(self,
                         sid,  # type: str
                         whenNoSessions,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnServiceStopRequest]
                self.set_sid(sid)
                self.set_when_no_sessions(whenNoSessions)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_sid(self):
                # type: () -> str
                if not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.get_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_when_no_sessions(self):
                # type: () -> bool
                if not isinstance(self.whenNoSessions, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.get_when_no_sessions type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.whenNoSessions)), -1)
                return self.whenNoSessions

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_sid(self,
                        sid  # type: str
                        ):
                # type: (...) -> None
                if not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.set_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_when_no_sessions(self,
                                     when_no_sessions  # type: bool
                                     ):
                # type: (...) -> None
                if not isinstance(when_no_sessions, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopRequest.set_when_no_sessions type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(when_no_sessions)), -1)
                self.whenNoSessions = when_no_sessions

        class GOnServiceStopResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnServiceStopResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnServiceStopResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GatewayIsUserOnlineRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.externalUserId = None  # type: str
                self.userPlugin = None  # type: str

            def __call__(self,
                         externalUserId,  # type: str
                         userPlugin,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewayIsUserOnlineRequest]
                self.set_external_user_id(externalUserId)
                self.set_user_plugin(userPlugin)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_external_user_id(self):
                # type: () -> str
                if not isinstance(self.externalUserId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.get_external_user_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.externalUserId)), -1)
                return self.externalUserId

            @property
            def get_user_plugin(self):
                # type: () -> str
                if not isinstance(self.userPlugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.get_user_plugin type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.userPlugin)), -1)
                return self.userPlugin

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_external_user_id(self,
                                     external_user_id  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(external_user_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.set_external_user_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(external_user_id)), -1)
                self.externalUserId = external_user_id

            def set_user_plugin(self,
                                user_plugin  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(user_plugin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineRequest.set_user_plugin type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(user_plugin)), -1)
                self.userPlugin = user_plugin

        class GatewayIsUserOnlineResponse(object):
            def __init__(self):
                # type: () -> None
                self.userIsOnline = None  # type: bool
                self.sid = None  # type: Optional[str]
                self.uniqueSessionId = None  # type: Optional[str]

            def __call__(self,
                         userIsOnline,  # type: bool
                         sid=None,  # type: Optional[str]
                         uniqueSessionId=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewayIsUserOnlineResponse]
                self.set_user_is_online(userIsOnline)
                self.set_sid(sid)
                self.set_unique_session_id(uniqueSessionId)
                return self

            @property
            def get_user_is_online(self):
                # type: () -> bool
                if not isinstance(self.userIsOnline, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.get_user_is_online type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.userIsOnline)), -1)
                return self.userIsOnline

            @property
            def get_sid(self):
                # type: () -> Optional[str]
                if self.sid is not None and not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.get_sid type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_unique_session_id(self):
                # type: () -> Optional[str]
                if self.uniqueSessionId is not None and not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            def set_user_is_online(self,
                                   user_is_online  # type: bool
                                   ):
                # type: (...) -> None
                if not isinstance(user_is_online, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.set_user_is_online type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(user_is_online)), -1)
                self.userIsOnline = user_is_online

            def set_sid(self,
                        sid  # type: Optional[str]
                        ):
                # type: (...) -> None
                if sid is not None and not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.set_sid type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_unique_session_id(self,
                                      unique_session_id  # type: Optional[str]
                                      ):
                # type: (...) -> None
                if unique_session_id is not None and not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewayIsUserOnlineResponse.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

        class GOnSession(object):
            def __init__(self):
                # type: () -> None
                self.uniqueSessionId = None  # type: str
                self.userId = None  # type: Optional[str]
                self.userLogin = None  # type: Optional[str]
                self.userName = None  # type: Optional[str]
                self.sessionStart = None  # type: Optional[str]
                self.serverTitle = None  # type: Optional[str]
                self.serverSid = None  # type: str
                self.clientIp = None  # type: Optional[str]

            def __call__(self,
                         uniqueSessionId,  # type: str
                         serverSid,  # type: str
                         userId=None,  # type: Optional[str]
                         userLogin=None,  # type: Optional[str]
                         userName=None,  # type: Optional[str]
                         sessionStart=None,  # type: Optional[str]
                         serverTitle=None,  # type: Optional[str]
                         clientIp=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GOnSession]
                self.set_unique_session_id(uniqueSessionId)
                self.set_server_sid(serverSid)
                self.set_user_id(userId)
                self.set_user_login(userLogin)
                self.set_user_name(userName)
                self.set_session_start(sessionStart)
                self.set_server_title(serverTitle)
                self.set_client_ip(clientIp)
                return self

            @property
            def get_unique_session_id(self):
                # type: () -> str
                if not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            @property
            def get_user_id(self):
                # type: () -> Optional[str]
                if self.userId is not None and not isinstance(self.userId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_user_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.userId)), -1)
                return self.userId

            @property
            def get_user_login(self):
                # type: () -> Optional[str]
                if self.userLogin is not None and not isinstance(self.userLogin, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_user_login type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.userLogin)), -1)
                return self.userLogin

            @property
            def get_user_name(self):
                # type: () -> Optional[str]
                if self.userName is not None and not isinstance(self.userName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_user_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.userName)), -1)
                return self.userName

            @property
            def get_session_start(self):
                # type: () -> Optional[str]
                if self.sessionStart is not None and not isinstance(self.sessionStart, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_session_start type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sessionStart)), -1)
                return self.sessionStart

            @property
            def get_server_title(self):
                # type: () -> Optional[str]
                if self.serverTitle is not None and not isinstance(self.serverTitle, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_server_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.serverTitle)), -1)
                return self.serverTitle

            @property
            def get_server_sid(self):
                # type: () -> str
                if not isinstance(self.serverSid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_server_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.serverSid)), -1)
                return self.serverSid

            @property
            def get_client_ip(self):
                # type: () -> Optional[str]
                if self.clientIp is not None and not isinstance(self.clientIp, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.get_client_ip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.clientIp)), -1)
                return self.clientIp

            def set_unique_session_id(self,
                                      unique_session_id  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

            def set_user_id(self,
                            user_id  # type: Optional[str]
                            ):
                # type: (...) -> None
                if user_id is not None and not isinstance(user_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_user_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(user_id)), -1)
                self.userId = user_id

            def set_user_login(self,
                               user_login  # type: Optional[str]
                               ):
                # type: (...) -> None
                if user_login is not None and not isinstance(user_login, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_user_login type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(user_login)), -1)
                self.userLogin = user_login

            def set_user_name(self,
                              user_name  # type: Optional[str]
                              ):
                # type: (...) -> None
                if user_name is not None and not isinstance(user_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_user_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(user_name)), -1)
                self.userName = user_name

            def set_session_start(self,
                                  session_start  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if session_start is not None and not isinstance(session_start, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_session_start type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(session_start)), -1)
                self.sessionStart = session_start

            def set_server_title(self,
                                 server_title  # type: Optional[str]
                                 ):
                # type: (...) -> None
                if server_title is not None and not isinstance(server_title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_server_title type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(server_title)), -1)
                self.serverTitle = server_title

            def set_server_sid(self,
                               server_sid  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(server_sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_server_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(server_sid)), -1)
                self.serverSid = server_sid

            def set_client_ip(self,
                              client_ip  # type: Optional[str]
                              ):
                # type: (...) -> None
                if client_ip is not None and not isinstance(client_ip, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GOnSession.set_client_ip type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(client_ip)), -1)
                self.clientIp = client_ip

        class GetGatewaySessionsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.searchFilter = None  # type: Optional[str]
                self.serverSid = list()  # type: Optional[List[str]]

            def __call__(self,
                         searchFilter=None,  # type: Optional[str]
                         serverSid=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetGatewaySessionsRequest]
                self.set_search_filter(searchFilter)
                self.set_server_sid(serverSid)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_search_filter(self):
                # type: () -> Optional[str]
                if self.searchFilter is not None and not isinstance(self.searchFilter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.get_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.searchFilter)), -1)
                return self.searchFilter

            @property
            def get_server_sid(self):
                # type: () -> Optional[List[str]]
                if self.serverSid is not None and not (
                    isinstance(self.serverSid, list) and all(isinstance(item, basestring) for item in self.serverSid)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.get_server_sid type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.serverSid)), -1)
                if self.serverSid is None:
                    return list()
                else:
                    return self.serverSid

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_search_filter(self,
                                  search_filter  # type: Optional[str]
                                  ):
                # type: (...) -> None
                if search_filter is not None and not isinstance(search_filter, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.set_search_filter type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(search_filter)), -1)
                self.searchFilter = search_filter

            def set_server_sid(self,
                               server_sid  # type: Optional[List[str]]
                               ):
                # type: (...) -> None
                if server_sid is not None and not (
                    isinstance(server_sid, list) and all(isinstance(item, basestring) for item in server_sid)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsRequest.set_server_sid type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(server_sid)), -1)
                if server_sid is None:
                    self.serverSid = list()
                else:
                    self.serverSid = server_sid

        class GetGatewaySessionsResponse(object):
            def __init__(self):
                # type: () -> None
                self.gonSessions = list()  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                self.nextSessionId = None  # type: Optional[str]
                self.updateEnabled = None  # type: Optional[bool]

            def __call__(self,
                         gonSessions=None,  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                         nextSessionId=None,  # type: Optional[str]
                         updateEnabled=None,  # type: Optional[bool]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetGatewaySessionsResponse]
                if gonSessions is not None:
                    if isinstance(gonSessions, list):
                        for index, item in enumerate(gonSessions):
                            if isinstance(item, dict):
                                gonSessions[index] = RestApi.Admin.GOnSession()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetGatewaySessionsResponse.__call__ gonSessions type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gonSessions)))
                self.set_gon_sessions(gonSessions)
                self.set_next_session_id(nextSessionId)
                self.set_update_enabled(updateEnabled)
                return self

            @property
            def get_gon_sessions(self):
                # type: () -> Optional[List[Type[RestApi.Admin.GOnSession]]]
                if self.gonSessions is not None and not (isinstance(self.gonSessions, list) and all(
                    isinstance(item, RestApi.Admin.GOnSession) for item in self.gonSessions)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.get_gon_sessions type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnSession]]]".format(
                            type_name=type(self.gonSessions)), -1)
                if self.gonSessions is None:
                    return list()
                else:
                    return self.gonSessions

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            @property
            def get_update_enabled(self):
                # type: () -> Optional[bool]
                if self.updateEnabled is not None and not isinstance(self.updateEnabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.get_update_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(self.updateEnabled)), -1)
                return self.updateEnabled

            def set_gon_sessions(self,
                                 gon_sessions  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                                 ):
                # type: (...) -> None
                if gon_sessions is not None and not (isinstance(gon_sessions, list) and all(
                    isinstance(item, RestApi.Admin.GOnSession) for item in gon_sessions)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.set_gon_sessions type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnSession]]]".format(
                            type_name=type(gon_sessions)), -1)
                if gon_sessions is None:
                    self.gonSessions = list()
                else:
                    self.gonSessions = gon_sessions

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

            def set_update_enabled(self,
                                   update_enabled  # type: Optional[bool]
                                   ):
                # type: (...) -> None
                if update_enabled is not None and not isinstance(update_enabled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsResponse.set_update_enabled type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                            type_name=type(update_enabled)), -1)
                self.updateEnabled = update_enabled

        class GetGatewaySessionsNextRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.nextSessionId = None  # type: str

            def __call__(self,
                         nextSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetGatewaySessionsNextRequest]
                self.set_next_session_id(nextSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_next_session_id(self):
                # type: () -> str
                if not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextRequest.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_next_session_id(self,
                                    next_session_id  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextRequest.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

        class GetGatewaySessionsNextResponse(object):
            def __init__(self):
                # type: () -> None
                self.gonSessions = list()  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                self.nextSessionId = None  # type: Optional[str]

            def __call__(self,
                         gonSessions=None,  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                         nextSessionId=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetGatewaySessionsNextResponse]
                if gonSessions is not None:
                    if isinstance(gonSessions, list):
                        for index, item in enumerate(gonSessions):
                            if isinstance(item, dict):
                                gonSessions[index] = RestApi.Admin.GOnSession()(**item)
                    else:
                        raise TypeError(
                            "RestApi.Admin.GetGatewaySessionsNextResponse.__call__ gonSessions type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gonSessions)))
                self.set_gon_sessions(gonSessions)
                self.set_next_session_id(nextSessionId)
                return self

            @property
            def get_gon_sessions(self):
                # type: () -> Optional[List[Type[RestApi.Admin.GOnSession]]]
                if self.gonSessions is not None and not (isinstance(self.gonSessions, list) and all(
                    isinstance(item, RestApi.Admin.GOnSession) for item in self.gonSessions)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextResponse.get_gon_sessions type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnSession]]]".format(
                            type_name=type(self.gonSessions)), -1)
                if self.gonSessions is None:
                    return list()
                else:
                    return self.gonSessions

            @property
            def get_next_session_id(self):
                # type: () -> Optional[str]
                if self.nextSessionId is not None and not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextResponse.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            def set_gon_sessions(self,
                                 gon_sessions  # type: Optional[List[Type[RestApi.Admin.GOnSession]]]
                                 ):
                # type: (...) -> None
                if gon_sessions is not None and not (isinstance(gon_sessions, list) and all(
                    isinstance(item, RestApi.Admin.GOnSession) for item in gon_sessions)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextResponse.set_gon_sessions type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.GOnSession]]]".format(
                            type_name=type(gon_sessions)), -1)
                if gon_sessions is None:
                    self.gonSessions = list()
                else:
                    self.gonSessions = gon_sessions

            def set_next_session_id(self,
                                    next_session_id  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if next_session_id is not None and not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetGatewaySessionsNextResponse.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

        class GatewaySessionCloseRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.sid = None  # type: str
                self.uniqueSessionId = None  # type: str

            def __call__(self,
                         sid,  # type: str
                         uniqueSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewaySessionCloseRequest]
                self.set_sid(sid)
                self.set_unique_session_id(uniqueSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_sid(self):
                # type: () -> str
                if not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.get_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_unique_session_id(self):
                # type: () -> str
                if not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_sid(self,
                        sid  # type: str
                        ):
                # type: (...) -> None
                if not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.set_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_unique_session_id(self,
                                      unique_session_id  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseRequest.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

        class GatewaySessionCloseResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewaySessionCloseResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionCloseResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GatewaySessionRecalculateMenuRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.sid = None  # type: str
                self.uniqueSessionId = None  # type: str

            def __call__(self,
                         sid,  # type: str
                         uniqueSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewaySessionRecalculateMenuRequest]
                self.set_sid(sid)
                self.set_unique_session_id(uniqueSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_sid(self):
                # type: () -> str
                if not isinstance(self.sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.get_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sid)), -1)
                return self.sid

            @property
            def get_unique_session_id(self):
                # type: () -> str
                if not isinstance(self.uniqueSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.get_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.uniqueSessionId)), -1)
                return self.uniqueSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_sid(self,
                        sid  # type: str
                        ):
                # type: (...) -> None
                if not isinstance(sid, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.set_sid type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(sid)), -1)
                self.sid = sid

            def set_unique_session_id(self,
                                      unique_session_id  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(unique_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuRequest.set_unique_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(unique_session_id)), -1)
                self.uniqueSessionId = unique_session_id

        class GatewaySessionRecalculateMenuResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GatewaySessionRecalculateMenuResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GatewaySessionRecalculateMenuResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetElementsStopRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.nextSessionId = None  # type: str

            def __call__(self,
                         nextSessionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementsStopRequest]
                self.set_next_session_id(nextSessionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_next_session_id(self):
                # type: () -> str
                if not isinstance(self.nextSessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopRequest.get_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.nextSessionId)), -1)
                return self.nextSessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_next_session_id(self,
                                    next_session_id  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(next_session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopRequest.set_next_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(next_session_id)), -1)
                self.nextSessionId = next_session_id

        class GetElementsStopResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.GetElementsStopResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetElementsStopResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetLicenseInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.Admin.GetLicenseInfoRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetLicenseInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetLicenseInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class LicenseCount(object):
            def __init__(self):
                # type: () -> None
                self.itemName = None  # type: str
                self.licensedItems = None  # type: int
                self.actualItems = None  # type: int

            def __call__(self,
                         itemName,  # type: str
                         licensedItems,  # type: int
                         actualItems,  # type: int
                         ):
                # type: (...) -> Type[RestApi.Admin.LicenseCount]
                self.set_item_name(itemName)
                self.set_licensed_items(licensedItems)
                self.set_actual_items(actualItems)
                return self

            @property
            def get_item_name(self):
                # type: () -> str
                if not isinstance(self.itemName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.get_item_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.itemName)), -1)
                return self.itemName

            @property
            def get_licensed_items(self):
                # type: () -> int
                if not isinstance(self.licensedItems, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.get_licensed_items type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.licensedItems)), -1)
                return self.licensedItems

            @property
            def get_actual_items(self):
                # type: () -> int
                if not isinstance(self.actualItems, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.get_actual_items type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.actualItems)), -1)
                return self.actualItems

            def set_item_name(self,
                              item_name  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(item_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.set_item_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(item_name)), -1)
                self.itemName = item_name

            def set_licensed_items(self,
                                   licensed_items  # type: int
                                   ):
                # type: (...) -> None
                if not isinstance(licensed_items, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.set_licensed_items type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(licensed_items)), -1)
                self.licensedItems = licensed_items

            def set_actual_items(self,
                                 actual_items  # type: int
                                 ):
                # type: (...) -> None
                if not isinstance(actual_items, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseCount.set_actual_items type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(actual_items)), -1)
                self.actualItems = actual_items

        class LicenseInfoTypeManagement(object):
            def __init__(self):
                # type: () -> None
                self.licensedTo = None  # type: str
                self.licenseNumber = None  # type: str
                self.licenseFileNumber = None  # type: str
                self.licenseExpires = None  # type: Optional[str]
                self.maintenanceExpires = None  # type: Optional[str]
                self.licenseStatusOk = None  # type: bool
                self.updateLicenseAllowed = None  # type: bool
                self.countTypeLicense = list()  # type: List[Type[RestApi.Admin.LicenseCount]]
                self.licenseFileContent = None  # type: Optional[str]

            def __call__(self,
                         licensedTo,  # type: str
                         licenseNumber,  # type: str
                         licenseFileNumber,  # type: str
                         licenseStatusOk,  # type: bool
                         updateLicenseAllowed,  # type: bool
                         countTypeLicense,  # type: List[Type[RestApi.Admin.LicenseCount]]
                         licenseExpires=None,  # type: Optional[str]
                         maintenanceExpires=None,  # type: Optional[str]
                         licenseFileContent=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.Admin.LicenseInfoTypeManagement]
                self.set_licensed_to(licensedTo)
                self.set_license_number(licenseNumber)
                self.set_license_file_number(licenseFileNumber)
                self.set_license_status_ok(licenseStatusOk)
                self.set_update_license_allowed(updateLicenseAllowed)
                if isinstance(countTypeLicense, list):
                    for index, item in enumerate(countTypeLicense):
                        if isinstance(item, dict):
                            countTypeLicense[index] = RestApi.Admin.LicenseCount()(**item)
                else:
                    raise TypeError(
                        "RestApi.Admin.LicenseInfoTypeManagement.__call__ countTypeLicense type {type_name} - erroneous type. Expected type: list".format(
                            type_name=type(countTypeLicense)))
                self.set_count_type_license(countTypeLicense)
                self.set_license_expires(licenseExpires)
                self.set_maintenance_expires(maintenanceExpires)
                self.set_license_file_content(licenseFileContent)
                return self

            @property
            def get_licensed_to(self):
                # type: () -> str
                if not isinstance(self.licensedTo, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_licensed_to type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licensedTo)), -1)
                return self.licensedTo

            @property
            def get_license_number(self):
                # type: () -> str
                if not isinstance(self.licenseNumber, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_license_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licenseNumber)), -1)
                return self.licenseNumber

            @property
            def get_license_file_number(self):
                # type: () -> str
                if not isinstance(self.licenseFileNumber, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_license_file_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licenseFileNumber)), -1)
                return self.licenseFileNumber

            @property
            def get_license_expires(self):
                # type: () -> Optional[str]
                if self.licenseExpires is not None and not isinstance(self.licenseExpires, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_license_expires type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.licenseExpires)), -1)
                return self.licenseExpires

            @property
            def get_maintenance_expires(self):
                # type: () -> Optional[str]
                if self.maintenanceExpires is not None and not isinstance(self.maintenanceExpires, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_maintenance_expires type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.maintenanceExpires)), -1)
                return self.maintenanceExpires

            @property
            def get_license_status_ok(self):
                # type: () -> bool
                if not isinstance(self.licenseStatusOk, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_license_status_ok type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.licenseStatusOk)), -1)
                return self.licenseStatusOk

            @property
            def get_update_license_allowed(self):
                # type: () -> bool
                if not isinstance(self.updateLicenseAllowed, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_update_license_allowed type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.updateLicenseAllowed)), -1)
                return self.updateLicenseAllowed

            @property
            def get_count_type_license(self):
                # type: () -> List[Type[RestApi.Admin.LicenseCount]]
                if not (isinstance(self.countTypeLicense, list) and all(
                    isinstance(item, RestApi.Admin.LicenseCount) for item in self.countTypeLicense)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_count_type_license type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.LicenseCount]]".format(
                            type_name=type(self.countTypeLicense)), -1)
                return self.countTypeLicense

            @property
            def get_license_file_content(self):
                # type: () -> Optional[str]
                if self.licenseFileContent is not None and not isinstance(self.licenseFileContent, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.get_license_file_content type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.licenseFileContent)), -1)
                return self.licenseFileContent

            def set_licensed_to(self,
                                licensed_to  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(licensed_to, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_licensed_to type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(licensed_to)), -1)
                self.licensedTo = licensed_to

            def set_license_number(self,
                                   license_number  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(license_number, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_license_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license_number)), -1)
                self.licenseNumber = license_number

            def set_license_file_number(self,
                                        license_file_number  # type: str
                                        ):
                # type: (...) -> None
                if not isinstance(license_file_number, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_license_file_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license_file_number)), -1)
                self.licenseFileNumber = license_file_number

            def set_license_expires(self,
                                    license_expires  # type: Optional[str]
                                    ):
                # type: (...) -> None
                if license_expires is not None and not isinstance(license_expires, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_license_expires type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(license_expires)), -1)
                self.licenseExpires = license_expires

            def set_maintenance_expires(self,
                                        maintenance_expires  # type: Optional[str]
                                        ):
                # type: (...) -> None
                if maintenance_expires is not None and not isinstance(maintenance_expires, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_maintenance_expires type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(maintenance_expires)), -1)
                self.maintenanceExpires = maintenance_expires

            def set_license_status_ok(self,
                                      license_status_ok  # type: bool
                                      ):
                # type: (...) -> None
                if not isinstance(license_status_ok, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_license_status_ok type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(license_status_ok)), -1)
                self.licenseStatusOk = license_status_ok

            def set_update_license_allowed(self,
                                           update_license_allowed  # type: bool
                                           ):
                # type: (...) -> None
                if not isinstance(update_license_allowed, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_update_license_allowed type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(update_license_allowed)), -1)
                self.updateLicenseAllowed = update_license_allowed

            def set_count_type_license(self,
                                       count_type_license  # type: List[Type[RestApi.Admin.LicenseCount]]
                                       ):
                # type: (...) -> None
                if not (isinstance(count_type_license, list) and all(
                    isinstance(item, RestApi.Admin.LicenseCount) for item in count_type_license)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_count_type_license type {type_name} - erroneous type/subtype. Expected type: List[Type[RestApi.Admin.LicenseCount]]".format(
                            type_name=type(count_type_license)), -1)
                self.countTypeLicense = count_type_license

            def set_license_file_content(self,
                                         license_file_content  # type: Optional[str]
                                         ):
                # type: (...) -> None
                if license_file_content is not None and not isinstance(license_file_content, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.LicenseInfoTypeManagement.set_license_file_content type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(license_file_content)), -1)
                self.licenseFileContent = license_file_content

        class GetLicenseInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.licenseInfo = None  # type: Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]

            def __call__(self,
                         licenseInfo=None,  # type: Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]
                         ):
                # type: (...) -> Type[RestApi.Admin.GetLicenseInfoResponse]
                if licenseInfo is not None:
                    if isinstance(licenseInfo, dict):
                        licenseInfo = RestApi.Admin.LicenseInfoTypeManagement()(**licenseInfo)
                self.set_license_info(licenseInfo)
                return self

            @property
            def get_license_info(self):
                # type: () -> Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]
                if self.licenseInfo is not None and not isinstance(self.licenseInfo,
                                                                   RestApi.Admin.LicenseInfoTypeManagement):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetLicenseInfoResponse.get_license_info type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]".format(
                            type_name=type(self.licenseInfo)), -1)
                return self.licenseInfo

            def set_license_info(self,
                                 license_info  # type: Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]
                                 ):
                # type: (...) -> None
                if license_info is not None and not isinstance(license_info,
                                                               RestApi.Admin.LicenseInfoTypeManagement):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.GetLicenseInfoResponse.set_license_info type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.LicenseInfoTypeManagement]]".format(
                            type_name=type(license_info)), -1)
                self.licenseInfo = license_info

        class SetLicenseRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.license = None  # type: str

            def __call__(self,
                         license,  # type: str
                         ):
                # type: (...) -> Type[RestApi.Admin.SetLicenseRequest]
                self.set_license(license)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_license(self):
                # type: () -> str
                if not isinstance(self.license, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseRequest.get_license type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.license)), -1)
                return self.license

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_license(self,
                            license  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(license, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseRequest.set_license type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license)), -1)
                self.license = license

        class SetLicenseResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.Admin.SetLicenseResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.Admin.SetLicenseResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

    class AdminDeploy(object):
        def __init__(self):  # type: () -> None
            pass

        class LoginRequest(object):
            def __init__(self):
                # type: () -> None
                self.username = None  # type: str
                self.password = None  # type: str

            def __call__(self,
                         username,  # type: str
                         password,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.LoginRequest]
                self.set_username(username)
                self.set_password(password)
                return self

            @property
            def get_username(self):
                # type: () -> str
                if not isinstance(self.username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginRequest.get_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.username)), -1)
                return self.username

            @property
            def get_password(self):
                # type: () -> str
                if not isinstance(self.password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginRequest.get_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.password)), -1)
                return self.password

            def set_username(self,
                             username  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginRequest.set_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(username)), -1)
                self.username = username

            def set_password(self,
                             password  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginRequest.set_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(password)), -1)
                self.password = password

        class LoginResponse(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.LoginResponse]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginResponse.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LoginResponse.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class LogoutRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.LogoutRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LogoutRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LogoutRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class LogoutResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.LogoutResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LogoutResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.LogoutResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetTokenTypesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.GetTokenTypesRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokenTypesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokenTypesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class TokenType(object):
            def __init__(self):
                # type: () -> None
                self.tokenTypeId = None  # type: str
                self.tokenTypeLabel = None  # type: str

            def __call__(self,
                         tokenTypeId,  # type: str
                         tokenTypeLabel,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.TokenType]
                self.set_token_type_id(tokenTypeId)
                self.set_token_type_label(tokenTypeLabel)
                return self

            @property
            def get_token_type_id(self):
                # type: () -> str
                if not isinstance(self.tokenTypeId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.TokenType.get_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenTypeId)), -1)
                return self.tokenTypeId

            @property
            def get_token_type_label(self):
                # type: () -> str
                if not isinstance(self.tokenTypeLabel, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.TokenType.get_token_type_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenTypeLabel)), -1)
                return self.tokenTypeLabel

            def set_token_type_id(self,
                                  token_type_id  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(token_type_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.TokenType.set_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_type_id)), -1)
                self.tokenTypeId = token_type_id

            def set_token_type_label(self,
                                     token_type_label  # type: str
                                     ):
                # type: (...) -> None
                if not isinstance(token_type_label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.TokenType.set_token_type_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_type_label)), -1)
                self.tokenTypeLabel = token_type_label

        class GetTokenTypesResponse(object):
            def __init__(self):
                # type: () -> None
                self.tokentypes = list()  # type: Optional[List[Type[RestApi.AdminDeploy.TokenType]]]

            def __call__(self,
                         tokentypes=None,  # type: Optional[List[Type[RestApi.AdminDeploy.TokenType]]]
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetTokenTypesResponse]
                if tokentypes is not None:
                    if isinstance(tokentypes, list):
                        for index, item in enumerate(tokentypes):
                            if isinstance(item, dict):
                                tokentypes[index] = RestApi.AdminDeploy.TokenType()(**item)
                    else:
                        raise TypeError(
                            "RestApi.AdminDeploy.GetTokenTypesResponse.__call__ tokentypes type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(tokentypes)))
                self.set_tokentypes(tokentypes)
                return self

            @property
            def get_tokentypes(self):
                # type: () -> Optional[List[Type[RestApi.AdminDeploy.TokenType]]]
                if self.tokentypes is not None and not (isinstance(self.tokentypes, list) and all(
                    isinstance(item, RestApi.AdminDeploy.TokenType) for item in self.tokentypes)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokenTypesResponse.get_tokentypes type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.TokenType]]]".format(
                            type_name=type(self.tokentypes)), -1)
                if self.tokentypes is None:
                    return list()
                else:
                    return self.tokentypes

            def set_tokentypes(self,
                               tokentypes  # type: Optional[List[Type[RestApi.AdminDeploy.TokenType]]]
                               ):
                # type: (...) -> None
                if tokentypes is not None and not (isinstance(tokentypes, list) and all(
                    isinstance(item, RestApi.AdminDeploy.TokenType) for item in tokentypes)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokenTypesResponse.set_tokentypes type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.TokenType]]]".format(
                            type_name=type(tokentypes)), -1)
                if tokentypes is None:
                    self.tokentypes = list()
                else:
                    self.tokentypes = tokentypes

        class GetTokensRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.GetTokensRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokensRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokensRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetTokensResponse(object):
            def __init__(self):
                # type: () -> None
                self.tokens = list()  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]

            def __call__(self,
                         tokens=None,  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetTokensResponse]
                if tokens is not None:
                    if isinstance(tokens, list):
                        for index, item in enumerate(tokens):
                            if isinstance(item, dict):
                                tokens[index] = RestApi.AdminDeploy.Token.TokenInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.AdminDeploy.GetTokensResponse.__call__ tokens type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(tokens)))
                self.set_tokens(tokens)
                return self

            @property
            def get_tokens(self):
                # type: () -> Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                if self.tokens is not None and not (isinstance(self.tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in self.tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokensResponse.get_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(self.tokens)), -1)
                if self.tokens is None:
                    return list()
                else:
                    return self.tokens

            def set_tokens(self,
                           tokens  # type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]
                           ):
                # type: (...) -> None
                if tokens is not None and not (isinstance(tokens, list) and all(
                    isinstance(item, RestApi.AdminDeploy.Token.TokenInfo) for item in tokens)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetTokensResponse.set_tokens type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.Token.TokenInfo]]]".format(
                            type_name=type(tokens)), -1)
                if tokens is None:
                    self.tokens = list()
                else:
                    self.tokens = tokens

        class InitTokenRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.tokenTypeId = None  # type: str
                self.tokenId = None  # type: str

            def __call__(self,
                         tokenTypeId,  # type: str
                         tokenId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.InitTokenRequest]
                self.set_token_type_id(tokenTypeId)
                self.set_token_id(tokenId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_token_type_id(self):
                # type: () -> str
                if not isinstance(self.tokenTypeId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.get_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenTypeId)), -1)
                return self.tokenTypeId

            @property
            def get_token_id(self):
                # type: () -> str
                if not isinstance(self.tokenId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.get_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenId)), -1)
                return self.tokenId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_token_type_id(self,
                                  token_type_id  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(token_type_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.set_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_type_id)), -1)
                self.tokenTypeId = token_type_id

            def set_token_id(self,
                             token_id  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(token_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenRequest.set_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_id)), -1)
                self.tokenId = token_id

        class InitTokenResponse(object):
            def __init__(self):
                # type: () -> None
                self.tokenSerial = None  # type: str

            def __call__(self,
                         tokenSerial,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.InitTokenResponse]
                self.set_token_serial(tokenSerial)
                return self

            @property
            def get_token_serial(self):
                # type: () -> str
                if not isinstance(self.tokenSerial, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenResponse.get_token_serial type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenSerial)), -1)
                return self.tokenSerial

            def set_token_serial(self,
                                 token_serial  # type: str
                                 ):
                # type: (...) -> None
                if not isinstance(token_serial, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InitTokenResponse.set_token_serial type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_serial)), -1)
                self.tokenSerial = token_serial

        class DeployTokenRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.tokenTypeId = None  # type: str
                self.tokenId = None  # type: str
                self.generateKeypair = None  # type: bool

            def __call__(self,
                         tokenTypeId,  # type: str
                         tokenId,  # type: str
                         generateKeypair,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.DeployTokenRequest]
                self.set_token_type_id(tokenTypeId)
                self.set_token_id(tokenId)
                self.set_generate_keypair(generateKeypair)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_token_type_id(self):
                # type: () -> str
                if not isinstance(self.tokenTypeId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.get_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenTypeId)), -1)
                return self.tokenTypeId

            @property
            def get_token_id(self):
                # type: () -> str
                if not isinstance(self.tokenId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.get_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.tokenId)), -1)
                return self.tokenId

            @property
            def get_generate_keypair(self):
                # type: () -> bool
                if not isinstance(self.generateKeypair, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.get_generate_keypair type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.generateKeypair)), -1)
                return self.generateKeypair

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_token_type_id(self,
                                  token_type_id  # type: str
                                  ):
                # type: (...) -> None
                if not isinstance(token_type_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.set_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_type_id)), -1)
                self.tokenTypeId = token_type_id

            def set_token_id(self,
                             token_id  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(token_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.set_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(token_id)), -1)
                self.tokenId = token_id

            def set_generate_keypair(self,
                                     generate_keypair  # type: bool
                                     ):
                # type: (...) -> None
                if not isinstance(generate_keypair, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenRequest.set_generate_keypair type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(generate_keypair)), -1)
                self.generateKeypair = generate_keypair

        class DeployTokenResponse(object):
            def __init__(self):
                # type: () -> None
                self.publicKey = None  # type: str

            def __call__(self,
                         publicKey,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.DeployTokenResponse]
                self.set_public_key(publicKey)
                return self

            @property
            def get_public_key(self):
                # type: () -> str
                if not isinstance(self.publicKey, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenResponse.get_public_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.publicKey)), -1)
                return self.publicKey

            def set_public_key(self,
                               public_key  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(public_key, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.DeployTokenResponse.set_public_key type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(public_key)), -1)
                self.publicKey = public_key

        class GetRuntimeEnvInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.runtimeEnvId = None  # type: str

            def __call__(self,
                         runtimeEnvId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetRuntimeEnvInfoRequest]
                self.set_runtime_env_id(runtimeEnvId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_runtime_env_id(self):
                # type: () -> str
                if not isinstance(self.runtimeEnvId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoRequest.get_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.runtimeEnvId)), -1)
                return self.runtimeEnvId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_runtime_env_id(self,
                                   runtime_env_id  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(runtime_env_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoRequest.set_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(runtime_env_id)), -1)
                self.runtimeEnvId = runtime_env_id

        class GPMInfo(object):
            def __init__(self):
                # type: () -> None
                self.gpmId = None  # type: str
                self.gpmLabel = None  # type: str
                self.gpmDescription = None  # type: str
                self.gpmVersion = None  # type: str
                self.gpmVersionRelease = None  # type: str
                self.gpmArch = None  # type: str

            def __call__(self,
                         gpmId,  # type: str
                         gpmLabel,  # type: str
                         gpmDescription,  # type: str
                         gpmVersion,  # type: str
                         gpmVersionRelease,  # type: str
                         gpmArch,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GPMInfo]
                self.set_gpm_id(gpmId)
                self.set_gpm_label(gpmLabel)
                self.set_gpm_description(gpmDescription)
                self.set_gpm_version(gpmVersion)
                self.set_gpm_version_release(gpmVersionRelease)
                self.set_gpm_arch(gpmArch)
                return self

            @property
            def get_gpm_id(self):
                # type: () -> str
                if not isinstance(self.gpmId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmId)), -1)
                return self.gpmId

            @property
            def get_gpm_label(self):
                # type: () -> str
                if not isinstance(self.gpmLabel, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmLabel)), -1)
                return self.gpmLabel

            @property
            def get_gpm_description(self):
                # type: () -> str
                if not isinstance(self.gpmDescription, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmDescription)), -1)
                return self.gpmDescription

            @property
            def get_gpm_version(self):
                # type: () -> str
                if not isinstance(self.gpmVersion, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmVersion)), -1)
                return self.gpmVersion

            @property
            def get_gpm_version_release(self):
                # type: () -> str
                if not isinstance(self.gpmVersionRelease, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_version_release type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmVersionRelease)), -1)
                return self.gpmVersionRelease

            @property
            def get_gpm_arch(self):
                # type: () -> str
                if not isinstance(self.gpmArch, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.get_gpm_arch type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmArch)), -1)
                return self.gpmArch

            def set_gpm_id(self,
                           gpm_id  # type: str
                           ):
                # type: (...) -> None
                if not isinstance(gpm_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_id)), -1)
                self.gpmId = gpm_id

            def set_gpm_label(self,
                              gpm_label  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(gpm_label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_label)), -1)
                self.gpmLabel = gpm_label

            def set_gpm_description(self,
                                    gpm_description  # type: str
                                    ):
                # type: (...) -> None
                if not isinstance(gpm_description, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_description)), -1)
                self.gpmDescription = gpm_description

            def set_gpm_version(self,
                                gpm_version  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(gpm_version, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_version)), -1)
                self.gpmVersion = gpm_version

            def set_gpm_version_release(self,
                                        gpm_version_release  # type: str
                                        ):
                # type: (...) -> None
                if not isinstance(gpm_version_release, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_version_release type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_version_release)), -1)
                self.gpmVersionRelease = gpm_version_release

            def set_gpm_arch(self,
                             gpm_arch  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(gpm_arch, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GPMInfo.set_gpm_arch type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_arch)), -1)
                self.gpmArch = gpm_arch

        class GetRuntimeEnvInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.runtimeEnvId = None  # type: str
                self.runtimeEnvLabel = None  # type: str
                self.installedGpms = list()  # type: Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]

            def __call__(self,
                         runtimeEnvId,  # type: str
                         runtimeEnvLabel,  # type: str
                         installedGpms=None,  # type: Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetRuntimeEnvInfoResponse]
                self.set_runtime_env_id(runtimeEnvId)
                self.set_runtime_env_label(runtimeEnvLabel)
                if installedGpms is not None:
                    if isinstance(installedGpms, list):
                        for index, item in enumerate(installedGpms):
                            if isinstance(item, dict):
                                installedGpms[index] = RestApi.AdminDeploy.GPMInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.__call__ installedGpms type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(installedGpms)))
                self.set_installed_gpms(installedGpms)
                return self

            @property
            def get_runtime_env_id(self):
                # type: () -> str
                if not isinstance(self.runtimeEnvId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.get_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.runtimeEnvId)), -1)
                return self.runtimeEnvId

            @property
            def get_runtime_env_label(self):
                # type: () -> str
                if not isinstance(self.runtimeEnvLabel, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.get_runtime_env_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.runtimeEnvLabel)), -1)
                return self.runtimeEnvLabel

            @property
            def get_installed_gpms(self):
                # type: () -> Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]
                if self.installedGpms is not None and not (isinstance(self.installedGpms, list) and all(
                    isinstance(item, RestApi.AdminDeploy.GPMInfo) for item in self.installedGpms)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.get_installed_gpms type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]".format(
                            type_name=type(self.installedGpms)), -1)
                if self.installedGpms is None:
                    return list()
                else:
                    return self.installedGpms

            def set_runtime_env_id(self,
                                   runtime_env_id  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(runtime_env_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.set_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(runtime_env_id)), -1)
                self.runtimeEnvId = runtime_env_id

            def set_runtime_env_label(self,
                                      runtime_env_label  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(runtime_env_label, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.set_runtime_env_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(runtime_env_label)), -1)
                self.runtimeEnvLabel = runtime_env_label

            def set_installed_gpms(self,
                                   installed_gpms  # type: Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]
                                   ):
                # type: (...) -> None
                if installed_gpms is not None and not (isinstance(installed_gpms, list) and all(
                    isinstance(item, RestApi.AdminDeploy.GPMInfo) for item in installed_gpms)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetRuntimeEnvInfoResponse.set_installed_gpms type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.AdminDeploy.GPMInfo]]]".format(
                            type_name=type(installed_gpms)), -1)
                if installed_gpms is None:
                    self.installedGpms = list()
                else:
                    self.installedGpms = installed_gpms

        class InstallGPMCollectionRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.runtimeEnvId = None  # type: str
                self.gpmCollectionId = None  # type: str

            def __call__(self,
                         runtimeEnvId,  # type: str
                         gpmCollectionId,  # type: str
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.InstallGPMCollectionRequest]
                self.set_runtime_env_id(runtimeEnvId)
                self.set_gpm_collection_id(gpmCollectionId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_runtime_env_id(self):
                # type: () -> str
                if not isinstance(self.runtimeEnvId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.get_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.runtimeEnvId)), -1)
                return self.runtimeEnvId

            @property
            def get_gpm_collection_id(self):
                # type: () -> str
                if not isinstance(self.gpmCollectionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.get_gpm_collection_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.gpmCollectionId)), -1)
                return self.gpmCollectionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_runtime_env_id(self,
                                   runtime_env_id  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(runtime_env_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.set_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(runtime_env_id)), -1)
                self.runtimeEnvId = runtime_env_id

            def set_gpm_collection_id(self,
                                      gpm_collection_id  # type: str
                                      ):
                # type: (...) -> None
                if not isinstance(gpm_collection_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionRequest.set_gpm_collection_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(gpm_collection_id)), -1)
                self.gpmCollectionId = gpm_collection_id

        class InstallGPMCollectionResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.InstallGPMCollectionResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.InstallGPMCollectionResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class GetGPMCollectionsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.GetGPMCollectionsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetGPMCollectionsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetGPMCollectionsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetGPMCollectionsResponse(object):
            def __init__(self):
                # type: () -> None
                self.gpmCollections = list()  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]

            def __call__(self,
                         gpmCollections=None,  # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetGPMCollectionsResponse]
                if gpmCollections is not None:
                    if isinstance(gpmCollections, list):
                        for index, item in enumerate(gpmCollections):
                            if isinstance(item, dict):
                                gpmCollections[index] = RestApi.Admin.Gpm.GPMCollectionInfo()(**item)
                    else:
                        raise TypeError(
                            "RestApi.AdminDeploy.GetGPMCollectionsResponse.__call__ gpmCollections type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gpmCollections)))
                self.set_gpm_collections(gpmCollections)
                return self

            @property
            def get_gpm_collections(self):
                # type: () -> Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                if self.gpmCollections is not None and not (isinstance(self.gpmCollections, list) and all(
                    isinstance(item, RestApi.Admin.Gpm.GPMCollectionInfo) for item in self.gpmCollections)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetGPMCollectionsResponse.get_gpm_collections type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]".format(
                            type_name=type(self.gpmCollections)), -1)
                if self.gpmCollections is None:
                    return list()
                else:
                    return self.gpmCollections

            def set_gpm_collections(self,
                                    gpm_collections
                                    # type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]
                                    ):
                # type: (...) -> None
                if gpm_collections is not None and not (isinstance(gpm_collections, list) and all(
                    isinstance(item, RestApi.Admin.Gpm.GPMCollectionInfo) for item in gpm_collections)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetGPMCollectionsResponse.set_gpm_collections type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.Admin.Gpm.GPMCollectionInfo]]]".format(
                            type_name=type(gpm_collections)), -1)
                if gpm_collections is None:
                    self.gpmCollections = list()
                else:
                    self.gpmCollections = gpm_collections

        class GetJobInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetJobInfoRequest]
                self.set_job_id(jobId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoRequest.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoRequest.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class GetJobInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobInfo = None  # type: Type[RestApi.AdminDeploy.Job.JobInfo]

            def __call__(self,
                         jobInfo,  # type: Type[RestApi.AdminDeploy.Job.JobInfo]
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.GetJobInfoResponse]
                if isinstance(jobInfo, dict):
                    jobInfo = RestApi.AdminDeploy.Job.JobInfo()(**jobInfo)
                self.set_job_info(jobInfo)
                return self

            @property
            def get_job_info(self):
                # type: () -> Type[RestApi.AdminDeploy.Job.JobInfo]
                if not isinstance(self.jobInfo, RestApi.AdminDeploy.Job.JobInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoResponse.get_job_info type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.AdminDeploy.Job.JobInfo]".format(
                            type_name=type(self.jobInfo)), -1)
                return self.jobInfo

            def set_job_info(self,
                             job_info  # type: Type[RestApi.AdminDeploy.Job.JobInfo]
                             ):
                # type: (...) -> None
                if not isinstance(job_info, RestApi.AdminDeploy.Job.JobInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.GetJobInfoResponse.set_job_info type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.AdminDeploy.Job.JobInfo]".format(
                            type_name=type(job_info)), -1)
                self.jobInfo = job_info

        class CancelJobRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.CancelJobRequest]
                self.set_job_id(jobId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobRequest.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobRequest.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class CancelJobResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.CancelJobResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.CancelJobResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class PingRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: Optional[str]

            def __call__(self
                         ):
                # type: () -> Type[RestApi.AdminDeploy.PingRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> Optional[str]
                if self.sessionId is not None and not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.PingRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: Optional[str]
                               ):
                # type: (...) -> None
                if session_id is not None and not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.PingRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class PingResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.AdminDeploy.PingResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.PingResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.AdminDeploy.PingResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class Job(object):
            def __init__(self):  # type: () -> None
                pass

            class JobInfo(object):
                def __init__(self):
                    # type: () -> None
                    self.jobMore = None  # type: bool
                    self.jobHeader = None  # type: str
                    self.jobSubHeader = None  # type: str
                    self.jobProgress = None  # type: Optional[int]
                    self.jobDoneMessage = None  # type: Optional[str]
                    self.jobDoneArg01 = None  # type: Optional[str]
                    self.jobStatus = None  # type: Optional[str]

                def __call__(self,
                             jobMore,  # type: bool
                             jobHeader,  # type: str
                             jobSubHeader,  # type: str
                             jobProgress=None,  # type: Optional[int]
                             jobDoneMessage=None,  # type: Optional[str]
                             jobDoneArg01=None,  # type: Optional[str]
                             jobStatus=None,  # type: Optional[str]
                             ):
                    # type: (...) -> Type[RestApi.AdminDeploy.Job.JobInfo]
                    self.set_job_more(jobMore)
                    self.set_job_header(jobHeader)
                    self.set_job_sub_header(jobSubHeader)
                    self.set_job_progress(jobProgress)
                    self.set_job_done_message(jobDoneMessage)
                    self.set_job_done_arg_01(jobDoneArg01)
                    self.set_job_status(jobStatus)
                    return self

                @property
                def get_job_more(self):
                    # type: () -> bool
                    if not isinstance(self.jobMore, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_more type {type_name} - erroneous type/subtype. Expected type: bool".format(
                                type_name=type(self.jobMore)), -1)
                    return self.jobMore

                @property
                def get_job_header(self):
                    # type: () -> str
                    if not isinstance(self.jobHeader, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_header type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.jobHeader)), -1)
                    return self.jobHeader

                @property
                def get_job_sub_header(self):
                    # type: () -> str
                    if not isinstance(self.jobSubHeader, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_sub_header type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.jobSubHeader)), -1)
                    return self.jobSubHeader

                @property
                def get_job_progress(self):
                    # type: () -> Optional[int]
                    if self.jobProgress is not None and not isinstance(self.jobProgress, (int, long)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_progress type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                                type_name=type(self.jobProgress)), -1)
                    return self.jobProgress

                @property
                def get_job_done_message(self):
                    # type: () -> Optional[str]
                    if self.jobDoneMessage is not None and not isinstance(self.jobDoneMessage, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_done_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.jobDoneMessage)), -1)
                    return self.jobDoneMessage

                @property
                def get_job_done_arg_01(self):
                    # type: () -> Optional[str]
                    if self.jobDoneArg01 is not None and not isinstance(self.jobDoneArg01, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_done_arg_01 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.jobDoneArg01)), -1)
                    return self.jobDoneArg01

                @property
                def get_job_status(self):
                    # type: () -> Optional[str]
                    if self.jobStatus is not None and not isinstance(self.jobStatus, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.get_job_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.jobStatus)), -1)
                    return self.jobStatus

                def set_job_more(self,
                                 job_more  # type: bool
                                 ):
                    # type: (...) -> None
                    if not isinstance(job_more, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_more type {type_name} - erroneous type/subtype. Expected type: bool".format(
                                type_name=type(job_more)), -1)
                    self.jobMore = job_more

                def set_job_header(self,
                                   job_header  # type: str
                                   ):
                    # type: (...) -> None
                    if not isinstance(job_header, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_header type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(job_header)), -1)
                    self.jobHeader = job_header

                def set_job_sub_header(self,
                                       job_sub_header  # type: str
                                       ):
                    # type: (...) -> None
                    if not isinstance(job_sub_header, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_sub_header type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(job_sub_header)), -1)
                    self.jobSubHeader = job_sub_header

                def set_job_progress(self,
                                     job_progress  # type: Optional[int]
                                     ):
                    # type: (...) -> None
                    if job_progress is not None and not isinstance(job_progress, (int, long)):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_progress type {type_name} - erroneous type/subtype. Expected type: Optional[int]".format(
                                type_name=type(job_progress)), -1)
                    self.jobProgress = job_progress

                def set_job_done_message(self,
                                         job_done_message  # type: Optional[str]
                                         ):
                    # type: (...) -> None
                    if job_done_message is not None and not isinstance(job_done_message, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_done_message type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(job_done_message)), -1)
                    self.jobDoneMessage = job_done_message

                def set_job_done_arg_01(self,
                                        job_done_arg_01  # type: Optional[str]
                                        ):
                    # type: (...) -> None
                    if job_done_arg_01 is not None and not isinstance(job_done_arg_01, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_done_arg_01 type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(job_done_arg_01)), -1)
                    self.jobDoneArg01 = job_done_arg_01

                def set_job_status(self,
                                   job_status  # type: Optional[str]
                                   ):
                    # type: (...) -> None
                    if job_status is not None and not isinstance(job_status, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Job.JobInfo.set_job_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(job_status)), -1)
                    self.jobStatus = job_status

        class Token(object):
            def __init__(self):  # type: () -> None
                pass

            class TokenInfo(object):
                def __init__(self):
                    # type: () -> None
                    self.tokenTypeId = None  # type: str
                    self.tokenInternalType = None  # type: str
                    self.tokenId = None  # type: str
                    self.tokenLabel = None  # type: str
                    self.tokenStatus = None  # type: str
                    self.tokenSerial = None  # type: Optional[str]
                    self.runtimeEnvId = None  # type: Optional[str]
                    self.tokenTypeLabel = None  # type: str
                    self.name = None  # type: Optional[str]
                    self.casing = None  # type: Optional[str]
                    self.description = None  # type: Optional[str]
                    self.enrollable = None  # type: Optional[bool]

                def __call__(self,
                             tokenTypeId,  # type: str
                             tokenInternalType,  # type: str
                             tokenId,  # type: str
                             tokenLabel,  # type: str
                             tokenStatus,  # type: str
                             tokenTypeLabel,  # type: str
                             tokenSerial=None,  # type: Optional[str]
                             runtimeEnvId=None,  # type: Optional[str]
                             name=None,  # type: Optional[str]
                             casing=None,  # type: Optional[str]
                             description=None,  # type: Optional[str]
                             enrollable=None,  # type: Optional[bool]
                             ):
                    # type: (...) -> Type[RestApi.AdminDeploy.Token.TokenInfo]
                    self.set_token_type_id(tokenTypeId)
                    self.set_token_internal_type(tokenInternalType)
                    self.set_token_id(tokenId)
                    self.set_token_label(tokenLabel)
                    self.set_token_status(tokenStatus)
                    self.set_token_type_label(tokenTypeLabel)
                    self.set_token_serial(tokenSerial)
                    self.set_runtime_env_id(runtimeEnvId)
                    self.set_name(name)
                    self.set_casing(casing)
                    self.set_description(description)
                    self.set_enrollable(enrollable)
                    return self

                @property
                def get_token_type_id(self):
                    # type: () -> str
                    if not isinstance(self.tokenTypeId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenTypeId)), -1)
                    return self.tokenTypeId

                @property
                def get_token_internal_type(self):
                    # type: () -> str
                    if not isinstance(self.tokenInternalType, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_internal_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenInternalType)), -1)
                    return self.tokenInternalType

                @property
                def get_token_id(self):
                    # type: () -> str
                    if not isinstance(self.tokenId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenId)), -1)
                    return self.tokenId

                @property
                def get_token_label(self):
                    # type: () -> str
                    if not isinstance(self.tokenLabel, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenLabel)), -1)
                    return self.tokenLabel

                @property
                def get_token_status(self):
                    # type: () -> str
                    if not isinstance(self.tokenStatus, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_status type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenStatus)), -1)
                    return self.tokenStatus

                @property
                def get_token_serial(self):
                    # type: () -> Optional[str]
                    if self.tokenSerial is not None and not isinstance(self.tokenSerial, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_serial type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.tokenSerial)), -1)
                    return self.tokenSerial

                @property
                def get_runtime_env_id(self):
                    # type: () -> Optional[str]
                    if self.runtimeEnvId is not None and not isinstance(self.runtimeEnvId, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.runtimeEnvId)), -1)
                    return self.runtimeEnvId

                @property
                def get_token_type_label(self):
                    # type: () -> str
                    if not isinstance(self.tokenTypeLabel, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_token_type_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(self.tokenTypeLabel)), -1)
                    return self.tokenTypeLabel

                @property
                def get_name(self):
                    # type: () -> Optional[str]
                    if self.name is not None and not isinstance(self.name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.name)), -1)
                    return self.name

                @property
                def get_casing(self):
                    # type: () -> Optional[str]
                    if self.casing is not None and not isinstance(self.casing, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_casing type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.casing)), -1)
                    return self.casing

                @property
                def get_description(self):
                    # type: () -> Optional[str]
                    if self.description is not None and not isinstance(self.description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(self.description)), -1)
                    return self.description

                @property
                def get_enrollable(self):
                    # type: () -> Optional[bool]
                    if self.enrollable is not None and not isinstance(self.enrollable, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.get_enrollable type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(self.enrollable)), -1)
                    return self.enrollable

                def set_token_type_id(self,
                                      token_type_id  # type: str
                                      ):
                    # type: (...) -> None
                    if not isinstance(token_type_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_type_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_type_id)), -1)
                    self.tokenTypeId = token_type_id

                def set_token_internal_type(self,
                                            token_internal_type  # type: str
                                            ):
                    # type: (...) -> None
                    if not isinstance(token_internal_type, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_internal_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_internal_type)), -1)
                    self.tokenInternalType = token_internal_type

                def set_token_id(self,
                                 token_id  # type: str
                                 ):
                    # type: (...) -> None
                    if not isinstance(token_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_id)), -1)
                    self.tokenId = token_id

                def set_token_label(self,
                                    token_label  # type: str
                                    ):
                    # type: (...) -> None
                    if not isinstance(token_label, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_label)), -1)
                    self.tokenLabel = token_label

                def set_token_status(self,
                                     token_status  # type: str
                                     ):
                    # type: (...) -> None
                    if not isinstance(token_status, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_status type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_status)), -1)
                    self.tokenStatus = token_status

                def set_token_serial(self,
                                     token_serial  # type: Optional[str]
                                     ):
                    # type: (...) -> None
                    if token_serial is not None and not isinstance(token_serial, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_serial type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(token_serial)), -1)
                    self.tokenSerial = token_serial

                def set_runtime_env_id(self,
                                       runtime_env_id  # type: Optional[str]
                                       ):
                    # type: (...) -> None
                    if runtime_env_id is not None and not isinstance(runtime_env_id, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_runtime_env_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(runtime_env_id)), -1)
                    self.runtimeEnvId = runtime_env_id

                def set_token_type_label(self,
                                         token_type_label  # type: str
                                         ):
                    # type: (...) -> None
                    if not isinstance(token_type_label, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_token_type_label type {type_name} - erroneous type/subtype. Expected type: str".format(
                                type_name=type(token_type_label)), -1)
                    self.tokenTypeLabel = token_type_label

                def set_name(self,
                             name  # type: Optional[str]
                             ):
                    # type: (...) -> None
                    if name is not None and not isinstance(name, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(name)), -1)
                    self.name = name

                def set_casing(self,
                               casing  # type: Optional[str]
                               ):
                    # type: (...) -> None
                    if casing is not None and not isinstance(casing, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_casing type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(casing)), -1)
                    self.casing = casing

                def set_description(self,
                                    description  # type: Optional[str]
                                    ):
                    # type: (...) -> None
                    if description is not None and not isinstance(description, basestring):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_description type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                                type_name=type(description)), -1)
                    self.description = description

                def set_enrollable(self,
                                   enrollable  # type: Optional[bool]
                                   ):
                    # type: (...) -> None
                    if enrollable is not None and not isinstance(enrollable, bool):
                        raise RestApi.Admin.FaultServerElement()(
                            "RestApi.AdminDeploy.Token.TokenInfo.set_enrollable type {type_name} - erroneous type/subtype. Expected type: Optional[bool]".format(
                                type_name=type(enrollable)), -1)
                    self.enrollable = enrollable

    class ServerConfig(object):
        def __init__(self):  # type: () -> None
            pass

        class LoginRequest(object):
            def __init__(self):
                # type: () -> None
                self.username = None  # type: str
                self.password = None  # type: str

            def __call__(self,
                         username,  # type: str
                         password,  # type: str
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.LoginRequest]
                self.set_username(username)
                self.set_password(password)
                return self

            @property
            def get_username(self):
                # type: () -> str
                if not isinstance(self.username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginRequest.get_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.username)), -1)
                return self.username

            @property
            def get_password(self):
                # type: () -> str
                if not isinstance(self.password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginRequest.get_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.password)), -1)
                return self.password

            def set_username(self,
                             username  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(username, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginRequest.set_username type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(username)), -1)
                self.username = username

            def set_password(self,
                             password  # type: str
                             ):
                # type: (...) -> None
                if not isinstance(password, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginRequest.set_password type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(password)), -1)
                self.password = password

        class LoginResponse(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.LoginResponse]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginResponse.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LoginResponse.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class LogoutRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.LogoutRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LogoutRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LogoutRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class LogoutResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.LogoutResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LogoutResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LogoutResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GetStatusRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetStatusRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetStatusRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetStatusRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetStatusResponse(object):
            def __init__(self):
                # type: () -> None
                self.configured = None  # type: bool

            def __call__(self,
                         configured,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetStatusResponse]
                self.set_configured(configured)
                return self

            @property
            def get_configured(self):
                # type: () -> bool
                if not isinstance(self.configured, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetStatusResponse.get_configured type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.configured)), -1)
                return self.configured

            def set_configured(self,
                               configured  # type: bool
                               ):
                # type: (...) -> None
                if not isinstance(configured, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetStatusResponse.set_configured type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(configured)), -1)
                self.configured = configured

        class GetJobInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetJobInfoRequest]
                self.set_job_id(jobId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoRequest.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoRequest.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class GetJobInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobInfo = None  # type: Type[RestApi.AdminDeploy.Job.JobInfo]

            def __call__(self,
                         jobInfo,  # type: Type[RestApi.AdminDeploy.Job.JobInfo]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetJobInfoResponse]
                if isinstance(jobInfo, dict):
                    jobInfo = RestApi.AdminDeploy.Job.JobInfo()(**jobInfo)
                self.set_job_info(jobInfo)
                return self

            @property
            def get_job_info(self):
                # type: () -> Type[RestApi.AdminDeploy.Job.JobInfo]
                if not isinstance(self.jobInfo, RestApi.AdminDeploy.Job.JobInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoResponse.get_job_info type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.AdminDeploy.Job.JobInfo]".format(
                            type_name=type(self.jobInfo)), -1)
                return self.jobInfo

            def set_job_info(self,
                             job_info  # type: Type[RestApi.AdminDeploy.Job.JobInfo]
                             ):
                # type: (...) -> None
                if not isinstance(job_info, RestApi.AdminDeploy.Job.JobInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetJobInfoResponse.set_job_info type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.AdminDeploy.Job.JobInfo]".format(
                            type_name=type(job_info)), -1)
                self.jobInfo = job_info

        class CancelJobRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.CancelJobRequest]
                self.set_job_id(jobId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobRequest.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobRequest.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class CancelJobResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.CancelJobResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.CancelJobResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class StartJobGenerateKnownsecretsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobGenerateKnownsecretsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateKnownsecretsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateKnownsecretsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobGenerateKnownsecretsResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobGenerateKnownsecretsResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateKnownsecretsResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateKnownsecretsResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobGenerateGPMSRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobGenerateGPMSRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateGPMSRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateGPMSRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobGenerateGPMSResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobGenerateGPMSResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateGPMSResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateGPMSResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobGenerateDemodataRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobGenerateDemodataRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateDemodataRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateDemodataRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobGenerateDemodataResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobGenerateDemodataResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateDemodataResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateDemodataResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobInstallServicesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobInstallServicesRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobInstallServicesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobInstallServicesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobInstallServicesResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobInstallServicesResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobInstallServicesResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobInstallServicesResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobGenerateSupportPackageRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobGenerateSupportPackageRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateSupportPackageRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateSupportPackageRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobGenerateSupportPackageResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobGenerateSupportPackageResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateSupportPackageResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobGenerateSupportPackageResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobPrepareInstallationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobPrepareInstallationRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareInstallationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareInstallationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobPrepareInstallationResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobPrepareInstallationResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareInstallationResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareInstallationResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobFinalizeInstallationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobFinalizeInstallationRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeInstallationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeInstallationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobFinalizeInstallationResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobFinalizeInstallationResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeInstallationResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeInstallationResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobPrepareChangeRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobPrepareChangeRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareChangeRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareChangeRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobPrepareChangeResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobPrepareChangeResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareChangeResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareChangeResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobFinalizeChangeRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobFinalizeChangeRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeChangeRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeChangeRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobFinalizeChangeResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobFinalizeChangeResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeChangeResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeChangeResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobPrepareUpgradeRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.systemName = None  # type: str

            def __call__(self,
                         systemName,  # type: str
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobPrepareUpgradeRequest]
                self.set_system_name(systemName)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_system_name(self):
                # type: () -> str
                if not isinstance(self.systemName, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeRequest.get_system_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.systemName)), -1)
                return self.systemName

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_system_name(self,
                                system_name  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(system_name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeRequest.set_system_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(system_name)), -1)
                self.systemName = system_name

        class StartJobPrepareUpgradeResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobPrepareUpgradeResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobPrepareUpgradeResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class StartJobFinalizeUpgradeRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.StartJobFinalizeUpgradeRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeUpgradeRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeUpgradeRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class StartJobFinalizeUpgradeResponse(object):
            def __init__(self):
                # type: () -> None
                self.jobId = None  # type: int

            def __call__(self,
                         jobId,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.StartJobFinalizeUpgradeResponse]
                self.set_job_id(jobId)
                return self

            @property
            def get_job_id(self):
                # type: () -> int
                if not isinstance(self.jobId, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeUpgradeResponse.get_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.jobId)), -1)
                return self.jobId

            def set_job_id(self,
                           job_id  # type: int
                           ):
                # type: (...) -> None
                if not isinstance(job_id, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.StartJobFinalizeUpgradeResponse.set_job_id type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(job_id)), -1)
                self.jobId = job_id

        class GetFirstConfigSpecificationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetFirstConfigSpecificationRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetFirstConfigSpecificationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetFirstConfigSpecificationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetFirstConfigSpecificationResponse(object):
            def __init__(self):
                # type: () -> None
                self.configSpecification = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configSpecification=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetFirstConfigSpecificationResponse]
                if configSpecification is not None:
                    if isinstance(configSpecification, dict):
                        configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                            **configSpecification)
                self.set_config_specification(configSpecification)
                return self

            @property
            def get_config_specification(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configSpecification is not None and not isinstance(self.configSpecification,
                                                                           RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetFirstConfigSpecificationResponse.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            def set_config_specification(self,
                                         config_specification
                                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                         ):
                # type: (...) -> None
                if config_specification is not None and not isinstance(config_specification,
                                                                       RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetFirstConfigSpecificationResponse.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

        class GetNextConfigSpecificationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetNextConfigSpecificationRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetNextConfigSpecificationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetNextConfigSpecificationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GetNextConfigSpecificationResponse(object):
            def __init__(self):
                # type: () -> None
                self.configSpecification = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configSpecification=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetNextConfigSpecificationResponse]
                if configSpecification is not None:
                    if isinstance(configSpecification, dict):
                        configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                            **configSpecification)
                self.set_config_specification(configSpecification)
                return self

            @property
            def get_config_specification(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configSpecification is not None and not isinstance(self.configSpecification,
                                                                           RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetNextConfigSpecificationResponse.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            def set_config_specification(self,
                                         config_specification
                                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                         ):
                # type: (...) -> None
                if config_specification is not None and not isinstance(config_specification,
                                                                       RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetNextConfigSpecificationResponse.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

        class SaveConfigSpecificationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.configSpecification = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         configSpecification,
                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SaveConfigSpecificationRequest]
                if isinstance(configSpecification, dict):
                    configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                        **configSpecification)
                self.set_config_specification(configSpecification)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_config_specification(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.configSpecification,
                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationRequest.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_config_specification(self,
                                         config_specification
                                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                         ):
                # type: (...) -> None
                if not isinstance(config_specification, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationRequest.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

        class SaveConfigSpecificationResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.errorLocation = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]

            def __call__(self,
                         rc,  # type: bool
                         errorLocation=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SaveConfigSpecificationResponse]
                self.set_rc(rc)
                if errorLocation is not None:
                    if isinstance(errorLocation, dict):
                        errorLocation = RestApi.Admin.ConfigTemplate.ErrorLocation()(**errorLocation)
                self.set_error_location(errorLocation)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_error_location(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                if self.errorLocation is not None and not isinstance(self.errorLocation,
                                                                     RestApi.Admin.ConfigTemplate.ErrorLocation):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationResponse.get_error_location type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]".format(
                            type_name=type(self.errorLocation)), -1)
                return self.errorLocation

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_error_location(self,
                                   error_location
                                   # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                                   ):
                # type: (...) -> None
                if error_location is not None and not isinstance(error_location,
                                                                 RestApi.Admin.ConfigTemplate.ErrorLocation):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigSpecificationResponse.set_error_location type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]".format(
                            type_name=type(error_location)), -1)
                self.errorLocation = error_location

        class ReloadConfigSpecificationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.ReloadConfigSpecificationRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.ReloadConfigSpecificationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.ReloadConfigSpecificationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class ReloadConfigSpecificationResponse(object):
            def __init__(self):
                # type: () -> None
                self.configSpecification = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         configSpecification,
                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.ReloadConfigSpecificationResponse]
                if isinstance(configSpecification, dict):
                    configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                        **configSpecification)
                self.set_config_specification(configSpecification)
                return self

            @property
            def get_config_specification(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.configSpecification,
                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.ReloadConfigSpecificationResponse.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            def set_config_specification(self,
                                         config_specification
                                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                         ):
                # type: (...) -> None
                if not isinstance(config_specification, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.ReloadConfigSpecificationResponse.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

        class GetConfigTemplateRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.configTemplateSpec = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]

            def __call__(self,
                         configTemplateSpec,
                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetConfigTemplateRequest]
                if isinstance(configTemplateSpec, dict):
                    configTemplateSpec = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()(
                        **configTemplateSpec)
                self.set_config_template_spec(configTemplateSpec)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_config_template_spec(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                if not isinstance(self.configTemplateSpec,
                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateRequest.get_config_template_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]".format(
                            type_name=type(self.configTemplateSpec)), -1)
                return self.configTemplateSpec

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_config_template_spec(self,
                                         config_template_spec
                                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                                         ):
                # type: (...) -> None
                if not isinstance(config_template_spec,
                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateRequest.set_config_template_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]".format(
                            type_name=type(config_template_spec)), -1)
                self.configTemplateSpec = config_template_spec

        class GetConfigTemplateResponse(object):
            def __init__(self):
                # type: () -> None
                self.configTemplate = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         configTemplate=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetConfigTemplateResponse]
                if configTemplate is not None:
                    if isinstance(configTemplate, dict):
                        configTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                            **configTemplate)
                self.set_config_template(configTemplate)
                return self

            @property
            def get_config_template(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configTemplate is not None and not isinstance(self.configTemplate,
                                                                      RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateResponse.get_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configTemplate)), -1)
                return self.configTemplate

            def set_config_template(self,
                                    config_template
                                    # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                    ):
                # type: (...) -> None
                if config_template is not None and not isinstance(config_template,
                                                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetConfigTemplateResponse.set_config_template type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_template)), -1)
                self.configTemplate = config_template

        class SaveConfigTemplateRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.entityType = None  # type: str
                self.subType = None  # type: Optional[str]
                self.elementId = None  # type: Optional[str]
                self.configTemplate = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]

            def __call__(self,
                         entityType,  # type: str
                         configTemplate,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         subType=None,  # type: Optional[str]
                         elementId=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SaveConfigTemplateRequest]
                self.set_entity_type(entityType)
                if isinstance(configTemplate, dict):
                    configTemplate = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(**configTemplate)
                self.set_config_template(configTemplate)
                self.set_sub_type(subType)
                self.set_element_id(elementId)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_entity_type(self):
                # type: () -> str
                if not isinstance(self.entityType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.get_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.entityType)), -1)
                return self.entityType

            @property
            def get_sub_type(self):
                # type: () -> Optional[str]
                if self.subType is not None and not isinstance(self.subType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.get_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.subType)), -1)
                return self.subType

            @property
            def get_element_id(self):
                # type: () -> Optional[str]
                if self.elementId is not None and not isinstance(self.elementId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.get_element_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.elementId)), -1)
                return self.elementId

            @property
            def get_config_template(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.configTemplate, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.get_config_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.configTemplate)), -1)
                return self.configTemplate

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_entity_type(self,
                                entity_type  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(entity_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.set_entity_type type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(entity_type)), -1)
                self.entityType = entity_type

            def set_sub_type(self,
                             sub_type  # type: Optional[str]
                             ):
                # type: (...) -> None
                if sub_type is not None and not isinstance(sub_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.set_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(sub_type)), -1)
                self.subType = sub_type

            def set_element_id(self,
                               element_id  # type: Optional[str]
                               ):
                # type: (...) -> None
                if element_id is not None and not isinstance(element_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.set_element_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(element_id)), -1)
                self.elementId = element_id

            def set_config_template(self,
                                    config_template
                                    # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                    ):
                # type: (...) -> None
                if not isinstance(config_template, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateRequest.set_config_template type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(config_template)), -1)
                self.configTemplate = config_template

        class SaveConfigTemplateResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.errorLocation = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                self.configSpec = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]

            def __call__(self,
                         rc,  # type: bool
                         errorLocation=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                         configSpec=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SaveConfigTemplateResponse]
                self.set_rc(rc)
                if errorLocation is not None:
                    if isinstance(errorLocation, dict):
                        errorLocation = RestApi.Admin.ConfigTemplate.ErrorLocation()(**errorLocation)
                self.set_error_location(errorLocation)
                if configSpec is not None:
                    if isinstance(configSpec, dict):
                        configSpec = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()(**configSpec)
                self.set_config_spec(configSpec)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_error_location(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                if self.errorLocation is not None and not isinstance(self.errorLocation,
                                                                     RestApi.Admin.ConfigTemplate.ErrorLocation):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.get_error_location type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]".format(
                            type_name=type(self.errorLocation)), -1)
                return self.errorLocation

            @property
            def get_config_spec(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]
                if self.configSpec is not None and not isinstance(self.configSpec,
                                                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.get_config_spec type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]".format(
                            type_name=type(self.configSpec)), -1)
                return self.configSpec

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_error_location(self,
                                   error_location
                                   # type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]
                                   ):
                # type: (...) -> None
                if error_location is not None and not isinstance(error_location,
                                                                 RestApi.Admin.ConfigTemplate.ErrorLocation):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.set_error_location type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ErrorLocation]]".format(
                            type_name=type(error_location)), -1)
                self.errorLocation = error_location

            def set_config_spec(self,
                                config_spec
                                # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]
                                ):
                # type: (...) -> None
                if config_spec is not None and not isinstance(config_spec,
                                                              RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SaveConfigTemplateResponse.set_config_spec type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]]".format(
                            type_name=type(config_spec)), -1)
                self.configSpec = config_spec

        class DeleteConfigTemplateRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.configSpec = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]

            def __call__(self,
                         configSpec,  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.DeleteConfigTemplateRequest]
                if isinstance(configSpec, dict):
                    configSpec = RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec()(**configSpec)
                self.set_config_spec(configSpec)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_config_spec(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                if not isinstance(self.configSpec, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateRequest.get_config_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]".format(
                            type_name=type(self.configSpec)), -1)
                return self.configSpec

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_config_spec(self,
                                config_spec
                                # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]
                                ):
                # type: (...) -> None
                if not isinstance(config_spec, RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateRequest.set_config_spec type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplateSpec]".format(
                            type_name=type(config_spec)), -1)
                self.configSpec = config_spec

        class DeleteConfigTemplateResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.DeleteConfigTemplateResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.DeleteConfigTemplateResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class TestConfigSpecificationRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.configSpecification = None  # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                self.moduleId = None  # type: str
                self.subType = None  # type: Optional[str]
                self.testType = None  # type: Optional[str]

            def __call__(self,
                         configSpecification,
                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                         moduleId,  # type: str
                         subType=None,  # type: Optional[str]
                         testType=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.TestConfigSpecificationRequest]
                if isinstance(configSpecification, dict):
                    configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                        **configSpecification)
                self.set_config_specification(configSpecification)
                self.set_module_id(moduleId)
                self.set_sub_type(subType)
                self.set_test_type(testType)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_config_specification(self):
                # type: () -> Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                if not isinstance(self.configSpecification,
                                  RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            @property
            def get_module_id(self):
                # type: () -> str
                if not isinstance(self.moduleId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.get_module_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.moduleId)), -1)
                return self.moduleId

            @property
            def get_sub_type(self):
                # type: () -> Optional[str]
                if self.subType is not None and not isinstance(self.subType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.get_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.subType)), -1)
                return self.subType

            @property
            def get_test_type(self):
                # type: () -> Optional[str]
                if self.testType is not None and not isinstance(self.testType, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.get_test_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.testType)), -1)
                return self.testType

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_config_specification(self,
                                         config_specification
                                         # type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]
                                         ):
                # type: (...) -> None
                if not isinstance(config_specification, RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

            def set_module_id(self,
                              module_id  # type: str
                              ):
                # type: (...) -> None
                if not isinstance(module_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.set_module_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(module_id)), -1)
                self.moduleId = module_id

            def set_sub_type(self,
                             sub_type  # type: Optional[str]
                             ):
                # type: (...) -> None
                if sub_type is not None and not isinstance(sub_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.set_sub_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(sub_type)), -1)
                self.subType = sub_type

            def set_test_type(self,
                              test_type  # type: Optional[str]
                              ):
                # type: (...) -> None
                if test_type is not None and not isinstance(test_type, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationRequest.set_test_type type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(test_type)), -1)
                self.testType = test_type

        class TestConfigSpecificationResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool
                self.message = None  # type: str
                self.configSpecification = None  # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]

            def __call__(self,
                         rc,  # type: bool
                         message,  # type: str
                         configSpecification=None,
                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.TestConfigSpecificationResponse]
                self.set_rc(rc)
                self.set_message(message)
                if configSpecification is not None:
                    if isinstance(configSpecification, dict):
                        configSpecification = RestApi.Admin.ConfigTemplate.ConfigurationTemplate()(
                            **configSpecification)
                self.set_config_specification(configSpecification)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            @property
            def get_message(self):
                # type: () -> str
                if not isinstance(self.message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.get_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.message)), -1)
                return self.message

            @property
            def get_config_specification(self):
                # type: () -> Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                if self.configSpecification is not None and not isinstance(self.configSpecification,
                                                                           RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.get_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(self.configSpecification)), -1)
                return self.configSpecification

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

            def set_message(self,
                            message  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(message, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.set_message type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(message)), -1)
                self.message = message

            def set_config_specification(self,
                                         config_specification
                                         # type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]
                                         ):
                # type: (...) -> None
                if config_specification is not None and not isinstance(config_specification,
                                                                       RestApi.Admin.ConfigTemplate.ConfigurationTemplate):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.TestConfigSpecificationResponse.set_config_specification type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.Admin.ConfigTemplate.ConfigurationTemplate]]".format(
                            type_name=type(config_specification)), -1)
                self.configSpecification = config_specification

        class GetServicesRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetServicesRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetServicesRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetServicesRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class Service(object):
            def __init__(self):
                # type: () -> None
                self.name = None  # type: str
                self.title = None  # type: str
                self.description = None  # type: str
                self.isInstalled = None  # type: bool
                self.isCurrentVersion = None  # type: bool
                self.isRunning = None  # type: bool

            def __call__(self,
                         name,  # type: str
                         title,  # type: str
                         description,  # type: str
                         isInstalled,  # type: bool
                         isCurrentVersion,  # type: bool
                         isRunning,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.Service]
                self.set_name(name)
                self.set_title(title)
                self.set_description(description)
                self.set_is_installed(isInstalled)
                self.set_is_current_version(isCurrentVersion)
                self.set_is_running(isRunning)
                return self

            @property
            def get_name(self):
                # type: () -> str
                if not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_title(self):
                # type: () -> str
                if not isinstance(self.title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.title)), -1)
                return self.title

            @property
            def get_description(self):
                # type: () -> str
                if not isinstance(self.description, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.description)), -1)
                return self.description

            @property
            def get_is_installed(self):
                # type: () -> bool
                if not isinstance(self.isInstalled, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_is_installed type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isInstalled)), -1)
                return self.isInstalled

            @property
            def get_is_current_version(self):
                # type: () -> bool
                if not isinstance(self.isCurrentVersion, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_is_current_version type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isCurrentVersion)), -1)
                return self.isCurrentVersion

            @property
            def get_is_running(self):
                # type: () -> bool
                if not isinstance(self.isRunning, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.get_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isRunning)), -1)
                return self.isRunning

            def set_name(self,
                         name  # type: str
                         ):
                # type: (...) -> None
                if not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_title(self,
                          title  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(title)), -1)
                self.title = title

            def set_description(self,
                                description  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(description, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_description type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(description)), -1)
                self.description = description

            def set_is_installed(self,
                                 is_installed  # type: bool
                                 ):
                # type: (...) -> None
                if not isinstance(is_installed, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_is_installed type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_installed)), -1)
                self.isInstalled = is_installed

            def set_is_current_version(self,
                                       is_current_version  # type: bool
                                       ):
                # type: (...) -> None
                if not isinstance(is_current_version, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_is_current_version type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_current_version)), -1)
                self.isCurrentVersion = is_current_version

            def set_is_running(self,
                               is_running  # type: bool
                               ):
                # type: (...) -> None
                if not isinstance(is_running, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Service.set_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_running)), -1)
                self.isRunning = is_running

        class GetServicesResponse(object):
            def __init__(self):
                # type: () -> None
                self.services = list()  # type: Optional[List[Type[RestApi.ServerConfig.Service]]]

            def __call__(self,
                         services=None,  # type: Optional[List[Type[RestApi.ServerConfig.Service]]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetServicesResponse]
                if services is not None:
                    if isinstance(services, list):
                        for index, item in enumerate(services):
                            if isinstance(item, dict):
                                services[index] = RestApi.ServerConfig.Service()(**item)
                    else:
                        raise TypeError(
                            "RestApi.ServerConfig.GetServicesResponse.__call__ services type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(services)))
                self.set_services(services)
                return self

            @property
            def get_services(self):
                # type: () -> Optional[List[Type[RestApi.ServerConfig.Service]]]
                if self.services is not None and not (isinstance(self.services, list) and all(
                    isinstance(item, RestApi.ServerConfig.Service) for item in self.services)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetServicesResponse.get_services type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.ServerConfig.Service]]]".format(
                            type_name=type(self.services)), -1)
                if self.services is None:
                    return list()
                else:
                    return self.services

            def set_services(self,
                             services  # type: Optional[List[Type[RestApi.ServerConfig.Service]]]
                             ):
                # type: (...) -> None
                if services is not None and not (isinstance(services, list) and all(
                    isinstance(item, RestApi.ServerConfig.Service) for item in services)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetServicesResponse.set_services type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.ServerConfig.Service]]]".format(
                            type_name=type(services)), -1)
                if services is None:
                    self.services = list()
                else:
                    self.services = services

        class GetGOnSystemsRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetGOnSystemsRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetGOnSystemsRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetGOnSystemsRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GOnSystem(object):
            def __init__(self):
                # type: () -> None
                self.name = None  # type: str
                self.title = None  # type: str
                self.version = None  # type: str
                self.managementIsRunning = None  # type: bool
                self.gatewayIsRunning = None  # type: bool
                self.isCurrentVersion = None  # type: bool
                self.canBeUsedForUpgrade = None  # type: bool
                self.defaultForUpgrade = None  # type: bool
                self.isCurrentInstallation = None  # type: bool
                self.isInstallation = None  # type: bool

            def __call__(self,
                         name,  # type: str
                         title,  # type: str
                         version,  # type: str
                         managementIsRunning,  # type: bool
                         gatewayIsRunning,  # type: bool
                         isCurrentVersion,  # type: bool
                         canBeUsedForUpgrade,  # type: bool
                         defaultForUpgrade,  # type: bool
                         isCurrentInstallation,  # type: bool
                         isInstallation,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GOnSystem]
                self.set_name(name)
                self.set_title(title)
                self.set_version(version)
                self.set_management_is_running(managementIsRunning)
                self.set_gateway_is_running(gatewayIsRunning)
                self.set_is_current_version(isCurrentVersion)
                self.set_can_be_used_for_upgrade(canBeUsedForUpgrade)
                self.set_default_for_upgrade(defaultForUpgrade)
                self.set_is_current_installation(isCurrentInstallation)
                self.set_is_installation(isInstallation)
                return self

            @property
            def get_name(self):
                # type: () -> str
                if not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_title(self):
                # type: () -> str
                if not isinstance(self.title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.title)), -1)
                return self.title

            @property
            def get_version(self):
                # type: () -> str
                if not isinstance(self.version, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.version)), -1)
                return self.version

            @property
            def get_management_is_running(self):
                # type: () -> bool
                if not isinstance(self.managementIsRunning, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_management_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.managementIsRunning)), -1)
                return self.managementIsRunning

            @property
            def get_gateway_is_running(self):
                # type: () -> bool
                if not isinstance(self.gatewayIsRunning, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_gateway_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.gatewayIsRunning)), -1)
                return self.gatewayIsRunning

            @property
            def get_is_current_version(self):
                # type: () -> bool
                if not isinstance(self.isCurrentVersion, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_is_current_version type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isCurrentVersion)), -1)
                return self.isCurrentVersion

            @property
            def get_can_be_used_for_upgrade(self):
                # type: () -> bool
                if not isinstance(self.canBeUsedForUpgrade, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_can_be_used_for_upgrade type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.canBeUsedForUpgrade)), -1)
                return self.canBeUsedForUpgrade

            @property
            def get_default_for_upgrade(self):
                # type: () -> bool
                if not isinstance(self.defaultForUpgrade, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_default_for_upgrade type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.defaultForUpgrade)), -1)
                return self.defaultForUpgrade

            @property
            def get_is_current_installation(self):
                # type: () -> bool
                if not isinstance(self.isCurrentInstallation, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_is_current_installation type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isCurrentInstallation)), -1)
                return self.isCurrentInstallation

            @property
            def get_is_installation(self):
                # type: () -> bool
                if not isinstance(self.isInstallation, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.get_is_installation type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isInstallation)), -1)
                return self.isInstallation

            def set_name(self,
                         name  # type: str
                         ):
                # type: (...) -> None
                if not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_name type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_title(self,
                          title  # type: str
                          ):
                # type: (...) -> None
                if not isinstance(title, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_title type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(title)), -1)
                self.title = title

            def set_version(self,
                            version  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(version, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_version type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(version)), -1)
                self.version = version

            def set_management_is_running(self,
                                          management_is_running  # type: bool
                                          ):
                # type: (...) -> None
                if not isinstance(management_is_running, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_management_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(management_is_running)), -1)
                self.managementIsRunning = management_is_running

            def set_gateway_is_running(self,
                                       gateway_is_running  # type: bool
                                       ):
                # type: (...) -> None
                if not isinstance(gateway_is_running, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_gateway_is_running type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(gateway_is_running)), -1)
                self.gatewayIsRunning = gateway_is_running

            def set_is_current_version(self,
                                       is_current_version  # type: bool
                                       ):
                # type: (...) -> None
                if not isinstance(is_current_version, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_is_current_version type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_current_version)), -1)
                self.isCurrentVersion = is_current_version

            def set_can_be_used_for_upgrade(self,
                                            can_be_used_for_upgrade  # type: bool
                                            ):
                # type: (...) -> None
                if not isinstance(can_be_used_for_upgrade, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_can_be_used_for_upgrade type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(can_be_used_for_upgrade)), -1)
                self.canBeUsedForUpgrade = can_be_used_for_upgrade

            def set_default_for_upgrade(self,
                                        default_for_upgrade  # type: bool
                                        ):
                # type: (...) -> None
                if not isinstance(default_for_upgrade, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_default_for_upgrade type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(default_for_upgrade)), -1)
                self.defaultForUpgrade = default_for_upgrade

            def set_is_current_installation(self,
                                            is_current_installation  # type: bool
                                            ):
                # type: (...) -> None
                if not isinstance(is_current_installation, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_is_current_installation type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_current_installation)), -1)
                self.isCurrentInstallation = is_current_installation

            def set_is_installation(self,
                                    is_installation  # type: bool
                                    ):
                # type: (...) -> None
                if not isinstance(is_installation, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnSystem.set_is_installation type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_installation)), -1)
                self.isInstallation = is_installation

        class GetGOnSystemsResponse(object):
            def __init__(self):
                # type: () -> None
                self.gonSystems = list()  # type: Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]

            def __call__(self,
                         gonSystems=None,  # type: Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetGOnSystemsResponse]
                if gonSystems is not None:
                    if isinstance(gonSystems, list):
                        for index, item in enumerate(gonSystems):
                            if isinstance(item, dict):
                                gonSystems[index] = RestApi.ServerConfig.GOnSystem()(**item)
                    else:
                        raise TypeError(
                            "RestApi.ServerConfig.GetGOnSystemsResponse.__call__ gonSystems type {type_name} - erroneous type. Expected type: list".format(
                                type_name=type(gonSystems)))
                self.set_gon_systems(gonSystems)
                return self

            @property
            def get_gon_systems(self):
                # type: () -> Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]
                if self.gonSystems is not None and not (isinstance(self.gonSystems, list) and all(
                    isinstance(item, RestApi.ServerConfig.GOnSystem) for item in self.gonSystems)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetGOnSystemsResponse.get_gon_systems type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]".format(
                            type_name=type(self.gonSystems)), -1)
                if self.gonSystems is None:
                    return list()
                else:
                    return self.gonSystems

            def set_gon_systems(self,
                                gon_systems  # type: Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]
                                ):
                # type: (...) -> None
                if gon_systems is not None and not (isinstance(gon_systems, list) and all(
                    isinstance(item, RestApi.ServerConfig.GOnSystem) for item in gon_systems)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetGOnSystemsResponse.set_gon_systems type {type_name} - erroneous type/subtype. Expected type: Optional[List[Type[RestApi.ServerConfig.GOnSystem]]]".format(
                            type_name=type(gon_systems)), -1)
                if gon_systems is None:
                    self.gonSystems = list()
                else:
                    self.gonSystems = gon_systems

        class GetLicenseInfoRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GetLicenseInfoRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetLicenseInfoRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetLicenseInfoRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class Date(object):
            def __init__(self):
                # type: () -> None
                self.yyyy = None  # type: int
                self.mm = None  # type: int
                self.dd = None  # type: int

            def __call__(self,
                         yyyy,  # type: int
                         mm,  # type: int
                         dd,  # type: int
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.Date]
                self.set_yyyy(yyyy)
                self.set_mm(mm)
                self.set_dd(dd)
                return self

            @property
            def get_yyyy(self):
                # type: () -> int
                if not isinstance(self.yyyy, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.get_yyyy type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.yyyy)), -1)
                return self.yyyy

            @property
            def get_mm(self):
                # type: () -> int
                if not isinstance(self.mm, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.get_mm type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.mm)), -1)
                return self.mm

            @property
            def get_dd(self):
                # type: () -> int
                if not isinstance(self.dd, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.get_dd type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(self.dd)), -1)
                return self.dd

            def set_yyyy(self,
                         yyyy  # type: int
                         ):
                # type: (...) -> None
                if not isinstance(yyyy, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.set_yyyy type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(yyyy)), -1)
                self.yyyy = yyyy

            def set_mm(self,
                       mm  # type: int
                       ):
                # type: (...) -> None
                if not isinstance(mm, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.set_mm type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(mm)), -1)
                self.mm = mm

            def set_dd(self,
                       dd  # type: int
                       ):
                # type: (...) -> None
                if not isinstance(dd, (int, long)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.Date.set_dd type {type_name} - erroneous type/subtype. Expected type: int".format(
                            type_name=type(dd)), -1)
                self.dd = dd

        class LicenseInfo(object):
            def __init__(self):
                # type: () -> None
                self.licensedTo = None  # type: str
                self.licenseNumber = None  # type: str
                self.licenseFileNumber = None  # type: str
                self.licenseExpires = None  # type: Optional[Type[RestApi.ServerConfig.Date]]
                self.maintenanceExpires = None  # type: Optional[Type[RestApi.ServerConfig.Date]]
                self.loginText = list()  # type: Optional[List[str]]
                self.isDefaultLicense = None  # type: bool

            def __call__(self,
                         licensedTo,  # type: str
                         licenseNumber,  # type: str
                         licenseFileNumber,  # type: str
                         isDefaultLicense,  # type: bool
                         licenseExpires=None,  # type: Optional[Type[RestApi.ServerConfig.Date]]
                         maintenanceExpires=None,  # type: Optional[Type[RestApi.ServerConfig.Date]]
                         loginText=None,  # type: Optional[List[str]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.LicenseInfo]
                self.set_licensed_to(licensedTo)
                self.set_license_number(licenseNumber)
                self.set_license_file_number(licenseFileNumber)
                self.set_is_default_license(isDefaultLicense)
                if licenseExpires is not None:
                    if isinstance(licenseExpires, dict):
                        licenseExpires = RestApi.ServerConfig.Date()(**licenseExpires)
                self.set_license_expires(licenseExpires)
                if maintenanceExpires is not None:
                    if isinstance(maintenanceExpires, dict):
                        maintenanceExpires = RestApi.ServerConfig.Date()(**maintenanceExpires)
                self.set_maintenance_expires(maintenanceExpires)
                self.set_login_text(loginText)
                return self

            @property
            def get_licensed_to(self):
                # type: () -> str
                if not isinstance(self.licensedTo, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_licensed_to type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licensedTo)), -1)
                return self.licensedTo

            @property
            def get_license_number(self):
                # type: () -> str
                if not isinstance(self.licenseNumber, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_license_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licenseNumber)), -1)
                return self.licenseNumber

            @property
            def get_license_file_number(self):
                # type: () -> str
                if not isinstance(self.licenseFileNumber, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_license_file_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.licenseFileNumber)), -1)
                return self.licenseFileNumber

            @property
            def get_license_expires(self):
                # type: () -> Optional[Type[RestApi.ServerConfig.Date]]
                if self.licenseExpires is not None and not isinstance(self.licenseExpires,
                                                                      RestApi.ServerConfig.Date):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_license_expires type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.Date]]".format(
                            type_name=type(self.licenseExpires)), -1)
                return self.licenseExpires

            @property
            def get_maintenance_expires(self):
                # type: () -> Optional[Type[RestApi.ServerConfig.Date]]
                if self.maintenanceExpires is not None and not isinstance(self.maintenanceExpires,
                                                                          RestApi.ServerConfig.Date):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_maintenance_expires type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.Date]]".format(
                            type_name=type(self.maintenanceExpires)), -1)
                return self.maintenanceExpires

            @property
            def get_login_text(self):
                # type: () -> Optional[List[str]]
                if self.loginText is not None and not (
                    isinstance(self.loginText, list) and all(isinstance(item, basestring) for item in self.loginText)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_login_text type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(self.loginText)), -1)
                if self.loginText is None:
                    return list()
                else:
                    return self.loginText

            @property
            def get_is_default_license(self):
                # type: () -> bool
                if not isinstance(self.isDefaultLicense, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.get_is_default_license type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.isDefaultLicense)), -1)
                return self.isDefaultLicense

            def set_licensed_to(self,
                                licensed_to  # type: str
                                ):
                # type: (...) -> None
                if not isinstance(licensed_to, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_licensed_to type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(licensed_to)), -1)
                self.licensedTo = licensed_to

            def set_license_number(self,
                                   license_number  # type: str
                                   ):
                # type: (...) -> None
                if not isinstance(license_number, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_license_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license_number)), -1)
                self.licenseNumber = license_number

            def set_license_file_number(self,
                                        license_file_number  # type: str
                                        ):
                # type: (...) -> None
                if not isinstance(license_file_number, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_license_file_number type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license_file_number)), -1)
                self.licenseFileNumber = license_file_number

            def set_license_expires(self,
                                    license_expires  # type: Optional[Type[RestApi.ServerConfig.Date]]
                                    ):
                # type: (...) -> None
                if license_expires is not None and not isinstance(license_expires,
                                                                  RestApi.ServerConfig.Date):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_license_expires type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.Date]]".format(
                            type_name=type(license_expires)), -1)
                self.licenseExpires = license_expires

            def set_maintenance_expires(self,
                                        maintenance_expires
                                        # type: Optional[Type[RestApi.ServerConfig.Date]]
                                        ):
                # type: (...) -> None
                if maintenance_expires is not None and not isinstance(maintenance_expires,
                                                                      RestApi.ServerConfig.Date):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_maintenance_expires type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.Date]]".format(
                            type_name=type(maintenance_expires)), -1)
                self.maintenanceExpires = maintenance_expires

            def set_login_text(self,
                               login_text  # type: Optional[List[str]]
                               ):
                # type: (...) -> None
                if login_text is not None and not (
                    isinstance(login_text, list) and all(isinstance(item, basestring) for item in login_text)):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_login_text type {type_name} - erroneous type/subtype. Expected type: Optional[List[str]]".format(
                            type_name=type(login_text)), -1)
                if login_text is None:
                    self.loginText = list()
                else:
                    self.loginText = login_text

            def set_is_default_license(self,
                                       is_default_license  # type: bool
                                       ):
                # type: (...) -> None
                if not isinstance(is_default_license, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.LicenseInfo.set_is_default_license type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(is_default_license)), -1)
                self.isDefaultLicense = is_default_license

        class GetLicenseInfoResponse(object):
            def __init__(self):
                # type: () -> None
                self.licenseInfo = None  # type: Optional[Type[RestApi.ServerConfig.LicenseInfo]]

            def __call__(self,
                         licenseInfo=None,  # type: Optional[Type[RestApi.ServerConfig.LicenseInfo]]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GetLicenseInfoResponse]
                if licenseInfo is not None:
                    if isinstance(licenseInfo, dict):
                        licenseInfo = RestApi.ServerConfig.LicenseInfo()(**licenseInfo)
                self.set_license_info(licenseInfo)
                return self

            @property
            def get_license_info(self):
                # type: () -> Optional[Type[RestApi.ServerConfig.LicenseInfo]]
                if self.licenseInfo is not None and not isinstance(self.licenseInfo,
                                                                   RestApi.ServerConfig.LicenseInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetLicenseInfoResponse.get_license_info type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.LicenseInfo]]".format(
                            type_name=type(self.licenseInfo)), -1)
                return self.licenseInfo

            def set_license_info(self,
                                 license_info  # type: Optional[Type[RestApi.ServerConfig.LicenseInfo]]
                                 ):
                # type: (...) -> None
                if license_info is not None and not isinstance(license_info,
                                                               RestApi.ServerConfig.LicenseInfo):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GetLicenseInfoResponse.set_license_info type {type_name} - erroneous type/subtype. Expected type: Optional[Type[RestApi.ServerConfig.LicenseInfo]]".format(
                            type_name=type(license_info)), -1)
                self.licenseInfo = license_info

        class SetLicenseRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.license = None  # type: str

            def __call__(self,
                         license,  # type: str
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SetLicenseRequest]
                self.set_license(license)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_license(self):
                # type: () -> str
                if not isinstance(self.license, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseRequest.get_license type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.license)), -1)
                return self.license

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_license(self,
                            license  # type: str
                            ):
                # type: (...) -> None
                if not isinstance(license, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseRequest.set_license type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(license)), -1)
                self.license = license

        class SetLicenseResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.SetLicenseResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.SetLicenseResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class PingRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: Optional[str]

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.PingRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> Optional[str]
                if self.sessionId is not None and not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.PingRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: Optional[str]
                               ):
                # type: (...) -> None
                if session_id is not None and not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.PingRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class PingResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.PingResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.PingResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.PingResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc

        class GOnManagementServiceGetStatusRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str

            def __call__(self
                         ):
                # type: () -> Type[RestApi.ServerConfig.GOnManagementServiceGetStatusRequest]
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

        class GOnManagementServiceGetStatusResponse(object):
            def __init__(self):
                # type: () -> None
                self.id_ = None  # type: Optional[str]
                self.name = None  # type: Optional[str]
                self.status = None  # type: Optional[str]

            def __call__(self,
                         id_=None,  # type: Optional[str]
                         name=None,  # type: Optional[str]
                         status=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GOnManagementServiceGetStatusResponse]
                self.set_id(id_)
                self.set_name(name)
                self.set_status(status)
                return self

            @property
            def get_id(self):
                # type: () -> Optional[str]
                if self.id_ is not None and not isinstance(self.id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.get_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.id_)), -1)
                return self.id_

            @property
            def get_name(self):
                # type: () -> Optional[str]
                if self.name is not None and not isinstance(self.name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.get_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.name)), -1)
                return self.name

            @property
            def get_status(self):
                # type: () -> Optional[str]
                if self.status is not None and not isinstance(self.status, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.get_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.status)), -1)
                return self.status

            def set_id(self,
                       id_  # type: Optional[str]
                       ):
                # type: (...) -> None
                if id_ is not None and not isinstance(id_, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.set_id type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(id_)), -1)
                self.id_ = id_

            def set_name(self,
                         name  # type: Optional[str]
                         ):
                # type: (...) -> None
                if name is not None and not isinstance(name, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.set_name type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(name)), -1)
                self.name = name

            def set_status(self,
                           status  # type: Optional[str]
                           ):
                # type: (...) -> None
                if status is not None and not isinstance(status, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceGetStatusResponse.set_status type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(status)), -1)
                self.status = status

        class GOnManagementServiceControlRequest(object):
            def __init__(self):
                # type: () -> None
                self.sessionId = None  # type: str
                self.command = None  # type: Optional[str]

            def __call__(self,
                         command=None,  # type: Optional[str]
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GOnManagementServiceControlRequest]
                self.set_command(command)
                return self

            @property
            def get_session_id(self):
                # type: () -> str
                if not isinstance(self.sessionId, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlRequest.get_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(self.sessionId)), -1)
                return self.sessionId

            @property
            def get_command(self):
                # type: () -> Optional[str]
                if self.command is not None and not isinstance(self.command, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlRequest.get_command type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(self.command)), -1)
                return self.command

            def set_session_id(self,
                               session_id  # type: str
                               ):
                # type: (...) -> None
                if not isinstance(session_id, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlRequest.set_session_id type {type_name} - erroneous type/subtype. Expected type: str".format(
                            type_name=type(session_id)), -1)
                self.sessionId = session_id

            def set_command(self,
                            command  # type: Optional[str]
                            ):
                # type: (...) -> None
                if command is not None and not isinstance(command, basestring):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlRequest.set_command type {type_name} - erroneous type/subtype. Expected type: Optional[str]".format(
                            type_name=type(command)), -1)
                self.command = command

        class GOnManagementServiceControlResponse(object):
            def __init__(self):
                # type: () -> None
                self.rc = None  # type: bool

            def __call__(self,
                         rc,  # type: bool
                         ):
                # type: (...) -> Type[RestApi.ServerConfig.GOnManagementServiceControlResponse]
                self.set_rc(rc)
                return self

            @property
            def get_rc(self):
                # type: () -> bool
                if not isinstance(self.rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlResponse.get_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(self.rc)), -1)
                return self.rc

            def set_rc(self,
                       rc  # type: bool
                       ):
                # type: (...) -> None
                if not isinstance(rc, bool):
                    raise RestApi.Admin.FaultServerElement()(
                        "RestApi.ServerConfig.GOnManagementServiceControlResponse.set_rc type {type_name} - erroneous type/subtype. Expected type: bool".format(
                            type_name=type(rc)), -1)
                self.rc = rc
