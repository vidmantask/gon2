import json
import requests


class HttpRequestService(object):
    def __init__(self):
        self.session = None
        self.base_url = None
        self.timeout = None
        self.allow_redirects = None
        self.headers = dict()

    def __del__(self):
        self.session = None

    def init(self, base_url, timeout, allow_redirects, headers):
        self.session = requests.Session()
        self.set_base_url(base_url)
        self.set_timeout(timeout)
        self.set_allow_redirects(allow_redirects)
        if headers is None:
            self.set_default_headers()
        else:
            self.set_headers(headers)

    def post(self, url, data=None):
        if data is not None:
            json_data = json.dumps(data, default=lambda o: o.__dict__, sort_keys=False, indent=4)
        else:
            json_data = None
        response = self.session.post(self.base_url + url,
                                     data=json_data,
                                     headers=self.headers, timeout=self.timeout,
                                     allow_redirects=self.allow_redirects)
        response.raise_for_status()
        return response.json()

    @property
    def get_base_url(self):
        return self.base_url

    def set_base_url(self, base_url):
        self.base_url = base_url

    @property
    def get_timeout(self):
        return self.timeout

    def set_timeout(self, timeout):
        self.timeout = timeout

    @property
    def get_allow_redirects(self):
        return self.allow_redirects

    def set_allow_redirects(self, allow_redirects):
        self.allow_redirects = allow_redirects

    @property
    def get_headers(self):
        return self.headers

    def set_headers(self, headers):
        if headers is not None:
            self.headers = headers
        else:
            self.headers = dict()

    def set_header(self, name, value):
        if value is not None:
            if "Authorization" == name:
                self.headers.update({name: "Bearer " + value})
            else:
                self.headers.update({name: value})
        else:
            self.headers.pop(name, None)

    def set_default_headers(self):
        self.set_header("Content-Type", "application/json")

    def set_authorization_header(self, token):
        self.set_header("Authorization", token)
