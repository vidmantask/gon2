"""
API for user interfaces.

-----------------------------------------------

Create a UserInterface object. This will give
you all the elements of a user interface. The
elements can be manipulated with the methods
listed below. Subscription lets a method be
notified of any changes to that element.

ui.start makes the gui enter a run loop. So
this should be the last thing you call in a
thread. ui.destroy should clean up all gui
parts.

Examples:

The menu has a topfolder with the id -1. So to
add an item to the top folder, set the destination
folder id to -1.

Selected menu items are added to a launchlist.
To be notified of changes in the launchlist
subscribe to the menu.

A message to the user can be set with the method
set_message(headline, message). In order for the
message to actually be displayed, you should call
display() on the message.

-----------------------------------------------

ui = UserInterface()

ui.login.set_greeting(greeting)
ui.login.set_credentials(username, password)
ui.login.clear_credentials()
ui.login.get_credentials()
ui.login.display()
ui.login.subscribe(subscriber)

ui.menu.add_item(destinationfolderid, id, text, icon, enabled)
ui.menu.add_folder(destinationfolderid, id, text, icon)
ui.menu.display()
ui.menu.subscribe(subscriber)
ui.menu.get_launch_list()
ui.menu.add_to_launch_list(id)
ui.menu.remove_from_launch_list(id)

ui.message.set_message(headline, message)
ui.message.display()
ui.message.subscribe(subscriber)

ui.start()
ui.destroy()

"""
import lib.dictionary
from threading import Timer
import traceback

from components.presentation.common import CommonModel
from components.presentation.login import LoginModel
from components.presentation.message import MessageModel
from components.presentation.menu import MenuModel
from components.presentation.update import UpdateModel
from components.presentation.modal_message import ModalMessageModel
from components.presentation.splash import SplashModel

import sys

if sys.platform == 'win32':
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.gui.mfc.mfc_login_view import MfcLoginView
    from components.presentation.gui.mfc.mfc_menu_view import MfcMenuView
    from components.presentation.gui.mfc.mfc_message_view import MfcMessageView
    from components.presentation.gui.mfc.mfc_update_view import MfcUpdateView
    from components.presentation.gui.mfc.mfc_modal_message_view import MfcModalMessageView
    from components.presentation.gui.mfc.mfc_splash_view import MfcSplashView
elif sys.platform == 'darwin':
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from components.presentation.gui.cocoa.cocoa_login_view import CocoaLoginView
    from components.presentation.gui.cocoa.cocoa_menu_view import CocoaMenuView
    from components.presentation.gui.cocoa.cocoa_message_view import CocoaMessageView
    from components.presentation.gui.cocoa.cocoa_update_view import CocoaUpdateView
    from components.presentation.gui.cocoa.cocoa_modal_message_view import CocoaModalMessageView
    from components.presentation.gui.cocoa.cocoa_splash_view import CocoaSplashView
elif sys.platform == 'linux2':
    try:
        from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
        from components.presentation.gui.gtk.gtk_login_view import GtkLoginView
        from components.presentation.gui.gtk.gtk_menu_view import GtkMenuView
        from components.presentation.gui.gtk.gtk_message_view import GtkMessageView
        from components.presentation.gui.gtk.gtk_update_view import GtkUpdateView
        from components.presentation.gui.gtk.gtk_modal_message_view import GtkModalMessageView
        from components.presentation.gui.gtk.gtk_splash_view import GtkSplashView
    except:
        traceback.print_exc()
        print "ERROR unable to load GTK Gui"

elif sys.platform == 'linux2-app': # Remove '-app' to use GTK APP
    from components.presentation.gui.gtk_app.gtk_app_common_view import GtkAppCommonView
    from components.presentation.gui.gtk_app.gtk_app_login_view import GtkAppLoginView
    from components.presentation.gui.gtk_app.gtk_app_menu_view import GtkAppMenuView
    #from components.presentation.gui.gtk_app.gtk_app_menu_view import GtkAppMenuViewEmpty
    from components.presentation.gui.gtk_app.gtk_app_message_view import GtkAppMessageView

from components.presentation.gui.simulator.simulator_common_view import SimulatorCommonView
from components.presentation.gui.simulator.simulator_login_view import SimulatorLoginView
from components.presentation.gui.simulator.simulator_menu_view import SimulatorMenuView
from components.presentation.gui.simulator.simulator_message_view import SimulatorMessageView
from components.presentation.gui.simulator.simulator_splash_view import SimulatorSplashView
from components.presentation.gui.simulator.simulator_modal_message_view import SimulatorModalMessageView
from components.presentation.gui.simulator.simulator_update_view import SimulatorUpdateView


class UserInterface(object): # Do not warn for unimplemented interface, pylint: disable=R0923

    VIEW_TYPE_AUTO = 0
    VIEW_TYPE_COCOA = 1
    VIEW_TYPE_MFC = 2
    VIEW_TYPE_GTK = 3
    VIEW_TYPE_GTK_APP = 4
    VIEW_TYPE_SIMULATOR = 5

    def __init__(self, view_type=VIEW_TYPE_AUTO, configuration=None, dictionary=None):

        self.dictionary = dictionary
        self._cb_close = None
        self.configuration = configuration
        self.configuration.dictionary = dictionary
        self._views = []

        # Detect GUI type.
        if view_type == UserInterface.VIEW_TYPE_AUTO:
            view_type = self.detect_view_type()

        # Create models.
        _models = (CommonModel(), LoginModel(self.dictionary), MessageModel(), MenuModel(self.dictionary), MenuModel(self.dictionary), UpdateModel(self.dictionary), ModalMessageModel(self.dictionary), SplashModel())

        # Create system specific views.
        _handle_labels = ['commonview', 'loginview', 'messageview', 'appmenu', 'sysmenu', 'updateview', 'modalmessageview', 'splashview']
        if view_type == UserInterface.VIEW_TYPE_COCOA:
            _cocoa_views = [CocoaCommonView, CocoaLoginView, CocoaMessageView, CocoaMenuView, CocoaMenuView, CocoaUpdateView, CocoaModalMessageView, CocoaSplashView]
            _views = self.set_views(_models, _cocoa_views, _handle_labels, self.configuration)
        elif view_type == UserInterface.VIEW_TYPE_MFC:
            _mfc_views = [MfcCommonView, MfcLoginView, MfcMessageView, MfcMenuView, MfcMenuView, MfcUpdateView, MfcModalMessageView, MfcSplashView]
            _views = self.set_views(_models, _mfc_views, _handle_labels, self.configuration)
        elif view_type == UserInterface.VIEW_TYPE_GTK:
            _gtk_views = [GtkCommonView, GtkLoginView, GtkMessageView, GtkMenuView, GtkMenuView, GtkUpdateView, GtkModalMessageView, GtkSplashView]
            _views = self.set_views(_models, _gtk_views, _handle_labels, self.configuration)
            # Get GtkWindow based views - we need something to run gtkApp in...
            self._views = _views
        elif view_type == UserInterface.VIEW_TYPE_GTK_APP:
            _gtk_app_views = [GtkAppCommonView, GtkAppLoginView, GtkAppMessageView, GtkAppMenuView, None, None, None, None]
            _views = self.set_views(_models, _gtk_app_views, _handle_labels, self.configuration)
        else:
            _simulator_views = [SimulatorCommonView, SimulatorLoginView, SimulatorMessageView, SimulatorMenuView, SimulatorMenuView, SimulatorUpdateView, SimulatorModalMessageView, SimulatorSplashView]
            _views = self.set_views(_models, _simulator_views, _handle_labels, self.configuration)

        # Set public GUI part controllers.
        self.common = _views[0].controller if _views[0] is not None else None
        self.login = _views[1].controller if _views[1] is not None else None
        self.message = _views[2].controller if _views[2] is not None else None
        self.menu = _views[3].controller if _views[3] is not None else None
        self.sysmenu = _views[4].controller if _views[4] is not None else None
        self.update = _views[5].controller if _views[5] is not None else None
        self.modalmessage = _views[6].controller if _views[6] is not None else None
        self.splash = _views[7].controller if _views[7] is not None else None
        self.controllers = (self.splash, self.modalmessage, self.update, self.sysmenu, self.menu, self.message, self.login, self.common)

        # Gtk overwrites for creating a main view
        #   - We still do create the splash view - it is just never shown
        #
        if view_type == UserInterface.VIEW_TYPE_GTK:
            print "Rewiring for GTK main window app"
            self.splash.set_text = self.common.splash_set_text
            self.splash.display = self.common.splash_display
            self.splash.hide = self.common.splash_clear_text

        self.set_system_shutdown_listener()
        self.set_system_menu_listener()

    def set_system_shutdown_listener(self):
        """ Setup a listener for calling call-back on shutting down. """
        def system_shutdown_listener():
            print "Shutdown listener notified (listens for events on common controller)"
            if self.common.get_shutdown():
                if self._cb_close:
                    self._cb_close()
        self.common.subscribe(system_shutdown_listener)

    def set_system_menu_listener(self):
        """ Setup a systems menu listener (tray right click). """
        def system_menu_listener():
            if self.sysmenu.get_launch_list() != []:
                _value = self.sysmenu.get_launch_list().pop()
                if _value == 1: # Exit selected
                    self.common.set_shutdown()

        if sys.platform in ['win32', 'darwin'] and self.sysmenu.view != None:
            self.sysmenu.subscribe(system_menu_listener)
            self.sysmenu.add_item(-1, 1, self.dictionary._("Exit"), None, True, 1)

    def set_views(self, _models, _view_classes, _labels, _configuration):
        """ Set views with the given models and labels. """
        _views = []
        _handles = None
        for i, view in enumerate(_view_classes):
            if i == 0:
                _common = view(_models[i], None, _labels[i], _configuration)
                _handles = _common.handles
                _views.append(_common)
            else:
                if view is not None:
                    _views.append(view(_models[i], _handles, _labels[i], _configuration))
        return tuple(_views)

    def detect_view_type(self):
        """ Determine the GUI platform. """
        if sys.platform == 'darwin':
            _type = UserInterface.VIEW_TYPE_COCOA
        elif sys.platform == 'win32':
            _type = UserInterface.VIEW_TYPE_MFC
        elif sys.platform == 'linux2':
            _type = UserInterface.VIEW_TYPE_GTK
        elif sys.platform == 'linux2':
            _type = UserInterface.VIEW_TYPE_GTK_APP
        else:
            _type = UserInterface.VIEW_TYPE_SIMULATOR
        return _type

    def set_cb_close(self, cb):
        """ Set the call-back method to be notified on close."""
        self._cb_close = cb

    def reset_cb_close(self):
        """ Reset the call-back method to be notified on close."""
        self._cb_close = None

    def start(self):
        """ Start the GUI run loop. """
        self.common.display(self._views)

    def destroy(self):
        """ Destroy all parts of the GUI and stop GUI run loop. """
        for controller in self.controllers:
            controller.destroy()

    def get_image_path(self):
        if self.configuration is None:
            return "."
        return self.configuration.gui_image_path

# Move this to a test area when appropriate.
#
# Notice that this is not threaded. So the subscribers
# are called before the login window is closed. This makes
# it difficult to open a second login window.
# Also the menu is updated before the login window closes.
# Threading would help here. But for illustrating the
# functionality this will do.
#


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    import time

    def credentials_subscriber():
        """ If correct credentials are given - add menu folders and items. If not - die. """
        credentials = ui.login.get_credentials()

        if credentials == ('', '', None):
            pass

        elif credentials == ('Mogens', 'test', None):
            ui.login.clear_credentials()

            ui.menu.add_folder(-1, 1, "Folder", None)
            ui.menu.add_item(-1, 2, "First Item", None, True, 1001)
            ui.menu.add_item(-1, 3, "Second item", None, True, 1002)
            ui.menu.add_folder(1, 4, "Folder in folder", None)
            ui.menu.add_item(1, 5, "Item in folder", None, True, 1003)
            ui.menu.add_item(1, 6, "Second item in folder", None, True, 1004)
            ui.menu.add_folder(4, 7, "Folder in folder in ...", None)
            ui.menu.add_item(4, 8, "Item in folder in ...", None, True, 1005)
            ui.menu.add_folder(-1, 9, "Folder with icon", "outlook.bmp")
            ui.menu.add_item(-1, 10, "Item with icon", "outlook.bmp", True, 1006)
            ui.menu.add_item(-1, 11, "Disabled item", None, False, 1007)
            ui.menu.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, 1008)

            ui.message.set_message("Welcome to Giritech", "Your menu has been updated")
            ui.message.display()
            ui.splash.set_text("Welcome to Giritech<br>Your menu has been updated")

            # I'm pretty sure there is a call to splash hide from the system - this is just to verify that it works
            Timer(5.0, ui.splash.hide).start() # Hide the splash after 5 secs.

        else:
            ui.login.clear_credentials()
            ui.message.set_message("Your Login was incorrect", "Please go away")
            ui.message.display()
            ui.destroy()

    def menu_subscriber():
        """ See if something should be launched. """
        _list = ui.menu.get_launch_list()
        if _list != []:
            print "Launch list is now: " + str(_list)

    def common_subscriber():
        """ See if the client should be shut down.

            Subscribes to changes in the common model.
            This includes the shutdown variable that is set to
            'True' if something suggested that the client should
            be shut down. This could be the cancel button in the
            login dialog or a system menu item.
        """
        if ui.common.get_shutdown():
            print "Common subscriber was told to shut down!!!"
            time.sleep(2.0)
            ui.destroy()

    __configuration = ClientGatewayOptions()
    __configuration.gui_image_path = 'gui'

    dictionary = lib.dictionary.Dictionary()
    ui = UserInterface(configuration=__configuration, dictionary=dictionary)
    ui.login.set_greeting("Welcome to Giritech")
    ui.login.subscribe(credentials_subscriber)
    ui.login.display()

    ui.menu.subscribe(menu_subscriber)
    ui.common.subscribe(common_subscriber)

    ui.message.set_message("Welcome", "Login to get a menu")
    ui.message.display()

    ui.modalmessage.set_text("Test - The login view is underneath here.")
    ui.modalmessage.display()

    ui.splash.set_text("hejsa")
    ui.splash.display()

    ui.start()
