""" 
The update view consists of a number of areas: 

* A banner area.
    Use set_banner(banner_id) to set set the banner.
    Banner_id can be one of these: 
        ID_BANNER_INSTALL, ID_BANNER_UPDATE, ID_BANNER_REMOVE 
    More banners will be added when they become available.

* A phase list
    Use set_phase_list(phases) to set the phases.
    phases is a structure looking like this:
        phases = [{'name': 'Select', 'state': ID_PHASE_COMPLETE}, 
                  {'name': 'Download', 'state': ID_PHASE_ACTIVE},
                  {'name': 'Install', 'state': ID_PHASE_PENDING}
                 ]
        State can be one of these:
            ID_PHASE_PENDING - displayed as inactive/gray text 
            ID_PHASE_ACTIVE - displayed as active/bold text
            ID_PHASE_COMPLETE - displayed as ordinary text


* An information area, which can be one of these:
 
    Use set_info_mode(mode) to set the mode of the info area.
    Mode can be one of these:
        ID_MODE_INFORMATION_FREE_TEXT' - for displaying free text information 
        ID_MODE_INFORMATION_BUTTON_TEXT' - for displaying button descriptions 
        ID_MODE_INFORMATION_PROGRESS' - for displaying progress information
        
    In each mode there is a headline that is displayed in a big bold type font.
    Use set_info_headline(headline) to set the headline text.
    
    In PROGRESS mode a progress bar is displayed. This progress bar can be set 
    to two modes. If you know exactly how long a job will take use the progress 
    known if not use the progress unknown. 
    
        Use set_info_progress_mode(mode) to set the progress bar mode.
        Mode can be one of these:
            ID_MODE_PROGRESS_KNOWN
            ID_MODE_PROGRESS_UNKNOWN
 
        If the mode is set to progress known a percentage of the progress bar is colored.
        Use set_info_progress_complete(percentage) to indicate the percentage of 
        completed progress.
 
        The progress bar has a line of text underneath it, that can be used to display
        details about the progress.
        Use set_info_progress_subtext(subtext) to set this piece of text.
 
    If the mode is set to FREE_TEXT ordinary text is displayed underneath the headline.
    Use set_info_free_text(text) to set the text under the headline.
 
    The BUTTON_TEXT mode is used for displaying text about what the buttons
    will do if pushed. Using this requires you to have set the button descriptions.
    See the button area section for how to do this. 
 
* A selection list

    The selection list can be hidden if it should not be used. 
    Use set_show_selection_list(state) where state is true or false to indicate 
    whether you want the selection list to be displayed.

    Use set_selection_list(self, list, autosort=True) to set the selection list.
    list is a structure looking like this: 
    list = [ 
            {'id': 0, 'selected': True, 'icon': None, 'name': 'G/On Client', 'details': 'win', 'description': 'Greatest remote solution ever - win style.'},
            {'id': 1, 'selected': False, 'icon': None, 'name': 'G/On Client', 'details': 'mac', 'message': 'Replaces old version 5.1', 'description': 'Greatest remote solution ever - mac style.'}
            ]

            The elements in each selection item has these features:
                id:            Uniquely identifying integer
                selected:      True | False - whether or not this item has a checked checkbox.
                icon:          None | filename - if None is chosen there will be a void in the listing. 
                name:          free text - shown as the upper line of text
                details:       free text - shown as the lower line of text
                description:   free text - currently not shown.

    The selection list supports two ways of selecting items - muliple or single.
    Use set_selection_method(self, method) to chose between the two ways.
    Method can be one of these:
        ID_SELECTION_MULTIPLE - the user can select more than one item.
        ID_SELECTION_SINGLE - the user can select only one item.
        
    If using single selection the highlighted item is the selected one.
    Use get_selection_highlighted() to get an index for which item is currently highlighted.
    Remember to get the models selection list to 
    
    If using multiple selection 
    Use get_selection_list() to get the models list back and check that for selections. 
    Remember that the order of the items in the list may have changed.

    The autosort value indicates a new feature where I'll try to automatically
    sort the elements in the selection list before displaying them. The sort is based 
    on the name field.

* A button area 
    The button area holds a cancel button and a next button.
    
    Use set_cancel_allowed(allowed), where allowed is a boolean, to 
    set whether the cancel button is enabled or not.
    
    Use set_cancel_description(description), where description is a text string, 
    to set the text presented when the info area is in BUTTON_TEXT mode.

    Use get_cancelled_state() to get find out if the user has pushed the cancel
    button. The return value is a boolean.

    Use set_cancel_label(label) to set the text on the cancel button. You can use 
    None to reset the value - to 'cancel'.
    
    Similar methods are available for the next button.

Besides the area based functionality there is a number of methods for supporting 
various other features. 

Use set_window_frame_text(text) to set the text in the windows frame.
Use get_minimized() to find out if the user has minimized the window.

Use set_gui_should_update_on_alerts(state) if you want to make a number of changes
to the model and do not want to alert the GUI for each separate change. State is a 
boolean. When the state is set back to true all pending updates are performed.   

"""

from components.presentation.base.base import ModelBase, ViewBase, ControllerBase
from components.presentation.base.button_area_base import ButtonAreaModelBase, ButtonAreaControllerBase

from copy import deepcopy
from operator import itemgetter

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class UpdateModel(ModelBase, ButtonAreaModelBase):
    def __init__(self, dictionary):
        ModelBase.__init__(self)
        ButtonAreaModelBase.__init__(self, dictionary)

        # General settings
        self.info_mode = None 
        self.gui_update_on_alerts = True
        self.minimized = False
        self.show_selection_list = False
        self.window_frame_text = "G/On"
        # Header area model part
        self.banner_id = -1
        self.banner_image = None
        # Phases information area.
        self.phase_list = []
        # Selection information area.
        self.selection_list = []
        self.selection_method = UpdateController.ID_SELECTION_MULTIPLE
        self.selection_user_highlighted = -1 # Use this for single selections only
        self.selection_highlighted_id = -1 # Use this for single selections only
        # Information area settings.
        self.info_headline = ''
        self.info_text = ''
        self.info_free_text = ''
        self.info_progress_mode = UpdateController.ID_MODE_PROGRESS_KNOWN
        self.info_progress_complete = 0
        self.info_progress_subtext = ''
        #self.info_mode = 0 # 0: button texts, 1: free text
        self.info_user_selected_folder_path = ''
        self.info_user_written_folder_path = ''
        self.info_folder_path = ''


# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class UpdateController(ControllerBase, ButtonAreaControllerBase):

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)
        ButtonAreaControllerBase.__init__(self, model)

    def set_window_frame_text(self, text):
        """ Set the text in the frame of the window.
        
            @param text: The text to be displayed in the window frame. 
            @since: 5.4
        """
        if self.model.window_frame_text != text:
            self.model.window_frame_text = text
            self.model.alert_subscribers()

    def set_minimized(self, value):
        """ Set whether the update view is minimized. 
            
            This method does not alert subscribers. Check this with
            the get_minimized method when you need to know.
            
            @param value: Boolean indicating whether the window is minimized. 
            @since: 5.0
            @note: This method should only be used from GUI side.
        """
        self.model.minimized = value
        
    def get_minimized(self):
        """ Get whether the update view is minimized. 
        
            The minimized value is set if the user has minimized 
            the window.
        
            @return: Boolean indicating whether the window is minimized.
            @since: 5.0
        """
        return self.model.minimized

    def set_show_selection_list(self, state):
        """ Set whether the selection list is shown or not. 
        
            The selection list area can be hidden if it is
            not required. Use this method to do that.
        
            @param state: [True | False] Shown or not
            @since: 5.4
        """
        if self.model.show_selection_list != state:
            self.model.show_selection_list = state
            self.model.alert_subscribers()

    ID_SELECTION_MULTIPLE = 0
    ID_SELECTION_SINGLE = 1

    def set_selection_method(self, method):
        """ Set the way selections are handled.
        
            Selection methods can be single or multiple. 
            Depending on whether one or more check-boxes 
            can be marked at one time.
            
            If multiple selection is chosen you should go through the
            models selection list to find out which items in the list has 
            been selected.
            @see: get_selection_list()

            If single selection is chosen the selection is the currently
            highlighted item.
            @see: get_selection_highlighted()
            
            @param method: ID_SELECTION_MULTIPLE | ID_SELECTION_SINGLE 
            @since: 5.4
        """
        if  self.model.selection_method != method:
            self.model.selection_method = method
            self.model.alert_subscribers()

    ID_BANNER_INSTALL = 0
    ID_BANNER_UPDATE = 1
    ID_BANNER_REMOVE = 2
    ID_BANNER_ENROLL = 3
    ID_BANNER_UNINSTALL = 4

    def set_banner(self, banner_id):
        """ Set the banner to display the banner_file. 
    
            This will set the banner to one of the corresponding
            files named in the tools for each GUI. 

            The banner file should be placed in the 
            images folder for each platforms GUI.
            The banner should be a 450 x 80 bitmap file.
            
            @param banner_id:  ID_BANNER_INSTALL | ID_BANNER_UPDATE | ID_BANNER_REMOVE | ID_BANNER_ENROLL
            @since: 5.4
        """
        if self.model.banner_id != banner_id:
            self.model.banner_id = banner_id
            self.model.alert_subscribers()

    ID_PHASE_COMPLETE = 'complete'
    ID_PHASE_ACTIVE = 'active'
    ID_PHASE_PENDING = 'pending' 

    def set_phase_list(self, phases):
        """ Set the phases in the phase information area. 
        
            This is list of dictionaries containing info on each phase. Each dictionary in the 
            list has a name and a state. The name is displayed and the state determines how. 
            
            There can be at most 5 phases. There may be less than that. The phases are presented 
            in the order listed.
            
            @param phases: A list of dictionaries of the following form:
                phases = [{'name': 'Select', 'state': ID_PHASE_COMPLETE}, 
                          {'name': 'Download', 'state': ID_PHASE_ACTIVE},
                          {'name': 'Install', 'state': ID_PHASE_PENDING}]
            @since: 5.0 
        """
        update_me_please = False
        # Length has changed - update...
        if len(self.model.phase_list) != len(phases):
            update_me_please = True
        # Content has changed - update ...
        else:
            for _index in range(0, len(phases)):
                if self.model.phase_list[_index]['name'] != phases[_index]['name']:
                    update_me_please = True
                if self.model.phase_list[_index]['state'] != phases[_index]['state']:
                    update_me_please = True
        # If we must, we must...
        if update_me_please:
            self.model.phase_list = deepcopy(phases)
            self.model.alert_subscribers()

    def set_selection_via_index(self, index, value):
        """ Change check setting for a selection item.

            Used for responding to user clicks in check-boxes.
            
            @param index: Integer indicating the position in the selection list.
            @param value: Boolean indicating whether the item should be selected or not. 
            @note: This method should only be used from GUI side.
            @since: 5.0
        """
        self.model.selection_list[index]['selected'] = value
        self.model.alert_subscribers()

    def get_selection_list(self):
        """ Get the current selection list. 
        
            Use this method to get a copy of the models selection list. The models
            selection list should reflect the GUI at any time. If you need to know which 
            items are selected in the list, use this method and go through the list. 
        
            @return: The models current selection list
            @since: 5.0
        """
        return self.model.selection_list

    def set_selection_list(self, selectionlist, autosort=True):
        """ Set the list of possible selections in the selections area. 
            
            The list parameter is a list of dictionaries, where each dictionary
            contains the following values: An identifier ('id') that should be unique.
            A selection state ('selected') that is a boolean. An icon field ('icon') that 
            is either None or a filename. And three fields ('name', 'details' and 'description')
            which are strings. 
           
            @param list: A list of dictionaries of the following form:
                list = [{'id': 0, 
                         'selected': True, 
                         'icon': 'giritech_32x32.bmp', 
                         'name': 'G/On Win Client', 
                         'details': 'win', 
                         'description': 'Greatest remote solution ever.'}, {...}]
            
            @param autosort: Boolean indicating whether the items in the list should automatically be sorted by the name field. Default is True. 
            @since: 5.0
        """
        # As default we will sort the items by name
        if autosort:
            selectionlist = sorted(selectionlist, key=itemgetter('name')) # Sort list items by name
         
        _listhaschanged = False
        # If lengths are different 
        if len(selectionlist) != len(self.model.selection_list):
            _listhaschanged = True
        # if list content has changed. 
        else:
            for index, item in enumerate(selectionlist):
                for label, value in item.iteritems():
                    if self.model.selection_list[index].get(label) != item.get(label):
                        _listhaschanged = True 
                        break
                if _listhaschanged:
                    break
        # Refresh models selection list and alert subscribers
        if _listhaschanged:
            self.model.selection_list = []
            for item in selectionlist:
                _modellist = {}
                for label, value in item.iteritems():
                    _modellist[label] = value
                self.model.selection_list.append(_modellist)
            self.model.alert_subscribers()

    def set_selection_user_highlighted(self, index):
        """ Set the highlighted element in the selection list.
        
            Each time the user highlights a different selection item, this index
            is set. This method does not alert subscribers because it is 
            assumed that the selection is only relevant after a button push. 
            Use the getter to get the unique identifier for which item is
            selected.
            @see: get_selection_highlighted()
            
            @param index: Integer indicating a position in the models list.  
            @since: 5.4
            @note: This method should only be used from GUI side.
        """
        self.model.selection_user_highlighted = index
        self.model.alert_subscribers()

    def set_selection_highlighted_by_id(self, selectionid):
        """ Set the highlighted element in the selection list by ID.
        
            The id is given in the list you added in set_selection_list().
            
            @param id: The id for the element that should be highlighted.
            @since: 5.4 
        """
        if self.model.selection_highlighted_id != selectionid:
            self.model.selection_highlighted_id = selectionid
            self.model.alert_subscribers()

    def get_selection_highlighted_id(self):
        """ Get the id of the item currently highlighted by the user. 
        
            This should be used when the selection method is set to single selection. 
            The highlighted item will be the one selected.
            
            @return: The unique id of the item in the models selection list that is currently selected.
            @since: 5.4
        """
        if self.model.selection_user_highlighted != -1:
            return self.model.selection_list[self.model.selection_user_highlighted]['id']
        else:
            return None

    ID_MODE_INFORMATION_FREE_TEXT = 'free-text' 
    ID_MODE_INFORMATION_BUTTON_TEXT = 'button-text'
    ID_MODE_INFORMATION_PROGRESS = 'progress'
    ID_MODE_INFORMATION_GET_FOLDER = 'get-folder'

    def set_info_mode(self, mode):
        """ Set the information area mode.
        
            Use this to change the information area mode. There are currently three
            different modes for the information area. 
            
            Free Text Mode: Displays a headline and some text
            Button Text Mode: Displays a headline and button descriptions
            Progress Mode: Displays a headline, a progress-bar and some progress sub-text.
            Get Folder Mode: Displays a headline, a path and a 'browse' button.
            
            @param mode: ID_MODE_INFORMATION_FREE_TEXT | ID_MODE_INFORMATION_BUTTON_TEXT | ID_MODE_INFORMATION_PROGRESS | ID_MODE_INFORMATION_GET_FOLDER
            @see: set_info_headline(), set_next_description(), set_cancel_description(), set_info_progress_mode(), set_info_progress_complete(), set_info_progress_subtext()            
            @since: 5.4
        """
        try:
            assert mode in [UpdateController.ID_MODE_INFORMATION_FREE_TEXT, 
                            UpdateController.ID_MODE_INFORMATION_PROGRESS,
                            UpdateController.ID_MODE_INFORMATION_BUTTON_TEXT,
                            UpdateController.ID_MODE_INFORMATION_GET_FOLDER]
            self.model.info_mode = mode
            if self.model.info_mode == UpdateController.ID_MODE_INFORMATION_FREE_TEXT:
                self.model.info_text = self.model.info_free_text
            elif self.model.info_mode == UpdateController.ID_MODE_INFORMATION_BUTTON_TEXT:
                #self.model.info_text = self.model.cancel_description + "\n" + self.model.next_description
                self.model.info_text = self.model.button_info_text
            self.model.alert_subscribers()
        except AssertionError:
            print "Update model assertion error: Mode should be set to one of two values [UpdateController.ID_MODE_INFORMATION_FREE_TEXT | UpdateController.ID_MODE_INFORMATION_PROGRESS]"

    def get_info_mode(self):
        """ Get the current info area mode setting.
            
            @return: The current info area mode setting.
            @see: set_info_mode() for possible return values.
            @since: 5.0
        """
        return self.model.info_mode
    
    def set_info_headline(self, headline):
        """ Set the headline in the information area. 
        
            This headline is displayed in free text mode, progress mode and
            button text mode.
            
            @param headline: String for the information area headline.
            @since: 5.0 
        """
        self.model.info_headline = headline
        self.model.alert_subscribers()
    
    def get_info_headline(self):
        """ Get the headline for the info area. 
        
            @return: The string currently set for the information area headline.
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.info_headline
    
    ID_MODE_PROGRESS_KNOWN = 0
    ID_MODE_PROGRESS_UNKNOWN = 1
    
    def set_info_progress_mode(self, mode):
        """ Set the progress mode.

            There are currently two progress modes. The progress known mode
            is used when you know how long a task will take and how far along 
            you are. The progress unknown mode is used when you do not know.
        
            @param mode: ID_MODE_PROGRESS_KNOWN | ID_MODE_PROGRESS_UNKNOWN
            @since: 5.4
        """
        if self.model.info_progress_mode != mode:
            self.model.info_progress_mode = mode
            self.model.alert_subscribers() 
    
    def set_info_progress_complete(self, percentage):
        """ Set how much of a task is completed. 
        
            When the progress bar is set to the mode progress known, this 
            value is used to set how much of a progress bar is colored.
            
            @param percentage: Percentage of task completion.
            @since: 5.0 
        """
        self.model.info_progress_complete = int(percentage)
        self.model.alert_subscribers()

    def get_info_progress_complete(self):
        """ Get how much of a task is completed. 
        
            @return: Percentage of how much of the current task is completed.
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.info_progress_complete

    def set_info_progress_subtext(self, subtext):
        """ Set the text to appear as a sub-text for the progress. 
        
            Set the detailed information about a task that uses the progress bar.
            This text is displayed underneath the progress bar, when the information
            area is in progress mode. 
            
            @param subtext: String a text to display underneath the progress bar. 
            @since: 5.0
        """
        self.model.info_progress_subtext = subtext
        self.model.alert_subscribers()

    def get_info_progress_subtext(self):
        """ Get the text to appear as a sub-text for the progress. 
        
            @return: String with text currently displayed under the progress bar.
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.info_progress_subtext

    def set_info_free_text(self, text):
        """ Set the information areas free text.
        
            Set the text directly below the headline. This text is
            displayed when the information area is set the free text
            mode.
            
            @param text: String to display as free text.  
            @since: 5.0
        """
        if self.model.info_free_text != text:
            self.model.info_free_text = text
            if self.model.info_mode == UpdateController.ID_MODE_INFORMATION_FREE_TEXT:
                self.model.info_text = text
            self.model.alert_subscribers()

    def set_info_user_written_folder_path_without_notification(self, path):
        """ Set the path the user has entered in a text field.
        
            Sets the path without alerting subscribers.
        
            @param path: The path the user has entered.
            @since: 5.4 
            @note: This method should only be used from GUI side.
            @see: set_info_folder_path()
        """
        self.model.info_user_written_folder_path = path

    def set_info_user_selected_folder_path(self, path):
        """ Set the path that the user has selected.  
        
            @param path: The path selected by the user as a string.
            @since: 5.4  
            @note: This method should only be used from GUI side.
            @see: set_info_folder_path()
        """
        if self.model.info_user_selected_folder_path != path:
            self.model.info_user_selected_folder_path = path
            self.model.alert_subscribers()

    def get_info_user_written_folder_path(self):
        """ Get the folder path written by the user.
        
            Use this method to get the path written to 
            an edit field by the user.
            
            @return: The folder path written by the user as a string
            @since: 5.4
        """
        return self.model.info_user_written_folder_path

    def get_info_user_selected_folder_path(self):
        """ Get the folder path selected by the user.
        
            Use this method to get the path selected by the user.
            Then use set_info_folder_path() to set the path that you 
            want. This can be used if you want to add a sub-folder to
            the folder that the user selected. The path set with 
            set_info_folder_path() should be displayed to the user.
        
            @return: The folder path selected by the user as a string.
            @since: 5.4
            @see: set_info_folder_path()
        """
        return self.model.info_user_selected_folder_path

    def set_info_folder_path(self, path):
        """ Set the path that should be displayed as the currently selected path.
        
            Use get_info_user_selected_folder_path() to get the path
            that the user has selected. Then use this method to set the 
            path that is displayed back to the user. If you do not set the 
            path using this method - the user will be missing feedback.
        
            @param path: The currently selected path as a string.
            @since: 5.4  
            @see: get_info_user_selected_folder_path()
        """
        if self.model.info_folder_path != path:
            self.model.info_folder_path = path
            self.model.alert_subscribers()
        
    def set_gui_should_update_on_alerts(self, state):
        """ Set whether the GUI should update on subscription alerts.
            
            This should be handled by the GUI itself so that subscribers
            are still notified. Use this if you want to make a lot of updates 
            but do not want the GUI to 'flicker'.
            
            @param state: Boolean indicating whether the GUI should update on alert.
            @since: 5.0 
        """
        self.model.gui_update_on_alerts = state
        if state == True:
            self.model.alert_subscribers()

    def get_gui_should_update_on_alerts(self):
        """ Get the setting saying if the gui should update on subscriber alerts. 
        
            @return: Boolean indicating whether the GUI updates on alerts.
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.gui_update_on_alerts
        

# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class UpdateView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = UpdateController(self.model, self)
