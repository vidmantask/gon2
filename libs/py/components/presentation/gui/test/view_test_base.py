'''
Created on 16/04/2010

@author: Christian
'''

import sys

if sys.platform == 'win32':
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView as TestCommonView #@UnusedImport
    from components.presentation.gui.mfc.mfc_login_view import MfcLoginView as TestLoginView #@UnusedImport
    from components.presentation.gui.mfc.mfc_message_view import MfcMessageView as TestMessageView #@UnusedImport
    from components.presentation.gui.mfc.mfc_modal_message_view import MfcModalMessageView as TestModalMessageView #@UnusedImport
    from components.presentation.gui.mfc.mfc_menu_view import MfcMenuView as TestMenuView #@UnusedImport
    from components.presentation.gui.mfc.mfc_update_view import MfcUpdateView as TestUpdateView #@UnusedImport
    from components.presentation.gui.mfc.test.mfc_view_test_base import * #@UnusedImport
elif sys.platform == 'linux2':
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView as TestCommonView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.gtk_login_view import GtkLoginView as TestLoginView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.gtk_message_view import GtkMessageView as TestMessageView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.gtk_modal_message_view import GtkModalMessageView as TestModalMessageView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.gtk_menu_view import GtkMenuView as TestMenuView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.gtk_update_view import GtkUpdateView as TestUpdateView #@UnusedImport @Reimport
    from components.presentation.gui.gtk.test.gtk_view_test_base import * #@UnusedImport
elif sys.platform == 'darwin':
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView as TestCommonView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.cocoa_login_view import CocoaLoginView as TestLoginView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.cocoa_message_view import CocoaMessageView as TestMessageView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.cocoa_modal_message_view import CocoaModalMessageView as TestModalMessageView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.cocoa_menu_view import CocoaMenuView as TestMenuView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.cocoa_update_view import CocoaUpdateView as TestUpdateView #@UnusedImport @Reimport
    from components.presentation.gui.cocoa.test.cocoa_view_test_base import * #@UnusedImport

standard_delay = 0.5
long_delay = 1.0
