""" 

Cross platform tests for the login-view.

"""

from components.presentation.common import CommonModel
from components.presentation.login import LoginModel
import components.presentation.gui.test.view_test_base as viewTestBase

from threading import Thread
from lib import giri_unittest
import time
import unittest


class LoginViewTestBase(Thread):
    def __init__(self, main, commonview, loginview):
        Thread.__init__(self)
        self.commonview = commonview
        self.loginview = loginview
        self.main = main

class ClientGatewayOptions():
    """ Simulate the client options settings. """
    def init(self):
        self.gui_image_path = ''

class LoginViewTest(unittest.TestCase):
    """ Collection of tests for the MFC based view of login. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.testusername = self.loginmodel.username
        self.testpassword = self.loginmodel.password
        self.testgreeting = self.loginmodel.greeting
        self.cancelled = self.loginmodel.cancel_clicked
        self.notified = True

    def setUp(self):
        self.commonmodel = CommonModel()
        self.loginmodel = LoginModel(giri_unittest.get_dictionary())
        self.loginmodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()
        
        self.commonview = viewTestBase.TestCommonView(self.commonmodel, None, 'commonview', configuration)
        self.loginview = viewTestBase.TestLoginView(self.loginmodel, self.commonview.handles, 'loginview', configuration)

        self.testusername = ""
        self.testpassword = ""
        self.testgreeting = ""
        self.cancelled = False
        self.notified = False

    def test_login_view_init(self):
        """ Test that the login view can be initialized and displayed. """
        class TestCase(LoginViewTestBase):
            def __init__(self, main, commonview, loginview):
                LoginViewTestBase.__init__(self, main, commonview, loginview)

            def run(self):
                self.loginview.display()
                time.sleep(viewTestBase.long_delay)
                self.loginview.destroy()
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview, self.loginview)
        self.test.start()
        self.commonview.display()

    def test_login_view_read_credentials_on_ok(self):
        """ Test to see if the login and password are read correctly on OK. """
        class TestCase(LoginViewTestBase):
            def __init__(self, main, commonview, loginview):
                LoginViewTestBase.__init__(self, main, commonview, loginview)
                #self.loginview.controller.set_state(self.loginview.controller.ID_STATE_CHANGE_LOGIN)
                
            def run(self):
                self.loginview.display()
                time.sleep(viewTestBase.standard_delay)
                
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_LOGIN_TEXTFIELD, "Giritech")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_PASSWORD_TEXTFIELD, "Secret")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.push_button(self.loginview, viewTestBase.ID_BUTTON_NEXT)
                self.main.assertEqual(self.main.testusername, "Giritech")
                self.main.assertEqual(self.main.testpassword, "Secret")
                self.main.assertEqual(self.main.notified, True)
        
                time.sleep(viewTestBase.long_delay)
                self.loginview.destroy()
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview, self.loginview)
        self.test.start()
        self.commonview.display()
        
    def test_login_view_set_cancelled_state_on_cancel(self):
        """ Test to see if the 'cancelled' variable is set correctly on cancel. """
        class TestCase(LoginViewTestBase):
            def __init__(self, main, commonview, loginview):
                LoginViewTestBase.__init__(self, main, commonview, loginview)

            def run(self):
                self.loginview.display()
                time.sleep(viewTestBase.standard_delay)
                
                viewTestBase.push_button(self.loginview, viewTestBase.ID_BUTTON_CANCEL) 
                self.main.assertEqual(self.main.cancelled, True)

                time.sleep(viewTestBase.long_delay)
                self.loginview.destroy()
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview, self.loginview)
        self.test.start()
        self.commonview.display()
        
    def test_login_set_state(self):
        """ Test to see if the state can be changed properly.
        
             @since: 5.3
        """
        class TestCase(LoginViewTestBase):
            def __init__(self, main, commonview, loginview):
                LoginViewTestBase.__init__(self, main, commonview, loginview)

            def run(self):
                self.loginview.display()
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_LOGIN_TEXTFIELD, "Giritech")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_PASSWORD_TEXTFIELD, "Secret")
                time.sleep(viewTestBase.standard_delay)
                self.loginview.controller.set_state(self.loginview.controller.ID_STATE_CHANGE_LOGIN)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.loginview, viewTestBase.ID_LOGIN_TEXTFIELD), "Giritech")
                self.main.assertEqual(viewTestBase.get_widget_text(self.loginview, viewTestBase.ID_PASSWORD_TEXTFIELD), "Secret")
                time.sleep(viewTestBase.standard_delay)
                self.loginview.controller.set_state(self.loginview.controller.ID_STATE_LOGIN)
                time.sleep(viewTestBase.standard_delay)
                self.loginview.controller.set_state(self.loginview.controller.ID_STATE_CHANGE_LOGIN)
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_NEW_PASSWORD_TEXTFIELD, "NewSecret")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD, "NewSecret")
                
                time.sleep(viewTestBase.long_delay)
                self.loginview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.loginview)
        self.test.start()
        self.commonview.display()

    def test_login_set_and_get_password_request(self):
        """ Test to see if the password request is set properly when the OK button is pushed. 
             
             @since: 5.3
        """
        class TestCase(LoginViewTestBase):
            def __init__(self, main, commonview, loginview):
                LoginViewTestBase.__init__(self, main, commonview, loginview)

            def run(self):
                self.loginview.display()
                time.sleep(viewTestBase.standard_delay)
                self.loginview.controller.set_state(self.loginview.controller.ID_STATE_CHANGE_LOGIN) 
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_LOGIN_TEXTFIELD, "Giritech")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_PASSWORD_TEXTFIELD, "Secret")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_NEW_PASSWORD_TEXTFIELD, "AnotherSecret")
                time.sleep(viewTestBase.standard_delay)
                viewTestBase.set_widget_text(self.loginview, viewTestBase.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD, "AnotherSecret")
                time.sleep(viewTestBase.standard_delay)

                self.main.assertEqual(self.loginview.controller.get_password_request(), None)
                viewTestBase.push_button(self.loginview, viewTestBase.ID_BUTTON_NEXT) 
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.loginview.controller.get_password_request(), "AnotherSecret")
                
                time.sleep(viewTestBase.long_delay)
                self.loginview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.loginview)
        self.test.start()
        self.commonview.display()


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
