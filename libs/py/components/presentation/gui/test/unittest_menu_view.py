""" 

Cross platform tests for the menu view.

"""

from components.presentation.common import CommonModel
from components.presentation.menu import MenuModel
from components.presentation.gui.test.view_test_base import *
from threading import Thread
from lib import giri_unittest

import time
import unittest


class MenuViewTestBase(Thread):
    def __init__(self, main, commonview, menuview):
        Thread.__init__(self)
        self.commonview = commonview
        self.menuview = menuview
        self.main = main

#    def view_sub_folders(self, window): 
#        """ Use simulated key-presses to open sub-folders for viewing. """
#        push_key_down(window)
#        time.sleep(standard_delay)
#        push_key_right(window)
#        time.sleep(standard_delay)
#        push_key_right(window)
#        time.sleep(standard_delay)

    def popupalate_menu(self):
        """ Add a number of folders and items to the main menu. """
        self.menuview.controller.add_folder(-1, 1, "Folder", None)
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(-1, 2, "Item", None, True, 1000)
        time.sleep(standard_delay)
        self.menuview.controller.add_item(-1, 3, "Second item", None, True, 1001)
        time.sleep(standard_delay) 
        self.menuview.controller.add_folder(1, 4, "Folder in folder", None)
        time.sleep(standard_delay)
        self.menuview.controller.add_item(1, 5, "Item in folder", None, True, 1002)
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(1, 6, "Second item in folder", None, True, 1003)
        time.sleep(standard_delay) 
        self.menuview.controller.add_folder(4, 7, "Folder in folder in ...", None)
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(4, 8, "Item in folder in ...", None, True, 1004)
        time.sleep(standard_delay) 
        self.menuview.controller.add_folder(-1, 9, "Folder with icon", "outlook.bmp")
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(-1, 10, "Item with icon", "outlook.bmp", True, 1005)
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(-1, 11, "Disabled item", None, False, 1006)
        time.sleep(standard_delay) 
        self.menuview.controller.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, 1007)
        time.sleep(standard_delay)

class ClientGatewayOptions():
    """ Simulate the client options settings. """
    def init(self):
        self.gui_image_path = ''

class MenuViewTest(unittest.TestCase):
    """ Collection of tests for the menu view. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.notified = True
        self.notify_count = self.notify_count + 1

    def setUp(self):
        self.commonmodel = CommonModel()
        self.menumodel = MenuModel(giri_unittest.get_dictionary())
        self.menumodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()
        
        self.commonview = TestCommonView(self.commonmodel, None, 'commonview', configuration)
        self.menuview = TestMenuView(self.menumodel, self.commonview.handles, 'appmenu', configuration)

        self.notified = False
        self.notify_count = 0

    def test_menu_view_init(self):
        """ Test that the view can be initialized and displayed. """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                
                self.main.assertEqual(self.main.menumodel, self.menuview.model)
                # Make sure that the index given by the menu item matches the correct
                # launch id in the launch-id map. 
                items = [(1, 1000), (2, 1001), (4, 1005), (5, 1006), (6, 1007)] #, (7, 1009), (8, 1010), (9, 1011)]
                for item, launchid in items:
                    self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder, item), get_menu_launch_id_from_index(self.menuview, launchid))
                items = [(1, 1002), (2, 1003)]
                for item, launchid in items:
                    self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1], item), get_menu_launch_id_from_index(self.menuview, launchid))
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1].contentids[4], 1), get_menu_launch_id_from_index(self.menuview, 1004))                    
                
                time.sleep(long_delay)
                
                # Make sure that folders are given a -1 identifier.
                items = [3] #[3, 10, 11, 12]
                for item in items:
                    self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder, item), -1)
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1], 0), -1)
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1].contentids[4], 0), -1)
                # Make sure that there is the correct number of items in specific menu folders.
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder), 7)
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder.contentids[1]), 3)
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder.contentids[1].contentids[4]), 2)
                
                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

    def test_menu_view_select_item(self):
        """ See that simulated item selections are registered correct. """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                time.sleep(standard_delay)

                select_menu_item(self.menuview, self.menuview.menu.topfolder.contentids[3])
                self.main.assertEqual(self.menuview.controller.get_launch_list(), [1001])
                select_menu_item(self.menuview, self.menuview.menu.topfolder.contentids[1].contentids[5])
                self.main.assertEqual(self.menuview.controller.get_launch_list(), [1001, 1002])
                
                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

    def test_menu_view_subscription(self):
        """ Test that subscribers are notified. 
        
            Subscriber is notified every time a 
            change has been made to the model. 
            So any menu insertion gives an alert.
            There are 18 valid menu insertions and
            3 launches. So 21 alerts.
        """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                time.sleep(standard_delay)
                
                self.main.assertEqual(self.main.notified, True)
                self.main.assertEqual(self.main.notify_count, 12)
                
                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

    def test_menu_view_update(self):
        """ Test that updates works. """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                time.sleep(standard_delay)

#                push_key_down(self.menuview.topfolder)
                self.menuview.controller.add_item(-1, 21, "Update 1", None, True, 1000)
#                push_key_right(self.menuview.topfolder)
                time.sleep(standard_delay)
                self.menuview.controller.add_item(1, 22, "Update 2", None, True, 1001)
#                push_key_right(self.menuview.topfolder)
                time.sleep(standard_delay)
                self.menuview.controller.add_item(4, 23, "Update 3", None, True, 1002)
                time.sleep(standard_delay)
                
                get_menu_launch_id_from_index
                
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder, 7), get_menu_launch_id_from_index(self.menuview, 1000))
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder), 8) 
                # Test for update in folder
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1], 3), get_menu_launch_id_from_index(self.menuview, 1001))
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder.contentids[1]), 4) 
                # Test for update in folders folder.
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder.contentids[1].contentids[4], 2), get_menu_launch_id_from_index(self.menuview, 1002))
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder.contentids[1].contentids[4]), 3)
                
                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

    def test_menu_view_update_fun(self):
        """ Test that updates works. 
        
            TODO: Not currently working in GTK GUI.
        """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                time.sleep(standard_delay)

                self.menuview.controller.update_item(-1, 2, "I have been updated :-)", None, False, 1000)
                time.sleep(standard_delay)
                self.menuview.controller.update_item(-1, 2, "Now I am enabled :-)", None, True, 1001)
                time.sleep(standard_delay)
                self.menuview.controller.update_item(-1, 2, "Just got a nice icon :-)", "giritech.bmp", True, 1002)
                time.sleep(standard_delay)

                self.menuview.controller.update_item(-1, 2, "Lost some folders at the bottom :-(", "giritech.bmp", True, 1003)
                time.sleep(standard_delay)
                self.menuview.controller.remove_folder(-1, 1)
                time.sleep(standard_delay)
                self.menuview.controller.remove_folder(-1, 9)
                time.sleep(standard_delay)
                self.menuview.controller.update_item(-1, 2, "No folders left :-(", "giritech.bmp", True, 1004)
                time.sleep(standard_delay)
                
                self.menuview.controller.remove_item(-1, 3)
                time.sleep(standard_delay)
                self.menuview.controller.remove_item(-1, 10)
                time.sleep(standard_delay)
                self.menuview.controller.remove_item(-1, 11)
                time.sleep(standard_delay)
                self.menuview.controller.remove_item(-1, 12)
                time.sleep(standard_delay)        
        
                self.menuview.controller.update_item(-1, 2, "Now I'm all alone :-(", "giritech.bmp", True, 1006)
                time.sleep(standard_delay)
                self.menuview.controller.update_item(-1, 2, "Now I'm all alone :-(", None, True, 1007)
               
                self.main.assertEqual(self.menuview.menu.topfolder.contentids.keys(), [2])
                self.main.assertEqual(get_menu_item_count(self.menuview.menu.topfolder), 1)
                self.main.assertEqual(get_menu_item_launch_id(self.menuview, self.menuview.menu.topfolder, 0), get_menu_launch_id_from_index(self.menuview, 1000))
                
                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

    def test_menu_view_handle_bad_image_types(self):
        """ Test that updates with bad image types are handled nicely. """
        class TestCase(MenuViewTestBase):
            def __init__(self, main, commonview, menuview):
                MenuViewTestBase.__init__(self, main, commonview, menuview)

            def run(self):
                self.menuview.display()
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                self.popupalate_menu()
                time.sleep(standard_delay)

                self.menuview.controller.add_folder(100, 13, "Folder with nonexisting parent", None)
                time.sleep(standard_delay) 
                self.menuview.controller.add_item(100, 14, "Item with nonexisting parent", None, True, 1008)
                time.sleep(standard_delay) 
                self.menuview.controller.add_item(-1, 15, "Item with nonexisting bmp icon", "nonexisting.bmp", True, 1009)
                time.sleep(standard_delay) 
                self.menuview.controller.add_item(-1, 16, "Item with unsupported icon type", "nonexisting.ico", True, 1010)
                time.sleep(standard_delay) 
                self.menuview.controller.add_item(-1, 17, "Item with bad icon type", "nonexisting.bad", True, 1011)
                time.sleep(standard_delay)
                self.menuview.controller.add_folder(-1, 18, "Folder with nonexisting bmp icon", "nonexisting.bmp")
                time.sleep(standard_delay) 
                self.menuview.controller.add_folder(-1, 19, "Folder with unsupported icon type", "nonexisting.ico")
                time.sleep(standard_delay)
                self.menuview.controller.add_folder(-1, 20, "Folder with bad icon type", "nonexisting.bad")

                time.sleep(long_delay)
                self.menuview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.menuview)
        self.test.start()
        self.commonview.display()

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_TIMEOUT = 60 * 3


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
