""" 

Cross platform tests for the message-view.

"""

from components.presentation.common import CommonModel
from components.presentation.message import MessageModel
import components.presentation.gui.test.view_test_base as ViewTestBase

from threading import Thread
from lib import giri_unittest

import time
import unittest

class MessageViewTestBase(Thread):
    def __init__(self, main, commonview, messageview):
        Thread.__init__(self)
        self.commonview = commonview
        self.messageview = messageview
        self.main = main

class ClientGatewayOptions():
    """ Simulate the client options settings. """
    def init(self):
        self.gui_image_path = ''

class MessageViewTest(unittest.TestCase):
    """ Collection of tests for the MFC based view of login. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.notified = True

    def setUp(self):
        self.commonmodel = CommonModel()
        self.messagemodel = MessageModel()
        self.messagemodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()
        
        self.commonview = ViewTestBase.TestCommonView(self.commonmodel, None, 'commonview', configuration)
        self.messageview = ViewTestBase.TestMessageView(self.messagemodel, self.commonview.handles, 'messageview', configuration)

        self.notified = False

    def test_message_view_init(self):
        """ Test that the message view can be initialized and displayed. """
        class TestCase(MessageViewTestBase):
            def __init__(self, main, commonview, messageview):
                MessageViewTestBase.__init__(self, main, commonview, messageview)

            def run(self):
                self.messageview.display()
                time.sleep(ViewTestBase.long_delay)
                self.main.assertEqual(self.main.messagemodel, self.messageview.model)
                self.main.assertEqual(self.messageview.headline, "")
                self.main.assertEqual(self.messageview.message, "")    
                self.messageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.messageview)
        self.test.start()
        self.commonview.display()

    def test_message_view_set_message(self):
        """ Test that the message view message can be set and displayed. """
        class TestCase(MessageViewTestBase):
            def __init__(self, main, commonview, messageview):
                MessageViewTestBase.__init__(self, main, commonview, messageview)

            def run(self):
                
                self.messageview.controller.set_message("TEST", "Message set by test")
                self.messageview.display()
                self.main.assertEqual(self.messageview.headline, "TEST")
                self.main.assertEqual(self.messageview.message, "Message set by test")        
                time.sleep(ViewTestBase.long_delay)
                self.messageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.messageview)
        self.test.start()
        self.commonview.display()

    def test_message_view_subscription(self):
        """ Update of message view while visible. """
        class TestCase(MessageViewTestBase):
            def __init__(self, main, commonview, messageview):
                MessageViewTestBase.__init__(self, main, commonview, messageview)

            def run(self):
                self.messageview.controller.set_message("TEST", "Message set by test")
                self.messageview.display()
                self.main.assertEqual(self.messageview.headline, "TEST")
                self.main.assertEqual(self.messageview.message, "Message set by test")
                time.sleep(ViewTestBase.standard_delay)
                self.messageview.controller.set_message("TEST 2", "Message changed by test")
                self.main.assertEqual(self.messageview.headline, "TEST 2")
                self.main.assertEqual(self.messageview.message, "Message changed by test") 
                
                time.sleep(ViewTestBase.long_delay)
                self.messageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.messageview)
        self.test.start()
        self.commonview.display()


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
