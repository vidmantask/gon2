""" 

Cross platform tests for the update view.

"""

#from appl.gon_client.gon_client import ClientGatewayOptions
from components.presentation.common import CommonModel
from components.presentation.update import UpdateModel
import components.presentation.gui.test.view_test_base as viewTestBase

from threading import Thread
from lib import giri_unittest
import time
import unittest

class ClientGatewayOptions():
        def init(self):
            self.gui_image_path = '..'

class UpdateViewTestBase(Thread):
    def __init__(self, main, commonview, updateview):
        Thread.__init__(self)
        self.commonview = commonview
        self.updateview = updateview
        self.main = main


class UpdateViewTest(unittest.TestCase):
    """ Collection of tests for the menu view. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.notified = True

    def setUp(self):
        self.commonmodel = CommonModel()
        self.updatemodel = UpdateModel(giri_unittest.get_dictionary())
        self.updatemodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()
        
        self.commonview = viewTestBase.TestCommonView(self.commonmodel, None, 'commonview', configuration)
        self.updateview = viewTestBase.TestUpdateView(self.updatemodel, self.commonview.handles, 'appmenu', configuration)

        self.notified = False
        self.current_path = self.updatemodel.info_user_selected_folder_path

        # Basic settings...
        self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
        self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_UPDATE)
        
        self.phase_list = [{'name': 'Test 1', 'state': self.updateview.controller.ID_PHASE_PENDING},
                           {'name': 'Test 2', 'state': self.updateview.controller.ID_PHASE_PENDING},
                           {'name': 'Test 3', 'state': self.updateview.controller.ID_PHASE_PENDING},
                           {'name': 'Test 4', 'state': self.updateview.controller.ID_PHASE_PENDING},
                           {'name': 'Test 5', 'state': self.updateview.controller.ID_PHASE_PENDING}]
        self.updateview.controller.set_phase_list(self.phase_list) 
        
        self.selection_list = [{'id':0, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'Package 0', 'details': 'details 0', 'description': 'Description 0'},
                               {'id':1, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Package 1', 'details': 'details 1', 'description': 'Description 1'},
                               {'id':2, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'Package 2', 'details': 'details 2', 'description': 'Description 2'},
                               {'id':3, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Package 3', 'details': 'details 3', 'description': 'Description 3'},
                               {'id':4, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'Package 4', 'details': 'details 4', 'description': 'Description 4'},
                               {'id':5, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Package 5', 'details': 'details 5', 'description': 'Description 5'},
                               {'id':6, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'Package 6', 'details': 'details 6', 'description': 'Description 6'},
                               {'id':7, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Package 7', 'details': 'details 7', 'description': 'Description 7'},
                               {'id':8, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'Package 8', 'details': 'details 8', 'description': 'Description 8'},
                               {'id':9, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Package 9', 'details': 'details 9', 'description': 'Description 9'}]
        self.updateview.controller.set_selection_list(self.selection_list) 

    def test_update_view_init(self):
        """ Test that the view can be initialized and displayed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                self.main.assertEqual(self.main.updatemodel, self.updateview.model)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_info_folder_path(self):
        """ Test that the info folder path can be set and changed by program side. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):

                def change_user_selected_folder():
                    if self.main.current_path != self.main.updatemodel.info_user_selected_folder_path: 
                        path = self.updateview.controller.get_info_user_selected_folder_path()
                        self.updateview.controller.set_info_folder_path(path + "/" + "extra")
                        self.current_path = self.main.updatemodel.info_folder_path  
                
                self.main.updatemodel.subscribe(change_user_selected_folder)

                self.updateview.display()
                self.updateview.controller.set_info_headline("Get Folder Test")
                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_GET_FOLDER)
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_info_user_selected_folder_path("base")
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def xtest_update_view_set_phase_list(self):
        """ Test that the phase list can be changed. 
        
            TODO: Not currently working on GTK GUI
        """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()

                # Phase box visibility when adding.
                for index in range(0, 5):
                    self.updateview.controller.set_phase_list(self.main.phase_list[0:index+1])
                    time.sleep(viewTestBase.standard_delay)
                    for true_index in range(0, index+1):
                        self.main.assertTrue(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxtexts[true_index]))
                    for false_index in range(index+1, 5):
                        self.main.assertFalse(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxtexts[false_index]))
                # Phase box content changes.
                for index in range(0, 5):
                    self.main.phase_list[index]['state'] = self.updateview.controller.ID_PHASE_ACTIVE
                    if index > 0:
                        self.main.phase_list[index-1]['state'] = self.updateview.controller.ID_PHASE_COMPLETE
                    self.updateview.controller.set_phase_list(self.main.phase_list)
                    time.sleep(viewTestBase.standard_delay) 

                self.main.phase_list[index]['state'] = self.updateview.controller.ID_PHASE_COMPLETE
                self.updateview.controller.set_phase_list(self.main.phase_list)
                time.sleep(viewTestBase.standard_delay) 

                # Phase box visibility when removing.
                for index in range(5, -1, -1):
                    self.updateview.controller.set_phase_list(self.main.phase_list[0:index])
                    time.sleep(viewTestBase.standard_delay)
                    for true_index in range(0, index):
                        self.main.assertTrue(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxtexts[true_index]))
                    for false_index in range(index+1, 5):
                        self.main.assertFalse(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxtexts[false_index]))
                    for true_index in range(0, index-1):
                        self.main.assertTrue(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxarrows[true_index]))
                    for false_index in range(index, 4):
                        self.main.assertFalse(viewTestBase.is_window_visible(self.updateview.phaseboxtexts.phaseboxarrows[false_index]))

                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_info_mode(self):
        """ Test that the information mode settings can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                # View the progress known info mode...
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_info_headline("Known Progress")
                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_PROGRESS)
                
                for index in range(0, 110, 10): # To get numbers from 0 to 100
                    self.updateview.controller.set_info_progress_complete(index)
                    text = str(index) + " percent complete"
                    self.updateview.controller.set_info_progress_subtext(text)
                    time.sleep(viewTestBase.standard_delay) 
                    self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_HEADLINE), "Known Progress")
                    self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_SUBTEXT), text)
                    #self.main.assertTrue(get_progress_bar_progress(self.updateview, ID_PROGRESS_BAR) >= index-1) 
                time.sleep(viewTestBase.long_delay)

                # View the progress unknown info mode
                self.updateview.controller.set_info_headline('Unknown Progress')
                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_PROGRESS)
                self.updateview.controller.set_info_progress_mode(self.updateview.controller.ID_MODE_PROGRESS_UNKNOWN)
                self.updateview.controller.set_info_progress_subtext("When will this end?")
                time.sleep(viewTestBase.long_delay) # make sure the GUI has time to change.
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_HEADLINE), "Unknown Progress")
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_SUBTEXT), "When will this end?")
                

                # View the free text info mode...
                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
                self.updateview.controller.set_info_headline('Free Text')
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_FREE_TEXT_HEADLINE), 'Free Text')

                self.updateview.controller.set_info_free_text('Server is available in 3')
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_FREE_TEXT), 'Server is available in 3') 

                self.updateview.controller.set_info_free_text('Server is available in 2')
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_FREE_TEXT), 'Server is available in 2')
                
                self.updateview.controller.set_info_free_text('Server is available in 1')
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_FREE_TEXT), 'Server is available in 1')
                
                
                self.updateview.controller.set_info_free_text('Server is available now!')
                time.sleep(viewTestBase.long_delay)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_FREE_TEXT), 'Server is available now!')
                
#                # View the locate folder info mode...
#                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_GET_FOLDER)
#                self.updateview.controller.set_info_headline("Get Folder")
#                self.updateview.controller.set_info_folder_path("selected path") 
#                self.main.assertEqual(get_widget_text(self.updateview, ID_LOCATE_FOLDER_HEADLINE), "Get Folder")
#                self.main.assertEqual(is_widget_visible(self.updateview, ID_LOCATE_FOLDER_BUTTON), True)
#                self.main.assertEqual(get_widget_text(self.updateview, ID_LOCATE_FOLDER_TEXTFIELD), "selected path")
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_banner(self):
        """ Test that the view banner can be set. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Banner Test")
                time.sleep(viewTestBase.standard_delay)

                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_INSTALL)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.updateview.header.current_banner_image, "g_install_banner.bmp")

                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_UNINSTALL)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.updateview.header.current_banner_image, "g_uninstall_banner.bmp")

                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_UPDATE)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.updateview.header.current_banner_image, "g_update_banner.bmp")
                
                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_REMOVE)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.updateview.header.current_banner_image, "g_remove_banner.bmp")
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_selection_list_remove_item(self):
        """ Test that the view can be initialized and displayed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Selection List Middle Item Removal")
                
                self.selection_list_copy = self.main.selection_list
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 10)
                                
                self.selection_list_copy.pop(2)
                self.updateview.controller.set_selection_list(self.selection_list_copy)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 9)
                
                self.selection_list_copy.pop(2)
                self.updateview.controller.set_selection_list(self.selection_list_copy)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 8)
                
                self.selection_list_copy.pop(1)
                self.updateview.controller.set_selection_list(self.selection_list_copy)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 7)
        
                self.selection_list_copy.pop(1)
                self.updateview.controller.set_selection_list(self.selection_list_copy)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 6)
        
                self.selection_list_copy.pop(0)
                self.updateview.controller.set_selection_list(self.selection_list_copy)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), 5)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_mfc_update_view_hide_and_display(self):
        """ Test that the view can be hidden and redisplayed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Hide and Display Test")
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.is_window_visible(self.updateview), True)

                self.updateview.hide()
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.is_window_visible(self.updateview), False)

                self.updateview.controller.set_info_headline("Hide and Display Test - Reappear")
                self.updateview.display()
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(viewTestBase.is_window_visible(self.updateview), True)

                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_change_selection_texts(self):
        """ Test that the selection lists texts can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                self.selection_list_copy = self.main.selection_list
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(0.25)

                for index in range(0, 5, 1):
                    # Check the name before changes
                    self.main.assertEqual(viewTestBase.get_selection_list_item_text(self.updateview, viewTestBase.ID_SELECTION_LIST, index), 'Package ' + str(index))
                    # Update the name of the item
                    self.selection_list_copy[index]['name'] = 'Test ' + str(index)
                    self.updateview.controller.set_selection_list(self.selection_list_copy, autosort=False)
                    # Check the name after change
                    time.sleep(viewTestBase.standard_delay)
                    self.main.assertEqual(viewTestBase.get_selection_list_item_text(self.updateview, viewTestBase.ID_SELECTION_LIST, index), 'Test ' + str(index))

                    # Check the details field before changes
                    self.main.assertEqual(viewTestBase.get_selection_list_item_details(self.updateview, viewTestBase.ID_SELECTION_LIST, index), 'details ' + str(index))
                    # Change the details.
                    self.selection_list_copy[index]['details'] = 'details test ' + str(index)
                    self.updateview.controller.set_selection_list(self.selection_list_copy, autosort=False)
                    # Check the details after change.
                    time.sleep(viewTestBase.standard_delay)
                    self.main.assertEqual(viewTestBase.get_selection_list_item_details(self.updateview, viewTestBase.ID_SELECTION_LIST, index), 'details test ' + str(index))
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_change_selection_selections(self):
        """ Test that the view can be initialized and displayed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                self.updateview.controller.set_info_headline("Selection List Cell Change")
                self.updateview.display()
                
                self.selection_list_copy = self.main.selection_list
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(0.25)

                for index in range(0, 5, 1):
                    if index % 2 == 0:
                        self.main.assertTrue(viewTestBase.get_selection_list_item_selection_selected(self.updateview, viewTestBase.ID_SELECTION_LIST, index))
                        self.selection_list_copy[index]['selected'] = False
                        self.updateview.controller.set_selection_list(self.selection_list_copy)
                        time.sleep(viewTestBase.standard_delay)
                        self.main.assertFalse(viewTestBase.get_selection_list_item_selection_selected(self.updateview, viewTestBase.ID_SELECTION_LIST, index))
                    else:
                        self.main.assertFalse(viewTestBase.get_selection_list_item_selection_selected(self.updateview, viewTestBase.ID_SELECTION_LIST, index))
                        self.selection_list_copy[index]['selected'] = True
                        self.updateview.controller.set_selection_list(self.selection_list_copy)
                        time.sleep(viewTestBase.standard_delay)
                        self.main.assertTrue(viewTestBase.get_selection_list_item_selection_selected(self.updateview, viewTestBase.ID_SELECTION_LIST, index))
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_change_selection_icons(self):
        """ Test that the selection list item icons can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Selection List Cell Change")
                
                self.selection_list_copy = self.main.selection_list
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(viewTestBase.standard_delay)

                # Add an icon for each row
                for index in range(0, 10, 1):
                    self.selection_list_copy[index]['icon'] = viewTestBase.ID_PACKAGE_PLATFORM_IMAGE
                    self.updateview.controller.set_selection_list(self.selection_list_copy)
                    time.sleep(viewTestBase.standard_delay)
                # Remove the icon again from each row
                for index in range(0, 10, 1):
                    self.selection_list_copy[index]['icon'] = None
                    self.updateview.controller.set_selection_list(self.selection_list_copy)
                    time.sleep(viewTestBase.standard_delay)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_selection_add_and_remove_items(self):
        """ Test that the selection list can add and remove items. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                time.sleep(0.5)
                self.updateview.controller.set_info_headline("Selection List Add and Remove")
                self.updateview.display()
                
                self.selection_list_copy = self.main.selection_list
                self.updateview.controller.set_show_selection_list(True)

                # Add one row at a time
                for index in range(0, 10):
                    self.updateview.controller.set_selection_list(self.selection_list_copy[0:index])
                    time.sleep(viewTestBase.standard_delay)
                    self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), index)

                for index in range(10, -1, -1):
                    self.updateview.controller.set_selection_list(self.selection_list_copy[0:index])
                    time.sleep(viewTestBase.standard_delay)                    
                    self.main.assertEqual(viewTestBase.get_selection_list_item_count(self.updateview, viewTestBase.ID_SELECTION_LIST), index)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_simulate_button_pushes(self):
        """ Simulate button clicks. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Simulate Button Pushes")

                self.main.assertEqual(self.updateview.controller.get_cancel_clicked(), False)
                viewTestBase.push_button(self.updateview, viewTestBase.ID_BUTTON_CANCEL)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertEqual(self.updateview.controller.get_cancel_clicked(), True)

                self.main.assertEqual(self.updateview.controller.get_next_clicked(), False)
                viewTestBase.push_button(self.updateview, viewTestBase.ID_BUTTON_NEXT)
                time.sleep(viewTestBase.long_delay)
                self.main.assertEqual(self.updateview.controller.get_next_clicked(), True)

                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_button_states(self):
        """ Test that button states can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline("Button States")

                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_next_allowed(False) 
                time.sleep(viewTestBase.standard_delay)
                self.main.assertFalse(viewTestBase.is_widget_enabled(self.updateview, viewTestBase.ID_BUTTON_NEXT))
                
                self.updateview.controller.set_cancel_allowed(False)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertFalse(viewTestBase.is_widget_enabled(self.updateview, viewTestBase.ID_BUTTON_CANCEL))
                
                self.updateview.controller.set_next_allowed(True) 
                time.sleep(viewTestBase.standard_delay)
                self.main.assertTrue(viewTestBase.is_widget_enabled(self.updateview, viewTestBase.ID_BUTTON_NEXT))
                
                self.updateview.controller.set_cancel_allowed(True)
                time.sleep(viewTestBase.standard_delay)
                self.main.assertTrue(viewTestBase.is_widget_enabled(self.updateview, viewTestBase.ID_BUTTON_CANCEL))
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_enable_and_disable_gui_updates(self):
        """ Test that the view is only updated when gui updates are enabled. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_gui_should_update_on_alerts(False)
                
                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_INSTALL)
                self.updateview.controller.set_phase_list([{'name': 'Test 1', 'state': 'complete'},
                                                           {'name': 'Test 2', 'state': 'complete'},
                                                           {'name': 'Test 3', 'state': 'active'},
                                                           {'name': 'Test 4', 'state': 'pending'},
                                                           {'name': 'Test 5', 'state': 'pending'}])

                self.updateview.controller.set_info_mode(self.updateview.controller.ID_MODE_INFORMATION_PROGRESS)
                self.updateview.controller.set_info_headline('Fetching stuff')
                self.updateview.controller.set_info_progress_complete(50)
                self.updateview.controller.set_info_progress_subtext('50 percent complete')
                
                self.updateview.controller.set_show_selection_list(True)
                self.updateview.controller.set_selection_list(self.main.selection_list[0:5]) 
        
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_HEADLINE), '')
                self.main.assertEqual(viewTestBase.get_progress_bar_progress(self.updateview, viewTestBase.ID_PROGRESS_BAR), 0)
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_SUBTEXT), '')
                 
                self.updateview.controller.set_gui_should_update_on_alerts(True)
                time.sleep(viewTestBase.standard_delay)
                
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_HEADLINE), "Fetching stuff")
                self.main.assertTrue(viewTestBase.get_progress_bar_progress(self.updateview, viewTestBase.ID_PROGRESS_BAR) >= 50-1) 
                self.main.assertEqual(viewTestBase.get_widget_text(self.updateview, viewTestBase.ID_PROGRESS_BAR_SUBTEXT), '50 percent complete')

                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_window_frame_text(self):
        """ Test that the windows frame text can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline('Change Frame Text')
                
                for index in range(1, 11): 
                    self.updateview.controller.set_window_frame_text("Test " + str(index))
                    self.main.assertEqual(viewTestBase.get_window_frame_text(self.updateview), "Test " + str(index))
                    time.sleep(0.25)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_show_selection_list(self):
        """ Test that the windows frame text can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline('Show/Hide Selection List')
                
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_show_selection_list(False)
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_show_selection_list(True)
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_show_selection_list(False)
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_show_selection_list(True)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

    def test_update_view_set_button_labels(self):
        """ Test that the button labels can be changed. """
        class TestCase(UpdateViewTestBase):
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline('Change Button Labels')
                
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_cancel_label("Close")
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_cancel_label("No")
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_cancel_label("Exit")                
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_next_label("Remove")
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_next_label("Install")
                time.sleep(viewTestBase.standard_delay)
                self.updateview.controller.set_next_label("Update")
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()
    
    def xtest_update_view_set_selection_method_and_highlighted(self):
        """ Test that the button labels can be changed. 
        
            TODO: Not currently working on GTK GUI.
        """
        class TestCase(UpdateViewTestBase): 
            def __init__(self, main, commonview, updateview):
                UpdateViewTestBase.__init__(self, main, commonview, updateview)

            def run(self):
                self.updateview.display()
                self.updateview.controller.set_info_headline('Set Selection Method and Highlight')
                self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_INSTALL)
                self.updateview.controller.set_show_selection_list(True)
               
                for nr in range(0,2):
                    if nr % 2 == 0:
                        self.updateview.controller.set_selection_method(self.updateview.controller.ID_SELECTION_SINGLE)
                    else:
                        self.updateview.controller.set_selection_method(self.updateview.controller.ID_SELECTION_MULTIPLE)
                    time.sleep(viewTestBase.standard_delay)

                    for index in range(0, 10):
                        viewTestBase.set_selection_list_highlighted_item(self.updateview, viewTestBase.ID_SELECTION_LIST, index) 
                        time.sleep(viewTestBase.standard_delay)
                        self.main.assertEqual(self.updateview.controller.get_selection_highlighted_id(), index)
                
                time.sleep(viewTestBase.long_delay)
                self.updateview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.updateview)
        self.test.start()
        self.commonview.display()

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_TIMEOUT = 120

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
