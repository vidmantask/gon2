""" 

Cross platform tests for the common-view.

"""

from components.presentation.common import CommonModel
from components.presentation.gui.test.view_test_base import TestCommonView,\
                                                             standard_delay,\
                                                             left_click_tray_icon,\
                                                             right_click_tray_icon,\
                                                             ID_TRAY_ICON
from threading import Thread
from lib import giri_unittest

import time
import unittest


class CommonViewTestBase(Thread):
    def __init__(self, main, commonview):
        Thread.__init__(self)
        self.commonview = commonview
        self.main = main

class ClientGatewayOptions():
    """ Simulate the client options settings. """
    def init(self):
        self.gui_image_path = ''

class LoginViewTest(unittest.TestCase):
    """ Collection of tests for the MFC based view of login. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.notified = True

    def setUp(self):
        self.commonmodel = CommonModel()
        self.commonmodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()        
        self.commonview = TestCommonView(self.commonmodel, None, 'commonview', configuration)

        self.notified = False

    def test_common_view_init(self):
        """ Test that the login view can be initialized and displayed. """
        class TestCase(CommonViewTestBase):
            def __init__(self, main, commonview):
                CommonViewTestBase.__init__(self, main, commonview)

            def run(self):
                time.sleep(standard_delay)
                self.main.assertEqual(self.main.commonview.model, self.main.commonmodel)
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview)
        self.test.start()
        self.commonview.display()

    def test_common_view_mouse_clicks(self):
        """ Test the different mouse click possibilities. """ 
        
        class TestCase(CommonViewTestBase):
            def __init__(self, main, commonview):
                CommonViewTestBase.__init__(self, main, commonview)

            def run(self):
                time.sleep(standard_delay)
                left_click_tray_icon(self.commonview, ID_TRAY_ICON)
                time.sleep(standard_delay)
                right_click_tray_icon(self.commonview, ID_TRAY_ICON)
                time.sleep(standard_delay)
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview)
        self.test.start()
        self.commonview.display()

    def test_common_view_hide_and_show(self):
        """ Test the different mouse click possibilities. """
        class TestCase(CommonViewTestBase):
            def __init__(self, main, commonview):
                CommonViewTestBase.__init__(self, main, commonview)

            def run(self):
                time.sleep(standard_delay)
                self.commonview.controller.set_visible(False)
                time.sleep(standard_delay)
                self.commonview.controller.set_visible(True)
                time.sleep(standard_delay)
                self.commonview.controller.set_visible(False)
                time.sleep(standard_delay)
                self.commonview.controller.set_visible(True)
                time.sleep(standard_delay)
                self.commonview.controller.set_visible(False)
                self.commonview.destroy()
        
        self.test = TestCase(self, self.commonview)
        self.test.start()
        self.commonview.display()
  

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
