""" 

Cross platform tests for the modal message view.

"""
from components.presentation.common import CommonModel
from components.presentation.modal_message import ModalMessageModel
import components.presentation.gui.test.view_test_base as ViewTestBase

from threading import Thread
from lib import giri_unittest
import time
import unittest

class ModalMessageViewTestBase(Thread):
    def __init__(self, main, commonview, modalmessageview):
        Thread.__init__(self)
        self.commonview = commonview
        self.modalmessageview = modalmessageview
        self.main = main

class ClientGatewayOptions():
    """ Simulate the client options settings. """
    def init(self):
        self.gui_image_path = ''

class ModalMessageViewTest(unittest.TestCase):
    """ Collection of tests for the MFC based view of login. """

    def subscriber(self):
        """ The subscriber is called when model changes. """
        self.notified = True

    def setUp(self):
        self.commonmodel = CommonModel()
        self.modalmessagemodel = ModalMessageModel(giri_unittest.get_dictionary())
        self.modalmessagemodel.subscribe(self.subscriber)
        
        configuration = ClientGatewayOptions()
        configuration.gui_image_path = giri_unittest.get_image_path()
        configuration.dictionary = giri_unittest.get_dictionary()
        
        self.commonview = ViewTestBase.TestCommonView(self.commonmodel, None, 'commonview', configuration)
        self.modalmessageview = ViewTestBase.TestModalMessageView(self.modalmessagemodel, self.commonview.handles, 'modalmessageview', configuration)

        self.notified = False

    def test_modal_message_view_init(self):
        """ Test that the message view can be initialized and displayed. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
                time.sleep(ViewTestBase.long_delay)
                self.main.assertEqual(self.main.modalmessagemodel, self.modalmessageview.model)
                self.main.assertEqual(self.modalmessageview.htmlview.current_text, "")
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def test_modal_message_view_set_text(self):
        """ Update of the modal message View. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
                self.testtext = "A <b>fish</b> <u>can't</u> whistle and neither can <i>I</i>.<br>"

                for i in range(1,10):
                    time.sleep(ViewTestBase.standard_delay)
                    self.modalmessageview.controller.set_text(self.testtext * i)

                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def test_modal_message_view_buttons(self):
        """ Update of Modal Message Views buttons. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()

                self.modalmessageview.controller.set_text("Update button states.")
                time.sleep(ViewTestBase.standard_delay)
                self.modalmessageview.controller.set_next_allowed(False)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertFalse(ViewTestBase.is_widget_enabled(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT))
                time.sleep(ViewTestBase.standard_delay)
                self.modalmessageview.controller.set_next_allowed(True)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertTrue(ViewTestBase.is_widget_enabled(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT))

                time.sleep(ViewTestBase.standard_delay)
                self.modalmessageview.controller.set_cancel_allowed(False)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertFalse(ViewTestBase.is_widget_enabled(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL))
                
                self.modalmessageview.controller.set_cancel_allowed(True)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertTrue(ViewTestBase.is_widget_enabled(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL))
                
                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def test_modal_message_view_button_pushes(self):
        """ Simulate OK and Cancel button clicks. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
                self.modalmessageview.controller.set_text("Simulate button clicks.")
        
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.controller.get_next_clicked(), False)
                ViewTestBase.push_button(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT) 
                self.main.assertEqual(self.modalmessageview.controller.get_next_clicked(), True)

                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.controller.get_cancel_clicked(), False)
                ViewTestBase.push_button(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL)
                self.main.assertEqual(self.modalmessageview.controller.get_cancel_clicked(), True)

                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def test_modal_message_view_change_size(self):
        """ Change the size of the modal message view. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
                self.modalmessageview.controller.set_text("Size changes.")
                self.main.assertEqual(self.modalmessageview.current_size, self.modalmessageview.controller.ID_SMALL_SIZE)
                
                self.modalmessageview.controller.set_size(self.modalmessageview.controller.ID_LARGE_SIZE)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.current_size, self.modalmessageview.controller.ID_LARGE_SIZE)
                
                self.modalmessageview.controller.set_size(self.modalmessageview.controller.ID_SMALL_SIZE)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.current_size, self.modalmessageview.controller.ID_SMALL_SIZE)
                
                self.modalmessageview.controller.set_size(self.modalmessageview.controller.ID_LARGE_SIZE)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.current_size, self.modalmessageview.controller.ID_LARGE_SIZE)

                self.modalmessageview.controller.set_size(self.modalmessageview.controller.ID_SMALL_SIZE)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(self.modalmessageview.current_size, self.modalmessageview.controller.ID_SMALL_SIZE)
                
                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def xtest_modal_message_view_hide_and_show_buttons(self):
        """ Hide and show OK and Cancel buttons in modal message view. 
        
            TODO: Not currently working on GTK GUI.
        """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
                self.modalmessageview.controller.set_text("Hide and show buttons.")
                
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT), True)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL), True)
                
                self.modalmessageview.controller.set_next_visible(False)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT), False)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL), True)
                
                self.modalmessageview.controller.set_next_visible(True)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT), True)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL), True)
                
                self.modalmessageview.controller.set_cancel_visible(False)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT), True)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL), False)

                self.modalmessageview.controller.set_cancel_visible(True)
                time.sleep(ViewTestBase.standard_delay)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_NEXT), True)
                self.main.assertEqual(ViewTestBase.is_widget_visible(self.modalmessageview, ViewTestBase.ID_BUTTON_CANCEL), True)

                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()

    def test_modal_message_view_fun_and_games(self):
        """ View animation in the HTML area. """
        class TestCase(ModalMessageViewTestBase):
            def __init__(self, main, commonview, modalmessageview):
                ModalMessageViewTestBase.__init__(self, main, commonview, modalmessageview)

            def run(self):
                self.modalmessageview.display()
               
                self.testtext = """
                    <html>
                    <head>
                    <title>Hej</title>
                    </head>
                    <body>
                    <h1>Hej med dig lille kat</h1>
                    <img src="data:image/gif;base64,
                    R0lGODlhAAEwALICAP79+gAAAP///+isPgAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwHoAwAh
                    +QQACgD/ACwAAAAAAAEwAAAD/wi63K7hyUmrvTjrzbv/oBVEAFmWghmubOu+cOwGQnpCqCrvfO//
                    QJptRMyNgMikcslc0IiBgXQAbVqv2OxGGJ16j9qweNx8SrvnqY7MbrtBI/VX/a7b75V4Wn7G+/92
                    aF6DfYCGh1hQcVBUioiPkEGEVJNrkZeYK0R7UV1gmaChHk96lI1UoqmqF6V7X5arsaqMpo2fsrir
                    aIulsLm/kIJ8X8DFl8KvvcbLh8iTdMzReJ6Mip7S2G1HzrXEt9ngTKR6irbUXOHpS081rd1nXDbq
                    80Htz87o9Po+m/Dk/bb2CfxRBEIjJ98GKiSYb6HDdSnkPZxYTyLFizwSYtzIsSajx48gQ4ocSbKk
                    yZMoU6pcybKly5cwY8qcSbOmzZs4c+rcyXNmAgAh+QQBCgAEACwAAAAAAAEwAAAD/yi6vNHswUmr
                    vTjrzbv/YCg2T+kogTSubOu+cAybqimocq7vfN+nNKAN5ysaj8jkTThoDoQppXRKrXICzqwT+7R6
                    v2Blqsklm4nhtHodGpu3WXSnxq7bke5nfCuf3+6AgTple1pRIEKCiosrQE+EZFCMk5RHeXBaj5Wb
                    nDmOelyhh52kpSIleaJdpqytG5dvfK6ztBNMepGjtbuzkJCrvMGmZY63wMLIlL+xccnOjMRMxn3P
                    1WnLy8fW219R0WNQW0vc5FSosMy5NuXsRiWP3498WOvt9jzZ6ZnU9/0un/BEpdLlr6CnUeAcEDTI
                    EB8Kfg0jyqAhsWIRCRAtamSxcCqjx48gQ4ocSbKkyZMoU6pcybKly5cwY8qcSbOmzZs4c+rcybOn
                    z586EwAAIfkEAQoABAAsBgAAAEUAIgAAA7Moutz+MMpJq7046827/2AojmRpnmiqrmzbBLArwkMd
                    yxw9wIFS45lAbTfU9YAWIXGnZB6REqW02HwubsjmsEi1CnhQ3XYsPvKwMvOYSi6Xnt7r96vdaqc2
                    Uy+uuO27PFxLeSNnEXtqazZ2a3woZn2NZ4qLOy57kZSVilKXVnVOd2WEYZRVNmJMUF+SrJpFq0Zg
                    qaardKR4TGS2dYFSs4a8u7+Ytg9+obPGFGCZjssvcGg4CQAh+QQBCgAEACwMAAQARQAeAAADuii6
                    3P6QhUlnvDjrNkcfYLCNZFQFYOp1YumWVPqp62tv7BrLgnX/D1QoJgzhFi0gBrWjeXqUiyiplOac
                    FQVVEq0uZ84adOuFaYuq6JRctrbWTDQP2ia94elwt45Ts4pyT3xmY3lYbINuHzNyiIlSHisyeo92
                    QoCTaZUjgZk0jpsOgZghRqEamJ1DoKdIkZJYc60mMk2lOrNLc6SAuYoSmUy+UlrAqz7DEHtnyMlf
                    osXONlnSL2/V06xACQAh+QQBCgAEACwkAAEAUQAhAAADvyi63P4rSEhdqDhrKOe21yeOTSiYYkeu
                    5KWmLytrJooFQ27P/ON6FUlOt+sZFTgiBZccDopHXnIKRDKd06j2+qRGpsOs1shtCl1Pp043Joff
                    YbA63Jau7915p/msz/hpeIBzfX4sg4KEalA+EYaIioR7Iy6GJ3CRcVUfk35ncXmZhZaHoB2iRIyk
                    N6khp3pMVqstXScve7gnSGyznBOqXz9rwL0lxCVWdMWPrpvLz9DR0tPU1dbX2Nna29zd3hUJACH5
                    BAEKAAQALEMAAQBPACcAAAPDKLrc/jDKSau9OOvNu/9gKI5kaZ5oqq5s675wLM+0Etx4UG+5LgSD
                    oBC3uwCFQdttqPMVKbjhYnn7PTPHAVHA7DidrFwwqh17ra8sUntsanIx8dhHrP7A18ZyOJf2pnhP
                    am1MbGVNW3k2a21Zg1GBV2pjhYxSigyDfGtsQJGKe0iTm5+Yk6OMmBGonJeqDo+OfG6vepyElHa1
                    mZSWubsPuK1mwLB9lrrFvHOhycrLzFTPEI6LxNPQX9fYU9dt3LDJzhMJACH5BAEKAAQALGYADQA9
                    AB0AAAOtKLrc/jDKSau9OOvtgueg5X1jEF6mkj5mKwTDUK4n5EqzF39vzY4i3W5nGvhCwqEMdjtm
                    kjLl8sWjOnExKXGbYl4jsGxUG27tvjbxMCxN0tALtlguV7/hr+xMr7bjO31sdX5/DGUlfIljd3CD
                    gnOJjGiHbiNtRJJOdYN9c4VxmJyBZ58qiqKEpXluWlyqoGuCOT2vsF5ltLm1pktdLkC7DbgzVsEd
                    TMDFxseZfwkAIfkEAQoABAAsbwAQAEcAGwAAA7Ioutz+MMpJq71YhLWz/9TWXUEJnk1pksHgrmjM
                    viUt31L7ukPd48DUrqcjjoI3nZLn+6WOyEqRx2TCFKrojMolMrEqqBbSmVKbtKJ47BBpzNW4mp0r
                    a6xh+ZkeEZW7cUN7fClYWF1hcHKEDG53gIGIL4wcR3B5eIuUZJBqaWdrm5cikFyhlD5hb6Vfmw+p
                    HIJoTq6Fj1lKuKq1bas/ZSanvI1/iSYDwxlZhhrJHjDLzgIJACH5BAEKAAQALHMAEQBpABoAAAO9
                    KLrc/jDKSau9OOvNuw+gJwRiaTpgyKlnK5IKnKVu/Y2slcqtzNsT3yUwKP5OpCNQksxBQEWjsrNb
                    zqJOBpEYNVq/Fa7YCeUaveD0YyuVLqADs1hNf8fP7eS9u5/WgXJYXXBmfHF/amKCbXw7g4hphXuT
                    koaHkF+VeJOWaJhLmp2WVZ9WgYOdpKWmenmhj6umb1twolJ+sS8xNDiaZW65NU29MDvGI43BLjS4
                    s01RI8rLzSi7b9KrKrzY3EAJACH5BAEKAAQALJ4ADwBRAB4AAAO8KLrc/jDKKcOyNOvNncVdKI5B
                    CY5oqq5s675w3JhynZVDXtr8HOi4wa5n++lyQuOJ+FICkTRm8yiEGoWepfSG7FKTy+hWE/R2yxjT
                    cExxmpNeE5R9E/zK8fNXS/cpPlZqUF9YfREfaW+EV3OGHn9/ZmqMeYWOF3aRioSVfH1rdpJygYOW
                    lxCUpVVgZ56noXGAm42vWUA7eIqur0EgbnBntaiaNErFasLDvZl2uMlkiZM4u8+oa2nVItegNQkA
                    IfkEAQoABAAsuAAEAEcAKQAAA8soutz+MMoZVp046y3q5WCYBeQnnmiqjmW7vuxADrQM31Zb16WN
                    wz3abCc0/VKzgEzH6xiPIWUxuXtCT8og0ea6inpSYtfqpQy3RUWpzLmcq6SOky1Zy8HhqppMV++d
                    eXk8cX0RLng8W3yFFnuBaISMhkxngouSajJLiYqYdVJhoWKehmhCkKSlYpyaPqkOooJwl5hZRZBN
                    rw2hWqdLtJKhmb7Curt2w61ZxrB+FoPIzDlGXdIQkY1y1m3R2xhu3tzh4+Tl5ufo6eoYCQAh+QQB
                    CgAEACzYAAQAJwAiAAADkSi63P4wykmrvThrHMLOnfdZoThOwaCaAnu2qbp63buksTy7X7fqspoN
                    9ysKX7Gc0abwDZQ4Xq8FfEo5pad29hyFjNVgCwQuVo+k5C5svUaI5q0uRC4HgW6JM6eUd8l8d4Jo
                    aWZ9O3kQUGxBiYpUgmGEaU2HVjMbInSWJSUgaF9zYzWOJKI0NEwwnZ2qrq+wGAkAIfkEAQoABAAs
                    3wAIACAAGwAAA4UoutzOIbxJW5Q1u4u1D0PYeVQUimQFgucQpRvLtiO8ujQKK3eLuzueyDerfWYu
                    JFDDWSlNS1VP6dOVZKfnJTrp4aDVl3T4KxMvArEQSzZ/0zy48Ec1NyX4fJsq48TFW21haAtqeGll
                    dUlMc4oiRl2BZ49BiIN3O5IoeXKZNHAvkEx3TRMJACH5BAEKAAQALN4ACAAhABsAAAN8KLrc/pCF
                    N6NdIbdcb+Qd6H1DGZZDN0poKgStur4xGM9Ya+ouTu8xlGwEAwqFvl9KV+zhmrybD8YBVofEqDSp
                    5GGfGaGG62h+uVAnWRGurdla9UyTvo2JbKi+cpZcqTZhIhdpZkdbH1UpIDaLjFhtSC9jhS+TZY8i
                    mZkPCQAh+QQBCgAEACzeAAgAGwAbAAADeii6EsEwyuXcvNENi7seW0dVFGhyWEVqgRmm7Vm54JPS
                    X3zebhvrrwmQ1gtKgMNazVNU9phEGYo3WzZsomTNhvWopFORQkcWM8hlc4OoUms33Ysl+bvKx0g0
                    /Dj7bE5/D3FrTX9KPlcoVYaBOHMNVzhbb22JKm2XmQwJACH5BAEKAAQALN4ACAAbABsAAANyKLrc
                    /i48CWG47dKa9dIbpwRDuZGm+JUpmqruAMahxbLxy7k5D+M3Ge5nCvpEueAQKbsUm6SaTXmTVpJL
                    1cdpwmgj2S+jJx73rDuqbifBttACCvfZHU00Tahz78kYgXRrI3h6M10zbWRxXmSJiDOLj5AJACH5
                    BAEKAAQALN4ACAAbABsAAAN5KLoSwTDK5dy80Q2Lux5bR1UUaHJYRWqBGabtWbngk9JffN5uG+uv
                    CZDWC0qAw1rNU1T2mEQZijdbNmyiZM2G9aikU5FCh+2KyWVxCUdSazdm4SP5u164SDT8OPtsTn9z
                    GTiASj5XKFV/hmyCc0M/fo14KpWIlW0KCQAh+QQBMgAEACzeAAgAGwAbAAADeCi63P4uPAlhuO3S
                    mvXSG6cEQ7mRpviVA3axYYXCL9uqpGa3bzzZuRYQZzLNUL5IjXLEiGY3ATTKgTJ3yQhsNFRxjV3v
                    qMbLElNibTgtnTrT07VMEt++I2MgkDnR9UA8ZT5uMIVybUsggIIgbVhjV0N0ipSTlYoCCQAh+QQB
                    KAAEACzeAAgAGwAbAAADdygS3KEwyikfvfRZzOPunVY54BKWTlOVIcm+8Blnw4dWQw0HusLnNs4v
                    lasFRz/goqg7LoZMI6MoWvGSRl+UMdHUolIs1/Odgs8qbRl7BlaXYiP6alIn7+avcswGbrcmFn1+
                    f1yGEH0aKYYqH4s+gWmCTk+PlSQJACH5BAEoAAQALOAACAAXABoAAANXKLpC/jDKJmts1GqXt8UX
                    o3UVCSlRcHrpgzrqFsQSrdkQjcNzn/eBQcBU68SGHhWS9fARdrfgIEdMVaFUzWB7EwSZJbDlCyY7
                    ps+WBA1ht8XwuJxZLScAACH5BAEKAAQALNYACAAhACMAAANsSLoc/oHJSSuJNtuItZ/dJ15kA42N
                    Fo6OJKSo9SqtMsf27eKM3i8+j2BI7BEFgcEqhvwRlDzbcxktPqMuZAiKbVIGXSptyEN6sV90hvth
                    qxRgRlwirlDr6rx+T+Pv/ICBgoOEIh14PByCJ3sJACH5BAEKAAQALL4ACAA7ACMAAAOxKLrc/izA
                    uSS9ODa7df4YFwycMlolqD7oQFZuuqqBLIwxnG/27NQbVw63M/V8EOBNGKsxX0qkj/h8HqUfIpUk
                    vGIrtZTW2gVHv5cxU4tGbqvFNo3k5NJP8il8nXfvX31udXcegXNxN4mGWVYVJosZb1CPkGl/KJUT
                    QJJDXoYoVKGYmYU3anSliqptYl1lqYmeK2E6doCmNmGyka13iGeQusKbw7qkx8jJysvMzc7P0McJ
                    ACH5BAEKAAQALKAACABYABwAAAO4KLrc/jDKSau9OOvNu/9gKI4kFpxlCp5Di6rw9Z6BG8R4VA+s
                    2/K54IL26+1uwtGLseM1j66kiNYg+n5YoPRDmxWb2e+W+wU7w1EBcrx5Xt9ZVJqdAUNt3aLaRjeF
                    7WhpKGt9E3doXWZaSISFEId/gXpqlAprjX2HVniKhEuUmHQ9eJGIFF2Oe5J6o5OpMqt8eU6vdXFP
                    lkN8tSaoqKBGubwdN1SgssMexYk1jKHJMsJqn9DKS9QRCQAh+QQBCgAEACyKAAwAawAYAAADuCi6
                    3P4wykmrvdiG7ILnYCgqX+iVY6pGJ7iRayyTLda+cz4Gw4BSOJyusxAOab3eD3I7sgTGIy+p9LGm
                    Vud1mZtifT7jqZrVmiXepDe4rkbPcBKZytvUsel3XPulgsN0amp7hF8nfYBzXoR7iH5/kH16jEOO
                    fniPSpSNY5mej5ObUp2gl4OijZ9qd4JcqDqkqmMep6+jboiHNLRltl2XUIc/ulC9vjF2SnauRMHH
                    M2wwNqHPJiXM1QkAIfkEAQoABAAsZgAMAGoAGAAAA7koutz+ML5Apb046y3p+BUnjmQphFQABmbr
                    vurggZ/83niW1nPM5kDYjxGTFX2goLKUauxotahtSdWkULyiNFuNDIfB4yy7TZ66EBY4zIO6pZXB
                    Ge24LrXI1ZU3D9H/Wm+BUT8fc39oeWVXg4UKa4hKiluDcI+HkXduT3qNRJCZOGNGjWVToXSVlCee
                    qICmhGpPfq6JcEeXjyu1iIysKL9moLypv5c+asSusoxTtMqRP1/P0KhNj8OhCQAh+QQBCgAEACxC
                    AAwAZwAYAAADuSi63P4wykmrvTjgF/r+YKh4YUeKaAqZoDaqcCyY7sXWct4Ow0nVON1vEUR1eDzf
                    ylMUTjTNEhIZ6EWOSeeHpqr2plWlKZnVmp/k9AnLtp7fnDTYGvZ+v1G40z5V8+2AeoJ/Y31Yd3d5
                    gjl8fVSPcmWLb42OiJWSk2c0lp2Wipo6nJ+GVKGDnlR1pkqnooepY2uuWiSrhiwzuaC0O25QND5c
                    M8S9MlBJwLzFrcYlRC7LDM3OIrvS1QoJACH5BAEKAAQALB4ACABgABwAAAPBGLK6/jDKSau9WKiW
                    u//gFYxcaJ5oqq5s25FuLD/jYI9zruJaPfC64OsWsP2MJaFSQkIWj7+lEuh4/qzPrDQIo/mgxnBy
                    2yLxsNaweExeodNXNZLRjmFvYjlRE62z0llOZnNZbH4fcGCKag04hocZgXpXTYx0GpAmknqJlpiP
                    mRWblURfc1WgoUyApnicqimdchuJqbAik2s9PlS3IXBfjVV7vihmtGe7N5fFf7t0hZjNZbuDjbbT
                    HsIMvdllQN2hCQAh+QQBCgAEACwFAAEATwAjAAADvxiyuv4syNmgvThrrGoOQzhIo7edaCpQXwmK
                    IWWqdP29cO7afO9IKx0MBPQZezihaHZsWlhJ5cBJ/URI0lB1C5lEsiKueNVxSRljLjYaS64W4bRx
                    ImRPtYKp/MjGYbUwe01/Q0JwglRRfnaITnZ2eI18OX46RZI+j1JMmCkvmpSdSISUI0uiPJWWnyOo
                    qWuUdDGuNap+DKe0NEQxuVecujeyrT9owaOXV8dIxg/Jyyos0I7N03zA1tna29zd3twJACH+H09w
                    dGltaXplZCBieSBVbGVhZCBTbWFydFNhdmVyIQAAOw==" alt="small_cat"/>
                    </body>
                    </html>
                    """
                self.modalmessageview.controller.set_text(self.testtext) 
                time.sleep(ViewTestBase.long_delay)

                time.sleep(ViewTestBase.long_delay)
                self.modalmessageview.destroy()
                self.commonview.destroy()

        self.test = TestCase(self, self.commonview, self.modalmessageview)
        self.test.start()
        self.commonview.display()


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
