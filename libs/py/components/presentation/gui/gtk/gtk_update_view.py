"""

GTK based update view.

"""
import lib.dictionary

from components.presentation.gui.gtk.area.gtk_header_area import GtkHeaderArea
from components.presentation.gui.gtk.area.gtk_button_area import GtkButtonArea
from components.presentation.gui.gtk.area.gtk_phase_status_area import GtkPhaseStatusArea
from components.presentation.gui.gtk.area.gtk_selection_area import GtkSelectionArea
from components.presentation.gui.gtk.area.gtk_progress_area import GtkProgressArea
from components.presentation.gui.gtk.area.gtk_text_area import GtkTextArea

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gdk

# import gobject
# import gtk.gdk

from components.presentation.update import UpdateView
from components.presentation.gui.gtk.gtk_base_view import GtkBaseView

class GtkUpdateView(GtkBaseView, UpdateView):
    """ A cocoa based update window. """
    def __init__(self, model, handles, name, configuration, unittest=False):
        GtkBaseView.__init__(self, "G/On", configuration)
        UpdateView.__init__(self, model, handles, name, configuration)
        
        # For Unit tests only - needed for thread balancing.
        self.unittest = unittest
        self.updatecount = 0

        # Currently displayed settings.
        self.currentheaderbanner = 0 # GIRI_GUI_INSTALL_BANNER 
        self.currentdetailsstate = True
        self.currentmode = self.controller.ID_MODE_INFORMATION_FREE_TEXT
        self.currentinfosystemdesc = self.model.info_text
        self.currentwindowframetext = "G/On"

        self.create_window()
        self.header = GtkHeaderArea(self.container, self.controller, self.configuration)
        self.phaseboxtexts = GtkPhaseStatusArea(self.container, self.controller, self.configuration)
        self.progress = GtkProgressArea(self.container, self.controller, self.configuration)
        self.info = GtkTextArea(self.container, self.controller, self.configuration)
        self.selection = GtkSelectionArea(self.container, self.controller, self.configuration)
        self.buttons = GtkButtonArea(self.container, self.controller, self.configuration)

#         To be added at a later time.
#
#        # Create a vertically split area for splitting info and selection 
#        self.vpaned = gtk.VPaned()
#        self.container.add(self.vpaned)
#        self.vpaned.pack1(gtk.HBox())
#        self.vpaned.pack2(gtk.HBox())
#        self.progress = GtkProgressArea(self.vpaned.get_child1(), self.controller, self.configuration)        
#        self.info = GtkTextArea(self.vpaned.get_child1(), self.controller, self.configuration)
#        self.selection = GtkSelectionArea(self.vpaned.get_child2(), self.controller, self.configuration)
#        self.vpaned.get_child1().connect("size_allocate", self._label_resize)        
                
        # Set default sizes
        self.resize(450, 500) 
        # Get ready for action!
        self.update()

    def _on_next_clicked(self, sender):
        try:
            self.controller.set_next_clicked(True)
        except:
            print "Exception caught in gtk_update_view._on_next_clicked()"

    def _on_cancel_clicked(self, sender):
        """ When cancel is clicked - set state. """
        try:
            self.controller.set_cancel_clicked(True)
        except:
            print "Exception caught in gtk_update_view._on_cancel_clicked()"

    def update(self):
        """ Update all areas and check for button pushes. """
        if self.controller.get_gui_should_update_on_alerts():
            self.header.update(self.model)
            self.phaseboxtexts.update(self.model)
            
            # Update the info text to reflect the button descriptions,
            #  when in button text mode...
            if self.model.info_mode == self.controller.ID_MODE_INFORMATION_BUTTON_TEXT:
                if self.model.info_text != self.model.button_info_text:
                    self.model.info_text = self.model.button_info_text
            
            self.info.update(self.model)
            self.progress.update(self.model)
            self.selection.update(self.model)
            self.buttons.update(self.model)

            # Window frame title...
            if self.currentwindowframetext != self.model.window_frame_text:
                self.view.set_title(self.model.window_frame_text)
                self.currentwindowframetext = self.model.window_frame_text

            # Handle areas visibilities...
            if self.currentmode != self.model.info_mode:
                if self.model.info_mode == self.controller.ID_MODE_INFORMATION_PROGRESS:
                    self.info.hide()
                    self.progress.show()
                elif self.model.info_mode == self.controller.ID_MODE_INFORMATION_FREE_TEXT \
                    or self.model.info_mode == self.controller.ID_MODE_INFORMATION_BUTTON_TEXT:
                    self.progress.hide()
                    self.info.show()
                self.currentmode = self.model.info_mode
            if self.currentdetailsstate != self.model.show_selection_list:
                if self.model.show_selection_list:
                    self.resize(450, 500)
                    self.selection.show()
                    self.info.expands(False)
                    self.progress.expands(False) 
                else:
                    self.resize(450, 270)
                    self.selection.hide()
                    self.info.expands(True)
                    self.progress.expands(True)
                self.currentdetailsstate = self.model.show_selection_list
            # Handle button events...
            if self.buttons.cancelbutton.set_data_button_clicked:
                self.buttons.cancelbutton.set_data_button_clicked = False
                self._on_cancel_clicked(None)
            if self.buttons.nextbutton.set_data_button_clicked:
                self.buttons.nextbutton.set_data_button_clicked = False
                self._on_next_clicked(None)
    
    def hide_in_gui_thread(self):
        self.view.hide()
        return False
        
    def hide(self):
        """ Hide the update window. """
        GObject.idle_add(self.hide_in_gui_thread)


#
# -------------------------------------------------------------------------------
#
        
if __name__ == '__main__':
    
    #from components.presentation.gui.gtk.gtk_update_view import GtkUpdateView
    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.common import CommonModel
    from components.presentation.update import UpdateModel

    dictionary = lib.dictionary.Dictionary()

    commonmodel = CommonModel()
    updatemodel = UpdateModel(dictionary)
    
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary
    
    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    updateview = GtkUpdateView(updatemodel, commonview.handles, 'updateview', configuration)

    updateview.controller.set_info_headline("Update packages")
    
    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Install',  'state': updateview.controller.ID_PHASE_ACTIVE},
                                          {'name': 'Restart',  'state': updateview.controller.ID_PHASE_PENDING},
                                          {'name': 'Done',     'state': updateview.controller.ID_PHASE_PENDING}])

    updateview.controller.set_selection_list([
                       {'id': 0, 'selected': True,  'icon': 'g_package_default_32x32.bmp', 'name': 'G/On Client for Windows',    'details': 'System: Windows, Version: 5.2, Size: 32 MB, Extra: A whole lot of extra text that will make the line wrap or something', 'description': 'Greatest remote solution ever - win style'},
                       {'id': 1, 'selected': False, 'icon': 'g_package_linux_32x32.bmp', 'name': 'G/On Client',    'details': 'mac', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 2, 'selected': True,  'icon': 'g_package_mac_32x32.bmp', 'name': 'G/On Client',    'details': 'linux', 'description': 'Greatest remote solution ever - linux style'},
                       {'id': 3, 'selected': False, 'icon': 'g_package_win_32x32.bmp', 'name': 'Extra files',    'details': 'all', 'description': 'Extra files for carrying around'},
                       {'id': 4, 'selected': True, 'icon': None,  'name': 'Remote Desktop', 'details': 'mac', 'description': 'Remote desktop for the mac.'},
                       {'id': 5, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'FileZilla',      'details': 'win', 'description': 'FTP client for windows'},
                       {'id': 6, 'selected': True, 'icon': 'g_package_default_32x32.bmp',  'name': 'Citrix ICA client', 'details': 'win', 'description': 'Citrix client for windows'},
                       {'id': 7, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Citrix ICA client', 'details': 'mac', 'description': 'Citrix client for mac'},
                       {'id': 8, 'selected': True, 'icon': 'g_package_default_32x32.bmp',  'name': 'Navision client', 'details': 'win', 'description': 'Navision client for windows'},
                       {'id': 9, 'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'TightVNC Viewer', 'details': 'win', 'description': 'Remote desktop connection to macs'},
                       {'id': 10,'selected': True, 'icon': 'g_package_default_32x32.bmp',  'name': 'G/On Management', 'details': 'win', 'description': 'Total world domination'},
                       {'id': 11,'selected': False, 'icon': 'g_package_default_32x32.bmp', 'name': 'Packaged IE', 'details': 'win', 'description': 'Exploder thingy'}
                   ])

    # Info commands
    # updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_PROGRESS)
    updateview.controller.set_info_free_text("Hejsa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris odio elit, consequat quis elementum eget, dignissim vel dui. Aenean tincidunt mauris sed mi lobortis luctus. Maecenas ultrices velit et lorem iaculis nec placerat tellus facilisis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris consectetur ornare sem. Curabitur ultricies, elit id convallis pretium, felis augue tempus magna, et dictum eros leo a orci. Vivamus varius pellentesque pretium. Praesent bibendum sapien quis sem molestie non tincidunt ante aliquam. Vestibulum volutpat pellentesque ullamcorper.")
    updateview.controller.set_info_progress_complete(65)
    updateview.controller.set_info_headline("Downloading")
    updateview.controller.set_info_progress_subtext("Package: G/On client for windows")
    
    updateview.controller.set_show_selection_list(True)
    updateview.controller.set_selection_method(updateview.controller.ID_SELECTION_MULTIPLE) 
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    updateview.controller.set_info_headline("Information")
#    updateview.controller.set_cancel_description("Click cancel to exit update.")
#    updateview.controller.set_next_description("Click next to proceed to the next phase.")
#     updateview.controller.set_info_system_description("Nu er det jul igen og nu er det jul igen ....")
#    updateview.controller.set_info_system_description(None)
    updateview.controller.set_banner(updateview.controller.ID_BANNER_UPDATE)
    updateview.controller.set_window_frame_text("Hejsa")
    updateview.controller.set_info_progress_mode(updateview.controller.ID_MODE_PROGRESS_UNKNOWN)
    updateview.controller.set_selection_highlighted_by_id(9)

    updateview.controller.set_cancel_label("Go Away")
    updateview.controller.set_next_label("Go On")
    
    # Button commands
    updateview.controller.set_cancel_allowed(False)
    updateview.controller.set_next_allowed(True)
    
    updateview.display()
    commonview.display()

