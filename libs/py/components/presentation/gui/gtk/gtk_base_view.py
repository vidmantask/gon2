from components.presentation.gui.gtk import gtk_tools
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gdk, Gio

"""

Base Class for GTK based views.

"""


class GtkBaseView:
    def __init__(self, windowtext, configuration):
        self.windowtext = windowtext
        self.configuration = configuration
        self.margin = 6

        self.width = 100  # Arbitrary start value
        self.height = 100  # Arbitrary start value

    def create_window(self):
        """ Create a basic view. """
        self.view = Gtk.Window(title=self.windowtext)
        self.view.set_icon_from_file('%s/%s' % (gtk_tools.locate_image_path(self.configuration.gui_image_path),
                                                'giritech.ico'))
        self.view.set_resizable(True) 
        self.view.set_deletable(False)

        self.view.set_type_hint(Gdk.WindowTypeHint.DIALOG)
        self.view.connect("window_state_event", self._on_window_state_change)

        self.container = Gtk.VBox()
        self.view.add(self.container)

    def _on_window_state_change(self, window, event):
        """ Handle window state event changes. """
        if hasattr(self.controller, 'set_minimized'):
            state = event.new_window_state
            if state & Gdk.WindowState.ICONIFIED:
                self.controller.set_minimized(True)
            else:
                self.controller.set_minimized(False)
    
    def centre_window_on_desktop(self):
        """ Make sure the view is centered on the desktop. """
        self.view.set_position(Gtk.WindowPosition.CENTER)
    
    def display(self, views=None):
        """ Pass a display message, so that it is done in the GUI thread. """
        self.centre_window_on_desktop()
        GObject.idle_add(self._display_in_gui_thread)

    def _display_in_gui_thread(self):
        """ Show the view. """
        self.view.show_all()
        return False

    def hide(self):
        """ Hide the window. """
        GObject.idle_add(self.hide_in_gui_thread)

    def hide_in_gui_thread(self):
        """ Hide the window using commands in the GUI thread. """
        # self.view.hide_all()
        self.view.hide()
        return False

    def destroy(self):
        """ Destroy the view. """
        pass

    def resize(self, width=100, height=100):
        """ Set the preferred size of the window. 
        
            Also adjust minimum and maximum sizes:
            - minimum is set to the given sizes.
            - maximum is smaller than screen size.
        """

        screen = Gdk.Screen.get_default()

        self.width = screen.get_width() - 50 if width > screen.get_width() else width
        self.height = screen.get_height() - 100 if height > screen.get_height() else height
        _max_width = screen.get_width() - 50 if screen.get_width() - 50 < 1500 else 1500
        _max_height = screen.get_height() - 100

        self.view.set_size_request(self.width, self.height)
