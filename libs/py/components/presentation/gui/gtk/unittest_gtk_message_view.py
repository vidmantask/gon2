"""
Unittest for the structure of the Gtk Message View.
"""
import sys
import time
from threading import Thread

import unittest
from lib import giri_unittest

from components.presentation.common import CommonModel
from components.presentation.message import MessageModel
if sys.platform == 'linux2':
    import gtk
    import gobject
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.gui.gtk.gtk_message_view import GtkMessageView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path =  giri_unittest.get_image_path()
        self.dictionary =  giri_unittest.get_dictionary()
gui_configuration = GuiConfiguration()        



class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """
    def __init__(self, commonmodel, messagemodel, commonview, messageview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.messagemodel = messagemodel
        self.commonview = commonview
        self.messageview = messageview
        
    def shutdown_in_gui_thread(self):
        gtk.main_quit()
        return False


class GtkMessageViewTest(unittest.TestCase):
    """ Collection of tests for the GTK based view of message. """

    def setUp(self):
        self.commonmodel = CommonModel()
        self.messagemodel = MessageModel()
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.messageview = GtkMessageView(self.messagemodel, self.commonview.handles, 'messageview', gui_configuration)
        self.test = ThreadedTestRunner(self.commonmodel, self.messagemodel, self.commonview, self.messageview)

    def tearDown(self):
        self.test.join()

    def test_gtk_message_view_init(self):
        """ Gtk Message View initialization. 
        
            Libnotify does not like empty messages. So we always
            add content in this test (though it could be blank).
            I guess this is OK since we would probably never want
            to display empty messages anyway. 
        """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.messageview.controller.set_message(' ', ' ')
            self.messageview.display()
            time.sleep(1.0) # Wait so we get a chance to see the window
            self.assertEqual(self.messagemodel, self.messageview.model)
            self.assertEqual(self.messageview.headline, " ")
            self.assertEqual(self.messageview.message, " ")        
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop


    def test_gtk_message_view_set_message(self):
        """ Update of Gtk Message View. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.messageview.controller.set_message("TEST", "Message set by test")
            self.assertEqual(self.messageview.headline, "TEST")
            self.assertEqual(self.messageview.message, "Message set by test")        
            self.messageview.display()
            time.sleep(0.1)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_message_view_subscription(self):
        """ Update of Gtk Message View while visible. 
        
            This may result in a new message getting 
            displayed. But that is OK.
        """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.messageview.controller.set_message("TEST", "Message set by test")
            self.assertEqual(self.messageview.headline, "TEST")
            self.assertEqual(self.messageview.message, "Message set by test")        
            self.messageview.display()
            time.sleep(1.0)
            self.messageview.controller.set_message("TEST 2", "Message changed by test")
            self.assertEqual(self.messageview.headline, "TEST 2")
            self.assertEqual(self.messageview.message, "Message changed by test") 
            self.messageview.display()
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        

#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = sys.platform not in ['linux2']
GIRI_UNITTEST_TIMEOUT = 60 * 4


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
