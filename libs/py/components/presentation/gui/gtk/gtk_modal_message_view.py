from components.presentation.gui.gtk.area.gtk_button_area import GtkButtonArea
from components.presentation.gui.gtk.area.gtk_html_area import GtkHtmlArea
from components.presentation.gui.gtk.gtk_base_view import GtkBaseView
from components.presentation.modal_message import ModalMessageView
import lib.dictionary
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
Gtk version of the modal message view.

"""


class GtkModalMessageView(GtkBaseView, ModalMessageView):
    
    def __init__(self, model, handles, name, configuration, unittest=False):
        GtkBaseView.__init__(self, "G/On", configuration)
        ModalMessageView.__init__(self, model, handles, name, configuration)
 
        # Currently visible model
        self.current_size = 'small'
 
        self.create_window()
        self.view.set_modal(True)
        self.centre_window_on_desktop()
        # For the event where we could not remove the frames close button...
        self.view.connect("delete_event", self._on_close)

        self.htmlview = GtkHtmlArea(self.container, self.controller, configuration)
        self.buttons = GtkButtonArea(self.container, self.controller, configuration)
        
        self.update()
        
    def update(self):
        GObject.idle_add(self.update_in_gui_thread)
        self.htmlview.update(self.model)
        self.buttons.update(self.model)
        # Handle button events...
        if self.buttons.cancelbutton.set_data_button_clicked:
            self.buttons.cancelbutton.set_data_button_clicked = False
            self._on_cancel_clicked(None)
        if self.buttons.nextbutton.set_data_button_clicked:
            self.buttons.nextbutton.set_data_button_clicked = False
            self._on_next_clicked(None)
        
    def update_in_gui_thread(self):
        """ Update the message window. """
        if self.current_size != self.model.size:
            if self.model.size == self.controller.ID_LARGE_SIZE:
                self.resize(450, 600)
            elif self.model.size == self.controller.ID_SMALL_SIZE:
                self.resize(450, 200)
            self.current_size = self.model.size

    def _on_close(self, widget, data):
        """ When the user clicks the frame close. """
        self._on_cancel_clicked(None)

    def _on_cancel_clicked(self, sender):
        """ When the cancel button is clicked. """
        self.controller.set_cancel_clicked(True) 
        self.hide()

    def _on_next_clicked(self, sender):
        """ When the ok button is clicked. """
        self.controller.set_next_clicked(True)
        self.hide()

    def hide_in_gui_thread(self):
        self.view.hide()
        return False
        
    def hide(self):
        """ Hide the window. """
        GObject.idle_add(self.hide_in_gui_thread)


#
# -------------------------------------------------------------------------------
#

if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions    
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.common import CommonModel
    from components.presentation.modal_message import ModalMessageModel

    commonmodel = CommonModel()
    modalmessagemodel = ModalMessageModel(lib.dictionary.Dictionary())
    
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()

    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    modalmessageview = GtkModalMessageView(modalmessagemodel, commonview.handles, 'modalmessageview', configuration)

    modalmessageview.display()
    
    #  testtext = "Test of modal window text"
    testtext = "Test of modal window text. This is a text of several lines in one window. " \
               "Up to three lines are supported!<br>"

    modalmessageview.controller.set_text(testtext)
    modalmessageview.controller.set_cancel_allowed(True)
    #modalmessageview.controller.set_cancel_visible(False)
    modalmessageview.controller.set_next_allowed(True)
    #modalmessageview.controller.set_next_visible(False)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_SMALL_SIZE)
    modalmessageview.controller.set_size(modalmessageview.controller.ID_LARGE_SIZE)
    modalmessageview.controller.set_size(modalmessageview.controller.ID_SMALL_SIZE)
    
    commonview.display()
