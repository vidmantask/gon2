from components.presentation.gui.gtk.gtk_base_view import GtkBaseView
from components.presentation.gui.gtk.area.gtk_header_area import GtkHeaderArea
from components.presentation.gui.gtk.area.gtk_button_area import GtkButtonArea
from components.presentation.gui.gtk.area.gtk_login_area import GtkLoginArea
from components.presentation.login import LoginView
import lib.dictionary


"""
Gtk version of the GUI for login.
"""


class GtkLoginView(GtkBaseView, LoginView):
    """ A Gtk based view of the login. """
    def __init__(self, model, i_handles, name, i_configuration):
        GtkBaseView.__init__(self, "G/On Login", i_configuration)
        LoginView.__init__(self, model, i_handles, name, i_configuration)

        self.create_window()
        self.resize(375, 300)

        self.header = GtkHeaderArea(self.container, self.controller, self.configuration)
        self.login = GtkLoginArea(self.container, self.controller, self.configuration)
        self.buttons = GtkButtonArea(self.container, self.controller, self.configuration)

        self.controller.configure(self.configuration)
        self.update()

    def _clear_fields(self):
        self.login.passwordfield.set_text('')
        self.login.newpasswordfield.set_text('')
        self.login.confirmnewpasswordfield.set_text('')

    def update(self):
        """ Update the areas. """
        self.header.update(self.model)
        self.login.update(self.model)
        self.buttons.update(self.model)
        # Handle button clicks...
        if self.buttons.cancelbutton.set_data_button_clicked:
            self.buttons.cancelbutton.set_data_button_clicked = False
            self._on_cancel_clicked(None)
        if self.buttons.nextbutton.set_data_button_clicked:
            self.buttons.nextbutton.set_data_button_clicked = False
            self._on_next_clicked(None)

    def _on_next_clicked(self, _sender):
        """ Handle a click on the OK button. """
        username = self.login.usernamefield.get_text().decode('utf-8', 'ignore')  # gtk returns in 'utf-8'
        password = self.login.passwordfield.get_text().decode('utf-8', 'ignore')  # gtk returns in 'utf-8'

        self.view.hide()
        if self.controller.get_state() == self.controller.ID_STATE_CHANGE_LOGIN:
            if self.login.newpasswordfield.get_text() == self.login.confirmnewpasswordfield.get_text():
                self.controller.set_password_request(self.login.newpasswordfield.get_text().decode('utf-8', 'ignore'))
                self.controller.set_credentials(username, password)
                self._clear_fields()
                self.controller.set_next_clicked(True)
        else:  # Standard login
            self.controller.set_credentials(username, password)
            self._clear_fields()
            self.controller.set_next_clicked(True)

    def _on_cancel_clicked(self, _sender):
        """ Handle a click on the cancel button. """
        self.view.hide()
        self._clear_fields()
        self.controller.set_cancel_clicked(True)


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
if __name__ == '__main__':

    # import sys
    # sys.path.append('../../../../')

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.login import LoginModel
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.common import CommonModel

    def test():
        if loginmodel.requestedpassword is not None:
            print("Reading model data: (username=" + loginmodel.username
                  + " password=" + loginmodel.password
                  + " new pass=" + loginmodel.requestedpassword
                  + ")")
        else:
            print("Reading model data: (username=" + loginmodel.username
                  + " password=" + loginmodel.password
                  + " new pass=None"
                  + ")")

        loginview.controller.clear_credentials()

    dictionary = lib.dictionary.Dictionary()

    # Create models
    commonmodel = CommonModel()
    loginmodel = LoginModel(dictionary)

    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary
    # configuration.gui_login_default_button = configuration.OPTION_GUI_DEFAULT_BUTTON_VALUE_NEXT

    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    loginview = GtkLoginView(loginmodel, commonview.handles, 'loginview', configuration)
    loginmodel.subscribe(test)

    loginview.controller.set_greeting("Welcome to the Gtk Login view test. "
                                      + "There is a lot more text to write "
                                      + "but I just can't think of anything right now.")
    # loginview.controller.set_state(loginview.controller.ID_STATE_CHANGE_LOGIN)
    loginview.controller.set_state(loginview.controller.ID_STATE_LOGIN)
    loginview.controller.set_suggested_username('Grethe')
    # loginview.controller.set_lock_username_field(True)
    # loginview.controller.lock_state(True)

    # Run
    loginview.display()
    commonview.display()
