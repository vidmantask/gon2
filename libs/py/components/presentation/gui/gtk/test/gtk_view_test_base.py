""" 

Specific GTK related test functionality.

"""
#from components.presentation.gui.gtk.area.gtk_login_area import GtkLoginArea
#from components.presentation.gui.gtk.gtk_login_view import GtkLoginView

import gtk
import gtk.gdk
import gobject

ID_TRAY_ICON = "tray-icon"

ID_LOGIN_TEXTFIELD = "login-text-field"
ID_PASSWORD_TEXTFIELD = "password-text-field"
ID_NEW_PASSWORD_TEXTFIELD = "new-password-text-field"
ID_NEW_PASSWORD_CONFIRM_TEXTFIELD = "confirm-new-password-text-field"

ID_PROGRESS_BAR = "progress-bar"
ID_PROGRESS_BAR_HEADLINE = "progress-bar-headline"
ID_PROGRESS_BAR_SUBTEXT = "progress-bar-sub-text"

ID_FREE_TEXT_HEADLINE = "free-text-headline"
ID_FREE_TEXT = "free-text"

ID_SELECTION_LIST = "selection-list"

ID_BUTTON_NEXT = "button-next"
ID_BUTTON_CANCEL = "button-cancel"

ID_PACKAGE_PLATFORM_IMAGE = "g_package_linux_32x32.bmp"

def set_widget_text(window, widget_id, text):
    """ Set the text in a text field widget. """
    if widget_id == ID_LOGIN_TEXTFIELD:
        window.login.usernamefield.set_text(text)
    elif widget_id == ID_PASSWORD_TEXTFIELD:
        window.login.passwordfield.set_text(text)
    elif widget_id == ID_NEW_PASSWORD_TEXTFIELD:
        window.login.newpasswordfield.set_text(text)
    elif widget_id == ID_NEW_PASSWORD_CONFIRM_TEXTFIELD:
        window.login.confirmnewpasswordfield.set_text(text)

def get_widget_text(window, widget_id):
    """ Get the text in a text field widget. """
    if widget_id == ID_LOGIN_TEXTFIELD:
        return window.login.usernamefield.get_text()
    elif widget_id == ID_PASSWORD_TEXTFIELD:
        return window.login.passwordfield.get_text()
    elif widget_id == ID_PROGRESS_BAR_HEADLINE:
        return window.progress.headline.get_text()
    elif widget_id == ID_PROGRESS_BAR_SUBTEXT:
        return window.progress.progressbar.get_text()
    elif widget_id == ID_FREE_TEXT_HEADLINE:
        return window.info.headline.get_text()
    elif widget_id == ID_FREE_TEXT:
        return window.info.textview.get_text()
    
def push_button(window, widget_id):
    """ Push a button with a given ID. """
    if widget_id == ID_BUTTON_NEXT:
        window._on_next_clicked(None)
    elif widget_id == ID_BUTTON_CANCEL:
        window._on_cancel_clicked(None)

def left_click_tray_icon(window, widget_id):
    """ Simulate a left click on the tray icon. """
    window._on_left_click(window.handles['status_icon'])

def right_click_tray_icon(window, widget_id):
    """ Simulate a right click on the tray icon. """
    window._on_right_click(window.handles['status_icon'], None, None)

def select_menu_item(window, widget_id):
    """ Select the menu item given by widget_id. """
    widget_id.view.select()
    widget_id.view.activate()

def select_menu_folder(window, widget_id):
    #widget_id.parent.placeholder.select()
    widget_id.view.popup(None, widget_id.parent.view, None, 3, 0)

#def push_key_down(window):
#    """ Simulate a key press (arrow down). """
#    event = gtk.gdk.Event(gtk.gdk.KEY_PRESS)
#    event.keyval = gtk.keysyms.Down
#    event.state = gtk.gdk.CONTROL_MASK
#    #event.send_event = True
#    event.time = 0 #int(time.time()) #0 # assign current time
#    #event.window = window.view.window
#    #gtk.gdk.event_put(event) # NOGO
#    #window.view.add_events(gtk.gdk.KEY_PRESS_MASK)
#    #window.view.grab_focus()
#    #buffer_ = window.view.get_buffer()
#    #buffer_.place_cursor(buffer_.get_iter_at_offset(0))
#    #event.put()
#    #gtk.main_iteration(False)
#    window.view.emit('key-press-event', event)
#
#def push_key_right(window):
#    """ Simulate a key press (arrow right). """
#    event = gtk.gdk.Event(gtk.gdk.KEY_PRESS)
#    event.keyval = gtk.keysyms.Right
#    event.send_event = True
#    event.window = window.view.window
#    event.time = 0 # assign current time
#    window.view.grab_focus()
#    window.view.emit('key-press-event', event)
#
#def push_key_return(window):
#    """ Simulate a key press (return). """
#    event = gtk.gdk.Event(gtk.gdk.KEY_PRESS)
#    event.keyval = gtk.keysyms.Return
#    event.time = 0 # assign current time
#    window.view.grab_focus()
#    window.view.emit('key_press_event', event)

def get_progress_bar_progress(window, widget_id):
    """ Get an integer status of the progression. """
    return window.progress.progressbar.get_fraction() * 100

def get_menu_item_launch_id(window, menu_folder, item_number):
    """ Return the launch ID associated with the given menu item. """
    if len(menu_folder.view.get_children()) > item_number:
        menu_folder.view.get_children()[item_number].activate()
        if len(window.controller.get_launch_list()) > 0:
            launch_id = window.controller.get_launch_list()[0]
            window.controller.remove_from_launch_list(window.controller.get_launch_list()[0])
        else:
            launch_id = -1
    else:
        launch_id = -1
    
    return launch_id
    
def get_menu_item_count(menu_folder):
    """ Return the number of items in a given menu folder. """
    return len(menu_folder.view.get_children())
    
def get_menu_launch_id_from_index(window, index):
    """ Get the actual launch id"""
    return index

def is_widget_enabled(window, widget_id):
    """ Find out if the widget is enabled. """
    if widget_id == ID_BUTTON_NEXT:
        return window.buttons.nextbutton.get_property('sensitive')
    elif widget_id == ID_BUTTON_CANCEL:
        return window.buttons.cancelbutton.get_property('sensitive')

def is_widget_visible(window, widget_id):
    """ Find out if a widget is visible. """
    if widget_id == ID_BUTTON_NEXT:
        print "visible (1):", window.buttons.nextbutton.get_property('visible') 
        return window.buttons.nextbutton.get_property('visible')
    elif widget_id == ID_BUTTON_CANCEL:
        print "visible (2):", window.buttons.nextbutton.get_property('visible')        
        return window.buttons.cancelbutton.get_property('visible')

def is_window_visible(window):
    """ find out if a window is currently visible. """
    try:
        if hasattr(window, 'view'):
            return window.view.get_property('visible')
        else:
            return window.get_property('visible')
    except:
        return False

def get_selection_list_item_count(window, widget_id):
    """ Get the number of items currently displayed. """
    return window.selection.packageselectiontable.get_model().iter_n_children(None)

def get_selection_list_item_text(window, widget_id, index):
    """ Get the text from the item in the selection list given by index. """
    iter = window.selection.packageselectiontable.get_model().iter_nth_child(None, index)
    return window.selection.packageselectiontable.get_model().get_value(iter, 1)

def get_selection_list_item_details(window, widget_id, index):
    """ Get the details-text from the item in the selection list given by index. """
    iter = window.selection.packageselectiontable.get_model().iter_nth_child(None, index)
    return window.selection.packageselectiontable.get_model().get_value(iter, 2)

def get_selection_list_item_selection_selected(window, widget_id, index):
    """ Get whether the indexed item is selected. """
    iter = window.selection.packageselectiontable.get_model().iter_nth_child(None, index)
    return window.selection.packageselectiontable.get_model().get_value(iter, 0)

def get_window_frame_text(window):
    """ Get the text in the window frame. """
    return window.view.get_title()

def set_selection_list_highlighted_item(window, widget_id, index):
    """ Set which item is highlighted in the selection list. """
    window.selection.packageselectiontable.set_cursor(index)
    window.selection.packageselectiontable.scroll_to_cell(index)



