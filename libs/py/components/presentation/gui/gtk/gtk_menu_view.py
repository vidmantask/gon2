from components.presentation.menu import MenuView
import lib.dictionary
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gio


"""
Gtk version of the GUI for menus.
"""


class GtkMenuView(MenuView):
    """ A Gtk based view for a menu. """

    def __init__(self, model, handles, name, configuration):
        MenuView.__init__(self, model, handles, name, configuration)
        # print "GtkMenuView.__init", name
        self.name = name
        # Quick fix for only having one menu - sysmenu should not create another Gtk Application menu
        if name == 'appmenu':
            self.topFolder = Gio.Menu.new()
            self.handles['on_main_menu_added'] = self.on_main_menu_added

    def on_main_menu_added(self):
        self.handles['gon_app_menu'].insert_section(0, "G/On Applications", self.topFolder)

    def _recursive_update(self, folderModelLocation, folderViewLocation):
        """ Recursively update the view of menu folders and items.

            Recursively controls consistency between model and 
            view. Anything that does not exist is created. 
            Anything that exists should be controlled so that 
            it reflects the model.
        """
        # Add or update folders in the current folder.
        for folderModel in folderModelLocation.folders:
            sub_menu = Gio.Menu.new()
            folderViewLocation.append_submenu(folderModel.text, sub_menu)
            self._recursive_update(folderModel, sub_menu)

        # Add or update items in the current folder.
        for item in folderModelLocation.items:
            tmp = Gio.MenuItem.new(item.text, "app.gon_app_" + str(item.id))
            # It looks cleaner without the icons - so commented out for now
            # if item.icon:
            #     tmp.set_icon(Gio.Icon.new_for_string(
            #           gtk_tools.locate_image_path(self.configuration.gui_image_path) + '/outlook.bmp'))
            folderViewLocation.append_item(tmp)
            tmp_action = Gio.SimpleAction.new("gon_app_" + str(item.id), None)
            if item.enabled:
                tmp_action.connect("activate", lambda a, b, mid: self._on_activate(mid), item.launch_id)
                self.handles['application'].add_action(tmp_action)

    def _on_activate(self, mid):
        """ Activate a menu item - launch something. """
        # print "GtkMenuView._on_activate:", mid
        self.controller.add_to_launch_list(mid)

    def update(self):
        """ Update the Gtk menu view.
        
            Makes sure that the view is 
            consistent with the model.
        """
        GObject.idle_add(self.update_in_gui_thread)
        
    def update_in_gui_thread(self):
        # print("GtkMenuView.update_in_gui_thread", self.model.topfolder)
        self.topFolder.remove_all()
        self._recursive_update(self.model.topfolder, self.topFolder)

    def display(self, views=None):
        """ Display the Gtk based menu.
        
            Makes sure the menu is visible to 
            the user. In this case it means to
            pop up the menu from the sys tray.
        """
        #  print "gtk_menu_view.display() called"
        #  self.status_icon_menu.show()
        pass

    def destroy(self):
        """ Destroy the menu so that it can be called again. """
        pass

        
# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.common import CommonModel
    from components.presentation.menu import MenuModel

    # Create models
    commonmodel = CommonModel()
    menumodel = MenuModel(lib.dictionary.Dictionary())

    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()
    
    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    menuview = GtkMenuView(menumodel, commonview.handles, 'appmenu', configuration)

    # Add content to the menu
    menuview.controller.add_folder(-1, 1, "Folder", None)
    menuview.controller.add_item(-1, 2, "First Item", None, True, 1000, True)    
    menuview.controller.add_item(-1, 3, "Second item", None, True, 1001)
        
    # menuview.controller.add_folder(1, 4, "Folder in folder", None)
    menuview.controller.add_item(1, 5, "Item in folder", None, True, 1002)
    menuview.controller.add_item(1, 6, "Second item in folder", None, True, 1003)
    #
    # menuview.controller.add_folder(4, 7, "Folder in folder in ...", None)
    # menuview.controller.add_item(4, 8, "Item in folder in ...", None, True, 1004)
    #
    # menuview.controller.add_folder(-1, 9, "Folder with icon", "giritech.bmp")
    # menuview.controller.add_item(-1, 10, "Item with icon", "outlook.bmp", True, 1005)
    menuview.controller.add_item(-1, 11, "Disabled item", None, False, 1006)
    menuview.controller.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, 1007)

    # # These should be handled nicely.
    menuview.controller.add_folder(100, 13, "Folder with nonexisting parent", None)
    # menuview.controller.add_item(100, 14, "Item with nonexisting parent", None, True, 1008)
    #
    # menuview.controller.add_item(-1, 15, "Item with nonexisting bmp icon", "nonexisting.bmp", True, 1009)
    # menuview.controller.add_item(-1, 16, "Item with unsupported icon type", "nonexisting.ico", True, 1010)
    # menuview.controller.add_item(-1, 17, "Item with bad icon type", "nonexisting.bad", True, 1011)
    #
    # menuview.controller.add_folder(-1, 18, "Folder with nonexisting bmp icon", "nonexisting.bmp")
    # menuview.controller.add_folder(-1, 19, "Folder with unsupported icon type", "nonexisting.ico")
    # menuview.controller.add_folder(-1, 20, "Folder with bad icon type", "nonexisting.bad")
    
    # What to do with these?? Remove/replace/ignore 
    # (now they are just not added, because the id already exists)
    #menuview.controller.add_item(-1, 3, "Item with existing id", None, True, 1001)
    #menuview.controller.add_folder(-1, 3, "Folder with existing id", None)
    
    # Start Gui
    commonview.display()

