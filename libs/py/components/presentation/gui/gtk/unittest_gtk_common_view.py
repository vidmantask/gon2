"""
Unit test for the structure of the GTK Common View.
"""
import time
import sys
from threading import Thread

import unittest
from lib import giri_unittest

from components.presentation.common import CommonModel
if sys.platform == 'linux2':

    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, GObject

    # import gtk
    # import gobject

    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path =  giri_unittest.get_image_path()
        self.dictionary = giri_unittest.get_dictionary()
        
gui_configuration = GuiConfiguration()        



class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """
    def __init__(self, commonmodel, commonview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.commonview = commonview
        
    def shutdown_in_gui_thread(self):
        Gtk.main_quit()
        return False


class GtkCommonViewTest(unittest.TestCase):
    """ Collection of tests for the GTK based view of common. """

    def setUp(self):
        self.commonmodel = CommonModel()
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.test = ThreadedTestRunner(self.commonmodel, self.commonview)

    def tearDown(self):
        self.test.join()

    def test_gtk_common_view_init(self):
        """ Gtk Common View initialization and destruction. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            time.sleep(1.0) # Wait so we get a chance to see the window
            self.assertEqual(self.test.commonview.model, self.commonmodel)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_common_view_mouse_clicks(self):
        """ Test the different mouse click possibilities. 
        
        See if the mouse clicks can be handled. 
        If no error is raised we assume all is good.
        There is some stdout output that could be tested.
        We catch the AssertionErrors so those can not be
        checked. This will do for now.
        """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            time.sleep(1.0) # Wait so we get a chance to see the window
            # Simulate left click - activate app menu
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            # Simulate right click - activate sys menu
            self.test.commonview._on_right_click(self.test.commonview.handles['status_icon'], None, None)
            time.sleep(0.5)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = sys.platform not in ['linux2']

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
