from components.presentation.gui.gtk.area.gtk_html_area import GtkHtmlArea
from components.presentation.gui.gtk.gtk_base_view import GtkBaseView
from components.presentation.splash import SplashView
import lib.dictionary
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
GTK version of a splash screen.

"""


class GtkSplashView(GtkBaseView, SplashView):
    
    def __init__(self, model, handles, name, configuration, unittest=False):
        GtkBaseView.__init__(self, "G/On", configuration)
        SplashView.__init__(self, model, handles, name, configuration)

        # Currently visible model
        self.current_hide = self.model.hide
         
        self.create_window()
        self.view.set_decorated(False)
        self.centre_window_on_desktop()
        self.resize(375, 175)
        
        self.htmlview = GtkHtmlArea(self.container, self.controller, configuration)
        self.update()
        
    def update(self):
        self.htmlview.update(self.model)
        if self.model.hide:
            self.hide()
            self.current_hide = self.model.hide
            
    def hide_in_gui_thread(self):
        self.view.hide()
        return False
        
    def hide(self):
        """ Hide the window. """
        GObject.idle_add(self.hide_in_gui_thread)

#
# -------------------------------------------------------------------------------
#


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions    
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.common import CommonModel
    from components.presentation.splash import SplashModel

    commonmodel = CommonModel()
    splashmodel = SplashModel()
    
    handles = None
    dictionary = lib.dictionary.Dictionary()
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary

    commonview = GtkCommonView(commonmodel, None, 'commonview', configuration)
    splashview = GtkSplashView(splashmodel, commonview.handles, 'splashview', configuration)

    splashview.display()
    splashview.controller.set_text("Splaske splask<br>")
    
    commonview.display()
