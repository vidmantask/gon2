"""
Unittest for the structure of the Gtk Update View.
"""
import sys
import time
from threading import Thread

import unittest
from lib import giri_unittest

from components.presentation.common import CommonModel
from components.presentation.update import UpdateModel
if sys.platform == 'linux2':

    # import gtk
    # import gobject

    import gi
    gi.require_version("Gtk", "3.0")
    from gi.repository import Gtk, GObject, Gdk

    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.gui.gtk.gtk_update_view import GtkUpdateView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path =  giri_unittest.get_image_path()
        self.dictionary =  giri_unittest.get_dictionary()


gui_configuration = GuiConfiguration()


class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """
    def __init__(self, commonmodel, updatemodel, commonview, updateview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.updatemodel = updatemodel
        self.commonview = commonview
        self.updateview = updateview
        
    def shutdown_in_gui_thread(self):
        Gtk.main_quit()
        return False


class GtkUpdateViewTest(unittest.TestCase):
    """ Collection of tests for the Gtk based view of update. """

    def setUp(self):
        self.commonmodel = CommonModel()
        self.updatemodel = UpdateModel(giri_unittest.get_dictionary())
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.updateview = GtkUpdateView(self.updatemodel, self.commonview.handles, 'updateview', gui_configuration, True)
        self.test = ThreadedTestRunner(self.commonmodel, self.updatemodel, self.commonview, self.updateview)

    def tearDown(self):
        self.updateview.hide()
        self.test.join()

    def test_gtk_update_view_init(self):
        """ Test the Gtk Update View display init. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_init"
            self.updateview.display()
            self.assertEqual(self.test.updateview.model, self.updatemodel)
            time.sleep(1.0)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_view_set_header_banner(self):
        """ Test that the header banner changed. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_header_banner"
            self.updateview.display()
            self.test.updateview.controller.set_info_headline("Banner test")
            # Set headers image
            time.sleep(0.2)
            self.test.updateview.controller.set_banner(self.test.updateview.controller.ID_BANNER_INSTALL)
            time.sleep(0.2)
            self.test.updateview.controller.set_banner(None)
            time.sleep(0.2)
            self.test.updateview.controller.set_banner(self.test.updateview.controller.ID_BANNER_UPDATE)
            time.sleep(0.2)
            self.test.updateview.controller.set_banner(None)
            time.sleep(0.2)
            self.test.updateview.controller.set_banner(self.test.updateview.controller.ID_BANNER_REMOVE)
            time.sleep(1.0)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_update_view_set_button_states(self):
        """ Test that the button states can be changed. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_button_states"
            self.updateview.display()
            
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_headline("Button state test")
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.cancelbutton.get_property('sensitive'), True)
            self.assertEqual(self.test.updateview.nextbutton.get_property('sensitive'), True)
        
            time.sleep(0.2)
            self.test.updateview.controller.set_next_allowed(False) 
            time.sleep(0.2)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_cancel_allowed(False)
            time.sleep(0.2)
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.cancelbutton.get_property('sensitive'), False)
            self.assertEqual(self.test.updateview.nextbutton.get_property('sensitive'), False)
    
            time.sleep(0.2)
            self.test.updateview.controller.set_cancel_allowed(True)
            time.sleep(0.2)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_next_allowed(True) 
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.cancelbutton.get_property('sensitive'), True)
            self.assertEqual(self.test.updateview.nextbutton.get_property('sensitive'), True)
            time.sleep(1.0)
                
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_view_set_selection_list_visibility(self):
        """ Test that the selection list can be shown and hidden. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_mode"
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]

            self.packagelist = [{'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'win', 'description': 'Greatest remote solution ever - win style'},
                           {'selected': False, 'icon': None, 'name': 'G/On Client', 'details': 'mac', 'description': 'Greatest remote solution ever - mac style'},
                           {'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'linux', 'description': 'Greatest remote solution ever - linux style'},
                           {'selected': False, 'icon': None, 'name': 'Extra files', 'details': 'all', 'description': 'Extra files for carrying around'},
                           {'selected': True,  'icon': None, 'name': 'Remote Desktop', 'details': 'mac', 'description': 'Remote desktop for the mac.'},
                           {'selected': False, 'icon': None, 'name': 'FileZilla', 'details': 'win', 'description': 'FTP client for windows'},
                           {'selected': True,  'icon': None, 'name': 'Citrix ICA client', 'details': 'win', 'description': 'Citrix client for windows'},
                           {'selected': False, 'icon': None, 'name': 'Citrix ICA client', 'details': 'mac', 'description': 'Citrix client for mac'},
                           {'selected': True,  'icon': None, 'name': 'Navision client', 'details': 'win', 'description': 'Navision client for windows'},
                           {'selected': False, 'icon': None, 'name': 'TightVNC Viewer', 'details': 'win', 'description': 'Remote desktop connection to macs'},
                           {'selected': True,  'icon': None, 'name': 'G/On Management', 'details': 'win', 'description': 'Total world domination'},
                           {'selected': False, 'icon': None, 'name': 'Packaged IE', 'details': 'win', 'description': 'Exploder thingy'}                                             
                           ]


            self.test.updateview.controller.set_phase_list(self.phaselist) 
            self.test.updateview.controller.set_info_headline("Selection List Visibility Test")
            self.test.updateview.controller.set_info_free_text("Hide and display the selection list.")
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
            self.test.updateview.controller.set_banner(self.test.updateview.controller.ID_BANNER_ENROLL)
            self.test.updateview.controller.set_selection_list(self.packagelist, False)
            
            self.pause = 0.25
            
            self.updateview.display()
            time.sleep(1.0)
            self.test.updateview.controller.set_show_selection_list(True)
            time.sleep(self.pause)
            self.test.updateview.controller.set_show_selection_list(False)
            time.sleep(self.pause)
            self.test.updateview.controller.set_show_selection_list(True)
            time.sleep(self.pause)
            self.test.updateview.controller.set_show_selection_list(False)
            #time.sleep(self.pause)
            
            self.test.updateview.hide()
            time.sleep(0.5)
            #self.test.updateview.controller.set_show_selection_list(False)
            self.test.updateview.display()
            #self.test.updateview.controller.set_show_selection_list(True)
            
            
            
            #self.test.updateview.controller.set_show_selection_list(False)
            #time.sleep(self.pause)
#            self.test.updateview.controller.set_show_selection_list(False)
#            time.sleep(self.pause)
#            self.test.updateview.controller.set_show_selection_list(True)
#            time.sleep(self.pause)
#            self.test.updateview.controller.set_show_selection_list(False)
#            time.sleep(self.pause)
#            self.test.updateview.controller.set_show_selection_list(True)
#            time.sleep(self.pause)
#            self.test.updateview.controller.set_show_selection_list(False)
            
            time.sleep(2.5)
        
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop



    def test_gtk_update_view_set_mode(self):
        """ Test that mode change works. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_mode"
            self.updateview.display()
            
            self.test.updateview.controller.set_info_headline("Info Mode Settings Test (free text)")
            self.test.updateview.controller.set_info_free_text("Test text (1)")
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
            # There is only two elements in the info container at any time
            # and the bottom one is either the description label or the progress bar.
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.infocontainer.get_children()[1], self.test.updateview.sysinfodescription)
            self.assertEqual(len(self.test.updateview.infocontainer.get_children()), 2)
            time.sleep(1.0)
            
            self.test.updateview.controller.set_info_headline("Info Mode Settings Test (progress)")
            self.test.updateview.controller.set_info_progress_complete(65)
            self.test.updateview.controller.set_info_progress_subtext("Progress subtext")
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_PROGRESS)
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.infocontainer.get_children()[1], self.test.updateview.progressbar)
            self.assertEqual(len(self.test.updateview.infocontainer.get_children()), 2)
            time.sleep(2.0)
            
            self.test.updateview.controller.set_info_headline("Mode settings test (button descriptions)")
            self.test.updateview.controller.set_cancel_description("Cancel button description")
            self.test.updateview.controller.set_next_description("Next button description")
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_BUTTON_TEXT)
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.infocontainer.get_children()[1], self.test.updateview.sysinfodescription)
            self.assertEqual(len(self.test.updateview.infocontainer.get_children()), 2)
            time.sleep(1.0)

            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
    
    def test_gtk_update_view_set_info_progress_mode(self):
        """ Test that mode change works. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_info_progress_mode"
            self.updateview.display()
            
            self.test.updateview.controller.set_info_headline("Info Progress Mode Settings Test")
            self.test.updateview.controller.set_info_free_text("Test text")
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
            time.sleep(1.0)
            
            self.test.updateview.controller.set_info_headline("Info Mode Settings Test (definite progress)")
            self.test.updateview.controller.set_info_progress_subtext("Progress subtext")
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_PROGRESS)
            
            for index in range(0, 105, 5):
                self.updateview.controller.set_info_progress_complete(index)
                time.sleep(0.1)
            time.sleep(1.0)
            
            self.test.updateview.controller.set_info_headline("Info Mode Settings Test (indefinite progress)")
            self.test.updateview.controller.set_info_progress_complete(65)
            self.test.updateview.controller.set_info_progress_subtext("Progress subtext")
            self.test.updateview.controller.set_info_progress_mode(self.test.updateview.controller.ID_MODE_PROGRESS_UNKNOWN)
            time.sleep(2.0)
            
            self.test.updateview.controller.set_info_progress_mode(self.test.updateview.controller.ID_MODE_PROGRESS_KNOWN)
            for index in range(0, 105, 5):
                self.updateview.controller.set_info_progress_complete(index)
                time.sleep(0.1)
            time.sleep(1.0)
            
            self.test.updateview.controller.set_info_headline("Mode settings test (button descriptions)")
            self.test.updateview.controller.set_cancel_description("Cancel button description")
            self.test.updateview.controller.set_next_description("Next button description")
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_BUTTON_TEXT)
            time.sleep(2.0)

            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
    
    def test_gtk_update_view_info_free_text_and_button_texts(self):
        """ Test that the system info is displayed in system info mode. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_info_free_text_and_button_texts"
            self.updateview.display()
                
            self.test.updateview.controller.set_info_headline("System Free Text Mode")
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_free_text('Server is available in 3 seconds (unittest)')
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Server is available in 3 seconds (unittest)')
            time.sleep(1.0)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_free_text('Server is available in 2 seconds (unittest)')
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Server is available in 2 seconds (unittest)')
            time.sleep(1.0)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_free_text('Server is available in 1 seconds (unittest)')
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Server is available in 1 seconds (unittest)')
            time.sleep(1.0)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_free_text('Server is available now! (unittest)')
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Server is available now! (unittest)')
            time.sleep(1.0)
            self.test.updateview.controller.set_cancel_description("Line 1")
            self.test.updateview.controller.set_next_description("Line 2")
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Server is available now! (unittest)')
            time.sleep(1.0)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_info_mode(self.test.updateview.controller.ID_MODE_INFORMATION_BUTTON_TEXT)
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "System Free Text Mode")
            self.assertEqual(self.test.updateview.sysinfodescription.get_text(), 'Line 1\nLine 2')
            time.sleep(1.0)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_update_view_progress(self):
        """ Test that the progress bar works as assumed. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_progress"
            self.updateview.display()
            
            self.test.updateview.controller.set_info_headline("Progress mode info test")

            # Package info in the information area.
            time.sleep(0.25)
            self.test.updateview.controller.set_info_mode('progress')
            
            for index in range(0, 101): # To get numbers from 0 to 100
                time.sleep(0.05)
                self.updatecount = self.test.updateview.get_update_count()
                text = str(index) + " percent complete"
                self.test.updateview.controller.set_info_progress_subtext(text)
                self.test.updateview.controller.set_info_progress_complete(index)
                while self.test.updateview.updatecount <= self.updatecount+1: pass
                self.assertEqual(self.test.updateview.progressbar.get_fraction(), index/100.0)
                self.assertEqual(self.test.updateview.progressbar.get_text(), text)
                self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "Progress mode info test")
            time.sleep(1.0)
            for index in range(0, 101): # To get numbers from 0 to 100
                time.sleep(0.05)
                self.updatecount = self.test.updateview.get_update_count()
                self.test.updateview.controller.set_info_progress_complete(index)
                text = str(index) + " percent complete"
                self.test.updateview.controller.set_info_progress_subtext(text)
                while self.test.updateview.updatecount <= self.updatecount+1: pass
                self.assertEqual(self.test.updateview.progressbar.get_fraction(), index/100.0)
                self.assertEqual(self.test.updateview.progressbar.get_text(), text)
                self.assertEqual(self.test.updateview.sysinfoheader.get_text(), "Progress mode info test")
            time.sleep(1.0)
                        
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_view_set_phase_list(self):
        """ Test that the phase list can be changed. 
            
            We add and remove elements from a container to
            show phases here. So check if the number of elements
            in the container is correct.
            
            I could not find a way to test the images between phases
            so we will have to rely on visual tests for now.
        """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_phase_list"
            self.updateview.display()
        
        
            self.test.updateview.controller.set_info_headline("Phases test")
            
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]
            
            # Set phases texts
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:1])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 1)
            self.assertEqual(self.test.updateview.phasescontainer.get_children()[0].get_text(), self.phaselist[0]['name'])
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:2])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 3)
            self.assertEqual(self.test.updateview.phasescontainer.get_children()[2].get_text(), self.phaselist[1]['name'])
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:3])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 5)
            self.assertEqual(self.test.updateview.phasescontainer.get_children()[4].get_text(), self.phaselist[2]['name'])
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:4])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 7)
            self.assertEqual(self.test.updateview.phasescontainer.get_children()[6].get_text(), self.phaselist[3]['name'])
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_phase_list(self.phaselist[0:5])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 9)
            self.assertEqual(self.test.updateview.phasescontainer.get_children()[8].get_text(), self.phaselist[4]['name'])
    
            # Set phases status
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
            
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
    
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_PENDING},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
    
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_PENDING}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
    
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_ACTIVE}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
    
            time.sleep(0.25)
            self.phaselist = [{'name': 'Test 1', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 2', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 3', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 4', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE},
                              {'name': 'Test 5', 'state': self.test.updateview.controller.ID_PHASE_COMPLETE}]
            self.test.updateview.controller.set_phase_list(self.phaselist)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:5])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 9)
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_phase_list(self.phaselist[0:4])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 7)
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_phase_list(self.phaselist[0:3])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 5)
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_phase_list(self.phaselist[0:2])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 3)
            time.sleep(0.25)
    
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_phase_list(self.phaselist[0:1])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(len(self.test.updateview.phasescontainer.get_children()), 1)
            time.sleep(0.25)
    
            self.test.updateview.controller.set_phase_list([])
    
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_view_set_selection_list_add_and_remove(self):
        """ Test that packages can be added and removed from the selection list. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_view_set_selection_list_add_and_remove"
            self.updateview.display()
        
            packagelist = [{'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'win', 'description': 'Greatest remote solution ever - win style'},
                           {'selected': False, 'icon': None, 'name': 'G/On Client', 'details': 'mac', 'description': 'Greatest remote solution ever - mac style'},
                           {'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'linux', 'description': 'Greatest remote solution ever - linux style'},
                           {'selected': False, 'icon': None, 'name': 'Extra files', 'details': 'all', 'description': 'Extra files for carrying around'},
                           {'selected': True,  'icon': None, 'name': 'Remote Desktop', 'details': 'mac', 'description': 'Remote desktop for the mac.'},
                           {'selected': False, 'icon': None, 'name': 'FileZilla', 'details': 'win', 'description': 'FTP client for windows'},
                           {'selected': True,  'icon': None, 'name': 'Citrix ICA client', 'details': 'win', 'description': 'Citrix client for windows'},
                           {'selected': False, 'icon': None, 'name': 'Citrix ICA client', 'details': 'mac', 'description': 'Citrix client for mac'},
                           {'selected': True,  'icon': None, 'name': 'Navision client', 'details': 'win', 'description': 'Navision client for windows'},
                           {'selected': False, 'icon': None, 'name': 'TightVNC Viewer', 'details': 'win', 'description': 'Remote desktop connection to macs'},
                           {'selected': True,  'icon': None, 'name': 'G/On Management', 'details': 'win', 'description': 'Total world domination'},
                           {'selected': False, 'icon': None, 'name': 'Packaged IE', 'details': 'win', 'description': 'Exploder thingy'}                                             
                           ]
    
            self.test.updateview.display()
            self.test.updateview.controller.set_info_headline("Package list add and remove test")
            self.test.updateview.controller.set_show_selection_list(True)
            # Set headers headline
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:1])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 1)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:2])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 2)
    
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:3])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 3)
    
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:4])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 4)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:5])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 5)
    
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:6])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 6)
    
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:7])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 7)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:8])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 8)
           
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:9])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 9)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:10])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 10)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:11])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 11)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:12])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 12)
    
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:11])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 11)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:10])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 10)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:9])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 9)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:8])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 8)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:7])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 7)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()        
            self.test.updateview.controller.set_selection_list(packagelist[0:6])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 6)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:5])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 5)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:4])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 4)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:3])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 3)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:2])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 2)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list(packagelist[0:1])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 1)
            
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_selection_list([])
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.packageselectiontable.get_model().iter_n_children(None), 0)
    
            time.sleep(1.0)

            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_hide_and_display_view(self):
        """ Test that the update view can be destroyed independently. 
                        
        """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_hide_and_display_view"
            self.updateview.display()
            self.test.updateview.controller.set_info_headline("Set Window Frame Text")

            self.test.updateview.display()
            time.sleep(0.25)
            self.test.updateview.hide()
            time.sleep(0.25)
            self.test.updateview.display()
            time.sleep(0.25)
            self.test.updateview.hide()
            time.sleep(0.25)
            self.test.updateview.display()
            time.sleep(1.0)
    
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
            
    def test_gtk_update_set_window_frame_text(self):
        """ Test that the update view frame text can be changed. 
            
            I could not find a way to determine if a window is visible.
            So this will be a visual test for now.
        """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_set_window_frame_text"
            self.updateview.display()
            self.test.updateview.controller.set_info_headline("Hide/display window test")

            self.test.updateview.display()
            time.sleep(0.25)
            
            for index in range(1, 11):
                self.updatecount = self.test.updateview.get_update_count()
                self.test.updateview.controller.set_window_frame_text("Test" + str(index))
                while self.test.updateview.updatecount <= self.updatecount: pass
                self.assertEqual(self.test.updateview.view.get_title(), "Test" + str(index))
                time.sleep(0.25)
            time.sleep(1.0)
    
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop


    def test_gtk_update_set_button_labels(self):
        """ Test that the update view button labels can be changed. 
            
            I could not find a way to determine if a window is visible.
            So this will be a visual test for now.
        """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_set_button_labels"
            self.updateview.display()
            self.test.updateview.controller.set_info_headline("Button Labels Text Test")

            self.test.updateview.display()
            time.sleep(0.25)
            
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_cancel_label("Go Back")
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.cancelbutton.get_label(), "Go Back")
            time.sleep(0.25)
            self.updatecount = self.test.updateview.get_update_count()
            self.test.updateview.controller.set_next_label("Go On")
            while self.test.updateview.updatecount <= self.updatecount: pass
            self.assertEqual(self.test.updateview.nextbutton.get_label(), "Go On")

            time.sleep(1.0)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_update_selection_method_and_highlighted(self):
        """ Test that the update view selection method can be changed. 
        """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            print "test_gtk_update_selection_method_and_highlighted"
            self.updateview.display()
            self.test.updateview.controller.set_info_headline("Button Labels Text Test")

            self.test.updateview.display()
            time.sleep(0.25)
            
            
            self.packagelist = [{'id': 0, 'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'win', 'description': 'Greatest remote solution ever - win style'},
                           {'id': 1, 'selected': False, 'icon': None, 'name': 'G/On Client', 'details': 'mac', 'description': 'Greatest remote solution ever - mac style'},
                           {'id': 2, 'selected': True,  'icon': None, 'name': 'G/On Client', 'details': 'linux', 'description': 'Greatest remote solution ever - linux style'},
                           {'id': 3, 'selected': False, 'icon': None, 'name': 'Extra files', 'details': 'all', 'description': 'Extra files for carrying around'},
                           {'id': 4, 'selected': True,  'icon': None, 'name': 'Remote Desktop', 'details': 'mac', 'description': 'Remote desktop for the mac.'},
                           {'id': 5, 'selected': False, 'icon': None, 'name': 'FileZilla', 'details': 'win', 'description': 'FTP client for windows'},
                           {'id': 6, 'selected': True,  'icon': None, 'name': 'Citrix ICA client', 'details': 'win', 'description': 'Citrix client for windows'},
                           {'id': 7, 'selected': False, 'icon': None, 'name': 'Citrix ICA client', 'details': 'mac', 'description': 'Citrix client for mac'},
                           {'id': 8, 'selected': True,  'icon': None, 'name': 'Navision client', 'details': 'win', 'description': 'Navision client for windows'},
                           {'id': 9, 'selected': False, 'icon': None, 'name': 'TightVNC Viewer', 'details': 'win', 'description': 'Remote desktop connection to macs'},
                           {'id': 10, 'selected': True,  'icon': None, 'name': 'G/On Management', 'details': 'win', 'description': 'Total world domination'},
                           {'id': 11, 'selected': False, 'icon': None, 'name': 'Packaged IE', 'details': 'win', 'description': 'Exploder thingy'}                                             
                           ]
            
            self.updateview.controller.set_banner(self.updateview.controller.ID_BANNER_INSTALL)
            self.updateview.controller.set_selection_list(self.packagelist)
            self.updateview.controller.set_show_selection_list(True)
            
            for nr in range(0, 4):
                if nr % 2 == 0:
                    self.updateview.controller.set_selection_method(self.updateview.controller.ID_SELECTION_SINGLE)
                else:
                    self.updateview.controller.set_selection_method(self.updateview.controller.ID_SELECTION_MULTIPLE)
                time.sleep(0.25)
            
            for index in range(0, 10):
                self.updatecount = self.test.updateview.updatecount
                self.updateview.controller.set_selection_highlighted_by_id(index) 
                while self.test.updateview.updatecount <= self.updatecount: pass
                self.assertEqual(self.updateview.controller.get_selection_highlighted_id(), index)
                time.sleep(0.25)
            
            time.sleep(1.0)
            GObject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop



#
#  GIRI_UNITTEST
#
#  GIRI_UNITTEST_TIMEOUT = 60
#  GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = True

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run()
