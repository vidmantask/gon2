"""
Unittest for the structure of the Gtk menu View.

Tests:
    See that the gtk menu corresponds to the model.
    See that menu item selections are placed correctly in the model launch list.
    See that subscribers are notified when an item is selected.
    See that updates to the model passes through to the gtk view.
"""
import sys
import time
from threading import Thread

import unittest
from lib import giri_unittest


from components.presentation.common import CommonModel
from components.presentation.menu import MenuModel
if sys.platform == 'linux2':
    import gtk
    import gobject
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.gui.gtk.gtk_menu_view import GtkMenuView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path =  giri_unittest.get_image_path()
        self.dictionary = giri_unittest.get_dictionary()
gui_configuration = GuiConfiguration()        



class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """
    def __init__(self, commonmodel, menumodel, commonview, menuview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.menumodel = menumodel
        self.commonview = commonview
        self.menuview = menuview
        
    def shutdown_in_gui_thread(self):
        gtk.main_quit()
        return False


class GtkMenuViewTest(unittest.TestCase):
    """ Collection of tests for the Gtk based view of menu. """

    def setUp(self):
        self.waitperiod = 0.1
        self.subscribernotifycount = 0
        self.commonmodel = CommonModel()
        self.menumodel = MenuModel(giri_unittest.get_dictionary())
        self.menumodel.subscribe(self.menusubscriber) 
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.menuview = GtkMenuView(self.menumodel, self.commonview.handles, 'appmenu', gui_configuration)
        self.test = ThreadedTestRunner(self.commonmodel, self.menumodel, self.commonview, self.menuview)

    def tearDown(self):
        self.test.menuview.destroy()
        self.test.join()

    def populate_menu(self):
        self.controller.add_folder(-1, 1, "Folder", None)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 2, "Item", None, True, 1000)
        time.sleep(self.waitperiod)
        self.controller.add_item(-1, 3, "Second item", None, True, 1001)
        time.sleep(self.waitperiod) 
        self.controller.add_folder(1, 4, "Folder in folder", None)
        time.sleep(self.waitperiod) 
        self.controller.add_item(1, 5, "Item in folder", None, True, 1002)
        time.sleep(self.waitperiod) 
        self.controller.add_item(1, 6, "Second item in folder", None, True, 1003)
        time.sleep(self.waitperiod) 
        self.controller.add_folder(4, 7, "Folder in folder in ...", None)
        time.sleep(self.waitperiod) 
        self.controller.add_item(4, 8, "Item in folder in ...", None, True, 1004)
        time.sleep(self.waitperiod) 
        self.controller.add_folder(-1, 9, "Folder with icon", "outlook.bmp")
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 10, "Item with icon", "outlook.bmp", True, 1005)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 11, "Disabled item", None, False, 1006)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, 1007)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 15, "Item with nonexisting bmp icon", "nonexisting.bmp", True, 1009)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 16, "Item with unsupported icon type", "nonexisting.ico", True, 1010)
        time.sleep(self.waitperiod) 
        self.controller.add_item(-1, 17, "Item with bad icon type", "nonexisting.bad", True, 1011)
        time.sleep(self.waitperiod)
        self.controller.add_folder(-1, 18, "Folder with nonexisting bmp icon", "nonexisting.bmp")
        time.sleep(self.waitperiod) 
        self.controller.add_folder(-1, 19, "Folder with unsupported icon type", "nonexisting.ico")
        time.sleep(self.waitperiod)
        self.controller.add_folder(-1, 20, "Folder with bad icon type", "nonexisting.bad")
        
    def menusubscriber(self):
        """ Set variable that indicates that method has been called. """
        self.subscribernotifycount = self.subscribernotifycount + 1
        self.subscribernotified = True

    def test_gtk_menu_view_init(self):
        """ Gtk Menu View initialization. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            self.controller = self.test.menuview.controller
            self.populate_menu()
            time.sleep(self.waitperiod)
        
            # Assert that the model is set correctly.
            self.assertEqual(self.menumodel, self.test.menuview.model)
            # Assert that the menu structure is what we think it should be.
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].child.get_text(), "Folder")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[1].child.get_text(), "Item")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[2].child.get_text(), "Second item")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[3].child.get_text(), "Folder with icon")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[4].child.get_text(), "Item with icon")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[5].child.get_text(), "Disabled item")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[6].child.get_text(), "Disabled item with icon")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[7].child.get_text(), "Item with nonexisting bmp icon")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[8].child.get_text(), "Item with unsupported icon type")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[9].child.get_text(), "Item with bad icon type")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[10].child.get_text(), "Folder with nonexisting bmp icon")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[11].child.get_text(), "Folder with unsupported icon type")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[12].child.get_text(), "Folder with bad icon type")
        
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[0].child.get_text(), "Folder in folder")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[1].child.get_text(), "Item in folder")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[2].child.get_text(), "Second item in folder")
        
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[0].get_submenu().get_children()[0].child.get_text(),
                         "Folder in folder in ...")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[0].get_submenu().get_children()[1].child.get_text(),
                         "Item in folder in ...")

            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_view_menu_subscription(self):
        """ Test that subscribers are notified. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            self.controller = self.test.menuview.controller
            self.populate_menu()
            time.sleep(self.waitperiod)
            self.assertEqual(self.subscribernotified, True)
            self.assertEqual(self.subscribernotifycount, 18)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_view_menu_select_item(self):
        """ See that simulated item selections are registered correct. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            self.controller = self.test.menuview.controller
            self.populate_menu()
            time.sleep(self.waitperiod)
            
            self.test.menuview.topfolder.view.get_children()[1].activate()        
            self.test.menuview.topfolder.view.get_children()[2].activate()
            self.test.menuview.topfolder.view.get_children()[4].activate()
            self.test.menuview.topfolder.view.get_children()[7].activate()
            self.test.menuview.topfolder.view.get_children()[8].activate()
            self.test.menuview.topfolder.view.get_children()[9].activate()
            self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[1].activate()
            self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[2].activate()
            self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()[0].get_submenu().get_children()[1].activate()
        
            self.assertEqual(self.menumodel.launchlist, [1000, 1001, 1005, 1009, 1010, 1011, 1002, 1003, 1004])        
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_view_menu_update(self):
        """ Test that updates works. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            self.controller = self.test.menuview.controller
            self.populate_menu()
            time.sleep(self.waitperiod)
            
            # Test that we can update the text in an item
            self.assertEqual(self.test.menuview.topfolder.contentids[2].view.child.get_text(), "Item")
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "Item (update)", None, True, 1002)
            time.sleep(self.waitperiod)
            self.assertEqual(self.test.menuview.topfolder.contentids[2].view.child.get_text(), "Item (update)")
    
            # Test that we can update an icon for an item
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[2].get_image(), None)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 3, "Second item (update)", "outlook.bmp", True, 1003)
            time.sleep(self.waitperiod)
            self.assertNotEquals(self.test.menuview.topfolder.view.get_children()[2].get_image(), None)

            # Test that we can remove an icon for an item
            self.assertNotEqual(self.test.menuview.topfolder.view.get_children()[4].get_image(), None)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 10, "Item with icon (update)", None, False, 1010)
            time.sleep(self.waitperiod)
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[4].get_image(), None)   
    
            # Test that we can enable an item
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[6].get_property('sensitive'), False)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 12, "Disabled item with icon (update)", "outlook.bmp", True, 1012)
            time.sleep(self.waitperiod)
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[6].get_property('sensitive'), True)

            # Test that we can update the text and icon for a folder (3)
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[3].child.get_text(), "Folder with icon")
            self.assertNotEqual(self.test.menuview.topfolder.view.get_children()[3].get_image(), None)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_folder(-1, 9, "Folder with icon (update)", None)
            time.sleep(self.waitperiod)
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[3].child.get_text(), "Folder with icon (update)")
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[3].get_image(), None)
        
            # Test that we can add an icon for a folder
            self.assertEqual(self.test.menuview.topfolder.view.get_children()[0].get_image(), None)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_folder(-1, 1, "Folder (update)", "giritech.bmp")
            time.sleep(self.waitperiod)
            self.assertNotEqual(self.test.menuview.topfolder.view.get_children()[0].get_image(), None)

            # Test that we can remove a folder
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()), 13)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 18)
            time.sleep(self.waitperiod)
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()), 12)
        
            # Test that we can remove a folder from a sub folder.
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()), 3)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(1, 4)
            time.sleep(self.waitperiod)
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()), 2)

            # Test that we can remove an item
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()), 12)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 2)
            time.sleep(self.waitperiod)
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()), 11)

            # Test that we can remove an item from a sub folder
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()), 2)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(1, 6)
            time.sleep(self.waitperiod)
            self.assertEqual(len(self.test.menuview.topfolder.view.get_children()[0].get_submenu().get_children()), 1)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
    def test_gtk_view_fun_menu_update(self):
        """ Test that updates works. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.test.commonview._on_left_click(self.test.commonview.handles['status_icon'])
            self.controller = self.test.menuview.controller
            self.populate_menu()
            time.sleep(self.waitperiod)

            # Do a number of updates and removals.
            self.test.menuview.controller.update_item(-1, 2, "I have been updated :-)", None, False, 1000)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "Now I am enabled :-)", None, True, 1001)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "Just got a nice icon :-)", "giritech.bmp", True, 1002)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 20)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 19)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 18)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "Lost some folders at the bottom :-(", "giritech.bmp", True, 1003)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 1)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_folder(-1, 9)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "No folders left :-(", "giritech.bmp", True, 1004)
            time.sleep(self.waitperiod)
        
            self.test.menuview.controller.remove_item(-1, 3)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 10)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 11)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 12)
            time.sleep(self.waitperiod)        

            self.test.menuview.controller.update_item(-1, 2, "Lost some items :-(", "giritech.bmp", True, 1005)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 15)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 16)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.remove_item(-1, 17)
            time.sleep(self.waitperiod)        

            self.test.menuview.controller.update_item(-1, 2, "Now I'm all alone :-(", "giritech.bmp", True, 1006)
            time.sleep(self.waitperiod)
            self.test.menuview.controller.update_item(-1, 2, "Now I'm all alone :-(", None, True, 1007)
            time.sleep(self.waitperiod)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop



#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = sys.platform not in ['linux2']

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
