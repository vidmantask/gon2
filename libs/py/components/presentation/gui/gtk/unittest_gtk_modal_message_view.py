"""
Unittest for the structure of the Gtk Modal Message View.
"""
import sys
import time

from threading import Thread

import unittest
from lib import giri_unittest

from components.presentation.common import CommonModel
from components.presentation.modal_message import ModalMessageModel
if sys.platform == 'linux2':
    import gtk
    import gobject
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.gui.gtk.gtk_modal_message_view import GtkModalMessageView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path =  giri_unittest.get_image_path()
        self.dictionary =  giri_unittest.get_dictionary()
gui_configuration = GuiConfiguration()        



class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """
    def __init__(self, commonmodel, modalmessagemodel, commonview, modalmessageview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.modalmessagemodel = modalmessagemodel
        self.commonview = commonview
        self.modalmessageview = modalmessageview
        
    def shutdown_in_gui_thread(self):
        gtk.main_quit()
        return False


class GtkMessageViewTest(unittest.TestCase):
    """ Collection of tests for the Gtk based modal view of message. """

    def setUp(self):
        self.commonmodel = CommonModel()
        self.modalmessagemodel = ModalMessageModel(giri_unittest.get_dictionary())
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.modalmessageview = GtkModalMessageView(self.modalmessagemodel, self.commonview.handles, 'modalmessageview', gui_configuration)
        self.test = ThreadedTestRunner(self.commonmodel, self.modalmessagemodel, self.commonview, self.modalmessageview)

    def tearDown(self):
        self.test.join()

    def test_gtk_modal_message_view_init(self):
        """ Gtk Modal Message View initialization. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.modalmessageview.display()
            time.sleep(1.0) # Wait so we get a chance to see the window
            self.assertEqual(self.modalmessagemodel, self.test.modalmessageview.model)
            self.assertEqual(self.test.modalmessageview.current_text, "")        
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_modal_message_view_set_text(self):
        """ Update of Gtk Modal Message View. """
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            testtext = "A fish can't whistle and neither can I. There's nothing wrong with not being able to whistle, especially if you're a fish. But there can be lots of things wrong with blindly trying to do what you aren't designed for. Unfortunately, some people aren't so wise, and end up causing big trouble for themselves and others. The wise know their limitations; the foolish do not. To demonstrate what we mean, we can think of no one better than Tigger, who doesn't know his limitations ('Tiggers' can do everything'), which brings him in lots of trouble. Piglet instead knows his limitations and that's what makes him sometimes more brave than you would expect from such a small animal. So, the first thing we need to do is recognize and trust our own Inner Nature, and not lose sight of it. Inside the Bouncy Tigger is the Rescuer who knows the Way, and in each of us is something Special, and that we need to keep:"
            self.modalmessageview.display()
            time.sleep(1.0) # Wait so we get a chance to see the window
            self.test.modalmessageview.controller.set_text(testtext)
            time.sleep(0.5)
            testtext = testtext + testtext
            self.test.modalmessageview.controller.set_text(testtext)
            time.sleep(0.5)
            testtext = testtext + testtext
            self.test.modalmessageview.controller.set_text(testtext)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.current_text, testtext)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop


    def test_gtk_modal_message_view_buttons(self):
        """ Update of Gtk Modal Message Views buttons. """

        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.modalmessageview.display()
            self.test.modalmessageview.controller.set_text("Update button states.")
            time.sleep(0.5)
            self.test.modalmessageview.controller.set_next_allowed(False)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.okbutton.get_property('sensitive'), False)
            self.test.modalmessageview.controller.set_next_allowed(True)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.okbutton.get_property('sensitive'), True)
            self.test.modalmessageview.controller.set_cancel_allowed(False)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.cancelbutton.get_property('sensitive'), False)
            self.test.modalmessageview.controller.set_cancel_allowed(True)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.cancelbutton.get_property('sensitive'), True)
            time.sleep(1.0)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop

    def test_gtk_modal_message_view_button_pushes(self):
        """ Simulate OK and Cancel button clicks. """
        
        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.modalmessageview.display()
            self.test.modalmessageview.controller.set_text("Simulate button clicks.")
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.controller.get_ok_clicked(), False)
            self.test.modalmessageview._on_ok_clicked(None)
            self.assertEqual(self.test.modalmessageview.controller.get_ok_clicked(), True)
            time.sleep(0.5)
            self.assertEqual(self.test.modalmessageview.controller.get_cancel_clicked(), False)
            self.test.modalmessageview._on_cancel_clicked(None)
            self.assertEqual(self.test.modalmessageview.controller.get_cancel_clicked(), True)
            time.sleep(0.5)
            gobject.idle_add(self.test.shutdown_in_gui_thread)
        
        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()         # Run the test thread  
        self.commonview.display() # Start main threads gui loop
        
        
#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = True


if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
