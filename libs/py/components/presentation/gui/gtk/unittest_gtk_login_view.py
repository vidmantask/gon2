"""
Unittest for the structure of the Gtk Login View.
"""
import sys
import time
from threading import Thread

import unittest
from lib import giri_unittest

from components.presentation.common import CommonModel
from components.presentation.login import LoginModel

if sys.platform == 'linux2':
    import gtk
    import gobject
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.gui.gtk.gtk_login_view import GtkLoginView


class GuiConfiguration:
    def __init__(self):
        self.gui_image_path = giri_unittest.get_image_path()
        self.dictionary = giri_unittest.get_dictionary()


gui_configuration = GuiConfiguration()


class ThreadedTestRunner(Thread):
    """ Keep the test in a thread on its own to enable simulations. 
    
        The run method is added in for each test.
    """

    def __init__(self, commonmodel, loginmodel, commonview, loginview):
        Thread.__init__(self)
        self.commonmodel = commonmodel
        self.loginmodel = loginmodel
        self.commonview = commonview
        self.loginview = loginview

    def shutdown_in_gui_thread(self):
        gtk.main_quit()
        return False


class GtkLoginViewTest(unittest.TestCase):
    """ Collection of tests for the Gtk based view of login. """

    def setUp(self):
        self.testusername = ''
        self.testpassword = ''
        self.testgreeting = ''
        self.testcancelled = ''
        self.notified = False
        self.commonmodel = CommonModel()
        self.loginmodel = LoginModel(giri_unittest.get_dictionary())
        self.loginmodel.subscribe(self.subscriber)
        self.commonview = GtkCommonView(self.commonmodel, None, 'commonview', gui_configuration)
        self.loginview = GtkLoginView(self.loginmodel, self.commonview.handles, 'loginview', gui_configuration)
        self.test = ThreadedTestRunner(self.commonmodel, self.loginmodel, self.commonview, self.loginview)

    def tearDown(self):
        self.test.join()

    def subscriber(self):
        """ Subscriber is called when model changes. """
        self.testusername = self.loginmodel.username
        self.testpassword = self.loginmodel.password
        self.testgreeting = self.loginmodel.greeting
        self.testcancelled = self.loginmodel.cancelled
        self.notified = True

    def test_gtk_login_view_init(self):
        """ Test the Gtk Login View display init. """

        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.loginview.display()
            time.sleep(1.0)  # Wait so we get a chance to see the window
            self.assertEqual(self.test.loginview.model, self.test.loginmodel)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop

    def test_gtk_login_view_set_greeting(self):
        """ Test the Gtk Login View Greeting Functionality. 
        
            Set the greeting via the controller and see
            that it is set in the correct window.
        """

        def run_test():
            """ Substituting the test threads run method. 
                Just for making it easier to write test methods.
            """
            self.testgreeting = "Greetings to the Giritech tester"
            self.test.loginview.controller.set_greeting(self.testgreeting)
            self.test.loginview.display()
            time.sleep(1.0)  # Wait so we get a chance to see the window
            self.assertEqual(self.test.loginview.messageview.get_text(), self.testgreeting)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop

    def test_gtk_login_view_read_credentials_on_ok(self):
        """ Test to see if the credentials are read correctly on ok. 
        
            Enter username and password in the correct fields.
            Simulate an OK button click and see that the correct 
            credentials are set in the model and that the subscribers
            are notified.
        """

        def run_test():
            self.loginview.display()
            # Delay needed because display_in_gui_thread
            # clears all fields. Wait for that and then
            # Write text into the fields for testing. 
            time.sleep(0.5)
            self.test.loginview.usernameview.set_text("Giritech")
            time.sleep(0.5)
            self.test.loginview.passwordview.set_text("Secret")
            time.sleep(0.5)
            self.test.loginview._on_ok_clicked_(None)
            time.sleep(0.5)
            self.assertEqual(self.testusername, "Giritech")
            self.assertEqual(self.testpassword, "Secret")
            self.assertEqual(self.notified, True)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop

    def test_gtk_login_view_read_credentials_on_cancel(self):
        """ Test to see if the credentials are read correctly on cancel. 
        
            Enter username and password in the correct fields.
            Simulate a Cancel button click and see that no 
            credentials are set in the model. 
        """

        def run_test():
            self.loginview.display()
            self.test.loginview.usernameview.set_text("Giritech")
            self.test.loginview.passwordview.set_text("Secret")
            time.sleep(1.0)  # Wait so we get a chance to see the window
            self.test.loginview._on_cancel_clicked_(None)
            self.assertEqual(self.testusername, "")
            self.assertEqual(self.testpassword, "")
            self.assertEqual(self.testcancelled, True)
            self.assertEqual(self.notified, True)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop

    def test_gtk_login_set_state(self):
        """Test to see if state can changed properly
        
            @summary: 
            State change should result in the change password
            area to become visible or disappear. Also the button
            used for changing state from GUI side should change.
            
            @since: 5.3
        """

        def run_test():
            self.loginview.display()
            time.sleep(1.0)  # Wait so we get a chance to see the window
            self.test.loginview.controller.set_state(self.test.loginview.controller.ID_STATE_CHANGE_LOGIN)
            time.sleep(0.5)
            self.assertEqual(self.test.loginview.changepassword_button.get_active(), True)
            self.assertEqual(self.test.loginview.view.get_size(), (375, 350))

            self.test.loginview.controller.set_state(self.test.loginview.controller.ID_STATE_LOGIN)
            time.sleep(0.5)
            self.assertEqual(self.test.loginview.changepassword_button.get_active(), False)
            self.assertEqual(self.test.loginview.view.get_size(), (375, 300))

            self.test.loginview.controller.set_state(self.test.loginview.controller.ID_STATE_CHANGE_LOGIN)
            time.sleep(0.5)
            self.assertEqual(self.test.loginview.changepassword_button.get_active(), True)
            self.assertEqual(self.test.loginview.view.get_size(), (375, 350))

            self.test.loginview.controller.set_state(self.test.loginview.controller.ID_STATE_LOGIN)
            time.sleep(0.5)
            self.assertEqual(self.test.loginview.changepassword_button.get_active(), False)
            self.assertEqual(self.test.loginview.view.get_size(), (375, 300))

            time.sleep(0.5)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop

    def test_gtk_login_set_and_get_password_request(self):
        """ Test to see if the password request is set properly
            
             @summary:
             The password request fields model.requestedpassword 
             and model.requestedpasswordretyped should be set to 
             the content of the related GUI fields. Also it should
             be possible to get the field content using a getter.
             
             The password request should be set on OK and not really 
             on any other occasion. The user of the model should check
             if the state is ID_STATE_CHANGE_LOGIN to see if the user 
             wants to change the password.
             
             @since: 5.3
        """

        def run_test():
            self.loginview.display()
            time.sleep(1.0)  # Wait so we get a chance to see the window
            self.test.loginview.usernameview.set_text("Giritech")
            time.sleep(0.5)
            self.test.loginview.passwordview.set_text("Secret")
            time.sleep(0.5)
            self.test.loginview.controller.set_state(self.test.loginview.controller.ID_STATE_CHANGE_LOGIN)
            time.sleep(0.5)
            self.test.loginview.newpasswordview.set_text("AnotherSecret")
            time.sleep(0.5)
            self.test.loginview.retypenewpasswordview.set_text("AnotherSecret")
            time.sleep(0.5)

            a = self.loginview.controller.get_password_request()
            self.assertEqual(a, None)
            self.test.loginview._on_ok_clicked_(None)
            a = self.loginview.controller.get_password_request()
            self.assertEqual(a, "AnotherSecret")

            time.sleep(0.5)
            gobject.idle_add(self.test.shutdown_in_gui_thread)

        self.test.run = run_test  # Add in the run method for the test thread
        self.test.start()  # Run the test thread
        self.commonview.display()  # Start main threads gui loop


#
# GIRI_UNITTEST
#
# GIRI_UNITTEST_TIMEOUT = 60
# GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'
GIRI_UNITTEST_IGNORE = True

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
