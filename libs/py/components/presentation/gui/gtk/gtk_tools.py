import os
import sys

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf

# import gobject
# import gtk

# Sizes
GIRI_GUI_MARGIN = 8
GIRI_GUI_BUTTON_HEIGHT = 25
GIRI_GUI_BUTTON_AREA_HEIGHT = GIRI_GUI_BUTTON_HEIGHT + (4 * GIRI_GUI_MARGIN)


## Image definitions
# GIRI_GUI_INSTALL_BANNER = "g_install_banner.bmp"
# GIRI_GUI_UPDATE_BANNER = "g_update_banner.bmp"
# GIRI_GUI_REMOVE_BANNER = "g_remove_banner.bmp"
# GIRI_GUI_ENROLL_BANNER = "g_enroll_banner.bmp"
# GIRI_GUI_UNINSTALL_BANNER = "g_uninstall_banner.bmp"

def locate_image_path(path=''):
    """ Locate a path for images, searching through relevant options. """
    temp_path = os.path.abspath(os.path.join(path, 'gtk', 'images'))
    if os.path.exists(temp_path):
        return temp_path
    return path


def locate_image(path='', filename=''):
    """ Find an icon to be displayed in a Gtk environment. """

    location = os.path.abspath(os.path.join(locate_image_path(path), filename))

    icon = Gtk.Image()
    if not os.path.isfile(location):
        defaulticon = "giritech.bmp"
        location = os.path.abspath(os.path.join(locate_image_path(path), defaulticon))
    icon.set_from_file(location)
    return icon


def get_pixbuf(path='', filename=''):
    location = os.path.abspath(os.path.join(locate_image_path(path), filename))
    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=location, width=1500, height=80,
                                                     preserve_aspect_ratio=False)
    return pixbuf


def get_icon_pixbuf(path='', filename=''):
    location = os.path.abspath(os.path.join(locate_image_path(path), filename))
    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=location, width=32, height=32,
                                                     preserve_aspect_ratio=False)
    return pixbuf
