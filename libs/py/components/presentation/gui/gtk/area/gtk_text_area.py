"""
Generic GTK based text displaying area.
"""

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

# import gobject
# import gtk

from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea

class GtkTextArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)
        
        # Currently visible model.
        self.currentheadline = ""
        self.currenttext = ""

        # Create an area for text.
        self.background = self.create_background_widget()
        self.background.set_size_request(450, 100)
        self.widgets.append(self.background) 
#        
        self.textcontainer = Gtk.VBox(homogeneous=False, spacing=6)
        self.textcontainer.set_border_width(6)
        self.background.add(self.textcontainer)

        self.headline = self.create_text_widget()
        self.textcontainer.pack_start(self.headline, expand=False, fill=True, padding=0)
        
        # Create a text area. 
        self.textview = Gtk.Label()
        self.textview.set_alignment(0, 0)
        self.textview.set_line_wrap(True)
        self.textview.set_padding(self.margin, self.margin)
        self.textview.set_markup(self.currenttext)
        # self.textview.connect("size_allocate", self._resize)
        self.textcontainer.add(self.textview)

        self.separator = Gtk.HSeparator()
        self.container.pack_start(self.separator, expand=False, fill=True, padding=0)
        self.widgets.append(self.separator)

    # def _resize(self, sender, rect):
    #     """ Needed for resizing the content properly.
    #         (workaround for a known issue with label resizing).
    #     """
    #     # sender.set_size_request(rect.width, -1)

    def update(self, model):
        """ Update relevant parts of the area. """
        GObject.idle_add(self.update_in_gui_thread, model)
    
    def update_in_gui_thread(self, model):
        """ Update relevant parts of the area using the GUI thread. """
        if self.currentheadline != model.info_headline:
            self.headline.set_markup('<b>' + model.info_headline + '</b>')
            self.currentheadline = model.info_headline
        if self.currenttext != model.info_text:
            self.textview.set_markup(model.info_text)
            self.currenttext = model.info_text

    def expands(self, expands):
        """ Set whether this area should expand on resize. """
        self.container.set_child_packing(self.background, expands, True, 0, Gtk.PackType.START)
