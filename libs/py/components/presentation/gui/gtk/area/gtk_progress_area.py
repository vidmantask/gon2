"""
Generic GTK based progress area.
"""

# import gobject
# import gtk

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea

class GtkProgressArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)

        # Currently visible model
        self.currentinfocaption = "Headline"
        self.currentprogress = 0
        self.currentprogresssubtext = "Subtext"     
        self.currentprogressmode = None

        # Create widgets.
        self.background = self.create_background_widget()
        self.background.set_size_request(450, 120)
        self.widgets.append(self.background)
        # Create a container for the widgets in this area.
        self.progresscontainer = Gtk.VBox(homogeneous=False, spacing=6)
        self.progresscontainer.set_border_width(6)
        self.background.add(self.progresscontainer)
        # Create an info header.
        self.headline = self.create_text_widget()
        self.progresscontainer.pack_start(self.headline, expand=False, fill=True, padding=0)
        # Create a progress bar.
        self.progressbar = Gtk.ProgressBar()
        self.progresscontainer.pack_start(self.progressbar, expand=False, fill=True, padding=0)
        # Create a bottom separator.
        self.separator = Gtk.HSeparator()
        self.container.pack_start(self.separator, expand=False, fill=True, padding=0)
        self.widgets.append(self.separator)

    def _update_indefinite_progress_bar(self, model):
        """ Method for timed calls for updating the indefinite progress bar. """
        if model.info_mode == self.controller.ID_MODE_INFORMATION_PROGRESS:
            if model.info_progress_mode == self.controller.ID_MODE_PROGRESS_UNKNOWN:
                self.progressbar.pulse()
                return True
        return False

    def update(self, model):
        """ Update all elements in this area. """
        GObject.idle_add(self.update_in_gui_thread, model)
        
    def update_in_gui_thread(self, model):
        """ Do updates in the GUI thread. """
        if self.currentinfocaption != model.info_headline:
            self.headline.set_markup('<b>' + model.info_headline + '</b>')
            self.currentinfocaption = model.info_headline
        if self.currentprogress != model.info_progress_complete:
            self.progressbar.set_fraction(model.info_progress_complete/100.0)
            self.currentprogress = model.info_progress_complete
        if self.currentprogresssubtext != model.info_progress_subtext:
            self.progressbar.set_text(model.info_progress_subtext)
            self.currentprogresssubtext = model.info_progress_subtext
        if self.currentprogressmode != model.info_progress_mode:
            if model.info_progress_mode == self.controller.ID_MODE_PROGRESS_UNKNOWN: 
                self.progressbar.set_pulse_step(0.05)
                self.timeout_handler_id = GObject.timeout_add(100, self._update_indefinite_progress_bar, model)
            if model.info_progress_mode == self.controller.ID_MODE_PROGRESS_KNOWN:
                self.progressbar.set_fraction(model.info_progress_complete/100)
            self.currentprogressmode = model.info_progress_mode

    def expands(self, expands):
        """ Set whether this area should expand on resize. """
        self.container.set_child_packing(self.background, expands, True, 0, Gtk.PackType.START)

