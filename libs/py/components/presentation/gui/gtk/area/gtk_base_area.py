""" 

Base Class for GTK based areas

"""

# import gobject
# import gtk

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gdk

class GtkBaseArea:
    """ A base area frame for all areas. """

    def __init__(self, container, controller, configuration=None):

        # self.parent = parent
        self.container = container
        self.controller = controller
        self.configuration = configuration
        self.dictionary = configuration.dictionary if configuration else None

        self.widgets = []
        self.visible = True
        self.margin = 6
        self.large_margin = 4 * self.margin

        self.left = 0
        self.top = 0
        self.width = 0
        self.height = 0

    def create_button_widget(self):
        widget = Gtk.Button()
        # widget.set_flags(Gtk.CAN_DEFAULT)
        # widget.set_data('button-clicked', False)
        widget.set_data_button_clicked = False
        return widget

    def create_background_widget(self):
        widget = Gtk.HBox()
        widget.set_name("background_widget")
        self.container.pack_start(widget, expand=True, fill=True, padding=0)
        return widget

    def create_text_widget(self):
        widget = Gtk.Label()
        widget.set_single_line_mode(True)
        widget.set_alignment(0, 0)
        return widget

    #    def create_large_text_widget(self):
    #        pass
    #
    #    def create_small_text_widget(self):
    #        pass

    def create_text_input_widget(self, defaulttext=""):
        """ Create a widget for text input. """
        widget = Gtk.Entry()
        widget.set_activates_default(True)
        widget.set_placeholder_text(defaulttext)
        return widget

    def create_secret_text_input_widget(self, defaulttext=""):
        widget = Gtk.Entry()
        widget.set_input_purpose(Gtk.InputPurpose.PASSWORD)
        widget.set_visibility(False)
        widget.set_activates_default(True)
        widget.set_placeholder_text(defaulttext)
        widget.set_property("caps-lock-warning", True)
        return widget

    def create_separator_widget(self):
        pass

    def set_sizes(self, left=0, top=0, width=0, height=0):
        """ Set the size of this area. """
        (self.left, self.top, self.width, self.height) = (left, top, width, height)

    def hide(self):
        """ Hide all widgets in this area. """
        for widget in self.widgets:
            # widget.hide_all()
            widget.hide()
            widget.set_no_show_all(True)
        self.visible = False

    def show(self):
        """ Show all widgets in this area. """
        for widget in self.widgets:
            widget.set_no_show_all(False)
            widget.show_all()
        self.visible = True
