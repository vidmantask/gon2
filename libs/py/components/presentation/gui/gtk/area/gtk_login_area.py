from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
from components.presentation.login import LoginController
import gi; gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
Generic GTK based user-name/password login area.
"""


class GtkLoginArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)
        
        # Currently visible model.
        self.current_state = 'login'
        self.current_suggestedusername = ""
        self.current_lockusernamefield = False
        
        self.background = self.create_background_widget()
        # Create a table for the login fields.
        self.logintable = Gtk.Table(4, 1, homogeneous=True)
        self.logintable.set_border_width(self.margin)
        self.verticalbox = Gtk.VBox(False, self.large_margin)
        self.background.add(self.verticalbox)
        # Make sure that the fields are centered in the area.
        self.verticalbox.pack_start(Gtk.HBox(), expand=False, fill=True, padding=0)
        self.verticalbox.pack_start(self.logintable, expand=False, fill=True, padding=0)
        self.verticalbox.pack_start(Gtk.HBox(), expand=False, fill=True, padding=0)
        
        self.usernamefield = self.create_text_input_widget(self.dictionary._("User name"))
        #self.usernamefield.set_name("ID_LOGIN_TEXTFIELD")
        self.logintable.attach(self.usernamefield, 0, 1, 0, 1)

        self.passwordfield = self.create_secret_text_input_widget(self.dictionary._("Password"))
        self.logintable.attach(self.passwordfield, 0, 1, 1, 2)

        self.newpasswordfield = self.create_secret_text_input_widget(self.dictionary._("New password"))
        self.logintable.attach(self.newpasswordfield, 0, 1, 2, 3)
        self.newpasswordfield.set_no_show_all(True)

        self.confirmnewpasswordfield = self.create_secret_text_input_widget(self.dictionary._("Confirm new password"))
        self.logintable.attach(self.confirmnewpasswordfield, 0, 1, 3, 4)
        self.confirmnewpasswordfield.set_no_show_all(True)

        # Create a separator for this area.
        self.container.pack_start(Gtk.HSeparator(), expand=False, fill=True, padding=0)

    def update(self, model):
        """ Update relevant parts of the area. """
        GObject.idle_add(self.update_in_gui_thread, model)
    
    def update_in_gui_thread(self, model):
        """ Update relevant parts of the area using the GUI thread. """
        
        if self.current_suggestedusername != model.suggestedusername:
            self.usernamefield.set_text(model.suggestedusername)
            self.current_suggestedusername = model.suggestedusername

        if self.current_lockusernamefield != model.lockusernamefield:
            self.usernamefield.set_sensitive(not model.lockusernamefield)
            self.current_lockusernamefield = model.lockusernamefield
        
        if self.current_state != model.state:
            # Set widgets for changing the password
            if model.state == LoginController.ID_STATE_CHANGE_LOGIN:
                self.newpasswordfield.set_no_show_all(False)
                self.newpasswordfield.set_visibility(False)
                self.newpasswordfield.show_all()
                self.confirmnewpasswordfield.set_no_show_all(False)
                self.confirmnewpasswordfield.set_visibility(False)
                self.confirmnewpasswordfield.show_all()
                #self.view.set_size_request(self.windowwidth, self.windowheight + self.changepasswordextraheight)
            # Set widgets for ordinary login
            elif model.state == LoginController.ID_STATE_LOGIN:
                self.newpasswordfield.hide_all()
                self.newpasswordfield.set_no_show_all(True)
                self.confirmnewpasswordfield.hide_all()
                self.confirmnewpasswordfield.set_no_show_all(True)
                #self.view.set_size_request(self.windowwidth, self.windowheight) 
            # This should never happen.
            else:
                print "Gtk_login_view.update reached an unexpected state."
            self.current_state = model.state
    
            
