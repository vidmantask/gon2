from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, WebKit2


"""
Generic GTK based HTML displaying area.
"""


class GtkHtmlArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)
        
        # Currently visible model.
        self.current_text = ""

        # Create an area for displaying HTML.
        scrollbox = Gtk.ScrolledWindow()
        # scrollbox.set_size_request(450, 200)
        scrollbox.set_size_request(375, 175)
        # scrollbox.set_policy(Gtk.POLICY_AUTOMATIC, Gtk.POLICY_AUTOMATIC)
        self.container.pack_start(scrollbox, expand=True, fill=True, padding=0)
        
        # Create an HTML widget. 
        self.htmlview = WebKit2.WebView()
        scrollbox.add(self.htmlview)

    def update(self, model):
        """ Update relevant parts of the area. """
        GObject.idle_add(self.update_in_gui_thread, model)
    
    def update_in_gui_thread(self, model):
        """ Update relevant parts of the area using the GUI thread. """
        if self.current_text != model.text:
            # self.htmlview.load_string(model.html_base.substitute(g_html_text=model.text.encode('utf-8', 'ignore')),
            #                           "text/html",
            #                           "utf-8",
            #                           "")
            self.htmlview.load_html(model.html_base.substitute(g_html_text=model.text.encode('utf-8', 'ignore')), None)

            self.current_text = model.text
