"""
Generic GTK based header area.
"""

# import gtk

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

from components.presentation.gui.gtk.gtk_tools import locate_image_path, locate_image, get_pixbuf
from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
#from components.presentation.login import LoginController
from components.presentation.update import UpdateController
from components.presentation.login import LoginController

class GtkHeaderArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)

        self.image_path = self.configuration.gui_image_path
        # Currently visible model
        self.current_banner_image = "g_install_banner.bmp"
        self.current_banner_id = UpdateController.ID_BANNER_INSTALL

        # Create widgets.
        self.headerbox = Gtk.HBox()
        self.headerbox.set_name("background_color_white")
        # self.packagebitmap = locate_image(path=self.image_path, filename = self.current_banner_image)
        # self.pixbuf = get_pixbuf(path=self.image_path, filename=self.current_banner_image)

        # Left side of the header
        self.left_side_img = Gtk.Image()
        self.headerbox.pack_start(self.left_side_img, expand=False, fill=True, padding=0)
        # self.left_side_img.set_from_pixbuf(self.pixbuf.new_subpixbuf(0, 0, 200, 80))

        # Right side of the header
        self.right_side_img = Gtk.Image()
        self.headerbox.pack_end(self.right_side_img, expand=False, fill=True, padding=0)
        # self.right_side_img.set_from_pixbuf(self.pixbuf.new_subpixbuf(250, 0, 200, 80))

        self.container.pack_start(self.headerbox, expand=False, fill=True, padding=0)
        self.container.pack_start(Gtk.HSeparator(), expand=False, fill=True, padding=0)

    def update(self, model):
        """ Update relevant parts of the header area. """
        if self.current_banner_id != model.banner_id:
            if model.banner_id == UpdateController.ID_BANNER_INSTALL:
                self.current_banner_image = "g_install_banner.bmp"
            elif model.banner_id == UpdateController.ID_BANNER_UPDATE:
                self.current_banner_image = "g_update_banner.bmp"
            elif model.banner_id == UpdateController.ID_BANNER_REMOVE:
                self.current_banner_image = "g_remove_banner.bmp"
                self.headerbox.set_name("background_color_dark")
            elif model.banner_id == UpdateController.ID_BANNER_ENROLL:
                self.current_banner_image = "g_enroll_banner.bmp"
            elif model.banner_id == UpdateController.ID_BANNER_UNINSTALL:
                self.current_banner_image = "g_uninstall_banner.bmp"
                self.headerbox.set_name("background_color_dark")
            elif model.banner_id == LoginController.ID_BANNER_LOGIN:
                self.current_banner_image = "g_login_banner.bmp"
            else:
                self.current_banner_image = None

            if self.current_banner_image is not None:
                # Old stuffs
                # self.packagebitmap.set_from_file(locate_image_path(path=self.image_path) + '/' + self.current_banner_image)
                # self.packagebitmap.set_alignment(0.0, 0.0)

                # New stuffs
                self.pixbuf = get_pixbuf(path=self.image_path, filename=self.current_banner_image)
                self.left_side_img.set_from_pixbuf(self.pixbuf.new_subpixbuf(0, 0, 200, 80))
                self.right_side_img.set_from_pixbuf(self.pixbuf.new_subpixbuf(250, 0, 200, 80))

            else:
                # Old stuffs
                # self.packagebitmap.clear()

                # New stuffs
                self.left_side_img.clear()
                self.right_side_img.clear()

            self.current_banner_id = model.banner_id

    # ------------------------------------------------------------------
    # EXAMPLE CODE
    #
    # self.pixbuf = get_pixbuf(path=self.image_path, filename=self.current_banner_image)
    # self.packagebitmap = Gtk.Image()
    # self.packagebitmap.set_from_pixbuf(self.pixbuf)
    #
    # pixbuf2 = self.pixbuf.new_subpixbuf(0, 0, self._box_width, 80)
    # widget.set_from_pixbuf(pixbuf2)
