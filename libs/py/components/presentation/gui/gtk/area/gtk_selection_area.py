from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
from components.presentation.gui.gtk.gtk_tools import locate_image_path
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('PangoCairo', '1.0')
from gi.repository import Gtk, GObject, Gdk, PangoCairo, GdkPixbuf, Pango


"""

Generic GTK based selection area.

"""


class GtkSelectionArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)

        # Currently visible model
        self.currentselectionlist = []
        self.currentselectionmethod = self.controller.ID_SELECTION_MULTIPLE
        self.currentselectionid = -1  # Will be updated by update method

        # Create widgets.
        self.packageselectionbox = Gtk.EventBox()
        #  self.packageselectionbox.set_border_width(6)
        self.packageselectionbox.set_size_request(450, 240)
        self.container.pack_start(self.packageselectionbox, expand=True, fill=True, padding=0)
        self.widgets.append(self.packageselectionbox)
        
        # Make the table contents scroll-able.
        scrollbox = Gtk.ScrolledWindow()
        # scrollbox.set_shadow_type(Gtk.SHADOW_IN)
        # scrollbox.set_policy(Gtk.POLICY_AUTOMATIC, Gtk.POLICY_AUTOMATIC)
        self.packageselectionbox.add(scrollbox)
        # Create a package table
        self.liststore = Gtk.ListStore(
            GObject.TYPE_BOOLEAN, GObject.TYPE_STRING, GObject.TYPE_STRING, GObject.TYPE_STRING)
        self.packageselectiontable = Gtk.TreeView(model=self.liststore)
        self.packageselectiontable.set_rules_hint(True)
        self.packageselectiontable.set_headers_visible(False)
        # self.packageselectiontable.connect("cursor_changed", self.on_row_activated)
        scrollbox.add(self.packageselectiontable)
        
        # Make it possible to select rows in single select mode
        treeselection = self.packageselectiontable.get_selection()
        treeselection.connect("changed", self.on_selected)
        
        # Create a column for toggling selections.
        self.selectioncolumn = Gtk.TreeViewColumn('')
        self.packageselectiontable.append_column(self.selectioncolumn)
        self.checkbox = Gtk.CellRendererToggle()
        self.checkbox.set_property('activatable', True)
        self.checkbox.connect("toggled", self.on_toggled)
        self.selectioncolumn.pack_start(self.checkbox, True)        
        self.selectioncolumn.add_attribute(self.checkbox, 'active', 0)
        
        # Create an info column.
        self.tilerenderer = TileCellRenderer()
        self.infocolumn = Gtk.TreeViewColumn('', self.tilerenderer)
        self.infocolumn.set_cell_data_func(self.tilerenderer, self.add_cell_data)
        self.infocolumn.set_expand(True)
        self.packageselectiontable.append_column(self.infocolumn)

    def show_packages(self, model):
        """ Show the relevant packages in the package info table. 
        
            TODO:
            For now we brute force this. Remove all and then
            add all elements in the list. Could be done more
            elegantly. 
        """
        self.liststore.clear()
        
        for index in range(0, len(model.selection_list)):
            self.liststore.append([model.selection_list[index]['selected'],
                                   model.selection_list[index]['name'],
                                   model.selection_list[index]['details'],
                                   model.selection_list[index]['icon']])
        
        self.packageselectiontable.columns_autosize()

    def on_selected(self, selection):
        """ Set the new selected value in the model. """
        (_, t_iter) = selection.get_selected()
        i = self.liststore.get_path(t_iter)[0]
        self.controller.set_selection_user_highlighted(i)

    def on_toggled(self, _widget, path):
        """ Set the new toggle value in GUI and model. 

            TODO: 
            The set_value should be removed when we update 
            properly by alerts.
            It requires a better check for changes in the model
            For now we check if the list is a new object, so we 
            do not catch changes within the object.
        """
        iter = self.liststore.get_iter(path)
        obj = self.liststore.get_value(iter, 0)
        self.controller.set_selection_via_index(int(path), not obj)            
        self.liststore.set_value(iter, 0, not obj)

    def update(self, model):
        """ Update all elements in this area. """
        GObject.idle_add(self.update_in_gui_thread, model)
        
    def update_in_gui_thread(self, model):
        """ Do updates in the GUI thread. """
        if self.currentselectionlist != model.selection_list:
            self.show_packages(model)
            self.currentselectionlist = model.selection_list

        if self.currentselectionmethod != model.selection_method:
            if model.selection_method == self.controller.ID_SELECTION_SINGLE:
                self.selectioncolumn.set_visible(False)
            elif model.selection_method == self.controller.ID_SELECTION_MULTIPLE:
                self.selectioncolumn.set_visible(True)
            self.currentselectionmethod = model.selection_method
        if self.currentselectionid != model.selection_highlighted_id:
            
            for index, package in enumerate(model.selection_list):
                location = 0
                if package['id'] == model.selection_highlighted_id:
                    location = index
                    break
                 
            self.packageselectiontable.set_cursor(location)
            self.packageselectiontable.scroll_to_cell(location)
            self.currentselectionid = model.selection_highlighted_id

    def add_cell_data(self, _column, cell, model, tree_iter, _user_data):
        """
        Set the fields in the renderer

        :param _column:
        :param cell:
        :param model:
        :param tree_iter:
        :param _user_data:
        :return:
        """
        cell.name = model.get_value(tree_iter, 1)
        cell.details = model.get_value(tree_iter, 2)
        if model.get_value(tree_iter, 3) is not None:
            cell.icon = '%s/%s' % (locate_image_path(self.configuration.gui_image_path), model.get_value(tree_iter, 3))
        else:
            cell.icon = None


class TileCellRenderer(Gtk.CellRenderer):
    
    def __init__(self):
        Gtk.CellRenderer.__init__(self)
        self.name = ""
        self.details = ""
        self.icon = None
        
        self.height = 40
        self.image_width = 32
        self.margin = 8

        self.font_size = 10
        self.font = "Arial {}".format(self.font_size)

        self.layout = None

    def do_get_aligned_area(self, _widget, _flags, cell_area):
        """
        Gets the aligned area used by self inside cell_area. Used for finding the appropriate edit and focus rectangle.

        :param _widget: Widget
        :param _flags: CellRendererState
        :param cell_area: Gdk.Rectangle
        :return: Gdk.Rectangle
        """
        return cell_area

    def do_get_size(self, _widget, _cell_area):
        """
        Return the size of the cell

        :param _widget: Gtk.Widget
        :param _cell_area: Gdk.Rectangle Seems to always be None?
        :return: Gdk.Rectangle
        """
        # print "TileCellRenderer.do_get_size()", _widget, _cell_area
        # More or less random width - I use the suggested container width - it seems to expand from there...
        return 0, 0, 450, self.height

    def do_render(self, ctx, _widget, _background_area, cell_area, _flags):
        """
        Draw elements in the tiled cell

        :param ctx: Cairo.Context
        :param _widget: Gtk.Widget
        :param _background_area: Gdk.Rectangle
        :param cell_area: Gdk.Rectangle
        :param _flags: CellRendererState
        :return: Gdk.Rectangle
        """
        # print "TileCellRenderer.do_render"

        if self.layout is None:
            self.layout = PangoCairo.create_layout(ctx)
            desc = Pango.font_description_from_string(self.font)
            self.layout.set_font_description(desc)

            self.layout.set_wrap(Pango.WrapMode.WORD)
            self.layout.set_ellipsize(Pango.EllipsizeMode.END)

        # Write Icon
        if self.icon is not None:
            ctx.save()
            Gdk.cairo_set_source_pixbuf(
                ctx,
                GdkPixbuf.Pixbuf.new_from_file(self.icon),
                cell_area.x + self.margin/2,
                cell_area.y + self.margin/2)
            ctx.paint()
            ctx.restore()

        # Write Name
        self.layout.set_markup(self.name, -1)
        ctx.save()
        PangoCairo.update_layout(ctx, self.layout)
        ctx.move_to(cell_area.x + self.image_width + self.margin, cell_area.y + self.margin / 2)
        PangoCairo.show_layout(ctx, self.layout)
        ctx.restore()

        # Write details
        self.layout.set_markup("<span foreground='dimgray'>" + self.details + "</span>", -1)
        ctx.save()
        PangoCairo.update_layout(ctx, self.layout)
        ctx.move_to(cell_area.x + self.image_width + self.margin, cell_area.y + 15 + self.margin / 2)
        PangoCairo.show_layout(ctx, self.layout)
        ctx.restore()


GObject.type_register(TileCellRenderer)
