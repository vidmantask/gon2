from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
from components.presentation.gui.gtk.gtk_tools import locate_image_path
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
Generic GTK based phase displaying area.
"""


class GtkPhaseStatusArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)

        self.image_path = configuration.gui_image_path

        # Currently visible model
        self.currentphaselist = []

        # Create widgets
        self.phasesbox = Gtk.EventBox()
        self.phasesbox.set_size_request(450, 30)
        self.container.pack_start(self.phasesbox, expand=False, fill=True, padding=0)
        # Create a horizontal container.
        self.phasescontainer = Gtk.HBox(homogeneous=False, spacing=6)
        self.phasescontainer.set_name("background_color_white")
        self.phasesbox.add(self.phasescontainer)
        # Create an array of images.
        self.phaseboxarrows = []
        for index in range(4):
            self.phaseboxarrows.append(Gtk.Image())
        # Create an array of labels.
        self.phaseboxtexts = []
        for index in range(5):
            self.phaseboxtexts.append(Gtk.Label())
        # Show relevant phases labels and images.
        #self.show_phases()

        self.container.pack_start(Gtk.HSeparator(), expand=False, fill=True, padding=0)

    def _show_phases(self, model):
        """ Show the relevant phase images and labels. """
        # If there are phases to show ...
        if (len(model.phase_list) > 0):
            # Manipulate the visible texts...
            for index in range(0, len(model.phase_list)):
                # Set the text appearance.
                if model.phase_list[index]['state'] == self.controller.ID_PHASE_PENDING:
                    self.phaseboxtexts[index].set_markup("<span color='gray'>" + model.phase_list[index]['name'] + "</span>")
                elif model.phase_list[index]['state'] == self.controller.ID_PHASE_ACTIVE:
                    self.phaseboxtexts[index].set_markup("<b>" + model.phase_list[index]['name'] + "</b>")
                else: # Assume complete
                    self.phaseboxtexts[index].set_markup(model.phase_list[index]['name'])
                # Add the label if it is not in the container.
                if self.phaseboxtexts[index] not in self.phasescontainer:
                    self.phasescontainer.add(self.phaseboxtexts[index])
                    self.phaseboxtexts[index].show_all()
            # Manipulate the visible images...
                if index < len(model.phase_list)-1:
                    if model.phase_list[index]['state'] == self.controller.ID_PHASE_PENDING:
                        self.phaseboxarrows[index].set_from_file(locate_image_path(path=self.image_path) + '/g_arrow_grey.bmp')
                    elif model.phase_list[index]['state'] == self.controller.ID_PHASE_ACTIVE:
                        self.phaseboxarrows[index].set_from_file(locate_image_path(path=self.image_path) + '/g_arrow_grey.bmp')
                    else: # Assume complete
                        self.phaseboxarrows[index].set_from_file(locate_image_path(path=self.image_path) + '/g_arrow_red.bmp')
                    # Add the image if it is not in the container.
                    if self.phaseboxarrows[index] not in self.phasescontainer:
                        self.phasescontainer.add(self.phaseboxarrows[index])
                        self.phaseboxarrows[index].show_now()
                    
            for index in range(len(model.phase_list), len(self.currentphaselist)):
                self.phasescontainer.remove(self.phaseboxtexts[index])
                self.phaseboxtexts[index].set_property('visible', False)
                self.phasescontainer.remove(self.phaseboxarrows[index-1])
                self.phaseboxarrows[index-1].set_property('visible', False)
        else: # The phase list is empty - clear the container.
            children = self.phasescontainer.get_children()
            for index in range(0, len(self.phasescontainer.get_children())):
                self.phasescontainer.remove(children[index])

    def update(self, model):
        """ Update all elements in this area. """
        GObject.idle_add(self.update_in_gui_thread, model)
        
    def update_in_gui_thread(self, model):
        """ Do updates in the GUI thread. """
        if self.currentphaselist != model.phase_list:
            self._show_phases(model)
            self.currentphaselist = model.phase_list
