from components.presentation.gui.gtk.area.gtk_base_area import GtkBaseArea
from components.presentation.base.button_area_base import ButtonAreaControllerBase
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
Generic GTK based button area.
"""


class GtkButtonArea(GtkBaseArea):

    def __init__(self, container, controller, configuration):
        GtkBaseArea.__init__(self, container, controller, configuration)

        # Currently visible model
        self.currentcancelallowed = True
        self.currentcancellabel = self.dictionary._("Cancel")
        self.currentcancelvisible = True
        self.currentnextallowed = True
        self.currentnextlabel = self.dictionary._("Next")
        self.currentnextvisible = True
        self.current_default_button = None

        buttonbox = Gtk.HButtonBox()
        buttonbox.set_border_width(self.margin*2)
        buttonbox.set_layout(Gtk.ButtonBoxStyle.END)
        buttonbox.set_spacing(self.margin)
        self.container.pack_end(buttonbox, expand=False, fill=True, padding=0)

        # Create a cancel button
        self.cancelbutton = self.create_button_widget()
        self.cancelbutton.set_label(self.currentcancellabel)
        self.cancelbutton.set_data_button_method = 'cancel'
        self.cancelbutton.set_can_default(True)
        self.cancelbutton.connect('clicked', self._on_button_clicked) 
        buttonbox.add(self.cancelbutton)

        # Create an OK button and set it to default.
        self.nextbutton = self.create_button_widget()
        self.nextbutton.set_label(self.currentnextlabel)
        self.nextbutton.set_data_button_method = 'next'
        self.nextbutton.set_can_default(True)
        self.nextbutton.connect('clicked', self._on_button_clicked)
        buttonbox.add(self.nextbutton)
        # Make the next button the default
        self.container.get_parent().set_default(self.nextbutton)
        # Setup shortcuts (ESC)
        # accelerators = Gtk.AccelGroup()
        # key, modifier = Gtk.accelerator_parse('Escape')
        # accelerators.connect_group(key, modifier, Gtk.ACCEL_VISIBLE, self._esc_key_pressed)
        # self.container.get_parent().add_accel_group(accelerators)

    def _esc_key_pressed(self, *args):
        """ Handle that the user pressed ESC. """
        if self.currentcancelallowed:
            self._on_button_clicked(self.cancelbutton)

    def _on_button_clicked(self, widget):
        """ called when a button is selected. """
        widget.set_data_button_clicked = True
        self.controller.model.alert_subscribers()

    def update(self, model):
        """ Update relevant elements in this area. """
        GObject.idle_add(self.update_in_gui_thread, model)
        
    def update_in_gui_thread(self, model):
        """ Update relevant parts of the area using the GUI thread. """
        # Update next button enabled state.
        if self.currentnextallowed != model.next_allowed:
            self.nextbutton.set_sensitive(model.next_allowed)
            self.currentnextallowed = model.next_allowed
        # Update the next button label.
        if self.currentnextlabel != model.next_label:
            self.nextbutton.set_label(model.next_label)
            self.currentnextlabel = model.next_label
        # Update the next button visibility.
        if self.currentnextvisible != model.next_visible:
            if model.next_visible:
                self.nextbutton.set_no_show_all(False)
                self.nextbutton.show_all()
            else:
                self.nextbutton.hide_all()
                self.nextbutton.set_no_show_all(True)
                
        # Update cancel button enabled state.
        if self.currentcancelallowed != model.cancel_allowed:
            self.cancelbutton.set_sensitive(model.cancel_allowed)
            self.currentcancelallowed = model.cancel_allowed
        # Update the cancel button label.
        if self.currentcancellabel != model.cancel_label:
            self.cancelbutton.set_label(model.cancel_label)
            self.currentcancellabel = model.cancel_label
        # Update the cancel button visibility.
        if self.currentcancelvisible != model.cancel_visible:
            if model.cancel_visible:
                self.cancelbutton.set_no_show_all(False)
                self.cancelbutton.show_all()
            else:
                self.cancelbutton.hide_all()
                self.cancelbutton.set_no_show_all(True)
        # Update the default button setting
        if self.current_default_button != model.default_button:
            if model.default_button == ButtonAreaControllerBase.ID_NEXT_BUTTON:
                self.container.get_parent().set_default(self.nextbutton)
            elif model.default_button == ButtonAreaControllerBase.ID_CANCEL_BUTTON:
                self.container.get_parent().set_default(self.cancelbutton)
            self.current_default_button = model.default_button
