import lib.dictionary
import sys
from components.presentation.gui.gtk import gtk_tools
from components.presentation.gui.gtk.gtk_base_view import GtkBaseView
from components.presentation.common import CommonView
from string import Template  # Do not warn for deprecated module 'string' - it is not, pylint: disable=W0402
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, Gdk, Gio, GLib, WebKit2

"""

GTK version of the central GUI common view.

"""


class GOnWindow(Gtk.Window):

    def __init__ (self, app, handles, configuration, model):
        Gtk.Window.__init__(self, title="G/On", application=app)

        self.handles = handles
        self.configuration = configuration
        self.model = model
        self.html = Template("""
            <div>
                <svg xmlns:dc='http://purl.org/dc/elements/1.1/' xmlns:cc='http://creativecommons.org/ns#' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' xmlns:svg='http://www.w3.org/2000/svg' xmlns='http://www.w3.org/2000/svg' style='display:inline' version='1.1' id='svg2' height='75' width='240'> <defs id='defs4'> <marker style='overflow:visible' id='TriangleOutM' refX='0.0' refY='0.0' orient='auto'> <path transform='scale(0.4)' style='fill-rule:evenodd;stroke:#b7193f;stroke-width:1pt;stroke-opacity:1;fill:#b7193f;fill-opacity:1' d='M 5.77,0.0 L -2.88,5.0 L -2.88,-5.0 L 5.77,0.0 z ' id='path1021' /> </marker> </defs> <metadata id='metadata7'> <rdf:RDF> <cc:Work rdf:about=''> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource='http://purl.org/dc/dcmitype/StillImage' /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g transform='translate(-170,-494.36218)' id='layer1'> <g transform='translate(-77.749996)' id='g872'> <g id='g1107' style='display:inline;fill:#b7193f;fill-opacity:1;stroke-width:3.97216153' transform='matrix(0.25175211,0,0,0.25175211,298.83511,439.9941)'> <path style='clip-rule:evenodd;fill:#b7193f;fill-opacity:1;fill-rule:evenodd;stroke-width:3.97216153' id='path3' d='m 123.09442,415.04671 c -6.1,2.35 -18.31,5.789 -32.550003,5.789 -15.96,0 -29.1,-4.069 -39.43,-13.93 -9.08,-8.771 -14.71,-22.859 -14.71,-39.3 0,-31.48 21.75,-54.49 57.12,-54.49 12.200003,0 21.900003,2.66 26.440003,4.85 l -3.44,11.12 c -5.63,-2.66 -12.68,-4.54 -23.320003,-4.54 -25.66,0 -42.4,15.97 -42.4,42.43 0,26.771 15.96,42.59 40.68,42.59 8.920003,0 15.020003,-1.25 18.150003,-2.819 v -31.47 H 88.354417 v -10.96 h 34.740003 z' /> <polygon transform='translate(-54.115583,-51.43329)' style='clip-rule:evenodd;fill:#b7193f;fill-opacity:1;fill-rule:evenodd;stroke-width:3.97216153' id='polygon5' points='240.12,363.92 196.3,477.439 185.82,477.439 229.32,363.92 ' /> <path style='clip-rule:evenodd;fill:#b7193f;fill-opacity:1;fill-rule:evenodd;stroke-width:3.97216153' id='path7' d='m 231.37442,410.34671 c 21.6,0 33.81,-19.88 33.81,-43.84 0,-20.98 -10.96,-42.9 -33.65,-42.9 -22.69,0 -33.96,21.14 -33.96,44 0,22.229 12.21,42.74 33.65,42.74 z m -0.62,11.12 c -28.01,0 -47.42,-21.61 -47.42,-53.55 0,-33.51 20.66,-55.43 48.83,-55.43 28.951,0 47.57,22.08 47.57,53.39 0,36.17 -22.07,55.59 -48.82,55.59 z' /> <path style='clip-rule:evenodd;fill:#b7193f;fill-opacity:1;fill-rule:evenodd;stroke-width:3.97216153' id='path9' d='m 296.94542,364.46671 c 0,-7.98 -0.16,-14.24 -0.63,-20.51 h 12.209 l 0.78,12.37 h 0.32 c 3.75,-7.04 12.51,-14.09 25.029,-14.09 10.49,0 26.76,6.26 26.76,32.261 v 45.25 h -13.77 v -43.69 c 0,-12.21 -4.54,-22.55 -17.529,-22.55 -8.92,0 -15.961,6.42 -18.461,14.1 -0.63,1.72 -0.939,4.22 -0.939,6.42 v 45.721 h -13.77 v -55.282 z' /> <path d='m 403.30442,334.15671 -0.77,-11.57 c -0.09,-1.49 0,-3.32 -0.051,-5.2 h -0.14 c -0.479,1.78 -1.06,3.81 -1.641,5.49 l -3.989,11.04 h -2.22 l -3.9,-11.33 c -0.479,-1.54 -0.96,-3.42 -1.391,-5.2 h -0.149 c -0.05,1.83 0,3.47 -0.101,5.2 l -0.72,11.57 h -2.789 l 1.489,-19.18 h 3.811 l 3.75,10.41 c 0.479,1.44 0.87,3.03 1.35,4.82 h 0.101 c 0.43,-1.79 0.859,-3.47 1.35,-4.87 l 3.75,-10.36 h 3.76 l 1.44,19.18 z m -18.53,-19.18 v 2.46 h -5.88 v 16.72 h -2.89 v -16.72 h -5.82 v -2.46 z' id='path13' style='clip-rule:evenodd;fill:#b7193f;fill-opacity:1;fill-rule:evenodd;stroke-width:3.97216153' /> </g> </g> <path id='path874' d='m 220.25,533.86216 c -23.41308,0 -33,-0.54233 -33,-20.33056' style='fill:none;fill-rule:evenodd;stroke:#b7193f;stroke-width:2.99999976;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;marker-end:url(#TriangleOutM)' /> </g> </svg> 
            </div>
            <div style='position:absolute; text-align: center; font-family: arial; font-size: 14; font-style: italic; font-weight: bold; left:50px; top:120px; width:300px; height:50; color:#3A3A3A'>
                $g_html_splash_text
            </div>
            """)
        self.html_splash_text = ""
        # Set App name and default window
        self.set_wmclass("G/On", "G/On")
        self.set_default_icon_from_file('%s/%s' %
                                        (gtk_tools.locate_image_path(configuration.gui_image_path),
                                         'giritech.ico'))
        # Window setup
        self.connect("delete_event", self.get_application().quit_cb)
        self.set_resizable(False)
        self.set_type_hint(Gdk.WindowTypeHint.DIALOG)

        self.set_size_request(400, 200)
        self.layout = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(self.layout)

        # Generate menu bar
        menu_model = Gio.Menu.new()
        self.menu = Gio.Menu.new()
        menu_model.append_submenu('G/On', self.menu)
        self.menu_bar = Gtk.MenuBar.new_from_model(menu_model)
        # Location for the app menu
        self.handles['gon_app_menu'] = self.menu
        # Generate system menu
        self.system_menu = Gio.Menu.new()
        self.menu.insert_section(1, "G/On System", self.system_menu)
        quit_menu_item = Gio.MenuItem.new("Quit", "app.quit")
        # It looks cleaner without the icons - so commented out for now
        # quit_menu_item.set_icon(Gio.ThemedIcon.new('window-close'))
        self.system_menu.append_item(quit_menu_item)
        self.layout.pack_start(self.menu_bar, False, False, 0)
        # Background image as a html area
        scrollbox = Gtk.ScrolledWindow()
        self.layout.pack_start(scrollbox, expand=True, fill=True, padding=0)
        self.htmlview = WebKit2.WebView()
        scrollbox.add(self.htmlview)

    def update(self):
        GObject.idle_add(self.update_in_gui_thread)

    def update_in_gui_thread(self):
        if self.html_splash_text != self.model.splash_text:
            self.html_splash_text = self.model.splash_text
            self.htmlview.load_html(self.html.substitute(
                g_html_splash_text=self.html_splash_text.encode('utf-8', 'ignore')), None)


class GtkCommonView(Gtk.Application, CommonView):

    def __init__(self, model, common, name, configuration):
        """
        Display a starting point in system tray.
        """
        Gtk.Application.__init__(self)
        CommonView.__init__(self, model, common, name, configuration)

        css = """
            box#background_color_white {
                background-color: white;
            }
            box#background_color_dark {
                background-color: #3A3A3A;
            }
            box#background_widget {
                background-color: white;
            }
        """

        styleprovider = Gtk.CssProvider()
        styleprovider.load_from_data(bytes(css.encode()))
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            styleprovider,
            Gtk.STYLE_PROVIDER_PRIORITY_USER
        )

        self.handles['application'] = self
        self.views = []
        self._is_destroyed = False

    def do_activate(self):
        self.win = GOnWindow(self, self.handles, self.configuration, self.model)
        self.win.show_all()
        self.update()

        # Link to gtk menu view
        if 'on_main_menu_added' in self.handles:
            self.handles['on_main_menu_added']()
        # Add all the views to the app
        if self.views is not None:
            for view in self.views:
                if isinstance(view, GtkBaseView):
                    view.view.set_application(self)

    def do_startup(self):
        Gtk.Application.do_startup(self)

        menu = Gio.Menu.new()
        self.handles['gon_app_menu'] = menu
        menu_item_quit = Gio.MenuItem.new("Quit", "app.quit")
        menu.append_item(menu_item_quit)
        self.set_app_menu(menu)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_cb)
        self.add_action(quit_action)

    def quit_cb(self, _action, _parameter):
        self.controller.set_shutdown()  # I think we need this to shut down properly???

    def display(self, views=None):
        self.views = views
        try:
            Gdk.threads_init()
            self.run(sys.argv)
        except:
            (_, evalue, _) = sys.exc_info()
            print evalue

    def update(self):
        if hasattr(self, "win"):
            self.win.update()

    def destroy(self):
        """ Destroy the gui by ending the main loop. """
        if not self._is_destroyed:
            GObject.idle_add(self._destroy_in_gui_thread)
            self._is_destroyed = True

    def _destroy_in_gui_thread(self):
        """ Destroy the common view using commands in the GUI thread. """
        self.quit()
        return False


#
# Move this to a test area when appropriate.
#


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.common import CommonModel
    import time

    def common_subscriber():
        """ See if the client should be shut down.

            Subscribes to changes in the common model.
            This includes the shutdown variable that is set to
            'True' if something suggested that the client should
            be shut down. This could be the cancel button in the
            login dialog or a system menu item.
        """
        if commonview.controller.get_shutdown():
            print "Common subscriber was told to shut down!!!"
            time.sleep(2.0)
            commonview.controller.destroy()

    commonmodel = CommonModel()
    
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()
    
    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    commonview.controller.subscribe(common_subscriber)
    commonview.display()
