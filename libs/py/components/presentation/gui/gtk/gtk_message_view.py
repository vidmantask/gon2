from components.presentation.message import MessageView
from components.presentation.gui.gtk import gtk_tools
import lib.dictionary
import os
import subprocess
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject


"""
Gtk version of the GUI for messages.
"""


def show_notification(headline, message, icon_filename, timeout_ms):
    if not os.path.exists("/usr/bin/notify-send"):
        return False
    
    command = []
    command.append("/usr/bin/notify-send")
    if os.path.exists(icon_filename):
        command.extend(["-i", icon_filename])
    if timeout_ms > 0:
        command.extend(["-t", "%d" % timeout_ms])
    command.append('"%s"' % headline)
    command.append('"%s"' % message)
    rc = subprocess.call(command)  
    return rc == 0


class GtkMessageView(MessageView):
    """ Gtk based view of messages. """
    def __init__(self, model, common, name, configuration):
        MessageView.__init__(self, model, common, name, configuration)

        self.headline = ""
        self.message = ""
        self.timeout_ms = 2000
        #  self.image_path = imagepath
         
        #  Variables for status icon location
        # (self.screen, self.location, self.orientation) = self.handles['status_icon'].get_geometry()
        # self.iconwidth = self.handles['status_icon'].get_pixbuf().get_width()
        # self.iconheight = self.handles['status_icon'].get_pixbuf().get_height()
        # self.top = self.location.y < (self.screen.get_height()/2)
        # self.horizontal = self.orientation == Gtk.ORIENTATION_HORIZONTAL
 
    def display(self, views=None):
        GObject.idle_add(self.display_in_gui_thread)
        
    def display_in_gui_thread(self):
        """ Display a message with the current content.
        
            We can not use the attach_to_status_icon method
            because it seems to be blocking. Or rather the
            get_geometry method that it uses is. However
            if we only use it once in init it all seems to
            work fine. 
        """
        if not show_notification(self.headline,
                                 self.message,
                                 os.path.abspath(os.path.join(self.configuration.gui_image_path, "giritech_48x48.png")),
                                 self.timeout_ms):
            print "ERROR notification not showed :", self.headline
            
    def update(self):
        """ Update anything relevant to this view. 
        
            No gui parts are updated here. So just do it.
        """
        self.headline = self.model.headline
        self.message = self.model.message

    def destroy(self):
        pass


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
import threading
class thread_show_message(threading.Thread):

    def __init__(self, messageview):
        threading.Thread.__init__(self, name="thread_show_message")
        self.messageview = messageview

    def run(self):
        time.sleep(2)
        messageview.controller.set_message("Message from Giritech",
                                           "This is a message set via the message controller. " +
                                           "It may contain information from the server, " +
                                           "for example to notify the user if the menu has changed.")
        messageview.display()


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    import threading
    import time

    from components.presentation.common import CommonModel
    from components.presentation.gui.gtk.gtk_common_view import GtkCommonView
    from components.presentation.message import MessageModel

    # Create models
    commonmodel = CommonModel()
    messagemodel = MessageModel()

    handles = None
    dictionary = lib.dictionary.Dictionary()
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary

    # Create Gui parts
    commonview = GtkCommonView(commonmodel, handles, 'commonview', configuration)
    messageview = GtkMessageView(messagemodel, commonview.handles, 'messageview', configuration)

    message_thread = thread_show_message(messageview)
    message_thread.start()
    
    commonview.display()
    message_thread.join()
