'''

Specific Cocoa related test functionality.

'''

import AppKit

ID_PACKAGE_PLATFORM_IMAGE = "g_package_mac_32x32.bmp"
ID_TRAY_ICON = "tray-icon"

ID_LOGIN_TEXTFIELD = "login-text-field"
ID_PASSWORD_TEXTFIELD = "password-text-field"
ID_NEW_PASSWORD_TEXTFIELD = "new-password-text-field"
ID_NEW_PASSWORD_CONFIRM_TEXTFIELD = "confirm-new-password-text-field"

ID_PROGRESS_BAR = "progress-bar"
ID_PROGRESS_BAR_HEADLINE = "progress-bar-headline"
ID_PROGRESS_BAR_SUBTEXT = "progress-bar-sub-text"

ID_FREE_TEXT_HEADLINE = "free-text-headline"
ID_FREE_TEXT = "free-text"

ID_SELECTION_LIST = "selection-list"

ID_BUTTON_NEXT = "button-next"
ID_BUTTON_CANCEL = "button-cancel"

def set_widget_text(window, widget_id, text):
    """ Set the text in a text field widget. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    
    if widget_id == ID_LOGIN_TEXTFIELD:
        window.login.usernamefield.performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', text, True)
    elif widget_id == ID_PASSWORD_TEXTFIELD:
        window.login.passwordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', text, True)
    elif widget_id == ID_NEW_PASSWORD_TEXTFIELD:
        window.login.newpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', text, True)
    elif widget_id == ID_NEW_PASSWORD_CONFIRM_TEXTFIELD:
        window.login.confirmnewpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', text, True)

def get_widget_text(window, widget_id):
    """ Get the text in a text field widget. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    
    if widget_id == ID_LOGIN_TEXTFIELD:
        return window.login.usernamefield.stringValue()
    elif widget_id == ID_PASSWORD_TEXTFIELD:
        return window.login.passwordfield.stringValue()
    elif widget_id == ID_NEW_PASSWORD_TEXTFIELD:
        return window.login.newpasswordfield.stringValue()
    elif widget_id == ID_NEW_PASSWORD_CONFIRM_TEXTFIELD:
        return window.login.confirmnewpasswordfield.stringValue()
    elif widget_id == ID_PROGRESS_BAR_HEADLINE:
        return window.progress.headline.stringValue()
    elif widget_id == ID_PROGRESS_BAR_SUBTEXT:
        return window.progress.progresssubtext.stringValue()
    elif widget_id == ID_FREE_TEXT_HEADLINE:
        return window.info.headlineview.stringValue()
    elif widget_id == ID_FREE_TEXT:
        return window.info.textview.string()

def push_button(window, widget_id):
    """ Push a button with a given ID. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()

    if widget_id == ID_BUTTON_NEXT:
        window.buttons.nextbutton.performClick_(None)
    elif widget_id == ID_BUTTON_CANCEL:
        window.buttons.cancelbutton.performClick_(None)

def left_click_tray_icon(window, widget_id):
    """ Simulate a left click on the tray icon. """
    pass

def right_click_tray_icon(window, widget_id):
    """ Simulate a right click on the tray icon. """
    pass

def is_widget_enabled(window, widget_id):
    """ Find out if the widget is enabled. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    
    if widget_id == ID_BUTTON_NEXT:
        return window.buttons.nextbutton.isEnabled()
    elif widget_id == ID_BUTTON_CANCEL:
        return window.buttons.cancelbutton.isEnabled()

def is_widget_visible(window, widget_id):
    """ Find out if the widget is visible. """
    if widget_id == ID_BUTTON_NEXT:
        return not window.buttons.nextbutton.isHidden()
    elif widget_id == ID_BUTTON_CANCEL:
        return not window.buttons.cancelbutton.isHidden()

def get_menu_item_launch_id(window, menu_folder, item_number):
    """ Return the launch id associated with the given menu item. """
    if menu_folder.view.itemAtIndex_(item_number).tag() >= 0:
        return window.launchidmap[menu_folder.view.itemAtIndex_(item_number).tag()]
    else:
        return menu_folder.view.itemAtIndex_(item_number).tag()
    
def get_menu_launch_id_from_index(window, index):
    """ Get the actual launch id"""
    return index
    
def get_menu_item_count(menu_folder):
    """ Return the number of menu items in a given menu folder. """
    return menu_folder.view.numberOfItems()

def select_menu_item(window, widget_id):
    """ Select the menu item given by widget_id. """
    widget_id.view.menu().performActionForItemAtIndex_(widget_id.view.menu().indexOfItem_(widget_id.view))

def push_key_down():
    """ Simulate a key press (arrow down). """
    pass    

def push_key_right():
    """ Simulate a key press (arrow right). """
    pass
    
def push_key_return():
    """ Simulate a key press (return). """
    pass

def get_progress_bar_progress(window, widget_id):
    """ Get an integer status of the progression. """
    return window.progress.progressbar.doubleValue()
 
def get_selection_list_item_count(window, widget_id):
    """ Get the amount of items currently displayed. """
    return window.selection.packageview.numberOfRows()

def is_window_visible(window):
    """ Find out if a window is currently visible. """
    if hasattr(window, 'win'):
        return window.win.isVisible()
    else:
        return not window.isHidden()

def get_selection_list_item_text(window, widget_id, index):
    """ Get the text from the item in the selection list given by index. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    return window.selection.packageview.dataSource().tableView_objectValueForTableColumn_row_(window.selection.packageview, 
                                                                                              window.selection.packageview.tableColumnWithIdentifier_('name'),
                                                                                              index)

def get_selection_list_item_details(window, widget_id, index):
    """ Get the details-text from the item in the selection list given by index."""
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    return window.selection.packageview.dataSource().tableView_objectValueForTableColumn_row_(window.selection.packageview, 
                                                                                              window.selection.packageview.tableColumnWithIdentifier_('details'),
                                                                                              index)

def get_selection_list_item_selection_selected(window, widget_id, index):
    """ Get whether the indexed item is selected. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()
    
    return window.selection.packageview.dataSource().tableView_objectValueForTableColumn_row_(window.selection.packageview, 
                                                                                              window.selection.packageview.tableColumnWithIdentifier_('selected'),
                                                                                              index)

def get_window_frame_text(window):
    """ Get the text in the windows frame. """
    return window.win.title()

def set_selection_list_highlighted_item(window, widget_id, index):
    """ Set which item is highlighted in the selection list. """
    _pool = AppKit.NSAutoreleasePool.alloc().init()

    window.selection.packageview.selectRowIndexes_byExtendingSelection_(AppKit.NSIndexSet.indexSetWithIndex_(index), False)
    window.selection.packageview.scrollRowToVisible_(index)  
    window.selection.packageview.setNeedsDisplay_(True)
    window.selection.container.displayIfNeeded()
