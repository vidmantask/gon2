"""
Cocoa version of the GUI for menus.

"""
import objc
import lib.dictionary

#import AppKit
from AppKit import NSObject, NSMenuItem, NSAutoreleasePool, NSMenu, NSAppKitVersionNumber10_4
from components.presentation.menu import MenuView
from components.presentation.gui.cocoa.cocoa_tools import locate_icon

class CocoaMenuItemView(NSObject):
    """Displays a menu item"""

    @objc.python_method
    def initWithParameters(self, parent, menuitemmodel, image_path, launchidmap):
        """ View a single item in the menu.

            The view registers itself to its parent.
            So that individual updates can be
            performed.

            Keyword arguments:
            parent -- where the menu item is placed
            menuitemmodel -- the model for this item
        """
        self = super(CocoaMenuItemView, self).init()
        self.parent = parent
        self.id = menuitemmodel.id
        self.launchid = menuitemmodel.launch_id
        self.text = menuitemmodel.text
        self.iconfilename = menuitemmodel.icon
        self.enabled = menuitemmodel.enabled
        self.type = 'item'
        self.imagepath = image_path
        self.launchidmap = launchidmap

        self.mapid = len(self.launchidmap)
        self.launchidmap.append(self.launchid)

        self.view = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_(self.text, 'oncommand:', '')

        # TODO: For some reason this will not work when performed on the main thread.
        # TODO: Figure out why at some point.
        #self.view.setTag_(self.launchid)
        self.view.setTag_(self.mapid)

        # TODO: This should use the validateMenuItem informal protocol instead.
        # See: http://developer.apple.com/mac/library/documentation/Cocoa/Reference/ApplicationKit/Protocols/NSMenuValidation_Protocol/Reference/Reference.html#//apple_ref/occ/cat/NSMenuValidation
        # Keeping it as is for now. setting enabled state via main thread seems to not be possible.
        self.view.setEnabled_(self.enabled)

        if menuitemmodel.restricted == True:
            self.iconfilename = 'g_zone_restricted_action.bmp'

        if self.iconfilename != None:
            self.icon = locate_icon(path=self.imagepath, filename=self.iconfilename)
            self.view.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.icon, True)
        self.parent.view.performSelectorOnMainThread_withObject_waitUntilDone_('addItem:', self.view, True)
        parent.contentids[self.id] = self
        return self

    @objc.python_method
    def update(self, menuitemmodel):
        """ Update this item to reflect the model

        It is assumed that the id is correct. So to reflect
        the model we need this menuitem to have correct
        text and icon.
        """
        _pool = NSAutoreleasePool.alloc().init()

        # Set the correct text for the menu item
        if self.text != menuitemmodel.text:
            self.view.performSelectorOnMainThread_withObject_waitUntilDone_('setTitle:', menuitemmodel.text, True)
            self.text = menuitemmodel.text
        # Set the correct icon for the menu item
        if self.iconfilename != menuitemmodel.icon:

            if menuitemmodel.restricted == True:
                menuitemmodel.icon = 'g_zone_restricted_action.bmp'
                self.iconfilename = menuitemmodel.icon
                self.icon = locate_icon(path=self.imagepath, filename=self.iconfilename)
                self.view.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.icon, True)


            elif menuitemmodel.icon == None:
                self.view.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', None, True)
                self.iconfilename = None
            else:
                self.iconfilename = menuitemmodel.icon
                self.icon = locate_icon(path=self.imagepath, filename=self.iconfilename)
                self.view.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.icon, True)
        # Set the enabled state for this menu item
        if self.enabled != menuitemmodel.enabled:
            self.enabled = menuitemmodel.enabled
            self.view.setEnabled_(self.enabled)
            # TODO: This should use the validateMenuItem informal protocol instead.
            # See: http://developer.apple.com/mac/library/documentation/Cocoa/Reference/ApplicationKit/Protocols/NSMenuValidation_Protocol/Reference/Reference.html#//apple_ref/occ/cat/NSMenuValidation
            # Keeping it as is for now. setting enabled state via main thread seems to not be possible.

    @objc.python_method
    def remove_view(self):
        """ Remove the view for this item. """
        try:
            self.parent.view.performSelectorOnMainThread_withObject_waitUntilDone_('removeItem:', self.view, True)
        except:
            pass


class CocoaMenuFolderView(NSObject): #@UndefinedVariable
    """ Mfc view for a folder in the menu.

        Display this folder and iterate over the folders
        and items that it contains. If the parent folder
        is None it is not attached as a subfolder.

        Keyword arguments:
        parentview -- an cocoa popup menu or None
        modelfolder -- the model behind this folder
    """
    @objc.python_method
    def initWithParameters(self, parent, menufoldermodel, image_path, type):
        self = super(CocoaMenuFolderView, self).init()
        self.parent = parent
        self.id = menufoldermodel.id
        self.text = menufoldermodel.text
        self.iconfilename = menufoldermodel.icon
        self.contentids = {} # dictionary for looking up folders and items by id (nice for testing and updates).
        self.type = 'folder'
        self.imagepath = image_path

        # Create a top folder for the launch menu in the menu bar
        if self.id == -1 and type == 'appmenu':
            self.mainplaceholder = NSMenuItem.alloc().init()
            self.mainplaceholder.performSelectorOnMainThread_withObject_waitUntilDone_('setTitle:', 'gon_application_launch_menu', True)
            self.view = NSMenu.alloc().initWithTitle_('Launch')
            self.mainplaceholder.performSelectorOnMainThread_withObject_waitUntilDone_('setSubmenu:', self.view, True)
            self.performSelectorOnMainThread_withObject_waitUntilDone_('insertAtIndex', None, True)
        else:
            # create a placeholder item for the subfolder
            self.placeholder = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_(self.text, '', '')
            self.placeholder.setTag_(-1)
            if self.iconfilename != None:
                self.icon = locate_icon(path=self.imagepath, filename=self.iconfilename)
                self.placeholder.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.icon, True)
            # Create a subfolder
            self.view = NSMenu.alloc().initWithTitle_(self.parent.view.title()) #@UndefinedVariable
            self.placeholder.performSelectorOnMainThread_withObject_waitUntilDone_('setSubmenu:', self.view, True)
            self.parent.view.performSelectorOnMainThread_withObject_waitUntilDone_('addItem:', self.placeholder, True)
            parent.contentids[self.id] = self

        return self

    def insertAtIndex(self):
        """ Allows this to be run in main thread. """
        self.parent.insertItem_atIndex_(self.mainplaceholder, 1)

    @objc.python_method
    def update(self, menufoldermodel):
        """ Check and repair this folder view. """
        # Set the correct text for the menu folder
        if self.text != menufoldermodel.text:
            self.placeholder.performSelectorOnMainThread_withObject_waitUntilDone_('setTitle:', menufoldermodel.text, True)
            self.text = menufoldermodel.text
        # Set the correct icon for the menu folder
        if self.iconfilename != menufoldermodel.icon:
            if menufoldermodel.icon == None:
                self.placeholder.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', None, True)
                self.iconpath = None
            else:
                self.iconfilename = menufoldermodel.icon
                self.icon = locate_icon(path=self.imagepath, filename=self.iconfilename)
                self.placeholder.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.icon, True)

    def remove_view(self):
        """ Remove the view for this folder. """
        try:
            self.parent.view.performSelectorOnMainThread_withObject_waitUntilDone_('removeItem:', self.placeholder, True)
        except:
            pass


class CocoaMenuView(MenuView):
    """ A Cocoa based view for a menu.

        The menu consists of two parts: a system menu and
        an application menu. This top level method keeps
        track of both types. The two types are seperated by
        the name variable which can be either sysmenu or appmenu.

        The system menu is no longer visible to the user. So
        there is no need for keeping track of its view. But we
        still need the model and controller part. So we will
        just filter the calls (menu view creation and update).

        Memory Management: This is called from the main thread.
        So we do not need to setup an autorelease pool.
    """
    def __init__(self, model, handles, name, configuration):
        MenuView.__init__(self, model, handles, name, configuration)
        self.name = name
        self.handles = handles
        self.handles[name] = self
        self.model = model
        self.hasdonesetup = False
        self.launchidmap = []

        # If the main menu is already created - do setup.
        if self.handles.has_key('mainmenu'):
            self.setup()

    def setup(self):
        """ Setup an application menu for the main menu bar.

            I am assuming that this method is called from init
            which is called from the main thread.
        """
        if self.name == 'appmenu':
            self.topfolder = CocoaMenuFolderView.alloc().initWithParameters(self.handles['mainmenu'], self.model.topfolder, self.configuration.gui_image_path, self.name)
            self.topfolder.view.setAutoenablesItems_(False)
            self.hasdonesetup = True
            self.update()

    def _recursive_update(self, foldermodellocation, folderviewlocation):
        """ Recursively update the view of menu folders and items.

            Recursively controls consistency between model and
            view. Anything that does not exist is created.
            Anything that exists should be controlled so that
            it reflects the model.

            Memory Management: A release pool has been setup in the
            update method that is the one called from outside and from
            a non main thread. Make sure to retain all new items that
            we want to keep.
        """
        self._cancel_tracking()
        folderviewlocation.update(foldermodellocation)

        # Find elements for removal
        self.removeelements = []
        for id in folderviewlocation.contentids:
            foundelement = False
            for folder in foldermodellocation.folders:
                if folder.id == folderviewlocation.contentids[id].id:
                    if folderviewlocation.contentids[id].type == 'folder':
                        foundelement = True
            for item in foldermodellocation.items:
                if item.id == folderviewlocation.contentids[id].id:
                    if folderviewlocation.contentids[id].type == 'item':
                        foundelement = True
            if not foundelement:
                self.removeelements.append(id)
        # ... and remove them.
        if self.removeelements != []:
            for id in self.removeelements:
                # TODO: I think this should be released here
                # folderviewlocation.contentids[id].release()
                folderviewlocation.contentids[id].remove_view()
                del folderviewlocation.contentids[id]
        # Add or update folders in the current folder
        for foldermodel in foldermodellocation.folders:
            if foldermodel.id not in folderviewlocation.contentids:
                tmp = CocoaMenuFolderView.alloc().initWithParameters(folderviewlocation, foldermodel, self.configuration.gui_image_path, "")
                tmp.retain()
                self._recursive_update(foldermodel, tmp)
            else:
                self._recursive_update(foldermodel, folderviewlocation.contentids[foldermodel.id])
        # Add or update items in the current folder
        for item in foldermodellocation.items:
            if item.id not in folderviewlocation.contentids:
                tmp = CocoaMenuItemView.alloc().initWithParameters(folderviewlocation, item, self.configuration.gui_image_path, self.launchidmap)
                tmp.retain()
            else:
                folderviewlocation.contentids[item.id].update(item)
                self.topfolder.view.performSelectorOnMainThread_withObject_waitUntilDone_('itemChanged:', folderviewlocation.contentids[item.id], True)

    def update(self):
        """ Update the cocoa menu view.

            Makes sure that the view is consistent with the model.

            CancelTracking is available for OS X 10.5 and is needed for
            crash prevention. Only works for 10.5 though.

            We need to have a global pool in place for the updates to
            avoid memory leaks. We may release the pool if
            we make sure to retain all the folders and items we need.
            See: http://developer.apple.com/documentation/Cocoa/Conceptual/
                        MemoryMgmt/Concepts/AutoreleasePools.html#//apple_ref/
                        doc/uid/20000047-CJBFBEDI
            for more information on autorelease pools.
        """
        _pool = NSAutoreleasePool.alloc().init()
        # Only update menu if it is visible (i.e. it is the app menu)
        if self.name == 'appmenu' and self.hasdonesetup:
            self._recursive_update(self.model.topfolder, self.topfolder)

        #menu_update_pool.drain()

    def _cancel_tracking(self):
        """ Close the menu if it is open. """
        # Prevent crash on OS X 10.5 systems.
        try:
            NSAppKitVersionNumber10_4 # This exists only from 10.5 and will break the try/except on 10.4 and lower.
            self.topfolder.view.performSelectorOnMainThread_withObject_waitUntilDone_('cancelTracking', None, False)
        except:
            pass

    def display(self, views=None):
        """ Display the Cocoa based menu. """
        pass

    def destroy(self):
        """ Destroy the menu so that it can be called again. """
        if self.name == 'appmenu':
            self._cancel_tracking()

# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from components.presentation.common import CommonModel
    from components.presentation.menu import MenuModel

    # Create models
    commonmodel = CommonModel()
    menumodel = MenuModel(lib.dictionary.Dictionary())
    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()

    commonview = CocoaCommonView(commonmodel, handles, 'commonview', configuration, True)
    commonview.mainmenu.itemWithTitle_('G/On').submenu().itemWithTitle_('Quit G/On').setTarget_(commonview)
    menuview = CocoaMenuView(menumodel, commonview.handles, 'appmenu', configuration)
    # Add content to the menu
    menuview.controller.add_folder(-1, 1, "Folder", None)
    menuview.controller.add_item(-1, 2, "First Item", None, True, "1000", True)
    menuview.controller.add_item(-1, 3, "Second item", None, True, "1001")

    menuview.controller.add_folder(1, 4, "Folder in folder", None)
    menuview.controller.add_item(1, 5, "Item in folder", None, True, "1002")
    menuview.controller.add_item(1, 6, "Second item in folder", None, True, 1003, True)

    menuview.controller.add_folder(4, 7, "Folder in folder in ...", None)
    menuview.controller.add_item(4, 8, "Item in folder in ...", None, True, 1004)

    menuview.controller.add_folder(-1, 9, "Folder with icon", "outlook.bmp")
    menuview.controller.add_item(-1, 10, "Item with icon", "outlook.bmp", True, 1005)
    menuview.controller.add_item(-1, 11, "Disabled item", None, False, 1006)
    menuview.controller.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, 1007)

    # These should be handled nicely.
    #menuview.controller.add_folder(100, 13, "Folder with nonexisting parent", None)
    #menuview.controller.add_item(100, 14, "Item with nonexisting parent", None, True)
    menuview.controller.add_item(-1, 15, "Item with nonexisting bmp icon", "nonexisting.bmp", True, 1012)
    #menuview.controller.add_folder(-1, 18, "Folder with nonexisting bmp icon", "nonexisting.bmp")

    # What to do with these?? Remove/replace/ignore
    # (now they are just not added, because the id allready exists)
    #menuview.controller.add_item(-1, 3, "Item with existing id", None, True)
    #menuview.controller.add_folder(-1, 3, "Folder with existing id", None)

    # Update an item (text / icon / enabled state)
    menuview.controller.update_item(-1, 2, "First item (update)", None, True, 1008, True)
    menuview.controller.update_item(-1, 3, "Second item (update)", None, True, 1009)
    menuview.controller.update_item(-1, 10, "Item with icon (update)", None, False, 1010)
    menuview.controller.update_item(-1, 12, "Disabled item with icon (update)", "outlook.bmp", True, 1011)

    # remove items (add is done already).
    menuview.controller.remove_item(-1, 11)

    # Update a folder (text / icon)
    menuview.controller.update_folder(-1, 9, "Folder with icon (update)", None)
    menuview.controller.update_folder(-1, 1, "Folder (update)", "giritech.bmp")

    # Remove a folder (add is done already).
    menuview.controller.remove_folder(1, 4)

    # Start Gui
    commonview.display()
