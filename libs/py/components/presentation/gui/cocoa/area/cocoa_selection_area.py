"""

Generic Cocoa based selection area.

"""

from AppKit import NSTableView,\
                    NSTableColumn,\
                    NSButtonCell,\
                    NSSwitchButton,\
                    NSIndexSet,\
                    NSObject,\
                    NSCell,\
                    NSAttributedString,\
                    NSMutableParagraphStyle,\
                    NSLineBreakByTruncatingTail,\
                    NSAppKitVersionNumber, \
                    NSFont,\
                    NSZeroRect,\
                    NSCompositeSourceOver,\
                    NSFontAttributeName,\
                    NSParagraphStyleAttributeName,\
                    NSForegroundColorAttributeName,\
                    NSColor
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea
from components.presentation.gui.cocoa.cocoa_tools import locate_image
import objc

AppKitVersionNumber_10_6 = 1038


class CocoaSelectionArea(CocoaBaseArea):
    """ An Area for letting the user locate a folder. """

    def __init__(self, parent, container, controller, configuration):
        """ Create elements for a text area. """
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)

        # Currently visible model
        self.currentselectionlist = []
        self.current_selection_method = None
        self.current_selection_highlighted_id = -1
        self.image_width = 32

        # Create widgets.
        self.packagecontainer = self.create_scroll_widget()
        self.container.contentView().addSubview_(self.packagecontainer)
        self.widgets.append(self.packagecontainer)

        # Add a table to the scroll view.
        self.packageview = NSTableView.alloc().init()
        self.packageview.setRowHeight_(self.image_width + self.margin/2.0)

        self.datasource = DataSource.alloc().init()
        self.datasource.setData(self.currentselectionlist)
        self.datasource.setController(self.controller)
        self.datasource.setConfiguration(configuration)
        self.packageview.setDataSource_(self.datasource)
        #self.packageview.setDelegate_(self)
        self.packageview.setDelegate_(self.datasource)
        self.packageview.setHeaderView_(None)
        self.packageview.setUsesAlternatingRowBackgroundColors_(True)
        self.packagecontainer.setDocumentView_(self.packageview)
        # Create a check box column for package selection.
        self.selectioncolumn = NSTableColumn.alloc().initWithIdentifier_('selected')
        self.selectioncolumn.setWidth_(20)
        self.selectioncolumn.headerCell().setStringValue_('')
        self.checkbox = NSButtonCell.alloc().init()
        self.checkbox.setButtonType_(NSSwitchButton)
        self.selectioncolumn.setDataCell_(self.checkbox)
        self.packageview.addTableColumn_(self.selectioncolumn)
        # Create a title and details column and set the custom cell to display as table elements.
        self.namecolumn = NSTableColumn.alloc().initWithIdentifier_('name')
        self.namecolumn.setEditable_(False)
        self.cell = TableCell.alloc().__init__(self.image_width, self.margin)
        self.namecolumn.setDataCell_(self.cell)
        self.packageview.addTableColumn_(self.namecolumn)
        self.packageview.setAllowsEmptySelection_(False)

    def update(self, model):
        """ Update relevant parts of the area. """

        # Update the content of the selection list.
        if self.currentselectionlist != model.selection_list:
            self.datasource.setData(model.selection_list)
            self.currentselectionlist = model.selection_list
            self.packageview.reloadData()
            self.currentselectionlist = model.selection_list
        # Select between multiple or single selection.
        if self.current_selection_method != model.selection_method:
            if model.selection_method == self.controller.ID_SELECTION_SINGLE:
                self.packageview.removeTableColumn_(self.selectioncolumn)
            elif model.selection_method == self.controller.ID_SELECTION_MULTIPLE:
                self.packageview.removeTableColumn_(self.selectioncolumn)
                self.packageview.removeTableColumn_(self.namecolumn)
                self.packageview.addTableColumn_(self.selectioncolumn)
                self.packageview.addTableColumn_(self.namecolumn)
            self.current_selection_method = model.selection_method
        # Highlight a given row.
        if self.current_selection_highlighted_id != model.selection_highlighted_id:
            #print "update selection"
            for index, package in enumerate(model.selection_list):
                location = 0
                if package['id'] == model.selection_highlighted_id:
                    location = index
                    break
            self.packageview.selectRowIndexes_byExtendingSelection_(NSIndexSet.indexSetWithIndex_(location), False)
            self.packageview.scrollRowToVisible_(location)
#            self.packageview.setNeedsDisplay_(True)
            self.packageview.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
#            self.container.displayIfNeeded()
            self.current_selection_highlighted_id = model.selection_highlighted_id

    def resize(self, left=0, top=0, width=450, height=150):
        """ Resize and relocate parts of the area. """
        self.set_sizes(left, top, width, height)
        self.packagecontainer.setFrame_(((left,top-height+1),(width,height-1)))
        self.packageview.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
#        self.packagecontainer.setNeedsDisplay_(True)


class TableCell(NSCell):

    def __init__(self, image_width, margin):
        self = NSCell.initTextCell_(self, "")
        self.name = None
        self.details = None
        self.icon = None
        self.paragraphstyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy()
        self.paragraphstyle.setLineBreakMode_(NSLineBreakByTruncatingTail)
        self.image_width = image_width
        self.margin = margin
        return self

    def drawWithFrame_inView_(self, frame, view):
        # Draw name
        nameRect = self.name.boundingRectWithSize_options_((frame.size.width - self.image_width - self.margin*2.0,
                                                            frame.size.height),
                                                            0)
        self.name.drawInRect_(((frame.origin.x + self.image_width + self.margin, frame.origin.y + self.margin/4.0),
                               (nameRect.size.width, nameRect.size.height)))
        # Draw detail
        detailRect = self.details.boundingRectWithSize_options_((frame.size.width - self.image_width - self.margin*2.0,
                                                                 frame.size.height),
                                                                 0)
        self.details.drawInRect_(((frame.origin.x + self.image_width + self.margin, frame.origin.y + NSFont.systemFontSize() + self.margin/2.0),
                                  (detailRect.size.width, detailRect.size.height)))
        # Draw image
        if self.icon is not None:
            if NSAppKitVersionNumber < AppKitVersionNumber_10_6:
                self.icon.setFlipped_(True)
                self.icon.drawInRect_fromRect_operation_fraction_(((frame.origin.x, frame.origin.y + self.margin/4.0),
                                                                  (self.image_width, self.image_width)),
                                                                  NSZeroRect,
                                                                  NSCompositeSourceOver,
                                                                1.0)
            else:
                self.icon.drawInRect_fromRect_operation_fraction_respectFlipped_hints_(((frame.origin.x, frame.origin.y + self.margin/4.0),
                                                                                    (self.image_width, self.image_width)),
                                                                                    NSZeroRect,
                                                                                    NSCompositeSourceOver,
                                                                                    1.0,
                                                                                    True,
                                                                                    None)

class DataSource(NSObject):
    """ Data provider for the package table.  """

    @objc.python_method
    def init(self):
        self = super(DataSource, self).init()
        self.paragraphStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy()
        self.paragraphStyle.setLineBreakMode_(NSLineBreakByTruncatingTail)
        self.stringAttributes = {
                                 NSFontAttributeName : NSFont.systemFontOfSize_(NSFont.systemFontSize()),
                                 NSParagraphStyleAttributeName : self.paragraphStyle
                                 }
        return self

    @objc.python_method
    def setData(self, indata):
        """ Set the table content. """
        self.data = indata

    @objc.python_method
    def setController(self, controller):
        """ Set the model controller so we can change selections. """
        self.controller = controller

    @objc.python_method
    def setConfiguration(self, configuration):
        """ Set the configuration so we can locate image path, etc."""
        self.configuration = configuration

    def numberOfRowsInTableView_(self, tableView):
        """ Set the length of the table. """
        return len(self.data)

    def tableView_objectValueForTableColumn_row_(self, tableView, tableColumn, row):
        """ Set table content per row and cell. """
        if row < len(self.data):
            return self.data[row][tableColumn.identifier()]
        else:
            return ""

    def tableView_setObjectValue_forTableColumn_row_(self, tableView, object, tableColumn, row):
        """ Set the selection state for a row in the selection list. """
        self.data[row]['selected'] = object
        self.controller.set_selection_via_index(row, object)

    def tableView_willDisplayCell_forTableColumn_row_(self, tableView, cell, tableColumn, row):
        """ Set the strings and image for the cell that is about to be displayed. """
        if tableColumn.identifier() == 'name':
            # Set the name label
            self.stringAttributes[NSForegroundColorAttributeName] = NSColor.blackColor()
            cell.name = NSAttributedString.alloc().initWithString_attributes_(self.data[row]['name'], self.stringAttributes)
            # Set the details label
            self.stringAttributes[NSForegroundColorAttributeName] = NSColor.grayColor()
            cell.details = NSAttributedString.alloc().initWithString_attributes_(self.data[row]['details'], self.stringAttributes)
            # Set the icon image
            if self.data[row]['icon'] != None:
                cell.icon = locate_image(self.configuration.gui_image_path, self.data[row]['icon'])
            else:
                cell.icon = None

    def tableViewSelectionDidChange_(self, sender):
        """ Called when a row is selected in a table. """
        self.controller.set_selection_user_highlighted(sender.object().selectedRowIndexes().firstIndex())
