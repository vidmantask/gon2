"""

Generic Cocoa based HTML text viewing area.

"""

from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea

class CocoaHtmlArea(CocoaBaseArea): 
    """ An Area for letting the user locate a folder. """
    
    def __init__(self, parent, container, controller, configuration):
        """ Create elements for a text area. """
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)
        
        # Currently visible model
        self.current_text = ""
        
        # Create an area for scrolling.
        self.scrollview = self.create_scroll_widget()
        # ... then a text view to put inside it.
        self.htmlview = self.create_html_widget()
        self.scrollview.setDocumentView_(self.htmlview)
        self.container.contentView().addSubview_(self.scrollview)
        
    def update(self, model):
        """ Update relevant parts of the area. """
        if self.current_text != model.text:
            self.container.delegate().performSelectorOnMainThread_withObject_waitUntilDone_('setHTMLforWebframe:', (model.html_base.substitute(g_html_text=model.text.encode('utf-8', 'ignore')), self.htmlview.mainFrame()), True)
            self.htmlview.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
            self.current_text = model.text    

    def resize(self, left=0, top=0, width=300, height=100):
        """ Resize and relocate parts of the area. """
        self.set_sizes(left, top, width, height)
        yvalue = top - height
        self.scrollview.setFrame_(((left, yvalue), (width, height)))
        self.htmlview.setFrame_(((left, yvalue), (width, height)))
        
        