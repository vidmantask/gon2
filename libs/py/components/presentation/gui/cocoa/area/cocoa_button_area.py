"""
Generic Cocoa based user-name/password login area.

"""

from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea

class CocoaButtonArea(CocoaBaseArea): 
    """ An Area for login and changing passwords. 
    
        By default buttons are not included in the key view loop order.
        Press CTRL + F7 to switch this.
    """
    
    def __init__(self, parent, container, controller, configuration):
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)

        # Currently visible model
        self.currentcancelallowed = True
        self.currentcancellabel = self.dictionary._("Cancel")
        self.currentcancelvisible = True
        self.currentnextallowed = True
        self.currentnextlabel = self.dictionary._("Next")
        self.currentnextvisible = True
        self.current_default_button = None

        # A Cancel button
        self.cancelbutton = self.create_button_widget()
        self.cancelbutton.setTitle_(self.currentcancellabel)
        self.cancelbutton.setAction_( "_on_cancel_clicked:" )
        self.cancelbutton.setKeyEquivalent_('\033')

        # Create an OK button and set it to default.
        self.nextbutton = self.create_button_widget()
        self.nextbutton.setTitle_(self.currentnextlabel)
        self.nextbutton.setAction_( "_on_ok_clicked:" )

        # Create a hidden default button
        self.defaultbutton = self.create_button_widget()
        self.defaultbutton.setFrame_(((0, 0), (0, 0)))
        self.defaultbutton.setAction_("_on_default_clicked:")
        self.defaultbutton.setRefusesFirstResponder_(True)
        self.defaultbutton.setKeyEquivalent_('\r')

    def update(self, model):
        """ Update relevant parts of the area. """
        if self.currentcancelallowed != model.cancel_allowed:
            self.cancelbutton.setEnabled_(model.cancel_allowed)
            self.currentcancelallowed = model.cancel_allowed
        if self.currentcancellabel != model.cancel_label:
            self.cancelbutton.performSelectorOnMainThread_withObject_waitUntilDone_('setTitle:', model.cancel_label, True)
            self.currentcancellabel = model.cancel_label
        if self.currentcancelvisible != model.cancel_visible:
            self.cancelbutton.setHidden_(not model.cancel_visible)
            self.currentcancelvisible = model.cancel_visible

        if self.currentnextallowed != model.next_allowed:
            self.nextbutton.setEnabled_(model.next_allowed)
            self.currentnextallowed = model.next_allowed
        if self.currentnextlabel != model.next_label:
            self.nextbutton.performSelectorOnMainThread_withObject_waitUntilDone_('setTitle:', model.next_label, True)
            self.currentnextlabel = model.next_label
        if self.currentnextvisible != model.next_visible:
            self.nextbutton.setHidden_(not model.next_visible)
            self.currentnextvisible = model.next_visible

        # Set the default push button
        if self.current_default_button != model.default_button:
            self.current_default_button = model.default_button

    def resize(self, left=0, top=0, width=375, height=40):
        """ Resize anything relevant in this area. """
        self.set_sizes(left, top, width, height)         
        
        self.cancelbutton.setFrameOrigin_((self.width-2*self.buttonwidth-3.5*self.margin, self.margin))
        self.cancelbutton.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
       
        self.nextbutton.setFrameOrigin_((self.width-self.buttonwidth-2.5*self.margin, self.margin))
        self.nextbutton.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
