"""

Generic Cocoa based progress display area.

"""

from AppKit import NSMutableAttributedString, NSFontAttributeName
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea

class CocoaProgressArea(CocoaBaseArea):
    """ An Area for letting the user locate a folder. """

    def __init__(self, parent, container, controller, configuration):
        """ Create elements for a progress area. """
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)

        # Currently visible model
        self.current_headline = ""
        self.current_progress_mode = None
        self.current_progress_complete = 0
        self.current_progress_subtext = ""

        # create widgets.
        self.background = self.create_background_widget()
        # Create an area for a headline
        self.headline = self.create_large_text_widget()
        self.container.contentView().addSubview_(self.headline)
        self.widgets.append(self.headline)

        self.progressbar = self.create_progress_widget()
        self.container.contentView().addSubview_(self.progressbar)
        self.widgets.append(self.progressbar)

        self.progresssubtext = self.create_small_text_widget()
        self.container.contentView().addSubview_(self.progresssubtext)
        self.widgets.append(self.progresssubtext)

        self.separator = self.create_separator_widget()
        self.container.contentView().addSubview_(self.separator)
        self.widgets.append(self.separator)


    def update(self, model):
        """ Update relevant parts of the area. """
        if self.current_progress_mode != model.info_progress_mode:
            if model.info_progress_mode == self.controller.ID_MODE_PROGRESS_KNOWN:
                self.progressbar.setIndeterminate_(False)
                self.progressbar.performSelectorOnMainThread_withObject_waitUntilDone_('stopAnimation:', None, True)
            elif model.info_progress_mode == self.controller.ID_MODE_PROGRESS_UNKNOWN:
                self.progressbar.setIndeterminate_(True)
                self.progressbar.performSelectorOnMainThread_withObject_waitUntilDone_('startAnimation:', None, True)
            self.current_progress_mode = model.info_progress_mode

        if self.current_progress_complete != model.info_progress_complete:
            self.progressbar.setDoubleValue_(model.info_progress_complete)
            self.progressbar.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
            self.current_progress_complete = model.info_progress_complete

        if self.current_headline != model.info_headline:
            self.headlinetext = NSMutableAttributedString.alloc().initWithString_(model.info_headline)
            self.headlinetext.setAttributes_range_({NSFontAttributeName: self.boldfont, }, (0, len(model.info_headline)))
            self.headline.performSelectorOnMainThread_withObject_waitUntilDone_('setAttributedStringValue:', self.headlinetext, True)
            self.headline.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
            self.current_headline = model.info_headline

        if self.current_progress_subtext != model.info_progress_subtext:
            self.subtext = NSMutableAttributedString.alloc().initWithString_(model.info_progress_subtext)
            self.subtext.setAttributes_range_({NSFontAttributeName: self.smallfont, }, (0, len(model.info_progress_subtext)))
            self.progresssubtext.performSelectorOnMainThread_withObject_waitUntilDone_('setAttributedStringValue:', self.subtext, True)
            self.progresssubtext.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
            self.current_progress_subtext = model.info_progress_subtext

    def resize(self, left=0, top=0, width=450, height=150):
        """ Resize and relocate parts of the area. """
        self.set_sizes(left, top, width, height)

        yvalue = top - height
        self.background.setFrame_(((left, yvalue),(width, height)))
        self.headline.setFrame_(((left + self.large_margin, top - self.large_margin - 25),
                                     (width - 2*self.large_margin, 25)))
        self.progressbar.setFrame_(((left + self.large_margin, top - 2*self.large_margin - 25),
                                     (width - 2*self.large_margin, 20)))
        self.progresssubtext.setFrame_(((left + self.large_margin, top - 3*self.large_margin - 25),
                                     (width - 2*self.large_margin, 20)))
        self.separator.setFrame_(((0.0, yvalue), (self.width, 1.0)))
