"""

Generic Cocoa based clear text display area.

"""

from AppKit import NSMutableAttributedString, NSFontAttributeName
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea

class CocoaTextArea(CocoaBaseArea): 
    """ An Area for letting the user locate a folder. """
    
    def __init__(self, parent, container, controller, configuration):
        """ Create elements for a text area. """
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)
        # Currently visible model
        self.current_headline = ""
        self.current_text = ""
        # create widgets.
        self.scrollview = self.create_scroll_widget()
        self.container.contentView().addSubview_(self.scrollview)
        self.textview = self.create_text_widget()
        self.scrollview.setDocumentView_(self.textview)
        self.separator = self.create_separator_widget()
        
    def update(self, model):
        """ Update relevant parts of the area. """
        if self.current_headline != model.info_headline or self.current_text != model.info_text:
            _text = NSMutableAttributedString.alloc().initWithString_(model.info_headline + '\n\n' + model.info_text)
            _text.setAttributes_range_({NSFontAttributeName: self.boldfont, }, (0, len(model.info_headline)))
            self.container.delegate().performSelectorOnMainThread_withObject_waitUntilDone_('setTextForTextView:', 
                                                                                            (model.info_headline, model.info_text, self.textview), 
                                                                                            True)
            # Needed for setting the scroll-bar in the correct position after inserting text.
            self.scrollview.performSelectorOnMainThread_withObject_waitUntilDone_('reflectScrolledClipView:', 
                                                                                  self.scrollview.contentView(), 
                                                                                  True)
            self.current_headline = model.info_headline
            self.current_text = model.info_text

    def resize(self, left=0, top=0, width=450, height=150):
        """ Resize and relocate parts of the area. """
        self.set_sizes(left, top, width, height)
        self.scrollview.setFrame_(((left, top-height+1),(width, height - 1.0)))
        self.textview.setFrame_(((left, top - height + 1), (width, height -1)))
        self.separator.setFrame_(((left, top - height), (self.width, 1.0)))
        