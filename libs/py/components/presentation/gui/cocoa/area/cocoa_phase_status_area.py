"""
Generic Cocoa based phase status area.

    Old notes:
            Texts and images position are set relative to
            the phase box location. So to move the phase box
            first move the outer frame and then call this.
            
            On 10.4 it is important to have the phase images 
            in place before the resize done by 'showdetails'.
            If not the images are not displayed. Hence the 
            slightly hard-coded origin settings.

"""
from AppKit import NSTextField, NSColor, NSCenterTextAlignment, NSImageView
from components.presentation.gui.cocoa.cocoa_tools import locate_image
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea

class CocoaPhaseStatusArea(CocoaBaseArea):
    """ Cocoa Area for showing a number of phaseboxtexts."""
    
    def __init__(self, parent, container, controller, configuration):
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)

        # Currently visible model
        self.currentphasecount = 0
        
        # Static settings
        self.phasesmaxcount = 5
        self.imagewidth = 20
        self.imageheight = 20
        self.phaseboxtexts = []
        self.phaseboxarrows = []
        self.arrowbitmap = locate_image(path=self.image_path, filename="g_arrow_red.bmp")
        self.grayarrowbitmap = locate_image(path=self.image_path, filename="g_arrow_grey.bmp")
        
        # Create a progression area for displaying phaseboxtexts.
        self.phasesbox = NSTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        self.phasesbox.setBackgroundColor_(NSColor.whiteColor())
        self.phasesbox.setEditable_(False)
        self.phasesbox.setBordered_(False)
        self.container.contentView().addSubview_(self.phasesbox)

        # Create the elements for displaying phaseboxtexts
        for _index in range(0, self.phasesmaxcount):
            # Create new text area
            self.phaseboxtexts.append(NSTextField.alloc().initWithFrame_(((0, 0), (0, 0))))
            self.phaseboxtexts[-1].setEditable_(False)
            self.phaseboxtexts[-1].setAlignment_(NSCenterTextAlignment)
            self.phaseboxtexts[-1].setBordered_(False)
            self.phasesbox.addSubview_(self.phaseboxtexts[-1])
            # Create new image area.
            self.phaseboxarrows.append(NSImageView.alloc().initWithFrame_(((0,0), (0,0))))
            self.phaseboxarrows[-1].setImage_(self.arrowbitmap)
            self.phasesbox.addSubview_(self.phaseboxarrows[-1])
    
        self.separator = self.create_separator_widget()
    
    def update(self, model):
        """ Update relevant parts of the area. """
        self.currentphasecount = len(model.phase_list)

        # If there is something to show 
        if (self.currentphasecount > 0):
            # Hide texts that are not needed now.
            for index in range(self.currentphasecount, self.phasesmaxcount):
                #self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.phaseboxtexts[index].setHidden_(True)
                self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('retain', None, True)
                
            # Hide images that are not needed now.
            for index in range(self.currentphasecount-1, self.phasesmaxcount):
                self.phaseboxarrows[index].setHidden_(True)
                #self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('retain', None, True)
    
            # Manipulate texts...
            for index in range(0, self.currentphasecount):
                # Set the text appearance.
                #self.phaseboxtexts[index].setStringValue_(model.phase_list[index]['name'])
                self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', model.phase_list[index]['name'], True)
                #self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', False, True)
                self.phaseboxtexts[index].setHidden_(False)
                if model.phase_list[index]['state'] == self.controller.ID_PHASE_PENDING:
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setFont:', self.standardfont, True)
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setTextColor:', NSColor.lightGrayColor(), True)
                    #self.phaseboxtexts[index].setNeedsDisplay_(True)
                elif model.phase_list[index]['state'] == self.controller.ID_PHASE_ACTIVE:
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setFont:', self.boldfont, True)
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setTextColor:', NSColor.blackColor(), True)
                else:
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setFont:', self.standardfont, True)
                    self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setTextColor:', NSColor.blackColor(), True)
                self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('retain', None, True)
                
            # Manipulate images...
            for index in range(0, self.currentphasecount-1):
                if model.phase_list[index]['state'] == self.controller.ID_PHASE_PENDING:
                    self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.grayarrowbitmap, True)
                elif model.phase_list[index]['state'] == self.controller.ID_PHASE_ACTIVE:
                    self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.grayarrowbitmap, True)
                else:
                    self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.arrowbitmap, True)
                #self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', False, True)
                self.phaseboxarrows[index].setHidden_(False)
                self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('retain', None, True)

        # Else - hide them all...
        else:
            for index in range(0, self.phasesmaxcount):
                #self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.phaseboxtexts[index].setHidden_(True)
                #self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.phaseboxarrows[index].setHidden_(True)
                self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('retain', None, True)

        # Refresh the sizes and locations.
        self.resize(self.left, self.top, self.width, self.height)
    
    def resize(self, left=0, top=0, width=450, height=30):
        """ Resize anything relevant in this area. """
        self.set_sizes(left, top, width, height)
        
        self.phasesbox.setFrame_(((left, top-height), (width, height)))
        if self.currentphasecount > 0:
            self.phase_width = (self.width + self.imagewidth) // self.currentphasecount
        else:
            self.phase_width = 0
        
        # Resize text boxes 
        for index in range(0, self.currentphasecount):
            self.phaseboxtexts[index].setFrameOrigin_(((index * self.phase_width) + 1, 4))
            self.phaseboxtexts[index].setFrameSize_((self.phase_width-self.imagewidth-2, height-8))
            #self.phaseboxtexts[index].setNeedsDisplay_(True)
            self.phaseboxtexts[index].performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        # Resize image boxes
        for index in range(0, self.currentphasecount-1):
            self.phaseboxarrows[index].setFrameOrigin_((self.phase_width - self.imagewidth + (index * self.phase_width), 4))
            self.phaseboxarrows[index].setFrameSize_((self.imagewidth, self.imageheight))
            #self.phaseboxarrows[index].setNeedsDisplay_(True)
            self.phaseboxarrows[index].performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        
        #self.phasesbox.setNeedsDisplay_(True)        
        self.phasesbox.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        
        self.separator.setFrame_(((0.0, top-height), (self.width, 1.0)))
        self.separator.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        