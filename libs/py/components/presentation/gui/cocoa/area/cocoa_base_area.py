"""

Base class for Cocoa based areas.

"""

from AppKit import NSFont, \
                    NSTextField, \
                    NSColor, \
                    NSTextView, \
                    NSProgressIndicator, \
                    NSBox, \
                    NSBoxSeparator, \
                    NSScrollView, \
                    NSSecureTextField, \
                    NSButton, \
                    NSTexturedSquareBezelStyle
from WebKit import WebView

class CocoaBaseArea:
    def __init__(self, parent, container, controller, configuration):
        
        self.parent = parent
        self.container = container
        self.controller = controller
        self.image_path = configuration.gui_image_path
        self.dictionary = configuration.dictionary
        
        self.widgets = []
        self.visible = True
        
        self.margin = 8
        self.large_margin = 4 * self.margin
        
        self.left = 0
        self.top = 0
        self.width = 0
        self.height = 0        
        
        # Button sizes
        self.buttonwidth = 65  # Retrieved from 'Interface Builder' standard sizes
        self.buttonheight = 25 # Retrieved from 'Interface Builder' standard sizes

        self.frameheight = 22 # Top frame (?)
        
        # Input field size
        self.fieldheight = 22
        
        # Create a number of fonts 
        self.smallfont = NSFont.systemFontOfSize_(10)
        self.standardfont = NSFont.systemFontOfSize_(NSFont.systemFontSize())
        self.boldfont = NSFont.boldSystemFontOfSize_(NSFont.systemFontSize())
        self.largeboldfont = NSFont.boldSystemFontOfSize_(16)


    def create_background_widget(self):
        """ Create a widget for displaying a background. """
        widget = NSTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setBackgroundColor_(NSColor.whiteColor())
        widget.setEditable_(False)
        widget.setBordered_(False)
        self.container.contentView().addSubview_(widget)
        self.widgets.append(widget)
        return widget

    def create_text_widget(self):
        """ Create a widget for displaying text. """
        widget = NSTextView.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setTextContainerInset_((self.large_margin, self.large_margin))
        widget.setEditable_(False)
        widget.setSelectable_(False)
        return widget

    def create_large_text_widget(self):
        """ Create a widget for displaying large text. """
        widget = NSTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setEditable_(False)
        widget.setBordered_(False)
        self.widgets.append(widget)
        return widget

    def create_small_text_widget(self):
        """ Create a widget for displaying small text. """
        widget = NSTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setEditable_(False)
        widget.setBordered_(False)
        self.widgets.append(widget)
        return widget

    def create_progress_widget(self):
        """ Create a widget for displaying progress. """
        widget = NSProgressIndicator.alloc().initWithFrame_(((0, 0), (0, 20.0)))
        widget.setIndeterminate_(False)
        self.widgets.append(widget)
        #self.container.contentView().addSubview_(widget)
        return widget

    def create_separator_widget(self):
        """ Create a widget for a thin line separator. """
        widget = NSBox.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setBoxType_(NSBoxSeparator)
        self.container.contentView().addSubview_(widget)
        return widget

    def create_html_widget(self):
        """ Create a widget for displaying a web page. """
        widget = WebView.alloc().init()
        widget.mainFrame().loadHTMLString_baseURL_("<HTML><HEAD></HEAD><BODY style='overflow:auto; background:#740000;'></BODY></HTML>", None)
        return widget

    def create_scroll_widget(self):
        """ Create a widget the has scroll-bars and can contain other widgets. """
        widget = NSScrollView.alloc().initWithFrame_(((0, 0), (0, 0)))
        widget.setHasVerticalScroller_(True)
        widget.setHasHorizontalScroller_(False)
        widget.setAutohidesScrollers_(True)
        return widget
        
    def create_text_input_widget(self, defaulttext=""):
        """ Create a widget for text input. """
        widget = NSTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        self.container.contentView().addSubview_(widget)
        widget.cell().setPlaceholderString_(defaulttext)
        widget.setBezelStyle_(4)
        return widget

    def create_secret_text_input_widget(self, defaulttext=""):
        """ Create a widget for secret text input. """
        widget = NSSecureTextField.alloc().initWithFrame_(((0, 0), (0, 0)))
        self.container.contentView().addSubview_(widget)
        widget.cell().setPlaceholderString_(defaulttext)
        widget.setBezelStyle_(4)
        return widget
        
    def create_button_widget(self):
        widget = NSButton.alloc().initWithFrame_ (((0.0, 0.0), (self.buttonwidth, self.buttonheight)))
        self.container.contentView().addSubview_ (widget)
        widget.setBezelStyle_( NSTexturedSquareBezelStyle )
        widget.setTarget_( self.parent )
        return widget
        
    def set_sizes(self, left=0, top=0, width=0, height=0):
        """ Set the size of this area. """
        (self.left, self.top, self.width, self.height) = (left, top, width, height) 
        
    def hide(self):
        """ Hide all widgets in this area. """
        for widget in self.widgets:
            widget.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
        self.visible = False

    def show(self):
        """  Show all widgets in this area. """
        for widget in self.widgets:
            widget.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', False, True)
        self.visible = True
