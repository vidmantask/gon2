"""

Generic Cocoa based header area.

"""

from AppKit import NSImageView, NSColor
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea
from components.presentation.gui.cocoa.cocoa_tools import locate_image

GIRI_GUI_INSTALL_BANNER = "g_install_banner.png"
GIRI_GUI_UPDATE_BANNER = "g_update_banner.png"
GIRI_GUI_REMOVE_BANNER = "g_remove_banner.png"
GIRI_GUI_ENROLL_BANNER = "g_enroll_banner.png"
GIRI_GUI_UNINSTALL_BANNER = "g_uninstall_banner.png"
GIRI_GUI_LOGIN_BANNER = "g_login_banner.png"

class CocoaHeaderArea(CocoaBaseArea):
    def __init__(self, parent, container, controller, configuration):
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)

        self.current_banner_id = None
        self.current_banner_image = None

        self.image_background_view = self.create_background_widget()

        # An image that looks pretty :)
        self.image_view = NSImageView.alloc().initWithFrame_(((0, 0), (0, 0)))

        self.container.contentView().performSelectorOnMainThread_withObject_waitUntilDone_('addSubview:', self.image_background_view, True)
        self.container.contentView().performSelectorOnMainThread_withObject_waitUntilDone_('addSubview:', self.image_view, True)

        # A line for dividing the image and the text area.
        self.horizontalline = self.create_separator_widget()

    def update(self, model):
        """ Update relevant parts of the area. """
        
        if self.current_banner_id != model.banner_id:
            # Locate banner depending on id and tools settings.
            if model.banner_id == 0:   # ID_BANNER_INSTALL:
                self.current_banner_image = GIRI_GUI_INSTALL_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.whiteColor())
            elif model.banner_id == 1: # ID_BANNER_UPDATE
                self.current_banner_image = GIRI_GUI_UPDATE_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.whiteColor())
            elif model.banner_id == 2: # ID_BANNER_REMOVE 
                self.current_banner_image = GIRI_GUI_REMOVE_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.colorWithRed_green_blue_alpha_(.2275, .2275, .2275, 1))
            elif model.banner_id == 3: # ID_BANNER_ENROLL
                self.current_banner_image = GIRI_GUI_ENROLL_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.whiteColor())
            elif model.banner_id == 4: # ID_BANNER_UNINSTALL
                self.current_banner_image = GIRI_GUI_UNINSTALL_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.colorWithRed_green_blue_alpha_(.2275, .2275, .2275, 1))
            elif model.banner_id == 5: # ID_BANNER_LOGIN
                self.current_banner_image = GIRI_GUI_LOGIN_BANNER
                self.image_background_view.setBackgroundColor_(NSColor.whiteColor())
                
            if self.current_banner_image is not None:

                self.banner = locate_image(path=self.image_path, filename=self.current_banner_image)
                self.image_view.performSelectorOnMainThread_withObject_waitUntilDone_('setImage:', self.banner, True)
                self.image_view.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)

            self.current_banner_id = model.banner_id

    def resize(self, left=0, top=0, width=375, height=60):
        """ Resize anything relevant in this area. """
        
        self.set_sizes(left, top, width, height)
        
        yvalue = top - height
        
        self.image_background_view.setFrame_(((left, yvalue+1), (self.width, self.height-1)))
        self.image_view.setFrame_(((left, yvalue+2), (1000, self.height-2)))

        self.image_background_view.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        self.image_view.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
        
        self.horizontalline.setFrame_(((left, yvalue), (self.width, 1.0)))
        self.horizontalline.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
