"""
Generic Cocoa based user-name/password login area.

"""

from AppKit import NSColor
from components.presentation.gui.cocoa.area.cocoa_base_area import CocoaBaseArea
from components.presentation.login import LoginController

class CocoaLoginArea(CocoaBaseArea): 
    """ An Area for login and changing passwords. """
    
    def __init__(self, parent, container, controller, configuration):
        CocoaBaseArea.__init__(self, parent, container, controller, configuration)
        
        # Currently visible model.
        self.current_state = self.controller.get_state()
        self.suggestedusername = ''
        self.lockusernamefield = False

        # Widgets
        self.usernamefield = self.create_text_input_widget(self.dictionary._("User name"))
        self.passwordfield = self.create_secret_text_input_widget(self.dictionary._("Password"))
        self.newpasswordfield = self.create_secret_text_input_widget(self.dictionary._("New password"))
        self.confirmnewpasswordfield = self.create_secret_text_input_widget(self.dictionary._("Confirm new password"))
        
    def update(self, model):
        """ Update relevant parts of the area. """
        if self.current_state != self.controller.get_state():
            
            if self.controller.get_state() == LoginController.ID_STATE_CHANGE_LOGIN:
                self.newpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', False, True)
                self.newpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
                self.newpasswordfield.setRefusesFirstResponder_(False)
                self.confirmnewpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', False, True)
                self.confirmnewpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
                self.confirmnewpasswordfield.setRefusesFirstResponder_(False)
            elif self.controller.get_state() == LoginController.ID_STATE_LOGIN:
                self.newpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.newpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
                self.newpasswordfield.setRefusesFirstResponder_(True)
                self.confirmnewpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setHidden:', True, True)
                self.confirmnewpasswordfield.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
                self.confirmnewpasswordfield.setRefusesFirstResponder_(True)
            self.current_state = self.controller.get_state()
            self.resize(self.left, self.top, self.width, self.height)
 
        if self.suggestedusername != model.suggestedusername:
            self.usernamefield.performSelectorOnMainThread_withObject_waitUntilDone_('setStringValue:', model.suggestedusername, True)
            self.usernamefield.performSelectorOnMainThread_withObject_waitUntilDone_('setNeedsDisplay:', True, True)
            self.suggestedusername = model.suggestedusername   
        
        if self.lockusernamefield != model.lockusernamefield:
            if model.lockusernamefield:
                self.usernamefield.setEnabled_(False)
                self.usernamefield.performSelectorOnMainThread_withObject_waitUntilDone_('setBackgroundColor:', NSColor.controlColor(), True)
                self.usernamefield.setRefusesFirstResponder_(True)
            else:
                self.usernamefield.setEnabled_(True)
                self.usernamefield.performSelectorOnMainThread_withObject_waitUntilDone_('setBackgroundColor:', NSColor.controlBackgroundColor(), True)
                self.usernamefield.setRefusesFirstResponder_(False)
            self.lockusernamefield = model.lockusernamefield
 
    def resize(self, left=0, top=0, width=375, height=80):
        """ Resize anything relevant in this area. """
        self.set_sizes(left, top, width, height)
        center = height/2

        if self.current_state == LoginController.ID_STATE_LOGIN:
            self.usernamefield.setFrame_(((self.large_margin, center + self.fieldheight + self.margin), 
                                          (self.width-2*self.large_margin, self.fieldheight)))
            self.passwordfield.setFrame_(((self.large_margin, center), 
                                          (self.width-2*self.large_margin, self.fieldheight)))
            self.newpasswordfield.setFrame_(((0, 0), (0, 0)))
            self.confirmnewpasswordfield.setFrame_(((0, 0), (0, 0)))
        
        elif self.current_state == LoginController.ID_STATE_CHANGE_LOGIN:
            self.usernamefield.setFrame_(((self.large_margin, center + 2*self.fieldheight + 2*self.margin), 
                                          (self.width-2*self.large_margin, self.fieldheight)))
            self.passwordfield.setFrame_(((self.large_margin, center + self.fieldheight + self.margin), 
                                          (self.width-2*self.large_margin, self.fieldheight)))
            self.newpasswordfield.setFrame_(((self.large_margin, center), 
                                             (self.width-2*self.large_margin, self.fieldheight)))
            self.confirmnewpasswordfield.setFrame_(((self.large_margin, center - self.fieldheight - self.margin), 
                                                    (self.width-2*self.large_margin, self.fieldheight)))
