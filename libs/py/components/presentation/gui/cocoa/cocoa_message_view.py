"""
Cocoa version of the GUI for messages.
"""

from os import path, popen
from time import sleep
import objc

from AppKit import NSObject,\
                    NSScreen,\
                    NSWindow,\
                    NSTextField,\
                    NSColor,\
                    NSTexturedBackgroundWindowMask,\
                    NSImageView,\
                    NSString,\
                    NSMutableAttributedString,\
                    NSRange,\
                    NSFont,\
                    NSFontAttributeName,\
                    NSStatusWindowLevel,\
                    NSTimer,\
                    NSRunLoop,\
                    NSDefaultRunLoopMode,\
                    NSDate
from components.presentation.message import MessageView
from components.presentation.gui.cocoa.cocoa_tools import locate_image_path, locate_icon

class CocoaMessageWindow(NSObject):
    """ Cocoa based view of messages in our own window.

        Memory Management: This is created in the main thread.
        So we do not need to create an auto-release pool. Anything
        that we want to keep between message displays should be
        created here to get it under cocoa's memory management.
    """
    @objc.python_method
    def initWithParameters(self, parent, width=350.0, height=95.0, margin=5.0):

        self.parent = parent
        self.timer = None

        self.WIDTH = width
        self.HEIGHT = height
        self.MARGIN = margin
        self.PICSIZE = 50.0
        self.TIME = 2.0

        self.ScreenWidth = NSScreen.mainScreen().visibleFrame().size.width
        self.ScreenHeight = NSScreen.mainScreen().visibleFrame().size.height

        # Location and size
        self.locationAndSize = ((self.ScreenWidth-(self.WIDTH+self.MARGIN), self.ScreenHeight-(self.HEIGHT+self.MARGIN)), (self.WIDTH, self.HEIGHT))
        self.messagewindow = NSWindow.alloc()
        self.messagewindow.initWithContentRect_styleMask_backing_defer_(self.locationAndSize, NSTexturedBackgroundWindowMask, 2, 0)
        self.messagewindow.setMovableByWindowBackground_(True)
        # Create a text area
        self.messagearea = NSTextField.alloc().initWithFrame_(((self.PICSIZE+self.MARGIN*2, self.MARGIN), (self.WIDTH-(self.PICSIZE+self.MARGIN*3), self.HEIGHT-(self.MARGIN*2))))
        self.messagewindow.contentView().addSubview_(self.messagearea)
        self.messagearea.setEditable_(True)
        self.messagearea.setSelectable_(False)
        self.messagearea.setBordered_(True)
        self.backgroundcolor = NSColor.colorWithCalibratedRed_green_blue_alpha_(0.75, 0.75, 0.75, 1.0)
        self.messagearea.setBackgroundColor_(self.backgroundcolor)
        # Insert an icon
        self.icon = locate_icon(path=self.parent.configuration.gui_image_path, filename=self.parent.image_file)
        self.imageview = NSImageView.alloc().initWithFrame_(((self.MARGIN, self.HEIGHT-(self.MARGIN+self.PICSIZE)), (self.PICSIZE, self.PICSIZE)))
        self.messagewindow.contentView().addSubview_(self.imageview)
        self.imageview.setImage_(self.icon)


    @objc.python_method
    def display(self):
        """ Display a new message.

            Memory Management: This is called from the views display method
            that has a release pool. So anything that is created here goes
            into that pool. The only things created here are texts that are
            only used one time so we do not need to retain anything.
        """
        self.msgwinheadline = NSString.alloc().initWithString_(self.parent.headline)
        self.msgwincontent = NSString.alloc().initWithString_(self.parent.message)
        self.msgwinformattedmessage = NSMutableAttributedString.alloc().initWithString_(self.msgwinheadline + '\n' + self.msgwincontent)
        self.msgrange = NSRange(0, self.msgwinheadline.length())
        self.boldFont = NSFont.boldSystemFontOfSize_(self.messagearea.font().pointSize())
        self.msgwinformattedmessage.addAttribute_value_range_(NSFontAttributeName, self.boldFont, self.msgrange)
        self.messagearea.setStringValue_(self.msgwinformattedmessage)

        # Resize to fit content
        (oldWidth, oldHeight) = self.messagearea.frame().size
        (newWidth, newHeight) = self.messagearea.cell().cellSizeForBounds_(((0, 0), (oldWidth, 1000)))
        extra_height = newHeight - oldHeight
        extra_width = newWidth - oldWidth
        if extra_height > 0 or extra_width > 0:
            ((winleft, wintop), (winwidth, winheight)) = self.messagewindow.frame()
            self.messagewindow.setFrame_display_(((winleft-extra_width, wintop-extra_height), (winwidth + extra_width, winheight + extra_height)), True)
            (_, (areawidth, areaheight)) = self.messagearea.frame()
            self.messagearea.setFrameSize_((areawidth + extra_width, areaheight + extra_height))
            self.imageview.setFrameOrigin_((self.MARGIN, winheight+extra_height-(self.MARGIN+self.PICSIZE)))

        # Display the view.
        self.messagewindow.orderFrontRegardless()
        self.messagewindow.setLevel_(NSStatusWindowLevel)
        # Set transparency value for the window.
        self.alpha = 1.0
        self.messagewindow.setAlphaValue_(self.alpha)
        # Cancel an existing timer if it exists.
        if self.timer != None:
            self.timer.invalidate()
        # Setup a timer for fading out the window.
        self.timer = NSTimer.timerWithTimeInterval_target_selector_userInfo_repeats_(self.TIME, self, 'fadeOut:', None, False)
        loop = NSRunLoop.currentRunLoop()

        loop.addTimer_forMode_(self.timer, NSDefaultRunLoopMode)
        later = NSDate.dateWithTimeIntervalSinceNow_(self.TIME + 1.0) # At least run until fired once.
        loop.runUntilDate_(later)

    def fadeOut_(self, timer):
        """ Fade out the message window."""
        while self.alpha > 0.0:
            self.alpha = self.alpha - 0.05
            self.messagewindow.setAlphaValue_(self.alpha)
            sleep(0.025)


class CocoaMessageView(MessageView):
    """ Cocoa based view of messages.

        Memory Management: This is called from the main thread.
        So we do not need to create a release pool.
    """
    def __init__(self, model, common, name, configuration, force_substitute=False):
        MessageView.__init__(self, model, common, name, configuration)
        self.headline = ""
        self.message = ""
        self.image_file = 'giritech_48x48.png'
        self.messagewindow = None
        self.FORCESUBSTITUTE = force_substitute

        from AppKit import NSUserNotification, NSScreen, NSWindow, NSTextField, NSTextField, NSColor, NSTexturedBackgroundWindowMask, NSImageView, NSString
        self.substitute = CocoaMessageWindow.alloc()
        self.substitute.initWithParameters(self)

    def display(self, views=None):
        """ Display the messages via either growl or our own message display

            Memory Management: This is called from a thread that is not main.
            If we are using our own messaging we should create a release pool.
            But since we do not create anything new that we want to keep - we
            can just release it again.
        """
        self.substitute.display()

    def update(self):
        """ Update anything relevant to this view. """
        self.headline = self.model.headline
        self.message = self.model.message

    def destroy(self):
        pass


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#

if __name__ == '__main__':

    #from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.message import MessageModel
    from AppKit import NSObject, NSTimer, NSRunLoop, NSDefaultRunLoopMode, NSDate, NSApplication, NSApp

    class AppDelegateForTesting(NSObject):
        def applicationDidFinishLaunching_(self, aNotification):
            self.messageview.display()
            # Setup a timer for self termination
            self.notification = aNotification
            self.timer = NSTimer.timerWithTimeInterval_target_selector_userInfo_repeats_(6.0, self, 'terminate:', None, False)
            try:
                loop = NSRunLoop.mainRunLoop()
            except:
                loop = NSRunLoop.currentRunLoop()
            loop.addTimer_forMode_(self.timer, NSDefaultRunLoopMode)
            later = NSDate.dateWithTimeIntervalSinceNow_(1.0)
            loop.runUntilDate_(later)

        def setMessageView(self, msgview):
            self.messageview = msgview

        def terminate_(self, timer):
            self.notification.object().terminate_(None)

    def test():
        print "Reading model data: (headline=" + messagemodel.headline + \
                                   " message=" + messagemodel.message + ")"

    # Setup Gui.
    app = NSApplication.sharedApplication()
    delegate = AppDelegateForTesting.alloc().init()
    # Create models
    messagemodel = MessageModel()
    messagemodel.subscribe(test)

    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'

    # Create Gui parts
    messageview = CocoaMessageView(messagemodel, None, 'messageview', configuration, True)

    #messageview.controller.set_message("Message from Giritech", "This is a message set via the message controller.")
    #messageview.controller.set_message("Message from Giritech", "This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.")
    messageview.controller.set_message("Message from Giritech", "This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.")
    #messageview.controller.set_message("Message from Giritech", "This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines. This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines. This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.This is a message set via the message controller. There is a lot of text in this message, so it may have to be displayed in multiple lines.")

    # Start Gui
    delegate.setMessageView(messageview)
    NSApp().setDelegate_(delegate)
    delegate.setMessageView(messageview)

    NSApp().run()
