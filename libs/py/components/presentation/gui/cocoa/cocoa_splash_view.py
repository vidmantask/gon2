# -*- coding: utf-8 -*-
"""
Cocoa version of the GUI for modal messages.
"""

from components.presentation.splash import SplashView
from components.presentation.gui.cocoa.cocoa_base_view import CocoaBaseView
from components.presentation.gui.cocoa.area.cocoa_html_area import CocoaHtmlArea
#import AppKit
from AppKit import NSBorderlessWindowMask, NSAutoreleasePool

class CocoaSplashView(CocoaBaseView, SplashView):
    """ A cocoa based update window. """
    def __init__(self, model, handles, name, configuration):
        SplashView.__init__(self, model, handles, name, configuration)
        CocoaBaseView.__init__(self, "G/On", mask=NSBorderlessWindowMask)

        # Currently visible model
        self.current_hide = False
        # Create areas
        self.htmlview = CocoaHtmlArea(self, self.win, self.controller, self.configuration)
        # Set the initial window size and force a resize of the areas
        self.set_frame_size(375, 175)
        self.resize(0, 0, 375, 175)

    def resize(self, left=0, top=0, width=375, height=175):
        """ Resize areas in the window """
        self.htmlview.resize(top=height, width=width, height=height)
        self.win.displayIfNeeded()
    
    def update(self):
        """ Update anything relevant for the modal message view. """
        _pool = NSAutoreleasePool.alloc().init()
        self.htmlview.update(self.model) 
        if self.model.hide:
            self.hide()
            self.current_hide = self.model.hide
        self.win.displayIfNeeded()

#
# -------------------------------------------------------------------------------
#
        
if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions    
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from components.presentation.common import CommonModel
    from components.presentation.splash import SplashModel

    commonmodel = CommonModel()
    splashmodel = SplashModel()
    
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    
    commonview = CocoaCommonView(commonmodel, handles, 'commonview', configuration, False)
    splashview = CocoaSplashView(splashmodel, commonview.handles, 'splashview', configuration)
    
    splashview.controller.set_text(u"Splask!! This is a test of a very long sentence...<br>æøå")
    splashview.display()
    
    commonview.display()
    