"""
Cocoa version of the GUI for updates.
"""
import lib.dictionary

from AppKit import NSAutoreleasePool
from components.presentation.update import UpdateView
from components.presentation.gui.cocoa.cocoa_base_view import CocoaBaseView
from components.presentation.gui.cocoa.area.cocoa_header_area import CocoaHeaderArea
from components.presentation.gui.cocoa.area.cocoa_button_area import CocoaButtonArea
from components.presentation.gui.cocoa.area.cocoa_phase_status_area import CocoaPhaseStatusArea
from components.presentation.gui.cocoa.area.cocoa_text_area import CocoaTextArea
from components.presentation.gui.cocoa.area.cocoa_selection_area import CocoaSelectionArea
from components.presentation.gui.cocoa.area.cocoa_progress_area import CocoaProgressArea
from components.presentation.base.button_area_base import ButtonAreaControllerBase

class CocoaUpdateView(CocoaBaseView, UpdateView):
    """ A cocoa based update window. """
    def __init__(self, model, handles, name, configuration):
        UpdateView.__init__(self, model, handles, name, configuration)
        CocoaBaseView.__init__(self, "G/On")

        # Currently visible model.
        self.current_window_frame_text = self.model.window_frame_text
        self.current_show_selection_list = self.model.show_selection_list
        self.current_info_mode = self.model.info_mode

        # Initial sizes
        self.initial_selection_area_height = 250
        self.initial_info_area_height = 150

        # Create parts for the update window.
        self.header = CocoaHeaderArea(self, self.win, self.controller, self.configuration)
        self.phaseboxtexts = CocoaPhaseStatusArea(self, self.win, self.controller, self.configuration)
        self.info = CocoaTextArea(self, self.win, self.controller, self.configuration)
        self.progress = CocoaProgressArea(self, self.win, self.controller, self.configuration)
        self.selection = CocoaSelectionArea(self, self.win, self.controller, self.configuration)
        self.buttons = CocoaButtonArea(self, self.win, self.controller, self.configuration)

        # Set the initial window size and force a resize of the areas
        if self.current_show_selection_list:
            self.set_frame_size(450, 400)
        else:
            self.set_frame_size(450, 320)
        (_, (_width, _height)) = self.win.contentView().frame()
        self.resize(0, 0, _width, _height)
        

    def resize(self, left=0, top=0, width=450, height=400):
        """ Handle resize calls from GUI side. """
        # Resize areas with standard heights...
        self.header.resize(left=0, top=height, width=width)
        self.phaseboxtexts.resize(top=height-self.header.height, width=width)
        self.buttons.resize(width = width)

        # Calculate flexible sizes...
        if self.current_show_selection_list:
            _info_area_height = self.initial_info_area_height
        else:
            _info_area_height = height - (self.header.height + self.phaseboxtexts.height + self.buttons.height)

        # Resize flexible height areas....
        self.info.resize(top=height-self.header.height-self.phaseboxtexts.height, width=width, height=_info_area_height)
        self.progress.resize(top=height-self.header.height-self.phaseboxtexts.height, width=width, height=_info_area_height)
        self.selection.resize(top=height-self.header.height-self.phaseboxtexts.height-self.info.height,
                              width=width,
                              height=height-self.header.height-self.phaseboxtexts.height-self.info.height-self.buttons.height )
        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('displayIfNeeded', None, True)


    def update(self):
        """ Update current settings to reflect the model.

            This is called from a thread that is not main. So we
            should setup an auto release pool. Nothing new should
            be created during updates. So we can safely release
            the pool again.

            Note: This seem to only work when the pool is not attached
            to the self object.
        """
        _pool = NSAutoreleasePool.alloc().init()

        try:
            if self.controller.get_gui_should_update_on_alerts():
                # Update the window frame text.
                if self.current_window_frame_text != self.model.window_frame_text:
                    self.win.setTitle_ (self.model.window_frame_text)
                    self.current_window_frame_text = self.model.window_frame_text

                # Show or hide the selection list.
                if self.current_show_selection_list != self.model.show_selection_list:
                    (_, (width, height)) = self.win.frame()
                    if self.model.show_selection_list:
                        self.selection.show()
                        _new_height = height+self.initial_selection_area_height
                    else:
                        self.selection.hide()
                        _new_height = height-self.selection.height
                    self.current_show_selection_list = self.model.show_selection_list # Needed before resize...
                    self.set_frame_size(width, height=_new_height)

                # Show the relevant information area.
                if self.current_info_mode != self.model.info_mode:
                    self.progress.hide()
                    self.info.hide()
                    if self.model.info_mode == self.controller.ID_MODE_INFORMATION_BUTTON_TEXT:
                        self.info.show()
                    elif self.model.info_mode == self.controller.ID_MODE_INFORMATION_FREE_TEXT:
                        self.info.show()
                    elif self.model.info_mode == self.controller.ID_MODE_INFORMATION_PROGRESS:
                        self.progress.show()

                # Update the info text to reflect the button descriptions,
                #  when in button text mode...
                if self.model.info_mode == self.controller.ID_MODE_INFORMATION_BUTTON_TEXT:
                    if self.model.info_text != self.model.button_info_text:
                        self.model.info_text = self.model.button_info_text

                # Update all areas.
                self.header.update(self.model)
                self.phaseboxtexts.update(self.model)
                self.info.update(self.model)
                self.progress.update(self.model)
                self.selection.update(self.model)
                self.buttons.update(self.model)

            # Always update the selection list, if shown, because elements can change
            if self.current_show_selection_list:
                self.selection.update(self.model)

        except:
            print "Exception caught in cocoa_update_view.update()"

        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('displayIfNeeded', None, True)

    def _on_default_clicked_(self, sender):
        """ Handle default button push (which is actually the enter key). """
        if self.buttons.current_default_button == ButtonAreaControllerBase.ID_NEXT_BUTTON:
            self.buttons.nextbutton.performClick_(None)
        elif self.buttons.current_default_button == ButtonAreaControllerBase.ID_CANCEL_BUTTON:
            self.buttons.cancelbutton.performClick_(None)

    def _on_ok_clicked_(self, sender):
        """ Called when the 'next' button is clicked. """
        try:
            self.controller.set_next_clicked(True)
        except:
            pass

    def _on_cancel_clicked_(self, sender):
        """ Called when the 'cancel' button is clicked. """
        try:
            self.controller.set_cancel_clicked(True)
        except:
            print "Exception caught in cocoa_update_view._on_cancel_clicked()"

#
# -------------------------------------------------------------------------------
#

if __name__ == '__main__':

    from AppKit import NSApp
    #from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from components.presentation.common import CommonModel
    from components.presentation.update import UpdateModel

    class ClientGatewayOptions():
        def init(self):
            self.gui_image_path = '..'

    dictionary = lib.dictionary.Dictionary()

    commonmodel = CommonModel()
    updatemodel = UpdateModel(dictionary)

    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary

    commonview = CocoaCommonView(commonmodel, handles, 'commonview', configuration, False)
    updateview = CocoaUpdateView(updatemodel, commonview.handles, 'updateview', configuration)

    #updateview.controller.set_header_headline("Update packages")
    #updateview.controller.set_header_icon("GOn_banner_200x80.bmp")
    #updateview.controller.set_header_icon("Package.bmp")

    updateview.controller.set_banner(updateview.controller.ID_BANNER_INSTALL)

    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Install',  'state': updateview.controller.ID_PHASE_ACTIVE},
                                          {'name': 'Restart',  'state': updateview.controller.ID_PHASE_PENDING},
                                          {'name': 'Done',     'state': updateview.controller.ID_PHASE_PENDING}])

#    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
#                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE},
#                                          {'name': 'Install',  'state': updateview.controller.ID_PHASE_ACTIVE},
#                                          {'name': 'Restart',  'state': updateview.controller.ID_PHASE_PENDING}])
#
#    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
#                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE},
#                                          {'name': 'Install',  'state': updateview.controller.ID_PHASE_ACTIVE}])
#
#    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
#                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE}])
#
#    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE}])
#
#    updateview.controller.set_phase_list([])

    updateview.controller.set_selection_list([
                       {'id': 1, 'selected': False,  'icon': 'g_package_default_32x32.bmp', 'name': 'G/On Client for Windows', 'details': 'System: Windows, Version: 5.2, Size: 32 MB', 'description': 'Greatest remote solution ever - win style'},
                       {'id': 2, 'selected': False, 'icon': 'g_package_linux_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 3, 'selected': True, 'icon': None, 'name': 'Packaged Excel for Windows', 'details': 'System: Windows, Version: 5.3, Size: 17 MB', 'description': 'Exploder thingy'},
                       {'id': 4, 'selected': False, 'icon': 'g_package_win_32x32.bmp', 'name': 'Packaged IE for Windows', 'details': 'System: Windows, Version: 5.3, Size: 17 MB, Extra: Here are some extra words that will make the line of text really really long.', 'description': 'Exploder thingy'}
                       ])


#    updateview.controller.set_selection_list([
#                       {'selected': True,  'icon': None, 'name': 'G/On Client', 'arch': 'win', 'ver': '5.2', 'size': '32 MB', 'description': 'Greatest remote solution ever - win style'},
#                       {'selected': False, 'icon': None, 'name': 'G/On Client', 'arch': 'mac', 'ver': '5.2', 'size': '65 MB', 'description': 'Greatest remote solution ever - mac style'},
#                       {'selected': True,  'icon': None, 'name': 'G/On Client', 'arch': 'linux', 'ver': '5.2', 'size': '12 MB', 'description': 'Greatest remote solution ever - linux style'},
#                       {'selected': False, 'icon': None, 'name': 'Extra files', 'arch': 'all', 'ver': '5.2', 'size': '12 MB', 'description': 'Extra files for carrying around'},
#                       {'selected': True, 'icon': None,  'name': 'Remote Desktop', 'arch': 'mac', 'ver': '2.0', 'size': '5 MB', 'description': 'Remote desktop for the mac.'},
#                       {'selected': False, 'icon': None, 'name': 'FileZilla', 'arch': 'win', 'ver': '2.2.2', 'size': '7 MB', 'description': 'FTP client for windows'},
#                       {'selected': True, 'icon': None,  'name': 'Citrix ICA client', 'arch': 'win', 'ver': '10.0', 'size': '19 MB', 'description': 'Citrix client for windows'},
#                       {'selected': False, 'icon': None, 'name': 'Citrix ICA client', 'arch': 'mac', 'ver': '9.0', 'size': '18 MB', 'description': 'Citrix client for mac'},
#                       {'selected': True, 'icon': None,  'name': 'Navision client', 'arch': 'win', 'ver': '4.0.3', 'size': '54 MB', 'description': 'Navision client for windows'},
#                       {'selected': False, 'icon': None, 'name': 'TightVNC Viewer', 'arch': 'win', 'ver': '1.3.9', 'size': '3 MB', 'description': 'Remote desktop connection to macs'},
#                       {'selected': True, 'icon': None,  'name': 'G/On Management', 'arch': 'win', 'ver': '5.2', 'size': '120 MB', 'description': 'Total world domination'},
#                       {'selected': False, 'icon': None, 'name': 'Packaged IE', 'arch': 'win', 'ver': '0.2', 'size': '15 MB', 'description': 'Exploder thingy'}
#                       ])

    # Info commands
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_PROGRESS)
    #updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    updateview.controller.set_info_progress_mode(updateview.controller.ID_MODE_PROGRESS_KNOWN)
    updateview.controller.set_window_frame_text("Total Installation Mayhem!")

    updateview.controller.set_info_progress_complete(45)
    updateview.controller.set_info_headline("Downloading a lot of packages will make this app work in all kinds of places.")
    updateview.controller.set_info_progress_subtext("Package: G/On client for windows")
    #updateview.controller.set_info_free_text("Token is registered and is now awaiting assignment to you. PLease wait for your G/On administrator to complete the enrollment. Please exit and start G/On again when tken enrollment is completed. The token is enrolled as 'Enrolled as 'SoftToken-0001''. When your administrator gets his act together this will have scrolled and maybe it will just change the window size.Token is registered and is now awaiting assignment to you. PLease wait for your G/On administrator to complete the enrollment. Please exit and start G/On again when tken enrollment is completed. The token is enrolled as 'Enrolled as 'SoftToken-0001''. When your administrator gets his act together this will have scrolled and maybe it will just change the window size.")
    updateview.controller.set_info_free_text("Token is registered and is now awaiting assignment to you. PLease wait for your G/On administrator to complete the enrollment. Please exit and start G/On again when tken enrollment is completed. The token is enrolled as 'Enrolled as 'SoftToken-0001''. ")
    #updateview.controller.set_info_free_text("Hej far")
    #updateview.controller.set_info_free_text("Hej mor")


    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_BUTTON_TEXT)


    updateview.controller.set_selection_method(updateview.controller.ID_SELECTION_MULTIPLE)
    #updateview.controller.set_selection_method(updateview.controller.ID_SELECTION_SINGLE)
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    #updateview.controller.set_info_headline("Information")
    #updateview.controller.set_info_free_text("Installation was successful.")
    updateview.controller.set_cancel_description("Click cancel to exit update.")
    updateview.controller.set_next_description("Click next to proceed to the next phase.")
    #updateview.controller.set_info_system_description("Nu er det jul igen og nu er det jul igen ....")

    #updateview.controller.set_info_system_description("Click next to proceed to the next step in the updating process.\n" +
    #                                                  "Click cancel to stop close the update window.")

    # Button commands
    updateview.controller.set_cancel_allowed(True)
    #updateview.controller.set_next_allowed(False)
    updateview.controller.set_cancel_label("Go Away")
    updateview.controller.set_next_label("Proceed")
    updateview.controller.set_show_selection_list(True)
    #updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    #updateview.controller.set_autocomplete_state(True)

    #updateview.controller.set_show_selection_list(False)
    #updateview.controller.set_show_selection_list(True)

    updateview.controller.set_selection_highlighted_by_id(3)
    updateview.display()
    NSApp().run()
