"""
Cocoa version of the GUI for login.
"""
import lib.dictionary

from AppKit import NSAutoreleasePool
from components.presentation.login import LoginView
from components.presentation.gui.cocoa.cocoa_base_view import CocoaBaseView
from components.presentation.gui.cocoa.area.cocoa_header_area import CocoaHeaderArea
from components.presentation.gui.cocoa.area.cocoa_login_area import CocoaLoginArea
from components.presentation.gui.cocoa.area.cocoa_button_area import CocoaButtonArea
from components.presentation.base.button_area_base import ButtonAreaControllerBase

class CocoaLoginView(CocoaBaseView, LoginView):
    """ A cocoa based view of the login.

        Memory Management: This is called from the main thread.
        So we do not need to setup an auto-release pool.
    """
    def __init__(self, model, handles, name, configuration):
        LoginView.__init__(self, model, handles, name, configuration)
        CocoaBaseView.__init__(self, self.dictionary._("G/On Login"))

        # Model reflection
        self.current_state = self.controller.ID_STATE_LOGIN

        self.header = CocoaHeaderArea(self, self.win, self.controller, self.configuration)
        self.login = CocoaLoginArea(self, self.win, self.controller, self.configuration)
        self.buttons = CocoaButtonArea(self, self.win, self.controller, self.configuration)

        # Set the initial window size and force a resize of the areas
        self.set_frame_size(375, 327)
        (_, (_width, _height)) = self.win.contentView().frame()
        self.resize(0, 0, _width, _height)
        # Change settings according to preferences.
        self.controller.configure(self.configuration)
        # Force an update
        self.update()

    def resize(self, left=0, top=0, width=375, height=305):
        """ Resize areas in the window """
        self.header.resize(left=0, top = height, width=width)
        self.buttons.resize(width = width)
        self.login.resize(top = height - self.header.height,
                               width = width,
                               height = height - self.buttons.height - self.header.height )
        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('displayIfNeeded', None, True)

    def clear(self):
        """ Clear model settings for button signals. """
        self.login.passwordfield.setStringValue_('')
        self.login.newpasswordfield.setStringValue_('')
        self.login.confirmnewpasswordfield.setStringValue_('')

        self.model.done = False
        self.model.cancelled = False
        self.model.ok = False
        self.suggestedusername = ""

    def _clear_fields(self):
        self.login.passwordfield.setStringValue_('')
        self.login.newpasswordfield.setStringValue_('')
        self.login.confirmnewpasswordfield.setStringValue_('')

    def update(self):
        """ Update changeable parts of the login window.

            Memory Management: This is called from a thread that is
            not main. So we need to setup an auto-release pool. But
            since we do not create anything we can safely release it
            again.
        """
        _pool = NSAutoreleasePool.alloc().init()

        self.header.update(self.model)
        self.login.update(self.model)
        self.buttons.update(self.model)

        # Update the state of the GUI (Change/plain)
        if self.current_state != self.controller.get_state():
            # Set which field is active when the window is displayed
            if self.login.suggestedusername == '':
                self.win.makeFirstResponder_(self.login.usernamefield)
            else:
                self.win.makeFirstResponder_(self.login.passwordfield)
            self.current_state = self.controller.get_state()
        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('displayIfNeeded', None, True)

    def _on_default_clicked_(self, sender):
        """ Handle default button push (which is actually the enter key). """
        if self.buttons.current_default_button == ButtonAreaControllerBase.ID_NEXT_BUTTON:
            self.buttons.nextbutton.performClick_(None)
        elif self.buttons.current_default_button == ButtonAreaControllerBase.ID_CANCEL_BUTTON:
            self.buttons.cancelbutton.performClick_(None)

    def _on_ok_clicked_(self, sender):
        """ Send credentials and close window. """
        if self.login.usernamefield.stringValue() == "":
            self.display_alert(self.dictionary._('No user name entered'), self.dictionary._("Please provide a user name to identify yourself."))
        else:
            # If there is a request for a new password ...
            if self.current_state == self.controller.ID_STATE_CHANGE_LOGIN:
                # If no password has been given...
                if self.login.passwordfield.stringValue() == "":
                    self.display_alert(self.dictionary._("No password entered"), self.dictionary._("Please enter your current password to validate your identity."))
                # If the new password is not empty...
                elif self.login.newpasswordfield.stringValue() != "":
                    # If the new passwords match...
                    if self.login.newpasswordfield.stringValue() == self.login.confirmnewpasswordfield.stringValue():
                        self.hide()
                        self.controller.set_password_request(self.login.newpasswordfield.stringValue())
                        self.controller.set_credentials(self.login.usernamefield.stringValue(),
                                                        self.login.passwordfield.stringValue())
                        self._clear_fields()
                        self.controller.set_next_clicked(True)
                    # If the new passwords do not match...
                    else:
                        self.display_alert(self.dictionary._("Passwords do not match"), self.dictionary._("Please make sure that the new password and the confirmed password are identical."))
                # If the new password is empty...
                else:
                    self.display_alert(self.dictionary._("Passwords can not be empty"), self.dictionary._("Please enter a new password."))
            else: # State is ordinary login
                if self.login.passwordfield.stringValue() == "":
                    self.display_alert(self.dictionary._("No password entered"), self.dictionary._("Please enter your password to validate your identity."))
                else:
                    self.hide()
                    self.controller.set_credentials(self.login.usernamefield.stringValue(),
                                                    self.login.passwordfield.stringValue())
                    self._clear_fields()
                    self.controller.set_next_clicked(True)

    def _on_cancel_clicked_(self, sender):
        """ Hide the windows and maybe even terminate. """
        self.hide()
        self._clear_fields()
        self.controller.set_cancel_clicked(True)


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
if __name__ == '__main__':

    from AppKit import NSApp
    from components.presentation.login import LoginModel

    class ClientGatewayOptions():
        def init(self):
            self.gui_image_path = '..'

    def listener():
        if loginmodel.requestedpassword != None:
            print "Reading model data: (username=" + loginmodel.username + \
                                      " password=" + loginmodel.password + \
                                      " new pass=" + loginmodel.requestedpassword + ")"
        else:
            print "Reading model data: (username=" + loginmodel.username + \
                                      " password=" + loginmodel.password + \
                                      " new pass=None)"

    # Create models
    dictionary = lib.dictionary.Dictionary()
    loginmodel = LoginModel(dictionary)
    loginmodel.subscribe(listener)

    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary
    #configuration.gui_login_default_button = configuration.OPTION_GUI_DEFAULT_BUTTON_VALUE_CANCEL

    # Create GUI parts
    loginview = CocoaLoginView(loginmodel, None, 'loginview', configuration)

    loginview.controller.set_state(loginview.controller.ID_STATE_CHANGE_LOGIN)
    #loginview.controller.set_state(loginview.controller.ID_STATE_LOGIN)
    loginview.controller.set_suggested_username('Grethe')
    loginview.controller.set_lock_username_field(True)
    loginview.controller.set_lock_username_field(False)

    # Run
    loginview.display()
    NSApp().run()
