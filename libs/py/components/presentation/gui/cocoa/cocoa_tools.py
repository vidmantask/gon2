from os import path as os_path
from AppKit import NSImage

GIRI_GUI_INSTALL_BANNER = "g_install_banner.bmp"
GIRI_GUI_UPDATE_BANNER = "g_update_banner.bmp"
GIRI_GUI_REMOVE_BANNER = "g_remove_banner.bmp"
GIRI_GUI_ENROLL_BANNER = "g_enroll_banner.bmp"
GIRI_GUI_UNINSTALL_BANNER = "g_uninstall_banner.bmp"

GIRI_GUI_HEADER_AREA_HEIGHT = 80.0
GIRI_GUI_MARGIN = 8.0
GIRI_GUI_BUTTON_WIDTH = 65.0
GIRI_GUI_BUTTON_HEIGHT = 25.0
GIRI_GUI_BUTTON_AREA_HEIGHT = GIRI_GUI_BUTTON_HEIGHT + (2 * GIRI_GUI_MARGIN) 


def locate_image_path(path=''):
    """ Locate a path for images, searching through relevant options. """
    temp_path = os_path.abspath(os_path.join(path, 'cocoa', 'images'))
    if os_path.exists(temp_path):
        return temp_path
    return path

def locate_image(path='', filename=''):
    """ Find an image to be displayed in a Cocoa environment. """
    location = os_path.abspath(os_path.join(locate_image_path(path), filename))
    if os_path.isfile(location):
        image = NSImage.alloc().initByReferencingFile_(location)
        return image
    else:
        print "File doesn't exist"

def locate_icon(path='', filename=''):
    """ Find an icon to be displayed in a Cocoa environment. """
    location = os_path.abspath(os_path.join(locate_image_path(path), filename))
    if os_path.isfile(location):
        icon = NSImage.alloc().initByReferencingFile_(location)
        return icon
    else:
        return None

def createNiblessMenu(self):
    """ Create a main menu without the use of a nib. """
    from AppKit import NSMenu, NSMenuItem
    
    self.mainmenu = NSMenu.alloc().init()
    self.app.setMainMenu_(self.mainmenu)
    # Add a system menu corresponding to the apple app menu.
    self.tmp_systemmenu = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('G/On', None, '')
    self.tmp_systemsubmenu = NSMenu.alloc().initWithTitle_('G/On')
    self.tmp_systemmenu.setSubmenu_(self.tmp_systemsubmenu)
    self.mainmenu.addItem_(self.tmp_systemmenu)
    # Add the menu items we relocate later.
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("About G/On", 'orderFrontStandardAboutPanel:', ''))
    self.tmp_systemsubmenu.addItem_(NSMenuItem.separatorItem())
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Services", None, ''))
    self.tmp_systemsubmenu.addItem_(NSMenuItem.separatorItem())
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Hide G/On", 'hide:', ''))
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Hide Others", 'hideOtherApplications:', ''))
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Show All", 'unhideAllApplications:', ''))
    self.tmp_systemsubmenu.addItem_(NSMenuItem.separatorItem())
    self.tmp_systemsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Quit G/On", 'terminate:', ''))

    # Add a standard window menu
    self.tmp_windowmenu = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('Window', None, '')
    self.tmp_windowsubmenu = NSMenu.alloc().initWithTitle_('Window')
    self.tmp_windowmenu.setSubmenu_(self.tmp_windowsubmenu)
    self.mainmenu.addItem_(self.tmp_windowmenu)
    # Add the menu items we get from the nib.
    self.tmp_windowsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Minimize", 'performMiniaturize:', ''))
    self.tmp_windowsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Zoom", 'performZoom:', ''))
    self.tmp_windowsubmenu.addItem_(NSMenuItem.separatorItem())
    self.tmp_windowsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Bring All to Front", 'arrangeInFront:', ''))
    self.app.setWindowsMenu_(self.tmp_windowsubmenu)

    # Add a standard help menu
    self.tmp_helpmenu = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('Help', None, '')
    self.tmp_helpsubmenu = NSMenu.alloc().initWithTitle_('Help')
    self.tmp_helpmenu.setSubmenu_(self.tmp_helpsubmenu)
    self.mainmenu.addItem_(self.tmp_helpmenu)
    # Add the menu items we get from the nib.
    self.tmp_helpsubmenu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("G/On Help", 'showHelp:', ''))

def createEmptyDockMenu():
    """ Creates an empty menu for the dockmenu when no menu is found. """
    from AppKit import NSMenu, NSMenuItem
    edm_menu = NSMenu.alloc().initWithTitle_('Empty')
    edm_menu.addItem_(NSMenuItem.alloc().initWithTitle_action_keyEquivalent_("Empty", None, ''))
    return edm_menu
