"""

Base class for Cocoa based windows.

"""

from AppKit import NSResponder,\
                    NSScreen,\
                    NSResizableWindowMask,\
                    NSMiniaturizableWindowMask,\
                    NSTitledWindowMask,\
                    NSApplication,\
                    NSWindow,\
                    NSAppKitVersionNumber10_4,\
                    NSMinYEdge,\
                    NSColor,\
                    NSAlert,\
                    NSInformationalAlertStyle,\
                    NSCriticalRequest,\
                    NSString,\
                    NSUTF8StringEncoding,\
                    NSFontAttributeName,\
                    NSFont,\
                    NSMutableAttributedString
import PyObjCTools.AppHelper
import objc

class WindowDelegate(NSResponder):
    """ Delegate for handling the closing of sheets, resizing, etc. """

    def windowDidResize_(self, notification):
        """ Called when the window has been resized """
        (_, (_width, _height)) = notification.object().contentView().frame()
        self.view.resize(width = _width, height = _height)
        self.view.win.display()

    @objc.python_method
    def setView(self, iView):
        """ Set the view for resizing and other handling."""
        self.view = iView

    def setHTMLforWebframe_(self, (html, webframe)):
        """ Sets the given HTML into the given Web-frame

            Placed here because this class already inherits from
            NSObject. So we can run it in the main thread.
        """
        html_string = NSString.stringWithString_(html.decode('utf-8', 'ignore'))
        data = html_string.dataUsingEncoding_(NSUTF8StringEncoding)
        webframe.loadData_MIMEType_textEncodingName_baseURL_(data, u"text/html", u"utf-8", None)

    def setTextForTextView_(self, (info_headline, info_text, text_view)):
        """ Set the given text in a text view with formatting. """
        self._text = NSMutableAttributedString.alloc().initWithString_(info_headline + '\n\n' + info_text)
        self._text.setAttributes_range_({NSFontAttributeName: NSFont.boldSystemFontOfSize_(NSFont.systemFontSize())},
                                        (0, len(info_headline)))
        text_view.textStorage().setAttributedString_(self._text)

    def setFrameSize_(self, (win, width, height)):
        """ Set the size of the given window frame.

            Placed here because this class already inherits from
            NSObject. So we can run it in the main thread.
        """
        self.origin, (_, _) = win.frame()
        win.setFrame_display_animate_((self.origin, (width, height)), True, True)
        # Make sure window size does not exceed screen size.
        ((_, _), (_screen_width, _screen_height)) = NSScreen.mainScreen().visibleFrame()
        width = _screen_width if width > _screen_width else width
        height = _screen_height if height > _screen_height else height
        _screen_width = 1500 if _screen_width > 1500 else _screen_width

        win.setMinSize_((width, height))
        win.setMaxSize_((_screen_width, _screen_height))

class CocoaBaseView():
    def __init__(self, windowtext, mask=None):

        self.windowtext = windowtext

        # Standard window settings.
        self.frameheight = 22

        # Build the dialog window
        self.app = NSApplication.sharedApplication()
        self.win = NSWindow.alloc()
        frame = ((0.0, 0.0), (0, 0))
        if mask==None:
            mask = NSTitledWindowMask | NSMiniaturizableWindowMask | NSResizableWindowMask # | NSTexturedBackgroundWindowMask # See Border Masks in cocoa ref. man.
        self.win.initWithContentRect_styleMask_backing_defer_ (frame, mask, 2, 0)
        self.win.setTitle_ (self.windowtext)
        self.win.setLevel_ (3)
        self.win.setAutorecalculatesKeyViewLoop_(True)

        # Set the bottom border for the window
        try:
            NSAppKitVersionNumber10_4 # This exists only from 10.5 and will break the try/except on 10.4 and lower.
            self.win.setAutorecalculatesContentBorderThickness_forEdge_(True, NSMinYEdge)
            self.win.setContentBorderThickness_forEdge_(41.0, NSMinYEdge)
        except NameError: # Occurs when running on 10.4 or below.
            # Create an area for buttons simulating content border.
            from AppKit import NSBox, NSBoxSeparator, NSTextField
            self.buttonbox = NSTextField.alloc().initWithFrame_(((0, 0), (self.windowwidth, 39.0)))
            self.buttonbox.setBordered_(False)
            self.buttonbox.setEditable_(False)
            self.buttonbox.setBackgroundColor_(NSColor.windowFrameColor())
            self.win.contentView().addSubview_(self.buttonbox)

            self.hzline = NSBox.alloc().initWithFrame_(((0, 0), (0, 0)))
            self.hzline.setBoxType_(NSBoxSeparator)
            self.win.contentView().addSubview_(self.hzline)
            self.hzline.setFrame_(((0, 40.0), (self.windowwidth, 1.0)))

        # Setup an alert sheet.
        self.alert = NSAlert.alloc().init()
        self.alert.setAlertStyle_(NSInformationalAlertStyle)

        # Setup the delegate for resizing and alert sheets
        self.delegate = WindowDelegate.alloc().init()
        self.delegate.setView(self)
        self.win.setDelegate_(self.delegate)
        # Other settings
        self.win.setReleasedWhenClosed_(False)
        self.win.makeKeyWindow()
        self.win.setMovableByWindowBackground_(True)
        self.app.requestUserAttention_(NSCriticalRequest) #NSInformationalRequest

    def display(self, views=None):
        """ Show the window """
        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('center', None, True)
        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('makeKeyAndOrderFront:', self.win, True)

    def destroy(self):
        """ Destroy the login window.

            Memory Management: This is called from a thread that is
            not main and it is not the last thing we do before shutdown.
            So we set up an autorelease pool and release it again because
            we do not create anything here.
        """
        from AppKit import NSAutoreleasePool
        _pool = NSAutoreleasePool.alloc().init()

        try:
            self.win.performSelectorOnMainThread_withObject_waitUntilDone_('setReleasedWhenClosed:', True, True)
            self.win.performSelectorOnMainThread_withObject_waitUntilDone_('close', None, True)
        except:
            pass

    def hide(self):
        """ Hide the window. """
        from AppKit import NSAutoreleasePool
        _pool = NSAutoreleasePool.alloc().init()

        self.win.performSelectorOnMainThread_withObject_waitUntilDone_('orderOut:', self, True)

    def display_alert_compleation_handler(self, returnCode):
        pass

    def display_alert(self, messageText, informativeText):
        """ Display an alert sheet. """
        self.alert.setMessageText_(messageText)
        self.alert.setInformativeText_(informativeText)
        self.alert.beginSheetModalForWindow_completionHandler_(self.win, self.display_alert_compleation_handler)

    def set_frame_size(self, width, height):
        """ Set the window size

            Done in delegate so it can be done in the main thread.
        """
        self.win.delegate().performSelectorOnMainThread_withObject_waitUntilDone_('setFrameSize:', (self.win, width, height), True)
