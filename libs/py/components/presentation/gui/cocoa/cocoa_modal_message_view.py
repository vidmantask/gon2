# -*- coding: iso-8859-1 -*-
"""
Cocoa version of the GUI for modal messages.
"""
import lib.dictionary

from AppKit import NSAutoreleasePool
from components.presentation.modal_message import ModalMessageView
from components.presentation.gui.cocoa.cocoa_base_view import CocoaBaseView
from components.presentation.gui.cocoa.area.cocoa_html_area import CocoaHtmlArea
from components.presentation.gui.cocoa.area.cocoa_button_area import CocoaButtonArea
from components.presentation.base.button_area_base import ButtonAreaControllerBase

class CocoaModalMessageView(CocoaBaseView, ModalMessageView):
    """ A cocoa based update window. """
    def __init__(self, model, handles, name, configuration):
        ModalMessageView.__init__(self, model, handles, name, configuration)
        CocoaBaseView.__init__(self, "G/On")
        
        # Currently visible model
        self.current_size = None
        # Create areas
        self.htmlview = CocoaHtmlArea(self, self.win, self.controller, self.configuration)
        self.buttons = CocoaButtonArea(self, self.win, self.controller, self.configuration) 
        # Set the initial window size and force a resize of the areas
        self.set_frame_size(500, 200)
        self.resize(0, 0, 500, 200-self.frameheight)

    def resize(self, left=0, top=0, width=375, height=305):
        """ Resize areas in the window """
        self.buttons.resize(width = width)
        self.htmlview.resize(top=height, width=width, height=height-self.buttons.height-1)
        self.win.displayIfNeeded()

    def _on_default_clicked_(self, sender):
        """ Handle default button push (which is actually the enter key). """
        if self.buttons.current_default_button == ButtonAreaControllerBase.ID_NEXT_BUTTON:
            self.buttons.nextbutton.performClick_(None)
        elif self.buttons.current_default_button == ButtonAreaControllerBase.ID_CANCEL_BUTTON:
            self.buttons.cancelbutton.performClick_(None)

    def _on_cancel_clicked_(self, sender):
        """ When the cancel button is clicked. """
        self.hide()
        self.controller.set_cancel_clicked(True)
        
    def _on_ok_clicked_(self, sender):
        """ When the OK button is clicked. """
        self.hide()
        self.controller.set_next_clicked(True)
    
    def update(self):
        """ Update anything relevant for the modal message view. """
        _pool = NSAutoreleasePool.alloc().init()
        
        if self.current_size != self.model.size:
            if self.model.size == self.controller.ID_LARGE_SIZE:
                self.set_frame_size(500, 600)
                self.resize(0, 0, 500, 600-self.frameheight)
            elif self.model.size == self.controller.ID_SMALL_SIZE:
                self.set_frame_size(500, 200)
                self.resize(0, 0, 500, 200-self.frameheight)
            self.current_size = self.model.size
        self.htmlview.update(self.model)    
        self.buttons.update(self.model)
        self.win.displayIfNeeded()

#
# -------------------------------------------------------------------------------
#
        
if __name__ == '__main__':
    
    #from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.cocoa.cocoa_common_view import CocoaCommonView
    from components.presentation.common import CommonModel
    from components.presentation.modal_message import ModalMessageModel

    class ClientGatewayOptions():
        def init(self):
            self.gui_image_path = '..'
            self.dictionary = lib.dictionary.Dictionary()



    commonmodel = CommonModel()
    modalmessagemodel = ModalMessageModel(lib.dictionary.Dictionary())
    
    handles = None
    configuration = ClientGatewayOptions()
    
    commonview = CocoaCommonView(commonmodel, handles, 'commonview', configuration, False)
    modalmessageview = CocoaModalMessageView(modalmessagemodel, commonview.handles, 'modalmessageview', configuration)
    #modalmessageview.display()
    
    #testtext = "This is a test of cocoa modal window."    
    testtext = u"This is a test of cocoa modal window. This is a test of several lines in the same window. Up to three lines are supported. ���"


    
    #modalmessageview.controller.set_text(testtext + testtext + testtext + testtext)
    modalmessageview.controller.set_text(testtext)
    modalmessageview.controller.set_cancel_allowed(False)
    modalmessageview.controller.set_cancel_visible(False)
    #modalmessageview.controller.set_next_allowed(False)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_SMALL_SIZE)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_LARGE_SIZE)
    #modalmessageview.hide()
    modalmessageview.display()
    commonview.display()
    


