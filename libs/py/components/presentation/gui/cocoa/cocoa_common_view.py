"""
Cocoa version of the central GUI common view.

Fix for deprecationWarnings:
    For some reason pyObjC does not appreciate if we call our
    own init functions init(). So these are renamed to initWithImagePath()
    and initWithHandle().

"""

from AppKit import NSObject, NSApp, NSApplication
from cocoa_tools import createEmptyDockMenu, createNiblessMenu, locate_icon
from components.presentation.common import CommonView
import objc

class AppDelegate(NSObject):

    @objc.python_method
    def initWithParameters(self, handles):
        """ The delegate for the application.

            Memory Management: This is create and called from the main
            thread. So no auto-release pool is needed here.
        """
        #self = super(AppDelegate, self).init() # For some reason this now gives an error in unit tests.
        self.handles = handles
        return self

    def applicationDockMenu_(self, sender):
        """ This will add a menu to the application icon in the dock."""
        if NSApp().mainMenu().itemWithTitle_('gon_application_launch_menu'):
            return NSApp().mainMenu().itemWithTitle_('gon_application_launch_menu').submenu()
        else:
            return createEmptyDockMenu()

    def oncommand_(self, sender):
        """ When a selection is made in the menu - add it to the launch list.
            TODO: This will work as long as the system menu only has a few items
            in one level. But we need a smarter solution as soon as possible.
        """
        if sender.menu().title() == 'Launch':
            self.handles['appmenu'].controller.add_to_launch_list(self.handles['appmenu'].launchidmap[sender.tag()])
        else:
            print "ERROR: Cocoa_common_view on_command received an unknown senders title:", sender.menu().title()

    def applicationShouldTerminate_(self, sender):
        """ Called when quit is selected from the dock menu.

            Return NSTerminateCancel to make sure that termination
            is handled from server side orders.
        """
        from AppKit import NSTerminateCancel
        if self.handles.has_key('sysmenu'):
            self.handles['sysmenu'].controller.add_to_launch_list(1)
        return NSTerminateCancel

    def applicationDidFinishLaunching_(self, sender):
        """ Location for setting up things available only after launch.

            For instance the main menu is only available after launch
            on 10.4.3 based systems.
        """
        if self.handles['commonview'].usenib:
            self.handles['commonview'].setupMainMenu()

    def windowShouldClose_(self, sender):
        print "user clicked close"

    def terminate_(self, sender):
        """ Called when quit is selected from the main menu.

            This should return NSTerminateCancel to make sure that termination
            is handled from server side orders.
        """
        if self.handles.has_key('sysmenu'):
            self.handles['sysmenu'].controller.add_to_launch_list(1)
        else:
            print "ERROR: System menu handle has not been set properly."

        return None

    def performZoom_(self, sender):
        """ Standard window menu item - here it brings the login window back from miniaturized state. """
        if self.handles.has_key('loginview') and self.handles['loginview'].win.isMiniaturized():
            self.handles['loginview'].win.orderFront_(sender)

    def arrangeInFront_(self, sender):
        """ Standard window menu item - should order all windows in front. """
        if self.handles.has_key('loginview') and self.handles['loginview'].win.isMiniaturized():
            self.handles['loginview'].win.orderFront_(sender)


class CocoaCommonView(CommonView):
    """ Setup of a menu starting point.

        Memory Management: This is called from the main thread.
        So we do not need to setup an auto-release pool.
    """
    def __init__(self, model, common, name, configuration, usenib=True):
        """ Display a starting point in top menu bar. """
        CommonView.__init__(self, model, common, name, configuration)
        self.app = NSApplication.sharedApplication()
        self.app.setWindowsNeedUpdate_(True)
        self.delegate = AppDelegate.alloc().initWithParameters(self.handles)
        self.app.setDelegate_(self.delegate)
        self.usenib = False

        # Setup main menu depending on whether we are using NIB or not.
        if self.usenib:
            from AppKit import NSBundle
            # See if the NIB can be loaded.
            if not NSBundle.loadNibNamed_owner_('gon_client', self):
                self.usenib = False
            else:
                NSBundle.loadNibNamed_owner_('gon_client', self)
        if not self.usenib:
            self.setupMainMenu()
        self._display_running = False

    def setupMainMenu(self):
        """ A setup method for accessing the main menu after it has been created.

            This is needed because we can not be sure of the main menus existence
            until applicationdidfinishlaunching: has been called. So this is usually
            called from that delegate method.
        """
        # See if a main menu is created by the NIB.
        if self.usenib: # Setup a main menu using the NIB as a skeleton.
            self.applemenu = NSApp().mainMenu().itemWithTitle_('')
        else: # Setup a hand built main menu without the use of a NIB.
            createNiblessMenu(self)
            self.applemenu = NSApp().mainMenu().itemWithTitle_('G/On')
            self.app.setApplicationIconImage_(locate_icon(path=self.configuration.gui_image_path, filename='giritech.icns'))

        self.windowmenu = NSApp().mainMenu().itemWithTitle_('Window')
        self.mainmenu = NSApp().mainMenu()
        self.handles['mainmenu'] = self.mainmenu

        # Re-target some system menu items.
        self.applemenu.submenu().itemWithTitle_('About G/On').setTarget_(NSApp())
        self.applemenu.submenu().itemWithTitle_('Hide G/On').setTarget_(NSApp())
        self.applemenu.submenu().itemWithTitle_('Hide Others').setTarget_(NSApp())
        self.applemenu.submenu().itemWithTitle_('Show All').setTarget_(NSApp())
        self.applemenu.submenu().itemWithTitle_('Quit G/On').setTarget_(self.delegate)
        self.windowmenu.submenu().itemWithTitle_('Bring All to Front').setTarget_(self.delegate)

        # Make sure the application menu is setup and linked.
        if self.handles.has_key('appmenu') and not self.handles['appmenu'].hasdonesetup:
            self.handles['appmenu'].setup()


    def orderFrontStandardAboutPanel_(self, sender): pass    # Dummy for 'About G/On' menu item.
    def hide_(self, sender): pass                            # Dummy for 'Hide G/On' menu item.
    def hideOtherApplications_(self, sender): pass           # Dummy for 'Hide Others' menu item.
    def unhideAllApplications_(self, sender): pass           # Dummy for 'Show All' menu item.
    def terminate_(self, sender): NSApp().terminate_(sender) # Dummy for 'Quit G/On' menu item.

    def display(self, views=None):
        """ Starts the GUI with all connected elements.

            Memory Management: This is called from the main thread
            so we do not need to setup an auto-release pool.
        """
        self._display_running = True
        NSApp().activateIgnoringOtherApps_(True)
        NSApp().run()
        self._display_running = False

    def update(self):
        """ Update anything not consistent with the model.

            Memory Management: This is called from a thread that is
            not main. But it doesn't really do anything. So we will
            not setup an auto release pool now.
        """
        pass

    def destroy(self):
        """ Stops the GUI thread on order from the server side.

            Memory Management: This is called from a thread that is not
            main. So we will set up a auto release pool for anything
            created here.

            We need to send an event for the thread to stop completely. This is
            apparently not that extraordinary. See:
            http://www.cocoabuilder.com/archive/message/cocoa/2008/10/9/219842
        """
        from AppKit import NSAutoreleasePool, NSEvent, NSApplicationDefined, NSMakePoint

        _pool = NSAutoreleasePool.alloc().init()
        try:
            if self._display_running:
                event = NSEvent.otherEventWithType_location_modifierFlags_timestamp_windowNumber_context_subtype_data1_data2_(NSApplicationDefined,
                                                                                                                              NSMakePoint(0,0),
                                                                                                                              0, 0.0, 0,
                                                                                                                              None,
                                                                                                                              0, 0, 0)
                NSApp().performSelectorOnMainThread_withObject_waitUntilDone_('stop:', None, True)
                NSApp().postEvent_atStart_(event, False)
        except:
            import sys
            (_etype, evalue, _etrace) = sys.exc_info()
            print "ERROR: CocoaCommonView destroy unexpected exception", evalue
        #pool.drain()

#
# Move this to a test area when appropriate.
#
if __name__ == '__main__':
    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.common import CommonModel

    commonmodel = CommonModel()

    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'

    commonview = CocoaCommonView(commonmodel, None, 'commonview', configuration, False)
    #commonview.mainmenu.itemWithTitle_('G/On').submenu().itemWithTitle_('Quit G/On').setTarget_(commonview)
    commonview.display()
