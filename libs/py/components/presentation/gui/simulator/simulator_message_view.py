"""
Simulator version of the GUI for messages.
"""

from components.presentation.message import MessageView

class SimulatorMessageView(MessageView):
    """ Simulator based view of messages. """
    def __init__(self, model, common, name, imagepath):
        MessageView.__init__(self, model, common, name, imagepath)
        
    def update(self):
        pass
    
    def display(self):
        pass
    
    def destroy(self):
        pass

