"""
Simulator version of the GUI for login.
"""
from components.presentation.login import LoginView

class SimulatorLoginView(LoginView):
    def __init__(self, model, common, name, imagepath):
        LoginView.__init__(self, model, common, name, imagepath)
        self.display_called = False 

    def update(self):
        pass

    def display(self):     
        self.display_called = True

    def destroy(self):
        pass
