# -*- coding: utf-8 -*-
"""
Simulator version of the modal message view.

"""
from components.presentation.modal_message import ModalMessageView

class SimulatorModalMessageView(ModalMessageView):
    def __init__(self, model, handles, name, configuration, unittest=False):
        ModalMessageView.__init__(self, model, handles, name, configuration)
        self.update()
        
    def update(self):
        pass
    
    def hide(self):
        pass
    
    def display(self):
        pass
    
    def destroy(self):
        pass

