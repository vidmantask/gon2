"""

Simulator based update view.

"""
from components.presentation.update import UpdateView

class SimulatorUpdateView(UpdateView):
    """ A cocoa based update window. """
    def __init__(self, model, handles, name, configuration, unittest=False):
        UpdateView.__init__(self, model, handles, name, configuration)

    def update(self):
        pass
    
    def hide(self):
        pass

    def display(self):
        pass

    def destroy(self):
        pass
