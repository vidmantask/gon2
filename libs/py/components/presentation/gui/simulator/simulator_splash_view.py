"""
Simulator version of a splash screen.

"""

from components.presentation.splash import SplashView

class SimulatorSplashView(SplashView):
    
    def __init__(self, model, handles, name, configuration, unittest=False):
        SplashView.__init__(self, model, handles, name, configuration)
        
    def update(self):
        pass
    
    def hide(self):
        pass
    
    def display(self):
        pass
    
    def destroy(self):
        pass

