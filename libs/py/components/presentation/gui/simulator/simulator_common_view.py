"""
Simulator version of the central GUI common view.
"""
from components.presentation.common import CommonView

class SimulatorCommonView(CommonView):
    def __init__(self, model, common, name, imagepath):
        CommonView.__init__(self, model, common, name, imagepath)

    def display(self):
        pass
    
    def update(self):
        pass

    def destroy(self):
        pass
