"""
Simulator version of the GUI for menus.
"""

from components.presentation.menu import MenuView

class SimulatorMenuView(MenuView):
    def __init__(self, model, common, name, imagepath):
        MenuView.__init__(self, model, common, name, imagepath)

    def update(self):
        pass
    
    def display(self):
        pass

    def destroy(self):
        pass
        
