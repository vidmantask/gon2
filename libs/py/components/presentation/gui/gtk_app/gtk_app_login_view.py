"""
GtkApp version of the GUI for login.
"""
import gobject
import gtk


from components.presentation.login import LoginView
from components.presentation.gui.gtk import gtk_tools


class GtkAppLoginView(LoginView):
    """ A GtkApp based view of the login. """    
    def __init__(self, model, handles, name, imagepath):
        LoginView.__init__(self, model, handles, name, imagepath)

#        print handles.keys()

        self._action_area = self.handles['action_area']
        self._build()
        self.update()
        self.is_active = False


    def _build(self):
        """ Build the dialog window. """
        
        
        self.view_vbox = gtk.VBox()

        
        # Message
        frame = gtk.Frame(None)
        frame.set_border_width(8)
        self.view_message = gtk.Label('')
        frame.add(self.view_message)
        self.view_vbox.add(frame)

        # Login and password box incl icon
        hbox = gtk.HBox(False, 8)
        hbox.set_border_width(8)
        self.view_vbox.add(hbox)

        stock = gtk.image_new_from_stock(gtk.STOCK_DIALOG_AUTHENTICATION, gtk.ICON_SIZE_DIALOG)
        hbox.pack_start(stock, False, False, 0)

        table = gtk.Table(2, 2)
        table.set_row_spacings(4)
        table.set_col_spacings(4)
        hbox.pack_start(table, True, True, 0)

        label = gtk.Label("Login")
        label.set_use_underline(True)
        table.attach(label, 0, 1, 0, 1)
        self.view_username = gtk.Entry()
        table.attach(self.view_username, 1, 2, 0, 1)
        label.set_mnemonic_widget(self.view_username)

        label = gtk.Label("Password")
        label.set_use_underline(True)
        table.attach(label, 0, 1, 1, 2)
        self.view_password = gtk.Entry()
        self.view_password.set_visibility(False)
        table.attach(self.view_password, 1, 2, 1, 2)
        label.set_mnemonic_widget(self.view_password)

        # Buttons
        bbox = gtk.HButtonBox()
        bbox.set_border_width(5)
        bbox.set_layout(gtk.BUTTONBOX_SPREAD)
        bbox.set_spacing(0)
        self.view_vbox.add(bbox)

        button = gtk.Button(stock='gtk-ok')
        bbox.add(button)
        button.connect('clicked', self._on_ok_clicked_)

        button = gtk.Button(stock='gtk-cancel')
        button.connect('clicked', self. _on_cancel_clicked_)
        bbox.add(button)

        self._action_area.add(self.view_vbox)

    def display(self):
        """ Show the login window to the user. """
        self.is_active = True
        self._action_area.resize_children()
        self._action_area.show_all()
        

    def destroy(self):
        """ Destroy the login window. """
        self.is_active = False

    def update(self):
        """ Update changable parts of the login window. """
        self.view_message.set_text(self.model.greeting)
        self.view_username.set_text(self.model.username)
        self.view_password.set_text(self.model.password)

    def _on_ok_clicked_(self, sender):
        """ Send credentials and close window. """
        self._action_area.hide()
        self.controller.set_credentials(self.view_username.get_text(), self.view_password.get_text())
        self.is_active = False

    def _on_cancel_clicked_(self, sender):
        """ Hide the windows and maybe even terminate. """
        self._action_area.hide()
        self.is_active = False
