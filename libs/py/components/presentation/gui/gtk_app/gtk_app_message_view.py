"""
GtkApp version of the GUI for messages.
"""
import gobject
import gtk
import pygtk
pygtk.require('2.0')
import pango
import time

from components.presentation.message import MessageView
from components.presentation.gui.gtk import gtk_tools


class GtkAppMessageView(MessageView):
    """ GtkApp based view of messages. """
    def __init__(self, model, common, name, imagepath):
        MessageView.__init__(self, model, common, name, imagepath)

        self._message_area = self.handles['message_area']
        self._message_area_box = gtk.VBox(False,0)
        self._message_area.add(self._message_area_box)

        self._messages = []
        self._messages_view = self._init_message_view()
        self._message_area_box.add(self._messages_view)

        self.timeout_ms = 1000
        self._destroyed = False
        self._auto_update_start()

    def _init_message_view(self):
        message_view = gtk.TextView();
        message_view.set_size_request(100, 80)
        message_view.set_editable(False)
        message_view_buffer = message_view.get_buffer()
        message_view_buffer.create_tag("headline", weight=pango.WEIGHT_BOLD, scale=pango.SCALE_SMALL)
        message_view_buffer.create_tag("message", scale=pango.SCALE_SMALL)
        return message_view

    def display(self):
        if not self._destroyed:
            self._messages_view.get_buffer().set_text('')
            message_view_buffer = self._messages_view.get_buffer()
            iter = message_view_buffer.get_iter_at_offset(0)
            for (headline, message, time) in self._messages:
                message_view_buffer.insert_with_tags_by_name(iter, headline, "headline")
                message_view_buffer.insert(iter, "\n")
                message_view_buffer.insert_with_tags_by_name(iter, message, "message")
                message_view_buffer.insert(iter, "\n")
            self._message_area.show_all()

    def update(self):
        """ Update anything relevant to this view. """
        self._messages.append( (self.model.headline, self.model.message, time.time()) )
        if len(self._messages) > 2:
            self._messages = self._messages[len(self._messages)-2:]

    def _auto_update_start(self, *args):
        if not self._destroyed:
            gobject.timeout_add(self.timeout_ms, self._auto_update)
    
    def _auto_update(self, *args):
        if not self._destroyed:
            new_messages = []
            for (headline, message, add_time) in self._messages:
                if time.time() - add_time < 10:
                    new_messages.append((headline, message, add_time))
            self._messages = new_messages 

            self.display()
            self._auto_update_start()

    def destroy(self):
        self._destroyed = True




