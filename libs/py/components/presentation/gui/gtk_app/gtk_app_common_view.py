"""
GtkApp version of the central GUI common view.
"""
import gobject
import gtk

from components.presentation.common import CommonView
from components.presentation.gui.gtk_app import gtk_app_tools

class GtkAppCommonView(CommonView):

    def __init__(self, model, common, name, imagepath):
        """ 
        Display a starting point in system tray.
        """
        CommonView.__init__(self, model, common, name, imagepath)

        self._main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self._main_window.set_title("G/On Client")

        self._main_window.connect("delete_event", lambda *w: self.destroy())

#        imagepath = "/media/card/GIRITECH-5.0.2-Linux/py/components/presentation/gui/gtk_app/images"
#        imagepath = "/root/source/py/components/presentation/gui/gtk_app/images"
        self.banner = gtk_app_tools.locate_image(path=imagepath, filename = 'GOn_banner_200x80.gif')

        self._main_window_hbox = gtk.VBox(False, 0)
        self._main_window_hbox.show()

        self._logo_area = gtk.VBox(False, 0)
        box = gtk.EventBox()
        box.add(self.banner)


        self._logo_area.add(box)
        self._logo_area.show()
        self._logo_area.show_all()

        self._main_window_hbox.pack_start(self._logo_area, expand=False, fill=False, padding=3)

        self._action_area = gtk.Frame(None)
        self._main_window_hbox.pack_start(self._action_area, expand=True, fill=True, padding=3)
        
        self._menu_area = gtk.Frame(None)
        self._menu_area_sw = gtk.ScrolledWindow()
        self._menu_area_sw.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self._menu_area.add(self._menu_area_sw)
        self._main_window_hbox.pack_start(self._menu_area, expand=True, fill=True, padding=3)
        self._menu_area_sw.hide()
        self._menu_area.show()
        
        self._message_area = gtk.Frame(None)
        self._message_area.show_all()
        self._main_window_hbox.pack_end(self._message_area, expand=False, fill=True, padding=3)
        
        self._main_window.add(self._main_window_hbox)
        self._main_window.show()

        self.handles['action_area'] = self._action_area
        self.handles['menu_area_sw'] = self._menu_area_sw
        self.handles['message_area'] = self._message_area

        self._is_destroyed = False


    def display(self):
        gtk.gdk.threads_init()  
        gtk.main()

    def update(self):
        pass

    def destroy(self):
        if not self._is_destroyed:
            gtk.main_quit()
        self._is_destroyed = True

