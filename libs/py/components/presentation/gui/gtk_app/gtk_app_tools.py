import os
import sys

import gobject
import gtk


def locate_image_path(path=''):
    """ Locate a path for images, searching through relevant options. """
    temp_path = os.path.abspath(os.path.join(path, 'gtk_app', 'images'))
    if os.path.exists(temp_path):
        return temp_path
    return path

def locate_image(path='', filename=''):
    """ Find an icon to be displayed in a GtkApp environment. """
    location = os.path.abspath(os.path.join(locate_image_path(path), filename))
    icon = gtk.Image()
    if not os.path.isfile(location):
        defaulticon = "giritech.bmp"
        location = os.path.abspath(os.path.join(locate_image_path(path), defaulticon))
    print location
    icon.set_from_file(location)
    return icon
