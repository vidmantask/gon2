"""
GtkApp version of the GUI for menus.
"""
import gobject
import gtk
import threading

from components.presentation.menu import MenuView
from components.presentation.gui.gtk import gtk_tools


class GtkAppMenuViewEmpty(MenuView):
    """ A GtkApp based view for a menu. """
    def __init__(self, model, handles, name, imagepath):
        MenuView.__init__(self, model, handles, name, imagepath)

    def update(self):
        """ Update the GtkApp menu view.
        
            Makes sure that the view is 
            consistent with the model.
        """
        pass

    def _update_tree_store(self, tree_store, model, tree_store_iterator):
        pass

    def display(self):
        """ Display the GtkApp based menu.
        
            Makes sure the menu is visible to 
            the user. In this case it means to
            pop up the menu from the sys tray.
        """
        pass

    def destroy(self):
        """ Destroy the menu so that it can be called again. """
        pass




class GtkAppMenuView(MenuView):
    """ A GtkApp based view for a menu. """
    def __init__(self, model, handles, name, imagepath):
        MenuView.__init__(self, model, handles, name, imagepath)
        self.name = name
        
        self.handles = handles
        self._login_view = self.handles['loginview']

        self._menu_area_sw = self.handles['menu_area_sw']
        self._tree_store = gtk.TreeStore(gobject.TYPE_STRING, gobject.TYPE_INT, gobject.TYPE_BOOLEAN)
        self._view_model_tree = gtk.TreeView(self._tree_store)
        self._view_model_tree.connect('row-activated', self._cb_clicked)
        self._view_model_tree.append_column(gtk.TreeViewColumn("Menu", gtk.CellRendererText(), text=0))
        self._menu_area_sw.add(self._view_model_tree)

        self._update_lock = threading.Lock()
        self._destroyed = False
        self._menu_changed = False

        self._auto_update_start()
 
    
    def _cb_clicked(self, tree_view, path, tree_view_column):
        iter = self._tree_store.get_iter(path)
        
        id = self._tree_store.get(iter, 1)
        id_is_runable = self._tree_store.get(iter, 2)
        if id_is_runable:
            launch_id = id[0]
            self.handles[self.name].controller.add_to_launch_list(launch_id)

    def _auto_update_start(self, *args):
        if not self._destroyed:
            gobject.timeout_add(2000, self._auto_update)
    
    def _auto_update(self, *args):
        if not self._destroyed:

            if self._menu_changed:
                self._update_lock.acquire()
                self._view_model_tree.hide()
                self._tree_store.clear()
                self._update_tree_store(self._tree_store, self.model.topfolder, None)
                self._menu_changed = False
                self._update_lock.release()

            self.display()
            self._auto_update_start()


    def update(self):
        """ Update the GtkApp menu view.
        
            Makes sure that the view is 
            consistent with the model.
        """
        self._menu_changed = True
        

    def _update_tree_store(self, tree_store, model, tree_store_iterator):
        for folder in model.folders:
           iter = tree_store.append(tree_store_iterator, [folder.text, folder.id, False] )
           self._update_tree_store(tree_store, folder, iter)
            
        for item in model.items:
            iter = tree_store.append(tree_store_iterator, [item.text, item.id, True] )


    def display(self):
        """ Display the GtkApp based menu.
        
            Makes sure the menu is visible to 
            the user. In this case it means to
            pop up the menu from the sys tray.
        """
        if not self._login_view.is_active:
            self._menu_area_sw.show_all()
        else:
            self._menu_area_sw.hide()



    def destroy(self):
        """ Destroy the menu so that it can be called again. """
        self._destroyed = True

