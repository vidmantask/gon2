# -*- coding: iso-8859-1 -*-
"""
MFC version of the GUI for a splash screen.
"""
import sys
import time


import win32gui
import win32con

from components.presentation.splash import SplashView
from components.presentation.gui.mfc.mfc_base_view import MfcBaseView
from components.presentation.gui.mfc.area.mfc_html_area import MfcHtmlArea

class MfcSplashView(MfcBaseView, SplashView):
    """ MFC based view of a splash screen. """
    def __init__(self, model, common, name, configuration):
        SplashView.__init__(self, model, common, name, configuration)
        MfcBaseView.__init__(self, "SplashView")

        self.current_hide = self.model.hide
        # Style for the window frame.
        self.style = win32con.WS_POPUP | win32con.WS_BORDER | win32con.DS_SETFONT
        # Create local message map
        self.message_map = {
                            win32con.WM_INITDIALOG: self._on_init_dialog,
                            win32con.WM_DESTROY: self._on_destroy,
                            win32con.WM_CLOSE: self.hide,
                            }
        # Create the window.
        self.create_window(self.message_map, self.style, self.configuration.gui_image_path)
        # Create areas.
        self.htmlview = MfcHtmlArea(self.hwnd, self.hinst, bottom_separator=False)
        # Get ready for action!
        self.update()
        self.resize()

    def _on_destroy(self, *params):
        """ Destroy method called by messages via the GUI loop. """
        self.htmlview.destroy()
        self.destroy()

        try:
            win32gui.DestroyWindow(self.hwnd)
            self.hwnd = None
            win32gui.UnregisterClass(self.classAtom, self.hinst)
            self.hinst = None
        except win32gui.error:
            pass

    def update(self):
        """ Update the things that can be changed via the controller. """
        self.htmlview.update(self.model)
        if self.model.hide:
            win32gui.SendMessage(self.hwnd, win32con.WM_CLOSE, 0, 0)
            self.current_hide = self.model.hide
        # Send a paint message to repaint invalidated areas.
        win32gui.UpdateWindow(self.hwnd)

    def resize(self, left=0, top=0, width=375, height=175):
        """ Set sizes and relative positions of window elements. """
        super(MfcSplashView, self).resize(left, top, width, height)
        self.htmlview.resize(0, 0, width, height)
        win32gui.UpdateWindow(self.hwnd)


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.common import CommonModel
    from components.presentation.splash import SplashModel
    import lib.dictionary

    # Create models
    commonmodel = CommonModel()
    splashmodel = SplashModel()
    # Create GUI parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary =   lib.dictionary.Dictionary()

    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    splashview = MfcSplashView(splashmodel, commonview.handles, 'modalmessageview', configuration)
    splashview.display()

    testtext = """
        <html>
        <head>
        </head>
        <body style="overflow:hidden; margin:0px;>
        <div style=width:375px; height:175px; overflow:hidden";>
        <img src="data:image/gif;base64,
        R0lGODlhdwGvAPcAAEUACEkAB00AB0gACFIABlAAB1UABlQAB1EDClIGDVcGDFoABVgABlkAB10ABV8ABlsGDFwGDFQJEFcNFFgNFF4QFlsWHFwTGFwTGV0ZH2IABGAABWAABmUABGQABWQABmoAA2gABGkABW0AA20ABWEQFmMWG2YZHnMAAnEAA3IABHEABXUAAnUABHcABXsAAXkAAngABHoABX0AAX0ABGMdImkdImIgJmYmLGgmLGgpL2stMm4tMmwwNW0zOHEsMXEzN3E5PnQ8QXZARXdDSHxDR3hDSHpGS4EAAYAABIUAAIUAA4QABIgAAIkAA4gABI4AA4wABJAAA5YAA5kAA50AAqEAAqAAA6UAAqoAAq8AAa0AArMAAbYAAbkAAbwAAYBGSoFMUIFQVIRQVIdTV4ZWWYVZXYlWWYtcX4dcYIlfY4pgZI5gY49jZo1maY9pbJBjZpFmaZFpbJNvcpVvcpZzdpd2eZh2eZx5e8MAAMEAAcQAAMgAAMwAAJ9/gZx/gqB/gaCChKKDhaCDhqGGiKOJi6WMjqiMjqqTlayTlauWmK6Zm6+cnrCcnbGfoLCfobKio7amp7epqripqrisrbqvsMC2t8C5usO8vMK8vcS/v8bCwsjGxsrJyczMzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAP8ALAAAAAB3Aa8AAAj/AAEIHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKPTmgqNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNq3cq169ahYMOKHevRq9mzaNOqXcu2rdu3cOPKnUu3rt27ePPq3cu3r9+/dwMIHky4sOHDiBMrXsy4sePHkCNLnky5suXLmDNr3sy5s+fPoEOLHk26tOnTqFOrXs26tevXsGPLnk27tu3buHPr3s27tYDfwIMLH068uPHjyJMrX868ufPn0KNLn069uvXr1Xtr3869O2/s4MOL/x9Pvrz58+jTU/fOvr3796PVy59Pv779+/jz69/Pv7///wAGKOCABBZo4IEIJqjgggw26OCDEEYoIX9BuBGJJxh6oskfbmSwnBsY3jDcB1Ts0ceJffBRBQnhZeDGiz648ZsRPkxo4434FaDjjjz26OOOCbhxSYZEEnnHjz5m4gknAvBIghUpoihlH1msgOSVWLroiRs+eGJBAZS4geWYZJZp5plopqnmmmy26eabcF5JXRqdFGknhjIipwOGchQA3AomSrlHoFJGcZ2OnhSAAyF19KBGnjhGKql5bhJCZCYv4oBAARQY4QYdnRhBZiEYSrAjDVJS4UKTOiZxRapx/v/miQBGmMHJIAKIGeeuvPbq66/ABivsm4pkuMkaOrL6W7IJlInhI8uSgKIXLDYJnJ9JSGllnIm6McQdixRgyLDklmvuueimG+cdGS5CAZxHYCiEjgx8ceIe2yK5gpS7igkEBTf4UIAY6hZs8MEIJ+zmDhlCsqskGO64BYqrjgnliVcorPHGHHfs8ZqMZJiDjgf4WLKZGGD4B8kobkEyjyfveAC/PHJgcwM7rgDFzk/E/PHPQActdAEEFG300UgbfUGGhiTt9NNIs+tJDUU7geISRENtdBUotnA0ilAQoAIXU+4RttNZa6322my37fbbcMct99x012333XgjDSKGVOP/jWElRnuBotsboEjF1ydCoQIffEx5IhZ5Ry755JRXbvnlmONdSYYG4A0EhmIYjeIVbhuA4h5HN97HxH1wIcXOF5+oQua012777bjnLnmGjuS9CIaI9yHF24QebS+KWSDdAop86O7889BHL33kGcqRN4aIGKA9AyhKoX3bXQxu9PGrF639+Vig6ADSnU/v/vvwx5+5ARnGYX77BGh/55ZOl4EhD0bzANjMl7/8de583yMA105kPBSRwGlLGCACDThB+Vnwghh8HgI3yMHvZcgN+DNg0fYXhw4OyRNHK9yJhne0ELJPCyhC4PH44MKiPQBsNWRfB3fIwx768IdADKIQ/4dIxCIa8YhITKISOwg3+mEoEBRsn/7uZD+kVYBPCOTeCjcYRS4Wb3wn+sIOCdC9LnIwg2hMoxonR0QCZIgTCTzf/QwICDxxcA4YgsAGu/e9AxKwhaa7lwzDyEM+tjBpS0ykIhfJyEY68pGQbGQjOCfCCSbQDxgq4QYxRImkoSgPBLSkJQ03yD6I8Wl8FGUkV8nKVrrylbCMJBwyJAYgYtITmjxfETAUBg5moWtBhAKKaFBKMe4wlbFMpjKXycxmujJDmrBlJjfoCAx1kAnI9GEeYlhKPRRyi84MpzjHSc5yag8RGULDD2+Zyyni4ZgO/GEMVGcFBpyvXoSEp/DMyf/Pfvrzn0pkQAmIBAQG2POe22MAOxEYBwyZIKEGNagUWmaAg24vi1KyqPaOJ8aIelSL+8wiQEdK0pKa86BoIJI6E3pRAyz0fHWKZkU7yLrVzdSgFY2oHlDkBA7u1JTfFN5Hc+pRkxr1qEgF4keXutSZGiARRNIEGQ6KU4P+DpcRZQOG0FDVpW7gp33owgqoqj1UoUgLTOUoUw3aPY1qFKJrjatc50rXutr1rnjNq173yte++vWvgG1qRGdpp0ksYhGaKFIcIioIa+a0gwwQAfn6sIcqSEEKv1Td6hqQ1jC+dXt8LGpgR0va0pr2tKhNrWpRa4ML7U+xBj0Bhg5R1wf/wNBxmu1DEhhwgM72QQ9xRdEUVkvc4hr3uMhNrnJLEAdM3KkTgDhDBCLaUE/8oK4GaAAJ0ue4PUhhrmpda1uVS97ymve86E3vWi2BIb6SgAYt4IB650vf+tr3voF9QET1ywD+8nepEYhDHMDQ34/6N66cNeh/C7zWBTP4vwfGr4QnTOEKy/XAFdUvUZfqYAZnWMEZ/rCCQVxgEYvYow8QcYr7m9MV2/PBI2awhWdM4xrv1QE4zrGOd4zjBfTYxw5YAJCD3OMcD9nIQi5ykH2c5CED+clPJvKSd9zkH0/5ykqOMpZ5zOUue/nLYA6zmMdM5jKb+cxoTrOa16xmIbv5/81wjrOc50znOtv5znjOs573zOc++/nPgA60oAdN6EIb+tCITrSiF83oRjv60ZCOtKKXzOQ+U9rNU77zpSl96Sa/OdOe1rSkR03qUpv61KUGNZ07jepWu/rVsI71qNnMYy17+ci0zrWud83rXvv618AOtrBpDWUr37rWUi52lbNsbCYbG8nJrvSzk51lXA/72tjOtra3DWw+XznJnA41q6EN7ihj2tNWlnani1zuZp9b2uWWtbznTW9Zc/ve+M63vvfN7377+98AD7jAB07wghv84AhPuMIXzvCGO/zhEL/3BiZO8YpbfOIOwHjGN4Bjim984xy/eI4xrnGOZxzkH/8vecg/DvKSs9zkKid5yGfe8Yvb/OY4z7nOd87znvv850APutCHTvSiG93MME/6yDu+9B17/OQpXznToe5ypeuY5DV/ucmnDvOsR/zrYB+40UUuc5y3fOxoT7va1872trv97XCP+9i1XvObn33lMuc6yvOecq0/neZUT3re+S73whv+8IhPvOIXz/jGO/7xkI+85CdP+cpb/vKYz7zbPUBxzgPd8xb3POhxPvqKl34DpRd953t+es27/vWwT3zrTR960k+89aBXfe1nT3vW3/73sQ++8If/edQbn/PIPz7wk5/83+de+bpX/fOND33aM1/5zq9+83Wv/e5f//veDz//+Mcv/vKT//zmTz/616/+9rP//e6PP/xFP34PMN/+3ac+/pG//+UfH//i13/op38CKH8GOH8HmIAIuIAK2IAM+IAOCH0a0AETWIEUeIEWOIEUmIEYqAEZ6IEYGIIXuIEdaIEj+IEayIEoGIIqeIIdsIEtGIMsOIMyWIM0eIM2mIM4uIM62IM8+IM+GIRAOIRCWIQeeIRImIRJCINK2IROeIQk+IQgOIVNyIRSeIVQSIVYuIVc2IVe+IVgGIZiOIZkWIZmeIZVGIRTOIMvaII4uIRuWIFKOIJMeINtCIRLSIR6WIR72Id8+Id+GIiAOIiCWIiEGIUsuIZZyIYpuIIp/3iHcviIMaiIO3iClEiImGiImpiJnLiJntiJoLiHkiiCpDiJJFiKUciBbWiDkMiDcviCnxiLoTiLsliLtHiLtmiHb+iIbAiLieiGvhiJwUiDl6iLsFiMuJiMuaiMzLiMzuiDvhiN0niM01iN1kiNdFiNjziNGniN3hiN2/iN4jiO5FiO5niO6JiO6riO7NiO7viO8BiP8jiP9FiP9niP+JiP+riP/NiP/viPABmQAjmQBFmQBnmQCJmQCimQIQCPDdkBD2mNDxmR0UiR7TiRC5mRGrmRA9mQHgmRIPmRH/mCIhmSISCSJxmSKlmRJjmSLHmSFFmSHpmSMqmSFgmRNf+Zkza5kzrZkzz5kz4ZlEA5lEJZlER5lEaZlEi5lErZlEyJkzpJkiAplTIJkzQ5lS1plViplTVJlURplRjplGL5lGNZlmR5lmaZlmi5lmqZk2D5lnAJlXA5l3SJk3IZlzSJl3S5l3Npl3z5l4AZmII5mIRZmIZ5mIiZmIq5mIzZmI75mJAZmZI5mZRZmZZ5mZiZmZq5mZzZmZ75maAZmqI5mqRZmqZ5mqiZmqq5mqzZmq6pmiMAk7F5krMZArVpm7iZm7hZm7HJm7L5m7tJm8I5nLPZm8Dpm7mJnLqJnMa5nMDpnMMJndKpnNT5nNUZndc5nda5ndjJndrZneD5neL/mZ3k6Z3lGZ7naZsigJvrGZvtGQLvOQLr2Z7vOZ/sCZ/36Z75aZ/yqZ776Z/16Z/62Z8Eip8DaqAGyp8BeqALiqAN+qD56aARCqEMOqEWWqEYKqEZSqEa2qEc+qEX6qEhCqIbOqImOgIEKgK9qaLquaIFmqLGKZ8xyqIy2qICKqD0uaI3WqMwiqM2yqM0GqS7maM/GqQuWqRHCqRJaqRI2qRK6qRM+qRSGqVUuqRWCqVXOqVZWqVY2qVa6qVcCqQhAAIgMKa4WaaxiaZmSqZnWqZqmqbBqaZy2qZ0yqZy6qZzmqZuWqd7Cqd+aqZ6WqdnOqh/mqeAeqiGmqiEiqiL/6qohdqokPqoksqok+qolHqplpqpkYqpm6qpldqpZzoCIICio1qqonqqpjqqqEqqq5qqrfqqqmqqp/qqs+qqsbqqtQqruoqrtqqrvfqrvhqswDqswlqsxHqsxpqsyLqsytqszPqswlqq0jqtokqmqEqm0lqt2Kqt1rqt3tqt31qt2iqu2Eqt4Jqt5zqu3zqt6Jqu66qu7hqv5iqv8Dqv9lqv+Pqu+kqv+3qv/Zqv/Bqw/iqwADuwBluw/1qu5+qt6lqw3MqtCgux8rquCnuw5SqxDpuwBKuxFruxHtuxIJuxHyuyIcuxJHuy+4qiKjurLKuqK+uyKiuruNqyMfuyK/97szBbs6yKszdrsztLszrrs0A7tC5btEFrtD+LtER7tEybtE27tE4btVA7tUpbtU9rtVKLtVR7tVybtV7LqjB7q0L7tSybq0AbtjqLtGK7tWnbtWz7tlobt24rt2RLt3A7t3hbt3l7t3pLtj37t4AbuII7uIRbuIZ7uIibuIq7uIzbuI77uJAbuZI7uZRbuZZ7uZibuZq7uZzbuZ77uaAbuqI7uqRbuqZ7uqibuqq7uqzbuq5LuSmAorEruyo7uyMQu7Z7u7S7srmru7vru8Dru7Nru8PLu7X7u8ULvMR7vL8rvMyrvM+7vMgbvdQ7vdYLvdcrvdi7vdrbvdXLvd//673ZG77kO77mC77n2724u77se7spgLvu+77uG7/y+772G7/4e7/2u77zq7/s2771G8D9G8AATMD8678FnMD5S8ALrMAI3MAQ/MASzMAT7MAUfMEWnMERjMEbrMEV3MEg/MH367zjm7vJ+7zpa7wkvMLYK70nLL7B+8Lli740DMM2PMM3nMI6XMM43MM7nMM8/MM4fMAc/L8DjMBFbMADvMD0y8T+e8Qh/MQinMQeTMVRXMVYfMVaPMVZzMVbbMVeHMYc7LzwW8a6W7xofMZqbMZsTLvJa8ZkrMZrvLttXMdufMd2nMdzrMd8vMd+3MeA/MeCHMiEPMiGXMiIfMiK/8zH+0vFSFy/+XvF/9vI9FvBj7y/klzJXQzGnLzJnvzFnyzGodzJoFzKorzBIdzEBjzJmSzAUjzBTuzJRDzKtGzKpHzKuHzLulzLuczLjtzIwBzMwjzMxFzMxnzMyJzMyrzMzNzMzvzM0BzN0jzN1FzN1nzN2JzN2rzN3NzN3vzN4KzMKPC+40zO9lvOKTDO6JzO5ry/68zO7QzP8gzP5YzO9ezO5xzP9yzP9pzP8UzP/szPAd3P+jzQBl3QCC3QCU3QCt3QDP3QB+3QEQ3RCz3RFl3RGC3RGQ3RKvC+HT3OH50CId3RHc3OJR3SIM3OKb3SIm3S5nzSHu3SLe3SKf/d0jUN0yw90jJ90zRt0z6t0znd00D900Jd1EF91ESN1EO91Ead1E7N1Erd1FD91FJd1VF91VQN0uqszirA1V6t0ijQ1ens1WId1madzmWd1ltd1luN1m2NAmcd118N12Pt1nYd12zN1net1ns913yN134d2H092IBN2H992IJd2IqN2Iad2Iy92I4d2Y092ZBN2SwA15ed2Sig2ZyN2Zvt2Z0d2p8t2qPt2aV92Z992qnd2aut2qKN2prt2rL92rNd27R927ad27i927rd27z9274d3MA93MKd2nB93Mid3Mq93Mzd3M793NAd3dI93dRd3dZ93did3dq93dzd3d7/vd0sEN7ivdnhTd6ZLd7ljd7pbd6jnd7ufd7vrd7wjd7sLd/mbd/jnd/xfd/vXd/9rd/w7d8BDuACXuAEfuD/neADruAGzuAIvuAQ3uAR/uASXuEUfuEOjtrIreEcrtwaftyxXdqmbdwf/uHJbeIjjuIoDuIsnuIb7uEtTuIx3uEuXuMybuM0fuM6nuM8PuM+juM/vuNB3uNAXuRCbuREfuRKDtvqLeEAnt/1beFP3uTyDeFTTt8YHt8ZPuFbLuVc/uVeHuZZLuZdPuZmXuZoDuZnPt9V3uZu/uZwHudyPud0Xud2fud4nud6vud83ud+/ueAHuiCPuiEXuiGfuiI/57oir7ojN7ojv7okB7pkj7plF7nMcACMXDpmo7pnL7pmu7pnt7poh7qpA7qmF7qpz7qqW7qma7qpN7prO7qsv7qtD7rtl7ruH7rup7rvL7rvt7rwP7rwh7sxC7r8g0D4o3s4a3s6M3syc4CzI7s0f7sy97sbu7s1A7tx37t2T7t2d7t4F7t4q7t4+7t5H7u5p7u4Y7u667u5d7u8P7u8s7u8+7u9H7v9p7v8Y7v+67s+u7s/q7e2K7v1B7w4g7wBZ/w/D7v5E7wC//wDh/x/T7x9U7xEG/xEl/xGn/xG5/x0P4CLPACIP8CMDDyJR/yJz/yJK/yKL/yLZ/yMG/yKv+f8izv8iIf8jZP8y2P8zyf8zK/8yKv8z4P9DFP9D8/9Ehf9El/9Erf9Ez/9EYf9Usv9U5P9VA/9Vhf9Vl/9Vpf9Q0P7wh/7uPe8db+9Qdf9mZv9h6f9h7f9hj/9mQf9xw/924v93VP93CP7jCw93zf937/94Af+II/+IRf+IZ/+Iif+Iq/+Izf+I7/+JAf+ZI/+ZG/7Qw/9tU+7Q4v9gJf5eb+7Z2/8ZyP93af93d/+qaf+qW/+qTf+hlP+bAf+7I/+7Rf+7Z/+7if+7q/+7zf+77/+8Af/MI//MRf/MZ//Mif/MEv8sy/8kHP/CXf/M7f/CUf/Stv/dgP/dQv/dv/z/3ZL/3W7/3a//zkX/7jP/3fj/7nn/7sv/7uX/7tD//vr/7yX//0f//xj//zn//8v//+b/8AAePFQIEEB74oiPBgQoYHHSo0GNFhwoUQFTaUSJHiw40ZH1r8iNGixpAVPZ4caTIlSpEtVbpk+VJmTJorbcK8OTNnTZw9dcKMQSNo0BdDZxgdKpRGUaVEYxwVehTqU6RRkzJNWjXr1KlKsTa1Gpaq2KVZy4rlqjUtWrVt2b4dG3etXLd04c7FWzfvXb19+f61G3iv4KozYBg2TAPxi8WID8NQ/DjxDMaHGVem3Niy48iONXvGjPlxZ8mbTWc+Ddmz6tOhP7tu/Vp2/2zaqG3Dvj07d23cvXX75v1beHDiu40DP665NOXHCA07lwx9scDm1J9Xvz59dfbS0KVbXo7d+njp35lz9y4+PXr17dm/J+8+Pvz18+3Xxy8/P339/fn/v8+/AGfI7LLLZlCMsQQLPNBABDNTbEEJIWSwQAsXPHDCBhu8kEIFPaSMw4FA1PDBD080McUSVySxRRRZfNFFFWWEccYYb7Qxxxp3pLFHHHn8MTMCh2SMwCKFHBJJIpE80MglnXwyySiTPPLIKaWsMkorrYSSSS2/7DLLMMH0ckwzy0RTzDTJVLNNNt8808044VxzTjvrlDJPPffks08//wQ0UEEHJbRQQ/8PRTRRRRdltFFHH4U0UkknpbRSSy/FNFNNN+W0U08/BTVUUUcltVRTT0U1VUmRCJRVKV1NFFZGZY01T1pVxTVXXR1ltVcCe0UCWF9dHfbXYGcINllkj1W22V+RhVbYaJeV1tdph2U222qftbZabakFt9lvxfW23HDHRddccs9VN1123103XnflbbdeeOfF115lr322X37/9ddfWAeGll+CrS3432IJFnjIbqdNmOGIHaa4YYsnvlhijSvGuOONM+b4Y49DJhlkk0c+WWSVS454YXWjdVnbfGE2mFmYtwW2Zn2XvVbfmem912ehgyYaaKN/Rnroo5VOuuhvex45Z2z/OU6Z54UpRvhgqwFe2WapWV65arFRJjvsssFGe2yz1077bLXbHjhZcuWmW2a768a7bmrz5jvvvfsGXNi5Aye8cMMPRzxxxRdnvHHHH4c8csknp7xyyy/HPHPNN+e8c88/Bz100UcnvXTTT0c9ddVXZ31xJQp/ne7YG58d8tppr/v21nfnvXfQZ9edb+CRGD7Y4gMP/njCj1cecOaTfd546KeXvnriqb/e+uiz535777H/Xnvwxxe//O7JP9/88NNnf/3iX4df+vjnz57+6+nH/3799c+/f/7l/5//7DfAABZQgAZE4AEVmEAGLtCBDYTgAyUYQQpO8IH/a9/+7Gc9//VNL3b5A+Dz4lfADgIQg+5DHwpLmMEVqpCFL3RhDFM4wxbSEIY2BN4SkKBDHSqBhz7cIRB5SLwh9nAJQvQhEn+4xB0SsYlFFOITlfhEJxYxiEw8Ihar2MQpZvGKX/RiGLs4RiySEYxmFGMZ1XjGNaaRjW90YxzROMc20hGOdpRjHXVIxT3usYnBsiIf5eZHQCbLj4f8oxUROcRCFpKRijRkJBtJRUH+0ZKInCQmLynJTWaSk5oE5SdF6UlSdtKUoSwlKk85ylWmkpWqhOUrP6kEWtbSlrfEZS51uUte9tKXvwRmMIU5TGIW05jHRGYylblMZjbTmc+EZjSlOU1qVv/TmtfEZja1uU1udtOb3wRnOMU5TnKW05znRGc61blOdrbTne+EZzzlOU96frMJwLznLfOJzH0us5/8xOU/6zlQgo7zngel5UGboFCE5rOhCV2oEhY6UYlGlKIXTahENcrQjVaUowjtaEMtOtKPZhSkHyWpR1V60ZSyFKUvXWlLZQpTl8aUpjO1aU5rulOc8vSmP9VpT4UKVIqG9KFHrahRQ7pUpCqUqU9talKjOtWNHpWqV4VqVrG6Va12latf9WpYwTpWsZaVrFadaFpnqla2ltSnHm1rWuHq07iqNagMretb9TrUu/Z1r0Tl61/9GljCAtawgz2sYF2aV8Y21rEYj4VsZCU7WcpW1rKXxWxmNbtZznbWsQEBADs="
        width="375" height="175"/>
            <div style="position:absolute; left:150px; top:25px; width:200px; height:125; color:#cccccc">
            <h3>Headline</h3>
            <p>Text</p>
            </div>
        </div>
        </body>
        </html>
        """

    splashview.controller.set_text(u"Hallo<br>.")

    # Start Gui
    commonview.display()
