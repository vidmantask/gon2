"""
Base class for MFC based windows.

"""
import win32api
import win32con
import struct
import sys
from components.presentation.gui.mfc.area.mfc_button_area import MfcButtonArea

if "--noxp" in sys.argv:
    import win32gui #@UnusedImport
else:
    import winxpgui as win32gui #@Reimport

from components.presentation.gui.mfc.mfc_tools import locate_icon

class MfcBaseView:
    def __init__(self, dlgclassname):

        self.processmessages = False
        self.dlgclassname = dlgclassname
        self.windowtext = "G/On"
        self.minimumwidth = 0
        self.minimumheight = 0
        self.selectionareaadded = False

        self.width = 300 # Arbitrary start value
        self.height = 300 # Arbitrary start value

        win32gui.InitCommonControls()
        self.hinst = win32api.GetModuleHandle(None)
        self.wc = win32gui.WNDCLASS()
        self.wc.SetDialogProc() # Make it a dialog class.
        self.wc.lpszClassName = dlgclassname
        self.wc.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW
        self.wc.hCursor = win32gui.LoadCursor( 0, win32con.IDC_ARROW )
        self.wc.hbrBackground = win32con.COLOR_WINDOW + 1
        self.wc.lpfnWndProc = {}
        self.wc.cbWndExtra = win32con.DLGWINDOWEXTRA + struct.calcsize("Pi")
        self.classAtom = win32gui.RegisterClass(self.wc)
        self.hwnd = None

        self.white_brush = win32gui.CreateSolidBrush(win32api.RGB(255,255,255))
        self.gray_rgb = win32api.RGB(200,200,200)
        self.black_rgb = win32api.RGB(0,0,0)

    def create_window(self, message_map, style, image_path):
        """ Create the actual window. """
        template = [ [self.windowtext, (0, 0, 0, 0), style, None, (8, "Tahoma"), None, self.dlgclassname] ]
        self.hwnd = win32gui.CreateDialogIndirect(self.hinst, template, 0, message_map)
        # Calculate frame sizes
        [left, top, right, bottom] = win32gui.GetWindowRect(self.hwnd)
        [_, _, width, height] = win32gui.GetClientRect(self.hwnd)
        self.frame_height = bottom - top - height
        self.frame_width = right - left - width
        # Set the system menu icon
        icon = locate_icon(path=image_path, filename="giritech.ico")
        win32gui.SendMessage(self.hwnd, win32con.WM_SETICON, win32con.ICON_SMALL, icon)

    def disable_close_button(self):
        """ Disable the close button and the close menu item in the system menu """
        framemenu = win32gui.GetSystemMenu(self.hwnd, False)
        win32gui.EnableMenuItem(framemenu, win32con.SC_CLOSE, win32con.MF_BYCOMMAND | win32con.MF_DISABLED)

    def centre_window_on_desktop(self):
        """ Make sure the window is centered on the desktop. """
        #assert self.hwnd is not None, "Dude"
        if self.hwnd is not None:
            desktop = win32gui.GetDesktopWindow()
            desktop_left, desktop_top, desktop_right, desktop_bottom = win32gui.GetWindowRect(desktop)
            horiz_centre, vert_centre = win32gui.ClientToScreen(desktop, ((desktop_right - desktop_left)/2, (desktop_bottom - desktop_top)/2))
            window_left, window_top, window_right, window_bottom = win32gui.GetWindowRect(self.hwnd)
            window_width, window_height = win32gui.ClientToScreen(desktop, ((window_right - window_left), (window_bottom - window_top)))
            win32gui.MoveWindow(self.hwnd,
                                horiz_centre - (window_width/2), # left
                                vert_centre - (window_height/2), # top
                                window_width,                    # width
                                window_height,                   # height
                                False)

    def display(self, views=None):
        """ Display the window. """
        self.update()
        self.centre_window_on_desktop()
        self.set_min_window_size()

        try:
            win32gui.ShowWindow(self.hwnd, win32con.SW_SHOW)
            win32gui.UpdateWindow(self.hwnd)

            try:
                win32gui.SetForegroundWindow(self.hwnd)
            except win32gui.error:
                pass
            try:
                win32gui.SetActiveWindow(self.hwnd)
            except win32gui.error:
                pass

        except win32gui.error:
            pass

    def hide(self, *_):
        """ Close the update window. """
        win32gui.ShowWindow(self.hwnd, win32con.SW_HIDE)

    def resize(self, left=0, top=0, width=300, height=300):
        """ Resize the window. """
        # Make sure resize does not exceed the screen size.
        (_, _, screen_width, screen_height) = win32gui.GetWindowRect(win32gui.GetDesktopWindow())
        self.width = screen_width - 50 if width > screen_width else width
        self.height = screen_height- 50 if height > screen_height else height
        # Do the resizing.
        win32gui.SetWindowPos(self.hwnd, 0, 0, 0, self.width + self.frame_width, self.height + self.frame_height, win32con.SWP_NOMOVE)
        win32gui.UpdateWindow(self.hwnd)

    def set_min_window_size(self, width=None, height=None):
        """ Set the minimum size values for user resizing.

            This method adjusts the settings so that the window
            size is never larger than the screen size in each
            direction.
        """
        self.minimumwidth = width
        self.minimumheight = height
        # if no values are given - use the current window size.
        if width==None or height==None:
            [left, top, right, bottom] = win32gui.GetWindowRect(self.hwnd)
            if width==None:
                self.minimumwidth = right-left
            if height==None:
                self.minimumheight = bottom-top

    def _on_destroy(self, *_):
        """ Destroy method called by messages via the GUI loop. """
        try:
            win32gui.DestroyWindow(self.hwnd)
            try:
                win32gui.UnregisterClass(self.classAtom, self.hinst)
            except win32gui.error:
                pass
            #win32api.PostQuitMessage(0)
        # Catch errors related to taking down the modal message user
        # interface and let others through.
        except win32gui.error:
            print "Exception caught in mfcBaseView._on_destroy() method"

    def _on_init_dialog(self, *_): #@UnusedVariable
        """ Center the window on the desktop. """
        self.centre_window_on_desktop()
        self.processmessages = True

    def _on_command(self, hwnd, message, wparam, lparam):
        """ Basic on command handler.

            Create default button behavior for all windows.
            - The ESC key is always mapped to the cancel button.
            - The ENTER is always mapped to the default key.
            - The cancel and next buttons always sets the controller state.
        """
        # When the ESC key is selected...
        if wparam == win32con.IDCANCEL:
            if hasattr(self, 'buttons') and self.buttons.current_cancelallowed:
                win32gui.PostMessage(self.hwnd, win32con.WM_COMMAND, win32api.MAKEWORD(MfcButtonArea.ID_BUTTON_CANCEL, win32con.BN_CLICKED), 0)
        # When the default button is selected - figure out which one it is...
        elif wparam == win32con.IDOK:
            if hasattr(self, 'buttons'):
                if self.buttons.current_default_button == self.controller.ID_NEXT_BUTTON and self.buttons.current_nextallowed:
                    win32gui.PostMessage(self.hwnd, win32con.WM_COMMAND, win32api.MAKEWORD(MfcButtonArea.ID_BUTTON_NEXT, win32con.BN_CLICKED), 0)
                elif self.buttons.current_default_button == self.controller.ID_CANCEL_BUTTON and self.buttons.current_cancelallowed:
                    win32gui.PostMessage(self.hwnd, win32con.WM_COMMAND, win32api.MAKEWORD(MfcButtonArea.ID_BUTTON_CANCEL, win32con.BN_CLICKED), 0)
        # When the Cancel button is clicked
        elif wparam == MfcButtonArea.ID_BUTTON_CANCEL:
            self.controller.set_cancel_clicked(True)
        # When the 'next' button is clicked
        elif wparam == MfcButtonArea.ID_BUTTON_NEXT:
            self.controller.set_next_clicked(True)

    def _on_min_max_info(self, hwnd, message, wparam, lparam):
        """ Determine minimum and potentially maximum sizes for the window.

            the minimum size is set to the size the window is created
            with.

            See: http://msdn.microsoft.com/en-us/library/ms632605(VS.85).aspx
            for MINMAXINFO Structure informtation.
        """
        POINT_FORMAT = "LL"
        MINMAXINFO_FORMAT = 5 * POINT_FORMAT
        data = win32gui.PyGetString (lparam, struct.calcsize (MINMAXINFO_FORMAT))
        minmaxinfo = list (struct.unpack (MINMAXINFO_FORMAT, data))

        # Set minimum tracking sizes.
        minmaxinfo[6] = self.minimumwidth
        minmaxinfo[7] = self.minimumheight

        # Set maximum tracking sizes (limit by banner size).
        if win32api.GetSystemMetrics(win32con.SM_CXMAXTRACK) > 1500:
            # Maximum horizontal tracking size
            minmaxinfo[8] = 1500
        else:
            minmaxinfo[8] = win32api.GetSystemMetrics(win32con.SM_CXMAXTRACK)
        # Maximum vertical tracking size ()
        minmaxinfo[9] = win32api.GetSystemMetrics(win32con.SM_CYMAXTRACK)

        win32gui.PySetMemory (lparam, struct.pack (MINMAXINFO_FORMAT, *minmaxinfo))
        return 0

    def destroy(self):
        """ Destroy the update window.

            Preferably through the messaging system. Alternatively
            through direct system calls.
        """
        try:
            win32gui.PostMessage(self.hwnd, win32con.WM_DESTROY)
        except win32gui.error:
            try:
                self._on_destroy()
            except win32gui.error:
                pass
