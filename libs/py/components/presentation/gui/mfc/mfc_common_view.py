"""
Mfc version of the central GUI common view.
"""

import win32api
import win32con
import sys

import lib.dictionary
from components.presentation.common import CommonView
from components.presentation.gui.mfc.mfc_tools import locate_icon, GIRI_GUI_DEFAULT_ICO_IMAGE

if "--noxp" in sys.argv:
    import win32gui #@UnusedImport
else:
    import winxpgui as win32gui #@Reimport

class MfcCommonView(CommonView):
    """ Setup of a menu starting point.

        The idea is that this is some sort of central
        starting point for any menu that the user may
        get to see. In this case the starting point is
        in the system tray but it could also be placed
        somewhere else.

        The windowhandler is created here and linked
        into the handles dict that is accessible from
        all gui elements.
    """

    ID_TRAY_ICON = 901

    def __init__(self, model, common, name, configuration):
        """ Display a starting point in sys tray.

            The message map indicates what methods to call
            in case of the given event. It is assumed that
            the menus themselves are created by the menuview
            class. So this class controls what menus are
            to be displayed in case of different user actions
            on the menu starter.
        """
        CommonView.__init__(self, model, common, name, configuration)

        self.tooltip = self.dictionary._("G/On menu")
        self.icofile = "giritech.ico"
        self.openmenu = None
        self.visible = True
        self.closing = False

        try:
            assert self.icofile.endswith('ico'), "File type not supported"
            self.icon = locate_icon(path=self.configuration.gui_image_path, filename=self.icofile)
        except AssertionError:
            print "File type not supported", self.icofile, ", use ICO files for the system tray (loading default icon instead)"
            self.icon = locate_icon(path=self.configuration.gui_image_path, filename=GIRI_GUI_DEFAULT_ICO_IMAGE)

        # If explorer restarts this is called.
        self.WM_TASKBAR_CREATED = win32gui.RegisterWindowMessage("TaskbarCreated")

        message_map = {
                       win32con.WM_COMMAND: self._on_command,
                       win32con.WM_DESTROY: self._on_destroy,
                       win32con.WM_USER+20: self._on_taskbar_notify,
                       self.WM_TASKBAR_CREATED: self._update_taskbar_icon,
                       }
        # Register the Window class.
        self.wc = win32gui.WNDCLASS()
        self.wc.hInstance = win32api.GetModuleHandle(None)
        self.wc.lpszClassName = "G/On Client Gui"
        self.wc.style = win32con.CS_VREDRAW | win32con.CS_HREDRAW
        self.wc.hCursor = win32api.LoadCursor(0, win32con.IDC_ARROW )
        self.wc.hbrBackground = win32con.COLOR_WINDOW
        self.wc.lpfnWndProc = message_map
        self.classAtom = win32gui.RegisterClass(self.wc)
        # Create the Window.
        style = win32con.WS_OVERLAPPED | win32con.WS_SYSMENU

        print "MfcCommonView.init - CreateWindow:", self.classAtom, self.wc.hInstance

        self.hwnd = win32gui.CreateWindow( self.classAtom, "G/On Client Gui", style, \
                                           0, 0, win32con.CW_USEDEFAULT, win32con.CW_USEDEFAULT, \
                                           0, 0, self.wc.hInstance, None)
        self.handles['windowhandler'] = self.hwnd

        print "MfcCommonView.init - Shell_Notify:", self.hwnd, self.icon
        try:
            win32gui.Shell_NotifyIcon(win32gui.NIM_ADD,
                                      (self.hwnd, MfcCommonView.ID_TRAY_ICON,
                                       win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP,
                                       win32con.WM_USER+20,
                                       self.icon,
                                       self.tooltip))
        except win32gui.error:
            print "MfcCommonView.init failed to add the taskbar icon"


    def display(self, views=None):
        """ Starts the gui with all connected elements. """
        win32gui.PumpMessages()

    def _update_taskbar_icon(self, *_):
        """ This is called if explorer restarted for some reason.

            @since: 5.4
        """
        self.visible = False
        self.update()

    def update(self):
        if self.visible != self.model.visible:
            if self.model.visible:
                try:
                    win32gui.Shell_NotifyIcon(win32gui.NIM_ADD,
                                              (self.hwnd, MfcCommonView.ID_TRAY_ICON,
                                               win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP,
                                               win32con.WM_USER+20,
                                               self.icon,
                                               self.tooltip))
                except win32gui.error:
                    print "MfcCommonView.update failed to add the taskbar icon"
            else:
                try:
                    win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, (self.hwnd, MfcCommonView.ID_TRAY_ICON))
                except win32gui.error:
                    print "MfcCommonView.update failed to remove the taskbar icon"

            win32gui.UpdateWindow(self.hwnd)
            self.visible = self.model.visible

    def _on_destroy(self, *_): #@UnusedVariable
        """ When the menu starter is to be destroyed.

            The destroy method may be called more than once.
            Because of the WM_DESTROY that is needed for the
            case where other exit buttons are hit. So we need
            to test to see if things exist at all. Hence the
            try/excepts thing.
        """
        if not self.closing:
            self.closing = True
            try:
                if self.visible:
                    try:
                        win32gui.Shell_NotifyIcon(win32gui.NIM_DELETE, (self.hwnd, MfcCommonView.ID_TRAY_ICON))
                    except win32gui.error:
                        print "MfcCommonView._on_destroy failed to remove the taskbar icon"
                win32gui.DestroyWindow(self.hwnd)
                win32gui.UnregisterClass(self.classAtom, self.wc.hInstance)
                win32api.PostQuitMessage(0)
            except win32gui.error:
                pass

    def _on_taskbar_notify(self, hwnd, msg, wparam, lparam): #@UnusedVariable
        """Handle user actions on the menustarter.

            When the user left or right clicks the menu starter
            some action is required. For instance a left click
            results in the application menu to be displayed. A
            right click may result in a preferences menu to be
            displayed.
        """
        try:
            if lparam==win32con.WM_LBUTTONUP: # Left-click
                assert 'appmenu' in self.handles, "Application menu handle is not set"
                self.handles['appmenu'].display()
                self.openmenu = 'app'
            elif lparam == win32con.WM_RBUTTONUP: # Right-click
                assert 'sysmenu' in self.handles, "System menu handle is not set"
                self.handles['sysmenu'].display()
                self.openmenu = 'sys'
        except AssertionError:
            print "Task-bar Notify Error: A menu handle has not been set"

    def _on_command(self, *args):
        try:
            if self.openmenu == 'sys' and 'sysmenu' in self.handles:
                self.handles['sysmenu'].on_command(*args)
            elif 'appmenu' in self.handles:
                self.handles['appmenu'].on_command(*args)
        except KeyError:
            print "MfcCommonView._on_command(): Could not find key in handles."

        self.openmenu = None

    def destroy(self):
        try:
            win32api.PostMessage(self.hwnd, win32con.WM_DESTROY)
        except win32gui.error:
            try:
                self._on_destroy()
            except win32gui.error:
                pass

#
# Move this to a test area when appropriate.
#
if __name__ == '__main__':
    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.common import CommonModel

    commonmodel = CommonModel()
    handles = None
    configuration = ClientGatewayOptions()
    configuration.dictionary = lib.dictionary.Dictionary()
    configuration.gui_image_path = '..'

    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    commonview.display()
