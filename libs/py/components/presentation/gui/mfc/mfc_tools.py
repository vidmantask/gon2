import win32api 
import win32con
import sys
import os

if "--noxp" in sys.argv: 
    import win32gui #@UnusedImport
else: 
    import winxpgui as win32gui #@Reimport

GIRI_GUI_DEFAULT_BITMAP_IMAGE = "giritech.bmp"
GIRI_GUI_DEFAULT_ICO_IMAGE = "giritech.ico"

def locate_image_path(path=''):
    """ Locate a path for images, searching through relevant options. """
    temp_path = os.path.abspath(os.path.join(path, 'mfc', 'images'))
    if os.path.exists(temp_path):
        return temp_path
    return path

def locate_image(path='', filename=''):
    """ Find an image to be displayed in an MFC environment. """
    location = os.path.abspath(os.path.join(locate_image_path(path), filename))
    hinst =  win32api.GetModuleHandle(None)
    
    try:
        assert os.path.isfile(location), "File doesn't exist"
        image = win32gui.LoadImage(hinst, location, win32con.IMAGE_BITMAP, 0, 0, 
                                   win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE)
        return image
    except AssertionError:
        print "File doesn't exist:", location, ", loading default giritech icon"
        location = os.path.abspath(os.path.join(locate_image_path(path), GIRI_GUI_DEFAULT_BITMAP_IMAGE))
        image = win32gui.LoadImage(hinst, location, win32con.IMAGE_BITMAP, 0, 0, win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE)
        return image

def locate_icon(path='', filename=''):
    """ Find an icon to be displayed in an MFC environment.
        
        There needs to be two kinds of icons. the ico
        file for sys tray and btmaps for menu items.
        If the icon requested in the arguments does not
        exist then some standard system icon should be
        used instead.
    """
    hinst =  win32api.GetModuleHandle(None)
    location = os.path.abspath(os.path.join(locate_image_path(path), filename))
    
    try:
        assert os.path.isfile(location), "File doesn't exist - check the images path in your client configuration"
        
        if location.endswith('ico'):
            icon = win32gui.LoadImage(hinst, location, win32con.IMAGE_ICON, 0, 0, 
                                      win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE)
        elif location.endswith('bmp'):
            icon = win32gui.LoadImage(hinst, location, win32con.IMAGE_BITMAP, 0, 0, 
                                      win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE)
        else:
            print "Unsupported file type:", location, ", ico and bmp files are supported"
            return None
        return icon
    except AssertionError:
        if location.endswith('ico'):
            print "File doesn't exist:", location, ", loading default system icon"
            icon = win32gui.LoadIcon(0, win32con.IDI_ASTERISK)
            return icon
        elif location.endswith('bmp'):
            print "File doesn't exist:", location, ", loading default giritech icon"
            location = os.path.abspath(os.path.join(locate_image_path(path), GIRI_GUI_DEFAULT_BITMAP_IMAGE))
            icon = win32gui.LoadImage(hinst, location, win32con.IMAGE_BITMAP, 0, 0, win32con.LR_LOADFROMFILE | win32con.LR_DEFAULTSIZE)
            return icon
        else:
            print "Unsupported file type:", location, ", ico and bmp files are supported"
            return None
