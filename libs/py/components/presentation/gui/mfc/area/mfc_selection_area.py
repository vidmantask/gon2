"""

Generic MFC based selection area.

"""
import win32gui
import win32con
import win32api # Imports ctypes
import commctrl
import win32gui_struct
import ctypes.wintypes
import components.presentation.gui.mfc.structure.mfc_list_structure as mfc_list_structure

from components.presentation.gui.mfc.mfc_tools import locate_icon
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

class MfcSelectionArea(MfcBaseArea):

    ID_SELECTION_TABLE = 901
    ID_SEPARATOR = 902
    
    def __init__(self, hwnd, hinst, configuration):
        MfcBaseArea.__init__(self, hwnd, hinst, configuration) 

        self.imageindex = []

        # Currently visible model.
        self.current_selection_method = 0 # ID_SELECTION_MULTIPLE
        self.current_selection_list = []
        self.highlighted = -1
        self.current_highlighted_by_id = -1
        self.mode = ''
        
        # Area view settings
        self.scrollbar_width = win32api.GetSystemMetrics(win32con.SM_CYVSCROLL)
        self.tilesettings = mfc_list_structure.LVTILEVIEWINFO()

        # Create widgets...
        self.list, self.imagelist = self.create_tile_list_widget(MfcSelectionArea.ID_SELECTION_TABLE)
        self.widgets.append(self.list) 

        self.separator = self.create_separator_widget(MfcSelectionArea.ID_SEPARATOR) 
        self.widgets.append(self.separator)

    def create_tile_list_widget(self, widget_id):
        """ Create a widget for displaying items in a tiled list. """
        colformat = commctrl.LVCF_FMT | commctrl.LVCF_WIDTH | commctrl.LVCF_TEXT | commctrl.LVCF_SUBITEM
        colstyle = win32con.WS_CHILD | win32con.WS_VSCROLL | win32con.WS_TABSTOP | \
                   commctrl.LVS_SINGLESEL | commctrl.LVS_SHOWSELALWAYS | win32con.WS_VISIBLE
        colexstyle = commctrl.LVS_EX_CHECKBOXES | commctrl.LVS_EX_ONECLICKACTIVATE  
        # Setup the list view with check-boxes.
        widget = win32gui.CreateWindow(commctrl.WC_LISTVIEW, None, colstyle,
                                          0, 0, 0, 0, 
                                          self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, commctrl.LVM_SETEXTENDEDLISTVIEWSTYLE, 0, colexstyle)
        # Setup list view column headers (reversed order).
        for index in range(0, 2):
            _column, _ = win32gui_struct.PackLVCOLUMN(colformat, 0, "", index)
            win32gui.SendMessage(widget, commctrl.LVM_INSERTCOLUMN, commctrl.LVCFMT_LEFT, _column)
        # Set the list view style to tiled.
        win32gui.SendMessage(widget, mfc_list_structure.LVM_SETVIEW, mfc_list_structure.LV_VIEW_TILE, 0)
        # Define settings for the tile view.
        self.tilesettings.cbSize = ctypes.sizeof(self.tilesettings) # Structure size
        self.tilesettings.dwMask = mfc_list_structure.LVTVIM_COLUMNS | mfc_list_structure.LVTVIM_TILESIZE #| LVTVIM_LABELMARGIN
        self.tilesettings.dwFlags = mfc_list_structure.LVTVIF_FIXEDWIDTH    
        self.tilesettings.cLines = 1 # Extra columns displayed as rows of text
        self.tilesettings.sizeTile.cx = 300 - 2*self.scrollbar_width
        #self.tilesettings.rcLabelMargin = (10,10,10,10)
        ctypes.windll.user32.SendMessageA(widget, mfc_list_structure.LVM_SETTILEVIEWINFO, 0, ctypes.byref(self.tilesettings))
        # Set the theme for the list to use XP explorer style ( nice! :-) )
        ctypes.windll.UxTheme.SetWindowTheme(widget, u"Explorer", 0)
        # Create an image list for tile icons.
        imagelist = win32gui.ImageList_Create(32,
                                              32,
                                              commctrl.ILC_COLOR32 | commctrl.ILC_MASK,
                                              10, # initial size
                                              100) # Expands to size
        win32gui.SendMessage(widget, commctrl.LVM_SETIMAGELIST, commctrl.LVSIL_NORMAL, imagelist)
        return (widget, imagelist)

    def resize(self, left=0, top=0, width=300, height=240):
        """ Resize and locate relevant parts in this area. """
        if not self.visible:
            left = top = width = height = 0
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcSelectionArea.ID_SELECTION_TABLE), 
                              0, 
                              left, 
                              top, 
                              width, 
                              height-2, 
                              win32con.SWP_NOZORDER)
        # Set the tile  width of elements in the list.
        self._resize_strings()
        self.tilesettings.sizeTile.cx = width - 2 * self.scrollbar_width
        ctypes.windll.user32.SendMessageA(self.list, mfc_list_structure.LVM_SETTILEVIEWINFO, 0, ctypes.byref(self.tilesettings))
        win32gui.InvalidateRect(self.list, None, False)        
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcSelectionArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcSelectionArea.ID_SELECTION_TABLE), 
                              -1,
                              top + height - 2,
                              width + 2, 
                              2, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.separator, None, False)
    
    def on_click(self, hwnd, _wm_id, _message, controller, lparam):
        """ Handle on click events needed for supporting Win XP.
        
            This should ONLY be used on Win XP.
            
            Apparently the check-box is seen as a part of the icon on XP.
            We know that the icon is 32x32. So clicking the part of the "icon"
            that is not an image should toggle the selection.    
            
        """
        _, _, _, _, _, _, _, _, (pt_x, pt_y), _ = win32gui_struct.UnpackLVNOTIFY(lparam)
        hit, _ = win32gui_struct.PackLVHITTEST((pt_x, pt_y))
        win32gui.SendMessage(self.list, commctrl.LVM_HITTEST, 0,  hit)
        (x, _), flags, item, _ = win32gui_struct.UnpackLVHITTEST(hit) 
        rect = ctypes.wintypes.RECT()
        rect.left = commctrl.LVIR_ICON
        win32gui.SendMessage(self.list, commctrl.LVM_GETITEMRECT, 0, rect)
        if flags == commctrl.LVHT_ONITEMICON and x < rect.right - 32 and x > rect.left:
            index = item
            _checkstate = win32gui.SendMessage(hwnd, commctrl.LVM_GETITEMSTATE, index, commctrl.LVIS_STATEIMAGEMASK)
            _checked = (_checkstate == commctrl.INDEXTOSTATEIMAGEMASK(2)) 

            # Set the check-box state, because it is not done automatically on XP
            if not _checked: # If not checked -> set to checked...
                selectedstate = commctrl.INDEXTOSTATEIMAGEMASK(2)
            else:
                selectedstate = commctrl.INDEXTOSTATEIMAGEMASK(1)
            _item, _ = win32gui_struct.PackLVITEM(item=index, state=selectedstate, 
                                                      stateMask=commctrl.LVIS_STATEIMAGEMASK, 
                                                      text=None)
            win32gui.SendMessage(self.list, commctrl.LVM_SETITEMSTATE, index, _item)
            controller.set_selection_via_index(index, not _checked)
    
    def on_change(self, hwnd, _wm_id, _message, controller):
        """ Change list element selection state. 
            
            Clicking a checkbox in tile view mode only selects the item; the state does not change.
            
            Return an index indicating which row is selected.
        """
        _itemcount = win32gui.SendMessage(hwnd, commctrl.LVM_GETITEMCOUNT)
    
        for index in range(0, _itemcount):
            # See if the change was a highlight change
            if win32gui.SendMessage(hwnd, commctrl.LVM_GETITEMSTATE, index, commctrl.LVIS_SELECTED):
                controller.set_selection_user_highlighted(index)
            # See if the change was a check-box change.
            _checkstate = win32gui.SendMessage(hwnd, commctrl.LVM_GETITEMSTATE, index, commctrl.LVIS_STATEIMAGEMASK)
            _checked = (_checkstate == commctrl.INDEXTOSTATEIMAGEMASK(2)) 
            if len(self.current_selection_list) > index:
                if _checked != self.current_selection_list[index]['selected']:
                    controller.set_selection_via_index(index, _checked)
        return self.highlighted
    
    def _add_package(self):
        """ Add an empty row to the list. """
        _index = win32gui.SendMessage(self.list, commctrl.LVM_GETITEMCOUNT)
        _item, _ = win32gui_struct.PackLVITEM(item=_index, text="")
        win32gui.SendMessage(self.list, commctrl.LVM_INSERTITEM, 0, _item)
        # Settings for each tile in the list.
        _tileinfo = mfc_list_structure.LVTILEINFO()
        _tileinfo.cbSize = ctypes.sizeof(_tileinfo)                        # Structure size
        _tileinfo.cColumns = 5                                                 # Max columns to show info from
        _tileinfo.iItem = _index                                           # Which tile
        _tileinfo.puColumns = ctypes.pointer(mfc_list_structure.UINT_ARRAY_5(1))                  # Which columns to show in the tile
        ctypes.windll.user32.SendMessageA(self.list, mfc_list_structure.LVM_SETTILEINFO, 0, ctypes.byref(_tileinfo))

    def _remove_package(self, index):
        """ Remove the last package in the list. 
        
            Use index here to make sure that we 
            do not remove more than needed. 
            Filters multiple calls to update that 
            comes out of the blue (really).
        """
        _maxindex = win32gui.SendMessage(self.list, commctrl.LVM_GETITEMCOUNT)
        if index <= _maxindex:
            win32gui.SendMessage(self.list, commctrl.LVM_DELETEITEM, index, 0)

    def update(self, model, controller):
        """ Update relevant parts in the package selection area. """
        _changeselect = _changename = _changedetails = _changeicon = _changemethod = False
        # Make sure the number of rows are correct.
        if len(model.selection_list) > len(self.current_selection_list):
            for index in range(len(self.current_selection_list), len(model.selection_list)):
                self._add_package()
        if len(model.selection_list) < len(self.current_selection_list):
            for index in range(len(self.current_selection_list), len(model.selection_list), -1):
                self._remove_package(index-1)
            if model.selection_user_highlighted >= len(model.selection_list):
                controller.set_selection_user_highlighted(-1)
        # Make sure the content of the rows is correct.
        for index, package in enumerate(model.selection_list):
            # For existing elements - only change what has changed.
            if len(self.current_selection_list) > index: 
                _changeselect = package['selected'] != self.current_selection_list[index]['selected']
                _changename = package['name'] != self.current_selection_list[index]['name']
                _changedetails = package['details'] != self.current_selection_list[index]['details']
                _changeicon = package['icon'] != self.current_selection_list[index]['icon']
            # For new elements - add everything.
            else:
                _changeselect = _changename = _changedetails = _changeicon = True
            # Set the selection in the check box.
            if _changeselect:
                if model.selection_list[index]['selected']:
                    selectedstate = commctrl.INDEXTOSTATEIMAGEMASK(2)
                else:
                    selectedstate = commctrl.INDEXTOSTATEIMAGEMASK(1)
                _item, _ = win32gui_struct.PackLVITEM(item=index, state=selectedstate, 
                                                          stateMask=commctrl.LVIS_STATEIMAGEMASK, 
                                                          text=None)
                win32gui.SendMessage(self.list, commctrl.LVM_SETITEMSTATE, index, _item)
            # Set the name text
            if _changename:
                rect = ctypes.wintypes.RECT()
                rect.left = commctrl.LVIR_BOUNDS
                win32gui.SendMessage(self.list, commctrl.LVM_GETITEMRECT, 0, rect)
                if (rect.right - rect.left) < win32gui.SendMessage(self.list, commctrl.LVM_GETSTRINGWIDTH, 0, package['name']):
                    r_string = self._cut_string_to_pixel_size(package['name'], rect.right - rect.left)
                    #r_string = package['name'] # remove this again
                else:
                    r_string = package['name']
                _item, _ = win32gui_struct.PackLVITEM(item=index, subItem=0, text=r_string)
                win32gui.SendMessage(self.list, commctrl.LVM_SETITEM, 0, _item) 
            if _changedetails:
                _item, _ = win32gui_struct.PackLVITEM(item=index, subItem=1, text=package['details'])
                win32gui.SendMessage(self.list, commctrl.LVM_SETITEM, 0, _item)

            # Make sure images are located in the image list.
            if _changeicon:
                if package['icon'] == None:
                    _item, _ = win32gui_struct.PackLVITEM(item=index, image=-1)
                else:
                    if not package['icon'] in self.imageindex:
                        win32gui.ImageList_Add(self.imagelist, 
                                               locate_icon(path=self.image_path, filename=package['icon']),
                                               None)
                        self.imageindex.append(package['icon'])
                    _item, _ = win32gui_struct.PackLVITEM(item=index, image=self.imageindex.index(package['icon']))
                win32gui.SendMessage(self.list, commctrl.LVM_SETITEM, 0, _item)
        # Set the current selection method single or multiple.
        if self.current_selection_method != model.selection_method:
            _changemethod = True
            if model.selection_method == 1:
                win32gui.SendMessage(self.list, commctrl.LVM_SETEXTENDEDLISTVIEWSTYLE, 0, commctrl.LVS_EX_ONECLICKACTIVATE)
            else:
                win32gui.SendMessage(self.list, commctrl.LVM_SETEXTENDEDLISTVIEWSTYLE, 0, commctrl.LVS_EX_CHECKBOXES | commctrl.LVS_EX_ONECLICKACTIVATE)
            self.current_selection_method = model.selection_method
        # Set which element is highlighted - due to change in the model.
        if self.current_highlighted_by_id != model.selection_highlighted_id:
            
            for index, package in enumerate(model.selection_list):
                location = 0
                if package['id'] == model.selection_highlighted_id:
                    location = index
                    break

            _lv_item, _ = win32gui_struct.PackLVITEM(item=location, state=commctrl.LVIS_SELECTED, stateMask=commctrl.LVIS_SELECTED)
            win32gui.SendMessage(self.list, commctrl.LVM_SETITEMSTATE, location, _lv_item)
            win32gui.SendMessage(self.list, commctrl.LVM_SETHOTITEM, location)
            self.current_highlighted_by_id = model.selection_highlighted_id
        if _changeselect == True or _changename == True or _changedetails == True or _changeicon == True or _changemethod == True:
            win32gui.InvalidateRect(self.list, None, True)
            win32gui.UpdateWindow(self.list)
        
        # Now the two lists should match.
        self.current_selection_list = model.selection_list
    
    def _resize_strings(self):
        """ Resize all current strings to the width of the table.
        
            @since: 5.5
         """
        for index, package in enumerate(self.current_selection_list):
            rect = ctypes.wintypes.RECT()
            rect.left = commctrl.LVIR_LABEL
            win32gui.SendMessage(self.list, commctrl.LVM_GETITEMRECT, 0, rect)
            if (rect.right - rect.left) <= win32gui.SendMessage(self.list, commctrl.LVM_GETSTRINGWIDTH, 0, package['name']):
                r_string = self._cut_string_to_pixel_size(package['name'], rect.right - rect.left - 10)
            else:
                r_string = package['name']
            
            _item, _ = win32gui_struct.PackLVITEM(item=index, subItem=0, text=r_string)
            win32gui.SendMessage(self.list, commctrl.LVM_SETITEM, 0, _item) 
    
    def _cut_string_to_pixel_size(self, istring, pixel_size):
        """ Internal method to fit string to a specific pixel size. 
        
            @since: 5.4
        """
        string_too_big = True
        ellipsis = '...'
        # Make sure that the method terminates...
        if pixel_size < 0 or istring == '':
            return istring
        # Cut the string if it is too big...
        while string_too_big:
            istring = istring[0:len(istring)-1]
            newwidth = win32gui.SendMessage(self.list, commctrl.LVM_GETSTRINGWIDTH, 0, istring + ellipsis)
            if (newwidth < pixel_size):
                string_too_big = False
        return istring + ellipsis
