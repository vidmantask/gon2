"""

Generic MFC based HTML text viewing area.

"""

import win32gui
import win32con
import os
import tempfile
import ctypes

from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

# Load proper DLL and initialize Active Template Library Window.
import sys
#if not hasattr(sys, "frozen"):
#   from comtypes.client import GetModule
#   GetModule("shdocvw.dll")

import comtypes.gen.SHDocVw
ctypes.windll.atl.AtlAxWinInit()


class MfcHtmlArea(MfcBaseArea):

    ID_MESSAGE_TEXTFIELD = 401
    ID_SEPARATOR = 402

    def __init__(self, hwnd, hinst, bottom_separator=True):
        MfcBaseArea.__init__(self, hwnd, hinst)

        self.done_init = False

        # Currently visible model.
        self.current_text = ""

        # Extras - show a bottom separator
        self.bottom_separator = bottom_separator

        self.atlAxWinHwnd = win32gui.CreateWindow('AtlAxWin', "{EAB22AC1-30C1-11CF-A7EB-0000C05BAE0B}",
                                                  win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.WS_OVERLAPPED,
                                                  win32con.CW_USEDEFAULT,
                                                  win32con.CW_USEDEFAULT,
                                                  win32con.CW_USEDEFAULT,
                                                  win32con.CW_USEDEFAULT,
                                                  self.hwnd, MfcHtmlArea.ID_MESSAGE_TEXTFIELD, self.hinst, None)
        win32gui.SendMessage(self.atlAxWinHwnd, win32con.WM_SETFONT, self.standard_font, 0)
        self.browserUnk = comtypes.POINTER(comtypes.IUnknown)()
        ctypes.windll.atl.AtlAxGetControl(self.atlAxWinHwnd, comtypes.automation.byref(self.browserUnk))
        self.browser = self.browserUnk.QueryInterface(comtypes.gen.SHDocVw.IWebBrowser2) # pylint: disable=E1101
        ctypes.windll.atl.AtlAxAttachControl(self.browser, self.atlAxWinHwnd, 0)
        # Load a nice red background into the browser area.
        tmp_fd, self.url = tempfile.mkstemp(suffix='.html')
        tmp_file = os.fdopen(tmp_fd, 'w')
        tmp_file.write("<HTML><HEAD></HEAD><BODY style='overflow:auto; background:#740000;'></BODY></HTML>")
        tmp_file.close()
        self.browser.Navigate2(self.url, 0, None, None, None)
        # Handle separator
        if self.bottom_separator:
            self.separator = self.create_separator_widget(MfcHtmlArea.ID_SEPARATOR)
            self.widgets.append(self.separator)

        self.done_init = True

    def update(self, model):

        if not self.done_init:
            print "Init not done - trying to run MFC Html area update."
            return

        if self.current_text != model.text:
            #Create temporary HTML file from HTML template.
            tmp_fd, self.url = tempfile.mkstemp(suffix='.html')
            tmp_file = os.fdopen(tmp_fd, 'wb')
            tmp_file.write(model.html_base.substitute(g_html_text=model.text.encode('utf-8', 'ignore')))
            tmp_file.close()
            # Temporarily hide the window to avoid a clicking sound.
            win32gui.SetWindowPos(self.atlAxWinHwnd, None, 0, 0, 0, 0, win32con.SWP_HIDEWINDOW | win32con.SWP_NOREDRAW |
                         win32con.SWP_NOREPOSITION | win32con.SWP_NOMOVE | win32con.SWP_NOSIZE | win32con.SWP_NOACTIVATE)
            # Navigate to the temporary HTML file.
            try:
                ### ---  Maybe this can be used later. --- ###
                #self.doc = self.browser.Document
                #self.doc.write(model.text)
                #self.doc.close()
                ### ---  Maybe this can be used later (end). --- ###
                variant = comtypes.automation.byref(comtypes.automation.VARIANT())
                self.browser.Navigate2(self.url, 0, variant, variant, variant)
            except comtypes.COMError, args:
                print "Caught COM error in MFC HTML area update - ignoring:", args[1]
            except:
                print "Caught unexpected exception in MFC HTML area update - ignoring", sys.exc_info()

            # Show the window again.
            win32gui.SetWindowPos(self.atlAxWinHwnd, None, 0, 0, 0, 0, win32con.SWP_SHOWWINDOW |
                                  win32con.SWP_NOREPOSITION | win32con.SWP_NOMOVE | win32con.SWP_NOSIZE | win32con.SWP_NOACTIVATE)
            win32gui.ShowWindow(self.atlAxWinHwnd, win32con.SW_SHOW)
            self.current_text = model.text

        win32gui.RedrawWindow(self.atlAxWinHwnd, None, None, win32con.RDW_ALLCHILDREN | win32con.RDW_UPDATENOW)

    def resize(self, left, top, width, height):
        """ Resize all elements to fit the new settings. """
        if self.bottom_separator:
            delta = 2
        else:
            delta = 0

        win32gui.SetWindowPos(self.atlAxWinHwnd,
                              0,
                              top,
                              left,
                              width,
                              height - delta,
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.atlAxWinHwnd, None, False)
        win32gui.UpdateWindow(self.atlAxWinHwnd)

        if self.bottom_separator:
            win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcHtmlArea.ID_SEPARATOR),
                                  0,
                                  -1,
                                  top + height - 2,
                                  width + 2,
                                  1,
                                  0)

    def destroy(self):
        """ Clean up """
        try:
            win32gui.UnregisterClass("AtlAxWin", self.hinst)
        except win32gui.error:
            pass
        #self.browser.Release()
        #self.browser = None
        #self.browserUnk.Release()
        #self.browserUnk = None
