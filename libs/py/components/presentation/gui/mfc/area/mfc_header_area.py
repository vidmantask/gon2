"""

Generic MFC based banner area.

"""
import win32gui
import win32con

from components.presentation.gui.mfc.mfc_tools import locate_image
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

GIRI_GUI_INSTALL_BANNER = "g_install_banner.bmp"
GIRI_GUI_UPDATE_BANNER = "g_update_banner.bmp"
GIRI_GUI_REMOVE_BANNER = "g_remove_banner.bmp"
GIRI_GUI_ENROLL_BANNER = "g_enroll_banner.bmp"
GIRI_GUI_UNINSTALL_BANNER = "g_uninstall_banner.bmp"
GIRI_GUI_LOGIN_BANNER = "g_login_banner.bmp"

class MfcHeaderArea(MfcBaseArea):
    
    ID_HEADER_IMAGE = 301
    ID_SEPARATOR_BELOW_HEADER = 302
    
    def __init__(self, hwnd, hinst, configuration):
        """ Create a header for the update window. """
        MfcBaseArea.__init__(self, hwnd, hinst, configuration)
        # Currently visible model.
        self.current_banner_id = None
        self.current_banner_image = None
        # Sizes
        self.height = 0
        self.width = 0
        # Create widgets...
        self.header = self.create_image_widget(MfcHeaderArea.ID_HEADER_IMAGE)
                 
        self.widgets.append(self.header)
        
        self.separator = self.create_separator_widget(MfcHeaderArea.ID_SEPARATOR_BELOW_HEADER)
        self.widgets.append(self.separator)

    def create_image_widget(self, widget_id):
        """ Create a widget for displaying an image. """
        return win32gui.CreateWindow('STATIC', "",
                                     win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.SS_BITMAP,
                                     0, 0, 0, 0,
                                     self.hwnd, widget_id, self.hinst, None)

    def update(self, model):
        """ Update relevant parts of the header area. """
        if self.current_banner_id != model.banner_id:
            # Locate banner depending on id and tools settings.
            if model.banner_id == 0:   # ID_BANNER_INSTALL:
                self.current_banner_image = GIRI_GUI_INSTALL_BANNER
            elif model.banner_id == 1: # ID_BANNER_UPDATE
                self.current_banner_image = GIRI_GUI_UPDATE_BANNER
            elif model.banner_id == 2: # ID_BANNER_REMOVE 
                self.current_banner_image = GIRI_GUI_REMOVE_BANNER
            elif model.banner_id == 3: # ID_BANNER_ENROLL
                self.current_banner_image = GIRI_GUI_ENROLL_BANNER
            elif model.banner_id == 4: # ID_BANNER_UNINSTALL
                self.current_banner_image = GIRI_GUI_UNINSTALL_BANNER
            elif model.banner_id == 5: # ID_BANNER_LOGIN
                self.current_banner_image = GIRI_GUI_LOGIN_BANNER
                
            if self.current_banner_image is not None:
                # Update the banner image according to model settings.
                win32gui.SendMessage(win32gui.GetDlgItem(self.hwnd, MfcHeaderArea.ID_HEADER_IMAGE), 
                                     win32con.STM_SETIMAGE, 
                                     win32con.IMAGE_BITMAP, 
                                     locate_image(path=self.image_path, filename=self.current_banner_image))
                        
                
                self.current_banner_id = model.banner_id
                self.resize()

    def resize(self, left=0, top=0, width=1500, height=80):
        """ Resize and relocate parts of the header area. """
        self.set_sizes(left, top, width, height)
        
        win32gui.SetWindowPos(self.header, 
                              0,                # on top of
                              0,                # x
                              0,                # y
                              width,            # width 
                              height,           # height
                              win32con.SWP_NOZORDER) # flags
        
        
         
        win32gui.InvalidateRect(self.header, None, False)
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcHeaderArea.ID_SEPARATOR_BELOW_HEADER), 
                              win32gui.GetDlgItem(self.hwnd, MfcHeaderArea.ID_HEADER_IMAGE), 
                              -1, 
                              height - 2, 
                              width + 2, 
                              2,
                              0)

