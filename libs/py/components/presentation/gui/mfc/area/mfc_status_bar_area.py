'''
MFC based status bar for the bottom of the window.
'''

import os
import win32gui
import win32con
import commctrl
import win32gui_struct

from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea
from components.presentation.gui.mfc.mfc_tools import locate_image_path
from components.presentation.main import MainController

class MfcStatusBarArea(MfcBaseArea):
    
    ID_STATUS_BAR = 1001
    
    def __init__(self, hwnd, hinst, configuration):
        MfcBaseArea.__init__(self, hwnd, hinst, configuration)
        #self.image_path = image_path
        self.statusbarheight = 23
        self.connectioninfowidth = 30
        self.serviceinfowidth = 25
        
        # Currently visible model
        self.current_statusbar_text = ""
        self.current_statusbar_connection_status = MainController.ID_STATUS_BAR_STATUS_UNCONNECTED
        self.current_statusbar_service_count = 0
        self.current_statusbar_services = []
        
        self.statusbar = win32gui.CreateWindowEx( 0, commctrl.STATUSCLASSNAME, None, 
                                 commctrl.SB_SIMPLE | win32con.WS_CHILD | win32con.WS_VISIBLE | commctrl.SBARS_SIZEGRIP | commctrl.SBT_TOOLTIPS, 
                                 0, 0, 0, 0, 
                                 self.hwnd, MfcStatusBarArea.ID_STATUS_BAR, self.hinst, None)

    def _set_service_areas(self):
        """ Set the number of services. 
            
            The rightmost column is used for connection info
            and should therefore always be present.
        """
        _struct_pos = range(self.width-self.connectioninfowidth-(self.current_statusbar_service_count*self.serviceinfowidth), 
                            self.width-self.connectioninfowidth, self.serviceinfowidth)
        _struct_pos.append(self.width-self.connectioninfowidth)
        _struct_pos.append(self.width)
        
        positions = win32gui_struct.struct.pack((self.current_statusbar_service_count*'i') + 'ii', 
                                                *tuple(_struct_pos))
        win32gui.SendMessage(self.statusbar, commctrl.SB_SETPARTS, self.current_statusbar_service_count+2, positions)
        win32gui.SendMessage(self.statusbar, win32con.WM_SIZE, 0, 0)
    
    def resize(self, left=0, top=0, width=375, height=300):
        """ Resize relevant items in this area. """
        self.set_sizes(left, top, width, height)
        win32gui.SetWindowPos(self.statusbar, 0, 
                              0, 
                              height-self.statusbarheight, 
                              width, 
                              0,
                              win32con.SWP_NOZORDER)
        self._set_service_areas() 

    def update(self, model):
        # Set text in the status bar...
        if self.current_statusbar_text != model.statusbartext:
            win32gui.SendMessage(self.statusbar, commctrl.SB_SETTEXT, 0, model.statusbartext)
            self.current_statusbar_text = model.statusbartext
        # Change the service icons in the status bar.
        if self.current_statusbar_service_count != len(model.statusbarservices):
            self.current_statusbar_service_count = len(model.statusbarservices)
            self.current_statusbar_services = model.statusbarservices
            self._set_service_areas()
            
            index = 1
            for service in model.statusbarservices:
                if service[2] != None:  # Set icon for the service...
                    _icofile = service[2]
                else:                   # ... or standard Giritech icon...
                    _icofile = "giritech.ico"
                location = os.path.abspath(os.path.join(locate_image_path(self.image_path), _icofile))
                _icon = win32gui.LoadImage(self.hinst, location, win32con.IMAGE_ICON, 16, 16, win32con.LR_LOADFROMFILE)
                win32gui.SendMessage(self.statusbar, commctrl.SB_SETICON, index, _icon)
                # Set tool tip text
                # Tool-tips are only working when there is only an icon or if text has been truncated. 
                win32gui.SendMessage(self.statusbar, commctrl.SB_SETTIPTEXT, index, service[1])
                index = index + 1
        # Set the connection status icon.
        if self.current_statusbar_connection_status != model.statusbarconnectionstatus:
            if model.statusbarconnectionstatus == MainController.ID_STATUS_BAR_STATUS_CONNECTED:
                _icofile = "greendot.ico"
                _text = "Connected to the G/On Server"
            elif model.statusbarconnectionstatus == MainController.ID_STATUS_BAR_STATUS_CONNECTING:
                _icofile = None
                _text = "Attempting to connect to the G/On Server"
            elif model.statusbarconnectionstatus == MainController.ID_STATUS_BAR_STATUS_UNCONNECTED:
                _icofile = None
                _text = "Not connected to the G/On Server"

            if _icofile != None:
                location = os.path.abspath(os.path.join(locate_image_path(self.image_path), _icofile))
                _icon = win32gui.LoadImage(self.hinst, location, win32con.IMAGE_ICON, 16, 16, win32con.LR_LOADFROMFILE)
                win32gui.SendMessage(self.statusbar, commctrl.SB_SETICON, 
                                     self.current_statusbar_service_count+1, 
                                     _icon)
            else:
                win32gui.SendMessage(self.statusbar, commctrl.SB_SETICON, 
                                     self.current_statusbar_service_count+1,
                                     None)
            win32gui.SendMessage(self.statusbar, commctrl.SB_SETTIPTEXT, self.current_statusbar_service_count+1, _text)
            self.current_statusbar_connection_status = model.statusbarconnectionstatus
