"""

Generic MFC based menu area.

"""

import win32gui
import win32con

from components.presentation.gui.mfc.mfc_tools import locate_icon, GIRI_GUI_DEFAULT_BITMAP_IMAGE
from components.presentation.menu import MenuFolder


class MfcMenuArea(object):
    def __init__(self, hwnd, _, configuration, modeltopfolder):
        
        self.configuration = configuration
        self.launchidmap = []
        
        #MenuFolder(-1, -1, "Test", None)
        
        #
        # We would like to avoid having the modeltopfolder added to __init__ !!!!
        #
        
        self.topfolder = MfcMenuFolderView(None, modeltopfolder, self.configuration.gui_image_path)
        #self.topfolder = MfcMenuFolderView(None, MenuFolder(-1, -1, "Test", None), self.configuration.gui_image_path)
    
    def update(self, model):
        self._recursive_update(model.topfolder, self.topfolder)
    
    def _recursive_update(self, foldermodellocation, folderviewlocation):
        """ Recursively update the view of menu folders and items.
    
            Recursively controls consistency between model and 
            view. Anything that does not exist is created. 
            Anything that exists should be controlled so that 
            it reflects the model. If elements exists in the view
            that does not exist in the model, it should be removed.
        """
        folderviewlocation.update(foldermodellocation)
        # Find elements for removal...
        self.removeelements = []
        for contentid in folderviewlocation.contentids:
            foundelement = False
            for folder in foldermodellocation.folders:
                if folder.id == folderviewlocation.contentids[contentid].id:
                    if folderviewlocation.contentids[contentid].type == 'folder':
                        foundelement = True
            for item in foldermodellocation.items:
                if item.id == folderviewlocation.contentids[contentid].id:
                    if folderviewlocation.contentids[contentid].type == 'item':
                        foundelement = True
            if not foundelement:
                self.removeelements.append(contentid)
        # ... and remove them.
        if self.removeelements != []:
            for contentid in self.removeelements:
                    folderviewlocation.contentids[contentid].remove_view()
                    del folderviewlocation.contentids[contentid]
        # Add or update folders in the current folder. 
        for foldermodel in foldermodellocation.folders:
            # If the element does not exist yet - create it
            if foldermodel.id not in folderviewlocation.contentids:
                tmp = MfcMenuFolderView(folderviewlocation, foldermodel, self.configuration.gui_image_path)
                self._recursive_update(foldermodel, tmp)
            # If it exists - see that it is correct.
            else:
                self._recursive_update(foldermodel, folderviewlocation.contentids[foldermodel.id])
        # Add or update items in the current folder.
        for item in foldermodellocation.items:
            if item.id not in folderviewlocation.contentids:
                MfcMenuItemView(folderviewlocation, item, self.configuration.gui_image_path, self.launchidmap)
                #self.launchidcounter = self.launchidcounter + 1
            else:
                folderviewlocation.contentids[item.id].update(item) 

class MfcMenuItemView():
    """Displays a menu item"""
    
    def __init__(self, parent, menuitemmodel, imagepath, launchidmap):
        """ View a single item in the menu.
        
            The view registers itself to its parent.
            So that individual updates can be 
            performed.
        
            Keyword arguments:
            parent -- where the menu item is placed
            menuitemmodel -- the model for this item
        """        
        self.parent = parent
        self.id = menuitemmodel.id
        self.launchid = menuitemmodel.launch_id
        self.text = menuitemmodel.text
        self.enabled = menuitemmodel.enabled
        self.iconpath = menuitemmodel.icon
        self.type = 'item'
        self.imagepath = imagepath
        self.launchidmap = launchidmap

        self.mapid = len(self.launchidmap)
        self.launchidmap.append(self.launchid)

        # Set appearance of the menu item 
        self.settings = win32con.MF_STRING
        if not self.enabled:
            self.settings = win32con.MF_STRING | win32con.MF_DISABLED
            
        # Create, append and register the item.
        #win32gui.AppendMenu(self.parent.view, self.settings, self.launchid, self.text)
        win32gui.AppendMenu(self.parent.view, self.settings, self.mapid, self.text)
        self.parent.contentids[self.id] = self
        # If an icon reference has been given - use it.
        if menuitemmodel.icon != None:
            self._set_icon(menuitemmodel)
        # If the menu item has an active zone restriction - show that.
        if menuitemmodel.restricted == True:
            menuitemmodel.icon = 'g_zone_restricted_action.bmp'
            self._set_icon(menuitemmodel)

    def _set_icon(self, menuitemmodel):
        """ Set an icon for the item view. """
        try:
            assert menuitemmodel.icon.endswith('bmp'), "Filetype not supported"
            self.iconpath = menuitemmodel.icon
            self.icon = locate_icon(path=self.imagepath, filename=menuitemmodel.icon)
            #win32gui.SetMenuItemBitmaps(self.parent.view, self.launchid, win32con.MF_BYCOMMAND, self.icon, self.icon)
            win32gui.SetMenuItemBitmaps(self.parent.view, self.mapid, win32con.MF_BYCOMMAND, self.icon, self.icon)
        except AssertionError:
            print "Filetype not supported:", menuitemmodel.icon, ", use bitmap files for menu items"
            #self.icon = locate_icon(path=self.imagepath, filename=GIRI_GUI_DEFAULT_BITMAP_IMAGE)
            win32gui.SetMenuItemBitmaps(self.parent.view, self.mapid, win32con.MF_BYCOMMAND, None, None)

    def update(self, menuitemmodel):
        """ Check and repair the item.
            
            On an item the following can be changed:
            The text, the icon, the enabled state.
            The enabled state is always set, because it
            seems more expensive to check it in the other 
            parts.
        """
        if self.text != menuitemmodel.text:
            #win32gui.ModifyMenu(self.parent.view, self.launchid, win32con.MF_BYCOMMAND | win32con.MF_STRING, self.launchid, menuitemmodel.text)
            win32gui.ModifyMenu(self.parent.view, self.mapid, win32con.MF_BYCOMMAND | win32con.MF_STRING, self.mapid, menuitemmodel.text)

        if self.iconpath != menuitemmodel.icon:
            if menuitemmodel.icon == None:
                #win32gui.SetMenuItemBitmaps(self.parent.view, self.launchid, win32con.MF_BYCOMMAND, 0, 0)
                win32gui.SetMenuItemBitmaps(self.parent.view, self.mapid, win32con.MF_BYCOMMAND, 0, 0)
            else:
                self._set_icon(menuitemmodel) 
            self.iconpath = menuitemmodel.icon

        if menuitemmodel.enabled == True:
            #win32gui.EnableMenuItem(self.parent.view, self.launchid, win32con.MF_BYCOMMAND | win32con.MF_ENABLED)
            win32gui.EnableMenuItem(self.parent.view, self.mapid, win32con.MF_BYCOMMAND | win32con.MF_ENABLED)
        else:
            #win32gui.EnableMenuItem(self.parent.view, self.launchid, win32con.MF_BYCOMMAND | win32con.MF_DISABLED)
            win32gui.EnableMenuItem(self.parent.view, self.mapid, win32con.MF_BYCOMMAND | win32con.MF_DISABLED)
        self.enabled = menuitemmodel.enabled

    def remove_view(self):
        """ Remove the view for this item. """
        try:
            #win32gui.DeleteMenu(self.parent.view, self.launchid, win32con.MF_BYCOMMAND)
            win32gui.DeleteMenu(self.parent.view, self.mapid, win32con.MF_BYCOMMAND)
        except:
            pass


class MfcMenuFolderView():
    """ MFC view for a folder in the menu.

        Display this folder and iterate over the folders
        and items that it contains. If the parent folder 
        is None it is not attached as a sub-folder.
        
        Keyword arguments:
        parentview -- an MFC pop-up menu or None
        modelfolder -- the model behind this folder
    """
    def __init__(self, parent, menufoldermodel, imagepath):
        self.parent = parent
        self.id = menufoldermodel.id
        self.text = menufoldermodel.text
        self.iconpath = menufoldermodel.icon # Needed for the update method.
        self.contentids = {} # dictionary for looking up folders and items by id (nice for testing and updates).
        self.view = win32gui.CreatePopupMenu()
        self.type = 'folder'
        self.imagepath = imagepath

        if parent != None: 
            win32gui.AppendMenu(parent.view, win32con.MF_POPUP, self.view, self.text)
            parent.contentids[self.id] = self

        if menufoldermodel.icon != None:
            self._set_icon(menufoldermodel)

    def _set_icon(self, menufoldermodel):
        """ Set an icon for the folder view. """
        try:
            assert menufoldermodel.icon.endswith('bmp'), "Filetype not supported"
            self.icon = locate_icon(path=self.imagepath, filename=menufoldermodel.icon)
            win32gui.SetMenuItemBitmaps(self.parent.view, self.view, win32con.MF_BYCOMMAND, self.icon, self.icon)
        except AssertionError:
            print "Filetype not supported:", menufoldermodel.icon, ", use bitmap files for menu items (loading default icon instead)"
            self.icon = locate_icon(path=self.imagepath, filename=GIRI_GUI_DEFAULT_BITMAP_IMAGE)
            win32gui.SetMenuItemBitmaps(self.parent.view, self.view, win32con.MF_BYCOMMAND, self.icon, self.icon)

    def update(self, menufoldermodel):
        """ Check and repair this folder view.
            
            On a folder the following can be changed:
            The text, the icon. 
            Folders can not be enabled or disabled.
        """
        if self.text != menufoldermodel.text:
            if self.parent is not None:
                win32gui.ModifyMenu(self.parent.view, self.view, win32con.MF_BYCOMMAND | win32con.MF_STRING, self.view, menufoldermodel.text)
        if self.iconpath != menufoldermodel.icon:
            if menufoldermodel.icon == None:
                if self.parent is not None:
                    win32gui.SetMenuItemBitmaps(self.parent.view, self.view, win32con.MF_BYCOMMAND, 0, 0)
            else:
                self._set_icon(menufoldermodel)
        self.iconpath = menufoldermodel.icon

    def remove_view(self):
        """ Remove the view for this folder. """
        try:
            win32gui.DeleteMenu(self.parent.view, self.view, win32con.MF_BYCOMMAND)
        except:
            pass
