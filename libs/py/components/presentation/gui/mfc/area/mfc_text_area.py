"""

Generic MFC based clear text display area.

"""
import win32gui
import win32con
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

class MfcTextArea(MfcBaseArea): 
    """ An Area for letting the user locate a folder. """
    
    ID_BACKGROUND = 701
    ID_HEADLINE = 702
    ID_TEXT = 703
    ID_SEPARATOR = 704
    
    def __init__(self, hwnd, hinst):
        """ Create elements for a text area. """
        MfcBaseArea.__init__(self, hwnd, hinst)
        
        self.current_headline = ""
        self.current_text = ""
        
        self.background = win32gui.CreateWindow('STATIC', "",
                                              win32con.WS_CHILD | win32con.SS_WHITERECT | win32con.WS_VISIBLE, 
                                              0, 0, 0, 0,
                                              self.hwnd, MfcTextArea.ID_BACKGROUND, self.hinst, None)
        self.widgets.append(self.background)
        
        self.headline = self.create_large_text_widget(MfcTextArea.ID_HEADLINE)
        self.widgets.append(self.headline)

        self.text = self.create_text_widget(MfcTextArea.ID_TEXT)
        self.widgets.append(self.text)

        self.separator = self.create_separator_widget(MfcTextArea.ID_SEPARATOR) 
        self.widgets.append(self.separator)

    def update(self, model):
        """ Update relevant parts of the area. """
        # Set the headline text...        
        if self.current_headline != model.info_headline:
            win32gui.SetWindowText(self.headline, model.info_headline)
            win32gui.InvalidateRect(self.headline, None, False)
            self.current_headline = model.info_headline
        # Set the text area text...
        if self.current_text != model.info_text:
            win32gui.SetWindowText(self.text, model.info_text)
            win32gui.InvalidateRect(self.text, None, False)
            self.current_text = model.info_text

    def resize(self, left=0, top=0, width=300, height=100):
        """ Resize and relocate parts of the area. """
        
        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.background, None, False)
        
        win32gui.SetWindowPos(self.headline, 0, left + self.large_margin,
                              top + self.large_margin,
                              width - 2 * self.large_margin, 
                              20,
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.headline, None, False)    

        win32gui.SetWindowPos(self.text, 0, left + self.large_margin,
                              top + self.large_margin + 20 + self.margin,
                              width - 2 * self.large_margin, 
                              height - self.large_margin - 20 - 2 * self.margin,
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.text, None, False)    

        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcTextArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcTextArea.ID_BACKGROUND), 
                              -1, 
                              top + height - 1, 
                              width + 2, 
                              1,
                              0)
