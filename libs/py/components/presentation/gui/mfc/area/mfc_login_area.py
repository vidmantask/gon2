""" 

Generic MFC based user-name/password login area.

"""
import win32gui
import win32con
import platform

from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea
from components.presentation.login import LoginController

EM_SETCUEBANNER = 0x1501

class MfcLoginArea(MfcBaseArea): 
    """ An Area for login and changing passwords. """
    
    ID_LOGIN_TEXTFIELD = 401
    ID_PASSWORD_TEXTFIELD = 402
    ID_NEW_PASSWORD_TEXTFIELD = 403
    ID_NEW_PASSWORD_CONFIRM_TEXTFIELD = 404
    ID_BACKGROUND = 405
    ID_SEPARATOR = 406
    
    def __init__(self, hwnd, hinst, configuration):
        """ Create elements for a login area. """
        MfcBaseArea.__init__(self, hwnd, hinst, configuration)
        
        # Currently visible model.
        self.current_state = ''
        self.suggestedusername = ""
        self.lockusernamefield = False
        
        # Cue banners are too left aligned on XP.
        if platform.release() == 'XP':
            self.cue_banner_indent = u"  "
        else:
            self.cue_banner_indent = u""
        
        # Create elements.
        self.background = self.create_background_widget(MfcLoginArea.ID_BACKGROUND)
        self.widgets.append(self.background) 
        
        self.userfield = self.create_text_input_widget(MfcLoginArea.ID_LOGIN_TEXTFIELD)
        win32gui.SendMessage(self.userfield, EM_SETCUEBANNER, True, self.cue_banner_indent + self.dictionary._("User name"))
        self.widgets.append(self.userfield)

        self.passwordfield = self.create_secret_text_input_widget(MfcLoginArea.ID_PASSWORD_TEXTFIELD) 
        win32gui.SendMessage(self.passwordfield, EM_SETCUEBANNER, True, self.cue_banner_indent + self.dictionary._("Password"))
        self.widgets.append(self.passwordfield)

        self.newpasswordfield = self.create_secret_text_input_widget(MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD) 
        win32gui.SendMessage(self.newpasswordfield, EM_SETCUEBANNER, True, self.cue_banner_indent + self.dictionary._("New password"))
        self.widgets.append(self.newpasswordfield) 

        self.confirmnewpasswordfield = self.create_secret_text_input_widget(MfcLoginArea.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD)
        win32gui.SendMessage(self.confirmnewpasswordfield, EM_SETCUEBANNER, True, self.cue_banner_indent + self.dictionary._("Confirm password"))
        self.widgets.append(self.confirmnewpasswordfield) 
        
        self.separator = self.create_separator_widget(MfcLoginArea.ID_SEPARATOR)
        self.widgets.append(self.separator)
        
        self.show()

    def update(self, model):
        """ Update relevant parts of the area. """
        # Set state
        if self.current_state != model.state:
            if model.state == LoginController.ID_STATE_LOGIN:
                win32gui.ShowWindow(self.newpasswordfield, win32con.SW_HIDE)
                win32gui.ShowWindow(self.confirmnewpasswordfield, win32con.SW_HIDE)
            elif model.state == LoginController.ID_STATE_CHANGE_LOGIN:
                win32gui.ShowWindow(self.newpasswordfield, win32con.SW_SHOW)
                win32gui.ShowWindow(self.confirmnewpasswordfield, win32con.SW_SHOW)
            self.current_state = model.state
        # Set a suggested user-name
        if self.suggestedusername != model.suggestedusername:
            win32gui.SetWindowText(self.userfield, model.suggestedusername)
            self.suggestedusername = model.suggestedusername
        # Set the locking state of the user name field.
        if self.lockusernamefield != model.lockusernamefield:
            win32gui.EnableWindow(self.userfield, not model.lockusernamefield)
            if model.lockusernamefield:
                win32gui.SendMessage(self.hwnd, win32con.WM_NEXTDLGCTL, self.passwordfield, True)
            else:
                win32gui.SendMessage(self.hwnd, win32con.WM_NEXTDLGCTL, self.userfield, True)
            self.lockusernamefield = model.lockusernamefield
            win32gui.InvalidateRect(self.userfield, None, False)

    def resize(self, left=0, top=0, width=375, height=300):
        """ Resize and relocate parts of the area. """
        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        
        center = height/2
        fieldheight = 20
        
        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        
        if self.current_state == LoginController.ID_STATE_LOGIN:
            win32gui.SetWindowPos(self.userfield, 0, self.large_margin, top + center - fieldheight, 
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.userfield, None, False)
            win32gui.SetWindowPos(self.passwordfield, 0, self.large_margin, top + center + self.margin, 
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.passwordfield, None, False)

        elif self.current_state == LoginController.ID_STATE_CHANGE_LOGIN:
            win32gui.SetWindowPos(self.userfield, 0, self.large_margin, top + center - 2 * fieldheight - self.margin,
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.userfield, None, False)
            
            win32gui.SetWindowPos(self.passwordfield, 0, self.large_margin, top + center - fieldheight,
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.passwordfield, None, False)
            
            win32gui.SetWindowPos(self.newpasswordfield, 0, self.large_margin, top + center + self.margin, 
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.newpasswordfield, None, False)
            
            win32gui.SetWindowPos(self.confirmnewpasswordfield, 0, self.large_margin, top + center + fieldheight + 2 * self.margin, 
                                  width - 2 * self.large_margin, fieldheight, win32con.SWP_NOZORDER)
            win32gui.InvalidateRect(self.confirmnewpasswordfield, None, False)

        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcLoginArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcLoginArea.ID_BACKGROUND), 
                              -1, 
                              top + height - 1, 
                              width + 2, 
                              1,
                              0)
