"""

Generic MFC based phase status area.

"""
import win32gui
import win32con

from components.presentation.gui.mfc.mfc_tools import locate_image
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

class MfcPhaseStatusArea(MfcBaseArea):

    ID_BACKGROUND = 802
    ID_SEPARATOR = 801
    
    def __init__(self, hwnd, hinst, configuration):
        """ MFC Area for showing a number of phases.
        
            There is always a fixed number of phases so text 
            and buttons are created for these first. 
            Updates can then set the different phases to 
            invisible and resize them to fill the window.
        
        """
        MfcBaseArea.__init__(self, hwnd, hinst, configuration)

        self.phaselist = []
        self.phaseboxcount = 5
        self.phaseboxtexts = []
        self.phaseboxarrows = []
        self.arrowwidth = 20

        self.current_phase_count = 0
        
        # Create elements
        self.background = win32gui.CreateWindow('STATIC', "White background",
                                        win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.SS_WHITERECT,
                                        0 , 0, 0, 0,
                                        self.hwnd, MfcPhaseStatusArea.ID_BACKGROUND, self.hinst, None)

        self.separator = win32gui.CreateWindow('STATIC', None, 
                                               win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.SS_ETCHEDHORZ,
                                               0, 0, 0, 0,
                                               self.hwnd, MfcPhaseStatusArea.ID_SEPARATOR, self.hinst, None)

        self.create_phases()

    def get_text_box_start(self, boxcount, index, left, width):
        """ Get the left based start position for text boxes. """
        return left + self.margin + 1 + (index * self.get_text_box_width(boxcount, width)) + (index * self.get_arrow_width()) 
          
    def get_text_box_width(self, boxcount, width):
        """ Get the width of each individual box. """ 
        _arrowcount = boxcount-1
        if _arrowcount < 0:
            _arrowcount = 0
        _textwidth = ((width - (2 * self.margin) - ((_arrowcount) * self.get_arrow_width())) / boxcount) - 2
        return _textwidth
    
    def get_arrow_box_start(self, boxcount, index, left, width):
        """ Get the left based start position for arrows. """
        return self.get_text_box_start(boxcount, index-1, left, width) + self.get_text_box_width(boxcount, width) 
        
    def get_arrow_width(self):
        """ Get the width of an arrow. """
        return self.arrowwidth
    
    def create_phases(self):
        """ Create a number of boxes for displaying phases. 
        
            There is absolutely no way of getting a white 
            background if we do not let the static be a child 
            of the top most window. Tried absolutely everything :-(
        """
        for index in range(self.phaseboxcount):
            self.phaseboxtexts.append(win32gui.CreateWindow('STATIC', "",
                                                            win32con.WS_CHILD | win32con.SS_CENTER | win32con.SS_CENTERIMAGE,  
                                                            0,0,0,0, self.hwnd, 1000 + index, self.hinst, None))

            self.phaseboxarrows.append(win32gui.CreateWindow('STATIC', "",
                                                              win32con.WS_CHILD | win32con.SS_BITMAP | win32con.SS_CENTERIMAGE,
                                                              0,0,0,0, self.hwnd, 0, self.hinst, None))
            win32gui.SendMessage(self.phaseboxarrows[-1], win32con.STM_SETIMAGE, win32con.IMAGE_BITMAP,
                                 locate_image(path=self.image_path, filename="g_arrow_grey.bmp"))
    
    def _update_box_positions(self, left=None, top=None, width=None, height=None):
        """ Hide fields that are not used and use the space for the others. """
        
        if left is None:
            left = self.left
        if top is None:
            top = self.top
        if width is None:
            width = self.width
        if height is None:
            height = self.height
        
        # Change box sizes and positions.
        if self.current_phase_count > 0:
            for index in range(0, self.current_phase_count):
                win32gui.SetWindowPos(self.phaseboxtexts[index], self.background,
                                      self.get_text_box_start(self.current_phase_count, index, left, width), 
                                      top + 1 , # + self.margin + 1, 
                                      self.get_text_box_width(self.current_phase_count, width), 
                                      height - 2,
                                      win32con.SWP_NOZORDER)
                if index > 0:
                    win32gui.SetWindowPos(self.phaseboxarrows[index], 0, 
                                      self.get_arrow_box_start(self.current_phase_count, index, left, width), 
                                      top + 1, # + self.margin + 1, 
                                      self.get_arrow_width(), 
                                      height - 2,
                                      win32con.SWP_NOZORDER)

        win32gui.InvalidateRect(self.hwnd, None, False)

    def _update_box_content(self, oldmodel, newmodel):
        """ Update the content of the phase boxes. 
        
            Text color is changed in mains onCtlColor.
        """
        for index, phase in enumerate(newmodel): 
            # Update if something changed compared to the old model.
            _changestate = False
            _changetext = False
            
            if index < len(oldmodel):
                if phase['name'] != oldmodel[index]['name']:
                    _changetext = True
                if phase['state'] != oldmodel[index]['state']:
                    _changestate = True
            # Update if index was not specified in the old model.
            elif index >= len(oldmodel):
                _changetext = True
                _changestate = True
            
            if _changetext:
                win32gui.SetWindowText(self.phaseboxtexts[index], phase['name'])            
                
            if _changestate:
                if phase['state'] == 'complete':
                    win32gui.SendMessage(self.phaseboxtexts[index], win32con.WM_SETFONT, self.standard_font, 0)
                    win32gui.SendMessage(self.phaseboxarrows[index], win32con.STM_SETIMAGE, win32con.IMAGE_BITMAP, 
                                         locate_image(path=self.image_path, filename="g_arrow_red.bmp"))
                elif phase['state'] == 'active':
                    win32gui.SendMessage(self.phaseboxtexts[index], win32con.WM_SETFONT, self.bold_font, 0)
                    win32gui.SendMessage(self.phaseboxarrows[index], win32con.STM_SETIMAGE, win32con.IMAGE_BITMAP, 
                                         locate_image(path=self.image_path, filename="g_arrow_red.bmp"))
                else: # Assuming 'pending'
                    win32gui.SendMessage(self.phaseboxtexts[index], win32con.WM_SETFONT, self.standard_font, 0)
                    win32gui.SendMessage(self.phaseboxarrows[index], win32con.STM_SETIMAGE, win32con.IMAGE_BITMAP, 
                                         locate_image(path=self.image_path, filename="g_arrow_grey.bmp"))

            if _changetext or _changestate:
                
                win32gui.UpdateWindow(self.phaseboxtexts[index])
                win32gui.InvalidateRect(self.phaseboxarrows[index], None, False)
                
                win32gui.UpdateWindow(self.phaseboxarrows[index])
                win32gui.InvalidateRect(self.phaseboxtexts[index], None, False)

                win32gui.UpdateWindow(self.hwnd)
                win32gui.InvalidateRect(self.hwnd, None, False)

    def update(self, model):
        """ Update the MFC phase area. 
        
            First see that the correct number of fields are visible.
            Then make sure the content of the visible fields are 
            correct. If box sizes are changed force the redraw of
            box contents by setting the old one to []. If not only 
            update relevant parts.
        """
        if (len(self.phaselist) != len(model.phase_list)):
            # Change box visibility.
            if len(model.phase_list) < len(self.phaselist):
                for index in range(len(model.phase_list), len(self.phaselist)):
                    win32gui.ShowWindow(self.phaseboxtexts[index], win32con.SW_HIDE)
                    win32gui.ShowWindow(self.phaseboxarrows[index], win32con.SW_HIDE)           
            else:
                for index in range(len(self.phaselist), len(model.phase_list)):
                    win32gui.ShowWindow(self.phaseboxtexts[index], win32con.SW_SHOW)
                    win32gui.ShowWindow(self.phaseboxarrows[index], win32con.SW_SHOW)            
            self.current_phase_count = len(model.phase_list)
            
           
            #[left, top, width, height] = win32gui.GetClientRect(self.background)
            self._update_box_positions()    
            
            self._update_box_content([], model.phase_list)
        else:
            self._update_box_content(self.phaselist, model.phase_list)
        self.phaselist = model.phase_list

    def resize(self, left=0, top=0, width=375, height=30):
        """ Resize and relocate parts of the header area. """
        self.set_sizes(left, top, width, height) 

        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.background, None, False)

        self._update_box_positions(left, top, width, height)    
        win32gui.InvalidateRect(self.background, None, False)
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcPhaseStatusArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcPhaseStatusArea.ID_BACKGROUND), 
                              -1, 
                              top + height - 1, 
                              width + 2, 
                              2,
                              0)
