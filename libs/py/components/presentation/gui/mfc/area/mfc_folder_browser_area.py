"""

Generic MFC based folder browser area.

"""

import win32gui
import win32con
from win32com.shell import shellcon, shell #@UnresolvedImport # pylint: disable=E0611,F0401
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea

GIRI_GUI_BUTTON_WIDTH = 90
GIRI_GUI_BUTTON_HEIGHT = 25 

class MfcFolderBrowserArea(MfcBaseArea): 
    """ An Area for letting the user locate a folder. """
    
    ID_HEADLINE = 501
    ID_FOLDER_PATH = 502
    ID_LOCATE_FOLDER_BUTTON = 503
    ID_SEPARATOR = 504
    ID_BACKGROUND = 505

    def __init__(self, hwnd, hinst):
        """ Create elements for a folder browser area. """
        MfcBaseArea.__init__(self, hwnd, hinst)
        
        self.current_folder_path = ""
        self.current_headline = "Headline"
        
        self.background = self.create_background_widget(MfcFolderBrowserArea.ID_BACKGROUND) 
        self.widgets.append(self.background) 

        self.headline = self.create_large_text_widget(MfcFolderBrowserArea.ID_HEADLINE)
        self.widgets.append(self.headline)
        
        self.folderpath = self.create_text_input_widget(MfcFolderBrowserArea.ID_FOLDER_PATH)
        self.widgets.append(self.folderpath)
        
        self.locate_folder_button = self.create_button_widget(MfcFolderBrowserArea.ID_LOCATE_FOLDER_BUTTON) 
        win32gui.SetWindowText(self.locate_folder_button, "Browse")
        self.widgets.append(self.locate_folder_button)
        win32gui.ShowWindow(self.locate_folder_button, win32con.SW_HIDE) # Buttons are visible as default (for now).
        
        self.separator = self.create_separator_widget(MfcFolderBrowserArea.ID_SEPARATOR) 
        self.widgets.append(self.separator)
        
    def browse_for_folder(self, controller):
        """ Locate a folder. """
        
        desktop_pidl = shell.SHGetFolderLocation(self.hwnd, shellcon.CSIDL_DESKTOP, 0, 0)
        pidl, _, _ = shell.SHBrowseForFolder(win32gui.GetDesktopWindow (),
                                                                 desktop_pidl,
                                                                 "",
                                                                 shellcon.BIF_RETURNONLYFSDIRS, None, None)
        try:
            path = shell.SHGetPathFromIDList(pidl)
            controller.set_info_user_selected_folder_path(path)
        except (shell.error, TypeError):
            controller.set_info_user_selected_folder_path('')
        
    def update(self, model):
        """ Update relevant parts of the area. """
        # Set the folder path string
        if self.current_folder_path != model.info_folder_path:
            win32gui.SetWindowText(self.folderpath, model.info_folder_path)
            self.current_folder_path = model.info_folder_path
        if self.current_headline != model.info_headline:
            win32gui.SetWindowText(self.headline, model.info_headline)
            win32gui.UpdateWindow(self.headline)
            win32gui.InvalidateRect(self.headline, None, True)
            self.current_headline = model.info_headline
    
    def resize(self, left=0, top=0, width=300, height=100):
        """ Resize and relocate parts of the area. """
        
        center = height/2
        fieldheight = 20
        
        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.background, None, False)
    
        win32gui.SetWindowPos(self.headline, 0, left + self.large_margin,
                              top + self.margin,
                              width, 20,
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.headline, None, False)    
    
        win32gui.SetWindowPos(self.folderpath, 0, left + self.large_margin, 
                              top + center - (fieldheight/2), 
                              width - 2 * self.large_margin - self.margin - GIRI_GUI_BUTTON_WIDTH, fieldheight, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.folderpath, None, False)
        win32gui.SetWindowPos(self.locate_folder_button, 0, left + width - self.large_margin - GIRI_GUI_BUTTON_WIDTH, 
                              top + center - (GIRI_GUI_BUTTON_HEIGHT/2), 
                              GIRI_GUI_BUTTON_WIDTH, 
                              GIRI_GUI_BUTTON_HEIGHT, 
                              win32con.SWP_NOZORDER)
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcFolderBrowserArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcFolderBrowserArea.ID_BACKGROUND), 
                              -1, 
                              top + height - 1, 
                              width + 2, 
                              1,
                              0)
    