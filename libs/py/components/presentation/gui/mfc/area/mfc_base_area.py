"""

Base class for MFC based areas.

"""

import win32gui
import win32con

def _long(hiword=0, loword=0):
    """ Create a long with the given 'HI'- and 'LO'-word"""
    return ((hiword & 0xFFFF) << 16) | (loword & 0xFFFF)

class MfcBaseArea(object):
    """ An base area frame for all areas. """
    def __init__(self, hwnd, hinst, configuration=None):
        self.hwnd = hwnd
        self.hinst = hinst
        self.image_path = '..'
        if configuration != None:
            self.image_path = configuration.gui_image_path
            self.dictionary = configuration.dictionary

        self.widgets = []
        self.visible = True
        self.margin = 8
        self.large_margin = 4 * self.margin

        # Sizes.
        self.left = 0
        self.top = 0
        self.width = 0
        self.height = 0

        # Setup fonts.
        _logfont = win32gui.LOGFONT()
        _logfont.lfFaceName = "Tahoma"
        # Small font.
        _logfont.lfHeight = 12
        self.small_font = win32gui.CreateFontIndirect(_logfont)
        # Standard font
        _logfont.lfHeight = 14
        self.standard_font = win32gui.CreateFontIndirect(_logfont)
        # Standard height + bold
        _logfont.lfWeight = win32con.FW_BOLD
        self.bold_font = win32gui.CreateFontIndirect(_logfont)
        # big height + bold
        _logfont.lfHeight = 20
        self.big_bold_font = win32gui.CreateFontIndirect(_logfont)


    def create_button_widget(self, widget_id):
        """ Create a widget for displaying a button. """
        widget = win32gui.CreateWindowEx(0, 'BUTTON', "",
                                         win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.BS_PUSHBUTTON | win32con.WS_TABSTOP,
                                         0, 0, 0, 0,
                                        self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.standard_font, 0)
        return widget

    def create_background_widget(self, widget_id):
        """ Create a widget for displaying a background. """
        return win32gui.CreateWindow('STATIC', "",
                                     win32con.WS_CHILD | win32con.SS_WHITERECT,
                                     0, 0, 0, 0,
                                     self.hwnd, widget_id, self.hinst, None)

    def create_text_widget(self, widget_id):
        """ Create a widget for displaying text. """
        widget = win32gui.CreateWindowEx(0, 'STATIC', "",
                                            win32con.WS_CHILD | win32con.WS_VISIBLE,
                                            0, 0, 0, 0,
                                            self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.standard_font, 0)
        return widget

    def create_large_text_widget(self, widget_id):
        """ Create a widget for displaying large text. """
        widget = win32gui.CreateWindow('STATIC', "",
                                         win32con.WS_CHILD,
                                         0, 0, 0, 0,
                                         self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.big_bold_font, 0)
        return widget

    def create_small_text_widget(self, widget_id):
        """ Create a widget for displaying small text. """
        widget = win32gui.CreateWindow('STATIC', "",
                                         win32con.WS_CHILD,
                                         0, 0, 0, 0,
                                         self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.small_font, 0)
        return widget

    def create_text_input_widget(self, widget_id):
        """ Create a widget for text input.

            The left margin is set to 5, because
            otherwise it is too left aligned on
            XP. Using EC_USEFONTINFO to determine
            the best left margin did not seem to
            work.
        """
        widget = win32gui.CreateWindowEx(0, 'EDIT', "",
                                         win32con.WS_CHILD | win32con.WS_BORDER | win32con.ES_AUTOHSCROLL | win32con.WS_TABSTOP,
                                         0, 0, 0, 20,
                                         self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.standard_font, 0)
        win32gui.SendMessage(widget, win32con.EM_SETMARGINS, win32con.EC_LEFTMARGIN, _long(loword=5))
        return widget

    def create_secret_text_input_widget(self, widget_id):
        """ Create a widget for text input. """
        widget = win32gui.CreateWindowEx(0, 'EDIT', "",
                                         win32con.WS_CHILD | win32con.WS_BORDER | win32con.ES_AUTOHSCROLL | win32con.ES_PASSWORD | win32con.WS_TABSTOP,
                                         0, 0, 0, 20,
                                         self.hwnd, widget_id, self.hinst, None)
        win32gui.SendMessage(widget, win32con.WM_SETFONT, self.standard_font, 0)
        win32gui.SendMessage(widget, win32con.EM_SETMARGINS, win32con.EC_LEFTMARGIN, _long(loword=5))
        return widget

    def create_separator_widget(self, widget_id):
        """ Create a widget for a thin line separator. """
        return win32gui.CreateWindow('STATIC', None,
                                                      win32con.WS_CHILD | win32con.WS_VISIBLE | win32con.SS_ETCHEDHORZ,
                                                      0, 0, 0, 0,
                                                      self.hwnd, widget_id, self.hinst, None)

    def set_sizes(self, left=0, top=0, width=0, height=0):
        """ Set the size of this area. """
        (self.left, self.top, self.width, self.height) = (left, top, width, height)

    def hide(self):
        """ Hide all widgets in this area. """
        for widget in self.widgets:
            win32gui.ShowWindow(widget, win32con.SW_HIDE)
        self.visible = False

    def show(self):
        """  Show all widgets in this area. """
        for widget in self.widgets:
            win32gui.ShowWindow(widget, win32con.SW_SHOW)
        self.visible = True
