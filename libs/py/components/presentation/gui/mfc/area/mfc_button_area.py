"""

Generic MFC based button area.

"""
import win32gui
import win32con
import win32api

from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea
from components.presentation.base.button_area_base import ButtonAreaControllerBase

GIRI_GUI_BUTTON_WIDTH = 90
GIRI_GUI_BUTTON_HEIGHT = 25 
GIRI_GUI_BUTTON_AREA_HEIGHT = GIRI_GUI_BUTTON_HEIGHT + (32)

class MfcButtonArea(MfcBaseArea): 
    """ An Area for buttons (Cancel/Next). """
    
    ID_BUTTON_NEXT = 209
    ID_BUTTON_PREVIOUS = 210
    ID_BUTTON_CANCEL = 211
    ID_SEPARATOR_OVER_BUTTONS = 212
    
    def __init__(self, hwnd, hinst, configuration):
        """ Create elememts for a button area. """
        MfcBaseArea.__init__(self, hwnd, hinst, configuration)
        
        # Currently visible model.
        self.current_cancelallowed = True
        self.current_cancel_label = self.dictionary._("Cancel")
        self.current_cancel_visible = True
        self.current_nextallowed = True
        self.current_next_label = self.dictionary._("Next")
        self.current_next_visible = True
        self.current_default_button = None
        
        # Create widgets
        self.nextbutton = self.create_button_widget(MfcButtonArea.ID_BUTTON_NEXT) 
        win32gui.SetWindowText(self.nextbutton, self.dictionary._("OK"))
        self.widgets.append(self.nextbutton)

        self.cancelbutton = self.create_button_widget(MfcButtonArea.ID_BUTTON_CANCEL)
        win32gui.SetWindowText(self.cancelbutton, self.dictionary._("Cancel"))
        self.widgets.append(self.cancelbutton)

    def update(self, model):
        """ Update relevant elements in the button area. """
        # Settings for the 'cancel' button
        if self.current_cancelallowed != model.cancel_allowed:
            win32gui.EnableWindow(self.cancelbutton, model.cancel_allowed)
            win32gui.InvalidateRect(self.cancelbutton, None, True)
            self.current_default_button = None
            self.current_cancelallowed = model.cancel_allowed
        if self.current_cancel_label != model.cancel_label:
            win32gui.SetWindowText(self.cancelbutton, model.cancel_label)
            win32gui.InvalidateRect(self.cancelbutton, None, True)
            self.current_cancel_label = model.cancel_label
        if self.current_cancel_visible != model.cancel_visible:
            if model.cancel_visible:
                win32gui.ShowWindow(self.cancelbutton, win32con.SW_SHOW)
            else:
                win32gui.ShowWindow(self.cancelbutton, win32con.SW_HIDE)
            win32gui.InvalidateRect(self.cancelbutton, None, False)
            self.current_cancel_visible = model.cancel_visible
        # Settings for the 'next' button
        if self.current_nextallowed != model.next_allowed:
            win32gui.EnableWindow(self.nextbutton, model.next_allowed)
            win32gui.InvalidateRect(self.nextbutton, None, False)
            self.current_default_button = None
            self.current_nextallowed = model.next_allowed
        if self.current_next_label != model.next_label:
            win32gui.SetWindowText(self.nextbutton, model.next_label)
            win32gui.InvalidateRect(self.nextbutton, None, False)
            self.current_next_label = model.next_label
        if self.current_next_visible != model.next_visible:
            if model.next_visible:
                win32gui.ShowWindow(self.nextbutton, win32con.SW_SHOW)
            else:
                win32gui.ShowWindow(self.nextbutton, win32con.SW_HIDE)
            win32gui.InvalidateRect(self.nextbutton, None, False)
            self.current_next_visible = model.next_visible
        # Set the default push button
        if self.current_default_button != model.default_button:
            if model.default_button == ButtonAreaControllerBase.ID_NEXT_BUTTON:
                win32gui.SendMessage(self.cancelbutton, win32con.BM_SETSTYLE, win32con.BS_PUSHBUTTON, win32api.LOWORD(False))
                win32gui.SendMessage(self.nextbutton, win32con.BM_SETSTYLE, win32con.BS_DEFPUSHBUTTON, win32api.LOWORD(False))
                win32gui.SendMessage(self.nextbutton, win32con.DM_SETDEFID, 0, 0)
            elif model.default_button == ButtonAreaControllerBase.ID_CANCEL_BUTTON:
                win32gui.SendMessage(self.nextbutton, win32con.BM_SETSTYLE, win32con.BS_PUSHBUTTON, win32api.LOWORD(False))
                win32gui.SendMessage(self.cancelbutton, win32con.BM_SETSTYLE, win32con.BS_DEFPUSHBUTTON, win32api.LOWORD(False))
                win32gui.SendMessage(self.cancelbutton, win32con.DM_SETDEFID, 0, 0)
            self.current_default_button = model.default_button

    def resize(self, left=0, top=0, width=300, height=57):
        """ Resize anything relevant in this area. """
        self.set_sizes(left, top, width, height) 

        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcButtonArea.ID_BUTTON_CANCEL), 
                              0, 
                              width - GIRI_GUI_BUTTON_WIDTH - self.margin,
                              top + height - GIRI_GUI_BUTTON_HEIGHT - (2 * self.margin), 
                              GIRI_GUI_BUTTON_WIDTH, 
                              GIRI_GUI_BUTTON_HEIGHT, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.nextbutton, None, True)
        
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcButtonArea.ID_BUTTON_NEXT), 
                              0, 
                              width - (2 * GIRI_GUI_BUTTON_WIDTH) - (2 * self.margin),
                              top + height - GIRI_GUI_BUTTON_HEIGHT - (2 * self.margin), 
                              GIRI_GUI_BUTTON_WIDTH, 
                              GIRI_GUI_BUTTON_HEIGHT, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.cancelbutton, None, True)

