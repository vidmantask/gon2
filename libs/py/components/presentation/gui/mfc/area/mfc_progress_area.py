"""

Generic MFC based progress display area.

"""

import win32gui
import win32con
import win32api
import commctrl
from components.presentation.gui.mfc.area.mfc_base_area import MfcBaseArea
from components.presentation.update import UpdateController

# MFC Progress bar settings
PBM_SETMARQUEE = commctrl.WM_USER + 10
PBS_MARQUEE = 8

class MfcProgressArea(MfcBaseArea): 
    """ An Area for letting the user locate a folder. """
    
    ID_HEADLINE = 601
    ID_PROGRESS_BAR = 602
    ID_PROGRESS_BAR_MARQUEE = 603
    ID_PROGRESS_BAR_SUBTEXT = 604
    ID_SEPARATOR = 605
    ID_BACKGROUND = 606
    
    def __init__(self, hwnd, hinst):
        """ Create elements for a progress area. """
        MfcBaseArea.__init__(self, hwnd, hinst)

        self.current_headline = ""
        self.current_progress_mode = None
        self.current_progress_complete = 0
        self.current_progress_subtext = "" 

        self.progressbar_height = win32api.GetSystemMetrics(win32con.SM_CYVSCROLL)

        # Standard widgets...
        self.background = self.create_background_widget(MfcProgressArea.ID_BACKGROUND)
        self.widgets.append(self.background)
        
        self.headline = self.create_large_text_widget(MfcProgressArea.ID_HEADLINE)
        self.widgets.append(self.headline)

        self.progresssubtext = self.create_small_text_widget(MfcProgressArea.ID_PROGRESS_BAR_SUBTEXT)
        self.widgets.append(self.progresssubtext) 

        self.separator = self.create_separator_widget(MfcProgressArea.ID_SEPARATOR)
        self.widgets.append(self.separator)

        # Special widgets...
        self.progressbar = win32gui.CreateWindowEx(0, commctrl.PROGRESS_CLASS, "",
                                                    win32con.WS_CHILD,
                                                    0, 0, 0, 0,
                                                    self.hwnd, MfcProgressArea.ID_PROGRESS_BAR, self.hinst, None)
        self.widgets.append(self.progressbar)
        
        self.progressbar_marquee = win32gui.CreateWindowEx(0, commctrl.PROGRESS_CLASS, "",
                                                    win32con.WS_CHILD | PBS_MARQUEE,
                                                    0, 0, 0, 0,
                                                    self.hwnd, MfcProgressArea.ID_PROGRESS_BAR_MARQUEE, self.hinst, None)
        self.widgets.append(self.progressbar_marquee)

    def update(self, model):
        """ Update relevant parts of the area. """
        # Set the headline...
        if self.current_headline != model.info_headline:
            win32gui.SetWindowText(self.headline, model.info_headline)
            win32gui.UpdateWindow(self.headline)
            win32gui.InvalidateRect(self.headline, None, True)
            self.current_headline = model.info_headline
        # Choose between unknown and known progress...
        if self.current_progress_mode != model.info_progress_mode: 
            if model.info_progress_mode == 0: # Known progress
                win32gui.SendMessage(self.progressbar_marquee, PBM_SETMARQUEE, False, 0)
                if self.visible:
                    win32gui.ShowWindow(self.progressbar_marquee, win32con.SW_HIDE)
                    win32gui.ShowWindow(self.progressbar, win32con.SW_SHOW)
            else: # Assume Unknown progress
                if self.visible:
                    win32gui.ShowWindow(self.progressbar, win32con.SW_HIDE)
                    win32gui.ShowWindow(self.progressbar_marquee, win32con.SW_SHOW)
                win32gui.SendMessage(self.progressbar_marquee, PBM_SETMARQUEE, True, 50)
            self.current_progress_mode = model.info_progress_mode
        # Set the progress for the known progress bar...
        if self.current_progress_complete != model.info_progress_complete:
            if model.info_progress_complete == 100:
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETRANGE32, 0, 1000)
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETPOS, 1000, 0)
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETPOS, 999, 0)
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETRANGE32, 0, 100)
            else:
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETPOS, model.info_progress_complete, 0)
                win32gui.SendMessage(self.progressbar, commctrl.PBM_SETPOS, model.info_progress_complete-1, 0)
                win32gui.UpdateWindow(self.progressbar)
                win32gui.InvalidateRect(self.progressbar, None, False)
            self.current_progress_complete = model.info_progress_complete
        # Set the text under the progress bar.
        if self.current_progress_subtext != model.info_progress_subtext:
            win32gui.SetWindowText(self.progresssubtext, model.info_progress_subtext)
            win32gui.InvalidateRect(self.progresssubtext, None, False)
            self.current_progress_subtext = model.info_progress_subtext
    
    def resize(self, left=0, top=0, width=300, height=100):
        """ Resize and relocate parts of the area. """
        center = height/2
        
        win32gui.SetWindowPos(self.background, 0, left, top, width, height, win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.background, None, False)
        
        win32gui.SetWindowPos(self.headline, 0, left + self.large_margin,
                              top + center - (self.progressbar_height/2) - 20 - self.margin,
                              width - 2 * self.large_margin, 20,
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.headline, None, False)    
    
        win32gui.SetWindowPos(self.progressbar, 0, left + self.large_margin, 
                              top + center - (self.progressbar_height/2), 
                              width - 2 * self.large_margin, self.progressbar_height, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.progressbar, None, False)
        
        
        win32gui.SetWindowPos(self.progressbar_marquee, 0, left + self.large_margin, 
                              top + center - (self.progressbar_height/2), 
                              width - 2 * self.large_margin, self.progressbar_height, 
                              win32con.SWP_NOZORDER)
        win32gui.InvalidateRect(self.progressbar_marquee, None, False)
    
        win32gui.SetWindowPos(self.progresssubtext, 0, left + self.large_margin, 
                              top + center + (self.progressbar_height/2) + self.margin, 
                              width - 2 * self.large_margin, 
                              (height/2) - self.margin - (self.progressbar_height/2) - 1, 
                              win32con.SWP_NOZORDER)
    
        win32gui.SetWindowPos(win32gui.GetDlgItem(self.hwnd, MfcProgressArea.ID_SEPARATOR), 
                              win32gui.GetDlgItem(self.hwnd, MfcProgressArea.ID_BACKGROUND), 
                              -1, 
                              top + height - 1, 
                              width + 2, 
                              1,
                              0)
    
    def show(self):
        """ Show all relevant elements in this area. """
        win32gui.ShowWindow(self.background, win32con.SW_SHOW)
        win32gui.ShowWindow(self.headline, win32con.SW_SHOW)
        if self.current_progress_mode == UpdateController.ID_MODE_PROGRESS_KNOWN:
            win32gui.ShowWindow(self.progressbar, win32con.SW_SHOW)
        elif self.current_progress_mode == UpdateController.ID_MODE_PROGRESS_UNKNOWN:
            win32gui.ShowWindow(self.progressbar_marquee, win32con.SW_SHOW)
        win32gui.ShowWindow(self.progresssubtext, win32con.SW_SHOW)
        win32gui.ShowWindow(self.separator, win32con.SW_SHOW)
        self.visible = True
