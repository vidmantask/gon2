""" 

Specific MFC related test functionality.

"""

import win32gui
import win32con
import win32api
import commctrl
import win32gui_struct

from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
#from components.presentation.gui.mfc.mfc_message_view import MfcMessageView

from components.presentation.gui.mfc.area.mfc_login_area import MfcLoginArea
from components.presentation.gui.mfc.area.mfc_button_area import MfcButtonArea
from components.presentation.gui.mfc.area.mfc_progress_area import MfcProgressArea
from components.presentation.gui.mfc.area.mfc_text_area import MfcTextArea 
from components.presentation.gui.mfc.area.mfc_folder_browser_area import MfcFolderBrowserArea
from components.presentation.gui.mfc.area.mfc_selection_area import MfcSelectionArea

ID_PACKAGE_PLATFORM_IMAGE = "g_package_win_32x32.bmp"

ID_TRAY_ICON = MfcCommonView.ID_TRAY_ICON

ID_LOGIN_TEXTFIELD = MfcLoginArea.ID_LOGIN_TEXTFIELD 
ID_PASSWORD_TEXTFIELD = MfcLoginArea.ID_PASSWORD_TEXTFIELD
ID_NEW_PASSWORD_TEXTFIELD = MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD
ID_NEW_PASSWORD_CONFIRM_TEXTFIELD = MfcLoginArea.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD

ID_BUTTON_NEXT = MfcButtonArea.ID_BUTTON_NEXT
ID_BUTTON_CANCEL = MfcButtonArea.ID_BUTTON_CANCEL

ID_PROGRESS_BAR_SUBTEXT = MfcProgressArea.ID_PROGRESS_BAR_SUBTEXT 
ID_PROGRESS_BAR = MfcProgressArea.ID_PROGRESS_BAR
ID_PROGRESS_BAR_HEADLINE = MfcProgressArea.ID_HEADLINE

ID_FREE_TEXT = MfcTextArea.ID_TEXT
ID_FREE_TEXT_HEADLINE = MfcTextArea.ID_HEADLINE

ID_LOCATE_FOLDER_TEXTFIELD = MfcFolderBrowserArea.ID_FOLDER_PATH
ID_LOCATE_FOLDER_BUTTON = MfcFolderBrowserArea.ID_LOCATE_FOLDER_BUTTON
ID_LOCATE_FOLDER_HEADLINE = MfcFolderBrowserArea.ID_HEADLINE

ID_SELECTION_LIST = MfcSelectionArea.ID_SELECTION_TABLE


def set_widget_text(window, widget_id, text):
    """ Set the text in a text field widget. """
    win32gui.SetWindowText(win32gui.GetDlgItem(window.hwnd, widget_id), text)
    win32gui.UpdateWindow(window.hwnd)

def get_widget_text(window, widget_id):
    """ Get the text in a text field widget. """
    return win32gui.GetWindowText(win32gui.GetDlgItem(window.hwnd, widget_id))

def push_button(window, widget_id):
    """ Push a button with a given ID. """
    win32gui.SendMessage(window.hwnd, win32con.WM_COMMAND, win32api.MAKEWORD(widget_id, win32con.BN_CLICKED), 0)

def left_click_tray_icon(window, widget_id):
    """ Simulate a left click on the tray icon. """
    win32gui.PostMessage(window.hwnd, win32con.WM_USER+20, widget_id, win32con.WM_LBUTTONUP)

def right_click_tray_icon(window, widget_id):
    """ Simulate a right click on the tray icon. """
    win32gui.PostMessage(window.hwnd, win32con.WM_USER+20, widget_id, win32con.WM_LBUTTONUP)

def is_widget_enabled(window, widget_id):
    """ Find out if the widget is enabled. """
    return win32gui.IsWindowEnabled(win32gui.GetDlgItem(window.hwnd, widget_id))

def is_widget_visible(window, widget_id):
    """ Find out if the widget is visible. """
    return win32gui.IsWindowVisible(win32gui.GetDlgItem(window.hwnd, widget_id))

def get_menu_item_launch_id(window, menu_folder, item_number):
    """ Return the launch id associated with the given menu item. """
    if win32gui.GetMenuItemID(menu_folder.view, item_number) >= 0:
        return window.menu.launchidmap[win32gui.GetMenuItemID(menu_folder.view, item_number)]
    else:
        return win32gui.GetMenuItemID(menu_folder.view, item_number)
    
def get_menu_launch_id_from_index(window, index):
    """ Get the actual launch id"""
    return index
    
def get_menu_item_count(menu_folder):
    """ Return the number of menu items in a given menu folder. """
    return win32gui.GetMenuItemCount(menu_folder.view)


def select_menu_item(window, widget_id):
    """ Select the menu item given by widget_id. 
        TODO: This is a temporary hack - modify for a better fit to reality.
        TODO: This should simulate a click on the menu item.
        TODO: Now it just adds the launch id to the launch list. 
    """
    window.controller.add_to_launch_list(widget_id.launchid)

def push_key_down():
    """ Simulate a key press (arrow down). """
    win32api.keybd_event(win32con.VK_DOWN, 0, 0, 0)
    
def push_key_right():
    """ Simulate a key press (arrow right). """
    win32api.keybd_event(win32con.VK_RIGHT, 0, 0, 0)
    
def push_key_return():
    """ Simulate a key press (return). """
    win32api.keybd_event(win32con.VK_RETURN, 0, 0, 0)
    
def get_progress_bar_progress(window, widget_id):
    """ Get an integer status of the progression. """
    return win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.PBM_GETPOS, 0, 0)
 
def get_selection_list_item_count(window, widget_id):
    """ Get the amount of items currently displayed. """
    return win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_GETITEMCOUNT)

def is_window_visible(window):
    """ Find out if a window is currently visible. """
    if hasattr(window, 'hwnd'):
        return win32gui.IsWindowVisible(window.hwnd)
    else:
        return win32gui.IsWindowVisible(window);

def get_selection_list_item_text(window, widget_id, index):
    """ Get the text from the item in the selection list given by index. """
    buf,_ = win32gui_struct.EmptyLVITEM(item=index, subitem=0)
    win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_GETITEMTEXT, index, buf)
    tmp = win32gui_struct.UnpackLVITEM(buf)
    return tmp[4]

def get_selection_list_item_details(window, widget_id, index):
    """ Get the details-text from the item in the selection list given by index."""
    buf,_ = win32gui_struct.EmptyLVITEM(index, 1)
    win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_GETITEMTEXT, index, buf)
    tmp = win32gui_struct.UnpackLVITEM(buf)
    return tmp[4]

def get_selection_list_item_selection_selected(window, widget_id, index):
    """ Get whether the indexed item is selected. """
    return win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_GETITEMSTATE, index, commctrl.LVIS_STATEIMAGEMASK) == commctrl.INDEXTOSTATEIMAGEMASK(2)

def get_window_frame_text(window):
    """ Get the text in the windows frame. """
    return win32gui.GetWindowText(window.hwnd)

def set_selection_list_highlighted_item(window, widget_id, index):
    """ Set which item is highlighted in the selection list. """
    lv_item, _ = win32gui_struct.PackLVITEM(item=0, state=commctrl.LVIS_SELECTED, stateMask=commctrl.LVIS_SELECTED, text="test")
    win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_SETITEMSTATE, index, lv_item)
    win32gui.SendMessage(win32gui.GetDlgItem(window.hwnd, widget_id), commctrl.LVM_ENSUREVISIBLE, index, False)
