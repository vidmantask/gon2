"""
MFC version of the GUI for login.
"""
import win32gui
import win32con
import ctypes

import components.presentation.gui.mfc.structure.mfc_edit_balloon_tip as mfc_edit_balloon_tip
import lib.dictionary

from components.presentation.login import LoginView
from components.presentation.gui.mfc.mfc_base_view import MfcBaseView
from components.presentation.gui.mfc.area.mfc_header_area import MfcHeaderArea
from components.presentation.gui.mfc.area.mfc_login_area import MfcLoginArea
from components.presentation.gui.mfc.area.mfc_button_area import MfcButtonArea

class MfcLoginView(MfcBaseView, LoginView):
    """ MFC based view of the login. """
    def __init__(self, model, common, name, configuration):
        LoginView.__init__(self, model, common, name, configuration)
        MfcBaseView.__init__(self, "LoginWindow")
        
        # Currently visible model.
        self.current_state = self.controller.ID_STATE_LOGIN
        # Create local message map
        self.message_map = {
            win32con.WM_INITDIALOG: self._on_init_dialog,
            win32con.WM_COMMAND: self._on_command,
            win32con.WM_SIZE: self._on_resize,
            win32con.WM_CLOSE: self.hide,
            win32con.WM_DESTROY: self._on_destroy,
            win32con.WM_GETMINMAXINFO: self._on_min_max_info
        }
        self.style = win32con.WS_POPUPWINDOW | win32con.WS_CAPTION | win32con.DS_SETFONT | win32con.WS_THICKFRAME | win32con.WS_MINIMIZEBOX
        # Create the window.
        self.create_window(self.message_map, self.style, self.configuration.gui_image_path)
        self.disable_close_button()
        # Create a balloon for edit fields.
        self.balloontip = mfc_edit_balloon_tip.EDITBALLOONTIP()
        self.balloontip.cbStruct = ctypes.sizeof(self.balloontip) # Structure size
        self.balloontip.ttiIcon = mfc_edit_balloon_tip.TTI_WARNING
        # Create areas
        self.header = MfcHeaderArea(self.hwnd, self.hinst, self.configuration)
        self.login = MfcLoginArea(self.hwnd, self.hinst, self.configuration)
        self.buttons = MfcButtonArea(self.hwnd, self.hinst, self.configuration)
        # Change settings according to preferences. 
        self.controller.configure(self.configuration)
        # Get ready for action!
        self.update()
        self.resize()

    def _on_command(self, hwnd, message, wparam, lparam): #@UnusedVariable
        """ Handles user events. """
        # When the Cancel button is clicked
        if wparam == MfcButtonArea.ID_BUTTON_CANCEL:
            self.hide()
            self.clear()
        # When the OK button is clicked
        elif wparam == MfcButtonArea.ID_BUTTON_NEXT:
            # If no user name was given....
            if win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_LOGIN_TEXTFIELD)) == "":
                self._set_ballon_content(self.dictionary._("No user name entered"), self.dictionary._("Please provide a user name to identify yourself."))
                win32gui.SendMessage(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_LOGIN_TEXTFIELD), mfc_edit_balloon_tip.EM_SHOWBALLOONTIP, 0, self.balloontip)
                wparam = None # No button click!
            else:
                # If there is a request for a new password ...
                if self.current_state == self.controller.ID_STATE_CHANGE_LOGIN:
                    # If no password has been given...
                    if win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD)) == "":
                        self._set_ballon_content(self.dictionary._("No password entered"), self.dictionary._("Please enter your current password to validate your identity."))
                        win32gui.SendMessage(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD), mfc_edit_balloon_tip.EM_SHOWBALLOONTIP, 0, self.balloontip)
                        wparam = None # No button click!
                    # If the new password is not empty...
                    elif win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD)) != "":
                        # If the new passwords match...
                        if win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD)) == win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD)):
                            self.hide()
                            self.controller.set_password_request(win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD)).decode('mbcs', 'ignore'))
                            self.controller.set_credentials(win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_LOGIN_TEXTFIELD)).decode('mbcs', 'ignore'),
                                                            win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD)).decode('mbcs', 'ignore'))
                            self.clear()
                        # If the new passwords do not match...
                        else:
                            self._set_ballon_content(self.dictionary._("Passwords do not match"), self.dictionary._("Please make sure that the new password and the confirmed password are identical."))
                            win32gui.SendMessage(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD), mfc_edit_balloon_tip.EM_SHOWBALLOONTIP, 0, self.balloontip)
                            wparam = None # No button click!
                    # If the new password is empty...
                    else:
                        self._set_ballon_content(self.dictionary._("Passwords can not be empty"), self.dictionary._("Please enter a new password."))
                        win32gui.SendMessage(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD), mfc_edit_balloon_tip.EM_SHOWBALLOONTIP, 0, self.balloontip)
                        wparam = None # No button click!
                else: # State is ordinary login
                    if win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD)) == "":
                        self._set_ballon_content(self.dictionary._("No password entered"), self.dictionary._("Please enter your password to validate your identity."))
                        win32gui.SendMessage(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD), mfc_edit_balloon_tip.EM_SHOWBALLOONTIP, 0, self.balloontip)
                        wparam = None # No button click!
                    else:
                        self.hide()
                        self.controller.set_credentials(win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_LOGIN_TEXTFIELD)).decode('mbcs', 'ignore'),
                                                        win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD)).decode('mbcs', 'ignore'))
                        self.clear()
        # Call super's generic _on_command handler...
        super(MfcLoginView, self)._on_command(hwnd, message, wparam, lparam)

    def _on_resize(self, hwnd, message, wparam, lparam):
        """ Respond to user resize events. """
        [_, _, new_window_width, new_window_height] = win32gui.GetClientRect(self.hwnd)
        self.resize(width=new_window_width, height=new_window_height)

    def _set_ballon_content(self, headline, text):
        """ Add headline and text to a balloon.
        
            This balloon can be displayed whenever we
            want to display extra information to the user.
            For instance if the password in the confirmation
            field does not match the one in the new password 
            field.
            
            @since: 5.5
        """
        self.balloontip.pszTitle = headline
        self.balloontip.pszText = text

    def clear(self):
        """ Clear model settings for button signals. """
        win32gui.SetWindowText(win32gui.GetDlgItem(self.hwnd, MfcLoginArea.ID_PASSWORD_TEXTFIELD), '')
        win32gui.SetWindowText(win32gui.GetDlgItem(self.hwnd, MfcLoginArea.ID_NEW_PASSWORD_TEXTFIELD), '')
        win32gui.SetWindowText(win32gui.GetDlgItem(self.hwnd, MfcLoginArea.ID_NEW_PASSWORD_CONFIRM_TEXTFIELD), '')

    def update(self):
        """ Update anything relevant in this view. """
        self.header.update(self.model)
        self.login.update(self.model) 
        self.buttons.update(self.model)
        if self.current_state != self.model.state:
            if self.model.state == self.controller.ID_STATE_LOGIN:
                self.resize()
            elif self.model.state == self.controller.ID_STATE_CHANGE_LOGIN:
                self.resize(height=400)
        self.current_state = self.model.state
        # Do the changes.
        win32gui.UpdateWindow(self.hwnd)        
        win32gui.SendMessage(self.hwnd, win32con.WM_NEXTDLGCTL, self.login.userfield, True)
        
    def resize(self, left=0, top=0, width=375, height=300):
        """ Set sizes and relative positions of window elements. """
        super(MfcLoginView, self).resize(left, top, width, height)
        
        self.header.resize(left, top) 
        self.buttons.resize(top=self.height-self.buttons.height, width=self.width)
        self.login.resize(top=top + self.header.height, height = self.height - self.header.height - self.buttons.height, width=self.width)

        win32gui.UpdateWindow(self.hwnd)


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.common import CommonModel
    from components.presentation.login import LoginModel

    def test():
        if loginmodel.cancel_clicked:
            print "Cancel clicked"
        if loginmodel.next_clicked:
            print "Next clicked" 
        if loginmodel.requestedpassword != None:
            print "Reading model data: (username=" + loginmodel.username + \
                                      " password=" + loginmodel.password + \
                                      " newpass=" + loginmodel.requestedpassword + ")"
        else:
            print "Reading model data: (username=" + loginmodel.username + \
                                      " password=" + loginmodel.password + \
                                      " newpass=None" + ")"

    dictionary = lib.dictionary.Dictionary()

    # Create models
    commonmodel = CommonModel()
    loginmodel = LoginModel(dictionary)
    loginmodel.subscribe(test)
    # Create GUI parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.dictionary = dictionary
    configuration.gui_image_path = '..'
    configuration.gui_login_default_button = configuration.OPTION_GUI_DEFAULT_BUTTON_VALUE_CANCEL
    #configuration.gui_login_default_button = configuration.OPTION_GUI_DEFAULT_BUTTON_VALUE_NEXT
    
    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    loginview = MfcLoginView(loginmodel, commonview.handles, 'loginview', configuration)
    
    loginview.controller.set_suggested_username("Nulle")
    #loginview.controller.set_lock_username_field(True)
 
    #loginview.controller.set_state(loginview.controller.ID_STATE_LOGIN)
    loginview.controller.set_state(loginview.controller.ID_STATE_CHANGE_LOGIN)
    
    loginview.controller.set_cancel_allowed(False)
    #loginview.controller.set_cancel_allowed(True)
    loginview.controller.set_default_button(loginview.controller.ID_NEXT_BUTTON)
    #loginview.controller.set_default_button(loginview.controller.ID_CANCEL_BUTTON)
    
    # Start Gui
    loginview.display()
    commonview.display()
    
    

