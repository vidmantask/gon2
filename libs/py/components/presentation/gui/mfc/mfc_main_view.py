"""
MFC version of the GUI for the main window.
"""

import win32gui
import win32con

import lib.dictionary

from components.presentation.main import MainView
from components.presentation.gui.mfc.mfc_base_view import MfcBaseView
from components.presentation.gui.mfc.area.mfc_status_bar_area import MfcStatusBarArea

class MfcMainView(MfcBaseView, MainView):
    """ Mfc based view of the login. """
    def __init__(self, model, common, name, configuration):
        MainView.__init__(self, model, common, name, configuration)
        MfcBaseView.__init__(self, "MainWindow")

        # Create local message map
        self.message_map = {
            win32con.WM_INITDIALOG: self._on_init_dialog,
            win32con.WM_COMMAND: self._on_command,
            win32con.WM_SIZE: self._on_resize,
            win32con.WM_CLOSE: self.hide,
            win32con.WM_DESTROY: self._on_destroy,
        }
        self.style = win32con.WS_POPUP | win32con.WS_CAPTION | win32con.DS_SETFONT | win32con.WS_THICKFRAME | win32con.WS_MINIMIZEBOX
        # Create the window.
        self.create_window(self.message_map, self.style, self.configuration.gui_image_path)
        #self.disable_close_button()
        
        # Create a status bar.
        self.statusbar = MfcStatusBarArea(self.hwnd, self.hinst, configuration)
        #self.resize()


        # TEST START
        
        #print "TEST", win32gui.GetMenu(self.hwnd) 
        
        
        
        mainmenu = win32gui.CreateMenu()
        
        
        
        popper = win32gui.CreatePopupMenu()
        
        win32gui.AppendMenu(mainmenu, win32con.MF_POPUP, popper, "File")
        
        win32gui.AppendMenu(popper, win32con.MF_POPUP, 100, "Juhuu")
        
        
        win32gui.SetMenu(self.hwnd, mainmenu)
        
        #mainmenu = win32gui.GetMenu(self.hwnd)
        #print "MENU", mainmenu
        #win32gui.AppendMenu(mainmenu, win32con.MF_STRING, 666, 'Hello')
        
        #win32gui.SetMenu(self.hwnd, mainmenu)
        
        
        
        
        # TEST END


    def _on_command(self, hwnd, message, wparam, lparam): #@UnusedVariable
        """ Handles user events. """
        print "mfc_main_menu._on_command", hwnd, message, wparam, lparam

    def _on_resize(self, hwnd, message, wparam, lparam):
        """ Respond to user resize events. """
        [_, _, window_width, window_height] = win32gui.GetClientRect(self.hwnd)
        self.resize(width=window_width, height=window_height)
        
    def update(self):
        """ Update anything relevant in this view. """
        self.statusbar.update(self.model)

    def resize(self, left=0, top=0, width=375, height=300):
        """ Set sizes and relative positions of window elements. """
        print "resize", left, top, width, height
        self.statusbar.resize(left, top, width, height)
        #win32gui.SetWindowPos(self.hwnd, 0, 0, 0, width + self.frame_width, height + self.frame_height, win32con.SWP_NOMOVE)
        win32gui.UpdateWindow(self.hwnd)


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.common import CommonModel
    from components.presentation.main import MainModel

    def listener():
        print "sound"
            
    # Create models
    commonmodel = CommonModel()
    mainmodel = MainModel(lib.dictionary.Dictionary())
    mainmodel.subscribe(listener)
    # Create GUI parts
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()
    
    handles = None
    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    mainview = MfcMainView(mainmodel, commonview.handles, 'mainview', configuration)
    
    mainview.controller.set_status_bar_text("Kowabunga!")
    mainview.controller.add_status_bar_service(1, "PrintService", None)
    mainview.controller.add_status_bar_service(2, "MailService", None)
    mainview.controller.add_status_bar_service(3, "FaxService", None)
    mainview.controller.set_status_bar_connection_status(mainview.controller.ID_STATUS_BAR_STATUS_CONNECTED)
    
    # Start GUI
    mainview.display()
    commonview.display()
    