import commctrl
import ctypes

LVM_SETTILEINFO = commctrl.LVM_FIRST + 164
LVM_GETTILEINFO = commctrl.LVM_FIRST + 165
# MFC List view messages
LVM_SETVIEW = commctrl.LVM_FIRST + 142 
LVM_SETTILEVIEWINFO = commctrl.LVM_FIRST + 162
LVM_GETTILEVIEWINFO = commctrl.LVM_FIRST + 163
LVM_ENABLEGROUPVIEW = commctrl.LVM_FIRST + 157
# MFC List view - view setting
LV_VIEW_TILE = 4
# MFC List view tile view info masks
LVTVIM_TILESIZE = 1
LVTVIM_COLUMNS = 2
LVTVIM_LABELMARGIN = 4
# MFC List view tile view info flags
LVTVIF_AUTOSIZE = 0 
LVTVIF_FIXEDWIDTH = 1
LVTVIF_FIXEDSIZE = 3
LVTVIF_EXTENDED = 4
# Arrays for use in the LVTILEINFO Structure.
UINT_ARRAY_5 = ctypes.c_uint * 5
INT_ARRAY_5 = ctypes.c_int *5

class LVTILEVIEWINFO(ctypes.Structure):
    """ Structure for LVTILEVIEWINFO
        
        @see: http://msdn.microsoft.com/en-us/library/bb774768(VS.85).aspx
        @since: 5.4
    """
    _fields_ = [("cbSize", ctypes.wintypes.UINT),
                ("dwMask", ctypes.wintypes.DWORD),
                ("dwFlags", ctypes.wintypes.DWORD),
                ("sizeTile", ctypes.wintypes.SIZE),
                ("cLines", ctypes.wintypes.INT),
                ("rcLabelMargin", ctypes.wintypes.RECT)
                ]

class LVTILEINFO(ctypes.Structure):
    """ Structure for LVTILEINFO
    
        Do not use the 'piColFmt' entry because it will not
        work on Win XP. We only use left aligned text anyway
        and that seems to be the default.
        
        @see: http://msdn.microsoft.com/en-us/library/bb774766(VS.85).aspx
        @since: 5.4
    """
    _fields_ = [("cbSize", ctypes.wintypes.UINT),
                ("iItem", ctypes.wintypes.INT),
                ("cColumns", ctypes.wintypes.UINT),
                ("puColumns", ctypes.POINTER(ctypes.c_uint * 5))
                ]
