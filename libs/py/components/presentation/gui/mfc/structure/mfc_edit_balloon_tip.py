#import ctypes
import ctypes.wintypes


# MFC Edit field balloon tip messages.
EM_SHOWBALLOONTIP = 0x1503
# MFC Tool Tip Icons.
TTI_INFO = 1
TTI_WARNING = 2
TTI_ERROR = 3

class EDITBALLOONTIP(ctypes.Structure):
    """ Structure for edit field balloontips. 
    
        @since: 5.5
    """
    _fields_ = [("cbStruct", ctypes.wintypes.DWORD),
                ("pszTitle", ctypes.wintypes.LPWSTR),
                ("pszText", ctypes.wintypes.LPWSTR),
                ("ttiIcon", ctypes.wintypes.INT)
                ]
