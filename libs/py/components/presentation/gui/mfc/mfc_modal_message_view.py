# -*- coding: iso-8859-1 -*-
"""
MFC version of the GUI for modal messages.

    Downloaded COMTYPES from: http://sourceforge.net/projects/comtypes/ 
    comtypes is a pure Python, lightweight COM client 
    and server framework, based on the ctypes Python 
    FFI package.
"""
import win32gui
import win32con

import lib.dictionary

from components.presentation.modal_message import ModalMessageView
from components.presentation.gui.mfc.mfc_base_view import MfcBaseView
from components.presentation.gui.mfc.area.mfc_html_area import MfcHtmlArea
from components.presentation.gui.mfc.area.mfc_button_area import MfcButtonArea

class MfcModalMessageView(MfcBaseView, ModalMessageView):
    """ Mfc based view of the modal messages. """
    def __init__(self, model, common, name, configuration):
        ModalMessageView.__init__(self, model, common, name, configuration)
        MfcBaseView.__init__(self, "ModalMessageView") 
        # Currently visible model
        self.current_size = None
        # Style for the window frame.
        self.style = win32con.WS_POPUP | win32con.WS_CAPTION | win32con.DS_SETFONT | win32con.WS_THICKFRAME
        # Create local message map
        self.message_map = {
                            win32con.WM_INITDIALOG: self._on_init_dialog,
                            win32con.WM_COMMAND: self._on_command,
                            win32con.WM_SIZE: self._on_resize,
                            win32con.WM_CLOSE: self._on_hide,
                            win32con.WM_DESTROY: self._on_destroy,
                            win32con.WM_GETMINMAXINFO: self._on_min_max_info
                            }
        # Create the window.
        self.create_window(self.message_map, self.style, self.configuration.gui_image_path)
        # Create areas.
        self.htmlview = MfcHtmlArea(self.hwnd, self.hinst)
        self.buttons = MfcButtonArea(self.hwnd, self.hinst, self.configuration)
        # Get ready for action!
        
        self.update()
        self.resize()

    def _on_command(self, hwnd, message, wparam, lparam): #@UnusedVariable
        """ Handles button events. """
        if wparam == MfcButtonArea.ID_BUTTON_NEXT:
            self._on_hide(self.hwnd, "", 0, 0)
        elif wparam == MfcButtonArea.ID_BUTTON_CANCEL:
            self._on_hide(self.hwnd, "", 0, 0)
        # Call super's generic _on_command handler...
        super(MfcModalMessageView, self)._on_command(hwnd, message, wparam, lparam)

    def _on_resize(self, hwnd, message, wparam, lparam):
        """ Respond to user resize events. """
        [_, _, window_width, window_height] = win32gui.GetClientRect(self.hwnd)
        self.resize(width=window_width, height=window_height)

    def _on_hide(self, hwnd, message, wparam, lparam): #@UnusedVariable
        """ Hide the window from the user (recycling). """
        win32gui.ShowWindow(hwnd, win32con.SW_HIDE)

    def _on_destroy(self, *params):
        """ Destroy method called by messages via the GUI loop. """
        self.htmlview.destroy()
        self.destroy()

        try:
            win32gui.DestroyWindow(self.hwnd)
            self.hwnd = None
            win32gui.UnregisterClass(self.classAtom, self.hinst)
            self.hinst = None
        except win32gui.error:
            pass

    def update(self):
        """ Update the things that can be changed via the controller. """
        if self.current_size != self.model.size:
            if self.model.size == self.controller.ID_LARGE_SIZE:
                self.set_min_window_size(width=500, height=600 + self.frame_height)
                self.resize(0, 0, 500, 600)
            elif self.model.size == self.controller.ID_SMALL_SIZE:
                self.set_min_window_size(width=500, height=200 + self.frame_height)
                self.resize(0, 0, 500, 200)
            self.current_size = self.model.size
        self.htmlview.update(self.model)
        self.buttons.update(self.model)
        # Send a paint message to repaint invalidated areas.
        win32gui.InvalidateRect(self.hwnd, None, False)
        win32gui.UpdateWindow(self.hwnd)

    def resize(self, left=0, top=0, width=500, height=200):
        """ Set sizes and relative positions of window elements. """
        super(MfcModalMessageView, self).resize(left, top, width, height)
        self.buttons.resize(top=height - self.buttons.height, width=width)
        self.htmlview.resize(0, 0, width, height - self.buttons.height)
        win32gui.InvalidateRect(self.hwnd, None, False)
        win32gui.UpdateWindow(self.hwnd)


if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView   
    from components.presentation.common import CommonModel
    from components.presentation.modal_message import ModalMessageModel

    def test():
        pass
        #print "Subscriber notified\n", 
        #print "Model status for OK button:", modalmessagemodel.ok_allowed
        #print "Model status for Cancel button:", modalmessagemodel.cancel_allowed
        
    # Create models
    commonmodel = CommonModel()
    modalmessagemodel = ModalMessageModel(lib.dictionary.Dictionary())
    modalmessagemodel.subscribe(test)
    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.set('gui', 'image_path', '..')
    configuration.dictionary = lib.dictionary.Dictionary()
    
    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    modalmessageview = MfcModalMessageView(modalmessagemodel, commonview.handles, 'modalmessageview', configuration)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_LARGE_SIZE)
    #modalmessageview.controller.set_default_button(modalmessageview.controller.ID_CANCEL_BUTTON)
    modalmessageview.display()

    #testtext = "This is a test of text strings in the G/On modal message view. Testing a very long sentence to make sure the margins are correct, and that the window handles up to three lines of text"
    #testtext = "This is a test of short string" 
    
    message = u'A new version of your G/On software is available.<br>It may take a little while to install it. So please be patient. ���.'
    headline = u'An update is available'
    testtext = '<b>' + headline + '</b><br><br>' + message
    
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_SMALL_SIZE) 

    modalmessageview.controller.set_text(testtext)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_LARGE_SIZE)
    #modalmessageview.controller.set_size(modalmessageview.controller.ID_SMALL_SIZE)

    #modalmessageview.controller.set_text('<b>The token already contains a G/On Client Installation</b><br><br>Warning: If you click OK, the token will need to be re-enrolled before it can be used. Please check with your administrator, before clicking OK.')

    #modalmessageview.controller.set_next_allowed(False)
    #modalmessageview.controller.set_cancel_allowed(True)
    #modalmessageview.controller.set_next_label("Install")
    #modalmessageview.controller.set_cancel_label("Not now")
    #modalmessageview.controller.set_default_button(modalmessageview.controller.ID_NEXT_BUTTON)
    
    #modalmessageview.controller.set_next_allowed(False)
    #modalmessageview.hide()
     
    #modalmessageview.display()
        
    # Start Gui
    commonview.display()
