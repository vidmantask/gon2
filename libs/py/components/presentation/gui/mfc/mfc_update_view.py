"""
Mfc version of the GUI for update.

"""
import sys
import win32gui
import win32con
import win32api
import commctrl
import ctypes
import win32gui_struct


import lib.dictionary

# Import model
from components.presentation.update import UpdateView
# Import MFC Tools
#from components.presentation.gui.mfc.mfc_tools import *
# Import area parts
from components.presentation.gui.mfc.mfc_base_view import MfcBaseView
from components.presentation.gui.mfc.area.mfc_button_area import MfcButtonArea
from components.presentation.gui.mfc.area.mfc_header_area import MfcHeaderArea
from components.presentation.gui.mfc.area.mfc_folder_browser_area import MfcFolderBrowserArea
from components.presentation.gui.mfc.area.mfc_progress_area import MfcProgressArea
from components.presentation.gui.mfc.area.mfc_text_area import MfcTextArea
from components.presentation.gui.mfc.area.mfc_phase_status_area import MfcPhaseStatusArea
from components.presentation.gui.mfc.area.mfc_selection_area import MfcSelectionArea

class MfcUpdateView(MfcBaseView, UpdateView):

    def __init__(self, model, common, name, configuration):
        UpdateView.__init__(self, model, common, name, configuration)
        MfcBaseView.__init__(self, "SelectionWindow")

        # Currently visible model.
        self.current_window_frame_text = self.model.window_frame_text
        self.current_show_selection_list = self.model.show_selection_list
        self.current_info_mode = self.model.info_mode
        # Initial height of individual parts
        self.initial_info_area_height = 120
        self.initial_selection_area_height = 240
        # Style
        self.style = win32con.WS_POPUP | win32con.WS_CAPTION | win32con.DS_SETFONT | win32con.WS_SYSMENU | win32con.WS_MINIMIZEBOX | win32con.WS_THICKFRAME
        # Setup the message map.
        WM_QueryCancelAutoPlay = win32gui.RegisterWindowMessage("QueryCancelAutoPlay")
        self.message_map = {
            win32con.WM_INITDIALOG: self._on_init_dialog,
            win32con.WM_CTLCOLORSTATIC: self._on_color_ctl,
            win32con.WM_CLOSE: self.hide,
            win32con.WM_SIZE: self._on_resize,
            win32con.WM_NOTIFY: self._on_wm_notify,
            win32con.WM_COMMAND: self._on_command,
            win32con.WM_DESTROY: self._on_destroy,
            win32con.WM_GETMINMAXINFO: self._on_min_max_info,
            WM_QueryCancelAutoPlay : self._on_QueryCancelAutoPlay,
        }
        # Allow this message in administrator mode (Only useful on Vista and 7).
        # - so that we always cancel auto-play. 
        try:
            MSGFLT_ADD = 1
            #MSGFLT_REMOVE = 2
            ctypes.windll.user32.ChangeWindowMessageFilter(WM_QueryCancelAutoPlay, MSGFLT_ADD)
        except:
            pass
        # Create the window with the templates layout
        self.create_window(self.message_map, self.style, self.configuration.gui_image_path)
        self.disable_close_button()
        # Create parts for the update window.
        self.header = MfcHeaderArea(self.hwnd, self.hinst, self.configuration)
        self.phaseboxtexts = MfcPhaseStatusArea(self.hwnd, self.hinst, self.configuration)        
        self.folderbrowser = MfcFolderBrowserArea(self.hwnd, self.hinst)
        self.progress = MfcProgressArea(self.hwnd, self.hinst)
        self.info = MfcTextArea(self.hwnd, self.hinst)
        self.selections = MfcSelectionArea(self.hwnd, self.hinst, self.configuration)
        self.buttons = MfcButtonArea(self.hwnd, self.hinst, self.configuration)
        # Prepare for action!
        self.update()
        self.resize()

    def _on_QueryCancelAutoPlay(self, hwnd, *_):
        win32gui.SetWindowLong(hwnd, win32con.DWL_MSGRESULT, 1)
        return True
        
    def _on_resize(self, hwnd, message, wparam, lparam):
        """ Handle minimize, restore and resize messages. """
        if wparam == win32con.SIZE_MINIMIZED:
            self.controller.set_minimized(True)
        # Handles restore from minimized and resizing...
        elif wparam == win32con.SIZE_RESTORED:
            if self.model.minimized:
                self.controller.set_minimized(False)
            [_, _, window_width, window_height] = win32gui.GetClientRect(hwnd)
            self.resize(width=window_width, height=window_height)

    def _on_color_ctl(self, hwnd, message, wparam, lparam):
        """ Control color on specific controls backgrounds. 
        
            Make sure that pending phaseboxtexts are displayed as 
            gray and the others as black. Phase texts has
            id's that are 1000 or above.
            Also make sure that most components backgrounds
            are white.
        """
        if win32gui.GetDlgCtrlID(lparam) >= 1000: 
            for index, phase in enumerate(self.phaseboxtexts.phaselist):
                if phase['state'] == 'pending' and lparam == win32gui.GetDlgItem(hwnd, 1000 + index):
                    win32gui.SetTextColor(wparam, self.gray_rgb)
                elif (phase['state'] == 'active' or phase['state'] == 'complete') and lparam == win32gui.GetDlgItem(hwnd, 1000 + index):
                    win32gui.SetTextColor(wparam, self.black_rgb)
                win32gui.UpdateWindow(lparam)
        return self.white_brush

    def _on_command(self, hwnd, message, wparam, lparam):
        """ Notifications for button pushes. """
        if wparam == MfcFolderBrowserArea.ID_LOCATE_FOLDER_BUTTON:
            self.folderbrowser.browse_for_folder(self.controller)
        elif win32api.HIWORD(wparam) == win32con.EN_CHANGE and win32api.LOWORD(wparam) == MfcFolderBrowserArea.ID_FOLDER_PATH: 
            self.controller.set_info_user_written_folder_path_without_notification(win32gui.GetWindowText(win32gui.GetDlgItem(hwnd, MfcFolderBrowserArea.ID_FOLDER_PATH)).decode('mbcs', 'ignore'))
        # Call super's generic _on_command handler...
        super(MfcUpdateView, self)._on_command(hwnd, message, wparam, lparam)

    def _on_wm_notify(self, hwnd, message, wparam, lparam):
        """ Notifications for most changes in the GUI. """
        if self.processmessages and (win32gui.LOWORD(wparam) == MfcSelectionArea.ID_SELECTION_TABLE):
            self.wm_hwnd, self.wm_id, self.wm_message = win32gui_struct.UnpackWMNOTIFY(lparam)
            if self.wm_message == commctrl.LVN_ITEMCHANGED:
                self.processmessages = False
                self.selections.on_change(self.wm_hwnd, self.wm_id, self.wm_message, self.controller)
                self.processmessages = True
            if self.wm_message == commctrl.NM_CLICK:
                (major_version, _, _, _, _) = sys.getwindowsversion()
                if major_version <= 5:
                    self.processmessages = False
                    self.selections.on_click(self.wm_hwnd, self.wm_id, self.wm_message, self.controller, lparam)
                    self.processmessages = True

    def update(self):
        """ Update all parts in the update view. 
        
            This is called because of a change in the model that 
            this method subscribes to.
        """
        if self.controller.get_gui_should_update_on_alerts():
            # Set the window frame text
            if self.model.window_frame_text != self.current_window_frame_text:
                win32gui.SetWindowText(self.hwnd, self.model.window_frame_text)
                self.current_window_frame_text = self.model.window_frame_text
            # Check if a selection list should be displayed.
            if self.current_show_selection_list != self.model.show_selection_list:
                if self.model.show_selection_list:
                    self.selections.show()
                else:
                    self.selections.hide()
                self.current_show_selection_list = self.model.show_selection_list
                # Resize the window frame according to the new settings...
                [_, _, _window_width, _window_height] = win32gui.GetClientRect(self.hwnd)                
                if self.current_show_selection_list:
                    #self.resize(width = _window_width, height = _window_height + self.initial_selection_area_height)
                    #self.resize(width = _window_width, height = self.minimumheight + self.initial_selection_area_height)
                    if self.selectionareaadded:
                        self.set_min_window_size(height=self.minimumheight)
                        self.resize(width = _window_width, height = _window_height)
                        self.selectionareaadded = False
                    else:
                        self.set_min_window_size(height=self.minimumheight + self.initial_selection_area_height)
                        self.resize(width = _window_width, height = _window_height + self.initial_selection_area_height)
                        self.selectionareaadded = True

                else:
                    #self.resize(width = _window_width, height = _window_height - self.initial_selection_area_height)
                    #self.set_min_window_size(height=self.minimumheight - self.initial_selection_area_height)
                    if self.selectionareaadded:
                        self.set_min_window_size(height=self.minimumheight - self.initial_selection_area_height)
                        self.resize(width = _window_width, height = _window_height - self.initial_selection_area_height)
                        self.selectionareaadded = False
                    else:
                        self.set_min_window_size(height=self.minimumheight)
                        self.resize(width = _window_width, height = _window_height)
                        self.selectionareaadded = True
                    
            # Set the mode and elements that depends on the mode.
            if self.current_info_mode != self.model.info_mode:
                # Initially hide them all...
                self.info.hide()
                self.folderbrowser.hide()
                self.progress.hide() 
                # Then show the one we want...
                if self.model.info_mode == 'progress':
                    self.progress.show()
                elif self.model.info_mode == 'free-text' or self.model.info_mode == 'button-text':
                    self.info.show()
                elif self.model.info_mode == 'get-folder':
                    self.folderbrowser.show()
                self.current_info_mode = self.model.info_mode
            # Set the button text into info if that mode is selected (updated in info.update()).
            if self.model.info_mode == self.controller.ID_MODE_INFORMATION_BUTTON_TEXT:
                if self.model.info_text != self.model.button_info_text:
                    self.model.info_text = self.model.button_info_text
            # Update local parts of the update view.
            self.header.update(self.model)
            self.phaseboxtexts.update(self.model) 
            self.selections.update(self.model, self.controller)
            self.info.update(self.model)
            self.folderbrowser.update(self.model)
            self.progress.update(self.model)
            self.buttons.update(self.model)
            # Send a general update to the parent window.        
            win32gui.UpdateWindow(self.hwnd)

    def resize(self, left=0, top=0, width=450, height=300):
        """ Resize the main window depending on the shown content.
        
            @since: 5.5
        """
        # Resize... (standard heights)
        # self.header.resize(left, top, width)
        super(MfcUpdateView, self).resize(left, top, width, height)
        
        self.header.resize(left, top)
        self.phaseboxtexts.resize(top=self.header.height, width=width)
        self.buttons.resize(top=height - self.buttons.height, width=width)
        # Calculate sizes...
        fixed = self.header.height + self.phaseboxtexts.height + self.buttons.height
        if self.current_show_selection_list:
            info_height = self.initial_info_area_height
            selection_height = height - fixed - info_height
        else: 
            info_height = height - fixed
            selection_height = 0
        # Resize (flexible heights)
        self.folderbrowser.resize(top=self.header.height + self.phaseboxtexts.height, width=width, height=info_height)
        self.progress.resize(top=self.header.height + self.phaseboxtexts.height, width=width, height=info_height)
        self.info.resize(top=self.header.height + self.phaseboxtexts.height, width=width, height=info_height)
        self.selections.resize(top=self.header.height + self.phaseboxtexts.height + info_height, width=width, height=selection_height)
        # Resize main frame.
        win32gui.UpdateWindow(self.hwnd)



# TESTCASE ---------------------------------------------------------
# This is here for easy testing. 
#

if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.common import CommonModel
    from components.presentation.update import UpdateModel

    dictionary = lib.dictionary.Dictionary()
    
    commonmodel = CommonModel()
    updatemodel = UpdateModel(dictionary)
    
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = dictionary

    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    updateview = MfcUpdateView(updatemodel, commonview.handles, 'updateview', configuration)

    current_path = updateview.controller.get_info_user_selected_folder_path()
    
    def test():
#        rect2 = ctypes.wintypes.RECT()
#        win32gui.SendMessage(updateview.selections.list, commctrl.LVM_GETVIEWRECT, 0, rect2)
#        print "VIEW:", rect2.left, rect2.top, rect2.right, rect2.bottom
        
        if updateview.controller.get_info_user_selected_folder_path() != current_path:
            print updateview.controller.get_info_user_selected_folder_path()
            print updateview.controller.get_info_user_written_folder_path()
            #current_path = updateview.controller.get_info_user_selected_folder_path() 
            #updateview.controller.set_info_folder_path(updateview.controller.get_info_user_selected_folder_path())
    updatemodel.subscribe(test)

    #updateview.controller.set_header_headline("Update packages")
    #updateview.controller.set_header_icon("GOn_banner_200x80.bmp")
    #updateview.controller.set_header_icon("Package.bmp")
    
    updateview.controller.set_window_frame_text("Testing the install window")
    updateview.controller.set_banner(updateview.controller.ID_BANNER_UPDATE)
    
    updateview.controller.set_phase_list([{'name': 'Select',   'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Download', 'state': updateview.controller.ID_PHASE_COMPLETE},
                                          {'name': 'Install',  'state': updateview.controller.ID_PHASE_ACTIVE},
                                          {'name': 'Restart',  'state': updateview.controller.ID_PHASE_PENDING},
                                          {'name': 'Done',     'state': updateview.controller.ID_PHASE_PENDING}])
    updateview.controller.set_selection_list([
                       {'id': 1, 'selected': False,  'icon': 'g_package_default_32x32.bmp', 'name': 'G/On Client for Windows', 'details': 'System: Windows, Version: 5.2, Size: 32 MB', 'description': 'Greatest remote solution ever - win style'},
                       {'id': 2, 'selected': True, 'icon': 'g_package_linux_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 3, 'selected': True, 'icon': 'g_package_mac_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 4, 'selected': True, 'icon': 'g_package_win_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 5, 'selected': True, 'icon': 'g_hagiwara_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 6, 'selected': True, 'icon': 'g_micro_smart_marked_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 7, 'selected': True, 'icon': 'g_soft_token_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 8, 'selected': True, 'icon': 'g_endpoint_marked_32x32.bmp', 'name': 'G/On Client for Apple Mac', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 9, 'selected': True, 'icon': 'g_smart_card_marked_32x32.bmp', 'name': 'G/On Client for Apple Mac which has a long descriptive name that goes beyond the size of the window.', 'details': 'System: Mac, Version: 5.3, Size: 40 MB', 'description': 'Greatest remote solution ever - mac style'},
                       {'id': 10, 'selected': False, 'icon': 'test_excel.bmp', 'name': 'Packaged Excel for Windows', 'details': 'System: Windows, Version: 5.3, Size: 17 MB', 'description': 'Exploder thingy'},
                       {'id': 11, 'selected': True, 'icon': 'test_outlook.bmp', 'name': 'Packaged IE for Windows', 'details': 'System: Windows, Version: 5.3, Size: 17 MB', 'description': 'Exploder thingy'}                                             
                       ], False)
    # Info commands
    #updateview.controller.set_info_free_text("Free text. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
    updateview.controller.set_info_free_text('A new version of your G/On software has been installed.\n\nClick "Start G/On" to start the new version.\nClick "Exit" to close the window without starting G/On')
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    #updateview.controller.set_info_free_text("Hejsa") 
    #updateview.controller.set_info_progress_complete(50)
    updateview.controller.set_info_headline("Upgrade is complete")
    updateview.controller.set_info_progress_subtext("Package: G/On client for windows. This extra text is inserted to make the line break over so that some text may dissappear.")
    
    #updateview.controller.set_info_headline("Information")
    
    updateview.controller.set_cancel_description("Click exit to stop.")
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_BUTTON_TEXT)
    updateview.controller.set_next_description("Click Start G/On to launch.")

    #updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_PROGRESS)
    #updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_FREE_TEXT)
    updateview.controller.set_info_mode(updateview.controller.ID_MODE_INFORMATION_GET_FOLDER)
    
    #updateview.controller.set_info_progress_mode(updateview.controller.ID_MODE_PROGRESS_UNKNOWN)
    #updateview.controller.set_info_progress_mode(updateview.controller.ID_MODE_PROGRESS_KNOWN) 
    updateview.controller.set_info_progress_complete(25)
    
    
    updateview.controller.set_next_label("Start G/On")
    updateview.controller.set_cancel_label("Exit") 
    #updateview.controller.set_info_system_description()
    #updateview.controller.set_next_label()
    #updateview.controller.set_cancel_label()
    
    #updateview.controller.set_show_selection_list(False)
    updateview.controller.set_show_selection_list(True)
    #updateview.controller.set_show_selection_list(False)
    updateview.controller.set_selection_method(updateview.controller.ID_SELECTION_MULTIPLE)
    updateview.controller.set_selection_highlighted_by_id(3)
    
    #print "ID", updateview.controller.get_selection_highlighted_id()
    #updateview.controller.set_info_system_description("Click next to proceed to the next step in the updating process.\n" +
    #                                                  "Click cancel to stop close the update window.") 

    # Button commands
    #updateview.controller.set_show_details_allowed(False)
    #updateview.controller.set_show_details_visible(True)
    #updateview.controller.set_show_details_allowed(True)
    #updateview.controller.set_autocomplete_visible(True)
    #updateview.controller.set_autocomplete_visible(False)
    
    #time.sleep(3)
    #updateview.controller.set_cancel_allowed(False)
    #time.sleep(3)
    #updateview.controller.set_next_allowed(False)
    
    #updateview.controller.set_default_button(updateview.controller.ID_CANCEL_BUTTON)
    
    updateview.display()
    commonview.display()
