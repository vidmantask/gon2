"""
Mfc version of the GUI for messages.
"""

import win32con
import sys

from components.presentation.message import MessageView
from components.presentation.gui.mfc.mfc_common_view import MfcCommonView

if "--noxp" in sys.argv:
    import win32gui #@UnusedImport
else:
    import winxpgui as win32gui #@Reimport


class MfcMessageView(MessageView):
    """ Mfc based view of messages. """
    def __init__(self, model, common, name, configuration):
        MessageView.__init__(self, model, common, name, configuration)
        self.headline = ""
        self.message = ""
        try:
            assert 'windowhandler' in self.handles, "Windowhandler is not set."
            self.hwnd = self.handles['windowhandler']
            assert 'commonview' in self.handles, "Commonview handler not set."
            self.commonicon = self.handles['commonview'].icon
            self.commontooltip = self.handles['commonview'].tooltip
        except AssertionError, e:
            print "Initializing MfcMessageView: " + e.message

    def update(self):
        """ Update anything relevant to this view. """
        self.headline = self.model.headline
        self.message = self.model.message
        self.display()

    def display(self, views=None):
        """ Displays a message as a balloon from the system tray. """
        if self.message == "":
            displaymessage = " "
        else:
            displaymessage = self.message
        try:
            win32gui.Shell_NotifyIcon(win32gui.NIM_MODIFY,
                                      (self.hwnd, MfcCommonView.ID_TRAY_ICON,
                                       win32gui.NIF_ICON | win32gui.NIF_MESSAGE | win32gui.NIF_TIP | win32gui.NIF_INFO,
                                       win32con.WM_USER+20,
                                       self.commonicon,
                                       self.commontooltip, # Tool-tip message
                                       displaymessage, # Balloon message
                                       5000, # Balloon message timeout
                                       self.headline, # Balloon message headline
                                       win32gui.NIIF_INFO )) # Balloon message icon
        except:
            print "Unable to display message '%s:%s'" % (self.headline, self.message)

    def destroy(self):
        pass

# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#

if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.common import CommonModel
    from components.presentation.message import MessageModel

    def test():
        print "Reading model data: (headline=" + messagemodel.headline + \
                                   " message=" + messagemodel.message + ")"

    # Create models
    commonmodel = CommonModel()
    messagemodel = MessageModel()
    messagemodel.subscribe(test)
    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'

    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    messageview = MfcMessageView(messagemodel, commonview.handles, 'messageview', configuration)
#    messageview.controller.set_message("Message from your friends at Giritech",
#                                       "This is a message set via the message controller.\n" + \
#                                       "It may contain information from the server, " + \
#                                       "for example to notify the user if the menu has changed.")
    messageview.controller.set_message("Message from your friends at Giritech", "")

    # Start Gui
    commonview.display()
