"""

MFC version of the GUI for menus.

"""

import win32gui
import win32api
import win32con

import lib.dictionary

from components.presentation.menu import MenuView
from components.presentation.gui.mfc.area.mfc_menu_area import MfcMenuArea


class MfcMenuView(MenuView):
    """ An MFC based view for a menu.

    Links the model top-folder to the view
    top folder. The MFC menu uses the same
    window-handler as common - this is exchanged
    via the handles dictionary.
    """
    def __init__(self, model, common, name, configuration):
        MenuView.__init__(self, model, common, name, configuration)
        self.hwnd = self.handles['windowhandler']
        #self.topfolder = MfcMenuFolderView(None, model.topfolder, self.configuration.gui_image_path)
        #self.topfolder = MfcMenuFolderView(self.handles['mainview'], model.topfolder, self.image_path)
        #self.launchidmap = []
        #self.launchidcounter = 0

        self.menu = MfcMenuArea(self.hwnd, 'self.hinst', self.configuration, self.model.topfolder)

    def on_command(self, hwnd, msg, wparam, lparam): #@UnusedVariable
        """ Launch an item by identifier. """
        self.controller.add_to_launch_list(self.menu.launchidmap[win32api.LOWORD(wparam)])

    def update(self):
        """ Update the MFC menu view. """
        self.menu.update(self.model)

    def display(self, views=None):
        """ Display the MFC based menu.

            Makes sure the menu is visible to
            the user. In this case it means to
            pop up the menu from the system tray.
        """
        try:
            self.pos = win32api.GetCursorPos()
            win32gui.SetForegroundWindow(self.hwnd)
            win32gui.TrackPopupMenu(self.menu.topfolder.view,
                                    win32con.TPM_LEFTALIGN,
                                    self.pos[0], self.pos[1], 0,
                                    self.hwnd, None)
            win32gui.PostMessage(self.hwnd, win32con.WM_NULL, 0, 0)
        except:
            pass

    def destroy(self):
        """ Destroy the menu so that it can be called again. """
        try:
            win32gui.DestroyMenu(self.topfolder.view)
        except:
            pass


# ----------------------------------------------------------------------
# Move this to a test area when appropriate.
#
if __name__ == '__main__':

    from appl.gon_client.gon_client import ClientGatewayOptions
    from components.presentation.gui.mfc.mfc_common_view import MfcCommonView
    from components.presentation.common import CommonModel
    from components.presentation.menu import MenuModel

    # Create models
    commonmodel = CommonModel()
    menumodel = MenuModel(lib.dictionary.Dictionary())
    # Create Gui parts
    handles = None
    configuration = ClientGatewayOptions()
    configuration.gui_image_path = '..'
    configuration.dictionary = lib.dictionary.Dictionary()

    commonview = MfcCommonView(commonmodel, handles, 'commonview', configuration)
    menuview = MfcMenuView(menumodel, commonview.handles, 'appmenu', configuration)
    # Add content to the menu
    menuview.controller.add_folder(-1, 1, "Folder", None)
    menuview.controller.add_item(-1, 2, "Item", None, True, "1000")
    menuview.controller.add_item(-1, 3, "Second item", None, True, "1001")

    menuview.controller.add_folder(1, 4, "Folder in folder", None)
    menuview.controller.add_item(1, 5, "Item in folder", None, True, "1002")
    menuview.controller.add_item(1, 6, "Second item in folder", None, True, "1003")

    menuview.controller.add_folder(4, 7, "Folder in folder in ...", None)
    menuview.controller.add_item(4, 8, "Item in folder in ...", None, True, "1004")

    menuview.controller.add_folder(-1, 9, "Folder with icon", "outlook.bmp")
    menuview.controller.add_item(-1, 10, "Item with icon", "outlook.bmp", True, "1005")
    menuview.controller.add_item(-1, 11, "Disabled item", None, False, "1006")
    menuview.controller.add_item(-1, 12, "Disabled item with icon", "outlook.bmp", False, "1007")

    # These should be handled nicely.
#    menuview.controller.add_folder(100, 13, "Folder with nonexisting parent", None)
#    menuview.controller.add_item(100, 14, "Item with nonexisting parent", None, True, "1008")

    menuview.controller.add_item(-1, 15, "Item with nonexisting bmp icon", "nonexisting.bmp", True, "1009")
    menuview.controller.add_item(-1, 16, "Item with unsupported icon type", "nonexisting.ico", True, "1010")
    menuview.controller.add_item(-1, 17, "Item with bad icon type", "nonexisting.bad", True, "1011")

    menuview.controller.add_folder(-1, 18, "Folder with nonexisting bmp icon", "nonexisting.bmp")
    menuview.controller.add_folder(-1, 19, "Folder with unsupported icon type", "nonexisting.ico")
    menuview.controller.add_folder(-1, 20, "Folder with bad icon type", "nonexisting.bad")

    # Re-adding folders and items with existing IDs replaces/overwrites the item.
    # This means that if you re-add a folder the old folders content is lost.
#    menuview.controller.add_item(-1, 3, "Item with existing id (readd)", "outlook.bmp", True, "1012")
#    menuview.controller.add_item(4, 8, "Item in folder in ...(readd)", "outlook.bmp", True, "1013")
#    menuview.controller.add_item(-1, 10, "Item with icon replaced (readd)", "giritech.bmp", True, "1014")
#    menuview.controller.add_item(-1, 11, "Disabled item (readd)", None, True, "1015")
#    menuview.controller.add_item(-1, 12, "Disabled item with icon (readd)", None, True, "1016")
#
#
#    menuview.controller.update_folder(-1, 1, "Folder (readd)", "outlook.bmp")
#    menuview.controller.update_folder(-1, 9, "Folder with icon replaced (readd)", "giritech.bmp")
#    menuview.controller.update_folder(4, 7, "Folder in folder in ... (readd)", "outlook.bmp")

    # Removing folder
    #menuview.controller.remove_folder(-1, 1)

    # Start Gui
    commonview.display()
