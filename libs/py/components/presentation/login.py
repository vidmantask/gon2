from components.presentation.base.base import ModelBase, ViewBase, ControllerBase
from components.presentation.base.button_area_base import ButtonAreaModelBase, ButtonAreaControllerBase

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class LoginModel(ModelBase, ButtonAreaModelBase):
    def __init__(self, dictionary):
        ModelBase.__init__(self)
        ButtonAreaModelBase.__init__(self, dictionary)
        
        self.greeting = "" 
        self.username = ""
        self.password = ""
        self.requestedpassword = None
                
        self.done = False
        self.state = LoginController.ID_STATE_LOGIN
        self.lockstate = False
        self.suggestedusername = ""
        self.lockusernamefield = False
        
        self.banner_id = LoginController.ID_BANNER_LOGIN
        
# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class LoginController(ControllerBase, ButtonAreaControllerBase):

    ID_BANNER_LOGIN = 5

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)
        ButtonAreaControllerBase.__init__(self, model)
    
    def configure(self, configuration):
        """ Configure buttons according to INI file settings. """
        if hasattr(configuration, 'gui_login_default_button'):
            if configuration.gui_login_default_button == 'next':
                self.set_default_button(self.ID_NEXT_BUTTON)
            elif configuration.gui_login_default_button == 'cancel':
                self.set_default_button(self.ID_CANCEL_BUTTON)
        else:
            self.set_default_button(self.ID_NEXT_BUTTON)

    def set_greeting(self, greeting):
        """ Set the login message that is displayed to the user"""
        if self.model.greeting != greeting:
            self.model.greeting = greeting
            self.model.alert_subscribers()

    def clear_greeting(self):
        """ Clear the message for the user.
        
            @since: 5.3
        """
        self.model.greeting = ""
        self.model.alert_subscribers()

    def set_credentials(self, username, password):
        """ Set the username and password etc. 
            Only call this method from GUI!
        """
        self._set_credentials(username, password)
        #self.model.alert_subscribers()

    def _set_credentials(self, username, password):
        """ Set the username and password etc. """
        if self.model.username != username or self.model.password != password:
            self.model.username = username
            self.model.password = password

    def clear_credentials(self):
        """ Clear username and password. """
        self.model.username = ''
        self.model.password = ''
        self.model.requestedpassword = None

    def get_credentials(self):
        return (self.model.username, self.model.password, self.model.requestedpassword)

    def set_done(self):
        """ Indicate that we are done using the window. 
            
            @since: 5.3
        """
        self.model.done = True

    def get_done(self):
        """ Get the indication of whether we are done with the window. 
        
            @since: 5.3
        """
        return self.model.done

    ID_STATE_LOGIN = 'login'
    ID_STATE_CHANGE_LOGIN = 'change_login'

    def set_state(self, state):
        """ Change the information entry state of the login view. 
        
            Valid states are:
                ID_STATE_LOGIN - for ordinary login
                ID_STATE_CHANGE_LOGIN - for changing the password
            
            The states are defined in the login controller class. 
            
            @since: 5.3
        """
        if self.model.state != state:
            self.model.state = state
            self.model.alert_subscribers()
        
    def get_state(self):
        """ Get the state of the login view. 
        
            This is used to figure out if the 
            user wants to change the password.
            Use get_new_passwords to get the 
            new password requested by the user.
            
            @since: 5.3
        """
        return self.model.state

    def set_password_request(self, requestedpassword):
        """ Set the user requested new password 
        
            @summary: 
            This lets the GUI enter a request for changing
            the password on behalf of the user. This method
            should only be used from the GUI side.
        
            There are two variables because the user is asked 
            to retype it. Someone should check that the two
            matches. 
            
            @param requestedpassword: the requested new password
            @param requestedpasswordretyped: the requested new password retyped
            Both parameters should be strings.
            
            @since: 5.3
        """
        self.model.requestedpassword = requestedpassword

    def get_password_request(self):
        """ Get the requested new password 
        
            @summary: 
            Get the new password requested by the user. To see 
            if the user intended to change the password check 
            the controllers ID_STATE_CHANGE_LOGIN. 
        
            @return: requestedpassword as a unicode string
       
            @since: 5.3 
        """
        return self.model.requestedpassword 

    def set_suggested_username(self, suggestedusername):
        """ Set a suggested user name in the user name field
        
            @summary: 
            This sets a suggested user name in the user name
            field but does not lock the user name field. So
            the user name can be changed from GUI side. The 
            suggested user name is only set when it is changed
            if the field is not locked the text that is added 
            from GUI side is what is used.

            @param suggestedusername: The suggested user name
    
            @since: 5.3
        """
        self.model.suggestedusername = suggestedusername
        self.model.alert_subscribers()
        
    def set_lock_username_field(self, shouldbelocked):
        """ Lock the user name field
        
            @summary: 
            Lock the user name field so that the user
            can not change the content. This is probably
            mostly used in combination with setting the
            suggested user name.
            
            @param shouldbelocked: Boolean indicating the wanted locking state
            
            @since: 5.3 
        """
        self.model.lockusernamefield = shouldbelocked
        self.model.alert_subscribers()

    def lock_state(self, shouldbelocked):
        """ Lock the login view in the state it is in.
        
            @summary: 
            This locks the state so that the user can not
            change it. This probably means that a check button
            is disabled in the GUI.
            
            @param shouldbelocked: Boolean indicating the wanted locking state
            
            @since: 5.3 
        """
        self.model.lockstate = shouldbelocked
        self.model.alert_subscribers()
        
# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class LoginView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = LoginController(self.model, self)
