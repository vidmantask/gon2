from components.presentation.base.base import ModelBase, ViewBase, ControllerBase
from components.presentation.menu import MenuModel


def populate_file_menu(menu):
    """ Add menu items to the main menu. """ 
    menu.controller.add_item(-1, 1, "Quit", None, True, "117")


# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class MainModel(ModelBase):
    def __init__(self, dictionary):
        ModelBase.__init__(self)

        self.filemenumodel = MenuModel(dictionary)
        #self.menumodel.topfolder.insert_item(-1, "jubii", None, True, 12345)
        self.statusbartext = ""
        self.statusbarconnectionstatus = MainController.ID_STATUS_BAR_STATUS_UNCONNECTED
        self.statusbarstatus = None
        self.statusbarservices = []
 
# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class MainController(ControllerBase):

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)

    def set_status_bar_text(self, text):
        "Set the text in the status bar area. "
        if self.model.statusbartext != text:
            self.model.statusbartext = text
            self.model.alert_subscribers()

    ID_STATUS_BAR_STATUS_CONNECTED = 1001
    ID_STATUS_BAR_STATUS_CONNECTING = 1002
    ID_STATUS_BAR_STATUS_UNCONNECTED = 1003

    def set_status_bar_connection_status(self, status):
        "Set the connection status in the status bar. "
        if self.model.statusbarconnectionstatus != status:
            self.model.statusbarconnectionstatus = status
            self.model.alert_subscribers()

    def add_status_bar_service(self, _id, text, icon):
        "Add a service to be displayed in the status bar. "
        self.model.statusbarservices.append((_id, text, icon))
        self.model.alert_subscribers()
        
    def remove_status_bar_service(self, _id):
        pass

        
# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class MainView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = MainController(self.model, self)
