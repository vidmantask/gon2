from components.presentation.base.base import ModelBase, ViewBase, ControllerBase

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class MenuItem(object):
    """ A menu item used to initiate application launch.
    
        Contains all information needed to display 
        a single point in any type of menu. 
    """
    def __init__(self, _id, text, icon, enabled, launch_id, restricted=False, restriction_text=""):
        self.id = _id
        self.text = text
        self.icon = icon
        self.enabled = enabled
        self.launch_id = launch_id
        self.restricted = restricted
        self.restriction_text = restriction_text

    def update(self, text, icon, enabled, launch_id, restricted=False, restriction_text=""):
        """ Update this item.
        
            All internal parameters are checked
            for changes. If any changes has occurred
            the calling method is informed.
            
            If the id has changed an update is not
            enough. Then the item should be removed
            and added in the new place.
        """
        changed = False
        if self.text != text:
            self.text = text
            changed = True
        if self.icon != icon:
            self.icon = icon
            changed = True
        if self.enabled != enabled:
            self.enabled = enabled
            changed = True
        if self.launch_id != launch_id:
            self.launch_id = launch_id
            changed = True
        if self.restricted != restricted:
            self.restricted = restricted
            changed = True
        if self.restriction_text != restriction_text:
            self.restriction_text = restriction_text
            changed = True
        return changed
        

class MenuFolder(object):
    """ A menu folder containing other folders and/or items.

        A folder can contain other folders and a group 
        of menu items. 
    """
    def __init__(self, parentid, _id, text, icon):
        self.parentid = parentid
        self.id = _id
        self.text = text
        self.icon = icon
        self.folders = []
        self.items = []

    def update(self, text, icon):
        """ Update this folder.
            
            All internal parameters are checked
            for changes. 
            
            If the id or parentid has changed a 
            simple update is not enough. Then the 
            folder should be removed and added in 
            the new location. 
            
            Folders and items located in this folder
            are not checked. If they are changed, they
            should be updated by them selves.
        """
        changed = False
        if self.text != text:
            self.text = text
            changed = True
        if self.icon != icon:
            self.icon = icon
            changed = True
        return changed

    def insert_folder(self, _id, text, icon):
        """ Create a new folder and insert it here. """
        self.folders.append(MenuFolder(self.id, _id, text, icon))

    def remove_folder(self, _id):
        """ Find the folder with the specified id and remove it. """
        folderfound = False
        for folder in self.folders:
            if folder.id == _id:
                folderfound = True
                break
        if folderfound:
            self.folders.remove(folder)

    def insert_item(self, _id, text, icon, enabled, launch_id, restricted=False, restriction_text=""):
        """ Create a new item and insert it here. """
        self.items.append(MenuItem(_id, text, icon, enabled, launch_id, restricted, restriction_text))

    def remove_item(self, _id):
        """ Find the item with the specified id and remove it. """
        itemfound = False
        for item in self.items:
            if item.id == _id:
                itemfound = True
                break
        if itemfound:
            self.items.remove(item)

    def get_item_list(self):
        result = []
        for item in self.items:
            result.append( (item.id, item.launch_id, item.text) )
        for folder in self.folders:
            result.extend(folder.get_item_list())
        return result


class MenuModel(ModelBase):
    """ Model for a generic menu.

        This is the starting point for the menu.
        There can be more than one menu for different
        types of apps. For instance there may be a
        menu for app. initiaters and another menu 
        for control, settings etc.
    
        The top folder is identified by the number -1.
        So the rest should be checked that they are 
        positive, to avoid a mess.
    """
    def __init__(self, dictionary):
        ModelBase.__init__(self)
        self.topfolder = MenuFolder(-1, -1, dictionary._("G/On Menu"), None)
        self.launchlist = []
        
        
    def get_item_list(self):
        """Returns a list of tuples (id, text) for all items in the topmenu""" 
        return self.topfolder.get_item_list()


# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class MenuController(ControllerBase):
    
    #id2launchid = {}

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view) 

    def _locate_folder(self, location, _id):
        """ Find a folder by an identifier. 
        
            This should probably always be called
            with the top folder as the starting 
            location.
        
            Keyword arguments:
            location -- the current folder
            id -- the wanted folders identifier
        """
        if location.id == _id:
            return location
        else:
            for folder in location.folders:
                location = self._locate_folder(folder, _id)
                if location != None:
                    return location

    def locate_item_in_folder(self, folder, _id):
        """ Find an item by id in a folder. """
        ritem = None
        for item in folder.items:
            if item.id == _id:
                ritem = item
                break
        return ritem

    def add_folder(self, folderid, _id, text, icon):
        """ Add a folder to the specified folder. """
        try:
            assert _id > 0
            folder = self._locate_folder(self.model.topfolder, folderid)
            folder.insert_folder(_id, text, icon)
            self.model.alert_subscribers()
        except AttributeError:
            print "Could not locate menu folder with id:", folderid
        except AssertionError:
            print "Menu identifier should be a positive number:", _id

    def update_folder(self, folderid, _id, text, icon):
        """ Update a folder with id in folder with folderid. """
        try:
            assert _id > 0
            changed = False
            folder = self._locate_folder(self.model.topfolder, _id)
            changed = folder.update(text, icon)
            if changed:
                self.model.alert_subscribers()
        except AttributeError:
            print "Could not locate menu folder with id:", folderid
        except AssertionError:
            print "Menu identifier should be a positive number:", _id

    def remove_folder(self, parentid, _id):
        """ Remove the folder id from parent with parentid. 
        
            If the folder can not be located, it is assumed that
            it is located in a folder that has already been
            removed. in that case nothing is done.
        """
        try:
            assert _id > 0
            folder = self._locate_folder(self.model.topfolder, parentid)
            if folder != None:
                folder.remove_folder(_id)
                self.model.alert_subscribers()
        except AssertionError:
            print "Menu identifier should be a positive number:", _id
        
    def add_item(self, folderid, _id, text, icon, enabled, launch_id, restricted=False, restriction_text=""):
        """ Add an item to the specified folder. """
        try:
            assert _id >= 0
            folder = self._locate_folder(self.model.topfolder, folderid)
            item = self.locate_item_in_folder(folder, _id)
            if item == None:
                folder.insert_item(_id, text, icon, enabled, launch_id, restricted, restriction_text)
            else:
                print "Item with id", _id, "already created in folder", folderid, "- try updating instead"
            self.model.alert_subscribers()
        except AttributeError:
            print "Could not locate menu folder with id:", folderid
        except AssertionError:
            print "Menu identifier should be a positive number:", _id

    def update_item(self, folderid, _id, text, icon, enabled, launch_id, restricted=False, restriction_text=""):
        """ Update an item with id in folder with parentid. """
        try:
            assert _id > 0
            founditem = False
            changed = False
            parentfolder = self._locate_folder(self.model.topfolder, folderid)
            if parentfolder != None:
                for item in parentfolder.items:
                    if  item.id == _id: 
                        founditem = True
                        break
                if founditem:
                    changed = item.update(text, icon, enabled, launch_id, restricted, restriction_text)
                if changed:
                    self.model.alert_subscribers()
        except AssertionError:
            print "Menu identifier should be a positive number:", _id

    def remove_item(self, parentid, _id):
        """ Remove the item with id from folder with parentid. 
        
            If the item can not be located, it is assumed that 
            it is located in a folder that has allready been
            removed and nothing is done.
        """
        try:
            assert _id > 0
            folder = self._locate_folder(self.model.topfolder, parentid)
            if folder != None:
                folder.remove_item(_id)
                self.model.alert_subscribers()
            else:
                print "folder is None"
        except AssertionError:
            print "Menu identifier should be a positive number:", _id

    def clear(self):
        """ Please don't use this. Use add, remove and update if at all possible. """
        self.model.topfolder = MenuFolder(-1, -1, "G/On Menu", None)
        self.model.alert_subscribers()

    def get_launch_list(self):
        return self.model.launchlist

    def add_to_launch_list(self, _id):
        """ Add an  item launch_id to the list of stuff to launch. """
        self.model.launchlist.append(_id)
        self.model.alert_subscribers()

    def remove_from_launch_list(self, _id):
        """ Remove an item from the list of stuff to launch. """
        try:
            assert _id in self.model.launchlist, "Can not remove non existing item from launch list"
            self.model.launchlist.remove(_id)
        except AssertionError, e:
            print e.message


# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class MenuView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 
    """ Base class for all menu view GUI implementations. """
    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration) 
        self.controller = MenuController(self.model, self)


