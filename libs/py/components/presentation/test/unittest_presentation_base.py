"""
Unittest for the model/view/controller base for the presentation.
"""

from components.presentation.base.base import ModelBase, ControllerBase, ViewBase
import unittest
from lib import giri_unittest


class Configuration(object):
    def __init__(self):
        self.dictionary = giri_unittest.get_dictionary()
        self.image_path = giri_unittest.get_image_path()


class ModelBaseTest(unittest.TestCase):
    """ Collection of tests for the base.py ModelBase class. """
    
    def subscriber1(self):
        self.test1 = True
    
    def subscriber2(self):
        self.test2 = True

    def subscriber3(self):
        self.test3 = True

    def test_subscribe_init(self):
        """ Model subscription initialization. """
        test = ModelBase()
        self.assertEqual(test.subscribers, [])

    def test_add_subscriber(self):
        """ Add subscriber to the model. """
        test = ModelBase()
        test.subscribe(self.subscriber1)
        self.assertEqual(test.subscribers, [self.subscriber1])

    def test_add_several_subscribers(self):
        """ Add several subscribers to the model. """
        test = ModelBase()
        test.subscribe(self.subscriber1)
        test.subscribe(self.subscriber2)
        test.subscribe(self.subscriber3)
        self.assertEqual(test.subscribers, [self.subscriber1, self.subscriber2, self.subscriber3])
    
    def test_alert_subscriber(self):
        """ Alert a single subscriber. """
        self.test1 = False
        test = ModelBase()
        test.subscribe(self.subscriber1)
        test.alert_subscribers()
        self.assertTrue(self.test1)

    def test_alert_several_subscribers(self):
        """ Alert several subscribers. """
        self.test1 = False
        self.test2 = False
        self.test3 = False
        test = ModelBase()
        test.subscribe(self.subscriber1)
        test.subscribe(self.subscriber2)
        test.subscribe(self.subscriber3)
        test.alert_subscribers()
        self.assertTrue(self.test1 and self.test2 and self.test3)
        

class ControllerBaseTest(unittest.TestCase):
    """ Collection of tests for the base.py ControllerBase class. """
    def test_controller_init(self):
        """ Validate correct settings when initializing a controller. """
        model = "model"
        view = "view"
        test = ControllerBase(model, view)
        self.assertEqual(test.model, "model")
        self.assertEqual(test.view, "view")


class ViewBaseTest(unittest.TestCase):
    """ Collection of tests for the base.py ViewBase class. """
    def test_viewer_init(self):
        """ Viewer base initializtion. """
        handles = {}
        testmodel = ModelBase()
        testview = ViewBase(testmodel, handles, 'testname', Configuration())
        self.assertEqual([testview.model, testview.handles], [testmodel, handles])

    def test_viewer_handle_creation(self):
        """ See if a handle is created and set properly when none is given. """
        testmodel = ModelBase()
        testview = ViewBase(testmodel, None, 'testname', Configuration())
        self.assertEqual(testview.handles['testname'], testview)
        
    def test_viewer_several_handles(self):
        """ See if we can deal with several handles. """
        testmodel = ModelBase()
        testview1 = ViewBase(testmodel, None, 'testname1', Configuration())
        testview2 = ViewBase(testmodel, testview1.handles, 'testname2', Configuration())
        testview3 = ViewBase(testmodel, testview1.handles, 'testname3', Configuration())
        self.assertEqual([testview1.handles['testname1'], testview1.handles['testname2'], testview1.handles['testname3']],
                         [testview1, testview2, testview3])
        
    def test_viewer_model_subscription(self):
        """ Automatically subscribe update method to the given model. """
        testmodel = ModelBase()
        testview = ViewBase(testmodel, None, 'testname', Configuration())
        self.assertEqual(testmodel.subscribers, [testview.update])


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
