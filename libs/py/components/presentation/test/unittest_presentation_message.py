"""
Unittest for the Message MVC for the presentation.
"""

from components.presentation.message import MessageModel, MessageController, MessageView
import unittest
from lib import giri_unittest


class Configuration(object):
    def __init__(self):
        self.dictionary = giri_unittest.get_dictionary()
        self.image_path = giri_unittest.get_image_path()



class MesageModelTest(unittest.TestCase):
    """ Collection of tests for the message.py MessageModel class. """

    def test_message_model_init(self):
        """ Creation of the Message model. """
        test = MessageModel()
        self.assertEqual(test.subscribers, [])
        self.assertEqual(test.headline, "")
        self.assertEqual(test.message, "")


class MessageControllerTest(unittest.TestCase):
    """ Collection of tests for the message.py MessageController class. """

    def subscriber(self):
        self.test = True

    def test_message_controller_init(self):
        """ Creation of the Message controller. """
        testmodel = MessageModel()
        testview = MessageView(testmodel, None, 'view', Configuration())
        testcontroller = MessageController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)        

    def test_message_controller_set_message(self):
        """ Set a message via the controller. """
        self.test = False
        testmodel = MessageModel()
        testcontroller = MessageController(testmodel, None)
        testmodel.subscribe(self.subscriber) 
        testcontroller.set_message("TESTHEADLINE", "testmessage")
        # See if the correct variables has been set.
        self.assertEqual(testmodel.headline, "TESTHEADLINE")
        self.assertEqual(testmodel.message, "testmessage")
        # See if subscribers was notified of changes.
        self.assertTrue(self.test)


class MessageViewTest(unittest.TestCase):
    """ Collection of tests for the message.py MessageView class. """

    def test_message_view_init(self):
        """ Creation of the common view. """
        testmodel = MessageModel()
        testview = MessageView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
