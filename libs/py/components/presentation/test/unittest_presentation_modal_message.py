"""
Unittest for the Modal Message MVC for the presentation.
"""
#GIRITECH_UNITTEST_IGNORE = True

from components.presentation.modal_message import ModalMessageModel, ModalMessageController, ModalMessageView
import unittest
from lib import giri_unittest


class Configuration(object):
    def __init__(self):
        self.image_path = giri_unittest.get_image_path()
        self.dictionary = giri_unittest.get_dictionary()
        

class ModalMesageModelTest(unittest.TestCase):
    """ Collection of tests for the ModalMessageModel class. """

    def test_message_model_init(self):
        """ Creation of the Message model. """
        test = ModalMessageModel(giri_unittest.get_dictionary())
        self.assertEqual(test.subscribers, [])
        self.assertEqual(test.text, "")


class ModalMessageControllerTest(unittest.TestCase):
    """ Collection of tests for the ModalMessageController class. """

    def subscriber(self):
        self.test = True

    def test_modal_message_controller_init(self):
        """ Creation of the Modal Message controller. """
        testmodel = ModalMessageModel(giri_unittest.get_dictionary())
        testview = ModalMessageView(testmodel, None, 'view', Configuration())
        testcontroller = ModalMessageController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)        

    def test_message_controller_set_message(self):
        """ Set the text via the controller. """
        self.test = False
        testmodel = ModalMessageModel(giri_unittest.get_dictionary())
        testcontroller = ModalMessageController(testmodel, None)
        testmodel.subscribe(self.subscriber) 
        testcontroller.set_text("text")
        # See if the correct variables has been set.
        self.assertEqual(testmodel.text, "text")
        # See if subscribers was notified of changes.
        self.assertTrue(self.test)

    def test_message_controller_set_button_states(self):
        """ Set the button states via the controller. """
        self.test = False
        testmodel = ModalMessageModel(giri_unittest.get_dictionary())
        testcontroller = ModalMessageController(testmodel, None)
        testmodel.subscribe(self.subscriber) 
        
        testcontroller.set_cancel_allowed(True)
        self.assertEqual(testmodel.cancel_allowed, True)
        testcontroller.set_next_allowed(True)
        self.assertEqual(testmodel.next_allowed, True)

        testcontroller.set_cancel_allowed(False)
        self.assertEqual(testmodel.cancel_allowed, False)
        testcontroller.set_next_allowed(False)
        self.assertEqual(testmodel.next_allowed, False)
        
        testcontroller.set_cancel_clicked(True)
        self.assertEqual(testmodel.cancel_clicked, True)
        testcontroller.set_next_clicked(True)
        self.assertEqual(testmodel.next_clicked, True)

        testcontroller.set_cancel_clicked(False)
        self.assertEqual(testmodel.cancel_clicked, False)
        testcontroller.set_next_clicked(False)
        self.assertEqual(testmodel.next_clicked, False)
        
        self.assertEqual(self.test, True)


class ModalMessageViewTest(unittest.TestCase):
    """ Collection of tests for the ModalMessageView class. """

    def test_modal_message_view_init(self):
        """ Creation of the common view. """
        testmodel = ModalMessageModel(giri_unittest.get_dictionary())
        testview = ModalMessageView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
