"""
Unit test for the Update MVC for the presentation.
"""

from components.presentation.update import UpdateModel, UpdateController, UpdateView
import unittest
from lib import giri_unittest

class Configuration(object):
    def __init__(self):
        self.image_path = giri_unittest.get_image_path()
        self.dictionary = giri_unittest.get_dictionary()


class UpdateModelTest(unittest.TestCase):
    """ Collection of tests for update.py class. """

    def test_update_model_init(self):
        """ Creation of the update model - check start settings."""
        test = UpdateModel(giri_unittest.get_dictionary())
        self.assertEqual(test.subscribers, [])
        
        self.assertEqual(test.info_mode, None)
        self.assertEqual(test.gui_update_on_alerts, True)
        self.assertEqual(test.minimized, False)
        self.assertEqual(test.show_selection_list, False)
        # Header area model part
        self.assertEqual(test.info_headline, "")
        self.assertEqual(test.banner_image, None) 
        # Phases information area.
        self.assertEqual(test.phase_list, [])
        # Selection information area.
        self.assertEqual(test.selection_list, [])
        # Information area settings.
        self.assertEqual(test.info_headline, '')
        self.assertEqual(test.info_text, '')
        self.assertEqual(test.info_progress_complete, 0)
        self.assertEqual(test.info_progress_subtext, '')
        self.assertEqual(test.info_mode, None)
        # Button area model part
        self.assertEqual(test.cancel_clicked, False)
        self.assertEqual(test.cancel_allowed, True)
        self.assertEqual(test.cancel_description, "")
        self.assertEqual(test.next_clicked, False)
        self.assertEqual(test.next_allowed, True)
        self.assertEqual(test.next_description, "")

class LoginControllerTest(unittest.TestCase):
    """ Collection of tests for the login.py loginController class. """

    def subscriber(self):
        self.test = True

    def test_login_controller_init(self):
        """ Creation of the update controller. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testview = UpdateView(testmodel, None, 'view', Configuration())
        testcontroller = UpdateController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)

    def test_set_minimized(self):
        """ Setting the minimized state value."""
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.minimized, False)
        testcontroller.set_minimized(True)
        self.assertEqual(testmodel.minimized, True)
        
    def test_get_minimized(self):
        """ Getting the minimized state value. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testcontroller.get_minimized(), False)
        testcontroller.set_minimized(True)
        self.assertEqual(testcontroller.get_minimized(), True)
        
    def test_set_show_selection_list(self):
        """ Setting the show selection list state value and observe subscription alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.show_selection_list, False)
        testcontroller.set_show_selection_list(True)
        self.assertEqual(testmodel.show_selection_list, True)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_banner(self):
        """ Setting the banner and observe subscription alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.banner_id, -1)
        testcontroller.set_banner(testcontroller.ID_BANNER_UPDATE)
        self.assertEqual(testmodel.banner_id, testcontroller.ID_BANNER_UPDATE)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_phase_list(self):
        """ Setting the phase list and observe subscription alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.phase_list, [])
        testcontroller.set_phase_list([{'name': 'Select', 'state': testcontroller.ID_PHASE_COMPLETE}, 
                                       {'name': 'Download', 'state': testcontroller.ID_PHASE_ACTIVE},
                                       {'name': 'Install', 'state': testcontroller.ID_PHASE_PENDING}
                                       ])
        self.assertEqual(testmodel.phase_list, [{'name': 'Select', 'state': testcontroller.ID_PHASE_COMPLETE}, 
                                                {'name': 'Download', 'state': testcontroller.ID_PHASE_ACTIVE},
                                                {'name': 'Install', 'state': testcontroller.ID_PHASE_PENDING}
                                                ])
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_selection_list(self):
        """ Set the package list and observe subscription alert."""
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.selection_list, [])
        testcontroller.set_selection_list([ 
                                           {'id': 0,
                                            'selected': True, 
                                            'icon': None, 
                                            'status': 'dependency',
                                            'action': 'A',
                                            'name': 'G/On Client', 
                                            'arch': 'win', 
                                            'ver': '5.2', 
                                            'size': '32 MB',
                                            'message': 'Replaces old version 5.1', 
                                            'description': 'Greatest remote solution ever - win style.'},
                                            {'id': 1,
                                             'selected': False, 
                                             'icon': None, 
                                             'status': 'dependency',
                                             'action': 'R',
                                             'name': 'G/On Client', 
                                             'arch': 'mac', 
                                             'ver': '5.2', 
                                             'size': '65 MB',
                                             'message': 'Replaces old version 5.1', 
                                             'description': 'Greatest remote solution ever - mac style.'}
                                            ])
        
        self.assertEqual(testmodel.selection_list, [ 
                                                    {'id': 0,
                                                     'selected': True, 
                                                     'icon': None, 
                                                     'status': 'dependency',
                                                     'action': 'A',
                                                     'name': 'G/On Client', 
                                                     'arch': 'win', 
                                                     'ver': '5.2', 
                                                     'size': '32 MB',
                                                     'message': 'Replaces old version 5.1', 
                                                     'description': 'Greatest remote solution ever - win style.'},
                                                     {'id': 1,
                                                      'selected': False, 
                                                      'icon': None, 
                                                      'status': 'dependency',
                                                      'action': 'R',
                                                      'name': 'G/On Client', 
                                                      'arch': 'mac', 
                                                      'ver': '5.2', 
                                                      'size': '65 MB',
                                                      'message': 'Replaces old version 5.1', 
                                                      'description': 'Greatest remote solution ever - mac style.'}
                                                     ])
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_selection_via_index(self):
        """ Set package list selections via index and observe subscription alert."""
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.selection_list, [])
        testcontroller.set_selection_list([ 
                                           {'id': 0,
                                            'selected': True, 
                                            'icon': None, 
                                            'status': 'dependency',
                                            'action': 'A',
                                            'name': 'G/On Client', 
                                            'arch': 'win', 
                                            'ver': '5.2', 
                                            'size': '32 MB',
                                            'message': 'Replaces old version 5.1', 
                                            'description': 'Greatest remote solution ever - win style.'},
                                            {'id': 1,
                                             'selected': False, 
                                             'icon': None, 
                                             'status': 'dependency',
                                             'action': 'R',
                                             'name': 'G/On Client', 
                                             'arch': 'mac', 
                                             'ver': '5.2', 
                                             'size': '65 MB',
                                             'message': 'Replaces old version 5.1', 
                                             'description': 'Greatest remote solution ever - mac style.'}
                                            ])
        testcontroller.set_selection_via_index(0, False)
        testcontroller.set_selection_via_index(1, True)
        self.assertEqual(testmodel.selection_list, [ 
                                                    {'id': 0,
                                                     'selected': False, 
                                                     'icon': None, 
                                                     'status': 'dependency',
                                                     'action': 'A',
                                                     'name': 'G/On Client', 
                                                     'arch': 'win', 
                                                     'ver': '5.2', 
                                                     'size': '32 MB',
                                                     'message': 'Replaces old version 5.1', 
                                                     'description': 'Greatest remote solution ever - win style.'},
                                                     {'id': 1,
                                                      'selected': True, 
                                                      'icon': None, 
                                                      'status': 'dependency',
                                                      'action': 'R',
                                                      'name': 'G/On Client', 
                                                      'arch': 'mac', 
                                                      'ver': '5.2', 
                                                      'size': '65 MB',
                                                      'message': 'Replaces old version 5.1', 
                                                      'description': 'Greatest remote solution ever - mac style.'}
                                                     ])
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_selection_list(self):
        """ Get the package list."""
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.selection_list, [])
        testcontroller.set_selection_list([ 
                                           {'id': 0,
                                            'selected': True, 
                                            'icon': None, 
                                            'status': 'dependency',
                                            'action': 'A',
                                            'name': 'G/On Client', 
                                            'arch': 'win', 
                                            'ver': '5.2', 
                                            'size': '32 MB',
                                            'message': 'Replaces old version 5.1', 
                                            'description': 'Greatest remote solution ever - win style.'},
                                            {'id': 1,
                                             'selected': False, 
                                             'icon': None, 
                                             'status': 'dependency',
                                             'action': 'R',
                                             'name': 'G/On Client', 
                                             'arch': 'mac', 
                                             'ver': '5.2', 
                                             'size': '65 MB',
                                             'message': 'Replaces old version 5.1', 
                                             'description': 'Greatest remote solution ever - mac style.'}
                                            ])
        self.assertEqual(testcontroller.get_selection_list(), [ 
                                                    {'id': 0,
                                                     'selected': True, 
                                                     'icon': None, 
                                                     'status': 'dependency',
                                                     'action': 'A',
                                                     'name': 'G/On Client', 
                                                     'arch': 'win', 
                                                     'ver': '5.2', 
                                                     'size': '32 MB',
                                                     'message': 'Replaces old version 5.1', 
                                                     'description': 'Greatest remote solution ever - win style.'},
                                                     {'id': 1,
                                                      'selected': False, 
                                                      'icon': None, 
                                                      'status': 'dependency',
                                                      'action': 'R',
                                                      'name': 'G/On Client', 
                                                      'arch': 'mac', 
                                                      'ver': '5.2', 
                                                      'size': '65 MB',
                                                      'message': 'Replaces old version 5.1', 
                                                      'description': 'Greatest remote solution ever - mac style.'}
                                                     ])

    def test_set_info_mode(self):
        """ Set the info area mode and observe subscriber alert."""
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_mode, None)
        testcontroller.set_info_mode(testcontroller.ID_MODE_INFORMATION_PROGRESS)
        self.assertEqual(testmodel.info_mode, testcontroller.ID_MODE_INFORMATION_PROGRESS)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_info_mode(self):
        """ Get the info area mode."""
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_mode, None)
        testcontroller.set_info_mode(testcontroller.ID_MODE_INFORMATION_PROGRESS)
        self.assertEqual(testcontroller.get_info_mode(), testcontroller.ID_MODE_INFORMATION_PROGRESS)

    def test_set_info_headline(self):
        """ Set the info area headline and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_headline, '')
        testcontroller.set_info_headline("test")
        self.assertEqual(testmodel.info_headline, "test")
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_info_headline(self):
        """ Get the info area headline. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_headline, '')
        testcontroller.set_info_headline("test")
        self.assertEqual(testcontroller.get_info_headline(), "test")

    def test_set_info_progress_complete(self):
        """ Set the info area progression state and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_progress_complete, 0)
        testcontroller.set_info_progress_complete(47)
        self.assertEqual(testmodel.info_progress_complete, 47)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_info_progress_complete(self):
        """ Get the info area progression state. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_progress_complete, 0)
        testcontroller.set_info_progress_complete(17)
        self.assertEqual(testcontroller.get_info_progress_complete(), 17)

    def test_set_info_progress_subtext(self):
        """ Set the info area progress sub text and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_progress_subtext, '')
        testcontroller.set_info_progress_subtext("test")
        self.assertEqual(testmodel.info_progress_subtext, "test")
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_info_progress_subtext(self):
        """ Get the info area progress sub text. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.info_progress_subtext, '')
        testcontroller.set_info_progress_subtext("test")
        self.assertEqual(testcontroller.get_info_progress_subtext(), "test")

    def test_set_cancel_allowed(self):
        """ Set the allowed state for he cancel button and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.cancel_allowed, True)
        testcontroller.set_cancel_allowed(False)
        self.assertEqual(testmodel.cancel_allowed, False)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_cancel_allowed(self):
        """ Get the allowed state for the cancel button. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.cancel_allowed, True)
        testcontroller.set_cancel_allowed(False)
        self.assertEqual(testcontroller.get_cancel_allowed(), False)

    def test_set_cancelled_state(self):
        """ Set the cancelled state and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.cancel_clicked, False)
        testcontroller.set_cancel_clicked(True)
        self.assertEqual(testmodel.cancel_clicked, True)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

#    def test_reset_cancelled_state(self):
#        """ Set the cancelled state and observe NO subscriber alert. """
#        testmodel = UpdateModel(giri_unittest.get_dictionary())
#        testmodel.subscribe(self.subscriber)
#        testcontroller = UpdateController(testmodel, None)
#        # See that variable is set.
#        self.assertEqual(testmodel.cancel_clicked, False)
#        testcontroller.set_cancel_clicked(True)
#        self.assertEqual(testmodel.cancel_clicked, True)
#        
#        self.test = False
#        testcontroller.reset_cancelled_state()
#        self.assertEqual(testmodel.cancelled, False)
#        # See that subscriber is alerted.
#        self.assertFalse(self.test)

    def test_get_cancelled_state(self):
        """ Get the cancelled state. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.cancel_clicked, False)
        self.assertEqual(testcontroller.get_cancel_clicked(), False)
        testcontroller.set_cancel_clicked(True)
        self.assertEqual(testcontroller.get_cancel_clicked(), True)

    def test_set_cancel_description(self):
        """ Set the cancel description and observe subscriber alert. 
            (only when info area shows button information)
        """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        testcontroller.set_info_mode(testcontroller.ID_MODE_INFORMATION_BUTTON_TEXT)
        self.assertEqual(testmodel.cancel_description, '')
        testcontroller.set_cancel_description("test")
        self.assertEqual(testmodel.cancel_description, "test")
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_next_state(self):
        """ Set the next state and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.next_clicked, False)
        testcontroller.set_next_clicked(True)
        self.assertEqual(testmodel.next_clicked, True)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

#    def test_reset_next_state(self):
#        """ Set the next state and observe NO subscriber alert. """
#        testmodel = UpdateModel(giri_unittest.get_dictionary())
#        testmodel.subscribe(self.subscriber)
#        testcontroller = UpdateController(testmodel, None)
#        # See that variable is set.
#        self.assertEqual(testmodel.next_clicked, False)
#        testcontroller.set_next_clicked(True)
#        self.assertEqual(testmodel.next_clicked, True)
#        
#        self.test = False
#        testcontroller.reset_next_state()
#        self.assertEqual(testmodel.next, False)
#        # See that subscriber is alerted.
#        self.assertFalse(self.test)

    def test_get_next_state(self):
        """ Get the next state. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.next_clicked, False)
        self.assertEqual(testcontroller.get_next_clicked(), False)
        testcontroller.set_next_clicked(True)
        self.assertEqual(testcontroller.get_next_clicked(), True)

    def test_set_next_allowed(self):
        """ Set the next allowed state and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.next_allowed, True)
        testcontroller.set_next_allowed(False)
        self.assertEqual(testmodel.next_allowed, False)
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_get_next_allowed(self):
        """ Get the next allowed state. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.next_allowed, True)
        self.assertEqual(testcontroller.get_next_allowed(), True)
        testcontroller.set_next_allowed(False)
        self.assertEqual(testcontroller.get_next_allowed(), False)

    def test_set_next_description(self):
        """ Set the next button description and observe subscriber alert
            (Only alert when info on buttons are displayed)
        """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.

        testcontroller.set_info_mode(testcontroller.ID_MODE_INFORMATION_BUTTON_TEXT) 
        self.assertEqual(testmodel.next_description, '')
        testcontroller.set_next_description("test")
        self.assertEqual(testmodel.next_description, "test")
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_set_gui_should_update_on_alerts(self):
        """ Set the gui alert state and observe subscriber alert. """
        self.test = False
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.gui_update_on_alerts, True)
        testcontroller.set_gui_should_update_on_alerts(False)
        self.assertEqual(testmodel.gui_update_on_alerts, False)
        # See that subscriber is alerted.
        self.assertFalse(self.test)

        testcontroller.set_gui_should_update_on_alerts(True)
        self.assertTrue(self.test)

    def test_get_gui_should_update_on_alerts(self):
        """ Get the gui alert state. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = UpdateController(testmodel, None)
        # See that variable is set.
        self.assertEqual(testmodel.gui_update_on_alerts, True)
        self.assertEqual(testcontroller.get_gui_should_update_on_alerts(), True)
        testcontroller.set_gui_should_update_on_alerts(False)
        self.assertEqual(testcontroller.get_gui_should_update_on_alerts(), False)

class UpdateViewTest(unittest.TestCase):
    """ Collection of tests for the login.py loginView class. """

    def test_update_view_init(self):
        """ Creation of the common view. """
        testmodel = UpdateModel(giri_unittest.get_dictionary())
        testview = UpdateView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
        