"""
Unittest for the Menu MVC for the presentation.
"""

from components.presentation.menu import MenuModel, MenuController, MenuView, MenuItem, MenuFolder
import unittest
from lib import giri_unittest


class Configuration(object):
    def __init__(self):
        self.dictionary = giri_unittest.get_dictionary()
        self.image_path = giri_unittest.get_image_path()



class MenuItemTest(unittest.TestCase):
    """ Collection of tests for menu.py MenuItem class. """
    
    def test_menu_item_init(self):
        """ Creation of a menu item. """
        testitem = MenuItem(7, "testtext", "testicon", "testenabled", 1000)
        self.assertEqual(testitem.id, 7)
        self.assertEqual(testitem.text, "testtext")
        self.assertEqual(testitem.icon, "testicon")
        self.assertEqual(testitem.enabled, "testenabled")
        self.assertEqual(testitem.launch_id, 1000)

    def test_menu_item_update(self):
        """ Update an existing menu item"""
        testitem = MenuItem(7, "testtext", "testicon", "testenabled", 1000)
        self.assertEqual(testitem.id, 7)
        self.assertEqual(testitem.text, "testtext")
        self.assertEqual(testitem.icon, "testicon")
        self.assertEqual(testitem.enabled, "testenabled")
        self.assertEqual(testitem.launch_id, 1000)
        rval = testitem.update("updatedtext", "updatedicon", "updatedenabled", 1005)
        self.assertEqual(rval, True)
        self.assertEqual(testitem.id, 7)
        self.assertEqual(testitem.text, "updatedtext")
        self.assertEqual(testitem.icon, "updatedicon")
        self.assertEqual(testitem.enabled, "updatedenabled")
        self.assertEqual(testitem.launch_id, 1005)


class MenuFolderTest(unittest.TestCase):
    """ Collection of tests for menu.py MenuFolder class. """

    def test_menu_folder_init(self):
        """ Creation of a menu folder. """
        testfolder = MenuFolder(-1, 7, "testtext", "testicon")

        self.assertEqual(testfolder.id, 7)
        self.assertEqual(testfolder.text, "testtext")
        self.assertEqual(testfolder.icon, "testicon")
        self.assertEqual(testfolder.folders, [])
        self.assertEqual(testfolder.items, [])

    def test_menu_folder_insert_folder(self):
        """ Insert a folder into a folder. """
        testfolder = MenuFolder(-1, 7, "testtext", "testicon")
        testfolder.insert_folder(8, "testtext2", "testicon2")
        self.assertEqual(testfolder.folders[0].id, 8)
        self.assertEqual(testfolder.folders[0].text, "testtext2")
        self.assertEqual(testfolder.folders[0].icon, "testicon2")

    def test_menu_folder_insert_several_folders(self):
        """ Insert a number of folders into a folder. """
        testfolder = MenuFolder(-1, 5, "testtext1", "testicon1")
        testfolder.insert_folder(10, "testtext2", "testicon2")
        testfolder.insert_folder(20, "testtext3", "testicon3")
        testfolder.insert_folder(30, "testtext4", "testicon4")

        # Insert folders and access folder elements.        
        self.assertEqual(testfolder.folders[0].id, 10)
        self.assertEqual(testfolder.folders[0].text, "testtext2")
        self.assertEqual(testfolder.folders[0].icon, "testicon2")
        self.assertEqual(testfolder.folders[0].folders, [])
        self.assertEqual(testfolder.folders[0].items, [])

        self.assertEqual(testfolder.folders[1].id, 20)
        self.assertEqual(testfolder.folders[1].text, "testtext3")
        self.assertEqual(testfolder.folders[1].icon, "testicon3")
        self.assertEqual(testfolder.folders[1].folders, [])
        self.assertEqual(testfolder.folders[1].items, [])

        self.assertEqual(testfolder.folders[2].id, 30)
        self.assertEqual(testfolder.folders[2].text, "testtext4")
        self.assertEqual(testfolder.folders[2].icon, "testicon4")
        self.assertEqual(testfolder.folders[2].folders, [])
        self.assertEqual(testfolder.folders[2].items, [])

    def test_menu_folder_remove_folder(self):
        """ Remove a folder from a folder. """
        testfolder = MenuFolder(-1, 5, "testtext1", "testicon1")
        testfolder.insert_folder(10, "testtext2", "testicon2")
        testfolder.insert_folder(20, "testtext3", "testicon3")
        testfolder.insert_folder(30, "testtext4", "testicon4")

        testfolder.remove_folder(20)

        self.assertEqual(testfolder.folders[0].id, 10)
        self.assertEqual(testfolder.folders[0].text, "testtext2")
        self.assertEqual(testfolder.folders[0].icon, "testicon2")
        self.assertEqual(testfolder.folders[0].folders, [])
        self.assertEqual(testfolder.folders[0].items, [])

        self.assertEqual(testfolder.folders[1].id, 30)
        self.assertEqual(testfolder.folders[1].text, "testtext4")
        self.assertEqual(testfolder.folders[1].icon, "testicon4")
        self.assertEqual(testfolder.folders[1].folders, [])
        self.assertEqual(testfolder.folders[1].items, [])

    def test_menu_folder_insert_item(self):
        """ Insert a item into a folder. """
        testfolder = MenuFolder(-1, 7, "testtext1", "testicon1")
        testfolder.insert_item(10, "testtext2", "testicon2", "testenabled2", 1000)
        self.assertEqual(testfolder.items[0].id, 10)
        self.assertEqual(testfolder.items[0].text, "testtext2")
        self.assertEqual(testfolder.items[0].icon, "testicon2")
        self.assertEqual(testfolder.items[0].enabled, "testenabled2")
        self.assertEqual(testfolder.items[0].launch_id, 1000)

    def test_menu_folder_insert_several_items(self):
        """ Insert a number of items into a folder. """
        testfolder = MenuFolder(-1, 7, "testtext1", "testicon1")
        testfolder.insert_item(10, "testtext2", "testicon2", "testenabled2", 1000)
        testfolder.insert_item(20, "testtext3", "testicon3", "testenabled3", 1001)
        testfolder.insert_item(30, "testtext4", "testicon4", "testenabled4", 1002)
        
        self.assertEqual(testfolder.items[0].id, 10)
        self.assertEqual(testfolder.items[0].text, "testtext2")
        self.assertEqual(testfolder.items[0].icon, "testicon2")
        self.assertEqual(testfolder.items[0].enabled, "testenabled2")
        self.assertEqual(testfolder.items[0].launch_id, 1000)

        self.assertEqual(testfolder.items[1].id, 20)
        self.assertEqual(testfolder.items[1].text, "testtext3")
        self.assertEqual(testfolder.items[1].icon, "testicon3")
        self.assertEqual(testfolder.items[1].enabled, "testenabled3")
        self.assertEqual(testfolder.items[1].launch_id, 1001)

        self.assertEqual(testfolder.items[2].id, 30)
        self.assertEqual(testfolder.items[2].text, "testtext4")
        self.assertEqual(testfolder.items[2].icon, "testicon4")
        self.assertEqual(testfolder.items[2].enabled, "testenabled4")
        self.assertEqual(testfolder.items[2].launch_id, 1002)

    def test_menu_folder_remove_item(self):
        """ Remove an item from a folder. """
        testfolder = MenuFolder(-1, 7, "testtext1", "testicon1")
        testfolder.insert_item(10, "testtext2", "testicon2", "testenabled2", 1000)
        testfolder.insert_item(20, "testtext3", "testicon3", "testenabled3", 1001)
        testfolder.insert_item(30, "testtext4", "testicon4", "testenabled4", 1002)
        
        testfolder.remove_item(20)
        
        self.assertEqual(testfolder.items[0].id, 10)
        self.assertEqual(testfolder.items[0].text, "testtext2")
        self.assertEqual(testfolder.items[0].icon, "testicon2")
        self.assertEqual(testfolder.items[0].enabled, "testenabled2")
        self.assertEqual(testfolder.items[0].launch_id, 1000)

        self.assertEqual(testfolder.items[1].id, 30)
        self.assertEqual(testfolder.items[1].text, "testtext4")
        self.assertEqual(testfolder.items[1].icon, "testicon4")
        self.assertEqual(testfolder.items[1].enabled, "testenabled4")
        self.assertEqual(testfolder.items[1].launch_id, 1002)

    def test_menu_folder_update(self):
        """ Update an existing menu folder. """
        testfolder = MenuFolder(-1, 7, "testtext", "testicon")

        self.assertEqual(testfolder.id, 7)
        self.assertEqual(testfolder.text, "testtext")
        self.assertEqual(testfolder.icon, "testicon")
        self.assertEqual(testfolder.folders, [])
        self.assertEqual(testfolder.items, [])

        testfolder.update("updatetext", "updateicon") 
        self.assertEqual(testfolder.id, 7)
        self.assertEqual(testfolder.text, "updatetext")
        self.assertEqual(testfolder.icon, "updateicon")
        self.assertEqual(testfolder.folders, [])
        self.assertEqual(testfolder.items, [])


class MenuModelTest(unittest.TestCase):
    """ Collection of tests for the menu.py MenuModel class. """

    def test_menu_model_init(self):
        """ Creation of the menu model. """
        test = MenuModel(giri_unittest.get_dictionary())
        self.assertEqual(test.subscribers, [])
        self.assertEqual(test.topfolder.parentid, -1)
        self.assertEqual(test.topfolder.id, -1)
        self.assertEqual(test.topfolder.folders, [])
        self.assertEqual(test.topfolder.items, [])


class MenuControllerTest(unittest.TestCase):
    """ Collection of tests for the menu.py MenuController class. """

    def subscriber(self):
        self.test = True

    def test_menu_controller_init(self):
        """ Creation of the menu controller. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testview = MenuView(testmodel, None, 'view', Configuration())
        testcontroller = MenuController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)        

    def test_menu_controller_add_folder(self):
        """ Add a folder to a folder with a given id. 
        
            The succes of this test implicates that the
            _locate_folder method is also working.
            So there is no seperate test of locate_folder.
        """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)
        testcontroller.add_folder(3, 4, "test4", None)
        testcontroller.add_folder(4, 5, "test5", None)
        testcontroller.add_folder(3, 6, "test6", None)
        testcontroller.add_folder(1, 8, "test8", None)

        # Locating topfolder and inserting folder.
        self.assertEqual([testmodel.topfolder.folders[0].id, 
                          testmodel.topfolder.folders[1].id], 
                          [1,3])
        # Locating first and last folders and inserting folders.
        self.assertEqual([testmodel.topfolder.folders[0].folders[0].id, 
                          testmodel.topfolder.folders[1].folders[0].id,
                          testmodel.topfolder.folders[1].folders[1].id,
                          testmodel.topfolder.folders[0].folders[1].id], 
                          [2,4,6,8])
        # Locating folder in folder and inserting folder.
        self.assertEqual([testmodel.topfolder.folders[1].folders[0].folders[0].id], [5])

    def test_menu_controller_update_folder(self):
        """ Update a folder with a given id. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)
        testcontroller.add_folder(3, 4, "test4", None)
        testcontroller.add_folder(4, 5, "test5", None)
        testcontroller.add_folder(3, 6, "test6", None)
        testcontroller.add_folder(1, 8, "test8", None)

        testcontroller.update_folder(-1, 1, "updatetext1", "updateicon1")
        self.assertEqual(testmodel.topfolder.folders[0].text, "updatetext1")
        self.assertEqual(testmodel.topfolder.folders[0].icon, "updateicon1")

        testcontroller.update_folder(-1, 3, "updatetext2", "updateicon2")
        self.assertEqual(testmodel.topfolder.folders[1].text, "updatetext2")
        self.assertEqual(testmodel.topfolder.folders[1].icon, "updateicon2")
        
        testcontroller.update_folder(4, 5, "updatetext3", "updateicon3")
        self.assertEqual(testmodel.topfolder.folders[1].folders[0].folders[0].text, "updatetext3")
        self.assertEqual(testmodel.topfolder.folders[1].folders[0].folders[0].icon, "updateicon3")

    def test_menu_controller_remove_folder(self):
        """ Remove a folder with a given id. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)
        testcontroller.add_folder(3, 4, "test4", None)
        testcontroller.add_folder(4, 5, "test5", None)
        testcontroller.add_folder(4, 6, "test6", None)

        self.assertEqual(testmodel.topfolder.folders[0].text, "test1")
        testcontroller.remove_folder(-1, 1) 
        self.assertEqual(testmodel.topfolder.folders[0].text, "test3")

        self.assertEqual(testmodel.topfolder.folders[0].folders[0].folders[0].text, "test5")
        testcontroller.remove_folder(4, 5)
        self.assertEqual(testmodel.topfolder.folders[0].folders[0].folders[0].text, "test6")

    def test_menu_controller_add_item(self):
        """ Add an item to a folder with a given id. 
        
            The succes of this test implicates that the
            _locate_folder method is also working.
            So there is no seperate test of locate_folder.
        """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)

        # Locate topfolder and insert item.
        testcontroller.add_item(-1, 10, "item1", None, True, 1000) 
        testcontroller.add_item(-1, 11, "item2", None, True, 1001)
        self.assertEqual([testmodel.topfolder.items[0].id, 
                          testmodel.topfolder.items[1].id],
                          [10,11])
        # Locating first folder and inserting item.
        testcontroller.add_item(1, 12, "item3", None, True, 1002) 
        testcontroller.add_item(1, 13, "item4", None, True, 1003)
        self.assertEqual([testmodel.topfolder.folders[0].items[0].id,
                          testmodel.topfolder.folders[0].items[1].id],
                          [12,13])
        # Locating folder in folder and inserting item.
        testcontroller.add_item(2, 14, "item5", None, True, 1004) 
        testcontroller.add_item(2, 15, "item6", None, True, 1005)
        self.assertEqual([testmodel.topfolder.folders[0].folders[0].items[0].id,
                          testmodel.topfolder.folders[0].folders[0].items[1].id],
                         [14,15])
        # Locating last folder and inserting item.
        testcontroller.add_item(3, 16, "item7", None, True, 1006) 
        testcontroller.add_item(3, 17, "item8", None, True, 1007)
        self.assertEqual([testmodel.topfolder.folders[1].items[0].id,
                          testmodel.topfolder.folders[1].items[1].id],
                          [16,17])
        
    def test_menu_controller_update_item(self):
        """ Update an item with a given id. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)
        # Locate topfolder and insert item.
        testcontroller.add_item(-1, 10, "item1", None, True, 1000) 
        testcontroller.add_item(-1, 11, "item2", None, True, 1001)
        # Locating first folder and inserting item.
        testcontroller.add_item(1, 12, "item3", None, True, 1002) 
        testcontroller.add_item(1, 13, "item4", None, True, 1003)
        # Locating folder in folder and inserting item.
        testcontroller.add_item(2, 14, "item5", None, True, 1004) 
        testcontroller.add_item(2, 15, "item6", None, True, 1005)
        # Locating last folder and inserting item.
        testcontroller.add_item(3, 16, "item7", None, True, 1006) 
        testcontroller.add_item(3, 17, "item8", None, True, 1007)

        testcontroller.update_item(-1, 10, "updatetext", "updateicon", "updateenabled", 1000) 
        self.assertEqual(testmodel.topfolder.items[0].text, "updatetext")

        testcontroller.update_item(2, 14, "updatetext", "updateicon", "updateenabled", 1004) 
        self.assertEqual(testmodel.topfolder.folders[0].folders[0].items[0].text, "updatetext")

    def test_menu_controller_remove_item(self):
        """ Remove an item with a given id. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testcontroller = MenuController(testmodel, None) 
        testcontroller.add_folder(-1, 1, "test1", None)
        testcontroller.add_folder(1, 2, "test2", None)        
        testcontroller.add_folder(-1, 3, "test3", None)
        # Locate topfolder and insert item.
        testcontroller.add_item(-1, 10, "item1", None, True, 1000) 
        testcontroller.add_item(-1, 11, "item2", None, True, 1001)
        # Locating first folder and inserting item.
        testcontroller.add_item(1, 12, "item3", None, True, 1002) 
        testcontroller.add_item(1, 13, "item4", None, True, 1003)
        # Locating folder in folder and inserting item.
        testcontroller.add_item(2, 14, "item5", None, True, 1004) 
        testcontroller.add_item(2, 15, "item6", None, True, 1005)
        # Locating last folder and inserting item.
        testcontroller.add_item(3, 16, "item7", None, True, 1006) 
        testcontroller.add_item(3, 17, "item8", None, True, 1007)

        self.assertEqual(testmodel.topfolder.items[0].text, "item1")
        testcontroller.remove_item(-1, 10)
        self.assertEqual(testmodel.topfolder.items[0].text, "item2")
        
        self.assertEqual(testmodel.topfolder.folders[0].folders[0].items[0].text, "item5")
        testcontroller.remove_item(2, 14)
        self.assertEqual(testmodel.topfolder.folders[0].folders[0].items[0].text, "item6")
        

class MenuViewTest(unittest.TestCase):
    """ Collection of tests for the menu.py MenuView class. """

    def test_menu_view_init(self):
        """ Creation of the common view. """
        testmodel = MenuModel(giri_unittest.get_dictionary())
        testview = MenuView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
