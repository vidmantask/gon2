"""
Unittest for the Common MVC for the presentation.
"""

from components.presentation.common import CommonModel, CommonController, CommonView
import unittest
from lib import giri_unittest


class Configuration(object):
    def __init__(self):
        self.dictionary = giri_unittest.get_dictionary()
        self.image_path = giri_unittest.get_image_path()



class CommonModelTest(unittest.TestCase):
    """ Collection of tests for the common.py CommonModel class. """

    def test_common_model_init(self):
        """ Creation of the Common model enherits from base. """
        test = CommonModel()
        self.assertEqual(test.subscribers, [])


class CommonControllerTest(unittest.TestCase):
    """ Collection of tests for the common.py CommonController class. """

    def test_common_controller_init(self):
        """ Creation of the common controller enherits from base. """
        testmodel = CommonModel()
        testview = CommonView(testmodel, None, 'view', Configuration())
        testcontroller = CommonController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)


class CommonViewTest(unittest.TestCase):
    """ Collection of tests for the common.py CommonView class. """

    def test_common_view_init(self):
        """ Creation of the common view enherits from base. """
        testmodel = CommonModel()
        testview = CommonView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
