"""
Unittest for the Login MVC for the presentation.
"""

from components.presentation.login import LoginModel, LoginController, LoginView
import unittest
from lib import giri_unittest

class Configuration(object):
    def __init__(self):
        self.dictionary = giri_unittest.get_dictionary()
        self.image_path = giri_unittest.get_image_path()


class LoginModelTest(unittest.TestCase):
    """ Collection of tests for the login.py loginModel class. """

    def test_login_model_init(self):
        """ Creation of the login model. """
        test = LoginModel(giri_unittest.get_dictionary())
        self.assertEqual(test.subscribers, [])
        self.assertEqual(test.greeting, "")
        self.assertEqual(test.username, "")
        self.assertEqual(test.password, "")


class LoginControllerTest(unittest.TestCase):
    """ Collection of tests for the login.py loginController class. """

    def subscriber(self):
        self.test = True

    def test_login_controller_init(self):
        """ Creation of the login controller. """
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testview = LoginView(testmodel, None, 'view', Configuration())
        testcontroller = LoginController(testmodel, testview)
        self.assertEqual(testcontroller.model, testmodel)
        self.assertEqual(testcontroller.view, testview)

    def test_login_controller_set_greeting(self):
        """ Setting the greeting and observe subscription alert. """
        self.test = False
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = LoginController(testmodel, None)
        # See that variable is set.
        testcontroller.set_greeting("testgreeting")
        self.assertEqual(testmodel.greeting, "testgreeting")
        # See that subscriber is alerted.
        self.assertTrue(self.test)

    def test_login_controller_set_credentials(self):
        """ Setting credentials and observe subscription alert. """
        self.test = False
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = LoginController(testmodel, None)
        # See that variable is set.
        testcontroller.set_credentials("testusername", "testpassword")
        self.assertEqual(testmodel.username, "testusername")
        self.assertEqual(testmodel.password, "testpassword")
        # See that subscriber is alerted when ok is clicked and not before.
        self.assertFalse(self.test)
        testcontroller.set_next_clicked(True)
        self.assertTrue(self.test)
        
    def test_login_set_and_get_state(self):
        """ Test setting the state of the login (change password or plain). 
        
            @since: 5.3
        """
        self.test = False
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = LoginController(testmodel, None)
        # See that variable is set.
        testcontroller.set_state(testcontroller.ID_STATE_LOGIN) 
        self.assertEqual(testmodel.state, testcontroller.ID_STATE_LOGIN)
        self.assertEqual(testcontroller.get_state(), testcontroller.ID_STATE_LOGIN)

        testcontroller.set_state(testcontroller.ID_STATE_CHANGE_LOGIN) 
        self.assertEqual(testmodel.state, testcontroller.ID_STATE_CHANGE_LOGIN)
        self.assertEqual(testcontroller.get_state(), testcontroller.ID_STATE_CHANGE_LOGIN)
        # See that subscriber is alerted.
        self.assertTrue(self.test)
        
    def test_login_set_and_get_password_request(self):
        """ Test setting and getting the request for a new password. 
        
            @since: 5.3
        """
        self.test = False
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testmodel.subscribe(self.subscriber)
        testcontroller = LoginController(testmodel, None)
        # See that variable is set.
        testcontroller.set_password_request("test1") 
        self.assertEqual(testmodel.requestedpassword, "test1")
        tmp1 = testcontroller.get_password_request()
        self.assertEqual(tmp1, "test1")
        # See that subscriber is NOT alerted.
        self.assertFalse(self.test)
        
class LoginViewTest(unittest.TestCase):
    """ Collection of tests for the login.py loginView class. """

    def test_login_view_init(self):
        """ Creation of the common view. """
        testmodel = LoginModel(giri_unittest.get_dictionary())
        testview = LoginView(testmodel, None, 'view', Configuration())
        self.assertEqual(testview.model, testmodel)
        self.assertEqual(testview.controller.model, testmodel)


#       
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
GIRI_UNITTEST_TAGS = ['gui']
GIRI_UNITTEST_OWNER = 'cso'

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
        