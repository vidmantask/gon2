
class ButtonAreaModelBase(object):
    
    def __init__(self, dictionary):

        self.cancel_clicked = False
        self.cancel_allowed = True
        self.cancel_label = dictionary._("Cancel")
        self.cancel_default_label = dictionary._('Cancel')
        self.cancel_description = ""
        self.cancel_visible = True
        
        self.next_clicked = False
        self.next_allowed = True
        self.next_label = dictionary._("Next")
        self.next_default_label = dictionary._('Next')
        self.next_description = ""
        self.next_visible = True

        self.button_info_text = "..."
        self.default_button = ButtonAreaControllerBase.ID_NEXT_BUTTON

class ButtonAreaControllerBase(object):
    
    def __init__(self, model):
        self.model = model
    
    def set_cancel_clicked(self, clicked):
        """ Set if cancel was requested for this stage. 
        
            This is called if the user clicks the cancel button.
        
            @param state: Boolean indicating the cancel state.
            @note: This method should only be used from GUI side.
            @since: 5.0
            @see: reset_cancelled_state() 
        """
        if self.model.cancel_clicked != clicked:
            self.model.cancel_clicked = clicked
            self.model.alert_subscribers()
    
    def get_cancel_clicked(self):
        """ Get the cancel state. 
            
            Use this to find out if the user has clicked the cancel button.
        
            @return: Boolean indicating the cancelled state. 
            @since: 5.0
        """
        return self.model.cancel_clicked

    def reset_cancel_clicked(self):
        """ Reset the cancel button without notifying. """
        self.model.cancel_clicked = False

    def set_cancel_allowed(self, allowed):
        """ Set if cancel is allowed or not.
        
            If the cancel button is allowed the user can push it. If not 
            then the button is disabled.
        
            @param allowed: Boolean indicating whether cancel is allowed.
            @since: 5.0
        """
        if self.model.cancel_allowed != allowed:
            self.model.cancel_allowed = allowed
            self.model.alert_subscribers()
    
    def get_cancel_allowed(self):
        """ Get whether cancel is allowed. 
        
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.cancel_allowed

    def set_cancel_visible(self, visible):
        """ Set whether the cancel button should be visible.
            
            @param visible: [True | False]
            
            @since: 5.4
        """
        if self.model.cancel_visible != visible:
            self.model.cancel_visible = visible
            self.model.alert_subscribers()
    
    def get_cancel_visible(self):
        """ Get the model setting for the cancel button visibility. 
            
            @return: [True | False]
            
            @since: 5.4
        """
        return self.model.cancel_visible

    def set_cancel_description(self, description):
        """ Set the description of the cancel button.
        
            This description is used when the information area is set
            to display button texts. 
            
            @param description: String describing the action taken when this button is clicked.
            @since: 5.0 
        """
        if self.model.cancel_description != description:
            self.model.cancel_description = description
            self.model.button_info_text = self.model.cancel_description + "\n" + self.model.next_description 
            self.model.alert_subscribers()

    def set_cancel_label(self, label=None):
        """ Set the text for the cancel button.

            This sets the label for the cancel button. 
            
            @param label: String that should display in the cancel button. Use None to reset to it's default value.  
            @since: 5.4
        """
        if self.model.cancel_label != label:
            if label == None:
                self.model.cancel_label = self.model.cancel_default_label
            else:
                self.model.cancel_label = label
            self.model.alert_subscribers()

    def set_next_clicked(self, clicked):
        """ Indicate that the next button has been clicked.
        
            If the user clicks the next button then this method is called
            to set the 'next' state to true indicating that the user requests
            the next stage.
    
            @param state: Boolean indicating the 'next' state.   
            @since: 5.0
            @change: Renamed set_next_state to set_next_clicked
            @note: This method should only be used from GUI side.
            @see: reset_next_state()
        """
        if self.model.next_clicked != clicked:
            self.model.next_clicked = clicked
            self.model.alert_subscribers()
        
    def get_next_clicked(self):
        """ Get the next-state setting. 
        
            Use this to find out if the user has clicked the 'next' button.
        
            @return: Boolean indicating if the user has clicked the 'next' button.
            @since: 5.0
            @change: Renamed get_next_state to get_next_clicked
        """
        return self.model.next_clicked

    def reset_next_clicked(self):
        """ Reset the next button without notifying. """
        self.model.next_clicked = False

    def set_next_allowed(self, allowed):
        """ Set if next stage is allowed or not. 
        
            If the next button is allowed the user can click it. If not it is 
            disabled.
        
            @param allowed: Boolean indicating whether the next button should be allowed.
            @since: 5.0
        """
        if self.model.next_allowed != allowed:
            self.model.next_allowed = allowed
            self.model.alert_subscribers()
        
    def get_next_allowed(self):
        """ Get whether next is allowed. 
        
            @return: Boolean indicating whether or not next is currently allowed.
            @since: 5.0
            @deprecated: I don't think anybody uses this. It will be removed unless someone says otherwise.
        """
        return self.model.next_allowed

    def set_next_visible(self, visible):
        """ Set whether the OK button should be visible.
        
            @param visible: [True | False]
            
            @since: 5.4 
        """
        if self.model.next_visible != visible:
            self.model.next_visible = visible
            self.model.alert_subscribers()

    def get_next_visible(self):
        """ Get the model setting for the OK buttons visibility.
        
            @return: [True | False]
            
            @since: 5.4
        """
        return self.model.next_visible

    def set_next_description(self, description):
        """ Set the description for the 'next' button. 
        
            The description is used in the info area when the information
            mode is set to button texts.
            
            @param description: String describing the action taken if the user clicks the 'next' button.
            @since: 5.0 
        """
        if self.model.next_description != description:
            self.model.next_description = description
            self.model.button_info_text = self.model.cancel_description + "\n" + self.model.next_description 
            self.model.alert_subscribers()
    
    def set_next_label(self, label=None):
        """ Set the text in the 'next' button. 
        
            Use this to set the text in the 'next' button.
            
            @param label: String to display as text in the 'next' button. Use None to set the default string.  
            @since: 5.4
        """
        if self.model.next_label != label:
            if label == None:
                self.model.next_label = self.model.next_default_label
            else:
                self.model.next_label = label
            self.model.alert_subscribers()

    ID_NEXT_BUTTON = 0
    ID_CANCEL_BUTTON = 1

    def set_default_button(self, button_id):
        """ Set the default button
        
            Use this to set the default button to the one 
            specified with the id.
            
            @param id: ID_NEXT_BUTTON | ID_CANCEL_BUTTON as defined in the controller.
            @since: 5.5
        """
        if self.model.default_button != button_id:
            self.model.default_button = button_id
            self.model.alert_subscribers()
