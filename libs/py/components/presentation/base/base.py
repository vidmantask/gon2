
# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------
class ModelBase(object):
    """ Base class for all GUI Models.
    
        You can add subscribers to this class with subscribe(). 
        Whenever the model has been updated, call the 
        alert_subscribers() method to let subscribers know.
    """
    def __init__(self):
        self.subscribers = []
        
    def subscribe(self, subscriber):
        """ Set a method as a subscriber to this model. """
        self.subscribers.append(subscriber)

    def unsubscribe(self, subscriber):
        """ Remove a method as a subscriber to this model. """
        self.subscribers.remove(subscriber)

    def alert_subscribers(self):
        """ Activate all subscribing methods. """
        for subscriber in self.subscribers:
            subscriber()

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class ControllerBase(object):
    """ Base class for all GUI controllers.
    
        This has limited functionality now. But is 
        included to give the possibility for easily
        expanding all inherited classes.
    """
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def subscribe(self, subscriber):
        self.model.subscribe(subscriber)

    def unsubscribe(self, subscriber):
        self.model.unsubscribe(subscriber)
        
    def display(self, views=None):
        self.view.display(views)

    def destroy(self):
        self.view.destroy()
        
# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class ViewBase(object):
    """ Base class for all GUI Views. 
    
        Makes sure that a handle to the view is set
        and distributed to other views.
        This gives you the self.handles dictionary, 
        that will help you connect the view elements.
    """
    def __init__(self, model, handles, name, configuration=None):
        self.model = model
        self.model.subscribe(self.update)
        
        if handles != None:
            self.handles = handles
        else:
            self.handles = {}
        self.handles[name] = self
        self.configuration = configuration
        self.dictionary = self.configuration.dictionary if self.configuration else None
         
    def display(self, views=None):
        """ Make sure the view is visible to the user.
            (This must be implemented for each GUI type.)
        """
        raise NotImplementedError, "There should be a display() method for every view"

    def update(self):
        """ 
            Update the view to be consistent with the model.
            (This must be implemented for each GUI type.)
        """
        raise NotImplementedError, "There should be an update() method for every view"

    def destroy(self):
        """ 
            Destroy the view - clean up nicely.
            (This must be implemented for each GUI type.)
        """
        raise NotImplementedError, "There should be a destroy() method for every view"
