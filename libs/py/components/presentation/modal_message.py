from components.presentation.base.base import ModelBase, ViewBase, ControllerBase
from components.presentation.base.button_area_base import ButtonAreaModelBase, ButtonAreaControllerBase
from string import Template # Do not warn for deprecated module 'string' - it is not, pylint: disable=W0402

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class ModalMessageModel(ModelBase, ButtonAreaModelBase):

    def __init__(self, dictionary):
        ModelBase.__init__(self)
        ButtonAreaModelBase.__init__(self, dictionary)
        
        self.text = ""
        self.background_image = "R0lGODlheABLAOfWALcZP7caQLgbQbgcQbgdQrgeQ7keRLkfRLkgRbohRroiR7ojR7okSLsmSrsnSrwnS7woTLwpTbwqTb0rTr0sT70tUL4vUb4wUr4wU78xU78yVL8zVb80VsA2V8A3WME4WcE5WcE5WsI8XMI9XcM+XsM/X8NAX8RCYcRCYsRDYsVFZMVGZcVHZcZJZ8ZKaMdLaMdMacdMasdNa8hOa8hPbMhQbclSbslTb8lUcMpVccpWcspXc8tYdMtZdMtadcxbdsxcd8xdd8xeeM1eec1fes1ges5ifM5jfc5kfc9mf89ngNBogdBpgtBqg9FrhNFshNFthdJuhtJvh9Jwh9JwiNNxidNyitR0i9R1jNR2jdR3jdV4jtV5j9V6kNZ7kdZ8ktZ9k9d+k9d/lNeAldeBltiCltiDl9iDmNmEmdmFmdmGmtmHm9qInNqJnNuMn9uNoNyPotyQot2Ro92SpN2Tpd2Upd6Vpt6Vp9+XqN+Yqd+Zqt+aq+Cbq+CcrOCdreGeruGgsOKhseKjsuKks+OnteSotuSot+Spt+SquOWrueWsuuauu+avvOawveaxveexvueyv+i2wui3w+m4xOm5xOq6xuq8x+q9yOu+yeu/yuvAyuzBy+zCzOzDze3Ezu3Fz+3G0O7I0e7J0u/M1O/M1e/N1vDO1vDP1/DQ2PHR2fHS2fHT2vLV3PLW3fPX3vPZ3/Pa4PTb4fTc4vTd4vXf5PXf5fXg5fbh5vbi5/bj6Pfl6ffm6vfn6/jo6/jo7Pjp7fnq7vnr7vns7/nt8Pru8frw8vrx8/vx9Pvy9Pvz9fz09vz19/z29/z3+P34+f35+v36+v76+/77/P78/f79/f/+/v///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////yH5BAEKAP8ALAAAAAB4AEsAAAj+AK0JHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXaoTWKVCddDI8YNJ10NbSZIwOjjM0BopSrrA8TTNo7FixZQlk2ZNWjKRu9CgCACgrt26INZUY8imbqqCpHQMuGtXQpljHNnMgGLoBiBrgXKAFLblAOHLAGAwpLYBwAmCzKLQxWy3giSObkZZc1LE2pXWHlF1tmuhSRxBkATRkeIBwByGl+r6GUgsht0HXSq5irXJjQa7AwRtTG0tyZVPbnp4vITA7o1OCU/+WV2YBICBYQKjubDLBHFBaWsE1A0wSWNqZ0w6paD1IxpHWQ/URUAdGBFj2RIDmWFXGQodYtcEvmSEyCvE7DFNGtbE4V5G0YxQ1wCWZMRHXZwI5Ip8APhAzUJk2IVFT4PYRWBGKQDQwYrWRFGXAbMwpMwFdR0AjI+utILMStJwUFcH/mGESl1uCCSMAXU94ZAdduVBkB4ssPCHQJgI9iEO4KEUil1fZsQFAALkIpAjdmHiUC+DATAEQWnUtQU0VmAWQJQnqWEXLhk5AwEAPAykxY7PPHRCXRDgaI0bdTFRRV0ZAFHEc3YlclINeGm0SF2PDNRCXSpANIVdwQxUR13+EQDAwCA4UsMHihg0Y1IIdUmWkQ4ASNCoQB3UhQRElNYFy0AjCriJQWXYVV9JDtSVREJVZKVtVmQclIt8YBDEQF1VQNSHXaIMBIhdUxwUTJ1dmNQdAEokNAFmnxmULCsE1ZkFRHrYpYq6dn2CEAl1CWHSbL4edO9l+RJUzQcAtFAQBXXV+1Abdo1nzbp19YLQDnW9YBILdYmQUC62tGwLFXVFPBAndQVSEMIAyACRjnXpKhDIAGxYEBN1oWAS0QAE0CpDYcRsUBMAJGBMQULUlUCTDZVQVwgEAS00QU4UbZIhdinSUNOeFXSMAgBAYRAedoHiEC8oXtG1XV8PFDb+AEaXxMtoK5ztNEF/1CV3Qaqw69CrdTVyd115C7R33yUZYZcjTA8+EMof7GUQygAgYAtDy2RQ1wXOPB40QpOfxMpoDciyENoyt1LXbwcxYlcQnieEhl13FOQ162Kf5AVtqiVEO0FgADCAxwVNQ4NdZyiUyGgjLCM83sTzjZI0ONh1wBe/ILS8QNBgHIRCu8Rq7dTv1VFAXQjwuz3k3VNuEjJH3GWADW84hCUoYYg6DGEBmoNEXSKxkFG4DwAUIIMnbrELUcABBHZpQCYOMryDtC4l1dhDtUhDmIj9AAAVwFpCXmECEtpFBK5ASAcN8kGVIOMOOMOMAFIwBlMIhBdzg2FQQ6Txh9lchgOAUOH9VufB4rVEGJLwgxvEgIY6EIIUX4tDXV4BkWqgIg5aGEIRtECHVfQOIbWAhBqVOJBSqFETPuHBB4jAlDra8Y54zKMe98jHPvrxj4AMpCAHSchCGvKQiEykIhfJyEY68pGQjGRAAAA7"
        self.window_closed_by_user = False
        self.size = 'small'
        self.html_base = Template("<HTML><HEAD><META http-equiv='Content-Type' content='text/html; charset=utf-8'></HEAD>\
                <BODY style=\"overflow:auto; margin:0px;\">\
                <DIV ID='g_background' style='width:100%; height:100%; overflow:auto;'>\
                    <DIV ID='g_logo_area' style='position:absolute; left:0px; top:0px; width:100px; height:100px;'>\
                    </DIV>\
                    <DIV ID='g_text_area' style='position:absolute; font-family: arial; text-align: left; font-size: 14; font-style: normal; font-weight: normal; line-height: 1.4em; left:125px; top:25px; color:#3A3A3A;'>\
                    $g_html_text\
                    </DIV>\
                </DIV>\
                <script type='text/javascript'>\
                g_logo_image=\"<IMG SRC='data:image/GIF;base64," + self.background_image + "'/>\";\
                g_logo_text=\"<DIV ID='g_logo' style='position:absolute; text-align: center; font-family: arial; font-size: 36; font-style: normal; font-weight: normal; left:15px; top:15px; width:50px; height:50'><a style='color:#CCCCCC'>G/</a><a style='color:#CC0000'>On</a><sub style='font-size: 12; color:#CCCCCC'>TM</sub></DIV>\";\
                g_on_explorer = false;\
                g_use_fallback_version = false;\
                if (navigator.appName == 'Microsoft Internet Explorer') {\
                    g_on_explorer = true;\
                    position = navigator.appVersion.indexOf('MSIE');\
                    if (position > -1) {\
                        g_major_version = navigator.appVersion.charAt(position+5);\
                        if (g_major_version < 7) {\
                            g_use_fallback_version = true;\
                        }\
                    }\
                    else {\
                        g_use_fallback_version = true;\
                    }\
                }\
                if (g_use_fallback_version) {\
                    document.getElementById('g_background').style.background = '#FFFFFF';\
                    document.getElementById('g_logo_area').innerHTML = g_logo_text;\
                }\
                else {\
                    if (g_on_explorer) {\
                        document.getElementById('g_background').style.filter=\"progid:DXImageTransform.Microsoft.Gradient(endColorstr='#FFFFFF', startColorstr='#FFFFFF', gradientType='0')\";\
                    }\
                    else {\
                        document.getElementById('g_background').style.background='#FFFFFF';\
                        document.getElementById('g_background').style.background=\"-webkit-gradient(linear, left top, left bottom, from(#FFFFFF), to(#FFFFFF))\";\
                    }\
                    document.getElementById('g_logo_area').innerHTML = g_logo_image;\
                }\
                </script>\
                </BODY>\
                </HTML>")


# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class ModalMessageController(ControllerBase, ButtonAreaControllerBase):

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)
        ButtonAreaControllerBase.__init__(self, model)

    def set_text(self, text):
        if self.model.text != text:
            #self.model.text = self.model.html_base.substitute(g_html_text=text)
            self.model.text = text
            self.model.alert_subscribers()

    def set_and_format_text(self, body, head = None):
        text = ''
        if head is not None:
            text += '<p><b>' + head + '</b></p>'
        text += '<p>' + body + '</p>'
        self.set_text(text)

    def set_window_closed_by_user(self, closed):
        if self.model.window_closed_by_user != closed:
            self.model.window_closed_by_user = closed
            self.model.alert_subscribers()
        
    def get_window_closed_by_user(self):
        return self.model.window_closed_by_user

    ID_SMALL_SIZE = 'small'
    ID_LARGE_SIZE = 'large'

    def set_size(self, size_id):
        """ Set the size of the modal message window.

            Use small size for alerts, info's, etc.
            Use large for a full page of text.
        
            @param size_id: [ID_SMALL_SIZE | ID_LARGE_SIZE]
            
            @since: 5.4
        """
        if self.model.size != size_id:
            self.model.size = size_id
            self.model.alert_subscribers()

# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class ModalMessageView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = ModalMessageController(self.model, self)
