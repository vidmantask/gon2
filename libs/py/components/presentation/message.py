from components.presentation.base.base import ModelBase, ViewBase, ControllerBase

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class MessageModel(ModelBase):
    def __init__(self):
        ModelBase.__init__(self)
        self.headline = ""
        self.message = ""


# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class MessageController(ControllerBase):

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)

    def set_message(self, headline, message):
        """ Set the message that is displayed to the user"""
        if self.model.headline != headline or self.model.message != message:
            self.model.headline = headline
            self.model.message = message
            self.model.alert_subscribers()


# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class MessageView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = MessageController(self.model, self)
