from components.presentation.base.base import ModelBase, ViewBase, ControllerBase

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------


class CommonModel(ModelBase):
    """ Definitions for the common model. """

    def __init__(self):
        ModelBase.__init__(self)
        self.shutdown = False
        self.visible = True
        self.splash_text = "..."


# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class CommonController(ControllerBase):
    """ Handle changes to the common model. """
    
    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view) 

    def set_shutdown(self):
        """ Set an indicator that we want to start the shutdown sequence. """
        self.model.shutdown = True
        self.model.alert_subscribers()
        
    def get_shutdown(self):
        return self.model.shutdown
    
    def set_visible(self, visible):
        """ Set whether the task bar icon should be visible. """
        if self.model.visible != visible:
            self.model.visible = visible
            self.model.alert_subscribers()

    # ----------------------------
    # Splash overwrites - for now only used for the gtk main window design
    #

    def splash_set_text(self, text):
        # print "CommonController.splash_set_text"
        if self.model.splash_text != text:
            self.model.splash_text = text
            self.model.alert_subscribers()

    def splash_display(self):
        # print "CommonController.splash_display"
        pass

    def splash_clear_text(self):
        # print "CommonController.splash_clear_text"
        self.model.splash_text = ""
        self.model.alert_subscribers()


# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class CommonView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 
    """ Base class for all common view GUI implementations. """

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = CommonController(self.model, self)
