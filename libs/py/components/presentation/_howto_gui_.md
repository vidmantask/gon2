# HOWTO run the G/On GUI

The client runs python version 2 and so does all GUI parts. The GUI parts can be run independently of the rest of the client.

## Setup

``` bash
export PYTHONPATH=/home/gbuilder/Source/gon/libs/py
```

## Run a simulation of the full GUI

``` bash
cd libs/py/components/presentation/
python user_interface.py
```

## Run the individual parts (f.ex. for Gtk login view) 

``` bash
cd libs/py/components/presentation/gui/gtk
python gtk_login_view.py
```

## Notes
- The gtk_app folder was an attempt at creating a one-view app. It is not functional.
- Tests seems to not be fully functional

## Changelog

###### `v.5.8`
- For the G/On OS Gtk GUI we attempted to combine the splash and the menu into a main window with a nice background image  

 