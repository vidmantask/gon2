from components.presentation.base.base import ModelBase, ViewBase, ControllerBase
from string import Template  # Do not warn for deprecated module 'string' - it is not, pylint: disable=W0402

# ---------------------------------------------------------------
# Model part of the MVC 
# ---------------------------------------------------------------

class SplashModel(ModelBase):

    def __init__(self):
        ModelBase.__init__(self)
        
        self.text = ""
        self.background_image = "R0lGODlhdwGvAOesALcZP7caQLgbQbgdQrgeQ7kfRLkgRbohRroiR7ojR7okSLsmSrwnS7woTLwqTb0rTr0sT70tUL0uUL4vUb80VsA1VsA3WME4WcI7XMI8XMI9XcRCYcRCYsRDYsVEY8VFZMVGZcVHZcZJZ8ZKaMdLaMdMashPbMhQbclSbslTb8pVccpWcspXc8tYdMtZdMtadcxbdsxdd8xeeM1fes5ifM5jfc5kfc9lfs9mf89ngNBngdBogdBpgtBqg9FrhNFthdJvh9Jwh9JwiNNyitNzitR2jdV4jtV6kNeAldiDl9iDmNmEmdmFmdmGmtqJnNuLntuMn9yOod2Ro92SpN2Tpd2Upd6Vpt6Vp96WqN+Yqd+aq+Cbq+CcrOCdreGeruKiseKjsuKks+OltOOmtOSotuSot+SquOWrueWsuuWtuuexvueyv+ezwOi1wei3w+m4xOm5xOm6xeq6xuq7x+q8x+q9yOu/yuvAyuzBy+zCzO3Ezu3Fz+3H0O7I0e7J0u/L0+/M1O/M1fDP1/DQ2PHR2fHT2vLV3PLW3PLW3fPa4PTb4fTc4vTd4vXe4/Xf5fbh5vbi5/bj6Pfl6ffm6vfn6/jo6/jo7Pjp7fns7/nt8Pru8frv8frw8vrx8/vx9Pvy9Pvz9fz09vz19/z3+P34+f35+v36+v76+/77/P78/f79/f/+/v///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////yH5BAEKAP8ALAAAAAB3Aa8AAAj+AFkJHEiwoMGDCBMqXMiwocOHECNKnEixosWLGDNq3Mixo8ePIEOKHEmypMmTKFOqXMmypcuXMGPKnEmzps2bOHPq3Mmzp8+fQIMKHUq0qNGjSJMqXcq0qdOnUKNKnUq1qtWrWLNq3cq1q9evYMOKHUu2rNmzaNOqXcu2rdu3cOPKnUu3rt27ePPqxZhpDA8RFChswCFGE0I6AJAMLPWFhYQAC0A4oTTxDZQ6V1ilsbS381BSSxQAGE169ByENwAcEvjnwugBEgiMTuBlIhRVJxrxAOS5t89KIUarKKNIk6VDaHSQOqipAAmBeRAA2BAHFatUdT6MjiIRSqYtOND+yPFNPueo4A1OP9QCIAyrShAArFhOsBQMAAL0RGRzKVCYS32UJ2BNTABQwB8RdZDAJ6wUAYADmxzkSQQAjDDghUdpkgAASUQ0CABBsMLJAQBIkRAWoxEikB44TMFKIT5gQEEMd2Boo01njDZJREYAEKAboz2SECWjWSEQGgCYYIZspAVQ241QwiQEABpEREoDGKzCyhIATLBQBQD0IBAbADxgQA6LoDJIBwAYIEmUcLLUAgAuGGQIIXjiqeVAaQBQhUBAAPCcQiYA0IJAcYwmw56sMBIAAFnEKSlKJQBAg0EOlAZAKgSpMEAlAtUAQAoLqQCACgIh5mNBIwDww6T+sJIUAwAoGFTDCit4MBqnAkEiwAwDTenBQtrpkOpomBSUAwAwxOosSD06oMphuw4EBQBvDOQEAAhYhxAqJC5xLACcFBSEoc+my9Eao/lB7aYCqVKBBKcMZMdo+iG0x2h1jFsuQecequ7AF33CAAAvvMsrHgAoQdAoDwBgQ0LLQjCKv+aiS/DGFEUxGhUHqcrrDgAkUtC1AIxnkB2PmogxwBpzLPNDppww2hCXFCQyK50cYIJBpHAAgAJwFFTHAgCIYMpAqv47UMAzR+0QJ7O22QITVUBRhAXVegEAGQdFkgEAAaiwxRpcuPDoB6AyPZrTAkEt9dwKrYIGm5qW2cP+tCAoAApCnBzBJGkINCGKzm9nLDDdjCM0CRteVNGFGohMywoiABCx0CZoPHEEFG2EcpAla6yxNEGCrMFH46xDdAgYjrQu++y012777bjnrvvuvPfu++/ABy/88MQXb/zxyCev/PLMN+/889BHL/301Fdv/fXYZ6/99tx37/334Icv/vjkl2/++einr/767Lfv/vvwxy///PTXb//9+Oev//789+///wAMoAAHSMACGvCACEygAhfIwAY68IEQjKAEJ0jBClrwghjMoAY3yMEOevCDIAyhCEdIwhKa8IQoTKEKV8jCFrrwhTCMoQxnSMMa2vCGOMyhDnfIwx768IdMQAyiEIdIxCIa8YhITKISl8jEJjrxiVCMohSnSMUqWvGKWMyiFrfIxS568YtgDKMYx0jGMprxjGhMoxrXyMY2uvGNcIyjHOdIxxoGBAA7"
        self.hide = False
        self.html_base = Template("<HTML><HEAD><META http-equiv='Content-Type' content='text/html; charset=utf-8'></HEAD>\
                <BODY style='overflow:hidden; margin:0px;'>\
                <DIV ID='g_splash' style='width:375px; height:175px; overflow:hidden;'>\
                    <DIV ID='g_background' style='width:100%; height:100%;'>\
                    </DIV>\
                </DIV>\
                <script type='text/javascript'>\
                g_text=\"<DIV ID='g_text' style='position:absolute; text-align: center; font-family: arial; font-size: 14; font-style: italic; font-weight: bold; left:50px; top:119px; width:275px; height:125; color:#3A3A3A'></DIV>\";\
                g_logo=\"<DIV ID='g_logo' style='position:absolute; text-align: center; font-family: arial; font-size: 36; font-style: normal; font-weight: normal; left:15px; top:15px; width:50px; height:50'><a style='color:#3A3A3A'>G/ON</a><sub style='font-size: 12; color:#3A3A3A'>TM</sub></DIV>\";\
                g_image=\"<IMG SRC='data:image/GIF;base64," + self.background_image + "'/>\";\
                \
                fallback_version = false;\
                if (navigator.appName == 'Microsoft Internet Explorer') {\
                    position = navigator.appVersion.indexOf('MSIE');\
                    if (position > -1) {\
                        g_major_version = navigator.appVersion.charAt(position+5);\
                        if (g_major_version < 7) {\
                            fallback_version = true;\
                        }\
                    }\
                    else {\
                        fallback_version = true;\
                    }\
                }\
                if (fallback_version) {\
                    document.getElementById('g_background').style.background = '#FFFFFF';\
                    document.getElementById('g_background').innerHTML = g_logo + g_text;\
                }\
                else {\
                    document.getElementById('g_background').innerHTML = g_image + g_text;\
                }\
                document.getElementById('g_text').innerHTML = '$g_html_text';\
                </script>\
                </BODY>\
                </HTML>")

# ---------------------------------------------------------------
# Controller part of the MVC 
# ---------------------------------------------------------------

class SplashController(ControllerBase):

    def __init__(self, model, view):
        ControllerBase.__init__(self, model, view)

    def set_text(self, text):
        """ Add HTML to the text and alert subscribers. 
        
            Explorer version 7 and greater can display in-line
            gifs. Older versions will display the fall-back. 
        """
        if self.model.text != text:
            self.model.text = text
            self.model.alert_subscribers()

    def hide(self):
        self.model.hide = True
        self.model.alert_subscribers()

# ---------------------------------------------------------------
# View part of the MVC 
# ---------------------------------------------------------------

class SplashView(ViewBase): # Do not warn for abstract methods, pylint: disable=W0223 

    def __init__(self, model, common, name, configuration):
        ViewBase.__init__(self, model, common, name, configuration)
        self.controller = SplashController(self.model, self)
