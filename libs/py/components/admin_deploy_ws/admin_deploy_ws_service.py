"""
This file contains the implementation of the web-services offered by the
AdminDeployWS web service interface.
"""
import lib.ws.zsi_hack
import lib.ws.ssl_hack
import lib.ws.zsi_extensions

import traceback

from ws.admin_deploy_ws_services_server import *
from components.rest.rest_api import RestApi
from components.rest.rest_service import RestService

import components.plugin.client_gateway.manager

import os
import os.path
import sys
import threading
import datetime
import httplib

import lib.checkpoint
import admin_deploy_ws_session
import lib.hardware.device

admin_ws_module_id = "admin_deploy_ws"


class AdminDeployWSService(threading.Thread, admin_deploy_wsService):
    """
    All admin deploy web-service calls is dispatched by this class,
    it handles logging and propper error reporting, but the real
    work is dispatched to the session.

    Notice that each time the soap_login method is called the janitor
    that clean-up dead sessions is activated.
    """
    def __init__(self, async_service, environment, config, admin_ws_url, download_root, dictionary):
        threading.Thread.__init__(self)
        admin_deploy_wsService.__init__(self)
        self.async_service = async_service
        self.environment = environment
        self.dictionary = dictionary
        self._sessions = {}
        self._session_id_next = 0
        self.config = config
        self.admin_ws_url = admin_ws_url
        self._session_timeout = datetime.timedelta(days=1, hours=0, minutes=0, seconds=0, microseconds=0)
        self._running = False
        self._sc = None
        self.checkpoint_handler = self.environment.checkpoint_handler
        self._download_root = download_root

        self._additional_device_roots = []
        if self.environment.soft_token_root and os.path.exists(self.environment.soft_token_root):
            self._additional_device_roots.append(self.environment.soft_token_root)

        self.environment.runtime_plugin_manager = components.plugin.client_gateway.manager.Manager(self.environment.checkpoint_handler, self.environment.plugin_modules_root)
        self.environment.runtime_plugins = self.environment.runtime_plugin_manager.create_instances(async_service=self.async_service, checkpoint_handler=self.environment.checkpoint_handler, user_interface=None, additional_device_roots=self._additional_device_roots)

        self._ping_last_ts = datetime.datetime.now()

    @property
    def get_admin_deploy_ws_port(self):
        return self.config.management_deploy_ws_port

    @property
    def get_admin_deploy_ws_ip_host(self):
        return self.config.management_deploy_ws_ip

    @property
    def get_session_timeout(self):
        return self._session_timeout

    def get_session(self, session_id):
        try:
            return self._sessions[session_id]
        except KeyError:
            self.handle_unknown_session_exception()

    def get_ping_ts(self):
        return self._ping_last_ts

    def run(self):
        try:
            self._running = True
            self.checkpoint_handler.Checkpoint("run", admin_ws_module_id, lib.checkpoint.DEBUG, ip = self.config.management_deploy_ws_ip, port = self.config.management_deploy_ws_port)
            services = (self,)
            address = (self.config.management_deploy_ws_ip, self.config.management_deploy_ws_port)

            # TODO (vazarovic): Implement RestService as daemon (self._sc)
            RestService.start_admin_deploy_wsgi(self)

            # self.admin_deploy_thread = threading.Thread(name="AdminDeployWSGI",
            #                                                 target=RestService.start_admin_deploy_wsgi,
            #                                                 args=(self,))
            # self.admin_deploy_thread.setDaemon(True)
            # self.admin_deploy_thread.start()

            # if self.config.management_deploy_ws_https_enabled:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTPS.create(self.checkpoint_handler,
            #    admin_ws_module_id, address, services, self.config.management_deploy_ws_https_certificate_filename_abspath, self.config.management_deploy_ws_https_key_filename_abspath)
            # else:
            #     self._sc = lib.ws.zsi_extensions.ServiceContainerHTTP.create(self.checkpoint_handler,
            #    admin_ws_module_id, address, services)

            self._sc.serve_forever()

            self.checkpoint_handler.Checkpoint("run.terminate", admin_ws_module_id, lib.checkpoint.DEBUG)
            self._sc.server_close()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("run.error", admin_ws_module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        self._running = False

    def stop(self):
        if self._running:
            self._sc.shutdown()

    def rest_login(self, request):
        self.janitor_remove_dead_sessions()
        session_id = u"%i" % self._session_id_next
        self._session_id_next += 1
        self.environment.checkpoint_handler.Checkpoint("login", admin_ws_module_id, lib.checkpoint.INFO, session_id="%r" % session_id)
        with self.environment.checkpoint_handler.CheckpointScope("rest_login", admin_ws_module_id, lib.checkpoint.DEBUG, session_id="%r" % session_id):
            try:
                session = admin_deploy_ws_session.AdminDeployWSSession(self.environment, session_id,
                                                                       request.get_username,
                                                                       request.get_password, self.admin_ws_url,
                                                                       self._download_root, self._additional_device_roots, self.dictionary)

                self._sessions[session_id] = session
                response = RestApi.AdminDeploy.LoginResponse()
                response.set_session_id(session_id)
                return response
            except:
                self.handle_unexpected_exception()

    def rest_logout(self, request):
        session_id = request.get_session_id
        self.environment.checkpoint_handler.Checkpoint("logout", admin_ws_module_id, lib.checkpoint.INFO, session_id="%r" % session_id)
        with self.environment.checkpoint_handler.CheckpointScope("rest_logout", admin_ws_module_id, lib.checkpoint.DEBUG, session_id="%r" % session_id):
            try:
                del self._sessions[session_id]
                response = RestApi.AdminDeploy.LogoutResponse()
                response.set_rc(True)
                return response
            except KeyError:
                response = RestApi.AdminDeploy.LogoutResponse()
                response.set_rc(False)
                return response
            except:
                self.handle_unexpected_exception()

    def rest_get_token_types(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_token_types", admin_ws_module_id, lib.checkpoint.DEBUG, session_id="%r" % session_id):
            try:
                return self.get_session(session_id).GetTokenTypes(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_tokens(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_tokens", admin_ws_module_id, lib.checkpoint.DEBUG, session_id="%r" % session_id):
            try:
                return self.get_session(session_id).GetTokens(request)
            except:
                self.handle_unexpected_exception()

    def rest_init_token(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_init_token", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).InitToken(request)
            except:
                self.handle_unexpected_exception()

    def rest_deploy_token(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_deploy_token", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).DeployToken(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_gpm_collections(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_gpm_collections", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetGPMCollections(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_runtime_env_info(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_get_runtime_env_info", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).GetRuntimeEnvInfo(request)
            except:
                self.handle_unexpected_exception()

    def rest_install_gpm_collection(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_install_gpm_collection", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).InstallGPMCollection(request)
            except:
                self.handle_unexpected_exception()

    def rest_get_job_info(self, request):
        session_id = request.get_session_id
        try:
            return self.get_session(session_id).GetJobInfo(request)
        except:
            self.handle_unexpected_exception()

    def rest_cancel_job(self, request):
        session_id = request.get_session_id
        with self.environment.checkpoint_handler.CheckpointScope("rest_cancel_job", admin_ws_module_id, lib.checkpoint.DEBUG):
            try:
                return self.get_session(session_id).CancelJob(request)
            except:
                self.handle_unexpected_exception()

    def rest_ping(self, request):
        session_id = request.get_session_id
        rc = True
        if session_id and session_id not in self._sessions:
            rc = False

        self._ping_last_ts = datetime.datetime.now()

        response = RestApi.AdminDeploy.PingResponse()
        response.set_rc(rc)
        return response

#
# Handling of exceptions
#
    def handle_unknown_session_exception(self):
        (etype, evalue, etrace) = sys.exc_info()
        self.environment.checkpoint_handler.CheckpointException("handle_unknown_session_exception", admin_ws_module_id, lib.checkpoint.WARNING, etype, evalue, etrace)
        error_message = 'Server sesssion lost or expired'
        fault_response = RestApi.Admin.FaultServerElement()
        fault_response.set_error_message(error_message)
        raise fault_response

    def handle_unexpected_exception(self):
        (etype, evalue, etrace) = sys.exc_info()
        self.environment.checkpoint_handler.CheckpointException("handle_unexpected_exception", admin_ws_module_id, lib.checkpoint.CRITICAL, etype, evalue, etrace)
        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        fault_response = RestApi.Admin.FaultServerElement()
        fault_response.set_error_message(error_message)
        raise fault_response

    def handle_exception_operation_not_allowed(self, message):
        (etype, evalue, etrace) = sys.exc_info()
        error_message = ''.join(traceback.format_exception_only(etype, evalue))
        self.environment.checkpoint_handler.Checkpoint("handle_exception_operation_not_allowed", admin_ws_module_id, lib.checkpoint.DEBUG, message=error_message)
        fault_response = RestApi.Admin.FaultServerOperationNotAllowedElement()
        fault_response.set_error_message(message.message)
        raise fault_response

    def janitor_remove_dead_sessions(self):
        for session in self._sessions.values():
            if session.is_dead(self._session_timeout):
                self.environment.checkpoint_handler.Checkpoint("janitor_remove_dead_session", admin_ws_module_id, lib.checkpoint.DEBUG, session_id="%r" % session.session_id)
                del self._sessions[session.session_id]

    @classmethod
    def run_default(cls, async_service, checkpoint_handler, db_url, config, plugin_root, config_root, soft_token_root, admin_ws_url, download_root, license_handler):
        import components.environment
        import components.database.server_common.database_connection
        import components.database.server_common.schema_api

        import components.plugin.server_management.manager
        import components.auth.server_common.database_schema
        import components.management_message.server_common.database_schema
        import components.traffic.server_common.database_schema

        import components.dictionary.common

        dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, '.')
        component_env = components.environment.Environment(checkpoint_handler, db_url)
        component_env.plugin_manager = components.plugin.server_management.manager.Manager(checkpoint_handler, component_env.get_default_database(), license_handler, plugin_root)
        component_env.plugin_modules_root = plugin_root
        component_env.soft_token_root = soft_token_root
        component_env.gpms_root = os.path.join(config_root, 'gpm', 'gpms')
        component_env.gpmc_defs_root = os.path.join(config_root, 'gpm', 'gpmcdefs')

        components.database.server_common.schema_api.SchemaFactory.connect(component_env.get_default_database())
        service = AdminDeployWSService(async_service, component_env, config, admin_ws_url, download_root, dictionary)
        service.start()
        return service



#
#
#if __name__ == "__main__" :
#    import components.communication.async_service
#    import components.config.common
#
#    _checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()
#    _config = components.config.common.ConfigClientManagementService()
#    _config_root = '../../../setup/dev_env/gon_installation/config'
#
#    async_service = components.communication.async_service.AsyncService(_checkpoint_handler, "Test", 1)
#    service = AdminDeployWSService.run_default(async_service, _checkpoint_handler, "sqlite:///:memory", _config, '../..', _config_root, '../../setup/demo/soft_token_root', 'https://127.0.0.1:8080', '../download_cache_root', None)
#    service.join()
