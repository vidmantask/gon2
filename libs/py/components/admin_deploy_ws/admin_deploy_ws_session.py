"""
This file contains the
"""
import datetime
import os
import os.path
import threading
import lib.ws
import ssl
import httplib

import components.admin_ws.ws.admin_ws_services
import components.admin_ws.ws.admin_ws_services_types
from components.rest.rest_api import RestApi
from components.rest.http_request_service import HttpRequestService

import lib.gpm.gpm_env
import lib.checkpoint
import lib.ws.ws_util as ws_util

import pprint

from lib import cryptfacility

from components.plugin.client_gateway import plugin_socket_token
from components.plugin.client_gateway import plugin_socket_client_runtime_env

import admin_deploy_job


class SessionError(Exception):
    def __init__(self, error_message):
        Exception.__init__(self)
        self.error_message = error_message

    def __str__(self):
        return repr(self.error_message)


class AdminDeployWSSession(object):
    def __init__(self, environment, session_id, username, password, admin_ws_server_url, download_root, additional_device_roots, dictionary):
        self.environment = environment
        self._download_root = download_root
        self._download_session_lock = threading.Lock()
        self.session_id = session_id
        self.username = username
        self.password = password
        self.report_activity()
        self.dictionary = dictionary
        self._job_handler = lib.ws.job.JobHandler()
        self._plugin_socket_token = plugin_socket_token.PluginSocket(None, self.environment.runtime_plugins)
        self._plugin_socket_client_runtime_env = plugin_socket_client_runtime_env.PluginSocket(None, self.environment.runtime_plugins, environment.checkpoint_handler)

        self._admin_ws_server_url = admin_ws_server_url
        self._admin_ws_login_credentials = (username, password)

        self._additional_device_roots = additional_device_roots

    def report_activity(self):
        self.last_activity = datetime.datetime.now()

    def is_dead(self, timeout):
        return datetime.datetime.now() - self.last_activity > timeout


    def _admin_ws_login(self, gon_admin_ws_server):
        response = gon_admin_ws_server.post("/admin/openSession")
        return response["sessionId"]

    def _admin_ws_logout(self, gon_admin_ws_server):
        response = gon_admin_ws_server.post("/admin/closeSession")
        return RestApi.Admin.CloseSessionResponse()(**response)

    def _admin_ws_get_collections_info(self):
        gon_admin_ws_server = HttpRequestService()
        gon_admin_ws_server.init(base_url=self._admin_ws_server_url, timeout=None, allow_redirects=False, headers=None)
        gon_admin_ws_server_session_id = self._admin_ws_login(gon_admin_ws_server)
        gon_admin_ws_server.set_authorization_header(gon_admin_ws_server_session_id)
        response = gon_admin_ws_server.post("/admin/getGPMCollections")
        response_result = RestApi.Admin.GetGPMCollectionsResponse()(**response)
        self._admin_ws_logout(gon_admin_ws_server)
        return response_result.get_gpm_collections

    def _admin_ws_get_knownsecret_client_and_servers(self):
        gon_admin_ws_server = HttpRequestService()
        gon_admin_ws_server.init(base_url=self._admin_ws_server_url, timeout=None, allow_redirects=False, headers=None)
        gon_admin_ws_server_session_id = self._admin_ws_login(gon_admin_ws_server)
        gon_admin_ws_server.set_authorization_header(gon_admin_ws_server_session_id)

        response = gon_admin_ws_server.post("/admin/deployGetKnownSecretClient")
        response_result = RestApi.Admin.DeployGetKnownSecretClientResponse()(**response)
        knownsecret_client = response_result.get_knownsecret_client

        response = gon_admin_ws_server.post("/admin/deployGetServers")
        response_result = RestApi.Admin.DeployGetServersResponse()(**response)
        servers = response_result.get_servers

        self._admin_ws_logout(gon_admin_ws_server)
        return knownsecret_client, servers

    def GetTokenTypes(self, request):
        response = RestApi.AdminDeploy.GetTokenTypesResponse()

        token_types = self._plugin_socket_token.get_token_types()

        response_token_types = []
        for token_type in token_types:
            response_token_type = RestApi.AdminDeploy.TokenType()
            response_token_type.set_token_type_id(token_type)
            response_token_type.set_token_type_label(token_type)
            response_token_types.append(response_token_type)

        response.set_tokentypes(response_token_types)
        return response

    def GetTokens(self, request):

        #
        # Filter out all runtime_envs that currently is in use
        #
        all_tokens = self._plugin_socket_token.get_tokens(self._additional_device_roots)
        runtime_env_ids_in_use = self._plugin_socket_client_runtime_env.get_in_use_ids(5*60)
        tokens = []
        for token in all_tokens:
            if token.runtime_env_id not in runtime_env_ids_in_use:
                tokens.append(token)
            else:
                self.environment.checkpoint_handler.Checkpoint('GetTokens.ignore_in_use_token', 'admin_deploy_ws_session', lib.checkpoint.DEBUG, runtime_env_id=token.runtime_env_id)

        response_tokens = []
        for token in tokens:
            if not token.token_id:
                continue
            response_token = RestApi.AdminDeploy.Token.TokenInfo()
            response_token.set_token_type_id(token.token_type)
            if token.token_internal_type:
                response_token.set_token_internal_type(token.token_internal_type)
            else:
                response_token.set_token_internal_type(token.token_type)
            response_token.set_token_id(token.token_id)
            response_token.set_token_label(token.get_token_title())
            response_token.set_token_status(token.get_token_status_text())
            response_token.set_token_serial(token.token_serial)
            response_token.set_runtime_env_id(token.runtime_env_id)
            response_token.set_token_type_label(token.get_token_type_title())
            response_tokens.append(response_token)


#        gon_admin_ws_server = components.admin_ws.ws.admin_ws_services.BindingSOAP(self._admin_ws_server_url)
#        gon_admin_ws_server_session_id = self._admin_ws_login(gon_admin_ws_server)
#        request = components.admin_ws.ws.admin_ws_services.GetTokenInfoRequest()
#        request_content = components.admin_ws.ws.admin_ws_services_types.ns0.GetTokenInfoRequestType_Def(None).pyclass()
#        request_content.set_element_session_id(gon_admin_ws_server_session_id)
#        request_content.set_element_tokens(response_tokens)
#        request.set_element_content(request_content)
#        server_response = gon_admin_ws_server.GetTokenInfo(request)
#        self._admin_ws_logout(gon_admin_ws_server, gon_admin_ws_server_session_id)
#
#        response_tokens = server_response._content.get_element_tokens()

        response = RestApi.AdminDeploy.GetTokensResponse()
        response.set_tokens(ws_util.copy_tokens(response_tokens))
        return response

    def InitToken(self, request):
        token_type_id = request.get_token_type_id
        token_id = request.get_token_id

        self._plugin_socket_token.initialize_token(token_type_id, token_id)
        token_serial = self._plugin_socket_token.get_serial(token_type_id, token_id)

        response = RestApi.AdminDeploy.InitTokenResponse()
        response.set_token_serial(token_serial)
        return response

    def DeployToken(self, request):
        token_type_id = request.get_token_type_id
        token_id = request.get_token_id
        generate_keypair = request.get_generate_keypair
        (client_knownsecret, servers) = self._admin_ws_get_knownsecret_client_and_servers()

        self.environment.checkpoint_handler.Checkpoint('DeployToken.deploy_token', 'admin_deploy_ws_session', lib.checkpoint.DEBUG)
        self._plugin_socket_token.deploy_token(token_type_id, token_id, client_knownsecret, servers)

        if generate_keypair:
            self.environment.checkpoint_handler.Checkpoint('DeployToken.generate_keypair', 'admin_deploy_ws_session', lib.checkpoint.DEBUG)
            self._plugin_socket_token.generate_keypair(token_type_id, token_id)
            self.environment.checkpoint_handler.Checkpoint('DeployToken.set_enrolled', 'admin_deploy_ws_session', lib.checkpoint.DEBUG)
            self._plugin_socket_token.set_enrolled(token_type_id, token_id)

        self.environment.checkpoint_handler.Checkpoint('DeployToken.build_response', 'admin_deploy_ws_session', lib.checkpoint.DEBUG)
        response = RestApi.AdminDeploy.DeployTokenResponse()
        response.set_public_key(self._plugin_socket_token.get_public_key(token_type_id, token_id))

        self.environment.checkpoint_handler.Checkpoint('DeployToken.done', 'admin_deploy_ws_session', lib.checkpoint.DEBUG)
        return response


    def GetGPMCollections(self, request):
        response = RestApi.AdminDeploy.GetGPMCollectionsResponse()
        gpm_collections = self._admin_ws_get_collections_info()

        response_collections = []
        for gpm_collection in gpm_collections:
            response_collection = RestApi.Admin.Gpm.GPMCollectionInfo()
            response_collection.set_gpm_collection_id(gpm_collection.get_gpm_collection_id)
            response_collection.set_gpm_collection_summary(gpm_collection.get_gpm_collection_summary)
            response_collection.set_gpm_collection_description(gpm_collection.get_gpm_collection_description)

            response_gpms = []
            for gpm in gpm_collection.get_gpm_collection_gpms:
                response_gpm = RestApi.Admin.Gpm.GPMInfo()
                response_gpm.set_gpm_id(gpm.get_gpm_id)
                response_gpm.set_gpm_summary(gpm.get_gpm_summary)
                response_gpm.set_gpm_description(gpm.get_gpm_description)
                response_gpm.set_gpm_version(gpm.get_gpm_version)
                response_gpm.set_gpm_arch(gpm.get_gpm_arch)
                response_gpm.set_gpm_checksum(gpm.get_gpm_checksum)
                response_gpms.append(response_gpm)
            response_collection.set_gpm_collection_gpms(response_gpms)
            response_collections.append(response_collection)

        response.set_gpm_collections(response_collections)
        return response

    def GetRuntimeEnvInfo(self, request):
        runtime_env_id = request.get_runtime_env_id

        info = self._plugin_socket_client_runtime_env.get_info(runtime_env_id)
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_meta_infos = self._plugin_socket_client_runtime_env.get_installed_gpm_meta_all(runtime_env_id, error_handler)
        if not error_handler.ok():
            self.environment.checkpoint_handler.CheckpointMultilineMessage('GetRuntimeEnvInfo.gpm_meta', 'admin_deploy_ws_session', lib.checkpoint.WARNING, error_handler.dump_as_string())

        response = RestApi.AdminDeploy.GetRuntimeEnvInfoResponse()

        response.set_runtime_env_id(info.id)
        response.set_runtime_env_label(info.title)
        response_gpm_meta_infos = []
        for gpm_meta_info in gpm_meta_infos:
            response_gpm_meta_info = RestApi.AdminDeploy.GPMInfo()
            response_gpm_meta_info.set_gpm_id(gpm_meta_info.header.get_package_id())
            response_gpm_meta_info.set_gpm_label(gpm_meta_info.header.get_package_id())
            response_gpm_meta_info.set_gpm_description(gpm_meta_info.header.descirptions.get_as_string())
            response_gpm_meta_info.set_gpm_version(gpm_meta_info.header.version.get_as_string())
            response_gpm_meta_info.set_gpm_version_release(gpm_meta_info.header.version_release.get_as_string())
            response_gpm_meta_info.set_gpm_arch(gpm_meta_info.header.arch)
            response_gpm_meta_infos.append(response_gpm_meta_info)
        response.set_installed_gpms(response_gpm_meta_infos)
        return response

    def InstallGPMCollection(self, request):
        runtime_env_id = request.get_runtime_env_id
        gpm_collection_id = request.get_gpm_collection_id
        (knownsecret, servers) = self._admin_ws_get_knownsecret_client_and_servers()
        job = admin_deploy_job.AdminDeployJobInstallGPMCollection(self.environment.checkpoint_handler, gpm_collection_id, runtime_env_id, self._plugin_socket_client_runtime_env, self._admin_ws_server_url, self._admin_ws_login_credentials, self._download_root, self._download_session_lock, self.dictionary, knownsecret, servers)
        job_id = self._job_handler.add_job(job)
        response = RestApi.AdminDeploy.InstallGPMCollectionResponse()
        response.set_job_id(job_id)
        return response

    def GetJobInfo(self, request):
        job_id = request.get_job_id
        job = self._job_handler.get_job(job_id)
        if job is not None:
            job_status = job.get_status()
            response = RestApi.AdminDeploy.GetJobInfoResponse()
            response_info = RestApi.AdminDeploy.Job.JobInfo()
            response_info.set_job_more(job_status.more)
            response_info.set_job_status(job_status.get_response_job_status())
            response_info.set_job_header(job_status.header)
            response_info.set_job_sub_header(job_status.sub_header)
            response_info.set_job_progress(job_status.progress)
            response.set_job_info(response_info)
            return response
        raise SessionError('Invalid job id %d' % job_id)

    def CancelJob(self, request):
        job_id = request.get_job_id
        job = self._job_handler.get_job(job_id)
        if job is not None:
            job.cancel()
            response = RestApi.AdminDeploy.CancelJobResponse()
            response.set_rc(True)
            return response
        raise SessionError('Invalid job id %d' % job_id)
