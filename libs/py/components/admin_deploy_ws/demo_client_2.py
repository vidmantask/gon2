import ws.admin_deploy_ws_services
from components.rest.rest_api import RestApi
import sys
import time


def main():
    gon_server_management = ws.admin_deploy_ws_services.BindingSOAP("https://127.0.0.1:8081", tracefile=sys.stdout)

    request = RestApi.AdminDeploy.LoginRequest()

    request.set_username("hej")
    request.set_password("davs")

    response = gon_server_management.Login(request)
    session_id = response.get_session_id
    print session_id

    request = RestApi.AdminDeploy.GetTokenTypesRequest()
    request.set_session_id(session_id)
    print "GetTokenTypes:"
    response = gon_server_management.GetTokenTypes(request)
    for tokentype in response.get_tokentypes:
        print "TokenType:", tokentype.get_token_type_id, tokentype.get_token_type_label

    request = RestApi.AdminDeploy.GetTokensRequest()
    request.set_session_id(session_id)
    print "GetTokens:"
    response = gon_server_management.GetTokens(request)
    for token in response.get_tokens:
        print "Token:", token.get_token_id, token.get_token_label, token.get_token_type_id, token.get_token_status, token.get_token_serial, token.get_runtime_env_id

    token_type_id = response.get_tokens[1].get_token_type_id
    token_id = response.get_tokens[1].get_token_id

    request = RestApi.AdminDeploy.InitTokenRequest()
    request.set_session_id(session_id)
    request.set_token_type_id(token_type_id)
    request.set_token_id(token_id)
    request.set_token_serial('serial_001')
    print "InitToken"
    response = gon_server_management.InitToken(request)
    print response.get_rc

#
#    request = ws.AdminDeployWS_services.DeployTokenRequest()
#    request_c = ws.AdminDeployWS_services.ns0.DeployTokenRequestType_Def(None).pyclass()
#    request_c.set_element_session_id(session_id)
#    request_c.set_element_token_type_id(token_type_id)
#    request_c.set_element_token_id(token_id)
#    request_c.set_element_client_knownsecret('0101010203040506070809')
#    request_c.set_element_servers('127.0.0.1 8042')
#    request.set_element_content(request_c)
#    print "DeployToken"
#    response = gon_server_management.DeployToken(request)
#    print response.get_element_content().get_element_public_key()

    request = RestApi.AdminDeploy.InstallGPMCollectionRequest()
    request.set_session_id(session_id)
    request.set_runtime_env_id('xxx')
    request.set_gpm_collection_id('yyy')
    print "DeployToken"
    response = gon_server_management.InstallGPMCollection(request)
    job_id = response.get_job_id
    print job_id

    done = False

    while not done:
        time.sleep(2)
        request = RestApi.AdminDeploy.GetJobInfoRequest()
        request.set_session_id(session_id)
        request.set_job_id(job_id)
        print "GetJobInfo"
        response = gon_server_management.GetJobInfo(request)
        print response.get_job_status
        print response.get_job_action_header
        print response.get_job_action_info

        done = response.get_job_status == 3


if __name__ == '__main__':
    main()
