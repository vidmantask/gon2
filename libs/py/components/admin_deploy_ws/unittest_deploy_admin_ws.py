"""
Unittest of admin deploy web-service
"""
from __future__ import with_statement
import unittest
from lib import giri_unittest

import time
import os
import os.path
import tempfile
import shutil

import components.communication.async_service

import components.admin_ws.admin_ws_service
import components.admin_deploy_ws.admin_deploy_ws_service
import components.admin_deploy_ws.ws.admin_deploy_ws_services as admin_deploy_ws_services


class AdminDeployWSTest(unittest.TestCase):

    def setUp(self):
        self.gon_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.gon_installation.generate_gpms()
        self.gon_installation.generate_setupdata()
        self.gon_installation.restore_plugins()

        self._admin_service_host = self.gon_installation.config_server_management.management_ws_ip
        self._admin_service_port = self.gon_installation.config_server_management.management_ws_port
        self._admin_service_url = "http://%s:%d" % (self._admin_service_host, self._admin_service_port)
        self._db_url = self.gon_installation.get_db_connect_url()
        self._config_root = self.gon_installation.get_config_folder()

        plugin_folder = giri_unittest.PY_ROOT
        self._admin_ws = components.admin_ws.admin_ws_service.AdminWSService.run_default(giri_unittest.get_checkpoint_handler(), self._db_url, self._admin_service_host, self._admin_service_port, plugin_folder, self._config_root, None)

        self._admin_deploy_ws_host = self.gon_installation.config_client_management_service.management_deploy_ws_ip
        self._management_deploy_ws_port = self.gon_installation.config_client_management_service.management_deploy_ws_port
        self._download_root = self.gon_installation.get_deployment_download_cache_folder()

        self._soft_token_path = self.gon_installation.get_soft_token_path()
        self._admin_deploy_ws = components.admin_deploy_ws.admin_deploy_ws_service.AdminDeployWSService.run_default(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), self._db_url, self.gon_installation.config_client_management_service, plugin_folder, self._config_root, self._soft_token_path,  self._admin_service_url, self._download_root, None)
        time.sleep(2)

    def tearDown(self):
        time.sleep(2)
        self._admin_deploy_ws.stop()
        self._admin_ws.stop()
        time.sleep(1)



    def _login(self, gon_deploy_service):
        request = admin_deploy_ws_services.LoginRequest()
        request_content = admin_deploy_ws_services.ns0.LoginRequestType_Def(None).pyclass()
        request_content.set_element_username("ib")
        request_content.set_element_password("FidoTheDog")
        request.set_element_content(request_content)
        response = gon_deploy_service.Login(request)
        return response.get_element_content().get_element_session_id()

    def _logout(self, gon_deploy_service, session_id):
        request = admin_deploy_ws_services.LogoutRequest()
        request_content = admin_deploy_ws_services.ns0.LogoutRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_deploy_service.Logout(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)


    def test_get_tokentypes(self):
        time.sleep(1)
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))
        session_id = self._login(gon_deploy_service)

        request = admin_deploy_ws_services.GetTokenTypesRequest()
        request_content = admin_deploy_ws_services.ns0.GetTokenTypesRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_deploy_service.GetTokenTypes(request)
        self.assertTrue(len(response.get_element_content().get_element_tokentypes()) > 0)
        print "GetTokenTypes:"
        for tokentype in response.get_element_content().get_element_tokentypes():
            print "TokenType:", tokentype.get_element_token_type_id(), tokentype.get_element_token_type_label()

        self._logout(gon_deploy_service, session_id)

    def test_list_tokens(self):
        time.sleep(1)
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))
        session_id = self._login(gon_deploy_service)

        request = admin_deploy_ws_services.GetTokensRequest()
        request_c = admin_deploy_ws_services.ns0.GetTokensRequestType_Def(None).pyclass()
        request_c.set_element_session_id(session_id)
        request.set_element_content(request_c)
        print "GetTokens:"
        response = gon_deploy_service.GetTokens(request)
        self.assertTrue(len(response.get_element_content().get_element_tokens()) > 0)

        for token in response.get_element_content().get_element_tokens():
            print "Token:", token.get_element_token_id(), token.get_element_token_label(), token.get_element_token_type_id(), token.get_element_token_status(), token.get_element_token_serial(), token.get_element_runtime_env_id(), token.get_element_token_type_label()

    def test_init_and_deploy_tokens(self):
        time.sleep(1)
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))
        session_id = self._login(gon_deploy_service)

        request = admin_deploy_ws_services.GetTokensRequest()
        request_c = admin_deploy_ws_services.ns0.GetTokensRequestType_Def(None).pyclass()
        request_c.set_element_session_id(session_id)
        request.set_element_content(request_c)
        print "GetTokens:"
        response = gon_deploy_service.GetTokens(request)
        self.assertTrue(len(response.get_element_content().get_element_tokens()) > 0)

        serial = 0
        for token in response.get_element_content().get_element_tokens():
            print "Token:", token.get_element_token_id(), token.get_element_token_label(), token.get_element_token_type_id(), token.get_element_token_status(), token.get_element_token_serial(), token.get_element_runtime_env_id()

            time.sleep(5)
            print "InitToken"
            request = admin_deploy_ws_services.InitTokenRequest()
            request_c = admin_deploy_ws_services.ns0.InitTokenRequestType_Def(None).pyclass()
            request_c.set_element_session_id(session_id)
            request_c.set_element_token_type_id(token.get_element_token_type_id())
            request_c.set_element_token_id(token.get_element_token_id())
            request.set_element_content(request_c)
            response = gon_deploy_service.InitToken(request)
            self.assertTrue(response.get_element_content().get_element_token_serial() != None )
            serial += 1

            print "DeployToken"
            request = admin_deploy_ws_services.DeployTokenRequest()
            request_c = admin_deploy_ws_services.ns0.DeployTokenRequestType_Def(None).pyclass()
            request_c.set_element_session_id(session_id)
            request_c.set_element_token_type_id(token.get_element_token_type_id())
            request_c.set_element_token_id(token.get_element_token_id())
            request_c.set_element_generate_keypair(True)
            request.set_element_content(request_c)
            response = gon_deploy_service.DeployToken(request)
            self.assertTrue(response.get_element_content().get_element_public_key() != "")
            print response.get_element_content().get_element_public_key()

        self._logout(gon_deploy_service, session_id)

    def test_get_collections(self):
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))
        session_id = self._login(gon_deploy_service)

        request = admin_deploy_ws_services.GetGPMCollectionsRequest()
        request_content = admin_deploy_ws_services.ns0.GetGPMCollectionsRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_deploy_service.GetGPMCollections(request)
        self.assertTrue(len(response.get_element_content().get_element_gpm_collections()) > 0)
        for collection in response.get_element_content().get_element_gpm_collections():
            print "Collection id: ", collection.get_element_gpm_collection_id()
            print "Collection summary: ", collection.get_element_gpm_collection_summary()
            print "Collection description: ", collection.get_element_gpm_collection_description()
#            self.assertTrue(len(collection.get_element_gpm_collection_gpms()) > 0)
            for gpm in collection.get_element_gpm_collection_gpms():
                print "GPM id", gpm.get_element_gpm_id()
                print "GPM summary", gpm.get_element_gpm_summary()
                print "GPM description", gpm.get_element_gpm_description()
                print "GPM version", gpm.get_element_gpm_version()
                print "GPM arch", gpm.get_element_gpm_arch()


    def test_install_collections(self):
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))
        session_id = self._login(gon_deploy_service)

        request = admin_deploy_ws_services.GetTokensRequest()
        request_c = admin_deploy_ws_services.ns0.GetTokensRequestType_Def(None).pyclass()
        request_c.set_element_session_id(session_id)
        request.set_element_content(request_c)
        print "GetTokens:"
        response = gon_deploy_service.GetTokens(request)
        self.assertTrue(len(response.get_element_content().get_element_tokens()) > 0)
        for token in response.get_element_content().get_element_tokens():
            if token.get_element_runtime_env_id() != None:
                print "Token:", token.get_element_token_id(), token.get_element_token_label(), token.get_element_token_type_id(), token.get_element_token_status(), token.get_element_token_serial(), token.get_element_runtime_env_id()
                request = admin_deploy_ws_services.InstallGPMCollectionRequest()
                request_c = admin_deploy_ws_services.ns0.InstallGPMCollectionRequestType_Def(None).pyclass()
                request_c.set_element_session_id(session_id)
                request_c.set_element_runtime_env_id(token.get_element_runtime_env_id())
                request_c.set_element_gpm_collection_id('collection_a')
                request.set_element_content(request_c)
                response = gon_deploy_service.InstallGPMCollection(request)
                job_id = response.get_element_content().get_element_job_id()

                job_more = True
                while(job_more):
                    request = admin_deploy_ws_services.GetJobInfoRequest()
                    request_c = admin_deploy_ws_services.ns0.GetJobInfoRequestType_Def(None).pyclass()
                    request_c.set_element_session_id(session_id)
                    request_c.set_element_job_id(job_id)
                    request.set_element_content(request_c)
                    response = gon_deploy_service.GetJobInfo(request)

                    job_info = response.get_element_content().get_element_job_info()
                    job_more = job_info.get_element_job_more()
                    job_status = job_info.get_attribute_job_status()
                    job_header = job_info.get_element_job_header()
                    job_sub_header = job_info.get_element_job_sub_header()
                    job_progress = job_info.get_element_job_progress()
                    print "Job Action", job_id, job_more, job_status, job_header, job_sub_header, job_progress
                    time.sleep(1)
                print "Job Done", job_id, job_more, job_status, job_header, job_sub_header, job_progress
                self.assertTrue( job_status == 'done_ok_job_status')

        self._logout(gon_deploy_service, session_id)

    def test_ping(self):
        gon_deploy_service = admin_deploy_ws_services.BindingSOAP("http://%s:%d" % (self._admin_deploy_ws_host, self._management_deploy_ws_port))


        # ping when logged in
        session_id = self._login(gon_deploy_service)
        request = admin_deploy_ws_services.PingRequest()
        request_content = admin_deploy_ws_services.ns0.PingRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_deploy_service.Ping(request)
        rc = response.get_element_content().get_element_rc()
        self.assertTrue(rc)
        self._logout(gon_deploy_service, session_id)

        # ping when not logged in
        request = admin_deploy_ws_services.PingRequest()
        request_content = admin_deploy_ws_services.ns0.PingRequestType_Def(None).pyclass()
        request_content.set_element_session_id(session_id)
        request.set_element_content(request_content)
        response = gon_deploy_service.Ping(request)
        rc = response.get_element_content().get_element_rc()
        self.assertFalse(rc)

#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 60 * 6

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
