"""
This file contains functionality for admin deploy jobs
"""
from __future__ import with_statement
import threading
import sys
import time
import os
import os.path
import httplib
import ssl


import lib.gpm.gpm_env
import lib.hagi.gon_hagi_authentication
import lib.ws.job
import lib.checkpoint

import components.admin_ws.ws.admin_ws_services
import components.admin_ws.ws.admin_ws_services_types
from components.rest.rest_api import RestApi

import plugin_types.client_gateway.plugin_type_client_runtime_env


MODULE_ID = "admin_deploy_job"

class HTTPSConnectionWrapper(httplib.HTTPSConnection):
    def __init__(self, *args, **kwargs):
        httplib.HTTPSConnection.__init__(self, *args, context=ssl._create_unverified_context(), **kwargs)



class AdminDeployJobInstallGPMCollection(lib.ws.job.Job, plugin_types.client_gateway.plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
    """
    Handling installation of GPM Collection
    """
    def __init__(self, checkpoint_handler, gpm_collection_id, client_runtime_env_id, plugin_socket_client_runtime_env, admin_ws_server_url, admin_ws_login_credentials, download_root, download_session_lock, dictionary, knownsecret, servers):
        lib.ws.job.Job.__init__(self, checkpoint_handler, MODULE_ID, 'admin_deploy_job_install_gpm_collection')
        self._plugin_socket_client_runtime_env = plugin_socket_client_runtime_env
        self._gpm_collection_id = gpm_collection_id
        self._client_runtime_env_id = client_runtime_env_id
        self._admin_ws_server_url = admin_ws_server_url
        self._admin_ws_login_credentials = admin_ws_login_credentials
        self._gpm_ids_install = []
        self._download_root = download_root
        self._download_session_lock = download_session_lock
        self._error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        self.dictionary = dictionary
        self.knownsecret = knownsecret
        self.servers = servers
        self.gon_admin_ws_server_session_id = None
        self._do_cancel = False
        self._error = False
        self._warning = False

    def do_run(self):
        self.set_status('Verify package collection', 'Connecting to Mangement Server')
        self.gon_admin_ws_server = components.admin_ws.ws.admin_ws_services.BindingSOAP(self._admin_ws_server_url, transport=HTTPSConnectionWrapper)
        self.gon_admin_ws_server_session_id = self._admin_ws_login(self.gon_admin_ws_server)

        self.set_status('Verify package collection', '')
        self._admin_ws_download_collection(self.gon_admin_ws_server, self.gon_admin_ws_server_session_id, self._gpm_collection_id)
        if not self._error and not self._do_cancel:
            client_runtime_env = self._plugin_socket_client_runtime_env.get_instance(self._client_runtime_env_id)
            client_runtime_env.set_download_root(self._download_root)
            client_runtime_env.install_gpms_clean(self._gpm_ids_install, self, self._error_handler, self.dictionary, self.knownsecret, self.servers, False)
        self._admin_ws_logout(self.gon_admin_ws_server, self.gon_admin_ws_server_session_id)

        if self._error_handler.error():
            self._error = True
            self.set_status_error('Error during installation', self._error_handler.dump_as_string(2))

        if self._error:
            self.checkpoint_handler.CheckpointMultilineMessage("AdminDeployJobInstallGPMCollection.errors", MODULE_ID, lib.checkpoint.DEBUG, self._error_handler.dump_as_string())
            return

        if self._do_cancel:
            self.checkpoint_handler.Checkpoint("AdminDeployJobInstallGPMCollection.cancled", MODULE_ID, lib.checkpoint.DEBUG)
            self.set_status_canceled()
            return

        if self._warning:
            self.checkpoint_handler.CheckpointMultilineMessage("AdminDeployJobInstallGPMCollection.warnings", MODULE_ID, lib.checkpoint.DEBUG, self._error_handler.dump_as_string())
            self.set_status_done('Warning during installation', self._error_handler.dump_as_string(5))
            return
        self.set_status_done('Installation', 'Done')

    def do_cancel(self):
        self.set_status_cancel()
        self._do_cancel = True

    def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
        self._install_number_of_tasks = tasks_count
        self._install_ticks_total = tasks_ticks_count
        self._install_task = 'Installing'
        self._install_action = ''
        self._install_ticks = 0
        self.set_status(self._install_task, self._install_action, self._install_ticks)

    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        self._install_task = task_title

    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        self._install_action = action_title

    def gpm_installer_cb_action_tick(self, ticks):
        self._install_ticks += ticks
        action_progress = (self._install_ticks * 100) / self._install_ticks_total
        self.set_status(self._install_task, self._install_action, action_progress)

    def gpm_installer_cb_action_done(self):
        self._install_action = ''

    def gpm_installer_cb_task_done(self):
        self._install_task = 'Installing'

    def gpm_installer_cb_task_all_done(self):
        pass

    def gpm_installer_do_cancel(self):
        return self._do_cancel

    def _admin_ws_login(self, gon_admin_ws_server):
        request = RestApi.Admin.OpenSessionRequest()
        response = gon_admin_ws_server.OpenSession(request)
        return response.get_session_id

    def _admin_ws_logout(self, gon_admin_ws_server, gon_admin_ws_server_session_id):
        request = RestApi.Admin.CloseSessionRequest()
        request.set_session_id(gon_admin_ws_server_session_id)
        response = gon_admin_ws_server.CloseSession(request)
        return response

    def _admin_ws_get_collection_info(self, gon_admin_ws_server, gon_admin_ws_server_session_id, gpm_collection_id):
        request = RestApi.Admin.GetGPMCollectionsRequest()
        request.set_session_id(gon_admin_ws_server_session_id)
        response = gon_admin_ws_server.GetGPMCollections(request)

        self._gpm_ids_install = []
        gpm_collection_info = None
        for gpm_collection in response.get_gpm_collections:
            if gpm_collection.get_gpm_collection_id == gpm_collection_id:
                gpm_collection_info = gpm_collection
                for gpm in gpm_collection_info.get_gpm_collection_gpms:
                    self._gpm_ids_install.append(gpm.get_gpm_id)
        return gpm_collection_info

    def _admin_ws_download_collection(self, gon_admin_ws_server, gon_admin_ws_server_session_id, gpm_collection_id):
        gpm_collection_info = self._admin_ws_get_collection_info(gon_admin_ws_server, gon_admin_ws_server_session_id, gpm_collection_id)
        if gpm_collection_info == None:
            self._error = True
            self.set_status_error('Download error', "The collection '%s' is not found on Management Server" % (gpm_collection_id))
            return

        with self._download_session_lock:
            for gpm_info in gpm_collection_info.get_gpm_collection_gpms:
                gpm_download_filename = os.path.join(self._download_root, '%s.gpm' % gpm_info.get_gpm_id)
                gpm_download_file_checksum = lib.gpm.gpm_builder.calculate_checksum(gpm_download_filename)
                if gpm_info.get_gpm_checksum != gpm_download_file_checksum:
                    self._admin_ws_download_gpm(gon_admin_ws_server, gon_admin_ws_server_session_id, gpm_info.get_gpm_id, gpm_download_filename)
                    if self._error:
                        return


    def _admin_ws_download_gpm(self, gon_admin_ws_server, gon_admin_ws_server_session_id, gpm_id, gpm_download_filename):
        gpm_download_filename_folder = os.path.dirname(gpm_download_filename)
        if not os.path.exists(gpm_download_filename_folder):
            os.makedirs(gpm_download_filename_folder)

        self.set_status('Downloading package in package collection', "Waiting to get access to download from Management Server", 0)
        request = RestApi.Admin.DownloadGPMStartRequest()
        request.set_session_id(gon_admin_ws_server_session_id)
        request.set_gpm_id(gpm_id)
        response = gon_admin_ws_server.DownloadGPMStart(request)
        rc = response.get_rc
        if rc != True:
            self._error = True
            self.set_status_error('Downloading package in package collection, Error', "The the package '%s' was not found on Management Server" % (gpm_id))
            return

        download_id = response.get_download_id
        download_size = response.get_download_size
        download_checksum = response.get_download_checksum
        download_file = open(gpm_download_filename, 'wb')
        self.set_status('Downloading package in package collection', "Downloading '%s' from Management Server" % (gpm_id), 0)

        size = 0
        more = rc
        while(more and not self._do_cancel):
            request = RestApi.Admin.DownloadGPMGetChunkRequest()
            request.set_session_id(gon_admin_ws_server_session_id)
            request.set_download_id(download_id)
            request.set_download_chunk_size(1000000)
            response = gon_admin_ws_server.DownloadGPMGetChunk(request)
            rc = response.get_rc
            if rc:
                download_chunk = response.get_download_chunk
                download_file.write(download_chunk)
                more = response.get_more
                size += len(download_chunk)
                action_progress = (size * 100) / download_size
                self.set_status('Downloading package in package collection', "Downloading '%s' from Management Server" % (gpm_id),  action_progress)
            else:
                more = False
        download_file.close()

        if not self._do_cancel:
            gpm_download_file_checksum = lib.gpm.gpm_builder.calculate_checksum(gpm_download_filename)
            if gpm_download_file_checksum != download_checksum:
                self._error = True
                self.set_status_error('Downloading package in package collection, Error', "The the package '%s' was downloaded but the checksum is invalid" % (gpm_id))
                return
            self.set_status('Downloading package in package collection', "Downloading '%s' from Management Server" % (gpm_id),  100)
