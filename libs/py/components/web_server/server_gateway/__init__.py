"""
Simpel web server intented for serving documentation of DME JsAPI for mobile development
"""
import threading
import sys
import os
import urllib
import mimetypes
import json
import urlparse
import datetime
import threading
import time
import cgi

import Cookie
import gzip
import tempfile

import BaseHTTPServer


import lib.checkpoint

import components.plugin.server_gateway.plugin_socket_web_app as plugin_socket_web_app


MODULE_ID = "WebServer"

MENU_ITEM_LAUNCHING = 0
MENU_ITEM_RUNNING = 1
MENU_ITEM_CLOSING = 2
MENU_ITEM_NOT_LAUNCHED = 3
MENU_ITEM_ERROR = 4

AUTH_TIMEOUT_SECONDS = 300


class ClientSessionSimulator(object):
    
    menu = [  
               {  'launchId':1, 
                 'title':'TestItem', 
                 'tooltip':'This is tooltip', 
                 'disabled':False, 
                 'imageId':'remote_desktop',
                 'error' : False,
                  'launch_time' : 3,
                  'run_time' : -1,
                  'close_time' : 3,
                  
                },
               {  'launchId':2, 
                 'title':'Error', 
                 'tooltip':'This is tooltip', 
                 'disabled':False, 
                 'imageId':'remote_desktop',
                 'error' : True,
                  
                },
               {  'launchId':3, 
                 'title':'Run10Seconds', 
                 'tooltip':'This is tooltip', 
                 'disabled':False, 
                 'imageId':'remote_desktop',
                 'error' : False,
                  'launch_time' : 0,
                  'run_time' : 10,
                  'close_time' : 0,
                },
            ]
    
    def __init__(self):
        self.launched_menu_items = dict()
        
    def get_menu(self):
        return self.menu
        
    def launch_menu_item(self, launch_id):
        return 0

    def terminate_menu_item(self, launch_id):
        current_launch = self.launched_menu_items.get(launch_id)
        if current_launch:
            current_launch.stop()

    def get_menu_item_status(self, launch_id):
        current_launch = self.launched_menu_items.get(launch_id)
        if current_launch:
            status = current_launch.get_status()
            if status == MENU_ITEM_NOT_LAUNCHED:
                self.launched_menu_items.pop(launch_id)
            return status
        else:
            return MENU_ITEM_NOT_LAUNCHED



def ext_to_content_type(path):
    ext = os.path.splitext(path)
    if ext[1] in ['.svg']:
        return 'image/svg+xml'
    (mime_type, _) = mimetypes.guess_type(path)
    return mime_type



class WebAppSession(object):
    
    def __init__(self, environment, async_service, checkpoint_handler, user_session, plugins):
        self._plugin_socket_web_app = plugin_socket_web_app.PluginSocket(environment.plugin_manager, plugins)
        self.checkpoint_handler = checkpoint_handler        
        for plugin in self._plugin_socket_web_app.get_plugins():
            plugin.set_user_session_callback(self)
        self._user_session = user_session

    def get_web_app(self, name):
        return self._plugin_socket_web_app.get_plugin(name)
    
    def get_user_id(self):
        return self._user_session.get_user_id()
    
    def validate_user(self, username, password):
        return self._user_session.validate_user(username, password)
#        return "-2"
    
    @classmethod
    def get_basic_auth_string(cls, base_http_server):
        basic_auth_header_list = base_http_server.headers.getheader('Authorization', "").split(" ")
        basic_auth_header_list = [x for x in basic_auth_header_list if x]
        if basic_auth_header_list:
            basic_auth_header = basic_auth_header_list[0].lower()
            if basic_auth_header == 'basic' and len(basic_auth_header_list)==2:
                return basic_auth_header_list[1]
            else:
                return None 
        else:
            return None 
        


class WebServerPageHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    checkpoint_handler = None
    module_id = "unknown"
    www_root = None
    image_abspath = None
    session_manager = None
    session_simulator = ClientSessionSimulator()
    local_storage = dict()
    
    web_sessions = dict()

    def log_error(self, *args):
        if WebServerPageHandler.checkpoint_handler is not None:
            try:
                WebServerPageHandler.checkpoint_handler.Checkpoint("serve_file.error", MODULE_ID, lib.checkpoint.ERROR, message=args[0] % args[1:])
            except:
                WebServerPageHandler.checkpoint_handler.Checkpoint("serve_file.error", MODULE_ID, lib.checkpoint.ERROR, message=str(args))

    def log_message(self, format, *args):
        pass
    
    def do_POST_web_app(self):
        plugin = self._get_web_app_plugin()
        if plugin:
            return plugin.do_POST(self)
        

    def do_POST(self):
        
        if self.path.startswith('/com.excitor.dme.web_app/'):
            self.do_POST_web_app()
            return
        
        url_path = urllib.unquote(self.path[1:])
        url = os.path.join(os.path.abspath(WebServerPageHandler.www_root), url_path)
        url_obj = urlparse.urlparse(self.path)
        args = urlparse.parse_qs(url_obj.query)
        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.getheader('content-length'))
            postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
        else:
            postvars = {}

        reply = {
                "success": True,
                "dates": [
                    { "day" : 2,
                      "month" : 11,
                      "year" : 2012,
                      "vacation_year" : 2012
                    },
                    { "day" : 2,
                      "month" : 3,
                      "year" : 2013,
                      "vacation_year" : 2012
                    },
                    { "day" : 4,
                      "month" : 5,
                      "year" : 2012,
                      "vacation_year" : 2013
                    },
                    ]
                }

        self._send_json_post_response(reply);
        

    def do_AUTHHEAD(self):
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Log in to your domain\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        
        

#        basic_auth_header_list = self.headers.getheader('Authorization', "").split(" ")
#        self.do_AUTHHEAD(self)
#        if basic_auth_header_list:
#            print basic_auth_header_list

    def do_GET(self):
#        basic_auth_string = WebAppSession.get_basic_auth_string(self)
#        if not basic_auth_string:
#            self.do_AUTHHEAD()
#            return
        
        if self.path.startswith('/com.excitor.dme.jsapi/'):
            self.do_GET_jsapi()
            return
        if self.path.startswith('/com.excitor.dme.test/'):
            self.do_GET_test()
            return

        if self.path.startswith('/com.excitor.dme.web_app/'):
            self.do_GET_web_app()
            return
        
        url_path = urllib.unquote(self.path[1:])
        url = os.path.join(os.path.abspath(WebServerPageHandler.www_root), url_path)
        if '?' in url:
            index = url.find('?')
            url = url[0:index]
        if os.path.isfile(url):
            WebServerPageHandler.checkpoint_handler.Checkpoint("serve_file", MODULE_ID, lib.checkpoint.DEBUG, url=url)
            url_file = open(url, 'rb')
            url_file_content = url_file.read()
            url_file.close()


            mime_type = ext_to_content_type(url_path)
            self.send_response(200)
            self.send_header('Content-Type', mime_type)
            self.send_header('Content-Length', str(len(url_file_content)))
            self.end_headers()

            self.wfile.write(url_file_content)
            self.wfile.flush()
            return
        self.send_error(404, 'File Not Found: %s' % url_path)


    def do_GET_test(self):
        url_obj = urlparse.urlparse(self.path)

        path_elements = url_obj.path.split('/')
        if len(path_elements) < 3:
            self.send_error(404, 'JsApi function not Found: %s' % self.path)
            return

        method_name = path_elements[2]
        
        if method_name == 'get_vacation':
            args = urlparse.parse_qs(url_obj.query)
            year = args.get('year', [None])[0]
            if not year:
                year = datetime.datetime.now().year
            year = int(year)
            if year == 2012:
                reply = {
                    "success": True,
                    "dates": [
                        { "day" : 1,
                          "month" : 11,
                          "year" : 2012
                        },
                        { "day" : 2,
                          "month" : 1,
                          "year" : 2013
                        },
                        { "day" : 3,
                          "month" : 5,
                          "year" : 2012
                        },
                        { "day" : 4,
                          "month" : 5,
                          "year" : 2012
                        },
    #                    { "dato": "2012-05-01" },                
    #                    { "dato": "2013-03-02" },                
    #                    { "dato": "2012-05-09" },                
    #                    { "dato": "2013-03-05" },                
    #                    { "dato": "2012-12-05" },                
                    ]
                }
            elif year == 2011:
                reply = {
                    "success": True,
                    "dates": [
                        { "day" : 2,
                          "month" : 1,
                          "year" : 2012
                        },
                        { "day" : 2,
                          "month" : 11,
                          "year" : 2011
                        },
                    ]
                }
            else:
                reply = {
                    "success": True,
                    "dates": [
                    ]
                }
            self._send_json_response(reply)            
            return


    def _get_web_app_plugin(self):
        url_obj = urlparse.urlparse(self.path)
        path_elements = url_obj.path.split('/')
        if len(path_elements) < 3:
            self.send_error(404, 'Web app name not found: %s' % self.path)
            return None
        
        plugin_name = path_elements[2]
        session_id = self.headers.get('DMESecureBrowser-Session', None)
        session = self.session_manager.get_session(session_id)
        if not session:
            basic_auth_string = WebAppSession.get_basic_auth_string(self)
            if basic_auth_string:
                session_dict = self.web_sessions.get(basic_auth_string)
                if session_dict:
                    session = session_dict.get("session")
                    last_access = session_dict.get("last_access")
                    delta = datetime.datetime.now() - last_access
                    if delta.days!=0 or delta.seconds > AUTH_TIMEOUT_SECONDS:
                        self.web_sessions.pop(basic_auth_string)
                        self.do_AUTHHEAD()
                        return
                if not session:
                    session = self.session_manager.create_web_server_session()
                    session_dict = dict()
                    session_dict["session"] = session
                    self.web_sessions[basic_auth_string] = session_dict
                
                session_dict["last_access"] = datetime.datetime.now()
            else:
                self.do_AUTHHEAD()
            
#            from plugin_modules.vacation_app.server_gateway import PluginVacationApp
#            plugin = PluginVacationApp(None, self.checkpoint_handler, None, None, None, None, None)
        if not session:
            self.send_error(404, 'Web app Plugin not found: %s' % self.path)
            plugin = None
        else:
            plugin = session.get_web_app_session().get_web_app(plugin_name)
        return plugin

    def do_GET_web_app(self):
        plugin = self._get_web_app_plugin()
        if plugin:
            return plugin.do_GET(self)
            
        
     
    def do_GET_jsapi(self):
        url_obj = urlparse.urlparse(self.path)
        session_id = self.headers.get('DMESecureBrowser-Session', None)
        
        path_elements = url_obj.path.split('/')
        if len(path_elements) < 3:
            self.send_error(404, 'JsApi function not Found: %s' % self.path)
            return
        
        method_name = path_elements[2]
        if method_name == 'get_menu':
            self.do_GET_jsapi_get_menu(session_id)
            return

        if method_name == 'get_image':
            self.do_GET_jsapi_get_image(urlparse.parse_qs(url_obj.query))
            return
        
        if method_name == 'launch_menu_item':
            self.do_GET_jsapi_launch_menu_item(session_id, urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'terminate_menu_item':
            self.do_GET_jsapi_terminate_menu_item(session_id, urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'get_menu_item_status':
            self.do_GET_jsapi_get_menu_item_status(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'localstorage_get_item':
            self.do_GET_jsapi_localstorage_get_item(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'localstorage_set_item':
            self.do_GET_jsapi_localstorage_set_item(urlparse.parse_qs(url_obj.query))
            return
        
        if method_name == 'localstorage_remove_item':
            self.do_GET_jsapi_localstorage_remove_item(urlparse.parse_qs(url_obj.query))
            return
        
        if method_name == 'localstorage_clear':
            self.do_GET_jsapi_localstorage_clear(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'is_simulator':
            if session_id:
                self._send_json_response(False);
            else:
                self._send_json_response(True);
            return
        
        if method_name == 'test_chunked':
            self.do_GET_jsapi_test_chunked(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'test_cache':
            self.do_GET_jsapi_test_cache(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'test_set_cookies':
            self.do_GET_jsapi_test_set_cookies(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'test_show_cookies':
            self.do_GET_jsapi_test_show_cookies(urlparse.parse_qs(url_obj.query))
            return

        if method_name == 'test_gzip':
            self.do_GET_jsapi_test_gzip(urlparse.parse_qs(url_obj.query))
            return




    def do_GET_jsapi_get_first_session(self):
        session_id = self.session_manager.get_first_session_id()
        self._send_json_response(session_id)

    def _send_json_post_response(self, result):
        result_raw = json.dumps(result)
        self.protocol_version = 'HTTP/1.1'
        self.send_response(201)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', str(len(result_raw)))
        self.send_header('Cache-Control', 'max-age=0')
        self.end_headers()
        self.wfile.write(result_raw)
        self.wfile.flush()

    def _send_json_response(self, result):
        result_raw = json.dumps(result)
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.send_header('Content-Length', str(len(result_raw)))
        self.send_header('Cache-Control', 'max-age=0')
        self.end_headers()
        self.wfile.write(result_raw)
        self.wfile.flush()

    def do_GET_jsapi_get_menu(self, session_id):
        
        if session_id:
            session = self.session_manager.get_session(session_id)
            if session:
                result = []
                keys = [ 'title', 'tooltip', 'disabled' ]
                rename_keys = [ ('icon_id', 'imageId'), ('launch_id', 'launchId')]
                for item in session.get_menu():
                    tags = item.get('tags')
                    if 'SHOW' in tags and not 'GUPDATE' in tags:
                        new_item = dict()
                        for (old_key, new_key) in rename_keys:
                            new_item[new_key] = item.get(old_key)
                        for key in keys:
                            new_item[key] = item.get(key)
                            
                        new_item['visible'] = True 
                        if 'APPBOX_MENU_HIDE' in tags:
                            new_item['visible'] = False 
                            
                        result.append(new_item)
            else:
                self.send_error(404, "Session '%s' not found" % session_id)
                return
        else:
            result = self.session_simulator.get_menu() 
        
        self._send_json_response(result)
        
        
    def do_GET_jsapi_get_image(self, args):
        print "do_GET_jsapi_get_image", args
        arg_x = int(args['x'][0])
        arg_y = int(args['y'][0])
        arg_style = args['style'][0]
        arg_imageId = args['imageId'][0]
        
        image_filename = self._image_id_to_filename(arg_imageId, arg_style, arg_x, arg_y, 'png')
        if image_filename is not None:
            image_file = open(image_filename, 'rb')
                                    
            self.protocol_version = 'HTTP/1.1'
            self.send_response(200)
            self.send_header('Content-Type', 'image/png')
            self.send_header('Content-Length', os.path.getsize(image_filename))
            self.send_header('Cache-Control', 'max-age=120')
            self.end_headers()

            self.wfile.write(image_file.read())
            self.wfile.flush()
            image_file.close()
            return
    
        self.send_error(404, 'Image not found: %s' % arg_imageId)
        

    def do_GET_jsapi_get_menu_item_status(self, args):
        print "do_GET_jsapi_get_menu_item_status", args
        launch_id = args.get('launchId', [-1])[0]
        if self.session_simulator:
            self._send_json_response(self.session_simulator.get_menu_item_status(launch_id))
        else:
            self.send_error(404, "No simulator started")
            

    def do_GET_jsapi_launch_menu_item(self, session_id, args):
        print "do_GET_jsapi_launch_menu_item", args
        launch_id = args.get('launchId', [-1])[0]
        if session_id:
            try:
                session = self.session_manager.get_session(session_id)
                if session:
                    session.launch_menu_item(launch_id);
                    self.send_response(200)
                else:
                    self.send_error(404, "Session '%s' not found" % session_id)
            except Exception,e:
                self.send_error(404, e.msg);
        else:
            try:
                self.session_simulator.launch_menu_item(launch_id) 
                self.send_response(200)
            except Exception,e:
                self.send_error(404, e.msg);
            

    def do_GET_jsapi_terminate_menu_item(self, session_id, args):
        print "do_GET_jsapi_terminate_menu_item", args
        launch_id = args.get('launchId', [-1])[0]
        if session_id:
            try:
                session = self.session_manager.get_session(session_id)
                if session:
                    session.terminate_menu_item(launch_id);
                    self.send_response(200)
                else:
                    self.send_error(404, "Session '%s' not found" % session_id)
            except Exception,e:
                self.send_error(404, e.msg);
        else:
            try:
                self.session_simulator.terminate_menu_item(launch_id) 
                self.send_response(200)
            except Exception,e:
                self.send_error(404, e.msg);


    def do_GET_jsapi_localstorage_get_item(self, args):
        print "do_GET_jsapi_localstorage_get_item", args
        item_key = args.get('key', [-1])[0]
#        self._send_json_response(self.local_storage.get(item_key))
        self._send_json_response(dict(value = self.local_storage.get(item_key)))
            
    def do_GET_jsapi_localstorage_set_item(self, args):
        print "do_GET_jsapi_localstorage_set_item", args
        item_key = args.get('key', [-1])[0]
        item_value = args.get('value', [-1])[0]
        self.local_storage[item_key] = item_value
        self._send_json_response(item_value)
            
    def do_GET_jsapi_localstorage_remove_item(self, args):
        print "do_GET_jsapi_localstorage_remove_item", args
        item_key = args.get('key', [-1])[0]
        try:
            item_value = self.local_storage.pop(item_key)
        except KeyError:
            item_value = None
        self._send_json_response(item_value)
            
    def do_GET_jsapi_localstorage_clear(self, args):
        print "do_GET_jsapi_localstorage_clear", args
        self.local_storage.clear()
        self._send_json_response(0)
            
            
            
    def do_GET_jsapi_test_chunked(self, args):
        def chunk_data(data, chunk_size):
            dl = len(data)
            ret = ""
            for i in range(dl // chunk_size):
                ret += "%s\r\n" % (hex(chunk_size)[2:])
                ret += "%s\r\n" % (data[i * chunk_size : (i + 1) * chunk_size])
        
            ret += "0\r\n\r\n"
            return ret
        
        print "do_GET_jsapi_test_chunked", args
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Transfer-Encoding', 'chunked')
        self.end_headers()

        data = "12345"
        self.wfile.write(chunk_data(data, 1))
        self.wfile.flush()
            

    def do_GET_jsapi_test_cache(self, args):
        print "do_GET_jsapi_test_cache", args
        data = datetime.datetime.now().isoformat("_")
        
        cache_control = args.get('cache_control', [-1])[0]
        
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Cache-Control', cache_control)
        self.send_header('Content-Length', len(data))
        self.end_headers()
        self.wfile.write(data)
        self.wfile.flush()

    def do_GET_jsapi_test_set_cookies(self, args):
        print "do_GET_jsapi_test_set_cookies", args
        
        data = datetime.datetime.now().isoformat("_")
        cookies = Cookie.SimpleCookie()
        cookies['cookie_basis'] = data
        cookies['cookie_with_domain'] = data
        cookies['cookie_with_domain']['Domain'] = '.excitor.com'

        cookies['cookie_with_domain_and_path'] = data
        cookies['cookie_with_domain_and_path']['Domain'] = '127.0.0.1'
        cookies['cookie_with_domain_and_path']['Path'] = '/com.excitor.dme.jsapi/'
        
        cookies['cookie_httponly'] = data
        cookies['cookie_httponly']['httponly'] = True
        
        cookies['cookie_secure'] = data
        cookies['cookie_secure']['secure'] = True

        cookies['cookie_with_version'] = data
        cookies['cookie_with_version']['Domain'] = '127.0.0.1'
        cookies['cookie_with_version']['Path'] = '/'
        cookies['cookie_with_version']['version'] = "1"

        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Content-Length', len(data))
        self.send_header('Set-Cookie', cookies.output(header="", sep="\015\012Set-Cookie:"))
        self.end_headers()
        self.wfile.write(data)
        self.wfile.flush()
    

    def do_GET_jsapi_test_show_cookies(self, args):
        print "do_GET_jsapi_test_show_cookies", args
        
        result = ""
        result += "<html>"
        result += "<body>"
        result += "<h1>Show cookies</h1>"
        result += "<pre>"
        for (name, value) in self.headers.items():
                result += name + " : " + value + "<br/>" 
        result += "</pre>"
        result += "</body>"
        result += "</html>"
        
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.send_header('Content-Length', len(result))
        self.end_headers()
        self.wfile.write(result)
        self.wfile.flush()
   

    def do_GET_jsapi_test_gzip(self, args):
        print "do_GET_jsapi_test_gzip", args
        data = args.get('data', [-1])[0]

        temp_folder = tempfile.mkdtemp()
        temp_filename = os.path.join(temp_folder, 'data.gz')

        f = gzip.open(temp_filename, 'wb')
        f.write(data)
        f.close()
        
        
        f = open(temp_filename, 'r')
        data_gz = f.read()
        f.close()

        os.unlink(temp_filename)
        os.rmdir(temp_folder)
        
        
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200)
        self.send_header('Content-Type', 'text/plain')
        self.send_header('Content-Length', len(data_gz))
        self.send_header('Content-Encoding' ,'gzip')
        self.end_headers()
        self.wfile.write(data_gz)
        self.wfile.flush()


    #
    # This has been copyed from dialog session gatewayserver
    #        
    def _image_id_to_filename(self, image_id, image_style, image_size_x, image_size_y, image_format):
        if image_id is None or image_id == "":
            return None

        image_size_str = '%ix%i' % (image_size_x, image_size_y)
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), image_style.lower(), image_size_str, image_format.lower())
        image_filename_abs = os.path.join(WebServerPageHandler.image_abspath, image_filename)
        print image_filename_abs
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), image_style.lower(), 'default', image_format.lower())
        image_filename_abs = os.path.join(WebServerPageHandler.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        
        image_filename = "%s_%s_%s.%s" %(image_id.lower(), 'default', image_size_str, image_format.lower())
        image_filename_abs = os.path.join(WebServerPageHandler.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs

        image_filename = "%s_%s_%s.%s" %(image_id.lower(), 'default', 'default', image_format.lower())
        image_filename_abs = os.path.join(WebServerPageHandler.image_abspath, image_filename)
        if os.path.exists(image_filename_abs):
            return image_filename_abs
        return None

        
     
     
     
class WebServer(threading.Thread):
    def __init__(self, checkpoint_handler, session_manager, listen_host, listen_port, www_root, image_abspath):
        threading.Thread.__init__(self, name="GOnWebServer")
        self.checkpoint_handler = checkpoint_handler
        self._has_started = False
        self._running = False
        BaseHTTPServer.HTTPServer.request_queue_size = 200
        self._web_server = BaseHTTPServer.HTTPServer((listen_host, listen_port), WebServerPageHandler)
        WebServerPageHandler.checkpoint_handler = checkpoint_handler
        WebServerPageHandler.www_root = www_root
        WebServerPageHandler.image_abspath = image_abspath
        WebServerPageHandler.session_manager = session_manager
        self.listen_host = listen_host
        self.listen_port = listen_port
        self.www_root = www_root
        self.image_abspath = image_abspath

    def run(self):
        self._has_started = True
        with self.checkpoint_handler.CheckpointScope("run", MODULE_ID, lib.checkpoint.INFO, host=self.listen_host, port=self.listen_port, www_root=self.www_root):
            try:
                self._running = True
                self._web_server.serve_forever()
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("run.error", MODULE_ID, lib.checkpoint.CRITICAL, etype, evalue, etrace)
            try:
                self._web_server.socket.close()
            except:
                pass
            self._running = False

    def stop(self):
        with self.checkpoint_handler.CheckpointScope("stop", MODULE_ID, lib.checkpoint.DEBUG):
            self._web_server.shutdown()

    def is_running(self):
        return self._running

    def has_started(self):
        return self._has_started
    

        