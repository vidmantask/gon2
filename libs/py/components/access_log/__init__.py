"""
Logging of gateway server access, hardcoded handling of some log events: connect/logon/logoff/disconnect 
Runtime servers sends management_message to Management Server.
"""

component_id = 'component_access_log'
