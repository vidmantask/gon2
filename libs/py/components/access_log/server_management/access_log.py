"""
Module for accessreporting used by the Management Server.
"""
from __future__ import with_statement
import lib.checkpoint
import datetime

from components.database.server_common import schema_api
from components.database.server_common import database_api

from components.management_message.server_common import from_sink_to_timestamp
from components.management_message.server_common import from_timestamp_to_sink
import components.access_log

from components.access_log.server_common import database_schema
import components.plugin.server_management.plugin_socket_access_notification
from components.plugin.server_management import plugin_socket_audit

import components.management_message.server_management.session_recieve


def is_user_online(user_plugin, external_user_id):
    with database_api.QuerySession() as dbs:
        sessions = dbs.select(database_schema.Session, database_api.and_(database_schema.Session.user_plugin==user_plugin, database_schema.Session.external_user_id==external_user_id, database_schema.Session.close_ts==None))
        if(len(sessions) > 0):
            session = sessions[0]
            return (True, session.server_sid, session.unique_session_id)
    return (False, None, None)

def get_online_user_sessions(server_sid_list):
    with database_api.QuerySession() as dbs:
        if server_sid_list:
            sessions = dbs.select(database_schema.Session, database_api.and_(database_api.in_(database_schema.Session.server_sid, server_sid_list), 
                                                                             database_schema.Session.close_ts==None))
        else:
            sessions = dbs.select(database_schema.Session, database_schema.Session.close_ts==None)
        return sessions

def get_number_of_sessions(server_sid):
    with database_api.QuerySession() as dbs:
        return dbs.select_count(database_schema.Session, database_api.and_(database_schema.Session.server_sid==server_sid, 
                                                                           database_schema.Session.close_ts==None))

class AccessLogAdminReceiver(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
    def __init__(self, environment):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, components.access_log.component_id)
        self.environment = environment
        self.access_notification_socket = components.plugin.server_management.plugin_socket_access_notification.PluginSocket(self.environment.plugin_manager)
        self.plugin_socket_audit = plugin_socket_audit.PluginSocketAudit(environment.plugin_manager)
        self.user_admin = None

    def _get_session(self, t, unique_session_id):
        sessions = t.select(database_schema.Session, database_schema.Session.unique_session_id==unique_session_id)
        if(len(sessions) == 0):
            return None
        else:
            return sessions[0]

    def _get_traffic_proxy(self, t, unique_session_id, proxy_id, proxy_id_sub):
        traffic_proxys = t.select(database_schema.TrafficProxy, database_api.and_(database_schema.TrafficProxy.unique_session_id==unique_session_id, database_schema.TrafficProxy.proxy_id==proxy_id,  database_schema.TrafficProxy.proxy_id_sub==proxy_id_sub))
        if(len(traffic_proxys) == 0):
            return None
        else:
            return traffic_proxys[0]
        
    def _to_timestamp(self, ts):
        if ts != None:
            return datetime.datetime(ts[0],ts[1],ts[2],ts[3],ts[4],ts[5],ts[6])
        else:
            return None
        
    def _create_audit_dict(self, unique_session_id):
        arg_dict = dict()
        arg_dict["source"] = "gateway"
        arg_dict["severity"] = "info"
        arg_dict["session_id"] = unique_session_id
        arg_dict["summary"] = ""
        arg_dict["details"] = ""
        arg_dict["timestamp"] = datetime.datetime.now()
        return arg_dict
        
            
    def access_log_start(self, unique_session_id, server_sid, start_ts, client_ip):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session == None:
                session = database_schema.Session()
                session.unique_session_id = unique_session_id
                session.server_sid = server_sid
                session.start_ts = from_sink_to_timestamp(start_ts)
                session.client_ip = client_ip
                t.add(session)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_start.session_already_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)
                session.start_ts = from_sink_to_timestamp(start_ts)
                session.client_ip = client_ip
            
            arg_dict = self._create_audit_dict(unique_session_id)
            arg_dict["summary"] = "Open session"
            arg_dict["client_ip"] = client_ip
            arg_dict["server_sid"] = server_sid
            arg_dict["timestamp"] = session.start_ts
            arg_dict["type"] = "Session"
            self.plugin_socket_audit.save_entry(**arg_dict)
                
    def get_session_ip(self, unique_session_id, db_session=None):
        with database_api.SessionWrapper(db_session) as dbs:
            session = self._get_session(db_session, unique_session_id)
            if session:
                return session.client_ip
    
    def access_log_auth_ad_login(self, unique_session_id, server_sid, login, plugin_name, external_user_id):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session != None:
                if login and len(login) > 256:
                    login = login[0:245] + "<truncated>"
                session.login = login
                session.external_user_id = external_user_id
                session.user_plugin = plugin_name
                if self.user_admin:
                    session.user_login, session.user_name = self.user_admin.get_login_and_name_from_id(plugin_name, external_user_id)
                self.module_notify_login(session.login, session.external_user_id, session.client_ip, session.start_ts)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_auth_ad_login.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)

            arg_dict = self._create_audit_dict(unique_session_id)
            arg_dict["summary"] = "Login"
            arg_dict["user_login"] = login
            arg_dict["server_sid"] = server_sid
            arg_dict["user_plugin"] = plugin_name
            arg_dict["user_id"] = external_user_id
            arg_dict["type"] = "Session"
            self.plugin_socket_audit.save_entry(**arg_dict)

    def _create_audit_dict_from_notification(self, notification, session):
        arg_dict = self._create_audit_dict(notification.unique_session_id)
        if session:
            arg_dict["login"] = session.login
            arg_dict["user_login"] = session.user_login
            arg_dict["user_name"] = session.user_name
            arg_dict["user_plugin"] = session.user_plugin
            arg_dict["user_id"] = session.external_user_id
            arg_dict["server_sid"] = session.server_sid
        arg_dict["summary"] = notification.summary
        arg_dict["details"] = notification.details
        arg_dict["type"] = notification.type
        arg_dict["severity"] = notification.severity
        arg_dict["component_source"] = notification.source
        arg_dict["error_code"] = notification.code
        self.plugin_socket_audit.save_entry(**arg_dict)
        

    def access_log_auth_ad_login_failed(self, unique_session_id, server_sid, login, error_source, error_code, error_message):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session:
                session.login = login
                notification = t.add(database_schema.Notification())
                notification.unique_session_id = unique_session_id
                notification.type = database_schema.Notification.TYPE_USER_LOGIN
                notification.severity = database_schema.Notification.SEVERITY_WARNING
                notification.source = error_source
                notification.code = error_code
                notification.summary = ""
                notification.details = error_message
                notification.timestamp = datetime.datetime.now()
                
                self._create_audit_dict_from_notification(notification, session)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_auth_ad_login.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)
    
    def access_log_close(self, unique_session_id, server_sid, close_ts):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session:
                session.close_ts = from_sink_to_timestamp(close_ts)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_close.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)
                session = database_schema.Session()
                session.unique_session_id = unique_session_id
                session.server_sid = server_sid
                session.start_ts = from_sink_to_timestamp(close_ts)
                session.close_ts = from_sink_to_timestamp(close_ts)
                t.add(session)

            arg_dict = self._create_audit_dict(unique_session_id)
            arg_dict["summary"] = "Close session"
            arg_dict["server_sid"] = server_sid
            arg_dict["timestamp"] = close_ts
            arg_dict["type"] = "Session"
            if session:
                arg_dict["login"] = session.login
                arg_dict["user_login"] = session.user_login
                arg_dict["user_name"] = session.user_name
                arg_dict["user_plugin"] = session.user_plugin
                arg_dict["user_id"] = session.external_user_id
                
            self.plugin_socket_audit.save_entry(**arg_dict)
            
            
    def access_log_server_sid_changed(self, unique_session_ids, server_sid, old_server_sid):
        with database_api.Transaction() as t:
            for unique_session_id in unique_session_ids:
                session = self._get_session(t, unique_session_id)
                if session:
                    session.server_sid = server_sid

                    # Audit                
                    arg_dict = self._create_audit_dict(unique_session_id)
                    arg_dict["summary"] = "Server sid changed"
                    arg_dict["server_sid"] = server_sid
                    arg_dict["type"] = "Session"
                    arg_dict["login"] = session.login
                    arg_dict["user_login"] = session.user_login
                    arg_dict["user_name"] = session.user_name
                    arg_dict["user_plugin"] = session.user_plugin
                    arg_dict["user_id"] = session.external_user_id
                    self.plugin_socket_audit.save_entry(**arg_dict)

    def access_log_traffic_error(self, unique_session_id, server_sid, error_source, error_code, error_message):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session != None:
                notification = t.add(database_schema.Notification())
                notification.unique_session_id = unique_session_id
                notification.type = database_schema.Notification.TYPE_LAUNCH
                notification.severity = database_schema.Notification.SEVERITY_ERROR
                notification.source = error_source
                notification.code = error_code
                notification.summary = ""
                notification.details = error_message
                notification.timestamp = datetime.datetime.now()

                self._create_audit_dict_from_notification(notification, session)

            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_auth_ad_login.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)


    def access_log_gateway_critical(self, unique_session_id, server_sid, ts, source, code, summary, details):
        notifycation_type = database_schema.Notification.TYPE_GATEWAY_SERVER
        severity = database_schema.Notification.SEVERITY_CRITICAL
        self._access_log_notification(unique_session_id, server_sid, from_sink_to_timestamp(ts), source, code, summary, notifycation_type, details, severity)

    def access_log_gateway_error(self, unique_session_id, server_sid, ts, source, code, summary, details):
        notifycation_type = database_schema.Notification.TYPE_GATEWAY_SERVER
        severity = database_schema.Notification.SEVERITY_ERROR
        self._access_log_notification(unique_session_id, server_sid, from_sink_to_timestamp(ts), source, code, summary, notifycation_type, details, severity)

    def access_log_gateway_warning(self, unique_session_id, server_sid, ts, source, code, summary, details):
        notifycation_type = database_schema.Notification.TYPE_GATEWAY_SERVER
        severity = database_schema.Notification.SEVERITY_WARNING
        self._access_log_notification(unique_session_id, server_sid, from_sink_to_timestamp(ts), source, code, summary, notifycation_type, details, severity)

    def access_log_gateway_info(self, unique_session_id, server_sid, ts, source, code, summary, details):
        notifycation_type = database_schema.Notification.TYPE_GATEWAY_SERVER
        severity = database_schema.Notification.SEVERITY_INFO
        self._access_log_notification(unique_session_id, server_sid, from_sink_to_timestamp(ts), source, code, summary, notifycation_type, details, severity)

    def access_log_management_critical(self, ts, source, code, summary, details):
        unique_session_id = ""
        server_sid = ""
        notifycation_type = database_schema.Notification.TYPE_MANAGEMENT_SERVER
        severity = database_schema.Notification.SEVERITY_CRITICAL
        self._access_log_notification(unique_session_id, server_sid, ts, source, code, summary, notifycation_type, details, severity)

    def access_log_management_error(self, ts, source, code, summary, details):
        unique_session_id = ""
        server_sid = ""
        notifycation_type = database_schema.Notification.TYPE_MANAGEMENT_SERVER
        severity = database_schema.Notification.SEVERITY_ERROR
        self._access_log_notification(unique_session_id, server_sid, ts, source, code, summary, notifycation_type, details, severity)

    def access_log_management_warning(self, ts, source, code, summary, details):
        unique_session_id = ""
        server_sid = ""
        notifycation_type = database_schema.Notification.TYPE_MANAGEMENT_SERVER
        severity = database_schema.Notification.SEVERITY_WARNING
        self._access_log_notification(unique_session_id, server_sid, ts, source, code, summary, notifycation_type, details, severity)

    def _access_log_notification(self, unique_session_id, server_sid, ts, source, code, summary, notifycation_type, details, severity):
        with database_api.Transaction() as t:
            notification = t.add(database_schema.Notification())
            notification.unique_session_id = unique_session_id
            notification.server_sid = server_sid
            notification.type = notifycation_type
            notification.severity = severity
            notification.source = source
            notification.code = code
            notification.summary = summary
            notification.details = details
            notification.timestamp =  ts
            
            try:
                session = self._get_session(t, unique_session_id)
                self._create_audit_dict_from_notification(notification, session)
            except Exception, e:
                self.environment.checkpoint_handler.Checkpoint("create_audit_trail", components.access_log.component_id, lib.checkpoint.WARNING, unique_session_id=unique_session_id, exception=e)
                
    
    def access_log_traffic_start(self, unique_session_id, server_sid, proxy_id_tuppel, start_ts, client_ip, client_port, server_ip, server_port):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session == None:
                session = database_schema.Session()
                session.unique_session_id = unique_session_id
                session.server_sid = server_sid
                session.start_ts = from_sink_to_timestamp(start_ts)
                session.client_ip = client_ip
                t.add(session)
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_start.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)
            proxy_id = proxy_id_tuppel[0]
            proxy_id_sub = proxy_id_tuppel[1]
            traffic_proxy = self._get_traffic_proxy(t, unique_session_id, proxy_id, proxy_id_sub)
            if traffic_proxy == None:
                traffic_proxy = database_schema.TrafficProxy()
                traffic_proxy.unique_session_id = unique_session_id
                traffic_proxy.proxy_id = proxy_id
                traffic_proxy.proxy_id_sub = proxy_id_sub
                traffic_proxy.start_ts = from_sink_to_timestamp(start_ts)
                traffic_proxy.client_ip = client_ip
                traffic_proxy.client_port = client_port
                traffic_proxy.server_ip = server_ip
                traffic_proxy.server_port = server_port
                t.add(traffic_proxy)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_start.traffic_proxy_already_exists", components.access_log.component_id, lib.checkpoint.WARNING, unique_session_id=unique_session_id, proxy_id_tuppel=proxy_id_tuppel)
                traffic_proxy.start_ts = from_sink_to_timestamp(start_ts)
                traffic_proxy.client_ip = client_ip
                traffic_proxy.client_port = client_port
                traffic_proxy.server_ip = server_ip
                traffic_proxy.server_port = server_port

    def access_log_traffic_close(self, unique_session_id, server_sid, proxy_id_tuppel, close_ts):
        with database_api.Transaction() as t:
            session = self._get_session(t, unique_session_id)
            if session == None:
                session = database_schema.Session()
                session.unique_session_id = unique_session_id
                session.server_sid = server_sid
                session.start_ts = from_sink_to_timestamp(close_ts)
                t.add(session)
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_close.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)
            proxy_id = proxy_id_tuppel[0]
            proxy_id_sub = proxy_id_tuppel[1]
            traffic_proxy = self._get_traffic_proxy(t, unique_session_id, proxy_id, proxy_id_sub)
            if traffic_proxy != None:
                traffic_proxy.close_ts = from_sink_to_timestamp(close_ts)
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_close.traffic_proxy_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id, proxy_id_tuppel=proxy_id_tuppel)



    def _get_or_create_session(self, unique_session_id, server_sid, start_ts, dbt):
        session = self._get_session(dbt, unique_session_id)
        if not session:
            session = database_schema.Session()
            session.unique_session_id = unique_session_id
            session.server_sid = server_sid
            if start_ts:
                session.start_ts = from_sink_to_timestamp(start_ts)
            else:
                session.start_ts = datetime.datetime.now()
            session.client_ip = client_ip
            t.add(session)
            self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_start.session_do_not_exists", components.access_log.component_id, lib.checkpoint.ERROR, unique_session_id=unique_session_id)

    def access_log_menu_action_launched(self, unique_session_id, server_sid, proxy_id, start_ts, launch_dict):
        with database_api.Transaction() as t:
            self._get_or_create_session(unique_session_id, server_sid, start_ts, t)
            traffic_launch = database_schema.TrafficLaunch()
            traffic_launch.unique_session_id = unique_session_id
            traffic_launch.proxy_id = proxy_id
            traffic_launch.start_ts = from_sink_to_timestamp(start_ts)
            traffic_launch.launch_id = launch_dict.pop("id", "N/A") 
            traffic_launch.menu_item_name = launch_dict.pop("name", "N/A") 
            traffic_launch.menu_item_title = launch_dict.pop("menu_title", "N/A") 
            traffic_launch.menu_item_properties = "\n".join(["%s: %s" % (key, value) for (key,value) in launch_dict.items()])
            traffic_launch.launch_info = ""
            t.add(traffic_launch)

    def access_log_menu_action_closed(self, unique_session_id, server_sid, proxy_id, close_ts, error_message):
        with database_api.Transaction() as t:
            self._get_or_create_session(unique_session_id, server_sid, None, t)
            traffic_launch = database_schema.get_traffic_launch(t, unique_session_id, proxy_id)
            if traffic_launch:
                traffic_launch.close_ts = from_sink_to_timestamp(close_ts)
                if error_message:
                    traffic_launch.launch_info += "Closed with error: '%s'\n" % error_message
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_menu_action_closed", components.access_log.component_id, lib.checkpoint.ERROR, msg="Traffic Launch entry not found", unique_session_id=unique_session_id, proxy_id=proxy_id, server_sid=server_sid)

    def access_log_menu_action_info(self, unique_session_id, server_sid, proxy_id, message):
        with database_api.Transaction() as t:
            self._get_or_create_session(unique_session_id, server_sid, None, t)
            traffic_launch = database_schema.get_traffic_launch(t, unique_session_id, proxy_id)
            if traffic_launch:
                traffic_launch.launch_info += "%s\n" % message
            else:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_menu_action_info", components.access_log.component_id, lib.checkpoint.ERROR, msg="Traffic Launch entry not found", unique_session_id=unique_session_id, proxy_id=proxy_id, server_sid=server_sid)

    def access_log_menu_action_details(self, unique_session_id, server_sid, proxy_id, proxy_id_sub, log_type, timestamp, message, value_dict):
        if log_type:
            timestamp = from_sink_to_timestamp(timestamp)
            with database_api.Transaction() as t:
                if log_type.startswith("http"):
                    url = value_dict.get("path")
                    host = value_dict.get("host")
                    method = value_dict.get("method")
                    if log_type=="http_request_error":
                        error_code, error_returned, error_message = value_dict.get("http_error", (None,None,None))
                        existing_error = None
                        if error_code:
                            existing_error = database_schema.get_http_proxy_error_log(t, unique_session_id, proxy_id, proxy_id_sub, host, url, error_code, error_message)
                        if not existing_error:
                            existing_error = t.add(database_schema.HttpProxyErrorLog())
                            existing_error.host_or_url = host if host else url
                            existing_error.method = method
                            existing_error.proxy_id = proxy_id
                            existing_error.proxy_id_sub = proxy_id_sub
                            existing_error.unique_session_id = unique_session_id
                            existing_error.first_ts = timestamp
                            existing_error.last_ts = timestamp
                            existing_error.error_code = error_code
                            existing_error.error_returned = error_returned
                            existing_error.error_message = error_message
                            existing_error.fix_string_sizes()
                        else:
                            existing_error.count += 1
                            existing_error.last_ts = timestamp
                    elif log_type in ["http_request_done", "http_request_cancelled"]:
                        proxy = t.add(database_schema.HttpProxyLog())
                        proxy.host = host
                        proxy.url = url
                        proxy.method = method
                        proxy.proxy_id = proxy_id
                        proxy.proxy_id_sub = proxy_id_sub
                        proxy.unique_session_id = unique_session_id
                        proxy.timestamp = timestamp
                        proxy.status_code = value_dict.get("status_code")
                        proxy.content_length = value_dict.get("content_length")
                        proxy.cancelled = log_type == "http_request_cancelled"
                        proxy.fix_string_sizes()
                elif log_type.startswith("tcp"):
                    connection = t.add(database_schema.TcpConnectionLog())
                    connection.proxy_id = proxy_id
                    connection.proxy_id_sub = proxy_id_sub
                    connection.unique_session_id = unique_session_id
                    connection.timestamp = timestamp
                    connection.remote_ip, remote_port = value_dict.get("remote_ip")
                    connection.remote_port = int(remote_port)
                    local_ip = value_dict.get("local_ip")
                    if local_ip:
                        connection.local_ip = local_ip[0]
                        connection.local_port = int(local_ip[1])
                    connection.failed = log_type == "tcp_connect_failed"
                
                else:
                    print value_dict
                        
                         
                     
        
    def access_log_traffic_start_server(self, server_sid, server_title, start_ts):
        start_ts = from_sink_to_timestamp(start_ts)
        self.environment.checkpoint_handler.Checkpoint("gateway_server_start", components.access_log.component_id, lib.checkpoint.INFO, server_sid=server_sid, start_ts=start_ts)
        self._access_log_clean_up_server_sessions(server_sid)

    def access_log_traffic_stop_server(self, server_sid, server_title, close_ts):
        close_ts = from_sink_to_timestamp(close_ts)
        self.environment.checkpoint_handler.Checkpoint("gateway_server_stop", components.access_log.component_id, lib.checkpoint.INFO, server_sid=server_sid, close_ts=close_ts)
        self._access_log_clean_up_server_sessions(server_sid, close_ts)

    def _access_log_clean_up_server_sessions(self, server_sid, close_ts=None):
        with database_api.Transaction() as t:
            sessions = t.select(database_schema.Session, database_api.and_(database_schema.Session.server_sid==server_sid, database_schema.Session.close_ts==None))
            for session in sessions:
                self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_start_server.cleanup_session", components.access_log.component_id, lib.checkpoint.WARNING, server_sid=server_sid, unique_session_id=session.unique_session_id)
                session.close_ts = close_ts
                if session.close_ts is None:
                    session.close_ts = session.start_ts 
                traffic_proxys = t.select(database_schema.TrafficProxy, database_api.and_(database_schema.TrafficProxy.unique_session_id==session.unique_session_id, database_schema.TrafficProxy.close_ts==None))
                for traffic_proxy in traffic_proxys:
                    self.environment.checkpoint_handler.Checkpoint("AccessLogAdminReceiver::access_log_traffic_start_server.cleanup_proxy", components.access_log.component_id, lib.checkpoint.WARNING, server_sid=server_sid, unique_session_id=traffic_proxy.unique_session_id)
                    traffic_proxy.close_ts = close_ts
                    if traffic_proxy.close_ts is None:
                        traffic_proxy.close_ts = traffic_proxy.start_ts
                    
    def module_notify_login(self, login, user_sid, ip, ts):
        self.access_notification_socket.notify_login(login, user_sid, ip, ts)
