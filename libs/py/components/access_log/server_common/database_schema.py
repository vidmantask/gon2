"""
This file contains the database schema for the access log
"""
from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api

import lib.checkpoint

dbapi = schema_api.SchemaFactory.get_creator("access_log_component")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())

def connect_to_database_using_connection(db_connection):
    dbapi.bind(db_connection)


dbapi_session = dbapi.create_table("session",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('server_id', schema_api.Integer),
                                   schema_api.Column('server_sid', schema_api.String(100)),
                                   schema_api.Column('start_ts', schema_api.DateTime, index=True),
                                   schema_api.Column('close_ts', schema_api.DateTime, index=True),
                                   schema_api.Column('client_ip', schema_api.String(100)),
                                   schema_api.Column('login', schema_api.String(256)),
                                   schema_api.Column('external_user_id', schema_api.String(1024)),
                                   schema_api.Column('user_plugin', schema_api.String(256)),
                                   schema_api.Column('user_name', schema_api.String(1024)),
                                   schema_api.Column('user_login', schema_api.String(256)),
                                   )

dbapi_traffic_proxy = dbapi.create_table("traffic_proxy",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('proxy_id', schema_api.Integer()),
                                   schema_api.Column('proxy_id_sub', schema_api.Integer()),
                                   schema_api.Column('start_ts', schema_api.DateTime),
                                   schema_api.Column('close_ts', schema_api.DateTime),
                                   schema_api.Column('client_ip', schema_api.String(100)),
                                   schema_api.Column('client_port', schema_api.Integer()),
                                   schema_api.Column('server_ip', schema_api.String(100)),
                                   schema_api.Column('server_port', schema_api.Integer())
                                   )

dbapi_notification = dbapi.create_table("notification",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('server_sid', schema_api.String(100)),
                                   schema_api.Column('timestamp', schema_api.DateTime, index=True),
                                   schema_api.Column('type', schema_api.String(100)),
                                   schema_api.Column('severity', schema_api.String(100)),
                                   schema_api.Column('source', schema_api.String(100)),
                                   schema_api.Column('code', schema_api.String(100)),
                                   schema_api.Column('summary', schema_api.Text),
                                   schema_api.Column('details', schema_api.Text),
                                   )

dbapi_auth_results = dbapi.create_table("auth_results",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('result_type', schema_api.Integer()),
                                   schema_api.Column('criteria_type', schema_api.String(100)),
                                   schema_api.Column('criteria_text', schema_api.String(256)),
                                   )



dbapi_traffic_launch = dbapi.create_table("traffic_launch",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('proxy_id', schema_api.Integer()),
                                   schema_api.Column('launch_id', schema_api.Integer()),
                                   schema_api.Column('menu_item_name', schema_api.String(100)),
                                   schema_api.Column('menu_item_title', schema_api.String(256)),
                                   schema_api.Column('start_ts', schema_api.DateTime),
                                   schema_api.Column('close_ts', schema_api.DateTime),
                                   schema_api.Column('menu_item_properties', schema_api.Text),
                                   schema_api.Column('launch_info', schema_api.Text, default=""),
                                   )


dbapi_http_proxy_log = dbapi.create_table("http_proxy_log",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('proxy_id', schema_api.Integer(), index=True),
                                   schema_api.Column('proxy_id_sub', schema_api.Integer(), index=True),
                                   schema_api.Column('host', schema_api.String(100)),
                                   schema_api.Column('url', schema_api.String(1024)),
                                   schema_api.Column('method', schema_api.String(256)),
                                   schema_api.Column('timestamp', schema_api.DateTime),
                                   schema_api.Column('status_code', schema_api.String(10)),
                                   schema_api.Column('cancelled', schema_api.Boolean),
                                   schema_api.Column('content_length', schema_api.String(50)),
                                   )

dbapi_http_proxy_error_log = dbapi.create_table("http_proxy_error_log",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('proxy_id', schema_api.Integer(), index=True),
                                   schema_api.Column('proxy_id_sub', schema_api.Integer(), index=True),
                                   schema_api.Column('host_or_url', schema_api.String(1024)),
                                   schema_api.Column('method', schema_api.String(256)),
                                   schema_api.Column('error_code', schema_api.String(10)),
                                   schema_api.Column('error_returned', schema_api.String(100)),
                                   schema_api.Column('error_message', schema_api.String(1024)),
                                   schema_api.Column('first_ts', schema_api.DateTime),
                                   schema_api.Column('last_ts', schema_api.DateTime),
                                   schema_api.Column('count', schema_api.Integer, default=1)
                                   )

dbapi_tcp_connection_log = dbapi.create_table("tcp_connection_log",
                                   schema_api.Column('unique_session_id', schema_api.String(100), index=True),
                                   schema_api.Column('proxy_id', schema_api.Integer(), index=True),
                                   schema_api.Column('proxy_id_sub', schema_api.Integer(), index=True),
                                   schema_api.Column('timestamp', schema_api.DateTime),
                                   schema_api.Column('local_ip', schema_api.String(100)),
                                   schema_api.Column('local_port', schema_api.Integer()),
                                   schema_api.Column('remote_ip', schema_api.String(100)),
                                   schema_api.Column('remote_port', schema_api.Integer()),
                                   schema_api.Column('failed', schema_api.Boolean),
                                   )

schema_api.SchemaFactory.register_creator(dbapi)

class Session(database_api.PersistentObject):   
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_session)


class TrafficProxy(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_traffic_proxy)


class Notification(database_api.PersistentObject):
    TYPE_USER_LOGIN = "User Login"
    TYPE_LAUNCH = "Launch"
    TYPE_AUTH = "Authorization"
    TYPE_GATEWAY_SERVER = "Gateway Server"
    TYPE_MANAGEMENT_SERVER = "Management Server"
       
    SEVERITY_INFO = "info"
    SEVERITY_WARNING = "warning"
    SEVERITY_ERROR = "error"
    SEVERITY_CRITICAL = "critical"
       
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_notification)


class AuthResult(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_auth_results)


class TrafficLaunch(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_traffic_launch)


class HttpProxyErrorLog(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_http_proxy_error_log)
        
    def fix_string_sizes(self):
        if self.host_or_url:
            self.host_or_url = self.host_or_url[:100]


class HttpProxyLog(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_http_proxy_log)
        
    def fix_string_sizes(self):
        if self.host:
            self.host = self.host[:100]
        if self.url:
            self.url = self.url[:1024]
        

class TcpConnectionLog(database_api.PersistentObject):
    
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, dbapi_tcp_connection_log)


Session.map_to_table()
TrafficProxy.map_to_table()
Notification.map_to_table()
AuthResult.map_to_table()
TrafficLaunch.map_to_table()
HttpProxyErrorLog.map_to_table()
HttpProxyLog.map_to_table()
TcpConnectionLog.map_to_table()
 
#        with database_api.Transaction() as t:
#            date_before = datetime.datetime.strptime("2010.09.26", '%Y.%m.%d') 
#            database_schema.delete_old_sessions(t, date_before)

def prune_data(transaction, date_before, checkpoint_handler, progress):
    with checkpoint_handler.CheckpointScope("prune_data", "access_log", lib.checkpoint.DEBUG, date_before=date_before):
        
        old_session_ids = transaction.select_expression(Session.unique_session_id, 
                                                        filter=database_api.and_(Session.start_ts < date_before,
                                                                                 Session.close_ts != None))

        old_notifications = transaction.delete_direct(Notification, database_api.in_(Notification.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_notifications", "access_log", lib.checkpoint.DEBUG, n=old_notifications)
        
        progress.update_prune_status('Delete auth results', 0)
        old_auth_results = transaction.delete_direct(AuthResult, database_api.in_(AuthResult.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_auth_results", "access_log", lib.checkpoint.DEBUG, n=old_auth_results)
        
        progress.update_prune_status('Delete traffic proxy', 30)
        old_traffic_proxy = transaction.delete_direct(TrafficProxy, database_api.in_(TrafficProxy.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_traffic_proxy", "access_log", lib.checkpoint.DEBUG, n=old_traffic_proxy)
        
        progress.update_prune_status('Delete traffic launch', 45)
        old_traffic_launch = transaction.delete_direct(TrafficLaunch, database_api.in_(TrafficLaunch.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_traffic_launch", "access_log", lib.checkpoint.DEBUG, n=old_traffic_launch)

        progress.update_prune_status('Delete http proxy errors', 55)
        old_traffic_http_proxy_error = transaction.delete_direct(HttpProxyErrorLog, database_api.in_(HttpProxyErrorLog.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_traffic_http_proxy_error", "access_log", lib.checkpoint.DEBUG, n=old_traffic_http_proxy_error)

        progress.update_prune_status('Delete http proxy log', 60)
        old_traffic_http_proxy = transaction.delete_direct(HttpProxyLog, database_api.in_(HttpProxyLog.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_traffic_http_proxy", "access_log", lib.checkpoint.DEBUG, n=old_traffic_http_proxy)

        progress.update_prune_status('Delete tcp connection log', 70)
        old_traffic_tcp = transaction.delete_direct(TcpConnectionLog, database_api.in_(TcpConnectionLog.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_traffic_tcp", "access_log", lib.checkpoint.DEBUG, n=old_traffic_tcp)
        
        progress.update_prune_status('Delete sessions', 75)
        old_sessions = transaction.delete_direct(Session, database_api.in_(Session.unique_session_id, old_session_ids))
        checkpoint_handler.Checkpoint("delete_sessions", "access_log", lib.checkpoint.DEBUG, n=old_sessions)


def create_demo_data():
    pass

def get_traffic_launch(dbs, unique_session_id, proxy_id):
    return dbs.select_first(TrafficLaunch, filter=database_api.and_(TrafficLaunch.unique_session_id==unique_session_id,
                                                                    TrafficLaunch.proxy_id==proxy_id))
    
    
def get_http_proxy_error_log(dbs, unique_session_id, proxy_id, proxy_id_sub, host, url, error_code, error_message):
    host_or_url = host if host else url
    if host_or_url:
        return dbs.select_first(HttpProxyErrorLog, filter=database_api.and_(HttpProxyErrorLog.unique_session_id==unique_session_id,
                                                                            HttpProxyErrorLog.proxy_id==proxy_id,
                                                                            HttpProxyErrorLog.proxy_id_sub==proxy_id_sub,
                                                                            HttpProxyErrorLog.host_or_url==host_or_url,
                                                                            HttpProxyErrorLog.error_code==error_code,
                                                                            HttpProxyErrorLog.error_message==error_message,
                                                                            ))
    return None
        
    

    
