"""
Module for accessreporting used by the Gateway Server.
"""
from __future__ import with_statement
import lib.checkpoint


from components.communication import message

import components.access_log

from components.management_message.server_common import from_timestamp_to_sink
import components.management_message.server_gateway.session_send
import datetime



class AccessLogServerSession(object):
    def __init__(self, access_log_server, unique_session_id, client_ip, log_types_enabled):
        self._access_log_server = access_log_server
        self._client_ip = client_ip
        self._unique_session_id = unique_session_id
        self._log_types_enabled = log_types_enabled

    def get_unique_session_id(self):
        return self._unique_session_id
    
    def get_sender(self):
        return self._access_log_server.get_sender()

    def report_access_log_corrected_ip(self, client_ip):
        self._client_ip = client_ip
        
    def report_access_log_start(self):
        start_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_start', unique_session_id=self._unique_session_id, start_ts=start_ts, client_ip=self._client_ip)

    def report_access_log_close(self):
        close_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_close', unique_session_id=self._unique_session_id, close_ts=close_ts)

    def report_access_log_auth_ad_login(self, login, plugin_name, external_user_id):
        self.get_sender().message_remote('access_log_auth_ad_login', unique_session_id=self._unique_session_id, login=login, plugin_name=plugin_name, external_user_id=external_user_id)

    def report_access_log_auth_ad_login_failed(self, login, error_source='N/A', error_code=-1, error_message='N/A'):
        self.get_sender().message_remote('access_log_auth_ad_login_failed', 
                                       unique_session_id=self._unique_session_id, 
                                       login=login,
                                       error_source=error_source,
                                       error_code=error_code,
                                       error_message=error_message,
                                       )
        
    def report_access_log_traffic_start(self, child_id, client_ip, client_port, server_ip, server_port):
        start_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_traffic_start', unique_session_id=self._unique_session_id, proxy_id_tuppel=child_id, start_ts=start_ts, client_ip=client_ip, client_port=client_port, server_ip=server_ip, server_port=server_port)

    def report_access_log_traffic_close(self, child_id):
        close_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_traffic_close', unique_session_id=self._unique_session_id, proxy_id_tuppel=child_id, close_ts=close_ts)

    def report_access_log_menu_action_launched(self, child_id, launch_dict):
        start_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_menu_action_launched', unique_session_id=self._unique_session_id, proxy_id=child_id, start_ts=start_ts, launch_dict=launch_dict)

    def report_access_log_menu_action_closed(self, child_id, error_message=None):
        close_ts = from_timestamp_to_sink(datetime.datetime.now())
        self.get_sender().message_remote('access_log_menu_action_closed', unique_session_id=self._unique_session_id, proxy_id=child_id, close_ts=close_ts, error_message=error_message)

    def report_access_log_menu_action_info(self, child_id, message):
        self.get_sender().message_remote('access_log_menu_action_info', unique_session_id=self._unique_session_id, proxy_id=child_id, message=message)

    def report_access_log_menu_action_details(self, log_type, proxy_id, proxy_id_sub, message, value_dict, force_enabled=False):
        send_log_to_server = force_enabled
        if not send_log_to_server:
            log_type_category = log_type.split("_")[0]
            send_log_to_server = log_type_category in self._log_types_enabled
        if send_log_to_server:
            timestamp = from_timestamp_to_sink(datetime.datetime.now())
            self.get_sender().message_remote('access_log_menu_action_details', unique_session_id=self._unique_session_id, proxy_id=proxy_id, proxy_id_sub=proxy_id_sub, log_type=log_type, timestamp=timestamp, message=message, value_dict=value_dict)


    def report_access_log_traffic_error(self, error_source, error_code, error_message):
        self.get_sender().message_remote('access_log_traffic_error', unique_session_id=self._unique_session_id, error_source=error_source, error_code=error_code, error_message=error_message)

    def report_access_log_gateway_info(self, source, code, summary, details, ts=None):
        if ts is None:
            ts = datetime.datetime.now()
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = self._unique_session_id
        self.get_sender().message_remote('access_log_gateway_info', unique_session_id=unique_session_id, ts=ts_sink, source=source, code=code, summary=summary, details=details)

    def report_access_log_gateway_warning(self, source, code, summary, details, ts=None):
        if ts is None:
            ts = datetime.datetime.now()
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = self._unique_session_id
        self.get_sender().message_remote('access_log_gateway_warning', unique_session_id=unique_session_id, ts=ts_sink, source=source, code=code, summary=summary, details=details)

    def report_access_log_gateway_error(self, source, code, summary, details, ts=None):
        if ts is None:
            ts = datetime.datetime.now()
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = self._unique_session_id
        self.get_sender().message_remote('access_log_gateway_error', unique_session_id=unique_session_id, ts=ts_sink, source=source, code=code, summary=summary, details=details)


class AccessLogServer(object):
    def __init__(self, service_management_message_send, server_config):
        self._management_message_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender(components.access_log.component_id, service_management_message_send)
        self._server_title = server_config.get_service_title()
        self._log_types_enabled = server_config.log_settings_traffic

    def create_access_log_session(self, unique_session_id, client_ip):
        return AccessLogServerSession(self, unique_session_id, client_ip, self._log_types_enabled)
        
    def get_sender(self):
        return self._management_message_sender

    def report_access_log_traffic_start_server(self):
        start_ts = from_timestamp_to_sink(datetime.datetime.now())
        self._management_message_sender.message_remote('access_log_traffic_start_server', server_title=self._server_title, start_ts=start_ts)

    def report_access_log_traffic_stop_server(self):
        close_ts = from_timestamp_to_sink(datetime.datetime.now())
        self._management_message_sender.message_remote('access_log_traffic_stop_server', server_title=self._server_title, close_ts=close_ts)

    def report_access_log_gateway_critical(self, ts, source, code, summary, details):
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = ""
        self.get_sender().message_remote('access_log_gateway_critical', unique_session_id=unique_session_id, ts=ts_sink, source=source, code=code, summary=summary, details=details)

    def report_access_log_gateway_error(self, ts, source, code, summary, details):
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = ""
        self.get_sender().message_remote('access_log_gateway_error', unique_session_id=unique_session_id, ts=ts_sink,  source=source, code=code, summary=summary, details=details)

    def report_access_log_gateway_warning(self, ts, source, code, summary, details):
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = ""
        self.get_sender().message_remote('access_log_gateway_warning', unique_session_id=unique_session_id, ts=ts_sink,  source=source, code=code, summary=summary, details=details)

    def report_access_log_gateway_info(self, ts, source, code, summary, details):
        ts_sink = from_timestamp_to_sink(ts)
        unique_session_id = ""
        self.get_sender().message_remote('access_log_gateway_info', unique_session_id=unique_session_id, ts=ts_sink, source=source, code=code, summary=summary, details=details)

    def report_server_sid_changed(self, unique_session_ids, old_server_sid, new_server_sid):
        self.get_sender().message_remote('access_log_server_sid_changed', unique_session_ids=unique_session_ids, old_server_sid=old_server_sid)

