import hashlib

def calc_servers_checksum(servers):
    if servers is None:
        return None
    checksum_calculator = hashlib.sha1()
    servers_normalized = "".join(servers.splitlines())
    checksum_calculator.update(servers_normalized)
    return checksum_calculator.hexdigest()
