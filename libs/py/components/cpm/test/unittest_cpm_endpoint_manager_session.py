"""
Unittest of of CPM endpoint-manager-session
"""
from __future__ import with_statement
import os.path

import unittest
from lib import giri_unittest
from lib import checkpoint

from components.communication.sim import sim_tunnel_endpoint

import components.cpm.client_gateway.cpm_endpoint_manager_session as cpm_client_module
import components.cpm.server_gateway.cpm_endpoint_manager_session as cpm_server_module

import plugin_modules.soft_token.client_gateway as soft_token_plugin

import components.dictionary.common




client_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F"
server_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400872A7507237DEA288BCDEFC0BEF38A1A239CCF96E9FDC22F2F62336A3F013DD6A903639422F8F74E11CC4B725B72BF3FDB88E666690BFB7676B0F03081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D241304C4665A621272CE151248A2B3D2BBA7C8986219366F10790F01D2"


def add_gpm_xml(root, gpm_filename, gpm_xml_string):
    gpm_file = open(os.path.join(root, gpm_filename), 'wb')
    gpm_file.write(gpm_xml_string)
    gpm_file.close()



class ServerConfigStub(object):
    def __init__(self, servers):
        self.server = servers
        self.session_offline_credential_timeout_min = 30
        self.session_close_when_entering_background = False

    def get_client_servers(self, checkpoint_handler):
        return self.server

class CPMSessionManagerSessionTest(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()

        self.gpms_installed_root = giri_unittest.mkdtemp()
        self.gpms_download_root = giri_unittest.mkdtemp()
        self.temp_root = giri_unittest.mkdtemp()
        self.install_ro_root = giri_unittest.mkdtemp()
        self.install_rw_root = giri_unittest.mkdtemp()

        self.gpms_folder = self.dev_installation.get_gpms_folder()

    def tearDown(self):
        pass

    def test_update_servers(self):
        """
        This tests the situation where the servers-file should be updated
        """
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        cpm_client_runtime_env = soft_token_plugin.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), self.temp_root)
        cpm_client_runtime_env.force_to_current()

        servers_old = "test"
        servers_new = "test-updated"

        cpm_client_runtime_env._gpm_meta_root = self.gpms_installed_root
        error_handler = None
        cpm_client_runtime_env.update_connection_info(client_knownsecret, servers_old, error_handler)
        cpm_client_endpoint_manager_session = cpm_client_module.CPMEndpointManager(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), giri_unittest.get_dictionary(), tunnel_endpoint_client, cpm_client_runtime_env)


        class Callback(cpm_server_module.CPMEndpointManagerCallback):
            def __init__(self):
                self.cpm_server_endpoint_manager_session = None
                self.do_update_servers = False
                self.servers = None

            def cpm_endpoint_manager_connect_update(self, do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
                self.do_update_servers = do_update_servers
                self.servers = servers
                print do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove
                self.cpm_server_endpoint_manager_session.connect_update_done()

        callback = Callback()
        server_config = ServerConfigStub(servers_new)
        gpms_root = self.dev_installation.get_gpms_folder()
        cpm_server_endpoint_manager_session = cpm_server_module.CPMEndpointManager(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), tunnel_endpoint_server, server_config, callback, gpms_root)
        callback.cpm_server_endpoint_manager_session = cpm_server_endpoint_manager_session

        cpm_client_endpoint_manager_session.session_start_and_connected()
        cpm_server_endpoint_manager_session.session_start_and_connected()

        def wait_for_check():
            return cpm_server_endpoint_manager_session.connect_check_ok() or cpm_server_endpoint_manager_session.connect_check_failed()
        giri_unittest.wait_until_with_timeout(wait_for_check, 50000)
        self.assertTrue(wait_for_check())

        self.assertTrue(callback.do_update_servers)
        self.assertEqual(callback.servers, servers_new)

        giri_unittest.get_async_service().stop_and_wait()



#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'twa'
GIRI_UNITTEST_TIMEOUT = 120

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
