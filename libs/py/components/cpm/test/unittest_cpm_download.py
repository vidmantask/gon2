"""
Unittest of of CPM(CLient Package Management) download functionality
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile
import datetime

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility

import components.cpm.server_gateway.cpm_file_downloader as server_file_downloader
import components.cpm.server_gateway.cpm_download_manager as server_download_manager
import components.cpm.client_gateway.cpm_file_downloader as client_file_downloader

from components.communication import tunnel_endpoint_base


from components.communication.sim import sim_tunnel_endpoint

import components.cpm.server_gateway.cpm_download_manager as dl_manager




#class CPMDownloadManager(unittest.TestCase):
#
#    class MyDownloadManagerCB(object):
#        def __init__(self, manager, xid, confirm=0):
#            self._manager = manager
#            self._id = xid
#            self._confirm = confirm
#            self.timeout = False
#            self.released = False
#
#        def download_manager_ticket_granted(self, download_ticket):
#            print "MyDownloadManagerCB::download_manager_ticket_granted(%d)" % self._id, download_ticket
#            self._download_ticket = download_ticket
#            async_service.sleep_start(0, self._id, 0, self, 'release_ticket')
#
#            if self._confirm > 0:
#                async_service.sleep_start(0, self._confirm, 0, self, 'confirm_ticket')
#
#        def download_manager_ticket_timeout(self, download_ticket):
#            print "MyDownloadManagerCB::download_manager_ticket_timeout(%d)" % self._id, download_ticket
#            self.timeout = True
#
#        def release_ticket(self):
#            self._manager.release_ticket(self._download_ticket)
#            self.released = True
#
#        def confirm_ticket(self):
#            self._manager.confirm_ticket(self._download_ticket)
#
#    def setUp(self):
#        pass
#
#    def tearDown(self):
#        pass
#
##
#    def test_manager(self):
#        manager = dl_manager.DownloadManager(async_service, 2)
#        manager._ticket_timeout = datetime.timedelta(seconds=5)
#        my_dlm_cb_010 = CPMDownloadManager.MyDownloadManagerCB(manager, 8, 4)
#        my_dlm_cb_012 = CPMDownloadManager.MyDownloadManagerCB(manager, 12)
#        my_dlm_cb_001 = CPMDownloadManager.MyDownloadManagerCB(manager, 1)
#        my_dlm_cb_002 = CPMDownloadManager.MyDownloadManagerCB(manager, 2)
#        my_dlm_cb_003 = CPMDownloadManager.MyDownloadManagerCB(manager, 3)
#        my_dlm_cb_004 = CPMDownloadManager.MyDownloadManagerCB(manager, 4)
#
#        manager.request_ticket(my_dlm_cb_010)
#        manager.request_ticket(my_dlm_cb_012)
#        manager.request_ticket(my_dlm_cb_001)
#        manager.request_ticket(my_dlm_cb_002)
#        manager.request_ticket(my_dlm_cb_003)
#        manager.request_ticket(my_dlm_cb_004)
#
#        async_service.run()
#        self.assertTrue(my_dlm_cb_001.released)
#        self.assertTrue(my_dlm_cb_002.released)
#        self.assertTrue(my_dlm_cb_003.released)
#        self.assertTrue(my_dlm_cb_004.released)
#        self.assertTrue(my_dlm_cb_010.released)
#        self.assertTrue(my_dlm_cb_012.released)
#
#        self.assertFalse(my_dlm_cb_001.timeout)
#        self.assertFalse(my_dlm_cb_002.timeout)
#        self.assertFalse(my_dlm_cb_003.timeout)
#        self.assertFalse(my_dlm_cb_004.timeout)
#        self.assertFalse(my_dlm_cb_010.timeout)
#        self.assertTrue(my_dlm_cb_012.timeout)

class CPMFileDownloader(unittest.TestCase):

    class MyClientControler(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
        def __init__(self, async_service, tunnelendpoint, download_root):
            tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, giri_unittest.get_checkpoint_handler(), tunnelendpoint)
            self.tunnelendpoint = tunnelendpoint
            self.async_service = async_service
            self.downloader = None
            self.download_root = download_root
            self.done = False

        def remote_download_granted(self, tunnel_id):
            self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(giri_unittest.get_checkpoint_handler(), tunnel_id)
            self.downloader = client_file_downloader.CPMFileDownloader(self.async_service, giri_unittest.get_checkpoint_handler(), self._download_tunnelendpoint, tunnel_id, self.download_root, self)
            self.downloader.start()

        def file_download_all_start(self, download_id, number_of_files, tick_value_max_total):
            print "MyClientControler::file_download_all_start", download_id, number_of_files, tick_value_max_total

        def file_download_start(self, download_id, filename, tick_value_max):
            print "MyClientControler::file_download_start", download_id, filename, tick_value_max
            self.tick_value_max = tick_value_max

        def file_download_tick(self, download_id, tick_value):
            print "MyClientControler::file_download_tick", download_id, tick_value

        def file_download_done(self, download_id):
            print "MyClientControler::file_download_done", download_id

        def file_download_verifying(self, download_id):
            print "MyServerControler::file_download_verifying", download_id

        def file_download_all_done(self, download_id):
            print "MyClientControler::file_download_all_done", download_id
            self.done = True

        def file_download_error(self, download_id, message, tell_remote=True):
            print "MyClientControler::file_download_error", download_id, message

        def file_download_canceling(self, download_id):
            print "MyClientControler::file_download_canceling", download_id

        def file_download_canceled(self, download_id):
            print "MyClientControler::file_download_canceled", download_id

        def is_done(self):
            return self.done


    class MyServerControler(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
        def __init__(self, async_service, manager, tunnelendpoint):
            tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, giri_unittest.get_checkpoint_handler(), tunnelendpoint)
            self.manager = manager
            self.tunnelendpoint = tunnelendpoint
            self.downloader = None
            self.async_service = async_service

        def start(self, download_root, download_filenames_relative):
            self.manager.request_ticket(self)
            self.download_root = download_root
            self.download_filenames_relative = download_filenames_relative

        def download_manager_ticket_granted_from_other_thread(self, download_ticket):
            download_id = 1
            self.download_ticket = download_ticket
            self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(giri_unittest.get_checkpoint_handler(), download_id)
            self.downloader = server_file_downloader.CPMFileDownloader(self.async_service, giri_unittest.get_checkpoint_handler(), self._download_tunnelendpoint, download_ticket, download_id, self, self.manager, self.download_root, self.download_filenames_relative)
            self.tunnelendpoint_remote('remote_download_granted', tunnel_id = download_id)


        def download_manager_ticket_timeout_from_other_thread(self, download_ticket):
            print "MyServerControler::download_manager_ticket_timeout", download_ticket

        def file_download_done(self, download_id):
            print "MyServerControler::file_download_done", download_id

        def file_download_all_done(self, download_id):
            print "MyServerControler::file_download_all_done", download_id
            self.tunnelendpoint.remove_tunnelendpoint(download_id)
            self.downloader = None
            self.manager.release_ticket(self.download_ticket)

        def file_download_verifying(self, download_id):
            print "MyServerControler::file_download_verifying", download_id

        def file_download_error(self, download_id, message, tell_remote=True):
            print "MyServerControler::file_download_error", download_id, message

        def file_download_ticket_expired(self, download_id):
            print "MyServerControler::file_download_ticket_expired", download_id



    def test_downloader(self):
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()

        download_root = giri_unittest.mkdtemp()

        manager = dl_manager.DownloadManager(giri_unittest.get_async_service(), tunnel_endpoint_client.get_mutex(), giri_unittest.get_checkpoint_handler(), 2)
        cpm_client_controler = CPMFileDownloader.MyClientControler(giri_unittest.get_async_service(), tunnel_endpoint_client, download_root)
        cpm_server_controler = CPMFileDownloader.MyServerControler(giri_unittest.get_async_service(), manager, tunnel_endpoint_server)


        download_root_server = giri_unittest.HG_ROOT
        filenames_relative = []
        filenames_relative.append('CMakeLists.txt')
        filenames_relative.append('CMakeLists.txt')

        cpm_server_controler.start(download_root_server, filenames_relative)

        giri_unittest.wait_until_with_timeout(cpm_client_controler.is_done, 60000)
        self.assertTrue(cpm_client_controler.is_done())

        giri_unittest.get_async_service().stop_and_wait()


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'
GIRI_UNITTEST_TIMEOUT = 120

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
