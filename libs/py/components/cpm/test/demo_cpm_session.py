"""
Demo program to show CPM(CLient Package Management) session functionality
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest

import lib.giri_unittest
import lib.checkpoint

import components.communication.async_service

from components.communication.sim import sim_tunnel_endpoint
from components.database.server_common.connection_factory import ConnectionFactory

import lib.gpm.gpm_builder as gpm_builder
import lib.gpm.gpm_analyzer as gpm_analyzer

import components.cpm.client_gateway.cpm_session as cpm_client_module
import components.cpm.server_gateway.cpm_session as cpm_server_module

import components.cpm.server_gateway.cpm_download_manager
import plugin_modules.soft_token.client_gateway as soft_token_plugin

import components.presentation.user_interface
import components.dictionary.common

checkpoint_handler_null = lib.checkpoint.CheckpointHandler.get_NULL()
checkpoint_handler = lib.checkpoint.CheckpointHandler.get_cout_all()


def add_gpm_xml(root, gpm_xml_string):
    gpm_file_num, gpm_filename = tempfile.mkstemp('.gpm.xml', dir=root)
    gpm_file = os.fdopen(gpm_file_num, 'wb')
    gpm_file.write(gpm_xml_string)
    gpm_file.close()


class MYCPMSessionCB(object):
    def __init__(self, user_interface):
        self.user_interface = user_interface

    def cpm_cb_restart(self, install_state):
        print "RESTART xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        print install_state
        print "RESTART xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        self._install_state = install_state
        self.user_interface.destroy()

    def cpm_cb_closed(self):
        print "CLOSE xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
        self.user_interface.destroy()

def main():
    gon_installation = lib.giri_unittest.UnittestDevEnvInstallation.create()
    gon_installation.generate_gpms()

#    gon_client_installation_root = lib.giri_unittest.mkdtemp()
#    gon_client_installation_root = os.path.join('c:/', 'tmp', 'test_installation')

    gpms_root = gon_installation.get_gpms_folder()
#    gpms_root = lib.giri_unittest.GPMS_FOLDER

    print gpms_root

    sim_tunnel_endpoint_connector = sim_tunnel_endpoint.SimThread(checkpoint_handler_null, lib.giri_unittest.get_async_service())
    sim_tunnel_endpoint_connector.start_and_wait_until_ready()
    async_service = sim_tunnel_endpoint_connector.get_async_service()

    tunnel_endpoint_client = sim_tunnel_endpoint_connector.get_tunnelendpoint_client()
    tunnel_endpoint_server = sim_tunnel_endpoint_connector.get_tunnelendpoint_server()

    dictionary = components.dictionary.common.load_dictionary(checkpoint_handler, '.')
    configuration = lib.giri_unittest.ConfigurationStubClient()

    gon_client_installation_root = gon_installation.get_instance_run_root()

    cpm_client_user_interface = components.presentation.user_interface.UserInterface(configuration=configuration, dictionary=dictionary)

    cpm_client_runtime_env = soft_token_plugin.PluginClientRuntimeEnvInstance(checkpoint_handler, gon_client_installation_root)
    cpm_client_runtime_env.force_to_current()

    cpm_client_session_cb = MYCPMSessionCB(cpm_client_user_interface)
    cpm_client_session = cpm_client_module.CPMSessionOnline(async_service, checkpoint_handler, tunnel_endpoint_client, cpm_client_user_interface, dictionary, cpm_client_runtime_env, cpm_client_session_cb)
    cpm_server_user_session = None
    server_knownsecret = None
    server_config = None
    access_log_server_session = None
    download_manager = components.cpm.server_gateway.cpm_download_manager.DownloadManager(async_service, cpm_client_session.get_mutex(), checkpoint_handler, 2)
    cpm_server_session = cpm_server_module.CPMSession(async_service, checkpoint_handler, tunnel_endpoint_server, cpm_server_user_session, gpms_root, download_manager, server_knownsecret, server_config, access_log_server_session)




#    cpm_server_session.analyze_start_remove()
#    cpm_server_session.analyze_start_update()
    cpm_server_session.analyze_start_install()
    cpm_client_user_interface.start()
###
###
#    cpm_client_session.analyze_start_install()
#    cpm_client_session.analyze_start_remove()
#    cpm_client_user_interface.start()

#    cpm_logfilename = os.path.join(gpms_root, 'cpm_logfile')
#    tempfile = open(cpm_logfilename, 'w')
#    tempfile.close()
#    cpm_session_data = {'cpm_logfilename': cpm_logfilename, 'gpm_ids_update': set([('a-0-0-win', 'a-1-1-win'), ('a_extra-0-0-win', 'a_extra-1-1-win')]), 'gpm_ids_remove': set(['a_extra-0-0-win', 'a-0-0-win']), 'gpm_ids_info': {'a_extra-1-1-win': {'size': 74184604, 'version': '1', 'arch': 'win', 'description': 'This is the package contains some extra files for package A, and requires that A is installed.', 'name': 'a_extra'}, 'a-1-1-win': {'size': 11292712, 'version': '1', 'arch': 'win', 'description': 'This is the long description for the famous a file package.', 'name': 'a'}, 'a_extra-0-0-win': {'size': None, 'version': '0', 'arch': 'win', 'description': 'No description', 'name': 'a_extra'}, 'a-0-0-win': {'size': None, 'version': '0', 'arch': 'win', 'description': 'No description', 'name': 'a'}}, 'xx': 1, 'mode': 0, 'gpm_ids_install': set([])}
    cpm_session_data = {'gpm_ids_update': set([('gon_client-5.4.0.0-1-win', 'gon_client-5.4.0.0-2-win')]),
                        'gpm_ids_remove': set(['gon_client-5.4.0.0-1-win', 'gon_client-5.4.0.0-2-win']),
                        'gpm_ids_info': {'gon_client-5.4.0.0-1-win': {'description': 'This package contains the G/On Client for the Windows platform.', 'checksum': None, 'summary': 'G/On Client', 'package_size': None, 'version_release': '1', 'version': '5.4.0.0', 'size': None, 'arch': 'win', 'name': 'gon_client'}, 'gon_client-5.4.0.0-2-win': {'description': 'This package contains the G/On Client for the Windows platform.', 'checksum': None, 'summary': 'G/On Client', 'package_size': None, 'version_release': '2', 'version': '5.4.0.0', 'size': None, 'arch': 'win', 'name': 'gon_client'}}, 'cpm_logfilename': 'c:tmp\\test_installation\\gon_client\\gon_temp\\gon_client_cpm.log', 'cpm_meta_signature': None, 'mode': 0, 'gpm_ids_install': set([])}

#
#    restart_state = lib.appl.gon_client.GOnClientInstallState(done=True)
#    restart_state.set_cpm_session_data(cpm_session_data)
#    cpm_client_user_interface = components.presentation.user_interface.UserInterface()
#    cpm_client_session_Offline = cpm_client_module.CPMSessionOffline(checkpoint_handler, cpm_client_runtime_env, cpm_client_user_interface, dictionary, restart_state, cpm_client_session_cb)
#    cpm_client_session_Offline.install_start_offline()
#    cpm_client_user_interface.start()



    sim_tunnel_endpoint_connector.stop()
    sim_tunnel_endpoint_connector.join()


#    shutil.rmtree(runtime_root)


if __name__ == '__main__':
    main()
