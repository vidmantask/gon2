"""
Unittest of of CPM(CLient Package Management) session
"""
from __future__ import with_statement
import os
import os.path

import sys
import shutil
import tempfile

import unittest
from lib import giri_unittest
from lib import checkpoint
from lib import cryptfacility

from components.database.server_common.connection_factory import ConnectionFactory

import lib.gpm.gpm_builder as gpm_builder
import lib.gpm.gpm_analyzer as gpm_analyzer

import components.cpm.client_gateway.cpm_session as cpm_client_module
import components.cpm.server_gateway.cpm_session as cpm_server_module

import components.cpm.server_gateway.cpm_download_manager
import components.presentation.user_interface

import plugin_modules.soft_token.client_gateway as soft_token_plugin

import components.dictionary.common



client_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400D7997B3756260EEE8D25B9F98256115CDB487F2CA598FD94D7A361E04C01E90B42A6124E64CD4994C01C5108899F64839E46E34A2C5BF4BF753A6E3081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D2AD5BB92B2200E1AF062FA9F29CB43E9969AC7D1F190F7791E52F8D34F"
server_knownsecret = "0119308201153081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF020104033E000400872A7507237DEA288BCDEFC0BEF38A1A239CCF96E9FDC22F2F62336A3F013DD6A903639422F8F74E11CC4B725B72BF3FDB88E666690BFB7676B0F03081FE0201003081D206072A8648CE3D02013081C6020101301D06072A8648CE3D01023012020200E906092A8648CE3D0102030202014A3040041E000000000000000000000000000000000000000000000000000000000000041E000000000000000000000000000000000000000000000000000000000001043D04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3021E008000000000000000000000000000069D5BB915BCD46EFB1AD5F173ABDF02010404243022020101041D241304C4665A621272CE151248A2B3D2BBA7C8986219366F10790F01D2"


def add_gpm_xml(root, gpm_filename, gpm_xml_string):
    gpm_file = open(os.path.join(root, gpm_filename), 'wb')
    gpm_file.write(gpm_xml_string)
    gpm_file.close()




class CPMSession(unittest.TestCase):

    def setUp(self):
        self.dev_installation = giri_unittest.UnittestDevEnvInstallation.create()
        self.dev_installation.generate_gpms()

        self.gpms_installed_root = giri_unittest.mkdtemp()
        self.gpms_download_root = giri_unittest.mkdtemp()
        self.temp_root = giri_unittest.mkdtemp()
        self.install_ro_root = giri_unittest.mkdtemp()
        self.install_rw_root = giri_unittest.mkdtemp()

        self.gpms_folder = self.dev_installation.get_gpms_folder()


        gpm_a_0_0_win_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpm>
  <header name="a">
    <version main="0" />
    <version_release main="0"/>
    <arch>win</arch>
    <description lang="en" summary=""></description>
  </header>
</gpm>"""
        add_gpm_xml(self.gpms_installed_root, 'a-0-0-win.gpm.xml', gpm_a_0_0_win_str)

        gpm_x_1_1_win_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpm>
  <header name="x">
    <version main="1" />
    <version_release main="1"/>
    <arch>win</arch>
    <description lang="en" summary=""></description>
  </header>
</gpm>"""
        add_gpm_xml(self.gpms_installed_root, 'x-1-1-win.gpm.xml', gpm_x_1_1_win_str)

        gpm_x_error_win_str = """<?xml version="1.0" encoding="UTF-8"?>
<gpm>
  <header name="x_error">
    <version main="1" />
    <version_release main="1"/>
    <arch>win</arch>
    <description lang="en" summary=""></description>
  <header>
</gpm>"""
        add_gpm_xml(self.gpms_installed_root, 'x_error-1-1-win.gpm.xml', gpm_x_error_win_str)

    def tearDown(self):
        pass

    def test_session_start(self):
        tunnel_endpoint_client = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_client()
        tunnel_endpoint_server = giri_unittest.get_sim_tunnel_endpoint_connector().get_tunnelendpoint_server()


        cpm_client_user_interface = None
        dictionary = components.dictionary.common.load_dictionary(giri_unittest.get_checkpoint_handler(), '.')

        cpm_client_runtime_env = soft_token_plugin.PluginClientRuntimeEnvInstance(giri_unittest.get_checkpoint_handler(), self.temp_root)
        cpm_client_runtime_env.force_to_current()

        cpm_client_runtime_env._gpm_meta_root = self.gpms_installed_root
        cpm_client_session = cpm_client_module.CPMSessionOnline(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), tunnel_endpoint_client, cpm_client_user_interface, dictionary, cpm_client_runtime_env)

        cpm_server_user_session = None
        server_config = None
        access_log_server_session = None
        self.download_manager = components.cpm.server_gateway.cpm_download_manager.DownloadManager(giri_unittest.get_async_service(), tunnel_endpoint_client.get_mutex(), giri_unittest.get_checkpoint_handler(), 2)
        cpm_server_session = cpm_server_module.CPMSession(giri_unittest.get_async_service(), giri_unittest.get_checkpoint_handler(), tunnel_endpoint_server, cpm_server_user_session, self.gpms_folder, self.download_manager, server_knownsecret, server_config, access_log_server_session)

        cpm_client_session._cpm_logging_active = False

        #
        # Test start for update
        #
        cpm_server_session.analyze_start_update()
        def wait_for_update():
            print 'installed', cpm_client_session._gpm_ids_installed
            print 'update', cpm_client_session._gpm_ids_update
            print 'remove', cpm_client_session._gpm_ids_remove

            if len(cpm_client_session._gpm_ids_installed.difference(['x-1-1-win','a-0-0-win'])) == 0:
                if ('a-0-0-win', 'a-1-1-win') in cpm_client_session._gpm_ids_update:
                    if 'a-0-0-win' in cpm_client_session._gpm_ids_remove:
                        return True
            return False
        giri_unittest.wait_until_with_timeout(wait_for_update, 50000)
        self.assertTrue(wait_for_update())

        cpm_client_session._reset_state()
        cpm_server_session._reset_state()

        #
        # Test start for install
        #
        cpm_server_session.analyze_start_install()
        def wait_for_install():
            if len(cpm_client_session._gpm_ids_installed) == 2:
                if len(cpm_client_session._gpm_ids_available) > 0:
                    if len(cpm_client_session._gpm_ids_update) == 0:
                        if len(cpm_client_session._gpm_ids_remove) == 0:
                            return True
            return False
        giri_unittest.wait_until_with_timeout(wait_for_install, 5000)
        self.assertTrue(wait_for_install())

        cpm_client_session._reset_state()
        cpm_server_session._reset_state()



        #
        # Test start for remove
        #
        cpm_server_session.analyze_start_remove()
        def wait_for_remove():
            if len(cpm_client_session._gpm_ids_installed) == 2:
                if len(cpm_client_session._gpm_ids_available) == 0:
                    if len(cpm_client_session._gpm_ids_update) == 0:
                        if len(cpm_client_session._gpm_ids_remove) == 0:
                            return True
            return False
        giri_unittest.wait_until_with_timeout(wait_for_remove, 5000)
        self.assertTrue(wait_for_remove())


        #
        # Test get info
        #
        cpm_client_session.analyze_get_info(cpm_client_session._gpm_ids_installed)
        def wait_for_info():
            print cpm_client_session._gpm_ids_info
            if 'a-0-0-win' in  cpm_client_session._gpm_ids_info:
                return True
            return False
        giri_unittest.wait_until_with_timeout(wait_for_info, 5000)
        self.assertTrue(wait_for_info())


        cpm_client_session._reset_state()
        cpm_server_session._reset_state()



        #
        # Test start sequence
        #
        cpm_server_session.analyze_start_install()
        def wait_for_install_2():
            if len(cpm_client_session._gpm_ids_installed) == 2:
                if len(cpm_client_session._gpm_ids_available) > 0:
                    if len(cpm_client_session._gpm_ids_update) == 0:
                        if len(cpm_client_session._gpm_ids_remove) == 0:
                            return True
            return False

        giri_unittest.wait_until_with_timeout(wait_for_install_2, 5000)
        self.assertTrue(wait_for_install_2())


        cpm_client_session.analyze_update_package_selection(set(['a_extra-1-1-win']), cpm_client_session._gpm_ids_update, cpm_client_session._gpm_ids_remove, )
        def wait_for_update_2():
            print cpm_client_session._gpm_ids_installed, cpm_client_session._gpm_ids_install, cpm_client_session._gpm_ids_update, cpm_client_session._gpm_ids_remove
            if len(cpm_client_session._gpm_ids_installed) == 2:
                if len(cpm_client_session._gpm_ids_available) > 0:
                    if len(cpm_client_session._gpm_ids_install) > 0:
                            if len(cpm_client_session._gpm_ids_remove) > 0:
                                return True
            return False
        giri_unittest.wait_until_with_timeout(wait_for_update_2, 5000)
        self.assertTrue(wait_for_update_2())

        cpm_client_session.download_start(cpm_client_session._gpm_ids_install, cpm_client_session._gpm_ids_update)
        def wait_for_install_done():
            return cpm_client_session._state in [cpm_client_session.STATE_READY, cpm_client_session.STATE_INSTALL_DONE]
        giri_unittest.wait_until_with_timeout(wait_for_install_done, 50000)
        self.assertTrue(wait_for_install_done())

        giri_unittest.get_async_service().stop_and_wait()
        


#
# GIRI_UNITTEST
#
#GIRI_UNITTEST_TIMEOUT = 60
#GIRI_UNITTEST_IGNORE = False
#GIRI_UNITTEST_TAGS = []
GIRI_UNITTEST_OWNER = 'pwl'
GIRI_UNITTEST_TIMEOUT = 120

if __name__ == '__main__':
    gut_py = giri_unittest.GiriUnittest()
    gut_py.run();
