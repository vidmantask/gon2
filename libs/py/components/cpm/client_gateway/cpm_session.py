"""
Runtime client CPM(Client Package Management) session handling
"""
from __future__ import with_statement

import os
import sys
import os.path
import StringIO
import time
import datetime
import threading
import shutil
import subprocess

import lib.encode
import lib.version
import lib.appl.gon_client
from lib import checkpoint 
from components.communication import tunnel_endpoint_base
import components.presentation.modal_message

import cpm_endpoint_manager_session
import plugin_types.client_gateway.plugin_type_client_runtime_env

import lib.gpm.gpm_spec
import lib.gpm.gpm_env
import lib.gpm.gpm_builder
import lib.gpm.gpm_installer
import lib.gpm.gpm_installer_base

from components.cpm import * 

from components.presentation.update import UpdateController as gui_update_controller

import cpm_file_downloader

import components.traffic.client_gateway.client_launch_internal



class CPMSessionCB(object):
    def cpm_cb_restart(self, gon_client_install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted 
        and that the install_stat should be passed to the new instance of the gon_client.
        """ 
        pass

    def cpm_cb_save_restart_state(self, gon_client_install_state):
        """
        Callback from cpm_session requisting that the gon_client should be restarted at some point
        and that the install_stat should be passed to the new instance of the gon_client.
        The 
        """ 
        pass

    def cpm_cb_closed(self):
        """
        Callback from cpm_session telleing that it has closed down.
        """ 
        pass



class CPMInstallationThreadCB(plugin_types.client_gateway.plugin_type_client_runtime_env.ClientRuntimeEnvInstallCB):
    def cpm_installation_thread_cb_error(self, error_handler):
        pass



class CPMInstallationThread(threading.Thread):
    """
    An instance of this class represent a installation, it is launched in a thread because otherwise the GUI would block 
    """
    def __init__(self, checkpoint_handler, runtime_env, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpm_meta, cb, gon_client_install_state, dictionary, knownsecret, servers):
        threading.Thread.__init__(self, name="CPMInstallationThread")
        self.checkpoint_handler = checkpoint_handler
        self.runtime_env = runtime_env
        self.gpm_ids_install = gpm_ids_install
        self.gpm_ids_update = gpm_ids_update
        self.gpm_ids_remove = gpm_ids_remove
        self.gpm_meta = gpm_meta
        self.cb = cb
        self.gon_client_install_state = gon_client_install_state
        self.dictionary = dictionary
        self._running = False
        self.error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        self.knownsecret = knownsecret 
        self.servers = servers
        
    def run(self):
        with self.checkpoint_handler.CheckpointScope("CPMInstallationThread.run", module_id, checkpoint.DEBUG): 
            try:
                self._running = True
                self.runtime_env.install_gpms(self.gpm_ids_install, self.gpm_ids_update, self.gpm_ids_remove, self.gpm_meta , self.cb, self.gon_client_install_state, self.error_handler, self.dictionary, self.knownsecret, self.servers)
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("CPMInstallationThread.run.error", module_id, checkpoint.ERROR, etype, evalue, etrace)
                self.error_handler.emmit_error(evalue)

            if self.error_handler.error():
                self.checkpoint_handler.CheckpointMultilineMessage("CPMInstallationThread::error", module_id, checkpoint.ERROR, self.error_handler.dump_as_string())
                self.cb.cpm_installation_thread_cb_error(self.error_handler)
            elif self.error_handler.warning():
                self.checkpoint_handler.CheckpointMultilineMessage("CPMInstallationThread::warning", module_id, checkpoint.WARNING, self.error_handler.dump_as_string())
            self._running = False

    def is_running(self):
        return self._running



class CPMSessionConnectUpdateServersGUIHandler(object):
    """
    This class handles the interaction with the user during connect update of servers
    """
    STATE_INIT = 0
    STATE_RUNNING = 1

    def __init__(self, cpm_session, async_service, checkpoint_handler, user_interface, dictionary):
        self.cpm_session = cpm_session
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.user_interface = user_interface
        self.dictionary = dictionary
        self._state = CPMSessionConnectUpdateServersGUIHandler.STATE_INIT
        self._do_restart = True

    def start(self, error_message=None):
        if self._state != CPMSessionConnectUpdateGUIHandler.STATE_INIT:
            self.checkpoint_handler.Checkpoint("CPMSessionConnectUpdateServersGUIHandler::start.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
        self._state = CPMSessionConnectUpdateGUIHandler.STATE_RUNNING

        if error_message is None:
            self._do_restart = True
            message_head = self.dictionary._("Connection information has been updated")
            message = self.dictionary._('The connection information has been updated and the G/On Client will be restarted.')
        else:
            self._do_restart = False
            message_head = self.dictionary._("Connection information has not been updated")
            message = error_message

        # Hack to make dialog work
        self.user_interface.modalmessage.set_and_format_text(message, message_head)
        self.user_interface.modalmessage.subscribe(self._gui_callback)
        self.user_interface.modalmessage.set_next_allowed(True)
        self.user_interface.modalmessage.set_cancel_allowed(False)
        self.user_interface.modalmessage.view.display()

    def _gui_callback(self):
        if self.user_interface.modalmessage.get_next_clicked():
            self.user_interface.modalmessage.set_next_clicked(False)
            self._gui_callback_ok()

    def _gui_callback_ok(self):
        self.user_interface.modalmessage.unsubscribe(self._gui_callback)
        self.user_interface.modalmessage.view.hide()
        if self._do_restart:
            self.cpm_session._connect_update_callback_restart()
        else:
            self.cpm_session._connect_update_callback_stop()


class CPMSessionConnectUpdateGUIHandler(object):
    """
    This class handles the interaction with the user during connect update
    """
    STATE_INIT = 0
    STATE_RUNNING = 1
    
    def __init__(self, cpm_session, async_service, checkpoint_handler, user_interface, dictionary):
        self.cpm_session = cpm_session
        self.async_service = async_service
        self.checkpoint_handler = checkpoint_handler
        self.user_interface = user_interface
        self.dictionary = dictionary
        self._state = CPMSessionConnectUpdateGUIHandler.STATE_INIT
        
    def start(self, do_update_servers, servers, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        if self._state != CPMSessionConnectUpdateGUIHandler.STATE_INIT:
            self.checkpoint_handler.Checkpoint("CPMSessionConnectUpdateGUIHandler::start.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return

        self._state = CPMSessionConnectUpdateGUIHandler.STATE_RUNNING
        self.do_update_servers = do_update_servers
        self.servers = servers
        self.is_security_update = is_security_update
        self.gpm_ids_install = gpm_ids_install
        self.gpm_ids_update = gpm_ids_update
        self.gpm_ids_remove = gpm_ids_remove
        self.support_download, self.support_install = self.cpm_session._support_download_install()

        if not self.support_download['supported']:
            error_message = self.support_download['message']
            self._start_no_update(error_message)
        elif not self.support_install['supported']:
            error_message = self.support_install['message']
            self._start_no_update(error_message)
        else:
            self._start_update(is_security_update)

    def _start_update(self, is_security_update):
        headline = self.dictionary._('An update is available')
        message = self.dictionary._('A new version of your G/On software is available.<br>It may take a little while to install it. So please be patient.')
        self.user_interface.modalmessage.set_size(self.user_interface.modalmessage.ID_SMALL_SIZE)
        self.user_interface.modalmessage.set_and_format_text(message, headline)
        self.user_interface.modalmessage.set_next_label(self.dictionary._("Install"))
        if self.is_security_update:
            self.user_interface.modalmessage.set_cancel_label(self.dictionary._("Exit"))
        else:
            self.user_interface.modalmessage.set_cancel_label(self.dictionary._("Not now"))
        self.user_interface.modalmessage.set_default_button(self.user_interface.modalmessage.ID_NEXT_BUTTON)
        self.user_interface.modalmessage.subscribe(self._gui_callback)
        self.user_interface.modalmessage.set_next_allowed(True)
        self.user_interface.modalmessage.set_cancel_allowed(True)
        self.user_interface.modalmessage.view.display()

    def _start_no_update(self, error_message):
        headline = self.dictionary._('An update is available')
        message = self.dictionary._('An update is available, but you are not able to update at the moment. %s') % error_message
        self.user_interface.modalmessage.set_and_format_text(message, headline)
        self.user_interface.modalmessage.set_next_label(self.dictionary._("Exit"))
        self.user_interface.modalmessage.set_cancel_label(self.dictionary._("Not now"))
        self.user_interface.modalmessage.set_next_allowed(True)
        self.user_interface.modalmessage.set_cancel_allowed(True)
        self.user_interface.modalmessage.set_default_button(self.user_interface.modalmessage.ID_CANCEL_BUTTON)
        self.user_interface.modalmessage.subscribe(self._gui_callback)
        self.user_interface.modalmessage.view.display()

    def _gui_callback(self):
        if self.user_interface.modalmessage.get_next_clicked():
            self.user_interface.modalmessage.set_next_clicked(False)
            self._gui_callback_ok()
            
        elif self.user_interface.modalmessage.get_cancel_clicked():
            self.user_interface.modalmessage.set_cancel_clicked(False)
            self._gui_callback_cancel()
        
    def _gui_callback_ok(self):
        self.user_interface.modalmessage.unsubscribe(self._gui_callback)
        self.user_interface.modalmessage.view.hide()
        if self.support_download['supported'] and self.support_install['supported']:
            self.cpm_session._connect_update_callback_update(self.do_update_servers, self.servers, self.gpm_ids_install, self.gpm_ids_update, self.gpm_ids_remove)
        else:
            self.cpm_session._connect_update_callback_stop()
    
    def _gui_callback_cancel(self):
        self.user_interface.modalmessage.unsubscribe(self._gui_callback)
        self.user_interface.modalmessage.view.hide()
        if self.is_security_update:
            self.cpm_session._connect_update_callback_stop()
        else:
            self.cpm_session._connect_update_callback_cancel()
    

class CPMSessionBase(CPMInstallationThreadCB):
    STATE_READY = 0
    STATE_ANALYZE = 2
    STATE_DOWNLOAD_WAIT_FOR_TICKET = 3
    STATE_DOWNLOAD_WAIT_FOR_TICKET_CANCELING = 4
    STATE_DOWNLOAD = 5
    STATE_DOWNLOAD_CANCELING = 6
    STATE_DOWNLOAD_DONE = 7
    STATE_INSTALL = 8
    STATE_INSTALL_DONE = 9
    STATE_INSTALL_ERROR = 10
    STATE_RESTART = 11
    STATE_RESTART_WITH_UNPLUG = 12
    STATE_RESTART_DONE = 13
    STATE_DONE = 14
    STATE_CLOSEING = 15
    STATE_ERROR = 16

    MODE_UPDATE = 0
    MODE_INSTALL = 1
    MODE_REMOVE = 2
    MODE_QUERY = 3

    CPM_LOGGING_FILENAME = 'gon_client_cpm.log'

    def __init__(self, async_service, checkpoint_handler, user_interface, dictionary, runtime_env, cb = None):
        self.checkpoint_handler = checkpoint_handler
        self.checkpoint_handler_org = checkpoint_handler
        self.async_service = async_service
        self._user_interface = user_interface
        self.dictionary = dictionary
        self._runtime_env = runtime_env
        self._cb = cb
        self._reset()
        self._installation_thread = None
        self._cpm_logging_active = True
        self._mode = CPMSessionBase.MODE_UPDATE

    def _reset(self):
        self._gpm_ids_available = set()
        self._gpm_ids_installed = set()
        self._gpm_ids_updates = set()
        self._gpm_ids_install = set()
        self._gpm_ids_update = set()
        self._gpm_ids_remove = set()
        self._knownsecret = None
        self._servers = None
        self._gpm_ids_info = {}
        self._gpm_ids_selected = {}
        self._gui_initialized = False
        self._gui_errormessage = ''
        self.gon_client_install_state = None
        self.cpm_checkpoint_handler = None

    def _reset_state(self):
        pass

    def _cpm_logging_start(self):
        if not self._cpm_logging_active:
            return
        
        if self.cpm_checkpoint_handler != None:
            self._cpm_logging_stop()
            
        self.cpm_logfilename = os.path.join(self._runtime_env.get_temp_root(), CPMSessionBase.CPM_LOGGING_FILENAME)
        if os.path.exists(self.cpm_logfilename):
            shutil.move(self.cpm_logfilename, lib.checkpoint.generate_rotate_filename(self.cpm_logfilename))
            
        cpm_checkpoint_output_handler = lib.checkpoint.CheckpointOutputHandlerTEXT(lib.encode.preferred(self.cpm_logfilename, 'replace'), True)
        cpm_checkpoint_filter = lib.checkpoint.CheckpointFilter_true()
        self.cpm_checkpoint_handler = lib.checkpoint.CheckpointHandler(cpm_checkpoint_filter, cpm_checkpoint_output_handler, self.checkpoint_handler)
        self.checkpoint_handler = self.cpm_checkpoint_handler

    def _cpm_logging_restart(self):
        if not self._cpm_logging_active:
            return

        if self.cpm_checkpoint_handler != None:
            self._cpm_logging_stop()

        cpm_logfilename_old = self.cpm_logfilename
        self.cpm_logfilename = os.path.join(self._runtime_env.get_temp_root(), CPMSessionBase.CPM_LOGGING_FILENAME)
        if os.path.normpath(cpm_logfilename_old) != os.path.normpath(self.cpm_logfilename):
            if os.path.exists(self.cpm_logfilename):
                shutil.move(self.cpm_logfilename, lib.checkpoint.generate_rotate_filename(lib.encode.preferred(self.cpm_logfilename,  'replace')))
            if os.path.exists(cpm_logfilename_old):
                shutil.move(cpm_logfilename_old, self.cpm_logfilename)
            
        cpm_checkpoint_output_handler = lib.checkpoint.CheckpointOutputHandlerTEXTAppend(lib.encode.preferred(self.cpm_logfilename, 'replace'), True)
        cpm_checkpoint_filter = lib.checkpoint.CheckpointFilter_true()
        self.cpm_checkpoint_handler = lib.checkpoint.CheckpointHandler(cpm_checkpoint_filter, cpm_checkpoint_output_handler, self.checkpoint_handler)
        self.checkpoint_handler = self.cpm_checkpoint_handler

    def _cpm_logging_stop(self):
        if self.cpm_checkpoint_handler != None:
            self.cpm_checkpoint_handler.close()
            self.cpm_checkpoint_handler = None
            self.checkpoint_handler = self.checkpoint_handler_org
        
    #
    # Methods for handling install state
    #
    def install_start(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove, knownsecret=None, servers=None):
        if not self._state in [CPMSessionBase.STATE_ANALYZE, CPMSessionBase.STATE_DOWNLOAD_DONE, CPMSessionBase.STATE_RESTART_DONE]:
            self.checkpoint_handler.Checkpoint("CPMSession::install_start.invalid_state", module_id, checkpoint.ERROR)
            return

        if self._installation_thread != None and self._installation_thread.is_running():
            self.checkpoint_handler.Checkpoint("CPMSession::install_start.installation_seems_to_be_already_running", module_id, checkpoint.ERROR)
            return
        
        with self.checkpoint_handler.CheckpointScope("CPMSession::install_start", module_id, checkpoint.DEBUG):
            self._state = CPMSessionBase.STATE_INSTALL
            if self._user_interface != None:
                if not self._gui_initialized:
                    self._gui_init()
                    self._user_interface.update.view.display()
                self._gui_update_state()
                self._user_interface.update.set_info_progress_complete(0)
            self._installation_thread = CPMInstallationThread(self.checkpoint_handler, self._runtime_env, gpm_ids_install, gpm_ids_update, gpm_ids_remove, self._gpm_ids_info, self, self.gon_client_install_state, self.dictionary, knownsecret, servers)
            self._installation_thread.start()
    
    def gpm_installer_cb_task_all_start(self, tasks_count, tasks_ticks_count):
        self.checkpoint_handler.Checkpoint("CPMSession::gpm_installer_cb_task_all_start", module_id, checkpoint.DEBUG, tasks_count=tasks_count)
        try:
            self._state = CPMSessionBase.STATE_INSTALL
            if self._user_interface != None:
                self._gui_tasknumber = 0
                self._gui_tasknumber_max = tasks_count
                self._gui_tick_value  = 0
                self._gui_tick_value_max = tasks_ticks_count
                self._user_interface.update.set_info_progress_complete(0)
                self._gui_update_state()
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("CPMSession.install_task_all_start.error", module_id, lib.checkpoint.ERROR, etype, evalue, etrace)

    def gpm_installer_cb_task_start(self, task_title, task_ticks_count):
        self.checkpoint_handler.Checkpoint("CPMSession::gpm_installer_cb_task_start", module_id, checkpoint.DEBUG, task_title=task_title)
        if self._user_interface != None:
            self._user_interface.update.set_info_progress_subtext(task_title)

    def gpm_installer_cb_task_done(self):
        self.checkpoint_handler.Checkpoint("CPMSession::gpm_installer_cb_task_done", module_id, checkpoint.DEBUG)
        if self._user_interface != None:
            self._user_interface.update.set_info_progress_subtext("")
    
    def gpm_installer_cb_action_start(self, action_title, action_ticks_conut):
        self.checkpoint_handler.Checkpoint("CPMSession::gpm_installer_cb_action_start", module_id, checkpoint.DEBUG, action_title=action_title)

    def gpm_installer_cb_action_done(self):
        self.checkpoint_handler.Checkpoint("CPMSession::gpm_installer_cb_action_done", module_id, checkpoint.DEBUG)
            
    def gpm_installer_cb_action_tick(self, ticks):
        if self._user_interface != None:
            self._gui_tick_value += ticks
            tick = (self._gui_tick_value * 100) / self._gui_tick_value_max
            self._user_interface.update.set_info_progress_complete(tick)

    def gpm_installer_cb_task_all_done(self):
        if self._state != CPMSessionBase.STATE_INSTALL:
            self.checkpoint_handler.Checkpoint("CPMSession::install_task_all_done.invalid_state", module_id, checkpoint.ERROR)
            return
        self.checkpoint_handler.Checkpoint("CPMSession::install_task_all_done", module_id, checkpoint.DEBUG)
        self._state = CPMSessionBase.STATE_INSTALL_DONE
        if self._user_interface != None:
            self._gui_update_state()

    def installation_done(self):
        """
        Intented to be overwritten if somthing need to be done after installation, eg. notify server in online mode
        """
        pass

    def cpm_installation_thread_cb_error(self, error_handler):
        self._state = CPMSessionBase.STATE_INSTALL_ERROR
        if self._user_interface != None:
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_info_headline(self.dictionary._('Error was found during installation'))
            self._user_interface.update.set_info_free_text(error_handler.dump_as_string(1))
            self._gui_update_state()

    def client_runtime_env_install_cb_restart(self, restart_gon_client_install_state, do_restart_but_skip_launch=False, wait_for_device=None):
        if self._state != CPMSessionBase.STATE_INSTALL_DONE:
            self.checkpoint_handler.Checkpoint("CPMSession::client_runtime_env_install_cb_restart.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return

        self.restart_gon_client_install_state = restart_gon_client_install_state
        self.checkpoint_handler.Checkpoint("CPMSession::client_runtime_env_install_cb_restart", module_id, checkpoint.DEBUG)
        
        if do_restart_but_skip_launch:
            self._state = CPMSessionBase.STATE_RESTART_WITH_UNPLUG
            self.restart_with_unplug_wait_for_device = wait_for_device
        else:
            self._state = CPMSessionBase.STATE_RESTART
        if  restart_gon_client_install_state.is_done():
            self._state = CPMSessionBase.STATE_DONE
        if self._user_interface != None:
            self._user_interface.update.set_info_progress_complete(100)
            self._user_interface.update.set_info_progress_subtext('')
            self._gui_update_state()
        if self._state == CPMSessionBase.STATE_RESTART:
            time.sleep(3)
            self.restart()
        if self._state == CPMSessionBase.STATE_RESTART_WITH_UNPLUG:
            self._build_install_state()
            self._cb.cpm_cb_save_restart_state(self.restart_gon_client_install_state)
            while(os.path.exists(wait_for_device)):
                time.sleep(1)
            self.close()
            
    def close(self):
        self._cpm_logging_stop()
        self._state = CPMSessionBase.STATE_CLOSEING
        if self._user_interface != None:
            self._user_interface.update.view.hide()
        if self._cb is not None:
            self._cb.cpm_cb_closed()
        
        self.installation_done()
        self._reset_state()

    def _build_install_state(self):
        gon_client_install_state_cpm_session_data = {}
        gon_client_install_state_cpm_session_data['mode'] = self._mode
        gon_client_install_state_cpm_session_data['gpm_ids_install'] = self._gpm_ids_install
        gon_client_install_state_cpm_session_data['gpm_ids_update'] = self._gpm_ids_update
        gon_client_install_state_cpm_session_data['gpm_ids_remove'] = self._gpm_ids_remove        
        gon_client_install_state_cpm_session_data['gpm_ids_info'] = self._gpm_ids_info        
        gon_client_install_state_cpm_session_data['cpm_logfilename'] = self.cpm_logfilename        
        gon_client_install_state_cpm_session_data['servers'] = self._servers        
        gon_client_install_state_cpm_session_data['knownsecret'] = self._knownsecret        
        self.restart_gon_client_install_state.set_cpm_session_data(gon_client_install_state_cpm_session_data)

    def restart(self):
        if self._user_interface != None:
            self._user_interface.update.view.hide()
        self._state = CPMSessionBase.STATE_CLOSEING
        self._build_install_state()
        self.checkpoint_handler.Checkpoint("CPMSession::restart", module_id, checkpoint.DEBUG, cpm_session_data=str(self.restart_gon_client_install_state.get_cpm_session_data()), plugin_data=str(self.restart_gon_client_install_state.get_plugin_data()))
        self._cb.cpm_cb_restart(self.restart_gon_client_install_state)

    #
    # Misc GUI related methods
    #
    def _gui_init(self):
        if not self._gui_initialized: 
            self._user_interface.update.subscribe(self._gui_on_update)
            if self._mode == CPMSessionBase.MODE_INSTALL:
                self._user_interface.update.set_banner(gui_update_controller.ID_BANNER_INSTALL)
            elif self._mode == CPMSessionBase.MODE_REMOVE:
                self._user_interface.update.set_banner(gui_update_controller.ID_BANNER_REMOVE)
            elif self._mode == CPMSessionBase.MODE_UPDATE:
                self._user_interface.update.set_banner(gui_update_controller.ID_BANNER_UPDATE)
            self._gui_initialized = True
            self._gui_update_state()

    def _gui_on_update(self):
        if self._state == CPMSessionBase.STATE_ANALYZE:
            self._gui_analyze_update_package_selection()
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                if self._gui_somthing_to_download():
                    self.download_start(self._gpm_ids_install, self._gpm_ids_update)
                else:
                    self.install_start(self._gpm_ids_install, self._gpm_ids_update, self._gpm_ids_remove, self._knownsecret, self._servers)
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.close()
        if self._state == CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET:
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.download_cancel_wait_for_download_ticket()
        if self._state == CPMSessionBase.STATE_DOWNLOAD:
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.download_cancel()
        elif self._state == CPMSessionBase.STATE_INSTALL_DONE:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.close()
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
        elif self._state == CPMSessionBase.STATE_INSTALL_ERROR:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.close()
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.close()
        elif self._state == CPMSessionBase.STATE_ERROR:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.close()
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.close()
        elif self._state == CPMSessionBase.STATE_RESTART:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.restart()
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
        elif self._state == CPMSessionBase.STATE_RESTART_WITH_UNPLUG:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.restart(do_restart_but_skip_launch=True)
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
        elif self._state == CPMSessionBase.STATE_DONE:
            if self._user_interface.update.get_next_clicked():
                self._user_interface.update.reset_next_clicked()
                self.restart() 
            if self._user_interface.update.get_cancel_clicked():
                self._user_interface.update.reset_cancel_clicked()
                self.close()

        if self._state != self._state == CPMSessionBase.STATE_CLOSEING:
            self._user_interface.update.reset_next_clicked()
            self._user_interface.update.reset_cancel_clicked()


    def _gui_update_package_selection(self):
        #
        # Examin if we have all package information available
        #
        missing_info_gpm_ids = []
        for gpm_id in self._gpm_ids_available:
            if gpm_id not in self._gpm_ids_info:
                missing_info_gpm_ids.append(gpm_id)
        for gpm_id_from, gpm_id_to in self._gpm_ids_update:
            if gpm_id_from not in self._gpm_ids_info:
                missing_info_gpm_ids.append(gpm_id_from)
            if gpm_id_to not in self._gpm_ids_info:
                missing_info_gpm_ids.append(gpm_id_to)
        for gpm_id in self._gpm_ids_install:
            if gpm_id not in self._gpm_ids_info:
                missing_info_gpm_ids.append(gpm_id)
        for gpm_id in self._gpm_ids_remove:
            if gpm_id not in self._gpm_ids_info:
                missing_info_gpm_ids.append(gpm_id)
        
        if len(missing_info_gpm_ids) != 0:
            self.analyze_get_info(missing_info_gpm_ids)
            
        selection_list = [] 
        if self._mode == CPMSessionBase.MODE_UPDATE:
            for gpm_id_from, gpm_id_to in self._gpm_ids_updates:
                gpm_id_key = (gpm_id_from, gpm_id_to)
                self._gui_append_selection(selection_list, gpm_id_to, gpm_id_key)
        elif self._mode == CPMSessionBase.MODE_INSTALL:
            for gpm_id in self._gpm_ids_available:
                self._gui_append_selection(selection_list, gpm_id, gpm_id)
        elif self._mode == CPMSessionBase.MODE_REMOVE:
            for gpm_id in self._gpm_ids_installed:
                self._gui_append_selection(selection_list, gpm_id, gpm_id)
        self._user_interface.update.set_selection_list(selection_list)


    def _gui_analyze_update_package_selection(self):
        selection_list = self._user_interface.update.get_selection_list()
        gpm_ids_selected = set()
        for selection in selection_list:
            if selection['selected']:
                gpm_ids_selected.add(selection['gpm_id_key'])
        gpm_ids_install = set()
        gpm_ids_update = set()
        gpm_ids_remove = set()
        
        if gpm_ids_selected != self._gpm_ids_selected:
            self._gpm_ids_selected = gpm_ids_selected 
            if self._mode == CPMSessionBase.MODE_UPDATE:
                for gpm_key in self._gpm_ids_updates:
                    if gpm_key in gpm_ids_selected:
                        gpm_ids_update.add(gpm_key)
            elif self._mode == CPMSessionBase.MODE_INSTALL:
                for gpm_key in self._gpm_ids_available:
                    if gpm_key in gpm_ids_selected:
                        gpm_ids_install.add(gpm_key)
            elif self._mode == CPMSessionBase.MODE_REMOVE:
                for gpm_key in self._gpm_ids_installed:
                    if gpm_key in gpm_ids_selected:
                        gpm_ids_remove.add(gpm_key)
            self.analyze_update_package_selection(gpm_ids_install, gpm_ids_update, gpm_ids_remove)

    def _gui_update_phases(self):
        phase_selection = {'name': self.dictionary._('Select'),  'state': gui_update_controller.ID_PHASE_PENDING, 'show': True}
        phase_download =  {'name': self.dictionary._('Download'),'state': gui_update_controller.ID_PHASE_PENDING, 'show': self._gui_somthing_to_download()}
        phase_install =   {'name': self.dictionary._('Install'), 'state': gui_update_controller.ID_PHASE_PENDING, 'show': True}
        phase_done =      {'name': self.dictionary._('Done'),    'state': gui_update_controller.ID_PHASE_PENDING, 'show': True}
        phase_error =     {'name': self.dictionary._('Error'),   'state': gui_update_controller.ID_PHASE_PENDING, 'show': False}

        if self._mode == CPMSessionBase.MODE_REMOVE:
            phase_install['name'] = self.dictionary._('Remove')
        elif self._mode == CPMSessionBase.MODE_UPDATE:
            phase_install['name'] = self.dictionary._('Update')

        (download, install) = self._support_download_install()
        if self._state == CPMSessionBase.STATE_ANALYZE:
            phase_selection['state'] = gui_update_controller.ID_PHASE_ACTIVE
            if not install['supported'] or self._gui_nothing_to_do():
                phase_download['show'] = False
                phase_install['show'] = False
                phase_done['show'] = False
        elif self._state in [CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET, CPMSessionBase.STATE_DOWNLOAD]:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state == CPMSessionBase.STATE_INSTALL:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_install['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state == CPMSessionBase.STATE_INSTALL_DONE:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_install['state'] = gui_update_controller.ID_PHASE_COMPLETE
            if not install['require_restart']:
                phase_done['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state == CPMSessionBase.STATE_INSTALL_ERROR:
            phase_error['state'] = gui_update_controller.ID_PHASE_ACTIVE
            phase_selection['show'] = False
            phase_download['show'] = False
            phase_install['show'] = False
            phase_done['show'] = False
            phase_error['show'] = True
        elif self._state == CPMSessionBase.STATE_ERROR:
            phase_error['state'] = gui_update_controller.ID_PHASE_ACTIVE
            phase_selection['show'] = False
            phase_download['show'] = False
            phase_install['show'] = False
            phase_done['show'] = False
            phase_error['show'] = True
        elif self._state == CPMSessionBase.STATE_RESTART:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_install['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state == CPMSessionBase.STATE_RESTART_WITH_UNPLUG:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_install['state'] = gui_update_controller.ID_PHASE_ACTIVE
        elif self._state == CPMSessionBase.STATE_DONE:
            phase_selection['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_download['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_install['state'] = gui_update_controller.ID_PHASE_COMPLETE
            phase_done['state'] = gui_update_controller.ID_PHASE_ACTIVE
        tasklist = []
        self._gui_append_to_tasklist(tasklist, phase_selection)
        self._gui_append_to_tasklist(tasklist, phase_download)
        self._gui_append_to_tasklist(tasklist, phase_install)
        self._gui_append_to_tasklist(tasklist, phase_done)
        self._gui_append_to_tasklist(tasklist, phase_error)
        self._user_interface.update.set_phase_list(tasklist)

    def _gui_append_to_tasklist(self, tasklist, phase):
        if phase['show']:
            tasklist.append(phase)

    def _gui_update_state(self):
        if self._state == CPMSessionBase.STATE_ANALYZE:
            (download, install) = self._support_download_install()
            self._user_interface.update.set_show_selection_list(True)
            self._user_interface.update.set_selection_method(gui_update_controller.ID_SELECTION_MULTIPLE)
            if not download['supported']:
                self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
                self._user_interface.update.set_info_headline(self.dictionary._('You are not able to download the selected packages'))
                self._user_interface.update.set_info_free_text(download['message'])
                self._user_interface.update.set_cancel_allowed(True)
                self._user_interface.update.set_next_allowed(False)
            elif not install['supported']:
                self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
                if self._mode == CPMSessionBase.MODE_INSTALL:
                    self._user_interface.update.set_info_headline(self.dictionary._('You are not able to install the selected packages'))
                elif self._mode == CPMSessionBase.MODE_UPDATE:
                    self._user_interface.update.set_info_headline(self.dictionary._('You are not able to update the selected packages'))
                elif self._mode == CPMSessionBase.MODE_REMOVE:
                    self._user_interface.update.set_info_headline(self.dictionary._('You are not able to remove the selected packages'))
                self._user_interface.update.set_info_free_text(install['message'])
                self._user_interface.update.set_cancel_allowed(True)
                self._user_interface.update.set_next_allowed(False)
            else:
                self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
                self._user_interface.update.set_info_free_text('')
                if self._gui_no_packages_for_selection():
                    self._user_interface.update.set_show_selection_list(False)
                    self._user_interface.update.set_next_allowed(False)
                    if self._mode == CPMSessionBase.MODE_INSTALL:
                        self._user_interface.update.set_info_headline(self.dictionary._('No packages available for installation'))
                    elif self._mode == CPMSessionBase.MODE_UPDATE:
                        self._user_interface.update.set_info_headline(self.dictionary._('No packages need to be updated'))
                    elif self._mode == CPMSessionBase.MODE_REMOVE:
                        self._user_interface.update.set_info_headline(self.dictionary._('No packages can be removed'))
                else:
                    if self._mode == CPMSessionBase.MODE_INSTALL:
                        self._user_interface.update.set_info_headline(self.dictionary._('Select packages to be installed'))
                    elif self._mode == CPMSessionBase.MODE_UPDATE:
                        self._user_interface.update.set_info_headline(self.dictionary._('Select packages to be updated'))
                    elif self._mode == CPMSessionBase.MODE_REMOVE:
                        self._user_interface.update.set_info_headline(self.dictionary._('Select packages to be removed'))
                    self._user_interface.update.set_cancel_allowed(True)
                    if self._gui_nothing_to_do():
                        self._user_interface.update.set_next_allowed(False)
                    else:
                        self._user_interface.update.set_next_allowed(True)
                        if install['require_restart']:
                            if self._mode in [CPMSessionBase.MODE_INSTALL]:
                                self._user_interface.update.set_info_free_text(self.dictionary._("A restart of the G/On Client is needed in order to install the selected packages, please close all applications currently using G/On"))
                            elif self._mode in [CPMSessionBase.MODE_UPDATE]:
                                self._user_interface.update.set_info_free_text(self.dictionary._("A restart of the G/On Client is needed in order to update the selected packages, please close all applications currently using G/On"))
                            elif self._mode in [CPMSessionBase.MODE_REMOVE]:
                                self._user_interface.update.set_info_free_text(self.dictionary._("A restart of the G/On Client is needed in order to remove the selected packages, please close all applications currently using G/On"))

        elif self._state == CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_BUTTON_TEXT)
            self._user_interface.update.set_info_headline(self.dictionary._('Waiting to get access to the download service.'))
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_cancel_description(self.dictionary._('Click Cancel to stop download.'))
            self._user_interface.update.set_next_allowed(False)
            self._user_interface.update.set_next_description(self.dictionary._("Click Next to begin download of packages"))
        elif self._state == CPMSessionBase.STATE_DOWNLOAD:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self._user_interface.update.set_info_headline(self.dictionary._('Downloading packages'))
            self._user_interface.update.set_info_progress_subtext('')
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_DOWNLOAD_CANCELING:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_info_headline(self.dictionary._('Canceling download, please wait'))
            self._user_interface.update.set_info_free_text('')
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_INSTALL:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_PROGRESS)
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(False)
            if self._mode == CPMSessionBase.MODE_INSTALL:
                self._user_interface.update.set_info_headline(self.dictionary._('Installing packages'))
            elif self._mode == CPMSessionBase.MODE_UPDATE:
                self._user_interface.update.set_info_headline(self.dictionary._('Updating packages'))
            elif self._mode == CPMSessionBase.MODE_REMOVE:
                self._user_interface.update.set_info_headline(self.dictionary._('Removing packages'))
        elif self._state == CPMSessionBase.STATE_INSTALL_DONE:
            (download, install) = self._support_download_install()
            if not install['require_restart']:
                self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_BUTTON_TEXT)
                self._user_interface.update.set_cancel_allowed(False)
                self._user_interface.update.set_next_allowed(True)
                self._user_interface.update.set_next_label(self.dictionary._('OK'))
                self._user_interface.update.set_cancel_description('')
                self._user_interface.update.set_next_description(self.dictionary._('Click OK to finish.'))
                if self._mode == CPMSessionBase.MODE_INSTALL:
                    self._user_interface.update.set_info_headline(self.dictionary._('Installation is now complete'))
                elif self._mode == CPMSessionBase.MODE_UPDATE:
                    self._user_interface.update.set_info_headline(self.dictionary._('Upgrade is now complete'))
                elif self._mode == CPMSessionBase.MODE_REMOVE:
                    self._user_interface.update.set_info_headline(self.dictionary._('Removal is now complete'))
            else:
                self._user_interface.update.set_cancel_allowed(False)
                self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_INSTALL_ERROR:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_ERROR:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(True)
            self._user_interface.update.set_info_headline(self.dictionary._('An unexpected error has occurred.'))
            self._user_interface.update.set_info_free_text(self._gui_errormessage)
        elif self._state == CPMSessionBase.STATE_RESTART:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_info_headline(self.dictionary._('The G/On Client is restarting.'))
            self._user_interface.update.set_info_free_text('')
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_RESTART_WITH_UNPLUG:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            self._user_interface.update.set_info_headline(self.dictionary._('Please re-plug your key, and launch G/On Client'))
            self._user_interface.update.set_info_free_text("In order to complete the installation you need to unplug your key from the computer. After the key has been unplugged, insert the key, and launch the G/On Client.")
            self._user_interface.update.set_cancel_allowed(False)
            self._user_interface.update.set_next_allowed(False)
        elif self._state == CPMSessionBase.STATE_DONE:
            self._user_interface.update.set_show_selection_list(False)
            self._user_interface.update.set_info_mode(gui_update_controller.ID_MODE_INFORMATION_FREE_TEXT)
            if self._mode == CPMSessionBase.MODE_INSTALL:
                self._user_interface.update.set_info_headline(self.dictionary._('Installation is complete'))
                self._user_interface.update.set_info_free_text(self.dictionary._('The installation of the selected packages is now successfully completed.'))
            elif self._mode == CPMSessionBase.MODE_UPDATE:
                self._user_interface.update.set_info_headline(self.dictionary._('Upgrade is complete'))
                self._user_interface.update.set_info_free_text(self.dictionary._('A new version of your G/On software has been installed.\n\nClick "Start G/On" to start the new version.\nClick "Exit" to close the window without starting G/On'))
            elif self._mode == CPMSessionBase.MODE_REMOVE:
                self._user_interface.update.set_info_headline(self.dictionary._('Remove operation is now complete'))
                self._user_interface.update.set_info_free_text(self.dictionary._('The selected packages have now successfully been removed.'))
            self._user_interface.update.set_cancel_allowed(True)
            self._user_interface.update.set_next_allowed(True)
            self._user_interface.update.set_cancel_label(self.dictionary._('Exit'))
            self._user_interface.update.set_next_label(self.dictionary._('Start G/On'))

        self._gui_update_phases()

    def _gui_pp_size(self, size_bytes):
        if size_bytes == None:
            return ''
        if size_bytes < 1024:
            return "%d B" % (size_bytes)
        elif size_bytes < 1024000:
            return "%.1f KB" % (size_bytes / 1000.0)
        else:
            return "%.1f MB" % (size_bytes / 1000000.0)

    def _gui_arch(self, arch):
        if arch == 'win':
            return self.dictionary._("Windows")
        elif arch == 'mac':
            return self.dictionary._("Mac")
        elif arch == 'linux':
            return self.dictionary._("Linux")
        elif arch == 'noarch':
            return self.dictionary._("All")
        return arch

    def _gui_pp_version(self, version, version_release):
        if version_release != None:
            return '%s(%s)' % (version, version_release)
        return '%s' % (version)

    def _gui_nothing_to_do(self):
        if self._mode == CPMSessionBase.MODE_INSTALL and len(self._gpm_ids_install) > 0:
            return False
        elif self._mode == CPMSessionBase.MODE_UPDATE and len(self._gpm_ids_update) > 0:
            return False
        elif self._mode == CPMSessionBase.MODE_REMOVE and len(self._gpm_ids_remove) > 0:
            return False
        return True

    def _gui_no_packages_for_selection(self):
        return len(self._user_interface.update.get_selection_list()) == 0
   
    def _gui_somthing_to_download(self):
        return self._mode != CPMSessionBase.MODE_REMOVE
     
    def _gui_append_selection(self, selection_list, gpm_id, gpm_id_key):
        hide = False
        gui_info = {}
        gui_info['id'] = len(selection_list)
        gui_info['selected'] =  gpm_id_key in self._gpm_ids_selected
        gui_info['icon'] = 'g_package_default_32x32.bmp'
        gui_info['name'] = gpm_id
        gui_info['details'] = self.dictionary._('No information available at this time')
        gui_info['description'] = ''
        gui_info['gpm_id_key'] = gpm_id_key
        if gpm_id in self._gpm_ids_info:
            gpm_id_info = self._gpm_ids_info[gpm_id]
            gui_info['icon'] = self._calc_package_icon_filename(gpm_id_info['arch'])
            gui_info['name'] = gpm_id_info['summary']
            gui_version = self._gui_pp_version(gpm_id_info['version'], gpm_id_info['version_release']) 
            gui_size = self._gui_pp_size(gpm_id_info['size'])
            gui_arch = self._gui_arch(gpm_id_info['arch'])
            gui_info['details'] = '%s(%s), %s, %s' %(gpm_id_info['name'], gui_size, gui_arch, gui_version)

            hide = gpm_id_info['visibility_type'] == lib.gpm.gpm_spec.GpmHeader.VISIBILITY_TYPE_hidden
        if not hide:
            selection_list.append(gui_info)

    def _calc_package_icon_filename(self, arch):
        if  arch in['win', 'linux',  'mac']:
            return 'g_package_%s_32x32.bmp' % arch
        return 'g_package_default_32x32.bmp'

    #
    # Misc methods
    #
    def remote_handle_error_state(self, message):
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_handle_error_state", module_id, checkpoint.DEBUG, message=message):
            self._error_state_start(message)

    def _error_state_start(self, message):
        self._state = CPMSessionBase.STATE_ERROR
        if self._user_interface != None:
            self._gui_errormessage = message
            self._gui_update_state()
        else:
            self.close()

    def _support_download_install(self):
        try:
            if self._runtime_env != None:
                size_total = 0
                for gpm_id in self._gpm_ids_install:
                    if self._gpm_ids_info.has_key(gpm_id):
                        if self._gpm_ids_info[gpm_id].has_key('size') and self._gpm_ids_info[gpm_id]['size'] != None:
                            size_total += self._gpm_ids_info[gpm_id]['size']
                        if self._gpm_ids_info[gpm_id].has_key('package_size') and self._gpm_ids_info[gpm_id]['package_size'] != None:
                            size_total += self._gpm_ids_info[gpm_id]['package_size']
    
                download = self._runtime_env.support_download(size_total, self.dictionary)
                install = self._runtime_env.support_install_gpms(self._gpm_ids_install, self._gpm_ids_update, self._gpm_ids_remove, self._gpm_ids_info, self.gon_client_install_state, self.dictionary, self._knownsecret, self._servers)
                return (download, install)

        except:
            self._runtime_env = None
            self._state = CPMSessionBase.STATE_ERROR
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("CPMSessionBase._support_download_install", module_id, checkpoint.ERROR, etype, evalue, etrace)
        
        download = {}
        download['supported'] = False
        download['message'] = self.dictionary._('No runtime environment found')
        
        install = {}
        install['supported'] = False        
        install['message'] = self.dictionary._('No runtime environment found')
        install['require_restart'] = True
        return (download, install)


    def analyze_update_package_selection(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        pass

    def analyze_get_info(self, missing_info_gpm_ids):
        pass

    def download_start(self, gpm_ids_install, gpm_ids_update):
        pass
    
    def download_cancel_wait_for_download_ticket(self):
        pass
    
    def download_cancel(self):
        pass






class CPMSessionOnline(CPMSessionBase, tunnel_endpoint_base.TunnelendpointSession, components.traffic.client_gateway.client_launch_internal.LaunchSession):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, dictionary, runtime_env, cb = None):
        CPMSessionBase.__init__(self, async_service, checkpoint_handler, user_interface, dictionary, runtime_env, cb)
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        components.traffic.client_gateway.client_launch_internal.LaunchSession.__init__(self)
        self._download_tunnel_id = None
        self._cpm_endpoint_manager_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, 1)
        self._cpm_endpoint_manager = cpm_endpoint_manager_session.CPMEndpointManager(self.async_service, self.checkpoint_handler, self.dictionary, self._cpm_endpoint_manager_tunnelendpoint, self._runtime_env)
        self._reset_state()
    
    def _reset_state(self):
        self._reset()
        self._state = CPMSessionBase.STATE_READY
        self._gpm_ids_installed = set()
        self._gpm_ids_available = set()
        self._gpm_ids_updates = set()
        self._gpm_ids_install = set()
        self._gpm_ids_update = set()
        self._gpm_ids_remove = set()
        self._gpm_ids_selected = set()
        self._gpm_ids_info = {}
        self._mode = CPMSessionBase.MODE_UPDATE
        self._download_cleanup()

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        self._cpm_endpoint_manager.session_start()
    
    def session_close(self):
        """Hard close of session. Called from main session when communication is terminated"""
        pass
      
    #
    # Methods for handling analyze state
    #
    def analyze_ready(self):
        return self._state == CPMSessionBase.STATE_READY
        
    def remote_analyze_start_query(self):
        """
        Remote request to start a analyze session with the quest to find current installed packages.
        """
        if self._state != CPMSessionBase.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_query.invalid_state", module_id, checkpoint.ERROR)
            return
 
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_start_query", module_id, checkpoint.DEBUG, version=lib.version.Version.create_current().get_version_string()):
            self._state = CPMSessionBase.STATE_ANALYZE
            self._mode = CPMSessionBase.MODE_QUERY
            self.tunnelendpoint_remote('remote_analyze_start_response')
    
    def remote_analyze_start_update(self):
        """
        Remote request to start a analyze session with the quest to find current installed packages that need to be installed.
        """
        if self._state != CPMSessionBase.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_update.invalid_state", module_id, checkpoint.ERROR)
            return
 
        self._cpm_logging_start()
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_start_update", module_id, checkpoint.DEBUG, version=lib.version.Version.create_current().get_version_string()):
            self._state = CPMSessionBase.STATE_ANALYZE
            self._mode = CPMSessionBase.MODE_UPDATE
            self.tunnelendpoint_remote('remote_analyze_start_response')
            if self._user_interface != None:
                self._gui_init()
                self._user_interface.update.view.display()
                self._gui_update_state()
            
    def remote_analyze_start_install(self):
        """
        Remote request to start a analyze session with the quest to install new packages.
        """
        if self._state != CPMSessionBase.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_install.invalid_state", module_id, checkpoint.ERROR)
            return

        self._cpm_logging_start()
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_start_install", module_id, checkpoint.DEBUG, version=lib.version.Version.create_current().get_version_string()):
            self._state = CPMSessionBase.STATE_ANALYZE
            self._mode = CPMSessionBase.MODE_INSTALL
            self.tunnelendpoint_remote('remote_analyze_start_response')
            if self._user_interface != None:
                self._gui_init()
                self._user_interface.update.view.display()
                self._gui_update_state()
    
    def remote_analyze_start_remove(self):
        """
        Remote request to start a analyze session with the quest to remove installed packages.
        """
        if self._state != CPMSessionBase.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_remove.invalid_state", module_id, checkpoint.ERROR)
            return

        self._cpm_logging_start()
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_start_remove", module_id, checkpoint.DEBUG, version=lib.version.Version.create_current().get_version_string()):
            self._state = CPMSessionBase.STATE_ANALYZE
            self._mode = CPMSessionBase.MODE_REMOVE
            self.tunnelendpoint_remote('remote_analyze_start_response')
            if self._user_interface != None:
                self.analyze_get_info_local()
                self._gui_init()
                self._user_interface.update.view.display()
                self._gui_update_state()

    def remote_analyze_package_selection_start(self, gpm_ids_installed, gpm_ids_available, gpm_ids_updates, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        """
        Initiel notification from server about available, installed and updates. 
        """
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_package_selection_start.invalid_state", module_id, checkpoint.ERROR)
            return
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_package_selection_start", module_id, checkpoint.DEBUG, gpm_ids_installed=str(gpm_ids_installed), gpm_ids_available=str(gpm_ids_available), gpm_ids_updates=str(gpm_ids_updates), gpm_ids_install=str(gpm_ids_install), gpm_ids_update=str(gpm_ids_update), gpm_ids_remove=str(gpm_ids_remove)):
            self._gpm_ids_installed = gpm_ids_installed
            self._gpm_ids_available = gpm_ids_available
            self._gpm_ids_updates = gpm_ids_updates
            self._gpm_ids_install = gpm_ids_install
            self._gpm_ids_update = gpm_ids_update
            self._gpm_ids_remove = gpm_ids_remove

            if self._mode == CPMSessionBase.MODE_UPDATE:
                self.tunnelendpoint_remote('remote_analyze_update_package_selection', gpm_ids_install=[], gpm_ids_update=set(gpm_ids_updates), gpm_ids_remove=[])
                self._gpm_ids_selected = set(gpm_ids_updates)
            elif self._mode == CPMSessionBase.MODE_REMOVE:
                lib.appl.gon_client.remove_current_instance(self._gpm_ids_installed)
            
            if self._user_interface != None:
                self._gui_update_package_selection()
                self._gui_update_state()

    def remote_analyze_get_installed_meta(self):
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_get_installed_meta.invalid_state", module_id, checkpoint.ERROR)
            return
            
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_get_installed_meta", module_id, checkpoint.DEBUG):
            meta_strings = []
            error_handler = lib.gpm.gpm_env.GpmErrorHandler()
            metas = self._runtime_env.get_installed_gpm_meta_all(error_handler)
            if not error_handler.ok():
                self.checkpoint_handler.CheckpointMultilineMessage('CPMSession::remote_analyze_get_installed_meta.gpm_meta', module_id, checkpoint.WARNING, error_handler.dump_as_string())
            for meta in metas:
                meta.files_ro = None
                meta.files_rw = None
                meta_file = StringIO.StringIO()
                meta.to_tree().write(meta_file)
                meta_strings.append(meta_file.getvalue())
                meta_file.close()
            self.tunnelendpoint_remote('remote_analyze_get_installed_meta_response', meta_strings=meta_strings)
            if self._mode == CPMSessionBase.MODE_QUERY:
                self._state = CPMSessionBase.STATE_READY
    
    def remote_analyze_update_package_selection(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        """
        Notification from server that the package selection has changed. 
        This could be a reponse to a update_package_selection, or a a response to one of the start commands
        """
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_update_package_selection.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
    
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_update_package_selection", module_id, checkpoint.DEBUG):
            self._gpm_ids_install = gpm_ids_install
            self._gpm_ids_update = gpm_ids_update
            self._gpm_ids_remove = gpm_ids_remove
            if self._user_interface != None:
                self._gui_update_package_selection()
                self._gui_update_state()

    def analyze_update_package_selection(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_update_package_selection.invalid_state", module_id, checkpoint.ERROR)
            return

        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_update_package_selection", module_id, checkpoint.DEBUG):
            self.tunnelendpoint_remote('remote_analyze_update_package_selection',  gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove)

    def analyze_get_info(self, gpm_ids):
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_get_info.invalid_state", module_id, checkpoint.ERROR)
            return
        self.tunnelendpoint_remote('remote_analyze_get_info',  gpm_ids=gpm_ids)


    def analyze_get_info_local(self):
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        metas = self._runtime_env.get_installed_gpm_meta_all(error_handler)
        if not error_handler.ok():
            self.checkpoint_handler.CheckpointMultilineMessage('CPMSession::analyze_get_info_local.gpm_meta', module_id, checkpoint.WARNING, error_handler.dump_as_string())
        for meta in metas:
            meta_info = {}
            meta_info['name'] = meta.header.name
            meta_info['arch'] = meta.header.arch.get_as_string()
            meta_info['version'] = meta.header.version.get_as_string()
            if meta.header.version_release != None:
                meta_info['version_release'] = meta.header.version_release.get_as_string()
            else:
                meta_info['version_release'] = None
            meta_info['size'] = meta.get_size()
            
            (summary, description) = meta.header.descriptions.get_default()
            meta_info['summary'] = summary
            meta_info['description'] = description
            meta_info['contains_ro_files'] = meta.contains_ro_files()
            meta_info['bootification_enabled'] = meta.bootification_enabled()
            meta_info['visibility_type'] = meta.header.get_visibility_type()
            self._gpm_ids_info[meta.header.get_package_id()] = meta_info    

    def remote_analyze_get_info_response(self, gpm_ids_info):
        if self._state not in [CPMSessionBase.STATE_ANALYZE]:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_get_info_response.invalid_state", module_id, checkpoint.ERROR)
            return
        self._gpm_ids_info.update(gpm_ids_info)
        if self._user_interface != None:
            self._gui_update_package_selection()
            self._gui_update_state()

    #
    # Functionality for handling connect_update
    #
    def remote_analyze_start_connect_update(self, do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove, gpm_ids_info):
        if self._state != CPMSessionBase.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_connect_update.invalid_state", module_id, checkpoint.ERROR)
            return

        self._gpm_ids_install = gpm_ids_install
        self._gpm_ids_update = gpm_ids_update
        self._gpm_ids_remove = gpm_ids_remove
        self._gpm_ids_info.update(gpm_ids_info)
        
        self._cpm_logging_start()
        update_supported = self._runtime_env.support_update_connection_info(self.dictionary)

        if do_update_servers and not do_update_packages:
            if update_supported['supported']:
                self._connect_update_start(servers)
            elif update_supported['supported_by_installation']:
                self._connect_update_gui_handler = CPMSessionConnectUpdateGUIHandler(self, self.async_service, self.checkpoint_handler, self._user_interface, self.dictionary)
                self._connect_update_gui_handler.start(do_update_servers, servers, True, gpm_ids_install, gpm_ids_update, gpm_ids_remove)
            else:
                self._connect_update_gui_handler = CPMSessionConnectUpdateServersGUIHandler(self, self.async_service, self.checkpoint_handler, self._user_interface, self.dictionary)
                self._connect_update_gui_handler.start(error_message=update_supported['message'])
        else:
            used_is_security_update = is_security_update or do_update_servers
            self._connect_update_gui_handler = CPMSessionConnectUpdateGUIHandler(self, self.async_service, self.checkpoint_handler, self._user_interface, self.dictionary)
            self._connect_update_gui_handler.start(do_update_servers, servers, used_is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove)


    def _connect_update_start(self, servers):
        error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        (knownsecret, _) = self._runtime_env.get_connection_info()
        self._runtime_env.update_connection_info(knownsecret, servers, error_handler)

        self._connect_update_gui_handler = CPMSessionConnectUpdateServersGUIHandler(self, self.async_service, self.checkpoint_handler, self._user_interface, self.dictionary)
        if not error_handler.ok():
            self.tunnelendpoint_remote('remote_connect_update_response', rc=False, message=error_handler.dump_as_string())
            self._connect_update_gui_handler.start(error_message=error_handler.dump_as_string())
        else:
            self._connect_update_gui_handler.start()
        
    def _connect_update_callback_restart(self):
        self._connect_update_gui_handler = None
        self._cb.cpm_cb_restart(None)

    def _connect_update_callback_stop(self):
        self._connect_update_gui_handler = None
        self.tunnelendpoint_remote('remote_connect_update_response', rc=False, message="Canceled by user")
    
    def _connect_update_callback_cancel(self):
        self._connect_update_gui_handler = None
        self.tunnelendpoint_remote('remote_connect_update_response', rc=True, message="Canceled by user")
    
    def _connect_update_callback_update(self, do_update_servers, servers, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        self._connect_update_gui_handler = None
        self.tunnelendpoint_remote('remote_connect_update_start_update')

        self._mode = CPMSessionBase.MODE_UPDATE
        self._state = CPMSessionBase.STATE_ANALYZE
        
        if do_update_servers:
            self._servers = servers
        
        if self._user_interface is not None:
            if not self._gui_initialized:
                self._gui_init()
                self._user_interface.update.view.display()
            self._gui_update_state()
            self._user_interface.update.set_info_progress_complete(0)
        self.download_start(gpm_ids_install, gpm_ids_update)

    def installation_done(self):
        """
        Notify the server that the installation is done.
        """
        if self._user_interface is not None:
            self._user_interface.update.unsubscribe(self._gui_on_update)
        self.tunnelendpoint_remote('remote_installation_done_with_info', gpm_ids_install=self._gpm_ids_install, gpm_ids_update=self._gpm_ids_update, gpm_ids_remove=self._gpm_ids_remove)


    #
    # Methods for handling download state
    #
    def download_start(self, gpm_ids_install, gpm_ids_update):
        if self._state != CPMSessionBase.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::download_start.invalid_state", module_id, checkpoint.ERROR)
            return

        with self.checkpoint_handler.CheckpointScope("CPMSession::download_start", module_id, checkpoint.DEBUG):
            self._state = CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET
            gpm_ids = gpm_ids_install | set(i[1] for i in gpm_ids_update)
            self.tunnelendpoint_remote('remote_download_start',  gpm_ids=gpm_ids)

            if self._user_interface != None:
                self._gui_update_state()

    def remote_download_start_response(self, download_tunnel_id):
        if self._state not in [CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET]:
            return
        self._state = CPMSessionBase.STATE_DOWNLOAD
        if self._user_interface != None:
            self._gui_update_state()
            
        self._download_tunnel_id = download_tunnel_id
        self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, download_tunnel_id)
        self._downloader = cpm_file_downloader.CPMFileDownloader(self.async_service, self.checkpoint_handler, self._download_tunnelendpoint, download_tunnel_id, self._runtime_env.get_download_root(), self)
        self._downloader.start()

    def download_cancel_wait_for_download_ticket(self):
        if self._state not in [CPMSessionBase.STATE_DOWNLOAD_WAIT_FOR_TICKET]:
            self.checkpoint_handler.Checkpoint("CPMSession::download_cancel_wait_for_download_ticket.invalid_state", module_id, checkpoint.ERROR)
            return
        self.tunnelendpoint_remote('remote_download_cancel_wait_for_ticket')
        self._state = CPMSessionBase.STATE_ANALYZE
        if self._user_interface != None:
            self._gui_update_state()

    def _download_cleanup(self):
        if self._download_tunnel_id != None:
            self.remove_tunnelendpoint_tunnel(self._download_tunnel_id)
            self._downloader = None
            self._download_tunnel_id = None

    def download_cancel(self):
        if self._state not in [CPMSessionBase.STATE_DOWNLOAD]:
            self.checkpoint_handler.Checkpoint("CPMSession::download_cancel.invalid_state", module_id, checkpoint.ERROR)
            return
        self._downloader.cancel()

    def file_download_all_start(self, download_id, number_of_files, tick_value_max_total):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_all_start", module_id, checkpoint.DEBUG, number_of_files=number_of_files):
            if self._user_interface != None:
                self._gui_tick_value_max = tick_value_max_total
                self._gui_tick_value = 0
                self._gui_tick_last = 0
                self._gui_filecounter = 0
                self._gui_filecounter_max = number_of_files
                self._download_all_start_time = datetime.datetime.now()
                self._user_interface.update.set_info_progress_complete(0)
                self._user_interface.update.set_info_progress_subtext(self._gui_calculate_eta())

    def file_download_start(self, download_id, filename, tick_value_max):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_start", module_id, checkpoint.DEBUG, filename=filename):
            if self._user_interface != None:
                self._gui_filecounter += 1 
                self._user_interface.update.set_info_progress_subtext(self._gui_calculate_eta())

    def file_download_tick(self, download_id, tick_value):
        if self._user_interface != None:
            self._gui_tick_value += tick_value
            tick = (self._gui_tick_value * 100) / self._gui_tick_value_max
            if tick != self._gui_tick_last:
                self._gui_tick_last = tick
                self._user_interface.update.set_info_progress_subtext(self._gui_calculate_eta())
                self._user_interface.update.set_info_progress_complete(tick)
    
    def file_download_done(self, download_id):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_done", module_id, checkpoint.DEBUG):
            pass
        
    def file_download_verifying(self, download_id):
        if self._user_interface != None:
            self._user_interface.update.set_info_progress_subtext(self.dictionary._('Verifying downloaded packages'))
    
    def file_download_all_done(self, download_id):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_all_done", module_id, checkpoint.DEBUG):
            self._state = CPMSessionBase.STATE_DOWNLOAD_DONE
            if self._user_interface != None:
                self._user_interface.update.set_info_progress_complete(100)
            self._download_cleanup()
            self.install_start(self._gpm_ids_install, self._gpm_ids_update, self._gpm_ids_remove, self._knownsecret, self._servers)
 
    def file_download_error(self, download_id, message, tell_remote=True):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_error", module_id, checkpoint.ERROR, message=message):
            self._error_state_start(message)

    def file_download_canceling(self, download_id):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_canceling", module_id, checkpoint.DEBUG):
            self._state = CPMSessionBase.STATE_DOWNLOAD_CANCELING
            if self._user_interface != None:
                self._gui_update_state()
    
    def file_download_canceled(self, download_id):
        with self.checkpoint_handler.CheckpointScope("CPMSession::file_download_canceled", module_id, checkpoint.DEBUG):
            self._state = CPMSessionBase.STATE_ANALYZE
            self._download_cleanup()
            if self._user_interface != None:
                self._gui_update_state()

    def _gui_calculate_eta(self):
        time_duration = (datetime.datetime.now() - self._download_all_start_time)
        if self._gui_tick_last > 2:
            time_eta = time_duration // self._gui_tick_last * (100 - self._gui_tick_last)
            return self.dictionary._("Estimated remaining time %s") % time.strftime("%H:%M:%S", time.gmtime(time_eta.seconds))
        return "Estimated remaining time unknown"

    def _handle_error_state(self, message):
        with self.checkpoint_handler.CheckpointScope("CPMSession::_handle_error_state", module_id, checkpoint.ERROR, message=message):
            self.tunnelendpoint_remote('remote_handle_error_state', message)
            self._error_state_start(message)

    def close(self):
        self.tunnelendpoint_remote('remote_closed')
        CPMSessionBase.close(self)



class CPMSessionOnlineVersionUpgrade(CPMSessionOnline):
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, dictionary, runtime_env, cb = None):
        CPMSessionOnline.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_interface, dictionary, runtime_env, cb)

    def start_version_update(self):
        self.tunnelendpoint_remote('remote_start_version_update')



class CPMSessionOffline(CPMSessionBase):
    def __init__(self, async_service, checkpoint_handler, runtime_env, user_interface, dictionary, gon_client_install_state, cb = None):
        CPMSessionBase.__init__(self, async_service, checkpoint_handler, user_interface, dictionary, runtime_env, cb)
        self._state = CPMSessionOffline.STATE_RESTART_DONE
        self._mode = CPMSessionBase.MODE_UPDATE
        self.gon_client_install_state = gon_client_install_state
        self._parse_install_state(self.gon_client_install_state)

    def _reset_state(self):
        self._reset()
        self._state = CPMSessionBase.STATE_READY
        self._mode = CPMSessionBase.MODE_UPDATE
        self._gpm_ids_install = set()
        self._gpm_ids_update = set()
        self._gpm_ids_remove = set()
        self._gpm_ids_info = {}
        self._knownsecret = None
        self._servers = None
        self.gon_client_install_state = None

    def _parse_install_state(self, gon_client_install_state):
        cpm_session_data = gon_client_install_state.get_cpm_session_data()
        if cpm_session_data.has_key('gpm_ids_install'):
            self._gpm_ids_install = cpm_session_data['gpm_ids_install']
        if cpm_session_data.has_key('gpm_ids_update'):
            self._gpm_ids_update = cpm_session_data['gpm_ids_update']
        if cpm_session_data.has_key('gpm_ids_remove'):
            self._gpm_ids_remove = cpm_session_data['gpm_ids_remove']
        if cpm_session_data.has_key('gpm_ids_info'):
            self._gpm_ids_info = cpm_session_data['gpm_ids_info']
        if cpm_session_data.has_key('mode'):
            self._mode = cpm_session_data['mode']
        if cpm_session_data.has_key('cpm_logfilename'):
            self.cpm_logfilename = cpm_session_data['cpm_logfilename']
        if cpm_session_data.has_key('knownsecret'):
            self._knownsecret = cpm_session_data['knownsecret']
        if cpm_session_data.has_key('servers'):
            self._servers = cpm_session_data['servers']
        
    def _kill_othes_gon_clients(self):
        # Only done on windows because of problems with long running request for windows-update status
        try:
            if sys.platform == 'win32':
                own_process_id = os.getpid()
                self.checkpoint_handler.Checkpoint("CPMSessionOffline::_kill_othes_gon_clients", module_id, checkpoint.DEBUG, own_process_id=own_process_id)
                subprocess.call('taskkill /f /fi "PID ne %d" /im gon_client.exe' % own_process_id, shell=True)
                self.checkpoint_handler.Checkpoint("CPMSessionOffline::_kill_othes_gon_clients.done", module_id, checkpoint.DEBUG)
        except:
            self.checkpoint_handler.CheckpointExceptionCurrent("CPMSessionOffline::_kill_othes_gon_clients.done", module_id, cp_type=checkpoint.WARNING)
        
    def install_start_offline(self):
        self._cpm_logging_restart()
        self._kill_othes_gon_clients()
        with self.checkpoint_handler.CheckpointScope("CPMSessionOffline::install_start_offline", module_id, checkpoint.DEBUG, version=lib.version.Version.create_current().get_version_string(), cpm_session_data=str(self.gon_client_install_state.get_cpm_session_data()), plugin_data=str(self.gon_client_install_state.get_plugin_data())):
            self.install_start(self._gpm_ids_install, self._gpm_ids_update, self._gpm_ids_remove, self._knownsecret, self._servers)
