"""
Runtime client functionality for handling cpm-endpoint-manager
"""
import StringIO
       
import components.cpm.common
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base 
import lib.checkpoint
import lib.gpm



MODULE_ID = "CPMEndpointManager"

class CPMEndpointManager(tunnel_endpoint_base.TunnelendpointSession):
    def __init__(self, async_service, checkpoint_handler, dictionary, tunnel_endpoint_tunnel, runtime_env):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self.dictionary = dictionary
        self.runtime_env = runtime_env

    def session_start_and_connected(self):
        pass
    
    def remote_query_installed_meta(self):
        with self.checkpoint_handler.CheckpointScope("remote_query_installed_meta", MODULE_ID, lib.checkpoint.DEBUG):
            meta_strings = []
            error_handler = lib.gpm.gpm_env.GpmErrorHandler()
            metas = self.runtime_env.get_installed_gpm_meta_all(error_handler)
            if not error_handler.ok():
                self.checkpoint_handler.CheckpointMultilineMessage('remote_query_installed_meta.gpm_meta.error', MODULE_ID, lib.checkpoint.WARNING, error_handler.dump_as_string())
                self.tunnelendpoint_remote('remote_query_installed_meta_response', rc=(False, error_handler.dump_as_string()), meta_strings=meta_strings)
                return
            for meta in metas:
                meta.files_ro = None
                meta.files_rw = None
                meta_file = StringIO.StringIO()
                meta.to_tree().write(meta_file)
                meta_strings.append(meta_file.getvalue())
                meta_file.close()
            self.tunnelendpoint_remote('remote_query_installed_meta_response', rc=(True, 'Ok'), meta_strings=meta_strings)

    def remote_query_servers_checksum(self):
        with self.checkpoint_handler.CheckpointScope("remote_query_servers_checksum", MODULE_ID, lib.checkpoint.DEBUG):
            servers_checksum = None
            try:
                (_, servers) = self.runtime_env.get_connection_info()
                servers_checksum = components.cpm.common.calc_servers_checksum(servers)
            except:
                self.checkpoint_handler.CheckpointExceptionCurrent("remote_query_servers_checksum.unexpected_exc", MODULE_ID)
            self.tunnelendpoint_remote_decoupled(self.async_service, 'remote_query_servers_checksum_response', servers_checksum=servers_checksum)
    
    def remote_set_offline_credential_timeout_min(self, timeout_min):
        """
        Not use in standard clients in 5.5.0. Is for iOS clients.
        """
        pass
    
    def remote_set_background_settings(self, close_when_entering_background):
        """
        Not use in standard clients in 5.5.0. Is for iOS clients.
        """
        pass
        