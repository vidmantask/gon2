"""
Runtime client functionality for handling download of a file
"""
from __future__ import with_statement

import sys
import os
import os.path
import datetime
import time
import string

import lib.utility

from lib import checkpoint 
from components.communication import tunnel_endpoint_base

from components.cpm import * 



class CPMFileDownloaderCB(object):
    """
    Callback interface for downloader
    """
    def file_download_all_start(self, download_id, number_of_files, tick_value_max_total):
        pass

    def file_download_start(self, download_id, filename, tick_value_max):
        pass

    def file_download_tick(self, download_id, tick_value):
        pass
    
    def file_download_done(self, download_id):
        pass

    def file_download_verifying(self, download_id):
        pass

    def file_download_all_done(self, download_id):
        pass

    def file_download_error(self, download_id, message, tell_remote=True):
        pass
    
    def file_download_canceling(self, download_id):
        pass

    def file_download_canceled(self, download_id):
        pass


class CPMFileDownloader(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    STATE_READY = 0
    STATE_DOWNLOADING = 1
    STATE_VERIFYING = 2
    STATE_CANCELING = 3
    STATE_DONE = 4
    STATE_ERROR = 5

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, download_id, download_root, cb):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self._state = CPMFileDownloader.STATE_READY
        self.download_id = download_id
        self.download_root = download_root
        self.cb = cb
        self.download_filenames_abs_downloaded = []

        self._file = None
        self._filename = None
        self._filename_abs = None
        self._filesize = None
        self._filesize_recived = None

    def start(self):
        """
        Start the download
        """
        if self._state != CPMFileDownloader.STATE_READY:
            self._error('Invalid state for start')
            return
        self.tunnelendpoint_remote('remote_start')
        self._dl_start()

    def _dl_start(self):
        if self._state != CPMFileDownloader.STATE_READY:
            self._error('Invalid state for _dl_start')
            return
        self._state = CPMFileDownloader.STATE_DOWNLOADING

    def remote_dl_start_all(self, total_filecount, total_filesize):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING, CPMFileDownloader.STATE_CANCELING]:
            self._error('Invalid state for remote_dl_start_all')
            return
        if self._state in [CPMFileDownloader.STATE_CANCELING]:
            return

        self._total_filecount = total_filecount
        self._total_filesize = total_filesize
        self.cb.file_download_all_start(self.download_id, self._total_filecount, self._total_filesize)

        
    def remote_dl_start(self, filename, filesize):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING, CPMFileDownloader.STATE_CANCELING, CPMFileDownloader.STATE_ERROR]:
            self._error('Invalid state for remote_dl_start')
            return
        if self._state in [CPMFileDownloader.STATE_CANCELING, CPMFileDownloader.STATE_ERROR]:
            return
        
        self._filename = filename
        self._filename_abs = os.path.join(self.download_root, self._filename)
        self._filesize = filesize
        self._filesize_recived = 0
        self.cb.file_download_start(self.download_id, self._filename, self._filesize)
        try:
            foldername_abs = os.path.dirname(self._filename_abs)
            if not os.path.exists(foldername_abs):
                os.makedirs(foldername_abs)
            self._file = open(self._filename_abs, 'wb')
        except IOError:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("CPMFileDownloader.remote_dl_start.error", module_id, checkpoint.ERROR, etype, evalue, etrace)
            self._error("Unable to download file. %s" % (evalue))
    
    def remote_dl_chunk(self, chunk):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING, CPMFileDownloader.STATE_CANCELING, CPMFileDownloader.STATE_ERROR]:
            self._error('Invalid state for remote_dl_chunk')
            return
        if self._state in [CPMFileDownloader.STATE_CANCELING, CPMFileDownloader.STATE_ERROR]:
            return
        if self._file != None:
            try:
                self.tc_report_push_buffer_size(len(chunk))
                self._file.write(chunk)
                self.tc_report_pop_buffer_size(len(chunk))
            except IOError:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("CPMFileDownloader.remote_dl_chunk.error", module_id, checkpoint.ERROR, etype, evalue, etrace)
                self._error("Unable to download file. %s" % (evalue))
                return
            
            self.cb.file_download_tick(self.download_id, len(chunk))
        else:
            self._error('Error while handling remote_chunk, invalid file handler')
            
    def remote_dl_done(self):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING, CPMFileDownloader.STATE_CANCELING, CPMFileDownloader.STATE_ERROR]:
            self._error('Invalid state for remote_dl_done')
            return
        
        if self._state in [CPMFileDownloader.STATE_ERROR, CPMFileDownloader.STATE_CANCELING]:
            return

        if self._file != None:
            try:
                self._file.close()
            except IOError:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("CPMFileDownloader.remote_dl_start.error", module_id, checkpoint.ERROR, etype, evalue, etrace)
                self._error("Unable to download file. %s" % (evalue))
                return
            
            self.download_filenames_abs_downloaded.append( (self._filename, self._filename_abs) )
            self.cb.file_download_done(self.download_id)
        else:
            self._error('Error while handling remote_dl_done, invalid file handler')
            
    def remote_verify_start(self):
        if not self._state in [CPMFileDownloader.STATE_DOWNLOADING, CPMFileDownloader.STATE_ERROR]: 
            self._error('Invalid state for remote_verify_start')
            return

        if self._state in [CPMFileDownloader.STATE_ERROR]:
            return

        self._state = CPMFileDownloader.STATE_VERIFYING

        self.cb.file_download_verifying(self.download_id)

        verify_data = {}
        for (filename, filename_abs) in self.download_filenames_abs_downloaded:
            if os.path.exists(filename_abs):
                verify_size = os.path.getsize(filename_abs)
                verify_checksum = lib.utility.calculate_checksum_for_file(filename_abs)
                verify_data[filename] = (verify_size, verify_checksum)
            else:
                self._error('Error while verifying checksum, file not found')
                return
        self.tunnelendpoint_remote('remote_verify', verify_data=verify_data)

    def remote_all_done(self):
        if self._state not in [CPMFileDownloader.STATE_VERIFYING]:
            self._error('Invalid state for remote_dl_all_done')
            return
        self._state = CPMFileDownloader.STATE_READY
        self.cb.file_download_all_done(self.download_id)

    def remote_error(self, message):
        self._state = CPMFileDownloader.STATE_ERROR
        self._cleanup()
        self.cb.file_download_error(self.download_id, 'Remote error %s' % message, False)
    
    def cancel(self):
        """
        Cancel the download in a controlled manner
        """
        self.cb.file_download_canceling(self.download_id)
        self._state = CPMFileDownloader.STATE_CANCELING
#        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, '_cancel_decupled')
#
#    def _cancel_decupled(self, dummy_args):
        self.tunnelendpoint_remote_decoupled(self.async_service, 'remote_cancel')

    def remote_cancel_response(self):
        self._state = CPMFileDownloader.STATE_READY
        self._cleanup()
        self.cb.file_download_canceled(self.download_id)
 
    def _error(self, message):
        self._state = CPMFileDownloader.STATE_ERROR
        self.tunnelendpoint_remote('remote_error', message=message)
        self._cleanup()
        self.cb.file_download_error(self.download_id, message)

    def _cleanup(self):
        if self._file != None and not self._file.closed:
            self._file.close()
        try:
            if self._filename_abs != None and os.path.exists(self._filename_abs):
                os.remove(self._filename_abs)
        except:
            self.cb.file_download_error(self.download_id, "Unable to remove file '%s'" % self._filename_abs)
        
        self._file = None
        self._filename = None
        self._filename_abs = None
        self._filesize = None
        self._filesize_recived = None
        
