"""
"""
       
import components.cpm.common
import components.communication.tunnel_endpoint_base as tunnel_endpoint_base 
import lib.checkpoint
import lib.gpm.gpm_env
import lib.gpm.gpm_analyzer

MODULE_ID = "CPMEndpointManager"



class CPMRepository(object):
    def __init__(self, checkpoint_handler, gpms_root):
        self.checkpoint_handler = checkpoint_handler
        self.gpms_root = gpms_root
        
    def get_instance(self, old_instance=None):
        if old_instance is not None and not old_instance.has_cache_changed(self.gpms_root):
            return old_instance
        return self._create_instance()
 
    def _create_instance(self):
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reporitory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reporitory.init_using_cache(gpm_error_handler, self.gpms_root, self.gpms_root)
        return gpm_reporitory
 


class CPMEndpointManagerCallback(object):
    def cpm_endpoint_manager_connect_update(self, do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        pass



class CPMEndpointManager(tunnel_endpoint_base.TunnelendpointSession):
    STATE_INIT = 0
    STATE_CONNECT_CHECK_WAIT = 1
    STATE_CONNECT_CHECK_FAILED = 2
    STATE_READY = 3
    
    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, server_config, callback, gpms_root):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self.server_config = server_config
        self.callback = callback
        self.gpms_root = gpms_root
        self._state = CPMEndpointManager.STATE_INIT
        self._state_got_meta = False
        self._state_got_servers_checksum = False
        self._meta_rc = None
        self._meta_strings = None
        self._servers_checksum = None
        self._repository = None
       
    def session_start_and_connected(self):
        self.start_connect_check()
        
    def session_close(self):
        self.async_service = None
        self.server_config = None
        self.callback = None
        self.gpms_root = None
        self._meta_rc = None
        self._meta_strings = None
        self._servers_checksum = None
        self._repository = None
        
    
    def get_repository(self):
        self._repository = CPMRepository(self.checkpoint_handler, self.gpms_root).get_instance(self._repository)
        return self._repository
    
    def start_connect_check(self):
        self._state = CPMEndpointManager.STATE_CONNECT_CHECK_WAIT

        self._start_connect_check_version_check()
        if self._state == CPMEndpointManager.STATE_CONNECT_CHECK_FAILED:
            return
        
        self._start_connect_check_update_check()
        self._start_send_offline_settings()
        self._start_send_background_settings()
        
    def _start_connect_check_version_check(self):
        version = self.get_version()
        version_remote = self.get_version_remote()

        #
        # Check for major version number dismatch
        #
        if version_remote.get_version_major() < version.get_version_major():
            error_message = 'Backward compability check failed, major version different, local version is %s and remote version is %s.' %(version.get_version_string(), version_remote.get_version_string())
            self.trick_check_version_failed(error_message)
            self._state = CPMEndpointManager.STATE_CONNECT_CHECK_FAILED
            return
        
        #
        # Version check for 5.4 is implemented in cpm_session, because this module is not available in 5.4, and 
        # thus is never connected to remote.
        #
        VERSION_5_5 = lib.version.Version.create(5,5) 
        if (not version.less(VERSION_5_5) and version_remote.less(VERSION_5_5)):
            self.checkpoint_handler.Checkpoint("version_check.failed", MODULE_ID, lib.checkpoint.WARNING)
            error_message = 'Backward compability check failed local version is %s and remote version is %s.' %(version.get_version_string(), version_remote.get_version_string())
            self.trick_check_version_failed(error_message)
            self._state = CPMEndpointManager.STATE_CONNECT_CHECK_FAILED
            return

    def _start_connect_check_update_check(self):
        self.tunnelendpoint_remote('remote_query_installed_meta')
        self.tunnelendpoint_remote('remote_query_servers_checksum')
    
    def remote_query_installed_meta_response(self, rc, meta_strings):
        self._state_got_meta = True
        self._meta_rc = rc
        self._meta_strings = meta_strings
        self._connect_check()

    def remote_query_servers_checksum_response(self, servers_checksum):
        self._state_got_servers_checksum = True
        self._servers_checksum = servers_checksum
        self._connect_check()
        
    def _connect_check(self):
        if not (self._state_got_meta and self._state_got_servers_checksum):
            return
        (do_update_servers, servers) = self._connect_check_servers_checksum()
        (do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove) = self._connect_check_meta()
        if do_update_servers or do_update_packages:
            self.callback.cpm_endpoint_manager_connect_update(do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove)
            return
        self._state = CPMEndpointManager.STATE_READY
        
    def _connect_check_servers_checksum(self):
        servers = self.server_config.get_client_servers(self.checkpoint_handler)
        servers_checksum = components.cpm.common.calc_servers_checksum(servers) 

        if self._servers_checksum is not None and servers_checksum.lower() != self._servers_checksum.lower():
            return (True, servers)
        return (False, "")
    
    def _connect_check_meta(self):
        repository = self.get_repository()
        gpm_ids_installed = set()
        for client_meta in self._meta_strings:
            file_package_id = repository.add_meta_data_from_string(client_meta)
            if file_package_id != None:
                gpm_ids_installed.add(file_package_id)
                
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        repository.build_dependencies(gpm_error_handler)
        if gpm_error_handler.error():
            self.checkpoint_handler.CheckpointMultilineMessage("connect_check_meta.error", MODULE_ID, lib.checkpoint.ERROR, gpm_error_handler.dump_as_string())
            self._state = CPMEndpointManager.STATE_CONNECT_CHECK_FAILED
            return 
            
        (gpm_ids_install, gpm_ids_update, gpm_ids_remove) = repository.solve(gpm_ids_installed,  set(), set(), set(), solve_for_update_connect=True)
        do_update_packages = len(gpm_ids_install)+len(gpm_ids_update)+len(gpm_ids_remove) > 0
        is_security_update = repository.query_is_security_update(gpm_ids_update)
        return (do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove)

    def connect_update_done(self):
        self._state = CPMEndpointManager.STATE_READY

    def connect_update_done_but_failed(self):
        self._state = CPMEndpointManager.STATE_CONNECT_CHECK_FAILED

    def connect_check_ok(self):
        return self._state in [CPMEndpointManager.STATE_READY]

    def connect_check_failed(self):
        return self._state in [CPMEndpointManager.STATE_CONNECT_CHECK_FAILED]

    def _start_send_offline_settings(self):
        self.tunnelendpoint_remote('remote_set_offline_credential_timeout_min', timeout_min=self.server_config.session_offline_credential_timeout_min)
        
    def _start_send_background_settings(self):
        self.tunnelendpoint_remote('remote_set_background_settings', close_when_entering_background=self.server_config.session_close_when_entering_background)
        

