"""
Runtime server functionality for handling download of a file
"""
from __future__ import with_statement

import os
import os.path
import copy

import lib.utility

from lib import checkpoint 
from components.communication import tunnel_endpoint_base

from components.cpm import * 



class CPMFileDownloaderCB(object):
    """
    Callback interface for downloader
    """
    def file_download_all_done(self, download_id):
        pass

    def file_download_cancled(self, download_id):
        pass

    def file_download_error(self, download_id, message, tell_remote=True):
        pass
    
    def file_download_ticket_expired(self, download_id):
        pass
    

class CPMFileDownloader(tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch):
    STATE_READY = 0
    STATE_DOWNLOADING = 1
    STATE_VERIFYING = 2
    STATE_DONE = 3
    STATE_ERROR = 4

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, download_ticket, download_id, cb, download_manager, download_root, download_filenames_relative, download_filenames_relative_client=None):
        tunnel_endpoint_base.TunnelendpointTunnelBaseWithMessageDispatch.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.async_service = async_service
        self.download_manager = download_manager
        self._state = CPMFileDownloader.STATE_READY
        self.download_ticket = download_ticket
        self.download_id = download_id
        self.cb = cb
        self._file = None
        self.download_root = download_root
        self.download_filenames_relative = download_filenames_relative
        self.download_filenames_downloaded = {}

        self.download_filenames_relative_client = download_filenames_relative_client
        if self.download_filenames_relative_client is None:
            self.download_filenames_relative_client = copy.copy(download_filenames_relative)


    def tunnelendpoint_connected(self):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader.tunnelendpoint_connected", module_id, checkpoint.DEBUG, state=self._state)
    
    def remote_start(self):
        """
        Start the download
        """
        if self._state != CPMFileDownloader.STATE_READY:
            self._error('Invalid state for remote_start')
            return

        total_filesize = 0L
        for filename_relative in self.download_filenames_relative:
            filename_abs = os.path.join(self.download_root, filename_relative)
            if os.path.exists(filename_abs):
                total_filesize += os.path.getsize(filename_abs)
        self.tunnelendpoint_remote('remote_dl_start_all', total_filecount=len(self.download_filenames_relative), total_filesize=total_filesize)
        self._dl_start()


    def _dl_start(self):
        if not self._state in [CPMFileDownloader.STATE_READY, CPMFileDownloader.STATE_DOWNLOADING]:
            self._error('Invalid state for _dl_start')
            return

        if self._state in [CPMFileDownloader.STATE_ERROR]:
            return 

        self._state = CPMFileDownloader.STATE_DOWNLOADING
        if len(self.download_filenames_relative) == 0:
            self._verify_start()
            return

        download_filename_relative = self.download_filenames_relative[0]
        download_filename_relative_client = self.download_filenames_relative_client[0]
        download_filename_abs = os.path.join(self.download_root, download_filename_relative)
        self.checkpoint_handler.Checkpoint("CPMFileDownloader._dl_start", module_id, checkpoint.DEBUG, download_filename_abs=download_filename_abs, download_filename_relative=download_filename_relative, download_root=self.download_root)
        if not os.path.exists(download_filename_abs):
            self._error('File to download not found: %s' % download_filename_abs)
            return
        
        filesize = os.path.getsize(download_filename_abs)
        self.download_filenames_downloaded[download_filename_relative_client] = (download_filename_abs, filesize)

        self._file = file(download_filename_abs, 'rb')
        self.tunnelendpoint_remote('remote_dl_start', filename=download_filename_relative_client, filesize=filesize)
        self._dl_chunk_start()

    def _dl_chunk_start(self):
        wait_ms = self.get_tc_read_delay_ms()
        chunk_size = self.get_tc_read_size()
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, wait_ms, self, '_dl_chunk', chunk_size)        

    def _dl_chunk(self, chunk_size=15000):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING]:
            return
        
        if not self.is_connected():
            self.checkpoint_handler.Checkpoint("CPMFileDownloader._dl_chunk.not_connected", module_id, checkpoint.DEBUG)
            self._cleanup()
            return
        
        if self._file != None:
            confirmed = self.download_manager.confirm_ticket(self.download_ticket)
            if confirmed:
                self.tc_report_read_begin()
                chunk = self._file.read(chunk_size)
                chunk_size_read = len(chunk)
                
                if chunk != "":
                    self.tunnelendpoint_remote('remote_dl_chunk', chunk=chunk)
                    self._dl_chunk_start()
                else:
                    self._file.close()
                    self._file = None
                    self.tunnelendpoint_remote('remote_dl_done')
                    self.download_filenames_relative.pop(0)
                    self.download_filenames_relative_client.pop(0)
                    self._dl_start()
                self.tc_report_read_end(chunk_size_read)
            else:
                self.cb.file_download_ticket_expired(self.download_id)

    def renew_download_ticket(self, download_ticket):
        if self._state not in [CPMFileDownloader.STATE_DOWNLOADING]:
            return

        self.download_ticket = download_ticket
        self._dl_chunk_start()

    def _verify_start(self):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader._verify_start", module_id, checkpoint.DEBUG, state=self._state)
        if self._state != CPMFileDownloader.STATE_DOWNLOADING:
            self._error('Invalid state for _verify_start')
            return
        self.download_manager.release_ticket(self.download_ticket)
        self._state = CPMFileDownloader.STATE_VERIFYING
        self.tunnelendpoint_remote('remote_verify_start')

    def remote_verify(self, verify_data):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader.remote_verify", module_id, checkpoint.DEBUG, state=self._state)
        if self._state not in [CPMFileDownloader.STATE_VERIFYING]:
            self._error('Invalid state for remote_verify')
            return
        if not len(verify_data) == len(self.download_filenames_downloaded):
            self._error("unexpected number of files when validating checksum")
            return

        for verify_client_filname in verify_data.keys():
            (verify_client_size, verify_client_checksum) = verify_data[verify_client_filname] 
            if not self.download_filenames_downloaded.has_key(verify_client_filname):
                self._error("unexpected file '%s' when validating checksum" % verify_client_filname)
                return

            (verify_server_filename_abs, verify_server_size) = self.download_filenames_downloaded[verify_client_filname]
            if not verify_client_size == verify_server_size:
                self._error("invalid filesize found for the file '%s' when validating checksum" % verify_client_filname)
                return

            verify_server_checksum = lib.utility.calculate_checksum_for_file(verify_server_filename_abs)
            if not verify_client_checksum == verify_server_checksum:
                self._error("invalid checksum found for the file '%s'" % verify_client_filname)
                return

        self._state = CPMFileDownloader.STATE_READY
        self._all_dl_done()

    def _all_dl_done(self):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader._all_dl_done", module_id, checkpoint.DEBUG, state=self._state)
        self._state = CPMFileDownloader.STATE_DONE
        self.tunnelendpoint_remote('remote_all_done')
        self._cleanup()
        self.cb.file_download_all_done(self.download_id)

    def remote_error(self, message):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader.remote_error", module_id, checkpoint.DEBUG, state=self._state, message=message)
        self._state = CPMFileDownloader.STATE_ERROR
        self._cleanup()
        self.cb.file_download_error(self.download_id, 'Remote error %s' % message, False)
    
    def remote_cancel(self):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader.remote_cancel", module_id, checkpoint.DEBUG, state=self._state)
        self._state = CPMFileDownloader.STATE_READY
        self.tunnelendpoint_remote('remote_cancel_response')
        self._cleanup()
        self.cb.file_download_cancled(self.download_id)
 
    def _error(self, message):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader._error", module_id, checkpoint.DEBUG, state=self._state, message=message)
        self._state = CPMFileDownloader.STATE_ERROR
        self.tunnelendpoint_remote('remote_error', message=message)
        self._cleanup()
        self.cb.file_download_error(self.download_id, message)

    def _cleanup(self):
        self.checkpoint_handler.Checkpoint("CPMFileDownloader._cleanup", module_id, checkpoint.DEBUG, state=self._state)
        if self._file != None and not self._file.closed:
            self._file.close()
        self._file = None
        
    def close(self):
        """
        Hard close from cpm_session
        """
        self._cleanup()
