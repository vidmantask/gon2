"""
Package plugin for Gateway Server
"""
from __future__ import with_statement
import sys

import lib.checkpoint



from components.communication import message

from plugin_types.server_gateway import plugin_type_tag


module_id = 'cpm_package_static'


class PluginTag(plugin_type_tag.PluginTypeTag):
    """
    Gateway Server part of package plugin module.
    """
    TAG_package_check_package = plugin_type_tag.Tag('package', 'CheckPackage')
    TAG_package_package_id = plugin_type_tag.Tag('package', 'package_id')
    
    TAG_PACKAGE_CHECK = plugin_type_tag.Tag(None, 'PACKAGE_CHECK')
    TAG_PACKAGE_INSTALLED = plugin_type_tag.Tag(None, 'PACKAGE_INSTALLED')
    
    def __init__(self, async_service, checkpoint_handler, license_handler, session_info, database, management_message_session, access_log_server_session):
        plugin_type_tag.PluginTypeTag.__init__(self, async_service, checkpoint_handler, 'package', license_handler, session_info)

    def client_connected(self):
        pass

    def generate_tags(self, tags):
        generated_tags = []
        tag_package_check_package_list = PluginTag.TAG_package_check_package.in_tags_get(tags)
        for tag_package_check_package in tag_package_check_package_list:
            package_name = tag_package_check_package.tag_args[0]
            package_arch = tag_package_check_package.tag_args[1]
            generated_tags.extend(self._generate_tags_package(package_name, package_arch))
        return generated_tags

    def get_argument_taginfos(self):
        return []
        
    def _generate_tags_package(self, package_name, package_arch):
        generated_tags = []
        if self.cb is not None:
            cpm_session = self.cb.get_cpm_session() 
            if cpm_session is not None:
                generated_tags.append(PluginTag.TAG_PACKAGE_CHECK)
                (installed, package_id) = cpm_session.is_package_installed(package_name, package_arch, self.notify_tag_changed)
                if installed:
                    generated_tags.append(PluginTag.TAG_PACKAGE_INSTALLED)
                if package_id is not None:
                    tag = PluginTag.TAG_package_package_id
                    tag.tag_args = package_id
                    generated_tags.append(tag)
        return generated_tags


    @classmethod
    def check_for_missing_package(cls, tags_string):
        tags = plugin_type_tag.Tag.create_tag_list_from_string_set(tags_string)
        tag_package_check_list = PluginTag.TAG_PACKAGE_CHECK.in_tags_get(tags)
        if tag_package_check_list:
            
            # Tag TAG_PACKAGE_INSTALLED indicate says that the package is installed
            tag_package_installed_list = PluginTag.TAG_PACKAGE_INSTALLED.in_tags_get(tags)
            if tag_package_installed_list:
                return None

            # Tag TAG_package_package_id tells which package is missing
            # If this tag is not yet available then it will act as if it was installed 
            # because when auto launching the information is not yet available. 
            # This should be redesigned.
            tag_package_id_list = PluginTag.TAG_package_package_id.in_tags_get(tags)
            if tag_package_id_list:
                tag_package_id = tag_package_id_list[0]
                return tag_package_id.tag_args

        return None
