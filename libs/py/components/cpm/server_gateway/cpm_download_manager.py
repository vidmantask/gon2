"""
Runtime server download manager
"""
from __future__ import with_statement

import os
import os.path
import datetime
import time

from lib import checkpoint 
from components.communication import tunnel_endpoint_base

from components.cpm import * 


class DownloadManagerCB(object):

    def download_manager_ticket_granted_from_other_thread(self, download_ticket):
        pass

    def download_manager_ticket_timeout_from_other_thread(self, download_ticket):
        pass



class DownloadManager(object):
    STATE_RUNNING = 0
    STATE_CLOSING = 1
    STATE_CLOSED = 2
    
    def __init__(self, async_service, session_mutex, checkpoint_handler, tickets_max):
        self.checkpoint_handler = checkpoint_handler
        self.session_mutex = session_mutex
        self._async_service = async_service
        self._tickets_max = tickets_max
        self._ticket_id_next = 0
        self._tickets = {} 
        self._tickets_waiting = []
        self._tickets_deleted = []
        self._janitor_running = False
        self._ticket_timeout = datetime.timedelta(minutes=1)
        self._state = DownloadManager.STATE_RUNNING
        
    def request_ticket(self, cb):
        if self._state not in [DownloadManager.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("DownloadManager.request_ticket.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_request_ticket', cb)        
        self.checkpoint_handler.Checkpoint("DownloadManager.request_ticket", module_id, checkpoint.DEBUG)

    def _request_ticket(self, cb):
        if self._state not in [DownloadManager.STATE_RUNNING]:
            return
        self._tickets_waiting.append(cb)
        if not self._janitor_running:
            self._janitor_start()

    def request_ticket_cancel(self, cb):
        if self._state not in [DownloadManager.STATE_RUNNING, DownloadManager.STATE_CLOSING]:
            self.checkpoint_handler.Checkpoint("DownloadManager.request_ticket_cancel.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_request_ticket_cancel', cb)        

    def _request_ticket_cancel(self, cb):
        if self._state not in [DownloadManager.STATE_RUNNING, DownloadManager.STATE_CLOSING]:
            return
        self._tickets_waiting.remove(cb)

    def release_ticket(self, download_ticket):
        if self._state not in [DownloadManager.STATE_RUNNING, DownloadManager.STATE_CLOSING]:
            self.checkpoint_handler.Checkpoint("DownloadManager.release_ticket.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_release_ticket', download_ticket)        
        self.checkpoint_handler.Checkpoint("DownloadManager.release_ticket", module_id, checkpoint.DEBUG, download_ticket=download_ticket)

    def _release_ticket(self, download_ticket):
        if self._state not in [DownloadManager.STATE_RUNNING, DownloadManager.STATE_CLOSING]:
            return
        self._tickets_deleted.append(download_ticket)
        if not self._janitor_running:
            self._janitor_start()

    def confirm_ticket(self, download_ticket):
        if self._state not in [DownloadManager.STATE_RUNNING]:
            self.checkpoint_handler.Checkpoint("DownloadManager.confirm_ticket.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, 0, 0, self, '_confirm_ticket', download_ticket)        
        return self._tickets.has_key(download_ticket)
    
    def _confirm_ticket(self, download_ticket):
        if self._state not in [DownloadManager.STATE_RUNNING]:
            return
        if self._tickets.has_key(download_ticket):
            self._tickets[download_ticket]['last_confirmed'] = datetime.datetime.now()

    def _janitor_start(self, sleep_sec=3):
        self._janitor_running = True
        self._async_service.sleep_start_mutex(self.checkpoint_handler, self.session_mutex, 0, sleep_sec, 0, self, '_janitor')        
    
    def _janitor(self):
        self.checkpoint_handler.Checkpoint("DownloadManager._janitor", module_id, checkpoint.DEBUG, tickets=self._tickets.keys(), num_tickets_waiting=len(self._tickets_waiting), tickets_deleted=self._tickets_deleted)

        if self._state in [DownloadManager.STATE_CLOSED]:
            self._janitor_running = False
            return

        if self._state in [DownloadManager.STATE_CLOSING]:
            for ticket in self._tickets.keys():
                self._tickets[ticket]['cb'].download_manager_ticket_timeout_from_other_thread(ticket)
                self.release_ticket(ticket)
            self._janitor_running = False
            return
        
        #
        # Deletion of tickets has been moved to the janitor to be thread safe
        #
        for ticket in self._tickets_deleted:
            if self._tickets.has_key(ticket):
                del self._tickets[ticket]
        self._tickets_deleted = []
        
        #
        # Remove tickets that has timed out
        #
        for ticket in self._tickets.keys():
            if datetime.datetime.now() - self._tickets[ticket]['last_confirmed'] > self._ticket_timeout:
                self._tickets[ticket]['cb'].download_manager_ticket_timeout_from_other_thread(ticket)
                self.release_ticket(ticket)
        
        while len(self._tickets_waiting) > 0 and len(self._tickets) < self._tickets_max:
            cb = self._tickets_waiting.pop(0)
            download_ticket = self._ticket_id_next
            self._ticket_id_next += 1
            start_time = datetime.datetime.now()
            self._tickets[download_ticket] = {'cb':cb, 'start':start_time, 'last_confirmed':start_time}
            cb.download_manager_ticket_granted_from_other_thread(download_ticket)

        self._janitor_running = False
        if len(self._tickets_waiting) > 0 or len(self._tickets) > 0:
            self._janitor_start()

    def stop_and_wait(self):
        if self._state in [DownloadManager.STATE_CLOSED]:
            return
        self._state = DownloadManager.STATE_CLOSING

        self.checkpoint_handler.Checkpoint("DownloadManager.stop_and_wait", module_id, checkpoint.DEBUG, tickets=self._tickets.keys(), num_tickets_waiting=len(self._tickets_waiting), tickets_deleted=self._tickets_deleted)
        if not self._janitor_running:
            self._janitor_start(sleep_sec=0)

        while self._janitor_running:
            time.sleep(1)
            
        self._state = DownloadManager.STATE_CLOSED

    
    @classmethod
    def get_instance(cls):
        assert(global_download_manager != None)
        return global_download_manager
        
global_download_manager = None
