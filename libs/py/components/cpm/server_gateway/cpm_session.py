"""
Runtime server CPM(Client Package Management) session handling
"""
from __future__ import with_statement
from lib import checkpoint 
from components.communication import tunnel_endpoint_base

import tempfile
import os.path
import shutil

import lib.gpm.gpm_analyzer
import lib.gpm.gpm_env
import lib.gpm.gpm_builder

import lib.hagi.gon_hagi_authentication
import cpm_file_downloader

import components.traffic.client_gateway.client_launch_internal

from components.cpm import * 

import components.cpm.server_gateway.plugins
import cpm_endpoint_manager_session


class CPMRepository(object):
    """
    Helper class for handling the cpm repository
    """
    def __init__(self, checkpoint_handler, gpms_root):
        self.checkpoint_handler = checkpoint_handler
        self.gpms_root = gpms_root
        
    def get_instance(self, old_instance=None):
        if old_instance is not None and not old_instance.has_cache_changed(self.gpms_root):
            return old_instance
        return self._create_instance()
 
    def _create_instance(self):
        self.checkpoint_handler.Checkpoint("CPMRepository::loading_new_instance_of_cpm_repository", module_id, checkpoint.DEBUG)
        gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
        gpm_reporitory = lib.gpm.gpm_analyzer.GpmRepository()
        gpm_reporitory.init_using_cache(gpm_error_handler, self.gpms_root, self.gpms_root)
        if not gpm_error_handler.ok():
            cp_type = checkpoint.ERROR if gpm_error_handler.error() else checkpoint.WARNING
            self.checkpoint_handler.Checkpoint("CPMRepository::loading_new_instance_of_cpm_repository", module_id, cp_type, msg=gpm_error_handler.dump_as_string())
        
        return gpm_reporitory
       


class CPMSession(tunnel_endpoint_base.TunnelendpointSession, 
                 components.traffic.client_gateway.client_launch_internal.LaunchSession,
                 cpm_endpoint_manager_session.CPMEndpointManagerCallback):
    
    STATE_READY = 0
    STATE_WAITING_FOR_CLIENT = 1
    STATE_ANALYZE = 2
    STATE_DOWNLOAD = 3
    
    MODE_UPDATE = 0
    MODE_INSTALL = 1
    MODE_REMOVE = 2
    MODE_QUERY = 3

    def __init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel, user_session, gpms_folder, download_manager, server_knownsecret, server_config, access_log_server_session):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        components.traffic.client_gateway.client_launch_internal.LaunchSession.__init__(self)
        self.checkpoint_handler = checkpoint_handler
        self._gpms_root = gpms_folder
        self._download_manager = download_manager
        self._async_service = async_service
        self._server_knownsecret = server_knownsecret
        self.access_log_server_session = access_log_server_session
        self._reset_state()

        self.query_package_done = False
        self.query_package_installed = {}
        self.query_package_callback = None
        self._cpm_repository = CPMRepository(self.checkpoint_handler, self._gpms_root).get_instance()
        self._cpm_endpoint_manager_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, 1)
        self._cpm_endpoint_manager = cpm_endpoint_manager_session.CPMEndpointManager(self.async_service, self.checkpoint_handler, self._cpm_endpoint_manager_tunnelendpoint, server_config, self, gpms_folder)
        self._connect_check_failed_signaled = False
    
    def _reset_state(self):
        self._state = CPMSession.STATE_READY
        self._state_analyze = None
        self._gpm_ids_installed = set()
        self._gpm_ids_available = set()
        self._gpm_ids_updates = set()
        self._connect_update_waiting_to_finish = False
        
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        self._cpm_endpoint_manager.session_start()

    def _inform_access_log_do_update_servers(self):
        source = 'cpm'
        code   = 'cpm:0001'
        summary = 'Connection information need to be updated'
        details = ''
        self.access_log_server_session.report_access_log_gateway_info(source, code, summary, details)
        
    def _inform_access_log_do_update_packages(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        source = 'cpm'
        code = 'cpm:0002'
        summary = 'Connect update, packages need to be updated'
        details =  ''
        if len(gpm_ids_install) > 0:
            details += 'Packages to be installed:\n%s\n\n' % ', '.join(gpm_ids_install)
        if len(gpm_ids_update) > 0:
            details += 'Packages to be updated:\n'
            for (from_id, to_id) in gpm_ids_update: 
                details += '%s --> %s\n' % (from_id, to_id)
            details += '\n'
        if len(gpm_ids_remove) > 0:
            details += 'Packages to be removed:\n%s\n\n' % ', '.join(gpm_ids_remove)
        self.access_log_server_session.report_access_log_gateway_info(source, code, summary, details)

    def _inform_access_log_did_update_packages(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        source = 'cpm'
        code = 'cpm:0003'
        summary = 'Packages was updated'
        details =  ''
        if len(gpm_ids_install) > 0:
            details += 'Packages installed:\n%s\n\n' % ', '.join(gpm_ids_install)
        if len(gpm_ids_update) > 0:
            details += 'Packages updated:\n'
            for (from_id, to_id) in gpm_ids_update: 
                details += '%s --> %s\n' % (from_id, to_id)
            details += '\n'
        if len(gpm_ids_remove) > 0:
            details += 'Packages removed:\n%s\n\n' % ', '.join(gpm_ids_remove)
        self.access_log_server_session.report_access_log_gateway_info(source, code, summary, details)

    def cpm_endpoint_manager_connect_update(self, do_update_servers, servers, do_update_packages, is_security_update, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        self._connect_update_waiting_to_finish = True

        gpm_ids_info = self._build_gpm_ids_info_from_lists(gpm_ids_install, gpm_ids_update, gpm_ids_remove)
        
        self.tunnelendpoint_remote('remote_analyze_start_connect_update', do_update_servers=do_update_servers, servers=servers, do_update_packages=do_update_packages, is_security_update=is_security_update, gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove, gpm_ids_info=gpm_ids_info)
        if do_update_servers:
            self._inform_access_log_do_update_servers()
        if do_update_packages:
            self._inform_access_log_do_update_packages(gpm_ids_install, gpm_ids_update, gpm_ids_remove)
    
    def _build_gpm_ids_info_from_lists(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        gpm_ids = []
        gpm_ids.extend(gpm_ids_install)
        gpm_ids.extend(gpm_ids_remove)
        gpm_ids.extend(i[1] for i in gpm_ids_update)
        return self._build_gpm_ids_info(gpm_ids)

    def _build_gpm_ids_info(self, gpm_ids):
        gpm_ids_info = {}
        for gpm_id in gpm_ids:
            repository = self._get_repository()
            gpm_ids_info[gpm_id] = repository.get_info(gpm_id)
        return gpm_ids_info
    
    def remote_connect_update_response(self, rc, message):
        if not rc:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_connect_update_response", module_id, checkpoint.ERROR, rc=rc, message=message)
            self.access_log_server_session.report_access_log_gateway_error('cpm', "cpm:1000", 'Connect update, error', message)
            self._connect_update_waiting_to_finish = False
            self._cpm_endpoint_manager.connect_update_done_but_failed()
            return
        self.checkpoint_handler.Checkpoint("CPMSession::remote_connect_update_response.ok", module_id, checkpoint.DEBUG)
        self.access_log_server_session.report_access_log_gateway_info('cpm', "cpm:0004", "Connect update", message)
        self._connect_update_waiting_to_finish = False
        self._cpm_endpoint_manager.connect_update_done()

    def remote_connect_update_start_update(self):
        self._state = CPMSession.STATE_ANALYZE
        self._mode = CPMSession.MODE_UPDATE

    def connect_check_ok(self):
        VERSION_5_5 = lib.version.Version.create(5,5) 
        version = self.get_version()
        version_remote = self.get_version_remote()
        if (not version.less(VERSION_5_5) and version_remote.less(VERSION_5_5)):
            if not self._connect_check_failed_signaled:
                self.checkpoint_handler.Checkpoint("version_check.failed", module_id, checkpoint.WARNING)
                error_message = 'Backward compability check failed local version is %s and remote version is %s.' %(version.get_version_string(), version_remote.get_version_string())
                self.trick_check_version_failed(error_message)
                self._connect_check_failed_signaled = True
            return False
        return self._cpm_endpoint_manager.connect_check_ok()

    def connect_check_failed(self):
        return self._cpm_endpoint_manager.connect_check_failed()

    def session_close(self):
        self.query_package_callback = None
        if self._state == CPMSession.STATE_DOWNLOAD and self._downloader is not None:
            self._downloader.close()
        self._cpm_endpoint_manager.session_close()
        self._cpm_endpoint_manager = None
    
    def _get_repository(self):
        self._cpm_repository = CPMRepository(self.checkpoint_handler, self._gpms_root).get_instance(self._cpm_repository)
        return self._cpm_repository

    def analyze_ready(self):
        return self._state == CPMSession.STATE_READY

    def analyze_start_query(self):
        """
        Start a analyze session with the quest to find current installed packages.
        """
        if self._state not in [CPMSession.STATE_READY, CPMSession.STATE_WAITING_FOR_CLIENT, CPMSession.STATE_ANALYZE]:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_start_query.invalid_state", module_id, checkpoint.ERROR, state=self._state)
            return

        if self._state in [CPMSession.STATE_WAITING_FOR_CLIENT, CPMSession.STATE_ANALYZE]:
            return
        
        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_start_query", module_id, checkpoint.DEBUG):
            self._state = CPMSession.STATE_WAITING_FOR_CLIENT
            self._mode = CPMSession.MODE_QUERY
            self.tunnelendpoint_remote('remote_analyze_start_query')

    def analyze_start_update(self):
        """
        Start a analyze session with the quest to find current installed packages that need to be installed.
        """
        if self._state != CPMSession.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_start_update.invalid_state", module_id, checkpoint.ERROR)
            return
            
        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_start_update", module_id, checkpoint.DEBUG):
            self._state = CPMSession.STATE_WAITING_FOR_CLIENT
            self._mode = CPMSession.MODE_UPDATE
            self.tunnelendpoint_remote('remote_analyze_start_update')

    def remote_start_version_update(self):
        """
        Start a forzed version upgrade tricked by client
        """ 
        if self._state != CPMSession.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_start_version_update.invalid_state", module_id, checkpoint.ERROR)
            return
        
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_start_version_update", module_id, checkpoint.DEBUG):
            self._state = CPMSession.STATE_WAITING_FOR_CLIENT
            self._mode = CPMSession.MODE_UPDATE
            self.tunnelendpoint_remote('remote_analyze_start_update')

    def analyze_start_install(self):
        """
        Start a analyze session with the quest to install new packages.
        """
        if self._state != CPMSession.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_start_install.invalid_state", module_id, checkpoint.ERROR)
            return
            
        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_start_install", module_id, checkpoint.DEBUG):
            self._state = CPMSession.STATE_WAITING_FOR_CLIENT
            self._mode = CPMSession.MODE_INSTALL
            self.tunnelendpoint_remote('remote_analyze_start_install')

    def analyze_start_remove(self):
        """
        Start a analyze session with the quest to remove already insalled pacages.
        """
        if self._state != CPMSession.STATE_READY:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_start_remove.invalid_state", module_id, checkpoint.ERROR)
            return
            
        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_start_remove", module_id, checkpoint.DEBUG):
            self._state = CPMSession.STATE_WAITING_FOR_CLIENT
            self._mode = CPMSession.MODE_REMOVE
            self.tunnelendpoint_remote('remote_analyze_start_remove')

    def remote_analyze_start_response(self):
        """
        Response from client that is is ready play
        """
        if self._state != CPMSession.STATE_WAITING_FOR_CLIENT:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_start_response.invalid_state", module_id, checkpoint.ERROR)
            return
        self._state = CPMSession.STATE_ANALYZE
        self.tunnelendpoint_remote('remote_analyze_get_installed_meta')

    def remote_analyze_get_installed_meta_response(self, meta_strings):
        """
        Information about installed packages on the client. The client_meta are expected to b a list of gpm_spec's 
        """        
        if self._state != CPMSession.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_get_installed_meta_response.invalid_state", module_id, checkpoint.ERROR)
            return

        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_get_installed_meta_response", module_id, checkpoint.DEBUG):
            repository = self._get_repository()
            gpm_ids_installed = set()
            for client_meta in meta_strings:
                file_package_id = repository.add_meta_data_from_string(client_meta)
                if file_package_id != None:
                    gpm_ids_installed.add(file_package_id)
                
            gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
            repository.build_dependencies(gpm_error_handler)
            if gpm_error_handler.error():
                self.checkpoint_handler.CheckpointMultilineMessage("CPMSession::remote_get_installed_meta_response.error", module_id, checkpoint.ERROR, gpm_error_handler.dump_as_string())
                self._handle_error_state('Error building dependency, contact you administrator')
                return 

            gpm_ids_available = set()
            gpm_ids_install = set() 
            gpm_ids_update = set() 
            gpm_ids_remove = set() 
            
            if self._mode == CPMSession.MODE_UPDATE:
                (gpm_ids_install, gpm_ids_update, gpm_ids_remove) = self._get_repository().solve(gpm_ids_installed,  set(), set(), set(), solve_for_update=True)
                self._gpm_ids_available = gpm_ids_available
                self._gpm_ids_installed = gpm_ids_installed 
                self._gpm_ids_updates = gpm_ids_update
                self.tunnelendpoint_remote('remote_analyze_package_selection_start', gpm_ids_installed=gpm_ids_installed, gpm_ids_available=gpm_ids_available, gpm_ids_updates=gpm_ids_update, gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove)

            elif self._mode == CPMSession.MODE_INSTALL:
                gpm_ids_available = self._get_repository().get_ids_available_for_installation(gpm_ids_installed)
                self._gpm_ids_available = gpm_ids_available
                self._gpm_ids_installed = gpm_ids_installed 
                self._gpm_ids_updates = gpm_ids_update
                self.tunnelendpoint_remote('remote_analyze_package_selection_start', gpm_ids_installed=gpm_ids_installed, gpm_ids_available=gpm_ids_available, gpm_ids_updates=gpm_ids_update, gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove)
            
            elif self._mode == CPMSession.MODE_REMOVE:
                self._gpm_ids_available = gpm_ids_available
                self._gpm_ids_installed = gpm_ids_installed 
                self._gpm_ids_updates = gpm_ids_update
                self.tunnelendpoint_remote('remote_analyze_package_selection_start', gpm_ids_installed=gpm_ids_installed, gpm_ids_available=gpm_ids_available, gpm_ids_updates=gpm_ids_update, gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove)

            elif self._mode == CPMSession.MODE_QUERY:
                self._gpm_ids_installed = gpm_ids_installed 
                self._analyze_query()
                

    def analyze_update_package_selection(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        if self._state != CPMSession.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::analyze_update_package_selection.invalid_state", module_id, checkpoint.ERROR)
            return

        with self.checkpoint_handler.CheckpointScope("CPMSession::analyze_update_package_selection", module_id, checkpoint.DEBUG):
            (gpm_ids_install, gpm_ids_update, gpm_ids_remove) = self._get_repository().solve(self._gpm_ids_installed, gpm_ids_install, gpm_ids_update, gpm_ids_remove)
            self.tunnelendpoint_remote('remote_analyze_update_package_selection', gpm_ids_install=gpm_ids_install, gpm_ids_update=gpm_ids_update, gpm_ids_remove=gpm_ids_remove)

            
    def remote_analyze_update_package_selection(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        if self._state != CPMSession.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_update_package_selection.remote_update_package_selection", module_id, checkpoint.ERROR)
            return
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_analyze_update_package_selection", module_id, checkpoint.DEBUG):
            self.analyze_update_package_selection(gpm_ids_install, gpm_ids_update, gpm_ids_remove)


    def remote_analyze_get_info(self, gpm_ids):
        if self._state not in [CPMSession.STATE_ANALYZE]:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_analyze_get_info.invalid_state", module_id, checkpoint.ERROR)
            return
        
        gpm_ids_info = self._build_gpm_ids_info(gpm_ids)
        self.tunnelendpoint_remote('remote_analyze_get_info_response', gpm_ids_info=gpm_ids_info)

    #
    # Methods for _download_cleanuphandling download state
    #
    def remote_download_start(self, gpm_ids):
        if self._state != CPMSession.STATE_ANALYZE:
            self.checkpoint_handler.Checkpoint("CPMSession::remote_download_start.invalid_state", module_id, checkpoint.ERROR)
            return

        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_download_start", module_id, checkpoint.DEBUG, gpm_ids="%s"%gpm_ids):
            self._state = CPMSession.STATE_DOWNLOAD
            self._download_filenames = self._get_repository().ids_to_filenames_relative(self._gpms_root, gpm_ids)
            self._download_manager.request_ticket(self)

    def _download_cleanup(self):
        self._state = CPMSession.STATE_ANALYZE
        self.remove_tunnelendpoint_tunnel(self._download_tunnel_id)
        self._download_tunnel_id = None
            
    def download_manager_ticket_granted_from_other_thread(self, download_ticket):
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, 'download_manager_ticket_granted', download_ticket)

    def download_manager_ticket_granted(self, download_ticket):
        if self._state != CPMSession.STATE_DOWNLOAD:
            self.checkpoint_handler.Checkpoint("CPMSession::download_manager_ticket_granted.invalid_state", module_id, checkpoint.ERROR)
            return
        self._download_ticket = download_ticket
        self._download_tunnel_id = 2
        self._download_tunnelendpoint = self.create_and_add_tunnelendpoint_tunnel(self.checkpoint_handler, self._download_tunnel_id)
        self._downloader = cpm_file_downloader.CPMFileDownloader(self._async_service, self.checkpoint_handler, self._download_tunnelendpoint, download_ticket, self._download_tunnel_id, self, self._download_manager, self._gpms_root, self._download_filenames)
        self.tunnelendpoint_remote('remote_download_start_response', download_tunnel_id = self._download_tunnel_id)

    def remote_download_cancel_wait_for_ticket(self):
        self._download_manager.request_ticket_cancel(self)
        self._state = CPMSession.STATE_ANALYZE

    def download_manager_ticket_timeout_from_other_thread(self, download_ticket):
        self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, 'download_manager_ticket_timeout', download_ticket)

    def download_manager_ticket_timeout(self, download_ticket):
        self._download_cleanup()
        self._handle_error_state('Download ticket expired')
    
    def file_download_all_done(self, download_id):
        self._download_manager.release_ticket(self._download_ticket)
        self._download_cleanup()
        self._state = CPMSession.STATE_READY

    def file_download_cancled(self, download_id):
        self._download_manager.release_ticket(self._download_ticket)
        self._download_cleanup()

    def file_download_error(self, download_id, message, tell_remote=True):
        self._download_manager.release_ticket(self._download_ticket)
        self._handle_error_state(message, tell_remote)
        self._download_cleanup()
    
    def file_download_ticket_expired(self, download_id):
        self._handle_error_state('download ticket expired')
        
    def remote_closed(self):
        if self._connect_update_waiting_to_finish:
            self._cpm_endpoint_manager.connect_update_done()
            self._connect_update_waiting_to_finish = False
        self._reset_state()

    def remote_installation_done(self):
        """
        This is deprecated but still used by clients < 5.5
        """
        self.checkpoint_handler.Checkpoint("CPMSession::remote_installation_done", module_id, checkpoint.DEBUG)
        self.analyze_start_query()

    def remote_installation_done_with_info(self, gpm_ids_install, gpm_ids_update, gpm_ids_remove):
        self.checkpoint_handler.Checkpoint("CPMSession::remote_installation_done_with_info", module_id, checkpoint.DEBUG)
        self.analyze_start_query()
        if len(gpm_ids_install) + len(gpm_ids_update) + len(gpm_ids_remove) > 0:
            self._inform_access_log_did_update_packages(gpm_ids_install, gpm_ids_update, gpm_ids_remove)
        
        
    #
    # Misc methods
    #
    def _handle_error_state(self, message, tell_remote=True):
        with self.checkpoint_handler.CheckpointScope("CPMSession::_handle_error_state", module_id, checkpoint.DEBUG, message=message):
            if tell_remote:
                self.tunnelendpoint_remote('remote_handle_error_state', message=message)
            self._reset_state()

    def remote_handle_error_state(self, message):
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_handle_error_state", module_id, checkpoint.DEBUG, message=message):
            self._reset_state()

    def remote_get_meta_signature(self, gpm_ids_install, update_challenge, update_challenge_signature):
        with self.checkpoint_handler.CheckpointScope("CPMSession::remote_get_meta_signature", module_id, checkpoint.DEBUG):
            ok = lib.hagi.gon_hagi_authentication.verify_update_challenge_signature(self._server_knownsecret, update_challenge, update_challenge_signature)
            if ok:
                gpm_meta_temp_folder = tempfile.mkdtemp()
                gpm_error_handler = lib.gpm.gpm_env.GpmErrorHandler()
                gpm_ids_str = []
                for gpm_id in gpm_ids_install:
                    gpm_filename = os.path.join(self._gpms_root, '%s.gpm' % gpm_id)
                    lib.gpm.gpm_builder.install_meta(gpm_meta_temp_folder, gpm_filename, gpm_error_handler)
                    gpm_ids_str.append(str(gpm_id))
                gpm_meta_signature = lib.hagi.gon_hagi_authentication.generate_gpms_signature(gpm_meta_temp_folder, 
                                                                                              gpm_ids_str,
                                                                                              str(update_challenge), 
                                                                                              self._server_knownsecret)
                shutil.rmtree(gpm_meta_temp_folder)
                self.tunnelendpoint_remote('remote_get_meta_signature_response', signature=gpm_meta_signature)
            else:
                self.checkpoint_handler.Checkpoint("CPMSession::remote_get_meta_signature.invalid_signature", module_id, checkpoint.ERROR)

    #
    # 
    #
    def is_package_installed(self, package_name, package_arch, callback):
        """
        Start querying for information about installed packages. Return (None, None) if no information is available yet,
        else (Installed(boolean), package_id) is returned.
        """
        query_id = '%s::%s' % (package_name, package_arch)
        if not self.query_package_done:
            self.query_package_installed[query_id] = {'package_name':package_name, 'package_arch':package_arch, 'installed':None, 'package_id':None}
            self.query_package_callback = callback
            self.analyze_start_query()
            return (None, None)
        else:
            if self.query_package_installed.has_key(query_id):
                return (self.query_package_installed[query_id]['installed'], self.query_package_installed[query_id]['package_id'])  
        self.checkpoint_handler.Checkpoint("CPMSession::is_package_installed.invalid_query", module_id, checkpoint.ERROR, query_id=query_id)
        return (None, None)

    def _analyze_query(self):
        with self.checkpoint_handler.CheckpointScope("CPMSession::_analyze_query", module_id, checkpoint.DEBUG):
            query_package_changed = False
            for query_id in self.query_package_installed:
                (installed_old, package_id_old) = (self.query_package_installed[query_id]['installed'], self.query_package_installed[query_id]['package_id'])
                (installed, package_id) = self._get_repository().query_package(self.query_package_installed[query_id]['package_name'], self.query_package_installed[query_id]['package_arch'], self._gpm_ids_installed)
                if installed != installed_old or package_id != package_id_old:
                    query_package_changed = True
                    self.query_package_installed[query_id]['installed'] = installed
                    self.query_package_installed[query_id]['package_id'] = package_id
            
            self.query_package_done = True
            self._state = CPMSession.STATE_READY
            if query_package_changed and self.query_package_callback is not None:
                with self.checkpoint_handler.CheckpointScope("CPMSession::_analyze_query.callback", module_id, checkpoint.DEBUG):
                    self.query_package_callback()
    
    def check_for_missing_package(self, tags_string):
        """
        Called from dialog before launch
        """
        return components.cpm.server_gateway.plugins.PluginTag.check_for_missing_package(tags_string)
        

