"""
"""
from __future__ import with_statement


import components.user.server_common.database_schema as database_schema
import components.traffic.server_common.database_schema as traffic_schema

from plugin_types.server_management import plugin_type_config,\
    plugin_type_element
import components.plugin.server_management.plugin_socket_user
import components.plugin.server_management.plugin_socket_element
import components.plugin.server_management.plugin_socket_config
from components.management_message.server_common import from_sink_to_timestamp
from components.database.server_common import database_api
from components.auth.server_management import ElementType, OperationNotAllowedException
import components.auth.server_management.rule_api as rule_api
import lib.checkpoint
import sys

import smtplib
import socket
import email.mime.text

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

import components.management_message.server_management.session_recieve

import components.admin_ws.activity_log as activity_log 


from components.user.server_common import sink_component_id as sink_component_id
from components.database.server_common.database_api import QuerySession
from components.user.server_common.database_schema import UserInfo
from components.user.server_common.gss_server import GSS

class UserNotFoundExpection(Exception):
    pass

def _add_user_column(column, columns):
    for plugin_col in columns:
        if plugin_col.column_name == column.column_name:
            return
    columns.append(column)

            

class TmpUser(ElementType):
    def __init__(self, module_name, (external_user_id, user_label)):
        self.element_id = "%s.%s" % (module_name, external_user_id)
        self.label = user_label
        
    def get_id(self):
        return self.element_id
    
    def get_label(self):
        return self.label
    
class RegisteredUser(ElementType):

    def __init__(self, user):
        self.entity_type = ""
#        self.internal_id = id
        self.external_id = "%s.%s" % (user.user_plugin, user.external_user_id)
        self._label = user.user_login
        self.info = user.user_name
        self.template = None
        self.element_type = "registered_user"
        
        
    def get_id(self):
        return self.external_id
    
    def get_info(self):
        return self.info
    
    def get_label(self):
        return self._label

    def get_entity_type(self):
        return self.entity_type

    def get_element_type(self):
        return self.element_type
    
    def get_template(self):
        return self.template

def get_datetime_string(dt):
    if dt:
        return dt.strftime("%Y-%m-%d %H:%M:%S")
    else:
        return dt    


class UserAdmin(components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase):
    def __init__(self, environment, license_handler, activity_log_handler):
        components.management_message.server_management.session_recieve.ManagementMessageSessionRecieverBase.__init__(self, environment.checkpoint_handler, sink_component_id)
        self.plugin_socket_user = components.plugin.server_management.plugin_socket_user.PluginSocket(environment.plugin_manager)
        self.plugin_socket_element = components.plugin.server_management.plugin_socket_element.PluginSocket(environment.plugin_manager)
        self.plugin_socket_config = components.plugin.server_management.plugin_socket_config.PluginSocket(environment.plugin_manager)
        self.message_sink_access_log = None
        self.server_config = None
        self.license_handler = license_handler
        self.activity_log_handler = activity_log_handler
        for plugin in self.plugin_socket_user.get_plugins():
            plugin.user_admin = self
        self._traffic_admin = None

        self.gss_dict = dict()

    def set_traffic_admin(self, traffic_admin):
        self._traffic_admin = traffic_admin
        
    def enable_message_receiver_access_log(self, message_sink_access_log, server_config):    
        self.message_sink_access_log = message_sink_access_log
        self.message_sink_access_log.user_admin = self
        self.server_config = server_config
        
    def get_number_of_registered_users(self):
        return QuerySession().select_count(database_schema.UserInfo)
    
    
    def create_user(self, transaction, plugin_name, user_id):
        license = self.license_handler.get_license()
        if not license.valid:
            raise OperationNotAllowedException("Invalid license")

        max_number_of_users = license.get_int('Number of Users') 
        no_of_users = self.get_number_of_registered_users()
        if no_of_users >= max_number_of_users:
            raise OperationNotAllowedException("Maximum number of licensed users (%d) reached" % max_number_of_users)

        internal_id = database_schema.get_unique_user_internal_id(transaction, plugin_name, user_id)
        return self._create_user1(transaction, internal_id, plugin_name, user_id)

    def _create_user1(self, transaction, internal_id, plugin_name, user_id, login_time=None):
        user = database_schema.UserInfo()
        user.external_user_id = user_id
        user.internal_id = internal_id
        user.user_plugin = plugin_name
        self.update_user_data(transaction, user, login_time)
        database_schema.check_unique_user_attributes(transaction, user)
        transaction.add(user)
        transaction.flush()
        
        # activity log
        lines = []
        lines.append("User login: %s" % user.user_login)
        lines.append("User id: %s" % user.external_user_id)
        lines.append("User plugin_name: %s" % user.user_plugin)
        details = "\n".join(lines)
        
#        user_args = dict()
#        if not self.activity_log_handler.default_values.get("user_id"):
#            user_args[activity_log.ActivityLog.USER_ID] = user.external_user_id
#            user_args[activity_log.ActivityLog.USER_LOGIN] = user.user_login
#            user_args[activity_log.ActivityLog.USER_PLUGIN] = user.user_plugin
        
        self.activity_log_handler.copyUpdateAndAdd(transaction,
                                                   access_right="Entity.User",
                                                 type="Update",
                                                 summary="Register User",
                                                 details=details)
    
        return user
    
    def get_login_and_name_from_id(self, user_plugin, external_user_id):
        with database_api.QuerySession() as dbs:
            user = database_schema.get_user(dbs, user_plugin, external_user_id)
            if user:
                return user.user_login, user.user_name
        
        return None, None
                
    
    def update_user_data(self, transaction, user, login_time):
        plugin = self.plugin_socket_user.get_plugin(user.user_plugin)
        internal_user_login, user_login, user_name = plugin.get_login_and_name_from_id(user.external_user_id, user.internal_user_login)
        if user_login != user.user_login:
            rule_api.element_updated("User", user.internal_id, user_login, transaction)
        user.internal_user_login, user.user_login, user.user_name = internal_user_login, user_login, user_name
        user.last_login = login_time

    def create_group(self, transaction, plugin_name, group_id):
        internal_id = database_schema.get_unique_group_internal_id(transaction, plugin_name, group_id)
        group = database_schema.GroupInfo()
        group.external_group_id = group_id
        group.internal_id = internal_id
        group.group_plugin = plugin_name
        transaction.add(group)
        
#        # activity log
#        lines = []
#        lines.append("Group id: %s" % group.external_group_id)
#        lines.append("Group plugin_name: %s" % group.group_plugin)
#        details = "\n".join(lines)
#        self.activity_log_handler.createAndSaveLogElement(summary="Register Group",
#                                                          details=details )

        return group
        
    def get_specific_elements(self, internal_element_type, element_ids):
        with database_api.ReadonlySession() as db_session:
            elements = []
            if internal_element_type == "user":
                users = db_session.select(database_schema.UserInfo, 
                                          database_api.in_(database_schema.UserInfo.internal_id, element_ids))
                for user in users:
                    label = user.user_login
                    info = user.user_name
                    plugin_name = user.user_plugin
                    config_spec = self._get_user_config_spec(user)
                    user_element = plugin_type_element.ModuleElementType(plugin_name, user.external_user_id, label, info)
    #                user_element._label += ":%s" % config_spec.get_value("registered_user")  
                    user_element.template = config_spec.get_template()
                    elements.append(user_element)
            else:
                groups = db_session.select(database_schema.GroupInfo, 
                                           database_api.in_(database_schema.GroupInfo.internal_id, element_ids))
                
                for group in groups:
                    pass
                 
            return elements


    def _add_users_to_list(self, users, user_id_dict, return_list):
        for user in users:
            value_dicts = user_id_dict.get(user.internal_id)
            for value_dict in value_dicts:
                user_dict = dict()
                user_dict.update(value_dict)
                user_dict["user_id"] = user.internal_id
                user_dict["user_login"] = user.user_login
                user_dict["user_name"] = user.user_name
                return_list.append(user_dict)
        

    def get_user_info(self, user_info_list, filter=None):
        with database_api.QuerySession() as db_session:
            index = 0
            return_list = []
            
            user_id_dict = dict()
            no_user_info = []
            
            for info in user_info_list:
                if info.user_plugin:
                    user_id = "%s.%s" % (info["user_plugin"], info["external_user_id"])
                    info_list = user_id_dict.get(user_id, [])
                    info_list.append(info)
                    user_id_dict[user_id] = info_list
                else:
                    no_user_info.append(info)

            user_ids = user_id_dict.keys()
            users = []
            while index<len(user_ids):
                if filter:
                    sqlfilter = "%%%s%%" % filter
                    users.extend(db_session.select(database_schema.UserInfo, 
                                                   database_api.and_(database_api.in_(database_schema.UserInfo.internal_id, user_ids[index:index + database_api.IN_CLAUSE_LIMIT]),
                                                                     database_api.or_(database_schema.UserInfo.user_login.like(sqlfilter),
                                                                                      database_schema.UserInfo.user_name.like(sqlfilter)
                                                                                     )
                                                                     )
                                                   )
                                )
                else:
                    users.extend(db_session.select(database_schema.UserInfo, 
                                                   database_api.in_(database_schema.UserInfo.internal_id, user_ids[index:index + database_api.IN_CLAUSE_LIMIT])
                                                   )
                                )
                index = index + database_api.IN_CLAUSE_LIMIT
                
            if not filter:
                for e in no_user_info:
                    user_dict = dict()
                    user_dict.update(e)
                    return_list.append(user_dict)
                    
            
            for user in users:
                value_dicts = user_id_dict.pop(user.internal_id)
                for value_dict in value_dicts:
                    user_dict = dict()
                    user_dict.update(value_dict)
                    user_dict["user_id"] = user.internal_id
                    user_dict["user_login"] = user.user_login
                    user_dict["user_name"] = user.user_name
                    return_list.append(user_dict)
                    
            if not filter:
                for info_list in user_id_dict.values():
                    for e in info_list:
                        user_dict = dict()
                        user_dict.update(e)
                        return_list.append(user_dict)
                
            return return_list
                
    
        

    def _create_user_element(self, user):
        user_element = RegisteredUser(user)
        #                    config_spec = self._get_user_config_spec(user.user_plugin, user.external_user_id, user)
        #                    user_element.template = config_spec.get_template()
        user_element.info += "\nLast Login: %s" % get_datetime_string(user.last_login)
        try:
            plugin = self.plugin_socket_user.get_plugin(user.user_plugin)
            if plugin:
                if not plugin.user_exists(user.external_user_id):
                    raise UserNotFoundExpection("User not found in User Directory")
            else:
                raise UserNotFoundExpection("User Directory '%s' not found" % user.user_plugin)
        except UserNotFoundExpection as e:
            user_element.element_type = "unmatched_registered_user"
            user_element.info += "\nError: %s" % e
        except Exception as e:
            user_element.element_type = "unmatched_registered_user"
            user_element.info += "\nError during User Directory lookup: '%s'" % e
        return user_element

    def get_elements(self, internal_element_type, search_filter=None):
        plugins = self.plugin_socket_user.get_plugins()
        plugin_providers = [ (plugin.plugin_name, internal_element_type) for plugin in plugins]
        if internal_element_type == "user":
            users = QuerySession().select(database_schema.UserInfo)
            if search_filter and search_filter.startswith("*__REGISTERED_USERS__"):
                if search_filter=="*__REGISTERED_USERS__*":
                    for user in users:
                        user_element = self._create_user_element(user)
                            
    
                        yield user_element
                else:
                    launch_type_category = search_filter.replace("*__REGISTERED_USERS__", "")
                    launch_type_category = launch_type_category[0:len(launch_type_category)-1]
                    user_dict = self._traffic_admin.get_user_ids_for_launch_type_category(launch_type_category)
                    for user in users:
                        launch_registration_date = user_dict.get(user.internal_id)
                        if launch_registration_date:
                            user_element = self._create_user_element(user)
                            user_element.info = "Last %s Launch: %s" % (launch_type_category, get_datetime_string(launch_registration_date))
                            yield user_element
                    
                
            else:
                if search_filter and search_filter.startswith("*__CATEGORY_SEARCH__"):
                     plugin_name_and_search_filter = search_filter.replace("*__CATEGORY_SEARCH__", "")
                     assert ":" in plugin_name_and_search_filter, "Invalid category search filter: '%s'" % plugin_name_and_search_filter
                     plugin_name, search_filter = plugin_name_and_search_filter.split(":")
                     plugin_providers = [ (plugin_name, internal_element_type) ]                
                     if len(search_filter)>1:
                         search_filter = "*%s" % search_filter
                     else:
                         search_filter = None
                user_dict = dict()                
                for user in users:
                    user_dict[user.internal_id] = user
                for user_element in self.plugin_socket_element.get_module_elements(plugin_providers, search_filter):
                    registered_user = user_dict.get(user_element.get_id())
#                    plugin_name, dot, element_id = user_element.get_id().partition(".")
#                    config_spec = self._get_user_config_spec(plugin_name, element_id, registered_user)
#                    user_element.template = config_spec.get_template()
                    if registered_user:
                        user_element.element_type = "registered_user" 
                        user_element._label = registered_user.user_login
                    yield user_element
        else:
            for e in self.plugin_socket_element.get_module_elements(plugin_providers, search_filter):
                yield e
    

    def _get_user_config_spec(self, registered_user):
        config_spec = ConfigTemplateSpec()
        config_spec.init("%s" % (registered_user.user_plugin,), "Edit User", "")
        
        config_spec.add_field('user_id', 'user_id', ConfigTemplateSpec.VALUE_TYPE_STRING, field_type=ConfigTemplateSpec.FIELD_TYPE_HIDDEN)
        field = config_spec.add_field("user_login", 'Login', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field("user_dir", 'User Directory', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field("user_name", 'Name', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field('external_id', 'User ID', ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_read_only(True)
        field = config_spec.add_field("registered_user", 'G/On User', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN)
        field.set_attribute_read_only(True)

        config_spec.set_value("user_id", registered_user.internal_id)
        config_spec.set_value("external_id", registered_user.external_user_id)
        config_spec.set_value("user_login", registered_user.user_login)
        config_spec.set_value("user_name", registered_user.user_name)
        config_spec.set_value("user_dir", registered_user.user_plugin)
        config_spec.set_value("registered_user", True)
        
        return config_spec

    def get_create_templates(self, internal_element_type, db_session=None):
        templates = []
        for plugin in self.plugin_socket_user.get_plugins():
            template = self.plugin_socket_config.config_get_template(plugin.plugin_name, internal_element_type, element_id=None, db_session=db_session)
            if template:
                templates.append(template)
        return templates

    def config_get_template(self, internal_element_type, element_id, custom_template, db_session):
        if internal_element_type == "user" and element_id:
            with database_api.ReadonlySession() as dbs:
                user = database_schema.get_user_by_internal_id(dbs, element_id)
            
                if user:
                    config_spec = self._get_user_config_spec(user)
                else:
                    try:
                        module_name, dot, element_id = element_id.partition(".")
                        template = self.plugin_socket_config.config_get_template(module_name, internal_element_type, element_id, db_session)
                        config_spec = ConfigTemplateSpec(template)
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.Checkpoint('config_get_template', "user_admin", lib.checkpoint.WARNING, message='Error finding user in user directory', plugin_name=module_name, element_id=element_id)
                        self.checkpoint_handler.CheckpointException("config_get_template", "user_admin", lib.checkpoint.WARNING, etype, evalue, etrace)
                        return None
                    
                self.add_user_config_fields(config_spec, user, dbs)
                return config_spec.get_template()
        return None
        

    def config_update_row(self, internal_element_type, element_id, template, dbt):
        # Check if this is an unregistration thing
        if internal_element_type == "user":
            with database_api.Transaction() as transaction:
                user = database_schema.get_user_by_internal_id(transaction, element_id)
                config_spec = ConfigTemplateSpec(template)
                registered = config_spec.get_value("registered_user")
                if user and not registered:
                    self.unregister_user(transaction, element_id)
                    
                    # activity log
                    lines = []
                    lines.append("User login: %s" % user.user_login)
                    lines.append("User id: %s" % user.external_user_id)
                    lines.append("User plugin_name: %s" % user.user_plugin)
                    details = "\n".join(lines)
                    self.activity_log_handler.copyUpdateAndAdd(transaction,
                                                               access_right="Entity.User",
                                                             type="Update",
                                                             summary="Unregister User",
                                                             details=details )
                    
                    return config_spec.get_value("user_login")
                    
                    
                plugin_name, dot, plugin_element_id = element_id.partition(".")
                user_id = config_spec.get_value("user_id")
    
                if not user:
                    user = self.create_user(transaction, plugin_name, user_id)
                
                config_spec.update_obj_with_values(user)
                values = config_spec.get_field_value_dict()    

                # Update lifetime values
                entity_type ="User"
                start_dt = values.get("start")
                end_dt = values.get("end")
                
                element_lifetime = rule_api.get_element_lifetime(entity_type, user.internal_id, transaction)
                if start_dt or end_dt:
                    if not element_lifetime:
                        element_lifetime = rule_api.get_or_create_element_lifetime(entity_type, user.internal_id, transaction)
                    element_lifetime.start = start_dt
                    element_lifetime.end = end_dt
                else:
                    if element_lifetime:
                        transaction.delete(element_lifetime)
            
                
                user_pc_dict = dict()
                user_row = dict()
                for (name,value) in values.items():
                    if name.startswith("user_pc."):
                        (dummy, line_no_str, field_name) = name.split(".")
                        line_no = int(line_no_str)
                        user_pc_row = user_pc_dict.get(line_no, dict())
                        user_pc_row[field_name] = value
                        user_pc_dict[line_no] = user_pc_row
                    else:
                        user_row[name] = value
                
                # Update existing user pc's
                for user_pc in user.user_pcs:
                    try:
                        user_pc_row = user_pc_dict.pop(user_pc.line_no)
                        self._update_obj_with_row(user_pc, user_pc_row, transaction)
                    except KeyError:
                        pass
        
                # create new user_pc's
                for (line_no, user_pc_row) in user_pc_dict.items():
                    if self._check_user_pc_row(user_pc_row):
                        user_pc = database_schema.UserPC()
                        user_pc.line_no = line_no
                        transaction.add(user_pc)
                        self._update_obj_with_row(user_pc, user_pc_row, transaction)
                        user.user_pcs.append(user_pc)
        
    
    def config_create_row(self, internal_element_type, template, dbt):
        # TODO : Fix when users can be created
        module_name = template.get_attribute_something()
        return self.plugin_socket_config.config_create_row(module_name, internal_element_type, template, dbt)
    

    def config_delete_row(self, internal_element_type, element_id, dbt):
        # TODO : Fix when users can be deleted
        raise NotImplementedError()
        
    def get_config_enabling(self, internal_element_type):
        if internal_element_type=="user":
            return True, False
        else:
            return False, False

    def get_element_id(self, module_name, element_id, internal_element_type):
        with database_api.ReadonlySession() as dbs:
            if internal_element_type == "user":
                user = database_schema.get_user(dbs, module_name, element_id)
                if user:
                    return user.internal_id
            elif internal_element_type == "group":
                group = database_schema.get_group(dbs, module_name, element_id)
                if group:
                    return group.internal_id
            else:
                return None

    def get_external_id(self, internal_element_type, element_id):
        with database_api.ReadonlySession() as dbs:
            if internal_element_type == "user":
                user = database_schema.get_user_by_internal_id(dbs, element_id)
                if user:
                    return "%s.%s" % (user.user_plugin, user.external_user_id)
            elif internal_element_type == "group":
                group = database_schema.get_group_by_internal_id(dbs, element_id)
                if group:
                    return "%s.%s" % (group.group_plugin, group.external_group_id)
            else:
                return None
    
    def register_element(self, transaction, element, plugin_element_type, override_license=False):
        module_name, dot, element_id = element.get_id().partition(".")
        if plugin_element_type == u"user":
            registered_user = database_schema.get_user(transaction, module_name, element_id)
            if not registered_user:
                if override_license:
                    internal_id = database_schema.get_unique_user_internal_id(transaction, module_name, element_id)
                    registered_user = self._create_user1(transaction, internal_id, module_name, element_id)
                else:
                    registered_user = self.create_user(transaction, module_name, element_id)
            return (registered_user.internal_id, registered_user.user_login)
        elif plugin_element_type == u"group":
            registered_group = database_schema.get_group(transaction, module_name, element_id)
            if not registered_group:
                registered_group = self.create_group(transaction, module_name, element_id)
            return (registered_group.internal_id, element.get_label())
        
    def unregister_user(self, transaction, internal_element_id):
        module_name, dot, element_id = internal_element_id.partition(".")
        rule_api.delete_criteria_for_element("User", internal_element_id, transaction)
        _element_lifetime = rule_api.get_element_lifetime("User", internal_element_id, transaction)
        if _element_lifetime:
            transaction.delete(_element_lifetime)
        traffic_schema.delete_launch_type_launched_for_user(internal_element_id, transaction)
        registered_user = database_schema.get_user(transaction, module_name, element_id)
        registered_user.delete(transaction)
        
        
    def _add_my_pc_fields(self, config_spec, line_no):
        field = config_spec.add_field("user_pc.%s.my_pc" % line_no, 'My-PC %s' % line_no, ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(256)
        field = config_spec.add_field("user_pc.%s.mac_address" % line_no, 'My PC %s MAC Address' % line_no, ConfigTemplateSpec.VALUE_TYPE_STRING)
        field.set_attribute_max_length(64)
        
        
        
    def get_all_user_ids(self):
        with database_api.ReadonlySession() as dbs:
            users = dbs.select(database_schema.UserInfo)
            return [user.internal_id for user in users]
        
    def add_user_config_fields(self, config_spec, user, dbs):
        config_spec.add_field("start", 'Access from', ConfigTemplateSpec.VALUE_TYPE_DATETIME, tooltip="Set this to limit access period for the user.")
        config_spec.add_field("end", 'Access to', ConfigTemplateSpec.VALUE_TYPE_DATETIME, tooltip="Set this to limit access period for the user.")
        self._add_my_pc_fields(config_spec, 1)
        if user:
            element_lifetime = rule_api.get_element_lifetime("User", user.internal_id, dbs)
            if element_lifetime:
                config_spec.set_values_from_object(element_lifetime)
            count = 1
            for user_pc in user.user_pcs:
                count += 1
                self._add_my_pc_fields(config_spec, count)
                
            for user_pc in user.user_pcs:
                prefix = "user_pc.%s." % user_pc.line_no
                config_spec.set_values_from_object(user_pc, attr_name_prefix=prefix)
                
                    
            
        
    
        
    def add_user_values(self, plugin_name, element_type, element_id, config_spec, db_session=None):
        if element_type.lower()=="user":
            with database_api.SessionWrapper(db_session) as dbs:
                user = database_schema.get_user(dbs, plugin_name, element_id)
                if user:
                    config_spec.set_value("registered_user", True)
                    for user_pc in user.user_pcs:
                        prefix = "user_pc.%s." % user_pc.line_no
                        config_spec.set_values_from_object(user_pc, attr_name_prefix=prefix)
                        

        
                        
    def add_members_to_one_time_enrollers(self, group_element):
        module_name, dot, element_id = group_element.get_id().partition(".")
        group_members = self.plugin_socket_user.get_plugin(module_name).get_group_members(element_id)

        with database_api.Transaction() as transaction:
            users = []
            for user in group_members:
                element = TmpUser(module_name, user)
                (user_id, user_label) = self.register_element(transaction, element, "user", override_license=True)
                element.label = user_label
                element.element_id = user_id
                users.append(element)
                
#            license = self.license_handler.get_license()
#            if not license.valid:
#                raise OperationNotAllowedException("Invalid license")
#    
#            max_number_of_users = license.get_int('Number of Users') 
#            no_of_users = self.get_number_of_registered_users()
#            if no_of_users >= max_number_of_users:
#                return
#                raise OperationNotAllowedException("Licensed users limit (%d) exceeded by %d" % (max_number_of_users, no_of_users-max_number_of_users))
            
            rule_api.create_gon_group_rules(self.checkpoint_handler, self, users, transaction)
            
            # activity log
            lines = []
            lines.append("Users:")
            for e in users:
                lines.append("User '%s' (%s)" % (e.get_label(), e.get_id()))
            details = "\n".join(lines)
            
            
            self.activity_log_handler.copyUpdateAndAdd(transaction,
                                                     access_right="Rule.%s" % rule_api.get_gon_group_rule_type(),
                                                     type="Update",
                                                     summary="Add users to One-Time Enrollers",
                                                     details=details )
        
            
    
    
    def create_or_update_user_values(self, plugin_name, user_id, values, transaction):
        user = database_schema.get_user(transaction, plugin_name, user_id)
        if not user:
            user = self.create_user(transaction, plugin_name, user_id)
            
        
        user_pc_dict = dict()
        user_row = dict()
        for (name,value) in values.items():
            if name.startswith("user_pc."):
                (dummy, line_no_str, field_name) = name.split(".")
                line_no = int(line_no_str)
                user_pc_row = user_pc_dict.get(line_no, dict())
                user_pc_row[field_name] = value
                user_pc_dict[line_no] = user_pc_row
            else:
                user_row[name] = value
        
        # Update existing user pc's
        for user_pc in user.user_pcs:
            try:
                user_pc_row = user_pc_dict.pop(user_pc.line_no)
                self._update_obj_with_row(user_pc, user_pc_row, transaction)
            except KeyError:
                pass

        # create new user_pc's
        for (line_no, user_pc_row) in user_pc_dict.items():
            if self._check_user_pc_row(user_pc_row):
                user_pc = database_schema.UserPC()
                user_pc.line_no = line_no
                transaction.add(user_pc)
                self._update_obj_with_row(user_pc, user_pc_row, transaction)
                user.user_pcs.append(user_pc)
        
                
    def _update_obj_with_row(self, obj, row, transaction): 
        for (name,value) in row.items():
            try:
                setattr(obj, name, value)
            except AttributeError, e:
                pass
        
                    
    def _check_user_pc_row(self, row):
        for (name, value) in row.items():
            if value:
                return True
        return False
                    
    def sink_user_login_received(self, server_sid, is_new_user, login, unique_session_id, internal_id, plugin_name, external_user_id, login_time):
        if self.message_sink_access_log:
            self.message_sink_access_log.access_log_auth_ad_login(unique_session_id, server_sid, login, plugin_name, external_user_id)
        with database_api.Transaction() as dbt:
            user = database_schema.get_user_by_internal_id(dbt, internal_id)
            login_time = from_sink_to_timestamp(login_time)
            if not user:
                user = self._create_user1(dbt, internal_id, plugin_name, external_user_id, login_time)
            elif is_new_user:
                self.checkpoint_handler.Checkpoint("UserAsmin::sink_user_login_received:user_already_exists", 
                                                   sink_component_id,
                                                   lib.checkpoint.WARNING, 
                                                   plugin_name=plugin_name, 
                                                   external_user_id=external_user_id, 
                                                   internal_id=internal_id, 
                                                   login_time=login_time.isoformat(), 
                                                   )
            else:
                self.update_user_data(dbt, user, login_time)
            
            if self.message_sink_access_log and self.server_config and self.server_config.access_notification_enabled:
                try:
                    ip = self.message_sink_access_log.get_session_ip(unique_session_id, dbt)
                    self.notify_login(plugin_name, external_user_id, ip, login_time)
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("notify_login.error", "user_admin", lib.checkpoint.ERROR, etype, evalue, etrace)
#            else:
#                self.checkpoint_handler.Checkpoint('notify_login.disabled', "user_admin", lib.checkpoint.DEBUG, message='No mail send because the feature has been disabled')
                    
    def sink_initialize_gss(self, server_sid, unique_session_id, pkg_name, principal, auth_data):
        if GSS:
            gss = GSS(self, pkg_name, self.checkpoint_handler)
            self.gss_dict[unique_session_id] = gss
            gss.initalize(auth_data)
        else:
            self.authorize_gss_failed_reply("Internal Error - GSS not supported on this server")
            
        
        
    def sink_authorize_gss(self, server_sid, unique_session_id, sec_buffer_in):
        gss = self.gss_dict.get(unique_session_id)
        if gss:
            gss.authorize(sec_buffer_in)
        else:
            self.authorize_gss_failed_reply("Internal Error - no GSS session started")

    def _cleanup_gss(self):
        for session_id, gss in self.gss_dict.items():
            if gss.done:
                self.gss_dict.pop(session_id)
                
            
    
    def initialize_gss_reply(self, credentials, credentials_expiry):
        self._cleanup_gss()
        remote_sender("initialize_gss_reply", credentials, credentials_expiry)

    def authorize_gss_failed_reply(self, err):
        self._cleanup_gss()
        remote_sender("authorize_gss_failed_reply", err)

    def authorize_gss_continue_reply(self, sec_buffer_out):
        remote_sender("authorize_gss_continue_reply", sec_buffer_out)

    def authorize_gss_worked_reply(self, user_name, user_SID, group_SIDs=None):
        self._cleanup_gss()
        remote_sender("authorize_gss_worked_reply", user_name, user_SID, group_SIDs)
    
    
    def notify_login(self, plugin_name, user_sid, ip, timestamp):
        with self.checkpoint_handler.CheckpointScope('notify_login', "user_admin", lib.checkpoint.DEBUG, user_id=user_sid, plugin_name=plugin_name):

            # Get email address for user
            plugin = self.plugin_socket_user.get_plugin(plugin_name)
            if not plugin:
                self.checkpoint_handler.Checkpoint('notify_login', "user_admin", lib.checkpoint.ERROR, message='Unable to find plugin', plugin_name=plugin_name)
                return 
                
            user_email = plugin.get_email(user_sid)

            if not user_email:
                self.checkpoint_handler.Checkpoint('notify_login.error', "user_admin", lib.checkpoint.WARNING, message='No mail send because the email address was missing')
                return 
            
            # Send mail
            message = 'You just logged in via G/On from %s' % (ip)
            subject = 'G/On Login'
            sender = self.server_config.access_notification_sender
            mail = email.mime.text.MIMEText(message)

            mail['Subject'] = subject
            mail['From'] = sender
            mail['To'] = user_email

            try:
                mail_server = smtplib.SMTP(host=self.server_config.access_notification_smtp_server, port=25)
                if self.server_config.access_notification_smtp_server_user:
                    mail_server.login(self.server_config.access_notification_smtp_server_user, self.server_config.access_notification_smtp_server_password)
                mail_server.sendmail(sender, [user_email], mail.as_string())
                mail_server.close()
            except smtplib.SMTPServerDisconnected:
                self.checkpoint_handler.Checkpoint('notify_login.error', "user_admin", lib.checkpoint.ERROR, message="SMTPServerDisconnected, Unable to deliver mail to '%s'" % self.server_config.access_notification_smtp_server)

            except smtplib.SMTPAuthenticationError:
                self.checkpoint_handler.Checkpoint('notify_login.error', "user_admin", lib.checkpoint.ERROR, message="SMTPAuthenticationError, Unable to deliver mail to '%s'" % self.server_config.access_notification_smtp_server)

            except socket.error, msg:
                self.checkpoint_handler.Checkpoint('notify_login.error', "user_admin", lib.checkpoint.ERROR, message="Unable to deliver mail to '%s': %s" % (self.server_config.access_notification_smtp_server, msg) )

        
    def user_mgmt_login_succeded(self, plugin_name, external_user_id, received_login):
        self.activity_log_handler.addValues(user_id = external_user_id, user_plugin = plugin_name)
        with database_api.ReadonlySession() as dbs:
            internal_id = "%s.%s" % (plugin_name, external_user_id)
            user = database_schema.get_user_by_internal_id(dbs, internal_id)
            if user:
                self.activity_log_handler.addValues(user_login = user.user_login, user_name = user.user_name)
            else:
                self.activity_log_handler.addValues(user_login = received_login)
        
    def get_search_categories(self):
        search_categories = []
        for plugin in self.plugin_socket_user.get_plugins():
            search_categories.append((plugin.plugin_name, plugin.get_plugin_title()))  
        return search_categories      
        