"""
"""
from __future__ import with_statement

from components.communication import tunnel_endpoint_base

import components.user

from lib import checkpoint
import lib.encode
import sys 

if sys.platform == 'win32':
    
    # code "stolen" from http://nullege.com/codes/show/src%40p%40y%40pywin32-HEAD%40win32%40lib%40sspi.py/132/win32security.InitializeSecurityContext/python
    import win32security, sspicon
    
    class GSS(object):
        
        def __init__(self,
                     pkg_name, # Name of the package to used.
                     checkpoint_handler,
                     client_name = None, # User for whom credentials are used.
                     auth_data = None, # or a tuple of (username, domain, password)
                     targetspn = None, # Target security context provider name.
                     scflags=None, # security context flags
                     datarep=sspicon.SECURITY_NETWORK_DREP):
            self.checkpoint_handler = checkpoint_handler
            if scflags is None:
                scflags = sspicon.ISC_REQ_INTEGRITY|sspicon.ISC_REQ_SEQUENCE_DETECT|\
                          sspicon.ISC_REQ_REPLAY_DETECT|sspicon.ISC_REQ_CONFIDENTIALITY|sspicon.ISC_REQ_DELEGATE
        
            self.scflags=scflags
            self.datarep=datarep
            self.targetspn=targetspn
            self.ctxt = None
            try:
                self.pkg_info=win32security.QuerySecurityPackageInfo(pkg_name)
                #auth_data=("admin01@devtest.giritech.com", None, "admin01")
                res = win32security.AcquireCredentialsHandle(client_name, 
                                                             self.pkg_info['Name'],
                                                             sspicon.SECPKG_CRED_OUTBOUND,
                                                             None, 
                                                             auth_data)
                self.credentials, self.credentials_expiry = res
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.error", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
                self.credentials = self.credentials_expiry = None
                
                 
        
        def authorize(self, sec_buffer_in):
            try:
                if sec_buffer_in is not None and type(sec_buffer_in) != win32security.PySecBufferDescType:
                    # User passed us the raw data - wrap it into a SecBufferDesc
                    sec_buffer_new=win32security.PySecBufferDescType()
                    tokenbuf=win32security.PySecBufferType(self.pkg_info['MaxToken'],
                                                         sspicon.SECBUFFER_TOKEN)
                    tokenbuf.Buffer=sec_buffer_in
                    sec_buffer_new.append(tokenbuf)
                    sec_buffer_in = sec_buffer_new
                sec_buffer_out=win32security.PySecBufferDescType()
                tokenbuf=win32security.PySecBufferType(self.pkg_info['MaxToken'], sspicon.SECBUFFER_TOKEN)
                sec_buffer_out.append(tokenbuf)
                ## input context handle should be NULL on first call
                ctxtin=self.ctxt
                if self.ctxt is None:
                    self.ctxt=win32security.PyCtxtHandleType()
                err, attr, exp=win32security.InitializeSecurityContext(self.credentials,
                                                                       ctxtin,
                                                                       self.targetspn,
                                                                       self.scflags,
                                                                       self.datarep,
                                                                       sec_buffer_in,
                                                                       self.ctxt,
                                                                       sec_buffer_out)
                # Stash these away incase someone needs to know the state from the
                # final call.
                self.ctxt_attr = attr
                self.ctxt_expiry = exp
          
                if err in (sspicon.SEC_I_COMPLETE_NEEDED,sspicon.SEC_I_COMPLETE_AND_CONTINUE):
                    self.ctxt.CompleteAuthToken(sec_buffer_out)
                self.authenticated = err == 0
                return err, sec_buffer_out[0].Buffer
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.error", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
                return -1, None
            

else:
    
    GSS = None


class UserSession(tunnel_endpoint_base.TunnelendpointSession):
    def __init__(self, async_service, checkpoint_handler, new_tunnel, user_interface, runtime_env):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, new_tunnel)
        self.user_interface = user_interface
        self.dictionary = self.user_interface.dictionary
        self.runtime_env = runtime_env
        
        self.gss = None
        self.use_sso = False
        self.use_interactive_login = True
        

    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        with self.checkpoint_handler.CheckpointScope("UserSession::session_start_and_connected", components.user.module_id, checkpoint.DEBUG):
            pass

    def session_close(self):
        """
        Hard close of session. Called from main session when communication is terminated
        """
        with self.checkpoint_handler.CheckpointScope("UserSession::session_close", components.user.module_id, checkpoint.DEBUG):
            pass
    
    def _credentials_subscriber(self):
        if self.user_interface.login.get_cancel_clicked or self.user_interface.login.get_next_clicked:
            """ If correct credentials are given - add menu folders and items. If not - die. """
            if not self.user_interface.login.get_cancel_clicked():
                credentials = self.user_interface.login.get_credentials()
                self.tunnelendpoint_remote('receive_login', login=credentials[0], password=credentials[1], requestedpassword=credentials[2])
            else:
                self.tunnelendpoint_remote('login_cancelled')
            self.user_interface.login.clear_credentials()
            self.user_interface.login.unsubscribe(self._credentials_subscriber)

    def show_login_prompt(self, msg, change_password_enabled=True):
        self.user_interface.login.set_greeting(msg)
        login = self.runtime_env.get_settings_value('user_last_login')
        if login:
            self.user_interface.login.set_suggested_username(login)        
        if self.use_sso:
            self.try_sso()
        else:
            self._show_login_prompt()
            
    def _show_login_prompt(self):
        if self.use_interactive_login:
            self.user_interface.login.subscribe(self._credentials_subscriber)
            self.user_interface.login.display()

    def get_client_encoding(self):
        client_encoding = lib.encode.get_current_encoding()
        self.tunnelendpoint_remote('get_client_encoding_cb', client_encoding=client_encoding)

    def _change_password_credentials_subscriber(self):
        """ If next button is clicked - change password. If cancel - cancel. """
        if self.user_interface.login.get_next_clicked() or self.user_interface.login.get_cancel_clicked():
            change_password = self.user_interface.login.get_state() == self.user_interface.login.ID_STATE_CHANGE_LOGIN
            if change_password and not self.user_interface.login.get_cancel_clicked():
                credentials = self.user_interface.login.get_credentials()
                self.tunnelendpoint_remote('receive_changed_password', old_password=credentials[1], new_password=credentials[2])
            else:
                self.tunnelendpoint_remote('change_password_cancelled')
            self.user_interface.login.clear_credentials()
            self.user_interface.login.unsubscribe(self._change_password_credentials_subscriber)

    def change_password_choice_subscriber(self):
        """ If the user wants to change the password - show the change password dialog. """
        if self.user_interface.modalmessage.get_next_clicked() or self.user_interface.modalmessage.get_cancel_clicked():
            if self.user_interface.modalmessage.get_next_clicked():
                self._show_change_password_prompt(self.login, self.login_locked, self.optional)
            elif self.user_interface.modalmessage.get_cancel_clicked():
                self.tunnelendpoint_remote('change_password_cancelled')
            self.user_interface.modalmessage.unsubscribe(self.change_password_choice_subscriber)
            # Reset cached settings
            self.login = ""
            self.login_locked = False
            self.optional = True
            # Reset button states
            self.user_interface.modalmessage.reset_next_clicked()
            self.user_interface.modalmessage.reset_cancel_clicked()

    def show_change_password_prompt(self, msg, login='', login_locked=True, optional=False):
        """
        Front for the actual 'show_change_password_prompt' method.
        
        This will ask the user if he wants to change the password
        and react accordingly.
        
        @param optional: If true the user can choose to change the password - else it is forced.
        """
        self.login = login
        self.login_locked = login_locked
        self.optional = optional

        if optional:
            self.user_interface.modalmessage.reset_cancel_clicked()
            self.user_interface.modalmessage.set_cancel_allowed(True)
        else:
            self.user_interface.modalmessage.set_cancel_allowed(False)

        self.user_interface.modalmessage.set_next_visible(True)
        self.user_interface.modalmessage.set_next_allowed(True)
        self.user_interface.modalmessage.reset_next_clicked()
        self.user_interface.modalmessage.set_cancel_visible(True)
        self.user_interface.modalmessage.set_text(msg)
        self.user_interface.modalmessage.subscribe(self.change_password_choice_subscriber)
        self.user_interface.modalmessage.display()

    def show_change_password(self, login):
        self._show_change_password_prompt(login, login_locked=True)
        
    def _show_change_password_prompt(self, login='', login_locked=True, optional=True):
        """ Let the user change the password. """
        if login:
            self.user_interface.login.set_suggested_username(login)
            if login_locked:
                self.user_interface.login.set_lock_username_field(True)
        self.user_interface.login.set_cancel_allowed(optional)
            
        self.user_interface.login.reset_next_clicked()
        self.user_interface.login.reset_cancel_clicked()
        self.user_interface.login.set_state(self.user_interface.login.ID_STATE_CHANGE_LOGIN)
        self.user_interface.login.subscribe(self._change_password_credentials_subscriber)
        self.user_interface.login.display()

    def show_messagebox(self, msg):
        self.user_interface.modalmessage.set_text(msg)
        self.user_interface.modalmessage.set_cancel_visible(True)
        self.user_interface.modalmessage.set_cancel_allowed(False)
        self.user_interface.modalmessage.reset_next_clicked()
        self.user_interface.modalmessage.set_next_visible(True)
        self.user_interface.modalmessage.set_next_allowed(True)
        self.user_interface.modalmessage.display()
        
    def get_domain_name(self):
        try:
            import win32com.client, pythoncom
            pythoncom.CoInitialize()
            strComputer = "."
            objWMIService = win32com.client.Dispatch("WbemScripting.SWbemLocator")
            objSWbemServices = objWMIService.ConnectServer(strComputer,"root\cimv2")
            colItems = objSWbemServices.ExecQuery("Select * from Win32_ComputerSystem")
            assert len(colItems)>0, "Error getting domain from WMI"
            return colItems[0].Domain
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("UserSession.get_domain_name", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
        try:
            import win32com.client, win32api, pythoncom
            pythoncom.CoInitialize()
            root_DN = win32com.client.GetObject('LDAP://rootDSE').Get('defaultNamingContext')  # "DC=giritech,DC=com"
            
            canonical = win32security.TranslateName(root_DN, win32api.NameFullyQualifiedDN, win32api.NameCanonical)
            return canonical.partition("/")[0]            
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("UserSession.get_domain_name", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
            return None
        
    def save_login(self, login):
        self.runtime_env.set_settings_value('user_last_login', login)
        
    def try_sso(self, pkg_name=None, targetspn=None):
        if not GSS:
            return False
        print "Try sso %s" % pkg_name
        if not pkg_name:
            print "domain = %s" % self.get_domain_name()
            self.tunnelendpoint_remote('initiate_gss', domain=self.get_domain_name())
        else:
            self.gss = GSS(pkg_name, self.checkpoint_handler, targetspn=targetspn)
            if self.gss.credentials:
                sec_buffer=None
                self._process_gss_buffer(sec_buffer)
            else:
                self._show_login_prompt()
            

    def _process_gss_buffer(self, sec_buffer):
        print "_process_gss_buffer"
        err, sec_buffer = self.gss.authorize(sec_buffer)
        if err==sspicon.SEC_E_OK or err==sspicon.SEC_I_CONTINUE_NEEDED:
            try:
                self.tunnelendpoint_remote('receive_gss_buffer', sec_buffer=sec_buffer)
                return
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.error", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
        self._show_login_prompt()

    def receive_gss_buffer(self, sec_buffer):
        self._process_gss_buffer(sec_buffer)

    def gss_failed(self, err):
        print "gss_failed %s" % err
        self._show_login_prompt()

    def gss_worked(self, name):
        print "gss success %s" % name


