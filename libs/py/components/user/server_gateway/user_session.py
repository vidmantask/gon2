"""
This file contains the functionality related to user(s)
"""
from __future__ import with_statement
import weakref
from lib import checkpoint 
from components.communication import tunnel_endpoint_base

from components.plugin.server_gateway import plugin_socket_user
import components.user
import components.user.server_common.database_schema as database
from components.user.server_common import sink_component_id as sink_component_id
from components.database.server_common import database_api

import components.management_message.server_gateway.session_send
from components.management_message.server_common import from_timestamp_to_sink 
from components.communication import message

import datetime
import sys
from components.database.server_common.database_api import QuerySession

from components.user.server_common.gss_server import GSS
if sys.platform == 'win32':
    
    import sspicon


class IUserCallback(object):
    """
    API to user session
    """
    def get_user_id(self):
        """
        return an id to the user which is currently logged in as primary user
        """
        raise NotImplementedError


class IUserPluginCallback(object):
    """
    API to user plugins
    """
    def request_login(self, plugin):
        """
        A plugin requests a login from the user
        """
        raise NotImplementedError


    
class IUserAuthCallback(object):
    """
    API to auth session
    """
    
    def all_plugins_started(self):
        raise NotImplementedError
    
    def set_auth_callback(self, auth_session_callback):
        raise NotImplementedError

    def reset_auth_callback(self):
        raise NotImplementedError

    def auth_access_given(self):
        raise NotImplementedError
        

class GSSRemote(object):
    
    def __init__(self,
                 management_message_sender,
                 unique_session_id,
                 pkg_name, # Name of the package to be used.
                 checkpoint_handler,
                 principal = None):
        self.checkpoint_handler = checkpoint_handler
        self._management_message_sender = management_message_sender
        self.pkg_name = pkg_name
        self.principal = principal
             
    def initalize(self, auth_data=None):
        self._management_message_sender.message_remote('sink_initialize_gss', 
                                                       unique_session_id = unique_session_id,
                                                       pkg_name = self.pkg_name,
                                                       principal = self.principal,
                                                       auth_data = auth_data)
        
    
    def authorize(self, sec_buffer_in):
        self._management_message_sender.message_remote('sink_authorize_gss', 
                                                       unique_session_id = unique_session_id,
                                                       sec_buffer_in = sec_buffer_in)
            
        


class UserSession(IUserCallback, IUserPluginCallback,IUserAuthCallback, tunnel_endpoint_base.TunnelendpointSession):
    
    def __init__(self, environment, async_service, checkpoint_handler, tunnel_endpoint_tunnel, plugins, dictionary, access_log_server_session = None, management_message_session = None):
        tunnel_endpoint_base.TunnelendpointSession.__init__(self, async_service, checkpoint_handler, tunnel_endpoint_tunnel)
        self.checkpoint_handler = checkpoint_handler
        self.auth_session_callback = None
        self.received_auth_access_given = False
        self.dictionary = dictionary
        self.access_log_server_session = access_log_server_session
        self.login_issued = False
        self.login_requested_plugins = []
        self.authentication_plugin = None
        self.login_was_cancelled = False
        self.login = None
        self.password = None
        self.internal_user_id = None
        self.plugin_socket_user = plugin_socket_user.PluginSocket(environment.plugin_manager, plugins)
        self.login_error_info = None
        self.change_password_disabled = False
        self._plugins_ready = None
        self._ready = False
        
        self.logged_in_user = None
        
        self.new_user = False
        
        self.show_last_login = False
        self.show_last_user_directory = False

        self._require_full_login = False
        
        self._cached_user_results = dict()
        self._cached_group_results = dict()
        
        
        self.gss = None
        self.pkg_name = "NTLM"
        self.targetspn = None
        self.possible_authentication_plugin = None
        
        self.client_encoding = "N/A"
        self._management_message_sender = components.management_message.server_gateway.session_send.ManagementMessageSessionSender(sink_component_id, management_message_session)
        
        try:
            password_disabled_for_all = None
            for plugin in self.plugin_socket_user.get_plugins():
                plugin.set_user_session_callback(self)
                if not plugin.is_change_password_disabled():
                    password_disabled_for_all = False
                elif password_disabled_for_all is None:
                    password_disabled_for_all = True
            
            if not password_disabled_for_all is None:
                self.change_password_disabled = password_disabled_for_all
                
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("UserSession.error", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
            
            
            
    def session_start_and_connected(self):
        """
        This is called when tunnelendpoint_connected and session_start both has been called
        """ 
        with self.checkpoint_handler.CheckpointScope("start_session", "user_session", checkpoint.DEBUG):
            self.tunnelendpoint_remote('get_client_encoding')
            #self.tunnelendpoint_remote('show_login_prompt', msg="Please log in")

    def session_close(self):
        """Hard close of session. Called from main session when communication is terminated"""
        with self.checkpoint_handler.CheckpointScope("session_close", "user_session", checkpoint.DEBUG):
            self.auth_session_callback = None
            self.plugin_socket_user.reset_user_session_callback_all()
            self.reset_tunnelendpoint()

            self.dictionary = None
            self.access_log_server_session = None
            self.login_issued = False
            self.login_requested_plugins = []
            self.authentication_plugin = None
            self.login_was_cancelled = False
            self.login = None
            self.password = None
            self.internal_user_id = None
            self.plugin_socket_user = None
            self.login_error_info = None
            self.change_password_disabled = False
            self._plugins_ready = None
            self._ready = False
            
            self.logged_in_user = None
            
            self.new_user = False
            
            self.show_last_login = False
            self.show_last_user_directory = False
    
            self._require_full_login = False
            
            self._cached_user_results = dict()
            self._cached_group_results = dict()
            
            
            self.gss = None
            self.pkg_name = "NTLM"
            self.targetspn = None
            self.possible_authentication_plugin = None
            
            self.client_encoding = "N/A"
            self._management_message_sender = None

    def initialize_gss(self, gateway, pkg_name, targetspn):
        self.pkg_name = pkg_name
        self.targetspn = targetspn
        if gateway:
            if GSS:
                self.gss = GSS(self, self.pkg_name, self.checkpoint_handler)
        else:
            self.gss = GSSRemote(self._management_message_sender,
                                 self.access_log_server_session.get_unique_session_id(), 
                                 self.pkg_name, 
                                 self.checkpoint_handler)

    def get_client_encoding_cb(self, client_encoding):
        self.client_encoding = client_encoding


    def request_login(self, plugin):
        if not self.login_issued:
            self.tunnelendpoint_remote('show_login_prompt', msg="", change_password_enabled=not self.change_password_disabled)
            self.login_issued = True
            self.login_requested_plugins = [plugin]
        else:
            self.login_requested_plugins.append(plugin)
            if self.login_was_cancelled:
                self.login_cancelled() 
            elif not self.authentication_plugin and self.login:
                self.receive_login(self.login, self.password)
                
                
    def show_change_password_prompt(self, days_to_expiration):
        if days_to_expiration<0:
            msg = self.dictionary._("Your password has expired and must be changed")
            optional = False
        else:
            msg = self.dictionary._("Your password will expire in %s days. Do you want to change it now?") % days_to_expiration
            optional = True
        self.show_change_password_prompt1(msg, optional)

    def show_change_password_prompt1(self, msg, optional=False):
        self.tunnelendpoint_remote('show_change_password_prompt', msg=msg, login=self.login, optional=optional)

    def launch_change_password(self):
        if self.authentication_plugin:
            login = self.authentication_plugin.get_normalised_login(self.login)
            self.tunnelendpoint_remote('show_change_password', login=login)
        else:
            self.show_messagebox(self.dictionary._("No user information available"))
        
    def show_messagebox(self, msg):
        self.tunnelendpoint_remote('show_messagebox', msg=msg)
        
    def login_achieved(self):
        plugin = self.get_authenticated_plugin()
        external_user_id = plugin.get_current_user_id()
        if not self.auth_session_callback().check_element_lifetime("User", "%s.%s" % (plugin.plugin_name ,external_user_id)):
            self.authentication_plugin = None
            self.access_log_server_session.report_access_log_auth_ad_login_failed(self.login, "user_session", "2", "user account not active")                
            return
        if self.received_auth_access_given:
            self.auth_access_given()
        with database_api.QuerySession() as dbs:
            user = database.get_user(dbs, plugin.plugin_name, external_user_id)
            if user:
                self.new_user = False
                self.logged_in_user = user
                self.internal_user_id = user.internal_id
            else:
                self.new_user = True
                self.internal_user_id = database.get_unique_user_internal_id(dbs, plugin.plugin_name, external_user_id)

        if self.show_last_login or self.show_last_user_directory:
            normalised_login = plugin.get_normalised_login(self.login)
            if normalised_login:
                self.async_service.sleep_start_mutex(self.checkpoint_handler, self.get_mutex(), 0, 0, 0, self, 'send_login_to_client', normalised_login)
            
#        self.access_log_server_session.report_access_log_auth_ad_login(self.login, plugin.plugin_name, external_user_id)
        if not self.new_user or self.received_auth_access_given:   
            self._management_message_sender.message_remote('sink_user_login_received', 
                                                           is_new_user = self.new_user,
                                                           login = self.login,
                                                           unique_session_id = self.access_log_server_session.get_unique_session_id(),
                                                           internal_id = self.internal_user_id,
                                                           plugin_name = plugin.plugin_name,
                                                           external_user_id = external_user_id, 
                                                           login_time = from_timestamp_to_sink(datetime.datetime.now()))

        


    def receive_changed_password(self, old_password, new_password):
        if self.authentication_plugin:
            was_authenticated = self.authentication_plugin.is_authenticated()
            error_msg = self.authentication_plugin.receive_changed_password(old_password, new_password)
            if error_msg:
                self.show_change_password_prompt1(error_msg, optional=was_authenticated)
                return False
            is_authenticated_now = self.authentication_plugin.is_authenticated()
            if not was_authenticated and is_authenticated_now:            
                self.login_achieved()
                self.notify_auth_session()
            self.password = new_password
            self.show_messagebox(self.dictionary._("Your password has been changed"))
            return True
        return False


    def change_password_cancelled(self):
        if self.authentication_plugin:
            self.authentication_plugin.change_password_cancelled()
        
        
    def all_plugins_started(self):
        pass

    def auth_access_given(self):
        # When access have been given we can put up change password dialog if necessary
        if self.authentication_plugin:
            days_to_password_expiration = self.authentication_plugin.get_days_to_password_expiration()
            if not days_to_password_expiration is None and days_to_password_expiration < self.authentication_plugin.get_password_warning_days():
                self.show_change_password_prompt(days_to_password_expiration)

            if self.new_user:   
                plugin = self.get_authenticated_plugin()
                external_user_id = plugin.get_current_user_id()
                self._management_message_sender.message_remote('sink_user_login_received', 
                                                               is_new_user = True,
                                                               login = self.login,
                                                               unique_session_id = self.access_log_server_session.get_unique_session_id(),
                                                               internal_id = self.internal_user_id,
                                                               plugin_name = plugin.plugin_name,
                                                               external_user_id = external_user_id, 
                                                               login_time = from_timestamp_to_sink(datetime.datetime.now()))
                
        else:
            self.received_auth_access_given = True
                
        

    def set_auth_callback(self, auth_session_callback):
        self.auth_session_callback = weakref.ref(auth_session_callback)
        if auth_session_callback:
            self._require_full_login = auth_session_callback.require_full_login()

    def reset_auth_callback(self):
        self.auth_session_callback = None

    def start_auth(self):
        self.start_plugins_if()
        return []
    
    def add_plugins(self, plugin_set):
        for name in self.plugin_socket_user.get_plugin_names():
            plugin_set.add(name)
#            self.start_plugin_if(name)
        
    def start_plugin_if(self, plugin_name):
        plugin = self.plugin_socket_user.get_plugin(plugin_name)
        if plugin and not plugin.is_started():
            plugin.start()

    def start_plugins_if(self):
        for plugin_name in self.plugin_socket_user.get_plugin_names():
            self.start_plugin_if(plugin_name)

    def is_ready(self):
        if self.authentication_plugin or self.login_was_cancelled:
            return True
        if self._plugins_ready is None:
            for plugin in self.plugin_socket_user.get_plugins():
                if not plugin.is_ready():
                    return False
            self._plugins_ready = True
        return self._ready and self._plugins_ready
        
    def get_all_user_ids(self):
        with database_api.QuerySession as dbs:
            users = dbs.select(database.UserInfo)
            return [user.internal_id for user in users]

    
    def check_condition(self, internal_type_name, value_id, value_title):
        with self.checkpoint_handler.CheckpointScope("check_condition", "user_session", checkpoint.DEBUG, internal_type_name=internal_type_name, value_id=value_id, value_title=value_title):
            self.start_plugins_if()
    
            if self.login_was_cancelled:
                return False
            
            if not self.authentication_plugin: 
                if self.is_ready():
                    return False
                else:
                    return None
    
    
            if internal_type_name=="user":
                if self.authentication_plugin:
                    return value_id==self.internal_user_id
    
            elif internal_type_name=="group":
                result = self._cached_group_results.get(value_id)
                if not result is None:
                    return result
                
                with database_api.QuerySession() as dbs:
                    plugin_name, dot, external_group_id = value_id.partition(".")
                    if plugin_name != self.authentication_plugin.plugin_name:
                        result = False
                    else:
                        result = self.authentication_plugin.is_logged_in_member((external_group_id, value_title))
                    self._cached_group_results[value_id] = result
                    return result
                
            else:
                self.checkpoint_handler.Checkpoint("UserSession::check_condition", components.user.module_id, checkpoint.ERROR, message="Unknown condition type '%s'" % internal_type_name)
                return False
            

        
    
    def login_cancelled(self):
        with self.checkpoint_handler.CheckpointScope("login_cancelled", "user_session", checkpoint.INFO):
            self.login_was_cancelled = True
            all_plugins = self.plugin_socket_user.get_plugins()
            for plugin in all_plugins:
                plugin.login_cancelled()
            self.login_requested_plugins = []
            if self.auth_session_callback():
                self.notify_auth_session()
        
    def notify_auth_session(self):
        user_predicate_info = dict()
        if self.authentication_plugin:
            user_predicate_info['value'] = self.internal_user_id
            self.auth_session_callback().component_result_ready("user", "user", [user_predicate_info])
            user_groups = None
            try:
                user_groups = self.authentication_plugin.get_group_ids_for_logged_in_user()
                if not user_groups is None:
                    with QuerySession() as dbs:
                        n_groups = dbs.select_count(database.GroupInfo)
                    if n_groups < len(user_groups):
                        user_groups = None
                        
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("notify_auth_session (get user groups)", components.user.module_id, checkpoint.WARNING, etype, evalue, etrace)
            if user_groups is None:
                self.auth_session_callback().component_result_ready("user", "group")
            else:
                group_predicate_list = []
                for user_group_id in user_groups:
                    group_predicate_info = dict()
                    group_predicate_info['value'] = "%s.%s" % (self.authentication_plugin.plugin_name, user_group_id)
                    group_predicate_list.append(group_predicate_info)
                if group_predicate_list:
                    self.auth_session_callback().component_result_ready("user", "group", group_predicate_list)
            
        self.auth_session_callback().component_ready(self)        
        
    def attempt_login(self, plugin_name, internal_user_login):
        with self.checkpoint_handler.CheckpointScope("attempt_login", "user_session", checkpoint.DEBUG, plugin_name=plugin_name, internal_user_login=internal_user_login) as cps:
            plugin = self.plugin_socket_user.get_plugin(plugin_name)
            if plugin:
                try: 
                    plugin.receive_login(self.login, self.password, internal_user_login)
                    self.login_error_info = plugin.get_login_error()
                    if self.login_error_info:
                        cps.add_complete_attr(login_error_info=repr(self.login_error_info))
                        self.login_error_info = (plugin.plugin_name, self.login_error_info[0], self.login_error_info[1])
                except:
                    (etype, evalue, etrace) = sys.exc_info()
                    self.checkpoint_handler.CheckpointException("attempt_login", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
                    self.login_error_info = ("user_session", "4", "Internal error during login : %s" % evalue)
            else:
                self.checkpoint_handler.Checkpoint("attempt_login", components.user.module_id, checkpoint.ERROR, msg="User plugin not loaded", plugin_name=plugin_name)
                self.login_error_info = ("user_session", "3", "User plugin '%s' not loaded" % plugin_name)
                    

    def reissue_login(self, msg):
        self.login = None
        self.password = None
        self.tunnelendpoint_remote('show_login_prompt', msg=msg, change_password_enabled=not self.change_password_disabled)


    def send_login_to_client(self, normalised_login):
        
        if self.show_last_login:
            login = normalised_login
        elif self.show_last_user_directory:
            login = "@%s" % normalised_login.rsplit("@", 1)[-1]
        else:
            return
        self.tunnelendpoint_remote('save_login', login=login)
        
    def get_user(self, dbs, login):
        with self.checkpoint_handler.CheckpointScope("get_user", "user_session", checkpoint.DEBUG, login=login):
            login_lower = login.lower()
            return dbs.select_first(database.UserInfo, filter=database_api.and_(database_api.lower(database.UserInfo.user_login)==login_lower))
        
                

    def try_login(self, all_plugins):
        with database_api.QuerySession() as dbs:
            if '@' in self.login:
                user = self.get_user(dbs, self.login)
                if user:
                    users_found = [user]
                else:
                    users_found = []
            else:
                users_found = []
                for plugin in all_plugins:
                    normalised_login = plugin.get_normalised_login(self.login)
                    if normalised_login:
                        user = self.get_user(dbs, normalised_login)
                        if user:
                            users_found.append(user) # get normalized user login from each plugin and see if that matches
            
            if users_found:
                if len(users_found) > 1:
                    self.login_error_info = "user_session", "2", "Found %s matches for user login '%s'" % (len(users_found), self.login)
                    self.checkpoint_handler.Checkpoint("UserSession::receive_login", components.user.module_id, checkpoint.ERROR, message="Found %s matches for user login '%s'" % (len(users_found), self.login))
                else:
                    user = users_found[0]
                    self.attempt_login(user.user_plugin, user.internal_user_login)
                    plugin = self.plugin_socket_user.get_plugin(user.user_plugin)
                    if not plugin:
                        self.login_error_info = "user_session", "3", "User plugin '%s' not loaded" % user.user_plugin
                        self.checkpoint_handler.Checkpoint("UserSession::receive_login", components.user.module_id, checkpoint.ERROR, message="User plugin '%s' not loaded" % user.user_plugin)
                    elif not (plugin.is_authenticated() or plugin.change_password_needed()):
                        # Check to see if user login have changed since last visit
                        internal_user_login = plugin.lookup_user(self.login)
                        if internal_user_login and internal_user_login != user.internal_user_login:
                            self.attempt_login(user.user_plugin, internal_user_login)
            else:
                # Now get each plugin to try and find the user. If exactly one user is found we try to login
                for plugin in all_plugins:
                    internal_user_login = plugin.lookup_user(self.login)
                    if internal_user_login:
                        users_found.append((plugin.plugin_name, internal_user_login))
                
                if users_found:
                    if len(users_found) > 1:
                        self.login_error_info = "user_session", "2", "Found %s matches for user '%s'" % (len(users_found), self.login)
                        self.checkpoint_handler.Checkpoint("UserSession::receive_login", components.user.module_id, checkpoint.ERROR, message="Found %s matches for user login '%s'" % (len(users_found), self.login))
                        for plugin_name, internal_user_login in users_found:
                            self.checkpoint_handler.Checkpoint("UserSession::receive_login:user_match", components.user.module_id, checkpoint.ERROR, plugin_name=plugin_name, internal_user_login=internal_user_login)
                    
                    else:
                        plugin_name, internal_user_login = users_found[0]
                        self.attempt_login(plugin_name, internal_user_login)
                else:
                    self.login_error_info = "user_session", "1", "The user '%s' could not be found" % self.login

    def receive_login(self, login, password, requestedpassword=None):
        with self.checkpoint_handler.CheckpointScope("receive_login", "user_session", checkpoint.INFO, login=login):
            all_plugins = self.plugin_socket_user.get_plugins()
            if not self.authentication_plugin and login:
                if self._require_full_login and not '@' in login:
                    self.reissue_login(self.dictionary._("Full User name required. Please enter full user name : %s@<your domain>" % login))
                    return
                    
                
                self.login = login
                self.password = password
                
                self.try_login(all_plugins)
                
                for plugin in all_plugins:
                    if plugin.is_authenticated() or plugin.change_password_needed():
                        self.authentication_plugin = plugin
                    else:
                        plugin.login_cancelled()
                        
            else:
                for plugin in all_plugins:
                    plugin.login_cancelled()
                    
            
            if self.authentication_plugin is None:
                if self.access_log_server_session:
                    if self.login_error_info:
                        error_source, error_code, error_message = self.login_error_info 
                        self.access_log_server_session.report_access_log_auth_ad_login_failed(login, error_source, error_code, error_message)
                    else:
                        self.access_log_server_session.report_access_log_auth_ad_login_failed(login)
            elif self.authentication_plugin.is_authenticated():
                self.login_achieved()
    
            if self.authentication_plugin and (requestedpassword or self.authentication_plugin.change_password_needed()):
                if requestedpassword:
                    self.receive_changed_password(self.password, requestedpassword)
                else:
                    self.show_change_password_prompt(-1)
    
            self._ready = True
            self.login_requested_plugins = []
                    
            if self.auth_session_callback():
                if not self.authentication_plugin or not self.authentication_plugin.change_password_needed():
                    self.notify_auth_session()
                
                

    def get_authenticated_plugin(self):
        if not self.authentication_plugin:
            for plugin in self.login_requested_plugins:
                if plugin.is_authenticated():
                    self.authentication_plugin = plugin
                    return plugin
            
        if self.authentication_plugin and self.authentication_plugin.is_authenticated():
            return self.authentication_plugin
        
        return None
        

    def get_user_id(self):
        """
        return an id to the user which is currently logged in as primary user
        """
        return self.internal_user_id
    
    def get_attribute(self, attribute_name):
        plugin = self.get_authenticated_plugin()
        if plugin:
            try:
                if attribute_name == 'login':
                    if self.login:
                        return self.login.split("@")[0]
                    else:
                        return self.login
                elif attribute_name == 'password':
                    return self.password
                elif attribute_name == 'password_base64':
                    try:
                        return self.password.encode(self.client_encoding).encode('base64').strip() # safe encoding and slightly obfuscated
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("UserSession.get_attribute", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
                        self.checkpoint_handler.Checkpoint("encoding error", components.user.module_id, checkpoint.ERROR, login=self.login)
                        return ""
                
                value = plugin.get_attribute(attribute_name)
                if value is None:
                    user_id = self.get_user_id()
                    value = database.get_attribute(user_id, attribute_name)
                if value is None:
                    self.checkpoint_handler.Checkpoint("UserSession::get_attribute", components.user.module_id, checkpoint.WARNING, message="Unable to get attribute '%s' for logged in user" % attribute_name)
                return value
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.get_attribute", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
                return None
        if attribute_name in ['login','password','password_base64']:
            return ''
        return None
        
        
    def initiate_gss(self, domain=None):
        if not self.gss or not self.pkg_name:
            self.tunnelendpoint_remote('gss_failed', err=-2)
            return
        print "initiate_gss %s" % domain
        all_plugins = [plugin for plugin in self.plugin_socket_user.get_plugins() if plugin.is_domain()]
        if len(all_plugins)==0:
            self.tunnelendpoint_remote('gss_failed', err=-3)
            return
        elif len(all_plugins)>1:
            if domain:
                for plugin in all_plugins:
                    if plugin.is_domain(domain):
                        self.possible_authentication_plugin = plugin
                        break
            if not self.possible_authentication_plugin:
                self.tunnelendpoint_remote('gss_failed', err=-3)
                return
        else:
            self.possible_authentication_plugin = all_plugins[0]
            
        print "initialize gss %s" % self.pkg_name
        auth_data = self.possible_authentication_plugin.get_auth_data()
        self.gss.initalize(auth_data)

    def initialize_gss_reply(self, credentials, credentials_expiry):
        if not credentials:
            self.tunnelendpoint_remote('gss_failed', err=-1)
        else:
            print "call try_sso client"
            self.tunnelendpoint_remote('try_sso', pkg_name=self.pkg_name, targetspn=self.targetspn)
        

    def receive_gss_buffer(self, sec_buffer):
        try:
            self.gss.authorize(sec_buffer)
        except:
            (etype, evalue, etrace) = sys.exc_info()
            self.checkpoint_handler.CheckpointException("UserSession.error", components.user.module_id, checkpoint.ERROR, etype, evalue, etrace)
            self.tunnelendpoint_remote('gss_failed', err=-1)

    def authorize_gss_failed_reply(self, err):
        self.tunnelendpoint_remote('gss_failed', err=err)
       
    def authorize_gss_worked_reply(self, nt_user_name, user_sid, group_SIDs=None):
        if not nt_user_name and not user_sid:
            self.tunnelendpoint_remote('gss_failed', err=-4)
        else:
            if not self.possible_authentication_plugin.sso_authenticate_user(nt_user_name, user_sid, group_SIDs):
                self.tunnelendpoint_remote('gss_failed', err=-5)
            else:
                all_plugins = self.plugin_socket_user.get_plugins()
                for plugin in all_plugins:
                    if not plugin.is_authenticated():
                        plugin.login_cancelled()
            
                self.login_achieved()
                self._ready = True
                self.login = nt_user_name
                self.login_requested_plugins = []
                self.notify_auth_session()
                self.tunnelendpoint_remote('gss_worked', name=nt_user_name)
                
        

    def authorize_gss_continue_reply(self, sec_buffer_out):
        self.tunnelendpoint_remote('receive_gss_buffer', sec_buffer=sec_buffer_out)
        
    def validate_user(self, username, password):
        self.login = username
        self.password = password
        all_plugins = self.plugin_socket_user.get_plugins()

        self.try_login(all_plugins)
        
        for plugin in all_plugins:
            if plugin.is_authenticated():
                self.authentication_plugin = plugin
                break
        if self.authentication_plugin:
            plugin = self.authentication_plugin
            external_user_id = plugin.get_current_user_id()
            with database_api.QuerySession() as dbs:
                user = database.get_user(dbs, plugin.plugin_name, external_user_id)
                if user:
                    self.internal_user_id = user.internal_id
                else:
                    self.internal_user_id = database.get_unique_user_internal_id(dbs, plugin.plugin_name, external_user_id)
                
                return self.internal_user_id
        else:
            return None
            
        
        