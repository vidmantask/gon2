'''
Created on 31/01/2011

@author: pwl
'''

import plugin_types.server_config.plugin_type_config_specification as ptcs
import lib.checkpoint 

from components.plugin.server_common.config_api_lib import ConfigTemplateSpec

class PluginSpecification(object):
    
    def __init__(self, plugin):
        self.plugin = plugin
        self.config_edit = plugin.get_config_specifications(UserDirectoriesSpecification.ID)
        self.config_new = plugin.get_create_config_specification(UserDirectoriesSpecification.ID)
        
    
    def add_config_specs(self, config_spec):
        if self.config_new or self.config_edit:
            enabled = self.plugin.is_enabled()
            config_spec.add_field('%s.enabled' % self.plugin.ID, 'Enabled', ConfigTemplateSpec.VALUE_TYPE_BOOLEAN, value=enabled)
            details = config_spec.add_details(self.plugin.TITLE, internal_name=self.plugin.ID, details_type="wizard_type")
            if self.plugin.is_singleton():
                details.set_attribute_delete_enabled(False)
            if self.config_new:
                if isinstance(self.config_new, list):
                    for config_new in self.config_new:
                        config_spec.add_config_spec_to_details(details, config_new.get_attribute_title(), UserDirectoriesSpecification.ID, config_new.get_attribute_name(), sub_type=self.plugin.ID)
                else:
                    config_spec.add_config_spec_to_details(details, self.config_new.get_attribute_title(), UserDirectoriesSpecification.ID, self.config_new.get_attribute_name(), sub_type=self.plugin.ID)
            for name, config_edit_spec in self.config_edit.items():
                config_spec.add_config_spec_to_details(details, config_edit_spec.get_attribute_title(), UserDirectoriesSpecification.ID, name, sub_type=self.plugin.ID, element_id=name)
        


class UserDirectoriesSpecification(ptcs.PluginTypeConfigSpecification):
    ID = 'user_dirctories_config'
    TITLE = 'User directory setup' 
    DESCRIPTION = 'Setup User directories' 
    
    def __init__(self, checkpoint_handler, server_configuration, config_specification_socket):
        ptcs.PluginTypeConfigSpecification.__init__(self, checkpoint_handler, server_configuration, "User Config", 'server_config_server_management')
        self.server_configuration = server_configuration
        self.config_specification_socket = config_specification_socket
        self._plugin_specifications = dict()

        plugins = self.config_specification_socket.get_plugins_sorted()
        plugin_list = [plugin for plugin in plugins if plugin.has_user_config()]
        for plugin in plugin_list:
            plugin_specification = PluginSpecification(plugin)
            if plugin_specification.config_edit or plugin_specification.config_new:
                self._plugin_specifications[plugin.ID] = plugin_specification
        
    def has_config(self):
        return self._plugin_specifications
            

    def get_config_specification(self):
        config_spec = ConfigTemplateSpec()
        config_spec.init(self.ID, self.TITLE, self.DESCRIPTION, entity_type="User")

#        plugin_list = sorted(plugins, cmp=self._compare_plugins)
        for plugin_spec in self._plugin_specifications.values():
            plugin_spec.add_config_specs(config_spec)
        
        
#        config_spec.set_values_from_object(self.server_configuration.configuration_server_management)
        
        return config_spec.get_template()
    
    def get_template(self, config_template_spec):
        sub_type = config_template_spec.get_element_sub_type()
        element_id = None
        template_name = None
        plugin_spec = self._plugin_specifications.get(sub_type)
        if plugin_spec:
            element_id = config_template_spec.get_element_id()
            if element_id:
                return plugin_spec.config_edit.get(element_id)
            else:
                if isinstance(plugin_spec.config_new, list):
                    template_name = config_template_spec.get_element_template_name()
                    for template in plugin_spec.config_new:
                        if template.get_attribute_name()==template_name:
                            return template
                else:
                    return plugin_spec.config_new
                
        self.checkpoint_handler.Checkpoint("get_template", "user_config", lib.checkpoint.ERROR, msg="Unable to find template", element_id=element_id, sub_type=sub_type, template_name=template_name)
        raise Exception("Internal error finding template")
            

    def delete_template(self, config_template_spec):
        sub_type = config_template_spec.get_element_sub_type()
        element_id = config_template_spec.get_element_id()
        plugin_spec = self._plugin_specifications.get(sub_type)
        if plugin_spec:
            try:
                plugin_spec.config_edit.pop(element_id)
            except KeyError:
                pass
        return True

    def save_template(self, config_template, element_id=None, sub_type=None):
        plugin_spec = self._plugin_specifications.get(sub_type)
        if plugin_spec:
            try:
                new_name = plugin_spec.plugin.check_template(config_template)
                if element_id and new_name != element_id:
                    try:
                        plugin_spec.config_edit.pop(element_id)
                    except KeyError:
                        pass
                plugin_spec.config_edit[new_name] = config_template
            except Exception, e:
                return self.create_save_template_response(False, message=e)
            
            config_spec = ConfigTemplateSpec(config_template)
            spec = config_spec.create_config_template_spec(config_template.get_attribute_title(), UserDirectoriesSpecification.ID, new_name, sub_type=plugin_spec.plugin.ID, element_id=new_name)
            return self.create_save_template_response(True, spec=spec)
        self.checkpoint_handler.Checkpoint("get_template", "user_config", lib.checkpoint.ERROR, msg="Unable to save template", element_id=element_id, sub_type=sub_type)
        raise Exception("Internal error saving template")
    

    def save(self, template):
        if template.get_attribute_name() != self.ID:
            return None
        
        config_spec = ConfigTemplateSpec(template)
        for plugin_spec in self._plugin_specifications.values():
            plugin = plugin_spec.plugin
            if plugin_spec.config_new or plugin_spec.config_edit:
                enabled = config_spec.get_value("%s.enabled" % plugin.ID)
                plugin.save_data(enabled, plugin_spec.config_edit.values())
            else:
                plugin.save_data(False, [])
        return self.create_save_response(True)
    
    def finalize(self):
        for plugin_spec in self._plugin_specifications.values():
            plugin_spec.plugin.finalize()
            
            
    def test_config(self, template, test_type=None, sub_type=None):
        plugin_spec = self._plugin_specifications.get(sub_type)
        if plugin_spec:
            return plugin_spec.plugin.test_config(template, test_type)
        self.checkpoint_handler.Checkpoint("test_config", "user_config", lib.checkpoint.ERROR, msg="Unable to find plugin", sub_type=sub_type, test_type=test_type)
        return False, "Internal Error: Unknown plugin '%s'" % sub_type
    
        
