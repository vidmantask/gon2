from __future__ import with_statement
import components.database.server_common.database_api as database_api


creator = database_api.SchemaFactory.get_creator("user_component")



table_user_info = creator.create_table("user",
                                     database_api.Column('internal_id', database_api.String(512), nullable=False, index=True),
                                     database_api.Column('external_user_id', database_api.String(256), nullable=False, index=True),
                                     database_api.Column('user_plugin', database_api.String(256), nullable=False, index=True),
                                     database_api.Column('user_login', database_api.String(512), nullable=False, index=True),
                                     database_api.Column('internal_user_login', database_api.String(1024), nullable=False),
                                     database_api.Column('user_name', database_api.String(512)),
                                     database_api.Column('email', database_api.String(256)),
                                     database_api.Column('old_my_pc_1', database_api.String(1000)),
                                     database_api.Column('old_my_pc_2', database_api.String(1000)),
                                     database_api.Column('last_login', database_api.DateTime),
                                        )

table_user_pc = creator.create_table("user_pc",
                                     database_api.Column('user_id', database_api.Integer, nullable=False),
                                     database_api.Column('line_no', database_api.Integer, nullable=False),
                                     database_api.Column('my_pc', database_api.String(256)),
                                     database_api.Column('mac_address', database_api.String(64)),
                                     database_api.Column('port', database_api.Integer),
                                     database_api.Column('last_login', database_api.DateTime),
                                        )


table_group_info = creator.create_table("group",
                                     database_api.Column('internal_id', database_api.String(512), nullable=False, index=True),
                                     database_api.Column('external_group_id', database_api.String(256), nullable=False, index=True),
                                     database_api.Column('group_plugin', database_api.String(256), nullable=False, index=True),
                                        )
 

creator.add_foreign_key_constraint(table_user_pc, "user_id", table_user_info)

creator.add_unique_constraint(table_user_info, "external_user_id", "user_plugin")
creator.add_unique_constraint(table_user_info, "user_login")
creator.add_unique_constraint(table_user_info, "internal_id")
creator.add_unique_constraint(table_user_pc, "user_id", "line_no")

creator.add_unique_constraint(table_group_info, "external_group_id", "group_plugin")
creator.add_unique_constraint(table_group_info, "internal_id")

database_api.SchemaFactory.register_creator(creator)
        
class UserInfo(database_api.PersistentObject):
    
    def get_attribute(self, attr_name):
        if attr_name:
            try:
                return getattr(self, attr_name)
            except AttributeError:
                name, underscore, line_no = attr_name.rpartition("_")
                if name and line_no.isdigit():
                    line_no = int(line_no)
                    for user_pc in self.user_pcs:
                        if user_pc.line_no == line_no:
                            return getattr(user_pc, name, None)
                             
        return None
    
    def delete(self, transaction):
        for pc in self.user_pcs:
            transaction.delete(pc)
        transaction.delete(self)
        

class GroupInfo(database_api.PersistentObject):
    pass

class UserPC(database_api.PersistentObject):
    pass


database_api.mapper(UserPC, table_user_pc)
database_api.mapper(UserInfo, table_user_info, 
                    relations = [database_api.Relation(UserPC, 'user_pcs')])
database_api.mapper(GroupInfo, table_group_info,)

def get_user(dbs, plugin_name, user_id):
    return dbs.select_first(UserInfo, filter=database_api.and_(UserInfo.user_plugin==plugin_name, UserInfo.external_user_id==user_id))

def get_user_by_internal_id(dbs, internal_id):
    return dbs.select_first(UserInfo, filter=(UserInfo.internal_id==internal_id))

def get_attribute(internal_id, attr_name):
    with database_api.ReadonlySession() as dbs:
        user_info = get_user_by_internal_id(dbs, internal_id)
        return user_info.get_attribute(attr_name)
    return None 
                
def _get_unique_internal_id(dbs, plugin_name, external_user_id, get_user_by_internal_id):
    internal_id = "%s.%s" % (plugin_name, external_user_id)
    return internal_id

def get_unique_user_internal_id(dbs, plugin_name, external_user_id):
    return _get_unique_internal_id(dbs, plugin_name, external_user_id, get_user_by_internal_id)

def get_unique_group_internal_id(dbs, plugin_name, external_user_id):
    return _get_unique_internal_id(dbs, plugin_name, external_user_id, get_group_by_internal_id)

def check_unique_user_attributes(dbs, user):
    other_user = get_user(dbs, user.user_plugin, user.external_user_id)
    if other_user:
        raise Exception("A user from this user directory with identical id is already registered. The user's login is '%s'" % other_user.internal_user_login)
    other_user = dbs.select_first(UserInfo, filter=UserInfo.user_login==user.user_login)
    if other_user:
        raise Exception("A user with this login is already registered. The user's login is '%s'" % other_user.user_login)
            
    
def get_group(dbs, plugin_name, group_id):
    return dbs.select_first(GroupInfo, filter=database_api.and_(GroupInfo.group_plugin==plugin_name, GroupInfo.external_group_id==group_id))

def get_group_by_internal_id(dbs, internal_id):
    return dbs.select_first(GroupInfo, filter=(GroupInfo.internal_id==internal_id))
        
    