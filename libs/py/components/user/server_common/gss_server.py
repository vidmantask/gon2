'''
Created on 31/08/2011

@author: pwl
'''
import sys
from lib import checkpoint 

if sys.platform == 'win32':
    
    # code "stolen" from http://nullege.com/codes/show/src%40p%40y%40pywin32-HEAD%40win32%40lib%40sspi.py/132/win32security.InitializeSecurityContext/python
    import win32security, sspicon
    
    class GSS(object):
        
        def __init__(self,
                     callback,
                     pkg_name, # Name of the package to used.
                     checkpoint_handler,
                     principal = None,
                     scflags=None, # security context flags
                     datarep=sspicon.SECURITY_NETWORK_DREP):
            self.checkpoint_handler = checkpoint_handler
            if scflags is None:
                scflags = sspicon.ASC_REQ_INTEGRITY|sspicon.ASC_REQ_SEQUENCE_DETECT|\
                          sspicon.ASC_REQ_REPLAY_DETECT|sspicon.ASC_REQ_CONFIDENTIALITY | sspicon.ASC_REQ_DELEGATE
        
            self.callback = callback
            self.scflags=scflags
            self.datarep=datarep
            self.ctxt_attr = None
            self.ctxt_expiry = None
            self.ctxt = None
            self.pkg_name = pkg_name
            self.principal = principal
            
            self.done = False
                 
        def initalize(self, auth_data=None):
            try:
                self.pkg_info = win32security.QuerySecurityPackageInfo(self.pkg_name)
#                principal = "devtest.giritech.com"
#                auth_data = ("admin01@devtest.giritech.com", "devtest.giritech.com", "admin01")
                res = win32security.AcquireCredentialsHandle(self.principal, 
                                                             self.pkg_info['Name'],
                                                             sspicon.SECPKG_CRED_INBOUND,
                                                             None, 
                                                             auth_data)
                self.credentials, self.credentials_expiry = res
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.error", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                self.credentials = self.credentials_expiry = None
                self.done = True
            
            self.callback.initialize_gss_reply(self.credentials, self.credentials_expiry)
            
        
        def authorize(self, sec_buffer_in):
            try:
                if sec_buffer_in is not None and type(sec_buffer_in) != win32security.PySecBufferDescType:
                    # User passed us the raw data - wrap it into a SecBufferDesc
                    sec_buffer_new=win32security.PySecBufferDescType()
                    tokenbuf=win32security.PySecBufferType(self.pkg_info['MaxToken'],
                                                         sspicon.SECBUFFER_TOKEN)
                    tokenbuf.Buffer=sec_buffer_in
                    sec_buffer_new.append(tokenbuf)
                    sec_buffer_in = sec_buffer_new
                sec_buffer_out=win32security.PySecBufferDescType()
                tokenbuf=win32security.PySecBufferType(self.pkg_info['MaxToken'], sspicon.SECBUFFER_TOKEN)
                sec_buffer_out.append(tokenbuf)
                ## input context handle should be NULL on first call
                ctxtin=self.ctxt
                if self.ctxt is None:
                    self.ctxt=win32security.PyCtxtHandleType()
                err, attr, exp=win32security.AcceptSecurityContext(self.credentials,
                                                                   ctxtin,
                                                                   sec_buffer_in,
                                                                   self.scflags,
                                                                   self.datarep,
                                                                   self.ctxt,
                                                                   sec_buffer_out)
                # Stash these away incase someone needs to know the state from the
                # final call.
                self.ctxt_attr = attr
                self.ctxt_expiry = exp
          
                if err in (sspicon.SEC_I_COMPLETE_NEEDED,sspicon.SEC_I_COMPLETE_AND_CONTINUE):
                    self.ctxt.CompleteAuthToken(sec_buffer_out)
                self.authenticated = err == 0
                
                if err!=sspicon.SEC_E_OK and err!=sspicon.SEC_I_CONTINUE_NEEDED:
                    self.done = True
                    self.callback.authorize_gss_failed_reply(err)
                elif err==sspicon.SEC_I_CONTINUE_NEEDED:
                    self.callback.authorize_gss_continue_reply(sec_buffer_out[0].Buffer)
                else:
                    try:
                        user_name = self.ctxt.QueryContextAttributes(sspicon.SECPKG_ATTR_NAMES)
                        print user_name
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("SECPKG_ATTR_TARGET_INFORMATION", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                        user_name = None

                    group_SIDs = []
                    try:
                        token = self.ctxt.QueryContextAttributes(sspicon.SECPKG_ATTR_ACCESS_TOKEN );
                        user_info = win32security.GetTokenInformation(token, win32security.TokenUser)
                        user_SID = win32security.ConvertSidToStringSid(user_info[0])
                        print user_SID
                        try:
                            for sid in win32security.GetTokenInformation(token, win32security.TokenGroups):
                                try:
                                    group_SIDs.append(win32security.ConvertSidToStringSid(sid[0]))
                                except:
                                    (etype, evalue, etrace) = sys.exc_info()
                                    self.checkpoint_handler.CheckpointException("GetTokenInformation", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                            print "groups: ", group_SIDs
                        except:
                            (etype, evalue, etrace) = sys.exc_info()
                            self.checkpoint_handler.CheckpointException("GetTokenInformation", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                            group_SIDs = None
                    except:
                        (etype, evalue, etrace) = sys.exc_info()
                        self.checkpoint_handler.CheckpointException("SECPKG_ATTR_TARGET_INFORMATION", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                        user_SID = None
                        
                    self.done = True
                    self.callback.authorize_gss_worked_reply(user_name, user_SID, group_SIDs)
                
            except:
                (etype, evalue, etrace) = sys.exc_info()
                self.checkpoint_handler.CheckpointException("UserSession.error", "gss_server", checkpoint.ERROR, etype, evalue, etrace)
                self.done = True
                self.callback.authorize_gss_failed_reply(-1)
                
            

else:
    GSS = None

