"""
This file contains the database schema for the endpoint component
"""
from __future__ import with_statement
from components.database.server_common import schema_api
from components.database.server_common import database_api

import hashlib
import datetime
import uuid
import sys

import lib.encode
import lib.checkpoint

dbapi = schema_api.SchemaFactory.get_creator("endpoint_component")


def connect_to_database(environment):
    dbapi.bind(environment.get_default_database())


table_endpoint_info = dbapi.create_table("endpoint_info",
                                         schema_api.Column('hash_key', schema_api.String(100)),
                                         schema_api.Column('first_seen', schema_api.DateTime()),
                                         schema_api.Column('last_seen', schema_api.DateTime()),
                                         schema_api.Column('create_session_id', schema_api.String(100))
                                         )

table_endpoint_access_log = dbapi.create_table("endpoint_access_log",
                                               schema_api.Column('hash_key', schema_api.String(100)),
                                               schema_api.Column('unique_session_id', schema_api.String(100), index=True)
                                            )

table_endpoint_attribute = dbapi.create_table("endpoint_attribute",
                                              schema_api.Column('endpoint_info_id', schema_api.Integer()),
                                              schema_api.Column('plugin_name', schema_api.String(100)),
                                              schema_api.Column('attribute_name', schema_api.String(100)),
                                              schema_api.Column('attribute_value', schema_api.Text())
                                            )

table_registered_endpoint = dbapi.create_table("registered_endpoint",
                                         schema_api.Column('rule_id', schema_api.Integer()),
                                         schema_api.Column('token_serial', schema_api.String(200), nullable=False, index=True),
                                         schema_api.Column('token_plugin', schema_api.String(100), nullable=False, index=True),
                                         schema_api.Column('deployed', schema_api.DateTime()),
                                         schema_api.Column('last_seen', schema_api.DateTime()),
                                         )

table_registered_endpoint_attribute = dbapi.create_table("registered_endpoint_attribute",
                                              schema_api.Column('endpoint_id', schema_api.Integer(), nullable=False, index=True),
                                              schema_api.Column('plugin_name', schema_api.String(100)),
                                              schema_api.Column('attribute_name', schema_api.String(100)),
                                              schema_api.Column('attribute_value', schema_api.Text())
                                            )

table_registered_endpoint_hardware_check = dbapi.create_table("registered_endpoint_hardware_check",
                                              schema_api.Column('endpoint_id', schema_api.Integer(), nullable=False, index=True),
                                              schema_api.Column('plugin_name', schema_api.String(100)),
                                            )

table_failed_enrollment = dbapi.create_table("failed_enrollment",
                                              schema_api.Column('token_serial', schema_api.String(200)),
                                              schema_api.Column('plugin_name', schema_api.String(100)),
                                              schema_api.Column('unique_session_id', schema_api.String(100)),
                                              schema_api.Column('error_message', schema_api.String(200)),
                                              schema_api.Column('timestamp', schema_api.DateTime()),
                                            )

dbapi.add_foreign_key_constraint(table_endpoint_attribute, 'endpoint_info_id', table_endpoint_info, ondelete="CASCADE")
dbapi.add_foreign_key_constraint(table_registered_endpoint_attribute, 'endpoint_id', table_registered_endpoint, ondelete="CASCADE")
dbapi.add_foreign_key_constraint(table_registered_endpoint_hardware_check, 'endpoint_id', table_registered_endpoint, ondelete="CASCADE")


schema_api.SchemaFactory.register_creator(dbapi)



class EndpointInfo(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_endpoint_info)

class EndpointAccessLog(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_endpoint_access_log)

class EndpointAttribute(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_endpoint_attribute)



class RegisteredEndpointAttribute(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_registered_endpoint_attribute)


class HardwareCheckPlugin(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_registered_endpoint_hardware_check)

class RegisteredEndpointBase(database_api.QueryResultBase):
    
    def init(self):
        self.attributes = []
        self.hardware_check_plugins = []
        self.attribute_dict = None
        self.token = None
    
    def get_attribute_dict(self):
        if self.attribute_dict is None:
            self.attribute_dict = dict()
            for attribute in self.attributes:
                value_set = set(self.attribute_dict.get(attribute.attribute_name, []))
                value_set.add(attribute.attribute_value)
                self.attribute_dict[attribute.attribute_name] = list(value_set)
        return self.attribute_dict
    

class RegisteredEndpoint(RegisteredEndpointBase):

    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_registered_endpoint, 
                          relations = [database_api.Relation(RegisteredEndpointAttribute, 'attributes', backref_property_name='endpoint'),
                                       database_api.Relation(HardwareCheckPlugin, 'hardware_check_plugins', backref_property_name='endpoint')])

    @database_api.reconstructor_method
    def init_on_load(self):
        self.attribute_dict = None

    def __init__(self, **kwargs):
#        super(database_api.PersistentObject,self).__init__(**kwargs)
        self.attribute_dict = None

    def _copy_attributes(self, endpoint_info, filter, db_session):
        attributes = db_session.select(EndpointAttribute, filter = EndpointAttribute.endpoint_info_id == endpoint_info.id)
        for attribute in attributes:
            attribute_blacklist = filter.get(attribute.attribute_name)
            blacklisted = False
            if attribute_blacklist:
                for value in attribute_blacklist:
                    if attribute.attribute_value.startswith(value):
                        blacklisted = True
                        break
            if not blacklisted:
                new_attr = db_session.add(RegisteredEndpointAttribute())
                new_attr.plugin_name = attribute.plugin_name
                new_attr.attribute_name = attribute.attribute_name
                new_attr.attribute_value = attribute.attribute_value
                new_attr.endpoint = self

        
    def update_attributes(self, unique_session_id, filter, db_session):
        endpoint_info = db_session.select_first(EndpointInfo, filter=EndpointInfo.create_session_id==unique_session_id)
        if endpoint_info:
            db_session.delete_sequence(self.attributes)
            self._copy_attributes(endpoint_info, filter, db_session) 
                
        
    def create_attributes(self, unique_session_id, filter, db_session):
        endpoint_access = db_session.select_first(EndpointAccessLog, filter=EndpointAccessLog.unique_session_id==unique_session_id)
        if endpoint_access:
            endpoint_info = db_session.select_first(EndpointInfo, filter=EndpointInfo.hash_key==endpoint_access.hash_key)
            if endpoint_info:
                self._copy_attributes(endpoint_info, filter, db_session)
                return
        raise Exception("No endpoint information found for session id '%s'" % unique_session_id) 
                
    def delete(self, transaction):
        transaction.delete_sequence(self.attributes)
        transaction.delete_sequence(self.hardware_check_plugins)
        transaction.delete(self)
    
            
            
class FailedEnrollment(database_api.PersistentObject):
    @classmethod
    def map_to_table(cls):
        schema_api.mapper(cls, table_failed_enrollment)
        

EndpointInfo.map_to_table()
EndpointAccessLog.map_to_table()
EndpointAttribute.map_to_table()
RegisteredEndpointAttribute.map_to_table()
HardwareCheckPlugin.map_to_table()
RegisteredEndpoint.map_to_table()
FailedEnrollment.map_to_table()




#def get_endpoint_info_attributes(dbs, endpoint_info_id):
#    with database_api.ReadonlySession() as dbs:
#        info_attributes = []
#        db_attributes = dbs.select(EndpointAttribute, filter=EndpointAttribute.endpoint_info_id==endpoint_info_id)
#        for db_attribute in db_attributes:
#            pass

def get_registered_endpoint(endpoint_id, db_session):
    return db_session.get(RegisteredEndpoint, endpoint_id)

def get_registered_endpoints_by_token_serial(token_serial, db_session):
    return db_session.select(RegisteredEndpoint, RegisteredEndpoint.token_serial==token_serial)

def get_registered_endpoints_by_token(token_serial, token_plugin, db_session):
    return db_session.select_first(RegisteredEndpoint, filter=database_api.and_(RegisteredEndpoint.token_serial==token_serial,
                                                                                RegisteredEndpoint.token_plugin==token_plugin))


CALC_HASH_KEY_WHITE_LIST = ['machine_gmid_user', 'machine_local_account', 'machine_name', 'machine_manufacture', 'machine_domain', 'machine_model', 'machine_name', 'machine_serial', 
                            'net_ip', 'net_mac',
                            'os_platform', 'os_distribution', 'os_serial', 'os_version', 'os_version_service_pack',
                            'firewall_company_name', 'firewall_version', 
                            ]

def calc_hash_key(checkpoint_handler, all_endpoint_info):
    """
    Calculate a hash value of endpoint information where plugin_name and multible occurences of attribute values are ignored.
    """
    try:
        all_endpoint_info.sort()
        hash_key_attributes = {}
        for (plugin_name, attribute_name, attribute_value) in all_endpoint_info:
            if attribute_name is not None:
                attribute_name  = lib.encode.safe_decode(attribute_name)
            if attribute_value is not None:
                attribute_value = lib.encode.safe_decode(attribute_value)
            if attribute_name in CALC_HASH_KEY_WHITE_LIST:
                if hash_key_attributes.has_key(attribute_name):
                    if not attribute_value in hash_key_attributes[attribute_name]:
                        hash_key_attributes[attribute_name].append(attribute_value)
                else:
                    hash_key_attributes[attribute_name] = [attribute_value]
        hash_key_data = u''
        for attribute_name in hash_key_attributes.keys():
            hash_key_attributes[attribute_name].sort()
            for attribute_value in hash_key_attributes[attribute_name]:
                if attribute_name is not None:
                    hash_key_data += lib.encode.safe_decode(attribute_name)
                if attribute_value is not None:
                    hash_key_data += lib.encode.safe_decode(attribute_value)
        hash_key = hashlib.sha1(hash_key_data.encode('utf8'))
        return hash_key.hexdigest()
    except:
        (etype, evalue, etrace) = sys.exc_info()
        checkpoint_handler.CheckpointException("calc_hash_key", 'endpoint', lib.checkpoint.ERROR, etype, evalue, etrace)
        checkpoint_handler.Checkpoint("calc_hash_key", 'endpoint', lib.checkpoint.ERROR, all_endpoint_info=repr(all_endpoint_info))
        hash_key_data = '%s' % uuid.uuid1()
        return hash_key_data.encode('utf8')
     
def create_attributes(dbt, endpoint_info_id, all_endpoint_info):
    for (plugin_name, attribute_name, attribute_value) in all_endpoint_info:
        endpoint_attribute = EndpointAttribute()
        endpoint_attribute.endpoint_info_id = endpoint_info_id
        endpoint_attribute.plugin_name = plugin_name
        endpoint_attribute.attribute_name = attribute_name
        endpoint_attribute.attribute_value = lib.encode.safe_decode(attribute_value)
        dbt.add(endpoint_attribute)


def get_failed_enrollment(plugin_name, token_serial, dbs):
    return dbs.select_first(FailedEnrollment, 
                            filter=database_api.and_(FailedEnrollment.token_serial==token_serial,
                                                     FailedEnrollment.plugin_name==plugin_name,
                                                    )
                            )
    
def log_failed_enrollment(plugin_name, token_serial, unique_session_id, error_message):
    with database_api.Transaction() as dbt:
        failed_enrollment = get_failed_enrollment(plugin_name, token_serial, dbt)
        if not failed_enrollment:
            failed_enrollment = dbt.add(FailedEnrollment())
            failed_enrollment.token_serial = token_serial
            failed_enrollment.plugin_name = plugin_name
        failed_enrollment.unique_session_id = unique_session_id
        failed_enrollment.error_message = error_message
        failed_enrollment.timestamp = datetime.datetime.today()
    
def clear_failed_enrollment(plugin_name, token_serial, dbt):
    failed_enrollment = get_failed_enrollment(plugin_name, token_serial, dbt)
    if failed_enrollment:
        dbt.delete(failed_enrollment)
    
    
    
def prune_data(transaction, date_before, checkpoint_handler, progress):
    with checkpoint_handler.CheckpointScope("prune_data", "endpoint", lib.checkpoint.DEBUG, date_before=date_before):
        old_endpoint_ids = transaction.select_expression(EndpointInfo.id, EndpointInfo.last_seen < date_before)
        progress.update_prune_status('Delete seen device attributes', 0)
        delete_endpoint_attributes = transaction.delete_direct(EndpointAttribute, database_api.in_(EndpointAttribute.endpoint_info_id, old_endpoint_ids))
        checkpoint_handler.Checkpoint("delete_endpoint_attributes", "endpoint", lib.checkpoint.DEBUG, n=delete_endpoint_attributes)
        
        old_endpoint_hash_keys = transaction.select_expression(EndpointInfo.hash_key, EndpointInfo.last_seen < date_before)
        progress.update_prune_status('Delete device access log', 40)
        delete_endpoint_access = transaction.delete_direct(EndpointAccessLog, database_api.in_(EndpointAccessLog.hash_key, old_endpoint_hash_keys))
        checkpoint_handler.Checkpoint("delete_endpoint_access", "endpoint", lib.checkpoint.DEBUG, n=delete_endpoint_access)

        progress.update_prune_status('Delete device info', 80)
        delete_endpoint_info = transaction.delete_direct(EndpointInfo, EndpointInfo.last_seen < date_before)
        checkpoint_handler.Checkpoint("delete_endpoint_info", "endpoint", lib.checkpoint.DEBUG, n=delete_endpoint_info)
            
